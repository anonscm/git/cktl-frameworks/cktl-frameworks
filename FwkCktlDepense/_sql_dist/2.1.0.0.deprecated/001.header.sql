SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.0 BETA
-- Date de publication : 06/09/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Modifications pour intégration de l'extourne
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2055';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/

create or replace procedure grhum.drop_constraint (ownerconstraint varchar2, tableconstraint varchar2, nameconstraint varchar2)
is
    -- Suppression d'une contrainte si elle existe
    
   flag   smallint;
begin
   flag := 0;

   select count (constraint_name)
   into   flag
   from   all_constraints
   where  upper (owner) = upper (ownerconstraint) and upper (table_name) = upper (tableconstraint) and upper (constraint_name) = upper (nameconstraint);

   if flag > 0 then
      execute immediate 'ALTER TABLE ' || ownerconstraint || '.' || tableconstraint || ' DROP CONSTRAINT ' || nameconstraint;
   end if;
end;
/

