
create or replace procedure grhum.inst_patch_jefy_depense_2100 is
begin
   jefy_admin.api_application.creerfonction (250, 'DELIQEXT', 'Depense', null, 'Liquider avec mode de paiement "A extourner"', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (251, 'LIQLIEXT', 'Depense', null, 'Liquider à partir d''une liquidation d''extourne', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (252, 'LIQENEXT', 'Depense', null, 'Liquider à partir d''une enveloppe d'' extourne', 'N', 'O', 3);
   jefy_admin.api_application.creerfonction (253, 'LIQSENG', 'Depense', null, 'Liquider sans engagement', 'N', 'O', 3);

	commit;
    INSERT INTO JEFY_ADMIN.TYPE_ETAT (TYET_ID, TYET_LIBELLE) select 220, 'sur extourne'  from dual where not exists (select * from jefy_admin.type_etat where tyet_id=220);
    INSERT INTO JEFY_ADMIN.TYPE_ETAT (TYET_ID, TYET_LIBELLE) select  221, 'sur budget exercice' from dual  where not exists (select * from jefy_admin.type_etat where tyet_id=221);
	commit;
	
    insert into jefy_depense.db_version values (2100,'2100',sysdate,sysdate,null);           
end;

