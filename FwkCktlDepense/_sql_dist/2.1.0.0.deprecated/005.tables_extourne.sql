
-- suppression "if exists". Permet de passer le script plusieurs fois
execute grhum.drop_object('jefy_depense','extourne_liq_repart','table');
execute grhum.drop_object('jefy_depense','extourne_liq_def','table');
execute grhum.drop_object('jefy_depense','extourne_liq','table');

execute grhum.drop_object('jefy_depense','extourne_liq_seq','sequence');
execute grhum.drop_object('jefy_depense','extourne_liq_def_seq','sequence');
execute grhum.drop_object('jefy_depense','extourne_liq_repart_seq','sequence');



create   sequence jefy_depense.extourne_liq_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
create sequence jefy_depense.extourne_liq_def_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
create sequence jefy_depense.extourne_liq_repart_seq
  start with 1
  maxvalue 9999999999999999999999999999
  minvalue 1
  nocycle
  nocache
  noorder;
-----------

create table jefy_depense.extourne_liq (
  el_id            number not null,
  dep_id_n         number null,
  dep_id_n1        number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq is 'Memorise une liquidation d''extourne sur N+1 et sa liquidation initiale correspondante sur N';
comment on column jefy_depense.extourne_liq.dep_id_n is 'Reference a la liquidation initiale sur N (null dans le cas de demarrage d''un exercice)';
comment on column jefy_depense.extourne_liq.dep_id_n1 is 'Reference a la liquidation d''extourne sur N+1';

alter table jefy_depense.extourne_liq add (primary key (el_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq add (constraint fk_extourne_liq_depidn  foreign key (dep_id_n)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq add (constraint fk_extourne_liq_depidn1  foreign key (dep_id_n1)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq add (constraint uk_extourne_liq  unique (dep_id_n, dep_id_n1) using index tablespace gfc_indx);

-----------

create table jefy_depense.extourne_liq_def (
  eld_id            number not null,
  dep_id        number not null,
  tyet_id           number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq_def is 'Memorise une liquidation définitive avec flag pour savoir si c’est une liquidation sur extourne ou sur budget';
comment on column jefy_depense.extourne_liq_def.dep_id is 'Reference a la liquidation definitive';
COMMENT ON COLUMN JEFY_DEPENSE.EXTOURNE_LIQ_DEF.TYET_ID IS 'Reference a la constante indiquant si liquidation sur extourne (220) ou sur budget (221)';



alter table jefy_depense.extourne_liq_def add (primary key (eld_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq_def add (constraint fk_extourne_liq_def_depid  foreign key (dep_id)  references jefy_depense.depense_budget (dep_id));
alter table jefy_depense.extourne_liq_def add (constraint fk_extourne_liq_def_tyetid  foreign key (tyet_id)  references jefy_admin.type_etat (tyet_id));
alter table jefy_depense.extourne_liq_def add (constraint uk_extourne_liq_def  unique (dep_id) using index tablespace gfc_indx);
alter table jefy_depense.extourne_liq_def add (constraint chk_extourne_liq_tyet_id  check (tyet_id in (220,221)));



-----------

create table jefy_depense.extourne_liq_repart (
  elr_id        number not null,
  el_id            number not null,
  eld_id        number not null
)
tablespace gfc;

comment on table jefy_depense.extourne_liq_repart is 'Lien entre la liquidation definitive et les liquidations d’extourne correspondantes. Si liquidation definitive sur poche, pas d’enregistrement';
comment on column jefy_depense.extourne_liq_repart.el_id is 'Reference a la liquidation d''extourne';
comment on column jefy_depense.extourne_liq_repart.eld_id is 'Reference a la liquidation definitive';

alter table jefy_depense.extourne_liq_repart add (primary key (elr_id) using index tablespace gfc_indx );
alter table jefy_depense.extourne_liq_repart add (constraint fk_extourne_liq_repart_elid  foreign key (el_id)  references jefy_depense.extourne_liq (el_id) deferrable initially deferred  );
alter table jefy_depense.extourne_liq_repart add (constraint fk_extourne_liq_repart_eldid  foreign key (eld_id)  references jefy_depense.extourne_liq_def (eld_id) deferrable initially deferred);
alter table jefy_depense.extourne_liq_repart add (constraint uk_extourne_liq_repart  unique (el_id,eld_id) using index tablespace gfc_indx);
