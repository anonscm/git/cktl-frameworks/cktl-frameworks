CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
     newCommRef             varchar2(500);
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI';
                      -- Ajout du vlco_id max --
                      --AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                        --    WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;
         
         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then
          
            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
              newCommRef := my_org_ub||'/'||my_org_cr;
              if (length(newCommRef)>=44) then
                newCommRef := substr(newCommRef,1,44);
              end if;
              newCommRef := newCommRef ||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF');
              
              
            update commande set comm_reference=newCommRef
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE corriger_engage_extourne (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      my_nb integer;
   begin
      my_nb:=0;
   end;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id; 
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action 
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


   /****************************************************************************/
   PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
        AND eana_montant_budgetaire_reste>0;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id /*AND eana_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eana_reste>my_reste+my_dana_bud then
           my_somme:=my_eana_reste-my_reste-my_dana_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE 
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_can_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE
              WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id  AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id /*AND econ_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE 
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;
              
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE 
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO 
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;
        
        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decal� un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;
           
           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;
            
           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then 
                 my_pourcentage:=0; 
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/
