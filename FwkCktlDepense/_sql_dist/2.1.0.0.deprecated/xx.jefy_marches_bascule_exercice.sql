CREATE OR REPLACE PACKAGE JEFY_MARCHES.Bascule_Marches IS
   

PROCEDURE bascule_exercice(exeOrdre NUMBER);


   PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   ) ;
   
   PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   ) ;
 
   PROCEDURE bascule_3CMP(
       exeOrdre NUMBER
   )   ;
   
   PROCEDURE bascule_monopole(
       exeOrdre NUMBER
   )   ;
END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_MARCHES.Bascule_Marches IS



PROCEDURE bascule_exercice(exeOrdre NUMBER)   
IS
begin

    bascule_code_exer(exeOrdre);
    bascule_mapace(exeOrdre);
    bascule_3cmp(exeOrdre);
    bascule_monopole(exeOrdre);

end;



 PROCEDURE bascule_code_exer(
    exeOrdre NUMBER
   )   IS


cursor c1
is select ce.* from code_exer ce , code_marche cm
    where ce.exe_ordre = exeordre and ce.cm_ordre = cm.cm_ordre and cm.cm_niveau > 1 and ce_pere is not null and ce_suppr = 'N';

   current_code_exer code_exer%ROWTYPE;
   current_code_marche code_marche%ROWTYPE;
   ceordrepere integer;
   cpt integer;

   BEGIN

     -- As t on deja passe cette procedure ?    
     select count(*) into cpt from jefy_marches.code_exer where exe_ordre=exeOrdre;
     if(cpt>0) THEN
            RAISE_APPLICATION_ERROR (-20001,'La bascule des codes nomenclatures a deja ete faite');
     end if;
 

    -- Insertion des codes marches pour l'annee exeordre a partir de exeordre - 1 
    insert into jefy_marches.code_exer (
        select ce_pere, ce_3cmp, ce_autres, ce_controle, ce_monopole,
            code_exer_seq.nextval, ce_rech, ce_suppr, ce_type, cm_ordre, exeOrdre, 
            tcn_id 
        from jefy_marches.code_exer 
        where exe_ordre = (exeOrdre-1)
        );
    
    
      open C1;
        loop
        fetch C1 into current_code_exer;
        exit when C1%notfound;
        
            select * into current_code_marche from code_marche where cm_ordre = current_code_exer.cm_ordre;
                      
            if (current_code_marche.cm_pere is null)
            then
                raise_application_error(-20001, 'Pas de code MARCHE pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE : '||current_code_marche.cm_ordre||')');
            end if;
            

            select count(*) into cpt from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';
            if (cpt = 0)
            then
                raise_application_error(-20001, 'Pas de code EXER pere defini pour le code ' ||current_code_marche.cm_code|| ' , CM_ORDRE_PERE : '||current_code_marche.cm_pere||')');
            end if;

            if (cpt > 1)
            then
            
                raise_application_error(-20001, 'Plusieurs occurences dans CODE_EXER pour le cm_ordre '||current_code_marche.cm_pere||' !');
            
            end if;

            -- 
            select ce_ordre into ceordrepere from code_exer where exe_ordre = exeordre and cm_ordre = current_code_marche.cm_pere and ce_suppr = 'N';


            update code_exer set ce_pere = ceordrepere where ce_ordre = current_code_exer.ce_ordre;

        end loop;
        close C1;
       
    
    
 
   END;

 
 PROCEDURE bascule_mapace(
    exeOrdre NUMBER
   )   IS
   
       CURSOR C1 is select m.* from mapa_ce m where ce_ordre in (select ce_ordre from code_exer where exe_ordre = (exeOrdre-1) and ce_suppr ='N');
       CURSOR C2 is select m.* from mapa_tranche_exer m where exe_ordre = (exeOrdre-1);
    cmOrdre NUMBER;
    ceOrdreBascul NUMBER;
    mce mapa_ce%rowtype;
    mte mapa_tranche_exer%rowtype;
    cnt Integer;
    mteId Integer;
 
 BEGIN
   
  select count(*) into cnt from jefy_marches.code_exer where exe_ordre=exeOrdre;
  if(cnt=0) THEN
         RAISE_APPLICATION_ERROR (-20001,'Veuillez commencer par basculer les codes nomenclatures');
  end if;

  select count(*) into cnt from jefy_marches.MAPA_CE where ce_ordre in (select ce_ordre from jefy_marches.code_exer where exe_ordre=exeOrdre);
  if(cnt>0) THEN
         RAISE_APPLICATION_ERROR (-20001,'La bascule des seuils MaPA a deja ete faite');
  end if;

  
  open C1;
    loop
      fetch C1 into mce;
      exit when C1%notfound;
   
   select cm_ordre into cmOrdre from code_exer where ce_ordre=mce.ce_ordre;
   select ce_ordre into ceOrdreBascul from code_exer where exe_ordre=exeOrdre and cm_ordre=cmOrdre;
   
   if(ceOrdreBascul IS NULL) THEN
          RAISE_APPLICATION_ERROR (-20001,'Un code nomenclature n a pas ete bascule sur le nouvel exercice');
   end if;
   
   INSERT INTO jefy_marches.MAPA_CE values (mapa_ce_seq.nextval,ceOrdreBascul,mce.MTE_ID, SYSDATE, mce.UTL_ORDRE, mce.MC_SUPP);
   
  end loop;
    close C1;
    
  open C2;
    loop
      fetch C2 into mte;
      exit when C2%notfound;
  
      select mapa_tranche_exer_seq.nextval into mteId from dual;
      -- INSERTING into MAPA_TRANCHE_EXER
      Insert into MAPA_TRANCHE_EXER (MTE_ID,TCN_ID,DATE_VOTE_CA,EXE_ORDRE,MTE_LIBELLE,MTE_MAX,MTE_MIN,UTL_ORDRE,DATE_MODIFICATION,MOD_ID) values (mteId,mte.TCN_ID,mte.DATE_VOTE_CA,exeOrdre,mte.MTE_LIBELLE,mte.MTE_MAX,mte.MTE_MIN,mte.UTL_ORDRE,SYSDATE,mte.MOD_ID);
      update mapa_ce set mte_id=mteId where mte_id=mte.mte_id and ce_ordre in (select ce_ordre from code_exer where exe_ordre=exeOrdre);
    end loop;
  close C2;
  
  commit;
 END;

 PROCEDURE bascule_3CMP(
    exeOrdre NUMBER
   )   IS
   
       CURSOR C1 is select m.* from code_marche_four m where m.exe_ordre=(exeOrdre-1) and cmf_pere is null and
        m.cm_ordre in (select cm_ordre from code_exer where ce_3cmp=1 and exe_ordre=exeOrdre)
     and (m.cm_ordre,m.fou_ordre) not in (select cm_ordre,fou_ordre from code_marche_four where exe_ordre=exeOrdre)
      order by m.cmf_ordre;
      
      CURSOR C2 is select m.* from code_marche_four m where m.exe_ordre=(exeOrdre-1) and cmf_pere is not null and
        m.cm_ordre in (select cm_ordre from code_exer where ce_3cmp=1 and exe_ordre=exeOrdre)
     and (m.cm_ordre,m.fou_ordre) not in (select cm_ordre,fou_ordre from code_marche_four where exe_ordre=exeOrdre)
      order by m.cmf_ordre;
      
    cmf code_marche_four%rowtype;
      pere integer;
       code integer;
      nb integer;
      pereFou integer;
   
   BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     insert into code_marche_four values(exeOrdre,cmf.cm_ordre,code_marche_four_seq.nextval,'MIGRATION',null,cmf.cmf_suppr,cmf.fou_ordre);

  end loop;
  close C1;
  

  
  open C2;
  loop
   fetch C2 into cmf;
   exit when C2%notfound;

     -- le pere existe-t-il ?
     select count(*) into nb from code_marche_four where cmf_ordre=cmf.cmf_pere;
    if nb=1 then
        -- on recupere le cm_ordre du pere
          select cm_ordre  into code from code_marche_four where cmf_ordre=cmf.cmf_pere;
          select fou_ordre into pereFou from code_marche_four where cmf_ordre=cmf.cmf_pere;

       -- on regarde si le futur papa a ete cree sur 200X
          select count(*) into nb from code_marche_four where cm_ordre=code and fou_ordre= pereFou and exe_ordre=exeOrdre;
       if nb=1 then
           -- on recupere le future papa
         select cmf_ordre into pere from code_marche_four where cm_ordre=code and fou_ordre= pereFou and exe_ordre=exeOrdre;
       -- et hop on accouche l'enfant
            insert into code_marche_four values(exeOrdre,cmf.cm_ordre,code_marche_four_seq.nextval,'MIGRATION',pere,cmf.cmf_suppr,cmf.fou_ordre);
          end if;
     end if;
  end loop;
  close C2;

  

  commit;
   
  END;
   
 PROCEDURE bascule_monopole(
    exeOrdre NUMBER
   )   IS
   
       CURSOR C1 is select m.* from code_marche_four m where m.exe_ordre=(exeOrdre-1) and cmf_pere is null and
        m.cm_ordre in (select cm_ordre from code_exer where ce_monopole=1 and exe_ordre=exeOrdre)
     and (m.cm_ordre,m.fou_ordre) not in (select cm_ordre,fou_ordre from code_marche_four where exe_ordre=exeOrdre)
      order by m.cmf_ordre;
      
      CURSOR C2 is select m.* from code_marche_four m where m.exe_ordre=(exeOrdre-1) and cmf_pere is not null and
        m.cm_ordre in (select cm_ordre from code_exer where ce_monopole=1 and exe_ordre=exeOrdre)
     and (m.cm_ordre,m.fou_ordre) not in (select cm_ordre,fou_ordre from code_marche_four where exe_ordre=exeOrdre)
      order by m.cmf_ordre;
      
    cmf code_marche_four%rowtype;
      pere integer;
       code integer;
      nb integer;
      pereFou integer;
   
   BEGIN
   
  open C1;
  loop
   fetch C1 into cmf;
   exit when C1%notfound;

     insert into code_marche_four values(exeOrdre,cmf.cm_ordre,code_marche_four_seq.nextval,'MIGRATION',null,cmf.cmf_suppr,cmf.fou_ordre);

  end loop;
  close C1;
  
  
  open C2;
  loop
   fetch C2 into cmf;
   exit when C2%notfound;

     -- le pere existe-t-il ?
     select count(*) into nb from code_marche_four where cmf_ordre=cmf.cmf_pere;
    if nb=1 then
        -- on recupere le cm_ordre du pere
          select cm_ordre  into code from code_marche_four where cmf_ordre=cmf.cmf_pere;
          select fou_ordre into pereFou from code_marche_four where cmf_ordre=cmf.cmf_pere;

       -- on regarde si le futur papa a ete cree sur 200X
          select count(*) into nb from code_marche_four where cm_ordre=code and fou_ordre= pereFou and exe_ordre=exeOrdre;
       if nb=1 then
           -- on recupere le future papa
         select cmf_ordre into pere from code_marche_four where cm_ordre=code and fou_ordre= pereFou and exe_ordre=exeOrdre;
       -- et hop on accouche l'enfant
            insert into code_marche_four values(exeOrdre,cmf.cm_ordre,code_marche_four_seq.nextval,'MIGRATION',pere,cmf.cmf_suppr,cmf.fou_ordre);
          end if;
     end if;
  end loop;
  close C2;
  commit;
  
  end;

END;
/


