SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.4.2
-- Date de publication :  11/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Ajout d'une procedure de verification de la possibilite de la reimputation comptable apres mandatement (exploitee par Maracuja)
-- Modification des vues implémentant des RIBS
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

CREATE OR REPLACE FORCE VIEW jefy_depense.v_rib_fournisseur (rib_ordre,
                                                             fou_ordre,
                                                             c_banque,
                                                             c_guichet,
                                                             no_compte,
                                                             cle_rib,
                                                             rib_titco,
                                                             mod_code,
                                                             rib_valide,
                                                             d_creation,
                                                             d_modification,
                                                             tem_paye_util,
                                                             iban,
                                                             bic,
                                                             domiciliation,
                                                             PERS_ID_CREATION,
                                                             PERS_ID_MODIFICATION
                                                            )
AS
   SELECT rib_ordre, fou_ordre, b.c_banque, b.c_guichet, no_compte,
     cle_rib,rib_titco,mod_code,rib_valide,r.d_creation,r.d_modification,
     tem_paye_util,iban,b.bic, b.DOMICILIATION,r.PERS_ID_CREATION, r.PERS_ID_MODIFICATION
     FROM grhum.ribfour_ulr r, grhum.banque b where r.banq_ordre=b.banq_ordre;
/

CREATE OR REPLACE FORCE VIEW jefy_depense.v_attribution (att_date,
                                                         att_ordre,
                                                         att_suppr,
                                                         att_valide,
                                                         fou_ordre,
                                                         lot_ordre,
                                                         att_debut,
                                                         att_fin,
                                                         att_type_controle,
                                                         att_acceptee,
                                                         utl_ordre,
                                                         att_ht,
                                                         tit_ordre,
                                                         att_index, 
                                                         rib_ordre
                                                        )
AS
   SELECT "ATT_DATE", "ATT_ORDRE", "ATT_SUPPR", "ATT_VALIDE", "FOU_ORDRE",
          "LOT_ORDRE", "ATT_DEBUT", "ATT_FIN", "ATT_TYPE_CONTROLE",
          "ATT_ACCEPTEE", "UTL_ORDRE", "ATT_HT", "TIT_ORDRE", "ATT_INDEX", rib_ordre
     FROM jefy_marches.attribution;
/

CREATE OR REPLACE FORCE VIEW jefy_depense.v_ctrl_seuil_mapa (exe_ordre,
                                                             ce_ordre,
                                                             cm_code,
                                                             cm_lib,
                                                             montant_ht,
                                                             seuil_min,
                                                             cm_code_fam,
                                                             cm_lib_fam
                                                            )
AS
   SELECT   exe_ordre, ce_ordre, cm_code, cm_lib,
            SUM (montant_ht) AS montant_ht,
            DECODE (SUM (seuil_min), 0, 4000, SUM (seuil_min)), cm_code_fam,
            cm_lib_fam
       FROM (SELECT ce.exe_ordre AS exe_ordre, dch.ce_ordre AS ce_ordre,
                    cm.cm_code AS cm_code, cm.cm_lib AS cm_lib,
                    dch.dhom_ht_saisie AS montant_ht, 0 AS seuil_min,
                    cm0.cm_code cm_code_fam, cm0.cm_lib cm_lib_fam
               FROM jefy_depense.depense_ctrl_hors_marche dch,
                    jefy_marches.code_exer ce,
                    jefy_marches.code_marche cm,
                    jefy_marches.code_marche cm0
              WHERE dch.typa_id = 1
                AND ce.ce_ordre = dch.ce_ordre
                AND cm.cm_ordre = ce.cm_ordre
                AND cm.cm_niveau = 2
                AND cm.cm_pere = cm0.cm_ordre
             UNION ALL
             SELECT ce.exe_ordre AS exe_ordre, ech.ce_ordre AS ce_ordre,
                    cm.cm_code AS cm_code, cm.cm_lib AS cm_lib,
                    ech.ehom_ht_reste AS montant_ht, 0 AS seuil_min,
                    cm0.cm_code cm_code_fam, cm0.cm_lib cm_lib_fam
               FROM jefy_depense.engage_ctrl_hors_marche ech,
                    jefy_marches.code_exer ce,
                    jefy_marches.code_marche cm,
                    jefy_marches.code_marche cm0
              WHERE ech.typa_id = 1
                AND ce.ce_ordre = ech.ce_ordre
                AND cm.cm_ordre = ce.cm_ordre
                AND cm.cm_niveau = 2
                AND cm.cm_pere = cm0.cm_ordre
             UNION ALL
             SELECT ce.exe_ordre AS exe_ordre, ce.ce_ordre AS ce_ordre,
                    cm.cm_code AS cm_code, cm.cm_lib AS cm_lib,
                    0 AS montant_ht, 0 AS seuil_min, cm0.cm_code cm_code_fam,
                    cm0.cm_lib cm_lib_fam
               FROM jefy_marches.code_exer ce,
                    jefy_marches.code_marche cm,
                    jefy_marches.code_marche cm0
              WHERE cm.cm_ordre = ce.cm_ordre
                AND cm.cm_niveau = 2
                AND ce.exe_ordre > 2005
                AND cm.cm_pere = cm0.cm_ordre
             UNION ALL
             SELECT   ce.exe_ordre, mce.ce_ordre, cm.cm_code AS cm_code,
                      cm.cm_lib AS cm_lib, 0, MIN (mte.mte_min),
                      cm0.cm_code cm_code_fam, cm0.cm_lib cm_lib_fam
                 FROM jefy_marches.code_exer ce,
                      jefy_marches.mapa_ce mce,
                      jefy_marches.mapa_tranche_exer mte,
                      jefy_marches.code_marche cm,
                      jefy_marches.code_marche cm0
                WHERE cm.cm_ordre = ce.cm_ordre
                  AND mce.ce_ordre = ce.ce_ordre
                  AND mce.mte_id = mte.mte_id
                  AND cm.cm_pere = cm0.cm_ordre
             GROUP BY ce.exe_ordre,
                      mce.ce_ordre,
                      cm.cm_code,
                      cm.cm_lib,
                      cm0.cm_code,
                      cm0.cm_lib)
   GROUP BY exe_ordre, ce_ordre, cm_code, cm_lib, cm_code_fam, cm_lib_fam;
/


CREATE OR REPLACE PACKAGE JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2);

PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type);

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type);
      
FUNCTION subdivise_engage_pour_depense(
      a_dep_id depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type)
return number;

FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type)
return number;

PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type);

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type)
return number;
      
PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type);

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type);
      
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type);

PROCEDURE rempli_reimputation(
      a_dep_id    depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre depense_budget.utl_ordre%type);

PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type);

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type);
      
procedure check_reimp_maracuja_possible(
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type);      
      
      
END;
/


GRANT EXECUTE ON JEFY_DEPENSE.REIMPUTER TO JEFY_MISSION;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2
   ) IS 
   BEGIN
      depense_avec_infos(a_dep_id, a_reim_libelle, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_pco_num, a_chaine_action,
         a_chaine_analytique, a_chaine_convention, a_chaine_hors_marche, a_chaine_marche, 3 /*OUI*/, null);
   END;
   
PROCEDURE depense_avec_infos (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type, /* 3:OUI, 4:NON */
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_depense       depense_budget%rowtype;
      my_engage        engage_budget%rowtype;
      my_nb            integer;
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id;
     select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
     
     -- si la depense a des reversements ou est un reversement, cas non pris en charge pour le moment
     select count(*) into my_nb from depense_budget where dep_id_reversement=a_dep_id;
     if my_nb>0 or my_depense.dep_id_reversement is not null then
        raise_application_error(-20001, 'la depense a des reversements, ce cas n''est pas encore traite'); 
     end if;
     
     if a_tyet_id_reimp_ordo<>4 /*4:NON*/ then
        rempli_reimputation(a_dep_id, a_reim_libelle, a_utl_ordre);
     end if;
   
     if a_org_id is not null and my_engage.org_id<>a_org_id then
       depense_organ(a_dep_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_chaine_analytique, a_chaine_convention, a_eng_id_new);
     else
       if a_chaine_analytique is not null then 
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
       end if;
       if a_chaine_convention is not null then 
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
       end if;
       if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
       end if;
       if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
       end if;
     end if;
     
     if a_chaine_action is not null then 
       depense_action(a_dep_id, a_chaine_action, a_utl_ordre);
     end if;

     if a_chaine_hors_marche is not null then 
       depense_hors_marche(a_dep_id, a_chaine_hors_marche, a_utl_ordre);
     end if;

     if a_chaine_marche is not null then 
       depense_marche(a_dep_id, a_chaine_marche, a_utl_ordre);
     end if;

     if a_pco_num is not null then 
       depense_planco(a_dep_id, a_pco_num, a_utl_ordre, a_tyet_id_reimp_ordo);
     end if;
   END;
   
PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_eng_id_new            engage_budget.eng_id%type
   ) IS
      my_nb        integer;
      my_eng_id    engage_budget.eng_id%type;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_tap_id    engage_budget.tap_id%type;
   BEGIN
--- changement de la ligne budgetaire
--    * depense sans inventaire : si la depense est mandatee, on choisit une ligne budgetaire de la meme UB, sinon toutes les lignes, dans la limite que le disponible le permette.
--    * depense avec inventaire : suivant un parametre a mettre en place, meme limitation que la depense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
--    * le changement de ligne peut impliquer la modification des conventions et des codes analytiques eventuellement associes a cette depense    cf. changement convention
--    * le changement de ligne peut impliquer une creation d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs lies a celle qu'on re-impute) par exemple
--    * si la depense re-imputee possede un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..   

      select * into my_depense from depense_budget where dep_id=a_dep_id; 
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;

      -- si la ligne budgetaire a change
      if my_engage.org_id<>a_org_id then
        my_tcd_ordre:=a_tcd_ordre;
        if my_tcd_ordre is null then my_tcd_ordre:=my_engage.tcd_ordre; end if;

        my_tap_id:=a_tap_id;
        if my_tap_id is null then my_tap_id:=my_depense.tap_id; end if;
        
        verifier.verifier_budget(my_depense.exe_ordre, my_tap_id, a_org_id, my_tcd_ordre);
        verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, a_org_id);

        -- si avec inventaire -> CR
        /* select count(*) into my_nb from v_inventaire where dpco_id in (select dpco_id from depense_ctrl_planco where dep_id in (a_dep_id));
        if my_nb>0 then
          select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
            and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub and dest.org_cr=orig.org_cr;
          if my_nb=0 then
            raise_application_error(-20001, 'cette depense comporte des inventaires, on ne peut reimputer que dans le meme CR');
          end if;
        else */
          -- la depense est mandatee -> restriction de la nouvelle ligne a la meme UB que l'ancienne
          select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
          if my_nb>0 then
            select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
              and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub;
            if my_nb=0 then
              raise_application_error(-20001, 'cette depense est mandatee, on ne peut reimputer que dans la meme UB');
            end if;
          end if;
        --end if;
        
        -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
        my_eng_id:=subdivise_engage_pour_depense(a_dep_id, a_org_id, my_tcd_ordre, my_tap_id, a_utl_ordre);
        
      end if;
      
      if a_chaine_analytique is not null then
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
      end if;
      
      if a_chaine_convention is not null then
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
      end if;

      if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
      end if;

      if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
      end if;
   END;

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
   BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer le type de credit de la depense');
   END;

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_eng_id    engage_budget.eng_id%type;
      my_par_value parametre.par_value%type;
      
      my_dep_id    depense_budget.dep_id%type;
      cursor orvs is select dep_id from depense_budget where dep_id_reversement=a_dep_id;
   BEGIN
      select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
      if my_nb>0 then
        select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and dpco_tva_saisie<>0 and man_id is not null;
        if my_nb>0 then
           RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier le taux de prorata d''une depense mandatee');
        end if;
      end if;
      
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      my_eng_id:=my_engage.eng_id;

      verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, my_engage.org_id);

      -- on ne peut reimputer que les depenses pas les ORvs pour le moment
        -- les orvs rattaches a cette depense seront mis a jour avec les memes infos
      if my_depense.dep_ttc_saisie<0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas reimputer un ORV, il faut reimputer la depense d''origine');
      end if;
            
      -- on verifie si le nouveau taux de prorata est autorise pour cette ligne budgetaire --
      verifier.verifier_budget(my_depense.exe_ordre, a_tap_id, my_engage.org_id, my_engage.tcd_ordre);
      
      select par_value into my_par_value from parametre where par_key='DEPENSE_IDEM_TAP_ID' and exe_ordre=my_depense.exe_ordre;
      if my_par_value='OUI' then
      
         -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
         my_eng_id:=subdivise_engage_pour_depense(a_dep_id, my_engage.org_id, my_engage.tcd_ordre, a_tap_id, a_utl_ordre);
         
      end if;
      
      update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=a_dep_id;
      corrige_depense_budgetaire(a_dep_id);
      
      open orvs();
      loop
         fetch  orvs into my_dep_id;
         exit when orvs%notfound;
         
         update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=my_dep_id;
         corrige_depense_budgetaire(my_dep_id);
      end loop;
      close orvs;
      
      budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
      
   END;

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_action where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_action(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_analytique where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_analytique(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_convention where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_convention(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est rattachee a une attribution on ne peut pas la transformer en hors marche');
       end if;

       select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
       delete from depense_ctrl_hors_marche where dep_id=a_dep_id;
       liquider.ins_depense_ctrl_hors_marche(my_exe_ordre, a_dep_id, a_chaine);
       Verifier.verifier_depense_coherence(a_dep_id);
     end if;     
   END;

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_att_ordre depense_ctrl_marche.att_ordre%type;
      my_old_att_ordre depense_ctrl_marche.att_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est hors marche on ne peut pas la transformer en marche');
       end if;

       SELECT grhum.en_nombre(SUBSTR(a_chaine,1,INSTR(a_chaine,'$')-1)) INTO my_att_ordre FROM dual;
       select nvl(att_ordre,0) into my_old_att_ordre from depense_ctrl_marche where dep_id=a_dep_id;
       
       if my_old_att_ordre<>my_att_ordre then 
          RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer l''attribution de la depense');
       end if;
     end if;
   END;

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_tyet_id_reimp_ordo    jefy_admin.type_etat.tyet_id%type
  ) is
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_org_id    engage_budget.org_id%type;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_pco_num   depense_ctrl_planco.pco_num%type;
      my_dep_id_reversement depense_budget.dep_id_reversement%type;
  begin 
      select nvl(pco_num,'0') into my_pco_num from depense_ctrl_planco where dep_id=a_dep_id;
      if my_pco_num<>a_pco_num then
         select dep_id_reversement into my_dep_id_reversement from depense_budget where dep_id=a_dep_id;
         if my_dep_id_reversement is not null then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''un ordre de reversement');
         end if;
       
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 and a_tyet_id_reimp_ordo=3 /*3:OUI*/ then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         
         reimputation_inventaire(a_dep_id, a_pco_num, null);
 
         update depense_ctrl_planco set pco_num=a_pco_num where dep_id=a_dep_id;
      end if;

      select d.exe_ordre, e.org_id, e.tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre 
        from depense_budget d, engage_budget e where d.eng_id=e.eng_id and d.dep_id=a_dep_id;
      if a_utl_ordre is not null then 
         Verifier.verifier_planco(my_exe_ordre, my_org_id, my_tcd_ordre, a_pco_num, a_utl_ordre);
      end if;
      Verifier.verifier_depense_coherence(a_dep_id);
  end;

PROCEDURE reimputation_inventaire (
      a_dep_id                depense_ctrl_planco.dep_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_tap_id                depense_budget.tap_id%type)
   IS
      my_nb        integer;
      my_dpco_id   depense_ctrl_planco.dpco_id%type;
      my_exe_ordre depense_ctrl_planco.exe_ordre%type;
      my_pcoa_num  maracuja.plan_comptable_amo.pcoa_num%type;
      my_pcoa_id   maracuja.plan_comptable_amo.pcoa_id%type;
   BEGIN
         select dpco_id, exe_ordre into my_dpco_id, my_exe_ordre from depense_ctrl_planco where dep_id=a_dep_id;
         select count(*) into my_nb from jefy_inventaire.inventaire_comptable 
           where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id);
         
         if my_nb=0 then return; end if;

         if a_pco_num is not null then 
            select count(*) into my_nb from jefy_inventaire.inventaire_comptable where clic_id in (         
                 select clic_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id))
               and dpco_id<>my_dpco_id and invc_id not in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)
               and dpco_id is not null;
         
            if my_nb>0 then
               RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas effectuer cette reimputation car elle concerne des inventaires comptables relies a d''autres depenses');
            end if;
            
			select count(*) into my_nb from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
			if my_nb<>1 then
               RAISE_APPLICATION_ERROR(-20001, 'il y a un probleme de configuration pour le compte d''amortissement de cette imputation '||a_pco_num||' sur l''exercice '||my_exe_ordre);
			end if;

			select pcoa_id into my_pcoa_id from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
			
            update jefy_inventaire.cle_inventaire_comptable set pco_num=a_pco_num where clic_id in (         
                 select clic_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id));

				 
            select pcoa_num into my_pcoa_num from maracuja.plan_comptable_amo 
               where pcoa_id in (select pcoa_id from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre);
               
            update jefy_inventaire.amortissement set pco_num=my_pcoa_num where invc_id in (select invc_id from jefy_inventaire.inventaire_comptable 
                 where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)); 
         end if;
         
         if a_tap_id is not null then
            raise_application_error(-20001, 'changement de prorata en presence d''inventaire, cas pas encore pris en compte');
         end if;
   END;
FUNCTION subdivise_engage_pour_depense(
      a_dep_id       depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
      my_nb          integer;
      bool_creation  integer;
      my_comm_id     commande.comm_id%type;
      my_cbud_id     commande_budget.cbud_id%type;
      my_old_commande_budget commande_budget%rowtype;
      my_commande_budget commande_budget%rowtype;
      my_ht          engage_budget.eng_ht_saisie%type;
      my_tva         engage_budget.eng_tva_saisie%type;
      my_ttc         engage_budget.eng_ttc_saisie%type;
      my_bud         engage_budget.eng_montant_budgetaire%type;
      my_depense     depense_budget%rowtype;
      my_engage      engage_budget%rowtype;
      my_new_engage  engage_budget%rowtype;
      my_eng_id      engage_budget.eng_id%type;
   BEGIN
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      my_eng_id:=my_depense.eng_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      
      -- si il n'y a pas d'autre depense sur cette engagement on n'en cr¿e pas de nouveau 
      select count(*) into bool_creation from depense_budget where eng_id=my_depense.eng_id and dep_id<>a_dep_id and dep_ttc_saisie>0;
      
      -- on regarde si l'engagement appartient a une commande
      my_comm_id:=null;
      select count(*) into my_nb from commande_engagement where eng_id=my_depense.eng_id;
      if my_nb>0 then
         select min(comm_id) into my_comm_id from commande_engagement where eng_id=my_depense.eng_id;
      end if;
   
      if bool_creation=0 then
         -- on modifie juste l'engagement et enventuellement les infos de la commande
   
         if my_comm_id is not null then 
            select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
            select count(*) into my_nb from commande_budget 
              where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
            if my_nb>0 then  
               select min(cbud_id) into my_cbud_id from commande_budget 
                 where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
               update commande_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id where cbud_id=my_cbud_id;
            
               --si le taux de prorata a change, on met a jour les montants budgetaires  
               if my_engage.tap_id<>a_tap_id then
                  update commande_budget set cbud_montant_budgetaire=Budget.calculer_budgetaire(my_engage.exe_ordre,a_tap_id,a_org_id, cbud_ht_saisie, cbud_ttc_saisie)
                    where cbud_id=my_cbud_id;
                  corriger.corriger_commande_ctrl(my_cbud_id);
                  verifier.verifier_cde_budget_coherence(my_cbud_id);
               end if;
            end if;
         end if;
         
         -- on log l'ancien engagement
         engager.log_engage(my_eng_id, a_utl_ordre);
                  
         -- modif engagement
         update engage_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id, utl_ordre=a_utl_ordre where eng_id=my_eng_id;

         if my_engage.tap_id<>a_tap_id then
            -- modification des montants budgetaires   
            corrige_engage_budgetaire(my_eng_id);         
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre then 
            -- si on a chang¿ de ligne budgetaire on met a jour l'"ancien" budget
            budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre or my_engage.tap_id<>a_tap_id then
            -- si le taux de prorata ou la ligne budgetaire a change on met a jour le budget
            budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);
         end if;
         
      else

        my_nb:=0;
      
        -- si l'engagement appartient a une commande on regarde si un engagement avec les nouvelles infos existe deja 
        if my_comm_id is not null then
           select count(*) into my_nb from commande_engagement ce, engage_budget e
             where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
           if my_nb>0 then
             select min(e.eng_id) into my_eng_id from commande_engagement ce, engage_budget e
               where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
             
             -- on augmente les montants de l'engagement avec les montants de la depense
             augmenter_engage(a_dep_id, my_eng_id);
             
             -- on augmente la commande_bugdet correspondante (normalement elle existe)
             select * into my_new_engage from engage_budget where eng_id=my_eng_id;
             select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
             if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
                update commande_budget set cbud_montant_budgetaire=my_new_engage.eng_montant_budgetaire, cbud_ht_saisie=my_new_engage.eng_ht_saisie,
                   cbud_tva_saisie=my_new_engage.eng_tva_saisie, cbud_ttc_saisie=my_new_engage.eng_ttc_saisie where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
             end if;

           else
             -- on cree le nouvel engagement avec les infos de la depense
             my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);
             insert into commande_engagement select commande_engagement_seq.nextval, my_comm_id, my_eng_id from dual;
             
             -- on cree la commande_budget correspondante
             my_cbud_id:=creer_commande_budget(my_eng_id, my_comm_id);
           end if;
           
           -- on diminue l'ancienne commande_budget
           select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
           if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id 
                   and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
                   
                select * into my_new_engage from engage_budget where eng_id=my_eng_id;
                select * into my_commande_budget from commande_budget where cbud_id=my_cbud_id;
                
                my_ht :=my_commande_budget.cbud_ht_saisie -my_new_engage.eng_ht_saisie;
                if my_ht<0 then my_ht:=0; end if;
                my_ttc:=my_commande_budget.cbud_ttc_saisie-my_new_engage.eng_ttc_saisie;
                if my_ttc<0 then my_ttc:=0; end if;
                my_bud:=my_commande_budget.cbud_montant_budgetaire-my_new_engage.eng_montant_budgetaire;
                if my_bud<0 then my_bud:=0; end if;
                
				my_tva:=my_commande_budget.cbud_tva_saisie-my_new_engage.eng_tva_saisie;
                if my_tva<0 then
                   if my_ht+my_tva>0 then
                      my_ht:=my_ht+my_tva; 
                      my_bud:=Budget.calculer_budgetaire(my_commande_budget.exe_ordre,my_commande_budget.tap_id,my_commande_budget.org_id, my_ht, my_ttc);
                   else
                      my_ht:=my_ttc; 
                      my_bud:=my_ttc;
                   end if;
                   my_tva:=0;
                end if;

                update commande_budget set cbud_montant_budgetaire=my_bud, cbud_ht_saisie=my_ht,
                   cbud_tva_saisie=my_ttc-my_ht, cbud_ttc_saisie=my_ttc where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
           end if; 

        else
           my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);       
        end if;

        -- on diminue l'ancien engagement du montant de la depense a reimputer
        -- ATTENTION : on ne diminue pas le budgetaire reste car on enleve la liquidation de cet engagement, et donc le reste ne change pas 
        select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
        my_ht :=my_engage.eng_ht_saisie -my_depense.dep_ht_saisie;
        if my_ht<0 then my_ht:=0; end if;
        my_ttc:=my_engage.eng_ttc_saisie-my_depense.dep_ttc_saisie;
        if my_ttc<0 then my_ttc:=0; end if;
        my_bud:=my_engage.eng_montant_budgetaire-my_depense.dep_montant_budgetaire;
        if my_bud<0 then my_bud:=0; end if;
        if my_bud<my_engage.eng_montant_budgetaire_reste then my_bud:=my_engage.eng_montant_budgetaire_reste; end if;
  
        my_tva:=my_engage.eng_tva_saisie-my_depense.dep_tva_saisie;
        if my_tva<0 then
           if my_ht+my_tva>0 then
              my_ht:=my_ht+my_tva; 
              my_bud:=Budget.calculer_budgetaire(my_engage.exe_ordre,my_engage.tap_id,my_engage.org_id, my_ht, my_ttc);
           else
              my_ht:=my_ttc; 
              my_bud:=my_ttc;
           end if;

           my_tva:=0;
        end if;

        update engage_budget set eng_ht_saisie=my_ht, eng_tva_saisie=my_tva, eng_ttc_saisie=my_ttc, eng_montant_budgetaire=my_bud
          where eng_id=my_engage.eng_id;
        corrige_engage_repartition(my_engage.eng_id);
        
        -- on associe la depense au nouvel engagement
        update depense_budget set eng_id=my_eng_id where dep_id=a_dep_id;
                     
        -- on lance la correction du budget
        budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
        budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);

      end if;
      
      return my_eng_id;
   END;
   
FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
     my_eng_id      engage_budget.eng_id%type;
     my_depense     depense_budget%rowtype;      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
   
     select engage_budget_seq.nextval into my_eng_id from dual;
     insert into engage_budget values (my_eng_id, my_depense.exe_ordre, Get_Numerotation(my_depense.exe_ordre, NULL, null,'ENGAGE_BUDGET'),
       a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, 0, 0, 0, 0, 0, sysdate, a_tyap_id, a_utl_ordre);
     
     augmenter_engage(a_dep_id,my_eng_id);
          
     return my_eng_id;
   END;
      
PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type)
   IS
     my_nb          integer;
     my_id          engage_ctrl_action.eact_id%type;
     my_engage      engage_budget%rowtype;
     my_depense     depense_budget%rowtype;
      
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     update engage_budget set eng_montant_budgetaire=eng_montant_budgetaire+my_depense.dep_montant_budgetaire, eng_ht_saisie=eng_ht_saisie+my_depense.dep_ht_saisie, 
       eng_tva_saisie=eng_tva_saisie+my_depense.dep_tva_saisie, eng_ttc_saisie=eng_ttc_saisie+my_depense.dep_ttc_saisie where eng_id=a_eng_id;
     
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
          select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id; 
          if my_nb=0 then 
             insert into engage_ctrl_action select engage_ctrl_action_seq.nextval, row_action.exe_ordre, a_eng_id, row_action.tyac_id,
                row_action.dact_montant_budgetaire, 0, row_action.dact_ht_saisie, row_action.dact_tva_saisie, row_action.dact_ttc_saisie, sysdate from dual;
          else
             select min(eact_id) into my_id from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id;
             update engage_ctrl_action set eact_montant_budgetaire=eact_montant_budgetaire+row_action.dact_montant_budgetaire,
               eact_ht_saisie=eact_ht_saisie+row_action.dact_ht_saisie, eact_tva_saisie=eact_tva_saisie+row_action.dact_tva_saisie,
               eact_ttc_saisie=eact_ttc_saisie+row_action.dact_ttc_saisie where eact_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
          select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id; 
          if my_nb=0 then 
            insert into engage_ctrl_analytique select engage_ctrl_analytique_seq.nextval, row_analytique.exe_ordre, a_eng_id, row_analytique.can_id,
               row_analytique.dana_montant_budgetaire, 0, row_analytique.dana_ht_saisie, row_analytique.dana_tva_saisie, row_analytique.dana_ttc_saisie, 
               sysdate from dual;
          else
             select min(eana_id) into my_id from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id;
             update engage_ctrl_analytique set eana_montant_budgetaire=eana_montant_budgetaire+row_analytique.dana_montant_budgetaire,
               eana_ht_saisie=eana_ht_saisie+row_analytique.dana_ht_saisie, eana_tva_saisie=eana_tva_saisie+row_analytique.dana_tva_saisie,
               eana_ttc_saisie=eana_ttc_saisie+row_analytique.dana_ttc_saisie where eana_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
          select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_convention select engage_ctrl_convention_seq.nextval, row_convention.exe_ordre, a_eng_id, row_convention.conv_ordre,
                row_convention.dcon_montant_budgetaire, 0, row_convention.dcon_ht_saisie, row_convention.dcon_tva_saisie, row_convention.dcon_ttc_saisie, 
                sysdate from dual;
          else
             select min(econ_id) into my_id from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre;
             update engage_ctrl_convention set econ_montant_budgetaire=econ_montant_budgetaire+row_convention.dcon_montant_budgetaire,
               econ_ht_saisie=econ_ht_saisie+row_convention.dcon_ht_saisie, econ_tva_saisie=econ_tva_saisie+row_convention.dcon_tva_saisie,
               econ_ttc_saisie=econ_ttc_saisie+row_convention.dcon_ttc_saisie where econ_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
          select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id; 
          if my_nb=0 then 
             insert into engage_ctrl_hors_marche select engage_ctrl_hors_marche_seq.nextval, row_hors_marche.exe_ordre, a_eng_id, row_hors_marche.typa_id, 
                row_hors_marche.ce_ordre, row_hors_marche.dhom_montant_budgetaire, 0,0, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_tva_saisie, 
                row_hors_marche.dhom_ttc_saisie, sysdate from dual;
          else
             select min(ehom_id) into my_id from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id;
             update engage_ctrl_hors_marche set ehom_montant_budgetaire=ehom_montant_budgetaire+row_hors_marche.dhom_montant_budgetaire,
               ehom_ht_saisie=ehom_ht_saisie+row_hors_marche.dhom_ht_saisie, ehom_tva_saisie=ehom_tva_saisie+row_hors_marche.dhom_tva_saisie,
               ehom_ttc_saisie=ehom_ttc_saisie+row_hors_marche.dhom_ttc_saisie where ehom_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
          select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_marche select engage_ctrl_marche_seq.nextval, row_marche.exe_ordre, a_eng_id, row_marche.att_ordre,
                row_marche.dmar_montant_budgetaire, 0, 0, row_marche.dmar_ht_saisie, row_marche.dmar_tva_saisie, row_marche.dmar_ttc_saisie, sysdate from dual;
          else
             select min(emar_id) into my_id from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre;
             update engage_ctrl_marche set emar_montant_budgetaire=emar_montant_budgetaire+row_marche.dmar_montant_budgetaire,
               emar_ht_saisie=emar_ht_saisie+row_marche.dmar_ht_saisie, emar_tva_saisie=emar_tva_saisie+row_marche.dmar_tva_saisie,
               emar_ttc_saisie=emar_ttc_saisie+row_marche.dmar_ttc_saisie where emar_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then
       for row_planco in planco
       loop
          select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num; 
          if my_nb=0 then 
             insert into engage_ctrl_planco select engage_ctrl_planco_seq.nextval, row_planco.exe_ordre, a_eng_id, row_planco.pco_num,
                row_planco.dpco_montant_budgetaire, 0, row_planco.dpco_ht_saisie, row_planco.dpco_tva_saisie, row_planco.dpco_ttc_saisie, sysdate from dual;
          else
             select min(epco_id) into my_id from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num;
             update engage_ctrl_planco set epco_montant_budgetaire=epco_montant_budgetaire+row_planco.dpco_montant_budgetaire,
               epco_ht_saisie=epco_ht_saisie+row_planco.dpco_ht_saisie, epco_tva_saisie=epco_tva_saisie+row_planco.dpco_tva_saisie,
               epco_ttc_saisie=epco_ttc_saisie+row_planco.dpco_ttc_saisie where epco_id=my_id;
          end if;
       end loop;
     end if;

     verifier.VERIFIER_ENGAGE_COHERENCE(a_eng_id);
   END;

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type) return number
   IS
     my_cbud_id       commande_budget.cbud_id%type;
     my_engage        engage_budget%rowtype;
     
     my_nb integer;
     my_pourcentage number(15,5);
     my_total_pourcentage number(15,5);
     
     my_chaine_action varchar2(3000);
     my_chaine_analytique varchar2(3000);
     my_chaine_convention varchar2(3000);
     my_chaine_hors_marche varchar2(3000);
     my_chaine_marche varchar2(3000);
     my_chaine_planco varchar2(3000);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;

   BEGIN
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     -- on definit les infos, notamment les pourcentages
     my_chaine_action:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_action.eact_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_action:=my_chaine_action||row_action.tyac_id||'$'||row_action.eact_ht_saisie||'$'||row_action.eact_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_action:=my_chaine_action||'$';

     my_chaine_analytique:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         my_pourcentage:=round(100*row_analytique.eana_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_analytique:=my_chaine_analytique||row_analytique.can_id||'$'||row_analytique.eana_ht_saisie||'$'
               ||row_analytique.eana_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_analytique:=my_chaine_analytique||'$';

     my_chaine_convention:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         my_pourcentage:=round(100*row_convention.econ_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_convention:=my_chaine_convention||row_convention.conv_ordre||'$'||row_convention.econ_ht_saisie||'$'
              ||row_convention.econ_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_convention:=my_chaine_convention||'$';

     my_chaine_hors_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
           my_chaine_hors_marche:=my_chaine_hors_marche||row_hors_marche.typa_id||'$'||row_hors_marche.ce_ordre||'$'
              ||row_hors_marche.ehom_ht_saisie||'$'||row_hors_marche.ehom_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_hors_marche:=my_chaine_hors_marche||'$';

     my_chaine_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
           my_chaine_marche:=my_chaine_marche||row_marche.att_ordre||'$'||row_marche.emar_ht_saisie||'$'||row_marche.emar_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_marche:=my_chaine_marche||'$';

     my_chaine_planco:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_planco.epco_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_planco:=my_chaine_planco||row_planco.pco_num||'$'||row_planco.epco_ht_saisie||'$'||row_planco.epco_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_planco:=my_chaine_planco||'$';
     
     -- on cree
     select commande_budget_seq.nextval into my_cbud_id from dual;
     commander.ins_commande_budget (my_cbud_id, a_comm_id, my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre, my_engage.tap_id, my_engage.eng_ht_saisie,
       my_engage.eng_ttc_saisie, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche, my_chaine_marche, my_chaine_planco);
               
     -- on corrige pour calculer les montants des repartitions
     corriger.corriger_commande_ctrl(my_cbud_id);
     verifier.verifier_cde_budget_coherence(my_cbud_id);
     
     
     return my_cbud_id;
   END;

PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;
     my_eng_montant_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_reste engage_budget.eng_montant_budgetaire%type;
     my_ctrl_bud engage_budget.eng_montant_budgetaire%type;
     my_dep_budgetaire depense_budget.dep_montant_budgetaire%type;
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant engage_budget
     select * into my_engage from engage_budget where eng_id=a_eng_id;
    
     my_eng_montant_budgetaire:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id,
         my_engage.org_id, my_engage.eng_ht_saisie, my_engage.eng_ttc_saisie);
     
     select nvl(SUM(Budget.calculer_budgetaire(exe_ordre, my_engage.tap_id, my_engage.org_id, dep_ht_saisie, dep_ttc_saisie)),0) 
        into my_dep_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
     my_reste:=my_eng_montant_budgetaire-my_dep_budgetaire;
     if my_reste<0 then my_reste:=0; end if;
     
     update engage_budget set eng_montant_budgetaire=my_eng_montant_budgetaire, eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;

     -- maj montant engage_ctrl_action
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update engage_ctrl_action set eact_montant_budgetaire=my_reste, eact_montant_budgetaire_reste=my_reste  where eact_id=row_action.eact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_action.eact_ht_saisie, row_action.eact_ttc_saisie);

            update engage_ctrl_action set eact_montant_budgetaire=my_ctrl_bud, eact_montant_budgetaire_reste=my_ctrl_bud where eact_id=row_action.eact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_analytique
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update engage_ctrl_analytique set eana_montant_budgetaire=my_reste, eana_montant_budgetaire_reste=my_reste  where eana_id=row_analytique.eana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_analytique.eana_ht_saisie, row_analytique.eana_ttc_saisie);

            update engage_ctrl_analytique set eana_montant_budgetaire=my_ctrl_bud, eana_montant_budgetaire_reste=my_ctrl_bud where eana_id=row_analytique.eana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(a_eng_id);
     end if;

     -- maj montant engage_ctrl_convention
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update engage_ctrl_convention set econ_montant_budgetaire=my_reste, econ_montant_budgetaire_reste=my_reste  where econ_id=row_convention.econ_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_convention.econ_ht_saisie, row_convention.econ_ttc_saisie);

            update engage_ctrl_convention set econ_montant_budgetaire=my_ctrl_bud, econ_montant_budgetaire_reste=my_ctrl_bud where econ_id=row_convention.econ_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(a_eng_id);
     end if;

     -- maj montant engage_ctrl_hors_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste, ehom_montant_budgetaire_reste=my_reste  where ehom_id=row_hors_marche.ehom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_hors_marche.ehom_ht_saisie, row_hors_marche.ehom_ttc_saisie);

            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_ctrl_bud, ehom_montant_budgetaire_reste=my_ctrl_bud where ehom_id=row_hors_marche.ehom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(a_eng_id);
     end if;

     -- maj montant engage_ctrl_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update engage_ctrl_marche set emar_montant_budgetaire=my_reste, emar_montant_budgetaire_reste=my_reste  where emar_id=row_marche.emar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_marche.emar_ht_saisie, row_marche.emar_ttc_saisie);

            update engage_ctrl_marche set emar_montant_budgetaire=my_ctrl_bud, emar_montant_budgetaire_reste=my_ctrl_bud where emar_id=row_marche.emar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update engage_ctrl_planco set epco_montant_budgetaire=my_reste, epco_montant_budgetaire_reste=my_reste  where epco_id=row_planco.epco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_planco.epco_ht_saisie, row_planco.epco_ttc_saisie);

            update engage_ctrl_planco set epco_montant_budgetaire=my_ctrl_bud, epco_montant_budgetaire_reste=my_ctrl_bud where epco_id=row_planco.epco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(a_eng_id);
     end if;

     verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;

     my_montant_ht_total engage_budget.eng_ht_saisie%type;
     my_montant_ttc_total engage_budget.eng_ttc_saisie%type;
     my_montant_budgetaire_total engage_budget.eng_montant_budgetaire%type;
     
     my_ht engage_budget.eng_ht_saisie%type;
     my_ttc engage_budget.eng_ttc_saisie%type;
     my_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_pourcentage number (18,8);
          
     my_reste_ht engage_budget.eng_ht_saisie%type;
     my_reste_ttc engage_budget.eng_ttc_saisie%type;
     my_reste_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_total_pourcentage number (18,8);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
     my_nb_decimales        NUMBER;
   BEGIN

       -- infos references
       select * into my_engage from engage_budget where eng_id=a_eng_id;
       my_nb_decimales:=liquider_outils.get_nb_decimales(my_engage.exe_ordre);
        
       select sum(eact_ht_saisie), sum(eact_montant_budgetaire), sum(eact_ttc_saisie)
          into my_montant_ht_total, my_montant_budgetaire_total, my_montant_ttc_total from engage_ctrl_action where eng_id=a_eng_id;
   
   
       -- maj montant engage_ctrl_action       
       select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_action in action
         loop
           if action%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_action set eact_montant_budgetaire=my_reste_budgetaire, eact_ht_saisie=my_reste_ht,
                 eact_ttc_saisie=my_reste_ttc,  eact_tva_saisie=my_reste_ttc-my_reste_ht where eact_id=row_action.eact_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_action.eact_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_action.eact_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_action.eact_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_action set eact_montant_budgetaire=my_budgetaire, eact_ht_saisie=my_ht,
                 eact_ttc_saisie=my_ttc, eact_tva_saisie=my_ttc-my_ht where eact_id=row_action.eact_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_analytique       
       select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(eana_ht_saisie) into my_total_pourcentage from engage_ctrl_analytique where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_analytique in analytique
         loop
           if analytique%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_analytique set eana_montant_budgetaire=my_reste_budgetaire, eana_ht_saisie=my_reste_ht,
                 eana_ttc_saisie=my_reste_ttc,  eana_tva_saisie=my_reste_ttc-my_reste_ht where eana_id=row_analytique.eana_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_analytique.eana_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_analytique.eana_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_analytique.eana_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_analytique set eana_montant_budgetaire=my_budgetaire, eana_ht_saisie=my_ht,
                 eana_ttc_saisie=my_ttc, eana_tva_saisie=my_ttc-my_ht where eana_id=row_analytique.eana_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_convention
       select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(econ_ht_saisie) into my_total_pourcentage from engage_ctrl_convention where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_convention in convention
         loop
           if convention%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_convention set econ_montant_budgetaire=my_reste_budgetaire, econ_ht_saisie=my_reste_ht,
                 econ_ttc_saisie=my_reste_ttc,  econ_tva_saisie=my_reste_ttc-my_reste_ht where econ_id=row_convention.econ_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_convention.econ_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_convention.econ_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_convention.econ_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_convention set econ_montant_budgetaire=my_budgetaire, econ_ht_saisie=my_ht,
                 econ_ttc_saisie=my_ttc, econ_tva_saisie=my_ttc-my_ht where econ_id=row_convention.econ_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_hors_marche       
       select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;

         for row_hors_marche in hors_marche
         loop
           if hors_marche%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste_budgetaire, ehom_ht_saisie=my_reste_ht,
                 ehom_ttc_saisie=my_reste_ttc,  ehom_tva_saisie=my_reste_ttc-my_reste_ht where ehom_id=row_hors_marche.ehom_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_hors_marche.ehom_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_hors_marche.ehom_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_hors_marche.ehom_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_budgetaire, ehom_ht_saisie=my_ht,
                 ehom_ttc_saisie=my_ttc, ehom_tva_saisie=my_ttc-my_ht where ehom_id=row_hors_marche.ehom_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_marche       
       select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;

         update engage_ctrl_marche set emar_montant_budgetaire=my_reste_budgetaire, emar_ht_saisie=my_reste_ht,
              emar_ttc_saisie=my_reste_ttc,  emar_tva_saisie=my_reste_ttc-my_reste_ht where eng_id=a_eng_id;
       end if;   

       -- maj montant engage_ctrl_planco
       select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_planco in planco
         loop
           if planco%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_planco set epco_montant_budgetaire=my_reste_budgetaire, epco_ht_saisie=my_reste_ht,
                 epco_ttc_saisie=my_reste_ttc,  epco_tva_saisie=my_reste_ttc-my_reste_ht where epco_id=row_planco.epco_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_planco.epco_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_planco.epco_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_planco.epco_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_planco set epco_montant_budgetaire=my_budgetaire, epco_ht_saisie=my_ht,
                 epco_ttc_saisie=my_ttc, epco_tva_saisie=my_ttc-my_ht where epco_id=row_planco.epco_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       --corrige_engage_budgetaire();
       verifier.verifier_engage_coherence(a_eng_id);
   END;
   
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type
   ) 
   IS
     my_depense depense_budget%rowtype;
     my_dep_montant_budgetaire depense_budget.dep_montant_budgetaire%type;
     my_reste depense_budget.dep_montant_budgetaire%type;
     my_ctrl_bud depense_budget.dep_montant_budgetaire%type;
     
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant depense_budget
     select * into my_depense from depense_budget where dep_id=a_dep_id;
    
     my_dep_montant_budgetaire:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, my_depense.dep_ht_saisie, my_depense.dep_ttc_saisie);
     update depense_budget set dep_montant_budgetaire=my_dep_montant_budgetaire where dep_id=a_dep_id;

     -- maj montant depense_ctrl_action
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update depense_ctrl_action set dact_montant_budgetaire=my_reste where dact_id=row_action.dact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_action.dact_ht_saisie, row_action.dact_ttc_saisie);

            update depense_ctrl_action set dact_montant_budgetaire=my_ctrl_bud where dact_id=row_action.dact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(my_depense.eng_id);
     end if;
     
     -- maj montant depense_ctrl_analytique
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update depense_ctrl_analytique set dana_montant_budgetaire=my_reste where dana_id=row_analytique.dana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_analytique.dana_ht_saisie, row_analytique.dana_ttc_saisie);

            update depense_ctrl_analytique set dana_montant_budgetaire=my_ctrl_bud where dana_id=row_analytique.dana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_convention
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update depense_ctrl_convention set dcon_montant_budgetaire=my_reste where dcon_id=row_convention.dcon_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_convention.dcon_ht_saisie, row_convention.dcon_ttc_saisie);

            update depense_ctrl_convention set dcon_montant_budgetaire=my_ctrl_bud where dcon_id=row_convention.dcon_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_hors_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_reste where dhom_id=row_hors_marche.dhom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_ttc_saisie);

            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_ctrl_bud where dhom_id=row_hors_marche.dhom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update depense_ctrl_marche set dmar_montant_budgetaire=my_reste where dmar_id=row_marche.dmar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_marche.dmar_ht_saisie, row_marche.dmar_ttc_saisie);

            update depense_ctrl_marche set dmar_montant_budgetaire=my_ctrl_bud where dmar_id=row_marche.dmar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(my_depense.eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update depense_ctrl_planco set dpco_montant_budgetaire=my_reste where dpco_id=row_planco.dpco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_planco.dpco_ht_saisie, row_planco.dpco_ttc_saisie);

            update depense_ctrl_planco set dpco_montant_budgetaire=my_ctrl_bud where dpco_id=row_planco.dpco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(my_depense.eng_id);
     end if;

     verifier.verifier_depense_coherence(a_dep_id);
   END;
   
PROCEDURE rempli_reimputation(
      a_dep_id       depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre    depense_budget.utl_ordre%type
   )
   IS
     my_nb          integer;
     my_reim_id     reimputation.reim_id%type;
     my_exe_ordre   depense_budget.exe_ordre%type;
     my_reim_numero reimputation.reim_numero%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     my_reim_numero := Get_Numerotation(my_exe_ordre, NULL, null, 'REIMPUTATION');

     select reimputation_seq.nextval into my_reim_id from dual;
     insert into reimputation values (my_reim_id, my_exe_ordre, my_reim_numero, a_dep_id, a_reim_libelle, a_utl_ordre, sysdate);
     
     insert into reimputation_action select reimputation_action_seq.nextval, my_reim_id, tyac_id, exe_ordre, dact_montant_budgetaire, dact_ht_saisie,
         dact_tva_saisie, dact_ttc_saisie from depense_ctrl_action where dep_id=a_dep_id;
         
     insert into reimputation_analytique select reimputation_analytique_seq.nextval, my_reim_id, can_id, dana_montant_budgetaire, dana_ht_saisie,
         dana_tva_saisie, dana_ttc_saisie from depense_ctrl_analytique where dep_id=a_dep_id;
         
     insert into reimputation_budget select reimputation_budget_seq.nextval, my_reim_id, d.eng_id, e.org_id, e.tcd_ordre, d.tap_id
        from depense_budget d, engage_budget e where e.eng_id=d.eng_id and dep_id=a_dep_id;
     
     insert into reimputation_convention select reimputation_convention_seq.nextval, my_reim_id, conv_ordre, dcon_montant_budgetaire, dcon_ht_saisie,
         dcon_tva_saisie, dcon_ttc_saisie from depense_ctrl_convention where dep_id=a_dep_id;
         
     insert into reimputation_hors_marche select reimputation_hors_marche_seq.nextval, my_reim_id, typa_id, ce_ordre, dhom_montant_budgetaire, dhom_ht_saisie,
         dhom_tva_saisie, dhom_ttc_saisie from depense_ctrl_hors_marche where dep_id=a_dep_id;
         
     insert into reimputation_marche select reimputation_marche_seq.nextval, my_reim_id, att_ordre, dmar_montant_budgetaire, dmar_ht_saisie,
         dmar_tva_saisie, dmar_ttc_saisie from depense_ctrl_marche where dep_id=a_dep_id;
         
     insert into reimputation_planco select reimputation_planco_seq.nextval, my_reim_id, pco_num, exe_ordre, dpco_montant_budgetaire, dpco_ht_saisie,
         dpco_tva_saisie, dpco_ttc_saisie from depense_ctrl_planco where dep_id=a_dep_id;
   END;
   
PROCEDURE reimputation_maracuja (
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type
   )
   IS
      my_dep_id       depense_budget.dep_id%type;
      cursor depenses is select dep_id from depense_ctrl_planco where man_id=a_man_id;
   BEGIN
      if a_man_id is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer un man_id');
      end if;
      if a_pco_num is null then
         raise_application_error(-20001, 'Pour une reimputation comptable il faut passer la nouvelle imputation');
      end if;
        
      open depenses();
      loop
         fetch depenses into my_dep_id;
         exit when depenses%notfound;
         
         depense_avec_infos(my_dep_id, 'reimputation comptable', null, null, null, null, a_pco_num, null,
            null, null, null, null, 4 /*NON*/, null);
      end loop;
      close depenses;
   END;
   
   
procedure check_reimp_maracuja_possible(
      a_man_id                depense_ctrl_planco.man_id%type,
      a_pco_num               depense_ctrl_planco.pco_num%type
   )
   IS    
      my_nb        integer;       
      my_dep_id       depense_budget.dep_id%type;
      my_dpco_id   depense_ctrl_planco.dpco_id%type;
      my_exe_ordre depense_ctrl_planco.exe_ordre%type;
      cursor depenses is select dep_id from depense_ctrl_planco where man_id=a_man_id;
    begin
       --- on reprend les controles effectues dans "reimputation_inventaire"
       
        
      open depenses();
      loop
         fetch depenses into my_dep_id;
         exit when depenses%notfound;
         
               select dpco_id, exe_ordre into my_dpco_id, my_exe_ordre from depense_ctrl_planco where dep_id=my_dep_id;
               select count(*) into my_nb from jefy_inventaire.inventaire_comptable 
                   where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id);
                 
                 if my_nb=0 then return; end if;

                if a_pco_num is not null then 
                    select count(*) into my_nb from jefy_inventaire.inventaire_comptable where clic_id in (         
                         select clic_id from jefy_inventaire.inventaire_comptable 
                         where dpco_id=my_dpco_id or invc_id in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id))
                       and dpco_id<>my_dpco_id and invc_id not in (select invc_id from jefy_inventaire.inventaire_comptable_orv where dpco_id=my_dpco_id)
                       and dpco_id is not null;
                 
                    if my_nb>0 then
                       RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas effectuer cette reimputation car elle concerne des inventaires comptables relies a d''autres depenses');
                    end if;
                    
                    select count(*) into my_nb from maracuja.planco_amortissement where pco_num=a_pco_num and exe_ordre=my_exe_ordre;
                    if my_nb<>1 then
                       RAISE_APPLICATION_ERROR(-20001, 'il y a un probleme de configuration pour le compte d''amortissement de cette imputation '||a_pco_num||' sur l''exercice '||my_exe_ordre);
                    end if;
                 end if;
         
      end loop;
      close depenses;
    
    end;
END;
/


GRANT EXECUTE ON JEFY_DEPENSE.REIMPUTER TO JEFY_MISSION;
GRANT EXECUTE ON JEFY_DEPENSE.REIMPUTER TO MARACUJA;


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Liquider
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE service_fait (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_depense_papier  depense_papier%rowtype;
      my_pers_id         jefy_admin.utilisateur.pers_id%type;
    BEGIN
      select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 
      
      select * into my_depense_papier from depense_papier where dpp_id=a_dpp_id;
      if my_depense_papier.dpp_date_service_fait is not null then 
         RAISE_APPLICATION_ERROR(-20001, 'La facture a deja une date de service fait ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if;
      
      select pers_id into my_pers_id from jefy_admin.utilisateur where utl_ordre=a_utl_ordre;
      update depense_papier set dpp_date_service_fait=a_dpp_date_service_fait, dpp_sf_pers_id=my_pers_id, dpp_sf_date=sysdate
          where dpp_id=a_dpp_id;      
    END;

   PROCEDURE service_fait_et_liquide (
      a_dpp_id                DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE
    ) IS
      my_nb              integer;
      my_pdep_id         pdepense_budget.pdep_id%type;
      CURSOR liste IS SELECT pdep_id FROM pDEPENSE_budget WHERE dpp_id=a_dpp_id;
    BEGIN
      service_fait(a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);
      
      select count(*) into my_nb from pdepense_budget where dpp_id=a_dpp_id;
      if my_nb=0 then
         RAISE_APPLICATION_ERROR(-20001, 'La facture n''a pas de pre-liquidation ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
      end if; 

      OPEN liste();
      LOOP
         FETCH liste INTO my_pdep_id;
         EXIT WHEN liste%NOTFOUND;

         pre_liquider.liquider_pdepense_budget(my_pdep_id, a_utl_ordre);
      END LOOP;
      CLOSE liste;
    END;

   PROCEDURE ins_depense_papier (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
   BEGIN
        ins_depense_papier_avec_IM(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
            a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
            a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, null, null, null);
   end;
   
PROCEDURE ins_depense_papier_avec_IM (
      a_dpp_id IN OUT         DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre             DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial        DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial       DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre             DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre             DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre             DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception    DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre             DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      imtt_id                 DEPENSE_PAPIER.imtt_id%type
   ) IS
   BEGIN
      ins_depense_papier_avec_im_sf(a_dpp_id, a_exe_ordre, a_dpp_numero_facture, a_dpp_ht_initial, a_dpp_ttc_initial,
          a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception, a_dpp_date_service_fait, 
          a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement, a_dpp_im_taux, a_dpp_im_dgp, imtt_id, null, null,null);
   END;
   
   PROCEDURE ins_depense_papier_avec_im_sf (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE,
      a_dpp_im_taux           DEPENSE_PAPIER.dpp_im_taux%TYPE,
      a_dpp_im_dgp            DEPENSE_PAPIER.dpp_im_dgp%type,
      a_imtt_id               DEPENSE_PAPIER.imtt_id%type,
      a_dpp_sf_pers_id        DEPENSE_PAPIER.dpp_sf_pers_id%type,
      a_dpp_sf_date           DEPENSE_PAPIER.dpp_sf_date%type,
      a_ecd_ordre             DEPENSE_PAPIER.ecd_ordre%type 
    ) IS
      my_dpp_ht_initial       DEPENSE_PAPIER.dpp_ht_initial%TYPE;
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpp_ttc_initial      DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
      my_dpp_sf_pers_id       DEPENSE_PAPIER.dpp_sf_pers_id%type;
      my_dpp_sf_date          DEPENSE_PAPIER.dpp_sf_date%type;
      my_nb_decimales         NUMBER;
      my_nb                   integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
        Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dpp_ht_initial:=round(a_dpp_ht_initial, my_nb_decimales);
        my_dpp_ttc_initial:=round(a_dpp_ttc_initial, my_nb_decimales);

        -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
        IF my_dpp_ht_initial<0 OR my_dpp_ttc_initial<0 OR a_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(my_dpp_ht_initial)>ABS(my_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(my_dpp_ht_initial, my_dpp_ttc_initial);

           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        my_dpp_sf_pers_id:=a_dpp_sf_pers_id;
        my_dpp_sf_date:=a_dpp_sf_date;
        if a_dpp_date_service_fait is not null then
           if a_dpp_sf_pers_id is null then
              select pers_id into my_dpp_sf_pers_id from v_utilisateur where utl_ordre=a_utl_ordre;
           end if;
           
           if a_dpp_sf_date is null then
              select sysdate into my_dpp_sf_date from dual;
           end if;  
        end if;
        
        select count(*) into my_nb from depense_papier where dpp_id=a_dpp_id;
        
        if my_nb=0 then
           INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
              0, 0, a_fou_ordre, a_rib_ordre, a_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
              a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
              my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial, a_dpp_im_taux, a_dpp_im_dgp, a_imtt_id,
              my_dpp_sf_pers_id, my_dpp_sf_date,a_ecd_ordre);
        else
           update DEPENSE_PAPIER set dpp_numero_facture=a_dpp_numero_facture, rib_ordre=a_rib_ordre, mod_ordre=a_mod_ordre, dpp_date_facture=a_dpp_date_facture, 
              dpp_date_reception=a_dpp_date_reception, dpp_date_service_fait=a_dpp_date_service_fait, dpp_nb_piece=a_dpp_nb_piece, utl_ordre=a_utl_ordre, 
              dpp_id_reversement=a_dpp_id_reversement, dpp_ht_initial=my_dpp_ht_initial, dpp_tva_initial=my_dpp_tva_initial, dpp_ttc_initial=my_dpp_ttc_initial, 
              dpp_im_taux=a_dpp_im_taux, dpp_im_dgp=a_dpp_im_dgp, imtt_id=a_imtt_id, dpp_sf_pers_id=my_dpp_sf_pers_id, dpp_sf_date=my_dpp_sf_date, dpp_date_saisie=sysdate, ecd_ordre=a_ecd_ordre
             where dpp_id=a_dpp_id;
        end if;
   END;

   PROCEDURE ins_commande_dep_papier (
      a_cdp_id IN OUT         COMMANDE_DEP_PAPIER.cdp_id%TYPE,
      a_comm_id               COMMANDE_DEP_PAPIER.comm_id%TYPE,
      a_dpp_id                COMMANDE_DEP_PAPIER.dpp_id%TYPE
   ) IS
     my_nb                    INTEGER;
     my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
     my_cde_exe_ordre         COMMANDE.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture n''existe pas ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture est deja utilise pour une commande ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT exe_ordre INTO my_cde_exe_ordre FROM COMMANDE WHERE comm_id=a_comm_id;

        IF my_dpp_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.dep_papier(a_dpp_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        -- si pas de probleme on insere.
        IF a_cdp_id IS NULL THEN
           SELECT commande_dep_papier_seq.NEXTVAL INTO a_cdp_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_DEP_PAPIER VALUES (a_cdp_id, a_comm_id, a_dpp_id);
   END;

   PROCEDURE ins_depense (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id               DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id               DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id               DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_org_id               engage_budget.org_id%type;
     my_nb_decimales         NUMBER;
     my_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE;
     my_dep_ttc_saisie       DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
   BEGIN
       IF a_dep_ttc_saisie<>0 THEN
       
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);

        select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
        
        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

        -- lancement des differentes procedures d'insertion des tables de depense.
        ins_depense_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie,
            a_tap_id, a_utl_ordre, a_dep_id_reversement);

        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie+my_dep_ttc_saisie-my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie+my_dep_ttc_saisie
           WHERE dpp_id=a_dpp_id;

           -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
        ins_depense_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

        ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
        ins_depense_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
        ins_depense_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
        ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
        ins_depense_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);

        --Corriger.upd_engage_reste(a_eng_id);

        -- on verifie la coherence des montants entre les differents depense_.
        Verifier.verifier_depense_coherence(a_dep_id);
        Verifier.verifier_depense_pap_coherence(a_dpp_id);
        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                     INTEGER;
      my_eng_id                 ENGAGE_BUDGET.eng_id%TYPE;
      my_zdep_id                Z_DEPENSE_BUDGET.zdep_id%TYPE;
      my_exe_ordre              DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire     DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dep_total_ht           DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_total_ttc          DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dep_total_bud          DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_eng_montant_bud        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_montant_bud_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
      my_dispo_ligne            v_budget_exec_credit.bdxc_disponible%TYPE;
      my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_dep_ht_saisie          DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_eng_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
      my_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE;
      my_comm_id                COMMANDE.comm_id%TYPE;
     my_dpco_id                 DEPENSE_CTRL_PLANCO.dpco_id%TYPE;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La liquidation n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT e.org_id, e.tcd_ordre, d.exe_ordre, d.dep_ht_saisie, d.dep_ttc_saisie,
               d.dep_montant_budgetaire, e.eng_id, e.tap_id, e.eng_montant_budgetaire, e.eng_montant_budgetaire_reste, d.dpp_id
          INTO my_org_id, my_tcd_ordre, my_exe_ordre, my_dep_ht_saisie, my_dep_ttc_saisie,
               my_montant_budgetaire, my_eng_id, my_eng_tap_id, my_eng_montant_bud, my_eng_montant_bud_reste, my_dpp_id
        FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e
        WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si ce n'est pas un ORV.
        IF my_montant_budgetaire<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- on recupere les montants des autres factures et ORV.
        SELECT NVL(SUM(Budget.calculer_budgetaire(my_exe_ordre,my_eng_tap_id,my_org_id,dep_ht_saisie, dep_ttc_saisie)),0) 
          INTO my_dep_total_bud
          FROM DEPENSE_BUDGET WHERE dep_id<>a_dep_id AND dep_id IN
             (SELECT dep_id FROM DEPENSE_CTRL_PLANCO
               WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE eng_id=my_eng_id)
               AND (dpco_montant_budgetaire>0 /*OR man_id IN
               (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

        -- on calcule le reste de l'engagement
        my_reste:=my_eng_montant_bud-my_dep_total_bud;
        IF my_reste<0 THEN
           my_reste:=0;
        END IF;

        -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
        my_dispo_ligne:=my_montant_budgetaire+
            Budget.engager_disponible(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>my_dispo_ligne+my_eng_montant_bud_reste  THEN
          my_reste:=my_dispo_ligne+my_eng_montant_bud_reste;
        END IF;

        -- on modifie l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=my_reste WHERE eng_id=my_eng_id;

        -- tout est bon ... on supprime la depense.
        log_depense_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        DELETE FROM reimputation_ACTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_ANALYTIQUE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_BUDGET WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_CONVENTION WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_HORS_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_MARCHE WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation_PLANCO WHERE reim_id in (select reim_id from reimputation where dep_id=a_dep_id);
        DELETE FROM reimputation WHERE dep_id=a_dep_id;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        Corriger.upd_engage_reste(my_eng_id);

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_engage_coherence(my_eng_id);
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

       Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);

   END;


   PROCEDURE del_depense_papier (
      a_dpp_id             DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre          Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE) IS
      my_nb                INTEGER;
      my_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
      my_dpp_ttc_initial   DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_depense_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM COMMANDE_DEP_PAPIER WHERE dpp_id=a_dpp_id;
        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;



--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------



   PROCEDURE log_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id            Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
       INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_depense_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      --a_der_id                 depense_budget.der_id%type,
      a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_dep_ht_saisie      DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_nb_decimales       NUMBER;
       
       my_nb                 INTEGER;
       my_par_value          PARAMETRE.par_value%TYPE;

       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_tap_id             ENGAGE_BUDGET.tap_id%TYPE;
       my_exe_ordre          ENGAGE_BUDGET.exe_ordre%TYPE;
       my_tcd_ordre          ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_eng_budgetaire     ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_eng_reste          ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
       my_eng_init           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
       my_dep_eng_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

       my_dpp_ht_initial     DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial    DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial    DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       --my_sum_ht_saisie         depense_budget.dep_ht_saisie%type;
       --my_sum_tva_saisie     depense_budget.dep_tva_saisie%type;
       --my_sum_ttc_saisie     depense_budget.dep_ttc_saisie%type;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_dep_ht_saisie:=round(a_dep_ht_saisie, my_nb_decimales);
        my_dep_ttc_saisie:=round(a_dep_ttc_saisie, my_nb_decimales);
      
        IF my_dep_ht_saisie<0 OR my_dep_ttc_saisie<0 OR a_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT exe_ordre, org_id, tap_id, tcd_ordre, eng_montant_budgetaire_reste, eng_montant_budgetaire
          INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_eng_reste, my_eng_init
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

          SELECT exe_ordre, dpp_ht_initial, dpp_tva_initial, dpp_ttc_initial
          INTO my_dpp_exe_ordre, my_dpp_ht_initial, my_dpp_tva_initial, my_dpp_ttc_initial
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        -- verification de la coherence du prorata.
        IF Get_Parametre(a_exe_ordre, 'DEPENSE_IDEM_TAP_ID')<>'NON' AND
           my_tap_id<>a_tap_id THEN
             RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
        END IF;

        -- on verifie la coherence des montants.
         IF ABS(my_dep_ht_saisie)>ABS(my_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(my_dep_ht_saisie, my_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
        /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
               into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          from depense_budget where dpp_id=a_dpp_id;

        if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
           abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
           abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
           raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        end if;
        */
        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              my_dep_ht_saisie,my_dep_ttc_saisie);

        -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
        --    de l'engage (si le tap_id peut etre different de celui de l'engage).
        my_eng_budgetaire:=Liquider_Outils.get_eng_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
          my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

        -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.
        IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde ('||indication_erreur.engagement(a_eng_id)||')');
        END IF;

        -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
        IF my_eng_reste<my_eng_budgetaire THEN
           my_eng_budgetaire:=my_eng_reste;
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

          -- on diminue le reste engage de l'engagement.
        UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=eng_montant_budgetaire_reste-my_eng_budgetaire
           WHERE eng_id=a_eng_id;

        -- on liquide.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre,
           a_dep_id_reversement);

        Budget.maj_budget(a_exe_ordre, my_org_id, my_tcd_ordre);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_depense_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                   DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                        DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie             DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie           DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie           DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                   DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dact_ht_saisie:=round(my_dact_ht_saisie, my_nb_decimales);
            my_dact_ttc_saisie:=round(my_dact_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            IF my_dact_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dact_montant_budgetaire THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id,
                   a_exe_ordre, a_dep_id, my_tyac_id, my_dact_montant_budgetaire,
                   my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_action(my_eng_id);

            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dana_ht_saisie:=round(my_dana_ht_saisie, my_nb_decimales);
            my_dana_ttc_saisie:=round(my_dana_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            IF my_dana_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dana_montant_budgetaire THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_analytique(my_eng_id);

            Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dcon_ht_saisie:=round(my_dcon_ht_saisie, my_nb_decimales);
            my_dcon_ttc_saisie:=round(my_dcon_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            IF my_dcon_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF my_dep_montant_budgetaire<=my_somme+my_dcon_montant_budgetaire THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_convention(my_eng_id);

            Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_?chat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dhom_ht_saisie:=round(my_dhom_ht_saisie, my_nb_decimales);
            my_dhom_ttc_saisie:=round(my_dhom_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            IF my_dhom_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dhom_montant_budgetaire THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Corriger.upd_engage_reste_hors_marche(my_eng_id);

            Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dmar_ht_saisie:=round(my_dmar_ht_saisie, my_nb_decimales);
            my_dmar_ttc_saisie:=round(my_dmar_ttc_saisie, my_nb_decimales);

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            IF my_dmar_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dmar_montant_budgetaire THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

                         dbms_output.put_line('insert depense : '||my_dmar_id);

               -- procedure de verification
                         dbms_output.put_line('upd engage : '||my_eng_id);
            Corriger.upd_engage_reste_marche(my_eng_id);

            Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_depense_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_par_value                PARAMETRE.par_value%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_mod_ordre                   DEPENSE_PAPIER.mod_ordre%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_eng_id                   DEPENSE_BUDGET.eng_id%TYPE;
       my_inventaires    VARCHAR2(30000);
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre, d.eng_id
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre,my_eng_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            select ecd_ordre into my_ecd_ordre from depense_papier where dpp_id in (select dpp_id from depense_budget where dep_id=a_dep_id);
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_dpco_ht_saisie:=round(my_dpco_ht_saisie, my_nb_decimales);
            my_dpco_ttc_saisie:=round(my_dpco_ttc_saisie, my_nb_decimales);

            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            IF my_ecd_ordre IS NOT NULL THEN
               SELECT mod_ordre INTO my_mod_ordre
                  FROM DEPENSE_BUDGET d, DEPENSE_PAPIER p WHERE p.dpp_id=d.dpp_id AND d.dep_id=a_dep_id;
               Verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
            END IF;

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            IF my_dpco_montant_budgetaire<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Pour une depense il faut un montant positif');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_dep_montant_budgetaire<=my_somme+my_dpco_montant_budgetaire THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, my_ecd_ordre);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Corriger.upd_engage_reste_planco(my_eng_id);

            Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dpco_montant_budgetaire;
        END LOOP;
   END;
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI';
                      -- Ajout du vlco_id max --
                      --AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                        --    WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;
         
         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then
            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
            update commande set comm_reference=my_org_ub||'/'||my_org_cr||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF')
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id; 
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action 
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


   /****************************************************************************/
   PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
        AND eana_montant_budgetaire_reste>0;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id /*AND eana_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eana_reste>my_reste+my_dana_bud then
           my_somme:=my_eana_reste-my_reste-my_dana_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE 
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_can_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE
              WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id  AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id /*AND econ_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE 
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;
              
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE 
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO 
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;
        
        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decalé un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;
           
           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;
            
           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then 
                 my_pourcentage:=0; 
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/


CREATE OR REPLACE FORCE VIEW jefy_depense.v_depense_a_transferer (c_banque,
                                                                  c_guichet,
                                                                  no_compte,
                                                                  iban,
                                                                  bic,
                                                                  cle_rib,
                                                                  domiciliation,
                                                                  mod_libelle,
                                                                  mod_code,
                                                                  mod_dom,
                                                                  pers_type,
                                                                  pers_libelle,
                                                                  pers_lc,
                                                                  exe_exercice,
                                                                  org_id,
                                                                  org_ub,
                                                                  org_cr,
                                                                  org_souscr,
                                                                  tcd_ordre,
                                                                  tcd_code,
                                                                  tcd_libelle,
                                                                  dpp_numero_facture,
                                                                  dpp_id,
                                                                  dpp_ht_saisie,
                                                                  dpp_tva_saisie,
                                                                  dpp_ttc_saisie,
                                                                  dpp_date_facture,
                                                                  dpp_date_saisie,
                                                                  dpp_date_reception,
                                                                  dpp_date_service_fait,
                                                                  dpp_nb_piece,
                                                                  utl_ordre,
                                                                  tbo_ordre,
                                                                  dep_id,
                                                                  pco_num,
                                                                  dpco_id,
                                                                  dpco_ht_saisie,
                                                                  dpco_tva_saisie,
                                                                  dpco_ttc_saisie,
                                                                  adr_civilite,
                                                                  adr_nom,
                                                                  adr_prenom,
                                                                  adr_adresse1,
                                                                  adr_adresse2,
                                                                  adr_ville,
                                                                  adr_cp,
                                                                  utlnomprenom,
                                                                  sfnomprenom,
                                                                  tyap_libelle
                                                                 )
AS
   SELECT r.c_banque, r.c_guichet, r.no_compte, r.iban, r.bic, r.cle_rib,
          r.domiciliation, mp.mod_libelle, mp.mod_code, mp.mod_dom,
          p.pers_type, p.pers_libelle, p.pers_lc, e.exe_exercice, eb.org_id,
          o.org_ub, o.org_cr, o.org_souscr, eb.tcd_ordre, tc.tcd_code,
          tc.tcd_libelle, dp.dpp_numero_facture, dp.dpp_id, dp.dpp_ht_saisie,
          dp.dpp_tva_saisie, dp.dpp_ttc_saisie, dp.dpp_date_facture,
          dp.dpp_date_saisie, dp.dpp_date_reception, dp.dpp_date_service_fait,
          dp.dpp_nb_piece, dp.utl_ordre, dcp.tbo_ordre, dcp.dep_id dep_id,
          dcp.pco_num, dcp.dpco_id dpco_id, dcp.dpco_ht_saisie,
          dcp.dpco_tva_saisie, dcp.dpco_ttc_saisie, f.adr_civilite, f.adr_nom,
          f.adr_prenom, f.adr_adresse1, f.adr_adresse2, f.adr_ville, f.adr_cp,
          pu.pers_libelle || ' ' || pu.pers_lc utlnomprenom,
          pa.pers_libelle || ' ' || pa.pers_lc sfnomprenom, tyap.tyap_libelle
     FROM jefy_depense.engage_budget eb,
          jefy_depense.depense_budget db,
          jefy_depense.depense_papier dp,
          jefy_depense.depense_ctrl_planco dcp,
          grhum.v_fournis_grhum f,
          jefy_admin.exercice e,
          jefy_admin.utilisateur u,
          grhum.personne p,
          grhum.personne pu,
          grhum.personne pa,
          jefy_depense.v_rib_fournisseur r,
          maracuja.mode_paiement mp,
          jefy_admin.type_credit tc,
          jefy_admin.organ o,
          jefy_admin.type_application tyap
    WHERE eb.eng_id = db.eng_id
      AND db.dpp_id = dp.dpp_id
      AND db.dep_id = dcp.dep_id
      AND dp.fou_ordre = f.fou_ordre
      AND dp.exe_ordre = e.exe_ordre
      AND dp.utl_ordre = u.utl_ordre
      AND p.pers_id = u.pers_id
      AND dp.dpp_sf_pers_id = pa.pers_id(+)
      AND dp.rib_ordre = r.rib_ordre(+)
      AND mp.mod_ordre = dp.mod_ordre
      AND eb.tcd_ordre = tc.tcd_ordre
      AND o.org_id = eb.org_id
      AND u.pers_id = pu.pers_id
      AND man_id IS NULL
      AND eb.tyap_id = tyap.tyap_id
      AND tbo_ordre IN (1, 2, 4);
/



CREATE OR REPLACE FUNCTION JEFY_DEPENSE.Get_ribs (
      a_fou_ordre v_rib_fournisseur.fou_ordre%TYPE)
     RETURN varchar2
   IS
     cursor ribs is select * from v_rib_fournisseur 
        where fou_ordre=a_fou_ordre and rib_valide='O';
     my_rib v_rib_fournisseur%rowtype;
     my_chaine varchar2(5000);
   BEGIN
        my_chaine:='';
	    OPEN ribs();
  		LOOP
		    FETCH  ribs INTO my_rib;
		    EXIT WHEN ribs%NOTFOUND;
            if my_chaine is not null and length(my_chaine)>0 then my_chaine:=my_chaine||','; end if;
			my_chaine:=my_chaine||my_rib.c_banque||' '||my_rib.c_guichet||' '||my_rib.no_compte||' '||my_rib.cle_rib||' '||my_rib.rib_titco;

		END LOOP;
        CLOSE ribs;
        
        return my_chaine;
   END;
/


CREATE OR REPLACE PACKAGE JEFY_DEPENSE.INDICATION_ERREUR  IS

-- procedure de traitement a faire apres le visa.

FUNCTION utilisateur (a_utl_ordre    jefy_admin.utilisateur.utl_ordre%TYPE) RETURN VARCHAR2;
FUNCTION organ       (a_org_id       jefy_admin.organ.org_id%TYPE) RETURN VARCHAR2;
FUNCTION taux_prorata(a_tap_id       jefy_admin.taux_prorata.tap_id%TYPE) RETURN VARCHAR2;
FUNCTION type_credit (a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE) RETURN VARCHAR2;
FUNCTION action      (a_tyac_id      jefy_admin.lolf_nomenclature_depense.lolf_id%TYPE) RETURN VARCHAR2;
FUNCTION analytique  (a_can_id       jefy_admin.code_analytique.can_id%TYPE) RETURN VARCHAR2;
FUNCTION convention  (a_conv_ordre   jefy_depense.v_convention.conv_ordre%TYPE) RETURN VARCHAR2;
FUNCTION code_exer   (a_ce_ordre     jefy_marches.code_exer.ce_ordre%TYPE) RETURN VARCHAR2;
FUNCTION fournisseur (a_fou_ordre    jefy_depense.v_fournisseur.fou_ordre%TYPE) RETURN VARCHAR2;
FUNCTION attribution (a_att_ordre    jefy_marches.attribution.att_ordre%TYPE) RETURN VARCHAR2;
FUNCTION lot         (a_lot_ordre    jefy_marches.lot.lot_ordre%TYPE) RETURN VARCHAR2;
FUNCTION imputation  (a_pco_num      jefy_depense.v_plan_comptable.pco_num%TYPE) RETURN VARCHAR2;
FUNCTION exercice    (a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE) RETURN VARCHAR2;
FUNCTION ecriture    (a_ecd_ordre    jefy_depense.v_ecriture_detail.ecd_ordre%TYPE) RETURN VARCHAR2;
FUNCTION mode_paiement(a_mod_ordre   maracuja.mode_paiement.mod_ordre%TYPE) RETURN VARCHAR2;
FUNCTION rib         (a_rib_ordre    V_rib_fournisseur.rib_ordre%TYPE) RETURN VARCHAR2;
FUNCTION engagement  (a_eng_id       jefy_depense.engage_budget.eng_id%TYPE) RETURN VARCHAR2;
FUNCTION commande    (a_comm_id      jefy_depense.commande.comm_id%TYPE) RETURN VARCHAR2;
FUNCTION dep_papier  (a_dpp_id       jefy_depense.depense_papier.dpp_id%TYPE) RETURN VARCHAR2;

END;
/



CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.INDICATION_ERREUR
IS

   FUNCTION utilisateur (a_utl_ordre jefy_admin.utilisateur.utl_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_prenom      grhum.individu_ulr.prenom%type;
       my_nom_usuel   grhum.individu_ulr.nom_usuel%type;
   BEGIN
        select count(*) into my_nb from grhum.individu_ulr i, jefy_admin.utilisateur u where i.no_individu=u.no_individu and u.utl_ordre=a_utl_ordre;
        if my_nb<>1 then return 'utilisateur:'||a_utl_ordre; end if;
           
        select i.nom_usuel, i.prenom into my_nom_usuel, my_prenom from grhum.individu_ulr i, jefy_admin.utilisateur u 
           where i.no_individu=u.no_individu and u.utl_ordre=a_utl_ordre; 
        return 'utilisateur:'||my_prenom||' '||my_nom_usuel||' ['||a_utl_ordre||']';
   END;

   FUNCTION organ (a_org_id jefy_admin.organ.org_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_organ       jefy_admin.organ%rowtype;
   BEGIN
        select count(*) into my_nb from jefy_admin.organ where org_id=a_org_id;
        if my_nb<>1 then return 'ligne:'||a_org_id; end if;
           
        select * into my_organ from jefy_admin.organ where org_id=a_org_id; 
        return 'ligne:'||my_organ.org_ub||'/'||my_organ.org_cr||'/'||nvl(my_organ.org_souscr,'')||' ['||a_org_id||']';
   END;

   FUNCTION taux_prorata (a_tap_id jefy_admin.taux_prorata.tap_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_tap_taux    jefy_admin.taux_prorata.tap_taux%type;
   BEGIN
        select count(*) into my_nb from jefy_admin.taux_prorata where tap_id=a_tap_id;
        if my_nb<>1 then return 'prorata:'||a_tap_id; end if;
           
        select tap_taux into my_tap_taux from jefy_admin.taux_prorata where tap_id=a_tap_id; 
        return 'prorata:'||my_tap_taux||' ['||a_tap_id||']';
   END;

   FUNCTION type_credit (a_tcd_ordre jefy_admin.type_credit.tcd_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_tcd_code    jefy_admin.type_credit.tcd_code%type;
   BEGIN
        select count(*) into my_nb from jefy_admin.type_credit where tcd_ordre=a_tcd_ordre;
        if my_nb<>1 then return 'type credit:'||a_tcd_ordre; end if;
           
        select tcd_code into my_tcd_code from jefy_admin.type_credit where tcd_ordre=a_tcd_ordre; 
        return 'type credit:'||my_tcd_code||' ['||a_tcd_ordre||']';
   END;

   FUNCTION action (a_tyac_id jefy_admin.lolf_nomenclature_depense.lolf_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_tyac_code    jefy_admin.lolf_nomenclature_depense.lolf_code%type;
   BEGIN
        select count(*) into my_nb from jefy_admin.lolf_nomenclature_depense where lolf_id=a_tyac_id;
        if my_nb<>1 then return 'action:'||a_tyac_id; end if;
           
        select lolf_code into my_tyac_code from jefy_admin.lolf_nomenclature_depense where lolf_id=a_tyac_id; 
        return 'action:'||my_tyac_code||' ['||a_tyac_id||']';
   END;

   FUNCTION analytique (a_can_id jefy_admin.code_analytique.can_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_can_code    jefy_admin.code_analytique.can_code%type;
   BEGIN
        select count(*) into my_nb from jefy_admin.code_analytique where can_id=a_can_id;
        if my_nb<>1 then return 'analytique:'||a_can_id; end if;
           
        select can_code into my_can_code from jefy_admin.code_analytique where can_id=a_can_id; 
        return 'analytique:'||my_can_code||' ['||a_can_id||']';
   END;

   FUNCTION convention (a_conv_ordre jefy_depense.v_convention.conv_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb             integer;
       my_conv_reference jefy_depense.v_convention.conv_reference%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.v_convention where conv_ordre=a_conv_ordre;
        if my_nb<>1 then return 'convention:'||a_conv_ordre; end if;
           
        select conv_reference into my_conv_reference from jefy_depense.v_convention where conv_ordre=a_conv_ordre; 
        return 'convention:'||my_conv_reference||' ['||a_conv_ordre||']';
   END;

   FUNCTION code_exer (a_ce_ordre jefy_marches.code_exer.ce_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_cm_code     jefy_marches.code_marche.cm_code%type;
   BEGIN
        select count(*) into my_nb from jefy_marches.code_exer e, jefy_marches.code_marche m where e.cm_ordre=m.cm_ordre and e.ce_ordre=a_ce_ordre;
        if my_nb<>1 then return 'nomenclature:'||a_ce_ordre; end if;
           
        select m.cm_code into my_cm_code from jefy_marches.code_exer e, jefy_marches.code_marche m where e.cm_ordre=m.cm_ordre and e.ce_ordre=a_ce_ordre; 
        return 'nomenclature:'||my_cm_code||' ['||a_ce_ordre||']';
   END;

   FUNCTION fournisseur (a_fou_ordre jefy_depense.v_fournisseur.fou_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_fou_nom    jefy_depense.v_fournisseur.fou_nom%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.v_fournisseur where fou_ordre=a_fou_ordre;
        if my_nb<>1 then return 'fournisseur:'||a_fou_ordre; end if;
           
        select fou_nom into my_fou_nom from jefy_depense.v_fournisseur where fou_ordre=a_fou_ordre; 
        return 'fournisseur:'||my_fou_nom||' ['||a_fou_ordre||']';
   END;

   FUNCTION attribution (a_att_ordre jefy_marches.attribution.att_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_exe_ordre   jefy_marches.marche.exe_ordre%type;
       my_mar_index   jefy_marches.marche.mar_index%type;
       my_lot_index   jefy_marches.lot.lot_index%type;
   BEGIN
        select count(*) into my_nb from jefy_marches.marche m, jefy_marches.lot l, jefy_marches.attribution a 
           where m.mar_ordre=l.mar_ordre and l.lot_ordre=a.lot_ordre and a.att_ordre=a_att_ordre;
        if my_nb<>1 then return 'attribution:'||a_att_ordre; end if;
           
        select m.exe_ordre, m.mar_index, l.lot_index into my_exe_ordre, my_mar_index, my_lot_index 
           from jefy_marches.marche m, jefy_marches.lot l, jefy_marches.attribution a 
           where m.mar_ordre=l.mar_ordre and l.lot_ordre=a.lot_ordre and a.att_ordre=a_att_ordre;
        return 'attribution:('||my_exe_ordre||'/'||my_mar_index||')'||my_lot_index||' ['||a_att_ordre||']';
   END;

   FUNCTION lot (a_lot_ordre jefy_marches.lot.lot_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_exe_ordre   jefy_marches.marche.exe_ordre%type;
       my_mar_index   jefy_marches.marche.mar_index%type;
       my_lot_index   jefy_marches.lot.lot_index%type;
   BEGIN
        select count(*) into my_nb from jefy_marches.marche m, jefy_marches.lot l where m.mar_ordre=l.mar_ordre and l.lot_ordre=a_lot_ordre;
        if my_nb<>1 then return 'lot:'||a_lot_ordre; end if;
           
        select m.exe_ordre, m.mar_index, l.lot_index into my_exe_ordre, my_mar_index, my_lot_index 
           from jefy_marches.marche m, jefy_marches.lot l where m.mar_ordre=l.mar_ordre and l.lot_ordre=a_lot_ordre;
        return 'lot:('||my_exe_ordre||'/'||my_mar_index||')'||my_lot_index||' ['||a_lot_ordre||']';
   END;

   FUNCTION imputation (a_pco_num jefy_depense.v_plan_comptable.pco_num%TYPE) RETURN VARCHAR2
   IS
   BEGIN
        return 'imputation:'||a_pco_num;
   END;

   FUNCTION exercice (a_exe_ordre jefy_admin.exercice.exe_ordre%TYPE) RETURN VARCHAR2
   IS
   BEGIN
        return 'exercice:'||a_exe_ordre;
   END;

   FUNCTION ecriture (a_ecd_ordre jefy_depense.v_ecriture_detail.ecd_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_ecr_numero  jefy_depense.v_ecriture_detail.ecr_numero%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.v_ecriture_detail where ecd_ordre=a_ecd_ordre;
        if my_nb<>1 then return 'ecriture:'||a_ecd_ordre; end if;
           
        select ecr_numero into my_ecr_numero from jefy_depense.v_ecriture_detail where ecd_ordre=a_ecd_ordre; 
        return 'ecriture:'||my_ecr_numero||' ['||a_ecd_ordre||']';
   END;

   FUNCTION mode_paiement(a_mod_ordre   maracuja.mode_paiement.mod_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_mod_code    maracuja.mode_paiement.mod_code%type;
   BEGIN
        select count(*) into my_nb from maracuja.mode_paiement where mod_ordre=a_mod_ordre;
        if my_nb<>1 then return 'mode paiement:'||a_mod_ordre; end if;
           
        select mod_code into my_mod_code from maracuja.mode_paiement where mod_ordre=a_mod_ordre; 
        return 'mode paiement:'||my_mod_code||' ['||a_mod_ordre||']';
   END;

   FUNCTION rib(a_rib_ordre V_rib_fournisseur.rib_ordre%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_c_banque    V_rib_fournisseur.c_banque%type;
       my_c_guichet   V_rib_fournisseur.c_guichet%type;
       my_no_compte   V_rib_fournisseur.no_compte%type;
   BEGIN
        select count(*) into my_nb from V_rib_fournisseur where rib_ordre=a_rib_ordre;
        if my_nb<>1 then return 'rib:'||a_rib_ordre; end if;
           
        select c_banque, c_guichet, no_compte into my_c_banque, my_c_guichet, my_no_compte from V_rib_fournisseur where rib_ordre=a_rib_ordre; 
        return 'rib:'||my_c_banque||' '||my_c_guichet||' '||my_no_compte||' ['||a_rib_ordre||']';
   END;

   FUNCTION engagement (a_eng_id jefy_depense.engage_budget.eng_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_eng_numero  jefy_depense.engage_budget.eng_numero%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.engage_budget where eng_id=a_eng_id;
        if my_nb<>1 then return 'engagement:'||a_eng_id; end if;
           
        select eng_numero into my_eng_numero from jefy_depense.engage_budget where eng_id=a_eng_id; 
        return 'engagement:'||my_eng_numero||' ['||a_eng_id||']';
   END;

   FUNCTION commande (a_comm_id jefy_depense.commande.comm_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_comm_numero jefy_depense.commande.comm_numero%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.commande where comm_id=a_comm_id;
        if my_nb<>1 then return 'commande:'||a_comm_id; end if;
           
        select comm_numero into my_comm_numero from jefy_depense.commande where comm_id=a_comm_id; 
        return 'commande:'||my_comm_numero||' ['||a_comm_id||']';
   END;

   FUNCTION dep_papier (a_dpp_id jefy_depense.depense_papier.dpp_id%TYPE) RETURN VARCHAR2
   IS
       my_nb          integer;
       my_dpp_numero  jefy_depense.depense_papier.dpp_numero_facture%type;
   BEGIN
        select count(*) into my_nb from jefy_depense.depense_papier where dpp_id=a_dpp_id;
        if my_nb<>1 then return 'facture:'||a_dpp_id; end if;
           
        select dpp_numero_facture into my_dpp_numero from jefy_depense.depense_papier where dpp_id=a_dpp_id; 
        return 'facture:'||my_dpp_numero||' ['||a_dpp_id||']';
   END;

END;
/







create procedure grhum.inst_patch_jefy_depense_2042 is
begin
    insert into jefy_depense.db_version values (2042,'2042',sysdate,sysdate,null);           
end;



 









