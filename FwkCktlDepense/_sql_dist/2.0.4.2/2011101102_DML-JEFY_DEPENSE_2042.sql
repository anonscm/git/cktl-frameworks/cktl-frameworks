--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 2.0.4.2 
-- Date de publication : 11/10/2011 
-- Licence : CeCILL version 2
--
--

----------------------------------------------
-- 
-- 
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

	execute grhum.inst_patch_jefy_depense_2042;
commit;

drop procedure grhum.inst_patch_jefy_depense_2042;