SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.5.5
-- Date de publication : 23/03/2012
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- correction package Reverser
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2054';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/



CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Reverser
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_reverse_papier (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpco_ht_saisie          DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
      my_dpco_ttc_saisie         DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
      my_sum_rev_ht              DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
      my_sum_rev_ttc          DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
      my_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE;
      my_dpp_id_reversement   DEPENSE_PAPIER.dpp_id_reversement%TYPE;
      my_nb       INTEGER;
      my_nb_verif    integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);

        -- verifier qu'on n'est pas sur une prestation interne
        select count(*) into my_nb from engage_budget eb, depense_budget db, jefy_admin.type_application ta
        where dpp_id=a_dpp_id_reversement
        and eb.eng_id=db.eng_id
        and eb.tyap_id=ta.tyap_id
        and ta.TYAP_STRID='PRESTATION INTERNE';
        if (my_nb>0) then
          RAISE_APPLICATION_ERROR(-20001, 'Impossible de créer un ordre de reversement sur une dépense rattachée à une prestation interne.');  
        end if;
        

        -- pour un ORV il faut des montants negatifs.
           IF a_dpp_ht_initial>0 OR a_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dpp_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture initiale n''existe pas (dpp_id='||a_dpp_id_reversement||')');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(a_dpp_ht_initial)>ABS(a_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(a_dpp_ht_initial, a_dpp_ttc_initial);

        -- on verifie que la facture referencee n'est pas un ORV (pas d'ORV sur un ORV);
         SELECT dpp_id_reversement, mod_ordre INTO my_dpp_id_reversement, my_mod_ordre
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;

        IF my_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ordre de reversement a partir d''un autre ORV');
        END IF;





        -- verifier que les montants du total des ORV ne depassent pas ceux mandates et vises de la facture d'origine.
        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dpco_ht_saisie, my_dpco_ttc_saisie
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET
              WHERE dpp_id=a_dpp_id_reversement) AND dpco_ttc_saisie>0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
           FROM DEPENSE_CTRL_PLANCO WHERE dpco_ttc_saisie<0 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE
             dpp_id IN (SELECT dpp_id FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id_reversement));

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id_reversement)
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dpco_ht_saisie<ABS(my_sum_rev_ht)+ABS(a_dpp_ht_initial) OR
           my_dpco_ttc_saisie<ABS(my_sum_rev_ttc)+ABS(a_dpp_ttc_initial)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui vise de la facture initiale');
        END IF;
        
           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
           0, 0, a_fou_ordre, a_rib_ordre, my_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
           a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
           a_dpp_ht_initial, my_dpp_tva_initial, a_dpp_ttc_initial,null,null,null,null,null,null);
   END;

   PROCEDURE ins_reverse (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
      my_org_id              engage_budget.org_id%type;
   BEGIN
          IF a_dep_ttc_saisie<>0 THEN
             select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
               
             -- verifier qu'on a le droit de liquider sur cet exercice.
             Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

             -- lancement des differentes procedures d'insertion des tables de depense.
             ins_reverse_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie,
                a_tap_id, a_utl_ordre, a_dep_id_reversement);

              UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+a_dep_ht_saisie,
                  dpp_tva_saisie=dpp_tva_saisie+a_dep_ttc_saisie-a_dep_ht_saisie,
                  dpp_ttc_saisie=dpp_ttc_saisie+a_dep_ttc_saisie
                 WHERE dpp_id=a_dpp_id;

             ins_reverse_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
             ins_reverse_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
             ins_reverse_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
             ins_reverse_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
             ins_reverse_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);
             ins_reverse_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Corriger.upd_engage_reste(a_eng_id);

             -- on verifie la coherence des montants entre les differents depense_.
             Verifier.verifier_depense_coherence(a_dep_id);
             Verifier.verifier_depense_pap_coherence(a_dpp_id);
             -- on verifie si la coherence des montants budgetaires restant est  conservee.
             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_reverse (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                            INTEGER;
      my_exe_ordre                    DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dpp_id                        DEPENSE_BUDGET.dpp_id%TYPE;
      my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dpco_id                    DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
      my_org_id              engage_budget.org_id%type;
      my_eng_id              engage_budget.eng_id%type;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
   BEGIN

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ORV n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT  d.exe_ordre, d.dep_montant_budgetaire, d.dpp_id, d.dep_ht_saisie, d.dep_ttc_saisie, d.eng_id
           INTO my_exe_ordre, my_montant_budgetaire, my_dpp_id, my_dep_ht_saisie, my_dep_ttc_saisie, my_eng_id
           FROM DEPENSE_BUDGET d WHERE d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        select org_id into my_org_id from engage_budget where eng_id=my_eng_id;
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si c'est un ORV.
           IF my_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- tout est bon ... on supprime la depense.
        log_reverse_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

        Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);
   END;

      PROCEDURE del_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE)
      IS
       my_nb         INTEGER;
       my_exe_ordre     DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dpp_ttc_initial  DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_reverse_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

      PROCEDURE log_reverse_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id                    Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
           INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_reverse_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                 DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                 DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement     DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_nb                 INTEGER;
       my_nb_verif    integer;

       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;
       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_exe_ordre       ENGAGE_BUDGET.exe_ordre%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

       my_dpp_ht_initial         DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial     DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial     DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dpp_ttc_saisie     DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
       my_dpp_id_reversement DEPENSE_PAPIER.dpp_id_reversement%TYPE;
       my_dpp_id_origine DEPENSE_BUDGET.dpp_id%TYPE;

       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN

           -- pour un ORV il faut des montants negatifs.
           IF a_dep_ht_saisie>0 OR a_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dep_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture initiale n''existe pas (dep_id='||a_dep_id_reversement||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT org_id, exe_ordre INTO my_org_id, my_exe_ordre FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
          SELECT exe_ordre, dpp_id_reversement INTO my_dpp_exe_ordre, my_dpp_id_reversement
         FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ORV doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        SELECT exe_ordre, eng_id, tap_id, dep_id_reversement, dpp_id
          INTO my_dep_exe_ordre, my_eng_id, my_dep_tap_id, my_dep_id_reversement, my_dpp_id_origine
          FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;

        IF my_dpp_id_reversement<>my_dpp_id_origine THEN
           RAISE_APPLICATION_ERROR(-20001,'la facture papier d''origine est differente de celle de la depense budget d''origine');
        END IF;

        IF a_eng_id<>my_eng_id THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ordre de reversement doit etre sur le meme engagement que la facture initiale');
        END IF;

        IF my_dep_exe_ordre<>my_dep_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit etre sur le meme exercice que la facture initiale');
        END IF;

        IF a_tap_id<>my_dep_tap_id THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit avoir le meme prorata que la facture initiale.');
        END IF;

        IF my_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ORV a partir d''un autre ORV.');
        END IF;

        -- verification des coherences entre les sommes (si elles ne depassent pas).
        SELECT dpp_ttc_saisie
               INTO my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0) , NVL(SUM(dep_ttc_saisie),0)
               INTO my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        -- on verifie la coherence des montants.
         IF ABS(a_dep_ht_saisie)>ABS(a_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste si le ttc de la depense papier est du meme signe que celui de depense_budget.
        IF my_dpp_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Incoherence de signe entre le montant de la facture et le montant budgetaire');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(a_dep_ht_saisie, a_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declaré dans la papier.
        /*IF ABS(my_sum_ht_saisie+a_dep_ht_saisie) > ABS(my_dpp_ht_saisie) OR
           ABS(my_sum_tva_saisie+my_dep_tva_saisie) > ABS(my_dpp_tva_saisie) OR
           ABS(my_sum_ttc_saisie+a_dep_ttc_saisie) > ABS(my_dpp_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        END IF;*/

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              a_dep_ht_saisie,a_dep_ttc_saisie);

        -- on verifie que la somme des ORV ne depasse pas le montant de la facture initiale ... mandate et vise !!.
          SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dep_ht_init, my_dep_ttc_init
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id_reversement AND dpco_montant_budgetaire>=0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
          FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id_reversement;

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id=a_dep_id_reversement
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dep_ht_init < ABS(my_sum_rev_ht) + ABS(a_dep_ht_saisie) OR
           my_dep_ttc_init < ABS(my_sum_rev_ttc) + ABS(a_dep_ttc_saisie)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui de la facture initiale');
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

        -- on reverse pour liberer les credits.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           a_dep_ht_saisie, my_dep_tva_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_reverse_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dact_id                  DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                  DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_montant_budgetaire DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_ht_saisie          DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       sum_dact_ttc_saisie         DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dact_ht_saisie           DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie          DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie          DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                DEPENSE_BUDGET.utl_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            -- Pour les O.R.
            IF my_dact_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un ORV.');
            END IF;

            -- verifier que l'action qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR pour cette action et que l'action existe bien au depart.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ACTION
               WHERE tyac_id=my_tyac_id
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet ORV et cette action n''est pas autorisee (tyac_id='||my_tyac_id||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dact_montant_budgetaire) THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dact_montant_budgetaire),0), NVL(SUM(dact_ht_saisie),0), NVL(SUM(dact_ttc_saisie),0)
                  INTO sum_dact_montant_budgetaire, sum_dact_ht_saisie, sum_dact_ttc_saisie
              FROM DEPENSE_CTRL_ACTION WHERE tyac_id=my_tyac_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            if my_dact_ht_saisie=-sum_dact_ht_saisie and my_dact_ttc_saisie=-sum_dact_ttc_saisie and my_dact_montant_budgetaire<>-sum_dact_montant_budgetaire then
                my_dact_montant_budgetaire:=-sum_dact_montant_budgetaire;
            end if;

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dact_montant_budgetaire+my_dact_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette action (tyac_id='||my_tyac_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id, a_exe_ordre, a_dep_id, my_tyac_id,
                my_dact_montant_budgetaire, my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dana_id                  DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                   DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_montant_budgetaire DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_ht_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       sum_dana_ttc_saisie         DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dana_ht_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie          DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            -- Pour les O.R.
            IF my_dana_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ANALYTIQUE
               WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code analytique n''est pas autorise pour cette depense (can_id='||my_can_id||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dana_montant_budgetaire) THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dana_montant_budgetaire),0), NVL(SUM(dana_ht_saisie),0), NVL(SUM(dana_ttc_saisie),0)
                  INTO sum_dana_montant_budgetaire, sum_dana_ht_saisie, sum_dana_ttc_saisie
               FROM DEPENSE_CTRL_ANALYTIQUE WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);
               
            if my_dana_ht_saisie=-sum_dana_ht_saisie and my_dana_ttc_saisie=-sum_dana_ttc_saisie and my_dana_montant_budgetaire<>-sum_dana_montant_budgetaire then
                my_dana_montant_budgetaire:=-sum_dana_montant_budgetaire;
            end if;

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dana_montant_budgetaire+my_dana_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code analytique (can_id='||my_can_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

      PROCEDURE ins_reverse_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                  DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre               DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_montant_budgetaire DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_ht_saisie          DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       sum_dcon_ttc_saisie         DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dcon_ht_saisie           DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie          DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie          DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere la convention.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            -- Pour les O.R.
            IF my_dcon_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_CONVENTION
               WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette convention n''est pas autorisee pour cette depense (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dcon_montant_budgetaire) THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dcon_montant_budgetaire),0), NVL(SUM(dcon_ht_saisie),0), NVL(SUM(dcon_ttc_saisie),0)
                  INTO sum_dcon_montant_budgetaire, sum_dcon_ht_saisie, sum_dcon_ttc_saisie
               FROM DEPENSE_CTRL_CONVENTION WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);

            if my_dcon_ht_saisie=-sum_dcon_ht_saisie and my_dcon_ttc_saisie=-sum_dcon_ttc_saisie and my_dcon_montant_budgetaire<>-sum_dcon_montant_budgetaire then
                my_dcon_montant_budgetaire:=-sum_dcon_montant_budgetaire;
            end if;

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dcon_montant_budgetaire+my_dcon_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cet engagement pour cette convention (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id        DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine        VARCHAR2
   ) IS
       my_dhom_id                  DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                  DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                 DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_montant_budgetaire DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_ht_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       sum_dhom_ttc_saisie         DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dhom_ht_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie          DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb                       INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                    DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
            IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_çchat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            -- Pour les O.R.
            IF my_dhom_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE
               WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code nomenclature n''est pas autorise pour cette depense (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
            IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dhom_montant_budgetaire) THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dhom_montant_budgetaire),0), NVL(SUM(dhom_ht_saisie),0), NVL(SUM(dhom_ttc_saisie),0)
                  INTO sum_dhom_montant_budgetaire, sum_dhom_ht_saisie, sum_dhom_ttc_saisie
              FROM DEPENSE_CTRL_HORS_MARCHE WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            if my_dhom_ht_saisie=-sum_dhom_ht_saisie and my_dhom_ttc_saisie=-sum_dhom_ttc_saisie and my_dhom_montant_budgetaire<>-sum_dhom_montant_budgetaire then
                my_dhom_montant_budgetaire:=-sum_dhom_montant_budgetaire;
            end if;
            
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dhom_montant_budgetaire+my_dhom_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code nomenclature (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       sum_dmar_montant_budgetaire DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            -- Pour les O.R.
            IF my_dmar_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE
               WHERE att_ordre=my_att_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                  OR dep_id_reversement=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette attribution n''est pas autorisee pour cette depense (att_ordre='||my_att_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dmar_montant_budgetaire) THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dmar_montant_budgetaire),0) INTO sum_dmar_montant_budgetaire
              FROM DEPENSE_CTRL_MARCHE WHERE att_ordre=my_att_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dmar_montant_budgetaire+my_dmar_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette attribution (att_ordre='||my_att_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       sum_dpco_montant_budgetaire DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_inventaires    VARCHAR2(30000);
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre,my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            -- Pour les O.R.
            IF my_dpco_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO
               WHERE pco_num=my_pco_num
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette imputation il n''existe pas d''equivalent pour cette depense (pco_num='||my_pco_num||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dpco_montant_budgetaire) THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dpco_montant_budgetaire),0) INTO sum_dpco_montant_budgetaire
              FROM DEPENSE_CTRL_PLANCO WHERE pco_num=my_pco_num
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dpco_montant_budgetaire+my_dpco_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette imputation (pco_num='||my_pco_num||')');
            END IF;


            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;


            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, NULL);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_dpco_montant_budgetaire;
		END LOOP;
   END;

END;
/



create procedure grhum.inst_patch_jefy_depense_2055 is
begin
    insert into jefy_depense.db_version values (2055,'2055',to_date('23/03/2012','dd/mm/yyyy'),sysdate,null);           
end;

