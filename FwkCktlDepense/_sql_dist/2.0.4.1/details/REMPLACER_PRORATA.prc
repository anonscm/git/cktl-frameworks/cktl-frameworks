CREATE OR REPLACE PROCEDURE JEFY_DEPENSE.remplacer_prorata(
   a_exe_ordre     jefy_admin.exercice.exe_ordre%type,
   a_old_tap_id    jefy_admin.taux_prorata.tap_id%type,
   a_new_tap_id    jefy_admin.taux_prorata.tap_id%type)
IS
           CURSOR liste IS SELECT * FROM  ENGAGE_BUDGET WHERE exe_ordre=a_exe_ordre and eng_montant_budgetaire_reste>0 AND tap_id=a_old_tap_id;
		   engage ENGAGE_BUDGET%ROWTYPE;
		   engageselect ENGAGE_BUDGET%ROWTYPE;
		   bud ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

           CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
		   depense DEPENSE_BUDGET%ROWTYPE;
		   depbud DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

      	   CURSOR listeaction(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		   action COMMANDE_CTRL_ACTION%ROWTYPE;
      	   CURSOR listeanalytique(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		   ANALYTIQUE COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;
      	   CURSOR listeconvention(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		   convention COMMANDE_CTRL_CONVENTION%ROWTYPE;
      	   CURSOR listehors_marche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		   hors_marche COMMANDE_CTRL_HORS_MARCHE%ROWTYPE;
      	   CURSOR listemarche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		   marche COMMANDE_CTRL_MARCHE%ROWTYPE;
      	   CURSOR listeplanco(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		   planco COMMANDE_CTRL_PLANCO%ROWTYPE;
		   
		   my_nb INTEGER;
		   cdebudget COMMANDE_BUDGET%ROWTYPE;
		   chaine_action  VARCHAR2(3000);
		   chaine_analytique  VARCHAR2(3000);
		   chaine_convention  VARCHAR2(3000);
		   chaine_hors_marche  VARCHAR2(3000);
		   chaine_marche  VARCHAR2(3000);
		   chaine_planco  VARCHAR2(3000);
BEGIN
           OPEN liste();
 		   LOOP
		      FETCH  liste INTO engage;
		      EXIT WHEN liste%NOTFOUND;
			  
              -- si l''engagement n''a pas de TVA, le changement de prorata ne change pas le budgetaire
              if engage.eng_tva_saisie<>0 then
			     -- correction de l'engagement
			     bud:=Budget.calculer_budgetaire(engage.exe_ordre, a_new_tap_id, engage.org_id, engage.eng_ht_saisie, engage.eng_ttc_saisie);
dbms_output.put_line('bud='||bud);
			     depbud:=0;
                 OPEN listedepense(engage.eng_id);
 	             LOOP
		            FETCH  listedepense INTO depense;
		            EXIT WHEN listedepense%NOTFOUND;
				   
				    depbud:=depbud+Budget.calculer_budgetaire(depense.exe_ordre, a_new_tap_id, engage.org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
dbms_output.put_line('depbud='||depbud);
                 END LOOP;
			     CLOSE listedepense;
			  
			     UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=bud, eng_montant_budgetaire_reste=bud-depbud, tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
select * into engageselect from engage_budget where eng_id=engage.eng_id;
dbms_output.put_line('new eng, bud='||engageselect.eng_montant_budgetaire||', reste='||engageselect.eng_montant_budgetaire_reste);
                 engager.UPD_ENGAGE(engage.eng_id, engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.utl_ordre,
                        get_chaine_action(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_analytique(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_convention(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id), 
                        get_chaine_hors_marche(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_marche(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id), 
                        get_chaine_planco_eng(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id));

select * into engageselect from engage_budget where eng_id=engage.eng_id;
dbms_output.put_line('new eng, bud='||engageselect.eng_montant_budgetaire||', reste='||engageselect.eng_montant_budgetaire_reste);
                 
			     Corriger.upd_engage_reste(engage.eng_id);
                 verifier.verifier_engage_coherence(engage.eng_id);
			     Budget.maj_budget(engage.exe_ordre, engage.org_id, engage.tcd_ordre);
             	  
  			     -- correction du commande_budget associe ... il doit normalement y en avoir un seul
			     SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id;
			  
			     IF my_nb>0 THEN
			   
			        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
				 
				    IF my_nb=1 THEN
				      SELECT * INTO cdebudget FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			                 AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				             AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
					  
					  chaine_action:='';
 	 	              OPEN listeaction(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeaction INTO action;
		      		       EXIT WHEN listeaction%NOTFOUND;
					  
					  	   chaine_action:=chaine_action||action.tyac_id||'$'||action.cact_ht_saisie||'$'||action.cact_ttc_saisie||'$'||action.cact_pourcentage||'$';
					  END LOOP;
					  CLOSE listeaction;
					  chaine_action:=chaine_action||'$';

					  chaine_analytique:='';
 	 	              OPEN listeanalytique(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeanalytique INTO analytique;
		      		       EXIT WHEN listeanalytique%NOTFOUND;
					  
					  	   chaine_analytique:=chaine_analytique||analytique.can_id||'$'||analytique.cana_ht_saisie||'$'||analytique.cana_ttc_saisie||'$'||analytique.cana_pourcentage||'$';
					  END LOOP;
					  CLOSE listeanalytique;
					  chaine_analytique:=chaine_analytique||'$';

					  chaine_convention:='';
 	 	              OPEN listeconvention(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeconvention INTO convention;
		      		       EXIT WHEN listeconvention%NOTFOUND;
					  
					  	   chaine_convention:=chaine_convention||convention.conv_ordre||'$'||convention.ccon_ht_saisie||'$'||convention.ccon_ttc_saisie||'$'||convention.ccon_pourcentage||'$';
					  END LOOP;
					  CLOSE listeconvention;
					  chaine_convention:=chaine_convention||'$';

					  chaine_hors_marche:='';
 	 	              OPEN listehors_marche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listehors_marche INTO hors_marche;
		      		       EXIT WHEN listehors_marche%NOTFOUND;
					  
					  	   chaine_hors_marche:=chaine_hors_marche||hors_marche.typa_id||'$'||hors_marche.ce_ordre||'$'||hors_marche.chom_ht_saisie||'$'||hors_marche.chom_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listehors_marche;
					  chaine_hors_marche:=chaine_hors_marche||'$';

					  chaine_marche:='';
 	 	              OPEN listemarche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listemarche INTO marche;
		      		       EXIT WHEN listemarche%NOTFOUND;
					  
					  	   chaine_marche:=chaine_marche||marche.att_ordre||'$'||marche.cmar_ht_saisie||'$'||marche.cmar_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listemarche;
					  chaine_marche:=chaine_marche||'$';

					  chaine_planco:='';
 	 	              OPEN listeplanco(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeplanco INTO planco;
		      		       EXIT WHEN listeplanco%NOTFOUND;
					  
					  	   chaine_planco:=chaine_planco||planco.pco_num||'$'||planco.cpco_ht_saisie||'$'||planco.cpco_ttc_saisie||'$'||planco.cpco_pourcentage||'$';
					  END LOOP;
					  CLOSE listeplanco;
					  chaine_planco:=chaine_planco||'$';
					  
					     Commander.upd_commande_budget(cdebudget.cbud_id, cdebudget.cbud_ht_saisie, cdebudget.cbud_ttc_saisie,
					    	  a_new_tap_id, chaine_action, chaine_analytique, chaine_convention, chaine_hors_marche, chaine_marche, chaine_planco);
				    END IF;
				 	 
                 END IF;
              else
                 update COMMANDE_BUDGET SET tap_id=a_new_tap_id WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
                 UPDATE ENGAGE_BUDGET SET tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
              end if;		
			      
		  END LOOP;
		  CLOSE liste;
END;
/
