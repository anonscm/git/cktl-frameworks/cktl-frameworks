CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Engager
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------


   PROCEDURE ins_engage (
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(a_exe_ordre, a_utl_ordre, a_org_id);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_engage_budget(a_eng_id,a_exe_ordre,a_eng_numero,a_fou_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_eng_libelle, a_eng_ht_saisie,a_eng_ttc_saisie,a_tyap_id,a_utl_ordre);

		ins_engage_ctrl_action(a_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(a_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(a_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(a_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(a_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(a_exe_ordre,a_eng_id,a_chaine_planco);

		Verifier.verifier_engage_coherence(a_eng_id);
		Apres_Engage.engage(a_eng_id);
   END;

   PROCEDURE del_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_montant_reste      ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est deja annule (eng_id:'||a_eng_id||')');
	    END IF;

   		SELECT exe_ordre, eng_montant_budgetaire, eng_montant_budgetaire_reste, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_montant_reste, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

   		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on traite le cas d'un engagement dont la somme des liquidations.
		-- est egale a la somme des ordres de reversement ... ce qui fait que l'engagement est reengage.
		--  donc on ne le supprime pas mais on le solde.
		SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb>0 AND my_montant_budgetaire=my_montant_reste THEN
		   solder_engage(a_eng_id, a_utl_ordre);
		ELSE
           Verifier.verifier_util_engage(a_eng_id);

		   log_engage(a_eng_id,a_utl_ordre);

	       DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        END IF;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.del_engage(a_eng_id);
   END;

   PROCEDURE solder_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE solder_engage_sansdroit (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE upd_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
	  my_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	  my_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_tap_id			    ENGAGE_BUDGET.tap_id%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_tva_saisie     ENGAGE_BUDGET.eng_tva_saisie%TYPE;
      my_budgetaire         ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_nb_decimales       NUMBER;
      
      CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
      depense DEPENSE_BUDGET%ROWTYPE;
	  my_dep_ht_saisie		DEPENSE_BUDGET.dep_ht_saisie%TYPE;
	  my_dep_ttc_saisie		DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      
   BEGIN
   		SELECT exe_ordre, tap_id, org_id, tcd_ordre INTO my_exe_ordre, my_tap_id, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_ttc_saisie),0) into my_dep_ht_saisie, my_dep_ttc_saisie
        from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        my_budgetaire:=0;
        if my_dep_ht_saisie<>0 or my_dep_ttc_saisie<>0 then
           if my_dep_ht_saisie=my_dep_ttc_saisie then
               my_budgetaire:=my_dep_ttc_saisie;
           else
               --my_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);
               
               OPEN listedepense(a_eng_id);
 	           LOOP
		           FETCH  listedepense INTO depense;
		           EXIT WHEN listedepense%NOTFOUND;
				   
				    my_budgetaire:=my_budgetaire+Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
                 END LOOP;
			     CLOSE listedepense;
                 
           end if;
        end if;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);
        my_budgetaire:=round(my_budgetaire, my_nb_decimales);

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		log_engage(a_eng_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	    IF my_budgetaire> my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja liquide ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- on modifie les montants.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=my_montant_budgetaire,
		   eng_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   eng_ht_saisie=my_eng_ht_saisie, eng_tva_saisie=my_eng_tva_saisie,
		   eng_ttc_saisie=my_eng_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE eng_id=a_eng_id;

		-- on supprime les anciens.
		DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

		-- on insere les nouveaux.
		ins_engage_ctrl_action(my_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(my_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(my_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(my_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(my_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(my_exe_ordre,a_eng_id,a_chaine_planco);

		-- on met a jour les restes engages des controleurs suivant les depenses.
		Corriger.upd_engage_reste(a_eng_id);

		-- on verifie.
		Verifier.verifier_engage_coherence(a_eng_id);
        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.engage(a_eng_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


   PROCEDURE log_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_zeng_id			Z_ENGAGE_BUDGET.zeng_id%TYPE;
   BEGIN
		SELECT z_engage_budget_seq.NEXTVAL INTO my_zeng_id FROM dual;

		-- engage_budget.
		INSERT INTO Z_ENGAGE_BUDGET SELECT my_zeng_id, SYSDATE, a_utl_ordre, e.*
		  FROM ENGAGE_BUDGET e WHERE eng_id=a_eng_id;

		-- engage_ctrl_action.
		INSERT INTO Z_ENGAGE_CTRL_ACTION SELECT z_engage_ctrl_action_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ACTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_analytique.
		INSERT INTO Z_ENGAGE_CTRL_ANALYTIQUE SELECT z_engage_ctrl_analytique_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ANALYTIQUE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_convention.
		INSERT INTO Z_ENGAGE_CTRL_CONVENTION SELECT z_engage_ctrl_convention_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_CONVENTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_hors_marche.
		INSERT INTO Z_ENGAGE_CTRL_HORS_MARCHE SELECT z_engage_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_HORS_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_marche.
		INSERT INTO Z_ENGAGE_CTRL_MARCHE SELECT z_engage_ctrl_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_planco.
		INSERT INTO Z_ENGAGE_CTRL_PLANCO SELECT z_engage_ctrl_planco_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_PLANCO e WHERE eng_id=a_eng_id;
   END;

   PROCEDURE ins_engage_budget(
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

   		-- lucrativite ??.
		--		UPDATE COMMANDE SET cde_lucrativite=(select org_lucrativite from organ where org_ordre=orgordre) WHERE cde_ordre=cdeordre;

        verifier.verifier_organ(a_org_id, a_tcd_ordre);
		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
	    Verifier.verifier_fournisseur(a_fou_ordre);

		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);

        Budget.maj_budget(a_exe_ordre, a_org_id, a_tcd_ordre);
	    Apres_Engage.Budget(a_eng_id);
   END;

   PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eact_id	               ENGAGE_CTRL_ACTION.eact_id%TYPE;
       my_tyac_id	  	   		   ENGAGE_CTRL_ACTION.tyac_id%TYPE;
       my_eact_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_eact_ht_saisie	  	   ENGAGE_CTRL_ACTION.eact_ht_saisie%TYPE;
       my_eact_tva_saisie		   ENGAGE_CTRL_ACTION.eact_tva_saisie%TYPE;
       my_eact_ttc_saisie		   ENGAGE_CTRL_ACTION.eact_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
	   my_utl_ordre                ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eact_ht_saisie:=round(my_eact_ht_saisie, my_nb_decimales);
            my_eact_ttc_saisie:=round(my_eact_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_tyac_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette action pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eact_ht_saisie<0 OR my_eact_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eact_tva_saisie := my_eact_ttc_saisie - my_eact_ht_saisie;
			IF my_eact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eact_ht_saisie,my_eact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_eact_montant_budgetaire THEN
			    my_eact_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_action_seq.NEXTVAL INTO my_eact_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ACTION VALUES (my_eact_id,
			       a_exe_ordre, a_eng_id, my_tyac_id, my_eact_montant_budgetaire, my_eact_montant_budgetaire,
				   my_eact_ht_saisie, my_eact_tva_saisie, my_eact_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_action(a_eng_id);
            
	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
			Apres_Engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eana_id	               ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
       my_can_id	  	   		   ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_eana_montant_budgetaire  ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_eana_ht_saisie	  	   ENGAGE_CTRL_ANALYTIQUE.eana_ht_saisie%TYPE;
       my_eana_tva_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_tva_saisie%TYPE;
       my_eana_ttc_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eana_ht_saisie:=round(my_eana_ht_saisie, my_nb_decimales);
            my_eana_ttc_saisie:=round(my_eana_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_can_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code analytique pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eana_ht_saisie<0 OR my_eana_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eana_tva_saisie := my_eana_ttc_saisie - my_eana_ht_saisie;
			IF my_eana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eana_ht_saisie,my_eana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_eana_montant_budgetaire THEN
			    my_eana_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_analytique_seq.NEXTVAL INTO my_eana_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ANALYTIQUE VALUES (my_eana_id,
			       a_exe_ordre, a_eng_id, my_can_id, my_eana_montant_budgetaire, my_eana_montant_budgetaire,
				   my_eana_ht_saisie, my_eana_tva_saisie, my_eana_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_analytique(a_eng_id);
            
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_econ_id	               ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
       my_conv_ordre 	   		   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_econ_montant_budgetaire  ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_econ_ht_saisie	  	   ENGAGE_CTRL_CONVENTION.econ_ht_saisie%TYPE;
       my_econ_tva_saisie		   ENGAGE_CTRL_CONVENTION.econ_tva_saisie%TYPE;
       my_econ_ttc_saisie		   ENGAGE_CTRL_CONVENTION.econ_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_econ_ht_saisie:=round(my_econ_ht_saisie, my_nb_decimales);
            my_econ_ttc_saisie:=round(my_econ_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_conv_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette convention pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_econ_ht_saisie<0 OR my_econ_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_econ_tva_saisie := my_econ_ttc_saisie - my_econ_ht_saisie;
			IF my_econ_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_econ_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_econ_ht_saisie,my_econ_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_econ_montant_budgetaire THEN
			    my_econ_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_convention_seq.NEXTVAL INTO my_econ_id FROM dual;

			INSERT INTO ENGAGE_CTRL_CONVENTION VALUES (my_econ_id,
			       a_exe_ordre, a_eng_id, my_conv_ordre, my_econ_montant_budgetaire, my_econ_montant_budgetaire,
				   my_econ_ht_saisie, my_econ_tva_saisie, my_econ_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_convention(a_eng_id);
            
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
	    	Apres_Engage.convention(my_econ_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_econ_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ehom_id	               ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
       my_typa_id	  	   		   ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre	  	   		   ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_ehom_montant_budgetaire  ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_ehom_ht_saisie	  	   ENGAGE_CTRL_HORS_MARCHE.ehom_ht_saisie%TYPE;
       my_ehom_tva_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_tva_saisie%TYPE;
       my_ehom_ttc_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code de nomenclature.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ehom_ht_saisie:=round(my_ehom_ht_saisie, my_nb_decimales);
            my_ehom_ttc_saisie:=round(my_ehom_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
			     WHERE eng_id=a_eng_id AND ce_ordre=my_ce_ordre AND typa_id=my_typa_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code nomenclature pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_ehom_ht_saisie<0 OR my_ehom_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_ehom_tva_saisie := my_ehom_ttc_saisie - my_ehom_ht_saisie;
			IF my_ehom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_ehom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ehom_ht_saisie,my_ehom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_ehom_montant_budgetaire THEN
			    my_ehom_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_hors_marche_seq.NEXTVAL INTO my_ehom_id FROM dual;

			INSERT INTO ENGAGE_CTRL_HORS_MARCHE VALUES (my_ehom_id,
			       a_exe_ordre, a_eng_id, my_typa_id, my_ce_ordre, my_ehom_montant_budgetaire, my_ehom_montant_budgetaire,
				   my_ehom_ht_saisie, my_ehom_ht_saisie, my_ehom_tva_saisie, my_ehom_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_hors_marche(a_eng_id);
            
			Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
	    	Apres_Engage.hors_marche(my_ehom_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ehom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_emar_id	               ENGAGE_CTRL_MARCHE.emar_id%TYPE;
       my_att_ordre	  	   		   ENGAGE_CTRL_MARCHE.att_ordre%TYPE;
       my_emar_montant_budgetaire  ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_emar_ht_saisie	  	   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
       my_emar_tva_saisie		   ENGAGE_CTRL_MARCHE.emar_tva_saisie%TYPE;
       my_emar_ttc_saisie		   ENGAGE_CTRL_MARCHE.emar_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
   	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
   	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'attribution.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_emar_ht_saisie:=round(my_emar_ht_saisie, my_nb_decimales);
            my_emar_ttc_saisie:=round(my_emar_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_att_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette attribution pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_emar_ht_saisie<0 OR my_emar_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_emar_tva_saisie := my_emar_ttc_saisie - my_emar_ht_saisie;
			IF my_emar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_emar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_emar_ht_saisie,my_emar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_emar_montant_budgetaire THEN
			    my_emar_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour un engagement');
			END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_marche_seq.NEXTVAL INTO my_emar_id FROM dual;

			INSERT INTO ENGAGE_CTRL_MARCHE VALUES (my_emar_id,
			       a_exe_ordre, a_eng_id, my_att_ordre, my_emar_montant_budgetaire, my_emar_montant_budgetaire,
				   my_emar_ht_saisie,my_emar_ht_saisie, my_emar_tva_saisie, my_emar_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_marche(a_eng_id);
            
			Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
	    	Apres_Engage.marche(my_emar_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_emar_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_epco_id	               ENGAGE_CTRL_PLANCO.epco_id%TYPE;
       my_pco_num	  	   		   ENGAGE_CTRL_PLANCO.pco_num%TYPE;
	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_epco_montant_budgetaire  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_epco_ht_saisie	  	   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
       my_epco_tva_saisie		   ENGAGE_CTRL_PLANCO.epco_tva_saisie%TYPE;
       my_epco_ttc_saisie		   ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre				   ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_epco_ht_saisie:=round(my_epco_ht_saisie, my_nb_decimales);
            my_epco_ttc_saisie:=round(my_epco_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_pco_num;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette imputation pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_epco_ht_saisie<0 OR my_epco_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_epco_tva_saisie := my_epco_ttc_saisie - my_epco_ht_saisie;
			IF my_epco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_epco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_epco_ht_saisie,my_epco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_epco_montant_budgetaire THEN
			    my_epco_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_planco_seq.NEXTVAL INTO my_epco_id FROM dual;

			INSERT INTO ENGAGE_CTRL_PLANCO VALUES (my_epco_id,
			       a_exe_ordre, a_eng_id, my_pco_num, my_epco_montant_budgetaire, my_epco_montant_budgetaire,
				   my_epco_ht_saisie, my_epco_tva_saisie, my_epco_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_planco(a_eng_id);
            
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_epco_montant_budgetaire;
		END LOOP;
   END;

END;
/
