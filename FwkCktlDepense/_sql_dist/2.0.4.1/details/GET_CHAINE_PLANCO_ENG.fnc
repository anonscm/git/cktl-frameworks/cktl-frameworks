CREATE OR REPLACE FUNCTION JEFY_DEPENSE.Get_Chaine_Planco_eng( a_ht NUMBER, a_ttc NUMBER, a_eng_id NUMBER )
RETURN VARCHAR2
IS
  	   my_engage_ctr_Planco jefy_depense.ENGAGE_CTRL_PLANCO%ROWTYPE;
	   CURSOR my_cursor IS SELECT * FROM jefy_depense.ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

	   my_chaine VARCHAR2(200);

	   my_montant_ht   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
  	   my_montant_ttc  ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
  	   my_temp_ht        ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
  	   my_temp_ttc       ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
  	   my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

  	   my_nb INTEGER;
  	   my_cpt INTEGER;
	   my_nb_decimales		NUMBER;
	   my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;

BEGIN
	   my_chaine:='';
	   my_cpt:=0;
	   my_montant_ht:=0;
	   my_montant_ttc:=0;
	   my_temp_ht:=0;
	   my_temp_ttc:=0;

	   SELECT eng_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
	   FROM jefy_depense.ENGAGE_BUDGET WHERE eng_id=a_eng_id;

	   	-- nb decimales.
		my_nb_decimales:=Get_Nb_Decimales(my_exe_ordre);
	   
	   SELECT COUNT(*) INTO my_nb FROM jefy_depense.ENGAGE_CTRL_PLANCO WHERE eng_id = a_eng_id;

	   OPEN my_cursor;
	   LOOP
	   	   FETCH my_cursor INTO my_engage_ctr_Planco;
	   	   EXIT WHEN my_cursor %NOTFOUND;

	   	   my_cpt:=my_cpt + 1;

	   	   IF (my_cpt < my_nb) THEN
	   	   	  my_montant_ht:=ROUND( a_ht * my_engage_ctr_Planco.epco_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
	   	   	  my_montant_ttc:= ROUND( a_ttc * my_engage_ctr_Planco.epco_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

	   	   	  IF my_montant_ht>a_ht-my_temp_ht THEN 
	   	   	  	 my_montant_ht:=a_ht-my_temp_ht;
	   	   	  END IF;
	   	   	  IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
	   	   	  	 my_montant_ttc:=a_ttc-my_temp_ttc;
	   	   	  END IF;

	   	   	  my_temp_ht:=my_temp_ht + my_montant_ht;
	   	   	  my_temp_ttc:=my_temp_ttc + my_montant_ttc;

	   	   ELSE
	   	   	   my_montant_ht:=a_ht - my_temp_ht;
	   	   	   my_montant_ttc :=a_ttc - my_temp_ttc;
	   	   END IF;

		   my_chaine:=my_chaine||my_engage_ctr_Planco.pco_num||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

	   END LOOP;
	   CLOSE my_cursor;

	   RETURN my_chaine||'$';
   END;
/
