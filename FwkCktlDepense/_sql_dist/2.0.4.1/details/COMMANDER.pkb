CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Commander
IS

   PROCEDURE annuler_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
      my_nb integer;
   BEGIN
        select count(*) into my_nb from depense_budget where eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
        if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande ne peut etre annule, car il y a des liquidations associees ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        end if;
        select count(*) into my_nb from pdepense_budget where eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
        if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande ne peut etre annule, car il y a des preliquidations associees ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        end if;
        
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   del_commande_engagement(a_comm_id, my_eng_id);
		   Engager.del_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		del_commande(a_comm_id);
   END;

   PROCEDURE basculer_commande (
      a_comm_id_origine     commande.comm_id%type,
	  a_comm_id_destination	commande.comm_id%type
   ) IS
     my_nb integer;
   begin
       select count(*) into my_nb from commande_bascule where comm_id_origine=a_comm_id_origine;
       if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande a deja ete basculee ('||INDICATION_ERREUR.commande(a_comm_id_origine)||')');
       end if;
       
       insert into commande_bascule select commande_bascule_seq.nextval, a_comm_id_origine, a_comm_id_destination from dual;
       
       insert into commande_utilisateur select commande_utilisateur_seq.nextval, a_comm_id_destination, c.utl_ordre from 
           (select utl_ordre from commande where comm_id=a_comm_id_origine
              union select utl_ordre from commande_utilisateur where comm_id=a_comm_id_origine) c;
   end;

   PROCEDURE solder_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE solder_commande_sansdroit (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage_sansdroit(my_eng_id,null);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande (
      a_comm_id IN OUT		COMMANDE.comm_id%TYPE,
	  a_exe_ordre			COMMANDE.exe_ordre%TYPE,
	  a_comm_numero	IN OUT  COMMANDE.comm_numero%TYPE,
	  a_tyet_id				COMMANDE.tyet_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_tyet_id_imprimable  COMMANDE.tyet_id_imprimable%TYPE,
	  a_dev_id				COMMANDE.dev_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
	   	-- enregistrement dans la table.
		IF a_comm_numero IS NULL THEN
   		   a_comm_numero := Get_Numerotation(a_exe_ordre, NULL, null, 'COMMANDE');
   		END IF;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(a_comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

	    IF a_comm_id IS NULL THEN
	       SELECT commande_seq.NEXTVAL INTO a_comm_id FROM dual;
	    END IF;

		-- insertion de la commande.
		INSERT INTO COMMANDE VALUES(a_comm_id, a_exe_ordre,a_comm_numero, a_tyet_id, a_fou_ordre,
		   my_comm_reference, a_comm_libelle, a_tyet_id_imprimable, a_dev_id, a_utl_ordre, SYSDATE, SYSDATE);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE upd_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
     my_nb                  INTEGER;
     my_commande            COMMANDE%ROWTYPE;
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
   		SELECT * INTO my_commande FROM COMMANDE WHERE comm_id=a_comm_id;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(my_commande.comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_annulee THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La commande est annulee on ne peut rien modifier ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_precommande THEN
		   UPDATE COMMANDE SET comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre WHERE comm_id=a_comm_id;

		   -- si c'est une precommande et que ce n'est pas un marche on peut changer le fournisseur.
           SELECT COUNT(*) INTO my_nb FROM ARTICLE WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;
		   IF my_nb=0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      UPDATE COMMANDE SET fou_ordre=a_fou_ordre WHERE comm_id=a_comm_id;
		   END IF;

		   -- pour le moment on bloque le fournisseur d'une commande sur marche... on pourrait chercher.
		   -- si le nouveau fournisseur est le titulaire ou un sous_traitant ... a voir plus tard.
		   IF my_nb>0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est sur marche on ne peut pas modifier le fournisseur ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		   END IF;
		ELSE
		   IF my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier le fournisseur ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		   END IF;

		   --if my_commande.comm_reference<>a_comm_reference then
		   --   raise_application_error(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier la reference');
		   --end if;

           UPDATE COMMANDE SET fou_ordre=a_fou_ordre, comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre, comm_date=SYSDATE WHERE comm_id=a_comm_id;
		END IF;

        update engage_budget set eng_libelle=a_comm_libelle where eng_libelle<>a_comm_libelle
           and eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
           
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande_engagement (
	  a_come_id	IN OUT	    COMMANDE_ENGAGEMENT.come_id%TYPE,
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_nb                       INTEGER;
	 my_tyap_id                  ENGAGE_BUDGET.tyap_id%TYPE;
	 my_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	 my_montant_budgetaire_reste ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
	 my_eng_exe_ordre			 ENGAGE_BUDGET.exe_ordre%TYPE;
	 my_eng_fou_ordre			 ENGAGE_BUDGET.fou_ordre%TYPE;
	 my_cde_exe_ordre			 COMMANDE.exe_ordre%TYPE;
	 my_cde_fou_ordre			 COMMANDE.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
		IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement est deja utilise pour une commande ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		SELECT tyap_id, eng_montant_budgetaire, eng_montant_budgetaire_reste, exe_ordre, fou_ordre
		  INTO my_tyap_id, my_montant_budgetaire, my_montant_budgetaire_reste,my_eng_exe_ordre,
		       my_eng_fou_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

		SELECT exe_ordre, fou_ordre INTO my_cde_exe_ordre, my_cde_fou_ordre
		  FROM COMMANDE WHERE comm_id=a_comm_id;

		IF my_eng_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_eng_fou_ordre<>my_cde_fou_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas pour le meme fournisseur ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_montant_budgetaire>my_montant_budgetaire_reste THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement est deja partiellement solde ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		IF my_tyap_id<>Get_Type_Application THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement n''est pas genere par l''application Carambole ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- si pas de probleme on insere.
        IF a_come_id IS NULL THEN
	       SELECT commande_engagement_seq.NEXTVAL INTO a_come_id FROM dual;
	    END IF;

		INSERT INTO COMMANDE_ENGAGEMENT VALUES (a_come_id, a_comm_id, a_eng_id);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande_engagement (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
   BEGIN
   		DELETE FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id AND eng_id=a_eng_id;
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
   BEGIN
        Verifier.verifier_util_commande(a_comm_id);

		UPDATE COMMANDE SET tyet_id=Etats.get_etat_annulee,
		     tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
   END;

   PROCEDURE del_commande_budget (
      a_cbud_id             COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_comm_id             COMMANDE_BUDGET.comm_id%TYPE;
   BEGIN
        Verifier.verifier_util_commande_budget(a_cbud_id);

		SELECT comm_id INTO my_comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_article (
      a_art_id IN OUT		ARTICLE.art_id%TYPE,
	  a_comm_id				ARTICLE.comm_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_artc_id				ARTICLE.artc_id%TYPE,
	  a_att_ordre			ARTICLE.att_ordre%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE,
	  a_art_id_pere			ARTICLE.art_id_pere%TYPE,
	  a_typa_id				ARTICLE.typa_id%TYPE
   ) IS
      my_cm_niveau			v_code_marche.cm_niveau%TYPE;
      my_exe_ordre			v_code_exer.exe_ordre%TYPE;
      my_article_cm_niveau	NUMBER;
   BEGIN

   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		-- verification de l'attribution et type achat.
		IF a_att_ordre IS NULL AND a_typa_id IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		IF a_att_ordre IS NOT NULL AND a_typa_id IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		-- verification du niveau du code_exer de l'article.
		SELECT cm_niveau, exe_ordre INTO my_cm_niveau, my_exe_ordre FROM v_code_exer e, v_code_marche c
		WHERE e.cm_ordre=c.cm_ordre AND e.ce_ordre=a_ce_ordre;

		my_article_cm_niveau:=grhum.en_nombre(Get_Parametre(my_exe_ordre, 'ARTICLE_CM_NIVEAU'));
		IF my_article_cm_niveau<>my_cm_niveau THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''article doit avoir un code de nomenclature de niveau'||
		     my_article_cm_niveau);
		END IF;

	   	-- enregistrement dans la table.
	    IF a_art_id IS NULL THEN
	       SELECT article_seq.NEXTVAL INTO a_art_id FROM dual;
	    END IF;

		INSERT INTO ARTICLE VALUES (a_art_id, a_comm_id, a_art_libelle, a_art_prix_ht, a_art_prix_ttc,
		   a_art_quantite, a_art_prix_total_ht, a_art_prix_total_ttc, a_art_reference, a_artc_id,
		   a_att_ordre, a_ce_ordre, a_tva_id, a_art_id_pere, a_typa_id);

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_article_catalogue (
      a_caar_id IN OUT		jefy_catalogue.catalogue_article.caar_id%TYPE,
	  a_fou_ordre			jefy_catalogue.catalogue.fou_ordre%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_cm_ordre			jefy_catalogue.ARTICLE.cm_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
   	 my_nb					INTEGER;
	 my_cat_id				jefy_catalogue.catalogue.cat_id%TYPE;
	 my_art_id				jefy_catalogue.ARTICLE.art_id%TYPE;
   BEGIN
      	-- recherche du catalogue depense.
		SELECT COUNT(*) INTO my_nb FROM jefy_catalogue.catalogue
		   WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		IF my_nb=0 THEN
		   jefy_catalogue.gestion_catalogue.ins_catalogue(my_cat_id, 'catalogue depense', a_fou_ordre, Get_Type_Application,
      	   	  Get_Type_Etat('VALIDE'), SYSDATE, NULL, 'cree par carambole');
		ELSE
		   SELECT MAX(cat_id) INTO my_cat_id FROM jefy_catalogue.catalogue
		      WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		END IF;

		-- on insere l'article.
		jefy_catalogue.gestion_catalogue.ins_article(my_art_id, a_art_libelle, NULL, a_cm_ordre,
		Get_Type_Article('ARTICLE'));

		-- on le rattache au catalogue.
		jefy_catalogue.gestion_catalogue.ins_catalogue_article(a_caar_id, my_cat_id, my_art_id,
           a_art_reference, a_art_prix_ht, a_art_prix_ttc, a_tva_id, Get_Type_Etat('VALIDE'),
      	   NULL, NULL);
   END;

   PROCEDURE upd_article (
      a_art_id      		ARTICLE.art_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
        my_comm_id			ARTICLE.comm_id%TYPE;
   BEGIN
   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;

		-- enregistrement des modifications.
		UPDATE ARTICLE SET art_libelle=a_art_libelle, art_prix_ht=a_art_prix_ht, art_prix_ttc=a_art_prix_ttc,
		   art_quantite=a_art_quantite, art_prix_total_ht=a_art_prix_total_ht,
		   art_prix_total_ttc=a_art_prix_total_ttc, art_reference=a_art_reference, ce_ordre=a_ce_ordre,
		   tva_id=a_tva_id WHERE art_id=a_art_id;

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE del_article (
      a_art_id 		 	ARTICLE.art_id%TYPE)
   IS
     my_comm_id         ARTICLE.comm_id%TYPE;
   BEGIN
        b2bcxml.DEL_B2BCXMLITEM(a_art_id);
   
   		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;
        DELETE FROM ARTICLE WHERE art_id=a_art_id;
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_commande_budget (
      a_cbud_id IN OUT		COMMANDE_BUDGET.cbud_id%TYPE,
      a_comm_id	   			COMMANDE_BUDGET.comm_id%TYPE,
	  a_exe_ordre			COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_org_id				COMMANDE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			COMMANDE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

		IF my_cbud_ht_saisie<0 OR my_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_cbud_id IS NULL THEN
	       SELECT commande_budget_seq.NEXTVAL INTO a_cbud_id FROM dual;
	    END IF;

	    INSERT INTO COMMANDE_BUDGET VALUES (a_cbud_id, a_comm_id, a_exe_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, my_montant_budgetaire, my_cbud_ht_saisie, my_cbud_tva_saisie,
		  my_cbud_ttc_saisie);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(a_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(a_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(a_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(a_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(a_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(a_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE upd_commande_budget (
      a_cbud_id     		COMMANDE_BUDGET.cbud_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_org_id     			COMMANDE_BUDGET.org_id%TYPE;
     my_exe_ordre  			COMMANDE_BUDGET.exe_ordre%TYPE;
     my_tcd_ordre  			COMMANDE_BUDGET.tcd_ordre%TYPE;
     my_nb_decimales        NUMBER;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
   BEGIN

		IF a_cbud_ht_saisie<0 OR a_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

		SELECT org_id, exe_ordre, tcd_ordre INTO my_org_id, my_exe_ordre, my_tcd_ordre
		  FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;


  		Verifier.verifier_budget(my_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,a_tap_id,my_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	    UPDATE COMMANDE_BUDGET SET cbud_montant_budgetaire=my_montant_budgetaire,
		    cbud_ht_saisie=my_cbud_ht_saisie, cbud_tva_saisie=my_cbud_tva_saisie,
		    cbud_ttc_saisie=my_cbud_ttc_saisie, tap_id=a_tap_id WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(my_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(my_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(my_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(my_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(my_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(my_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE ins_commande_ctrl_action (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cact_id	               COMMANDE_CTRL_ACTION.cact_id%TYPE;
       my_tyac_id	  	   		   COMMANDE_CTRL_ACTION.tyac_id%TYPE;
       my_cact_montant_budgetaire  COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_cact_ht_saisie	  	   COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
       my_cact_tva_saisie		   COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
       my_cact_ttc_saisie		   COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
       my_cact_pourcentage		   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cact_ht_saisie:=round(my_cact_ht_saisie, my_nb_decimales);
            my_cact_ttc_saisie:=round(my_cact_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aact_ht_saisie<0 or my_aact_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cact_tva_saisie := my_cact_ttc_saisie - my_cact_ht_saisie;
			IF my_cact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cact_ht_saisie, my_cact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cact_montant_budgetaire THEN
			    my_cact_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_action_seq.NEXTVAL INTO my_cact_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ACTION VALUES (my_cact_id,
			       a_exe_ordre, a_cbud_id, my_tyac_id, my_cact_montant_budgetaire, my_cact_pourcentage,
				   my_cact_ht_saisie, my_cact_tva_saisie, my_cact_ttc_saisie);

	        --verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id);
			--apres_engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_analytique (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cana_id	               COMMANDE_CTRL_ANALYTIQUE.cana_id%TYPE;
       my_can_id	  	   		   COMMANDE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_cana_montant_budgetaire  COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_cana_ht_saisie	  	   COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
       my_cana_tva_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
       my_cana_ttc_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
       my_cana_pourcentage		   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cana_ht_saisie:=round(my_cana_ht_saisie, my_nb_decimales);
            my_cana_ttc_saisie:=round(my_cana_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cana_tva_saisie := my_cana_ttc_saisie - my_cana_ht_saisie;
			IF my_cana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cana_ht_saisie,my_cana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_cana_montant_budgetaire THEN
			    my_cana_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_analytique_seq.NEXTVAL INTO my_cana_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ANALYTIQUE VALUES (my_cana_id,
			       a_exe_ordre, a_cbud_id, my_can_id, my_cana_montant_budgetaire, my_cana_pourcentage,
				   my_cana_ht_saisie, my_cana_tva_saisie, my_cana_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_convention (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ccon_id	               COMMANDE_CTRL_CONVENTION.ccon_id%TYPE;
       my_conv_ordre  	   		   COMMANDE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_ccon_montant_budgetaire  COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_ccon_ht_saisie	  	   COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
       my_ccon_tva_saisie		   COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
       my_ccon_ttc_saisie		   COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
       my_ccon_pourcentage		   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ccon_ht_saisie:=round(my_ccon_ht_saisie, my_nb_decimales);
            my_ccon_ttc_saisie:=round(my_ccon_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_ccon_tva_saisie := my_ccon_ttc_saisie - my_ccon_ht_saisie;
			IF my_ccon_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_ccon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ccon_ht_saisie,my_ccon_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_ccon_montant_budgetaire THEN
			    my_ccon_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_convention_seq.NEXTVAL INTO my_ccon_id FROM dual;

			INSERT INTO COMMANDE_CTRL_CONVENTION VALUES (my_ccon_id,
			       a_exe_ordre, a_cbud_id, my_conv_ordre, my_ccon_montant_budgetaire, my_ccon_pourcentage,
				   my_ccon_ht_saisie, my_ccon_tva_saisie, my_ccon_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ccon_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_hors_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_chom_id	               COMMANDE_CTRL_HORS_MARCHE.chom_id%TYPE;
       my_ce_ordre	  	   		   COMMANDE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_typa_id	  	   		   COMMANDE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_chom_montant_budgetaire  COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_chom_ht_saisie	  	   COMMANDE_CTRL_HORS_MARCHE.chom_ht_saisie%TYPE;
       my_chom_tva_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_tva_saisie%TYPE;
       my_chom_ttc_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code nomenclature.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_chom_ht_saisie:=round(my_chom_ht_saisie, my_nb_decimales);
            my_chom_ttc_saisie:=round(my_chom_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_chom_tva_saisie := my_chom_ttc_saisie - my_chom_ht_saisie;
			IF my_chom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_chom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_chom_ht_saisie,my_chom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_chom_montant_budgetaire THEN
			    my_chom_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_hors_marche_seq.NEXTVAL INTO my_chom_id FROM dual;

			INSERT INTO COMMANDE_CTRL_HORS_MARCHE VALUES (my_chom_id,
			       a_exe_ordre, a_cbud_id, my_typa_id, my_ce_ordre, my_chom_montant_budgetaire,
				   NULL, my_chom_ht_saisie, my_chom_tva_saisie, my_chom_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_chom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cmar_id	               COMMANDE_CTRL_MARCHE.cmar_id%TYPE;
       my_att_ordre	  	   		   COMMANDE_CTRL_MARCHE.att_ordre%TYPE;
       my_cmar_montant_budgetaire  COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_cmar_ht_saisie	  	   COMMANDE_CTRL_MARCHE.cmar_ht_saisie%TYPE;
       my_cmar_tva_saisie		   COMMANDE_CTRL_MARCHE.cmar_tva_saisie%TYPE;
       my_cmar_ttc_saisie		   COMMANDE_CTRL_MARCHE.cmar_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cmar_ht_saisie:=round(my_cmar_ht_saisie, my_nb_decimales);
            my_cmar_ttc_saisie:=round(my_cmar_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cmar_tva_saisie := my_cmar_ttc_saisie - my_cmar_ht_saisie;
			IF my_cmar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cmar_ht_saisie,my_cmar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cmar_montant_budgetaire THEN
			    my_cmar_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour une commande');
			END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_marche_seq.NEXTVAL INTO my_cmar_id FROM dual;

			INSERT INTO COMMANDE_CTRL_MARCHE VALUES (my_cmar_id,
			       a_exe_ordre, a_cbud_id, my_att_ordre, my_cmar_montant_budgetaire, NULL,
				   my_cmar_ht_saisie, my_cmar_tva_saisie, my_cmar_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cmar_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_planco (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cpco_id	               COMMANDE_CTRL_PLANCO.cpco_id%TYPE;
       my_pco_num	  	   		   COMMANDE_CTRL_PLANCO.pco_num%TYPE;
       my_cpco_montant_budgetaire  COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_cpco_ht_saisie	  	   COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
       my_cpco_tva_saisie		   COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
       my_cpco_ttc_saisie		   COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
       my_cpco_pourcentage		   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cpco_ht_saisie:=round(my_cpco_ht_saisie, my_nb_decimales);
            my_cpco_ttc_saisie:=round(my_cpco_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cpco_tva_saisie := my_cpco_ttc_saisie - my_cpco_ht_saisie;
			IF my_cpco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cpco_ht_saisie,my_cpco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cpco_montant_budgetaire THEN
			    my_cpco_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_planco_seq.NEXTVAL INTO my_cpco_id FROM dual;

			INSERT INTO COMMANDE_CTRL_PLANCO VALUES (my_cpco_id,
			       a_exe_ordre, a_cbud_id, my_pco_num, my_cpco_montant_budgetaire, my_cpco_pourcentage,
				   my_cpco_ht_saisie, my_cpco_tva_saisie, my_cpco_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cpco_montant_budgetaire;
		END LOOP;
   END;

END;
/
