SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.0.4.1
-- Date de publication :  05/05/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Service facturier : changement du type de numerotation des dossiers de liquidation (maracuja)
-- corrections de la procédure de changement de taux de prorata
-- 
----------------------------------------------


whenever sqlerror exit sql.sqlcode ;

CREATE OR REPLACE FUNCTION JEFY_DEPENSE.Get_Chaine_Planco_eng( a_ht NUMBER, a_ttc NUMBER, a_eng_id NUMBER )
RETURN VARCHAR2
IS
  	   my_engage_ctr_Planco jefy_depense.ENGAGE_CTRL_PLANCO%ROWTYPE;
	   CURSOR my_cursor IS SELECT * FROM jefy_depense.ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

	   my_chaine VARCHAR2(200);

	   my_montant_ht   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
  	   my_montant_ttc  ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
  	   my_temp_ht        ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
  	   my_temp_ttc       ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
  	   my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

  	   my_nb INTEGER;
  	   my_cpt INTEGER;
	   my_nb_decimales		NUMBER;
	   my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;

BEGIN
	   my_chaine:='';
	   my_cpt:=0;
	   my_montant_ht:=0;
	   my_montant_ttc:=0;
	   my_temp_ht:=0;
	   my_temp_ttc:=0;

	   SELECT eng_montant_budgetaire, exe_ordre INTO my_montant_budgetaire, my_exe_ordre
	   FROM jefy_depense.ENGAGE_BUDGET WHERE eng_id=a_eng_id;

	   	-- nb decimales.
		my_nb_decimales:=Get_Nb_Decimales(my_exe_ordre);
	   
	   SELECT COUNT(*) INTO my_nb FROM jefy_depense.ENGAGE_CTRL_PLANCO WHERE eng_id = a_eng_id;

	   OPEN my_cursor;
	   LOOP
	   	   FETCH my_cursor INTO my_engage_ctr_Planco;
	   	   EXIT WHEN my_cursor %NOTFOUND;

	   	   my_cpt:=my_cpt + 1;

	   	   IF (my_cpt < my_nb) THEN
	   	   	  my_montant_ht:=ROUND( a_ht * my_engage_ctr_Planco.epco_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);
	   	   	  my_montant_ttc:= ROUND( a_ttc * my_engage_ctr_Planco.epco_montant_budgetaire/my_montant_budgetaire, my_nb_decimales);

	   	   	  IF my_montant_ht>a_ht-my_temp_ht THEN 
	   	   	  	 my_montant_ht:=a_ht-my_temp_ht;
	   	   	  END IF;
	   	   	  IF my_montant_ttc>a_ttc-my_temp_ttc THEN 
	   	   	  	 my_montant_ttc:=a_ttc-my_temp_ttc;
	   	   	  END IF;

	   	   	  my_temp_ht:=my_temp_ht + my_montant_ht;
	   	   	  my_temp_ttc:=my_temp_ttc + my_montant_ttc;

	   	   ELSE
	   	   	   my_montant_ht:=a_ht - my_temp_ht;
	   	   	   my_montant_ttc :=a_ttc - my_temp_ttc;
	   	   END IF;

		   my_chaine:=my_chaine||my_engage_ctr_Planco.pco_num||'$'||my_montant_ht||'$'||my_montant_ttc||'$';

	   END LOOP;
	   CLOSE my_cursor;

	   RETURN my_chaine||'$';
   END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Commander
IS

   PROCEDURE annuler_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
      my_nb integer;
   BEGIN
        select count(*) into my_nb from depense_budget where eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
        if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande ne peut etre annule, car il y a des liquidations associees ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        end if;
        select count(*) into my_nb from pdepense_budget where eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
        if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande ne peut etre annule, car il y a des preliquidations associees ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        end if;
        
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   del_commande_engagement(a_comm_id, my_eng_id);
		   Engager.del_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		del_commande(a_comm_id);
   END;

   PROCEDURE basculer_commande (
      a_comm_id_origine     commande.comm_id%type,
	  a_comm_id_destination	commande.comm_id%type
   ) IS
     my_nb integer;
   begin
       select count(*) into my_nb from commande_bascule where comm_id_origine=a_comm_id_origine;
       if my_nb>0 then
		   RAISE_APPLICATION_ERROR(-20001, 'Cette commande a deja ete basculee ('||INDICATION_ERREUR.commande(a_comm_id_origine)||')');
       end if;
       
       insert into commande_bascule select commande_bascule_seq.nextval, a_comm_id_origine, a_comm_id_destination from dual;
       
       insert into commande_utilisateur select commande_utilisateur_seq.nextval, a_comm_id_destination, c.utl_ordre from 
           (select utl_ordre from commande where comm_id=a_comm_id_origine
              union select utl_ordre from commande_utilisateur where comm_id=a_comm_id_origine) c;
   end;

   PROCEDURE solder_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage(my_eng_id, a_utl_ordre);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE solder_commande_sansdroit (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
	  my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
   		OPEN engagements;
		LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

		   Engager.solder_engage_sansdroit(my_eng_id,null);
        END LOOP;
		CLOSE engagements;

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande (
      a_comm_id IN OUT		COMMANDE.comm_id%TYPE,
	  a_exe_ordre			COMMANDE.exe_ordre%TYPE,
	  a_comm_numero	IN OUT  COMMANDE.comm_numero%TYPE,
	  a_tyet_id				COMMANDE.tyet_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_tyet_id_imprimable  COMMANDE.tyet_id_imprimable%TYPE,
	  a_dev_id				COMMANDE.dev_id%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
	   	-- enregistrement dans la table.
		IF a_comm_numero IS NULL THEN
   		   a_comm_numero := Get_Numerotation(a_exe_ordre, NULL, null, 'COMMANDE');
   		END IF;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(a_comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

	    IF a_comm_id IS NULL THEN
	       SELECT commande_seq.NEXTVAL INTO a_comm_id FROM dual;
	    END IF;

		-- insertion de la commande.
		INSERT INTO COMMANDE VALUES(a_comm_id, a_exe_ordre,a_comm_numero, a_tyet_id, a_fou_ordre,
		   my_comm_reference, a_comm_libelle, a_tyet_id_imprimable, a_dev_id, a_utl_ordre, SYSDATE, SYSDATE);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE upd_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
	  a_fou_ordre			COMMANDE.fou_ordre%TYPE,
	  a_comm_reference		COMMANDE.comm_reference%TYPE,
	  a_comm_libelle		COMMANDE.comm_libelle%TYPE,
	  a_utl_ordre			COMMANDE.utl_ordre%TYPE
   ) IS
     my_nb                  INTEGER;
     my_commande            COMMANDE%ROWTYPE;
      my_comm_reference		COMMANDE.comm_reference%TYPE;
   BEGIN
   		SELECT * INTO my_commande FROM COMMANDE WHERE comm_id=a_comm_id;

		IF a_comm_reference IS NULL THEN
		   my_comm_reference:=TO_CHAR(my_commande.comm_numero);
		ELSE
		   my_comm_reference:=a_comm_reference;
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_annulee THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La commande est annulee on ne peut rien modifier ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_commande.tyet_id=Etats.get_etat_precommande THEN
		   UPDATE COMMANDE SET comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre WHERE comm_id=a_comm_id;

		   -- si c'est une precommande et que ce n'est pas un marche on peut changer le fournisseur.
           SELECT COUNT(*) INTO my_nb FROM ARTICLE WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;
		   IF my_nb=0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      UPDATE COMMANDE SET fou_ordre=a_fou_ordre WHERE comm_id=a_comm_id;
		   END IF;

		   -- pour le moment on bloque le fournisseur d'une commande sur marche... on pourrait chercher.
		   -- si le nouveau fournisseur est le titulaire ou un sous_traitant ... a voir plus tard.
		   IF my_nb>0 AND my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est sur marche on ne peut pas modifier le fournisseur ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		   END IF;
		ELSE
		   IF my_commande.fou_ordre<>a_fou_ordre THEN
		      RAISE_APPLICATION_ERROR(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier le fournisseur ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		   END IF;

		   --if my_commande.comm_reference<>a_comm_reference then
		   --   raise_application_error(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier la reference');
		   --end if;

           UPDATE COMMANDE SET fou_ordre=a_fou_ordre, comm_reference=my_comm_reference,
		      comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre, comm_date=SYSDATE WHERE comm_id=a_comm_id;
		END IF;

        update engage_budget set eng_libelle=a_comm_libelle where eng_libelle<>a_comm_libelle
           and eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
           
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);

		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande_engagement (
	  a_come_id	IN OUT	    COMMANDE_ENGAGEMENT.come_id%TYPE,
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_nb                       INTEGER;
	 my_tyap_id                  ENGAGE_BUDGET.tyap_id%TYPE;
	 my_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	 my_montant_budgetaire_reste ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
	 my_eng_exe_ordre			 ENGAGE_BUDGET.exe_ordre%TYPE;
	 my_eng_fou_ordre			 ENGAGE_BUDGET.fou_ordre%TYPE;
	 my_cde_exe_ordre			 COMMANDE.exe_ordre%TYPE;
	 my_cde_fou_ordre			 COMMANDE.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
		IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
		IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement est deja utilise pour une commande ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		SELECT tyap_id, eng_montant_budgetaire, eng_montant_budgetaire_reste, exe_ordre, fou_ordre
		  INTO my_tyap_id, my_montant_budgetaire, my_montant_budgetaire_reste,my_eng_exe_ordre,
		       my_eng_fou_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

		SELECT exe_ordre, fou_ordre INTO my_cde_exe_ordre, my_cde_fou_ordre
		  FROM COMMANDE WHERE comm_id=a_comm_id;

		IF my_eng_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_eng_fou_ordre<>my_cde_fou_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas pour le meme fournisseur ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
		END IF;

		IF my_montant_budgetaire>my_montant_budgetaire_reste THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement est deja partiellement solde ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		IF my_tyap_id<>Get_Type_Application THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement n''est pas genere par l''application Carambole ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- si pas de probleme on insere.
        IF a_come_id IS NULL THEN
	       SELECT commande_engagement_seq.NEXTVAL INTO a_come_id FROM dual;
	    END IF;

		INSERT INTO COMMANDE_ENGAGEMENT VALUES (a_come_id, a_comm_id, a_eng_id);

		-- verification et correction de l'etat de la commande.
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande_engagement (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
   BEGIN
   		DELETE FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id AND eng_id=a_eng_id;
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
   BEGIN
        Verifier.verifier_util_commande(a_comm_id);

		UPDATE COMMANDE SET tyet_id=Etats.get_etat_annulee,
		     tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
   END;

   PROCEDURE del_commande_budget (
      a_cbud_id             COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_comm_id             COMMANDE_BUDGET.comm_id%TYPE;
   BEGIN
        Verifier.verifier_util_commande_budget(a_cbud_id);

		SELECT comm_id INTO my_comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_article (
      a_art_id IN OUT		ARTICLE.art_id%TYPE,
	  a_comm_id				ARTICLE.comm_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_artc_id				ARTICLE.artc_id%TYPE,
	  a_att_ordre			ARTICLE.att_ordre%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE,
	  a_art_id_pere			ARTICLE.art_id_pere%TYPE,
	  a_typa_id				ARTICLE.typa_id%TYPE
   ) IS
      my_cm_niveau			v_code_marche.cm_niveau%TYPE;
      my_exe_ordre			v_code_exer.exe_ordre%TYPE;
      my_article_cm_niveau	NUMBER;
   BEGIN

   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		-- verification de l'attribution et type achat.
		IF a_att_ordre IS NULL AND a_typa_id IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		IF a_att_ordre IS NOT NULL AND a_typa_id IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
		END IF;

		-- verification du niveau du code_exer de l'article.
		SELECT cm_niveau, exe_ordre INTO my_cm_niveau, my_exe_ordre FROM v_code_exer e, v_code_marche c
		WHERE e.cm_ordre=c.cm_ordre AND e.ce_ordre=a_ce_ordre;

		my_article_cm_niveau:=grhum.en_nombre(Get_Parametre(my_exe_ordre, 'ARTICLE_CM_NIVEAU'));
		IF my_article_cm_niveau<>my_cm_niveau THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''article doit avoir un code de nomenclature de niveau'||
		     my_article_cm_niveau);
		END IF;

	   	-- enregistrement dans la table.
	    IF a_art_id IS NULL THEN
	       SELECT article_seq.NEXTVAL INTO a_art_id FROM dual;
	    END IF;

		INSERT INTO ARTICLE VALUES (a_art_id, a_comm_id, a_art_libelle, a_art_prix_ht, a_art_prix_ttc,
		   a_art_quantite, a_art_prix_total_ht, a_art_prix_total_ttc, a_art_reference, a_artc_id,
		   a_att_ordre, a_ce_ordre, a_tva_id, a_art_id_pere, a_typa_id);

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);
		Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_article_catalogue (
      a_caar_id IN OUT		jefy_catalogue.catalogue_article.caar_id%TYPE,
	  a_fou_ordre			jefy_catalogue.catalogue.fou_ordre%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_cm_ordre			jefy_catalogue.ARTICLE.cm_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
   	 my_nb					INTEGER;
	 my_cat_id				jefy_catalogue.catalogue.cat_id%TYPE;
	 my_art_id				jefy_catalogue.ARTICLE.art_id%TYPE;
   BEGIN
      	-- recherche du catalogue depense.
		SELECT COUNT(*) INTO my_nb FROM jefy_catalogue.catalogue
		   WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		IF my_nb=0 THEN
		   jefy_catalogue.gestion_catalogue.ins_catalogue(my_cat_id, 'catalogue depense', a_fou_ordre, Get_Type_Application,
      	   	  Get_Type_Etat('VALIDE'), SYSDATE, NULL, 'cree par carambole');
		ELSE
		   SELECT MAX(cat_id) INTO my_cat_id FROM jefy_catalogue.catalogue
		      WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
		END IF;

		-- on insere l'article.
		jefy_catalogue.gestion_catalogue.ins_article(my_art_id, a_art_libelle, NULL, a_cm_ordre,
		Get_Type_Article('ARTICLE'));

		-- on le rattache au catalogue.
		jefy_catalogue.gestion_catalogue.ins_catalogue_article(a_caar_id, my_cat_id, my_art_id,
           a_art_reference, a_art_prix_ht, a_art_prix_ttc, a_tva_id, Get_Type_Etat('VALIDE'),
      	   NULL, NULL);
   END;

   PROCEDURE upd_article (
      a_art_id      		ARTICLE.art_id%TYPE,
	  a_art_libelle			ARTICLE.art_libelle%TYPE,
	  a_art_prix_ht			ARTICLE.art_prix_ht%TYPE,
	  a_art_prix_ttc		ARTICLE.art_prix_ttc%TYPE,
	  a_art_quantite		ARTICLE.art_quantite%TYPE,
	  a_art_prix_total_ht	ARTICLE.art_prix_total_ht%TYPE,
	  a_art_prix_total_ttc	ARTICLE.art_prix_total_ttc%TYPE,
	  a_art_reference		ARTICLE.art_reference%TYPE,
	  a_ce_ordre			ARTICLE.ce_ordre%TYPE,
	  a_tva_id				ARTICLE.tva_id%TYPE
   ) IS
        my_comm_id			ARTICLE.comm_id%TYPE;
   BEGIN
   	   	-- verification des montants.
		IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
		END IF;

		IF a_art_quantite<=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
		END IF;

		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;

		-- enregistrement des modifications.
		UPDATE ARTICLE SET art_libelle=a_art_libelle, art_prix_ht=a_art_prix_ht, art_prix_ttc=a_art_prix_ttc,
		   art_quantite=a_art_quantite, art_prix_total_ht=a_art_prix_total_ht,
		   art_prix_total_ttc=a_art_prix_total_ttc, art_reference=a_art_reference, ce_ordre=a_ce_ordre,
		   tva_id=a_tva_id WHERE art_id=a_art_id;

		-- verification et correction de l'etat de la commande.
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE del_article (
      a_art_id 		 	ARTICLE.art_id%TYPE)
   IS
     my_comm_id         ARTICLE.comm_id%TYPE;
   BEGIN
        b2bcxml.DEL_B2BCXMLITEM(a_art_id);
   
   		SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;
        DELETE FROM ARTICLE WHERE art_id=a_art_id;
		jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
		Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_commande_budget (
      a_cbud_id IN OUT		COMMANDE_BUDGET.cbud_id%TYPE,
      a_comm_id	   			COMMANDE_BUDGET.comm_id%TYPE,
	  a_exe_ordre			COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_org_id				COMMANDE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			COMMANDE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

		IF my_cbud_ht_saisie<0 OR my_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_cbud_id IS NULL THEN
	       SELECT commande_budget_seq.NEXTVAL INTO a_cbud_id FROM dual;
	    END IF;

	    INSERT INTO COMMANDE_BUDGET VALUES (a_cbud_id, a_comm_id, a_exe_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, my_montant_budgetaire, my_cbud_ht_saisie, my_cbud_tva_saisie,
		  my_cbud_ttc_saisie);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(a_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(a_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(a_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(a_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(a_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(a_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE upd_commande_budget (
      a_cbud_id     		COMMANDE_BUDGET.cbud_id%TYPE,
	  a_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
	  a_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
	  a_tap_id				COMMANDE_BUDGET.tap_id%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_org_id     			COMMANDE_BUDGET.org_id%TYPE;
     my_exe_ordre  			COMMANDE_BUDGET.exe_ordre%TYPE;
     my_tcd_ordre  			COMMANDE_BUDGET.tcd_ordre%TYPE;
     my_nb_decimales        NUMBER;
     my_cbud_ht_saisie		COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie		COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
   BEGIN

		IF a_cbud_ht_saisie<0 OR a_cbud_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
		END IF;

		SELECT org_id, exe_ordre, tcd_ordre INTO my_org_id, my_exe_ordre, my_tcd_ordre
		  FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

   		my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

		IF my_cbud_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;


  		Verifier.verifier_budget(my_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,a_tap_id,my_org_id,
		      my_cbud_ht_saisie,my_cbud_ttc_saisie);

	    UPDATE COMMANDE_BUDGET SET cbud_montant_budgetaire=my_montant_budgetaire,
		    cbud_ht_saisie=my_cbud_ht_saisie, cbud_tva_saisie=my_cbud_tva_saisie,
		    cbud_ttc_saisie=my_cbud_ttc_saisie, tap_id=a_tap_id WHERE cbud_id=a_cbud_id;

		DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_commande_ctrl_action(my_exe_ordre,a_cbud_id,a_chaine_action);
		ins_commande_ctrl_analytique(my_exe_ordre,a_cbud_id,a_chaine_analytique);
		ins_commande_ctrl_convention(my_exe_ordre,a_cbud_id,a_chaine_convention);
		ins_commande_ctrl_hors_marche(my_exe_ordre,a_cbud_id,a_chaine_hors_marche);
		ins_commande_ctrl_marche(my_exe_ordre,a_cbud_id,a_chaine_marche);
		ins_commande_ctrl_planco(my_exe_ordre,a_cbud_id,a_chaine_planco);

		Verifier.verifier_cde_budget_coherence(a_cbud_id);
		--verifier.verifier_engage_coherence(a_eng_id);
		--apres_engage.engage(a_eng_id);
   END;

   PROCEDURE ins_commande_ctrl_action (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cact_id	               COMMANDE_CTRL_ACTION.cact_id%TYPE;
       my_tyac_id	  	   		   COMMANDE_CTRL_ACTION.tyac_id%TYPE;
       my_cact_montant_budgetaire  COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_cact_ht_saisie	  	   COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
       my_cact_tva_saisie		   COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
       my_cact_ttc_saisie		   COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
       my_cact_pourcentage		   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cact_ht_saisie:=round(my_cact_ht_saisie, my_nb_decimales);
            my_cact_ttc_saisie:=round(my_cact_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aact_ht_saisie<0 or my_aact_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cact_tva_saisie := my_cact_ttc_saisie - my_cact_ht_saisie;
			IF my_cact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cact_ht_saisie, my_cact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cact_montant_budgetaire THEN
			    my_cact_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_action_seq.NEXTVAL INTO my_cact_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ACTION VALUES (my_cact_id,
			       a_exe_ordre, a_cbud_id, my_tyac_id, my_cact_montant_budgetaire, my_cact_pourcentage,
				   my_cact_ht_saisie, my_cact_tva_saisie, my_cact_ttc_saisie);

	        --verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id);
			--apres_engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_analytique (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cana_id	               COMMANDE_CTRL_ANALYTIQUE.cana_id%TYPE;
       my_can_id	  	   		   COMMANDE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_cana_montant_budgetaire  COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_cana_ht_saisie	  	   COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
       my_cana_tva_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
       my_cana_ttc_saisie		   COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
       my_cana_pourcentage		   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cana_ht_saisie:=round(my_cana_ht_saisie, my_nb_decimales);
            my_cana_ttc_saisie:=round(my_cana_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cana_tva_saisie := my_cana_ttc_saisie - my_cana_ht_saisie;
			IF my_cana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cana_ht_saisie,my_cana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_cana_montant_budgetaire THEN
			    my_cana_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_analytique_seq.NEXTVAL INTO my_cana_id FROM dual;

			INSERT INTO COMMANDE_CTRL_ANALYTIQUE VALUES (my_cana_id,
			       a_exe_ordre, a_cbud_id, my_can_id, my_cana_montant_budgetaire, my_cana_pourcentage,
				   my_cana_ht_saisie, my_cana_tva_saisie, my_cana_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_convention (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ccon_id	               COMMANDE_CTRL_CONVENTION.ccon_id%TYPE;
       my_conv_ordre  	   		   COMMANDE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_ccon_montant_budgetaire  COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_ccon_ht_saisie	  	   COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
       my_ccon_tva_saisie		   COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
       my_ccon_ttc_saisie		   COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
       my_ccon_pourcentage		   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ccon_ht_saisie:=round(my_ccon_ht_saisie, my_nb_decimales);
            my_ccon_ttc_saisie:=round(my_ccon_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_ccon_tva_saisie := my_ccon_ttc_saisie - my_ccon_ht_saisie;
			IF my_ccon_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_ccon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ccon_ht_saisie,my_ccon_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_cbud_montant_budgetaire<=my_somme+my_ccon_montant_budgetaire THEN
			    my_ccon_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_convention_seq.NEXTVAL INTO my_ccon_id FROM dual;

			INSERT INTO COMMANDE_CTRL_CONVENTION VALUES (my_ccon_id,
			       a_exe_ordre, a_cbud_id, my_conv_ordre, my_ccon_montant_budgetaire, my_ccon_pourcentage,
				   my_ccon_ht_saisie, my_ccon_tva_saisie, my_ccon_ttc_saisie);

			--verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	--apres_engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ccon_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_hors_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_chom_id	               COMMANDE_CTRL_HORS_MARCHE.chom_id%TYPE;
       my_ce_ordre	  	   		   COMMANDE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_typa_id	  	   		   COMMANDE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_chom_montant_budgetaire  COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_chom_ht_saisie	  	   COMMANDE_CTRL_HORS_MARCHE.chom_ht_saisie%TYPE;
       my_chom_tva_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_tva_saisie%TYPE;
       my_chom_ttc_saisie		   COMMANDE_CTRL_HORS_MARCHE.chom_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code nomenclature.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_chom_ht_saisie:=round(my_chom_ht_saisie, my_nb_decimales);
            my_chom_ttc_saisie:=round(my_chom_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_chom_tva_saisie := my_chom_ttc_saisie - my_chom_ht_saisie;
			IF my_chom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_chom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_chom_ht_saisie,my_chom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_chom_montant_budgetaire THEN
			    my_chom_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_hors_marche_seq.NEXTVAL INTO my_chom_id FROM dual;

			INSERT INTO COMMANDE_CTRL_HORS_MARCHE VALUES (my_chom_id,
			       a_exe_ordre, a_cbud_id, my_typa_id, my_ce_ordre, my_chom_montant_budgetaire,
				   NULL, my_chom_ht_saisie, my_chom_tva_saisie, my_chom_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_chom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cmar_id	               COMMANDE_CTRL_MARCHE.cmar_id%TYPE;
       my_att_ordre	  	   		   COMMANDE_CTRL_MARCHE.att_ordre%TYPE;
       my_cmar_montant_budgetaire  COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_cmar_ht_saisie	  	   COMMANDE_CTRL_MARCHE.cmar_ht_saisie%TYPE;
       my_cmar_tva_saisie		   COMMANDE_CTRL_MARCHE.cmar_tva_saisie%TYPE;
       my_cmar_ttc_saisie		   COMMANDE_CTRL_MARCHE.cmar_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cmar_ht_saisie:=round(my_cmar_ht_saisie, my_nb_decimales);
            my_cmar_ttc_saisie:=round(my_cmar_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cmar_tva_saisie := my_cmar_ttc_saisie - my_cmar_ht_saisie;
			IF my_cmar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cmar_ht_saisie,my_cmar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cmar_montant_budgetaire THEN
			    my_cmar_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour une commande');
			END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_marche_seq.NEXTVAL INTO my_cmar_id FROM dual;

			INSERT INTO COMMANDE_CTRL_MARCHE VALUES (my_cmar_id,
			       a_exe_ordre, a_cbud_id, my_att_ordre, my_cmar_montant_budgetaire, NULL,
				   my_cmar_ht_saisie, my_cmar_tva_saisie, my_cmar_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cmar_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_planco (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cpco_id	               COMMANDE_CTRL_PLANCO.cpco_id%TYPE;
       my_pco_num	  	   		   COMMANDE_CTRL_PLANCO.pco_num%TYPE;
       my_cpco_montant_budgetaire  COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_cpco_ht_saisie	  	   COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
       my_cpco_tva_saisie		   COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
       my_cpco_ttc_saisie		   COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
       my_cpco_pourcentage		   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cpco_ht_saisie:=round(my_cpco_ht_saisie, my_nb_decimales);
            my_cpco_ttc_saisie:=round(my_cpco_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cpco_tva_saisie := my_cpco_ttc_saisie - my_cpco_ht_saisie;
			IF my_cpco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cpco_ht_saisie,my_cpco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cpco_montant_budgetaire THEN
			    my_cpco_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_planco_seq.NEXTVAL INTO my_cpco_id FROM dual;

			INSERT INTO COMMANDE_CTRL_PLANCO VALUES (my_cpco_id,
			       a_exe_ordre, a_cbud_id, my_pco_num, my_cpco_montant_budgetaire, my_cpco_pourcentage,
				   my_cpco_ht_saisie, my_cpco_tva_saisie, my_cpco_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cpco_montant_budgetaire;
		END LOOP;
   END;

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Engager
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------


   PROCEDURE ins_engage (
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(a_exe_ordre, a_utl_ordre, a_org_id);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_engage_budget(a_eng_id,a_exe_ordre,a_eng_numero,a_fou_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_eng_libelle, a_eng_ht_saisie,a_eng_ttc_saisie,a_tyap_id,a_utl_ordre);

		ins_engage_ctrl_action(a_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(a_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(a_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(a_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(a_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(a_exe_ordre,a_eng_id,a_chaine_planco);

		Verifier.verifier_engage_coherence(a_eng_id);
		Apres_Engage.engage(a_eng_id);
   END;

   PROCEDURE del_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_montant_reste      ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est deja annule (eng_id:'||a_eng_id||')');
	    END IF;

   		SELECT exe_ordre, eng_montant_budgetaire, eng_montant_budgetaire_reste, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_montant_reste, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

   		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on traite le cas d'un engagement dont la somme des liquidations.
		-- est egale a la somme des ordres de reversement ... ce qui fait que l'engagement est reengage.
		--  donc on ne le supprime pas mais on le solde.
		SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb>0 AND my_montant_budgetaire=my_montant_reste THEN
		   solder_engage(a_eng_id, a_utl_ordre);
		ELSE
           Verifier.verifier_util_engage(a_eng_id);

		   log_engage(a_eng_id,a_utl_ordre);

	       DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        END IF;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.del_engage(a_eng_id);
   END;

   PROCEDURE solder_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE solder_engage_sansdroit (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE upd_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
	  my_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	  my_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_tap_id			    ENGAGE_BUDGET.tap_id%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_tva_saisie     ENGAGE_BUDGET.eng_tva_saisie%TYPE;
      my_budgetaire         ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_nb_decimales       NUMBER;
      
      CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
      depense DEPENSE_BUDGET%ROWTYPE;
	  my_dep_ht_saisie		DEPENSE_BUDGET.dep_ht_saisie%TYPE;
	  my_dep_ttc_saisie		DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      
   BEGIN
   		SELECT exe_ordre, tap_id, org_id, tcd_ordre INTO my_exe_ordre, my_tap_id, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_ttc_saisie),0) into my_dep_ht_saisie, my_dep_ttc_saisie
        from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        my_budgetaire:=0;
        if my_dep_ht_saisie<>0 or my_dep_ttc_saisie<>0 then
           if my_dep_ht_saisie=my_dep_ttc_saisie then
               my_budgetaire:=my_dep_ttc_saisie;
           else
               --my_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);
               
               OPEN listedepense(a_eng_id);
 	           LOOP
		           FETCH  listedepense INTO depense;
		           EXIT WHEN listedepense%NOTFOUND;
				   
				    my_budgetaire:=my_budgetaire+Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
                 END LOOP;
			     CLOSE listedepense;
                 
           end if;
        end if;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);
        my_budgetaire:=round(my_budgetaire, my_nb_decimales);

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		log_engage(a_eng_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	    IF my_budgetaire> my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja liquide ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- on modifie les montants.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=my_montant_budgetaire,
		   eng_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   eng_ht_saisie=my_eng_ht_saisie, eng_tva_saisie=my_eng_tva_saisie,
		   eng_ttc_saisie=my_eng_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE eng_id=a_eng_id;

		-- on supprime les anciens.
		DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

		-- on insere les nouveaux.
		ins_engage_ctrl_action(my_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(my_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(my_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(my_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(my_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(my_exe_ordre,a_eng_id,a_chaine_planco);

		-- on met a jour les restes engages des controleurs suivant les depenses.
		Corriger.upd_engage_reste(a_eng_id);

		-- on verifie.
		Verifier.verifier_engage_coherence(a_eng_id);
        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.engage(a_eng_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


   PROCEDURE log_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_zeng_id			Z_ENGAGE_BUDGET.zeng_id%TYPE;
   BEGIN
		SELECT z_engage_budget_seq.NEXTVAL INTO my_zeng_id FROM dual;

		-- engage_budget.
		INSERT INTO Z_ENGAGE_BUDGET SELECT my_zeng_id, SYSDATE, a_utl_ordre, e.*
		  FROM ENGAGE_BUDGET e WHERE eng_id=a_eng_id;

		-- engage_ctrl_action.
		INSERT INTO Z_ENGAGE_CTRL_ACTION SELECT z_engage_ctrl_action_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ACTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_analytique.
		INSERT INTO Z_ENGAGE_CTRL_ANALYTIQUE SELECT z_engage_ctrl_analytique_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ANALYTIQUE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_convention.
		INSERT INTO Z_ENGAGE_CTRL_CONVENTION SELECT z_engage_ctrl_convention_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_CONVENTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_hors_marche.
		INSERT INTO Z_ENGAGE_CTRL_HORS_MARCHE SELECT z_engage_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_HORS_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_marche.
		INSERT INTO Z_ENGAGE_CTRL_MARCHE SELECT z_engage_ctrl_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_planco.
		INSERT INTO Z_ENGAGE_CTRL_PLANCO SELECT z_engage_ctrl_planco_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_PLANCO e WHERE eng_id=a_eng_id;
   END;

   PROCEDURE ins_engage_budget(
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

   		-- lucrativite ??.
		--		UPDATE COMMANDE SET cde_lucrativite=(select org_lucrativite from organ where org_ordre=orgordre) WHERE cde_ordre=cdeordre;

        verifier.verifier_organ(a_org_id, a_tcd_ordre);
		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
	    Verifier.verifier_fournisseur(a_fou_ordre);

		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);

        Budget.maj_budget(a_exe_ordre, a_org_id, a_tcd_ordre);
	    Apres_Engage.Budget(a_eng_id);
   END;

   PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eact_id	               ENGAGE_CTRL_ACTION.eact_id%TYPE;
       my_tyac_id	  	   		   ENGAGE_CTRL_ACTION.tyac_id%TYPE;
       my_eact_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_eact_ht_saisie	  	   ENGAGE_CTRL_ACTION.eact_ht_saisie%TYPE;
       my_eact_tva_saisie		   ENGAGE_CTRL_ACTION.eact_tva_saisie%TYPE;
       my_eact_ttc_saisie		   ENGAGE_CTRL_ACTION.eact_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
	   my_utl_ordre                ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eact_ht_saisie:=round(my_eact_ht_saisie, my_nb_decimales);
            my_eact_ttc_saisie:=round(my_eact_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_tyac_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette action pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eact_ht_saisie<0 OR my_eact_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eact_tva_saisie := my_eact_ttc_saisie - my_eact_ht_saisie;
			IF my_eact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eact_ht_saisie,my_eact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_eact_montant_budgetaire THEN
			    my_eact_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_action_seq.NEXTVAL INTO my_eact_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ACTION VALUES (my_eact_id,
			       a_exe_ordre, a_eng_id, my_tyac_id, my_eact_montant_budgetaire, my_eact_montant_budgetaire,
				   my_eact_ht_saisie, my_eact_tva_saisie, my_eact_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_action(a_eng_id);
            
	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
			Apres_Engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eana_id	               ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
       my_can_id	  	   		   ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_eana_montant_budgetaire  ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_eana_ht_saisie	  	   ENGAGE_CTRL_ANALYTIQUE.eana_ht_saisie%TYPE;
       my_eana_tva_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_tva_saisie%TYPE;
       my_eana_ttc_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eana_ht_saisie:=round(my_eana_ht_saisie, my_nb_decimales);
            my_eana_ttc_saisie:=round(my_eana_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_can_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code analytique pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eana_ht_saisie<0 OR my_eana_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eana_tva_saisie := my_eana_ttc_saisie - my_eana_ht_saisie;
			IF my_eana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eana_ht_saisie,my_eana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_eana_montant_budgetaire THEN
			    my_eana_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_analytique_seq.NEXTVAL INTO my_eana_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ANALYTIQUE VALUES (my_eana_id,
			       a_exe_ordre, a_eng_id, my_can_id, my_eana_montant_budgetaire, my_eana_montant_budgetaire,
				   my_eana_ht_saisie, my_eana_tva_saisie, my_eana_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_analytique(a_eng_id);
            
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_econ_id	               ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
       my_conv_ordre 	   		   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_econ_montant_budgetaire  ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_econ_ht_saisie	  	   ENGAGE_CTRL_CONVENTION.econ_ht_saisie%TYPE;
       my_econ_tva_saisie		   ENGAGE_CTRL_CONVENTION.econ_tva_saisie%TYPE;
       my_econ_ttc_saisie		   ENGAGE_CTRL_CONVENTION.econ_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_econ_ht_saisie:=round(my_econ_ht_saisie, my_nb_decimales);
            my_econ_ttc_saisie:=round(my_econ_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_conv_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette convention pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_econ_ht_saisie<0 OR my_econ_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_econ_tva_saisie := my_econ_ttc_saisie - my_econ_ht_saisie;
			IF my_econ_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_econ_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_econ_ht_saisie,my_econ_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_econ_montant_budgetaire THEN
			    my_econ_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_convention_seq.NEXTVAL INTO my_econ_id FROM dual;

			INSERT INTO ENGAGE_CTRL_CONVENTION VALUES (my_econ_id,
			       a_exe_ordre, a_eng_id, my_conv_ordre, my_econ_montant_budgetaire, my_econ_montant_budgetaire,
				   my_econ_ht_saisie, my_econ_tva_saisie, my_econ_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_convention(a_eng_id);
            
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
	    	Apres_Engage.convention(my_econ_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_econ_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ehom_id	               ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
       my_typa_id	  	   		   ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre	  	   		   ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_ehom_montant_budgetaire  ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_ehom_ht_saisie	  	   ENGAGE_CTRL_HORS_MARCHE.ehom_ht_saisie%TYPE;
       my_ehom_tva_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_tva_saisie%TYPE;
       my_ehom_ttc_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code de nomenclature.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ehom_ht_saisie:=round(my_ehom_ht_saisie, my_nb_decimales);
            my_ehom_ttc_saisie:=round(my_ehom_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
			     WHERE eng_id=a_eng_id AND ce_ordre=my_ce_ordre AND typa_id=my_typa_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code nomenclature pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_ehom_ht_saisie<0 OR my_ehom_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_ehom_tva_saisie := my_ehom_ttc_saisie - my_ehom_ht_saisie;
			IF my_ehom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_ehom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ehom_ht_saisie,my_ehom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_ehom_montant_budgetaire THEN
			    my_ehom_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_hors_marche_seq.NEXTVAL INTO my_ehom_id FROM dual;

			INSERT INTO ENGAGE_CTRL_HORS_MARCHE VALUES (my_ehom_id,
			       a_exe_ordre, a_eng_id, my_typa_id, my_ce_ordre, my_ehom_montant_budgetaire, my_ehom_montant_budgetaire,
				   my_ehom_ht_saisie, my_ehom_ht_saisie, my_ehom_tva_saisie, my_ehom_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_hors_marche(a_eng_id);
            
			Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
	    	Apres_Engage.hors_marche(my_ehom_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ehom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_emar_id	               ENGAGE_CTRL_MARCHE.emar_id%TYPE;
       my_att_ordre	  	   		   ENGAGE_CTRL_MARCHE.att_ordre%TYPE;
       my_emar_montant_budgetaire  ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_emar_ht_saisie	  	   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
       my_emar_tva_saisie		   ENGAGE_CTRL_MARCHE.emar_tva_saisie%TYPE;
       my_emar_ttc_saisie		   ENGAGE_CTRL_MARCHE.emar_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
   	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
   	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'attribution.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_emar_ht_saisie:=round(my_emar_ht_saisie, my_nb_decimales);
            my_emar_ttc_saisie:=round(my_emar_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_att_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette attribution pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_emar_ht_saisie<0 OR my_emar_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_emar_tva_saisie := my_emar_ttc_saisie - my_emar_ht_saisie;
			IF my_emar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_emar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_emar_ht_saisie,my_emar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_emar_montant_budgetaire THEN
			    my_emar_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour un engagement');
			END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_marche_seq.NEXTVAL INTO my_emar_id FROM dual;

			INSERT INTO ENGAGE_CTRL_MARCHE VALUES (my_emar_id,
			       a_exe_ordre, a_eng_id, my_att_ordre, my_emar_montant_budgetaire, my_emar_montant_budgetaire,
				   my_emar_ht_saisie,my_emar_ht_saisie, my_emar_tva_saisie, my_emar_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_marche(a_eng_id);
            
			Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
	    	Apres_Engage.marche(my_emar_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_emar_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_epco_id	               ENGAGE_CTRL_PLANCO.epco_id%TYPE;
       my_pco_num	  	   		   ENGAGE_CTRL_PLANCO.pco_num%TYPE;
	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_epco_montant_budgetaire  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_epco_ht_saisie	  	   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
       my_epco_tva_saisie		   ENGAGE_CTRL_PLANCO.epco_tva_saisie%TYPE;
       my_epco_ttc_saisie		   ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre				   ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_epco_ht_saisie:=round(my_epco_ht_saisie, my_nb_decimales);
            my_epco_ttc_saisie:=round(my_epco_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_pco_num;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette imputation pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_epco_ht_saisie<0 OR my_epco_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_epco_tva_saisie := my_epco_ttc_saisie - my_epco_ht_saisie;
			IF my_epco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_epco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_epco_ht_saisie,my_epco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_epco_montant_budgetaire THEN
			    my_epco_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_planco_seq.NEXTVAL INTO my_epco_id FROM dual;

			INSERT INTO ENGAGE_CTRL_PLANCO VALUES (my_epco_id,
			       a_exe_ordre, a_eng_id, my_pco_num, my_epco_montant_budgetaire, my_epco_montant_budgetaire,
				   my_epco_ht_saisie, my_epco_tva_saisie, my_epco_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_planco(a_eng_id);
            
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_epco_montant_budgetaire;
		END LOOP;
   END;

END;
/

CREATE OR REPLACE PROCEDURE JEFY_DEPENSE.remplacer_prorata(
   a_exe_ordre     jefy_admin.exercice.exe_ordre%type,
   a_old_tap_id    jefy_admin.taux_prorata.tap_id%type,
   a_new_tap_id    jefy_admin.taux_prorata.tap_id%type)
IS
           CURSOR liste IS SELECT * FROM  ENGAGE_BUDGET WHERE exe_ordre=a_exe_ordre and eng_montant_budgetaire_reste>0 AND tap_id=a_old_tap_id;
		   engage ENGAGE_BUDGET%ROWTYPE;
		   engageselect ENGAGE_BUDGET%ROWTYPE;
		   bud ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

           CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
		   depense DEPENSE_BUDGET%ROWTYPE;
		   depbud DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

      	   CURSOR listeaction(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
		   action COMMANDE_CTRL_ACTION%ROWTYPE;
      	   CURSOR listeanalytique(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
		   ANALYTIQUE COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;
      	   CURSOR listeconvention(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
		   convention COMMANDE_CTRL_CONVENTION%ROWTYPE;
      	   CURSOR listehors_marche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
		   hors_marche COMMANDE_CTRL_HORS_MARCHE%ROWTYPE;
      	   CURSOR listemarche(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
		   marche COMMANDE_CTRL_MARCHE%ROWTYPE;
      	   CURSOR listeplanco(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE) IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
		   planco COMMANDE_CTRL_PLANCO%ROWTYPE;
		   
		   my_nb INTEGER;
		   cdebudget COMMANDE_BUDGET%ROWTYPE;
		   chaine_action  VARCHAR2(3000);
		   chaine_analytique  VARCHAR2(3000);
		   chaine_convention  VARCHAR2(3000);
		   chaine_hors_marche  VARCHAR2(3000);
		   chaine_marche  VARCHAR2(3000);
		   chaine_planco  VARCHAR2(3000);
BEGIN
           OPEN liste();
 		   LOOP
		      FETCH  liste INTO engage;
		      EXIT WHEN liste%NOTFOUND;
			  
              -- si l''engagement n''a pas de TVA, le changement de prorata ne change pas le budgetaire
              if engage.eng_tva_saisie<>0 then
			     -- correction de l'engagement
			     bud:=Budget.calculer_budgetaire(engage.exe_ordre, a_new_tap_id, engage.org_id, engage.eng_ht_saisie, engage.eng_ttc_saisie);
dbms_output.put_line('bud='||bud);
			     depbud:=0;
                 OPEN listedepense(engage.eng_id);
 	             LOOP
		            FETCH  listedepense INTO depense;
		            EXIT WHEN listedepense%NOTFOUND;
				   
				    depbud:=depbud+Budget.calculer_budgetaire(depense.exe_ordre, a_new_tap_id, engage.org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
dbms_output.put_line('depbud='||depbud);
                 END LOOP;
			     CLOSE listedepense;
			  
			     UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=bud, eng_montant_budgetaire_reste=bud-depbud, tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
select * into engageselect from engage_budget where eng_id=engage.eng_id;
dbms_output.put_line('new eng, bud='||engageselect.eng_montant_budgetaire||', reste='||engageselect.eng_montant_budgetaire_reste);
                 engager.UPD_ENGAGE(engage.eng_id, engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.utl_ordre,
                        get_chaine_action(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_analytique(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_convention(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id), 
                        get_chaine_hors_marche(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id),
                        get_chaine_marche(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id), 
                        get_chaine_planco_eng(engage.eng_ht_saisie, engage.eng_ttc_saisie, engage.eng_id));

select * into engageselect from engage_budget where eng_id=engage.eng_id;
dbms_output.put_line('new eng, bud='||engageselect.eng_montant_budgetaire||', reste='||engageselect.eng_montant_budgetaire_reste);
                 
			     Corriger.upd_engage_reste(engage.eng_id);
                 verifier.verifier_engage_coherence(engage.eng_id);
			     Budget.maj_budget(engage.exe_ordre, engage.org_id, engage.tcd_ordre);
             	  
  			     -- correction du commande_budget associe ... il doit normalement y en avoir un seul
			     SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id;
			  
			     IF my_nb>0 THEN
			   
			        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
				 
				    IF my_nb=1 THEN
				      SELECT * INTO cdebudget FROM COMMANDE_BUDGET WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			                 AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				             AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
					  
					  chaine_action:='';
 	 	              OPEN listeaction(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeaction INTO action;
		      		       EXIT WHEN listeaction%NOTFOUND;
					  
					  	   chaine_action:=chaine_action||action.tyac_id||'$'||action.cact_ht_saisie||'$'||action.cact_ttc_saisie||'$'||action.cact_pourcentage||'$';
					  END LOOP;
					  CLOSE listeaction;
					  chaine_action:=chaine_action||'$';

					  chaine_analytique:='';
 	 	              OPEN listeanalytique(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeanalytique INTO analytique;
		      		       EXIT WHEN listeanalytique%NOTFOUND;
					  
					  	   chaine_analytique:=chaine_analytique||analytique.can_id||'$'||analytique.cana_ht_saisie||'$'||analytique.cana_ttc_saisie||'$'||analytique.cana_pourcentage||'$';
					  END LOOP;
					  CLOSE listeanalytique;
					  chaine_analytique:=chaine_analytique||'$';

					  chaine_convention:='';
 	 	              OPEN listeconvention(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeconvention INTO convention;
		      		       EXIT WHEN listeconvention%NOTFOUND;
					  
					  	   chaine_convention:=chaine_convention||convention.conv_ordre||'$'||convention.ccon_ht_saisie||'$'||convention.ccon_ttc_saisie||'$'||convention.ccon_pourcentage||'$';
					  END LOOP;
					  CLOSE listeconvention;
					  chaine_convention:=chaine_convention||'$';

					  chaine_hors_marche:='';
 	 	              OPEN listehors_marche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listehors_marche INTO hors_marche;
		      		       EXIT WHEN listehors_marche%NOTFOUND;
					  
					  	   chaine_hors_marche:=chaine_hors_marche||hors_marche.typa_id||'$'||hors_marche.ce_ordre||'$'||hors_marche.chom_ht_saisie||'$'||hors_marche.chom_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listehors_marche;
					  chaine_hors_marche:=chaine_hors_marche||'$';

					  chaine_marche:='';
 	 	              OPEN listemarche(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listemarche INTO marche;
		      		       EXIT WHEN listemarche%NOTFOUND;
					  
					  	   chaine_marche:=chaine_marche||marche.att_ordre||'$'||marche.cmar_ht_saisie||'$'||marche.cmar_ttc_saisie||'$';
					  END LOOP;
					  CLOSE listemarche;
					  chaine_marche:=chaine_marche||'$';

					  chaine_planco:='';
 	 	              OPEN listeplanco(cdebudget.cbud_id);
 		              LOOP
		       		       FETCH  listeplanco INTO planco;
		      		       EXIT WHEN listeplanco%NOTFOUND;
					  
					  	   chaine_planco:=chaine_planco||planco.pco_num||'$'||planco.cpco_ht_saisie||'$'||planco.cpco_ttc_saisie||'$'||planco.cpco_pourcentage||'$';
					  END LOOP;
					  CLOSE listeplanco;
					  chaine_planco:=chaine_planco||'$';
					  
					     Commander.upd_commande_budget(cdebudget.cbud_id, cdebudget.cbud_ht_saisie, cdebudget.cbud_ttc_saisie,
					    	  a_new_tap_id, chaine_action, chaine_analytique, chaine_convention, chaine_hors_marche, chaine_marche, chaine_planco);
				    END IF;
				 	 
                 END IF;
              else
                 update COMMANDE_BUDGET SET tap_id=a_new_tap_id WHERE org_id=engage.org_id AND exe_ordre=engage.exe_ordre 
			            AND tcd_ordre=engage.tcd_ordre AND tap_id=engage.tap_id 
				        AND comm_id IN (SELECT comm_id FROM COMMANDE_ENGAGEMENT WHERE eng_id=engage.eng_id);
                 UPDATE ENGAGE_BUDGET SET tap_id=a_new_tap_id WHERE eng_id=engage.eng_id;
              end if;		
			      
		  END LOOP;
		  CLOSE liste;
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Reverser
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

   PROCEDURE ins_reverse_papier (
      a_dpp_id IN OUT          DEPENSE_PAPIER.dpp_id%TYPE,
      a_exe_ordre              DEPENSE_PAPIER.exe_ordre%TYPE,
      a_dpp_numero_facture    DEPENSE_PAPIER.dpp_numero_facture%TYPE,
      a_dpp_ht_initial          DEPENSE_PAPIER.dpp_ht_initial%TYPE,
      a_dpp_ttc_initial          DEPENSE_PAPIER.dpp_ttc_initial%TYPE,
      a_fou_ordre              DEPENSE_PAPIER.fou_ordre%TYPE,
      a_rib_ordre              DEPENSE_PAPIER.rib_ordre%TYPE,
      a_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE,
      a_dpp_date_facture      DEPENSE_PAPIER.dpp_date_facture%TYPE,
      a_dpp_date_saisie       DEPENSE_PAPIER.dpp_date_saisie%TYPE,
      a_dpp_date_reception      DEPENSE_PAPIER.dpp_date_reception%TYPE,
      a_dpp_date_service_fait DEPENSE_PAPIER.dpp_date_service_fait%TYPE,
      a_dpp_nb_piece          DEPENSE_PAPIER.dpp_nb_piece%TYPE,
      a_utl_ordre              DEPENSE_PAPIER.utl_ordre%TYPE,
      a_dpp_id_reversement    DEPENSE_PAPIER.dpp_id_reversement%TYPE
   ) IS
      my_dpp_tva_initial      DEPENSE_PAPIER.dpp_tva_initial%TYPE;
      my_dpco_ht_saisie          DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
      my_dpco_ttc_saisie         DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
      my_sum_rev_ht              DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
      my_sum_rev_ttc          DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
      my_mod_ordre              DEPENSE_PAPIER.mod_ordre%TYPE;
      my_dpp_id_reversement   DEPENSE_PAPIER.dpp_id_reversement%TYPE;
      my_nb       INTEGER;
      my_nb_verif    integer;
   BEGIN
        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);

        -- pour un ORV il faut des montants negatifs.
           IF a_dpp_ht_initial>0 OR a_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dpp_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture initiale n''existe pas (dpp_id='||a_dpp_id_reversement||')');
        END IF;

        -- on verifie la coherence des montants.
        IF ABS(a_dpp_ht_initial)>ABS(a_dpp_ttc_initial) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dpp_tva_initial:=Liquider_Outils.get_tva(a_dpp_ht_initial, a_dpp_ttc_initial);

        -- on verifie que la facture referencee n'est pas un ORV (pas d'ORV sur un ORV);
         SELECT dpp_id_reversement, mod_ordre INTO my_dpp_id_reversement, my_mod_ordre
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id_reversement;

        IF my_dpp_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ordre de reversement a partir d''un autre ORV');
        END IF;

        -- verifier que les montants du total des ORV ne depassent pas ceux mandates et vises de la facture d'origine.
        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dpco_ht_saisie, my_dpco_ttc_saisie
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET
              WHERE dpp_id=a_dpp_id_reversement) AND dpco_ttc_saisie>0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
           FROM DEPENSE_CTRL_PLANCO WHERE dpco_ttc_saisie<0 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE
             dpp_id IN (SELECT dpp_id FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id_reversement));

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id_reversement)
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dpco_ht_saisie<ABS(my_sum_rev_ht)+ABS(a_dpp_ht_initial) OR
           my_dpco_ttc_saisie<ABS(my_sum_rev_ttc)+ABS(a_dpp_ttc_initial)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui vise de la facture initiale');
        END IF;
        
           -- enregistrement dans la table.
        IF a_dpp_id IS NULL THEN
           SELECT depense_papier_seq.NEXTVAL INTO a_dpp_id FROM dual;
        END IF;

        INSERT INTO DEPENSE_PAPIER VALUES (a_dpp_id, a_exe_ordre, a_dpp_numero_facture, 0,
           0, 0, a_fou_ordre, a_rib_ordre, my_mod_ordre, a_dpp_date_facture, a_dpp_date_saisie, a_dpp_date_reception,
           a_dpp_date_service_fait, a_dpp_nb_piece, a_utl_ordre, a_dpp_id_reversement,
           a_dpp_ht_initial, my_dpp_tva_initial, a_dpp_ttc_initial,null,null,null,null,null,null);
   END;

   PROCEDURE ins_reverse (
      a_dep_id IN OUT        DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre            DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie        DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre            DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement    DEPENSE_BUDGET.dep_id_reversement%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
      my_org_id              engage_budget.org_id%type;
   BEGIN
          IF a_dep_ttc_saisie<>0 THEN
             select org_id into my_org_id from engage_budget where eng_id=a_eng_id;
               
             -- verifier qu'on a le droit de liquider sur cet exercice.
             Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

             -- lancement des differentes procedures d'insertion des tables de depense.
             ins_reverse_budget(a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, a_dep_ht_saisie, a_dep_ttc_saisie,
                a_tap_id, a_utl_ordre, a_dep_id_reversement);

              UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie+a_dep_ht_saisie,
                  dpp_tva_saisie=dpp_tva_saisie+a_dep_ttc_saisie-a_dep_ht_saisie,
                  dpp_ttc_saisie=dpp_ttc_saisie+a_dep_ttc_saisie
                 WHERE dpp_id=a_dpp_id;

             ins_reverse_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
             ins_reverse_ctrl_analytique(a_exe_ordre, a_dep_id, a_chaine_analytique);
             ins_reverse_ctrl_convention(a_exe_ordre, a_dep_id, a_chaine_convention);
             ins_reverse_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
             ins_reverse_ctrl_marche(a_exe_ordre, a_dep_id, a_chaine_marche);
             ins_reverse_ctrl_planco(a_exe_ordre, a_dep_id, a_chaine_planco);

             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Corriger.upd_engage_reste(a_eng_id);

             -- on verifie la coherence des montants entre les differents depense_.
             Verifier.verifier_depense_coherence(a_dep_id);
             Verifier.verifier_depense_pap_coherence(a_dpp_id);
             -- on verifie si la coherence des montants budgetaires restant est  conservee.
             -- on vire ca ... car comme l'OR est jsute créé et donc pas encore visé ca ne change pas le restant de l'engagement
             --Verifier.verifier_engage_coherence(a_eng_id);
        END IF;
   END;

   PROCEDURE del_reverse (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_nb                            INTEGER;
      my_exe_ordre                    DEPENSE_BUDGET.exe_ordre%TYPE;
      my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
      my_dpp_id                        DEPENSE_BUDGET.dpp_id%TYPE;
      my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
      my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
      my_dpco_id                    DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
      my_org_id              engage_budget.org_id%type;
      my_eng_id              engage_budget.eng_id%type;

      CURSOR liste  IS SELECT dpco_id FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
   BEGIN

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ORV n''existe pas ou est deja annule (dep_id:'||a_dep_id||')');
        END IF;

           SELECT  d.exe_ordre, d.dep_montant_budgetaire, d.dpp_id, d.dep_ht_saisie, d.dep_ttc_saisie, d.eng_id
           INTO my_exe_ordre, my_montant_budgetaire, my_dpp_id, my_dep_ht_saisie, my_dep_ttc_saisie, my_eng_id
           FROM DEPENSE_BUDGET d WHERE d.dep_id=a_dep_id;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        select org_id into my_org_id from engage_budget where eng_id=my_eng_id;
        Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

           -- on teste si c'est un ORV.
           IF my_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        Verifier.verifier_util_depense_budget(a_dep_id);

        -- on met a jour les montants de la depense papier.
        UPDATE DEPENSE_PAPIER SET dpp_ht_saisie=dpp_ht_saisie-my_dep_ht_saisie,
               dpp_tva_saisie=dpp_tva_saisie-my_dep_ttc_saisie+my_dep_ht_saisie,
               dpp_ttc_saisie=dpp_ttc_saisie-my_dep_ttc_saisie
           WHERE dpp_id=my_dpp_id;

        -- tout est bon ... on supprime la depense.
        log_reverse_budget(a_dep_id,a_utl_ordre);

        DELETE FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;
        DELETE FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_dpco_id;
           EXIT WHEN liste%NOTFOUND;
              jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);
              DELETE FROM DEPENSE_CTRL_PLANCO WHERE dpco_id=my_dpco_id;
        END LOOP;
        CLOSE liste;

        DELETE FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

        -- on verifie si la coherence des montants budgetaires restant est  conservee.
        Verifier.verifier_depense_pap_coherence(my_dpp_id);

        Apres_Liquide.del_depense_budget(my_eng_id, a_dep_id);
   END;

      PROCEDURE del_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE)
      IS
       my_nb         INTEGER;
       my_exe_ordre     DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dpp_ttc_initial  DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:'||a_dpp_id||')');
        END IF;

           SELECT exe_ordre, dpp_ttc_initial INTO my_exe_ordre, my_dpp_ttc_initial  FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

           -- on teste si ce n'est pas un ORV.
        IF my_dpp_ttc_initial>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour une liquidation il faut utiliser le package "liquider"');
        END IF;

        -- verifier qu'on a le droit de liquider sur cet exercice.
        --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);

        Verifier.verifier_util_depense_papier(a_dpp_id);

        log_reverse_papier(a_dpp_id, a_utl_ordre);

        DELETE FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        Apres_Liquide.del_depense_papier(a_dpp_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

      PROCEDURE log_reverse_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE,
      a_utl_ordre           Z_DEPENSE_BUDGET.zdep_utl_ordre%TYPE
   ) IS
      my_zdep_id                    Z_DEPENSE_BUDGET.zdep_id%TYPE;
   BEGIN
        SELECT z_depense_budget_seq.NEXTVAL INTO my_zdep_id FROM dual;

        INSERT INTO Z_DEPENSE_BUDGET SELECT my_zdep_id, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_BUDGET e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ACTION SELECT z_depense_ctrl_action_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ACTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_ANALYTIQUE SELECT z_depense_ctrl_analytique_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_ANALYTIQUE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_CONVENTION SELECT z_depense_ctrl_convention_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_CONVENTION e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_HORS_MARCHE SELECT z_depense_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_HORS_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_MARCHE SELECT z_depense_ctrl_marche_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_MARCHE e WHERE dep_id=a_dep_id;

        INSERT INTO Z_DEPENSE_CTRL_PLANCO SELECT z_depense_ctrl_planco_seq.NEXTVAL, e.*, my_zdep_id
          FROM DEPENSE_CTRL_PLANCO e WHERE dep_id=a_dep_id;
   END;

   PROCEDURE log_reverse_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE,
      a_utl_ordre           Z_DEPENSE_PAPIER.zdpp_utl_ordre%TYPE
   ) IS
   BEGIN
           INSERT INTO Z_DEPENSE_PAPIER SELECT z_depense_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, e.*
          FROM DEPENSE_PAPIER e WHERE dpp_id=a_dpp_id;
   END;

   PROCEDURE ins_reverse_budget (
      a_dep_id IN OUT         DEPENSE_BUDGET.dep_id%TYPE,
      a_exe_ordre             DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dpp_id                 DEPENSE_BUDGET.dpp_id%TYPE,
      a_eng_id                 DEPENSE_BUDGET.eng_id%TYPE,
      a_dep_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie         DEPENSE_BUDGET.dep_ttc_saisie%TYPE,
      a_tap_id                 DEPENSE_BUDGET.tap_id%TYPE,
      a_utl_ordre             DEPENSE_BUDGET.utl_ordre%TYPE,
      a_dep_id_reversement     DEPENSE_BUDGET.dep_id_reversement%TYPE
   ) IS
       my_nb                 INTEGER;
       my_nb_verif    integer;

       my_eng_id             ENGAGE_BUDGET.eng_id%TYPE;
       my_org_id             ENGAGE_BUDGET.org_id%TYPE;
       my_exe_ordre       ENGAGE_BUDGET.exe_ordre%TYPE;

       my_montant_budgetaire DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;

       my_dpp_ht_initial         DEPENSE_PAPIER.dpp_ht_initial%TYPE;
       my_dpp_tva_initial     DEPENSE_PAPIER.dpp_tva_initial%TYPE;
       my_dpp_ttc_initial     DEPENSE_PAPIER.dpp_ttc_initial%TYPE;
       my_dpp_ttc_saisie     DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
       my_dpp_id_reversement DEPENSE_PAPIER.dpp_id_reversement%TYPE;
       my_dpp_id_origine DEPENSE_BUDGET.dpp_id%TYPE;

       my_dep_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ht_saisie         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_tva_saisie     DEPENSE_BUDGET.dep_tva_saisie%TYPE;
       my_sum_ttc_saisie     DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_sum_rev_ht         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_sum_rev_ttc         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dep_id_reversement DEPENSE_BUDGET.dep_id_reversement%TYPE;

       my_dep_ht_init         DEPENSE_BUDGET.dep_ht_saisie%TYPE;
       my_dep_ttc_init         DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
       my_dpp_exe_ordre         DEPENSE_PAPIER.exe_ordre%TYPE;
       my_dep_exe_ordre      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_dep_tap_id         DEPENSE_BUDGET.tap_id%TYPE;
   BEGIN

           -- pour un ORV il faut des montants negatifs.
           IF a_dep_ht_saisie>0 OR a_dep_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Les montants d''un ordre de reversement doivent etre negatifs.');
        END IF;

        -- il faut un dpp_id_reversement (c'est un ORV).
        IF a_dep_id_reversement IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pour un ordre de reversement il faut la reference de la facture initiale');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (dpp_id='||a_dpp_id||')');
        END IF;

           SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La facture initiale n''existe pas (dep_id='||a_dep_id_reversement||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT org_id, exe_ordre INTO my_org_id, my_exe_ordre FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
          SELECT exe_ordre, dpp_id_reversement INTO my_dpp_exe_ordre, my_dpp_id_reversement
         FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- verification de la coherence de l'exercice.
        IF a_exe_ordre<>my_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ORV doit etre sur le meme exercice que l''engagement');
        END IF;

        IF a_exe_ordre<>my_dpp_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
        END IF;

        SELECT exe_ordre, eng_id, tap_id, dep_id_reversement, dpp_id
          INTO my_dep_exe_ordre, my_eng_id, my_dep_tap_id, my_dep_id_reversement, my_dpp_id_origine
          FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id_reversement;

        IF my_dpp_id_reversement<>my_dpp_id_origine THEN
           RAISE_APPLICATION_ERROR(-20001,'la facture papier d''origine est differente de celle de la depense budget d''origine');
        END IF;

        IF a_eng_id<>my_eng_id THEN
           RAISE_APPLICATION_ERROR(-20001,'L''ordre de reversement doit etre sur le meme engagement que la facture initiale');
        END IF;

        IF my_dep_exe_ordre<>my_dep_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit etre sur le meme exercice que la facture initiale');
        END IF;

        IF a_tap_id<>my_dep_tap_id THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''ordre de reversement doit avoir le meme prorata que la facture initiale.');
        END IF;

        IF my_dep_id_reversement IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas faire un ORV a partir d''un autre ORV.');
        END IF;

        -- verification des coherences entre les sommes (si elles ne depassent pas).
        SELECT dpp_ttc_saisie
               INTO my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0) , NVL(SUM(dep_ttc_saisie),0)
               INTO my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        -- on verifie la coherence des montants.
         IF ABS(a_dep_ht_saisie)>ABS(a_dep_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        -- on teste si le ttc de la depense papier est du meme signe que celui de depense_budget.
        IF my_dpp_ttc_saisie>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Incoherence de signe entre le montant de la facture et le montant budgetaire');
        END IF;

        -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
        my_dep_tva_saisie:=Liquider_Outils.get_tva(a_dep_ht_saisie, a_dep_ttc_saisie);

        -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declaré dans la papier.
        /*IF ABS(my_sum_ht_saisie+a_dep_ht_saisie) > ABS(my_dpp_ht_saisie) OR
           ABS(my_sum_tva_saisie+my_dep_tva_saisie) > ABS(my_dpp_tva_saisie) OR
           ABS(my_sum_ttc_saisie+a_dep_ttc_saisie) > ABS(my_dpp_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
        END IF;*/

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
              a_dep_ht_saisie,a_dep_ttc_saisie);

        -- on verifie que la somme des ORV ne depasse pas le montant de la facture initiale ... mandate et vise !!.
          SELECT NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_ttc_saisie),0) INTO my_dep_ht_init, my_dep_ttc_init
           FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id_reversement AND dpco_montant_budgetaire>=0 AND man_id IN
               (SELECT man_id FROM maracuja.mandat WHERE man_etat IN ('VISE','PAYE'));

        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_ttc_saisie),0) INTO my_sum_rev_ht, my_sum_rev_ttc
          FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id_reversement;

        select count(*) into my_nb_verif from depense_ctrl_planco 
           where dep_id=a_dep_id_reversement
             and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
        IF my_nb_verif=0 and (my_dep_ht_init < ABS(my_sum_rev_ht) + ABS(a_dep_ht_saisie) OR
           my_dep_ttc_init < ABS(my_sum_rev_ttc) + ABS(a_dep_ttc_saisie)) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le montant d''un ordre de reversement ne peut pas depasser celui de la facture initiale');
        END IF;

        -- insertion dans la table.
        IF a_dep_id IS NULL THEN
           SELECT depense_budget_seq.NEXTVAL INTO a_dep_id FROM dual;
        END IF;

        -- on reverse pour liberer les credits.
        INSERT INTO DEPENSE_BUDGET VALUES (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_montant_budgetaire,
           a_dep_ht_saisie, my_dep_tva_saisie, a_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

        -- procedure de verification
        Apres_Liquide.Budget(a_dep_id);
   END;

   PROCEDURE ins_reverse_ctrl_action (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dact_id                   DEPENSE_CTRL_ACTION.dact_id%TYPE;
       my_tyac_id                        DEPENSE_CTRL_ACTION.tyac_id%TYPE;
       my_dact_montant_budgetaire  DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       sum_dact_montant_budgetaire DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
       my_dact_ht_saisie             DEPENSE_CTRL_ACTION.dact_ht_saisie%TYPE;
       my_dact_tva_saisie           DEPENSE_CTRL_ACTION.dact_tva_saisie%TYPE;
       my_dact_ttc_saisie           DEPENSE_CTRL_ACTION.dact_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                   DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

          IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dact_ht_saisie)>ABS(my_dact_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dact_tva_saisie:=Liquider_Outils.get_tva(my_dact_ht_saisie, my_dact_ttc_saisie);

            -- on calcule le montant budgetaire.
            my_dact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dact_ht_saisie,my_dact_ttc_saisie);

            -- Pour les O.R.
            IF my_dact_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un ORV.');
            END IF;

            -- verifier que l'action qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR pour cette action et que l'action existe bien au depart.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ACTION
               WHERE tyac_id=my_tyac_id
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet ORV et cette action n''est pas autorisee (tyac_id='||my_tyac_id||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dact_montant_budgetaire) THEN
                my_dact_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dact_montant_budgetaire),0) INTO sum_dact_montant_budgetaire
              FROM DEPENSE_CTRL_ACTION WHERE tyac_id=my_tyac_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dact_montant_budgetaire+my_dact_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette action (tyac_id='||my_tyac_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_action_seq.NEXTVAL INTO my_dact_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ACTION VALUES (my_dact_id, a_exe_ordre, a_dep_id, my_tyac_id,
                my_dact_montant_budgetaire, my_dact_ht_saisie, my_dact_tva_saisie, my_dact_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.action(my_dact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dact_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_analytique (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dana_id                   DEPENSE_CTRL_ANALYTIQUE.dana_id%TYPE;
       my_can_id                        DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_dana_montant_budgetaire  DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       sum_dana_montant_budgetaire DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
       my_dana_ht_saisie             DEPENSE_CTRL_ANALYTIQUE.dana_ht_saisie%TYPE;
       my_dana_tva_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_tva_saisie%TYPE;
       my_dana_ttc_saisie           DEPENSE_CTRL_ANALYTIQUE.dana_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dana_ht_saisie)>ABS(my_dana_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dana_tva_saisie:=Liquider_Outils.get_tva(my_dana_ht_saisie, my_dana_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dana_ht_saisie,my_dana_ttc_saisie);

            -- Pour les O.R.
            IF my_dana_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_ANALYTIQUE
               WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code analytique n''est pas autorise pour cette depense (can_id='||my_can_id||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dana_montant_budgetaire) THEN
                my_dana_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dana_montant_budgetaire),0) INTO sum_dana_montant_budgetaire
               FROM DEPENSE_CTRL_ANALYTIQUE WHERE can_id=my_can_id
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);
               
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dana_montant_budgetaire+my_dana_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code analytique (can_id='||my_can_id||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_analytique_seq.NEXTVAL INTO my_dana_id FROM dual;

            INSERT INTO DEPENSE_CTRL_ANALYTIQUE VALUES (my_dana_id,
                   a_exe_ordre, a_dep_id, my_can_id, my_dana_montant_budgetaire,
                   my_dana_ht_saisie, my_dana_tva_saisie, my_dana_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.analytique(my_dana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dana_montant_budgetaire;
        END LOOP;
   END;

      PROCEDURE ins_reverse_ctrl_convention (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dcon_id                   DEPENSE_CTRL_CONVENTION.dcon_id%TYPE;
       my_conv_ordre                    DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_dcon_montant_budgetaire  DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       sum_dcon_montant_budgetaire DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
       my_dcon_ht_saisie             DEPENSE_CTRL_CONVENTION.dcon_ht_saisie%TYPE;
       my_dcon_tva_saisie           DEPENSE_CTRL_CONVENTION.dcon_tva_saisie%TYPE;
       my_dcon_ttc_saisie           DEPENSE_CTRL_CONVENTION.dcon_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere la convention.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dcon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dcon_ht_saisie)>ABS(my_dcon_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dcon_tva_saisie:=Liquider_Outils.get_tva(my_dcon_ht_saisie, my_dcon_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dcon_ht_saisie,my_dcon_ttc_saisie);

            -- Pour les O.R.
            IF my_dcon_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_CONVENTION
               WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette convention n''est pas autorisee pour cette depense (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- on teste si il n'y a pas assez de dispo.
              IF ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dcon_montant_budgetaire) THEN
                my_dcon_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dcon_montant_budgetaire),0) INTO sum_dcon_montant_budgetaire
               FROM DEPENSE_CTRL_CONVENTION WHERE conv_ordre=my_conv_ordre 
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
               OR dep_id_reversement=my_dep_id_reversement);
               
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dcon_montant_budgetaire+my_dcon_montant_budgetaire<0) THEN
                 RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cet engagement pour cette convention (conv_ordre='||my_conv_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_convention_seq.NEXTVAL INTO my_dcon_id FROM dual;

            INSERT INTO DEPENSE_CTRL_CONVENTION VALUES (my_dcon_id,
                   a_exe_ordre, a_dep_id, my_conv_ordre, my_dcon_montant_budgetaire,
                   my_dcon_ht_saisie, my_dcon_tva_saisie, my_dcon_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.convention(my_dcon_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dcon_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_hors_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dhom_id                   DEPENSE_CTRL_HORS_MARCHE.dhom_id%TYPE;
       my_typa_id                        DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre                        DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_dhom_montant_budgetaire  DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       sum_dhom_montant_budgetaire DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
       my_dhom_ht_saisie             DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
       my_dhom_tva_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_tva_saisie%TYPE;
       my_dhom_ttc_saisie           DEPENSE_CTRL_HORS_MARCHE.dhom_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_fou_ordre                   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.exe_ordre, e.fou_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre, my_fou_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type_çchat.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dhom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dhom_ht_saisie)>ABS(my_dhom_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dhom_tva_saisie:=Liquider_Outils.get_tva(my_dhom_ht_saisie, my_dhom_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dhom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dhom_ht_saisie,my_dhom_ttc_saisie);

            -- Pour les O.R.
            IF my_dhom_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE
               WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et ce code nomenclature n''est pas autorise pour cette depense (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dhom_montant_budgetaire) THEN
                my_dhom_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dhom_montant_budgetaire),0) INTO sum_dhom_montant_budgetaire
              FROM DEPENSE_CTRL_HORS_MARCHE WHERE typa_id=my_typa_id AND ce_ordre=my_ce_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);

            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dhom_montant_budgetaire+my_dhom_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour ce code nomenclature (ce_ordre='||my_ce_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_hors_marche_seq.NEXTVAL INTO my_dhom_id FROM dual;

            INSERT INTO DEPENSE_CTRL_HORS_MARCHE VALUES (my_dhom_id,
                   a_exe_ordre, a_dep_id, my_typa_id, my_ce_ordre, my_dhom_montant_budgetaire,
                   my_dhom_ht_saisie, my_dhom_tva_saisie, my_dhom_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.hors_marche(my_dhom_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dhom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_marche (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dmar_id                   DEPENSE_CTRL_MARCHE.dmar_id%TYPE;
       my_att_ordre                    DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
       my_dmar_montant_budgetaire  DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       sum_dmar_montant_budgetaire DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dmar_ht_saisie             DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_dmar_tva_saisie           DEPENSE_CTRL_MARCHE.dmar_tva_saisie%TYPE;
       my_dmar_ttc_saisie           DEPENSE_CTRL_MARCHE.dmar_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                      DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
       my_dpp_id                   DEPENSE_BUDGET.dpp_id%TYPE;
       my_fou_ordre                   DEPENSE_PAPIER.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.dpp_id, d.exe_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre, my_dpp_id,
                    my_exe_ordre, my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        SELECT fou_ordre INTO my_fou_ordre FROM DEPENSE_PAPIER WHERE dpp_id=my_dpp_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'attribution.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dmar_ht_saisie)>ABS(my_dmar_ttc_saisie) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dmar_tva_saisie:=Liquider_Outils.get_tva(my_dmar_ht_saisie, my_dmar_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dmar_ht_saisie,my_dmar_ttc_saisie);

            -- Pour les O.R.
            IF my_dmar_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE
               WHERE att_ordre=my_att_ordre
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                  OR dep_id_reversement=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette attribution n''est pas autorisee pour cette depense (att_ordre='||my_att_ordre||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dmar_montant_budgetaire) THEN
                my_dmar_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dmar_montant_budgetaire),0) INTO sum_dmar_montant_budgetaire
              FROM DEPENSE_CTRL_MARCHE WHERE att_ordre=my_att_ordre
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dmar_montant_budgetaire+my_dmar_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette attribution (att_ordre='||my_att_ordre||')');
            END IF;

            -- insertion dans la base.
            SELECT depense_ctrl_marche_seq.NEXTVAL INTO my_dmar_id FROM dual;

            INSERT INTO DEPENSE_CTRL_MARCHE VALUES (my_dmar_id,
                   a_exe_ordre, a_dep_id, my_att_ordre, my_dmar_montant_budgetaire,
                   my_dmar_ht_saisie, my_dmar_tva_saisie, my_dmar_ttc_saisie);

               -- procedure de verification
            Apres_Liquide.marche(my_dmar_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_dmar_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_reverse_ctrl_planco (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_dep_id          DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_dpco_id                   DEPENSE_CTRL_PLANCO.dpco_id%TYPE;
       my_pco_num                     DEPENSE_CTRL_PLANCO.pco_num%TYPE;
       my_dpco_montant_budgetaire  DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       sum_dpco_montant_budgetaire DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_dpco_ht_saisie             DEPENSE_CTRL_PLANCO.dpco_ht_saisie%TYPE;
       my_dpco_tva_saisie           DEPENSE_CTRL_PLANCO.dpco_tva_saisie%TYPE;
       my_dpco_ttc_saisie           DEPENSE_CTRL_PLANCO.dpco_ttc_saisie%TYPE;
       my_dep_montant_budgetaire   DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
       my_dep_id_reversement       DEPENSE_BUDGET.dep_id_reversement%TYPE;
       my_dep_tap_id               DEPENSE_BUDGET.tap_id%TYPE;
       my_exe_ordre                      DEPENSE_BUDGET.exe_ordre%TYPE;
       my_utl_ordre                   DEPENSE_BUDGET.utl_ordre%TYPE;
          my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
       my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
       my_tbo_ordre                   DEPENSE_CTRL_PLANCO.tbo_ordre%TYPE;
       my_ecd_ordre                   DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE;
       my_inventaires    VARCHAR2(30000);
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||')');
        END IF;

        SELECT d.dep_montant_budgetaire, d.tap_id, e.org_id, e.tcd_ordre, d.utl_ordre
               INTO my_dep_montant_budgetaire, my_dep_tap_id, my_org_id, my_tcd_ordre,my_utl_ordre
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE e.eng_id=d.eng_id AND d.dep_id=a_dep_id;

        IF my_exe_ordre<>a_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent.');
        END IF;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_dpco_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere l'ecriture.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));


            -- on recupere la chaine des inventaires.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_inventaires FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
             IF ABS(my_dpco_ht_saisie)>ABS(my_dpco_ttc_saisie) THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

            my_dpco_tva_saisie:=Liquider_Outils.get_tva(my_dpco_ht_saisie, my_dpco_ttc_saisie);

            -- on calcule le montant budgetaire.
              my_dpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_dep_tap_id,my_org_id,
                   my_dpco_ht_saisie,my_dpco_ttc_saisie);

            -- Pour les O.R.
            IF my_dpco_montant_budgetaire>0 THEN
                        RAISE_APPLICATION_ERROR(-20001, 'c''est un ORV le montant doit etre negatif.');
            END IF;

            -- on verifie que la depense_budget a aussi un montant negatif.
            IF my_dep_montant_budgetaire>0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'depense_budget correspondant n''est pas un OR.');
            END IF;

            -- verifier que ce qu'on enleve ne fait pas passer en negatif la montant de la depense de depart
            --     moins la somme des OR  et que cz existe bien au depart.
            -- et que l'engagement correspondant est le meme engage_ctrl_.
            SELECT dep_id_reversement INTO my_dep_id_reversement FROM DEPENSE_BUDGET WHERE dep_id=a_dep_id;

            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO
               WHERE pco_num=my_pco_num
                 AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement);

            IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'Pour cet OR et cette imputation il n''existe pas d''equivalent pour cette depense (pco_num='||my_pco_num||')');
            END IF;

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              ABS(my_dep_montant_budgetaire)<=ABS(my_somme+my_dpco_montant_budgetaire) THEN
                my_dpco_montant_budgetaire:=my_dep_montant_budgetaire - my_somme;
            END IF;

            SELECT NVL(SUM(dpco_montant_budgetaire),0) INTO sum_dpco_montant_budgetaire
              FROM DEPENSE_CTRL_PLANCO WHERE pco_num=my_pco_num
               AND dep_id IN (SELECT dep_id FROM DEPENSE_BUDGET WHERE dep_id=my_dep_id_reversement
                OR dep_id_reversement=my_dep_id_reversement);
                
            select count(*) into my_nb from depense_ctrl_planco 
              where dep_id=my_dep_id_reversement
                and pco_num in (select pco_num from v_plan_comptable_no_verif_orv);
              
            IF my_nb=0 and (sum_dpco_montant_budgetaire+my_dpco_montant_budgetaire<0) THEN
                  RAISE_APPLICATION_ERROR(-20001, 'On ne peut pas depasser le montant de depart sur cette liquidation pour cette imputation (pco_num='||my_pco_num||')');
            END IF;


            -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
            SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

            IF my_nb>0 THEN
               RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une depense ');
            END IF;


            -- insertion dans la base.
            SELECT depense_ctrl_planco_seq.NEXTVAL INTO my_dpco_id FROM dual;

            INSERT INTO DEPENSE_CTRL_PLANCO VALUES (my_dpco_id,
                   a_exe_ordre, a_dep_id, my_pco_num, NULL, my_dpco_montant_budgetaire,
                   my_dpco_ht_saisie, my_dpco_tva_saisie, my_dpco_ttc_saisie, 1, NULL);
            my_tbo_ordre:=Get_Tbo_Ordre(my_dpco_id);
            UPDATE DEPENSE_CTRL_PLANCO SET tbo_ordre=my_tbo_ordre WHERE dpco_id=my_dpco_id;

                  -- procedure de verification
            Apres_Liquide.planco(my_dpco_id, my_inventaires);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_dpco_montant_budgetaire;
		END LOOP;
   END;

END;
/



create procedure grhum.inst_patch_jefy_depense_2041 is
begin
	-- basculer la numerotation des dossiers de liquidation vers la numerotation des bordereaux de mandat
	update maracuja.type_bordereau set tnu_ordre=14 where tbo_ordre=300;	

    insert into jefy_depense.db_version values (2041,'2041',sysdate,sysdate,null);           
end;



 









