
Script de modification du user JEFY_DEPENSE
-------------------------------------------


ATTENTION ... il faut suivre les �tapes d�crites ci dessous dans l'ordre  et en respectant les consignes donn�es.

Si vous rencontrez un quelconque probl�me ou si vous avez des questions sur ces scripts ...
ne t�l�phonez pas, mais privil�giez plutot la solution email (carambole@univ-lr.fr).


Etape 1 : script '1.maj_base_jefy_depense.sql'
-----------------------------------------------------

   - remplacer le <mot de passe> par le mot de passe ad�quats des users concern�s (jefy_depense)
   - �x�cuter le script � partir du user jefy_depense

Etape 2 : v�rifications
-----------------------------------------------------

   - v�rifier que les vues, les fonctions, les procedures et les packages oracle sont bien compil�s
