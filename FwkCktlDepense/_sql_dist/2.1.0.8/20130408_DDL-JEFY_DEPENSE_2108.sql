SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.8
-- Date de publication : 08/04/2013
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Controle de depassement des marches : ajout des restes à engagés sur l'execice N-1 (tant qu'il n'est pas cloturé) dans les montants controlés.
-- DT4424 - Correction du problème d'arrondi lié à la répartition par code analytique
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2107';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/


CREATE OR REPLACE FUNCTION jefy_depense.IS_CODE_ANALYTIQUE_OBLIGATOIRE (p_organigrammeId jefy_admin.organ.org_id%TYPE) RETURN BOOLEAN
IS
	canalObligatoirePourOrg       	jefy_admin.organ.org_canal_obligatoire%TYPE;
	typeEtatOui											jefy_admin.type_etat.tyet_id%TYPE;
	typeEtatDepenses								jefy_admin.type_etat.tyet_id%TYPE;
	isObligatoire										boolean;

	BEGIN

		select org_canal_obligatoire into canalObligatoirePourOrg from jefy_admin.organ where org_id = p_organigrammeId;
		select tyet_id into typeEtatOui from jefy_admin.type_etat where tyet_libelle = 'OUI';
		select tyet_id into typeEtatDepenses from jefy_admin.type_etat where tyet_libelle = 'DEPENSES';

		if canalObligatoirePourOrg is not null and (canalObligatoirePourOrg = typeEtatOui or canalObligatoirePourOrg = typeEtatDepenses) then
		  isObligatoire := TRUE;
		else
      isObligatoire := FALSE;
		end if;

		RETURN isObligatoire;
	END;
	/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Engager
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------


   PROCEDURE ins_engage (
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
        if a_eng_ttc_saisie>=0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   Verifier.verifier_engage_exercice(a_exe_ordre, a_utl_ordre, a_org_id);
        --else
		--   Verifier.verifier_extourne_exercice(a_exe_ordre-1, a_utl_ordre, a_org_id);
        end if;

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_engage_budget(a_eng_id,a_exe_ordre,a_eng_numero,a_fou_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_eng_libelle, a_eng_ht_saisie,a_eng_ttc_saisie,a_tyap_id,a_utl_ordre);

        if a_eng_ttc_saisie>=0 then
		   ins_engage_ctrl_action(a_exe_ordre,a_eng_id,a_chaine_action);
		   ins_engage_ctrl_analytique(a_exe_ordre,a_eng_id,a_chaine_analytique);
		   ins_engage_ctrl_convention(a_exe_ordre,a_eng_id,a_chaine_convention);
		   ins_engage_ctrl_hors_marche(a_exe_ordre,a_eng_id,a_chaine_hors_marche);
		   ins_engage_ctrl_marche(a_exe_ordre,a_eng_id,a_chaine_marche);
		   ins_engage_ctrl_planco(a_exe_ordre,a_eng_id,a_chaine_planco);
        end if;

		Verifier.verifier_engage_coherence(a_eng_id);
		Apres_Engage.engage(a_eng_id);
   END;

PROCEDURE ins_engage_extourne_poche (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type
   ) is
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   begin
		IF a_eng_ttc_saisie<0 or a_eng_ht_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : pour une consommation sur poche d''extourne les montants doivent etre positifs');
		END IF;

        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

--        verifier.verifier_organ(a_org_id, a_tcd_ordre);
	    verifier.verifier_fournisseur(a_fou_ordre);

		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);
   end;

   PROCEDURE del_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_montant_reste      ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est deja annule (eng_id:'||a_eng_id||')');
	    END IF;

   		SELECT exe_ordre, eng_montant_budgetaire, eng_montant_budgetaire_reste, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_montant_reste, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

   		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on traite le cas d'un engagement dont la somme des liquidations.
		-- est egale a la somme des ordres de reversement ... ce qui fait que l'engagement est reengage.
		--  donc on ne le supprime pas mais on le solde.
		SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb>0 AND my_montant_budgetaire=my_montant_reste THEN
		   solder_engage(a_eng_id, a_utl_ordre);
		ELSE
           Verifier.verifier_util_engage(a_eng_id);

		   log_engage(a_eng_id,a_utl_ordre);

	       DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        END IF;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.del_engage(a_eng_id);
   END;

   PROCEDURE solder_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_ht_saisie      engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie     engage_budget.eng_ttc_saisie%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste, eng_ht_saisie, eng_ttc_saisie
          INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste, my_eng_ht_saisie, my_eng_ttc_saisie
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if my_eng_ttc_saisie>=0 and my_eng_montant_bud_reste=0 then return; end if;

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		solder_engage_sansdroit(a_eng_id, a_utl_ordre);
   END;

   PROCEDURE solder_engage_sansdroit (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_ht_saisie      engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie     engage_budget.eng_ttc_saisie%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste, eng_ht_saisie, eng_ttc_saisie
          INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste, my_eng_ht_saisie, my_eng_ttc_saisie
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if my_eng_ttc_saisie>=0 and my_eng_montant_bud_reste=0 then return; end if;

		-- on solde l'engagement et ses controles.
        if my_eng_ttc_saisie>=0 then
		   UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        else
		   UPDATE ENGAGE_BUDGET SET eng_ht_saisie=0, eng_tva_saisie=0, eng_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ACTION SET eact_ht_saisie=0, eact_tva_saisie=0, eact_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_ht_saisie=0, eana_tva_saisie=0, eana_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_CONVENTION SET econ_ht_saisie=0, econ_tva_saisie=0, econ_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_ht_saisie=0, ehom_tva_saisie=0, ehom_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_MARCHE SET emar_ht_saisie=0, emar_tva_saisie=0, emar_ttc_saisie=0 WHERE eng_id=a_eng_id;
		   UPDATE ENGAGE_CTRL_PLANCO SET epco_ht_saisie=0, epco_tva_saisie=0, epco_ttc_saisie=0 WHERE eng_id=a_eng_id;
        end if;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE upd_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
	  my_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	  my_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_tap_id			    ENGAGE_BUDGET.tap_id%TYPE;
      my_tyap_id            ENGAGE_BUDGET.tyap_id%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_tva_saisie     ENGAGE_BUDGET.eng_tva_saisie%TYPE;
      my_budgetaire         ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_nb_decimales       NUMBER;

      CURSOR listedepense(a_eng_id DEPENSE_BUDGET.eng_id%TYPE) IS SELECT * FROM  DEPENSE_BUDGET WHERE eng_id=a_eng_id and dep_montant_budgetaire>0;
      depense DEPENSE_BUDGET%ROWTYPE;
	  my_dep_ht_saisie		DEPENSE_BUDGET.dep_ht_saisie%TYPE;
	  my_dep_ttc_saisie		DEPENSE_BUDGET.dep_ttc_saisie%TYPE;

   BEGIN
   		SELECT exe_ordre, tap_id, org_id, tcd_ordre, tyap_id INTO my_exe_ordre, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_ttc_saisie),0) into my_dep_ht_saisie, my_dep_ttc_saisie
            from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
        my_budgetaire:=0;
        if my_dep_ht_saisie<>0 or my_dep_ttc_saisie<>0 then
           if my_dep_ht_saisie=my_dep_ttc_saisie then
               my_budgetaire:=my_dep_ttc_saisie;
           else
               OPEN listedepense(a_eng_id);
 	           LOOP
		           FETCH  listedepense INTO depense;
		           EXIT WHEN listedepense%NOTFOUND;

				    my_budgetaire:=my_budgetaire+Budget.calculer_budgetaire(my_exe_ordre, my_tap_id, my_org_id, depense.dep_ht_saisie, depense.dep_ttc_saisie);
                 END LOOP;
			     CLOSE listedepense;
           end if;
        end if;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);
        my_budgetaire:=round(my_budgetaire, my_nb_decimales);

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		log_engage(a_eng_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
        /*if my_tyap_id=get_type_application_EXTOURNE then
            IF my_eng_ht_saisie>0 OR my_eng_ttc_saisie>0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		    END IF;
        else
            IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;
        end if;*/

		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;
        if my_eng_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
           my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id, my_eng_ht_saisie,my_eng_ttc_saisie);
        end if;

	    IF my_budgetaire> my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja liquide ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- on modifie les montants.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=my_montant_budgetaire,
		   eng_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   eng_ht_saisie=my_eng_ht_saisie, eng_tva_saisie=my_eng_tva_saisie,
		   eng_ttc_saisie=my_eng_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE eng_id=a_eng_id;

		-- on supprime les anciens.
		DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

		-- on insere les nouveaux.
		ins_engage_ctrl_action(my_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(my_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(my_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(my_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(my_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(my_exe_ordre,a_eng_id,a_chaine_planco);

		-- on met a jour les restes engages des controleurs suivant les depenses.
		Corriger.upd_engage_reste(a_eng_id);

		-- on verifie.
		Verifier.verifier_engage_coherence(a_eng_id);
        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.engage(a_eng_id);
   END;

   --
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


   PROCEDURE log_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_zeng_id			Z_ENGAGE_BUDGET.zeng_id%TYPE;
   BEGIN
		SELECT z_engage_budget_seq.NEXTVAL INTO my_zeng_id FROM dual;

		-- engage_budget.
		INSERT INTO Z_ENGAGE_BUDGET SELECT my_zeng_id, SYSDATE, a_utl_ordre, e.*
		  FROM ENGAGE_BUDGET e WHERE eng_id=a_eng_id;

		-- engage_ctrl_action.
		INSERT INTO Z_ENGAGE_CTRL_ACTION SELECT z_engage_ctrl_action_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ACTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_analytique.
		INSERT INTO Z_ENGAGE_CTRL_ANALYTIQUE SELECT z_engage_ctrl_analytique_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ANALYTIQUE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_convention.
		INSERT INTO Z_ENGAGE_CTRL_CONVENTION SELECT z_engage_ctrl_convention_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_CONVENTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_hors_marche.
		INSERT INTO Z_ENGAGE_CTRL_HORS_MARCHE SELECT z_engage_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_HORS_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_marche.
		INSERT INTO Z_ENGAGE_CTRL_MARCHE SELECT z_engage_ctrl_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_planco.
		INSERT INTO Z_ENGAGE_CTRL_PLANCO SELECT z_engage_ctrl_planco_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_PLANCO e WHERE eng_id=a_eng_id;
   END;

   PROCEDURE ins_engage_budget(
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

   		-- lucrativite ??.
		--		UPDATE COMMANDE SET cde_lucrativite=(select org_lucrativite from organ where org_ordre=orgordre) WHERE cde_ordre=cdeordre;

        verifier.verifier_organ(a_org_id, a_tcd_ordre);
        if my_eng_ttc_saisie>0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
        end if;
	    Verifier.verifier_fournisseur(a_fou_ordre);

        /*if a_tyap_id=get_type_application_EXTOURNE then
		   IF my_eng_ht_saisie>0 OR my_eng_ttc_saisie>0 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		   END IF;
        else
		   IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		   END IF;
        end if;*/

		IF abs(my_eng_ttc_saisie)-abs(my_eng_ht_saisie)<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		-- calcul du montant budgetaire.
        my_montant_budgetaire:=0;
        if my_eng_ttc_saisie>0 /*a_tyap_id<>get_type_application_EXTOURNE*/ then
		   my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id, my_eng_ht_saisie,my_eng_ttc_saisie);
        end if;

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);

        Budget.maj_budget(a_exe_ordre, a_org_id, a_tcd_ordre);
	    Apres_Engage.Budget(a_eng_id);
   END;

   PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eact_id	               ENGAGE_CTRL_ACTION.eact_id%TYPE;
       my_tyac_id	  	   		   ENGAGE_CTRL_ACTION.tyac_id%TYPE;
       my_eact_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_eact_ht_saisie	  	   ENGAGE_CTRL_ACTION.eact_ht_saisie%TYPE;
       my_eact_tva_saisie		   ENGAGE_CTRL_ACTION.eact_tva_saisie%TYPE;
       my_eact_ttc_saisie		   ENGAGE_CTRL_ACTION.eact_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
	   my_utl_ordre                ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eact_ht_saisie:=round(my_eact_ht_saisie, my_nb_decimales);
            my_eact_ttc_saisie:=round(my_eact_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_tyac_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette action pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_eact_ht_saisie>0 OR my_eact_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_eact_ht_saisie<0 OR my_eact_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_eact_ttc_saisie)-abs(my_eact_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_eact_tva_saisie:=my_eact_ttc_saisie-my_eact_ht_saisie;

            my_eact_montant_budgetaire:=0;
            if my_eact_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_eact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_eact_ht_saisie,my_eact_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_eact_montant_budgetaire THEN
			    my_eact_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_action_seq.NEXTVAL INTO my_eact_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ACTION VALUES (my_eact_id, a_exe_ordre, a_eng_id, my_tyac_id, my_eact_montant_budgetaire,
                my_eact_montant_budgetaire, my_eact_ht_saisie, my_eact_tva_saisie, my_eact_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_action(a_eng_id);

	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
			Apres_Engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eact_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eana_id	               ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
       my_can_id	  	   		   ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_eana_montant_budgetaire  ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_eana_ht_saisie	  	   ENGAGE_CTRL_ANALYTIQUE.eana_ht_saisie%TYPE;
       my_eana_tva_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_tva_saisie%TYPE;
       my_eana_ttc_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
       org_canal_obligatoire       BOOLEAN;

   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		org_canal_obligatoire := IS_CODE_ANALYTIQUE_OBLIGATOIRE(my_org_id);

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eana_ht_saisie:=round(my_eana_ht_saisie, my_nb_decimales);
            my_eana_ttc_saisie:=round(my_eana_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_can_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code analytique pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_eana_ht_saisie>0 OR my_eana_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_eana_ht_saisie<0 OR my_eana_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_eana_ttc_saisie)-abs(my_eana_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_eana_tva_saisie := my_eana_ttc_saisie - my_eana_ht_saisie;

            my_eana_montant_budgetaire:=0;
            if my_eana_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_eana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_eana_ht_saisie,my_eana_ttc_saisie);
            end if;

			-- on teste si il n'y a pas assez de dispo ou si on a un problème d'arrondi sur le dernier element dans le cadre du code analytique obligatoire
      		IF (my_eng_montant_budgetaire <= my_somme + my_eana_montant_budgetaire) OR (SUBSTR(my_chaine,1,1) = '$' and org_canal_obligatoire = true) THEN
			    my_eana_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_analytique_seq.NEXTVAL INTO my_eana_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ANALYTIQUE VALUES (my_eana_id, a_exe_ordre, a_eng_id, my_can_id, my_eana_montant_budgetaire,
                   my_eana_montant_budgetaire, my_eana_ht_saisie, my_eana_tva_saisie, my_eana_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_analytique(a_eng_id);

			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_econ_id	               ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
       my_conv_ordre 	   		   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_econ_montant_budgetaire  ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_econ_ht_saisie	  	   ENGAGE_CTRL_CONVENTION.econ_ht_saisie%TYPE;
       my_econ_tva_saisie		   ENGAGE_CTRL_CONVENTION.econ_tva_saisie%TYPE;
       my_econ_ttc_saisie		   ENGAGE_CTRL_CONVENTION.econ_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_econ_ht_saisie:=round(my_econ_ht_saisie, my_nb_decimales);
            my_econ_ttc_saisie:=round(my_econ_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_conv_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette convention pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_econ_ht_saisie>0 OR my_econ_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_econ_ht_saisie<0 OR my_econ_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_econ_ttc_saisie)-abs(my_econ_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_econ_tva_saisie := my_econ_ttc_saisie - my_econ_ht_saisie;

            my_econ_montant_budgetaire:=0;
            if my_econ_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
               my_econ_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_econ_ht_saisie,my_econ_ttc_saisie);
            end if;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_econ_montant_budgetaire THEN
			    my_econ_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_convention_seq.NEXTVAL INTO my_econ_id FROM dual;

			INSERT INTO ENGAGE_CTRL_CONVENTION VALUES (my_econ_id,
			       a_exe_ordre, a_eng_id, my_conv_ordre, my_econ_montant_budgetaire, my_econ_montant_budgetaire,
				   my_econ_ht_saisie, my_econ_tva_saisie, my_econ_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_convention(a_eng_id);

			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
	    	Apres_Engage.convention(my_econ_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_econ_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ehom_id	               ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
       my_typa_id	  	   		   ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre	  	   		   ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_ehom_montant_budgetaire  ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_ehom_ht_saisie	  	   ENGAGE_CTRL_HORS_MARCHE.ehom_ht_saisie%TYPE;
       my_ehom_tva_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_tva_saisie%TYPE;
       my_ehom_ttc_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code de nomenclature.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ehom_ht_saisie:=round(my_ehom_ht_saisie, my_nb_decimales);
            my_ehom_ttc_saisie:=round(my_ehom_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
			     WHERE eng_id=a_eng_id AND ce_ordre=my_ce_ordre AND typa_id=my_typa_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code nomenclature pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_ehom_ht_saisie>0 OR my_ehom_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_ehom_ht_saisie<0 OR my_ehom_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_ehom_ttc_saisie)-abs(my_ehom_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_ehom_tva_saisie := my_ehom_ttc_saisie - my_ehom_ht_saisie;

            my_ehom_montant_budgetaire:=0;
            if my_ehom_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_ehom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ehom_ht_saisie,my_ehom_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_ehom_montant_budgetaire THEN
			    my_ehom_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_hors_marche_seq.NEXTVAL INTO my_ehom_id FROM dual;

			INSERT INTO ENGAGE_CTRL_HORS_MARCHE VALUES (my_ehom_id,
			       a_exe_ordre, a_eng_id, my_typa_id, my_ce_ordre, my_ehom_montant_budgetaire, my_ehom_montant_budgetaire,
				   my_ehom_ht_saisie, my_ehom_ht_saisie, my_ehom_tva_saisie, my_ehom_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_hors_marche(a_eng_id);

			Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
	    	Apres_Engage.hors_marche(my_ehom_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ehom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_emar_id	               ENGAGE_CTRL_MARCHE.emar_id%TYPE;
       my_att_ordre	  	   		   ENGAGE_CTRL_MARCHE.att_ordre%TYPE;
       my_emar_montant_budgetaire  ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_emar_ht_saisie	  	   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
       my_emar_tva_saisie		   ENGAGE_CTRL_MARCHE.emar_tva_saisie%TYPE;
       my_emar_ttc_saisie		   ENGAGE_CTRL_MARCHE.emar_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
   	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
   	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'attribution.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_emar_ht_saisie:=round(my_emar_ht_saisie, my_nb_decimales);
            my_emar_ttc_saisie:=round(my_emar_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_att_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette attribution pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_emar_ht_saisie>0 OR my_emar_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_emar_ht_saisie<0 OR my_emar_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_emar_ttc_saisie)-abs(my_emar_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_emar_tva_saisie := my_emar_ttc_saisie - my_emar_ht_saisie;

            my_emar_montant_budgetaire:=0;
            if my_emar_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			    my_emar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_emar_ht_saisie,my_emar_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_emar_montant_budgetaire THEN
			    my_emar_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour un engagement');
			END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_marche_seq.NEXTVAL INTO my_emar_id FROM dual;

			INSERT INTO ENGAGE_CTRL_MARCHE VALUES (my_emar_id,
			       a_exe_ordre, a_eng_id, my_att_ordre, my_emar_montant_budgetaire, my_emar_montant_budgetaire,
				   my_emar_ht_saisie,my_emar_ht_saisie, my_emar_tva_saisie, my_emar_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_marche(a_eng_id);

			Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
	    	Apres_Engage.marche(my_emar_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_emar_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_epco_id	               ENGAGE_CTRL_PLANCO.epco_id%TYPE;
       my_pco_num	  	   		   ENGAGE_CTRL_PLANCO.pco_num%TYPE;
	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_epco_montant_budgetaire  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_epco_ht_saisie	  	   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
       my_epco_tva_saisie		   ENGAGE_CTRL_PLANCO.epco_tva_saisie%TYPE;
       my_epco_ttc_saisie		   ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
	   my_tyap_id				   ENGAGE_BUDGET.tyap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre				   ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre, tyap_id
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre, my_tyap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_epco_ht_saisie:=round(my_epco_ht_saisie, my_nb_decimales);
            my_epco_ttc_saisie:=round(my_epco_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_pco_num;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette imputation pour cet engagement');
			END IF;

            /*if my_tyap_id=get_type_application_EXTOURNE then
			   IF my_epco_ht_saisie>0 OR my_epco_ttc_saisie>0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement d''extourne doit se faire pour un montant negatif');
		       END IF;
            else
	   		-- verification que les montants sont bien positifs !!!.
			   IF my_epco_ht_saisie<0 OR my_epco_ttc_saisie<0 THEN
		          RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		       END IF;
            end if;*/

			-- on calcule la tva et le montant budgetaire.
			IF abs(my_epco_ttc_saisie)-abs(my_epco_ht_saisie)<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

			my_epco_tva_saisie := my_epco_ttc_saisie - my_epco_ht_saisie;

            my_epco_montant_budgetaire:=0;
            if my_epco_ttc_saisie>0 /*my_tyap_id<>get_type_application_EXTOURNE*/ then
   			   my_epco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id, my_epco_ht_saisie,my_epco_ttc_saisie);
            end if;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR my_eng_montant_budgetaire<=my_somme+my_epco_montant_budgetaire THEN
			    my_epco_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_planco_seq.NEXTVAL INTO my_epco_id FROM dual;

			INSERT INTO ENGAGE_CTRL_PLANCO VALUES (my_epco_id,
			       a_exe_ordre, a_eng_id, my_pco_num, my_epco_montant_budgetaire, my_epco_montant_budgetaire,
				   my_epco_ht_saisie, my_epco_tva_saisie, my_epco_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_planco(a_eng_id);

			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_epco_montant_budgetaire;
		END LOOP;
   END;

END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.liquider
is
--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------
   procedure service_fait (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre depense_papier.utl_ordre%type, a_dpp_date_service_fait depense_papier.dpp_date_service_fait%type)
   is
      my_nb               integer;
      my_depense_papier   depense_papier%rowtype;
      my_pers_id          jefy_admin.utilisateur.pers_id%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''existe pas (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select *
      into   my_depense_papier
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_depense_papier.dpp_date_service_fait is not null then
         raise_application_error (-20001, 'La facture a deja une date de service fait (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select pers_id
      into   my_pers_id
      from   jefy_admin.utilisateur
      where  utl_ordre = a_utl_ordre;

      update depense_papier
         set dpp_date_service_fait = a_dpp_date_service_fait,
             dpp_sf_pers_id = my_pers_id,
             dpp_sf_date = sysdate
       where dpp_id = a_dpp_id;
   end;

   procedure service_fait_et_liquide (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre depense_papier.utl_ordre%type, a_dpp_date_service_fait depense_papier.dpp_date_service_fait%type)
   is
      my_nb        integer;
      my_pdep_id   pdepense_budget.pdep_id%type;

      cursor liste
      is
         select pdep_id
         from   pdepense_budget
         where  dpp_id = a_dpp_id;
   begin
      service_fait (a_dpp_id, a_utl_ordre, a_dpp_date_service_fait);

      select count (*)
      into   my_nb
      from   pdepense_budget
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''a pas de pre-liquidation (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      open liste ();

      loop
         fetch liste
         into  my_pdep_id;

         exit when liste%notfound;
         pre_liquider.liquider_pdepense_budget (my_pdep_id, a_utl_ordre);
      end loop;

      close liste;
   end;

   procedure ins_depense_papier (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type
   )
   is
   begin
      ins_depense_papier_avec_im (a_dpp_id,
                                  a_exe_ordre,
                                  a_dpp_numero_facture,
                                  a_dpp_ht_initial,
                                  a_dpp_ttc_initial,
                                  a_fou_ordre,
                                  a_rib_ordre,
                                  a_mod_ordre,
                                  a_dpp_date_facture,
                                  a_dpp_date_saisie,
                                  a_dpp_date_reception,
                                  a_dpp_date_service_fait,
                                  a_dpp_nb_piece,
                                  a_utl_ordre,
                                  a_dpp_id_reversement,
                                  null,
                                  null,
                                  null
                                 );
   end;

   procedure ins_depense_papier_avec_im (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type,
      a_dpp_im_taux                      depense_papier.dpp_im_taux%type,
      a_dpp_im_dgp                       depense_papier.dpp_im_dgp%type,
      imtt_id                            depense_papier.imtt_id%type
   )
   is
   begin
      ins_depense_papier_avec_im_sf (a_dpp_id,
                                     a_exe_ordre,
                                     a_dpp_numero_facture,
                                     a_dpp_ht_initial,
                                     a_dpp_ttc_initial,
                                     a_fou_ordre,
                                     a_rib_ordre,
                                     a_mod_ordre,
                                     a_dpp_date_facture,
                                     a_dpp_date_saisie,
                                     a_dpp_date_reception,
                                     a_dpp_date_service_fait,
                                     a_dpp_nb_piece,
                                     a_utl_ordre,
                                     a_dpp_id_reversement,
                                     a_dpp_im_taux,
                                     a_dpp_im_dgp,
                                     imtt_id,
                                     null,
                                     null,
                                     null
                                    );
   end;

   procedure ins_depense_papier_avec_im_sf (
      a_dpp_id                  in out   depense_papier.dpp_id%type,
      a_exe_ordre                        depense_papier.exe_ordre%type,
      a_dpp_numero_facture               depense_papier.dpp_numero_facture%type,
      a_dpp_ht_initial                   depense_papier.dpp_ht_initial%type,
      a_dpp_ttc_initial                  depense_papier.dpp_ttc_initial%type,
      a_fou_ordre                        depense_papier.fou_ordre%type,
      a_rib_ordre                        depense_papier.rib_ordre%type,
      a_mod_ordre                        depense_papier.mod_ordre%type,
      a_dpp_date_facture                 depense_papier.dpp_date_facture%type,
      a_dpp_date_saisie                  depense_papier.dpp_date_saisie%type,
      a_dpp_date_reception               depense_papier.dpp_date_reception%type,
      a_dpp_date_service_fait            depense_papier.dpp_date_service_fait%type,
      a_dpp_nb_piece                     depense_papier.dpp_nb_piece%type,
      a_utl_ordre                        depense_papier.utl_ordre%type,
      a_dpp_id_reversement               depense_papier.dpp_id_reversement%type,
      a_dpp_im_taux                      depense_papier.dpp_im_taux%type,
      a_dpp_im_dgp                       depense_papier.dpp_im_dgp%type,
      a_imtt_id                          depense_papier.imtt_id%type,
      a_dpp_sf_pers_id                   depense_papier.dpp_sf_pers_id%type,
      a_dpp_sf_date                      depense_papier.dpp_sf_date%type,
      a_ecd_ordre                        depense_papier.ecd_ordre%type
   )
   is
      my_dpp_ht_initial    depense_papier.dpp_ht_initial%type;
      my_dpp_tva_initial   depense_papier.dpp_tva_initial%type;
      my_dpp_ttc_initial   depense_papier.dpp_ttc_initial%type;
      my_dpp_sf_pers_id    depense_papier.dpp_sf_pers_id%type;
      my_dpp_sf_date       depense_papier.dpp_sf_date%type;
      my_nb_decimales      number;
      my_nb                integer;
   begin
      -- verifier qu'on a le droit de liquider sur cet exercice.
      --Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre);
      verifier.verifier_rib (a_fou_ordre, a_rib_ordre, a_mod_ordre, a_exe_ordre);
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dpp_ht_initial := round (a_dpp_ht_initial, my_nb_decimales);
      my_dpp_ttc_initial := round (a_dpp_ttc_initial, my_nb_decimales);

      -- si les montants sont negatifs ou si il y a un dpp_id_reversement (c'est un ORV) -> package reverser.
      if my_dpp_ht_initial < 0 or my_dpp_ttc_initial < 0 or a_dpp_id_reversement is not null then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- on verifie la coherence des montants.
      if abs (my_dpp_ht_initial) > abs (my_dpp_ttc_initial) then
         raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
      end if;

      -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
      my_dpp_tva_initial := liquider_outils.get_tva (my_dpp_ht_initial, my_dpp_ttc_initial);

      -- enregistrement dans la table.
      if a_dpp_id is null then
         select depense_papier_seq.nextval
         into   a_dpp_id
         from   dual;
      end if;

      my_dpp_sf_pers_id := a_dpp_sf_pers_id;
      my_dpp_sf_date := a_dpp_sf_date;

      if a_dpp_date_service_fait is not null then
         if a_dpp_sf_pers_id is null then
            select pers_id
            into   my_dpp_sf_pers_id
            from   v_utilisateur
            where  utl_ordre = a_utl_ordre;
         end if;

         if a_dpp_sf_date is null then
            select sysdate
            into   my_dpp_sf_date
            from   dual;
         end if;
      end if;

      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         insert into depense_papier
         values      (a_dpp_id,
                      a_exe_ordre,
                      a_dpp_numero_facture,
                      0,
                      0,
                      0,
                      a_fou_ordre,
                      a_rib_ordre,
                      a_mod_ordre,
                      a_dpp_date_facture,
                      a_dpp_date_saisie,
                      a_dpp_date_reception,
                      a_dpp_date_service_fait,
                      a_dpp_nb_piece,
                      a_utl_ordre,
                      a_dpp_id_reversement,
                      my_dpp_ht_initial,
                      my_dpp_tva_initial,
                      my_dpp_ttc_initial,
                      a_dpp_im_taux,
                      a_dpp_im_dgp,
                      a_imtt_id,
                      my_dpp_sf_pers_id,
                      my_dpp_sf_date,
                      a_ecd_ordre
                     );
      else
         update depense_papier
            set dpp_numero_facture = a_dpp_numero_facture,
                rib_ordre = a_rib_ordre,
                mod_ordre = a_mod_ordre,
                dpp_date_facture = a_dpp_date_facture,
                dpp_date_reception = a_dpp_date_reception,
                dpp_date_service_fait = a_dpp_date_service_fait,
                dpp_nb_piece = a_dpp_nb_piece,
                utl_ordre = a_utl_ordre,
                dpp_id_reversement = a_dpp_id_reversement,
                dpp_ht_initial = my_dpp_ht_initial,
                dpp_tva_initial = my_dpp_tva_initial,
                dpp_ttc_initial = my_dpp_ttc_initial,
                dpp_im_taux = a_dpp_im_taux,
                dpp_im_dgp = a_dpp_im_dgp,
                imtt_id = a_imtt_id,
                dpp_sf_pers_id = my_dpp_sf_pers_id,
                dpp_sf_date = my_dpp_sf_date,
                dpp_date_saisie = sysdate,
                ecd_ordre = a_ecd_ordre
          where dpp_id = a_dpp_id;
      end if;
   end;

   procedure ins_commande_dep_papier (a_cdp_id in out commande_dep_papier.cdp_id%type, a_comm_id commande_dep_papier.comm_id%type, a_dpp_id commande_dep_papier.dpp_id%type)
   is
      my_nb              integer;
      my_dpp_exe_ordre   depense_papier.exe_ordre%type;
      my_cde_exe_ordre   commande.exe_ordre%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture n''existe pas (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select count (*)
      into   my_nb
      from   commande
      where  comm_id = a_comm_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La commande n''existe pas (' || indication_erreur.commande (a_comm_id) || ')');
      end if;

      select count (*)
      into   my_nb
      from   commande_dep_papier
      where  dpp_id = a_dpp_id;

      if my_nb > 0 then
         raise_application_error (-20001, 'La facture est deja utilise pour une commande (' || indication_erreur.dep_papier (a_dpp_id) || ')');
      end if;

      select exe_ordre
      into   my_cde_exe_ordre
      from   commande
      where  comm_id = a_comm_id;

      if my_dpp_exe_ordre <> my_cde_exe_ordre then
         raise_application_error (-20001, 'La facture et la commande ne sont pas sur le meme exercice (' || indication_erreur.dep_papier (a_dpp_id) || ', ' || indication_erreur.commande (a_comm_id) || ')');
      end if;

      -- si pas de probleme on insere.
      if a_cdp_id is null then
         select commande_dep_papier_seq.nextval
         into   a_cdp_id
         from   dual;
      end if;

      insert into commande_dep_papier
      values      (a_cdp_id,
                   a_comm_id,
                   a_dpp_id
                  );
   end;

   procedure ins_depense (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_eng_id                        depense_budget.eng_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type,
      a_chaine_action                 varchar2,
      a_chaine_analytique             varchar2,
      a_chaine_convention             varchar2,
      a_chaine_hors_marche            varchar2,
      a_chaine_marche                 varchar2,
      a_chaine_planco                 varchar2
   )
   is
      my_org_id           engage_budget.org_id%type;
      my_nb_decimales     number;
      my_dep_ht_saisie    depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie   depense_budget.dep_ttc_saisie%type;
   begin
      if a_dep_ttc_saisie <> 0 then
         my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
         my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
         my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

         select org_id
         into   my_org_id
         from   engage_budget
         where  eng_id = a_eng_id;

         -- verifier qu'on a le droit de liquider sur cet exercice.
         verifier.verifier_depense_exercice (a_exe_ordre, a_utl_ordre, my_org_id);
         -- lancement des differentes procedures d'insertion des tables de depense.
         ins_depense_budget (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, a_dep_id_reversement);

         update depense_papier
            set dpp_ht_saisie = dpp_ht_saisie + my_dep_ht_saisie,
                dpp_tva_saisie = dpp_tva_saisie + my_dep_ttc_saisie - my_dep_ht_saisie,
                dpp_ttc_saisie = dpp_ttc_saisie + my_dep_ttc_saisie
          where dpp_id = a_dpp_id;

         -- on le passe en premier car utilis? par les autres pour les upd_engage_reste_.
         ins_depense_ctrl_planco (a_exe_ordre, a_dep_id, a_chaine_planco);
         ins_depense_ctrl_action (a_exe_ordre, a_dep_id, a_chaine_action);
         ins_depense_ctrl_analytique (a_exe_ordre, a_dep_id, a_chaine_analytique);
         ins_depense_ctrl_convention (a_exe_ordre, a_dep_id, a_chaine_convention);
         ins_depense_ctrl_hors_marche (a_exe_ordre, a_dep_id, a_chaine_hors_marche);
         ins_depense_ctrl_marche (a_exe_ordre, a_dep_id, a_chaine_marche);
         --Corriger.upd_engage_reste(a_eng_id);

         -- on verifie la coherence des montants entre les differents depense_.
         verifier.verifier_depense_coherence (a_dep_id);
         verifier.verifier_depense_pap_coherence (a_dpp_id);
         -- on verifie si la coherence des montants budgetaires restant est  conservee.
         verifier.verifier_engage_coherence (a_eng_id);
      end if;
   end;

   procedure ins_depense_extourne (
      a_dep_id              in out   depense_budget.dep_id%type,
      a_exe_ordre                    depense_budget.exe_ordre%type,
      a_dpp_id                       depense_budget.dpp_id%type,
      a_eng_id              in out   depense_budget.eng_id%type,
      a_dep_ht_saisie                depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie               depense_budget.dep_ttc_saisie%type,
      a_fou_ordre                    engage_budget.fou_ordre%type,
      a_org_id                       engage_budget.org_id%type,
      a_tcd_ordre                    engage_budget.tcd_ordre%type,
      a_tap_id                       depense_budget.tap_id%type,
      a_eng_libelle                  engage_budget.eng_libelle%type,
      a_tyap_id                      engage_budget.tyap_id%type,
      a_utl_ordre                    depense_budget.utl_ordre%type,
      --a_dep_id_reversement   DEPENSE_BUDGET.dep_id_reversement%TYPE,
      --a_chaine_action        VARCHAR2,
      a_chaine_analytique            varchar2,
      a_chaine_convention            varchar2,
      --a_chaine_hors_marche   VARCHAR2,
      a_chaine_marche                varchar2,
      a_chaine_planco                varchar2
   )
   is
      my_eng_numero        engage_budget.eng_numero%type;
      my_org_id            engage_budget.org_id%type;
      my_nb_decimales      number;
      my_dep_ht_saisie     depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie    depense_budget.dep_ttc_saisie%type;
      my_eng_ht_saisie     engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie    engage_budget.eng_ttc_saisie%type;
      my_dep_id_initiale   depense_budget.dep_id%type;
      my_eld_id            extourne_liq_def.eld_id%type;
      my_el_id             extourne_liq.el_id%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
      my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

      -- si pas d''engagement prealable c'est qu'on consomme sur poche, donc on en cree un
      if a_eng_id is null then
         if a_fou_ordre is null or a_org_id is null or a_tcd_ordre is null or a_tap_id is null then
            raise_application_error (-20001, 'il faut un fournisseur, une ligne budgetaire, un type de credit et un taux de prorata');
         end if;

         -- quid exercice du tcd_ordre et dates ouverture org_id ????
         -- autre probleme, dans la procedure de creation d'engagement si montant>0 alors engagement normal donc creer procedure engagement_extourne_poche
         engager.ins_engage_extourne_poche (a_eng_id, a_exe_ordre, my_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, a_dep_ht_saisie, a_dep_ttc_saisie, a_tyap_id, a_utl_ordre);
      end if;

      select eng_ht_saisie,
             eng_ttc_saisie,
             org_id
      into   my_eng_ht_saisie,
             my_eng_ttc_saisie,
             my_org_id
      from   engage_budget
      where  eng_id = a_eng_id;

    -- on ne controle pas les montants HT et TTC de la liquidation definitive par rapport à ceux de la liquidation d'extourne
    -- car dans le cas d'un changement de taux de tva ou de taux de prorata
    -- entre la liquidation initiale et la liquidation definitive
    -- les HT et TTC sont recalculés a partir du montant budgetaire
    -- et peuvent depasser ceux de la liquidation d'extourne
--      if my_eng_ht_saisie < 0 or my_eng_ttc_saisie < 0 then
--         if abs (my_eng_ht_saisie) < a_dep_ht_saisie then
--            raise_application_error (-20001, 'Le montant HT de la depense (' || to_char (a_dep_ht_saisie) || ') depasse celui du credit d''extourne associe (' || to_char (abs (my_eng_ht_saisie)) || ')');
--         end if;

--         if abs (my_eng_ttc_saisie) < a_dep_ttc_saisie then
--            raise_application_error (-20001, 'Le montant TTC de la depense (' || to_char (a_dep_ttc_saisie) || ') depasse celui du credit d''extourne associe (' || to_char (abs (my_eng_ttc_saisie)) || ')');
--         end if;
--      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      -- Verifier.verifier_depense_exercice(a_exe_ordre, a_utl_ordre, my_org_id);

      -- c'est une liquidation sur credit extourne fleche
      if my_eng_ttc_saisie < 0 then
         -- on memorise la reference a la liquidation d''extourne avant d'inserer la nouvelle depensebudget
         select max (dep_id)
         into   my_dep_id_initiale
         from   depense_budget
         where  eng_id = a_eng_id and dep_ttc_saisie < 0;

         select max (el_id)
         into   my_el_id
         from   extourne_liq
         where  dep_id_n1 = my_dep_id_initiale;
      end if;

      -- lancement des differentes procedures d'insertion des tables de depense.
      ins_depense_budget (a_dep_id, a_exe_ordre, a_dpp_id, a_eng_id, my_dep_ht_saisie, my_dep_ttc_saisie, a_tap_id, a_utl_ordre, null);

      update depense_papier
         set dpp_ht_saisie = dpp_ht_saisie + my_dep_ht_saisie,
             dpp_tva_saisie = dpp_tva_saisie + my_dep_ttc_saisie - my_dep_ht_saisie,
             dpp_ttc_saisie = dpp_ttc_saisie + my_dep_ttc_saisie
       where dpp_id = a_dpp_id;

      -- on le passe en premier car utilise par les autres pour les upd_engage_reste_.
      ins_depense_ctrl_planco (a_exe_ordre, a_dep_id, a_chaine_planco);
      --ins_depense_ctrl_action(a_exe_ordre, a_dep_id, a_chaine_action);
      ins_depense_ctrl_analytique (a_exe_ordre, a_dep_id, a_chaine_analytique);
      ins_depense_ctrl_convention (a_exe_ordre, a_dep_id, a_chaine_convention);
      --ins_depense_ctrl_hors_marche(a_exe_ordre, a_dep_id, a_chaine_hors_marche);
      ins_depense_ctrl_marche (a_exe_ordre, a_dep_id, a_chaine_marche);

      ------ on met a jour les tables d'extourne
      select extourne_liq_def_seq.nextval
      into   my_eld_id
      from   dual;

      insert into extourne_liq_def
         select my_eld_id,
                a_dep_id,
                get_type_etat ('sur extourne')
         from   dual;

      -- c'est une liquidation sur credit extourne fleche
      if my_eng_ttc_saisie < 0 then
--        select max(dep_id) into my_dep_id_initiale from depense_budget where eng_id=a_eng_id;
--        select max(el_id) into my_el_id from extourne_liq where dep_id_n1=my_dep_id_initiale;
         insert into extourne_liq_repart
            select extourne_liq_repart_seq.nextval,
                   my_el_id,
                   my_eld_id
            from   dual;
      end if;

      -- on verifie la coherence des montants entre les differents depense_.
      verifier.verifier_depense_coherence (a_dep_id);
      verifier.verifier_depense_pap_coherence (a_dpp_id);
     -- on verifie si la coherence des montants budgetaires restant est  conservee.
     -- verif inutile pour extourne
--     Verifier.verifier_engage_coherence(a_eng_id);
   end;

   procedure ins_depense_directe (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_fou_ordre                     engage_budget.fou_ordre%type,
      a_org_id                        engage_budget.org_id%type,
      a_tcd_ordre                     engage_budget.tcd_ordre%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_eng_libelle                   engage_budget.eng_libelle%type,
      a_tyap_id                       engage_budget.tyap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type,
      a_chaine_action                 varchar2,
      a_chaine_analytique             varchar2,
      a_chaine_convention             varchar2,
      a_chaine_hors_marche            varchar2,
      a_chaine_marche                 varchar2,
      a_chaine_planco                 varchar2
   )
   is
      my_eng_id            engage_budget.eng_id%type;
      my_eng_numero        engage_budget.eng_numero%type;
      my_chaine_planco     varchar2 (30000);
      my_chaine            varchar2 (30000);
      my_pco_num           depense_ctrl_planco.pco_num%type;
      my_dpco_ht_saisie    depense_ctrl_planco.dpco_ht_saisie%type;
      my_dpco_ttc_saisie   depense_ctrl_planco.dpco_ttc_saisie%type;
   begin
      my_eng_id := null;
      my_eng_numero := null;
      -- on retravaille la chaine_planco, car c'est la seule qui differencie pour les parametres d'ins_engage
      my_chaine_planco := '';
      my_chaine := a_chaine_planco;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'imputation.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_pco_num
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         -- on enleve l'ecriture.
         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         -- on enleve la chaine des inventaires.
         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_chaine_planco := my_chaine_planco || my_pco_num || '$' || my_dpco_ht_saisie || '$' || my_dpco_ttc_saisie || '$';
      end loop;

      my_chaine_planco := my_chaine_planco || '$';
      -- on cree l'engagement
      engager.ins_engage (my_eng_id,
                          a_exe_ordre,
                          my_eng_numero,
                          a_fou_ordre,
                          a_org_id,
                          a_tcd_ordre,
                          a_tap_id,
                          a_eng_libelle,
                          a_dep_ht_saisie,
                          a_dep_ttc_saisie,
                          a_tyap_id,
                          a_utl_ordre,
                          a_chaine_action,
                          a_chaine_analytique,
                          a_chaine_convention,
                          a_chaine_hors_marche,
                          a_chaine_marche,
                          my_chaine_planco
                         );

      if a_dep_ttc_saisie < 0 then
         reverser.ins_reverse (a_dep_id,
                               a_exe_ordre,
                               a_dpp_id,
                               my_eng_id,
                               a_dep_ht_saisie,
                               a_dep_ttc_saisie,
                               a_tap_id,
                               a_utl_ordre,
                               a_dep_id_reversement,
                               a_chaine_action,
                               a_chaine_analytique,
                               a_chaine_convention,
                               a_chaine_hors_marche,
                               a_chaine_marche,
                               a_chaine_planco
                              );
      else
         -- on cree la depense
         ins_depense (a_dep_id,
                      a_exe_ordre,
                      a_dpp_id,
                      my_eng_id,
                      a_dep_ht_saisie,
                      a_dep_ttc_saisie,
                      a_tap_id,
                      a_utl_ordre,
                      a_dep_id_reversement,
                      a_chaine_action,
                      a_chaine_analytique,
                      a_chaine_convention,
                      a_chaine_hors_marche,
                      a_chaine_marche,
                      a_chaine_planco
                     );
      end if;
   end;

   procedure del_depense_budget (a_dep_id depense_budget.dep_id%type, a_utl_ordre z_depense_budget.zdep_utl_ordre%type)
   is
      my_nb                        integer;
      my_reim_id                   integer;
      my_eng_id                    engage_budget.eng_id%type;
      my_zdep_id                   z_depense_budget.zdep_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_montant_budgetaire        depense_budget.dep_montant_budgetaire%type;
      my_dep_total_ht              depense_budget.dep_ht_saisie%type;
      my_dep_total_ttc             depense_budget.dep_ttc_saisie%type;
      my_dep_total_bud             depense_budget.dep_montant_budgetaire%type;
      my_eng_montant_bud           engage_budget.eng_montant_budgetaire%type;
      my_eng_montant_bud_reste     engage_budget.eng_montant_budgetaire_reste%type;
      my_reste                     engage_budget.eng_montant_budgetaire_reste%type;
      my_dispo_ligne               v_budget_exec_credit.bdxc_disponible%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_dep_ht_saisie             depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie            depense_budget.dep_ttc_saisie%type;
      my_eng_tap_id                engage_budget.tap_id%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_dpp_id                    depense_budget.dpp_id%type;
      my_comm_id                   commande.comm_id%type;
      my_dpco_id                   depense_ctrl_planco.dpco_id%type;
      my_eng_ht_saisie             engage_budget.eng_ht_saisie%type;
      my_eng_ttc_saisie            engage_budget.eng_ttc_saisie%type;
      my_nb_extourne               integer;
      my_taux_tva                  number (5, 3);
      my_tap_taux                  jefy_admin.taux_prorata.tap_taux%type;
      my_new_ht                    engage_budget.eng_ht_saisie%type;
      my_new_ttc                   engage_budget.eng_ttc_saisie%type;

      cursor liste
      is
         select dpco_id
         from   depense_ctrl_planco
         where  dep_id = a_dep_id;

      isliquidationfromcarambole   smallint;
      isliquidationdirecte         smallint;
      isliqsurextourneflechee      smallint;
      wantsupprimerengagement      smallint;
   begin
      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La liquidation n''existe pas ou est deja annulee (dep_id:' || a_dep_id || ')');
      end if;

      -- on memorise les differents montants avant suppression
      select e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.dep_ht_saisie,
             d.dep_ttc_saisie,
             e.eng_ht_saisie,
             e.eng_ttc_saisie,
             d.dep_montant_budgetaire,
             e.eng_id,
             e.tap_id,
             e.eng_montant_budgetaire,
             e.eng_montant_budgetaire_reste,
             d.dpp_id,
             d.tap_id
      into   my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_dep_ht_saisie,
             my_dep_ttc_saisie,
             my_eng_ht_saisie,
             my_eng_ttc_saisie,
             my_montant_budgetaire,
             my_eng_id,
             my_eng_tap_id,
             my_eng_montant_bud,
             my_eng_montant_bud_reste,
             my_dpp_id,
             my_dep_tap_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      dbms_output.put_line ('a_dep_id=' || a_dep_id);
      dbms_output.put_line ('my_dep_ht_saisie=' || my_dep_ht_saisie);
      dbms_output.put_line ('my_dep_ttc_saisie=' || my_dep_ttc_saisie);
      dbms_output.put_line ('my_montant_budgetaire=' || my_montant_budgetaire);
      dbms_output.put_line ('my_eng_montant_bud=' || my_eng_montant_bud);
      dbms_output.put_line ('my_eng_montant_bud_reste=' || my_eng_montant_bud_reste);
      dbms_output.put_line ('my_eng_ht_saisie=' || my_eng_ht_saisie);
      dbms_output.put_line ('my_eng_ttc_saisie=' || my_eng_ttc_saisie);

      -- on teste si ce n'est pas un ORV.
      if my_montant_budgetaire < 0 then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      verifier.verifier_depense_exercice (my_exe_ordre, a_utl_ordre, my_org_id);
      verifier.verifier_util_depense_budget (a_dep_id);

      -- on met a jour les montants de la depense papier (en enlevant ceux de la depense_budget qu'on supprime)
      update depense_papier
         set dpp_ht_saisie = dpp_ht_saisie - my_dep_ht_saisie,
             dpp_tva_saisie = dpp_tva_saisie - my_dep_ttc_saisie + my_dep_ht_saisie,
             dpp_ttc_saisie = dpp_ttc_saisie - my_dep_ttc_saisie
       where dpp_id = my_dpp_id;

      -- est-ce qu'on est sur une liquidation passee sur un engagement d'extourne N+1
      select count (*)
      into   my_nb_extourne
      from   extourne_liq
      where  dep_id_n1 in (select dep_id
                           from   depense_budget
                           where  eng_id = my_eng_id);

      isliquidationfromcarambole := 0;
      isliquidationdirecte := is_dep_directe (a_dep_id);

      if (get_app_dep (a_dep_id) = 'DEPENSE') then
         isliquidationfromcarambole := 1;
      end if;

      -- si liquidation sur extourne flechee on ne met a jour que les montants HT et TTC
      isliqsurextourneflechee := is_dep_sur_extourne_flechee (a_dep_id);

      -- si liquidation directe de carambole (extourne ou pas), alors on supprime l'engagement
      if (isliquidationdirecte > 0 and isliquidationfromcarambole > 0) then
         dbms_output.put_line ('CAS liquidation directe carambole (extourne ou non)');
         wantsupprimerengagement := 1;
      -- on ne cherche pas à mettre à jour les montants de l'engagement
      else
         wantsupprimerengagement := 0;

          -- si on est sur une liquidation passee sur un engagement d'extourne N+1
          -- on recalcule les montants HT et TTC de l'engagement à partir de ce qui reste liquidé
          -- dans le cas de l'extourne les montants budgétaire sont toujours à 0 car ils n'impactent pas le budget
         -- if my_eng_ttc_saisie<0 or my_nb_extourne>0 then
         if (isliqsurextourneflechee > 0) then
            dbms_output.put_line ('CAS extourne flechee');

            -- on calcule le montant budgetaire de l'engagement sans la depense qu'on supprime
            select nvl (sum (budget.calculer_budgetaire (my_exe_ordre, my_eng_tap_id, my_org_id, dep_ht_saisie, dep_ttc_saisie)), 0)
            into   my_dep_total_bud
            from   depense_budget
            where  dep_id <> a_dep_id and eng_id = my_eng_id;

            -- si le taux de prorata est le meme entre la depense et l'engagement et que
            -- le ttc et le HT de l'engagement sont à zero
            -- (ROD : donc on est forcement dans le cas my_nb_extourne>0)
            -- ROD : dans quels cas on peut avoir my_eng_ttc_saisie=0 and my_eng_ht_saisie=0 ? )
            if my_eng_tap_id = my_dep_tap_id and my_eng_ttc_saisie = 0 and my_eng_ht_saisie = 0 then
               dbms_output.put_line ('on ne devrait pas passer par la ?');

               update engage_budget
                  set eng_ht_saisie = -my_dep_ht_saisie,
                      eng_tva_saisie = -my_dep_ttc_saisie + my_dep_ht_saisie,
                      eng_ttc_saisie = -my_dep_ttc_saisie
                where eng_id = my_eng_id;
            else
               --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
               select tap_taux
               into   my_tap_taux
               from   jefy_admin.taux_prorata
               where  tap_id = my_eng_tap_id;

               dbms_output.put_line (' my_tap_taux=' || my_tap_taux);

               if my_eng_ttc_saisie = 0 and my_eng_ht_saisie = 0 then
                  my_taux_tva := my_dep_ttc_saisie / my_dep_ht_saisie - 1;
               else
                  my_taux_tva := my_eng_ttc_saisie / my_eng_ht_saisie - 1;
               end if;

               dbms_output.put_line ('my_taux_tva=' || my_taux_tva);
               my_new_ht := round ((my_dep_total_bud) / (1 + (my_taux_tva * (1 - (my_tap_taux / 100)))), liquider_outils.get_nb_decimales (my_exe_ordre));
               --setScale(2, BigDecimal.ROUND_HALF_DOWN);
               my_new_ttc := round (my_new_ht * (1 + my_taux_tva), liquider_outils.get_nb_decimales (my_exe_ordre));   --.setScale(2, BigDecimal.ROUND_HALF_DOWN);
               dbms_output.put_line ('my_new_ht=' || my_new_ht);
               dbms_output.put_line ('my_dep_total_bud=' || my_dep_total_bud);
               dbms_output.put_line ('my_new_ttc=' || my_new_ttc);

               -- on ne touche pas au motant budgétaire qui doit rester à 0
               update engage_budget
                  set eng_ht_saisie = my_new_ht,
                      eng_ttc_saisie = my_new_ttc,
                      eng_tva_saisie = my_new_ttc - my_new_ht
                where eng_id = my_eng_id;
            end if;
         else
            -- si on est sur une liquidation passee sur un engagement NON d'extourne N+1 (ou ORV ???)
            -- on met a jour le reste engagé (on ne touche pas aux montants HT et TTC)
            dbms_output.put_line ('Cas NON extourne');

            -- on recupere les montants des autres factures et ORV.
            select nvl (sum (budget.calculer_budgetaire (my_exe_ordre, my_eng_tap_id, my_org_id, dep_ht_saisie, dep_ttc_saisie)), 0)
            into   my_dep_total_bud
            from   depense_budget
            where  dep_id <> a_dep_id and dep_id in (select dep_id
                                                     from   depense_ctrl_planco
                                                     where  dep_id in (select dep_id
                                                                       from   depense_budget
                                                                       where  eng_id = my_eng_id) and (dpco_montant_budgetaire > 0                                /*OR man_id IN
                                                                                                                                   (SELECT man_id FROM v_mandat WHERE man_etat IN ('VISE','PAYE'))*/));

            -- on calcule le reste de l'engagement
            my_reste := my_eng_montant_bud - my_dep_total_bud;

            if my_reste < 0 then
               my_reste := 0;
            end if;

            dbms_output.put_line ('my_reste (budgetaire) = ' || my_reste);
            -- on compare au dispo de la ligne budgetaire ... pour voir si on peut tt reengager.
            my_dispo_ligne := my_montant_budgetaire + budget.engager_disponible (my_exe_ordre, my_org_id, my_tcd_ordre);
            dbms_output.put_line (' my_dispo_ligne = ' || my_dispo_ligne);

            if my_reste > my_dispo_ligne + my_eng_montant_bud_reste then
               my_reste := my_dispo_ligne + my_eng_montant_bud_reste;
            end if;

            dbms_output.put_line ('my_reste (budgetaire) = ' || my_reste);

            -- on modifie l'engagement.
            update engage_budget
               set eng_montant_budgetaire_reste = my_reste
             where eng_id = my_eng_id;
         end if;
      end if;

      -- tout est bon ... on supprime la depense.
      log_depense_budget (a_dep_id, a_utl_ordre);

      delete from depense_ctrl_action
            where dep_id = a_dep_id;

      delete from depense_ctrl_analytique
            where dep_id = a_dep_id;

      delete from depense_ctrl_convention
            where dep_id = a_dep_id;

      delete from depense_ctrl_hors_marche
            where dep_id = a_dep_id;

      delete from depense_ctrl_marche
            where dep_id = a_dep_id;

      open liste ();

      loop
         fetch liste
         into  my_dpco_id;

         exit when liste%notfound;
         jefy_inventaire.api_corossol.supprimer_lien_inv_dep (my_dpco_id);

         delete from depense_ctrl_planco
               where dpco_id = my_dpco_id;
      end loop;

      close liste;

      -- Suppression des reimputations associees (Dont celles effectuees dans Kiwi)
      select count (*)
      into   my_nb
      from   reimputation
      where  dep_id = a_dep_id;

      if my_nb > 0 then
         jefy_mission.kiwi_paiement.del_reimputation (a_dep_id);

         delete from reimputation_action
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_analytique
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_budget
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_convention
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_hors_marche
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_marche
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation_planco
               where reim_id in (select reim_id
                                 from   reimputation
                                 where  dep_id = a_dep_id);

         delete from reimputation
               where dep_id = a_dep_id;
      end if;

      delete from extourne_liq_repart
            where eld_id in (select eld_id
                             from   extourne_liq_def
                             where  dep_id = a_dep_id);

      delete from extourne_liq_def
            where dep_id = a_dep_id;

      delete from depense_budget
            where dep_id = a_dep_id;

      if (wantsupprimerengagement > 0) then
         -- on supprime l'engagement (le budget est mis a jour dans la procedure)
         dbms_output.put_line ('Suppression de l''engagement eng_id='||my_eng_id);
         engager.del_engage (my_eng_id, a_utl_ordre);
      else
         if my_eng_ttc_saisie >= 0 then
            dbms_output.put_line ('Mise a jour du budget');
            budget.maj_budget (my_exe_ordre, my_org_id, my_tcd_ordre);
            dbms_output.put_line ('Correction reste engage');
            corriger.upd_engage_reste (my_eng_id);
         end if;

         -- on verifie si la coherence des montants budgetaires restant est  conservee.
         dbms_output.put_line ('Verification si coherence des montants budgetaires restant est conservee');
         verifier.verifier_engage_coherence (my_eng_id);
         verifier.verifier_depense_pap_coherence (my_dpp_id);
      end if;

      apres_liquide.del_depense_budget (my_eng_id, a_dep_id);
   end;

   procedure del_depense_papier (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre z_depense_papier.zdpp_utl_ordre%type)
   is
      my_nb                integer;
      my_exe_ordre         depense_papier.exe_ordre%type;
      my_dpp_ttc_initial   depense_papier.dpp_ttc_initial%type;
   begin
      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb = 0 then
         raise_application_error (-20001, 'La facture papier n''existe pas ou est deja annule (dpp_id:' || a_dpp_id || ')');
      end if;

      select exe_ordre,
             dpp_ttc_initial
      into   my_exe_ordre,
             my_dpp_ttc_initial
      from   depense_papier
      where  dpp_id = a_dpp_id;

      -- on teste si ce n'est pas un ORV.
      if my_dpp_ttc_initial < 0 then
         raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
      end if;

      -- verifier qu'on a le droit de liquider sur cet exercice.
      --Verifier.verifier_depense_exercice(my_exe_ordre, a_utl_ordre);
      verifier.verifier_util_depense_papier (a_dpp_id);
      log_depense_papier (a_dpp_id, a_utl_ordre);

      delete from commande_dep_papier
            where dpp_id = a_dpp_id;

      delete from depense_papier
            where dpp_id = a_dpp_id;

      apres_liquide.del_depense_papier (a_dpp_id);
   end;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------
   procedure log_depense_budget (a_dep_id depense_budget.dep_id%type, a_utl_ordre z_depense_budget.zdep_utl_ordre%type)
   is
      my_zdep_id   z_depense_budget.zdep_id%type;
   begin
      select z_depense_budget_seq.nextval
      into   my_zdep_id
      from   dual;

      insert into z_depense_budget
         select my_zdep_id,
                sysdate,
                a_utl_ordre,
                e.*
         from   depense_budget e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_action
         select z_depense_ctrl_action_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_action e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_analytique
         select z_depense_ctrl_analytique_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_analytique e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_convention
         select z_depense_ctrl_convention_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_convention e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_hors_marche
         select z_depense_ctrl_hors_marche_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_hors_marche e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_marche
         select z_depense_ctrl_marche_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_marche e
         where  dep_id = a_dep_id;

      insert into z_depense_ctrl_planco
         select z_depense_ctrl_planco_seq.nextval,
                e.*,
                my_zdep_id
         from   depense_ctrl_planco e
         where  dep_id = a_dep_id;
   end;

   procedure log_depense_papier (a_dpp_id depense_papier.dpp_id%type, a_utl_ordre z_depense_papier.zdpp_utl_ordre%type)
   is
   begin
      insert into z_depense_papier
         select z_depense_papier_seq.nextval,
                sysdate,
                a_utl_ordre,
                e.*
         from   depense_papier e
         where  dpp_id = a_dpp_id;
   end;

   procedure ins_depense_budget (
      a_dep_id               in out   depense_budget.dep_id%type,
      a_exe_ordre                     depense_budget.exe_ordre%type,
      a_dpp_id                        depense_budget.dpp_id%type,
      a_eng_id                        depense_budget.eng_id%type,
      a_dep_ht_saisie                 depense_budget.dep_ht_saisie%type,
      a_dep_ttc_saisie                depense_budget.dep_ttc_saisie%type,
      a_tap_id                        depense_budget.tap_id%type,
      a_utl_ordre                     depense_budget.utl_ordre%type,
      --a_der_id             depense_budget.der_id%type,
      a_dep_id_reversement            depense_budget.dep_id_reversement%type
   )
   is
      my_dep_ht_saisie             depense_budget.dep_ht_saisie%type;
      my_dep_ttc_saisie            depense_budget.dep_ttc_saisie%type;
      my_nb_decimales              number;
      my_nb                        integer;
      my_par_value                 parametre.par_value%type;
      my_org_id                    engage_budget.org_id%type;
      my_tap_id                    engage_budget.tap_id%type;
      my_exe_ordre                 engage_budget.exe_ordre%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_eng_id                    engage_budget.eng_id%type;
      my_montant_budgetaire        depense_budget.dep_montant_budgetaire%type;
      my_eng_budgetaire            engage_budget.eng_montant_budgetaire%type;
      my_eng_reste                 engage_budget.eng_montant_budgetaire_reste%type;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
      my_eng_ht                    engage_budget.eng_ht_saisie%type;
      my_eng_ttc                   engage_budget.eng_ttc_saisie%type;
      my_dep_eng_budgetaire        engage_budget.eng_montant_budgetaire%type;
      my_dpp_ht_initial            depense_papier.dpp_ht_initial%type;
      my_dpp_tva_initial           depense_papier.dpp_tva_initial%type;
      my_dpp_ttc_initial           depense_papier.dpp_ttc_initial%type;
      my_dep_tva_saisie            depense_budget.dep_tva_saisie%type;
      --my_sum_ht_saisie    depense_budget.dep_ht_saisie%type;
      --my_sum_tva_saisie   depense_budget.dep_tva_saisie%type;
      --my_sum_ttc_saisie   depense_budget.dep_ttc_saisie%type;
      my_sum_rev_ht                depense_budget.dep_ht_saisie%type;
      my_sum_rev_ttc               depense_budget.dep_ttc_saisie%type;
      my_dep_id_reversement        depense_budget.dep_id_reversement%type;
      my_dep_ht_init               depense_budget.dep_ht_saisie%type;
      my_dep_ttc_init              depense_budget.dep_ttc_saisie%type;
      my_dpp_exe_ordre             depense_papier.exe_ordre%type;
      my_dep_exe_ordre             depense_budget.exe_ordre%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_eng_negatif_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_extourne_budgetaire   depense_budget.dep_montant_budgetaire%type;
      my_taux_tva                  number (5, 3);
      my_tap_taux                  jefy_admin.taux_prorata.tap_taux%type;
      my_new_ht                    engage_budget.eng_ht_saisie%type;
      my_new_ttc                   engage_budget.eng_ttc_saisie%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);
      my_dep_ht_saisie := round (a_dep_ht_saisie, my_nb_decimales);
      my_dep_ttc_saisie := round (a_dep_ttc_saisie, my_nb_decimales);

      select count (*)
      into   my_nb
      from   depense_papier
      where  dpp_id = a_dpp_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La facture n''existe pas (dpp_id=' || a_dpp_id || ')');
      end if;

      select count (*)
      into   my_nb
      from   engage_budget
      where  eng_id = a_eng_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'L''engagement n''existe pas (eng_id=' || a_eng_id || ')');
      end if;

      select exe_ordre,
             org_id,
             tap_id,
             tcd_ordre,
             eng_montant_budgetaire_reste,
             eng_montant_budgetaire,
             eng_ht_saisie,
             eng_ttc_saisie
      into   my_exe_ordre,
             my_org_id,
             my_tap_id,
             my_tcd_ordre,
             my_eng_reste,
             my_eng_init,
             my_eng_ht,
             my_eng_ttc
      from   engage_budget
      where  eng_id = a_eng_id;

      --extourne
      if (my_eng_ht < 0 or my_eng_ttc < 0) and (my_dep_ht_saisie >= 0 and my_dep_ttc_saisie >= 0) then
         -- on compare les montants budgetaires
         my_eng_negatif_budgetaire := budget.calculer_budgetaire (my_exe_ordre, my_tap_id, my_org_id, my_eng_ht, my_eng_ttc);
         my_dep_extourne_budgetaire := budget.calculer_budgetaire (a_exe_ordre, a_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

         if abs (my_eng_negatif_budgetaire) < my_dep_extourne_budgetaire then
            raise_application_error (-20001, 'Pour une liquidation sur mandat d''extourne on ne peut pas depasser le montant des credits extournes');
         end if;
      else
         if my_dep_ht_saisie < 0 or my_dep_ttc_saisie < 0 or a_dep_id_reversement is not null or my_eng_ttc < 0 then
            raise_application_error (-20001, 'Pour un ordre de reversement il faut utiliser le package "reverser"');
         end if;
      end if;

      select exe_ordre,
             dpp_ht_initial,
             dpp_tva_initial,
             dpp_ttc_initial
      into   my_dpp_exe_ordre,
             my_dpp_ht_initial,
             my_dpp_tva_initial,
             my_dpp_ttc_initial
      from   depense_papier
      where  dpp_id = a_dpp_id;

      --    RAISE_APPLICATION_ERROR(-20001,'On ne peut pas liquider sur un engagement d''extourne');

      -- verification de la coherence de l'exercice.
      if a_exe_ordre <> my_exe_ordre then
         raise_application_error (-20001, 'La facture doit etre sur le meme exercice que l''engagement');
      end if;

      if a_exe_ordre <> my_dpp_exe_ordre then
         raise_application_error (-20001, 'La facture doit etre sur le meme exercice que la facture papier.');
      end if;

      -- verification de la coherence du prorata.
      if get_parametre (a_exe_ordre, 'DEPENSE_IDEM_TAP_ID') <> 'NON' and my_tap_id <> a_tap_id then
         raise_application_error (-20001, 'il faut que le taux de prorata de la depense soit le meme que l''engagement initial.');
      end if;

      -- on verifie la coherence des montants.
      if abs (my_dep_ht_saisie) > abs (my_dep_ttc_saisie) then
         raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
      end if;

      -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
      my_dep_tva_saisie := liquider_outils.get_tva (my_dep_ht_saisie, my_dep_ttc_saisie);

      -- on teste si la facture qu'on insere ne depasse pas ce que l'on a declar? dans la papier.
      /*select nvl(sum(dep_ht_saisie),0), nvl(sum(dep_tva_saisie),0) , nvl(sum(dep_ttc_saisie),0)
             into my_sum_ht_saisie, my_sum_tva_saisie, my_sum_ttc_saisie
        from depense_budget where dpp_id=a_dpp_id;

      if abs(my_sum_ht_saisie+a_dep_ht_saisie) > abs(my_dpp_ht_initial) or
         abs(my_sum_tva_saisie+my_dep_tva_saisie) > abs(my_dpp_tva_initial) or
         abs(my_sum_ttc_saisie+a_dep_ttc_saisie) > abs(my_dpp_ttc_initial) then
         raise_application_error(-20001,'Les sommes ne sont pas coherentes (dpp_id='||a_dpp_id||')');
      end if;
      */
      select count (*)
      into   my_nb
      from   engage_ctrl_action
      where  eng_id = a_eng_id;

      if my_eng_init = 0 and my_nb = 0 then
         my_montant_budgetaire := 0;
         my_eng_budgetaire := 0;
      else
         verifier.verifier_budget (a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);
         -- calcul du montant budgetaire.
         my_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, a_tap_id, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);
         -- on calcule pour diminuer le reste de l'engagement du montant liquid? ou celui par rapport au tap_id.
         --    de l'engage (si le tap_id peut etre different de celui de l'engage).
         my_eng_budgetaire := liquider_outils.get_eng_montant_budgetaire (a_exe_ordre, my_tap_id, a_tap_id, my_montant_budgetaire, my_org_id, my_dep_ht_saisie, my_dep_ttc_saisie);

         -- si le reste engage est 0, et qu'on passe une liquidation positive -> erreur.

         --> ON ENLEVE TEMPORAIREMENT POUR L'EXTOURNE, A CONTOURNER PLUS TARD
            --IF my_eng_reste<=0 AND my_eng_budgetaire>0 THEN
               --   RAISE_APPLICATION_ERROR(-20001,'L''engagement est deja solde ('||indication_erreur.engagement(a_eng_id)||')');
            --END IF;
         --<

         -- on verifie qu'on ne diminue pas plus que ce qu'il y a d'engage.
         if my_eng_reste < my_eng_budgetaire then
            my_eng_budgetaire := my_eng_reste;
         end if;
      end if;

      -- insertion dans la table.
      if a_dep_id is null then
         select depense_budget_seq.nextval
         into   a_dep_id
         from   dual;
      end if;

      -- on diminue le reste engage de l'engagement.
      update engage_budget
         set eng_montant_budgetaire_reste = eng_montant_budgetaire_reste - my_eng_budgetaire
       where eng_id = a_eng_id;

      if (my_eng_ht < 0 or my_eng_ttc < 0) and (my_dep_ht_saisie >= 0 and my_dep_ttc_saisie >= 0) then
         if abs (my_eng_negatif_budgetaire) < my_dep_extourne_budgetaire then
            update engage_budget
               set eng_ht_saisie = 0,
                   eng_tva_saisie = 0,
                   eng_ttc_saisie = 0
             where eng_id = a_eng_id;
         else
            --- VERIFIER SI IL N'Y A PAS UN PB DE CENTIME (arrondi, prorata ...)
            select tap_taux
            into   my_tap_taux
            from   jefy_admin.taux_prorata
            where  tap_id = my_tap_id;

            my_taux_tva := my_eng_ttc / my_eng_ht - 1;
            my_new_ht := round ((my_eng_negatif_budgetaire + my_dep_extourne_budgetaire) / (1 + (my_taux_tva * (1 - (my_tap_taux / 100)))), liquider_outils.get_nb_decimales (a_exe_ordre));
            --setScale(2, BigDecimal.ROUND_HALF_DOWN);
            my_new_ttc := round (my_new_ht * (1 + my_taux_tva), liquider_outils.get_nb_decimales (a_exe_ordre));   --.setScale(2, BigDecimal.ROUND_HALF_DOWN);

            update engage_budget
               set eng_ht_saisie = my_new_ht,
                   eng_ttc_saisie = my_new_ttc,
                   eng_tva_saisie = my_new_ttc - my_new_ht
             where eng_id = a_eng_id;
         end if;
      end if;

      -- on liquide.
      insert into depense_budget
      values      (a_dep_id,
                   a_exe_ordre,
                   a_dpp_id,
                   a_eng_id,
                   my_montant_budgetaire,
                   my_dep_ht_saisie,
                   my_dep_tva_saisie,
                   my_dep_ttc_saisie,
                   a_tap_id,
                   a_utl_ordre,
                   a_dep_id_reversement
                  );

      if my_montant_budgetaire <> 0 then
         budget.maj_budget (a_exe_ordre, my_org_id, my_tcd_ordre);
      end if;

      -- procedure de verification
      apres_liquide.budget (a_dep_id);
   end;

   procedure ins_depense_ctrl_action (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dact_id                   depense_ctrl_action.dact_id%type;
      my_tyac_id                   depense_ctrl_action.tyac_id%type;
      my_dact_montant_budgetaire   depense_ctrl_action.dact_montant_budgetaire%type;
      my_dact_ht_saisie            depense_ctrl_action.dact_ht_saisie%type;
      my_dact_tva_saisie           depense_ctrl_action.dact_tva_saisie%type;
      my_dact_ttc_saisie           depense_ctrl_action.dact_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_action.dact_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.utl_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_utl_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'action.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_tyac_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dact_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dact_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dact_ht_saisie := round (my_dact_ht_saisie, my_nb_decimales);
         my_dact_ttc_saisie := round (my_dact_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dact_ht_saisie) > abs (my_dact_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dact_tva_saisie := liquider_outils.get_tva (my_dact_ht_saisie, my_dact_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dact_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dact_ht_saisie, my_dact_ttc_saisie);

         if my_dact_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dact_montant_budgetaire then
            my_dact_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_action_seq.nextval
         into   my_dact_id
         from   dual;

         insert into depense_ctrl_action
         values      (my_dact_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_tyac_id,
                      my_dact_montant_budgetaire,
                      my_dact_ht_saisie,
                      my_dact_tva_saisie,
                      my_dact_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_action (my_eng_id);
         verifier.verifier_action (a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
         apres_liquide.action (my_dact_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dact_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_analytique (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dana_id                   depense_ctrl_analytique.dana_id%type;
      my_can_id                    depense_ctrl_analytique.can_id%type;
      my_dana_montant_budgetaire   depense_ctrl_analytique.dana_montant_budgetaire%type;
      my_dana_ht_saisie            depense_ctrl_analytique.dana_ht_saisie%type;
      my_dana_tva_saisie           depense_ctrl_analytique.dana_tva_saisie%type;
      my_dana_ttc_saisie           depense_ctrl_analytique.dana_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_analytique.dana_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
      org_canal_obligatoire        BOOLEAN;

   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

	  org_canal_obligatoire := IS_CODE_ANALYTIQUE_OBLIGATOIRE(my_org_id);

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le code analytique.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_can_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dana_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dana_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dana_ht_saisie := round (my_dana_ht_saisie, my_nb_decimales);
         my_dana_ttc_saisie := round (my_dana_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dana_ht_saisie) > abs (my_dana_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dana_tva_saisie := liquider_outils.get_tva (my_dana_ht_saisie, my_dana_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_analytique
         where  eng_id = my_eng_id;

         /*if my_eng_init=0 and my_nb=0 then
            my_dana_montant_budgetaire:=0;
         else*/-- on calcule le montant budgetaire.
         my_dana_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dana_ht_saisie, my_dana_ttc_saisie);

         --end if;
         if my_dana_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si il n'y a pas assez de dispo ou si on a un problème d'arrondi sur le dernier element dans le cadre du code analytique obligatoire
         if (my_dep_montant_budgetaire <= my_somme + my_dana_montant_budgetaire) OR (SUBSTR(my_chaine,1,1) = '$' and org_canal_obligatoire = true) THEN
           my_dana_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_analytique_seq.nextval
         into   my_dana_id
         from   dual;

         insert into depense_ctrl_analytique
         values      (my_dana_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_can_id,
                      my_dana_montant_budgetaire,
                      my_dana_ht_saisie,
                      my_dana_tva_saisie,
                      my_dana_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_analytique (my_eng_id);
         verifier.verifier_analytique (a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
         apres_liquide.analytique (my_dana_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dana_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_convention (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dcon_id                   depense_ctrl_convention.dcon_id%type;
      my_conv_ordre                depense_ctrl_convention.conv_ordre%type;
      my_dcon_montant_budgetaire   depense_ctrl_convention.dcon_montant_budgetaire%type;
      my_dcon_ht_saisie            depense_ctrl_convention.dcon_ht_saisie%type;
      my_dcon_tva_saisie           depense_ctrl_convention.dcon_tva_saisie%type;
      my_dcon_ttc_saisie           depense_ctrl_convention.dcon_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_convention.dcon_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le code analytique.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_conv_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dcon_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dcon_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dcon_ht_saisie := round (my_dcon_ht_saisie, my_nb_decimales);
         my_dcon_ttc_saisie := round (my_dcon_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dcon_ht_saisie) > abs (my_dcon_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dcon_tva_saisie := liquider_outils.get_tva (my_dcon_ht_saisie, my_dcon_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_action
         where  eng_id = my_eng_id;

         /*if my_eng_init=0 and my_nb=0 then
            my_dcon_montant_budgetaire:=0;
         else*/-- on calcule le montant budgetaire.
         my_dcon_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dcon_ht_saisie, my_dcon_ttc_saisie);

         --end if;
         if my_dcon_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si il n'y a pas assez de dispo.
         if my_nb > 0 then
            if my_dep_montant_budgetaire <= my_somme + my_dcon_montant_budgetaire then
               my_dcon_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
            end if;
         end if;

         -- insertion dans la base.
         select depense_ctrl_convention_seq.nextval
         into   my_dcon_id
         from   dual;

         insert into depense_ctrl_convention
         values      (my_dcon_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_conv_ordre,
                      my_dcon_montant_budgetaire,
                      my_dcon_ht_saisie,
                      my_dcon_tva_saisie,
                      my_dcon_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_convention (my_eng_id);
         verifier.verifier_convention (a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
         apres_liquide.convention (my_dcon_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dcon_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_hors_marche (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dhom_id                   depense_ctrl_hors_marche.dhom_id%type;
      my_typa_id                   depense_ctrl_hors_marche.typa_id%type;
      my_ce_ordre                  depense_ctrl_hors_marche.ce_ordre%type;
      my_dhom_montant_budgetaire   depense_ctrl_hors_marche.dhom_montant_budgetaire%type;
      my_dhom_ht_saisie            depense_ctrl_hors_marche.dhom_ht_saisie%type;
      my_dhom_tva_saisie           depense_ctrl_hors_marche.dhom_tva_saisie%type;
      my_dhom_ttc_saisie           depense_ctrl_hors_marche.dhom_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_fou_ordre                 engage_budget.fou_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_hors_marche.dhom_montant_budgetaire%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.exe_ordre,
             e.fou_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_exe_ordre,
             my_fou_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere le type_?chat.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_typa_id
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le code nomenclature.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_ce_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dhom_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dhom_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dhom_ht_saisie := round (my_dhom_ht_saisie, my_nb_decimales);
         my_dhom_ttc_saisie := round (my_dhom_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dhom_ht_saisie) > abs (my_dhom_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dhom_tva_saisie := liquider_outils.get_tva (my_dhom_ht_saisie, my_dhom_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dhom_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dhom_ht_saisie, my_dhom_ttc_saisie);

         if my_dhom_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dhom_montant_budgetaire then
            my_dhom_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_hors_marche_seq.nextval
         into   my_dhom_id
         from   dual;

         insert into depense_ctrl_hors_marche
         values      (my_dhom_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_typa_id,
                      my_ce_ordre,
                      my_dhom_montant_budgetaire,
                      my_dhom_ht_saisie,
                      my_dhom_tva_saisie,
                      my_dhom_ttc_saisie
                     );

         -- procedure de verification
         corriger.upd_engage_reste_hors_marche (my_eng_id);
         verifier.verifier_hors_marche (a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
         apres_liquide.hors_marche (my_dhom_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dhom_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_marche (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dmar_id                   depense_ctrl_marche.dmar_id%type;
      my_att_ordre                 depense_ctrl_marche.att_ordre%type;
      my_dmar_montant_budgetaire   depense_ctrl_marche.dmar_montant_budgetaire%type;
      my_dmar_ht_saisie            depense_ctrl_marche.dmar_ht_saisie%type;
      my_dmar_tva_saisie           depense_ctrl_marche.dmar_tva_saisie%type;
      my_dmar_ttc_saisie           depense_ctrl_marche.dmar_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_marche.dmar_montant_budgetaire%type;
      my_dpp_id                    depense_budget.dpp_id%type;
      my_fou_ordre                 depense_papier.fou_ordre%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_nb_decimales              number;
      my_eng_init                  engage_budget.eng_montant_budgetaire%type;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.dpp_id,
             d.exe_ordre,
             d.utl_ordre,
             d.eng_id,
             e.eng_montant_budgetaire
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_dpp_id,
             my_exe_ordre,
             my_utl_ordre,
             my_eng_id,
             my_eng_init
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      select fou_ordre
      into   my_fou_ordre
      from   depense_papier
      where  dpp_id = my_dpp_id;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'attribution.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_att_ordre
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dmar_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dmar_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dmar_ht_saisie := round (my_dmar_ht_saisie, my_nb_decimales);
         my_dmar_ttc_saisie := round (my_dmar_ttc_saisie, my_nb_decimales);

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dmar_ht_saisie) > abs (my_dmar_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dmar_tva_saisie := liquider_outils.get_tva (my_dmar_ht_saisie, my_dmar_ttc_saisie);

         select count (*)
         into   my_nb
         from   engage_ctrl_action
         where  eng_id = my_eng_id;

         if my_eng_init = 0 and my_nb = 0 then
            my_dmar_montant_budgetaire := 0;
         else
            -- on calcule le montant budgetaire.
            my_dmar_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dmar_ht_saisie, my_dmar_ttc_saisie);
         end if;

         if my_dmar_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dmar_montant_budgetaire then
            my_dmar_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- insertion dans la base.
         select depense_ctrl_marche_seq.nextval
         into   my_dmar_id
         from   dual;

         insert into depense_ctrl_marche
         values      (my_dmar_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_att_ordre,
                      my_dmar_montant_budgetaire,
                      my_dmar_ht_saisie,
                      my_dmar_tva_saisie,
                      my_dmar_ttc_saisie
                     );

         dbms_output.put_line ('insert depense : ' || my_dmar_id);
         -- procedure de verification
         dbms_output.put_line ('upd engage : ' || my_eng_id);
         corriger.upd_engage_reste_marche (my_eng_id);
         verifier.verifier_marche (a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
         apres_liquide.marche (my_dmar_id);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dmar_montant_budgetaire;
      end loop;
   end;

   procedure ins_depense_ctrl_planco (a_exe_ordre depense_budget.exe_ordre%type, a_dep_id depense_budget.dep_id%type, a_chaine varchar2)
   is
      my_dpco_id                   depense_ctrl_planco.dpco_id%type;
      my_pco_num                   depense_ctrl_planco.pco_num%type;
      my_dpco_montant_budgetaire   depense_ctrl_planco.dpco_montant_budgetaire%type;
      my_dpco_ht_saisie            depense_ctrl_planco.dpco_ht_saisie%type;
      my_dpco_tva_saisie           depense_ctrl_planco.dpco_tva_saisie%type;
      my_dpco_ttc_saisie           depense_ctrl_planco.dpco_ttc_saisie%type;
      my_dep_montant_budgetaire    depense_budget.dep_montant_budgetaire%type;
      my_dep_tap_id                depense_budget.tap_id%type;
      my_exe_ordre                 depense_budget.exe_ordre%type;
      my_utl_ordre                 depense_budget.utl_ordre%type;
      my_org_id                    engage_budget.org_id%type;
      my_tcd_ordre                 engage_budget.tcd_ordre%type;
      my_par_value                 parametre.par_value%type;
      my_nb                        integer;
      my_chaine                    varchar2 (30000);
      my_somme                     depense_ctrl_planco.dpco_montant_budgetaire%type;
      my_ecd_ordre                 depense_ctrl_planco.ecd_ordre%type;
      my_mod_ordre                 depense_papier.mod_ordre%type;
      my_tbo_ordre                 depense_ctrl_planco.tbo_ordre%type;
      my_eng_id                    depense_budget.eng_id%type;
      my_inventaires               varchar2 (30000);
      my_nb_decimales              number;
   begin
      my_nb_decimales := liquider_outils.get_nb_decimales (a_exe_ordre);

      select count (*)
      into   my_nb
      from   depense_budget
      where  dep_id = a_dep_id;

      if my_nb <> 1 then
         raise_application_error (-20001, 'La depense n''existe pas (dep_id=' || a_dep_id || ')');
      end if;

      select d.dep_montant_budgetaire,
             d.tap_id,
             e.org_id,
             e.tcd_ordre,
             d.utl_ordre,
             d.eng_id
      into   my_dep_montant_budgetaire,
             my_dep_tap_id,
             my_org_id,
             my_tcd_ordre,
             my_utl_ordre,
             my_eng_id
      from   depense_budget d, engage_budget e
      where  e.eng_id = d.eng_id and d.dep_id = a_dep_id;

      if my_exe_ordre <> a_exe_ordre then
         raise_application_error (-20001, 'l''exercice n''est pas coherent.');
      end if;

      my_chaine := a_chaine;
      my_somme := 0;

      loop
         if substr (my_chaine, 1, 1) = '$' then
            exit;
         end if;

         -- on recupere l'imputation.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_pco_num
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ht.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ht_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere le ttc.
         select grhum.en_nombre (substr (my_chaine, 1, instr (my_chaine, '$') - 1))
         into   my_dpco_ttc_saisie
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         -- on recupere l'ecriture.
         --SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ecd_ordre FROM dual;
         select ecd_ordre
         into   my_ecd_ordre
         from   depense_papier
         where  dpp_id in (select dpp_id
                           from   depense_budget
                           where  dep_id = a_dep_id);

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));
         my_dpco_ht_saisie := round (my_dpco_ht_saisie, my_nb_decimales);
         my_dpco_ttc_saisie := round (my_dpco_ttc_saisie, my_nb_decimales);

         -- on recupere la chaine des inventaires.
         select substr (my_chaine, 1, instr (my_chaine, '$') - 1)
         into   my_inventaires
         from   dual;

         my_chaine := substr (my_chaine, instr (my_chaine, '$') + 1, length (my_chaine));

         if my_ecd_ordre is not null then
            select mod_ordre
            into   my_mod_ordre
            from   depense_budget d, depense_papier p
            where  p.dpp_id = d.dpp_id and d.dep_id = a_dep_id;

            verifier.verifier_emargement (a_exe_ordre, my_mod_ordre, my_ecd_ordre);
         end if;

         -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
         if abs (my_dpco_ht_saisie) > abs (my_dpco_ttc_saisie) then
            raise_application_error (-20001, 'Probleme : le HT est superieur au TTC');
         end if;

         my_dpco_tva_saisie := liquider_outils.get_tva (my_dpco_ht_saisie, my_dpco_ttc_saisie);
         -- on calcule le montant budgetaire.
         my_dpco_montant_budgetaire := budget.calculer_budgetaire (a_exe_ordre, my_dep_tap_id, my_org_id, my_dpco_ht_saisie, my_dpco_ttc_saisie);

         if my_dpco_montant_budgetaire < 0 then
            raise_application_error (-20001, 'Pour une depense il faut un montant positif');
         end if;

         -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
         if substr (my_chaine, 1, 1) = '$' or my_dep_montant_budgetaire <= my_somme + my_dpco_montant_budgetaire then
            my_dpco_montant_budgetaire := my_dep_montant_budgetaire - my_somme;
         end if;

         -- on bloque a une seule imputation pour regler le probleme d'eventuels rejets partiel des mandats.
         select count (*)
         into   my_nb
         from   depense_ctrl_planco
         where  dep_id = a_dep_id;

         if my_nb > 0 then
            raise_application_error (-20001, 'On ne peut mettre qu''une imputation comptable pour une depense ');
         end if;

         -- insertion dans la base.
         select depense_ctrl_planco_seq.nextval
         into   my_dpco_id
         from   dual;

         insert into depense_ctrl_planco
         values      (my_dpco_id,
                      a_exe_ordre,
                      a_dep_id,
                      my_pco_num,
                      null,
                      my_dpco_montant_budgetaire,
                      my_dpco_ht_saisie,
                      my_dpco_tva_saisie,
                      my_dpco_ttc_saisie,
                      1,
                      my_ecd_ordre
                     );

         my_tbo_ordre := get_tbo_ordre (my_dpco_id);

         update depense_ctrl_planco
            set tbo_ordre = my_tbo_ordre
          where dpco_id = my_dpco_id;

         -- procedure de verification
         corriger.upd_engage_reste_planco (my_eng_id);
         verifier.verifier_planco (a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
         apres_liquide.planco (my_dpco_id, my_inventaires);
         -- mise a jour de la somme de controle.
         my_somme := my_somme + my_dpco_montant_budgetaire;
      end loop;
   end;

-- renvoie >0 s'il s'agit d'une depense directe (sans engagement prealable) generee a partir de carambole
-- inclue les liquidation d'extourne, et liquidations sur enveloppe d'extourne
-- inclue egalement les liquidations issues de kiwi ou kaki ou autres
   function is_dep_directe (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      db                  depense_budget%rowtype;
      engid               engage_budget.eng_id%type;
      unseulengagement    smallint;
      montantidentiques   smallint;
      pasdecommande       smallint;
      flag                integer;
   begin
      -- depense directe =
      -- + un seul engagement par depense_budget
      -- + montants identiques entre depense_budget et engage_budget et reste = 0
      -- + pas de commande associee a l'engagement
      unseulengagement := 0;
      montantidentiques := 0;
      pasdecommande := 0;

      select eng_id
      into   engid
      from   depense_budget
      where  dep_id = a_dep_id;

      select count (*)
      into   flag
      from   depense_budget
      where  eng_id = engid;

      if (flag = 1) then
         unseulengagement := 1;
      end if;

--    select count(*) into flag from engage_budget eb inner join jefy_admin.type_application ta on eb.tyap_id=ta.tyap_id
--    where eng_id=engid and ta.TYAP_STRID='DEPENSE';
--    if (flag=1) then
--        creepardepense := 1;
--    end if;
      select count (*)
      into   flag
      from   depense_budget db inner join engage_budget eb on db.eng_id = eb.eng_id
      where  db.eng_id = engid and db.dep_montant_budgetaire = eb.eng_montant_budgetaire and db.dep_ht_saisie = eb.eng_ht_saisie and db.dep_tva_saisie = eb.eng_tva_saisie and db.dep_ttc_saisie = eb.eng_ttc_saisie and eb.eng_montant_budgetaire_reste = 0;

      if (flag = 1) then
         montantidentiques := 1;
      end if;

      select count (*)
      into   flag
      from   depense_budget db inner join engage_budget eb on db.eng_id = eb.eng_id
             inner join commande_engagement ce on ce.eng_id = eb.eng_id
      where  db.eng_id = engid;

      if (flag = 0) then
         pasdecommande := 1;
      end if;

      return unseulengagement * montantidentiques * pasdecommande;
   end;

-- renvoie >0 si la depense est une liquidation definitive sur enveloppe d'extourne
   function is_dep_sur_extourne_env (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isdepensedirecte      smallint;
      issurcreditextourne   integer;
      flag                  integer;
   begin
      -- depense sur enveloppe d'extourne =
      -- + depense directe
      -- + depense sur credit extourne
      isdepensedirecte := 0;
      issurcreditextourne := is_dep_sur_extourne (a_dep_id);

      if (issurcreditextourne > 0) then
         isdepensedirecte := is_dep_directe (a_dep_id);
      end if;

      return isdepensedirecte * issurcreditextourne;
   end;

-- renvoie >0 si la depense est une liquidation sur credit d'extourne fleche
   function is_dep_sur_extourne_flechee (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      issurcreditextourne   integer;
      isfleche              integer;
   begin
      -- liquidation sur credit d'extourne fleche =
      --  + depense sur credit extourne
      --  + lien existant entre liquidation definitive et liquidation d'extourne
      issurcreditextourne := is_dep_sur_extourne (a_dep_id);
      isfleche := 0;

      if (issurcreditextourne > 0) then
         select count (*)
         into   isfleche
         from   extourne_liq_def eld inner join extourne_liq_repart elr on eld.eld_id = elr.eld_id
                inner join extourne_liq el on elr.el_id = el.el_id
         where  eld.dep_id = a_dep_id and eld.tyet_id = 220;
      end if;

      return issurcreditextourne * isfleche;
   end;

-- renvoie >0 si la depense est une liquidation sur credit d'extourne
   function is_dep_sur_extourne (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isdansextourneliqdef   integer;
   begin
      --liquidation sur credit d'extourne = refernce dans la table  extourne_liq_def
      select count (*)
      into   isdansextourneliqdef
      from   extourne_liq_def
      where  dep_id = a_dep_id and tyet_id = 220;

      return isdansextourneliqdef;
   end;

-- renvoie >0 si la depense est une liquidation d'extourne
   function is_dep_extourne (a_dep_id depense_budget.dep_id%type)
      return integer
   as
      isliqextourne   integer;
   begin
      --liquidation d'extourne =
          -- + presence dans la table extourne_liq en dep_idN1
      select count (*)
      into   isliqextourne
      from   extourne_liq
      where  dep_id_n1 = a_dep_id;

      return isliqextourne;
   end;

-- renvoie l'application a l'origine de la depense (via l'engagement)
   function get_app_dep (a_dep_id depense_budget.dep_id%type)
      return jefy_admin.type_application.tyap_strid%type
   as
      res   jefy_admin.type_application.tyap_strid%type;
   begin
      select ta.tyap_strid
      into   res
      from   jefy_admin.type_application ta inner join engage_budget eb on ta.tyap_id = eb.tyap_id
             inner join depense_budget db on db.eng_id = eb.eng_id
      where  db.dep_id = a_dep_id;

      return res;
   end;
end;
/





CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Apres_Commun IS

   PROCEDURE hors_marche (
      a_typa_id         ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre        ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre       ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
      my_exe_ordre      ENGAGE_CTRL_HORS_MARCHE.exe_ordre%TYPE;
      my_ehom_ht_reste  ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
      my_dhom_ht_saisie DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
      my_ce_controle    jefy_marches.code_exer.ce_controle%TYPE;
      my_cm_code        jefy_marches.code_marche.cm_code%TYPE;
   BEGIN
        SELECT NVL(ce_controle,0), m.cm_code, e.exe_ordre INTO my_ce_controle, my_cm_code, my_exe_ordre
          FROM jefy_marches.code_exer e, jefy_marches.code_marche m WHERE ce_ordre=a_ce_ordre AND e.cm_ordre=m.cm_ordre;

          -- ajout sans achat
        IF a_typa_id<>Get_Type_Achat('MONOPOLE') AND a_typa_id<>Get_Type_Achat('3CMP')
            AND a_typa_id<>Get_Type_Achat('SANS ACHAT') THEN
            jefy_marches.creerCodeMarcheFour(a_fou_ordre, a_ce_ordre, 'jefy_depense');

              SELECT NVL(SUM(ehom_ht_reste),0) INTO my_ehom_ht_reste FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre AND typa_id=a_typa_id;
              SELECT NVL(SUM(dhom_ht_saisie),0) INTO my_dhom_ht_saisie FROM DEPENSE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre  AND typa_id=a_typa_id AND (dhom_ht_saisie>0 OR dep_id IN
                (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')));

            IF my_ce_controle=0 THEN
              my_ce_controle:=150000;
           END IF;

           IF my_ehom_ht_reste+my_dhom_ht_saisie>my_ce_controle THEN
              RAISE_APPLICATION_ERROR(-20001,'Le seuil '||my_ce_controle||' ne peut etre depasse pour le code '||my_cm_code);
           END IF;
        END IF;
   END;

   PROCEDURE marche (
       a_att_ordre      ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
       a_fou_ordre        ENGAGE_BUDGET.fou_ordre%TYPE,
       a_exe_ordre        ENGAGE_CTRL_MARCHE.exe_ordre%TYPE
   ) IS
       my_nb               INTEGER;
       my_att_execution    v_attribution_execution.aee_execution%TYPE;
       my_emar_ht_reste    ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_emar_ht_reste_restreint  ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_dmar_ht_saisie   DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_montant_consomme ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_att_ht           jefy_marches.attribution.att_ht%TYPE;
       my_lot_ordre           jefy_marches.lot.lot_ordre%TYPE;
       my_st_montant       jefy_marches.sous_traitant.st_montant%TYPE;
       my_montant           ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        select count(*) into my_nb from jefy_marches.attribution where att_ordre=a_att_ordre;
        if my_nb<>1 then
           RAISE_APPLICATION_ERROR(-20001,'L''attribution n''existe pas (att_ordre='||a_att_ordre||')');
        end if;

-- DEBUT MODIF THIERRY 17/07/2009
        -- on verifie le total de la conso du lot
        select lot_ordre into my_lot_ordre from jefy_marches.attribution where att_ordre=a_att_ordre;

        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution FROM v_attribution_execution
           WHERE att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND exe_ordre<a_exe_ordre;

        SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

        SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;

        SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
		 WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

        if my_att_ht<my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie then
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     to_char(my_att_ht)||' et avec cette commande la consommation serait de '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie));
        end if;

        -- Rod : on tient compte des restes engagés sur exercice N-1 tant que l''exercice n'est pas cloturé
        SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste_restreint FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e, jefy_admin.exercice ex
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND m.exe_ordre<a_exe_ordre AND e.eng_id=m.eng_id
           and e.exe_ordre=ex.exe_ordre and ex.EXE_STAT='R' ;

        if my_att_ht<my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie+my_emar_ht_reste_restreint then
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     to_char(my_att_ht)||' et avec cette commande la consommation (incluant les restes engagés sur l''exercice '|| (a_exe_ordre-1) ||') serait de '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie+my_emar_ht_reste_restreint));
        end if;

-- FIN MODIF THIERRY 17/07/2009


        -- verifier le dispo pour ce lot/attribution.
        -- recuperation de l'execution de l'attribution pour les annees anterieures --
        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
           FROM v_attribution_execution
           -- EGE 07/02/2007 where att_ordre=a_att_ordre and fou_ordre=a_fou_ordre and exe_ordre<=a_exe_ordre;
           WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre AND exe_ordre<a_exe_ordre;

           SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

           SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;


        my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;

        SELECT COUNT(*) INTO my_nb FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;
        IF (my_nb>0) THEN
	 	   SELECT NVL(att_ht,0), lot_ordre INTO my_att_ht, my_lot_ordre
		       FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF my_att_ht=0 THEN
		      SELECT COUNT(*) INTO my_nb FROM jefy_marches.lot
			     WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

		      IF my_nb=1 THEN
		      	 SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
			        WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

                 -- on recherche les executions de ttes les attributions du lot
                 SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
                    FROM v_attribution_execution
		            WHERE att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND exe_ordre<a_exe_ordre;

   		         SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

   		         SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
		            FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND (m.dmar_ht_saisie>0 OR m.dep_id IN
		              (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
		              AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;

		         my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;

			  END IF;

		   END IF;

	       -- enlevons la somme allou¿e au sous-traitants
		   SELECT NVL(SUM(st_montant),0) INTO my_st_montant FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre;

		   my_montant:=grhum.En_Nombre(my_att_ht)-grhum.En_Nombre(my_st_montant);
		   IF my_montant<my_montant_consomme THEN
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||' et avec cette commande la consommation serait de '||my_montant_consomme);
		    /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||', et on a deja consomme '||my_montant_consomme);*/
		   END IF;
	    ELSE
	 	   -- le fournisseur est un sous-traitant, ou alors l'attribution n'existe pas
	 	   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF (my_nb>0) THEN
		 	  SELECT st_montant INTO my_st_montant FROM jefy_marches.sous_traitant
			     WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

			  IF (my_st_montant<my_montant_consomme) THEN
                 RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||' et avec cette commande la consommation serait de '||my_montant_consomme);
  		         /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||', et on a deja consomme '||my_montant_consomme);*/
		      END IF;
           else
  		         RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur ne peut etre utilise pour cette attribution');
	       END IF;
        END IF;
     END;

    procedure analytique (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_can_id 		ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
    ) is
      my_code_analytique jefy_admin.code_analytique%rowtype;
      my_nb              integer;
      my_montant         V_CODE_ANALYTIQUE_SUIVI.montant_budgetaire%type;
    begin
      select count(*) into my_nb from jefy_admin.code_analytique where can_id=a_can_id;
      if my_nb!=1 then
         RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
      end if;

      select * into my_code_analytique from jefy_admin.code_analytique where can_id=a_can_id;
      if my_code_analytique.can_montant is not null and my_code_analytique.can_montant_depassement=15 then
         select nvl(montant_budgetaire,0) into my_montant from V_CODE_ANALYTIQUE_SUIVI where can_id=a_can_id;
         if my_montant>my_code_analytique.can_montant then
            RAISE_APPLICATION_ERROR(-20001, 'Le montant max pour ce code analytique est atteint  ('||INDICATION_ERREUR.analytique(a_can_id)||
              ', '||my_montant||'>'||my_code_analytique.can_montant||')');
         end if;
      end if;
    end;

	PROCEDURE convention(
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_conv_ordre		ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE,
	   a_tcd_ordre      ENGAGE_BUDGET.tcd_ordre%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
    )
	 IS
	 BEGIN
	 	  accords.suivi_exec_depense.verif_disponible(a_exe_ordre, a_conv_ordre, a_tcd_ordre, a_org_id);
	 END;

	 PROCEDURE planco (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_pco_num		ENGAGE_CTRL_PLANCO.pco_num%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
	 )
	 IS
	   my_message   VARCHAR2(1000);
	 BEGIN
	     jefy_budget.budget_execution.verif_depense_nature(a_exe_ordre, a_org_id, a_pco_num, 0, my_message);
	 END;

 END;
/


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
     newCommRef             varchar2(500);
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI';
                      -- Ajout du vlco_id max --
                      --AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                        --    WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;

         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then

            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
              newCommRef := my_org_ub||'/'||my_org_cr;
              if (length(newCommRef)>=44) then
                newCommRef := substr(newCommRef,1,44);
              end if;
              newCommRef := newCommRef ||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF');


            update commande set comm_reference=newCommRef
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE corriger_engage_extourne (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      my_nb integer;
   begin
      my_nb:=0;
   end;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id;
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;

        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
   /*
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id ;
     */
     /* Correctif V1 : probleme contexte montant budgetaire;
           CURSOR liste  IS SELECT can_id, SUM(dc.dana_montant_budgetaire)
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id ;
	 */
	 /* Correctif V2 : Il faut prendre en compte : le contexte de l'engagement, le fait que les depenses analytiques ne font pas nécessairement 100% */
	 CURSOR listeCodeAnalytique IS
	   SELECT can_id
	     FROM DEPENSE_CTRL_ANALYTIQUE dc JOIN DEPENSE_BUDGET d ON dc.dep_id=d.dep_id
	     JOIN ENGAGE_BUDGET e ON e.eng_id=d.eng_id
	    WHERE e.eng_id=a_eng_id
        GROUP BY dc.can_id
        ORDER BY dc.can_id ASC;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
                         AND eana_montant_budgetaire_reste > 0
                    ORDER BY can_id ASC;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_depBudgetCtxEng	              DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
     my_danaCtxEng_somme			  DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
     my_cpt_engAnalytiqueAvecReste    NUMBER;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id /*AND eana_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        /* Version originale sans rattrapage */
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        /* Correctif V1 :
        SELECT nvl(SUM(dc.dana_montant_budgetaire),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
        */
        /* Correctif V2 : Il faut prendre en compte : le contexte de l'engagement + le fait que les depenses analytiques ne font pas nécessairement 100% */
		-- Il faut calculer le montant budgetaire des depenses (ctx Engagement)
		SELECT NVL(SUM(Budget.calculer_budgetaire(d.exe_ordre, e.tap_id, e.org_id, d.dep_ht_saisie, d.dep_ttc_saisie)), 0)
		 INTO my_depBudgetCtxEng
         FROM ENGAGE_BUDGET e JOIN DEPENSE_BUDGET d ON e.eng_id = d.eng_id
        WHERE e.eng_id = a_eng_id AND d.dep_montant_budgetaire > 0;

	    -- on verifie le sum(montant budgetaire analytique) avec le montant budgetaire total ; on borne si besoin
	    IF my_depBudgetCtxEng <= my_dana_bud THEN
	        my_dana_bud := my_depBudgetCtxEng;
	        -- dbms_output.put_line('Rattrapage réalisé niveau global (new) : ' || my_dana_bud);
	    END IF;

        if my_eana_reste > my_reste + my_dana_bud then

           --SYS.dbms_output.put_line('eana_reste / my_reste / dana_bud : ' || my_eana_reste || ' / ' || my_reste || ' / ' || my_dana_bud);
           my_somme:=my_eana_reste-my_reste-my_dana_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          --SYS.dbms_output.put_line('eana_reste maj : ' || my_eana_reste);

        end if;

		-- raz de la somme
		my_danaCtxEng_somme := 0;

        dbms_output.put_line('my_depBudgetCtxEngl : ' || my_depBudgetCtxEng);

        OPEN listeCodeAnalytique();
          LOOP
           FETCH listeCodeAnalytique INTO my_depense_can_id;
           EXIT WHEN listeCodeAnalytique%NOTFOUND;

			-- SYS.dbms_output.put_line('mt bud depense / canal : ' || my_montant_budgetaire || ' / ' || my_depense_can_id);

			-- on calcule le montant budgetaire par code analytique dans le contexte de l'engagement
			SELECT NVL(SUM(Budget.calculer_budgetaire(dc.exe_ordre, e.tap_id, e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)), 0)
			  INTO my_montant_budgetaire
              FROM ENGAGE_BUDGET e JOIN DEPENSE_BUDGET d ON e.eng_id = d.eng_id
              JOIN DEPENSE_CTRL_ANALYTIQUE dc ON d.dep_id = dc.dep_id
             WHERE e.eng_id = a_eng_id AND dep_montant_budgetaire > 0 AND dc.can_id = my_depense_can_id;

            my_danaCtxEng_somme := my_danaCtxEng_somme + my_montant_budgetaire;
            dbms_output.put_line('1. my_montant_budgetaire / my_depBudgetCtxEng / my_danaCtxEng_somme : ' || my_montant_budgetaire || ' / ' || my_depBudgetCtxEng || ' / ' || my_danaCtxEng_somme);

            -- on verifie le montant budgetaire en cours de cumul avec le montant budgetaire total
            IF my_depBudgetCtxEng <= my_danaCtxEng_somme THEN
                my_montant_budgetaire := my_montant_budgetaire + (my_depBudgetCtxEng - my_danaCtxEng_somme);
                dbms_output.put_line('1bis. Rattrapage réalisé sur codeAnalytique (new) : ' || my_montant_budgetaire);
            END IF;

           	SELECT COUNT(*)
           	  INTO my_nb
           	  FROM ENGAGE_CTRL_ANALYTIQUE
             WHERE eng_id = a_eng_id AND can_id = my_depense_can_id;

           -- s'il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
              SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 --SYS.dbms_output.put_line('MAJ Engage Ctrl Analytique');

                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                 my_montant_budgetaire := 0;
                 my_eana_montant_bud_reste := 0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                my_somme:=0;

	            SELECT COUNT(*)
	              INTO my_cpt_engAnalytiqueAvecReste
	              FROM ENGAGE_CTRL_ANALYTIQUE
	             WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_cpt_engAnalytiqueAvecReste THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE listeCodeAnalytique;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

           SELECT COUNT(*)
             INTO my_cpt_engAnalytiqueAvecReste
             FROM ENGAGE_CTRL_ANALYTIQUE
            WHERE eng_id=a_eng_id
              AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                 IF liste2%rowcount=my_cpt_engAnalytiqueAvecReste THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

				-- FLA : upd_engage_montant doit etre buggué ou plutot le booleen my_dernier !
				-- Visiblement on soustrait tout le montant du reste de l'engagement analytique.
				-- FLA : on peut donc avoir des restes négatifs je pense.
                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;


                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id /*AND econ_montant_budgetaire_reste>0*/;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;

              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;

        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;

        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;

        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;

           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;

              if my_somme>0 then
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id;
                 end if;
              end if;

           END LOOP;
           CLOSE liste2;

          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;

        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decal¿ un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then
                 my_pourcentage:=0;
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/
create or replace procedure grhum.ins_patch_2108 is
begin

    insert into jefy_depense.db_version values (2108,'2108',sysdate,sysdate,null);
    commit;
end;

