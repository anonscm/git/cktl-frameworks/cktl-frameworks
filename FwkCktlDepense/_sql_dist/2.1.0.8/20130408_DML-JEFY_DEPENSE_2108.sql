--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.8
-- Date de publication : 08/04/2013
-- Licence : CeCILL version 2
--
--

----------------------------------------------
--
--
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

	execute grhum.ins_patch_2108;

commit;

drop procedure grhum.ins_patch_2108;
