SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_DEPENSE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  2.1.0.8
-- Date de publication :
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Controle de depassement des marches : ajout des restes à engagés sur l'execice N-1 (tant qu'il n'est pas cloturé) dans les montants controlés.
-- DT4424 - Correction du problème d'arrondi lié à la répartition par code analytique
----------------------------------------------



whenever sqlerror exit sql.sqlcode ;

declare
cpt integer;
begin
    select count(*) into cpt from jefy_depense.db_version where db_version_libelle='2107';
    if cpt = 0 then
        raise_application_error(-20000,'Le user jefy_depense n''est pas à jour pour passer ce patch !');
    end if;
end;
/


CREATE OR REPLACE FUNCTION jefy_depense.IS_CODE_ANALYTIQUE_OBLIGATOIRE (p_organigrammeId jefy_admin.organ.org_id%TYPE) RETURN BOOLEAN
IS
	canalObligatoirePourOrg       	jefy_admin.organ.org_canal_obligatoire%TYPE;
	typeEtatOui											jefy_admin.type_etat.tyet_id%TYPE;
	typeEtatDepenses								jefy_admin.type_etat.tyet_id%TYPE;
	isObligatoire										boolean;

	BEGIN

		select org_canal_obligatoire into canalObligatoirePourOrg from jefy_admin.organ where org_id = p_organigrammeId;
		select tyet_id into typeEtatOui from jefy_admin.type_etat where tyet_libelle = 'OUI';
		select tyet_id into typeEtatDepenses from jefy_admin.type_etat where tyet_libelle = 'DEPENSES';

		if canalObligatoirePourOrg is not null and (canalObligatoirePourOrg = typeEtatOui or canalObligatoirePourOrg = typeEtatDepenses) then
		  isObligatoire := TRUE;
		else
      isObligatoire := FALSE;
		end if;

		RETURN isObligatoire;
	END;
	/


