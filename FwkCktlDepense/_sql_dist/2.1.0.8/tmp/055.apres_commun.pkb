


CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Apres_Commun IS

   PROCEDURE hors_marche (
      a_typa_id         ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre        ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre       ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
      my_exe_ordre      ENGAGE_CTRL_HORS_MARCHE.exe_ordre%TYPE;
      my_ehom_ht_reste  ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
      my_dhom_ht_saisie DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
      my_ce_controle    jefy_marches.code_exer.ce_controle%TYPE;
      my_cm_code        jefy_marches.code_marche.cm_code%TYPE;
   BEGIN
        SELECT NVL(ce_controle,0), m.cm_code, e.exe_ordre INTO my_ce_controle, my_cm_code, my_exe_ordre
          FROM jefy_marches.code_exer e, jefy_marches.code_marche m WHERE ce_ordre=a_ce_ordre AND e.cm_ordre=m.cm_ordre;

          -- ajout sans achat
        IF a_typa_id<>Get_Type_Achat('MONOPOLE') AND a_typa_id<>Get_Type_Achat('3CMP')
            AND a_typa_id<>Get_Type_Achat('SANS ACHAT') THEN
            jefy_marches.creerCodeMarcheFour(a_fou_ordre, a_ce_ordre, 'jefy_depense');

              SELECT NVL(SUM(ehom_ht_reste),0) INTO my_ehom_ht_reste FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre AND typa_id=a_typa_id;
              SELECT NVL(SUM(dhom_ht_saisie),0) INTO my_dhom_ht_saisie FROM DEPENSE_CTRL_HORS_MARCHE
              WHERE ce_ordre=a_ce_ordre  AND typa_id=a_typa_id AND (dhom_ht_saisie>0 OR dep_id IN
                (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')));

            IF my_ce_controle=0 THEN
              my_ce_controle:=150000;
           END IF;

           IF my_ehom_ht_reste+my_dhom_ht_saisie>my_ce_controle THEN
              RAISE_APPLICATION_ERROR(-20001,'Le seuil '||my_ce_controle||' ne peut etre depasse pour le code '||my_cm_code);
           END IF;
        END IF;
   END;

   PROCEDURE marche (
       a_att_ordre      ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
       a_fou_ordre        ENGAGE_BUDGET.fou_ordre%TYPE,
       a_exe_ordre        ENGAGE_CTRL_MARCHE.exe_ordre%TYPE
   ) IS
       my_nb               INTEGER;
       my_att_execution    v_attribution_execution.aee_execution%TYPE;
       my_emar_ht_reste    ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_emar_ht_reste_restreint  ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_dmar_ht_saisie   DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_montant_consomme ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
       my_att_ht           jefy_marches.attribution.att_ht%TYPE;
       my_lot_ordre           jefy_marches.lot.lot_ordre%TYPE;
       my_st_montant       jefy_marches.sous_traitant.st_montant%TYPE;
       my_montant           ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        select count(*) into my_nb from jefy_marches.attribution where att_ordre=a_att_ordre;
        if my_nb<>1 then
           RAISE_APPLICATION_ERROR(-20001,'L''attribution n''existe pas (att_ordre='||a_att_ordre||')');
        end if;

-- DEBUT MODIF THIERRY 17/07/2009
        -- on verifie le total de la conso du lot
        select lot_ordre into my_lot_ordre from jefy_marches.attribution where att_ordre=a_att_ordre;
        
        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution FROM v_attribution_execution
           WHERE att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND exe_ordre<a_exe_ordre;

        SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;
           
        SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;
        
        SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
		 WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

        if my_att_ht<my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie then
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     to_char(my_att_ht)||' et avec cette commande la consommation serait de '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie)); 
        end if;
        
        -- Rod : on tient compte des restes engagés sur exercice N-1 tant que l''exercice n'est pas cloturé
        SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste_restreint FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e, jefy_admin.exercice ex
           WHERE m.att_ordre in (select att_ordre from jefy_marches.attribution where lot_ordre=my_lot_ordre) AND m.exe_ordre<a_exe_ordre AND e.eng_id=m.eng_id
           and e.exe_ordre=ex.exe_ordre and ex.EXE_STAT='R' ;        
        
        if my_att_ht<my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie+my_emar_ht_reste_restreint then
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     to_char(my_att_ht)||' et avec cette commande la consommation (incluant les restes engagés sur l''exercice '|| (a_exe_ordre-1) ||') serait de '||to_char(my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie+my_emar_ht_reste_restreint)); 
        end if;

-- FIN MODIF THIERRY 17/07/2009

         
        -- verifier le dispo pour ce lot/attribution.
        -- recuperation de l'execution de l'attribution pour les annees anterieures --
        SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
           FROM v_attribution_execution
           -- EGE 07/02/2007 where att_ordre=a_att_ordre and fou_ordre=a_fou_ordre and exe_ordre<=a_exe_ordre;
           WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre AND exe_ordre<a_exe_ordre;

           SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

           SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
           FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
           WHERE m.att_ordre=a_att_ordre AND (m.dmar_ht_saisie>0 OR m.dep_id IN
           (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
           AND e.fou_ordre=a_fou_ordre AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;


        my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;

        SELECT COUNT(*) INTO my_nb FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;
        IF (my_nb>0) THEN
	 	   SELECT NVL(att_ht,0), lot_ordre INTO my_att_ht, my_lot_ordre
		       FROM jefy_marches.attribution WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF my_att_ht=0 THEN
		      SELECT COUNT(*) INTO my_nb FROM jefy_marches.lot
			     WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;

		      IF my_nb=1 THEN
		      	 SELECT NVL(lot_ht,0) INTO my_att_ht FROM jefy_marches.lot
			        WHERE lot_ordre=my_lot_ordre AND NVL(lot_ht,0)<>0;
                    
                 -- on recherche les executions de ttes les attributions du lot
                 SELECT NVL(SUM(aee_execution),0) INTO my_att_execution
                    FROM v_attribution_execution
		            WHERE att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND exe_ordre<a_exe_ordre;

   		         SELECT NVL(SUM(m.emar_ht_reste),0) INTO my_emar_ht_reste FROM ENGAGE_CTRL_MARCHE m, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND m.exe_ordre=a_exe_ordre AND e.eng_id=m.eng_id;

   		         SELECT NVL(SUM(m.dmar_ht_saisie),0) INTO my_dmar_ht_saisie
		            FROM DEPENSE_CTRL_MARCHE m, DEPENSE_BUDGET d, ENGAGE_BUDGET e
		            WHERE m.att_ordre in (select att_ordre from v_attribution where lot_ordre=my_lot_ordre)
                    AND (m.dmar_ht_saisie>0 OR m.dep_id IN
		              (SELECT p.dep_id FROM DEPENSE_CTRL_PLANCO p, maracuja.mandat m WHERE p.man_id=m.man_id AND m.man_etat IN ('PAYE','VISE')))
		              AND m.exe_ordre=a_exe_ordre AND m.dep_id=d.dep_id AND d.eng_id=e.eng_id;

		         my_montant_consomme:=my_att_execution+my_emar_ht_reste+my_dmar_ht_saisie;                    
                    
			  END IF;

		   END IF;

	       -- enlevons la somme allou¿e au sous-traitants
		   SELECT NVL(SUM(st_montant),0) INTO my_st_montant FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre;

		   my_montant:=grhum.En_Nombre(my_att_ht)-grhum.En_Nombre(my_st_montant);
		   IF my_montant<my_montant_consomme THEN
            RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||' et avec cette commande la consommation serait de '||my_montant_consomme); 
		    /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			     my_montant||', et on a deja consomme '||my_montant_consomme);*/
		   END IF;
	    ELSE
	 	   -- le fournisseur est un sous-traitant, ou alors l'attribution n'existe pas
	 	   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sous_traitant WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

		   IF (my_nb>0) THEN
		 	  SELECT st_montant INTO my_st_montant FROM jefy_marches.sous_traitant
			     WHERE att_ordre=a_att_ordre AND fou_ordre=a_fou_ordre;

			  IF (my_st_montant<my_montant_consomme) THEN
                 RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||' et avec cette commande la consommation serait de '||my_montant_consomme); 
  		         /*RAISE_APPLICATION_ERROR(-20001,'Le montant de l''attribution pour ce fournisseur est de '||
			        my_st_montant||', et on a deja consomme '||my_montant_consomme);*/
		      END IF;
           else
  		         RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur ne peut etre utilise pour cette attribution');           
	       END IF;
        END IF;
     END;

    procedure analytique (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_can_id 		ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
    ) is
      my_code_analytique jefy_admin.code_analytique%rowtype;
      my_nb              integer;
      my_montant         V_CODE_ANALYTIQUE_SUIVI.montant_budgetaire%type;
    begin
      select count(*) into my_nb from jefy_admin.code_analytique where can_id=a_can_id;
      if my_nb!=1 then
         RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
      end if;
      
      select * into my_code_analytique from jefy_admin.code_analytique where can_id=a_can_id;
      if my_code_analytique.can_montant is not null and my_code_analytique.can_montant_depassement=15 then
         select nvl(montant_budgetaire,0) into my_montant from V_CODE_ANALYTIQUE_SUIVI where can_id=a_can_id;
         if my_montant>my_code_analytique.can_montant then
            RAISE_APPLICATION_ERROR(-20001, 'Le montant max pour ce code analytique est atteint  ('||INDICATION_ERREUR.analytique(a_can_id)||
              ', '||my_montant||'>'||my_code_analytique.can_montant||')');
         end if;
      end if;
    end;

	PROCEDURE convention(
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_conv_ordre		ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE,
	   a_tcd_ordre      ENGAGE_BUDGET.tcd_ordre%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
    )
	 IS
	 BEGIN
	 	  accords.suivi_exec_depense.verif_disponible(a_exe_ordre, a_conv_ordre, a_tcd_ordre, a_org_id);
	 END;
	 
	 PROCEDURE planco (
	   a_exe_ordre      ENGAGE_BUDGET.exe_ordre%TYPE,
	   a_pco_num		ENGAGE_CTRL_PLANCO.pco_num%TYPE,
	   a_org_id      ENGAGE_BUDGET.org_id%TYPE
	 )
	 IS
	   my_message   VARCHAR2(1000);
	 BEGIN
	     jefy_budget.budget_execution.verif_depense_nature(a_exe_ordre, a_org_id, a_pco_num, 0, my_message);
	 END;

 END;
/


