connect grhum/<mot de passe>;

begin
 JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 210, 'REIMP', 'Depense', 'Reimputation d''une liquidation (hors periode de blocage)', 'Reimputation (hors periode de blocage)', 'N', 'O', 3 );
 JEFY_ADMIN.API_APPLICATION.CREERFONCTION ( 211, 'REIMPINV', 'Depense', 'Reimputation d''une liquidation (pendant periode de blocage)', 'Reimputation (pendant la periode de blocage)', 'N', 'O', 3 );
 commit;
end;



CREATE OR REPLACE VIEW JEFY_DEPENSE.V_Catalogue_article
(caar_id, ATT_ORDRE, caar_REFerence, caar_Libelle, cat_id, cat_libelle, caar_PRIX_HT, 
 caar_PRIX_TTC, TVA_ID, CM_ORDRE, TYET_ID, caar_id_PERE, TYAR_ID, fou_ordre, cat_date_debut, cat_date_fin, tyap_id)
AS 
select ca.caar_id, ac.att_ordre, ca.caar_reference, a.art_libelle, c.cat_id, c.cat_libelle, ca.caar_prix_ht, ca.caar_prix_ttc, ca.tva_id,
  nvl(a.cm_ordre,c.cm_ordre), decode(c.tyet_id,1,ca.tyet_id,c.tyet_id), capere.caar_id, a.tyar_id, c.fou_ordre,c.cat_DATE_DEBUT, c.cat_date_fin, c.tyap_id
from jefy_marches.attribution_catalogue ac, jefy_catalogue.catalogue c,
     jefy_catalogue.catalogue_article ca, jefy_catalogue.article a,
     jefy_catalogue.catalogue_article capere, jefy_catalogue.article apere
where a.art_id=ca.art_id and ca.cat_id=c.cat_id
 and a.art_id_pere=apere.art_id(+) and apere.art_id=capere.art_id(+) and c.cat_id=ac.cat_id(+)
 and (tyap_id=get_type_application or tyap_id=get_type_application_pi or ac.att_ordre is not null);


grant select, references on courrier.courrier to jefy_depense;
grant select, references on courrier.reference_lien to jefy_depense;

create table jefy_depense.type_document (
  tcom_id      number not null,
  tcom_libelle varchar2(50) not null) 
LOGGING NOCACHE NOPARALLEL;

CREATE UNIQUE INDEX jefy_depense.PK_type_document ON jefy_depense.type_document (tcom_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.type_document ADD (CONSTRAINT PK_type_document PRIMARY KEY (tcom_id));

insert into jefy_depense.type_document values (1, 'Devis');
insert into jefy_depense.type_document values (2, 'Facture');
insert into jefy_depense.type_document values (3, 'Bon de commande');
insert into jefy_depense.type_document values (4, 'Documentations diverses');

create sequence jefy_depense.type_document_SEQ start with 5 nocycle nocache;

CREATE OR REPLACE FUNCTION JEFY_DEPENSE.get_type_document (a_tcom_libelle jefy_depense.type_document.tcom_libelle%type)
     return jefy_depense.type_document.tcom_id%type
   is
   	 my_nb        integer;
	 my_tcom_id   jefy_depense.type_document.tcom_id%type;
BEGIN
   	select count(*) into my_nb from jefy_depense.type_document where tcom_libelle=a_tcom_libelle;

	if my_nb=1 then
           select tcom_id into my_tcom_id from jefy_depense.type_document where tcom_libelle=a_tcom_libelle;
	   return my_tcom_id;
	end if;

        return null;
END;
/

create table jefy_depense.commande_document (
  comd_id    number not null,
  comm_id    number not null,
  tcom_id    number not null, 
  utl_ordre  number not null,
  comd_date  date   not null,
  cimp_id    number,
  cou_numero number,
  comd_url   varchar2(200))
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.commande_document is 'table des documents associes a la commande';
COMMENT ON COLUMN jefy_depense.commande_document.comd_id is 'Cle';
COMMENT ON COLUMN jefy_depense.commande_document.comm_id is 'Cle etrangere table jefy_depense.commande';
COMMENT ON COLUMN jefy_depense.commande_document.tcom_id is 'Cle etrangere table jefy_depense.type_document';
COMMENT ON COLUMN jefy_depense.commande_document.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_depense.commande_document.comd_date is 'date';
COMMENT ON COLUMN jefy_depense.commande_document.cimp_id is 'Cle etrangere table jefy_depense.commande_impression';
COMMENT ON COLUMN jefy_depense.commande_document.cou_numero is 'Cle etrangere table courrier.courrier';
COMMENT ON COLUMN jefy_depense.commande_document.comd_url is 'URL du document';

CREATE UNIQUE INDEX jefy_depense.PK_commande_document ON jefy_depense.commande_document (comd_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.commande_document ADD (CONSTRAINT PK_commande_document PRIMARY KEY (comd_id));

ALTER TABLE jefy_depense.commande_document ADD ( CONSTRAINT FK_commande_document_comm_id FOREIGN KEY (comm_id) 
    REFERENCES jefy_depense.commande(comm_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.commande_document ADD ( CONSTRAINT FK_commande_document_cimp_id FOREIGN KEY (cimp_id) 
    REFERENCES jefy_depense.commande_impression(cimp_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.commande_document ADD ( CONSTRAINT FK_commande_document_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur(utl_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.commande_document ADD ( CONSTRAINT FK_commande_document_cou_num FOREIGN KEY (cou_numero) 
    REFERENCES courrier.courrier(cou_numero) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.commande_document_SEQ start with 1 nocycle nocache;

CREATE OR REPLACE PACKAGE JEFY_DEPENSE.documenter  IS
   PROCEDURE ins_commande_document (
     a_comd_id        in out     commande_document.comd_id%type,
 	 a_comm_id           		 commande_document.comm_id%type,
 	 a_tcom_id                   commande_document.tcom_id%type,
 	 a_utl_ordre                 commande_document.utl_ordre%type,
	 a_cimp_id                   commande_document.cimp_id%type,
	 a_cou_numero                commande_document.cou_numero%type,
	 a_comd_url                  commande_document.comd_url%type);

   PROCEDURE del_commande_document (
     a_comd_id                   commande_document.comd_id%type,
     a_utl_ordre                 commande_document.utl_ordre%type);
END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.documenter
IS

   PROCEDURE ins_commande_document (
     a_comd_id        in out     commande_document.comd_id%type,
 	 a_comm_id           		 commande_document.comm_id%type,
 	 a_tcom_id                   commande_document.tcom_id%type,
 	 a_utl_ordre                 commande_document.utl_ordre%type,
	 a_cimp_id                   commande_document.cimp_id%type,
	 a_cou_numero                commande_document.cou_numero%type,
	 a_comd_url                  commande_document.comd_url%type)
   is
   BEGIN
	    if a_comd_id is null then
	       select commande_document_seq.nextval into a_comd_id from dual;
	    end if;

        if a_tcom_id=get_type_document('Bon de commande') and a_cimp_id is null then
		   RAISE_APPLICATION_ERROR(-20001, 'Pour document de type "Bon de commande", il faut associer une commande_impression');
        end if;

   		insert into commande_document values (a_comd_id, a_comm_id, a_tcom_id, a_utl_ordre, sysdate, a_cimp_id, a_cou_numero, a_comd_url);
   end;

   PROCEDURE del_commande_document (
     a_comd_id                   commande_document.comd_id%type,
     a_utl_ordre                 commande_document.utl_ordre%type)
   is
     my_nb        integer;
   BEGIN
     select count(*) into my_nb from jefy_depense.commande_document where comd_id=a_comd_id;
     if my_nb=0 then
        RAISE_APPLICATION_ERROR(-20001, 'Le document (comd_id='||a_comd_id||') n''existe pas');
     end if;
     
   END;

END;
/





CREATE OR REPLACE PACKAGE JEFY_DEPENSE.Corriger  IS

-- procedure de correction entre les commandes et ses engagements.
-- peut etre a prevoir dans interface ??

-- correction entre engage_budget et les engage_ctrl_...

PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE);

PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE);

PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE);

FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
	  a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
	  a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
	  a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
  	  a_dernier				BOOLEAN,
	  a_exe_ordre			ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;

PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE);

FUNCTION montant_correction_ctrl (
     a_somme_pourcentage	COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
	 a_montant_reste		COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
	 a_pourcentage			COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
	 a_montant				COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
	 a_reste				BOOLEAN)
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Corriger
IS

   PROCEDURE corriger_etats_commande (
      a_comm_id IN        COMMANDE.comm_id%TYPE)
   IS
     my_nb INTEGER;
     my_comm_reference     commande.comm_reference%type;
     my_comm_numero        commande.comm_numero%type;
     my_exe_ordre          commande.exe_ordre%type;
     my_eng_id             engage_budget.eng_id%type;
     my_org_ub             v_organ.org_ub%type;
     my_org_cr             v_organ.org_cr%type;
     my_eng_ht_saisie      ENGAGE_BUDGET.eng_ht_saisie%TYPE;
     my_eng_ttc_saisie     ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_eng_montant        ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_montant_reste  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_art_prix_total_ht  ARTICLE.art_prix_total_ht%TYPE;
     my_art_prix_total_ttc ARTICLE.art_prix_total_ttc%TYPE;
     my_tyet_id_imprimable COMMANDE.tyet_id_imprimable%TYPE;
   BEGIN
      SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;

      -- si il n'y a pas d'engagement c'est une precommande.
      IF my_nb=0 THEN
         UPDATE COMMANDE SET tyet_id=Etats.get_etat_precommande,
            tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
      ELSE
         SELECT SUM(eng_ht_saisie), SUM(eng_ttc_saisie), SUM(eng_montant_budgetaire),
             SUM(eng_montant_budgetaire_reste)
           INTO my_eng_ht_saisie, my_eng_ttc_saisie, my_eng_montant, my_eng_montant_reste
           FROM ENGAGE_BUDGET e, COMMANDE_ENGAGEMENT ce
           WHERE ce.comm_id=a_comm_id AND ce.eng_id=e.eng_id;

         SELECT SUM(art_prix_total_ht), SUM(art_prix_total_ttc) INTO my_art_prix_total_ht, my_art_prix_total_ttc
           FROM ARTICLE WHERE comm_id=a_comm_id;

         -- si le montant engage est inferieur au montant commande c'est partiellement engage.
         IF my_eng_ht_saisie<my_art_prix_total_ht OR my_eng_ttc_saisie<my_art_prix_total_ttc THEN
             UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_engagee,
               tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
         ELSE
            -- si le reste engage est egal au montant engage c'est une commande engagee.
            IF my_eng_montant=my_eng_montant_reste THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_engagee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le montant engage est superieur au reste c'est partiellement solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste>0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_part_soldee,
                  tyet_id_imprimable=Etats.get_etat_imprimable WHERE comm_id=a_comm_id;
            END IF;

            -- si le reste est a 0 c'est solde.
            IF my_eng_montant>my_eng_montant_reste AND my_eng_montant_reste=0 THEN
                UPDATE COMMANDE SET tyet_id=Etats.get_etat_soldee,
                 tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
            END IF;

             -- on verifie dans les services validations pour savoir si la commande est imprimable.
            SELECT tyet_id_imprimable INTO my_tyet_id_imprimable FROM COMMANDE WHERE comm_id=a_comm_id;
            IF my_tyet_id_imprimable=Etats.get_etat_imprimable THEN

                SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande WHERE vlco_comid=a_comm_id
                     AND vlco_valide ='OUI';
                IF my_nb>0 THEN

                   SELECT COUNT(*) INTO my_nb FROM jefy_marches.sa_validation_commande
                      WHERE vlco_comid=a_comm_id AND vlco_etat<>'ACCEPTEE' AND vlco_valide ='OUI'
                      -- Ajout du vlco_id max --
                      AND vlco_id = (SELECT MAX(vlco_id) FROM jefy_marches.sa_validation_commande
                            WHERE vlco_comid=a_comm_id AND vlco_valide = 'OUI');

                   IF my_nb>0 THEN
                        UPDATE COMMANDE SET tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
                   END IF;

                END IF;
            END IF;

         END IF;
         
         -- reference automatique.
         SELECT comm_numero, comm_reference, exe_ordre INTO my_comm_numero, my_comm_reference, my_exe_ordre
            FROM COMMANDE WHERE comm_id=a_comm_id;
         if to_char(my_comm_numero)=my_comm_reference then
            select min(eng_id) into my_eng_id from commande_engagement where comm_id=a_comm_id;
            select o.org_ub, o.org_cr into my_org_ub, my_org_cr from v_organ o, engage_budget e
              where o.org_id=e.org_id and e.eng_id=my_eng_id;
            update commande set comm_reference=my_org_ub||'/'||my_org_cr||'/'||Get_Numerotation(my_exe_ordre,my_org_ub,my_org_cr,'COMMANDE_REF')
              where comm_id=a_comm_id;
         end if;
      END IF;
   END;

   PROCEDURE corriger_montant_commande_bud (
      a_comm_id               COMMANDE_BUDGET.comm_id%TYPE   ) IS
     CURSOR liste IS SELECT * FROM COMMANDE_BUDGET WHERE comm_id=a_comm_id;

     my_commande_budget                COMMANDE_BUDGET%ROWTYPE;
   BEGIN
        OPEN liste();
          LOOP
           FETCH  liste INTO my_commande_budget;
           EXIT WHEN liste%NOTFOUND;

           corriger_commande_ctrl(my_commande_budget.cbud_id);
        END LOOP;
        CLOSE liste;
   END;

   PROCEDURE upd_engage_reste (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_reste         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_dep_bud       depense_budget.dep_montant_budgetaire%type;
     my_eng_bud       engage_budget.eng_montant_budgetaire%type;
     my_exe_ordre     engage_budget.exe_ordre%type;
     my_org_id        engage_budget.org_id%type;
     my_tcd_ordre     engage_budget.tcd_ordre%type;
   BEGIN
        select nvl(sum(eng_montant_budgetaire_reste),0) into my_reste from engage_budget where eng_id=a_eng_id;

--        select nvl(sum(dep_montant_budgetaire),0) into my_dep_bud from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
--        select nvl(sum(eng_montant_budgetaire),0) into my_eng_bud from engage_budget where eng_id=a_eng_id;
--        my_reste:=my_eng_bud-my_dep_bud;
--        if my_reste<0 then my_reste:=0; end if;

--        select exe_ordre, org_id, tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre from engage_budget where eng_id=a_eng_id; 
--        update engage_budget set eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;
--        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);

        IF my_reste>0 THEN
        --dbms_output.put_line ('debut '||a_eng_id||' '||TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_action(a_eng_id);
        --dbms_output.put_line (TO_CHAR(SYSDATE,'dd/mm/yyyy HH24:MI:SS'));
             upd_engage_reste_analytique(a_eng_id);
             upd_engage_reste_convention(a_eng_id);
             upd_engage_reste_hors_marche(a_eng_id);
             upd_engage_reste_marche(a_eng_id);
             upd_engage_reste_planco(a_eng_id);
        ELSE
          UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
          UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
        END IF;
   END;


   /*********************************************************************************/
   PROCEDURE upd_engage_reste_action (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT tyac_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie))
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY tyac_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
        AND eact_montant_budgetaire_reste>0;

     my_depense_tyac_id               DEPENSE_CTRL_ACTION.tyac_id%TYPE;
     my_engage_ctrl_action            ENGAGE_CTRL_ACTION%ROWTYPE;

     my_eact_montant_bud              ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
     my_eact_montant_bud_reste        ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_eact_id                       ENGAGE_CTRL_ACTION.eact_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     --my_org_id                      ENGAGE_BUDGET.org_id%TYPE;
     --my_tcd_ordre                   ENGAGE_BUDGET.tcd_ordre%TYPE;
     --my_tap_id                      ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ACTION.dact_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eact_reste                    engage_ctrl_action.eact_montant_budgetaire_reste%type;
     my_dact_bud                      depense_ctrl_action.dact_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre INTO my_exe_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eact_montant_budgetaire_reste) into my_eact_reste
          from engage_ctrl_action where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dact_ht_saisie, dc.dact_ttc_saisie)),0)
          into my_dact_bud
         FROM DEPENSE_CTRL_ACTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eact_reste>my_reste+my_dact_bud then
           my_somme:=my_eact_reste-my_reste-my_dact_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_action;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_action.eact_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_action 
                      set eact_montant_budgetaire_reste=my_engage_ctrl_action.eact_montant_budgetaire_reste-my_somme
                      where eact_id=my_engage_ctrl_action.eact_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_action.eact_montant_budgetaire_reste;
                    update engage_ctrl_action set eact_montant_budgetaire_reste=0 where eact_id=my_engage_ctrl_action.eact_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eact_montant_budgetaire_reste) into my_eact_reste
             from engage_ctrl_action where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_tyac_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION
              WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eact_id, eact_montant_budgetaire_reste, eact_montant_budgetaire
                INTO my_eact_id, my_eact_montant_bud_reste, my_eact_montant_bud
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_depense_tyac_id;

              -- cas de la liquidation.
              IF my_eact_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ACTION
                   SET eact_montant_budgetaire_reste=my_eact_montant_bud_reste-my_montant_budgetaire
                   WHERE eact_id=my_eact_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eact_id=my_eact_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eact_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0)
                INTO my_eact_montant_bud, my_eact_montant_bud_reste
                FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND eact_montant_budgetaire_reste>0;

              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>=my_eact_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eact_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id
                         AND eact_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_action;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant := upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_action.eact_montant_budgetaire_reste, my_eact_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=eact_montant_budgetaire_reste-my_montant
                          WHERE eact_id=my_engage_ctrl_action.eact_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;


   /****************************************************************************/
   PROCEDURE upd_engage_reste_analytique (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
      CURSOR liste  IS SELECT can_id, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie))
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY can_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id
        AND eana_montant_budgetaire_reste>0;

     my_depense_can_id                DEPENSE_CTRL_ANALYTIQUE.can_id%TYPE;
     my_engage_ctrl_analytique        ENGAGE_CTRL_ANALYTIQUE%ROWTYPE;

     my_eana_montant_bud              ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
     my_eana_montant_bud_reste        ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eana_id                       ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
     my_exe_ordre                     ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                        ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                     ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                        ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire            DEPENSE_CTRL_ANALYTIQUE.dana_montant_budgetaire%TYPE;
     my_nb                            INTEGER;
     my_somme                         ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste                 ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                       NUMBER;
     my_dernier                       BOOLEAN;
     my_reste                         ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eana_reste                    engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste%type;
     my_dana_bud                      depense_ctrl_ANALYTIQUE.dana_montant_budgetaire%type;
     my_eng                           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(eana_montant_budgetaire_reste) into my_eana_reste
          from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dana_ht_saisie, dc.dana_ttc_saisie)),0)
          into my_dana_bud
         FROM DEPENSE_CTRL_ANALYTIQUE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_eana_reste>my_reste+my_dana_bud then
           my_somme:=my_eana_reste-my_reste-my_dana_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_ANALYTIQUE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_ANALYTIQUE 
                      set eana_montant_budgetaire_reste=my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste-my_somme
                      where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_ANALYTIQUE.eana_montant_budgetaire_reste;
                    update engage_ctrl_ANALYTIQUE set eana_montant_budgetaire_reste=0 where eana_id=my_engage_ctrl_ANALYTIQUE.eana_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(eana_montant_budgetaire_reste) into my_eana_reste
             from engage_ctrl_ANALYTIQUE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_can_id, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE
              WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT eana_id, eana_montant_budgetaire_reste, eana_montant_budgetaire
                INTO my_eana_id, my_eana_montant_bud_reste, my_eana_montant_bud
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_depense_can_id;

              -- cas de la liquidation.
              IF my_eana_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE
                   SET eana_montant_budgetaire_reste=my_eana_montant_bud_reste-my_montant_budgetaire
                   WHERE eana_id=my_eana_id;
              ELSE
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eana_id=my_eana_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_eana_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0)
                INTO my_eana_montant_bud, my_eana_montant_bud_reste
                FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND eana_montant_budgetaire_reste>0;

              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>=my_eana_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_eana_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_analytique;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                          WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(eana_montant_budgetaire_reste),0) INTO my_eana_montant_bud_reste
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;

        IF my_eana_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_eana_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id  AND eana_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_analytique;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_analytique.eana_montant_budgetaire_reste, my_eana_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=eana_montant_budgetaire_reste-my_montant
                    WHERE eana_id=my_engage_ctrl_analytique.eana_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_convention (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT conv_ordre, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie))
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY conv_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id
        AND econ_montant_budgetaire_reste>0;

     my_depense_conv_ordre       DEPENSE_CTRL_CONVENTION.conv_ordre%TYPE;
     my_engage_ctrl_convention   ENGAGE_CTRL_CONVENTION%ROWTYPE;

     my_econ_montant_bud         ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
     my_econ_montant_bud_reste   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_econ_id                  ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_CONVENTION.dcon_montant_budgetaire%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire_reste%TYPE;
     my_eng_bud_reste            ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_montant                  NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_econ_reste               engage_ctrl_CONVENTION.econ_montant_budgetaire_reste%type;
     my_dcon_bud                 depense_ctrl_CONVENTION.dcon_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
   BEGIN
        select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;
        if my_nb=0 then return; end if;

        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(econ_montant_budgetaire_reste) into my_econ_reste
          from engage_ctrl_CONVENTION where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dcon_ht_saisie, dc.dcon_ttc_saisie)),0)
          into my_dcon_bud
         FROM DEPENSE_CTRL_CONVENTION dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_econ_reste>my_reste+my_dcon_bud then
           my_somme:=my_econ_reste-my_reste-my_dcon_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_CONVENTION;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_CONVENTION
                      set econ_montant_budgetaire_reste=my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste-my_somme
                      where econ_id=my_engage_ctrl_CONVENTION.econ_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_CONVENTION.econ_montant_budgetaire_reste;
                    update engage_ctrl_CONVENTION set econ_montant_budgetaire_reste=0 where econ_id=my_engage_ctrl_CONVENTION.econ_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(econ_montant_budgetaire_reste) into my_econ_reste
             from engage_ctrl_CONVENTION where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_conv_ordre, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION
              WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT econ_id, econ_montant_budgetaire_reste, econ_montant_budgetaire
                INTO my_econ_id, my_econ_montant_bud_reste, my_econ_montant_bud
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_depense_conv_ordre;

              -- cas de la liquidation.
              IF my_econ_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_CONVENTION
                   SET econ_montant_budgetaire_reste=my_econ_montant_bud_reste-my_montant_budgetaire
                   WHERE econ_id=my_econ_id;
              ELSE
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE econ_id=my_econ_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_econ_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0)
                INTO my_econ_montant_bud, my_econ_montant_bud_reste
                FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND econ_montant_budgetaire_reste>0;

              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>=my_econ_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_econ_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_convention;
                        EXIT WHEN liste2%NOTFOUND;

                        IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                          WHERE econ_id=my_engage_ctrl_convention.econ_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;

        --- penser a diminuer le reste eng si superieur au eng reste engagement.
        ----   (cf. dep sans code analytique).
        SELECT eng_montant_budgetaire_reste INTO my_eng_bud_reste
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT NVL(SUM(econ_montant_budgetaire_reste),0) INTO my_econ_montant_bud_reste
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_econ_montant_bud_reste>my_eng_bud_reste THEN

           my_montant_budgetaire:=my_econ_montant_bud_reste-my_eng_bud_reste;
           my_somme:=0;

            SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id  AND econ_montant_budgetaire_reste>0;

           OPEN liste2();
             LOOP
                 FETCH liste2 INTO my_engage_ctrl_convention;
                 EXIT WHEN liste2%NOTFOUND;

                  IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                      my_engage_ctrl_convention.econ_montant_budgetaire_reste, my_econ_montant_bud_reste,
                    my_dernier,my_exe_ordre);

                -- modification dans la base.
                UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=econ_montant_budgetaire_reste-my_montant
                    WHERE econ_id=my_engage_ctrl_convention.econ_id;

                my_somme:=my_somme+my_montant;
           END LOOP;
           CLOSE liste2;
        END IF;
    END IF;
   END;

   PROCEDURE upd_engage_reste_hors_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT ce_ordre, typa_id, SUM(dc.dhom_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie))
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY ce_ordre, typa_id;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id
        AND ehom_montant_budgetaire_reste>0;

     my_depense_ce_ordre         DEPENSE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
     my_depense_typa_id          DEPENSE_CTRL_HORS_MARCHE.typa_id%TYPE;
     my_engage_ctrl_hors_marche  ENGAGE_CTRL_HORS_MARCHE%ROWTYPE;

     my_ehom_montant_bud         ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
     my_ehom_montant_bud_reste   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_ehom_ht_reste            ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_ehom_id                  ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_HORS_MARCHE.dhom_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_HORS_MARCHE.dhom_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_HORS_MARCHE.ehom_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_ehom_reste               engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste%type;
     my_dhom_bud                 depense_ctrl_HORS_MARCHE.dhom_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_HORS_MARCHE
          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire, ehom_ht_reste=ehom_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
          from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dhom_ht_saisie, dc.dhom_ttc_saisie)),0)
          into my_dhom_bud
         FROM DEPENSE_CTRL_HORS_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_ehom_reste>my_reste+my_dhom_bud then
           my_somme:=my_ehom_reste-my_reste-my_dhom_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_HORS_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_HORS_MARCHE 
                      set ehom_montant_budgetaire_reste=my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste-my_somme
                      where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_HORS_MARCHE.ehom_montant_budgetaire_reste;
                    update engage_ctrl_HORS_MARCHE set ehom_montant_budgetaire_reste=0 where ehom_id=my_engage_ctrl_HORS_MARCHE.ehom_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(ehom_montant_budgetaire_reste) into my_ehom_reste
             from engage_ctrl_HORS_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_ce_ordre, my_depense_typa_id, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
              WHERE eng_id=a_eng_id AND typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT ehom_id, ehom_montant_budgetaire_reste, ehom_montant_budgetaire, ehom_ht_reste
                INTO my_ehom_id, my_ehom_montant_bud_reste, my_ehom_montant_bud, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND
                typa_id=my_depense_typa_id AND ce_ordre=my_depense_ce_ordre;

              -- cas de la liquidation.
              IF my_ehom_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE
                   SET ehom_montant_budgetaire_reste=my_ehom_montant_bud_reste-my_montant_budgetaire,
                       ehom_ht_reste=decode(ehom_ht_reste-my_montant_budht, abs(ehom_ht_reste-my_montant_budht),
                          ehom_ht_reste-my_montant_budht,0)
                   WHERE ehom_id=my_ehom_id;
              ELSE
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                   WHERE ehom_id=my_ehom_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_ehom_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_ehom_ht_reste;
              
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0), NVL(SUM(ehom_ht_reste),0)
                INTO my_ehom_montant_bud, my_ehom_montant_bud_reste, my_ehom_ht_reste
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id AND ehom_montant_budgetaire_reste>0;

              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>=my_ehom_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0
                    WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_ehom_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id  AND ehom_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_hors_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_hors_marche.ehom_montant_budgetaire_reste, my_ehom_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_hors_marche.ehom_ht_reste, my_ehom_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_HORS_MARCHE
                          SET ehom_montant_budgetaire_reste=ehom_montant_budgetaire_reste-my_montant,
                              ehom_ht_reste=decode(ehom_ht_reste-my_montant_ht, abs(ehom_ht_reste-my_montant_ht),
                                  ehom_ht_reste-my_montant_ht,0)
                          WHERE ehom_id=my_engage_ctrl_hors_marche.ehom_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_marche (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
        CURSOR liste  IS SELECT att_ordre, SUM(dc.dmar_ht_saisie), SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie))
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY att_ordre;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id
        AND emar_montant_budgetaire_reste>0;

     my_depense_att_ordre        DEPENSE_CTRL_MARCHE.att_ordre%TYPE;
     my_engage_ctrl_marche       ENGAGE_CTRL_MARCHE%ROWTYPE;

     my_emar_montant_bud         ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
     my_emar_montant_bud_reste   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_emar_ht_reste            ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_emar_id                  ENGAGE_CTRL_MARCHE.emar_id%TYPE;
     my_exe_ordre                ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                   ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                   ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire       DEPENSE_CTRL_MARCHE.dmar_montant_budgetaire%TYPE;
     my_montant_budht            DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
     my_nb                       INTEGER;
     my_somme                    ENGAGE_CTRL_MARCHE.emar_montant_budgetaire_reste%TYPE;
     my_somme_ht                 ENGAGE_CTRL_MARCHE.emar_ht_reste%TYPE;
     my_montant                  NUMBER;
     my_montant_ht               NUMBER;
     my_dernier                  BOOLEAN;
     my_reste                    ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_emar_reste               engage_ctrl_MARCHE.emar_montant_budgetaire_reste%type;
     my_dmar_bud                 depense_ctrl_MARCHE.dmar_montant_budgetaire%type;
     my_eng                      ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_MARCHE
          SET emar_montant_budgetaire_reste=emar_montant_budgetaire, emar_ht_reste=emar_ht_saisie
          WHERE eng_id=a_eng_id;

        select sum(emar_montant_budgetaire_reste) into my_emar_reste
          from engage_ctrl_MARCHE where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dmar_ht_saisie, dc.dmar_ttc_saisie)),0)
          into my_dmar_bud
         FROM DEPENSE_CTRL_MARCHE dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_emar_reste>my_reste+my_dmar_bud then
           my_somme:=my_emar_reste-my_reste-my_dmar_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_MARCHE;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_MARCHE 
                      set emar_montant_budgetaire_reste=my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste-my_somme
                      where emar_id=my_engage_ctrl_MARCHE.emar_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_MARCHE.emar_montant_budgetaire_reste;
                    update engage_ctrl_MARCHE set emar_montant_budgetaire_reste=0 where emar_id=my_engage_ctrl_MARCHE.emar_id; 
                 end if; 
              end if;
              
           END LOOP;
           CLOSE liste2;
           
          select sum(emar_montant_budgetaire_reste) into my_emar_reste
             from engage_ctrl_MARCHE where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_att_ordre, my_montant_budht, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE
              WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT emar_id, emar_montant_budgetaire_reste, emar_montant_budgetaire, emar_ht_reste
                INTO my_emar_id, my_emar_montant_bud_reste, my_emar_montant_bud, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_depense_att_ordre;

              -- cas de la liquidation.
              IF my_emar_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_MARCHE
                   SET emar_montant_budgetaire_reste=my_emar_montant_bud_reste-my_montant_budgetaire,
                   emar_ht_reste=decode(emar_ht_reste-my_montant_budht,abs(emar_ht_reste-my_montant_budht),
                       emar_ht_reste-my_montant_budht,0)
                   WHERE emar_id=my_emar_id;
              ELSE
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE emar_id=my_emar_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_emar_montant_bud_reste;
              my_montant_budht:=my_montant_budht-my_emar_ht_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
              IF my_montant_budht<0 THEN
                 my_montant_budht:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;
           my_somme_ht:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0), NVL(SUM(emar_ht_reste),0)
                INTO my_emar_montant_bud, my_emar_montant_bud_reste, my_emar_ht_reste
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND emar_montant_budgetaire_reste>0;

              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>=my_emar_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_emar_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;
                 my_somme_ht:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id  AND emar_montant_budgetaire_reste>0;

                    OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_marche;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_marche.emar_montant_budgetaire_reste, my_emar_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        my_montant_ht:=upd_engage_montant(my_somme_ht, my_montant_budht,
                              my_engage_ctrl_marche.emar_ht_reste, my_emar_ht_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_MARCHE
                          SET emar_montant_budgetaire_reste=emar_montant_budgetaire_reste-my_montant,
                              emar_ht_reste=decode(emar_ht_reste-my_montant_ht, abs(emar_ht_reste-my_montant_ht),
                                 emar_ht_reste-my_montant_ht,0)
                          WHERE emar_id=my_engage_ctrl_marche.emar_id;

                        my_somme:=my_somme+my_montant;
                        my_somme_ht:=my_somme_ht+my_montant_ht;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   PROCEDURE upd_engage_reste_planco (
      a_eng_id               ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     CURSOR liste  IS SELECT pco_num, SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie))
        FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0
          GROUP BY pco_num;

     CURSOR liste2 IS SELECT * FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id
        AND epco_montant_budgetaire_reste>0;

     my_depense_pco_num        DEPENSE_CTRL_PLANCO.pco_num%TYPE;
     my_engage_ctrl_planco     ENGAGE_CTRL_PLANCO%ROWTYPE;

     my_epco_montant_bud       ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
     my_epco_montant_bud_reste ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_epco_id                ENGAGE_CTRL_PLANCO.epco_id%TYPE;
     my_exe_ordre              ENGAGE_BUDGET.exe_ordre%TYPE;
     my_org_id                 ENGAGE_BUDGET.org_id%TYPE;
     my_tcd_ordre              ENGAGE_BUDGET.tcd_ordre%TYPE;
     my_tap_id                 ENGAGE_BUDGET.tap_id%TYPE;
     my_montant_budgetaire     DEPENSE_CTRL_PLANCO.dpco_montant_budgetaire%TYPE;
     my_nb                     INTEGER;
     my_somme                  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire_reste%TYPE;
     my_montant                NUMBER;
     my_dernier                BOOLEAN;
     my_reste                  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_epco_reste             engage_ctrl_PLANCO.epco_montant_budgetaire_reste%type;
     my_dpco_bud               depense_ctrl_PLANCO.dpco_montant_budgetaire%type;
     my_eng                    ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
        END IF;

        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste INTO my_eng, my_reste FROM ENGAGE_BUDGET 
           WHERE eng_id=a_eng_id;

    IF my_reste<=0 THEN
      UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
    ELSE

        SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire
          WHERE eng_id=a_eng_id;

        select sum(epco_montant_budgetaire_reste) into my_epco_reste
          from engage_ctrl_PLANCO where eng_id=a_eng_id;
          
        SELECT nvl(SUM(Budget.calculer_budgetaire(dc.exe_ordre,e.tap_id,e.org_id, dc.dpco_ht_saisie, dc.dpco_ttc_saisie)),0)
          into my_dpco_bud
         FROM DEPENSE_CTRL_PLANCO dc, DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dc.dep_id=d.dep_id AND e.eng_id=d.eng_id AND
          e.eng_id=a_eng_id AND dep_montant_budgetaire>0;
          
        if my_epco_reste>my_reste+my_dpco_bud then
           my_somme:=my_epco_reste-my_reste-my_dpco_bud;
           
           OPEN liste2();
             LOOP
              FETCH  liste2 INTO my_engage_ctrl_PLANCO;
              EXIT WHEN liste2%NOTFOUND;
              
              if my_somme>0 then 
                 if my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste>=my_somme then
                    update engage_ctrl_PLANCO 
                      set epco_montant_budgetaire_reste=my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste-my_somme
                      where epco_id=my_engage_ctrl_PLANCO.epco_id;
                    my_somme:=0;
                 else
                    my_somme:=my_somme-my_engage_ctrl_PLANCO.epco_montant_budgetaire_reste;
                    update engage_ctrl_PLANCO set epco_montant_budgetaire_reste=0 where epco_id=my_engage_ctrl_PLANCO.epco_id; 
                 end if; 
              end if;

           END LOOP;
           CLOSE liste2;
           
          select sum(epco_montant_budgetaire_reste) into my_epco_reste
             from engage_ctrl_PLANCO where eng_id=a_eng_id;

        end if;

        OPEN liste();
          LOOP
           FETCH  liste INTO my_depense_pco_num, my_montant_budgetaire;
           EXIT WHEN liste%NOTFOUND;

           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO
              WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

           -- si il existe un engage_ctrl avec la meme donnee on le modifie.
           IF my_nb=1 THEN
                 SELECT epco_id, epco_montant_budgetaire_reste, epco_montant_budgetaire
                INTO my_epco_id, my_epco_montant_bud_reste, my_epco_montant_bud
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_depense_pco_num;

              -- cas de la liquidation.
              IF my_epco_montant_bud_reste>=my_montant_budgetaire THEN
                 UPDATE ENGAGE_CTRL_PLANCO
                   SET epco_montant_budgetaire_reste=my_epco_montant_bud_reste-my_montant_budgetaire
                   WHERE epco_id=my_epco_id;
              ELSE
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE epco_id=my_epco_id;
              END IF;

              my_montant_budgetaire:=my_montant_budgetaire-my_epco_montant_bud_reste;
              IF my_montant_budgetaire<0 THEN
                 my_montant_budgetaire:=0;
              END IF;
           END IF;

           -- on modifie tous les controleurs avec le reste en repartissant.
           my_somme:=0;

           IF my_montant_budgetaire>0 THEN

              SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0)
                INTO my_epco_montant_bud, my_epco_montant_bud_reste
                FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND epco_montant_budgetaire_reste>0;

              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>=my_epco_montant_bud_reste THEN
                 UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
                    my_montant_budgetaire:=0;
              END IF;

              -- il en reste encore.
              IF my_epco_montant_bud_reste>0 AND my_montant_budgetaire>0 THEN

                 my_somme:=0;

                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id  AND epco_montant_budgetaire_reste>0;

                   OPEN liste2();
                   LOOP
                          FETCH  liste2 INTO my_engage_ctrl_planco;
                        EXIT WHEN liste2%NOTFOUND;

                          IF liste2%rowcount=my_nb THEN my_dernier:=TRUE; ELSE my_dernier:=FALSE; END IF;

                        my_montant:=upd_engage_montant(my_somme, my_montant_budgetaire,
                              my_engage_ctrl_planco.epco_montant_budgetaire_reste, my_epco_montant_bud_reste,
                            my_dernier,my_exe_ordre);

                        -- modification dans la base.
                        UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=epco_montant_budgetaire_reste-my_montant
                          WHERE epco_id=my_engage_ctrl_planco.epco_id;

                        my_somme:=my_somme+my_montant;

                 END LOOP;
                 CLOSE liste2;

              END IF;

           END IF;
        END LOOP;
        CLOSE liste;
    END IF;
   END;

   FUNCTION  upd_engage_montant (
      a_somme               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_reste               ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_total_reste         ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE,
      a_dernier             BOOLEAN,
      a_exe_ordre           ENGAGE_CTRL_ACTION.exe_ordre%TYPE)
   RETURN ENGAGE_CTRL_ACTION.eact_montant_budgetaire_reste%TYPE
   IS
     my_nb                 INTEGER;
     my_par_value          jefy_admin.PARAMETRE.par_value%TYPE;
     my_dev                jefy_admin.devise.dev_nb_decimales%TYPE;
     my_montant            NUMBER;
     my_pourcentage        NUMBER;
     my_nb_decimales       NUMBER;
   BEGIN
        -- nb decimales.
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
        -- calcul
        if a_total_reste=0 then
           my_pourcentage:=0;
        else
           my_pourcentage:=a_reste / a_total_reste;
        end if;
        
        my_montant:=a_montant_budgetaire*my_pourcentage;

        -- arrondir.
        my_montant:= ROUND(my_montant,my_nb_decimales);

        -- on teste avec +1 si les arrondis ont decal� un peu par rapport au total budgetaire
            IF a_dernier THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;
            IF my_montant>a_reste OR my_montant>a_montant_budgetaire - a_somme THEN
            my_montant:=a_montant_budgetaire - a_somme;
        END IF;

        --if a_dernier or my_montant>a_reste then
        --   my_montant:=a_reste;
        --end if;

        RETURN my_montant;
   END;


   PROCEDURE corriger_commande_ctrl (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE) IS
   BEGIN
           corriger_commande_action(a_cbud_id);
           corriger_commande_analytique(a_cbud_id);
           corriger_commande_convention(a_cbud_id);
           corriger_commande_planco(a_cbud_id);

           corriger_commande_hors_marche(a_cbud_id);
           corriger_commande_marche(a_cbud_id);
   END;

   PROCEDURE corriger_commande_action (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ACTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;

           my_somme_pourcentage:=0;
           
           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cact_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ht, my_reste);
              --my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
              --    my_commande_ctrl.cact_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cact_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ACTION SET cact_ht_saisie=my_ht, cact_tva_saisie=my_ttc-my_ht,--my_tva,
                     cact_ttc_saisie=my_ttc, cact_montant_budgetaire=my_budgetaire
               WHERE cact_id=my_commande_ctrl.cact_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_analytique (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_ANALYTIQUE%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cana_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cana_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_ANALYTIQUE SET cana_ht_saisie=my_ht, cana_tva_saisie=my_tva,
                     cana_ttc_saisie=my_ttc, cana_montant_budgetaire=my_budgetaire
               WHERE cana_id=my_commande_ctrl.cana_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_convention (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_CONVENTION%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.ccon_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.ccon_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_CONVENTION SET ccon_ht_saisie=my_ht, ccon_tva_saisie=my_tva,
                     ccon_ttc_saisie=my_ttc, ccon_montant_budgetaire=my_budgetaire
               WHERE ccon_id=my_commande_ctrl.ccon_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_hors_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;
     my_reste              BOOLEAN;

     my_ht_reste           COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste          COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste          COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste   COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_somme_ttc          COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;

     my_ht                 COMMANDE_CTRL_hors_marche.chom_ht_saisie%TYPE;
     my_tva                COMMANDE_CTRL_hors_marche.chom_tva_saisie%TYPE;
     my_ttc                COMMANDE_CTRL_hors_marche.chom_ttc_saisie%TYPE;
     my_budgetaire         COMMANDE_CTRL_hors_marche.chom_montant_budgetaire%TYPE;
     my_somme_pourcentage  COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_pourcentage        COMMANDE_CTRL_hors_marche.chom_pourcentage%TYPE;
     my_commande_ctrl      COMMANDE_CTRL_hors_marche%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_hors_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           select sum(chom_ttc_saisie) into my_somme_ttc from commande_ctrl_hors_marche where cbud_id=a_cbud_id;
            
           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              if my_somme_ttc=0 then 
                 my_pourcentage:=0; 
              else
                 my_pourcentage:=my_commande_ctrl.chom_ttc_saisie*100.0/my_somme_ttc;
              end if;
              my_somme_pourcentage:=my_somme_pourcentage+my_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste, my_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste, my_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste, my_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste, my_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_hors_marche SET chom_ht_saisie=my_ht, chom_tva_saisie=my_tva,
                     chom_ttc_saisie=my_ttc, chom_montant_budgetaire=my_budgetaire
               WHERE chom_id=my_commande_ctrl.chom_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   PROCEDURE corriger_commande_marche (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                 INTEGER;

     my_cde_ht             COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva            COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc            COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire     COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_marche WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
           
           UPDATE COMMANDE_CTRL_marche SET cmar_ht_saisie=my_cde_ht, cmar_tva_saisie=my_cde_tva,
                  cmar_ttc_saisie=my_cde_ttc, cmar_montant_budgetaire=my_cde_budgetaire
               WHERE cbud_id=a_cbud_id;

        END IF;
   END;

   PROCEDURE corriger_commande_planco (
      a_cbud_id               COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_nb                  INTEGER;
     my_reste               BOOLEAN;

     my_ht_reste            COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_tva_reste           COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_ttc_reste           COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_budgetaire_reste    COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_cde_ht              COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cde_tva             COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cde_ttc             COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_cde_budgetaire      COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;

     my_ht                  COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
     my_tva                 COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
     my_ttc                 COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
     my_budgetaire          COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
     my_somme_pourcentage   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
     my_commande_ctrl       COMMANDE_CTRL_PLANCO%ROWTYPE;

     CURSOR liste IS SELECT * FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

   BEGIN
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_nb>0 THEN
           SELECT cbud_ht_saisie, cbud_tva_saisie, cbud_ttc_saisie, cbud_montant_budgetaire
             INTO my_cde_ht, my_cde_tva, my_cde_ttc, my_cde_budgetaire
             FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

           my_ht_reste:=my_cde_ht;
           my_tva_reste:=my_cde_tva;
           my_ttc_reste:=my_cde_ttc;
           my_budgetaire_reste:=my_cde_budgetaire;
           my_somme_pourcentage:=0;

           OPEN liste();
             LOOP
              FETCH  liste INTO my_commande_ctrl;
              EXIT WHEN liste%NOTFOUND;

              IF liste%rowcount=my_nb THEN my_reste:=TRUE; ELSE my_reste:=FALSE; END IF;
              my_somme_pourcentage:=my_somme_pourcentage+my_commande_ctrl.cpco_pourcentage;

              my_ht:=montant_correction_ctrl(my_somme_pourcentage, my_ht_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ht, my_reste);
              my_tva:=montant_correction_ctrl(my_somme_pourcentage, my_tva_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_tva, my_reste);
              my_ttc:=montant_correction_ctrl(my_somme_pourcentage, my_ttc_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_ttc, my_reste);
              my_budgetaire:=montant_correction_ctrl(my_somme_pourcentage, my_budgetaire_reste,
                  my_commande_ctrl.cpco_pourcentage, my_cde_budgetaire, my_reste);

              UPDATE COMMANDE_CTRL_PLANCO SET cpco_ht_saisie=my_ht, cpco_tva_saisie=my_tva,
                     cpco_ttc_saisie=my_ttc, cpco_montant_budgetaire=my_budgetaire
               WHERE cpco_id=my_commande_ctrl.cpco_id;

              my_ht_reste:=my_ht_reste-my_ht;
              my_tva_reste:=my_tva_reste-my_tva;
              my_ttc_reste:=my_ttc_reste-my_ttc;
              my_budgetaire_reste:=my_budgetaire_reste-my_budgetaire;
           END LOOP;
           CLOSE liste;
        END IF;
   END;

   FUNCTION montant_correction_ctrl (
     a_somme_pourcentage    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant_reste        COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_pourcentage          COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE,
     a_montant              COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE,
     a_reste                BOOLEAN
   )
   RETURN COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE
   IS
        my_calcul                NUMBER;
   BEGIN
           IF a_somme_pourcentage>=100.0 AND a_reste=TRUE THEN
           RETURN a_montant_reste;
        END IF;

        my_calcul:=ROUND(a_pourcentage*a_montant/100, 2);

        IF my_calcul>a_montant_reste THEN
           RETURN a_montant_reste;
        END IF;

        RETURN my_calcul;
   END;

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Verifier
IS

   PROCEDURE verifier_engage_exercice (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     ENGAGE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);

        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit d'engager hors periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENG');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENG pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit d'engager pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DEENGINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DEENGINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_eng='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit d''engager pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
               INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_exercice (
      a_exe_ordre     DEPENSE_BUDGET.exe_ordre%TYPE,
      a_utl_ordre     DEPENSE_BUDGET.utl_ordre%TYPE,
      a_org_id        engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
       my_droit          INTEGER;
       my_fon_ordre   v_fonction.fon_ordre%TYPE;
   BEGIN
        verifier_organ_utilisateur(a_utl_ordre, a_org_id);
        
        -- verifier que l'on peux engager liquider et/ou mandater sur cet exercice.

        my_droit:=0;

        -- on verifie si l"utilisateur a le droit de liquider hors periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DELIQ');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQ pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='O';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- on verifie si l"utilisateur a le droit de liquider pendant la periode d'inventaire.
           my_fon_ordre:=Get_Fonction('DELIQINV');
        IF my_fon_ordre IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'La fonction DELIQINV pour le type d''application DEPENSE n''existe pas.');
        END IF;

        SELECT COUNT(*) INTO my_nb
          FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_liq='R';

        IF my_nb>0 THEN my_droit:=1; END IF;

        -- si droit=0 alors pas de droit trouver.
        IF my_droit=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de liquider pour cet exercice et cet utilisateur ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_organ_utilisateur (
         a_utl_ordre          engage_budget.utl_ordre%type,
      a_org_id          engage_budget.org_id%type
   ) IS
       my_nb          INTEGER;
   BEGIN
        select count(*) into my_nb from v_utilisateur_organ where utl_ordre=a_utl_ordre and org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''a pas le droit sur cette ligne budgetaire ('||INDICATION_ERREUR.utilisateur(a_utl_ordre)||', '||
              INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;
   
   PROCEDURE verifier_budget (
       a_exe_ordre    ENGAGE_BUDGET.exe_ordre%TYPE,
          a_tap_id       ENGAGE_BUDGET.tap_id%TYPE,
       a_org_id       ENGAGE_BUDGET.org_id%TYPE,
       a_tcd_ordre    ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_organ_prorata WHERE tap_id=a_tap_id AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce taux de prorata n''est pas autorise pour cette ligne budgetaire et cet exercice ('||
              INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.taux_prorata(a_tap_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_budget_exec_credit WHERE tcd_ordre=a_tcd_ordre AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il n''y a pas de credit ouvert pour cette ligne budgetaire, ce type de credit et cet exercice ('
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_action (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_tyac_id                 ENGAGE_CTRL_ACTION.tyac_id%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
      my_nb           INTEGER;
      my_par_value    PARAMETRE.par_value%TYPE;
      my_fon_ordre      v_fonction.fon_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_type_action WHERE exe_ordre=a_exe_ordre AND tyac_id=a_tyac_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorise pour cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '||
              INDICATION_ERREUR.action(a_tyac_id)||')');
        END IF;

        -- si parametre tester suivant les destinations du budget de gestion.
        my_par_value:=Get_Parametre(a_exe_ordre, 'CTRL_ORGAN_DEST');
        IF my_par_value = 'OUI' THEN

           my_fon_ordre:=Get_Fonction('DEAUTACT');
           IF my_fon_ordre IS NULL THEN
               RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTACT pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb  FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
              IF my_nb>0 THEN
                 SELECT COUNT(*) INTO my_nb FROM v_organ_action WHERE tyac_id=a_tyac_id AND tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
                 IF my_nb=0 THEN
                    RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorisee pour cette ligne budgetaire et ce type de credit ('||
                       INDICATION_ERREUR.action(a_tyac_id)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
                 END IF;
              END IF;
           END IF;
        END IF;
   END;

   PROCEDURE verifier_analytique (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_can_id                  ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN

        -- verifier si ce code analytique est utilisable.
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas utilisable  ('||INDICATION_ERREUR.analytique(a_can_id)||')');
        END IF;

        -- verifier si droit utilisation code analytique (suivant organ).
        SELECT COUNT(*) INTO my_nb FROM v_code_analytique c, v_code_analytique_exercice e
           WHERE c.can_id=a_can_id AND can_utilisable=Etats.get_etat_canal_utilisable
             AND tyet_id=Etats.get_etat_canal_valide AND e.can_id=c.can_id AND e.exe_ordre=a_exe_ordre
           AND can_public=Etats.get_etat_canal_public;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_analytique_organ WHERE
              can_id=a_can_id AND (org_id=Get_Composante(a_org_id) OR org_id=get_cr(a_org_id) OR org_id=a_org_id);
           IF my_nb=0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique ne peut etre utilise avec cette ligne budgetaire ('||
                  INDICATION_ERREUR.analytique(a_can_id)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_convention (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_conv_ordre              ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE
   ) IS
       my_nb      INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_convention_non_limitative
           WHERE exe_ordre=a_exe_ordre AND org_id=a_org_id AND tcd_ordre=a_tcd_ordre
             AND conv_ordre=a_conv_ordre;

        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette convention ne peut etre utilisee avec cette ligne budgetaire et ce type de credit ('||
              INDICATION_ERREUR.convention(a_conv_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||', '||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_hors_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_typa_id                 ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE,
      a_ce_ordre                ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE,
      a_fou_ordre                ENGAGE_BUDGET.fou_ordre%TYPE
   ) IS
       my_nb         INTEGER;
       my_cm_niveau  v_code_marche.cm_niveau%TYPE;
       my_cm_suppr   v_code_marche.cm_suppr%TYPE;
       my_ce_suppr   v_code_exer.ce_suppr%TYPE;
   BEGIN
           -- les seuils sont en HT.

        -- verifier que le ce_ordre est bien sur l'ann�e choisie.
        SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Ce code nomenclature n''existe pas sur cet exercice ('||INDICATION_ERREUR.exercice(a_exe_ordre)||', '
             ||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- que le cm_code est un niveau 2.
        SELECT m.cm_niveau, m.cm_suppr, e.ce_suppr INTO my_cm_niveau, my_cm_suppr, my_ce_suppr
           FROM v_code_marche m, v_code_exer e
           WHERE m.cm_ordre=e.cm_ordre AND e.ce_ordre=a_ce_ordre;
        IF my_cm_niveau<>2 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut mettre un code nomenclature de niveau 2 ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        IF my_ce_suppr='O' OR my_cm_suppr='O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le code nomenclature n''est plus utilisable ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
        END IF;

        -- verifier que ce type achat est utilisable pour cet exercice.
        --select count(*) into my_nb from type_achat_exercice where typa_id=a_typa_id and exe_ordre=a_exe_ordre;
        --if my_nb<>1 then
        --   raise_application_error(-20001, 'Ce type achat n''est pas autorise sur cet exercice (typa_id:'||a_typa_id||', exercice:'||a_exe_ordre||')');
        --end if;


        -- inclure le verifs concernant eventuellement service_achat.


        -- verifier si fournisseur est mono ou 3CMP (mettre tag dans ces types).
        IF a_typa_id=Get_Type_Achat('3CMP') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_3cmp=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "3CMP" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('MONOPOLE') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_monopole=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "MONOPOLE" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;

           SELECT COUNT(*) INTO my_nb FROM v_code_marche_four
              WHERE fou_ordre=a_fou_ordre AND ce_ordre=a_ce_ordre;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur n''est pas autorise pour ce code nomenclature ('||
                INDICATION_ERREUR.code_exer(a_ce_ordre)||', '||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
           END IF;
        END IF;

        IF a_typa_id=Get_Type_Achat('SANS ACHAT') THEN
           SELECT COUNT(*) INTO my_nb FROM v_code_exer WHERE ce_ordre=a_ce_ordre AND ce_autres=1;
           IF my_nb=0 THEN
             RAISE_APPLICATION_ERROR(-20001,'Ce code nomenclature n''est pas "SANS ACHAT" ('||INDICATION_ERREUR.code_exer(a_ce_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_marche (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
      a_fou_ordre               ENGAGE_BUDGET.fou_ordre%TYPE,
      a_att_ordre               ENGAGE_CTRL_MARCHE.att_ordre%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
       my_nb             INTEGER;
       my_lot_ordre      v_attribution.lot_ordre%TYPE;
       my_att_execution  v_attribution_execution.aee_execution%TYPE;
       my_reversement    DEPENSE_CTRL_MARCHE.dmar_ht_saisie%TYPE;
       my_reste_engage   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
   BEGIN
        -- verifier que le fournisseur est bon.
        --    que c'est celui de l'attribution ou un sous-traitant.
        SELECT COUNT(*) INTO my_nb FROM v_attribution WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
        IF my_nb=0 THEN
           SELECT COUNT(*) INTO my_nb FROM v_sous_traitant WHERE fou_ordre=a_fou_ordre AND att_ordre=a_att_ordre;
           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas celui de l''attribution ni un sous traitant ('||
                 INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '||INDICATION_ERREUR.attribution(a_att_ordre)||')');
           END IF;
        END IF;

        -- verif sur lot_organ et lot_agent ... mais il faut changer les clefs.
        SELECT lot_ordre INTO my_lot_ordre FROM v_attribution WHERE att_ordre=a_att_ordre;

        SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_organ WHERE lot_ordre=my_lot_ordre AND org_id=a_org_id;
              IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cette ligne budgetaire ('||
                 INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.organ(a_org_id)||')');
             END IF;
        END IF;

        SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre;
        IF my_nb>0 THEN
              SELECT COUNT(*) INTO my_nb FROM v_lot_utilisateur WHERE lot_ordre=my_lot_ordre AND utl_ordre=a_utl_ordre;
           
              IF my_nb=0 THEN      
              SELECT COUNT(*) INTO my_nb FROM v_utilisateur_fonct WHERE utl_ordre=a_utl_ordre AND fon_ordre=Get_Fonction_jefyadmin('TOUTORG');
              
              IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande sur ce lot n''est pas autorisee pour cet utilisateur ('||
                      INDICATION_ERREUR.lot(my_lot_ordre)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
              END IF;
             END IF;
        END IF;
   END;

   PROCEDURE verifier_planco (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
         a_org_id                  ENGAGE_BUDGET.org_id%TYPE,
         a_tcd_ordre               ENGAGE_BUDGET.tcd_ordre%TYPE,
      a_pco_num                 ENGAGE_CTRL_PLANCO.pco_num%TYPE,
      a_utl_ordre               ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
          my_nb              INTEGER;
       my_pco_validite    v_plan_comptable.pco_validite%TYPE;
       my_fon_ordre          jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
        -- verifier la validite du compte.
        SELECT COUNT(*) INTO my_nb FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''existe pas pour cet exercice ('||
              INDICATION_ERREUR.imputation(a_pco_num)||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        SELECT pco_validite INTO my_pco_validite FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre=a_exe_ordre;
        IF my_pco_validite<>'VALIDE' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas valide pour cet exercice ('||INDICATION_ERREUR.imputation(a_pco_num)
              ||INDICATION_ERREUR.exercice(a_exe_ordre)||')');
        END IF;

        -- verifier si correct avec le type de credit.
        SELECT COUNT(*) INTO my_nb FROM v_planco_credit
         WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pla_quoi='D' and exe_ordre=a_exe_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas associe a ce type de credit ('||
              INDICATION_ERREUR.imputation(a_pco_num)||', type de credit:'||INDICATION_ERREUR.type_credit(a_tcd_ordre)||')');
        END IF;

        -- on verifie si l"utilisateur a le droit d'utiliser des comptes autres que de classe 2 ou 6.
        IF SUBSTR(a_pco_num,1,1)<>'2' AND SUBSTR(a_pco_num,1,1)<>'6' THEN
              my_fon_ordre:=Get_Fonction('DEAUTIMP');
           IF my_fon_ordre IS NULL THEN
              RAISE_APPLICATION_ERROR(-20001, 'La fonction DEAUTIMP pour le type d''application DEPENSE n''existe pas.');
           END IF;

           SELECT COUNT(*) INTO my_nb
             FROM v_utilisateur_fonct uf, v_utilisateur_fonct_exercice ufe
             WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
               uf.fon_ordre=my_fon_ordre;

           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation ('||
                 INDICATION_ERREUR.imputation(a_pco_num)||', '||INDICATION_ERREUR.utilisateur(a_utl_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_emargement (
      a_exe_ordre               ENGAGE_BUDGET.exe_ordre%TYPE,
      a_mod_ordre                DEPENSE_PAPIER.mod_ordre%TYPE,
      a_ecd_ordre               DEPENSE_CTRL_PLANCO.ecd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_ecriture_detail_a_emarger
          WHERE exe_ordre=a_exe_ordre AND mod_ordre=a_mod_ordre AND ecd_ordre=a_ecd_ordre;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cette ecriture n''est pas autorisee pour ce mode de paiement ('
              ||INDICATION_ERREUR.ecriture(a_ecd_ordre)||', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_inventaire IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_monnaie IS
       i INTEGER;
   BEGIN
        -- a completer --
        i:=1;
   END;

   PROCEDURE verifier_fournisseur (
         a_fou_ordre        v_fournisseur.fou_ordre%TYPE
   ) IS
          my_nb            INTEGER;
       my_fou_valide    v_fournisseur.fou_valide%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''existe pas ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;

        SELECT fou_valide INTO my_fou_valide FROM v_fournisseur WHERE fou_ordre=a_fou_ordre;
        IF my_fou_valide<>'O' THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas valide ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||')');
        END IF;
   END;

   PROCEDURE verifier_rib (
         a_fou_ordre        v_rib_fournisseur.fou_ordre%TYPE,
         a_rib_ordre        v_rib_fournisseur.rib_ordre%TYPE,
         a_mod_ordre        DEPENSE_PAPIER.mod_ordre%TYPE,
         a_exe_ordre        DEPENSE_PAPIER.exe_ordre%TYPE
   ) IS
          my_nb               INTEGER;
       my_rib_valide    v_rib_fournisseur.rib_valide%TYPE;
       my_mod_code        v_rib_fournisseur.mod_code%TYPE;
       my_mod_dom        v_mode_paiement.mod_dom%TYPE;
   BEGIN
           SELECT COUNT(*) INTO my_nb FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre AND exe_ordre=a_exe_ordre;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le mode de paiement n''existe pas pour cet exercice  ('||INDICATION_ERREUR.exercice(a_exe_ordre)||
              ', '||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
        END IF;

         SELECT mod_code, mod_dom INTO my_mod_code, my_mod_dom FROM v_mode_paiement WHERE mod_ordre=a_mod_ordre;

        IF a_rib_ordre IS NOT NULL THEN

           IF my_mod_dom<>'VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il ne faut pas de rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;

              SELECT COUNT(*) INTO my_nb FROM v_rib_fournisseur WHERE fou_ordre=a_fou_ordre AND rib_ordre=a_rib_ordre;

           IF my_nb<>1 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''existe pas pour ce fournisseur ('||INDICATION_ERREUR.fournisseur(a_fou_ordre)||', '
                 ||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;

           SELECT rib_valide INTO my_rib_valide FROM v_rib_fournisseur WHERE rib_ordre=a_rib_ordre;
           IF my_rib_valide<>'O' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Le rib n''est pas valide ('||INDICATION_ERREUR.rib(a_rib_ordre)||')');
           END IF;
        ELSE
           IF my_mod_dom='VIREMENT' THEN
              RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de paiement il faut un rib ('||INDICATION_ERREUR.mode_paiement(a_mod_ordre)||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_engage_coherence (a_eng_id ENGAGE_BUDGET.eng_id%TYPE) IS
        my_eng_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_eng_montant_budgetaire_rest  ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_eng_ht_saisie                ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_eng_tva_saisie               ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_eng_ttc_saisie               ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;
        
        my_montant_budgetaire           ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
        my_montant_budgetaire_reste     ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
        my_ht_saisie                    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
        my_tva_saisie                   ENGAGE_BUDGET.eng_tva_saisie%TYPE;
        my_ttc_saisie                   ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
        my_tyap_id                      ENGAGE_BUDGET.tyap_id%TYPE;

        my_tag_marche                   INTEGER;
        my_nb                           INTEGER;
   BEGIN
        my_tag_marche:=0;
        my_nb:=0;

           -- recupere les montants de engage_budget.
        SELECT eng_montant_budgetaire, eng_montant_budgetaire_reste, 
               eng_ht_saisie, eng_tva_saisie, eng_ttc_saisie, org_id
          INTO my_eng_montant_budgetaire, my_eng_montant_budgetaire_rest,
               my_eng_ht_saisie, my_eng_tva_saisie, my_eng_ttc_saisie, my_org_id
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;
        
        -- recupere et compare les montants de engage_ctrl_action.
        SELECT NVL(SUM(eact_montant_budgetaire),0), NVL(SUM(eact_montant_budgetaire_reste),0),
               NVL(SUM(eact_ht_saisie),0), NVL(SUM(eact_tva_saisie),0), NVL(SUM(eact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_action ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- recupere et compare les montants de engage_ctrl_analytique.

        SELECT NVL(SUM(eana_montant_budgetaire),0), NVL(SUM(eana_montant_budgetaire_reste),0),
               NVL(SUM(eana_ht_saisie),0), NVL(SUM(eana_tva_saisie),0), NVL(SUM(eana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;


        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
              my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
              my_eng_ttc_saisie<>my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        else
        -- on ne verifie pas la coherence exacte de engage_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de l'engagement.
           IF my_eng_montant_budgetaire<my_montant_budgetaire OR
              my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
              my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
              my_eng_ttc_saisie<my_ttc_saisie THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_analytique ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
           END IF;
        end if;
        -- recupere et compare les montants de engage_ctrl_convention.

        -- on ne verifie pas la coherence exacte de engage_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de l'engagement.

        SELECT NVL(SUM(econ_montant_budgetaire),0), NVL(SUM(econ_montant_budgetaire_reste),0),
               NVL(SUM(econ_ht_saisie),0), NVL(SUM(econ_tva_saisie),0), NVL(SUM(econ_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<my_montant_budgetaire_reste OR
           my_eng_ht_saisie<my_ht_saisie OR my_eng_tva_saisie<my_tva_saisie OR
           my_eng_ttc_saisie<my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_convention ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- recupere et compare les montants de engage_ctrl_planco.
        SELECT NVL(SUM(epco_montant_budgetaire),0), NVL(SUM(epco_montant_budgetaire_reste),0),
               NVL(SUM(epco_ht_saisie),0), NVL(SUM(epco_tva_saisie),0), NVL(SUM(epco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_montant_budgetaire_reste,
               my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

        IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
           my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
           my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
           my_eng_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_planco ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de engage_ctrl_hors_marche.
           -- on teste si c'est un engagement hors marche.
           SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(ehom_montant_budgetaire),0), NVL(SUM(ehom_montant_budgetaire_reste),0),
                  NVL(SUM(ehom_ht_saisie),0), NVL(SUM(ehom_tva_saisie),0), NVL(SUM(ehom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;

                 IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                   my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                   my_eng_ttc_saisie<>my_ttc_saisie THEN
                   RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_hors_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;

           -- recupere et compare les montants de engage_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve un engagement hors marche, on teste qu'il n'y a pas d'engagement marche.
              SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           ELSE
              -- si pas d'engagement hors marche on teste qu'il y a un engagement marche.
                 SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
                 IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit etre MARCHE ou HORS MARCHE ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;

              SELECT NVL(SUM(emar_montant_budgetaire),0), NVL(SUM(emar_montant_budgetaire_reste),0),
                  NVL(SUM(emar_ht_saisie),0), NVL(SUM(emar_tva_saisie),0), NVL(SUM(emar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_montant_budgetaire_reste,
                  my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

              IF my_eng_montant_budgetaire<>my_montant_budgetaire OR
                 my_eng_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
                 my_eng_ht_saisie<>my_ht_saisie OR my_eng_tva_saisie<>my_tva_saisie OR
                 my_eng_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles engage_ctrl_marche ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
              END IF;
           END IF;
        END IF;
   END;

   PROCEDURE verifier_commande_coherence(a_comm_id COMMANDE.comm_id%TYPE)
   IS
        my_nb_art_marche      INTEGER;
        my_nb_art_hors_marche INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb_art_hors_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NULL;

        SELECT COUNT(*) INTO my_nb_art_marche FROM ARTICLE
          WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;

        IF my_nb_art_hors_marche>0 AND my_nb_art_marche>0 THEN
          RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre marche ou hors marche');
        END IF;
   END;

   PROCEDURE verifier_cde_budget_coherence(a_cbud_id COMMANDE_BUDGET.cbud_id%TYPE)
   IS
           my_exe_ordre                    COMMANDE.exe_ordre%TYPE;

        my_ht                            COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_tva                            COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
        my_ttc                            COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
        my_budgetaire                    COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
        my_pourcentage                    COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;

        my_budget_ht                    COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
        my_budget_tva                    COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
        my_budget_ttc                    COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
        my_budget_montant_budget        COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
        my_tag_marche                    INTEGER;
        my_nb                            INTEGER;
   BEGIN
           my_tag_marche:=0;
        my_nb:=0;

        SELECT exe_ordre INTO my_exe_ordre FROM COMMANDE
           WHERE comm_id IN (SELECT comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id);

        SELECT NVL(cbud_montant_budgetaire,0),
                NVL(cbud_ht_saisie,0), NVL(cbud_tva_saisie,0), NVL(cbud_ttc_saisie,0)
           INTO my_budget_montant_budget,
                my_budget_ht, my_budget_tva, my_budget_ttc
           FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        -- recupere et compare les montants de commande_ctrl_action.
        SELECT NVL(SUM(cact_montant_budgetaire),0), NVL(SUM(cact_pourcentage),0),
            NVL(SUM(cact_ht_saisie),0), NVL(SUM(cact_tva_saisie),0), NVL(SUM(cact_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
              100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_action (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de commande_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(cana_montant_budgetaire),0), NVL(SUM(cana_pourcentage),0),
               NVL(SUM(cana_ht_saisie),0), NVL(SUM(cana_tva_saisie),0), NVL(SUM(cana_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_analytique (commande_budget:'||a_cbud_id||')');
         END IF;

        -- recupere et compare les montants de commande_ctrl_convention.

        -- on ne verifie pas la coherence exacte de commande_ctrl_convention.
        --   puisque les coventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la commande_budget.

        SELECT NVL(SUM(ccon_montant_budgetaire),0), NVL(SUM(ccon_pourcentage),0),
               NVL(SUM(ccon_ht_saisie),0), NVL(SUM(ccon_tva_saisie),0), NVL(SUM(ccon_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
         FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;

         IF my_budget_montant_budget<my_budgetaire OR
                100.0<my_pourcentage OR
               my_budget_ht<my_ht OR my_budget_tva<my_tva OR
               my_budget_ttc<my_ttc THEN
               RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_convention (commande_budget:'||a_cbud_id||')');
         END IF;

         -- recupere et compare les montants de commande_ctrl_planco.
         SELECT NVL(SUM(cpco_montant_budgetaire),0), NVL(SUM(cpco_pourcentage),0),
                   NVL(SUM(cpco_ht_saisie),0), NVL(SUM(cpco_tva_saisie),0), NVL(SUM(cpco_ttc_saisie),0)
           INTO my_budgetaire, my_pourcentage, my_ht, my_tva, my_ttc
           FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        IF my_budget_montant_budget<>my_budgetaire OR
               100.0<my_pourcentage OR
              my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
              my_budget_ttc<>my_ttc THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_planco (commande_budget:'||a_cbud_id||')');
        END IF;

        -- recupere et compare les montants de commande_ctrl_hors_marche.
           -- on teste si c'est une commande hors marche.
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           -- si c'est hors marche on teste.
        IF my_nb>0 THEN
           SELECT NVL(SUM(chom_montant_budgetaire),0),
                  NVL(SUM(chom_ht_saisie),0), NVL(SUM(chom_tva_saisie),0), NVL(SUM(chom_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_hors_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;

        -- recupere et compare les montants de commande_ctrl_marche.
        IF my_nb>0 THEN
           -- si on a trouve une commande hors marche, on teste qu'il n'y a pas de commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb>0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;
        ELSE
           -- si pas de commande hors marche on teste qu'il y a une commande marche.
           SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
           IF my_nb=0 THEN
                  RAISE_APPLICATION_ERROR(-20001, 'La commande doit etre MARCHE ou HORS MARCHE (commande_budget:'||a_cbud_id||')');
           END IF;

           SELECT NVL(SUM(cmar_montant_budgetaire),0),
                  NVL(SUM(cmar_ht_saisie),0), NVL(SUM(cmar_tva_saisie),0), NVL(SUM(cmar_ttc_saisie),0)
             INTO my_budgetaire, my_ht, my_tva, my_ttc
             FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

           IF my_budget_montant_budget<>my_budgetaire OR
                 my_budget_ht<>my_ht OR my_budget_tva<>my_tva OR
                 my_budget_ttc<>my_ttc THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles commande_ctrl_marche (commande_budget:'||a_cbud_id||')');
           END IF;
        END IF;
   END;

   PROCEDURE verifier_depense_pap_coherence(a_dpp_id DEPENSE_PAPIER.dpp_id%TYPE)
   IS
        my_dpp_ht_saisie                DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_dpp_tva_saisie                DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_dpp_ttc_saisie                DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;

        my_ht_saisie                    DEPENSE_PAPIER.dpp_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_PAPIER.dpp_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_PAPIER.dpp_ttc_saisie%TYPE;
   BEGIN

           -- recupere les montants de depense_papier.
        SELECT dpp_ht_saisie, dpp_tva_saisie, dpp_ttc_saisie
          INTO my_dpp_ht_saisie, my_dpp_tva_saisie, my_dpp_ttc_saisie
          FROM DEPENSE_PAPIER WHERE dpp_id=a_dpp_id;

        -- recupere et compare les montants de depense_budget.
        SELECT NVL(SUM(dep_ht_saisie),0), NVL(SUM(dep_tva_saisie),0), NVL(SUM(dep_ttc_saisie),0)
          INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;

        IF my_dpp_ht_saisie<>my_ht_saisie OR my_dpp_tva_saisie<>my_tva_saisie OR
           my_dpp_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_budget ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;
   END;

   PROCEDURE verifier_depense_coherence(a_dep_id DEPENSE_BUDGET.dep_id%TYPE)
   IS
           my_dep_montant_budgetaire       DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_dep_ht_saisie                DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_dep_tva_saisie                DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_dep_ttc_saisie                DEPENSE_BUDGET.dep_ttc_saisie%TYPE;

        my_montant_budgetaire            DEPENSE_BUDGET.dep_montant_budgetaire%TYPE;
        my_ht_saisie                    DEPENSE_BUDGET.dep_ht_saisie%TYPE;
        my_tva_saisie                    DEPENSE_BUDGET.dep_tva_saisie%TYPE;
        my_ttc_saisie                    DEPENSE_BUDGET.dep_ttc_saisie%TYPE;
        my_tyap_id                        ENGAGE_BUDGET.tyap_id%TYPE;

        my_org_id                       engage_budget.org_id%type;
        my_org_canal_obligatoire        jefy_admin.organ.org_canal_obligatoire%type;

        my_tag_marche                    INTEGER;
        my_nb                            INTEGER;
   BEGIN
           my_tag_marche:=0;
        my_nb:=0;

        -- recupere les montants de depense_budget.
        SELECT dep_montant_budgetaire, dep_ht_saisie, dep_tva_saisie, dep_ttc_saisie, tyap_id, org_id
          INTO my_dep_montant_budgetaire, my_dep_ht_saisie, my_dep_tva_saisie, my_dep_ttc_saisie, my_tyap_id, my_org_id
          FROM DEPENSE_BUDGET d, ENGAGE_BUDGET e WHERE dep_id=a_dep_id AND d.eng_id=e.eng_id;

        select org_canal_obligatoire into my_org_canal_obligatoire from jefy_admin.organ where org_id=my_org_id;

        -- recupere et compare les montants de depense_ctrl_action.
        SELECT NVL(SUM(dact_montant_budgetaire),0), NVL(SUM(dact_ht_saisie),0), NVL(SUM(dact_tva_saisie),0),
           NVL(SUM(dact_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ACTION WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_action (depense:'||a_dep_id||')');
        END IF;

        -- recupere et compare les montants de depense_ctrl_analytique.

        -- on ne verifie pas la coherence exacte de depense_ctrl_analytique.
        --   puisque les codes analytiques sont facultatifs .
        -- on verifie juste qu'ils ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dana_montant_budgetaire),0), NVL(SUM(dana_ht_saisie),0), NVL(SUM(dana_tva_saisie),0),
          NVL(SUM(dana_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_ANALYTIQUE WHERE dep_id=a_dep_id;

        if my_org_canal_obligatoire is not null and (my_org_canal_obligatoire=3 or my_org_canal_obligatoire=21) then
           IF ABS(my_dep_montant_budgetaire)<>ABS(my_montant_budgetaire) OR
              ABS(my_dep_ht_saisie)<>ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<>ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<>ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;
        else
           IF ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire) OR
              ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
              ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
              RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_analytique (depense:'||a_dep_id||')');
           END IF;
        end if;
        -- recupere et compare les montants de depense_ctrl_convention.

        -- on ne verifie pas la coherence exacte de depense_ctrl_convention.
        --   puisque les conventions sont facultatives .
        -- on verifie juste qu'elles ne depassent pas le montant de la depense.

        SELECT NVL(SUM(dcon_montant_budgetaire),0), NVL(SUM(dcon_ht_saisie),0), NVL(SUM(dcon_tva_saisie),0),
          NVL(SUM(dcon_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_CONVENTION WHERE dep_id=a_dep_id;

        IF ABS(my_dep_montant_budgetaire)<ABS(my_montant_budgetaire) OR
           ABS(my_dep_ht_saisie)<ABS(my_ht_saisie) OR ABS(my_dep_tva_saisie)<ABS(my_tva_saisie) OR
           ABS(my_dep_ttc_saisie)<ABS(my_ttc_saisie) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_convention (depense:'||a_dep_id||')');
        END IF;

        -- recupere et compare les montants de depense_ctrl_planco.

          -- pour les problemes de rejet d'une partie d'une depense_budget on limite pour le moment.
          --  a un seul compte d'imputation.
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001, 'On limite a une seule imputation pour les depenses (depense:'||a_dep_id||')');
        END IF;

        SELECT NVL(SUM(dpco_montant_budgetaire),0), NVL(SUM(dpco_ht_saisie),0), NVL(SUM(dpco_tva_saisie),0),
          NVL(SUM(dpco_ttc_saisie),0)
          INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
          FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id;

        IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
           my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
           my_dep_ttc_saisie<>my_ttc_saisie THEN
           RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_planco (depense:'||a_dep_id||')');
        END IF;

        IF my_tyap_id<>Get_Tyap_Id('PRESTATION INTERNE') THEN
           -- recupere et compare les montants de depense_ctrl_hors_marche.
           -- on teste si c'est une depense hors marche.
           SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

           -- si c'est hors marche on teste.
           IF my_nb>0 THEN
              SELECT NVL(SUM(dhom_montant_budgetaire),0), NVL(SUM(dhom_ht_saisie),0), NVL(SUM(dhom_tva_saisie),0),
                NVL(SUM(dhom_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_HORS_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_hors_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;

           -- recupere et compare les montants de depense_ctrl_marche.
           IF my_nb>0 THEN
              -- si on a trouve une depense hors marche, on teste qu'il n'y a pas de depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb>0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;
           ELSE
              -- si pas de depense hors marche on teste qu'il y a une depense marche.
              SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;
              IF my_nb=0 THEN
                 RAISE_APPLICATION_ERROR(-20001, 'La depense doit etre MARCHE ou HORS MARCHE (depense:'||a_dep_id||')');
              END IF;

              SELECT NVL(SUM(dmar_montant_budgetaire),0), NVL(SUM(dmar_ht_saisie),0), NVL(SUM(dmar_tva_saisie),0),
                NVL(SUM(dmar_ttc_saisie),0)
                INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
                FROM DEPENSE_CTRL_MARCHE WHERE dep_id=a_dep_id;

              IF my_dep_montant_budgetaire<>my_montant_budgetaire OR
                 my_dep_ht_saisie<>my_ht_saisie OR my_dep_tva_saisie<>my_tva_saisie OR
                 my_dep_ttc_saisie<>my_ttc_saisie THEN
                 RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles depense_ctrl_marche (depense:'||a_dep_id||')');
              END IF;
           END IF;
        END IF;
   END;

   PROCEDURE verifier_organ(
        a_org_id       ENGAGE_BUDGET.org_id%TYPE,
     a_tcd_ordre   ENGAGE_BUDGET.tcd_ordre%TYPE
   ) IS
       my_org_niv        jefy_admin.organ.org_niv%TYPE;
       my_nb            INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM jefy_admin.organ WHERE org_id=a_org_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La ligne budgetaire n''existe pas ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;

           SELECT org_niv INTO my_org_niv FROM jefy_admin.organ WHERE org_id=a_org_id;

        IF my_org_niv<3 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il faut engager sur un CR ou une convention ('||INDICATION_ERREUR.organ(a_org_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des commandes sur cet engagement ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- a rajouter.

        -- missions.
        /*select count(*) into my_nb from jefy_mission.mission_paiement_engage where eng_id=a_eng_id;
        if my_nb>0 then
           raise_application_error(-20001, 'Il y a des missions sur cet engagement (eng_id='||a_eng_id||')');
        end if;
        */
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_budget (
      a_dep_id              DEPENSE_BUDGET.dep_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dep_id_reversement=a_dep_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_CTRL_PLANCO WHERE dep_id=a_dep_id AND man_id IS NOT NULL;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des mandats sur cette liquidation (dep_id='||a_dep_id||')');
        END IF;

        -- a rajouter.

        -- missions.
        --select count(*) into my_nb from jefy_mission.mission_paiement_liquide where dep_id=a_dep_id;
        --if my_nb>0 then
        --   raise_application_error(-20001, 'Il y a des missions sur cette liquidation (dep_id='||a_dep_id||')');
        --end if;

        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_depense_papier (
      a_dpp_id              DEPENSE_PAPIER.dpp_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE dpp_id=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des liquidations sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM DEPENSE_PAPIER WHERE dpp_id_reversement=a_dpp_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des reversements sur cette facture ('||INDICATION_ERREUR.dep_papier(a_dpp_id)||')');
        END IF;


        -- a rajouter.

        -- missions.
        -- telephonie.
        -- prestation interne.
        -- papaye.
        -- inventaire.
        -- arretes en masse.
   END;

   PROCEDURE verifier_util_commande (
      a_comm_id             COMMANDE.comm_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Il y a des engagements sur cette commande ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;
   END;

   PROCEDURE verifier_util_commande_budget (
      a_cbud_id              COMMANDE_BUDGET.cbud_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
           my_nb:=0;
   END;

END;
/








create table jefy_depense.reimputation (
  reim_id      number not null,
  exe_ordre    number not null,
  reim_numero  number not null,
  dep_id       number not null,
  reim_libelle varchar2(500),
  utl_ordre    number not null,
  reim_date    date not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation is 'table des depenses reimputees';
COMMENT ON COLUMN jefy_depense.reimputation.reim_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_depense.reimputation.reim_numero is 'numero de la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation.dep_id is 'Cle etrangere table jefy_depense.depense_budget';
COMMENT ON COLUMN jefy_depense.reimputation.reim_libelle is 'libelle de la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_depense.reimputation.reim_date is 'date de la reimputation';

CREATE UNIQUE INDEX PK_reimputation ON jefy_depense.reimputation (reim_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation ADD (CONSTRAINT PK_reimputation PRIMARY KEY (reim_id));

ALTER TABLE jefy_depense.reimputation ADD ( CONSTRAINT FK_reimputation_dep_id FOREIGN KEY (dep_id) 
    REFERENCES jefy_depense.depense_budget(dep_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation ADD ( CONSTRAINT FK_reimputation_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur(utl_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation ADD ( CONSTRAINT FK_reimputation_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice(exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_budget (
  rebu_id   number not null,
  reim_id   number not null,
  eng_id    number not null,
  org_id    number not null,
  tcd_ordre number not null,
  tap_id    number not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_budget is 'table des infos budgetaires avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_budget.rebu_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_budget.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_budget.eng_id is 'Cle de l''engagement present dans table engage_budget ou z_engage_budget';
COMMENT ON COLUMN jefy_depense.reimputation_budget.org_id is 'Cle etrangere table jefy_admin.organ';
COMMENT ON COLUMN jefy_depense.reimputation_budget.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_depense.reimputation_budget.tap_id is 'Cle etrangere table jefy_admin.taux_prorata';

CREATE UNIQUE INDEX PK_reimputation_budget ON jefy_depense.reimputation_budget (rebu_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_budget ADD (CONSTRAINT PK_reimputation_budget PRIMARY KEY (rebu_id));

ALTER TABLE jefy_depense.reimputation_budget ADD ( CONSTRAINT FK_reimp_budget_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_budget ADD ( CONSTRAINT FK_reimp_budget_org_id FOREIGN KEY (org_id) 
    REFERENCES jefy_admin.organ(org_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_budget ADD ( CONSTRAINT FK_reimp_budget_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit(tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_budget ADD ( CONSTRAINT FK_reimp_budget_tap_id FOREIGN KEY (tap_id) 
    REFERENCES jefy_admin.taux_prorata(tap_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_budget_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_action (
  reac_id   number not null,
  reim_id   number not null,
  tyac_id   number not null,
  exe_ordre number not null,
  reac_montant_budgetaire number(12,2) not null,
  reac_ht_saisie  number(12,2) not null,
  reac_tva_saisie number(12,2) not null,
  reac_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_action is 'table de la repartition par action avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_action.reac_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_action.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_action.tyac_id is '';
COMMENT ON COLUMN jefy_depense.reimputation_action.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_depense.reimputation_action.reac_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_action.reac_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_action.reac_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_action.reac_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_action ON jefy_depense.reimputation_action (reac_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_action ADD (CONSTRAINT PK_reimputation_action PRIMARY KEY (reac_id));

ALTER TABLE jefy_depense.reimputation_action ADD ( CONSTRAINT FK_reimp_action_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_action ADD ( CONSTRAINT FK_reimp_action_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice(exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_action_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_analytique (
  rean_id   number not null,
  reim_id   number not null,
  can_id    number not null,
  rean_montant_budgetaire number(12,2) not null,
  rean_ht_saisie  number(12,2) not null,
  rean_tva_saisie number(12,2) not null,
  rean_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_analytique is 'table de la repartition par code analytique avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.rean_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.can_id is 'Cle etrangere table jefy_admin.code_analytique';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.rean_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.rean_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.rean_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_analytique.rean_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_analytique ON jefy_depense.reimputation_analytique (rean_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_analytique ADD (CONSTRAINT PK_reimputation_analytique PRIMARY KEY (rean_id));

ALTER TABLE jefy_depense.reimputation_analytique ADD ( CONSTRAINT FK_reimp_analytique_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_analytique ADD ( CONSTRAINT FK_reimp_analytique_can_id FOREIGN KEY (can_id) 
    REFERENCES jefy_admin.code_analytique(can_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_analytique_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_convention (
  reco_id    number not null,
  reim_id    number not null,
  conv_ordre number not null,
  reco_montant_budgetaire number(12,2) not null,
  reco_ht_saisie  number(12,2) not null,
  reco_tva_saisie number(12,2) not null,
  reco_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_convention is 'table de la repartition par convention avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reco_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_convention.conv_ordre is 'Cle etrangere table accords.convention_non_limitative';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reco_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reco_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reco_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_convention.reco_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_convention ON jefy_depense.reimputation_convention (reco_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_convention ADD (CONSTRAINT PK_reimputation_convention PRIMARY KEY (reco_id));

ALTER TABLE jefy_depense.reimputation_convention ADD ( CONSTRAINT FK_reimp_convention_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_convention_SEQ start with 1 nocycle nocache;

create table jefy_depense.reimputation_hors_marche (
  rehm_id   number not null,
  reim_id   number not null,
  typa_id   number not null,
  ce_ordre  number not null,
  rehm_montant_budgetaire number(12,2) not null,
  rehm_ht_saisie  number(12,2) not null,
  rehm_tva_saisie number(12,2) not null,
  rehm_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_hors_marche is 'table de la repartition par code nomenclature avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.rehm_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.ce_ordre is 'Cle etrangere table jefy_marches.code_exer';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.typa_id is 'Cle etrangere table jefy_depense.type_achat';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.rehm_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.rehm_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.rehm_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_hors_marche.rehm_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_hors_marche ON jefy_depense.reimputation_hors_marche (rehm_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_hors_marche ADD (CONSTRAINT PK_reimputation_hors_marche PRIMARY KEY (rehm_id));

ALTER TABLE jefy_depense.reimputation_hors_marche ADD ( CONSTRAINT FK_reimp_hm_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_hors_marche ADD ( CONSTRAINT FK_reimp_hm_ce_ordre FOREIGN KEY (ce_ordre) 
    REFERENCES jefy_marches.code_exer(ce_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_hors_marche ADD ( CONSTRAINT FK_reimp_hm_typa_id FOREIGN KEY (typa_id) 
    REFERENCES jefy_depense.type_achat(typa_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_hors_marche_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_marche (
  rema_id   number not null,
  reim_id   number not null,
  att_ordre number not null,
  rema_montant_budgetaire number(12,2) not null,
  rema_ht_saisie  number(12,2) not null,
  rema_tva_saisie number(12,2) not null,
  rema_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_marche is 'table de la repartition par action avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_marche.rema_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_marche.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_marche.att_ordre is 'Cle etrangere table jefy_marches.attribution';
COMMENT ON COLUMN jefy_depense.reimputation_marche.rema_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_marche.rema_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_marche.rema_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_marche.rema_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_marche ON jefy_depense.reimputation_marche (rema_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_marche ADD (CONSTRAINT PK_reimputation_marche PRIMARY KEY (rema_id));

ALTER TABLE jefy_depense.reimputation_marche ADD ( CONSTRAINT FK_reimp_marche_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_marche ADD ( CONSTRAINT FK_reimp_marche_att_ordre FOREIGN KEY (att_ordre) 
    REFERENCES jefy_marches.attribution(att_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_marche_SEQ start with 1 nocycle nocache;



create table jefy_depense.reimputation_planco (
  repc_id   number not null,
  reim_id   number not null,
  pco_num   varchar2(20) not null,
  exe_ordre number not null,
  repc_montant_budgetaire number(12,2) not null,
  repc_ht_saisie  number(12,2) not null,
  repc_tva_saisie number(12,2) not null,
  repc_ttc_saisie number(12,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_depense.reimputation_planco is 'table de la repartition par action avant la reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_planco.repc_id is 'Cle';
COMMENT ON COLUMN jefy_depense.reimputation_planco.reim_id is 'Cle etrangere table jefy_depense.reimputation';
COMMENT ON COLUMN jefy_depense.reimputation_planco.pco_num is 'Cle etrangere table maracuja.plan_comptable';
COMMENT ON COLUMN jefy_depense.reimputation_planco.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_depense.reimputation_planco.repc_montant_budgetaire is '';
COMMENT ON COLUMN jefy_depense.reimputation_planco.repc_ht_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_planco.repc_tva_saisie is '';
COMMENT ON COLUMN jefy_depense.reimputation_planco.repc_ttc_saisie is '';

CREATE UNIQUE INDEX PK_reimputation_planco ON jefy_depense.reimputation_planco (repc_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_depense.reimputation_planco ADD (CONSTRAINT PK_reimputation_planco PRIMARY KEY (repc_id));

ALTER TABLE jefy_depense.reimputation_planco ADD ( CONSTRAINT FK_reimp_planco_reim_id FOREIGN KEY (reim_id) 
    REFERENCES jefy_depense.reimputation(reim_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_planco ADD ( CONSTRAINT FK_reimp_planco_pco_num FOREIGN KEY (pco_num) 
    REFERENCES maracuja.plan_comptable(pco_num) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_depense.reimputation_planco ADD ( CONSTRAINT FK_reimp_planco_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice(exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_depense.reimputation_planco_SEQ start with 1 nocycle nocache;

insert into jefy_depense.type_numerotation values (4, 'REIMPUTATION', 'pour le champ REIM_NUMERO');




create or replace view jefy_depense.v_reimputation_new_action
as select a.reac_id, r.reim_id, a.tyac_id, a.exe_ordre, a.reac_montant_budgetaire, reac_ht_saisie, reac_tva_saisie, reac_ttc_saisie
 from reimputation_action a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dact_id, r.reim_id, a.tyac_id, a.exe_ordre, a.dact_montant_budgetaire, a.dact_ht_saisie, a.dact_tva_saisie, a.dact_ttc_saisie
 from depense_ctrl_action a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);
 
create or replace view jefy_depense.v_reimputation_new_analytique
as select a.rean_id, r.reim_id, a.can_id, a.rean_montant_budgetaire, rean_ht_saisie, rean_tva_saisie, rean_ttc_saisie
 from reimputation_analytique a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dana_id, r.reim_id, a.can_id, a.dana_montant_budgetaire, a.dana_ht_saisie, a.dana_tva_saisie, a.dana_ttc_saisie
 from depense_ctrl_analytique a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);
 
create or replace view jefy_depense.v_reimputation_new_budget
as select a.rebu_id, r.reim_id, a.eng_id, a.org_id, a.tcd_ordre, a.tap_id
 from reimputation_budget a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dep_id, r.reim_id, a.eng_id, e.org_id, e.tcd_ordre, a.tap_id
 from depense_budget a, engage_budget e, reimputation r
 where a.dep_id=r.dep_id and a.eng_id=e.eng_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);
 
create or replace view jefy_depense.v_reimputation_new_convention
as select a.reco_id, r.reim_id, a.conv_ordre, a.reco_montant_budgetaire, reco_ht_saisie, reco_tva_saisie, reco_ttc_saisie
 from reimputation_convention a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dcon_id, r.reim_id, a.conv_ordre, a.dcon_montant_budgetaire, a.dcon_ht_saisie, a.dcon_tva_saisie, a.dcon_ttc_saisie
 from depense_ctrl_convention a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);
 
create or replace view jefy_depense.v_reimputation_new_hors_marche
as select a.rehm_id, r.reim_id, a.typa_id, a.ce_ordre, a.rehm_montant_budgetaire, rehm_ht_saisie, rehm_tva_saisie, rehm_ttc_saisie
 from reimputation_hors_marche a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dhom_id, r.reim_id, a.typa_id, a.ce_ordre, a.dhom_montant_budgetaire, a.dhom_ht_saisie, a.dhom_tva_saisie, a.dhom_ttc_saisie
 from depense_ctrl_hors_marche a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);
 
create or replace view jefy_depense.v_reimputation_new_marche
as select a.rema_id, r.reim_id, a.att_ordre, a.rema_montant_budgetaire, rema_ht_saisie, rema_tva_saisie, rema_ttc_saisie
 from reimputation_marche a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dmar_id, r.reim_id, a.att_ordre, a.dmar_montant_budgetaire, a.dmar_ht_saisie, a.dmar_tva_saisie, a.dmar_ttc_saisie
 from depense_ctrl_marche a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);

create or replace view jefy_depense.v_reimputation_new_planco
as select a.repc_id, r.reim_id, a.pco_num, a.exe_ordre, a.repc_montant_budgetaire, repc_ht_saisie, repc_tva_saisie, repc_ttc_saisie
 from reimputation_planco a, reimputation r
 where a.reim_id in (select min(rnew.reim_id) from reimputation rnew where rnew.reim_id>r.reim_id and rnew.dep_id=r.dep_id)
union all
select -a.dpco_id, r.reim_id, a.pco_num, a.exe_ordre, a.dpco_montant_budgetaire, a.dpco_ht_saisie, a.dpco_tva_saisie, a.dpco_ttc_saisie
 from depense_ctrl_planco a, reimputation r
 where a.dep_id=r.dep_id and r.reim_id in (select max(rnew.reim_id) from reimputation rnew where rnew.dep_id=r.dep_id);




CREATE OR REPLACE PACKAGE JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2);

PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2);

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type);

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type);
      
FUNCTION subdivise_engage_pour_depense(
      a_dep_id depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type)
return number;

FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type)
return number;

PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type);

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type)
return number;
      
PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type);

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type);
      
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type);

PROCEDURE rempli_reimputation(
      a_dep_id    depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre depense_budget.utl_ordre%type);
      
END; 
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.reimputer  IS

PROCEDURE depense (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_reim_libelle          reimputation.reim_libelle%type,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_chaine_action         VARCHAR2,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche         VARCHAR2
   ) IS
      my_depense       depense_budget%rowtype;
      my_engage        engage_budget%rowtype;
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id;
     select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
    
     rempli_reimputation(a_dep_id, a_reim_libelle, a_utl_ordre);
   
     if a_org_id is not null and my_engage.org_id<>a_org_id then
       depense_organ(a_dep_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre, a_chaine_analytique, a_chaine_convention);
     else
       if a_chaine_analytique is not null then 
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
       end if;
       if a_chaine_convention is not null then 
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
       end if;
       if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
       end if;
       if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
       end if;
     end if;
     
     if a_chaine_action is not null then 
       depense_action(a_dep_id, a_chaine_action, a_utl_ordre);
     end if;

     if a_chaine_hors_marche is not null then 
       depense_hors_marche(a_dep_id, a_chaine_hors_marche, a_utl_ordre);
     end if;

     if a_chaine_marche is not null then 
       depense_marche(a_dep_id, a_chaine_marche, a_utl_ordre);
     end if;

     if a_pco_num is not null then 
       depense_planco(a_dep_id, a_pco_num, a_utl_ordre);
     end if;
   END;
   
PROCEDURE depense_organ (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_org_id                engage_budget.org_id%type,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type,
      a_chaine_analytique     VARCHAR2,
      a_chaine_convention     VARCHAR2
   ) IS
      my_nb        integer;
      my_eng_id    engage_budget.eng_id%type;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_tap_id    engage_budget.tap_id%type;
   BEGIN
--- changement de la ligne budg�taire
--    * d�pense sans inventaire : si la d�pense est mandat�e, on choisit une ligne budg�taire de la m�me UB, sinon toutes les lignes, dans la limite que le disponible le permette.
--    * d�pense avec inventaire : suivant un param�tre a mettre en place, m�me limitation que la d�pense sans inventaire ou restriction au niveau du CR (puisque le code inventaire prend en compte le CR)
--    * le changement de ligne peut impliquer la modification des conventions et des codes analytiques �ventuellement associ�s � cette d�pense    cf. changement convention
--    * le changement de ligne peut impliquer une cr�ation d'un nouvel engagement, si l'engagement concerne d'autres liquidations (hors ORVs li�s a celle qu'on r�-impute) par exemple
--    * si la d�pense r�-imput�e poss�de un ou plusieurs ORVs on modifie aussi l'ORV et inversement ..   

      select * into my_depense from depense_budget where dep_id=a_dep_id; 
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;

      -- si la ligne budgetaire a change
      if my_engage.org_id<>a_org_id then
        my_tcd_ordre:=a_tcd_ordre;
        if my_tcd_ordre is null then my_tcd_ordre:=my_engage.tcd_ordre; end if;

        my_tap_id:=a_tap_id;
        if my_tap_id is null then my_tap_id:=my_depense.tap_id; end if;
        
        verifier.verifier_budget(my_depense.exe_ordre, my_tap_id, a_org_id, my_tcd_ordre);
        verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, a_org_id);

        -- si avec inventaire -> CR
        select count(*) into my_nb from v_inventaire where dpco_id in (select dpco_id from depense_ctrl_planco where dep_id in (a_dep_id));
        if my_nb>0 then
          select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
            and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub and dest.org_cr=orig.org_cr;
          if my_nb=0 then
            raise_application_error(-20001, 'cette depense comporte des inventaires, on ne peut reimputer que dans le meme CR');
          end if;
        else
          -- la depense est mandatee -> restriction de la nouvelle ligne a la meme UB que l'ancienne
          select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
          if my_nb>0 then
            select count(*) into my_nb from jefy_admin.organ orig, jefy_admin.organ dest where dest.org_id=a_org_id and orig.org_id=my_engage.org_id 
              and dest.org_univ=orig.org_univ and dest.org_etab=orig.org_etab and dest.org_ub=orig.org_ub;
            if my_nb=0 then
              raise_application_error(-20001, 'cette depense est mandatee, on ne peut reimputer que dans la meme UB');
            end if;
          end if;
        end if;
        
        -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
        my_eng_id:=subdivise_engage_pour_depense(a_dep_id, a_org_id, my_tcd_ordre, my_tap_id, a_utl_ordre);
        
      end if;
      
      if a_chaine_analytique is not null then
         depense_analytique(a_dep_id, a_chaine_analytique, a_utl_ordre);
      end if;
      
      if a_chaine_convention is not null then
         depense_convention(a_dep_id, a_chaine_convention, a_utl_ordre);
      end if;

      if a_tcd_ordre is not null and a_tcd_ordre<>my_engage.tcd_ordre then
         depense_type_credit(a_dep_id, a_tcd_ordre, a_utl_ordre);
      end if;

      if a_tap_id is not null and a_tap_id<>my_depense.tap_id then
         depense_taux_prorata(a_dep_id, a_tap_id, a_utl_ordre);
      end if;
   END;

PROCEDURE depense_type_credit (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tcd_ordre             engage_budget.tcd_ordre%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
   BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer le type de credit de la depense');
   END;

PROCEDURE depense_taux_prorata (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_tap_id                engage_budget.tap_id%type,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_depense   depense_budget%rowtype;
      my_engage    engage_budget%rowtype;
      my_eng_id    engage_budget.eng_id%type;
      my_par_value parametre.par_value%type;
      
      my_dep_id    depense_budget.dep_id%type;
      cursor orvs is select dep_id from depense_budget where dep_id_reversement=a_dep_id;
   BEGIN
      select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
      if my_nb>0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier le taux de prorata d''une depense mandatee');
      end if;
      
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      my_eng_id:=my_engage.eng_id;

      verifier.verifier_depense_exercice(my_depense.exe_ordre, a_utl_ordre, my_engage.org_id);

      -- on ne peut reimputer que les depenses pas les ORvs pour le moment
        -- les orvs rattaches a cette depense seront mis a jour avec les memes infos
      if my_depense.dep_ttc_saisie<0 then
        RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas reimputer un ORV, il faut reimputer la depense d''origine');
      end if;
            
      -- on verifie si le nouveau taux de prorata est autorise pour cette ligne budgetaire --
      verifier.verifier_budget(my_depense.exe_ordre, a_tap_id, my_engage.org_id, my_engage.tcd_ordre);
      
      select par_value into my_par_value from parametre where par_key='DEPENSE_IDEM_TAP_ID' and exe_ordre=my_depense.exe_ordre;
      if my_par_value='OUI' then
      
         -- on subdivise eventuellement l'ancien engagement et on travaille sur le nouveau
         my_eng_id:=subdivise_engage_pour_depense(a_dep_id, my_engage.org_id, my_engage.tcd_ordre, a_tap_id, a_utl_ordre);
         
      end if;
      
      update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=a_dep_id;
      corrige_depense_budgetaire(a_dep_id);
      
      open orvs();
      loop
         fetch  orvs into my_dep_id;
         exit when orvs%notfound;
         
         update depense_budget set tap_id=a_tap_id, utl_ordre=a_utl_ordre where dep_id=my_dep_id;
         corrige_depense_budgetaire(my_dep_id);
      end loop;
      close orvs;
      
      budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
      
   END;

PROCEDURE depense_action (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_action where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_action(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_analytique (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_analytique where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_analytique(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_convention (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_exe_ordre       depense_budget.exe_ordre%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     delete from depense_ctrl_convention where dep_id=a_dep_id;
     liquider.ins_depense_ctrl_convention(my_exe_ordre, a_dep_id, a_chaine);
     Verifier.verifier_depense_coherence(a_dep_id);
   END;

PROCEDURE depense_hors_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est rattachee a une attribution on ne peut pas la transformer en hors marche');
       end if;

       select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
       delete from depense_ctrl_hors_marche where dep_id=a_dep_id;
       liquider.ins_depense_ctrl_hors_marche(my_exe_ordre, a_dep_id, a_chaine);
       Verifier.verifier_depense_coherence(a_dep_id);
     end if;     
   END;

PROCEDURE depense_marche (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_chaine                VARCHAR2,
      a_utl_ordre             depense_budget.utl_ordre%type
   ) IS
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_att_ordre depense_ctrl_marche.att_ordre%type;
      my_old_att_ordre depense_ctrl_marche.att_ordre%type;
   BEGIN
     if a_chaine is not null and a_chaine<>'$' then
       select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
       if my_nb>0 then
          RAISE_APPLICATION_ERROR(-20001, 'la depense est hors marche on ne peut pas la transformer en marche');
       end if;

       SELECT grhum.en_nombre(SUBSTR(a_chaine,1,INSTR(a_chaine,'$')-1)) INTO my_att_ordre FROM dual;
       select nvl(att_ordre,0) into my_old_att_ordre from depense_ctrl_marche where dep_id=a_dep_id;
       
       if my_old_att_ordre<>my_att_ordre then 
          RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas changer l''attribution de la depense');
       end if;
     end if;
   END;

procedure depense_planco (
      a_dep_id                DEPENSE_BUDGET.dep_id%TYPE,
      a_pco_num               depense_ctrl_planco.pco_num%type,
      a_utl_ordre             depense_budget.utl_ordre%type
  ) is
      my_nb        integer;
      my_exe_ordre depense_budget.exe_ordre%type;
      my_org_id    engage_budget.org_id%type;
      my_tcd_ordre engage_budget.tcd_ordre%type;
      my_pco_num   depense_ctrl_planco.pco_num%type;
  begin 
      select nvl(pco_num,'0') into my_pco_num from depense_ctrl_planco where dep_id=a_dep_id;
      if my_pco_num<>a_pco_num then 
         select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id and man_id is not null;
         if my_nb>0 then
            RAISE_APPLICATION_ERROR(-20001, 'on ne peut pas modifier l''imputation comptable d''une depense mandatee');
         end if;
         update depense_ctrl_planco set pco_num=a_pco_num where dep_id=a_dep_id;
      end if;

      select d.exe_ordre, e.org_id, e.tcd_ordre into my_exe_ordre, my_org_id, my_tcd_ordre 
        from depense_budget d, engage_budget e where d.eng_id=e.eng_id and d.dep_id=a_dep_id;
      Verifier.verifier_planco(my_exe_ordre, my_org_id, my_tcd_ordre, a_pco_num, a_utl_ordre);
      Verifier.verifier_depense_coherence(a_dep_id);
  end;

FUNCTION subdivise_engage_pour_depense(
      a_dep_id       depense_budget.dep_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
      my_nb          integer;
      bool_creation  integer;
      my_comm_id     commande.comm_id%type;
      my_cbud_id     commande_budget.cbud_id%type;
      my_old_commande_budget commande_budget%rowtype;
      my_commande_budget commande_budget%rowtype;
      my_ht          engage_budget.eng_ht_saisie%type;
      my_tva         engage_budget.eng_tva_saisie%type;
      my_ttc         engage_budget.eng_ttc_saisie%type;
      my_bud         engage_budget.eng_montant_budgetaire%type;
      my_depense     depense_budget%rowtype;
      my_engage      engage_budget%rowtype;
      my_new_engage  engage_budget%rowtype;
      my_eng_id      engage_budget.eng_id%type;
   BEGIN
      select * into my_depense from depense_budget where dep_id=a_dep_id;
      my_eng_id:=my_depense.eng_id;
      select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
      
      -- si il n'y a pas d'autre depense sur cette engagement on n'en cr�e pas de nouveau 
      select count(*) into bool_creation from depense_budget where eng_id=my_depense.eng_id and dep_id<>a_dep_id and dep_ttc_saisie>0;
      
      -- on regarde si l'engagement appartient a une commande
      my_comm_id:=null;
      select count(*) into my_nb from commande_engagement where eng_id=my_depense.eng_id;
      if my_nb>0 then
         select min(comm_id) into my_comm_id from commande_engagement where eng_id=my_depense.eng_id;
      end if;
   
      if bool_creation=0 then
         -- on modifie juste l'engagement et enventuellement les infos de la commande
   
         if my_comm_id is not null then 
            select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
            select count(*) into my_nb from commande_budget 
              where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
            if my_nb>0 then  
               select min(cbud_id) into my_cbud_id from commande_budget 
                 where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
               update commande_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id where cbud_id=my_cbud_id;
            
               --si le taux de prorata a change, on met a jour les montants budgetaires  
               if my_engage.tap_id<>a_tap_id then
                  update commande_budget set cbud_montant_budgetaire=Budget.calculer_budgetaire(my_engage.exe_ordre,a_tap_id,a_org_id, cbud_ht_saisie, cbud_ttc_saisie)
                    where cbud_id=my_cbud_id;
                  corriger.corriger_commande_ctrl(my_cbud_id);
                  verifier.verifier_cde_budget_coherence(my_cbud_id);
               end if;
            end if;
         end if;
         
         -- on log l'ancien engagement
         engager.log_engage(my_eng_id, a_utl_ordre);
                  
         -- modif engagement
         update engage_budget set org_id=a_org_id, tcd_ordre=a_tcd_ordre, tap_id=a_tap_id, utl_ordre=a_utl_ordre where eng_id=my_eng_id;

         if my_engage.tap_id<>a_tap_id then
            -- modification des montants budgetaires   
            corrige_engage_budgetaire(my_eng_id);         
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre then 
            -- si on a chang� de ligne budgetaire on met a jour l'"ancien" budget
            budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
         end if;
         
         if my_engage.org_id<>a_org_id or my_engage.tcd_ordre<>a_tcd_ordre or my_engage.tap_id<>a_tap_id then
            -- si le taux de prorata ou la ligne budgetaire a change on met a jour le budget
            budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);
         end if;
         
      else

        my_nb:=0;
      
        -- si l'engagement appartient a une commande on regarde si un engagement avec les nouvelles infos existe deja 
        if my_comm_id is not null then
           select count(*) into my_nb from commande_engagement ce, engage_budget e
             where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
           if my_nb>0 then
             select min(e.eng_id) into my_eng_id from commande_engagement ce, engage_budget e
               where ce.eng_id=e.eng_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id and ce.comm_id=my_comm_id;
             
             -- on augmente les montants de l'engagement avec les montants de la depense
             augmenter_engage(a_dep_id, my_eng_id);
             
             -- on augmente la commande_bugdet correspondante (normalement elle existe)
             select * into my_new_engage from engage_budget where eng_id=my_eng_id;
             select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
             if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id and org_id=a_org_id and tcd_ordre=a_tcd_ordre and tap_id=a_tap_id;
                update commande_budget set cbud_montant_budgetaire=my_new_engage.eng_montant_budgetaire, cbud_ht_saisie=my_new_engage.eng_ht_saisie,
                   cbud_tva_saisie=my_new_engage.eng_tva_saisie, cbud_ttc_saisie=my_new_engage.eng_ttc_saisie where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
             end if;

           else
             -- on cree le nouvel engagement avec les infos de la depense
             my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);
             insert into commande_engagement select commande_engagement_seq.nextval, my_comm_id, my_eng_id from dual;
             
             -- on cree la commande_budget correspondante
             my_cbud_id:=creer_commande_budget(my_eng_id, my_comm_id);
           end if;
           
           -- on diminue l'ancienne commande_budget
           select count(*) into my_nb from commande_budget where comm_id=my_comm_id and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
           if my_nb>0 then
                select min(cbud_id) into my_cbud_id from commande_budget where comm_id=my_comm_id 
                   and org_id=my_engage.org_id and tcd_ordre=my_engage.tcd_ordre and tap_id=my_engage.tap_id;
                   
                select * into my_new_engage from engage_budget where eng_id=my_eng_id;
                select * into my_commande_budget from commande_budget where cbud_id=my_cbud_id;
                
                my_ht :=my_commande_budget.cbud_ht_saisie -my_new_engage.eng_ht_saisie;
                if my_ht<0 then my_ht:=0; end if;
                my_tva:=my_commande_budget.cbud_tva_saisie-my_new_engage.eng_tva_saisie;
                if my_tva<0 then my_tva:=0; end if;
                my_ttc:=my_commande_budget.cbud_ttc_saisie-my_new_engage.eng_ttc_saisie;
                if my_ttc<0 then my_ttc:=0; end if;
                my_bud:=my_commande_budget.cbud_montant_budgetaire-my_new_engage.eng_montant_budgetaire;
                if my_bud<0 then my_bud:=0; end if;
                
                update commande_budget set cbud_montant_budgetaire=my_bud, cbud_ht_saisie=my_ht,
                   cbud_tva_saisie=my_tva, cbud_ttc_saisie=my_ttc where cbud_id=my_cbud_id;
                corriger.corriger_commande_ctrl(my_cbud_id);
                verifier.verifier_cde_budget_coherence(my_cbud_id);
           end if; 

        else
           my_eng_id:=creer_engage(a_dep_id, my_engage.fou_ordre, my_engage.eng_libelle, my_engage.tyap_id, a_org_id, a_tcd_ordre, a_tap_id, a_utl_ordre);       
        end if;

        -- on diminue l'ancien engagement du montant de la depense a reimputer
        -- ATTENTION : on ne diminue pas le budgetaire reste car on enleve la liquidation de cet engagement, et donc le reste ne change pas 
        select * into my_engage from engage_budget where eng_id=my_depense.eng_id;
        my_ht :=my_engage.eng_ht_saisie -my_depense.dep_ht_saisie;
        if my_ht<0 then my_ht:=0; end if;
        my_tva:=my_engage.eng_tva_saisie-my_depense.dep_tva_saisie;
        if my_tva<0 then my_tva:=0; end if;
        my_ttc:=my_engage.eng_ttc_saisie-my_depense.dep_ttc_saisie;
        if my_ttc<0 then my_ttc:=0; end if;
        my_bud:=my_engage.eng_montant_budgetaire-my_depense.dep_montant_budgetaire;
        if my_bud<0 then my_bud:=0; end if;
        if my_bud<my_engage.eng_montant_budgetaire_reste then my_bud:=my_engage.eng_montant_budgetaire_reste; end if;
  
        update engage_budget set eng_ht_saisie=my_ht, eng_tva_saisie=my_tva, eng_ttc_saisie=my_ttc, eng_montant_budgetaire=my_bud
          where eng_id=my_engage.eng_id;
        corrige_engage_repartition(my_engage.eng_id);
        
        -- on associe la depense au nouvel engagement
        update depense_budget set eng_id=my_eng_id where dep_id=a_dep_id;
                     
        -- on lance la correction du budget
        budget.maj_budget(my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre);
        budget.maj_budget(my_engage.exe_ordre, a_org_id, a_tcd_ordre);

      end if;
      
      return my_eng_id;
   END;
   
FUNCTION creer_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_fou_ordre    engage_budget.fou_ordre%type,
      a_eng_libelle  engage_budget.eng_libelle%type,
      a_tyap_id      engage_budget.tyap_id%type,
      a_org_id       engage_budget.org_id%type,
      a_tcd_ordre    engage_budget.tcd_ordre%type,
      a_tap_id       engage_budget.tap_id%type,
      a_utl_ordre    depense_budget.utl_ordre%type) return number
   IS
     my_eng_id      engage_budget.eng_id%type;
     my_depense     depense_budget%rowtype;      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
   
     select engage_budget_seq.nextval into my_eng_id from dual;
     insert into engage_budget values (my_eng_id, my_depense.exe_ordre, Get_Numerotation(my_depense.exe_ordre, NULL, null,'ENGAGE_BUDGET'),
       a_fou_ordre, a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, 0, 0, 0, 0, 0, sysdate, a_tyap_id, a_utl_ordre);
     
     augmenter_engage(a_dep_id,my_eng_id);
          
     return my_eng_id;
   END;
      
PROCEDURE augmenter_engage(
      a_dep_id       depense_budget.dep_id%type,
      a_eng_id       engage_budget.eng_id%type)
   IS
     my_nb          integer;
     my_id          engage_ctrl_action.eact_id%type;
     my_engage      engage_budget%rowtype;
     my_depense     depense_budget%rowtype;
      
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
      
   BEGIN
     select * into my_depense from depense_budget where dep_id=a_dep_id; 
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     update engage_budget set eng_montant_budgetaire=eng_montant_budgetaire+my_depense.dep_montant_budgetaire, eng_ht_saisie=eng_ht_saisie+my_depense.dep_ht_saisie, 
       eng_tva_saisie=eng_tva_saisie+my_depense.dep_tva_saisie, eng_ttc_saisie=eng_ttc_saisie+my_depense.dep_ttc_saisie where eng_id=a_eng_id;
     
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
          select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id; 
          if my_nb=0 then 
             insert into engage_ctrl_action select engage_ctrl_action_seq.nextval, row_action.exe_ordre, a_eng_id, row_action.tyac_id,
                row_action.dact_montant_budgetaire, 0, row_action.dact_ht_saisie, row_action.dact_tva_saisie, row_action.dact_ttc_saisie, sysdate from dual;
          else
             select min(eact_id) into my_id from engage_ctrl_action where eng_id=a_eng_id and tyac_id=row_action.tyac_id;
             update engage_ctrl_action set eact_montant_budgetaire=eact_montant_budgetaire+row_action.dact_montant_budgetaire,
               eact_ht_saisie=eact_ht_saisie+row_action.dact_ht_saisie, eact_tva_saisie=eact_tva_saisie+row_action.dact_tva_saisie,
               eact_ttc_saisie=eact_ttc_saisie+row_action.dact_ttc_saisie where eact_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
          select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id; 
          if my_nb=0 then 
            insert into engage_ctrl_analytique select engage_ctrl_analytique_seq.nextval, row_analytique.exe_ordre, a_eng_id, row_analytique.can_id,
               row_analytique.dana_montant_budgetaire, 0, row_analytique.dana_ht_saisie, row_analytique.dana_tva_saisie, row_analytique.dana_ttc_saisie, 
               sysdate from dual;
          else
             select min(eana_id) into my_id from engage_ctrl_analytique where eng_id=a_eng_id and can_id=row_analytique.can_id;
             update engage_ctrl_analytique set eana_montant_budgetaire=eana_montant_budgetaire+row_analytique.dana_montant_budgetaire,
               eana_ht_saisie=eana_ht_saisie+row_analytique.dana_ht_saisie, eana_tva_saisie=eana_tva_saisie+row_analytique.dana_tva_saisie,
               eana_ttc_saisie=eana_ttc_saisie+row_analytique.dana_ttc_saisie where eana_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
          select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_convention select engage_ctrl_convention_seq.nextval, row_convention.exe_ordre, a_eng_id, row_convention.conv_ordre,
                row_convention.dcon_montant_budgetaire, 0, row_convention.dcon_ht_saisie, row_convention.dcon_tva_saisie, row_convention.dcon_ttc_saisie, 
                sysdate from dual;
          else
             select min(econ_id) into my_id from engage_ctrl_convention where eng_id=a_eng_id and conv_ordre=row_convention.conv_ordre;
             update engage_ctrl_convention set econ_montant_budgetaire=econ_montant_budgetaire+row_convention.dcon_montant_budgetaire,
               econ_ht_saisie=econ_ht_saisie+row_convention.dcon_ht_saisie, econ_tva_saisie=econ_tva_saisie+row_convention.dcon_tva_saisie,
               econ_ttc_saisie=econ_ttc_saisie+row_convention.dcon_ttc_saisie where econ_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
          select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id; 
          if my_nb=0 then 
             insert into engage_ctrl_hors_marche select engage_ctrl_hors_marche_seq.nextval, row_hors_marche.exe_ordre, a_eng_id, row_hors_marche.typa_id, 
                row_hors_marche.ce_ordre, row_hors_marche.dhom_montant_budgetaire, 0,0, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_tva_saisie, 
                row_hors_marche.dhom_ttc_saisie, sysdate from dual;
          else
             select min(ehom_id) into my_id from engage_ctrl_hors_marche where eng_id=a_eng_id and ce_ordre=row_hors_marche.ce_ordre and typa_id=row_hors_marche.typa_id;
             update engage_ctrl_hors_marche set ehom_montant_budgetaire=ehom_montant_budgetaire+row_hors_marche.dhom_montant_budgetaire,
               ehom_ht_saisie=ehom_ht_saisie+row_hors_marche.dhom_ht_saisie, ehom_tva_saisie=ehom_tva_saisie+row_hors_marche.dhom_tva_saisie,
               ehom_ttc_saisie=ehom_ttc_saisie+row_hors_marche.dhom_ttc_saisie where ehom_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
          select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre; 
          if my_nb=0 then 
             insert into engage_ctrl_marche select engage_ctrl_marche_seq.nextval, row_marche.exe_ordre, a_eng_id, row_marche.att_ordre,
                row_marche.dmar_montant_budgetaire, 0, 0, row_marche.dmar_ht_saisie, row_marche.dmar_tva_saisie, row_marche.dmar_ttc_saisie, sysdate from dual;
          else
             select min(emar_id) into my_id from engage_ctrl_marche where eng_id=a_eng_id and att_ordre=row_marche.att_ordre;
             update engage_ctrl_marche set emar_montant_budgetaire=emar_montant_budgetaire+row_marche.dmar_montant_budgetaire,
               emar_ht_saisie=emar_ht_saisie+row_marche.dmar_ht_saisie, emar_tva_saisie=emar_tva_saisie+row_marche.dmar_tva_saisie,
               emar_ttc_saisie=emar_ttc_saisie+row_marche.dmar_ttc_saisie where emar_id=my_id;
          end if;
       end loop;
     end if;

     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then
       for row_planco in planco
       loop
          select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num; 
          if my_nb=0 then 
             insert into engage_ctrl_planco select engage_ctrl_planco_seq.nextval, row_planco.exe_ordre, a_eng_id, row_planco.pco_num,
                row_planco.dpco_montant_budgetaire, 0, row_planco.dpco_ht_saisie, row_planco.dpco_tva_saisie, row_planco.dpco_ttc_saisie, sysdate from dual;
          else
             select min(epco_id) into my_id from engage_ctrl_planco where eng_id=a_eng_id and pco_num=row_planco.pco_num;
             update engage_ctrl_planco set epco_montant_budgetaire=epco_montant_budgetaire+row_planco.dpco_montant_budgetaire,
               epco_ht_saisie=epco_ht_saisie+row_planco.dpco_ht_saisie, epco_tva_saisie=epco_tva_saisie+row_planco.dpco_tva_saisie,
               epco_ttc_saisie=epco_ttc_saisie+row_planco.dpco_ttc_saisie where epco_id=my_id;
          end if;
       end loop;
     end if;

     verifier.VERIFIER_ENGAGE_COHERENCE(a_eng_id);
   END;

FUNCTION creer_commande_budget(
      a_eng_id       depense_budget.dep_id%type,
      a_comm_id      commande.comm_id%type) return number
   IS
     my_cbud_id       commande_budget.cbud_id%type;
     my_engage        engage_budget%rowtype;
     
     my_nb integer;
     my_pourcentage number(15,5);
     my_total_pourcentage number(15,5);
     
     my_chaine_action varchar2(3000);
     my_chaine_analytique varchar2(3000);
     my_chaine_convention varchar2(3000);
     my_chaine_hors_marche varchar2(3000);
     my_chaine_marche varchar2(3000);
     my_chaine_planco varchar2(3000);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;

   BEGIN
     select * into my_engage from engage_budget where eng_id=a_eng_id; 
   
     -- on definit les infos, notamment les pourcentages
     my_chaine_action:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_action.eact_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_action:=my_chaine_action||row_action.tyac_id||'$'||row_action.eact_ht_saisie||'$'||row_action.eact_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_action:=my_chaine_action||'$';

     my_chaine_analytique:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         my_pourcentage:=round(100*row_analytique.eana_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_analytique:=my_chaine_analytique||row_analytique.can_id||'$'||row_analytique.eana_ht_saisie||'$'
               ||row_analytique.eana_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_analytique:=my_chaine_analytique||'$';

     my_chaine_convention:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         my_pourcentage:=round(100*row_convention.econ_ttc_saisie/my_engage.eng_ttc_saisie,5);
         if my_pourcentage+my_total_pourcentage>100 then
            my_pourcentage:=100.0-my_total_pourcentage;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_convention:=my_chaine_convention||row_convention.conv_ordre||'$'||row_convention.econ_ht_saisie||'$'
              ||row_convention.econ_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_convention:=my_chaine_convention||'$';

     my_chaine_hors_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
           my_chaine_hors_marche:=my_chaine_hors_marche||row_hors_marche.typa_id||'$'||row_hors_marche.ce_ordre||'$'
              ||row_hors_marche.ehom_ht_saisie||'$'||row_hors_marche.ehom_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_hors_marche:=my_chaine_hors_marche||'$';

     my_chaine_marche:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
           my_chaine_marche:=my_chaine_marche||row_marche.att_ordre||'$'||row_marche.emar_ht_saisie||'$'||row_marche.emar_ttc_saisie||'$';
       end loop;
     end if;
     my_chaine_marche:=my_chaine_marche||'$';

     my_chaine_planco:='';
     my_total_pourcentage:=0;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            my_pourcentage:=100.0-my_total_pourcentage;
         else
            my_pourcentage:=round(100*row_planco.epco_ttc_saisie/my_engage.eng_ttc_saisie,5);
            if my_pourcentage+my_total_pourcentage>100 then
               my_pourcentage:=100.0-my_total_pourcentage;
            end if;
         end if;
         
         my_total_pourcentage:=my_total_pourcentage+my_pourcentage;
         if my_pourcentage>0 then
            my_chaine_planco:=my_chaine_planco||row_planco.pco_num||'$'||row_planco.epco_ht_saisie||'$'||row_planco.epco_ttc_saisie||'$'||my_pourcentage||'$';
         end if;
       end loop;
     end if;
     my_chaine_planco:=my_chaine_planco||'$';
     
     -- on cree
     select commande_budget_seq.nextval into my_cbud_id from dual;
     commander.ins_commande_budget (my_cbud_id, a_comm_id, my_engage.exe_ordre, my_engage.org_id, my_engage.tcd_ordre, my_engage.tap_id, my_engage.eng_ht_saisie,
	   my_engage.eng_ttc_saisie, my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_hors_marche, my_chaine_marche, my_chaine_planco);
               
     -- on corrige pour calculer les montants des repartitions
     corriger.corriger_commande_ctrl(my_cbud_id);
     verifier.verifier_cde_budget_coherence(my_cbud_id);
     
     
     return my_cbud_id;
   END;

PROCEDURE corrige_engage_budgetaire(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;
     my_eng_montant_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_reste engage_budget.eng_montant_budgetaire%type;
     my_ctrl_bud engage_budget.eng_montant_budgetaire%type;
     my_dep_budgetaire depense_budget.dep_montant_budgetaire%type;
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_marche engage_ctrl_marche%rowtype;
     cursor marche is select * from engage_ctrl_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant engage_budget
     select * into my_engage from engage_budget where eng_id=a_eng_id;
    
     my_eng_montant_budgetaire:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id,
		 my_engage.org_id, my_engage.eng_ht_saisie, my_engage.eng_ttc_saisie);
     
     select nvl(SUM(Budget.calculer_budgetaire(exe_ordre, my_engage.tap_id, my_engage.org_id, dep_ht_saisie, dep_ttc_saisie)),0) 
        into my_dep_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;
     my_reste:=my_eng_montant_budgetaire-my_dep_budgetaire;
     if my_reste<0 then my_reste:=0; end if;
     
     update engage_budget set eng_montant_budgetaire=my_eng_montant_budgetaire, eng_montant_budgetaire_reste=my_reste where eng_id=a_eng_id;

     -- maj montant engage_ctrl_action
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update engage_ctrl_action set eact_montant_budgetaire=my_reste, eact_montant_budgetaire_reste=my_reste  where eact_id=row_action.eact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_action.eact_ht_saisie, row_action.eact_ttc_saisie);

            update engage_ctrl_action set eact_montant_budgetaire=my_ctrl_bud, eact_montant_budgetaire_reste=my_ctrl_bud where eact_id=row_action.eact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_analytique
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update engage_ctrl_analytique set eana_montant_budgetaire=my_reste, eana_montant_budgetaire_reste=my_reste  where eana_id=row_analytique.eana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_analytique.eana_ht_saisie, row_analytique.eana_ttc_saisie);

            update engage_ctrl_analytique set eana_montant_budgetaire=my_ctrl_bud, eana_montant_budgetaire_reste=my_ctrl_bud where eana_id=row_analytique.eana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(a_eng_id);
     end if;

     -- maj montant engage_ctrl_convention
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update engage_ctrl_convention set econ_montant_budgetaire=my_reste, econ_montant_budgetaire_reste=my_reste  where econ_id=row_convention.econ_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_convention.econ_ht_saisie, row_convention.econ_ttc_saisie);

            update engage_ctrl_convention set econ_montant_budgetaire=my_ctrl_bud, econ_montant_budgetaire_reste=my_ctrl_bud where econ_id=row_convention.econ_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(a_eng_id);
     end if;

     -- maj montant engage_ctrl_hors_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste, ehom_montant_budgetaire_reste=my_reste  where ehom_id=row_hors_marche.ehom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_hors_marche.ehom_ht_saisie, row_hors_marche.ehom_ttc_saisie);

            update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_ctrl_bud, ehom_montant_budgetaire_reste=my_ctrl_bud where ehom_id=row_hors_marche.ehom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(a_eng_id);
     end if;

     -- maj montant engage_ctrl_marche
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update engage_ctrl_marche set emar_montant_budgetaire=my_reste, emar_montant_budgetaire_reste=my_reste  where emar_id=row_marche.emar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_marche.emar_ht_saisie, row_marche.emar_ttc_saisie);

            update engage_ctrl_marche set emar_montant_budgetaire=my_ctrl_bud, emar_montant_budgetaire_reste=my_ctrl_bud where emar_id=row_marche.emar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(a_eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_eng_montant_budgetaire;
     select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update engage_ctrl_planco set epco_montant_budgetaire=my_reste, epco_montant_budgetaire_reste=my_reste  where epco_id=row_planco.epco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_engage.exe_ordre, my_engage.tap_id, my_engage.org_id, row_planco.epco_ht_saisie, row_planco.epco_ttc_saisie);

            update engage_ctrl_planco set epco_montant_budgetaire=my_ctrl_bud, epco_montant_budgetaire_reste=my_ctrl_bud where epco_id=row_planco.epco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(a_eng_id);
     end if;

     verifier.verifier_engage_coherence(a_eng_id);
   END;

PROCEDURE corrige_engage_repartition(
      a_eng_id    engage_budget.eng_id%type
   ) 
   IS
     my_engage engage_budget%rowtype;

     my_montant_ht_total engage_budget.eng_ht_saisie%type;
     my_montant_ttc_total engage_budget.eng_ttc_saisie%type;
     my_montant_budgetaire_total engage_budget.eng_montant_budgetaire%type;
     
     my_ht engage_budget.eng_ht_saisie%type;
     my_ttc engage_budget.eng_ttc_saisie%type;
     my_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_pourcentage number (12,8);
          
     my_reste_ht engage_budget.eng_ht_saisie%type;
     my_reste_ttc engage_budget.eng_ttc_saisie%type;
     my_reste_budgetaire engage_budget.eng_montant_budgetaire%type;
     my_total_pourcentage number (12,8);
     
     row_action engage_ctrl_action%rowtype;
     cursor action is select * from engage_ctrl_action where eng_id=a_eng_id;
     row_analytique engage_ctrl_analytique%rowtype;
     cursor analytique is select * from engage_ctrl_analytique where eng_id=a_eng_id;
     row_convention engage_ctrl_convention%rowtype;
     cursor convention is select * from engage_ctrl_convention where eng_id=a_eng_id;
     row_hors_marche engage_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from engage_ctrl_hors_marche where eng_id=a_eng_id;
     row_planco engage_ctrl_planco%rowtype;
     cursor planco is select * from engage_ctrl_planco where eng_id=a_eng_id;
     
     my_nb integer;
     my_nb_decimales        NUMBER;
   BEGIN

       -- infos references
       select * into my_engage from engage_budget where eng_id=a_eng_id;
       my_nb_decimales:=liquider_outils.get_nb_decimales(my_engage.exe_ordre);
        
       select sum(eact_ht_saisie), sum(eact_montant_budgetaire), sum(eact_ttc_saisie)
          into my_montant_ht_total, my_montant_budgetaire_total, my_montant_ttc_total from engage_ctrl_action where eng_id=a_eng_id;
   
   
       -- maj montant engage_ctrl_action       
       select count(*) into my_nb from engage_ctrl_action where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_action in action
         loop
           if action%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_action set eact_montant_budgetaire=my_reste_budgetaire, eact_ht_saisie=my_reste_ht,
                 eact_ttc_saisie=my_reste_ttc,  eact_tva_saisie=my_reste_ttc-my_reste_ht where eact_id=row_action.eact_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_action.eact_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_action.eact_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_action.eact_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_action set eact_montant_budgetaire=my_budgetaire, eact_ht_saisie=my_ht,
                 eact_ttc_saisie=my_ttc, eact_tva_saisie=my_ttc-my_ht where eact_id=row_action.eact_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_analytique       
       select count(*) into my_nb from engage_ctrl_analytique where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(eana_ht_saisie) into my_total_pourcentage from engage_ctrl_analytique where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_analytique in analytique
         loop
           if analytique%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_analytique set eana_montant_budgetaire=my_reste_budgetaire, eana_ht_saisie=my_reste_ht,
                 eana_ttc_saisie=my_reste_ttc,  eana_tva_saisie=my_reste_ttc-my_reste_ht where eana_id=row_analytique.eana_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_analytique.eana_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_analytique.eana_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_analytique.eana_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_analytique set eana_montant_budgetaire=my_budgetaire, eana_ht_saisie=my_ht,
                 eana_ttc_saisie=my_ttc, eana_tva_saisie=my_ttc-my_ht where eana_id=row_analytique.eana_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_convention
       select count(*) into my_nb from engage_ctrl_convention where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         select sum(econ_ht_saisie) into my_total_pourcentage from engage_ctrl_convention where eng_id=a_eng_id;
         my_total_pourcentage:=my_total_pourcentage*100.0/my_montant_ht_total;
         if my_total_pourcentage>99.0 then my_total_pourcentage:=100.0; end if;

         for row_convention in convention
         loop
           if convention%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_convention set econ_montant_budgetaire=my_reste_budgetaire, econ_ht_saisie=my_reste_ht,
                 econ_ttc_saisie=my_reste_ttc,  econ_tva_saisie=my_reste_ttc-my_reste_ht where econ_id=row_convention.econ_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_convention.econ_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_convention.econ_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_convention.econ_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_convention set econ_montant_budgetaire=my_budgetaire, econ_ht_saisie=my_ht,
                 econ_ttc_saisie=my_ttc, econ_tva_saisie=my_ttc-my_ht where econ_id=row_convention.econ_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_hors_marche       
       select count(*) into my_nb from engage_ctrl_hors_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;

         for row_hors_marche in hors_marche
         loop
           if hors_marche%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_reste_budgetaire, ehom_ht_saisie=my_reste_ht,
                 ehom_ttc_saisie=my_reste_ttc,  ehom_tva_saisie=my_reste_ttc-my_reste_ht where ehom_id=row_hors_marche.ehom_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_hors_marche.ehom_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_hors_marche.ehom_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_hors_marche.ehom_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_hors_marche set ehom_montant_budgetaire=my_budgetaire, ehom_ht_saisie=my_ht,
                 ehom_ttc_saisie=my_ttc, ehom_tva_saisie=my_ttc-my_ht where ehom_id=row_hors_marche.ehom_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       -- maj montant engage_ctrl_marche       
       select count(*) into my_nb from engage_ctrl_marche where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;

         update engage_ctrl_marche set emar_montant_budgetaire=my_reste_budgetaire, emar_ht_saisie=my_reste_ht,
              emar_ttc_saisie=my_reste_ttc,  emar_tva_saisie=my_reste_ttc-my_reste_ht where eng_id=a_eng_id;
       end if;   

       -- maj montant engage_ctrl_planco
       select count(*) into my_nb from engage_ctrl_planco where eng_id=a_eng_id;
       if my_nb>0 then
         my_reste_ht:=my_engage.eng_ht_saisie;
         my_reste_ttc:=my_engage.eng_ttc_saisie;
         my_reste_budgetaire:=my_engage.eng_montant_budgetaire;
         my_total_pourcentage:=100.0;
       
         for row_planco in planco
         loop
           if planco%rowcount=my_nb and my_total_pourcentage=100.0 then
              update engage_ctrl_planco set epco_montant_budgetaire=my_reste_budgetaire, epco_ht_saisie=my_reste_ht,
                 epco_ttc_saisie=my_reste_ttc,  epco_tva_saisie=my_reste_ttc-my_reste_ht where epco_id=row_planco.epco_id;
           else
              my_pourcentage:=0;
              if my_montant_ht_total<>0 then my_pourcentage:=row_planco.epco_ht_saisie/my_montant_ht_total; end if;
              my_ht:=ROUND(my_pourcentage*my_engage.eng_ht_saisie, my_nb_decimales);
              if my_ht>my_reste_ht then my_ht:=my_reste_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_ttc_total<>0 then my_pourcentage:=row_planco.epco_ttc_saisie/my_montant_ttc_total; end if;
              my_ttc:=ROUND(my_pourcentage*my_engage.eng_ttc_saisie, my_nb_decimales);
              if my_ttc>my_reste_ttc then my_ttc:=my_reste_ttc; end if;
              if my_ttc<my_ht then my_ttc:=my_ht; end if;
              
              my_pourcentage:=0;
              if my_montant_budgetaire_total<>0 then my_pourcentage:=row_planco.epco_montant_budgetaire/my_montant_budgetaire_total; end if;
              my_budgetaire:=ROUND(my_pourcentage*my_engage.eng_montant_budgetaire, my_nb_decimales);
              if my_budgetaire>my_reste_budgetaire then my_budgetaire:=my_reste_budgetaire; end if;
              if my_budgetaire>my_ttc then my_budgetaire:=my_ttc; end if;
              
              update engage_ctrl_planco set epco_montant_budgetaire=my_budgetaire, epco_ht_saisie=my_ht,
                 epco_ttc_saisie=my_ttc, epco_tva_saisie=my_ttc-my_ht where epco_id=row_planco.epco_id;

              my_reste_ht:=my_reste_ht-my_ht;
              my_reste_ttc:=my_reste_ttc-my_ttc;
              my_reste_budgetaire:=my_reste_budgetaire-my_budgetaire;
           end if;
         end loop;
       end if;   

       --corrige_engage_budgetaire();
       verifier.verifier_engage_coherence(a_eng_id);
   END;
   
PROCEDURE corrige_depense_budgetaire(
      a_dep_id    depense_budget.dep_id%type
   ) 
   IS
     my_depense depense_budget%rowtype;
     my_dep_montant_budgetaire depense_budget.dep_montant_budgetaire%type;
     my_reste depense_budget.dep_montant_budgetaire%type;
     my_ctrl_bud depense_budget.dep_montant_budgetaire%type;
     
     row_action depense_ctrl_action%rowtype;
     cursor action is select * from depense_ctrl_action where dep_id=a_dep_id;
     row_analytique depense_ctrl_analytique%rowtype;
     cursor analytique is select * from depense_ctrl_analytique where dep_id=a_dep_id;
     row_convention depense_ctrl_convention%rowtype;
     cursor convention is select * from depense_ctrl_convention where dep_id=a_dep_id;
     row_hors_marche depense_ctrl_hors_marche%rowtype;
     cursor hors_marche is select * from depense_ctrl_hors_marche where dep_id=a_dep_id;
     row_marche depense_ctrl_marche%rowtype;
     cursor marche is select * from depense_ctrl_marche where dep_id=a_dep_id;
     row_planco depense_ctrl_planco%rowtype;
     cursor planco is select * from depense_ctrl_planco where dep_id=a_dep_id;
     
     my_nb integer;
   BEGIN
   
     -- maj montant depense_budget
     select * into my_depense from depense_budget where dep_id=a_dep_id;
    
     my_dep_montant_budgetaire:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, my_depense.dep_ht_saisie, my_depense.dep_ttc_saisie);
     update depense_budget set dep_montant_budgetaire=my_dep_montant_budgetaire where dep_id=a_dep_id;

     -- maj montant depense_ctrl_action
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_action where dep_id=a_dep_id;
     if my_nb>0 then
       for row_action in action
       loop
         if action%rowcount=my_nb then
            update depense_ctrl_action set dact_montant_budgetaire=my_reste where dact_id=row_action.dact_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_action.dact_ht_saisie, row_action.dact_ttc_saisie);

            update depense_ctrl_action set dact_montant_budgetaire=my_ctrl_bud where dact_id=row_action.dact_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_action(my_depense.eng_id);
     end if;
     
     -- maj montant depense_ctrl_analytique
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_analytique where dep_id=a_dep_id;
     if my_nb>0 then
       for row_analytique in analytique
       loop
         if analytique%rowcount=my_nb then
            update depense_ctrl_analytique set dana_montant_budgetaire=my_reste where dana_id=row_analytique.dana_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_analytique.dana_ht_saisie, row_analytique.dana_ttc_saisie);

            update depense_ctrl_analytique set dana_montant_budgetaire=my_ctrl_bud where dana_id=row_analytique.dana_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_analytique(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_convention
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_convention where dep_id=a_dep_id;
     if my_nb>0 then
       for row_convention in convention
       loop
         if convention%rowcount=my_nb then
            update depense_ctrl_convention set dcon_montant_budgetaire=my_reste where dcon_id=row_convention.dcon_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_convention.dcon_ht_saisie, row_convention.dcon_ttc_saisie);

            update depense_ctrl_convention set dcon_montant_budgetaire=my_ctrl_bud where dcon_id=row_convention.dcon_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_convention(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_hors_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_hors_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_hors_marche in hors_marche
       loop
         if hors_marche%rowcount=my_nb then
            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_reste where dhom_id=row_hors_marche.dhom_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_hors_marche.dhom_ht_saisie, row_hors_marche.dhom_ttc_saisie);

            update depense_ctrl_hors_marche set dhom_montant_budgetaire=my_ctrl_bud where dhom_id=row_hors_marche.dhom_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_hors_marche(my_depense.eng_id);
     end if;

     -- maj montant depense_ctrl_marche
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_marche where dep_id=a_dep_id;
     if my_nb>0 then
       for row_marche in marche
       loop
         if marche%rowcount=my_nb then
            update depense_ctrl_marche set dmar_montant_budgetaire=my_reste where dmar_id=row_marche.dmar_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_marche.dmar_ht_saisie, row_marche.dmar_ttc_saisie);

            update depense_ctrl_marche set dmar_montant_budgetaire=my_ctrl_bud where dmar_id=row_marche.dmar_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_marche(my_depense.eng_id);
     end if;
     
     -- maj montant engage_ctrl_planco
     my_reste:=my_dep_montant_budgetaire;
     select count(*) into my_nb from depense_ctrl_planco where dep_id=a_dep_id;
     if my_nb>0 then    
       for row_planco in planco
       loop
         if planco%rowcount=my_nb then
            update depense_ctrl_planco set dpco_montant_budgetaire=my_reste where dpco_id=row_planco.dpco_id;
         else
            my_ctrl_bud:=budget.calculer_budgetaire(my_depense.exe_ordre, my_depense.tap_id, null, row_planco.dpco_ht_saisie, row_planco.dpco_ttc_saisie);

            update depense_ctrl_planco set dpco_montant_budgetaire=my_ctrl_bud where dpco_id=row_planco.dpco_id;

            my_reste:=my_reste-my_ctrl_bud;
            if my_reste<0 then  my_reste:=0; end if;
         end if;
       end loop;
       corriger.upd_engage_reste_planco(my_depense.eng_id);
     end if;

     verifier.verifier_depense_coherence(a_dep_id);
   END;
   
PROCEDURE rempli_reimputation(
      a_dep_id       depense_budget.dep_id%type,
      a_reim_libelle reimputation.reim_libelle%type,
      a_utl_ordre    depense_budget.utl_ordre%type
   )
   IS
     my_nb          integer;
     my_reim_id     reimputation.reim_id%type;
     my_exe_ordre   depense_budget.exe_ordre%type;
     my_reim_numero reimputation.reim_numero%type;
   BEGIN
     select exe_ordre into my_exe_ordre from depense_budget where dep_id=a_dep_id;
     my_reim_numero := Get_Numerotation(my_exe_ordre, NULL, null, 'REIMPUTATION');

     select reimputation_seq.nextval into my_reim_id from dual;
     insert into reimputation values (my_reim_id, my_exe_ordre, my_reim_numero, a_dep_id, a_reim_libelle, a_utl_ordre, sysdate);
     
     insert into reimputation_action select reimputation_action_seq.nextval, my_reim_id, tyac_id, exe_ordre, dact_montant_budgetaire, dact_ht_saisie,
         dact_tva_saisie, dact_ttc_saisie from depense_ctrl_action where dep_id=a_dep_id;
         
     insert into reimputation_analytique select reimputation_analytique_seq.nextval, my_reim_id, can_id, dana_montant_budgetaire, dana_ht_saisie,
         dana_tva_saisie, dana_ttc_saisie from depense_ctrl_analytique where dep_id=a_dep_id;
         
     insert into reimputation_budget select reimputation_budget_seq.nextval, my_reim_id, d.eng_id, e.org_id, e.tcd_ordre, d.tap_id
        from depense_budget d, engage_budget e where e.eng_id=d.eng_id and dep_id=a_dep_id;
     
     insert into reimputation_convention select reimputation_convention_seq.nextval, my_reim_id, conv_ordre, dcon_montant_budgetaire, dcon_ht_saisie,
         dcon_tva_saisie, dcon_ttc_saisie from depense_ctrl_convention where dep_id=a_dep_id;
         
     insert into reimputation_hors_marche select reimputation_hors_marche_seq.nextval, my_reim_id, typa_id, ce_ordre, dhom_montant_budgetaire, dhom_ht_saisie,
         dhom_tva_saisie, dhom_ttc_saisie from depense_ctrl_hors_marche where dep_id=a_dep_id;
         
     insert into reimputation_marche select reimputation_marche_seq.nextval, my_reim_id, att_ordre, dmar_montant_budgetaire, dmar_ht_saisie,
         dmar_tva_saisie, dmar_ttc_saisie from depense_ctrl_marche where dep_id=a_dep_id;
         
     insert into reimputation_planco select reimputation_planco_seq.nextval, my_reim_id, pco_num, exe_ordre, dpco_montant_budgetaire, dpco_ht_saisie,
         dpco_tva_saisie, dpco_ttc_saisie from depense_ctrl_planco where dep_id=a_dep_id;
   END;
   
END; 
/

CREATE OR REPLACE PACKAGE JEFY_DEPENSE.engager  IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------

PROCEDURE ins_engage (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

PROCEDURE del_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE solder_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           engage_budget.utl_ordre%type);

PROCEDURE solder_engage_sansdroit (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE);

PROCEDURE upd_engage (
      a_eng_id 		        engage_budget.eng_id%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_utl_ordre			engage_budget.utl_ordre%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------

PROCEDURE log_engage (
      a_eng_id              engage_budget.eng_id%type,
      a_utl_ordre           z_engage_budget.zeng_utl_ordre%type);

PROCEDURE ins_engage_budget (
      a_eng_id in out		engage_budget.eng_id%type,
	  a_exe_ordre			engage_budget.exe_ordre%type,
	  a_eng_numero in out	engage_budget.eng_numero%type,
	  a_fou_ordre			engage_budget.fou_ordre%type,
	  a_org_id				engage_budget.org_id%type,
	  a_tcd_ordre			engage_budget.tcd_ordre%type,
	  a_tap_id				engage_budget.tap_id%type,
	  a_eng_libelle			engage_budget.eng_libelle%type,
	  a_eng_ht_saisie		engage_budget.eng_ht_saisie%type,
	  a_eng_ttc_saisie		engage_budget.eng_ttc_saisie%type,
	  a_tyap_id				engage_budget.tyap_id%type,
	  a_utl_ordre			engage_budget.utl_ordre%type);

PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre         engage_budget.exe_ordre%type,
	  a_eng_id			  engage_budget.eng_id%type,
	  a_chaine			  varchar2);

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Engager
IS

--
--
-- procedures a executer par les applications clientes.
-------------------------------------------------------
-------------------------------------------------------


   PROCEDURE ins_engage (
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(a_exe_ordre, a_utl_ordre, a_org_id);

		-- lancement des differentes procedures d'insertion des tables d'engagement.
		ins_engage_budget(a_eng_id,a_exe_ordre,a_eng_numero,a_fou_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_eng_libelle, a_eng_ht_saisie,a_eng_ttc_saisie,a_tyap_id,a_utl_ordre);

		ins_engage_ctrl_action(a_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(a_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(a_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(a_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(a_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(a_exe_ordre,a_eng_id,a_chaine_planco);

		Verifier.verifier_engage_coherence(a_eng_id);
		Apres_Engage.engage(a_eng_id);
   END;

   PROCEDURE del_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_montant_reste      ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est deja annule (eng_id:'||a_eng_id||')');
	    END IF;

   		SELECT exe_ordre, eng_montant_budgetaire, eng_montant_budgetaire_reste, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_montant_reste, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

   		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on traite le cas d'un engagement dont la somme des liquidations.
		-- est egale a la somme des ordres de reversement ... ce qui fait que l'engagement est reengage.
		--  donc on ne le supprime pas mais on le solde.
		SELECT COUNT(*) INTO my_nb FROM DEPENSE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb>0 AND my_montant_budgetaire=my_montant_reste THEN
		   solder_engage(a_eng_id, a_utl_ordre);
		ELSE
           Verifier.verifier_util_engage(a_eng_id);

		   log_engage(a_eng_id,a_utl_ordre);

	       DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;
	       DELETE FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        END IF;

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.del_engage(a_eng_id);
   END;

   PROCEDURE solder_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE solder_engage_sansdroit (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           ENGAGE_BUDGET.utl_ordre%TYPE)
   IS
      my_nb			        INTEGER;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_eng_montant_bud_reste engage_budget.eng_montant_budgetaire_reste%type;
   BEGIN
		-- on verifie que l'engagement existe.
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ou est annule (eng_id:'||a_eng_id||')');
	    END IF;

	    SELECT exe_ordre, org_id, tcd_ordre, eng_montant_budgetaire_reste INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_eng_montant_bud_reste
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        if (my_eng_montant_bud_reste=0) then return; end if;
        
		-- on solde l'engagement et ses controles.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ACTION SET eact_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_ANALYTIQUE SET eana_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_CONVENTION SET econ_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_HORS_MARCHE SET ehom_montant_budgetaire_reste=0, ehom_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_MARCHE SET emar_montant_budgetaire_reste=0, emar_ht_reste=0 WHERE eng_id=a_eng_id;
		UPDATE ENGAGE_CTRL_PLANCO SET epco_montant_budgetaire_reste=0 WHERE eng_id=a_eng_id;

		-- on verifie la coherence.
		Verifier.verifier_engage_coherence(a_eng_id);

        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
	    Apres_Engage.solder_engage(a_eng_id);
   END;

   PROCEDURE upd_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_hors_marche	VARCHAR2,
	  a_chaine_marche		VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
	  my_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	  my_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
      my_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE;
      my_tap_id			    ENGAGE_BUDGET.tap_id%TYPE;
      my_org_id			    ENGAGE_BUDGET.org_id%TYPE;
      my_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE;
      my_montant_budgetaire ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_eng_tva_saisie     ENGAGE_BUDGET.eng_tva_saisie%TYPE;
      my_budgetaire         ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
      my_nb_decimales       NUMBER;
   BEGIN

        select nvl(sum(dep_montant_budgetaire),0) into my_budgetaire from depense_budget where eng_id=a_eng_id and dep_montant_budgetaire>0;

   		SELECT exe_ordre, tap_id, org_id, tcd_ordre INTO my_exe_ordre, my_tap_id, my_org_id, my_tcd_ordre
		  FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

		-- on verifie que l'utilisateur a le droit d'engager sur cet exercice.
		Verifier.verifier_engage_exercice(my_exe_ordre, a_utl_ordre, my_org_id);

		log_engage(a_eng_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	    IF my_budgetaire> my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja liquide ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
		END IF;

		-- on modifie les montants.
		UPDATE ENGAGE_BUDGET SET eng_montant_budgetaire=my_montant_budgetaire,
		   eng_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   eng_ht_saisie=my_eng_ht_saisie, eng_tva_saisie=my_eng_tva_saisie,
		   eng_ttc_saisie=my_eng_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE eng_id=a_eng_id;

		-- on supprime les anciens.
		DELETE FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_HORS_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;
		DELETE FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id;

		-- on insere les nouveaux.
		ins_engage_ctrl_action(my_exe_ordre,a_eng_id,a_chaine_action);
		ins_engage_ctrl_analytique(my_exe_ordre,a_eng_id,a_chaine_analytique);
		ins_engage_ctrl_convention(my_exe_ordre,a_eng_id,a_chaine_convention);
		ins_engage_ctrl_hors_marche(my_exe_ordre,a_eng_id,a_chaine_hors_marche);
		ins_engage_ctrl_marche(my_exe_ordre,a_eng_id,a_chaine_marche);
		ins_engage_ctrl_planco(my_exe_ordre,a_eng_id,a_chaine_planco);

		-- on met a jour les restes engages des controleurs suivant les depenses.
		Corriger.upd_engage_reste(a_eng_id);

		-- on verifie.
		Verifier.verifier_engage_coherence(a_eng_id);
        Budget.maj_budget(my_exe_ordre, my_org_id, my_tcd_ordre);
		Apres_Engage.engage(a_eng_id);
   END;

--
--
-- procedures appelees par les procedures publiques.
----------------------------------------------------
----------------------------------------------------


   PROCEDURE log_engage (
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE,
      a_utl_ordre           Z_ENGAGE_BUDGET.zeng_utl_ordre%TYPE)
   IS
      my_zeng_id			Z_ENGAGE_BUDGET.zeng_id%TYPE;
   BEGIN
		SELECT z_engage_budget_seq.NEXTVAL INTO my_zeng_id FROM dual;

		-- engage_budget.
		INSERT INTO Z_ENGAGE_BUDGET SELECT my_zeng_id, SYSDATE, a_utl_ordre, e.*
		  FROM ENGAGE_BUDGET e WHERE eng_id=a_eng_id;

		-- engage_ctrl_action.
		INSERT INTO Z_ENGAGE_CTRL_ACTION SELECT z_engage_ctrl_action_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ACTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_analytique.
		INSERT INTO Z_ENGAGE_CTRL_ANALYTIQUE SELECT z_engage_ctrl_analytique_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_ANALYTIQUE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_convention.
		INSERT INTO Z_ENGAGE_CTRL_CONVENTION SELECT z_engage_ctrl_convention_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_CONVENTION e WHERE eng_id=a_eng_id;

		-- engage_ctrl_hors_marche.
		INSERT INTO Z_ENGAGE_CTRL_HORS_MARCHE SELECT z_engage_ctrl_hors_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_HORS_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_marche.
		INSERT INTO Z_ENGAGE_CTRL_MARCHE SELECT z_engage_ctrl_marche_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_MARCHE e WHERE eng_id=a_eng_id;

		-- engage_ctrl_planco.
		INSERT INTO Z_ENGAGE_CTRL_PLANCO SELECT z_engage_ctrl_planco_seq.NEXTVAL, e.*, my_zeng_id
		  FROM ENGAGE_CTRL_PLANCO e WHERE eng_id=a_eng_id;
   END;

   PROCEDURE ins_engage_budget(
      a_eng_id IN OUT		ENGAGE_BUDGET.eng_id%TYPE,
	  a_exe_ordre			ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_numero IN OUT	ENGAGE_BUDGET.eng_numero%TYPE,
	  a_fou_ordre			ENGAGE_BUDGET.fou_ordre%TYPE,
	  a_org_id				ENGAGE_BUDGET.org_id%TYPE,
	  a_tcd_ordre			ENGAGE_BUDGET.tcd_ordre%TYPE,
	  a_tap_id				ENGAGE_BUDGET.tap_id%TYPE,
	  a_eng_libelle			ENGAGE_BUDGET.eng_libelle%TYPE,
	  a_eng_ht_saisie		ENGAGE_BUDGET.eng_ht_saisie%TYPE,
	  a_eng_ttc_saisie		ENGAGE_BUDGET.eng_ttc_saisie%TYPE,
	  a_tyap_id				ENGAGE_BUDGET.tyap_id%TYPE,
	  a_utl_ordre			ENGAGE_BUDGET.utl_ordre%TYPE
   ) IS
	 my_eng_ht_saisie	    ENGAGE_BUDGET.eng_ht_saisie%TYPE;
	 my_eng_ttc_saisie	    ENGAGE_BUDGET.eng_ttc_saisie%TYPE;
     my_montant_budgetaire  ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_eng_tva_saisie      ENGAGE_BUDGET.eng_tva_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_eng_ht_saisie:=round(a_eng_ht_saisie, my_nb_decimales);
        my_eng_ttc_saisie:=round(a_eng_ttc_saisie, my_nb_decimales);

   		-- lucrativite ??.
		--		UPDATE COMMANDE SET cde_lucrativite=(select org_lucrativite from organ where org_ordre=orgordre) WHERE cde_ordre=cdeordre;

        verifier.verifier_organ(a_org_id, a_tcd_ordre);
		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
	    Verifier.verifier_fournisseur(a_fou_ordre);

		IF my_eng_ht_saisie<0 OR my_eng_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		END IF;

   		my_eng_tva_saisie:=my_eng_ttc_saisie-my_eng_ht_saisie;

		IF my_eng_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      my_eng_ht_saisie,my_eng_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_eng_id IS NULL THEN
	       SELECT engage_budget_seq.NEXTVAL INTO a_eng_id FROM dual;
	    END IF;

		IF a_eng_numero IS NULL THEN
   		   a_eng_numero := Get_Numerotation(a_exe_ordre, NULL, null,'ENGAGE_BUDGET');
   		END IF;

	    INSERT INTO ENGAGE_BUDGET VALUES (a_eng_id, a_exe_ordre, a_eng_numero, a_fou_ordre, a_org_id, a_tcd_ordre,
	      a_tap_id, a_eng_libelle, my_montant_budgetaire, my_montant_budgetaire, my_eng_ht_saisie, my_eng_tva_saisie,
		  my_eng_ttc_saisie, SYSDATE, a_tyap_id, a_utl_ordre);

        Budget.maj_budget(a_exe_ordre, a_org_id, a_tcd_ordre);
	    Apres_Engage.Budget(a_eng_id);
   END;

   PROCEDURE ins_engage_ctrl_action (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eact_id	               ENGAGE_CTRL_ACTION.eact_id%TYPE;
       my_tyac_id	  	   		   ENGAGE_CTRL_ACTION.tyac_id%TYPE;
       my_eact_montant_budgetaire  ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_eact_ht_saisie	  	   ENGAGE_CTRL_ACTION.eact_ht_saisie%TYPE;
       my_eact_tva_saisie		   ENGAGE_CTRL_ACTION.eact_tva_saisie%TYPE;
       my_eact_ttc_saisie		   ENGAGE_CTRL_ACTION.eact_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
	   my_utl_ordre                ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ACTION.eact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        
		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eact_ht_saisie:=round(my_eact_ht_saisie, my_nb_decimales);
            my_eact_ttc_saisie:=round(my_eact_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ACTION WHERE eng_id=a_eng_id AND tyac_id=my_tyac_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette action pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eact_ht_saisie<0 OR my_eact_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eact_tva_saisie := my_eact_ttc_saisie - my_eact_ht_saisie;
			IF my_eact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eact_ht_saisie,my_eact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_eact_montant_budgetaire THEN
			    my_eact_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_action_seq.NEXTVAL INTO my_eact_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ACTION VALUES (my_eact_id,
			       a_exe_ordre, a_eng_id, my_tyac_id, my_eact_montant_budgetaire, my_eact_montant_budgetaire,
				   my_eact_ht_saisie, my_eact_tva_saisie, my_eact_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_action(a_eng_id);
            
	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id, my_utl_ordre);
			Apres_Engage.action(my_eact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eact_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_analytique (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_eana_id	               ENGAGE_CTRL_ANALYTIQUE.eana_id%TYPE;
       my_can_id	  	   		   ENGAGE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_eana_montant_budgetaire  ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_eana_ht_saisie	  	   ENGAGE_CTRL_ANALYTIQUE.eana_ht_saisie%TYPE;
       my_eana_tva_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_tva_saisie%TYPE;
       my_eana_ttc_saisie		   ENGAGE_CTRL_ANALYTIQUE.eana_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_ANALYTIQUE.eana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_eana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_eana_ht_saisie:=round(my_eana_ht_saisie, my_nb_decimales);
            my_eana_ttc_saisie:=round(my_eana_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_ANALYTIQUE WHERE eng_id=a_eng_id AND can_id=my_can_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code analytique pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_eana_ht_saisie<0 OR my_eana_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_eana_tva_saisie := my_eana_ttc_saisie - my_eana_ht_saisie;
			IF my_eana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_eana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_eana_ht_saisie,my_eana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_eana_montant_budgetaire THEN
			    my_eana_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_analytique_seq.NEXTVAL INTO my_eana_id FROM dual;

			INSERT INTO ENGAGE_CTRL_ANALYTIQUE VALUES (my_eana_id,
			       a_exe_ordre, a_eng_id, my_can_id, my_eana_montant_budgetaire, my_eana_montant_budgetaire,
				   my_eana_ht_saisie, my_eana_tva_saisie, my_eana_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_analytique(a_eng_id);
            
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Engage.analytique(my_eana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_eana_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_convention (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_econ_id	               ENGAGE_CTRL_CONVENTION.econ_id%TYPE;
       my_conv_ordre 	   		   ENGAGE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_econ_montant_budgetaire  ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_econ_ht_saisie	  	   ENGAGE_CTRL_CONVENTION.econ_ht_saisie%TYPE;
       my_econ_tva_saisie		   ENGAGE_CTRL_CONVENTION.econ_tva_saisie%TYPE;
       my_econ_ttc_saisie		   ENGAGE_CTRL_CONVENTION.econ_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre                ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_CONVENTION.econ_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_econ_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_econ_ht_saisie:=round(my_econ_ht_saisie, my_nb_decimales);
            my_econ_ttc_saisie:=round(my_econ_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_CONVENTION WHERE eng_id=a_eng_id AND conv_ordre=my_conv_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette convention pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_econ_ht_saisie<0 OR my_econ_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_econ_tva_saisie := my_econ_ttc_saisie - my_econ_ht_saisie;
			IF my_econ_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_econ_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_econ_ht_saisie,my_econ_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_eng_montant_budgetaire<=my_somme+my_econ_montant_budgetaire THEN
			    my_econ_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_convention_seq.NEXTVAL INTO my_econ_id FROM dual;

			INSERT INTO ENGAGE_CTRL_CONVENTION VALUES (my_econ_id,
			       a_exe_ordre, a_eng_id, my_conv_ordre, my_econ_montant_budgetaire, my_econ_montant_budgetaire,
				   my_econ_ht_saisie, my_econ_tva_saisie, my_econ_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_convention(a_eng_id);
            
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_conv_ordre);
	    	Apres_Engage.convention(my_econ_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_econ_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_hors_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_ehom_id	               ENGAGE_CTRL_HORS_MARCHE.ehom_id%TYPE;
       my_typa_id	  	   		   ENGAGE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_ce_ordre	  	   		   ENGAGE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_ehom_montant_budgetaire  ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_ehom_ht_saisie	  	   ENGAGE_CTRL_HORS_MARCHE.ehom_ht_saisie%TYPE;
       my_ehom_tva_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_tva_saisie%TYPE;
       my_ehom_ttc_saisie		   ENGAGE_CTRL_HORS_MARCHE.ehom_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_HORS_MARCHE.ehom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le type achat.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_typa_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le code de nomenclature.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ce_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ehom_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ehom_ht_saisie:=round(my_ehom_ht_saisie, my_nb_decimales);
            my_ehom_ttc_saisie:=round(my_ehom_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_HORS_MARCHE
			     WHERE eng_id=a_eng_id AND ce_ordre=my_ce_ordre AND typa_id=my_typa_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja ce code nomenclature pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_ehom_ht_saisie<0 OR my_ehom_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_ehom_tva_saisie := my_ehom_ttc_saisie - my_ehom_ht_saisie;
			IF my_ehom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_ehom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_ehom_ht_saisie,my_ehom_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_ehom_montant_budgetaire THEN
			    my_ehom_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_hors_marche_seq.NEXTVAL INTO my_ehom_id FROM dual;

			INSERT INTO ENGAGE_CTRL_HORS_MARCHE VALUES (my_ehom_id,
			       a_exe_ordre, a_eng_id, my_typa_id, my_ce_ordre, my_ehom_montant_budgetaire, my_ehom_montant_budgetaire,
				   my_ehom_ht_saisie, my_ehom_ht_saisie, my_ehom_tva_saisie, my_ehom_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_hors_marche(a_eng_id);
            
			Verifier.verifier_hors_marche(a_exe_ordre, my_org_id, my_typa_id, my_ce_ordre, my_fou_ordre);
	    	Apres_Engage.hors_marche(my_ehom_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_ehom_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_engage_ctrl_marche (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_emar_id	               ENGAGE_CTRL_MARCHE.emar_id%TYPE;
       my_att_ordre	  	   		   ENGAGE_CTRL_MARCHE.att_ordre%TYPE;
       my_emar_montant_budgetaire  ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_emar_ht_saisie	  	   ENGAGE_CTRL_MARCHE.emar_ht_saisie%TYPE;
       my_emar_tva_saisie		   ENGAGE_CTRL_MARCHE.emar_tva_saisie%TYPE;
       my_emar_ttc_saisie		   ENGAGE_CTRL_MARCHE.emar_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
   	   my_fou_ordre				   ENGAGE_BUDGET.fou_ordre%TYPE;
   	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_MARCHE.emar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, fou_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_fou_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'attribution.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_att_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_emar_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_emar_ht_saisie:=round(my_emar_ht_saisie, my_nb_decimales);
            my_emar_ttc_saisie:=round(my_emar_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id AND att_ordre=my_att_ordre;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette attribution pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_emar_ht_saisie<0 OR my_emar_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_emar_tva_saisie := my_emar_ttc_saisie - my_emar_ht_saisie;
			IF my_emar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_emar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_emar_ht_saisie,my_emar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_emar_montant_budgetaire THEN
			    my_emar_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_MARCHE WHERE eng_id=a_eng_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour un engagement');
			END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_marche_seq.NEXTVAL INTO my_emar_id FROM dual;

			INSERT INTO ENGAGE_CTRL_MARCHE VALUES (my_emar_id,
			       a_exe_ordre, a_eng_id, my_att_ordre, my_emar_montant_budgetaire, my_emar_montant_budgetaire,
				   my_emar_ht_saisie,my_emar_ht_saisie, my_emar_tva_saisie, my_emar_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_marche(a_eng_id);
            
			Verifier.verifier_marche(a_exe_ordre, my_org_id, my_fou_ordre, my_att_ordre, my_utl_ordre);
	    	Apres_Engage.marche(my_emar_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_emar_montant_budgetaire;
		END LOOP;

   END;

   PROCEDURE ins_engage_ctrl_planco (
      a_exe_ordre     ENGAGE_BUDGET.exe_ordre%TYPE,
	  a_eng_id		  ENGAGE_BUDGET.eng_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_epco_id	               ENGAGE_CTRL_PLANCO.epco_id%TYPE;
       my_pco_num	  	   		   ENGAGE_CTRL_PLANCO.pco_num%TYPE;
	   my_utl_ordre				   ENGAGE_BUDGET.utl_ordre%TYPE;
       my_epco_montant_budgetaire  ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_epco_ht_saisie	  	   ENGAGE_CTRL_PLANCO.epco_ht_saisie%TYPE;
       my_epco_tva_saisie		   ENGAGE_CTRL_PLANCO.epco_tva_saisie%TYPE;
       my_epco_ttc_saisie		   ENGAGE_CTRL_PLANCO.epco_ttc_saisie%TYPE;
       my_eng_montant_budgetaire   ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
	   my_tap_id				   ENGAGE_BUDGET.tap_id%TYPE;
   	   my_org_id				   ENGAGE_BUDGET.org_id%TYPE;
	   my_tcd_ordre				   ENGAGE_BUDGET.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   ENGAGE_CTRL_PLANCO.epco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||')');
		END IF;

		SELECT eng_montant_budgetaire, tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_eng_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_epco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_epco_ht_saisie:=round(my_epco_ht_saisie, my_nb_decimales);
            my_epco_ttc_saisie:=round(my_epco_ttc_saisie, my_nb_decimales);

			SELECT COUNT(*) INTO my_nb FROM ENGAGE_CTRL_PLANCO WHERE eng_id=a_eng_id AND pco_num=my_pco_num;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Il existe deja cette imputation pour cet engagement');
			END IF;

	   		-- verification que les montants sont bien positifs !!!.
			IF my_epco_ht_saisie<0 OR my_epco_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'L''engagement doit se faire pour un montant positif');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_epco_tva_saisie := my_epco_ttc_saisie - my_epco_ht_saisie;
			IF my_epco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

   			my_epco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_epco_ht_saisie,my_epco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_eng_montant_budgetaire<=my_somme+my_epco_montant_budgetaire THEN
			    my_epco_montant_budgetaire:=my_eng_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT engage_ctrl_planco_seq.NEXTVAL INTO my_epco_id FROM dual;

			INSERT INTO ENGAGE_CTRL_PLANCO VALUES (my_epco_id,
			       a_exe_ordre, a_eng_id, my_pco_num, my_epco_montant_budgetaire, my_epco_montant_budgetaire,
				   my_epco_ht_saisie, my_epco_tva_saisie, my_epco_ttc_saisie, SYSDATE);

            corriger.upd_engage_reste_planco(a_eng_id);
            
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_epco_montant_budgetaire;
		END LOOP;
   END;

END;
/

CREATE OR REPLACE PACKAGE JEFY_DEPENSE.commander  IS

--
--
-- procedures a executer par les applications clientes.
--
--

PROCEDURE annuler_commande (
      a_comm_id             commande.comm_id%type,
	  a_utl_ordre			commande.utl_ordre%type);

PROCEDURE basculer_commande (
      a_comm_id_origine     commande.comm_id%type,
	  a_comm_id_destination	commande.comm_id%type);

PROCEDURE solder_commande (
      a_comm_id             commande.comm_id%type,
	  a_utl_ordre			commande.utl_ordre%type);

PROCEDURE solder_commande_sansdroit (
      a_comm_id             commande.comm_id%type);

PROCEDURE ins_commande (
      a_comm_id in out		commande.comm_id%type,
	  a_exe_ordre			commande.exe_ordre%type,
	  a_comm_numero	in out 	commande.comm_numero%type,
	  a_tyet_id				commande.tyet_id%type,
	  a_fou_ordre			commande.fou_ordre%type,
	  a_comm_reference		commande.comm_reference%type,
	  a_comm_libelle		commande.comm_libelle%type,
	  a_tyet_id_imprimable  commande.tyet_id_imprimable%type,
	  a_dev_id				commande.dev_id%type,
	  a_utl_ordre			commande.utl_ordre%type);

PROCEDURE upd_commande (
      a_comm_id             commande.comm_id%type,
	  a_fou_ordre			commande.fou_ordre%type,
	  a_comm_reference		commande.comm_reference%type,
	  a_comm_libelle		commande.comm_libelle%type,
	  a_utl_ordre			commande.utl_ordre%type);

PROCEDURE ins_commande_engagement (
	  a_come_id	in out	    commande_engagement.come_id%type,
      a_comm_id             commande.comm_id%type,
      a_eng_id              engage_budget.eng_id%type);

PROCEDURE del_commande_engagement (
      a_comm_id             commande.comm_id%type,
      a_eng_id              engage_budget.eng_id%type);

PROCEDURE del_commande (
      a_comm_id             commande.comm_id%type);

PROCEDURE del_commande_budget (
      a_cbud_id             commande_budget.cbud_id%type);

PROCEDURE ins_article (
      a_art_id in out		article.art_id%type,
	  a_comm_id				article.comm_id%type,
	  a_art_libelle			article.art_libelle%type,
	  a_art_prix_ht			article.art_prix_ht%type,
	  a_art_prix_ttc		article.art_prix_ttc%type,
	  a_art_quantite		article.art_quantite%type,
	  a_art_prix_total_ht	article.art_prix_total_ht%type,
	  a_art_prix_total_ttc	article.art_prix_total_ttc%type,
	  a_art_reference		article.art_reference%type,
	  a_artc_id				article.artc_id%type,
	  a_att_ordre			article.att_ordre%type,
	  a_ce_ordre			article.ce_ordre%type,
	  a_tva_id				article.tva_id%type,
	  a_art_id_pere			article.art_id_pere%type,
	  a_typa_id				article.typa_id%type);

PROCEDURE ins_article_catalogue (
      a_caar_id in out		jefy_catalogue.catalogue_article.caar_id%type,
	  a_fou_ordre			jefy_catalogue.catalogue.fou_ordre%type,
	  a_art_libelle			article.art_libelle%type,
	  a_art_prix_ht			article.art_prix_ht%type,
	  a_art_prix_ttc		article.art_prix_ttc%type,
	  a_art_reference		article.art_reference%type,
	  a_cm_ordre			jefy_catalogue.article.cm_ordre%type,
	  a_tva_id				article.tva_id%type);

PROCEDURE upd_article (
      a_art_id      		article.art_id%type,
	  a_art_libelle			article.art_libelle%type,
	  a_art_prix_ht			article.art_prix_ht%type,
	  a_art_prix_ttc		article.art_prix_ttc%type,
	  a_art_quantite		article.art_quantite%type,
	  a_art_prix_total_ht	article.art_prix_total_ht%type,
	  a_art_prix_total_ttc	article.art_prix_total_ttc%type,
	  a_art_reference		article.art_reference%type,
	  a_ce_ordre			article.ce_ordre%type,
	  a_tva_id				article.tva_id%type);

PROCEDURE del_article (
      a_art_id 		 	article.art_id%type);

PROCEDURE ins_commande_budget (
      a_cbud_id in out		commande_budget.cbud_id%type,
      a_comm_id	   			commande_budget.comm_id%type,
	  a_exe_ordre			commande_budget.exe_ordre%type,
	  a_org_id				commande_budget.org_id%type,
	  a_tcd_ordre			commande_budget.tcd_ordre%type,
	  a_tap_id				commande_budget.tap_id%type,
	  a_cbud_ht_saisie		commande_budget.cbud_ht_saisie%type,
	  a_cbud_ttc_saisie		commande_budget.cbud_ttc_saisie%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

PROCEDURE upd_commande_budget (
      a_cbud_id     		commande_budget.cbud_id%type,
	  a_cbud_ht_saisie		commande_budget.cbud_ht_saisie%type,
	  a_cbud_ttc_saisie		commande_budget.cbud_ttc_saisie%type,
	  a_tap_id				commande_budget.tap_id%type,
	  a_chaine_action		varchar2,
	  a_chaine_analytique	varchar2,
	  a_chaine_convention	varchar2,
	  a_chaine_hors_marche	varchar2,
	  a_chaine_marche		varchar2,
	  a_chaine_planco		varchar2);

--
--
-- procedures appelees par ins_commande_budget.
--
--

PROCEDURE ins_commande_ctrl_action (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_commande_ctrl_analytique (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_commande_ctrl_convention (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_commande_ctrl_hors_marche (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_commande_ctrl_marche (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

PROCEDURE ins_commande_ctrl_planco (
      a_exe_ordre         commande_budget.exe_ordre%type,
	  a_cbud_id			  commande_budget.cbud_id%type,
	  a_chaine			  varchar2);

END;
/

CREATE OR REPLACE PACKAGE BODY JEFY_DEPENSE.Commander
IS

   PROCEDURE annuler_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_utl_ordre            COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
      my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
           OPEN engagements;
        LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

           del_commande_engagement(a_comm_id, my_eng_id);
           Engager.del_engage(my_eng_id, a_utl_ordre);
        END LOOP;
        CLOSE engagements;

        del_commande(a_comm_id);
   END;

   PROCEDURE basculer_commande (
      a_comm_id_origine     commande.comm_id%type,
      a_comm_id_destination    commande.comm_id%type
   ) IS
     my_nb integer;
   begin
       select count(*) into my_nb from commande_bascule where comm_id_origine=a_comm_id_origine;
       if my_nb>0 then
           RAISE_APPLICATION_ERROR(-20001, 'Cette commande a deja ete basculee');
       end if;
       
       insert into commande_bascule select commande_bascule_seq.nextval, a_comm_id_origine, a_comm_id_destination from dual;
       
       insert into commande_utilisateur select commande_utilisateur_seq.nextval, a_comm_id_destination, c.utl_ordre from 
           (select utl_ordre from commande where comm_id=a_comm_id_origine
              union select utl_ordre from commande_utilisateur where comm_id=a_comm_id_origine) c;
   end;

   PROCEDURE solder_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_utl_ordre            COMMANDE.utl_ordre%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
      my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
           OPEN engagements;
        LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

           Engager.solder_engage(my_eng_id, a_utl_ordre);
        END LOOP;
        CLOSE engagements;

        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE solder_commande_sansdroit (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
      CURSOR engagements IS SELECT eng_id FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id;
      my_eng_id     ENGAGE_BUDGET.eng_id%TYPE;
   BEGIN
           OPEN engagements;
        LOOP
           FETCH engagements INTO my_eng_id;
           EXIT WHEN engagements%NOTFOUND;

           Engager.solder_engage_sansdroit(my_eng_id,null);
        END LOOP;
        CLOSE engagements;

        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande (
      a_comm_id IN OUT        COMMANDE.comm_id%TYPE,
      a_exe_ordre            COMMANDE.exe_ordre%TYPE,
      a_comm_numero    IN OUT  COMMANDE.comm_numero%TYPE,
      a_tyet_id                COMMANDE.tyet_id%TYPE,
      a_fou_ordre            COMMANDE.fou_ordre%TYPE,
      a_comm_reference        COMMANDE.comm_reference%TYPE,
      a_comm_libelle        COMMANDE.comm_libelle%TYPE,
      a_tyet_id_imprimable  COMMANDE.tyet_id_imprimable%TYPE,
      a_dev_id                COMMANDE.dev_id%TYPE,
      a_utl_ordre            COMMANDE.utl_ordre%TYPE
   ) IS
      my_comm_reference        COMMANDE.comm_reference%TYPE;
   BEGIN
           -- enregistrement dans la table.
        IF a_comm_numero IS NULL THEN
              a_comm_numero := Get_Numerotation(a_exe_ordre, NULL, null, 'COMMANDE');
           END IF;

        IF a_comm_reference IS NULL THEN
           my_comm_reference:=TO_CHAR(a_comm_numero);
        ELSE
           my_comm_reference:=a_comm_reference;
        END IF;

        IF a_comm_id IS NULL THEN
           SELECT commande_seq.NEXTVAL INTO a_comm_id FROM dual;
        END IF;

        -- insertion de la commande.
        INSERT INTO COMMANDE VALUES(a_comm_id, a_exe_ordre,a_comm_numero, a_tyet_id, a_fou_ordre,
           my_comm_reference, a_comm_libelle, a_tyet_id_imprimable, a_dev_id, a_utl_ordre, SYSDATE, SYSDATE);

        -- verification et correction de l'etat de la commande.
        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE upd_commande (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_fou_ordre            COMMANDE.fou_ordre%TYPE,
      a_comm_reference        COMMANDE.comm_reference%TYPE,
      a_comm_libelle        COMMANDE.comm_libelle%TYPE,
      a_utl_ordre            COMMANDE.utl_ordre%TYPE
   ) IS
     my_nb                  INTEGER;
     my_commande            COMMANDE%ROWTYPE;
      my_comm_reference        COMMANDE.comm_reference%TYPE;
   BEGIN
           SELECT * INTO my_commande FROM COMMANDE WHERE comm_id=a_comm_id;

        IF a_comm_reference IS NULL THEN
           my_comm_reference:=TO_CHAR(my_commande.comm_numero);
        ELSE
           my_comm_reference:=a_comm_reference;
        END IF;

        IF my_commande.tyet_id=Etats.get_etat_annulee THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande est annulee on ne peut rien modifier');
        END IF;

        IF my_commande.tyet_id=Etats.get_etat_precommande THEN
           UPDATE COMMANDE SET comm_reference=my_comm_reference,
              comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre WHERE comm_id=a_comm_id;

           -- si c'est une precommande et que ce n'est pas un marche on peut changer le fournisseur.
           SELECT COUNT(*) INTO my_nb FROM ARTICLE WHERE comm_id=a_comm_id AND att_ordre IS NOT NULL;
           IF my_nb=0 AND my_commande.fou_ordre<>a_fou_ordre THEN
              UPDATE COMMANDE SET fou_ordre=a_fou_ordre WHERE comm_id=a_comm_id;
           END IF;

           -- pour le moment on bloque le fournisseur d'une commande sur marche... on pourrait chercher.
           -- si le nouveau fournisseur est le titulaire ou un sous_traitant ... a voir plus tard.
           IF my_nb>0 AND my_commande.fou_ordre<>a_fou_ordre THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande est sur marche on ne peut pas modifier le fournisseur');
           END IF;
        ELSE
           IF my_commande.fou_ordre<>a_fou_ordre THEN
              RAISE_APPLICATION_ERROR(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier le fournisseur');
           END IF;

           --if my_commande.comm_reference<>a_comm_reference then
           --   raise_application_error(-20001, 'La commande est engagee ou partiellement engagee on ne peut pas modifier la reference');
           --end if;

           UPDATE COMMANDE SET fou_ordre=a_fou_ordre, comm_reference=my_comm_reference,
              comm_libelle=a_comm_libelle, utl_ordre=a_utl_ordre, comm_date=SYSDATE WHERE comm_id=a_comm_id;
        END IF;

        update engage_budget set eng_libelle=a_comm_libelle where eng_libelle<>a_comm_libelle
           and eng_id in (select eng_id from commande_engagement where comm_id=a_comm_id);
           
        jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);

        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_commande_engagement (
      a_come_id    IN OUT        COMMANDE_ENGAGEMENT.come_id%TYPE,
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
     my_nb                       INTEGER;
     my_tyap_id                  ENGAGE_BUDGET.tyap_id%TYPE;
     my_montant_budgetaire       ENGAGE_BUDGET.eng_montant_budgetaire%TYPE;
     my_montant_budgetaire_reste ENGAGE_BUDGET.eng_montant_budgetaire_reste%TYPE;
     my_eng_exe_ordre             ENGAGE_BUDGET.exe_ordre%TYPE;
     my_eng_fou_ordre             ENGAGE_BUDGET.fou_ordre%TYPE;
     my_cde_exe_ordre             COMMANDE.exe_ordre%TYPE;
     my_cde_fou_ordre             COMMANDE.fou_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement n''existe pas ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE WHERE comm_id=a_comm_id;
        IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La commande n''existe pas ('||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_ENGAGEMENT WHERE eng_id=a_eng_id;
        IF my_nb>0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement est deja utilise pour une commande ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        SELECT tyap_id, eng_montant_budgetaire, eng_montant_budgetaire_reste, exe_ordre, fou_ordre
          INTO my_tyap_id, my_montant_budgetaire, my_montant_budgetaire_reste,my_eng_exe_ordre,
               my_eng_fou_ordre
          FROM ENGAGE_BUDGET WHERE eng_id=a_eng_id;

        SELECT exe_ordre, fou_ordre INTO my_cde_exe_ordre, my_cde_fou_ordre
          FROM COMMANDE WHERE comm_id=a_comm_id;

        IF my_eng_exe_ordre<>my_cde_exe_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas sur le meme exercice ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        IF my_eng_fou_ordre<>my_cde_fou_ordre THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement et la commande ne sont pas pour le meme fournisseur ('||
              INDICATION_ERREUR.engagement(a_eng_id)||', '||INDICATION_ERREUR.commande(a_comm_id)||')');
        END IF;

        IF my_montant_budgetaire>my_montant_budgetaire_reste THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement est deja partiellement solde ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        IF my_tyap_id<>Get_Type_Application THEN
           RAISE_APPLICATION_ERROR(-20001, 'Cet engagement n''est pas genere par l''application Carambole ('||INDICATION_ERREUR.engagement(a_eng_id)||')');
        END IF;

        -- si pas de probleme on insere.
        IF a_come_id IS NULL THEN
           SELECT commande_engagement_seq.NEXTVAL INTO a_come_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_ENGAGEMENT VALUES (a_come_id, a_comm_id, a_eng_id);

        -- verification et correction de l'etat de la commande.
        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande_engagement (
      a_comm_id             COMMANDE.comm_id%TYPE,
      a_eng_id              ENGAGE_BUDGET.eng_id%TYPE
   ) IS
   BEGIN
           DELETE FROM COMMANDE_ENGAGEMENT WHERE comm_id=a_comm_id AND eng_id=a_eng_id;
        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE del_commande (
      a_comm_id             COMMANDE.comm_id%TYPE
   ) IS
   BEGIN
        Verifier.verifier_util_commande(a_comm_id);

        UPDATE COMMANDE SET tyet_id=Etats.get_etat_annulee,
             tyet_id_imprimable=Etats.get_etat_non_imprimable WHERE comm_id=a_comm_id;
   END;

   PROCEDURE del_commande_budget (
      a_cbud_id             COMMANDE_BUDGET.cbud_id%TYPE
   ) IS
     my_comm_id             COMMANDE_BUDGET.comm_id%TYPE;
   BEGIN
        Verifier.verifier_util_commande_budget(a_cbud_id);

        SELECT comm_id INTO my_comm_id FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_article (
      a_art_id IN OUT        ARTICLE.art_id%TYPE,
      a_comm_id                ARTICLE.comm_id%TYPE,
      a_art_libelle            ARTICLE.art_libelle%TYPE,
      a_art_prix_ht            ARTICLE.art_prix_ht%TYPE,
      a_art_prix_ttc        ARTICLE.art_prix_ttc%TYPE,
      a_art_quantite        ARTICLE.art_quantite%TYPE,
      a_art_prix_total_ht    ARTICLE.art_prix_total_ht%TYPE,
      a_art_prix_total_ttc    ARTICLE.art_prix_total_ttc%TYPE,
      a_art_reference        ARTICLE.art_reference%TYPE,
      a_artc_id                ARTICLE.artc_id%TYPE,
      a_att_ordre            ARTICLE.att_ordre%TYPE,
      a_ce_ordre            ARTICLE.ce_ordre%TYPE,
      a_tva_id                ARTICLE.tva_id%TYPE,
      a_art_id_pere            ARTICLE.art_id_pere%TYPE,
      a_typa_id                ARTICLE.typa_id%TYPE
   ) IS
      my_cm_niveau            v_code_marche.cm_niveau%TYPE;
      my_exe_ordre            v_code_exer.exe_ordre%TYPE;
      my_article_cm_niveau    NUMBER;
   BEGIN

              -- verification des montants.
        IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
        END IF;

        IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
        END IF;

        IF a_art_quantite<=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
        END IF;

        -- verification de l'attribution et type achat.
        IF a_att_ordre IS NULL AND a_typa_id IS NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
        END IF;

        IF a_att_ordre IS NOT NULL AND a_typa_id IS NOT NULL THEN
           RAISE_APPLICATION_ERROR(-20001, 'il faut une attribution ou un type achat');
        END IF;

        -- verification du niveau du code_exer de l'article.
        SELECT cm_niveau, exe_ordre INTO my_cm_niveau, my_exe_ordre FROM v_code_exer e, v_code_marche c
        WHERE e.cm_ordre=c.cm_ordre AND e.ce_ordre=a_ce_ordre;

        my_article_cm_niveau:=grhum.en_nombre(Get_Parametre(my_exe_ordre, 'ARTICLE_CM_NIVEAU'));
        IF my_article_cm_niveau<>my_cm_niveau THEN
           RAISE_APPLICATION_ERROR(-20001, 'l''article doit avoir un code de nomenclature de niveau'||
             my_article_cm_niveau);
        END IF;

           -- enregistrement dans la table.
        IF a_art_id IS NULL THEN
           SELECT article_seq.NEXTVAL INTO a_art_id FROM dual;
        END IF;

        INSERT INTO ARTICLE VALUES (a_art_id, a_comm_id, a_art_libelle, a_art_prix_ht, a_art_prix_ttc,
           a_art_quantite, a_art_prix_total_ht, a_art_prix_total_ttc, a_art_reference, a_artc_id,
           a_att_ordre, a_ce_ordre, a_tva_id, a_art_id_pere, a_typa_id);

        -- verification et correction de l'etat de la commande.
        jefy_marches.service_achat_execution.demande_controle_commande(a_comm_id);
        Corriger.corriger_etats_commande(a_comm_id);
   END;

   PROCEDURE ins_article_catalogue (
      a_caar_id IN OUT        jefy_catalogue.catalogue_article.caar_id%TYPE,
      a_fou_ordre            jefy_catalogue.catalogue.fou_ordre%TYPE,
      a_art_libelle            ARTICLE.art_libelle%TYPE,
      a_art_prix_ht            ARTICLE.art_prix_ht%TYPE,
      a_art_prix_ttc        ARTICLE.art_prix_ttc%TYPE,
      a_art_reference        ARTICLE.art_reference%TYPE,
      a_cm_ordre            jefy_catalogue.ARTICLE.cm_ordre%TYPE,
      a_tva_id                ARTICLE.tva_id%TYPE
   ) IS
        my_nb                    INTEGER;
     my_cat_id                jefy_catalogue.catalogue.cat_id%TYPE;
     my_art_id                jefy_catalogue.ARTICLE.art_id%TYPE;
   BEGIN
          -- recherche du catalogue depense.
        SELECT COUNT(*) INTO my_nb FROM jefy_catalogue.catalogue
           WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
        IF my_nb=0 THEN
           jefy_catalogue.gestion_catalogue.ins_catalogue(my_cat_id, 'catalogue depense', a_fou_ordre, Get_Type_Application,
                   Get_Type_Etat('VALIDE'), SYSDATE, NULL, 'cree par carambole');
        ELSE
           SELECT MAX(cat_id) INTO my_cat_id FROM jefy_catalogue.catalogue
              WHERE fou_ordre=a_fou_ordre AND tyap_id=Get_Type_Application;
        END IF;

        -- on insere l'article.
        jefy_catalogue.gestion_catalogue.ins_article(my_art_id, a_art_libelle, NULL, a_cm_ordre,
        Get_Type_Article('ARTICLE'));

        -- on le rattache au catalogue.
        jefy_catalogue.gestion_catalogue.ins_catalogue_article(a_caar_id, my_cat_id, my_art_id,
           a_art_reference, a_art_prix_ht, a_art_prix_ttc, a_tva_id, Get_Type_Etat('VALIDE'),
             NULL, NULL);
   END;

   PROCEDURE upd_article (
      a_art_id              ARTICLE.art_id%TYPE,
      a_art_libelle            ARTICLE.art_libelle%TYPE,
      a_art_prix_ht            ARTICLE.art_prix_ht%TYPE,
      a_art_prix_ttc        ARTICLE.art_prix_ttc%TYPE,
      a_art_quantite        ARTICLE.art_quantite%TYPE,
      a_art_prix_total_ht    ARTICLE.art_prix_total_ht%TYPE,
      a_art_prix_total_ttc    ARTICLE.art_prix_total_ttc%TYPE,
      a_art_reference        ARTICLE.art_reference%TYPE,
      a_ce_ordre            ARTICLE.ce_ordre%TYPE,
      a_tva_id                ARTICLE.tva_id%TYPE
   ) IS
        my_comm_id            ARTICLE.comm_id%TYPE;
   BEGIN
              -- verification des montants.
        IF ABS(a_art_prix_ht)>ABS(a_art_prix_ttc) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
        END IF;

        IF ABS(a_art_prix_total_ht)>ABS(a_art_prix_total_ttc) THEN
           RAISE_APPLICATION_ERROR(-20001, 'Le HT doit etre inferieur ou egal au TTC');
        END IF;

        IF a_art_quantite<=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La quantite doit etre superieure ou egale a 0');
        END IF;

        SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;

        -- enregistrement des modifications.
        UPDATE ARTICLE SET art_libelle=a_art_libelle, art_prix_ht=a_art_prix_ht, art_prix_ttc=a_art_prix_ttc,
           art_quantite=a_art_quantite, art_prix_total_ht=a_art_prix_total_ht,
           art_prix_total_ttc=a_art_prix_total_ttc, art_reference=a_art_reference, ce_ordre=a_ce_ordre,
           tva_id=a_tva_id WHERE art_id=a_art_id;

        -- verification et correction de l'etat de la commande.
        jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
        Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE del_article (
      a_art_id              ARTICLE.art_id%TYPE)
   IS
     my_comm_id         ARTICLE.comm_id%TYPE;
   BEGIN
           SELECT comm_id INTO my_comm_id FROM ARTICLE WHERE art_id=a_art_id;
        DELETE FROM ARTICLE WHERE art_id=a_art_id;
        jefy_marches.service_achat_execution.demande_controle_commande(my_comm_id);
        Corriger.corriger_etats_commande(my_comm_id);
   END;

   PROCEDURE ins_commande_budget (
      a_cbud_id IN OUT        COMMANDE_BUDGET.cbud_id%TYPE,
      a_comm_id                   COMMANDE_BUDGET.comm_id%TYPE,
      a_exe_ordre            COMMANDE_BUDGET.exe_ordre%TYPE,
      a_org_id                COMMANDE_BUDGET.org_id%TYPE,
      a_tcd_ordre            COMMANDE_BUDGET.tcd_ordre%TYPE,
      a_tap_id                COMMANDE_BUDGET.tap_id%TYPE,
      a_cbud_ht_saisie        COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
      a_cbud_ttc_saisie        COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_cbud_ht_saisie        COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie        COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
     my_nb_decimales        NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

        IF my_cbud_ht_saisie<0 OR my_cbud_ttc_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
        END IF;

           my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

        IF my_cbud_tva_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;

        Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
              my_cbud_ht_saisie,my_cbud_ttc_saisie);

           -- enregistrement dans la table.
        IF a_cbud_id IS NULL THEN
           SELECT commande_budget_seq.NEXTVAL INTO a_cbud_id FROM dual;
        END IF;

        INSERT INTO COMMANDE_BUDGET VALUES (a_cbud_id, a_comm_id, a_exe_ordre, a_org_id, a_tcd_ordre,
          a_tap_id, my_montant_budgetaire, my_cbud_ht_saisie, my_cbud_tva_saisie,
          my_cbud_ttc_saisie);

        -- lancement des differentes procedures d'insertion des tables d'engagement.
        ins_commande_ctrl_action(a_exe_ordre,a_cbud_id,a_chaine_action);
        ins_commande_ctrl_analytique(a_exe_ordre,a_cbud_id,a_chaine_analytique);
        ins_commande_ctrl_convention(a_exe_ordre,a_cbud_id,a_chaine_convention);
        ins_commande_ctrl_hors_marche(a_exe_ordre,a_cbud_id,a_chaine_hors_marche);
        ins_commande_ctrl_marche(a_exe_ordre,a_cbud_id,a_chaine_marche);
        ins_commande_ctrl_planco(a_exe_ordre,a_cbud_id,a_chaine_planco);

        Verifier.verifier_cde_budget_coherence(a_cbud_id);
        --verifier.verifier_engage_coherence(a_eng_id);
        --apres_engage.engage(a_eng_id);
   END;

   PROCEDURE upd_commande_budget (
      a_cbud_id             COMMANDE_BUDGET.cbud_id%TYPE,
      a_cbud_ht_saisie        COMMANDE_BUDGET.cbud_ht_saisie%TYPE,
      a_cbud_ttc_saisie        COMMANDE_BUDGET.cbud_ttc_saisie%TYPE,
      a_tap_id                COMMANDE_BUDGET.tap_id%TYPE,
      a_chaine_action        VARCHAR2,
      a_chaine_analytique    VARCHAR2,
      a_chaine_convention    VARCHAR2,
      a_chaine_hors_marche    VARCHAR2,
      a_chaine_marche        VARCHAR2,
      a_chaine_planco        VARCHAR2
   ) IS
     my_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
     my_cbud_tva_saisie     COMMANDE_BUDGET.cbud_tva_saisie%TYPE;
     my_org_id                 COMMANDE_BUDGET.org_id%TYPE;
     my_exe_ordre              COMMANDE_BUDGET.exe_ordre%TYPE;
     my_tcd_ordre              COMMANDE_BUDGET.tcd_ordre%TYPE;
     my_nb_decimales        NUMBER;
     my_cbud_ht_saisie        COMMANDE_BUDGET.cbud_ht_saisie%TYPE;
     my_cbud_ttc_saisie        COMMANDE_BUDGET.cbud_ttc_saisie%TYPE;
   BEGIN

        IF a_cbud_ht_saisie<0 OR a_cbud_ttc_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'L''engagement devra se faire pour un montant positif');
        END IF;

        SELECT org_id, exe_ordre, tcd_ordre INTO my_org_id, my_exe_ordre, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_nb_decimales:=liquider_outils.get_nb_decimales(my_exe_ordre);
        my_cbud_ht_saisie:=round(a_cbud_ht_saisie, my_nb_decimales);
        my_cbud_ttc_saisie:=round(a_cbud_ttc_saisie, my_nb_decimales);

           my_cbud_tva_saisie:=my_cbud_ttc_saisie-my_cbud_ht_saisie;

        IF my_cbud_tva_saisie<0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
        END IF;


          Verifier.verifier_budget(my_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

        -- calcul du montant budgetaire.
        my_montant_budgetaire:=Budget.calculer_budgetaire(my_exe_ordre,a_tap_id,my_org_id,
              my_cbud_ht_saisie,my_cbud_ttc_saisie);

        UPDATE COMMANDE_BUDGET SET cbud_montant_budgetaire=my_montant_budgetaire,
            cbud_ht_saisie=my_cbud_ht_saisie, cbud_tva_saisie=my_cbud_tva_saisie,
            cbud_ttc_saisie=my_cbud_ttc_saisie, tap_id=a_tap_id WHERE cbud_id=a_cbud_id;

        DELETE FROM COMMANDE_CTRL_ACTION WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_ANALYTIQUE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_CONVENTION WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_HORS_MARCHE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;
        DELETE FROM COMMANDE_CTRL_PLANCO WHERE cbud_id=a_cbud_id;

        -- lancement des differentes procedures d'insertion des tables d'engagement.
        ins_commande_ctrl_action(my_exe_ordre,a_cbud_id,a_chaine_action);
        ins_commande_ctrl_analytique(my_exe_ordre,a_cbud_id,a_chaine_analytique);
        ins_commande_ctrl_convention(my_exe_ordre,a_cbud_id,a_chaine_convention);
        ins_commande_ctrl_hors_marche(my_exe_ordre,a_cbud_id,a_chaine_hors_marche);
        ins_commande_ctrl_marche(my_exe_ordre,a_cbud_id,a_chaine_marche);
        ins_commande_ctrl_planco(my_exe_ordre,a_cbud_id,a_chaine_planco);

        Verifier.verifier_cde_budget_coherence(a_cbud_id);
        --verifier.verifier_engage_coherence(a_eng_id);
        --apres_engage.engage(a_eng_id);
   END;

   PROCEDURE ins_commande_ctrl_action (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
      a_cbud_id          COMMANDE_BUDGET.cbud_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_cact_id                   COMMANDE_CTRL_ACTION.cact_id%TYPE;
       my_tyac_id                        COMMANDE_CTRL_ACTION.tyac_id%TYPE;
       my_cact_montant_budgetaire  COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_cact_ht_saisie             COMMANDE_CTRL_ACTION.cact_ht_saisie%TYPE;
       my_cact_tva_saisie           COMMANDE_CTRL_ACTION.cact_tva_saisie%TYPE;
       my_cact_ttc_saisie           COMMANDE_CTRL_ACTION.cact_ttc_saisie%TYPE;
       my_cact_pourcentage           COMMANDE_CTRL_ACTION.cact_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
       my_tap_id                   COMMANDE_BUDGET.tap_id%TYPE;
          my_org_id                   COMMANDE_BUDGET.org_id%TYPE;
       my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       COMMANDE_CTRL_ACTION.cact_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
        END IF;

        SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
               INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere l'action.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_tyac_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le pourcentage.
            SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cact_pourcentage FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cact_ht_saisie:=round(my_cact_ht_saisie, my_nb_decimales);
            my_cact_ttc_saisie:=round(my_cact_ttc_saisie, my_nb_decimales);

               -- verification que les montants sont bien positifs !!!.
            --if my_aact_ht_saisie<0 or my_aact_ttc_saisie<0 then
            --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
            --end if;

            -- on calcule la tva et le montant budgetaire.
            my_cact_tva_saisie := my_cact_ttc_saisie - my_cact_ht_saisie;
            IF my_cact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

              my_cact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
                   my_cact_ht_saisie, my_cact_ttc_saisie);

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_cbud_montant_budgetaire<=my_somme+my_cact_montant_budgetaire THEN
                my_cact_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT commande_ctrl_action_seq.NEXTVAL INTO my_cact_id FROM dual;

            INSERT INTO COMMANDE_CTRL_ACTION VALUES (my_cact_id,
                   a_exe_ordre, a_cbud_id, my_tyac_id, my_cact_montant_budgetaire, my_cact_pourcentage,
                   my_cact_ht_saisie, my_cact_tva_saisie, my_cact_ttc_saisie);

            --verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_tyac_id);
            --apres_engage.action(my_eact_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_cact_montant_budgetaire;
        END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_analytique (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
      a_cbud_id          COMMANDE_BUDGET.cbud_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_cana_id                   COMMANDE_CTRL_ANALYTIQUE.cana_id%TYPE;
       my_can_id                        COMMANDE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_cana_montant_budgetaire  COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_cana_ht_saisie             COMMANDE_CTRL_ANALYTIQUE.cana_ht_saisie%TYPE;
       my_cana_tva_saisie           COMMANDE_CTRL_ANALYTIQUE.cana_tva_saisie%TYPE;
       my_cana_ttc_saisie           COMMANDE_CTRL_ANALYTIQUE.cana_ttc_saisie%TYPE;
       my_cana_pourcentage           COMMANDE_CTRL_ANALYTIQUE.cana_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
       my_tap_id                   COMMANDE_BUDGET.tap_id%TYPE;
          my_org_id                   COMMANDE_BUDGET.org_id%TYPE;
       my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       COMMANDE_CTRL_ANALYTIQUE.cana_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
        END IF;

        SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
               INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le pourcentage.
            SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cana_pourcentage FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cana_ht_saisie:=round(my_cana_ht_saisie, my_nb_decimales);
            my_cana_ttc_saisie:=round(my_cana_ttc_saisie, my_nb_decimales);

               -- verification que les montants sont bien positifs !!!.
            --if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
            --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
            --end if;

            -- on calcule la tva et le montant budgetaire.
            my_cana_tva_saisie := my_cana_ttc_saisie - my_cana_ht_saisie;
            IF my_cana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

              my_cana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
                   my_cana_ht_saisie,my_cana_ttc_saisie);

            -- on teste si il n'y a pas assez de dispo.
              IF my_cbud_montant_budgetaire<=my_somme+my_cana_montant_budgetaire THEN
                my_cana_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT commande_ctrl_analytique_seq.NEXTVAL INTO my_cana_id FROM dual;

            INSERT INTO COMMANDE_CTRL_ANALYTIQUE VALUES (my_cana_id,
                   a_exe_ordre, a_cbud_id, my_can_id, my_cana_montant_budgetaire, my_cana_pourcentage,
                   my_cana_ht_saisie, my_cana_tva_saisie, my_cana_ttc_saisie);

            --verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            --apres_engage.analytique(my_eana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_cana_montant_budgetaire;
        END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_convention (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
      a_cbud_id          COMMANDE_BUDGET.cbud_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_ccon_id                   COMMANDE_CTRL_CONVENTION.ccon_id%TYPE;
       my_conv_ordre                    COMMANDE_CTRL_CONVENTION.conv_ordre%TYPE;
       my_ccon_montant_budgetaire  COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_ccon_ht_saisie             COMMANDE_CTRL_CONVENTION.ccon_ht_saisie%TYPE;
       my_ccon_tva_saisie           COMMANDE_CTRL_CONVENTION.ccon_tva_saisie%TYPE;
       my_ccon_ttc_saisie           COMMANDE_CTRL_CONVENTION.ccon_ttc_saisie%TYPE;
       my_ccon_pourcentage           COMMANDE_CTRL_CONVENTION.ccon_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
       my_tap_id                   COMMANDE_BUDGET.tap_id%TYPE;
          my_org_id                   COMMANDE_BUDGET.org_id%TYPE;
       my_tcd_ordre                COMMANDE_BUDGET.tcd_ordre%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       COMMANDE_CTRL_CONVENTION.ccon_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
        END IF;

        SELECT cbud_montant_budgetaire, tap_id, org_id, tcd_ordre
               INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le code analytique.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_conv_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le pourcentage.
            SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ccon_pourcentage FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_ccon_ht_saisie:=round(my_ccon_ht_saisie, my_nb_decimales);
            my_ccon_ttc_saisie:=round(my_ccon_ttc_saisie, my_nb_decimales);

               -- verification que les montants sont bien positifs !!!.
            --if my_aana_ht_saisie<0 or my_aana_ttc_saisie<0 then
            --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
            --end if;

            -- on calcule la tva et le montant budgetaire.
            my_ccon_tva_saisie := my_ccon_ttc_saisie - my_ccon_ht_saisie;
            IF my_ccon_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

              my_ccon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
                   my_ccon_ht_saisie,my_ccon_ttc_saisie);

            -- on teste si il n'y a pas assez de dispo.
              IF my_cbud_montant_budgetaire<=my_somme+my_ccon_montant_budgetaire THEN
                my_ccon_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT commande_ctrl_convention_seq.NEXTVAL INTO my_ccon_id FROM dual;

            INSERT INTO COMMANDE_CTRL_CONVENTION VALUES (my_ccon_id,
                   a_exe_ordre, a_cbud_id, my_conv_ordre, my_ccon_montant_budgetaire, my_ccon_pourcentage,
                   my_ccon_ht_saisie, my_ccon_tva_saisie, my_ccon_ttc_saisie);

            --verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
            --apres_engage.analytique(my_eana_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_ccon_montant_budgetaire;
        END LOOP;

   END;

   PROCEDURE ins_commande_ctrl_hors_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
      a_cbud_id          COMMANDE_BUDGET.cbud_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_chom_id                   COMMANDE_CTRL_HORS_MARCHE.chom_id%TYPE;
       my_ce_ordre                        COMMANDE_CTRL_HORS_MARCHE.ce_ordre%TYPE;
       my_typa_id                        COMMANDE_CTRL_HORS_MARCHE.typa_id%TYPE;
       my_chom_montant_budgetaire  COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_chom_ht_saisie             COMMANDE_CTRL_HORS_MARCHE.chom_ht_saisie%TYPE;
       my_chom_tva_saisie           COMMANDE_CTRL_HORS_MARCHE.chom_tva_saisie%TYPE;
       my_chom_ttc_saisie           COMMANDE_CTRL_HORS_MARCHE.chom_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
       my_tap_id                   COMMANDE_BUDGET.tap_id%TYPE;
          my_org_id                   COMMANDE_BUDGET.org_id%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       COMMANDE_CTRL_HORS_MARCHE.chom_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
        END IF;

        SELECT cbud_montant_budgetaire, tap_id, org_id
               INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le type achat.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_typa_id FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le code nomenclature.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ce_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_chom_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_chom_ht_saisie:=round(my_chom_ht_saisie, my_nb_decimales);
            my_chom_ttc_saisie:=round(my_chom_ttc_saisie, my_nb_decimales);

               -- verification que les montants sont bien positifs !!!.
            --if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
            --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
            --end if;

            -- on calcule la tva et le montant budgetaire.
            my_chom_tva_saisie := my_chom_ttc_saisie - my_chom_ht_saisie;
            IF my_chom_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

              my_chom_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
                   my_chom_ht_saisie,my_chom_ttc_saisie);

            -- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
              IF SUBSTR(my_chaine,1,1)='$' OR
              my_cbud_montant_budgetaire<=my_somme+my_chom_montant_budgetaire THEN
                my_chom_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
            END IF;

            -- insertion dans la base.
            SELECT commande_ctrl_hors_marche_seq.NEXTVAL INTO my_chom_id FROM dual;

            INSERT INTO COMMANDE_CTRL_HORS_MARCHE VALUES (my_chom_id,
                   a_exe_ordre, a_cbud_id, my_typa_id, my_ce_ordre, my_chom_montant_budgetaire,
                   NULL, my_chom_ht_saisie, my_chom_tva_saisie, my_chom_ttc_saisie);

            --verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
            --apres_engage.planco(my_epco_id);

            -- mise a jour de la somme de controle.
              my_somme:=my_somme + my_chom_montant_budgetaire;
        END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_marche (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
      a_cbud_id          COMMANDE_BUDGET.cbud_id%TYPE,
      a_chaine          VARCHAR2
   ) IS
       my_cmar_id                   COMMANDE_CTRL_MARCHE.cmar_id%TYPE;
       my_att_ordre                        COMMANDE_CTRL_MARCHE.att_ordre%TYPE;
       my_cmar_montant_budgetaire  COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_cmar_ht_saisie             COMMANDE_CTRL_MARCHE.cmar_ht_saisie%TYPE;
       my_cmar_tva_saisie           COMMANDE_CTRL_MARCHE.cmar_tva_saisie%TYPE;
       my_cmar_ttc_saisie           COMMANDE_CTRL_MARCHE.cmar_ttc_saisie%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
       my_tap_id                   COMMANDE_BUDGET.tap_id%TYPE;
          my_org_id                   COMMANDE_BUDGET.org_id%TYPE;
       my_nb                         INTEGER;
       my_chaine                   VARCHAR2(30000);
       my_somme                       COMMANDE_CTRL_MARCHE.cmar_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

        SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
        IF my_nb<>1 THEN
           RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
        END IF;

        SELECT cbud_montant_budgetaire, tap_id, org_id
               INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
        my_somme:=0;

        LOOP
              IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

            -- on recupere le compte d'imputation.
            SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_att_ordre FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ht.
            SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ht_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
            -- on recupere le ttc.
            SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cmar_ttc_saisie FROM dual;
            my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cmar_ht_saisie:=round(my_cmar_ht_saisie, my_nb_decimales);
            my_cmar_ttc_saisie:=round(my_cmar_ttc_saisie, my_nb_decimales);

               -- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cmar_tva_saisie := my_cmar_ttc_saisie - my_cmar_ht_saisie;
			IF my_cmar_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cmar_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cmar_ht_saisie,my_cmar_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cmar_montant_budgetaire THEN
			    my_cmar_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule attribution.
			SELECT COUNT(*) INTO my_nb FROM COMMANDE_CTRL_MARCHE WHERE cbud_id=a_cbud_id;

			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une attribution pour une commande');
			END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_marche_seq.NEXTVAL INTO my_cmar_id FROM dual;

			INSERT INTO COMMANDE_CTRL_MARCHE VALUES (my_cmar_id,
			       a_exe_ordre, a_cbud_id, my_att_ordre, my_cmar_montant_budgetaire, NULL,
				   my_cmar_ht_saisie, my_cmar_tva_saisie, my_cmar_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cmar_montant_budgetaire;
		END LOOP;
   END;

   PROCEDURE ins_commande_ctrl_planco (
      a_exe_ordre     COMMANDE_BUDGET.exe_ordre%TYPE,
	  a_cbud_id		  COMMANDE_BUDGET.cbud_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_cpco_id	               COMMANDE_CTRL_PLANCO.cpco_id%TYPE;
       my_pco_num	  	   		   COMMANDE_CTRL_PLANCO.pco_num%TYPE;
       my_cpco_montant_budgetaire  COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_cpco_ht_saisie	  	   COMMANDE_CTRL_PLANCO.cpco_ht_saisie%TYPE;
       my_cpco_tva_saisie		   COMMANDE_CTRL_PLANCO.cpco_tva_saisie%TYPE;
       my_cpco_ttc_saisie		   COMMANDE_CTRL_PLANCO.cpco_ttc_saisie%TYPE;
       my_cpco_pourcentage		   COMMANDE_CTRL_PLANCO.cpco_pourcentage%TYPE;
       my_cbud_montant_budgetaire  COMMANDE_BUDGET.cbud_montant_budgetaire%TYPE;
	   my_tap_id				   COMMANDE_BUDGET.tap_id%TYPE;
   	   my_org_id				   COMMANDE_BUDGET.org_id%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   COMMANDE_CTRL_PLANCO.cpco_montant_budgetaire%TYPE;
       my_nb_decimales             NUMBER;
   BEGIN
        my_nb_decimales:=liquider_outils.get_nb_decimales(a_exe_ordre);

		SELECT COUNT(*) INTO my_nb FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'Le commandeBudget n''existe pas (cbud_id='||a_cbud_id||')');
		END IF;

		SELECT cbud_montant_budgetaire, tap_id, org_id
		       INTO my_cbud_montant_budgetaire, my_tap_id, my_org_id
          FROM COMMANDE_BUDGET WHERE cbud_id=a_cbud_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le pourcentage.
			SELECT en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_cpco_pourcentage FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

            my_cpco_ht_saisie:=round(my_cpco_ht_saisie, my_nb_decimales);
            my_cpco_ttc_saisie:=round(my_cpco_ttc_saisie, my_nb_decimales);

	   		-- verification que les montants sont bien positifs !!!.
			--if my_apco_ht_saisie<0 or my_apco_ttc_saisie<0 then
		    --   raise_application_error(-20001, 'L''engagement doit se faire pour un montant positif');
		    --end if;

			-- on calcule la tva et le montant budgetaire.
			my_cpco_tva_saisie := my_cpco_ttc_saisie - my_cpco_ht_saisie;
			IF my_cpco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
            END IF;

  			my_cpco_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_cpco_ht_saisie,my_cpco_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1)='$' OR
			  my_cbud_montant_budgetaire<=my_somme+my_cpco_montant_budgetaire THEN
			    my_cpco_montant_budgetaire:=my_cbud_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT commande_ctrl_planco_seq.NEXTVAL INTO my_cpco_id FROM dual;

			INSERT INTO COMMANDE_CTRL_PLANCO VALUES (my_cpco_id,
			       a_exe_ordre, a_cbud_id, my_pco_num, my_cpco_montant_budgetaire, my_cpco_pourcentage,
				   my_cpco_ht_saisie, my_cpco_tva_saisie, my_cpco_ttc_saisie);

			--verifier.verifier_planco(a_exe_ordre, my_org_id, my_pco_num);
	    	--apres_engage.planco(my_epco_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_cpco_montant_budgetaire;
		END LOOP;
   END;

END;
/





insert into jefy_depense.db_version values (2003,'2003',to_date('21/01/2010','dd/mm/yyyy'),sysdate,null);

commit;
