/*
 * Copyright Cocktail (Consortium) 1995-2006
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.depense.server;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;


// Versions...
// nom de l'application
public final class Version {

	// Nom de l'appli
	private static final String		FRAMEWORK_ID				= "FwkCktlDepense";

	// Version appli
	private static final String		VERSIONDATE				= "07/01/2009";

	public static final int			VERSIONNUMMAJ			= 1;
	public static final int			VERSIONNUMMIN			= 4;
	public static final int			VERSIONNUMPATCH			= 2;
	public static final int			VERSIONNUMBUILD			= 3;

	// Version minimum de la base de donnees necessaire pour fonctionner avec cette version
	private static final String		MIN_APPLI_BD_VERSION	= "1422";

	
	// Collecte des informations ou non ??
	private static final boolean	COCKTAIL_COLLECTE		= true;

	public static int[] version() {
		return new int[] { VERSIONNUMMAJ, VERSIONNUMMIN, VERSIONNUMPATCH, VERSIONNUMBUILD };
	}

	public static void checkDependencies() throws Exception {
	}
	
	// Pour affichage en ligne de commande...
	public static void main(String argv[]) {
		System.out.println("");
		System.out.println(Version.appliId());
		System.out.println(Version.txtAppliVersion());
		System.out.println(Version.copyright());
		System.out.println("");
		System.out.println("Version minimum de la base de donnees necessaire : " + minAppliBdVersion());
		System.out.println("");
	}

	public static boolean isVersionBaseOk(EOEditingContext ed) {
		if (Version.minAppliBdVersion() == null)
			return true;

		String ver=getBdVersion(ed);
		if (ver == null)
			return false;

		if (ver.compareTo(Version.minAppliBdVersion()) >= 0)
			return true;

		return false;
	}

	public static String getBdVersion(EOEditingContext ed) {
		
		if (ed==null) {
			ed= new EOEditingContext();
		}
		
		NSArray a = null;
		try {
			a = EOUtilities.rawRowsForSQL(ed, "carambole", "select max(db_version_libelle) db_version_libelle from db_version", null);
			return (String) ((NSDictionary) a.lastObject()).valueForKey("DB_VERSION_LIBELLE");
		}
		catch (Exception e) {
			return null;
		}
	}

	public static String appliId() {
		return FRAMEWORK_ID;
	}

	public static String appliDate() {
		return VERSIONDATE;
	}

	public static String appliVersion() {
		return VERSIONNUMMAJ + "." + VERSIONNUMMIN + "." + VERSIONNUMPATCH + "." + VERSIONNUMBUILD;
	}

	public static Integer appliVersionMajeure() {
		return new Integer(VERSIONNUMMAJ);
	}

	public static Integer appliVersionMineure() {
		return new Integer(VERSIONNUMMIN);
	}

	public static String appliVersionPatch() {
		return "" + VERSIONNUMPATCH;
	}

	public static String minAppliBdVersion() {
		return MIN_APPLI_BD_VERSION;
	}

	public static String htmlAppliVersion() {
		return "<font color=\"#FF0000\"><b>Version " + appliVersion() + " du " + appliDate() + "</b></font>";
	}

	public static String txtAppliVersion() {
		return "Version " + appliVersion() + " du " + appliDate();
	}

	public static String copyright() {
		return "(c) " + appliDate().substring(appliDate().length() - 4) + " Consortium Cocktail";
	}

	public static boolean cocktailCollecte() {
		return COCKTAIL_COLLECTE;
	}
}
