package org.cocktail.kava.server.metier;

import static org.mockito.Mockito.*;

import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;


/**
 * Classe de test pour {@link EOPrestationLigne}.
 *
 * @author flagouey
 */
public class EOPrestationLigneTest {

	@Test
	public void testSuppressionRemisesDevenusInutilesEtMiseAJourDesQuantitesNull() {
		EOEditingContext mockEditingContext = mock(EOEditingContext.class);

		EOPrestationLigne mockLigne = mock(EOPrestationLigne.class);
		when(mockLigne.prestationLignes()).thenReturn(null);

		NSMutableArray remises = new NSMutableArray();

		EOPrestationLigne.suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(mockLigne, mockEditingContext, remises);
		verify(mockLigne, times(1)).prestationLignes();
	}

	@Test
	public void testSuppressionRemisesDevenusInutilesEtMiseAJourDesQuantitesUsecaseOptions() {
		EOEditingContext mockEditingContext = mock(EOEditingContext.class);

		NSMutableArray optionsExistantes = new NSMutableArray();
		EOPrestationLigne optionExistante1 = mock(EOPrestationLigne.class);
		EOPrestationLigne optionExistante2 = mock(EOPrestationLigne.class);
		optionsExistantes.addObject(optionExistante1);
		optionsExistantes.addObject(optionExistante2);

		EOPrestationLigne mockLigne = mock(EOPrestationLigne.class);
		when(mockLigne.prestationLignes()).thenReturn(optionsExistantes);
		when(mockLigne.isPrestationUneRemise(mockEditingContext, null)).thenReturn(Boolean.FALSE);

		NSMutableArray options = new NSMutableArray();
		EOPrestationLigne option1 = mock(EOPrestationLigne.class);
		EOPrestationLigne option2 = mock(EOPrestationLigne.class);
		options.addObject(option1);
		options.addObject(option2);

		EOPrestationLigne.suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(mockLigne, mockEditingContext, options);
		verify(mockLigne, never()).removeFromPrestationLignesRelationship(null);
		//verify(mockLigne, atLeastOnce()).isRemiseEncoreValide(options, optionExistante1);
	}

}
