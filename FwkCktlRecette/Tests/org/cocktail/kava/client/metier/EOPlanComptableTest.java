package org.cocktail.kava.client.metier;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Classe de test pour {@link EOPlanComptable}.
 *
 * @author flagouey
 */
public class EOPlanComptableTest {

	@Test
	public void testConstantes() {
		assertEquals("pcoNum", EOPlanComptable.PCO_NUM_KEY);
		assertEquals("pcoValidite", EOPlanComptable.PCO_VALIDITE_KEY);
		assertEquals("exercice", EOPlanComptable.EXERCICE_KEY);
	}

}
