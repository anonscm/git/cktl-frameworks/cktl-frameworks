package org.cocktail.kava.client.qualifier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;

/**
 * Classe de test pour {@link PlanComptableCtpQualifiers}.
 *
 * @author flagouey
 */
public class PlanComptableCtpQualifiersTest {

	private static final String PCO_PATH = "planComptable";
	private static final String PCO_NUM_PATH = "pcoNum";
	private static final String CLASSE_1_INTERDITE = "1";

	@Test
	public void testInstance() {
		assertNotNull(PlanComptableCtpQualifiers.instance());
	}

	@Test
	public void testBuildTypeEtatValide() {
		PlanComptableCtpQualifiers pcoCtpQualifiersBuilder = PlanComptableCtpQualifiers.instance();
		EOQualifier typeValideQualifier = pcoCtpQualifiersBuilder.buildTypeEtatValide(PCO_PATH);

		assertEquals(
				"(" + PCO_PATH + "." + EOPlanComptable.PCO_VALIDITE_KEY + " = 'VALIDE')",
				typeValideQualifier.toString());
	}

	@Test
	public void testBuildExercice() {
		PlanComptableCtpQualifiers pcoCtpQualifiersBuilder = PlanComptableCtpQualifiers.instance();
		EOExercice exercice = mock(EOExercice.class);

		// exercice null
		assertNull(pcoCtpQualifiersBuilder.buildExercice(PCO_PATH, null));

		// exercice non null
		EOQualifier exerciceQualifier = pcoCtpQualifiersBuilder.buildExercice(PCO_PATH, exercice);
		assertNotNull(exerciceQualifier);
		assertEquals(1, exerciceQualifier.allQualifierKeys().count());
		assertEquals(
				PCO_PATH + "." + EOPlanComptable.EXERCICE_KEY,
				exerciceQualifier.allQualifierKeys().toArray()[0]);
	}

	@Test
	public void testBuildClassesInterdites() {
		PlanComptableCtpQualifiers pcoCtpQualifiersBuilder = PlanComptableCtpQualifiers.instance();

		// parametre null
		assertNull(pcoCtpQualifiersBuilder.buildClassesInterdites(PCO_NUM_PATH, null));

		// classes interdites '1'
		List<String> classesInterdites = new ArrayList<String>();
		classesInterdites.add(CLASSE_1_INTERDITE);

		EOQualifier classesInterditesQualifier =
				pcoCtpQualifiersBuilder.buildClassesInterdites(PCO_NUM_PATH, classesInterdites);
		assertNotNull(classesInterditesQualifier);
		assertEquals(
				"(not ((" + PCO_NUM_PATH + " like '" + CLASSE_1_INTERDITE + "*')))",
				classesInterditesQualifier.toString());
	}

}
