/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.kava.client.selector;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.selector.FournisseurEtatControle;
import org.cocktail.kava.client.selector.FournisseurFiltre;
import org.cocktail.pieFwk.selector.IFiltre;
import org.cocktail.pieFwk.selector.IListeFiltre;
import org.junit.Test;

/**
 * Classe de test pour {@link FournisseurFiltre}.
 *
 * @author flagouey
 */
public class FournisseurFiltreTest {


	@Test
	public void testFiltrerFournisseursValidesNull() {
		IListeFiltre<EOFournisUlr> listeFiltreFournisseurValide = new FournisseurFiltre();
		IFiltre<EOFournisUlr> filtreFournisseurValide = new FournisseurEtatControle(EOFournisUlr.FOU_ETAT_VALIDE);

		List<EOFournisUlr> fournisseursValides = listeFiltreFournisseurValide.filtrer(null, filtreFournisseurValide);
		assertNotNull(fournisseursValides);
		assertTrue(fournisseursValides.isEmpty());
	}

	@Test
	public void testFiltrerFournisseursValidesEtAttente() {
		IListeFiltre<EOFournisUlr> listeFiltreFournisseurs = new FournisseurFiltre();
		IFiltre<EOFournisUlr> filtreFournisseurValide = new FournisseurEtatControle(EOFournisUlr.FOU_ETAT_VALIDE);
		IFiltre<EOFournisUlr> filtreFournisseurEnAttente = new FournisseurEtatControle(EOFournisUlr.FOU_ETAT_ATTENTE);


		// test avec donnees (fournisseurs valide, en attente, invalide)
		EOFournisUlr fournisseurValide = buildFournisseur(EOFournisUlr.FOU_ETAT_VALIDE, true);
		EOFournisUlr fournisseurEnAttente = buildFournisseur(EOFournisUlr.FOU_ETAT_ATTENTE, true);
		EOFournisUlr fournisseurInvalide = buildFournisseur(EOFournisUlr.FOU_ETAT_INVALIDE, true);
		List<EOFournisUlr> fournisseurs = new ArrayList<EOFournisUlr>();
		fournisseurs.add(fournisseurValide);
		fournisseurs.add(fournisseurEnAttente);
		fournisseurs.add(fournisseurInvalide);

		fournisseurs = listeFiltreFournisseurs.filtrer(fournisseurs, filtreFournisseurValide, filtreFournisseurEnAttente);
		assertEquals(2, fournisseurs.size());
	}


	private EOFournisUlr buildFournisseur(String etatFournisseur, boolean valide) {
		EOFournisUlr fournisseur = mock(EOFournisUlr.class);
		when(fournisseur.isEtat(etatFournisseur)).thenReturn(valide);
		return fournisseur;
	}
}
