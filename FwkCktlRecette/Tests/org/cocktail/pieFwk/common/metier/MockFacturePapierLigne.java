package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class MockFacturePapierLigne implements FacturePapierLigne {

	private BigDecimal quantite;
	private BigDecimal ligneHT;
	private BigDecimal ligneTTC;
	private BigDecimal ligneTotalHT;
	private BigDecimal ligneTotalTTC;

	private PrestationLigne prestationLigne;
	private FacturePapier facturePapier;
	private NSMutableArray lignesEnfants;

	private ApplicationConfig applicationConfig;
	private Number exerciceAsNumber;

	private FacturePapierLigneCompanion companion;

	public MockFacturePapierLigne() {
		this(null);
	}

	public MockFacturePapierLigne(FacturePapier facturePapier) {
		this.prestationLigne = null;
		this.facturePapier = facturePapier;
		this.lignesEnfants = new NSMutableArray();
		this.companion = new FacturePapierLigneCompanion(this);
	}

	public FacturePapierLigneCompanion companion() {
		return this.companion;
	}

	public void setFligQuantite(BigDecimal quantite) {
		this.quantite = quantite;
	}

	public BigDecimal fligQuantite() {
		return this.quantite;
	}

	public BigDecimal fligArtHt() {
		return this.ligneHT;
	}

	public BigDecimal fligArtTtc() {
		return this.ligneTTC;
	}

	public void setFligTotalHt(BigDecimal totalHT) {
		this.ligneTotalHT = totalHT;
	}

	public void setFligTotalTtc(BigDecimal totalTTC) {
		this.ligneTotalTTC = totalTTC;
	}

	public void addToFacturePapierLignesRelationship(FacturePapierLigne ligne) {
		this.lignesEnfants.addObject(ligne);
	}

	public NSArray facturePapierLignes() {
		return this.lignesEnfants;
	}

	public BigDecimal fligTotalHt() {
		return ligneTotalHT;
	}

	public BigDecimal fligTotalTtc() {
		return ligneTotalTTC;
	}

	public void setFligArtHt(BigDecimal montantHT) {
		this.ligneHT = montantHT;
	}

	public void setFligArtTtc(BigDecimal montantTTC) {
		this.ligneTTC = montantTTC;
	}

	public FacturePapier getFacturePapierCommon() {
		return this.facturePapier;
	}

	public PrestationLigne getPrestationLigneCommon() {
		return prestationLigne;
	}

	public TauxTva tauxTva() {
		return null;
	}

	public ApplicationConfig applicationConfig() {
		return this.applicationConfig;
	}

	public EOEditingContext editingContext() {
		return null;
	}

	public Number exerciceAsNumber() {
		return this.exerciceAsNumber;
	}

	public void setApplicationConfig(ApplicationConfig applicationConfig) {
		this.applicationConfig = applicationConfig;
	}

	public void setExerciceAsNumber(Number exerciceAsNumber) {
		this.exerciceAsNumber = exerciceAsNumber;
	}
}
