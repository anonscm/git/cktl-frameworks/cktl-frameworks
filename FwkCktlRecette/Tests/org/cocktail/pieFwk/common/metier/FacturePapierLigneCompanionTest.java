package org.cocktail.pieFwk.common.metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.service.AbstractFacturationService;
import org.junit.Ignore;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;

public class FacturePapierLigneCompanionTest {

	@Test
	public void testIsCalculPossible() {
		FacturePapierLigne ligne = new MockFacturePapierLigne();
		FacturePapierLigneCompanion companion = new FacturePapierLigneCompanion(ligne);

		assertFalse(companion.isCalculPossible(null, null));
		assertFalse(companion.isCalculPossible(BigDecimal.TEN, null));
		assertFalse(companion.isCalculPossible(null, BigDecimal.TEN));
		assertTrue(companion.isCalculPossible(BigDecimal.TEN, BigDecimal.ONE));
	}

	@Test
	public void testUpdateTotalHtEchec() {
		FacturePapierLigne ligne = new MockFacturePapierLigne();
		FacturePapierLigneCompanion companion = new FacturePapierLigneCompanion(ligne) {
			@Override
			protected boolean isCalculPossible(BigDecimal montant, BigDecimal quantite) {
				return false;
			}
		};

		companion.updateTotalHt();
		assertNull(ligne.fligTotalHt());
	}

	@Test
	public void testUpdateTotalHtSucces() {
		MockFacturePapierLigne ligne = new MockFacturePapierLigne();
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});

		FacturePapierLigneCompanion companion = new FacturePapierLigneCompanion(ligne);

		// test
		ligne.setFligArtHt(BigDecimal.ZERO);
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalHt();
		assertEquals(BigDecimal.ZERO, ligne.fligTotalHt());

		// test - scale 3
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(BigDecimal.valueOf(100.33d));
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalHt();
		assertEquals(BigDecimal.valueOf(200.66d).setScale(3), ligne.fligTotalHt());

		// test - scale 0
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtHt(BigDecimal.valueOf(100d).setScale(0));
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalHt();
		assertEquals(BigDecimal.valueOf(200d).setScale(0), ligne.fligTotalHt());
	}

	@Test
	public void testUpdateTotalTtc() {
		MockFacturePapierLigne ligne = new MockFacturePapierLigne();
		FacturePapierLigneCompanion companion = new FacturePapierLigneCompanion(ligne);

		// test echec
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtTtc(null);
		ligne.setFligQuantite(null);
		companion.updateTotalTtc();
		assertNull(ligne.fligTotalTtc());

		// test succes
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtHt(null);
		ligne.setFligArtTtc(BigDecimal.TEN);
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalTtc();
		assertEquals(BigDecimal.ZERO, ligne.fligTotalTtc());

		// test succes avec TVA par défaut (0%) --> on considere qu'il s'agit d'un montant de TVA sans HT.
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(BigDecimal.ZERO.setScale(3));
		ligne.setFligArtTtc(BigDecimal.valueOf(4.25));
		ligne.setFligQuantite(BigDecimal.valueOf(3d));
		companion.updateTotalTtc();
		assertEquals(BigDecimal.valueOf(12.75d).setScale(3), ligne.fligTotalTtc());

		// test succes avec TVA (19.6%) avec scale 2
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(BigDecimal.valueOf(100d));
		ligne.setFligArtTtc(BigDecimal.valueOf(119.6d));
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalTtc();
		assertEquals(BigDecimal.valueOf(239.2d).setScale(3), ligne.fligTotalTtc());

		// test succes avec TVA (19.6%) avec scale 0
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtHt(BigDecimal.valueOf(100d).setScale(0));
		ligne.setFligArtTtc(BigDecimal.valueOf(119.6d));
		ligne.setFligQuantite(BigDecimal.valueOf(2d));
		companion.updateTotalTtc();
		assertEquals(BigDecimal.valueOf(239d).setScale(0), ligne.fligTotalTtc());
	}

	@Test
	public void testModifierQuantiteAvecVerificationSetter() {
		// montant HT et TTC non renseignés
		FacturePapierLigne ligneQteSeule = new MockFacturePapierLigne();
		FacturePapierLigneCompanion companionQteSeule = new FacturePapierLigneCompanion(ligneQteSeule);
		companionQteSeule.modifierQuantite(BigDecimal.TEN);
		assertEquals(BigDecimal.TEN, ligneQteSeule.fligQuantite());
		assertNull(ligneQteSeule.fligTotalHt());
		assertNull(ligneQteSeule.fligTotalTtc());
	}

	@Test
	public void testModifierQuantiteAvecEnfantsAvecVerificationSetter() {
		// montant HT et TTC non renseignés
		MockFacturePapierLigne ligneQteSeule = new MockFacturePapierLigne();
		ligneQteSeule.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});

		ligneQteSeule.addToFacturePapierLignesRelationship(new MockFacturePapierLigne());

		ligneQteSeule.setFligQuantite(BigDecimal.ZERO);
		ligneQteSeule.companion().modifierQuantite(BigDecimal.TEN);
		assertEquals(BigDecimal.TEN, ligneQteSeule.fligQuantite());
		assertNull(ligneQteSeule.fligTotalHt());
		assertNull(ligneQteSeule.fligTotalTtc());

		FacturePapierLigne enfantQteSeule = (FacturePapierLigne) ligneQteSeule.facturePapierLignes().objectAtIndex(0);
		assertEquals(BigDecimal.TEN, enfantQteSeule.fligQuantite());
		assertNull(enfantQteSeule.fligTotalHt());
		assertNull(enfantQteSeule.fligTotalTtc());

		// montant HT renseigné et TTC non renseigné
		MockFacturePapierLigne ligneQteEtHT = new MockFacturePapierLigne();
		ligneQteEtHT.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});

		ligneQteEtHT.addToFacturePapierLignesRelationship(new MockFacturePapierLigne());

		ligneQteEtHT.setFligQuantite(BigDecimal.TEN);
		ligneQteEtHT.setFligArtHt(BigDecimal.valueOf(2d));
		ligneQteEtHT.companion().modifierQuantite(BigDecimal.TEN);
		assertEquals(BigDecimal.TEN, ligneQteEtHT.fligQuantite());
		BigDecimal totalHTExpected = BigDecimal.valueOf(20d).setScale(3);
		assertEquals(totalHTExpected, ligneQteEtHT.fligTotalHt());
		assertNull(ligneQteEtHT.fligTotalTtc());

		FacturePapierLigne enfantQteEtHT = (FacturePapierLigne) ligneQteEtHT.facturePapierLignes().objectAtIndex(0);
		assertEquals(BigDecimal.TEN, enfantQteEtHT.fligQuantite());
		assertNull(enfantQteEtHT.fligTotalHt());
		assertNull(enfantQteEtHT.fligTotalTtc());
	}

	@Test
	public void testModifierQuantiteAvec2DecimalesEtVerificationMiseAJourTotaux() {
		final BigDecimal MT_UNITAIRE_HT = BigDecimal.valueOf(774d).setScale(3);
		final BigDecimal MT_UNITAIRE_TTC = BigDecimal.valueOf(813d).setScale(3);
		final BigDecimal QUANTITE = BigDecimal.valueOf(25d);

		final BigDecimal TOTAL_HT = BigDecimal.valueOf(19350d).setScale(3);;
		final BigDecimal TOTAL_TTC = BigDecimal.valueOf(20317.50d).setScale(3);;

		MockFacturePapierLigne ligne = new MockFacturePapierLigne();
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(MT_UNITAIRE_HT);
		ligne.setFligArtTtc(MT_UNITAIRE_TTC);

		ligne.companion().modifierQuantite(QUANTITE);
		assertEquals(MT_UNITAIRE_HT, ligne.fligArtHt());
		assertEquals(MT_UNITAIRE_TTC, ligne.fligArtTtc());
		assertEquals(TOTAL_HT, ligne.fligTotalHt());
		assertEquals(TOTAL_TTC, ligne.fligTotalTtc());
	}

	@Test
	public void testModifierQuantiteSansDecimaleEtVerificationMiseAJourTotaux() {
		final BigDecimal MT_UNITAIRE_HT = BigDecimal.valueOf(774d).setScale(0);
		final BigDecimal MT_UNITAIRE_TTC = BigDecimal.valueOf(813d).setScale(0);
		final BigDecimal QUANTITE = BigDecimal.valueOf(25d);

		final BigDecimal TOTAL_HT = BigDecimal.valueOf(19350d).setScale(0);
		final BigDecimal TOTAL_TTC = BigDecimal.valueOf(20318d).setScale(0);

		MockFacturePapierLigne ligne = new MockFacturePapierLigne();
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});		ligne.setFligArtHt(MT_UNITAIRE_HT);
		ligne.setFligArtTtc(MT_UNITAIRE_TTC);

		ligne.companion().modifierQuantite(QUANTITE);
		assertEquals(MT_UNITAIRE_HT, ligne.fligArtHt());
		assertEquals(MT_UNITAIRE_TTC, ligne.fligArtTtc());
		assertEquals(TOTAL_HT, ligne.fligTotalHt());
		assertEquals(TOTAL_TTC, ligne.fligTotalTtc());
	}

}
