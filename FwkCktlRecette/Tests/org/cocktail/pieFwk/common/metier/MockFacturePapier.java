package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class MockFacturePapier implements FacturePapier {

	private FacturePapierCompanion companion;

	private BigDecimal totalHT;
	private BigDecimal totalTVA;
	private BigDecimal totalTTC;
	private BigDecimal remiseGlobale;

	public MockFacturePapier() {
		this.companion = new FacturePapierCompanion(this);
	}

	public FacturePapierCompanion companion() {
		return this.companion;
	}

	public void setTotalHT(BigDecimal totalHT) {
		this.totalHT = totalHT;
	}

	public void setTotalTVA(BigDecimal totalTVA) {
		this.totalTVA = totalTVA;
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		this.totalTTC = totalTTC;
	}

	public BigDecimal remiseGlobale() {
		return null;
	}

	public NSArray getFacturePapierLignesCommon() {
		return null;
	}

	public ApplicationConfig applicationConfig() {
		return null;
	}

	public EOEditingContext editingContext() {
		return null;
	}

	public Number exerciceAsNumber() {
		return null;
	}

}
