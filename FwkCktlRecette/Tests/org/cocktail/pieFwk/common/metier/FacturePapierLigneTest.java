package org.cocktail.pieFwk.common.metier;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.service.AbstractFacturationService;
import org.junit.Ignore;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;

public class FacturePapierLigneTest {

	@Test
	public void testSetFligQuantite() {
		// montant HT et TTC non renseignés
		FacturePapierLigne ligneQteSeule = new MockFacturePapierLigne();
		ligneQteSeule.setFligQuantite(BigDecimal.TEN);
		assertEquals(BigDecimal.TEN, ligneQteSeule.fligQuantite());
		assertNull(ligneQteSeule.fligTotalHt());
		assertNull(ligneQteSeule.fligTotalTtc());

		// montant HT renseigné et TTC non renseigné
		MockFacturePapierLigne ligneQteEtHT = new MockFacturePapierLigne();
		ligneQteEtHT.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligneQteEtHT.setFligArtHt(BigDecimal.valueOf(2d));
		ligneQteEtHT.companion().modifierQuantite(BigDecimal.TEN);

		assertEquals(BigDecimal.TEN, ligneQteEtHT.fligQuantite());
		BigDecimal totalHTExpected = BigDecimal.valueOf(20d).setScale(3);
		assertEquals(totalHTExpected, ligneQteEtHT.fligTotalHt());
		assertNull(ligneQteEtHT.fligTotalTtc());
	}

}
