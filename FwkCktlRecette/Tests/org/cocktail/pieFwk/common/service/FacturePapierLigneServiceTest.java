package org.cocktail.pieFwk.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.MockFacturePapier;
import org.cocktail.pieFwk.common.metier.MockFacturePapierLigne;
import org.junit.Ignore;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;

public class FacturePapierLigneServiceTest {

	private FacturePapierLigneService ligneService = FacturePapierLigneService.instance();

	@Test
	public void testEstDivisible() {
		assertFalse(ligneService.estDivisible(null));
		assertFalse(ligneService.estDivisible(BigDecimal.ZERO));
		assertTrue(ligneService.estDivisible(BigDecimal.TEN));
	}

	@Test
	public void testIsCalculPossible() {
		assertFalse(ligneService.isCalculPossible(null, null, null));
		assertFalse(ligneService.isCalculPossible(BigDecimal.TEN, null, null));
		assertFalse(ligneService.isCalculPossible(null, BigDecimal.TEN, null));
		assertFalse(ligneService.isCalculPossible(null, null, BigDecimal.TEN));
		assertFalse(ligneService.isCalculPossible(BigDecimal.TEN, BigDecimal.TEN, null));
		assertFalse(ligneService.isCalculPossible(BigDecimal.TEN, null, BigDecimal.TEN));
		assertFalse(ligneService.isCalculPossible(null, BigDecimal.TEN, BigDecimal.TEN));
		assertTrue(ligneService.isCalculPossible(BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN));
	}

	@Test
	public void testCalculerTauxTva() {
		assertEquals(BigDecimal.valueOf(1.196d),
				ligneService.calculerTauxTva(
						BigDecimal.valueOf(100.00d), BigDecimal.valueOf(119.60d), 3, BigDecimal.ONE));

		assertEquals(BigDecimal.valueOf(1.2d).setScale(2),
				ligneService.calculerTauxTva(
						BigDecimal.valueOf(100.00d), BigDecimal.valueOf(119.60d), 2, BigDecimal.ONE));

		assertEquals(BigDecimal.valueOf(1.058d),
				ligneService.calculerTauxTva(
						BigDecimal.valueOf(52d), BigDecimal.valueOf(55d), 3, BigDecimal.ONE));

		assertEquals(BigDecimal.ONE,
				ligneService.calculerTauxTva(
						BigDecimal.ZERO, BigDecimal.valueOf(55d), 3, BigDecimal.ONE));

		assertEquals(
				BigDecimal.valueOf(1.196d),
				ligneService.calculerTauxTva(BigDecimal.valueOf(19.6d), 3));
	}

	@Test
	public void testCalculerMontantTotalHTExact() {
		BigDecimal TOTAL_HT = BigDecimal.valueOf(30d).setScale(2);
		assertEquals(TOTAL_HT, ligneService.calculerMontantTotalHTExact(BigDecimal.valueOf(10d), BigDecimal.valueOf(3d)));
	}

	@Test
	public void testIsCalculTotalTTCAvecTVACommeBaseCalcul() {
		assertFalse(ligneService.isCalculTotalTTCAvecTVACommeBaseCalcul(BigDecimal.TEN, BigDecimal.TEN, BigDecimal.TEN));
		assertFalse(ligneService.isCalculTotalTTCAvecTVACommeBaseCalcul(BigDecimal.TEN, BigDecimal.ZERO, BigDecimal.TEN));
		assertFalse(ligneService.isCalculTotalTTCAvecTVACommeBaseCalcul(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.TEN));
		assertTrue(ligneService.isCalculTotalTTCAvecTVACommeBaseCalcul(BigDecimal.ZERO, BigDecimal.TEN, BigDecimal.TEN));
	}

	@Test
	public void testCalculerMontantTotalTTCRemiseIncluseAvecDecimales() {
		FacturePapier facturePapierSansRemise = new MockFacturePapier() {
			public BigDecimal remiseGlobale() {
				return BigDecimal.ZERO;
			}
		};

		final BigDecimal MT_HT = BigDecimal.valueOf(100d).setScale(3, BigDecimal.ROUND_HALF_UP);
		final BigDecimal MT_TTC = BigDecimal.valueOf(119.6).setScale(3, BigDecimal.ROUND_HALF_UP);
		final BigDecimal QUANTITE = BigDecimal.valueOf(1d);

		MockFacturePapierLigne ligne = new MockFacturePapierLigne(facturePapierSansRemise);
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(MT_HT);
		ligne.setFligArtTtc(MT_TTC);
		ligne.setFligQuantite(QUANTITE);


		BigDecimal expected = BigDecimal.valueOf(119.6d).setScale(3, BigDecimal.ROUND_HALF_UP);
		assertEquals(expected, ligneService.calculerMontantTotalTTCRemiseIncluse(ligne));
	}

	@Test
	public void testCalculerMontantTotalTTCRemiseIncluseSansDecimales() {
		FacturePapier facturePapierSansRemise = new MockFacturePapier() {
			public BigDecimal remiseGlobale() {
				return BigDecimal.ZERO;
			}
		};

		final BigDecimal MT_HT = BigDecimal.valueOf(100d).setScale(0, BigDecimal.ROUND_HALF_UP);
		final BigDecimal MT_TTC = BigDecimal.valueOf(119.6).setScale(0, BigDecimal.ROUND_HALF_UP);
		final BigDecimal QUANTITE = BigDecimal.valueOf(1d);

		MockFacturePapierLigne ligne = new MockFacturePapierLigne(facturePapierSansRemise);
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtHt(MT_HT);
		ligne.setFligArtTtc(MT_TTC);
		ligne.setFligQuantite(QUANTITE);

		BigDecimal expected = BigDecimal.valueOf(120d).setScale(0, BigDecimal.ROUND_HALF_UP);
		assertEquals(expected, ligneService.calculerMontantTotalTTCRemiseIncluse(ligne));
	}

	@Test
	public void testCalculerMontantTotalTTCRemiseIncluseNouvelleCaledonie() {
		FacturePapier facturePapierSansRemise = new MockFacturePapier() {
			public BigDecimal remiseGlobale() {
				return BigDecimal.ZERO;
			}
		};

		final BigDecimal MT_HT = BigDecimal.valueOf(774d).setScale(0, BigDecimal.ROUND_HALF_UP);
		final BigDecimal MT_TTC = BigDecimal.valueOf(813d).setScale(0, BigDecimal.ROUND_HALF_UP);
		final BigDecimal QUANTITE = BigDecimal.valueOf(4d);

		MockFacturePapierLigne ligne = new MockFacturePapierLigne(facturePapierSansRemise);
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.ZERO_DECIMALE;
			}
		});
		ligne.setFligArtHt(MT_HT);
		ligne.setFligArtTtc(MT_TTC);
		ligne.setFligQuantite(QUANTITE);

		BigDecimal expected = BigDecimal.valueOf(3251d).setScale(0, BigDecimal.ROUND_HALF_UP);
		assertEquals(expected, ligneService.calculerMontantTotalTTCRemiseIncluse(ligne));
	}


	@Test
	public void testCalculerMontantTotalTTCRemiseIncluseAvecTVASeulement() {
		FacturePapier facturePapierSansRemise = new MockFacturePapier() {
			public BigDecimal remiseGlobale() {
				return BigDecimal.ZERO;
			}
		};

		final BigDecimal MT_HT = BigDecimal.ZERO.setScale(3, BigDecimal.ROUND_HALF_UP);
		final BigDecimal MT_TTC = BigDecimal.valueOf(1500.43d).setScale(3, BigDecimal.ROUND_HALF_UP);
		final BigDecimal QUANTITE = BigDecimal.valueOf(4d);

		MockFacturePapierLigne ligne = new MockFacturePapierLigne(facturePapierSansRemise);
		ligne.setApplicationConfig(new ApplicationConfig() {
			public int nbDecimalesImposees(EOEditingContext edc, Number exercice) {
				return AbstractFacturationService.DEUX_DECIMALES;
			}
		});
		ligne.setFligArtHt(MT_HT);
		ligne.setFligArtTtc(MT_TTC);
		ligne.setFligQuantite(QUANTITE);

		BigDecimal expected = BigDecimal.valueOf(6001.72d).setScale(3, BigDecimal.ROUND_HALF_UP);
		assertEquals(expected, ligneService.calculerMontantTotalTTCRemiseIncluse(ligne));
	}
}
