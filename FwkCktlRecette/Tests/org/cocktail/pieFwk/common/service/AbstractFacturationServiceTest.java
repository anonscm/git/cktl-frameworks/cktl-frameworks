package org.cocktail.pieFwk.common.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.math.BigDecimal;

import org.junit.Test;

public class AbstractFacturationServiceTest {

    private FacturationService dummyFacturationService = new DummyFacturationService();
    
    // A remonter dans une classe de test parent etendu par les tests GFC.
    protected BigDecimal bigdecimal(double value, int scale) {
        return BigDecimal.valueOf(value).setScale(scale);
    }
    
    @Test
    public void testCalculerMontantTotalHTExactAvecParamsVides() {
        assertEquals(BigDecimal.ZERO, dummyFacturationService.calculerMontantTotalHTExact(null, null));
        assertEquals(BigDecimal.ZERO.setScale(2), dummyFacturationService.calculerMontantTotalHTExact(BigDecimal.valueOf(100.23d), null));
        assertEquals(BigDecimal.ZERO, dummyFacturationService.calculerMontantTotalHTExact(null, BigDecimal.valueOf(12)));
    }
    
    @Test
    public void testCalculerMontantTotalHTExactScale2() {
        BigDecimal expected = bigdecimal(1202.76d, 2);
        BigDecimal montantHT = BigDecimal.valueOf(100.23d);
        BigDecimal tauxTVA = BigDecimal.valueOf(12);
        assertEquals(expected, dummyFacturationService.calculerMontantTotalHTExact(montantHT, tauxTVA));
    }
    
    @Test
    public void testCalculerMontantTotalHTExactScale3() {
        BigDecimal expected = bigdecimal(1202.808d, 3);
        BigDecimal montantHT = BigDecimal.valueOf(100.234d);
        BigDecimal tauxTVA = BigDecimal.valueOf(12);
        assertEquals(expected, dummyFacturationService.calculerMontantTotalHTExact(montantHT, tauxTVA));
    }

    @Test
    public void testCalculerMontantTotalTTCExactParamsNull() {
        assertNull(dummyFacturationService.calculerMontantTotalTTCExact(null, null, null, null));
        assertNull(dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), null, BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
    }
    
    @Test
    public void testCalculerMontantTotalTTCExactQuantiteNull() {
        assertEquals(bigdecimal(0d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), BigDecimal.valueOf(119.6d), null, null));
    }
    
    @Test
    public void testCalculerMontantTotalTTCExact() {
        assertEquals(bigdecimal(119.6d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), BigDecimal.valueOf(119.6d), BigDecimal.ONE, null));
        assertEquals(bigdecimal(119.6d, 1), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(0d), BigDecimal.valueOf(119.6d), BigDecimal.ONE, null));
        assertEquals(bigdecimal(0d, 1), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), BigDecimal.ONE, null));
        
        assertEquals(bigdecimal(119.6d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), BigDecimal.valueOf(200.6d), BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
    }
    
    @Test
    public void testCalculerMontantTotalTTCExactAvecErreurTTC() {
        assertEquals(bigdecimal(119.6d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), BigDecimal.ZERO, BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
        assertEquals(bigdecimal(119.6d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100d), BigDecimal.valueOf(222.6d), BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
    }
    
    @Test
    public void testCalculerMontantTotalTTCExactScale() {
        assertEquals(bigdecimal(120.3176d, 4), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100.6d), BigDecimal.ZERO, BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
        assertEquals(bigdecimal(120.32956d, 5), dummyFacturationService.calculerMontantTotalTTCExact(BigDecimal.valueOf(100.61d), BigDecimal.ZERO, BigDecimal.ONE, BigDecimal.valueOf(19.6d)));
    }

    protected static class DummyFacturationService extends AbstractFacturationService {
    }
}
