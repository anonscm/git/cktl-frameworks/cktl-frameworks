SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.4.5
-- Date de publication : 13/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Desactivation de la possibilite de creer/mettre a jour des clients a partir de PIE (ne respecte pas les regles AGrhum), suite de la suite :
-- retablie la vue v_structure_ulr sinon pb pour selectionner les fournisseurs internes.
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;


CREATE OR REPLACE FORCE VIEW jefy_recette.v_structure_ulr (c_structure,
                                                           pers_id,
                                                           ll_structure,
                                                           lc_structure,
                                                           c_type_structure,
                                                           c_structure_pere,
                                                           c_rne,
                                                           siret,
                                                           siren,
                                                           c_naf,
                                                           org_ordre,
                                                           tem_valide
                                                          )
AS
   SELECT c_structure, s.pers_id, ll_structure, lc_structure,
          c_type_structure, c_structure_pere, c_rne, siret, siren, c_naf,
          org_ordre, tem_valide
     FROM grhum.structure_ulr s;


/
     insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.3.4.5',to_date('13/10/2011','dd/mm/yyyy'),'Desactivation de la creation des clients, suite de la suite.');
commit;









