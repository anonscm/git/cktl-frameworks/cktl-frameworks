SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°2/2
-- Type : DML
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.4.3
-- Date de publication : 13/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Desactivation de la possibilite de creer/mettre a jour des clients a partir de PIE (ne respecte pas les regles AGrhum).
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;

update jefy_recette.parametres set par_value='NON', par_description='Obsolete, laisser à NON' where par_key='AUTORISE_CREATION_CLIENT';
update jefy_recette.parametres set par_value='NON', par_description='Obsolete, laisser à NON' where par_key='AUTORISE_CREATION_RIB';
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.3.4.3',to_date('13/10/2011','dd/mm/yyyy'),'Desactivation de la creation des clients.');
commit;






