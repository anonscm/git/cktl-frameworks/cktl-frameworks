SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.4.3
-- Date de publication : 13/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Desactivation de la possibilite de creer/mettre a jour des clients a partir de PIE (ne respecte pas les regles AGrhum).
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;



CREATE OR REPLACE PROCEDURE JEFY_RECETTE.Ins_Client (
    a_pers_id                IN OUT GRHUM.PERSONNE.PERS_ID%TYPE,
    a_c_structure            IN OUT GRHUM.STRUCTURE_ULR.C_STRUCTURE%TYPE,
    a_no_individu            IN OUT GRHUM.INDIVIDU_ULR.NO_INDIVIDU%TYPE,
    a_fou_ordre                IN OUT GRHUM.FOURNIS_ULR.fou_ordre%TYPE,
    a_type_client            VARCHAR2,
    a_c_civilite            VARCHAR2,
    a_nom                    VARCHAR2,
    a_prenom                VARCHAR2,
    a_adresse1                VARCHAR2,
    a_adresse2                VARCHAR2,
    a_bp                    VARCHAR2,
    a_cp                    VARCHAR2,
    a_cp_etranger            VARCHAR2,
    a_ville                    VARCHAR2,
    a_c_pays                VARCHAR2,
    a_tel                    VARCHAR2,
    a_fax                    VARCHAR2,
    a_login                    VARCHAR2,
    a_password                VARCHAR2,
    a_email                    VARCHAR2,
    a_rib_pays                VARCHAR2,
    a_rib_banque            VARCHAR2,
    a_rib_guichet            VARCHAR2,
    a_rib_compte            VARCHAR2,
    a_rib_cle                VARCHAR2,
    a_adr_ordre                GRHUM.ADRESSE.ADR_ORDRE%TYPE,
    a_utl_ordre             jefy_admin.utilisateur.UTL_ORDRE%type
    )
IS

BEGIN
     RAISE_APPLICATION_ERROR(-20001, 'LA CREATION DES CLIENTS DOIT DESORMAIS ETRE EFFECTUEE A PARTIR D''AGRHUM. VEUILLEZ EGALEMENT COCHER "FOURNISSEUR" AVANT D''EFFECTUER UNE RECHERCHE.');
END;
/

CREATE OR REPLACE PROCEDURE JEFY_RECETTE.Ins_Client_Rib (
	a_fou_ordre				GRHUM.FOURNIS_ULR.fou_ordre%TYPE,
	a_rib_pays				VARCHAR2,
	a_rib_banque			VARCHAR2,
	a_rib_guichet			VARCHAR2,
	a_rib_compte			VARCHAR2,
	a_rib_cle				VARCHAR2
	)
IS
 
BEGIN
	 RAISE_APPLICATION_ERROR(-20001, 'LA CREATION DES CLIENTS DOIT DESORMAIS ETRE EFFECTUEE A PARTIR D''AGRHUM. VEUILLEZ EGALEMENT COCHER "FOURNISSEUR" AVANT D''EFFECTUER UNE RECHERCHE.');
END;
/

CREATE OR REPLACE FORCE VIEW jefy_recette.v_structure_ulr (c_structure,
                                                           pers_id,
                                                           ll_structure,
                                                           lc_structure,
                                                           c_type_structure,
                                                           c_structure_pere,
                                                           c_rne,
                                                           siret,
                                                           siren,
                                                           c_naf,
                                                           org_ordre,
                                                           tem_valide
                                                          )
AS
   SELECT c_structure, s.pers_id, ll_structure, lc_structure, c_type_structure,
          c_structure_pere, c_rne, siret, siren, c_naf, org_ordre, tem_valide
     FROM grhum.structure_ulr s, grhum.fournis_ulr f where s.pers_id=f.pers_id and f.FOU_VALIDE='O' and f.FOU_TYPE in ('T', 'C');
/     








