SET DEFINE OFF;
--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
-- 
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.3.4.4
-- Date de publication : 13/10/2011
-- Licence : CeCILL version 2
--
--


----------------------------------------------
-- Desactivation de la possibilite de creer/mettre a jour des clients a partir de PIE (ne respecte pas les regles AGrhum), suite.
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;


CREATE OR REPLACE FORCE VIEW jefy_recette.v_fournis_ulr (fou_ordre,
                                                         pers_id,
                                                         adr_ordre,
                                                         fou_code,
                                                         fou_date,
                                                         fou_marche,
                                                         fou_valide,
                                                         agt_ordre,
                                                         fou_type,
                                                         d_creation,
                                                         d_modification,
                                                         cpt_ordre,
                                                         fou_etranger,
                                                         adr_adresse1,
                                                         adr_adresse2,
                                                         adr_cp,
                                                         adr_ville,
                                                         adr_nom,
                                                         adr_prenom,
                                                         adr_civilite,
                                                         siret
                                                        )
AS
   SELECT f.fou_ordre, f.pers_id, f.adr_ordre, f.fou_code, f.fou_date,
          f.fou_marche, f.fou_valide, f.agt_ordre, f.fou_type, f.d_creation,
          f.d_modification, f.cpt_ordre, f.fou_etranger, f.adr_adresse1,
          f.adr_adresse2, f.adr_cp, f.adr_ville, f.adr_nom, f.adr_prenom,
          f.adr_civilite, f.siret
         FROM grhum.v_fournis_grhum f where fou_valide<>'A' and adr_ordre is not null;
/
     insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.3.4.4',to_date('13/10/2011','dd/mm/yyyy'),'Desactivation de la creation des clients, suite.');
commit;









