SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.4.0.0
-- Date de publication :
-- Licence : CeCILL version 2
--
--

--------------------------------------------------------
--  DDL for Package API
--------------------------------------------------------

CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API" IS

--
--
-- procedures publiques a executer par les applications clientes.
--
--

-- Crée une recette indépendante (recette + adresse recette + facture )
-- Delegue la creation recette + facture a ins_facture_recette
-- puis complete en creant l'adresse sur la recette papier.
PROCEDURE ins_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Crée une recette indépendante (recette + facture)
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Update une recette simple indépendante
-- en ajoutant la gestion de l'adresse de la recette papier.
PROCEDURE upd_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Update une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Delete une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

-- Insère une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Update une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Delete une facture simple
PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE);

-- insère une recette papier simple visible
PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero IN OUT	  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

-- Insère une recette
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
--
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Permet de recetter en automatique une facture (recette totale)
-- en créant aussi la recette papier non visible
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE upd_recette_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE upd_facture_eche_id (
      a_fac_id              FACTURE.fac_id%TYPE,
	  a_eche_id				FACTURE.eche_id%TYPE);

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           RECETTE_PAPIER.utl_ordre%TYPE);

-- Supprime une recette simple
PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           RECETTE.utl_ordre%TYPE);

-- Reduit une recette existante
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

PROCEDURE upd_reduction_ctrl_planco (
      a_rpco_id              RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

END;
/

--------------------------------------------------------
--  Fichier créé - mercredi-novembre-30-2011
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body API
--------------------------------------------------------

CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."API"
IS

-- Crée une recette indépendante (recette + adresse recette + facture )
-- Delegue la creation recette + facture a ins_facture_recette
-- puis complete en creant l'adresse sur la recette papier.
PROCEDURE ins_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE)
IS
	BEGIN
		ins_facture_recette(
			a_fac_id, a_rec_id, a_rpp_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre, a_pers_id, a_fac_lib,
			a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie,
			a_fac_ttc_saisie, a_tyap_id, a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention,
			a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);
		IF a_rpp_adc_adr_ordre IS NOT NULL AND a_rpp_adc_pers_id IS NOT NULL THEN
			Recetter.ins_recette_papier_adr_client(a_rpp_id, a_rpp_adc_adr_ordre, a_rpp_adc_pers_id);
		END IF;
	END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit de facturer et recetter sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer_Recetter.ins_facture_recette(a_fac_id, a_rec_id, a_rpp_id, a_exe_ordre, a_fac_numero, a_rec_numero, NULL, a_fou_ordre,
			a_pers_id, a_fac_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie, a_fac_ttc_saisie, a_tyap_id,
			a_utl_ordre,a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva,
			a_chaine_planco_ctp);

		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		-- on verifie la coherence des montants entre les differents ctrl_.
		Verifier.verifier_facture_coherence(a_fac_id);
		Verifier.verifier_recette_coherence(a_rec_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
	    Apres_Recette.ins_recette(a_rec_id);
   END;

-- Maj Facture Recette avec Gestion de l'adresse de la recette papier.
PROCEDURE upd_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE)
IS
	my_rpp_id				RECETTE.rpp_id%TYPE;
	my_nb					INTEGER;
	BEGIN
		upd_facture_recette(
			a_fac_id, a_rec_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre, a_pers_id, a_fac_lib,
			a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie,
			a_fac_ttc_saisie, a_tyap_id, a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention,
			a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);
		IF a_rpp_adc_adr_ordre IS NOT NULL AND a_rpp_adc_pers_id IS NOT NULL THEN
			SELECT rpp_id INTO my_rpp_id FROM RECETTE WHERE rec_id = a_rec_id;
			Recetter.ins_recette_papier_adr_client(my_rpp_id, a_rpp_adc_adr_ordre, a_rpp_adc_pers_id);
		END IF;
	END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
        Verifier.verifier_utilisation_recette(a_rec_id);
		-- on verifie que l'utilisateur a le droit de facturer et recetter sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer_Recetter.upd_facture_recette(a_fac_id, a_rec_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre,
			a_pers_id, a_fac_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie, a_fac_ttc_saisie, a_tyap_id,
			a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva,
			a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents ctrl_.
		Verifier.verifier_facture_coherence(a_fac_id);
		Verifier.verifier_recette_coherence(a_rec_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
   	 my_fac_id				FACTURE.fac_id%TYPE;
   BEGIN
        Verifier.verifier_utilisation_recette(a_rec_id);
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id = a_rec_id;
		Facturer_Recetter.del_facture_recette(a_rec_id, a_utl_ordre);
		Apres_Recette.del_recette(a_rec_id);
		Apres_Facture.del_facture(my_fac_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit de facturer sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer.ins_facture(a_fac_id,a_exe_ordre,a_fac_numero,a_fou_ordre,a_pers_id,a_fac_lib,a_mor_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_fac_ht_saisie,a_fac_ttc_saisie,a_tyap_id,a_utl_ordre,a_chaine_action,a_chaine_analytique,a_chaine_convention,a_chaine_planco);

		-- on verifie la coherence des montants entre les differents facture_.
		Verifier.verifier_facture_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   	 my_exe_ordre			FACTURE.exe_ordre%TYPE;
	 my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (upd_facture)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM FACTURE WHERE fac_id=a_fac_id;

		-- on verifie que l'utilisateur a le droit de facturer sur cet exercice.
		Verifier.verifier_droit_facture(my_exe_ordre, a_utl_ordre);

		-- mise a jour.
		Facturer.upd_facture(a_fac_id,a_fac_lib,a_mor_ordre, a_tyap_id,a_fac_ht_saisie,a_fac_ttc_saisie,a_utl_ordre,
		    a_chaine_action,a_chaine_analytique,a_chaine_convention,a_chaine_planco);

		-- on verifie la coherence des montants entre les differents facture_.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.upd_facture(a_fac_id);
   END;

PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE)
   IS
   BEGIN
        Verifier.verifier_utilisation_facture(a_fac_id);
		Facturer.del_facture(a_fac_id, a_utl_ordre);
		Apres_Facture.del_facture(a_fac_id);
   END;

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero IN OUT	  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
   BEGIN
        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette papier.
		Recetter.ins_recette_papier(a_rpp_id, a_exe_ordre, a_rpp_numero, a_rpp_ht_saisie, a_rpp_ttc_saisie,
		a_pers_id, a_fou_ordre, a_rib_ordre, a_mor_ordre, a_rpp_date_recette, a_rpp_date_reception,
		a_rpp_date_service_fait, a_rpp_nb_piece, a_utl_ordre, 'O');

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		Apres_Recette.ins_recette_papier(a_rpp_id);
   END;

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
	   my_exe_ordre			 RECETTE_PAPIER.exe_ordre%TYPE;
	   my_nb INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette papier n''existe pas (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette papier.
		Recetter.upd_recette_papier(a_rpp_id, a_rpp_numero, a_rpp_ht_saisie, a_rpp_ttc_saisie,
		a_pers_id, a_fou_ordre, a_rib_ordre, a_mor_ordre, a_rpp_date_recette, a_rpp_date_reception,
		a_rpp_date_service_fait, a_rpp_nb_piece, a_utl_ordre);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		Apres_Recette.upd_recette_papier(a_rpp_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette.
		Recetter.ins_recette(a_rec_id, a_exe_ordre, a_rpp_id, a_rec_numero, a_fac_id, a_rec_lib, a_rib_ordre, a_mor_ordre,
			a_rec_ht_saisie, a_rec_ttc_saisie, a_tap_id, a_utl_ordre, a_chaine_action,
			a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);

   END;

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE
   ) IS
   	 my_exe_ordre			FACTURE.exe_ordre%TYPE;
	 my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb = 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (ins_recette_from_facture)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM FACTURE WHERE fac_id = a_fac_id;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);

		-- creation
		Recetter.ins_recette_from_facture(a_rec_id, a_rec_numero, a_fac_id, a_utl_ordre, a_rib_ordre, a_mor_ordre,
		   a_tap_id, a_pco_num_tva, a_pco_num_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE upd_recette_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE
   ) IS
	  my_rec_id_reduction	RECETTE.rec_id_reduction%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rpco_id = a_rpco_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette planco demandee n''existe pas (rpco_id:'||a_rpco_id||') (upd_recette_ctrl_planco)');
		END IF;
   		SELECT r.rec_id_reduction INTO my_rec_id_reduction FROM RECETTE r, RECETTE_CTRL_PLANCO rpco
		  WHERE rpco.rpco_id = a_rpco_id AND rpco.rec_id = r.rec_id;
		IF my_rec_id_reduction IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee est une reduction, utiliser upd_reduction_ctrl_planco (rpco_id:'||a_rpco_id||') (upd_recette_ctrl_planco)');
		END IF;
		UPDATE RECETTE_CTRL_PLANCO SET tit_id = a_tit_id WHERE rpco_id = a_rpco_id;
   END;

PROCEDURE upd_facture_eche_id (
      a_fac_id              FACTURE.fac_id%TYPE,
	  a_eche_id				FACTURE.eche_id%TYPE
   ) IS
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (upd_facture_eche_id)');
		END IF;
		UPDATE FACTURE SET eche_id = a_eche_id WHERE fac_id = a_fac_id;
   END;

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
       my_nb         			INTEGER;
	   my_exe_ordre	 		    RECETTE_PAPIER.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La recette papier n''existe pas ou est deja annule (rpp_id:'||a_rpp_id||')');
	    END IF;
   		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;

		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		Verifier.verifier_util_recette_papier(a_rpp_id);

		Recetter.del_recette_papier(a_rpp_id, a_utl_ordre);

		Apres_Recette.del_recette_papier(a_rpp_id);
   END;


PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           RECETTE.utl_ordre%TYPE
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id||') (del_recette)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id=a_rec_id;

        Verifier.verifier_utilisation_recette(a_rec_id);

		Recetter.del_recette(a_rec_id, a_utl_ordre);

		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);
		-- on verifie la coherence entre la facture et la(les) recette(s) s'il en reste
		Verifier.verifier_fac_rec_coherence(my_fac_id);

		Apres_Recette.del_recette(a_rec_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id_reduction;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id_reduction||') (ins_reduction)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id = a_rec_id_reduction;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette.
		Reduire.ins_reduction(a_rec_id, a_exe_ordre, a_rec_numero, a_rec_lib, a_rpp_nb_piece,
			a_rec_ht_saisie, a_rec_ttc_saisie, a_utl_ordre, a_rec_id_reduction, a_chaine_action,
			a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(my_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE upd_reduction_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE
   ) IS
	  my_rec_id_reduction	RECETTE.rec_id_reduction%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rpco_id = a_rpco_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette planco demandee n''existe pas (rpco_id:'||a_rpco_id||') (upd_reduction_ctrl_planco)');
		END IF;
   		SELECT r.rec_id_reduction INTO my_rec_id_reduction FROM RECETTE r, RECETTE_CTRL_PLANCO rpco
		  WHERE rpco.rpco_id = a_rpco_id AND rpco.rec_id = r.rec_id;
		IF my_rec_id_reduction IS NULL THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''est pas une reduction, utiliser upd_recette (rpco_id:'||a_rpco_id||') (upd_reduction_ctrl_planco)');
		END IF;
		UPDATE RECETTE_CTRL_PLANCO SET tit_id = a_tit_id WHERE rpco_id = a_rpco_id;
   END;

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id||') (del_reduction)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id=a_rec_id;

        Verifier.verifier_utilisation_recette(a_rec_id);

		Reduire.del_reduction(a_rec_id, a_utl_ordre);

		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);
		-- on verifie la coherence entre la facture et la(les) recette(s) s'il en reste
		Verifier.verifier_fac_rec_coherence(my_fac_id);

		Apres_Recette.del_recette(a_rec_id);
   END;

END;

/

commit;



