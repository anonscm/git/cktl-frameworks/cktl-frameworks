SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/2
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.4.0.0
-- Date de publication :
-- Licence : CeCILL version 2
--
--


----------------------------------------------
--
----------------------------------------------

whenever sqlerror exit sql.sqlcode ;


CREATE TABLE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT
(
  FAPADC_ID NUMBER NOT NULL,
  FAP_ID NUMBER NOT NULL,
  ADR_ORDRE NUMBER NOT NULL,
  Date_CREATION DATE NOT NULL,
  Date_FIN DATE NULL,
  PERS_ID_CREATION NUMBER NOT NULL
);

COMMENT ON TABLE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT IS 'Stocke l''adresse client affectee a la facture';
COMMENT ON COLUMN JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT.FAP_ID IS 'Cle etrangere vers la table jefy_recette.facture_papier';
COMMENT ON COLUMN JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT.ADR_ORDRE IS 'Cle etrangere vers la table grhum.adresse';
ALTER TABLE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT ADD CONSTRAINT PK_FAP_ADR_CLIENT PRIMARY KEY (FAPADC_ID);
ALTER TABLE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT ADD CONSTRAINT FK_FAP_ADR_CLIENT_FAP_ID FOREIGN KEY (FAP_ID) REFERENCES jefy_recette.facture_papier (fap_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT ADD CONSTRAINT FK_FAP_ADR_CLIENT_ADR_ORDRE FOREIGN KEY (ADR_ORDRE) REFERENCES GRHUM.ADRESSE (ADR_ORDRE) DEFERRABLE INITIALLY DEFERRED;


CREATE TABLE JEFY_RECETTE.PRESTATION_ADR_CLIENT
(
  PADC_ID NUMBER NOT NULL,
  PREST_ID NUMBER NOT NULL,
  ADR_ORDRE NUMBER NOT NULL,
  Date_CREATION DATE NOT NULL,
  Date_FIN DATE NULL,
  PERS_ID_CREATION NUMBER NOT NULL
);


COMMENT ON TABLE  JEFY_RECETTE.PRESTATION_ADR_CLIENT is 'Stocke l''adresse client affectee a la prestation';
COMMENT ON COLUMN JEFY_RECETTE.PRESTATION_ADR_CLIENT.PREST_ID IS 'cle etrangere vers la table jefy_recette.prestation';
COMMENT ON COLUMN JEFY_RECETTE.PRESTATION_ADR_CLIENT.ADR_ORDRE IS 'Cle etrangere vers la table grhum.adresse';
ALTER TABLE JEFY_RECETTE.PRESTATION_ADR_CLIENT ADD CONSTRAINT PK_PREST_ADR_CLIENT PRIMARY KEY (PADC_ID);
ALTER TABLE JEFY_RECETTE.PRESTATION_ADR_CLIENT ADD CONSTRAINT FK_PREST_ADR_CLIENT_PREST_ID FOREIGN KEY (PREST_ID) REFERENCES jefy_recette.prestation (PREST_ID) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE JEFY_RECETTE.PRESTATION_ADR_CLIENT ADD CONSTRAINT FK_PREST_ADR_CIENTL_ADR FOREIGN KEY (ADR_ORDRE) REFERENCES GRHUM.ADRESSE (ADR_ORDRE) DEFERRABLE INITIALLY DEFERRED;


CREATE TABLE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT
(
  RPPADC_ID        NUMBER NOT NULL,
  RPP_ID           NUMBER NOT NULL,
  ADR_ORDRE        NUMBER NOT NULL,
  Date_CREATION    DATE   NOT NULL,
  Date_FIN         DATE   NULL,
  PERS_ID_CREATION NUMBER NOT NULL
);

COMMENT ON TABLE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT IS 'Stocke l''adresse client affectee a la recette papier';
COMMENT ON COLUMN JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT.RPP_ID    IS 'Cle etrangere vers la table jefy_recette.recette_papier';
COMMENT ON COLUMN JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT.ADR_ORDRE IS 'Cle etrangere vers la table grhum.adresse';
ALTER TABLE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT ADD CONSTRAINT PK_RPP_ADR_CLIENT PRIMARY KEY (RPPADC_ID);
ALTER TABLE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT ADD CONSTRAINT FK_RPP_ADR_CLIENT_RPP_ID FOREIGN KEY (RPP_ID) REFERENCES jefy_recette.recette_papier (rpp_id) DEFERRABLE INITIALLY DEFERRED;
ALTER TABLE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT ADD CONSTRAINT FK_RPP_ADR_CLIENT_ADR_ORDRE FOREIGN KEY (ADR_ORDRE) REFERENCES GRHUM.ADRESSE (ADR_ORDRE) DEFERRABLE INITIALLY DEFERRED;

grant select on JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT to maracuja with grant option;

CREATE SEQUENCE JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE SEQUENCE JEFY_RECETTE.PRESTATION_ADR_CLIENT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

CREATE SEQUENCE JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 1
  NOCYCLE
  NOCACHE
  NOORDER;

insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.4.0.0',to_date('25/11/2011','dd/mm/yyyy'),'Ajouts de table pour la gestion des adresses de facturation.');
commit;









