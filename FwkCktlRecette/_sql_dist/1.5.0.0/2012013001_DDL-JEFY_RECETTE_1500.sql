SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.5.0.0
-- Date de publication : 30/01/2012
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode ;

----------------------------------------------
-- Mise a jour des VUES.
----------------------------------------------
CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_CTP_REAICTP" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER;


CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_TVA" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER WHERE pco_num LIKE '445%' ;


CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_TVA_REAITVA" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER ;


create or replace force view jefy_recette.v_planco_credit_rec (pcc_ordre, tcd_ordre, pco_num, pcc_etat, pco_validite)
as
   select v.pcc_ordre,
          v.tcd_ordre,
          v.pco_num,
          v.pcc_etat,
          p.pco_validite
   from   maracuja.v_planco_credit_rec v, jefy_admin.type_credit tc, maracuja.plan_comptable_exer p
   where  v.pco_num = p.pco_num and v.tcd_ordre = tc.tcd_ordre and p.exe_ordre = tc.exe_ordre and v.pco_num not like '18%' and (v.pco_num like '1%' or v.pco_num like '7%');

create or replace force view jefy_recette.v_planco_credit_dep (pcc_ordre, tcd_ordre, pco_num, pcc_etat, pco_validite)
as
   select v.pcc_ordre,
          v.tcd_ordre,
          v.pco_num,
          v.pcc_etat,
          p.pco_validite
   from   maracuja.v_planco_credit_dep v, jefy_admin.type_credit tc, maracuja.plan_comptable_exer p
   where  v.pco_num = p.pco_num and v.tcd_ordre = tc.tcd_ordre and p.exe_ordre = tc.exe_ordre and p.pco_num not like '18%';

----------------------------------------------
-- Mise a jour des PACKAGES.
----------------------------------------------
--------------------------------------------------------
--  DDL for Package API_PRESTATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API_PRESTATION"
IS

-- ETAPE NO 1
-- valide la prestation cote client (date de validation client)
-- si c'est de la prestation interne, va generer en plus l'engagement
PROCEDURE valide_prestation_client (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 1
-- devalide la prestation cote client si possible
-- si c'est de la prestation interne, supprime en meme temps l'engagement si c'etait une PI de Pie
-- si c'etait un PI issue d'une commande Carambole, on ne supprimer pas l'engagement, on change juste son tyap_id
-- pour le remettre ! DEPENSE
PROCEDURE devalide_prestation_client (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 2
-- valide la prestation cote prestataire (date de validation prestataire)
-- pas d'autres incidences
PROCEDURE valide_prestation_prest (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 2
-- valide la prestation cote prestataire (date de validation prestataire)
-- pas d'autres incidences
PROCEDURE devalide_prestation_prest (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 3
-- clos la prestation (date de cloture)
-- pas d'autres incidences
PROCEDURE cloture_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 3
-- "declos" la prestation (date de cloture)
-- pas d'autres incidences
PROCEDURE decloture_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 4
-- genere la (les) facture(s) papier(s) a partir d'une prestation close
PROCEDURE genere_facture_papier (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 4
-- suppression d'une facture papier non recettee
-- si issue d'une prestation, met a jour les quantites restantes a facturer
PROCEDURE del_facture_papier (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 5
-- validation de la facture papier cote client (date validation client)
-- aucune autre incidence
PROCEDURE valide_facture_papier_client (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- annulation etape no 5
-- devalidation de la facture cote client si non validee prestataire (date validation client)
-- aucune autre incidence
PROCEDURE devalide_facture_papier_client (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- ETAPE NO 6
-- valide la facture papier cote prestataire
-- genere la facture (table facture), lie a l'engagement si c'est de l'interne,
-- puis genere la recette (table recette) et liquide l'engagement si c'est de l'interne (puis lie les 2)
-- a_fac_id, a_utl_ordre et a_mor_ordre obligatoires
-- autres facultatifs (determination auto si null)
PROCEDURE valide_facture_papier_prest (
    a_fap_id           jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre        jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE,
    a_tap_id_depense   jefy_depense.depense_budget.tap_id%TYPE,
    a_rib_ordre        jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
    a_mor_ordre        jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
    a_tap_id_recette   jefy_recette.RECETTE.tap_id%TYPE);

-- annulation etape no 6
-- devalidation facture cote prestataire si possible (recette non titree)
-- supprime la facture/recette correspondante
-- si c'est de l'interne, supprime aussi la liquidation si possible
PROCEDURE devalide_facture_papier_prest (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- Archivage d'une prestation (= suppression, mais on ne supprime jamais réellement une prestation)
PROCEDURE archive_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- met a jour une reference de facture papier existante
-- a utiliser quand elle a ete creee directement (non issue d'une prestation, donc non generee par genere_facture_papier)
PROCEDURE upd_fap_ref (
    a_fap_id          jefy_recette.FACTURE_PAPIER.fap_id%TYPE);

-- Generation d'une prestation interne ! partir d'une commande
-- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- toutes les autres infos budgetaires recette facultatives
PROCEDURE prestation_from_commande (
    a_comm_id             jefy_depense.commande.comm_id%TYPE,
    a_utl_ordre             jefy_recette.prestation.utl_ordre%TYPE,
    a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
    a_org_id_recette     jefy_recette.prestation.org_id%TYPE,
    a_tap_id_recette     jefy_recette.prestation.tap_id%TYPE,
    a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
    a_lolf_id_recette     jefy_recette.prestation.lolf_id%TYPE,
    a_pco_num_recette     jefy_recette.prestation.pco_num%TYPE,
    a_can_id_recette     jefy_recette.prestation.can_id%TYPE,
    a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
    a_prest_id    IN OUT     jefy_recette.prestation.prest_id%TYPE);

-- Generation d'une facture papier interne ! partir d'une commande
-- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- toutes les autres infos budgetaires recette facultatives
PROCEDURE facture_papier_from_commande (
    a_comm_id             jefy_depense.commande.comm_id%TYPE,
    a_utl_ordre             jefy_recette.prestation.utl_ordre%TYPE,
    a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
    a_org_id_recette     jefy_recette.prestation.org_id%TYPE,
    a_tap_id_recette     jefy_recette.prestation.tap_id%TYPE,
    a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
    a_lolf_id_recette     jefy_recette.prestation.lolf_id%TYPE,
    a_pco_num_recette     jefy_recette.prestation.pco_num%TYPE,
    a_can_id_recette     jefy_recette.prestation.can_id%TYPE,
    a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
    a_prest_id    IN OUT     jefy_recette.prestation.prest_id%TYPE,
    a_fap_id    IN OUT     jefy_recette.facture_papier.fap_id%TYPE);

 -- Duplication d'une facture papier en specifiant l'adresse du fournisseur.
 -- a_fap_id, a_utl_ordre, a_fou_ordre_client, a_four_adr_ordre et a_prest_id_creation obligatoires
 -- renvoie en OUT le fap_id et le fap_numero de la facture créée
PROCEDURE duplicate_facture_papier_adr (
    a_fap_id                  jefy_recette.facture_papier.fap_id%TYPE,
    a_utl_ordre               jefy_recette.facture_papier.utl_ordre%TYPE,
    a_fou_ordre_client        jefy_recette.facture_papier.fou_ordre%TYPE,
    a_fap_id_out      IN OUT  jefy_recette.facture_papier.fap_id%TYPE,
    a_fap_numero_out  IN OUT  jefy_recette.facture_papier.fap_numero%TYPE,
    a_fou_adr_ordre           jefy_recette.facture_papier_adr_client.adr_ordre%TYPE,
    a_pers_id_creation        jefy_recette.facture_papier_adr_client.pers_id_creation%TYPE);

-- Duplication d'une facture papier
-- a_fap_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- renvoie en OUT le fap_id et le fap_numero de la facture créée
PROCEDURE duplicate_facture_papier (
    a_fap_id                   jefy_recette.facture_papier.fap_id%TYPE,
    a_utl_ordre                   jefy_recette.facture_papier.utl_ordre%TYPE,
    a_fou_ordre_client         jefy_recette.facture_papier.fou_ordre%TYPE,
    a_fap_id_out      IN OUT    jefy_recette.facture_papier.fap_id%TYPE,
    a_fap_numero_out IN OUT    jefy_recette.facture_papier.fap_numero%TYPE);

-- Solde une prestation
PROCEDURE solde_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- Duplication d'une prestation
-- a_prest_id, a_utl_ordre et a_exe_ordre obligatoires
-- renvoie en OUT le prest_id et le prest_numero de la facture créée
PROCEDURE duplicate_prestation (
    a_prest_id                   jefy_recette.prestation.prest_id%TYPE,
    a_utl_ordre                   jefy_recette.prestation.utl_ordre%TYPE,
    a_exe_ordre             jefy_recette.prestation.exe_ordre%TYPE,
    a_prest_id_out      IN OUT    jefy_recette.prestation.prest_id%TYPE,
    a_prest_numero_out IN OUT    jefy_recette.prestation.prest_numero%TYPE);

-- la suite est "private"...

FUNCTION get_fap_ref (
      a_exe_ordre            jefy_recette.FACTURE_PAPIER.exe_ordre%TYPE,
      a_org_id                jefy_recette.FACTURE_PAPIER.org_id%TYPE,
      a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
      a_old_fap_ref            jefy_recette.FACTURE_PAPIER.fap_ref%TYPE)
   RETURN VARCHAR2;

PROCEDURE ins_engage (
      a_eng_id IN OUT        jefy_depense.engage_budget.eng_id%TYPE,
      a_exe_ordre            jefy_depense.engage_budget.exe_ordre%TYPE,
      a_eng_numero IN OUT    jefy_depense.engage_budget.eng_numero%TYPE,
      a_fou_ordre            jefy_depense.engage_budget.fou_ordre%TYPE,
      a_org_id                jefy_depense.engage_budget.org_id%TYPE,
      a_tcd_ordre            jefy_depense.engage_budget.tcd_ordre%TYPE,
      a_tap_id                jefy_depense.engage_budget.tap_id%TYPE,
      a_eng_libelle            jefy_depense.engage_budget.eng_libelle%TYPE,
      a_eng_ht_saisie        jefy_depense.engage_budget.eng_ht_saisie%TYPE,
      a_eng_ttc_saisie        jefy_depense.engage_budget.eng_ttc_saisie%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE,
      a_action                VARCHAR2,
      a_analytique            VARCHAR2,
      a_convention            VARCHAR2,
      a_planco                VARCHAR2);

PROCEDURE del_engage (
      a_eng_id                  jefy_depense.engage_budget.eng_id%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE);

PROCEDURE ins_facture (
    a_fap_id          jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

PROCEDURE del_facture (
    a_fac_id          jefy_recette.FACTURE.fac_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

-- recette totalement une facture
-- et s'il s'agit d'une prestation interne, liquide l'engagement correspondant
-- a_fac_id et a_utl_ordre obligatoires
-- autres facultatifs (détermination auto si possible, sinon erreur)
PROCEDURE ins_recette (
    a_fac_id          jefy_recette.FACTURE.fac_id%TYPE,
    a_fap_numero       jefy_recette.FACTURE_PAPIER.fap_numero%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE,
    a_tap_id_depense  jefy_depense.depense_budget.tap_id%TYPE,
    a_rib_ordre          jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
    a_mor_ordre          jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
    a_tap_id_recette  jefy_recette.RECETTE.tap_id%TYPE,
    a_pco_num_tva      jefy_recette.RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
    a_pco_num_ctp      jefy_recette.RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE del_recette (
    a_rec_id          jefy_recette.RECETTE.rec_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

-- a_rib_ordre peut "tre null, en fonction du mod_ordre
-- a_tap_id peut etre null (reprend le tap_id de l'engagement dans ce cas)
-- a_dep_ht_saisie et a_dep_ttc_saisie peuvent etre null (liquide tout l'engagement dans ce cas)
PROCEDURE ins_depense_from_engage (
      a_dep_id IN OUT        jefy_depense.depense_budget.dep_id%TYPE,
      a_eng_id                jefy_depense.engage_budget.eng_id%TYPE,
      a_dpp_numero            jefy_depense.depense_papier.dpp_numero_facture%TYPE,
      a_mod_ordre            jefy_depense.depense_papier.mod_ordre%TYPE,
      a_rib_ordre            jefy_depense.depense_papier.rib_ordre%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE,
      a_tap_id                jefy_depense.depense_budget.tap_id%TYPE,
      a_dep_ht_saisie        jefy_depense.depense_budget.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        jefy_depense.depense_budget.dep_ttc_saisie%TYPE);

FUNCTION chaine_action (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_analytique (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_convention (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_hors_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;


procedure ins_facture_papier_adr_client (
    a_fap_id            facture_papier_adr_client.fap_id%type,
    a_adr_ordre         facture_papier_adr_client.adr_ordre%type,
    a_pers_id_creation  facture_papier_adr_client.pers_id_creation%type);

procedure ins_prestation_adr_client (
    a_prest_id        prestation_adr_client.prest_id%type,
    a_adr_ordre     prestation_adr_client.adr_ordre%type,
    a_pers_id_creation  prestation_adr_client.pers_id_creation%type);

--
procedure controle_prestation_bascule ( a_prest_id jefy_recette.PRESTATION.prest_id%TYPE);
procedure controle_facture_bascule (a_fap_id       jefy_recette.FACTURE_papier.fap_id%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body API_PRESTATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."API_PRESTATION"
 IS

 -- ETAPE NO 1
 -- valide la prestation cote client (date de validation client)
 -- si c'est de la prestation interne, va generer en plus l'engagement
 PROCEDURE valide_prestation_client (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_pbc jefy_recette.PRESTATION_BUDGET_CLIENT%ROWTYPE;
         my_tyap_id jefy_recette.TYPE_PUBLIC.tyap_id%TYPE;
         my_eng_id jefy_recette.PRESTATION_BUDGET_CLIENT.eng_id%TYPE;
         my_eng_numero jefy_depense.engage_budget.eng_numero%TYPE;
         my_sum_ht jefy_recette.PRESTATION_LIGNE.prlig_total_ht%TYPE;
         my_sum_ttc jefy_recette.PRESTATION_LIGNE.prlig_total_ttc%TYPE;
     my_nb INTEGER;
   BEGIN
  api_prestation.controle_prestation_bascule ( a_prest_id);

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider n''existe pas (prest_id='||a_prest_id||') (api_prestation.valide_prestation_client)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_valide_client IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider est deja validee par le client (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- validation client
           UPDATE jefy_recette.PRESTATION SET prest_date_valide_client = SYSDATE WHERE prest_id = a_prest_id;

           -- determination du type de prestation concernée (interne ou externe)
           IF my_prest.typu_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation n''a pas de type client, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La type de client de la prestation n''existe pas, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT tyap_id INTO my_tyap_id FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;

           -- si on est en interne, on genere l'engagement en plus
           IF my_tyap_id = Type_Application.get_type_prestation_interne
           THEN
                 SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;
                 IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque les informations budgétaires client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;
                 SELECT * INTO my_pbc FROM jefy_recette.PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;
                 IF my_pbc.eng_id IS NOT NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation interne semble déjà engagée (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||', eng_id='||my_pbc.eng_id||')');
                 END IF;
                 IF my_pbc.org_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque la ligne budgetaire client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;
                 IF my_pbc.tap_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque le taux de prorata client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;
                 IF my_pbc.tcd_ordre IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque le type de credit client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;
                 IF my_pbc.lolf_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque la destination (action lolf) client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;
                 IF my_pbc.pco_num IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Il manque l''imputation comptable depense client (pour valider la prestation interne et generer l''engagement) (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                 END IF;

                 -- calcul du total de la prestation
                 SELECT NVL(SUM(prlig_total_ht),0), NVL(SUM(prlig_total_ttc),0)
                         INTO my_sum_ht, my_sum_ttc
                         FROM PRESTATION_LIGNE WHERE prest_id = a_prest_id;

                 -- engage
                 ins_engage(my_eng_id, my_prest.exe_ordre, my_eng_numero, my_prest.fou_ordre_prest,
                     my_pbc.org_id, my_pbc.tcd_ordre, my_pbc.tap_id, my_prest.prest_libelle,
                         my_sum_ht, my_sum_ttc, a_utl_ordre, my_pbc.lolf_id, my_pbc.can_id, my_pbc.con_ordre, my_pbc.pco_num);

                 -- enregistrement de l'engagement genere
                 UPDATE PRESTATION_BUDGET_CLIENT SET eng_id = my_eng_id WHERE prest_id = a_prest_id;
           END IF;

   END;

 -- annulation etape no 1
 -- devalide la prestation cote client si possible
 -- si c'est de la prestation interne, supprime en meme temps l'engagement si c'etait une PI de Pie
 -- si c'etait un PI issue d'une commande Carambole, on ne supprimer pas l'engagement, on change juste son tyap_id
 -- pour le remettre ! DEPENSE
 PROCEDURE devalide_prestation_client (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
         my_tyap_id jefy_recette.TYPE_PUBLIC.tyap_id%TYPE;
         my_eng_id jefy_recette.PRESTATION_BUDGET_CLIENT.eng_id%TYPE;
         my_org_id jefy_recette.PRESTATION_BUDGET_CLIENT.org_id%TYPE;
     my_nb INTEGER;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider n''existe pas (prest_id='||a_prest_id||') (api_prestation.devalide_prestation_client)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_valide_client IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider est deja devalidee par le client (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.prest_date_valide_prest IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider est deja validee par le prestataire (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- devalidation client
           UPDATE jefy_recette.PRESTATION SET prest_date_valide_client = NULL WHERE prest_id = a_prest_id;

           -- determination du type de prestation concernée (interne ou externe)
           IF my_prest.typu_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation n''a pas de type client, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La type de client de la prestation n''existe pas, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT tyap_id INTO my_tyap_id FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;

           -- si on est en interne, on supprime l'engagement
           IF my_tyap_id = Type_Application.get_type_prestation_interne
           THEN
                 SELECT COUNT(*) INTO my_nb FROM PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;
                 IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation est une prestation interne, mais aucune information budgetaire client n''existe, pas normal ! (prest_id='||a_prest_id||')');
                 END IF;
                 SELECT eng_id, org_id INTO my_eng_id, my_org_id FROM PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;
                 IF my_eng_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation est une prestation interne, mais aucun engagement n''existe, pas normal ! (prest_id='||a_prest_id||')');
                 END IF;
                 IF my_org_id IS NULL THEN
                         -- c'etait une PI issue d'une commande Carambole, on ne supprime pas l'engagement
                         -- mais on change son tyap_id qu'on lui avait "vole"
                         UPDATE jefy_depense.engage_budget SET tyap_id = jefy_recette.type_application.get_type_depense WHERE eng_id = my_eng_id;
                 ELSE
                         del_engage(my_eng_id, a_utl_ordre);
                 END IF;
                 UPDATE PRESTATION_BUDGET_CLIENT SET eng_id = NULL WHERE prest_id = a_prest_id;
           END IF;

   END;

 -- ETAPE NO 2
 -- valide la prestation cote prestataire (date de validation prestataire)
 -- pas d'autres incidences
 PROCEDURE valide_prestation_prest (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_nb INTEGER;
   BEGIN
    api_prestation.controle_prestation_bascule ( a_prest_id);
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider n''existe pas (prest_id='||a_prest_id||') (api_prestation.valide_prestation_prest)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_valide_client IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider prestataire n''est pas encore validee par le client (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.prest_date_valide_prest IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider est deja validee par le prestataire (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a valider n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- validation prestataire
           UPDATE jefy_recette.PRESTATION SET prest_date_valide_prest = SYSDATE WHERE prest_id = a_prest_id;
   END;

 -- annulation etape no 2
 -- valide la prestation cote prestataire (date de validation prestataire)
 -- pas d'autres incidences
 PROCEDURE devalide_prestation_prest (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_nb INTEGER;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider n''existe pas (prest_id='||a_prest_id||') (api_prestation.devalide_prestation_prest)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_valide_prest IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider est deja devalidee par le prestataire (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.prest_date_cloture IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider est deja cloturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a devalider n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- devalidation prestataire
           UPDATE jefy_recette.PRESTATION SET prest_date_valide_prest = NULL WHERE prest_id = a_prest_id;
   END;

 -- ETAPE NO 3
 -- clos la prestation (date de cloture)
 -- pas d'autres incidences
 PROCEDURE cloture_prestation (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_nb INTEGER;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a cloturer n''existe pas (prest_id='||a_prest_id||') (api_prestation.cloture_prestation)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;

           IF my_prest.prest_date_valide_prest IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a cloturer n''est pas encore validee par le prestataire (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.prest_date_cloture IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a cloturer est deja cloturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a cloturer n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- cloture
           UPDATE jefy_recette.PRESTATION SET prest_date_cloture = SYSDATE WHERE prest_id = a_prest_id;
   END;

 -- annulation etape no 3
 -- "declos" la prestation (date de cloture)
 -- pas d'autres incidences
 PROCEDURE decloture_prestation (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_nb INTEGER;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a decloturer n''existe pas (prest_id='||a_prest_id||') (api_prestation.decloture_prestation)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_cloture IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a decloturer est deja decloturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.prest_date_facturation IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a decloturer est deja facturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a decloturer n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- decloture
           UPDATE jefy_recette.PRESTATION SET prest_date_cloture = NULL WHERE prest_id = a_prest_id;
   END;

 -- ETAPE NO 4
 -- genere la (les) facture(s) papier(s) a partir d'une prestation close
 PROCEDURE genere_facture_papier (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     CURSOR liste1 IS SELECT * FROM PRESTATION_LIGNE WHERE prest_id = a_prest_id AND prlig_quantite_reste > 0 ORDER BY prlig_id_pere DESC;
     CURSOR liste2 IS SELECT DISTINCT pl.pco_num
           FROM PRESTATION_LIGNE pl
           WHERE pl.prest_id = a_prest_id AND pl.prlig_quantite_reste > 0;
     my_prest         jefy_recette.PRESTATION%ROWTYPE;
     my_prest_ligne   jefy_recette.PRESTATION_LIGNE%ROWTYPE;
     my_prest_adr_cli jefy_recette.PRESTATION_ADR_CLIENT%ROWTYPE;
     my_tyap_id       jefy_recette.TYPE_PUBLIC.tyap_id%TYPE;
     my_pco_num       jefy_recette.PRESTATION.pco_num%TYPE;
     my_exe_ordre     jefy_recette.PRESTATION.exe_ordre%TYPE;
     my_fap_id        jefy_recette.FACTURE_PAPIER.fap_id%TYPE;
     my_flig_id_pere  jefy_recette.FACTURE_PAPIER_LIGNE.flig_id_pere%TYPE;
     my_pco_num_tva   jefy_recette.FACTURE_PAPIER.pco_num_tva%TYPE;
     my_pco_num_ctp   jefy_recette.FACTURE_PAPIER.pco_num_ctp%TYPE;
     my_eng_id        jefy_recette.prestation_budget_client.eng_id%TYPE;
     my_fap_total_ht  jefy_recette.facture_papier.fap_total_ht%TYPE;
     my_fap_total_ttc jefy_recette.facture_papier.fap_total_ttc%TYPE;
     my_temoin        INTEGER;
     my_nb            INTEGER;
     my_nbPrestAdrCli INTEGER;
     CURSOR liste3 IS SELECT pl.* FROM PRESTATION_LIGNE pl
           WHERE pl.prest_id = a_prest_id AND pl.pco_num = my_pco_num
             AND pl.prlig_quantite_reste > 0 ORDER BY pl.prlig_id_pere DESC;
     CURSOR liste4 IS SELECT pl.* FROM PRESTATION_LIGNE pl
           WHERE pl.prest_id = a_prest_id AND pl.pco_num IS NULL
             AND pl.prlig_quantite_reste > 0 ORDER BY pl.prlig_id_pere DESC;
   BEGIN
        select count(*) into my_nb from dt3.V_PREST_ID_DT_NON_CLOTUREE where prest_id=a_prest_id;
        if my_nb>0 then

 RAISE_APPLICATION_ERROR(-20001,'Impossible de generer la facture, la DT associee n''est pas close');
        end if;

  api_prestation.controle_prestation_bascule ( a_prest_id);

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a generer en facture n''existe pas (prest_id='||a_prest_id||') (api_prestation.genere_facture_papier)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_prest.prest_date_cloture IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a generer en facture n''est pas cloturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a generer en facture n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- determination du type de prestation concernée (interne ou externe)
           IF my_prest.typu_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation n''a pas de type client, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La type de client de la prestation n''existe pas, pas normal (prest_id='||a_prest_id||')');
           END IF;
           SELECT tyap_id INTO my_tyap_id FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_prest.typu_id;

           SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation interne n''a pas d''informations budgetaires client, pas normal ! (prest_id='||a_prest_id||')');
           END IF;
           SELECT eng_id INTO my_eng_id FROM jefy_recette.PRESTATION_BUDGET_CLIENT WHERE prest_id = a_prest_id;

           -- recupere la derniere adresse de la prestation si celle-ci existe.
  		   SELECT COUNT(*) INTO my_nbPrestAdrCli FROM jefy_recette.PRESTATION_ADR_CLIENT WHERE prest_id = a_prest_id AND date_fin IS NULL;
 		   IF my_nbPrestAdrCli = 1 THEN
 		     SELECT * INTO my_prest_adr_cli FROM jefy_recette.PRESTATION_ADR_CLIENT WHERE prest_id = a_prest_id AND date_fin IS NULL;
 		   END IF;

           -- creer la facture_papier, puis les facture_papier_ligne, puis l'adresse de facturation.
           my_pco_num   := my_prest.pco_num;
           my_exe_ordre := my_prest.exe_ordre;

           IF my_pco_num IS NOT NULL
           THEN
                   -- on est dans le cas où on force l'imputation recette ==> une seule facture papier, facile !

                   -- recup des planco tva et ctp si possible, sinon null
                   my_pco_num_tva := Recetter_Outils.get_pco_num_tva(my_pco_num, my_exe_ordre);
                   my_pco_num_ctp := Recetter_Outils.get_pco_num_ctp(my_pco_num, my_exe_ordre, my_prest.mor_ordre);

                   -- recup id sequence
                   SELECT facture_papier_seq.NEXTVAL INTO my_fap_id FROM dual;

                   INSERT INTO JEFY_RECETTE.FACTURE_PAPIER (FAP_ID, EXE_ORDRE, FAP_NUMERO,
                                 FAC_ID, UTL_ORDRE, PREST_ID, FOU_ORDRE_PREST, PERS_ID, FOU_ORDRE,
                                 NO_INDIVIDU, FAP_DATE, FAP_REF, FAP_LIB, FAP_DATE_VALIDATION_CLIENT, FAP_DATE_VALIDATION_PREST,
                                 FAP_DATE_LIMITE_PAIEMENT, FAP_DATE_REGLEMENT, FAP_REFERENCE_REGLEMENT,
                                 FAP_COMMENTAIRE_PREST, FAP_COMMENTAIRE_CLIENT, FAP_REMISE_GLOBALE,
                                 FAP_APPLY_TVA, MOR_ORDRE, RIB_ORDRE, ORG_ID, TAP_ID, TCD_ORDRE, LOLF_ID,
                                 PCO_NUM, PCO_NUM_TVA, PCO_NUM_CTP, CAN_ID, CON_ORDRE, ECHE_ID, TYPU_ID, TYET_ID,
                                 FAP_UTL_VALIDATION_CLIENT, FAP_UTL_VALIDATION_PREST, FAP_TOTAL_HT, FAP_TOTAL_TVA, FAP_TOTAL_TTC, ENG_ID)
                         VALUES (
                         my_fap_id, my_prest.exe_ordre, Get_Numerotation(my_prest.exe_ordre, NULL, 'FACTURE_PAPIER'), NULL, a_utl_ordre, a_prest_id,
                         my_prest.fou_ordre_prest, my_prest.pers_id, my_prest.fou_ordre, my_prest.no_individu, SYSDATE,
                         get_fap_ref(my_prest.exe_ordre, my_prest.org_id, my_fap_id, NULL),
                         my_prest.prest_libelle, NULL, NULL, NULL, NULL, NULL,
                         my_prest.prest_commentaire_prest, my_prest.prest_commentaire_client,
                         my_prest.prest_remise_globale, my_prest.prest_apply_tva, my_prest.mor_ordre, NULL, my_prest.org_id,
                         my_prest.tap_id, my_prest.tcd_ordre, my_prest.lolf_id,
                         my_prest.pco_num, my_pco_num_tva, my_pco_num_ctp, my_prest.can_id,
                         my_prest.con_ordre, NULL, my_prest.typu_id, my_prest.tyet_id,
                         NULL, NULL, my_prest.prest_total_ht, my_prest.prest_total_tva, my_prest.prest_total_ttc, my_eng_id);

                   -- inserer les lignes
                   my_temoin := 0;
                OPEN liste1();
                   LOOP
                     FETCH liste1 INTO my_prest_ligne;
                     EXIT WHEN liste1%NOTFOUND;

                         my_temoin := 1;

                         IF my_prest_ligne.prlig_id_pere IS NOT NULL
                         THEN
                                 SELECT flig_id INTO my_flig_id_pere FROM FACTURE_PAPIER_LIGNE WHERE fap_id = my_fap_id AND prlig_id = my_prest_ligne.prlig_id_pere;
                         ELSE
                                 my_flig_id_pere := NULL;
                         END IF;

                         INSERT INTO FACTURE_PAPIER_LIGNE (
                            FLIG_ID, FAP_ID, FLIG_ID_PERE,
                            PRLIG_ID, FLIG_DATE, FLIG_REFERENCE,
                            FLIG_DESCRIPTION, FLIG_ART_HT, FLIG_ART_TTC,
                            FLIG_ART_TTC_INITIAL, FLIG_QUANTITE, FLIG_TOTAL_HT,
                            FLIG_TOTAL_TTC, TVA_ID, TVA_ID_INITIAL,
                            TYAR_ID)
                         VALUES (facture_papier_ligne_seq.NEXTVAL, my_fap_id, my_flig_id_pere, my_prest_ligne.prlig_id,
                                 my_prest_ligne.prlig_date, my_prest_ligne.prlig_reference, my_prest_ligne.prlig_description,
             					 my_prest_ligne.prlig_art_ht, my_prest_ligne.prlig_art_ttc, my_prest_ligne.prlig_art_ttc_initial,
								 my_prest_ligne.prlig_quantite_reste, my_prest_ligne.prlig_total_reste_ht, my_prest_ligne.prlig_total_reste_ttc,
								 my_prest_ligne.tva_id, my_prest_ligne.tva_id_initial, my_prest_ligne.tyar_id);

                         UPDATE PRESTATION_LIGNE
                           SET prlig_quantite_reste = 0, prlig_total_reste_ht = 0, prlig_total_reste_ttc = 0
                           WHERE prlig_id = my_prest_ligne.prlig_id;
                   END LOOP;
                CLOSE liste1;

                   IF my_temoin = 0
                   THEN

 RAISE_APPLICATION_ERROR(-20001,'Plus rien ! facturer sur cette prestation (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                   END IF;

                   -- affecter l'adresse, si elle existe, de la prestation a la facture papier
                   -- sinon l'adresse par defaut du fournisseur sera utilisee (aucune ligne facture_papier_adr_client)
				  IF my_nbPrestAdrCli = 1 THEN
           		    ins_facture_papier_adr_client(my_fap_id, my_prest_adr_cli.adr_ordre, my_prest_adr_cli.pers_id_creation);
           		  END IF;

                   -- renseigner si besoin la date de facturation
                   UPDATE PRESTATION SET prest_date_facturation = NVL(prest_date_facturation, SYSDATE) WHERE prest_id = a_prest_id;

           ELSE
                   -- on est dans le cas où on regroupe par compte d'imputation recette article, autant de factures papier que de comptes différents, moins facile !

                   -- la liste des comptes imputation recette différents
                   my_temoin := 0;
           OPEN liste2();
                   LOOP
                     FETCH liste2 INTO my_pco_num;
                     EXIT WHEN liste2%NOTFOUND;

                         my_temoin := 1;

                         -- recup des planco tva et ctp si possible, sinon null
                         IF my_pco_num IS NOT NULL THEN
                            my_pco_num_tva := Recetter_Outils.get_pco_num_tva(my_pco_num, my_exe_ordre);
                            my_pco_num_ctp := Recetter_Outils.get_pco_num_ctp(my_pco_num, my_exe_ordre, my_prest.mor_ordre);
                         ELSE
                            my_pco_num_tva := NULL;
                            my_pco_num_ctp := NULL;
                         END IF;

                         -- recup des totaux
                         IF my_pco_num IS NOT NULL THEN
                            SELECT SUM(prlig.prlig_total_reste_ht), SUM(prlig.prlig_total_reste_ttc)
                                           INTO my_fap_total_ht, my_fap_total_ttc
                                           FROM jefy_recette.prestation_ligne prlig
                                           WHERE prlig.prest_id = a_prest_id AND prlig.pco_num = my_pco_num;
                         ELSE
                            SELECT SUM(prlig.prlig_total_reste_ht), SUM(prlig.prlig_total_reste_ttc)
                                           INTO my_fap_total_ht, my_fap_total_ttc
                                           FROM jefy_recette.prestation_ligne prlig
                                           WHERE prlig.prest_id = a_prest_id AND prlig.pco_num IS NULL;

                         END IF;

                         -- recup id sequence
                         SELECT facture_papier_seq.NEXTVAL INTO my_fap_id FROM dual;

                     INSERT INTO JEFY_RECETTE.FACTURE_PAPIER (FAP_ID, EXE_ORDRE, FAP_NUMERO,
                                 FAC_ID, UTL_ORDRE, PREST_ID, FOU_ORDRE_PREST, PERS_ID, FOU_ORDRE,
                                 NO_INDIVIDU, FAP_DATE, FAP_REF, FAP_LIB, FAP_DATE_VALIDATION_CLIENT, FAP_DATE_VALIDATION_PREST,
								 FAP_DATE_LIMITE_PAIEMENT, FAP_DATE_REGLEMENT, FAP_REFERENCE_REGLEMENT,
								 FAP_COMMENTAIRE_PREST, FAP_COMMENTAIRE_CLIENT, FAP_REMISE_GLOBALE,
                                 FAP_APPLY_TVA, MOR_ORDRE, RIB_ORDRE, ORG_ID, TAP_ID, TCD_ORDRE, LOLF_ID,
                                 PCO_NUM, PCO_NUM_TVA, PCO_NUM_CTP, CAN_ID, CON_ORDRE, ECHE_ID, TYPU_ID, TYET_ID,
								 FAP_UTL_VALIDATION_CLIENT, FAP_UTL_VALIDATION_PREST, FAP_TOTAL_HT, FAP_TOTAL_TVA, FAP_TOTAL_TTC, ENG_ID)
                         VALUES (
                           		my_fap_id, my_prest.exe_ordre, Get_Numerotation(my_prest.exe_ordre, NULL, 'FACTURE_PAPIER'), NULL, a_utl_ordre, a_prest_id,
								my_prest.fou_ordre_prest, my_prest.pers_id, my_prest.fou_ordre, my_prest.no_individu, SYSDATE,
							 	get_fap_ref(my_prest.exe_ordre, my_prest.org_id, my_fap_id, NULL),
								my_prest.prest_libelle, NULL, NULL, NULL, NULL, NULL,
 								my_prest.prest_commentaire_prest, my_prest.prest_commentaire_client,
 								my_prest.prest_remise_globale, my_prest.prest_apply_tva, my_prest.mor_ordre, NULL, my_prest.org_id,
                           		my_prest.tap_id, my_prest.tcd_ordre, my_prest.lolf_id,
                           		my_pco_num, my_pco_num_tva, my_pco_num_ctp, my_prest.can_id, my_prest.con_ordre, NULL, my_prest.typu_id,
                           		my_prest.tyet_id,
                           		NULL, NULL, my_fap_total_ht, my_fap_total_ttc - my_fap_total_ht, my_fap_total_ttc, my_eng_id);

                     -- inserer les lignes
                         IF my_pco_num IS NOT NULL THEN

                    OPEN liste3();
                            LOOP
                         FETCH liste3 INTO my_prest_ligne;
                         EXIT WHEN liste3%NOTFOUND;

                                 IF my_prest_ligne.prlig_id_pere IS NOT NULL
                                 THEN
                                         SELECT flig_id INTO my_flig_id_pere FROM FACTURE_PAPIER_LIGNE WHERE fap_id = my_fap_id AND prlig_id = my_prest_ligne.prlig_id_pere;
                                 ELSE
                                         my_flig_id_pere := NULL;
                                 END IF;

                                 INSERT INTO FACTURE_PAPIER_LIGNE (
                                             FLIG_ID, FAP_ID, FLIG_ID_PERE,
											 PRLIG_ID, FLIG_DATE, FLIG_REFERENCE,
 											 FLIG_DESCRIPTION, FLIG_ART_HT, FLIG_ART_TTC,
											 FLIG_ART_TTC_INITIAL, FLIG_QUANTITE, FLIG_TOTAL_HT,
											 FLIG_TOTAL_TTC, TVA_ID, TVA_ID_INITIAL,
                                             TYAR_ID)
                                 VALUES (
                                 	facture_papier_ligne_seq.NEXTVAL, my_fap_id, my_flig_id_pere, my_prest_ligne.prlig_id,
									my_prest_ligne.prlig_date, my_prest_ligne.prlig_reference, my_prest_ligne.prlig_description,
 									my_prest_ligne.prlig_art_ht, my_prest_ligne.prlig_art_ttc, my_prest_ligne.prlig_art_ttc_initial,
 									my_prest_ligne.prlig_quantite_reste, my_prest_ligne.prlig_total_reste_ht,
 									my_prest_ligne.prlig_total_reste_ttc, my_prest_ligne.tva_id, my_prest_ligne.tva_id_initial, my_prest_ligne.tyar_id);

                              UPDATE PRESTATION_LIGNE
                                SET prlig_quantite_reste = 0, prlig_total_reste_ht = 0, prlig_total_reste_ttc = 0
                                WHERE prlig_id = my_prest_ligne.prlig_id;

                    END LOOP;
                    CLOSE liste3;

                         ELSE

                            OPEN liste4();
                            LOOP
                         FETCH liste4 INTO my_prest_ligne;
                         EXIT WHEN liste4%NOTFOUND;

                                 IF my_prest_ligne.prlig_id_pere IS NOT NULL
                                 THEN
                                         SELECT flig_id INTO my_flig_id_pere FROM FACTURE_PAPIER_LIGNE WHERE fap_id = my_fap_id AND prlig_id = my_prest_ligne.prlig_id_pere;
                                 ELSE
                                         my_flig_id_pere := NULL;
                                 END IF;

                                 INSERT INTO FACTURE_PAPIER_LIGNE (
                                             FLIG_ID, FAP_ID, FLIG_ID_PERE,
	 										 PRLIG_ID, FLIG_DATE, FLIG_REFERENCE,
 											 FLIG_DESCRIPTION, FLIG_ART_HT, FLIG_ART_TTC,
 											 FLIG_ART_TTC_INITIAL, FLIG_QUANTITE, FLIG_TOTAL_HT,
   											 FLIG_TOTAL_TTC, TVA_ID, TVA_ID_INITIAL,
                                             TYAR_ID)
                                 VALUES (
                                 	facture_papier_ligne_seq.NEXTVAL, my_fap_id, my_flig_id_pere, my_prest_ligne.prlig_id,
 									my_prest_ligne.prlig_date, my_prest_ligne.prlig_reference, my_prest_ligne.prlig_description,
 									my_prest_ligne.prlig_art_ht, my_prest_ligne.prlig_art_ttc, my_prest_ligne.prlig_art_ttc_initial,
 									my_prest_ligne.prlig_quantite_reste, my_prest_ligne.prlig_total_reste_ht,
 									my_prest_ligne.prlig_total_reste_ttc, my_prest_ligne.tva_id, my_prest_ligne.tva_id_initial, my_prest_ligne.tyar_id);

                              UPDATE PRESTATION_LIGNE
                                SET prlig_quantite_reste = 0, prlig_total_reste_ht = 0, prlig_total_reste_ttc = 0
                                WHERE prlig_id = my_prest_ligne.prlig_id;

                    END LOOP;
                    CLOSE liste4;

                         END IF;

                         -- affecter l'adresse, si elle existe, de la prestation a la facture papier
                         -- sinon l'adresse par defaut du fournisseur sera utilisee (aucune ligne facture_papier_adr_client)
					     IF my_nbPrestAdrCli = 1 THEN
           		    	   ins_facture_papier_adr_client(my_fap_id, my_prest_adr_cli.adr_ordre, my_prest_adr_cli.pers_id_creation);
           		  		 END IF;

                         -- renseigner si besoin la date de facturation
                         UPDATE PRESTATION SET prest_date_facturation = NVL(prest_date_facturation, SYSDATE) WHERE prest_id = a_prest_id;

               END LOOP;
               CLOSE liste2;

                   IF my_temoin = 0
                   THEN
                           -- on n'est jamais passé dans la boucle ==> aucun reste ! facturer

 RAISE_APPLICATION_ERROR(-20001,'Plus aucun reste ! facturer sur cette prestation ! (prestation numero '||my_prest.prest_numero||', prest_id='||a_prest_id||')');
                   END IF;

           END IF;

   END;

 -- annulation etape no 4
 -- suppression d'une facture papier non recettee
 -- si issue d'une prestation, met a jour les quantites restantes a facturer
 PROCEDURE del_facture_papier (
         a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
    ) IS
         my_fac_id jefy_recette.FACTURE_PAPIER.fac_id%TYPE;
         my_fap_numero jefy_recette.FACTURE_PAPIER.fap_numero%TYPE;
         my_prest_id jefy_recette.FACTURE_PAPIER.prest_id%TYPE;
         my_exe_ordre jefy_recette.FACTURE_PAPIER.exe_ordre%TYPE;
         my_flig jefy_recette.FACTURE_PAPIER_LIGNE%ROWTYPE;
         my_num_numero jefy_recette.NUMEROTATION.num_numero%TYPE;
         my_num_id jefy_recette.NUMEROTATION.num_id%TYPE;
         my_tnu_id jefy_recette.TYPE_NUMEROTATION.tnu_id%TYPE;
         my_nb INTEGER;
      CURSOR liste IS SELECT * FROM FACTURE_PAPIER_LIGNE WHERE fap_id = a_fap_id;
    BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier n''existe pas (fap_id='||a_fap_id||') (api_prestation.del_facture_papier)');
           END IF;

           -- TODO verifier si y'a pas de facture dessus (fac_id null)
           SELECT fac_id, fap_numero, prest_id, exe_ordre INTO my_fac_id, my_fap_numero, my_prest_id, my_exe_ordre FROM FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_fac_id IS NOT NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a deja ete recettee, impossible de la supprimer (facture papier numero '||my_fap_numero||', fap_id='||a_fap_id||')');
           END IF;

           IF my_prest_id IS NOT NULL
           THEN
           OPEN liste();
                   LOOP
                     FETCH liste INTO my_flig;
                     EXIT WHEN liste%NOTFOUND;

                         IF my_flig.prlig_id IS NOT NULL
                         THEN
                                 UPDATE PRESTATION_LIGNE
                                   SET prlig_quantite_reste = prlig_quantite_reste + my_flig.flig_quantite,

 prlig_total_reste_ht = prlig_total_reste_ht + my_flig.flig_total_ht,

 prlig_total_reste_ttc = prlig_total_reste_ttc + my_flig.flig_total_ttc
                                   WHERE prlig_id = my_flig.prlig_id;
                         END IF;

               END LOOP;
               CLOSE liste;

                   -- si y'a plus de facture papier, nullifier la date de facturation, sinon la mettre ! la première date de facture
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id <> a_fap_id AND prest_id = my_prest_id;
                   IF my_nb = 0
                   THEN
                         UPDATE PRESTATION
                            SET prest_date_facturation = NULL
                            WHERE prest_id = my_prest_id;
                   ELSE
                         UPDATE PRESTATION
                            SET prest_date_facturation = (SELECT MIN(fap_date) FROM FACTURE_PAPIER WHERE prest_id = my_prest_id AND fap_id <> a_fap_id)
                            WHERE prest_id = my_prest_id;
                   END IF;

           END IF;

           DELETE FROM FACTURE_PAPIER_LIGNE WHERE fap_id = a_fap_id;

           DELETE FROM FACTURE_PAPIER_ADR_CLIENT WHERE fap_id = a_fap_id;

           DELETE FROM FACTURE_PAPIER WHERE fap_id = a_fap_id;

           -- verif si c etait la derniere facture papier generee, auquel cas un decremente le numero dans la numerotation
           SELECT COUNT(*) INTO my_nb FROM TYPE_NUMEROTATION WHERE tnu_entite = 'FACTURE_PAPIER';
           IF my_nb = 1 THEN
                 SELECT tnu_id INTO my_tnu_id FROM TYPE_NUMEROTATION WHERE tnu_entite = 'FACTURE_PAPIER';
                 SELECT COUNT(*) INTO my_nb FROM NUMEROTATION
                   WHERE tnu_id = my_tnu_id
                   AND DECODE(exe_ordre, NULL, '-', exe_ordre) = DECODE(my_exe_ordre, NULL, '-', my_exe_ordre);

                 IF my_nb = 1 THEN
                   SELECT num_numero, num_id INTO my_num_numero, my_num_id FROM NUMEROTATION
                     WHERE tnu_id = my_tnu_id
                     AND DECODE(exe_ordre, NULL, '-', exe_ordre) = DECODE(my_exe_ordre, NULL, '-', my_exe_ordre);

                   IF my_num_numero = my_fap_numero THEN
                     UPDATE NUMEROTATION SET num_numero = my_num_numero - 1 WHERE num_id = my_num_id;
                   END IF;
                 END IF;
           END IF;

    END;

 -- ETAPE NO 5
 -- validation de la facture papier cote client (date validation client)
 -- aucune autre incidence
 PROCEDURE valide_facture_papier_client (
         a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE
   ) IS
     my_fap jefy_recette.FACTURE_PAPIER%ROWTYPE;
     my_nb INTEGER;
   BEGIN
   api_prestation.controle_facture_bascule (a_fap_id);

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider n''existe pas (fap_id='||a_fap_id||') (api_prestation.valide_facture_papier_client)');
           END IF;
       SELECT * INTO my_fap FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;

           IF my_fap.fap_date_validation_client IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider est deja validee par le client (fap_id='||a_fap_id||')');
           END IF;
           IF my_fap.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider n''est pas une facture papier valide (fap_id='||a_fap_id||')');
           END IF;

           -- validation client
           UPDATE jefy_recette.FACTURE_PAPIER SET fap_date_validation_client = SYSDATE, fap_utl_validation_client = a_utl_ordre WHERE fap_id = a_fap_id;
  END;

 -- annulation etape no 5
 -- devalidation de la facture cote client si non validee prestataire (date validation client)
 -- aucune autre incidence
 PROCEDURE devalide_facture_papier_client (
         a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE
   ) IS
     my_fap jefy_recette.FACTURE_PAPIER%ROWTYPE;
     my_nb INTEGER;
      CURSOR liste IS SELECT rec_id FROM RECETTE WHERE fac_id = my_fap.fac_id;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider n''existe pas (fap_id='||a_fap_id||') (api_prestation.devalide_facture_papier_client)');
           END IF;
       SELECT * INTO my_fap FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;

           IF my_fap.fap_date_validation_client IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider est deja devalidee par le client (fap_id='||a_fap_id||')');
           END IF;
           IF my_fap.fap_date_validation_prest IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider est deja validee par le prestataire (fap_id='||a_fap_id||')');
           END IF;
           IF my_fap.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider n''est pas une facture papier valide (fap_id='||a_fap_id||')');
           END IF;

           -- devalidation client
           UPDATE jefy_recette.FACTURE_PAPIER SET fap_date_validation_client = NULL, fap_utl_validation_client = a_utl_ordre WHERE fap_id = a_fap_id;
   END;

 -- ETAPE NO 6
 -- valide la facture papier cote prestataire
 -- c.a.d genere la facture (table facture), lie à l'engagement si c'est de l'interne,
 -- puis genere la recette (table recette) et liquide l'engagement si c'est de l'interne (puis lie les 2)
 -- a_fac_id, a_utl_ordre et a_mor_ordre obligatoires
 -- autres facultatifs (determination auto si null)
 PROCEDURE valide_facture_papier_prest (
         a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE,
         a_tap_id_depense jefy_depense.depense_budget.tap_id%TYPE,
         a_rib_ordre jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
         a_mor_ordre jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
         a_tap_id_recette                jefy_recette.RECETTE.tap_id%TYPE
   ) IS
     my_fap jefy_recette.FACTURE_PAPIER%ROWTYPE;
     my_fac_id jefy_recette.FACTURE_PAPIER.fac_id%TYPE;
     my_nb INTEGER;
   BEGIN
     api_prestation.controle_facture_bascule (a_fap_id);

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider n''existe pas (fap_id='||a_fap_id||') (api_prestation.valide_facture_papier_prest)');
           END IF;
       SELECT * INTO my_fap FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;

           IF my_fap.fap_date_validation_prest IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider est deja validee par le prestataire (fap_id='||a_fap_id||')');
           END IF;
           IF my_fap.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a valider n''est pas une facture papier valide (fap_id='||a_fap_id||')');
           END IF;

           -- validation prestataire
           UPDATE jefy_recette.FACTURE_PAPIER SET fap_date_validation_prest = SYSDATE, fap_utl_validation_prest = a_utl_ordre WHERE fap_id = a_fap_id;

           -- recettage
           ins_facture(a_fap_id, a_utl_ordre);
           SELECT fac_id INTO my_fac_id FROM FACTURE_PAPIER WHERE fap_id = a_fap_id;
           ins_recette(my_fac_id, my_fap.fap_numero, a_utl_ordre, a_tap_id_depense, a_rib_ordre, a_mor_ordre, a_tap_id_recette, my_fap.pco_num_tva, my_fap.pco_num_ctp);
   END;

 -- annulation etape no 6
 -- devalidation facture cote prestataire si possible (recette non titree)
 -- supprime la facture/recette correspondante
 -- si c'est de l'interne, supprime aussi la liquidation si possible
 PROCEDURE devalide_facture_papier_prest (
         a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE
   ) IS
     my_fap jefy_recette.FACTURE_PAPIER%ROWTYPE;
         my_rec_id jefy_recette.RECETTE.rec_id%TYPE;
     my_nb INTEGER;
      CURSOR liste IS SELECT rec_id FROM RECETTE WHERE fac_id = my_fap.fac_id;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider n''existe pas (fap_id='||a_fap_id||') (api_prestation.devalide_facture_papier_prest)');
           END IF;
       SELECT * INTO my_fap FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;

           IF my_fap.fap_date_validation_prest IS NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider est deja devalidee par le prestataire (fap_id='||a_fap_id||')');
           END IF;
           IF my_fap.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier a devalider n''est pas une facture papier valide (fap_id='||a_fap_id||')');
           END IF;

           -- vire les recettes
           OPEN liste();
           LOOP
                   FETCH liste INTO my_rec_id;
                   EXIT WHEN liste%NOTFOUND;

                   del_recette(my_rec_id, a_utl_ordre);

           END LOOP;
           CLOSE liste;
           del_facture(my_fap.fac_id, a_utl_ordre);

           -- devalidation prestataire
           UPDATE jefy_recette.FACTURE_PAPIER SET fap_date_validation_prest = NULL, fap_utl_validation_prest = a_utl_ordre WHERE fap_id = a_fap_id;
   END;

 -- Archivage d'une prestation (= suppression, mais on ne supprime jamais réellement une prestation)
 PROCEDURE archive_prestation (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE
   ) IS
     my_prest                            jefy_recette.PRESTATION%ROWTYPE;
     my_nb INTEGER;
   BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a archiver n''existe pas (prest_id='||a_prest_id||') (api_prestation.archive_prestation)');
           END IF;
       SELECT * INTO my_prest FROM jefy_recette.PRESTATION WHERE prest_id = a_prest_id;

           IF my_prest.prest_date_facturation IS NOT NULL
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a archiver est deja facturee (prest_id='||a_prest_id||')');
           END IF;
           IF my_prest.tyet_id <> Type_Etat.get_etat_valide
           THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation a archiver n''est pas une prestation valide (prest_id='||a_prest_id||')');
           END IF;

           -- si cloturee, on decloture...
           IF my_prest.prest_date_cloture IS NOT NULL
           THEN
                 decloture_prestation(a_prest_id, a_utl_ordre);
           END IF;
           -- si validee prestataire, on devalide...
           IF my_prest.prest_date_valide_prest IS NOT NULL
           THEN
                 devalide_prestation_prest(a_prest_id, a_utl_ordre);
           END IF;
           -- si validee client, on devalide...
           IF my_prest.prest_date_valide_client IS NOT NULL
           THEN
                 devalide_prestation_client(a_prest_id, a_utl_ordre);
           END IF;
           -- archivage...
           UPDATE PRESTATION SET tyet_id = Type_Etat.get_etat_annule WHERE prest_id = a_prest_id;
   END;

 -- met a jour une reference de facture papier existante
 -- a utiliser quand elle a ete creee directement (non issue d'une prestation, donc non generee par genere_facture_papier)
 PROCEDURE upd_fap_ref (
     a_fap_id              jefy_recette.FACTURE_PAPIER.fap_id%TYPE
    ) IS
         my_exe_ordre      jefy_recette.FACTURE_PAPIER.exe_ordre%TYPE;
         my_org_id                 jefy_recette.FACTURE_PAPIER.org_id%TYPE;
         my_fap_ref                jefy_recette.FACTURE_PAPIER.fap_ref%TYPE;
         my_nb INTEGER;
    BEGIN
   api_prestation.controle_facture_bascule (a_fap_id);

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier n''existe pas (fap_id='||a_fap_id||') (api_prestation.upd_fap_ref)');
           END IF;
           LOCK TABLE FACTURE_PAPIER IN EXCLUSIVE MODE;
           SELECT exe_ordre, org_id, fap_ref INTO my_exe_ordre, my_org_id, my_fap_ref
             FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           my_fap_ref := get_fap_ref(my_exe_ordre, my_org_id, a_fap_id, my_fap_ref);
           UPDATE FACTURE_PAPIER SET fap_ref = my_fap_ref WHERE fap_id = a_fap_id;
    END;

 -- Generation d'une prestation interne ! partir d'une commande
 -- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
 -- toutes les autres infos budgétaires recette facultatives
 PROCEDURE prestation_from_commande (
         a_comm_id                    jefy_depense.commande.comm_id%TYPE,
         a_utl_ordre                  jefy_recette.prestation.utl_ordre%TYPE,
         a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
         a_org_id_recette        jefy_recette.prestation.org_id%TYPE,
         a_tap_id_recette        jefy_recette.prestation.tap_id%TYPE,
         a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
         a_lolf_id_recette       jefy_recette.prestation.lolf_id%TYPE,
         a_pco_num_recette       jefy_recette.prestation.pco_num%TYPE,
         a_can_id_recette        jefy_recette.prestation.can_id%TYPE,
         a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
         a_prest_id      IN OUT  jefy_recette.prestation.prest_id%TYPE
   ) IS
         my_fon_ordre                    jefy_admin.fonction.fon_ordre%TYPE;
     my_prest_id jefy_recette.prestation.prest_id%TYPE;
         my_article jefy_recette.v_article%ROWTYPE;
         my_commande jefy_recette.v_commande%ROWTYPE;
         my_cat_id jefy_recette.prestation.cat_id%TYPE;
         my_eng_id jefy_recette.v_commande_engagement.eng_id%TYPE;
         my_engage jefy_recette.v_engage_budget%ROWTYPE;
         my_pers_id_client jefy_recette.v_fournis_ulr.pers_id%TYPE;
         my_pers_id_prest jefy_recette.v_fournis_ulr.pers_id%TYPE;
         my_mod_code jefy_recette.v_mode_paiement.mod_code%TYPE;
         my_mor_ordre jefy_recette.prestation.mor_ordre%TYPE;
     CURSOR liste_article IS SELECT * FROM jefy_depense.article WHERE comm_id = a_comm_id;
     my_nb                                       INTEGER;
   BEGIN
     -- verif parametres
     IF a_comm_id IS NULL OR a_utl_ordre IS NULL OR (a_prest_id is null and a_fou_ordre_client IS NULL) THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible de generer la prestation interne depuis la commande, il manque des parametres (api_prestation.prestation_from_commande)');
         END IF;

         -- recup infos commande
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_commande WHERE comm_id = a_comm_id;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'La commande n''existe pas (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         SELECT * INTO my_commande FROM jefy_recette.v_commande WHERE comm_id = a_comm_id;

         -- verif autorise a creer des pi depuis une commande...
         IF a_prest_id is null and get_parametre(my_commande.exe_ordre, 'AUTORISE_PI_FROM_COMMANDE') <> 'OUI' THEN

 RAISE_APPLICATION_ERROR(-20001,'Interdit de creer des PI depuis une commande (verifier dans jefy_recette.parametres, clé AUTORISE_PI_FROM_COMMANDE) (api_prestation.prestation_from_commande)');
         END IF;

         -- verif droits de creer une prestation
     if a_prest_id is null then
         my_fon_ordre := Get_Fonction('PRGPR');
         IF my_fon_ordre IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001, 'La fonction PRGPR pour le type d''application RECETTE n''existe pas.');
         END IF;
         SELECT COUNT(*) INTO my_nb
           FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
           WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=my_commande.exe_ordre AND uf.utl_ordre=a_utl_ordre
               AND uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='O';
             -- frivalla : control de l'exercice restreint  ????
             --AND exe_stat_fac='O';
         IF my_nb = 0 THEN
             my_fon_ordre := Get_Fonction('REFACINV');
                 SELECT COUNT(*) INTO my_nb
             FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
         WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=my_commande.exe_ordre AND uf.utl_ordre=a_utl_ordre
             AND uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre ;

             IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cet utilisateur n''a pas le droit de créer des prestations (utl_ordre='||a_utl_ordre||') (api_prestation.prestation_from_commande)');
              END IF;
                 END IF;
     end if;

         -- frivalla :
         -- verifs etat commande
         IF my_commande.tyet_id = jefy_depense.etats.get_etat_annulee THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande est annulee (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_commande.tyet_id = jefy_depense.etats.get_etat_part_engagee THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande n''est que partiellement engagee (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_commande.tyet_id = jefy_depense.etats.get_etat_part_soldee THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande est partiellement soldee (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_commande.tyet_id = jefy_depense.etats.get_etat_soldee THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande est soldee (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_commande.tyet_id = jefy_depense.etats.get_etat_precommande THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande n''est qu''une precommande (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_commande.tyet_id <> jefy_depense.etats.get_etat_engagee THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: cette commande n''est pas a l''etat engage (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;

         -- commande_engagement
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_commande_engagement WHERE comm_id = a_comm_id;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'La commande n''a pas d''engagement lie, bizarre... (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_nb > 1 THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: la commande est declinee en plusieurs engagements (plusieurs lignes budgetaires), on ne peut pas l''utiliser pour generer une prestation interne (comm_id='||a_comm_id||') (api_prestation.prestation_from_commande)');
         END IF;
         SELECT eng_id INTO my_eng_id FROM jefy_recette.v_commande_engagement WHERE comm_id = a_comm_id;

         -- engage_budget
         SELECT * INTO my_engage FROM jefy_recette.v_engage_budget WHERE eng_id = my_eng_id;
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_engage_ctrl_planco WHERE eng_id = my_eng_id;
         IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'L''engagement n''a pas d''imputation definie, bizarre... (eng_id='||my_eng_id||') (api_prestation.prestation_from_commande)');
         END IF;
         IF my_nb > 1 THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible: L''engagement possède plusieurs imputations, on ne peut pas l''utiliser pour generer une prestation interne (eng_id='||my_eng_id||') (api_prestation.prestation_from_commande)');
         END IF;

         -- fournisseur client... existe-t-il ? fait-il parti du groupe des fournisseurs valides internes ?
     if a_prest_id is null then
         SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = a_fou_ordre_client;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur client n''existe pas (fou_ordre='||a_fou_ordre_client||') (api_prestation.prestation_from_commande)');
         END IF;
         SELECT pers_id INTO my_pers_id_client FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = a_fou_ordre_client AND ROWNUM=1;
         SELECT COUNT(*) INTO my_nb FROM grhum.REPART_STRUCTURE
           WHERE c_structure = (SELECT param_value FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE')
             AND pers_id = my_pers_id_client;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'Ce fournisseur client n''est pas un fournisseur valide interne (fou_ordre='||a_fou_ordre_client||') (api_prestation.prestation_from_commande)');
         END IF;
     end if;
         -- fournisseur prestataire... existe-t-il ? fait-il parti du groupe des fournisseurs valides internes ?
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = my_commande.fou_ordre;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'Le fournisseur prestataire n''existe pas (fou_ordre='||my_commande.fou_ordre||') (api_prestation.prestation_from_commande)');
         END IF;
         SELECT pers_id INTO my_pers_id_prest FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = my_commande.fou_ordre AND ROWNUM=1;
         SELECT COUNT(*) INTO my_nb FROM grhum.REPART_STRUCTURE
           WHERE c_structure = (SELECT param_value FROM grhum.GRHUM_PARAMETRES WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE')
             AND pers_id = my_pers_id_prest;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'Le fournisseur prestataire n''est pas un fournisseur valide interne (fou_ordre='||my_commande.fou_ordre||') (api_prestation.prestation_from_commande)');
         END IF;

         -- mode de recouvrement interne
     if a_prest_id is null then
         my_mod_code := jefy_recette.get_parametre(my_commande.exe_ordre, 'MODE_RECOUVREMENT_PI');
         IF my_mod_code IS NULL THEN
            RAISE_APPLICATION_ERROR(-20001,'Le mode de recouvrement des prestations internes n''est pas paramétré (api_prestation.prestation_from_commande)');
         END IF;
         SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_mode_recouvrement
           WHERE mod_code = my_mod_code AND exe_ordre = my_commande.exe_ordre AND ROWNUM = 1;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'Le mode de recouvrement paramétré (code ' || my_mod_code || ') n''existe pas (api_prestation.prestation_from_commande)');
         END IF;
         SELECT mod_ordre INTO my_mor_ordre FROM jefy_recette.v_mode_recouvrement
           WHERE mod_code = my_mod_code AND exe_ordre = my_commande.exe_ordre AND ROWNUM = 1;
     end if;

         -- creation...
     if a_prest_id is null then
            SELECT jefy_recette.prestation_seq.NEXTVAL INTO my_prest_id FROM dual;

         INSERT INTO JEFY_RECETTE.PRESTATION (PREST_ID, PREST_NUMERO, EXE_ORDRE, CAT_ID, PERS_ID, FOU_ORDRE,
                    NO_INDIVIDU, UTL_ORDRE, PREST_LIBELLE, PREST_DATE, PREST_COMMENTAIRE_CLIENT, PREST_COMMENTAIRE_PREST,
                    PREST_DATE_VALIDE_CLIENT, PREST_DATE_VALIDE_PREST, PREST_DATE_CLOTURE,
            PREST_DATE_FACTURATION, PREST_REMISE_GLOBALE, PREST_APPLY_TVA,
            MOR_ORDRE, ORG_ID, TAP_ID, TCD_ORDRE, LOLF_ID, PCO_NUM,
            CAN_ID, CON_ORDRE, TYPU_ID, TYET_ID, PREST_TOTAL_HT, PREST_TOTAL_TVA, PREST_TOTAL_TTC, FOU_ORDRE_PREST)
         VALUES (my_prest_id, get_numerotation(my_commande.exe_ordre, NULL, 'PRESTATION'), my_commande.exe_ordre, NULL, my_pers_id_client, a_fou_ordre_client,
               NULL, a_utl_ordre, my_commande.comm_libelle, SYSDATE, NULL, NULL,
               SYSDATE, NULL, NULL,
               NULL, NULL, 'N',
               my_mor_ordre, a_org_id_recette, a_tap_id_recette, a_tcd_ordre_recette, a_lolf_id_recette, a_pco_num_recette,
               a_can_id_recette, a_con_ordre_recette, 1, type_etat.get_etat_valide, my_engage.eng_ht_saisie, 0, my_engage.eng_ht_saisie, my_commande.fou_ordre);

         INSERT INTO JEFY_RECETTE.PRESTATION_BUDGET_CLIENT (PREST_ID, ORG_ID, TAP_ID,
                            TCD_ORDRE, LOLF_ID, PCO_NUM, CAN_ID, CON_ORDRE, ENG_ID)
           VALUES (my_prest_id, NULL, NULL, NULL, NULL, NULL, NULL, NULL, my_eng_id);

         my_cat_id := NULL;

         OPEN liste_article();
         LOOP
                 FETCH liste_article INTO my_article;
                 EXIT WHEN liste_article%NOTFOUND;

                 INSERT INTO JEFY_RECETTE.PRESTATION_LIGNE (PRLIG_ID, PREST_ID, PRLIG_ID_PERE,
                            CAAR_ID, PRLIG_DATE, PRLIG_REFERENCE,
                            PRLIG_DESCRIPTION, PRLIG_ART_HT, PRLIG_ART_TTC,

 PRLIG_ART_TTC_INITIAL, PRLIG_TOTAL_HT, PRLIG_TOTAL_TTC,

 PRLIG_TOTAL_RESTE_HT, PRLIG_TOTAL_RESTE_TTC, PRLIG_QUANTITE,
                            PRLIG_QUANTITE_RESTE, TVA_ID, TVA_ID_INITIAL,
                            TYAR_ID, PCO_NUM)
                 VALUES (jefy_recette.prestation_ligne_seq.NEXTVAL, my_prest_id, NULL,
                            my_article.artc_id, SYSDATE, my_article.art_reference,

 my_article.art_libelle, my_article.art_prix_ht, my_article.art_prix_ht,

 my_article.art_prix_ttc, my_article.art_prix_total_ht, my_article.art_prix_total_ht,

 my_article.art_prix_total_ht, my_article.art_prix_total_ht, my_article.art_quantite,
                            my_article.art_quantite, null, my_article.tva_id,
                            1, a_pco_num_recette);

                 IF my_cat_id IS NULL AND my_article.artc_id IS NOT NULL THEN
                    SELECT cat_id INTO my_cat_id FROM jefy_catalogue.catalogue_article WHERE caar_id = my_article.artc_id AND ROWNUM = 1;
                 END IF;

         END LOOP;
         CLOSE liste_article;

         -- met ! jour le catalogue si y'en a un
         UPDATE jefy_recette.prestation SET cat_id = my_cat_id WHERE prest_id = my_prest_id;

     else
        my_prest_id:=a_prest_id;

        select count(*) into my_nb from prestation where prest_id = my_prest_id;
        if my_nb=0 then
           RAISE_APPLICATION_ERROR(-20001,'La prestation n''existe pas (api_prestation.prestation_from_commande)');
        end if;

        select count(*) into my_nb from prestation p, type_public t where p.prest_id = my_prest_id and p.typu_id=t.typu_id and
           t.tyap_id=type_application.get_type_prestation_interne;
        if my_nb=0 then
           RAISE_APPLICATION_ERROR(-20001,'La prestation n''est pas une prestation interne (api_prestation.prestation_from_commande)');
        end if;

        select count(*) into my_nb from prestation_budget_client where prest_id = my_prest_id and eng_id is not null;
        if my_nb<>0 then
           RAISE_APPLICATION_ERROR(-20001,'La prestation est deja rattachee a un engagement (api_prestation.prestation_from_commande)');
        end if;

        update prestation_budget_client set eng_id=my_eng_id WHERE prest_id = my_prest_id;
        update prestation set PREST_DATE_VALIDE_CLIENT=sysdate WHERE prest_id = my_prest_id;
     end if;


         -- on change le tyap_id de l'engagement...
         UPDATE jefy_depense.engage_budget SET tyap_id = jefy_recette.type_application.get_type_prestation_interne WHERE eng_id = my_eng_id;

         a_prest_id := my_prest_id;
   END;

 -- Generation d'une facture papier interne ! partir d'une commande
 -- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
 -- toutes les autres infos budgétaires recette facultatives
 PROCEDURE facture_papier_from_commande (
         a_comm_id                    jefy_depense.commande.comm_id%TYPE,
         a_utl_ordre                  jefy_recette.prestation.utl_ordre%TYPE,
         a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
         a_org_id_recette        jefy_recette.prestation.org_id%TYPE,
         a_tap_id_recette        jefy_recette.prestation.tap_id%TYPE,
         a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
         a_lolf_id_recette       jefy_recette.prestation.lolf_id%TYPE,
         a_pco_num_recette       jefy_recette.prestation.pco_num%TYPE,
         a_can_id_recette        jefy_recette.prestation.can_id%TYPE,
         a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
         a_prest_id      IN OUT  jefy_recette.prestation.prest_id%TYPE,
         a_fap_id        IN OUT  jefy_recette.facture_papier.fap_id%TYPE)
   IS
     my_nb INTEGER;
   BEGIN
     prestation_from_commande(a_comm_id, a_utl_ordre, a_fou_ordre_client, a_org_id_recette,
                 a_tap_id_recette, a_tcd_ordre_recette, a_lolf_id_recette, a_pco_num_recette, a_can_id_recette,
                 a_con_ordre_recette, a_prest_id);

         valide_prestation_prest(a_prest_id, a_utl_ordre);
         cloture_prestation(a_prest_id, a_utl_ordre);
         genere_facture_papier(a_prest_id, a_utl_ordre);

         SELECT COUNT(*) INTO my_nb FROM jefy_recette.facture_papier WHERE prest_id = a_prest_id;
         IF my_nb = 1 THEN
            SELECT fap_id INTO a_fap_id FROM jefy_recette.facture_papier WHERE prest_id = a_prest_id;
         END IF;
   END;

 -- Duplication d'une facture papier en specifiant l'adresse du fournisseur.
 -- a_fap_id, a_utl_ordre, a_fou_ordre_client, a_four_adr_ordre et a_prest_id_creation obligatoires
 -- renvoie en OUT le fap_id et le fap_numero de la facture créée
 PROCEDURE duplicate_facture_papier_adr (
         a_fap_id                jefy_recette.facture_papier.fap_id%TYPE,
         a_utl_ordre             jefy_recette.facture_papier.utl_ordre%TYPE,
         a_fou_ordre_client      jefy_recette.facture_papier.fou_ordre%TYPE,
         a_fap_id_out     IN OUT jefy_recette.facture_papier.fap_id%TYPE,
         a_fap_numero_out IN OUT jefy_recette.facture_papier.fap_numero%TYPE,
         a_fou_adr_ordre         jefy_recette.facture_papier_adr_client.adr_ordre%TYPE,
         a_pers_id_creation      jefy_recette.facture_papier_adr_client.pers_id_creation%TYPE
         )
IS
	BEGIN
		duplicate_facture_papier(a_fap_id, a_utl_ordre, a_fou_ordre_client, a_fap_id_out, a_fap_numero_out);
		ins_facture_papier_adr_client(a_fap_id_out, a_fou_adr_ordre, a_pers_id_creation);
	END;

 -- Duplication d'une facture papier
 -- a_fap_id, a_utl_ordre et a_fou_ordre_client obligatoires
 -- renvoie en OUT le fap_id et le fap_numero de la facture créée
 PROCEDURE duplicate_facture_papier (
         a_fap_id                jefy_recette.facture_papier.fap_id%TYPE,
         a_utl_ordre             jefy_recette.facture_papier.utl_ordre%TYPE,
         a_fou_ordre_client      jefy_recette.facture_papier.fou_ordre%TYPE,
         a_fap_id_out     IN OUT jefy_recette.facture_papier.fap_id%TYPE,
         a_fap_numero_out IN OUT jefy_recette.facture_papier.fap_numero%TYPE)
   IS
     my_fap            jefy_recette.facture_papier%ROWTYPE;
     my_pers_id_client jefy_recette.facture_papier.pers_id%TYPE;
     my_flig           jefy_recette.facture_papier_ligne%ROWTYPE;
     my_flig_fils      jefy_recette.facture_papier_ligne%ROWTYPE;
     my_flig_id        jefy_recette.facture_papier_ligne.flig_id%TYPE;
     CURSOR lignes      IS SELECT * FROM jefy_recette.facture_papier_ligne WHERE fap_id = a_fap_id AND flig_id_pere IS NULL;
     CURSOR lignes_fils IS SELECT * FROM jefy_recette.facture_papier_ligne WHERE flig_id_pere = my_flig.flig_id;
     my_nb INTEGER;
   BEGIN
     -- verif parametres
     IF a_fap_id IS NULL OR a_utl_ordre IS NULL OR a_fou_ordre_client IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'Impossible de dupliquer la facture_papier, il manque des parametres (api_prestation.duplicate_facture_papier)');
         END IF;

         -- recup infos facture_papier
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.facture_papier WHERE fap_id = a_fap_id;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'La facture_papier a dupliquer n''existe pas (fap_id='||a_fap_id||') (api_prestation.duplicate_facture_papier)');
         END IF;
         SELECT * INTO my_fap FROM jefy_recette.facture_papier WHERE fap_id = a_fap_id;

         -- verif droit creer une facture
         Verifier.verifier_droit_facture(my_fap.exe_ordre, a_utl_ordre);

     SELECT COUNT(*) INTO my_nb FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = a_fou_ordre_client;
         IF my_nb = 0 THEN
            RAISE_APPLICATION_ERROR(-20001,'La fournisseur client n''existe pas (fou_ordre='||a_fou_ordre_client||') (api_prestation.duplicate_facture_papier)');
         END IF;
         SELECT pers_id INTO my_pers_id_client FROM jefy_recette.v_fournis_ulr WHERE fou_ordre = a_fou_ordre_client AND ROWNUM = 1;

         SELECT jefy_recette.facture_papier_seq.NEXTVAL INTO a_fap_id_out FROM dual;
         a_fap_numero_out := jefy_recette.get_numerotation(my_fap.exe_ordre, NULL, 'FACTURE_PAPIER');

         INSERT INTO jefy_recette.facture_papier (FAP_ID, EXE_ORDRE, FAP_NUMERO,
       FAC_ID, UTL_ORDRE, PREST_ID, FOU_ORDRE_PREST, PERS_ID, FOU_ORDRE,
           NO_INDIVIDU, FAP_DATE, FAP_REF,
           FAP_LIB, FAP_DATE_VALIDATION_CLIENT, FAP_DATE_VALIDATION_PREST,
           FAP_DATE_LIMITE_PAIEMENT, FAP_DATE_REGLEMENT, FAP_REFERENCE_REGLEMENT,
           FAP_COMMENTAIRE_PREST, FAP_COMMENTAIRE_CLIENT, FAP_REMISE_GLOBALE,
           FAP_APPLY_TVA, MOR_ORDRE, RIB_ORDRE,
           ORG_ID, TAP_ID, TCD_ORDRE, LOLF_ID, PCO_NUM, PCO_NUM_TVA,
           PCO_NUM_CTP, CAN_ID, CON_ORDRE, ECHE_ID, TYPU_ID, TYET_ID,
           FAP_UTL_VALIDATION_CLIENT, FAP_UTL_VALIDATION_PREST, FAP_TOTAL_HT,
           FAP_TOTAL_TVA, FAP_TOTAL_TTC, ENG_ID)
         VALUES (a_fap_id_out, my_fap.exe_ordre, a_fap_numero_out,
           NULL, a_utl_ordre, NULL, my_fap.fou_ordre_prest, my_pers_id_client, a_fou_ordre_client,
           NULL, SYSDATE, get_fap_ref(my_fap.exe_ordre, my_fap.org_id, a_fap_id_out, NULL),
           my_fap.fap_lib, NULL, NULL,
           my_fap.fap_date_limite_paiement, NULL, NULL,
           my_fap.fap_commentaire_prest, my_fap.fap_commentaire_client, my_fap.fap_remise_globale,
           my_fap.fap_apply_tva, my_fap.mor_ordre, NULL,
           my_fap.org_id, my_fap.tap_id, my_fap.tcd_ordre, my_fap.lolf_id, my_fap.pco_num, my_fap.pco_num_tva,
           my_fap.pco_num_ctp, my_fap.can_id, my_fap.con_ordre, NULL, my_fap.typu_id, jefy_recette.type_etat.get_etat_valide,
           NULL, NULL, my_fap.fap_total_ht,
           my_fap.fap_total_tva, my_fap.fap_total_ttc, NULL);

         OPEN lignes();
         LOOP
                 FETCH lignes INTO my_flig;
                 EXIT WHEN lignes%NOTFOUND;

                 SELECT jefy_recette.facture_papier_ligne_seq.NEXTVAL INTO my_flig_id FROM dual;

                 INSERT INTO jefy_recette.facture_papier_ligne (FLIG_ID, FAP_ID, FLIG_ID_PERE,
                   PRLIG_ID, FLIG_DATE, FLIG_REFERENCE,
                   FLIG_DESCRIPTION, FLIG_ART_HT, FLIG_ART_TTC,
                   FLIG_ART_TTC_INITIAL, FLIG_QUANTITE, FLIG_TOTAL_HT,
                   FLIG_TOTAL_TTC, TVA_ID, TVA_ID_INITIAL, TYAR_ID)
                 VALUES (my_flig_id, a_fap_id_out, NULL,
                   NULL, SYSDATE, my_flig.flig_reference,
                   my_flig.flig_description, my_flig.flig_art_ht, my_flig.flig_art_ttc,
                   my_flig.flig_art_ttc_initial, my_flig.flig_quantite, my_flig.flig_total_ht,
                   my_flig.flig_total_ttc, my_flig.tva_id, my_flig.tva_id_initial, my_flig.tyar_id);

                 OPEN lignes_fils();
                 LOOP
                 FETCH lignes_fils INTO my_flig_fils;
                 EXIT WHEN lignes_fils%NOTFOUND;

                         INSERT INTO jefy_recette.facture_papier_ligne (FLIG_ID, FAP_ID, FLIG_ID_PERE,
                                 PRLIG_ID, FLIG_DATE, FLIG_REFERENCE,
                                 FLIG_DESCRIPTION, FLIG_ART_HT, FLIG_ART_TTC,

 FLIG_ART_TTC_INITIAL, FLIG_QUANTITE, FLIG_TOTAL_HT,
                                 FLIG_TOTAL_TTC, TVA_ID, TVA_ID_INITIAL, TYAR_ID)
                         VALUES (jefy_recette.facture_papier_ligne_seq.NEXTVAL, a_fap_id_out, my_flig_id,
                         NULL, SYSDATE, my_flig_fils.flig_reference,

 my_flig_fils.flig_description, my_flig_fils.flig_art_ht, my_flig_fils.flig_art_ttc,

 my_flig_fils.flig_art_ttc_initial, my_flig_fils.flig_quantite, my_flig_fils.flig_total_ht,

 my_flig_fils.flig_total_ttc, my_flig_fils.tva_id, my_flig_fils.tva_id_initial, my_flig_fils.tyar_id);

                 END LOOP;
                 CLOSE lignes_fils;

         END LOOP;
         CLOSE lignes;

   END;

 -- Solde une prestation
 PROCEDURE solde_prestation (
         a_prest_id jefy_recette.PRESTATION.prest_id%TYPE,
         a_utl_ordre jefy_recette.PRESTATION.utl_ordre%TYPE)
   is
     CURSOR liste_facturepapier IS SELECT * FROM jefy_recette.facture_papier WHERE prest_id = a_prest_id;
     CURSOR liste_recette(a_fac_id jefy_recette.recette.fac_id%type) IS SELECT * FROM jefy_recette.recette WHERE fac_id = a_fac_id;
     CURSOR liste_engage(a_fac_id jefy_recette.recette.fac_id%type) IS SELECT eng_id FROM jefy_recette.pi_eng_fac WHERE fac_id = a_fac_id;

     my_facture_papier   jefy_recette.facture_papier%rowtype;
     my_recette          jefy_recette.recette%rowtype;
     my_fac_id           jefy_recette.facture_papier.fac_id%type;
     my_eng_id           jefy_recette.pi_eng_fac.eng_id%type;
     my_nb  integer;
   begin
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.prestation WHERE prest_id = a_prest_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La prestation n''existe pas (prest_id='||a_prest_id||') (api_prestation.solde_prestation)');
           END IF;

       -- on supprime ttes les recettes non titrees, les factures non recettees et les factures papier non facturees
       OPEN liste_facturepapier();
           LOOP
                 FETCH liste_facturepapier INTO my_facture_papier;
                 EXIT WHEN liste_facturepapier%NOTFOUND;

          if my_facture_papier.fac_id is not null then
             OPEN liste_recette(my_facture_papier.fac_id);
                 LOOP
                         FETCH liste_recette INTO my_recette;
                         EXIT WHEN liste_recette%NOTFOUND;

                 select count(*) into my_nb from recette_ctrl_planco where rec_id=my_recette.rec_id and tit_id is not null;
                 if my_nb=0 then

 api_prestation.DEL_RECETTE(my_recette.rec_id, a_utl_ordre);
                 end if;

             end loop;
             close liste_recette;

             select count(*) into my_nb from recette where fac_id=my_facture_papier.fac_id;
             if my_nb=0 then
                 OPEN liste_engage(my_facture_papier.fac_id);
                     LOOP
                             FETCH liste_engage INTO my_eng_id;
                             EXIT WHEN liste_engage%NOTFOUND;


 jefy_depense.engager.solder_engage(my_eng_id, a_utl_ordre);
                 end loop;
                 close liste_engage;


 api_prestation.del_facture(my_facture_papier.fac_id, a_utl_ordre);
             end if;

          end if;

          -- on reinterroge la facture papier pour voir si l'eventuelle facture a ete detachee
         select fac_id into my_fac_id from facture_papier where fap_id=my_facture_papier.fap_id;

         -- si la facture papier n'est pas facturee on la supprime
         if my_fac_id is null then

 api_prestation.del_facture_papier(my_facture_papier.fap_id, a_utl_ordre);

         end if;

           END LOOP;
           CLOSE liste_facturepapier;

       -- on solde les restes de la prestations
       UPDATE PRESTATION_LIGNE SET prlig_quantite_reste = 0, prlig_total_reste_ht = 0, prlig_total_reste_ttc = 0
           WHERE prest_id = a_prest_id;
   end;

 -- Duplication d'une prestation
 -- a_prest_id, a_utl_ordre et a_exe_ordre obligatoires
 -- renvoie en OUT le prest_id et le prest_numero de la facture créée
 PROCEDURE duplicate_prestation (
     a_prest_id                   jefy_recette.prestation.prest_id%TYPE,
     a_utl_ordre                   jefy_recette.prestation.utl_ordre%TYPE,
     a_exe_ordre             jefy_recette.prestation.exe_ordre%TYPE,
     a_prest_id_out      IN OUT    jefy_recette.prestation.prest_id%TYPE,
     a_prest_numero_out IN OUT    jefy_recette.prestation.prest_numero%TYPE
   ) is
      my_prestation        prestation%rowtype;
      my_organ             jefy_admin.organ%rowtype;
      my_type_credit       jefy_admin.type_credit%rowtype;
      my_tcd_ordre         prestation_budget_client.tcd_ordre%type;
      my_prestation_budget_client prestation_budget_client%rowtype;
      my_mode_recouvrement maracuja.mode_recouvrement%rowtype;
      my_mor_ordre         prestation.mor_ordre%type;
      my_nb                integer;
      my_prest_id          jefy_recette.prestation.prest_id%TYPE;
      CURSOR liste_article IS SELECT * FROM jefy_recette.prestation_ligne WHERE prest_id = a_prest_id and prlig_id_pere is null;
      CURSOR liste_fils(prlig jefy_recette.prestation_ligne.prlig_id%type)
         IS SELECT * FROM jefy_recette.prestation_ligne WHERE prest_id = a_prest_id and prlig_id_pere=prlig;
      my_article           jefy_recette.prestation_ligne%ROWTYPE;
      my_article_fils      jefy_recette.prestation_ligne%ROWTYPE;
      my_prlig_id          jefy_recette.prestation_ligne.prlig_id%type;
   begin
       -- verif parametres
     IF a_prest_id IS NULL OR a_utl_ordre IS NULL OR a_exe_ordre IS NULL THEN
        RAISE_APPLICATION_ERROR(-20001,'Impossible de dupliquer la prestation, il manque des parametres (api_prestation.duplicate_prestation)');
     END IF;

     -- recup infos prestation
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.prestation WHERE prest_id = a_prest_id;
     IF my_nb = 0 THEN
        RAISE_APPLICATION_ERROR(-20001,'La prestation a dupliquer n''existe pas (prest_id='||a_prest_id||') (api_prestation.duplicate_facture_papier)');
     END IF;
     SELECT * INTO my_prestation FROM jefy_recette.prestation WHERE prest_id = a_prest_id;

     my_prestation_budget_client:=null;
     SELECT COUNT(*) INTO my_nb FROM jefy_recette.prestation_budget_client WHERE prest_id = a_prest_id;
     IF my_nb=1 THEN
        SELECT * INTO my_prestation_budget_client FROM jefy_recette.prestation_budget_client WHERE prest_id = a_prest_id;
     END IF;


     -- creation...
     SELECT jefy_recette.prestation_seq.NEXTVAL INTO my_prest_id FROM dual;
     a_prest_numero_out := get_numerotation(a_exe_ordre, NULL, 'PRESTATION');

     INSERT INTO JEFY_RECETTE.PRESTATION (PREST_ID, PREST_NUMERO, EXE_ORDRE, CAT_ID, PERS_ID, FOU_ORDRE,
           NO_INDIVIDU, UTL_ORDRE, PREST_LIBELLE, PREST_DATE, PREST_COMMENTAIRE_CLIENT, PREST_COMMENTAIRE_PREST,
           PREST_DATE_VALIDE_CLIENT, PREST_DATE_VALIDE_PREST, PREST_DATE_CLOTURE,
        PREST_DATE_FACTURATION, PREST_REMISE_GLOBALE, PREST_APPLY_TVA,
        MOR_ORDRE, ORG_ID, TAP_ID, TCD_ORDRE, LOLF_ID, PCO_NUM,
        CAN_ID, CON_ORDRE, TYPU_ID, TYET_ID, PREST_TOTAL_HT, PREST_TOTAL_TVA, PREST_TOTAL_TTC, FOU_ORDRE_PREST)
     VALUES (my_prest_id, a_prest_numero_out, a_exe_ordre, my_prestation.cat_id, my_prestation.pers_id, my_prestation.fou_ordre,
        NULL, a_utl_ordre, my_prestation.prest_libelle, SYSDATE, my_prestation.prest_commentaire_client, my_prestation.PREST_COMMENTAIRE_PREST,null, NULL, NULL,NULL, NULL, my_prestation.prest_apply_tva,
        null, null, null , null, my_prestation.lolf_id, null,
        null, null, my_prestation.typu_id, type_etat.get_etat_valide, my_prestation.prest_total_ht, my_prestation.prest_total_tva,
        my_prestation.prest_total_ttc, my_prestation.fou_ordre_prest);

       if my_prestation.org_id is not null then
         select * into my_organ from jefy_admin.organ where org_id=my_prestation.org_id;
         if (my_organ.org_date_cloture is null or my_organ.org_date_cloture>to_date('15/11/'||a_exe_ordre,'dd/mm/yyyy')) and
            my_organ.org_date_ouverture<to_date('15/11/'||a_exe_ordre,'dd/mm/yyyy') then
           update JEFY_RECETTE.PRESTATION set org_id=my_prestation.ORG_ID, tap_id=my_prestation.TAP_ID,
               can_id=my_prestation.CAN_ID, con_ordre=my_prestation.CON_ORDRE where prest_id=my_prest_id;
         end if;
       end if;

       if my_prestation.tcd_ordre is not null then
         select * into my_type_credit from jefy_admin.type_credit where tcd_ordre=my_prestation.tcd_ordre;
         select count(*) into my_nb from jefy_admin.type_credit where tcd_code=my_type_credit.tcd_code and exe_ordre=a_exe_ordre;
         if my_nb>0 then
         select tcd_ordre into my_tcd_ordre from jefy_admin.type_credit where tcd_code=my_type_credit.tcd_code and exe_ordre=a_exe_ordre;
           update JEFY_RECETTE.PRESTATION set tcd_ordre=my_tcd_ordre, pco_num=my_prestation.PCO_NUM where prest_id=my_prest_id;
         end if;
       end if;

       if my_prestation.mor_ordre is not null then
         select * into my_mode_recouvrement from maracuja.mode_recouvrement where mod_ordre=my_prestation.mor_ordre;
         select count(*) into my_nb from maracuja.mode_recouvrement where mod_code=my_mode_recouvrement.mod_code and exe_ordre=a_exe_ordre;
         if my_nb>0 then
         select mod_ordre into my_mor_ordre from maracuja.mode_recouvrement where mod_code=my_mode_recouvrement.mod_code and exe_ordre=a_exe_ordre;
           update JEFY_RECETTE.PRESTATION set mor_ordre=my_mor_ordre where prest_id=my_prest_id;
         end if;
       end if;

     INSERT INTO JEFY_RECETTE.PRESTATION_BUDGET_CLIENT (PREST_ID, ORG_ID, TAP_ID,
               TCD_ORDRE, LOLF_ID, PCO_NUM, CAN_ID, CON_ORDRE, ENG_ID)
       VALUES (my_prest_id, NULL, NULL, NULL, NULL, NULL, NULL, NULL, null);

     OPEN liste_article();
     LOOP
         FETCH liste_article INTO my_article;
         EXIT WHEN liste_article%NOTFOUND;

            select jefy_recette.prestation_ligne_seq.NEXTVAL into my_prlig_id from dual;

            INSERT INTO JEFY_RECETTE.PRESTATION_LIGNE (PRLIG_ID, PREST_ID, PRLIG_ID_PERE,
                CAAR_ID, PRLIG_DATE, PRLIG_REFERENCE,
                PRLIG_DESCRIPTION, PRLIG_ART_HT, PRLIG_ART_TTC,
                PRLIG_ART_TTC_INITIAL, PRLIG_TOTAL_HT, PRLIG_TOTAL_TTC,
                PRLIG_TOTAL_RESTE_HT, PRLIG_TOTAL_RESTE_TTC, PRLIG_QUANTITE,
                PRLIG_QUANTITE_RESTE, TVA_ID, TVA_ID_INITIAL,
                TYAR_ID, PCO_NUM)
            VALUES (my_prlig_id, my_prest_id, NULL,
                my_article.caar_id, SYSDATE, my_article.prlig_reference,
                my_article.prlig_description, my_article.prlig_art_ht, my_article.prlig_art_ttc,

 my_article.prlig_art_ttc_initial, my_article.prlig_total_ht, my_article.prlig_total_ttc,
                my_article.prlig_total_reste_ht, my_article.prlig_total_reste_ttc, my_article.prlig_quantite,
                my_article.prlig_quantite_reste, my_article.tva_id, my_article.tva_id_initial,
                my_article.tyar_id, my_article.pco_num);

           OPEN liste_fils(my_article.prlig_id);
           LOOP
              FETCH liste_fils INTO my_article_fils;
              EXIT WHEN liste_fils%NOTFOUND;

              INSERT INTO JEFY_RECETTE.PRESTATION_LIGNE (PRLIG_ID, PREST_ID, PRLIG_ID_PERE,
                CAAR_ID, PRLIG_DATE, PRLIG_REFERENCE,
                PRLIG_DESCRIPTION, PRLIG_ART_HT, PRLIG_ART_TTC,
                PRLIG_ART_TTC_INITIAL, PRLIG_TOTAL_HT, PRLIG_TOTAL_TTC,
                PRLIG_TOTAL_RESTE_HT, PRLIG_TOTAL_RESTE_TTC, PRLIG_QUANTITE,
                PRLIG_QUANTITE_RESTE, TVA_ID, TVA_ID_INITIAL,
                TYAR_ID, PCO_NUM)
              VALUES (jefy_recette.prestation_ligne_seq.NEXTVAL, my_prest_id, my_prlig_id,
                my_article_fils.caar_id, SYSDATE, my_article_fils.prlig_reference,

 my_article_fils.prlig_description, my_article_fils.prlig_art_ht, my_article_fils.prlig_art_ttc,

 my_article_fils.prlig_art_ttc_initial, my_article_fils.prlig_total_ht, my_article_fils.prlig_total_ttc,

 my_article_fils.prlig_total_reste_ht, my_article_fils.prlig_total_reste_ttc, my_article_fils.prlig_quantite,

 my_article_fils.prlig_quantite_reste, my_article_fils.tva_id, my_article_fils.tva_id_initial,
                my_article_fils.tyar_id, my_article_fils.pco_num);

           END LOOP;
           CLOSE liste_fils;

     END LOOP;
     CLOSE liste_article;

     if my_prestation.exe_ordre<>a_exe_ordre then
       insert into prestation_bascule values (prestation_bascule_seq.nextval, my_prestation.prest_id, my_prest_id);
       -- ajout pour DT : on met a jour les informations en pointant sur la nouvelle prestation
       select count(*) into my_nb from all_tables where owner = 'DT3' and table_name = 'INTERVENTION_INFIN';
       if my_nb > 0 THEN
           EXECUTE IMMEDIATE JEFY_RECETTE.UpdateDTPrestId(a_prest_id, my_prest_id, a_prest_numero_out);
       end if;

       if my_prestation_budget_client.org_id is not null then
         select * into my_organ from jefy_admin.organ where org_id=my_prestation_budget_client.org_id;
         if (my_organ.org_date_cloture is null or my_organ.org_date_cloture>to_date('15/11/'||a_exe_ordre,'dd/mm/yyyy')) and
            my_organ.org_date_ouverture<to_date('15/11/'||a_exe_ordre,'dd/mm/yyyy') then
           update JEFY_RECETTE.PRESTATION_BUDGET_CLIENT set org_id=my_prestation_budget_client.ORG_ID, tap_id=my_prestation_budget_client.TAP_ID,
               can_id=my_prestation_budget_client.CAN_ID, con_ordre=my_prestation_budget_client.CON_ORDRE where prest_id=my_prest_id;
         end if;
       end if;

       if my_prestation_budget_client.tcd_ordre is not null then
         select * into my_type_credit from jefy_admin.type_credit where tcd_ordre=my_prestation_budget_client.tcd_ordre;
         select count(*) into my_nb from jefy_admin.type_credit where tcd_code=my_type_credit.tcd_code and exe_ordre=a_exe_ordre;
         if my_nb>0 then
         select tcd_ordre into my_tcd_ordre from jefy_admin.type_credit where tcd_code=my_type_credit.tcd_code and exe_ordre=a_exe_ordre;
           update JEFY_RECETTE.PRESTATION_BUDGET_CLIENT set tcd_ordre=my_tcd_ordre, pco_num=my_prestation_budget_client.PCO_NUM where prest_id=my_prest_id;
         end if;
       end if;

       update JEFY_RECETTE.PRESTATION_BUDGET_CLIENT set lolf_id=my_prestation_budget_client.LOLF_ID where prest_id=my_prest_id;
    else
       update JEFY_RECETTE.PRESTATION_BUDGET_CLIENT set org_id=my_prestation_budget_client.ORG_ID, tap_id=my_prestation_budget_client.TAP_ID,
               tcd_ordre=my_prestation_budget_client.TCD_ORDRE, lolf_id=my_prestation_budget_client.LOLF_ID, pco_num=my_prestation_budget_client.PCO_NUM,
               can_id=my_prestation_budget_client.CAN_ID, con_ordre=my_prestation_budget_client.CON_ORDRE where prest_id=my_prest_id;
    end if;

     a_prest_id_out := my_prest_id;
   end;

 -- la suite est "private"...

 -- constitution d'une reference de facture papier
 -- format : exercice-orgub-orgcr-xxx (xxx = numero incremental pour le triplet precedent, sauf si org_niv=2, pas de orgcr)
 FUNCTION get_fap_ref (
           a_exe_ordre jefy_recette.FACTURE_PAPIER.exe_ordre%TYPE,
           a_org_id jefy_recette.FACTURE_PAPIER.org_id%TYPE,
           a_fap_id jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
           a_old_fap_ref jefy_recette.FACTURE_PAPIER.fap_ref%TYPE)
    RETURN VARCHAR2
   IS
         my_ref jefy_recette.FACTURE_PAPIER.fap_ref%TYPE;
         my_org_ub jefy_recette.v_organ.org_ub%TYPE;
         my_org_cr jefy_recette.v_organ.org_cr%TYPE;
         my_org_niv jefy_recette.v_organ.org_niv%TYPE;
     my_nb INTEGER;
   BEGIN
           IF a_exe_ordre IS NULL OR a_org_id IS NULL OR a_fap_id IS NULL
           THEN
                   RETURN NULL;
           END IF;

           -- exercice
           SELECT exe_exercice INTO my_ref FROM v_exercice WHERE exe_ordre = a_exe_ordre;
           my_ref := my_ref || '-';

           SELECT org_ub, org_cr, org_niv INTO my_org_ub, my_org_cr, my_org_niv
             FROM v_organ WHERE org_id = a_org_id;
           IF my_org_ub IS NULL
           THEN
                   RETURN NULL;
           END IF;

           -- ub
           my_ref := my_ref || my_org_ub || '-';
           -- cr (si niveau 3 ou 4)
           IF my_org_niv > 2 AND my_org_cr IS NOT NULL
           THEN
                   my_ref := my_ref || my_org_cr || '-';
           END IF;

           -- index
           IF my_org_niv > 2
           THEN
                   IF a_old_fap_ref IS NOT NULL AND a_old_fap_ref LIKE my_ref||'%'
                   THEN
                           my_ref := a_old_fap_ref;
                   ELSE
                           SELECT MAX(TO_NUMBER(SUBSTR(fap_ref, LENGTH(my_ref) + 1)))
                         INTO my_nb FROM FACTURE_PAPIER
                             WHERE fap_ref LIKE my_ref||'%';

                           IF my_nb IS NULL
                           THEN
                                   my_ref := my_ref || '1';
                           ELSE
                                   my_nb := my_nb + 1;
                                   my_ref := my_ref || my_nb;
                           END IF;
                   END IF;
           ELSE
                   IF a_old_fap_ref IS NOT NULL AND INSTR(a_old_fap_ref, '-', 1, 3) = 0 AND a_old_fap_ref LIKE my_ref||'%'
                   THEN
                           my_ref := a_old_fap_ref;
                   ELSE
                           SELECT MAX(TO_NUMBER(SUBSTR(fap_ref, LENGTH(my_ref) + 1)))
                         INTO my_nb FROM FACTURE_PAPIER fp, v_organ o
                                 WHERE fp.fap_ref LIKE my_ref||'%'
                                 AND fp.org_id = o.org_id
                                 AND fp.fap_id <> a_fap_id
                                 AND o.org_niv = 2;

                           IF my_nb IS NULL
                           THEN
                                   my_ref := my_ref || '1';
                           ELSE
                                   my_nb := my_nb + 1;
                                   my_ref := my_ref || my_nb;
                           END IF;
                   END IF;
           END IF;

           RETURN my_ref;
   END;

 PROCEDURE ins_engage (
       a_eng_id IN OUT           jefy_depense.engage_budget.eng_id%TYPE,
           a_exe_ordre jefy_depense.engage_budget.exe_ordre%TYPE,
           a_eng_numero IN OUT   jefy_depense.engage_budget.eng_numero%TYPE,
           a_fou_ordre jefy_depense.engage_budget.fou_ordre%TYPE,
           a_org_id jefy_depense.engage_budget.org_id%TYPE,
           a_tcd_ordre jefy_depense.engage_budget.tcd_ordre%TYPE,
           a_tap_id jefy_depense.engage_budget.tap_id%TYPE,
           a_eng_libelle jefy_depense.engage_budget.eng_libelle%TYPE,
           a_eng_ht_saisie jefy_depense.engage_budget.eng_ht_saisie%TYPE,
           a_eng_ttc_saisie jefy_depense.engage_budget.eng_ttc_saisie%TYPE,
           a_utl_ordre jefy_depense.engage_budget.utl_ordre%TYPE,
           a_action                              VARCHAR2,
           a_analytique                  VARCHAR2,
           a_convention                  VARCHAR2,
           a_planco                              VARCHAR2
    ) IS
       my_chaine_action          VARCHAR2(100);
       my_chaine_analytique      VARCHAR2(100);
       my_chaine_convention      VARCHAR2(100);
       my_chaine_planco          VARCHAR2(100);
    BEGIN
       IF a_action IS NULL
                 THEN my_chaine_action := '$';
                 ELSE my_chaine_action := a_action||'$'|| a_eng_ht_saisie ||'$'|| a_eng_ttc_saisie ||'$$';
           END IF;
       IF a_analytique IS NULL
                 THEN my_chaine_analytique := '$';
                 ELSE my_chaine_analytique := a_analytique||'$'|| a_eng_ht_saisie ||'$'|| a_eng_ttc_saisie ||'$$';
           END IF;
       IF a_convention IS NULL
                 THEN my_chaine_convention := '$';
                 ELSE my_chaine_convention := a_convention||'$'|| a_eng_ht_saisie ||'$'|| a_eng_ttc_saisie ||'$$';
           END IF;
       IF a_planco IS NULL
                 THEN my_chaine_planco := '$';
                 ELSE my_chaine_planco := a_planco||'$'|| a_eng_ht_saisie ||'$'|| a_eng_ttc_saisie ||'$$';
           END IF;

       jefy_depense.engager.ins_engage(a_eng_id, a_exe_ordre,a_eng_numero, a_fou_ordre,
              a_org_id, a_tcd_ordre, a_tap_id, a_eng_libelle, a_eng_ht_saisie,
                 a_eng_ttc_saisie, jefy_recette.Api_Pi.get_type_application_pi, a_utl_ordre, my_chaine_action,
                 my_chaine_analytique, my_chaine_convention, '$', '$', my_chaine_planco);
    END;

 PROCEDURE del_engage (
       a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
           a_utl_ordre jefy_depense.engage_budget.utl_ordre%TYPE
    ) IS
         my_nb INTEGER;
    BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PI_ENG_FAC WHERE eng_id = a_eng_id;
           IF my_nb > 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'L''engagement est utilise par une prestation interne (eng_id='||a_eng_id||') (api_prestation.del_engage)');
           END IF;
       jefy_depense.engager.del_engage(a_eng_id, a_utl_ordre);
    END;

 PROCEDURE ins_facture (
     a_fap_id              jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
         a_utl_ordre               jefy_recette.FACTURE.utl_ordre%TYPE
    ) IS
      my_fap                       jefy_recette.FACTURE_PAPIER%ROWTYPE;
         my_fac_numero     jefy_recette.FACTURE.fac_numero%TYPE;
         my_fac_id                 jefy_recette.FACTURE.fac_id%TYPE;

         my_tyap_id                jefy_admin.Type_Application.tyap_id%TYPE;

         my_sum_ht                 jefy_recette.FACTURE.fac_ht_saisie%TYPE;
         my_sum_ttc                jefy_recette.FACTURE.fac_ttc_saisie%TYPE;

      my_chaine_action           VARCHAR2(100);
      my_chaine_analytique       VARCHAR2(100);
      my_chaine_convention       VARCHAR2(100);
      my_chaine_planco           VARCHAR2(100);

         my_pef_id                 jefy_recette.PI_ENG_FAC.pef_id%TYPE;
         my_nb INTEGER;
    BEGIN
       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier n''existe pas (fap_id='||a_fap_id||') (api_prestation.ins_facture)');
           END IF;
           SELECT * INTO my_fap FROM jefy_recette.FACTURE_PAPIER WHERE fap_id = a_fap_id;

           SELECT NVL(SUM(flig_total_ht), 0), NVL(SUM(flig_total_ttc), 0) INTO my_sum_ht, my_sum_ttc
             FROM FACTURE_PAPIER_LIGNE WHERE fap_id = a_fap_id;

       IF my_fap.lolf_id IS NULL
                 THEN my_chaine_action := '$';
                 ELSE my_chaine_action := my_fap.lolf_id||'$'|| my_sum_ht ||'$'|| my_sum_ttc ||'$$';
           END IF;
       IF my_fap.can_id IS NULL
                 THEN my_chaine_analytique := '$';
                 ELSE my_chaine_analytique := my_fap.can_id||'$'|| my_sum_ht ||'$'|| my_sum_ttc ||'$$';
           END IF;
       IF my_fap.con_ordre IS NULL
                 THEN my_chaine_convention := '$';
                 ELSE my_chaine_convention := my_fap.con_ordre||'$'|| my_sum_ht ||'$'|| my_sum_ttc ||'$$';
           END IF;
       IF my_fap.pco_num IS NULL
                 THEN my_chaine_planco := '$';
                 ELSE my_chaine_planco := my_fap.pco_num||'$'|| my_sum_ht ||'$'|| my_sum_ttc ||'$$';
           END IF;

           -- determination du type de prestation concernée (interne ou externe)
           IF my_fap.typu_id IS NULL THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture papier n''a pas de type client, pas normal (fap_id='||a_fap_id||')');
           END IF;
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_fap.typu_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La type de client de la facture papier n''existe pas, pas normal (fap_id='||a_fap_id||')');
           END IF;
           SELECT tyap_id INTO my_tyap_id FROM jefy_recette.TYPE_PUBLIC WHERE typu_id = my_fap.typu_id;

       jefy_recette.Api.ins_facture(my_fac_id, my_fap.exe_ordre, my_fac_numero,
              my_fap.fou_ordre, my_fap.pers_id, my_fap.fap_lib, my_fap.mor_ordre,
                 my_fap.org_id, my_fap.tcd_ordre, my_fap.tap_id, my_sum_ht,
                 my_sum_ttc,
                 my_tyap_id, a_utl_ordre, my_chaine_action,
                 my_chaine_analytique, my_chaine_convention, my_chaine_planco);

           -- mettre ! jour le eche_id s'il y en avait un
           IF my_fap.eche_id IS NOT NULL THEN
             UPDATE FACTURE SET eche_id = my_fap.eche_id WHERE fac_id = my_fac_id;
           END IF;

           -- mettre ! jour la facture_papier avec la clé...
           UPDATE jefy_recette.FACTURE_PAPIER SET fac_id = my_fac_id WHERE fap_id = a_fap_id;
           -- si le numero de facture etait null, le mettre ! jour aussi
           IF my_fap.fap_numero IS NULL THEN
                 UPDATE FACTURE_PAPIER SET fap_numero = my_fac_numero WHERE fap_id = a_fap_id;
           END IF;

           -- créer la prestation interne si c'en est une
           IF my_tyap_id = jefy_recette.Api_Pi.get_type_application_pi THEN

 jefy_recette.Api_Pi.ins_eng_fac(my_pef_id, my_fap.eng_id, my_fac_id, my_fap.prest_id);
           END IF;

    END;

 PROCEDURE del_facture (
     a_fac_id              jefy_recette.FACTURE.fac_id%TYPE,
         a_utl_ordre               jefy_recette.FACTURE.utl_ordre%TYPE
    ) IS
         my_tyap_id                jefy_recette.FACTURE.tyap_id%TYPE;
         my_nb INTEGER;
    BEGIN
           -- verifie si c'est de la prestation interne, auquel cas il faut virer la prestation interne avant la facture
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (api_prestation.del_facture)');
           END IF;
           SELECT tyap_id INTO my_tyap_id FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;
           IF my_tyap_id = jefy_recette.Api_Pi.get_type_application_pi THEN
                 jefy_recette.Api_Pi.del_eng_fac_facture(a_fac_id);
           END IF;

           -- suppression de la facture
           jefy_recette.Api.del_facture(a_fac_id, a_utl_ordre);

           -- mise ! jour dans facture_papier
           UPDATE FACTURE_PAPIER SET fac_id = NULL WHERE fac_id = a_fac_id;
    END;

 -- recette totalement une facture
 -- et s'il s'agit d'une prestation interne, liquide l'engagement correspondant
 -- a_fac_id et a_utl_ordre obligatoires
 -- autres facultatifs (détermination auto si possible, sinon erreur)
 PROCEDURE ins_recette (
     a_fac_id           jefy_recette.FACTURE.fac_id%TYPE,
     a_fap_numero       jefy_recette.FACTURE_PAPIER.fap_numero%TYPE,
     a_utl_ordre        jefy_recette.FACTURE.utl_ordre%TYPE,
     a_tap_id_depense   jefy_depense.depense_budget.tap_id%TYPE,
     a_rib_ordre        jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
     a_mor_ordre        jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
     a_tap_id_recette   jefy_recette.RECETTE.tap_id%TYPE,
     a_pco_num_tva      jefy_recette.RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
     a_pco_num_ctp      jefy_recette.RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE
    ) IS
      my_exe_ordre      jefy_recette.FACTURE.exe_ordre%TYPE;
      my_tyap_id        jefy_recette.FACTURE.tyap_id%TYPE;
      my_fac_ht_saisie  jefy_recette.FACTURE.fac_ht_saisie%TYPE;
      my_fac_ttc_saisie jefy_recette.FACTURE.fac_ttc_saisie%TYPE;
      my_fac_libelle    jefy_recette.FACTURE.fac_lib%TYPE;

      my_eng_id         jefy_recette.PI_ENG_FAC.eng_id%TYPE;

      my_eng_ht_saisie     jefy_depense.engage_budget.eng_ht_saisie%TYPE;
      my_sum_dep_ht_saisie jefy_depense.depense_budget.dep_ht_saisie%TYPE;
      my_mod_ordre         jefy_depense.depense_papier.mod_ordre%TYPE;

      my_pdr_id            jefy_recette.PI_DEP_REC.pdr_id%TYPE;
      my_dep_id            jefy_recette.PI_DEP_REC.dep_id%TYPE;
      my_rec_id            jefy_recette.RECETTE.rec_id%TYPE;
      my_rec_numero        jefy_recette.RECETTE.rec_numero%TYPE;

      my_nb 			   INTEGER;
      my_utl_ordre         jefy_depense.engage_budget.utl_ordre%TYPE;
    BEGIN
       IF a_fac_id IS NULL OR a_utl_ordre IS NULL THEN
		 RAISE_APPLICATION_ERROR(-20001,'Parametres a_fac_id et a_utl_ordre obligatoires (api_prestation.ins_recette)');
       END IF;

       SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;
       IF my_nb = 0 THEN
       	RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (api_prestation.ins_recette)');
       END IF;

       SELECT exe_ordre, tyap_id, fac_ht_saisie, fac_ttc_saisie, fac_lib
       INTO my_exe_ordre, my_tyap_id, my_fac_ht_saisie, my_fac_ttc_saisie, my_fac_libelle
       FROM jefy_recette.FACTURE
       WHERE fac_id = a_fac_id;

       -- recettage
       jefy_recette.Api.ins_recette_from_facture(
          my_rec_id, my_rec_numero, a_fac_id, a_utl_ordre, a_rib_ordre, a_mor_ordre, a_tap_id_recette, a_pco_num_tva, a_pco_num_ctp);

       -- verifie si c'est de la prestation interne, auquel cas il faut liquider
       -- en meme temps l'engagement lie, du montant de la facture/recette
       IF my_tyap_id = jefy_recette.Api_Pi.get_type_application_pi THEN

	       SELECT COUNT(*) INTO my_nb FROM jefy_recette.PI_ENG_FAC WHERE fac_id = a_fac_id;
	       IF my_nb = 0 THEN
	 		RAISE_APPLICATION_ERROR(-20001,'La facture correspond a une prestation interne, mais aucune PI n''a ete trouvee dans pi_eng_fac (fac_id='||a_fac_id||')');
	  	   END IF;

	  	   IF my_nb > 1 THEN
			RAISE_APPLICATION_ERROR(-20001,'La facture correspond a une prestation interne sur plusieurs engagements, impossible de liquider en auto (fac_id='||a_fac_id||')');
	       END IF;

	       SELECT eng_id INTO my_eng_id FROM jefy_recette.PI_ENG_FAC WHERE fac_id = a_fac_id;

	       -- determination du mod_ordre pour les prestations internes
	       my_mod_ordre := jefy_recette.Api_Pi.get_mode_paiement_pi(my_exe_ordre);

	       -- liquidation correspondante
	       IF length(my_fac_libelle)<480 THEN
	       	my_fac_libelle:=my_fac_libelle||', PI-FACT NO ' || a_fap_numero;
	       END IF;

		   ins_depense_from_engage(my_dep_id, my_eng_id, my_fac_libelle, my_mod_ordre, NULL, a_utl_ordre, a_tap_id_depense, my_fac_ht_saisie, my_fac_ttc_saisie);

	       -- generation de la prestation interne entre la depense (liquidation) et la recette
		   jefy_recette.Api_Pi.ins_dep_rec(my_pdr_id, my_dep_id, my_rec_id);

	       -- si le total des liquidations sur cet engagement >= le ht initial de l engagement, on solde l engagement...
	       SELECT eng_ht_saisie INTO my_eng_ht_saisie FROM jefy_depense.engage_budget
	       WHERE eng_id = my_eng_id;

	       SELECT SUM(dep_ht_saisie) INTO my_sum_dep_ht_saisie FROM jefy_depense.depense_budget
	       WHERE eng_id = my_eng_id;

	       IF my_sum_dep_ht_saisie >= my_eng_ht_saisie THEN
	       	select utl_ordre into my_utl_ordre from jefy_depense.depense_budget where dep_id=my_dep_id;
	        jefy_depense.engager.solder_engage(my_eng_id, my_utl_ordre);
	       END IF;

       END IF;
    END;

 PROCEDURE del_recette (
     a_rec_id              jefy_recette.RECETTE.rec_id%TYPE,
         a_utl_ordre               jefy_recette.FACTURE.utl_ordre%TYPE
    ) IS
         my_tyap_id                jefy_recette.FACTURE.tyap_id%TYPE;

         my_pdr_id                 jefy_recette.PI_DEP_REC.pdr_id%TYPE;
         my_dep_id                 jefy_recette.PI_DEP_REC.dep_id%TYPE;
         my_dpp_id                 jefy_depense.depense_budget.dpp_id%TYPE;

         my_nb INTEGER;

      CURSOR liste IS SELECT pdr_id, dep_id FROM jefy_recette.PI_DEP_REC WHERE rec_id = a_rec_id;
    BEGIN
           SELECT COUNT(*) INTO my_nb FROM jefy_recette.RECETTE WHERE rec_id = a_rec_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (api_prestation.del_recette)');
           END IF;
           SELECT f.tyap_id INTO my_tyap_id FROM jefy_recette.RECETTE r, jefy_recette.FACTURE f
              WHERE r.rec_id = a_rec_id AND r.fac_id = f.fac_id;

           -- verifie si c'est de la prestation interne, auquel cas il faut supprimer d'abord la pi
           IF my_tyap_id = jefy_recette.Api_Pi.get_type_application_pi THEN

         OPEN liste();
                 LOOP
                     FETCH liste INTO my_pdr_id, my_dep_id;
                     EXIT WHEN liste%NOTFOUND;

                     -- virer la pi
                         jefy_recette.Api_Pi.del_dep_rec(my_pdr_id);

                 -- virer la depense (depense_budget + depense_papier)
                         SELECT COUNT(*) INTO my_nb FROM jefy_depense.depense_budget WHERE dep_id = my_dep_id;
                         IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||my_dep_id||') (api_prestation.del_recette)');
                         END IF;
                         SELECT dpp_id INTO my_dpp_id FROM jefy_depense.depense_budget WHERE dep_id = my_dep_id;


 jefy_depense.liquider.del_depense_budget(my_dep_id, a_utl_ordre);

 jefy_depense.liquider.del_depense_papier(my_dpp_id, a_utl_ordre);

              END LOOP;
              CLOSE liste;

           END IF;

           -- suppression
           jefy_recette.Api.del_recette(a_rec_id, a_utl_ordre);

    END;

 -- a_rib_ordre peut "tre null, en fonction du mod_ordre
 -- a_tap_id peut etre null (reprend le tap_id de l'engagement dans ce cas)
 -- a_dep_ht_saisie et a_dep_ttc_saisie peuvent etre null (liquide tout l'engagement dans ce cas)
 PROCEDURE ins_depense_from_engage (
       a_dep_id IN OUT           jefy_depense.depense_budget.dep_id%TYPE,
           a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
           a_dpp_numero jefy_depense.depense_papier.dpp_numero_facture%TYPE,
           a_mod_ordre jefy_depense.depense_papier.mod_ordre%TYPE,
           a_rib_ordre jefy_depense.depense_papier.rib_ordre%TYPE,
           a_utl_ordre jefy_depense.engage_budget.utl_ordre%TYPE,
           a_tap_id jefy_depense.depense_budget.tap_id%TYPE,
           a_dep_ht_saisie jefy_depense.depense_budget.dep_ht_saisie%TYPE,
           a_dep_ttc_saisie jefy_depense.depense_budget.dep_ttc_saisie%TYPE
    ) IS
         my_engage jefy_depense.engage_budget%ROWTYPE;

         my_dpp_id jefy_depense.depense_papier.dpp_id%TYPE;
         my_dpp_numero jefy_depense.depense_papier.dpp_numero_facture%TYPE;

         my_tap_id jefy_depense.depense_budget.tap_id%TYPE;
         my_dep_ht_saisie jefy_depense.depense_budget.dep_ht_saisie%TYPE;
         my_dep_ttc_saisie jefy_depense.depense_budget.dep_ttc_saisie%TYPE;

         my_nb INTEGER;
      my_stat jefy_admin.exercice.exe_stat_liq%type;
      my_fon_ordre jefy_admin.fonction.fon_ordre%type;
      my_utl_ordre jefy_depense.engage_budget.utl_ordre%TYPE;
    BEGIN
           SELECT COUNT(*) INTO my_nb FROM jefy_depense.engage_budget WHERE eng_id = a_eng_id;
           IF my_nb = 0 THEN

 RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||') (api_prestation.ins_depense_from_engage)');
           END IF;
           -- recup des infos de l'engagement
           SELECT * INTO my_engage FROM jefy_depense.engage_budget WHERE eng_id = a_eng_id;

           -- determination du taux de prorata
           my_tap_id := a_tap_id;
           IF my_tap_id IS NULL THEN
                 my_tap_id := my_engage.tap_id;
           END IF;

           -- montants
           IF a_dep_ht_saisie IS NOT NULL AND a_dep_ttc_saisie IS NOT NULL THEN
                 my_dep_ht_saisie := a_dep_ht_saisie;
                 my_dep_ttc_saisie := a_dep_ttc_saisie;
           ELSE
                 my_dep_ht_saisie := my_engage.eng_ht_saisie;
                 my_dep_ttc_saisie := my_engage.eng_ttc_saisie;
           END IF;

           my_dpp_numero := a_dpp_numero;

           -- paf!

 jefy_depense.liquider.ins_depense_papier(my_dpp_id, my_engage.exe_ordre, my_dpp_numero,
              my_dep_ht_saisie, my_dep_ttc_saisie, my_engage.fou_ordre, a_rib_ordre,
                 a_mod_ordre, SYSDATE, SYSDATE, SYSDATE, SYSDATE, 0, a_utl_ordre, NULL);

       -- on cherche l'utl_ordre qui a le droit de liquider
       my_utl_ordre:=a_utl_ordre;
       select exe_stat_liq into my_stat from jefy_admin.exercice where exe_ordre=my_engage.exe_ordre;
       if my_stat='O' then
         my_fon_ordre:=202;
       else
         my_fon_ordre:=203;
       end if;

       select count(*) into my_nb
         from jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, jefy_admin.utilisateur_organ uo
         where uf.utl_ordre=my_utl_ordre and uf.fon_ordre=my_fon_ordre and uf.UF_ORDRE=ufe.UF_ORDRE and exe_ordre=my_engage.exe_ordre
         and uo.utl_ordre=uf.utl_ordre and org_id=my_engage.org_id;

       if my_nb=0 then
         select max(uf.utl_ordre) into my_utl_ordre
           from jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, jefy_admin.utilisateur_organ uo
           where uf.fon_ordre=my_fon_ordre and uf.UF_ORDRE=ufe.UF_ORDRE and exe_ordre=my_engage.exe_ordre
           and uo.utl_ordre=uf.utl_ordre and org_id=my_engage.org_id;
       end if;

       -- on liquide
           jefy_depense.liquider.ins_depense(
              a_dep_id, my_engage.exe_ordre, my_dpp_id, a_eng_id, my_dep_ht_saisie,
                 my_dep_ttc_saisie, my_tap_id, my_utl_ordre, NULL,

 jefy_depense.get_chaine_action(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id),

 jefy_depense.get_chaine_analytique(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id),

 jefy_depense.get_chaine_convention(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id),

 jefy_depense.get_chaine_hors_marche(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id),

 jefy_depense.get_chaine_marche(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id),

 jefy_depense.get_chaine_planco(my_dep_ht_saisie, my_dep_ttc_saisie, a_eng_id));
    END;

 FUNCTION chaine_action (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_action WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_action%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.tyac_id || '$' || a_ht || '$' || a_ttc || '$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.tyac_id || '$' || my_ctrl.eact_ht_saisie || '$' || my_ctrl.eact_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;

 FUNCTION chaine_analytique (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_analytique WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_analytique%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.can_id || '$' || a_ht || '$' || a_ttc || '$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.can_id || '$' || my_ctrl.eana_ht_saisie || '$' || my_ctrl.eana_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;

 FUNCTION chaine_convention (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_convention WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_convention%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.conv_ordre || '$' || a_ht || '$' || a_ttc || '$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.conv_ordre || '$' || my_ctrl.econ_ht_saisie || '$' || my_ctrl.econ_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;

 FUNCTION chaine_hors_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_hors_marche WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_hors_marche%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.typa_id || '$' || my_ctrl.ce_ordre || '$' || a_ht || '$' || a_ttc || '$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.typa_id || '$' || my_ctrl.ce_ordre || '$' || my_ctrl.ehom_ht_saisie || '$' || my_ctrl.ehom_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;

 FUNCTION chaine_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_marche WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_marche%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.att_ordre || '$' || a_ht || '$' || a_ttc || '$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.att_ordre || '$' || my_ctrl.emar_ht_saisie || '$' || my_ctrl.emar_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;

 FUNCTION chaine_planco (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
                 a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
                 a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
    RETURN VARCHAR2
    IS
      CURSOR liste IS SELECT * FROM jefy_depense.engage_ctrl_planco WHERE eng_id = a_eng_id;
         my_ctrl         jefy_depense.engage_ctrl_planco%ROWTYPE;
      my_chaine          VARCHAR2(30000);
    BEGIN
       my_chaine := '';
       OPEN liste();
           LOOP
                 FETCH liste INTO my_ctrl;
                 EXIT WHEN liste%NOTFOUND;
                     -- pour l'instant on n'en met qu'un, le premier trouve, du montant passe en param...
                         -- apres faudra gerer les multiples, avec proportionnel... eventuellement... :-(
                     my_chaine := my_chaine || my_ctrl.pco_num || '$' || a_ht || '$' || a_ttc || '$$$';
                         EXIT;
                     --my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_ctrl.epco_ht_saisie || '$' || my_ctrl.epco_ttc_saisie || '$';
           END LOOP;
           CLOSE liste;
           my_chaine := my_chaine || '$';
       RETURN my_chaine;
    END;


  procedure controle_prestation_bascule ( a_prest_id jefy_recette.PRESTATION.prest_id%TYPE) is
  my_nb integer;

  begin
   select count(*) into my_nb
   from prestation_bascule
   where prest_id_origine = a_prest_id;

   if my_nb != 0 then

 RAISE_APPLICATION_ERROR(-20001,'Impossible !!! votre prestation existe sur l''exercice N+1 ');
   end if;
  end;

  procedure controle_facture_bascule (a_fap_id jefy_recette.FACTURE_papier.fap_id%TYPE) is
  nb INTEGER;
  prestid integer;
  my_nb integer;

  BEGIN

   select max(prest_id) into prestid
   from facture_papier
   where fap_id = a_fap_id;

   select count(*) into my_nb from prestation_bascule
   where prest_id_origine = prestid;

   if my_nb != 0 then
    RAISE_APPLICATION_ERROR(-20001,'Impossible !!! votre prestation existe sur l''exercice N+1 ');
   end if;
  END;

  procedure ins_facture_papier_adr_client (
    a_fap_id            facture_papier_adr_client.fap_id%type,
    a_adr_ordre         facture_papier_adr_client.adr_ordre%type,
    a_pers_id_creation  facture_papier_adr_client.pers_id_creation%type)
    is
    begin
        INSERT INTO JEFY_RECETTE.FACTURE_PAPIER_ADR_CLIENT (
                FAPADC_ID, FAP_ID, ADR_ORDRE,
                DATE_CREATION, DATE_FIN, PERS_ID_CREATION)
        VALUES (facture_papier_adr_client_seq.nextval,
                a_fap_id,
                a_adr_ordre,
                sysdate,
                null,
                a_pers_id_creation);
    end;

    procedure ins_prestation_adr_client (
    a_prest_id          prestation_adr_client.prest_id%type,
    a_adr_ordre         prestation_adr_client.adr_ordre%type,
    a_pers_id_creation  prestation_adr_client.pers_id_creation%type)
    is
    begin
        INSERT INTO JEFY_RECETTE.prestation_ADR_CLIENT (
                PADC_ID, prest_id, ADR_ORDRE,
                DATE_CREATION, DATE_FIN, PERS_ID_CREATION)
        VALUES (jefy_recette.prestation_adr_client_seq.nextval,
                a_prest_id,
                a_adr_ordre,
                sysdate,
                null,
                a_pers_id_creation);
    end;

 END;

/

--------------------------------------------------------
--  DDL for Package RECETTER_OUTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."RECETTER_OUTILS" IS

FUNCTION  get_fac_montant_budgetaire(
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_fac_tap_id	        FACTURE.tap_id%TYPE,
	  a_rec_tap_id	        RECETTE.tap_id%TYPE,
	  a_montant_budgetaire  RECETTE.rec_montant_budgetaire%TYPE,
	  a_org_id	            FACTURE.org_id%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE)
   RETURN FACTURE.fac_montant_budgetaire%TYPE;

FUNCTION  get_tva (
      a_rec_ht_saisie       RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie	    RECETTE.rec_ttc_saisie%TYPE)
   RETURN RECETTE.rec_tva_saisie%TYPE;

-- recupere le plan comptable tva en fonction du pco_num, dans planco_visa
FUNCTION get_pco_num_tva (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE)
   RETURN v_planco_visa.pco_num_tva%TYPE;

-- recupere le plan comptable tva en fonction du pco_num et du mor_ordre,
-- dans mode de recouvrement en priorite, sinon dans planco_visa
FUNCTION get_pco_num_ctp (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE,
	  a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN v_planco_visa.pco_num_ctrepartie%TYPE;

FUNCTION chaine_action_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_analytique_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_convention_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_tva_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_ctp_from_facture (a_fac_id FACTURE.fac_id%TYPE, a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN VARCHAR2;

FUNCTION get_recette_debiteur_adresse(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE) RETURN VARCHAR2;

END;

/

--------------------------------------------------------
--  DDL for Package Body RECETTER_OUTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."RECETTER_OUTILS"
IS

FUNCTION  get_fac_montant_budgetaire(
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_fac_tap_id	        FACTURE.tap_id%TYPE,
	  a_rec_tap_id	        RECETTE.tap_id%TYPE,
	  a_montant_budgetaire  RECETTE.rec_montant_budgetaire%TYPE,
	  a_org_id	            FACTURE.org_id%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE)
   RETURN FACTURE.fac_montant_budgetaire%TYPE
   IS
   	   my_par_value                PARAMETRES.par_value%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
   BEGIN
        my_par_value:=Get_Parametre(a_exe_ordre, 'RECETTE_IDEM_TAP_ID');
	    IF my_par_value <> 'NON' THEN
           IF a_rec_tap_id<>a_fac_tap_id THEN
	          RAISE_APPLICATION_ERROR(-20001,'La recette doit avoir le meme prorata que sa facture');
	       END IF;

	        RETURN a_montant_budgetaire;
	    ELSE
	        RETURN Budget.calculer_budgetaire(a_exe_ordre,a_fac_tap_id,a_org_id,
	          a_rec_ht_saisie,a_rec_ttc_saisie);
	    END IF;
   END;

FUNCTION get_tva (
      a_rec_ht_saisie       RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie	    RECETTE.rec_ttc_saisie%TYPE)
     RETURN RECETTE.rec_tva_saisie%TYPE
   IS
   BEGIN
   	    -- on teste si le ht et le ttc sont du meme signe.
		IF (a_rec_ht_saisie<0 AND a_rec_ttc_saisie>0) OR (a_rec_ht_saisie>0 AND a_rec_ttc_saisie<0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT et le TTC doivent etre du meme signe');
		END IF;

		-- on teste que le ttc est superieur au ht (idem pour les reductions de recette avec la valeur absolue).
		IF ABS(a_rec_ht_saisie)>ABS(a_rec_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		RETURN a_rec_ttc_saisie - a_rec_ht_saisie;

   END;

-- recupere le plan comptable tva en fonction du pco_num, dans planco_visa
FUNCTION get_pco_num_tva (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE)
   RETURN v_planco_visa.pco_num_tva%TYPE
   IS
     my_pco_num_tva   v_planco_visa.pco_num_tva%TYPE;
     my_nb			  INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_planco_visa
		  WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE' AND ROWNUM = 1;
		IF my_nb > 0
		THEN
		   SELECT pco_num_tva INTO my_pco_num_tva FROM v_planco_visa
		   		  WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE' AND ROWNUM = 1;
		ELSE
			my_pco_num_tva := NULL;
		END IF;
		RETURN my_pco_num_tva;
   END;

-- recupere le plan comptable ctp en fonction du pco_num et du mor_ordre,
-- dans mode de recouvrement en priorite, sinon dans planco_visa
FUNCTION get_pco_num_ctp (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE,
	  a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN v_planco_visa.pco_num_ctrepartie%TYPE
   IS
     my_pco_num_ctp   v_planco_visa.pco_num_ctrepartie%TYPE;
     my_nb			  INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre AND pco_num_visa is not null;
		IF my_nb > 0
		THEN
		    SELECT pco_num_visa INTO my_pco_num_ctp FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre;
		ELSE
   			SELECT COUNT(*) INTO my_nb FROM v_planco_visa
		  		WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE';
			IF my_nb > 0
			THEN
				SELECT pco_num_ctrepartie INTO my_pco_num_ctp FROM v_planco_visa
				   WHERE pco_num_ordonnateur = a_pco_num AND pvi_etat = 'VALIDE' AND exe_ordre = a_exe_ordre AND ROWNUM = 1;
			ELSE
				my_pco_num_ctp := NULL;
			END IF;
		END IF;
		RETURN my_pco_num_ctp;
   END;

FUNCTION chaine_action_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_ACTION WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_ACTION%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.lolf_id || '$' || my_ctrl.fact_ht_saisie || '$' || my_ctrl.fact_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_analytique_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_ANALYTIQUE%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.can_id || '$' || my_ctrl.fana_ht_saisie || '$' || my_ctrl.fana_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_convention_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_CONVENTION WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_CONVENTION%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.con_ordre || '$' || my_ctrl.fcon_ht_saisie || '$' || my_ctrl.fcon_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_ctrl.fpco_ht_saisie || '$' || my_ctrl.fpco_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_tva_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
	 my_pco_num_tva	 v_planco_visa.pco_num_tva%TYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 IF my_ctrl.fpco_tva_saisie <> 0 THEN
			my_pco_num_tva := get_pco_num_tva(my_ctrl.pco_num, my_ctrl.exe_ordre);

			IF my_pco_num_tva IS NULL THEN
			   RAISE_APPLICATION_ERROR(-20001,'INDIQUER UN PLAN COMPTABLE TVA (un plan comptable de la facture n''a pas de plan comptable TVA dans planco visa (pco_num='||my_ctrl.pco_num||'))');
			END IF;

		    my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_pco_num_tva || '$' || my_ctrl.fpco_tva_saisie || '$';
		 END IF;

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_ctp_from_facture (a_fac_id FACTURE.fac_id%TYPE, a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
	 my_pco_num_ctp	 v_planco_visa.pco_num_ctrepartie%TYPE;
     my_chaine		 VARCHAR2(30000);
   	 my_nb			 INTEGER;
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 IF my_ctrl.fpco_ttc_saisie <> 0 THEN

		    my_pco_num_ctp := get_pco_num_ctp(my_ctrl.pco_num, my_ctrl.exe_ordre, a_mor_ordre);

		    IF my_pco_num_ctp IS NULL THEN
		       RAISE_APPLICATION_ERROR(-20001,'INDIQUER UN PLAN COMPTABLE CONTREPARTIE (un plan comptable de la facture n''a pas de plan comptable contrepartie dans planco visa (pco_num='||my_ctrl.pco_num||'))');
		    END IF;

		    my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_pco_num_ctp || '$' || my_ctrl.fpco_ttc_saisie || '$';
		 END IF;

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;


FUNCTION get_recette_debiteur_adresse(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
	RETURN VARCHAR2
	IS
	    my_adr_adr1      GRHUM.ADRESSE.ADR_ADRESSE1%TYPE;
	    my_adr_adr2      GRHUM.ADRESSE.ADR_ADRESSE2%TYPE;
	    my_adr_cp        GRHUM.ADRESSE.CODE_POSTAL%TYPE;
	    my_adr_ville     GRHUM.ADRESSE.VILLE%TYPE;
	    my_chaine		 VARCHAR2(500) := '';
		my_nb			 INTEGER;
	BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		-- on recupere la recette papier.
		IF my_nb = 1 THEN
			SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER_ADR_CLIENT WHERE rpp_id = a_rpp_id AND date_fin IS NULL;
			IF my_nb = 1 THEN
				-- on recupere l'adresse specifiee sur la recette papier
				SELECT ADR.ADR_ADRESSE1, ADR.ADR_ADRESSE2, ADR.CODE_POSTAL, ADR.VILLE
				INTO my_adr_adr1, my_adr_adr2, my_adr_cp, my_adr_ville
				FROM JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT RPP_ADR JOIN GRHUM.ADRESSE ADR ON RPP_ADR.adr_ordre = ADR.adr_ordre
				WHERE RPP_ADR.rpp_id = a_rpp_id AND RPP_ADR.date_fin IS NULL;
			ELSE
				-- pas d'adresse sur la recette papier, on utilise l'adresse du fournisseur
				SELECT ADR.ADR_ADRESSE1, ADR.ADR_ADRESSE2, ADR.ADR_CP, ADR.ADR_VILLE
				INTO my_adr_adr1, my_adr_adr2, my_adr_cp, my_adr_ville
				FROM JEFY_RECETTE.RECETTE_PAPIER RPP JOIN JEFY_RECETTE.V_FOURNIS_ULR ADR ON RPP.fou_ordre = ADR.fou_ordre
				WHERE RPP.rpp_id = a_rpp_id;
			END IF;
			-- construire l'adresse resultat.
			my_chaine := my_adr_adr1 ||' - '|| my_adr_adr2 ||' '|| my_adr_cp ||' '|| my_adr_ville;
		END IF;

		RETURN my_chaine;
	END;

END;

/

--------------------------------------------------------
--  DDL for Package RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."RECETTER" IS

--
-- procedures appelees par le package api
--

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE,
	  a_rpp_visible			  RECETTE_PAPIER.rpp_visible%TYPE);

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

PROCEDURE ins_recette_papier_adr_client (
      a_rpp_id                RECETTE_PAPIER_ADR_CLIENT.rpp_id%type,
      a_adr_ordre             RECETTE_PAPIER_ADR_CLIENT.adr_ordre%type,
      a_pers_id_creation      RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%type);

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqué, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE);

PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE log_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE);

PROCEDURE log_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE ins_recette_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE del_recette_ctrl_action (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_analytique (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_convention (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco_tva (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco_ctp (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE upd_date_recette_restrinct (
      a_exe_ordre         RECETTE.exe_ordre%TYPE,
	  a_rec_id			  RECETTE.REC_ID%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."RECETTER"
IS

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE,
	  a_rpp_visible			  RECETTE_PAPIER.rpp_visible%TYPE
   ) IS
	   my_rpp_id			 RECETTE_PAPIER.rpp_id%TYPE;
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;
	   my_rpp_tva_saisie	 RECETTE_PAPIER.rpp_tva_saisie%TYPE;
   BEGIN
		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rpp_tva_saisie := Recetter_Outils.get_tva(a_rpp_ht_saisie, a_rpp_ttc_saisie);

	   	-- enregistrement dans la table.
		IF a_rpp_id IS NULL THEN
	       SELECT recette_papier_seq.NEXTVAL INTO a_rpp_id FROM dual;
        END IF;

		my_rpp_numero := a_rpp_numero;
		IF my_rpp_numero IS NULL THEN
   		   my_rpp_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE_PAPIER');
   		END IF;

		Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mor_ordre, a_exe_ordre);

		INSERT INTO JEFY_RECETTE.RECETTE_PAPIER (
   			   RPP_ID, EXE_ORDRE, RPP_NUMERO, RPP_HT_SAISIE, RPP_TVA_SAISIE, RPP_TTC_SAISIE,
   			   PERS_ID, FOU_ORDRE, RIB_ORDRE,
   			   MOR_ORDRE, RPP_DATE_RECETTE, RPP_DATE_SAISIE, RPP_DATE_RECEPTION,
			   RPP_DATE_SERVICE_FAIT, RPP_NB_PIECE, UTL_ORDRE, RPP_VISIBLE)
		VALUES (
			   a_rpp_id, a_exe_ordre, my_rpp_numero, a_rpp_ht_saisie, my_rpp_tva_saisie, a_rpp_ttc_saisie,
   			   a_pers_id, a_fou_ordre, a_rib_ordre,
   			   a_mor_ordre, a_rpp_date_recette, SYSDATE, a_rpp_date_reception,
   			   a_RPP_DATE_SERVICE_FAIT, a_RPP_NB_PIECE, a_UTL_ORDRE, a_rpp_visible);

   END;

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;
	   my_rpp_tva_saisie	 RECETTE_PAPIER.rpp_tva_saisie%TYPE;
	   my_rpp_visible		 RECETTE_PAPIER.rpp_visible%TYPE;
	   my_exe_ordre			 RECETTE_PAPIER.exe_ordre%TYPE;
	   my_nb INTEGER;
   BEGIN
		IF a_rpp_id IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001,'Il faut un rpp_id !! (upd_recette_papier)');
        END IF;

   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette papier n''existe pas (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;

		SELECT rpp_visible, exe_ordre INTO my_rpp_visible, my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_rpp_visible <> 'O' THEN
		   RAISE_APPLICATION_ERROR(-20001,'Cette recette papier ne doit pas etre modifiee directement, passer par la recette correspondante (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;

		Verifier.verifier_util_recette_papier(a_rpp_id);

		IF a_rpp_ht_saisie<0 OR a_rpp_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pour une reduction de recette, il faut utiliser le package "reduire" (ins_recette_papier)');
		END IF;

		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rpp_tva_saisie := Recetter_Outils.get_tva(a_rpp_ht_saisie, a_rpp_ttc_saisie);

		my_rpp_numero := a_rpp_numero;
		IF my_rpp_numero IS NULL THEN
   		   my_rpp_numero := Get_Numerotation(my_exe_ordre, NULL, 'RECETTE_PAPIER');
   		END IF;

		Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mor_ordre, my_exe_ordre);

		UPDATE JEFY_RECETTE.RECETTE_PAPIER SET RPP_NUMERO = my_rpp_numero, RPP_HT_SAISIE = a_rpp_ht_saisie,
			   RPP_TVA_SAISIE = my_rpp_tva_saisie, RPP_TTC_SAISIE = a_rpp_ttc_saisie,
   			   PERS_ID = a_pers_id, FOU_ORDRE = a_fou_ordre, RIB_ORDRE = a_rib_ordre,
   			   MOR_ORDRE = a_mor_ordre, RPP_DATE_RECETTE = a_rpp_date_recette, RPP_DATE_SAISIE = SYSDATE, RPP_DATE_RECEPTION = a_rpp_date_reception,
			   RPP_DATE_SERVICE_FAIT = a_rpp_date_service_fait, RPP_NB_PIECE = a_rpp_nb_piece, UTL_ORDRE = a_utl_ordre
			   WHERE rpp_id = a_rpp_id;

   END;

PROCEDURE ins_recette_papier_adr_client (
  a_rpp_id            RECETTE_PAPIER_ADR_CLIENT.rpp_id%type,
  a_adr_ordre         RECETTE_PAPIER_ADR_CLIENT.adr_ordre%type,
  a_pers_id_creation  RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%type)
IS
BEGIN
    INSERT INTO JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT (
            RPPADC_ID, RPP_ID, ADR_ORDRE, DATE_CREATION, DATE_FIN, PERS_ID_CREATION)
    VALUES (recette_papier_adr_client_seq.nextval,
            a_rpp_id,
            a_adr_ordre,
            sysdate,
            null,
            a_pers_id_creation);
END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
       my_nb                 INTEGER;
	   my_par_value          PARAMETRES.par_value%TYPE;

	   my_org_id			 FACTURE.org_id%TYPE;
	   my_tap_id			 FACTURE.tap_id%TYPE;
	   my_exe_ordre			 FACTURE.exe_ordre%TYPE;
	   my_tcd_ordre			 FACTURE.tcd_ordre%TYPE;
	   my_fou_ordre			 FACTURE.fou_ordre%TYPE;
	   my_pers_id			 FACTURE.pers_id%TYPE;
	   my_fac_id			 FACTURE.fac_id%TYPE;
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;

	   my_montant_budgetaire RECETTE.rec_montant_budgetaire%TYPE;
	   my_fac_budgetaire     FACTURE.fac_montant_budgetaire%TYPE;
	   my_fac_reste          FACTURE.fac_montant_budgetaire_reste%TYPE;
	   my_fac_init           FACTURE.fac_montant_budgetaire%TYPE;
	   my_rec_fac_budgetaire FACTURE.fac_montant_budgetaire%TYPE;

	   my_rec_tva_saisie	 RECETTE.rec_tva_saisie%TYPE;
   BEGIN
		IF a_rec_ht_saisie<0 OR a_rec_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pour une reduction de recette, il faut utiliser le package "reduire" (ins_recette)');
		END IF;

		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

		SELECT exe_ordre, org_id, tap_id, tcd_ordre, fac_montant_budgetaire_reste, fac_montant_budgetaire, fou_ordre, pers_id
		  INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_fac_reste, my_fac_init, my_fou_ordre, my_pers_id
		  FROM FACTURE WHERE fac_id=a_fac_id;

		Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

		-- verification de la coherence de l'exercice.
		IF a_exe_ordre<>my_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette doit etre sur le meme exercice que la facture (ins_recette)');
		END IF;

		-- verification de la coherence du prorata.
		IF Get_Parametre(a_exe_ordre, 'RECETTE_IDEM_TAP_ID')<>'NON' AND
		   my_tap_id<>a_tap_id THEN
		     RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la recette soit le meme que la facture initiale (ins_recette)');
		END IF;

	    -- on verifie la coherence des montants.
 		IF ABS(a_rec_ht_saisie) > ABS(a_rec_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette)');
		END IF;

		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rec_tva_saisie := Recetter_Outils.get_tva(a_rec_ht_saisie, a_rec_ttc_saisie);

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
		      a_rec_ht_saisie,a_rec_ttc_saisie);

		-- on calcule pour diminuer le reste de la facture du montant recetté ou celui par rapport au tap_id.
		--    de la facture (si le tap_id peut etre different de celui de la facture).
		my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
	      my_montant_budgetaire, my_org_id, a_rec_ht_saisie, a_rec_ttc_saisie);

		-- si le reste facture est inferieur a 0, et qu'on passe une recette positive -> erreur.
		IF my_fac_reste<=0 AND my_fac_budgetaire>0 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture est deja soldee (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

		-- on verifie qu'on ne diminue pas plus que ce qu'il y a de reste de facture.
		IF my_fac_reste < my_fac_budgetaire THEN
		   --my_fac_budgetaire := my_fac_reste;
		   RAISE_APPLICATION_ERROR(-20001,'Le reste de la facture est inferieur au montant a recetter (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

  		-- on diminue le reste de la facture.
        UPDATE FACTURE SET fac_montant_budgetaire_reste = fac_montant_budgetaire_reste - my_fac_budgetaire
		   WHERE fac_id=a_fac_id;

		IF a_rpp_id IS NULL
		THEN
	  		Recetter.ins_recette_papier(a_rpp_id, a_exe_ordre, my_rpp_numero, a_rec_ht_saisie, a_rec_ttc_saisie,
		   								my_pers_id, my_fou_ordre, a_rib_ordre, a_mor_ordre, SYSDATE, SYSDATE,
		   								SYSDATE, 1, a_utl_ordre, 'N');
	    END IF;

		-- insertion dans la table.
	    IF a_rec_id IS NULL THEN
	       SELECT recette_seq.NEXTVAL INTO a_rec_id FROM dual;
	    END IF;

		IF a_rec_numero IS NULL THEN
   		   a_rec_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE');
   		END IF;

		INSERT INTO RECETTE (
			   REC_ID, EXE_ORDRE, RPP_ID, REC_NUMERO, FAC_ID, REC_LIB,
   			   REC_DATE_SAISIE, REC_MONTANT_BUDGETAIRE, REC_HT_SAISIE, REC_TVA_SAISIE, REC_TTC_SAISIE,
			   TAP_ID, TYET_ID, UTL_ORDRE, REC_ID_REDUCTION)
		VALUES (a_rec_id, a_exe_ordre, a_rpp_id, a_rec_numero, a_fac_id, a_rec_lib,
			   SYSDATE, my_montant_budgetaire, a_rec_ht_saisie, my_rec_tva_saisie, a_rec_ttc_saisie,
			   a_tap_id, Type_Etat.get_etat_valide, a_utl_ordre, NULL);

		ins_recette_ctrl_action(a_exe_ordre, a_rec_id, a_chaine_action);
		ins_recette_ctrl_analytique(a_exe_ordre, a_rec_id, a_chaine_analytique);
		ins_recette_ctrl_convention(a_exe_ordre, a_rec_id, a_chaine_convention);
		ins_recette_ctrl_planco(a_exe_ordre, a_rec_id, a_chaine_planco);
		ins_recette_ctrl_planco_tva(a_exe_ordre, a_rec_id, a_chaine_planco_tva);
		ins_recette_ctrl_planco_ctp(a_exe_ordre, a_rec_id, a_chaine_planco_ctp);

		-- mise a jour des restes des ctrl de la facture
		Corriger.maj_restes_ctrl(a_fac_id);

		Budget.ins_recette(a_exe_ordre, my_org_id, my_tcd_ordre, my_montant_budgetaire);


          upd_date_recette_restrinct(a_exe_ordre,a_rec_id);
   END;

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE
   ) IS
   	 my_facture		  		FACTURE%ROWTYPE;
   	 my_fap_id              FACTURE_PAPIER.fap_id%TYPE;
   	 my_fap_adr_client		FACTURE_PAPIER_ADR_CLIENT%ROWTYPE;

	 my_somme_ht			FACTURE.fac_ht_saisie%TYPE;
	 my_somme_tva			FACTURE_CTRL_PLANCO.fpco_tva_saisie%TYPE;
	 my_somme_ttc			FACTURE_CTRL_PLANCO.fpco_ttc_saisie%TYPE;
	 my_pco_num_pere		FACTURE_CTRL_PLANCO.pco_num%TYPE;

	 my_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE;
	 my_tap_id				RECETTE.tap_id%TYPE;

	 my_rpp_id				RECETTE_PAPIER.rpp_id%TYPE;
	 my_rpp_numero			RECETTE_PAPIER.rpp_numero%TYPE;

     my_chaine_action		VARCHAR2(30000);
     my_chaine_analytique	VARCHAR2(30000);
     my_chaine_convention	VARCHAR2(30000);
     my_chaine_planco		VARCHAR2(30000);
     my_chaine_planco_tva	VARCHAR2(30000);
     my_chaine_planco_ctp	VARCHAR2(30000);

   	 my_nb                  INTEGER;
   	 my_nbFapAdrClient      INTEGER;
   BEGIN
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;
	  IF my_nb = 0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.RECETTE WHERE fac_id = a_fac_id;
	  IF my_nb > 0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture a deja ete recettee, impossible de recetter automatiquement cette facture (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;

	  -- recup des infos de la facture
	  SELECT * INTO my_facture FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;

	  -- recuperation de l'adresse de la facture papier (si elles existent)
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER where fac_id = a_fac_id;
	  IF my_nb > 0 THEN
		SELECT fap_id INTO my_fap_id FROM jefy_recette.FACTURE_PAPIER WHERE fac_id = a_fac_id;
	  END IF;
	  my_nbFapAdrClient := 0;
	  SELECT COUNT(*) INTO my_nbFapAdrClient FROM jefy_recette.FACTURE_PAPIER_ADR_CLIENT WHERE fap_id = my_fap_id AND date_fin IS NULL;
      IF my_nbFapAdrClient = 1 THEN
        SELECT * INTO my_fap_adr_client FROM jefy_recette.FACTURE_PAPIER_ADR_CLIENT WHERE fap_id = my_fap_id AND date_fin IS NULL;
      END IF;

	  -- determination du mode de recouvrement
	  my_mor_ordre := a_mor_ordre;
	  IF my_mor_ordre IS NULL THEN
	  	 my_mor_ordre := my_facture.mor_ordre;
	  END IF;
	  IF my_mor_ordre IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Il faut un mode de recouvrement pour la recette, la facture n''en a pas (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;

	  -- determination du taux de prorata
	  my_tap_id := a_tap_id;
	  IF my_tap_id IS NULL THEN
	  	 my_tap_id := my_facture.tap_id;
	  END IF;

	  -- construction des chaines
	  my_chaine_action := Recetter_Outils.chaine_action_from_facture(a_fac_id);
	  my_chaine_analytique := Recetter_Outils.chaine_analytique_from_facture(a_fac_id);
	  my_chaine_convention := Recetter_Outils.chaine_convention_from_facture(a_fac_id);
	  my_chaine_planco := Recetter_Outils.chaine_planco_from_facture(a_fac_id);
	  -- construction des chaines planco tva et planco contrepartie
	  SELECT NVL(SUM(fpco_tva_saisie), 0), NVL(SUM(fpco_ttc_saisie), 0)
	     INTO my_somme_tva, my_somme_ttc
		 FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	  -- tva : si pas de tva (=0), pas de compte tva
	  IF my_facture.fac_tva_saisie = 0 THEN
	  	 my_chaine_planco_tva := '$';
	  ELSE
	     -- sinon prend celui passé
	     IF a_pco_num_tva IS NOT NULL THEN
		 	SELECT COUNT(*) INTO my_nb FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;
			IF my_nb <> 1 THEN
	  	 	   RAISE_APPLICATION_ERROR(-20001,'Pas de facture_ctrl_planco, pas normal (fac_id='||a_fac_id||') (ins_recette_from_facture)');
			END IF;
		 	SELECT pco_num INTO my_pco_num_pere FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;

		 	my_chaine_planco_tva := my_pco_num_pere || '$' || a_pco_num_tva || '$' || my_somme_tva || '$$$';
		 ELSE
		    -- sinon construit en auto
			my_chaine_planco_tva := Recetter_Outils.chaine_planco_tva_from_facture(a_fac_id);
		 END IF;
	  END IF;
	  -- ctp : si passe en param, prend celui-ci
	  IF a_pco_num_ctp IS NOT NULL THEN
	  		SELECT COUNT(*) INTO my_nb FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;
			IF my_nb <> 1 THEN
	  	 	   RAISE_APPLICATION_ERROR(-20001,'Pas de facture_ctrl_planco, pas normal (fac_id='||a_fac_id||') (ins_recette_from_facture)');
			END IF;
		 	SELECT pco_num INTO my_pco_num_pere FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;

	  	 	my_chaine_planco_ctp := my_pco_num_pere || '$' || a_pco_num_ctp || '$' || my_somme_ttc || '$$$';
	  ELSE
	     -- sinon construit en auto
	     my_chaine_planco_ctp := Recetter_Outils.chaine_planco_ctp_from_facture(a_fac_id, my_facture.mor_ordre);
	  END IF;

	  -- inserer la recette papier
	  ins_recette_papier(my_rpp_id, my_facture.exe_ordre, my_rpp_numero, my_facture.fac_ht_saisie, my_facture.fac_ttc_saisie,
		my_facture.pers_id, my_facture.fou_ordre, a_rib_ordre, my_mor_ordre, SYSDATE, SYSDATE,
		SYSDATE, 1, a_utl_ordre, 'N');

	  -- inserer l'adresse liee a la recette papier et provenant de la facture papier.
	  IF my_nbFapAdrClient = 1 THEN
        ins_recette_papier_adr_client(my_rpp_id, my_fap_adr_client.adr_ordre, my_fap_adr_client.pers_id_creation);
      END IF;

	  -- finalement inserer la recette
	  ins_recette(a_rec_id, my_facture.exe_ordre, my_rpp_id, a_rec_numero, a_fac_id, my_facture.fac_lib, a_rib_ordre, my_mor_ordre,
	     my_facture.fac_ht_saisie, my_facture.fac_ttc_saisie, my_tap_id, a_utl_ordre,
		 my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_planco,
		 my_chaine_planco_tva, my_chaine_planco_ctp);

   END;

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE
   ) IS
       my_nb         			INTEGER;
	   my_exe_ordre	 		    RECETTE_PAPIER.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La recette papier n''existe pas ou est deja annule (rpp_id:'||a_rpp_id||')');
	    END IF;
   		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;

		--Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		--Verifier.verifier_util_recette_papier(a_rpp_id);

		log_recette_papier(a_rpp_id, a_utl_ordre);

	    DELETE FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
	    DELETE FROM RECETTE_PAPIER_ADR_CLIENT where rpp_id = a_rpp_id;

   END;

PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      my_fac_id						RECETTE.fac_id%TYPE;
	  my_fac_tap_id					FACTURE.tap_id%TYPE;
   	  my_rec_tap_id					RECETTE.tap_id%TYPE;
   	  my_exe_ordre					RECETTE.exe_ordre%TYPE;
   	  my_org_id						FACTURE.org_id%TYPE;
	  my_tcd_ordre					FACTURE.tcd_ordre%TYPE;
	  my_rec_montant_budgetaire		RECETTE.rec_montant_budgetaire%TYPE;
	  my_rec_id_reduction		    RECETTE.rec_id_reduction%TYPE;
	  my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
	  my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;
	  my_fac_budgetaire				FACTURE.fac_montant_budgetaire%TYPE;
	  my_rpp_id						RECETTE.rpp_id%TYPE;
	  my_nb							INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (del_recette)');
		END IF;

		SELECT fac_id, exe_ordre, tap_id, rec_montant_budgetaire, rec_ht_saisie, rec_ttc_saisie, rec_id_reduction, rpp_id
		  INTO my_fac_id, my_exe_ordre, my_rec_tap_id, my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_ttc_saisie, my_rec_id_reduction, my_rpp_id
		  FROM RECETTE WHERE rec_id = a_rec_id;

		IF my_rec_id_reduction IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette est une reduction, utiliser le package REDUIRE (del_recette)');
		END IF;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		--Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		--Verifier.verifier_utilisation_recette(a_rec_id);

		log_recette(a_rec_id,a_utl_ordre);

	    del_recette_ctrl_action(a_rec_id);
	    del_recette_ctrl_analytique(a_rec_id);
	    del_recette_ctrl_convention(a_rec_id);
	    del_recette_ctrl_planco_tva(a_rec_id);
	    del_recette_ctrl_planco_ctp(a_rec_id);
	    del_recette_ctrl_planco(a_rec_id);

		-- mise a jour de la facture
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = my_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||my_fac_id||') (del_recette)');
		END IF;

		SELECT org_id, tap_id, tcd_ordre INTO my_org_id, my_fac_tap_id, my_tcd_ordre FROM FACTURE WHERE fac_id = my_fac_id;

	    my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(my_exe_ordre, my_fac_tap_id, my_rec_tap_id,
	          my_rec_montant_budgetaire, my_org_id, my_rec_ht_saisie, my_rec_ttc_saisie);

		UPDATE FACTURE SET fac_montant_budgetaire_reste = fac_montant_budgetaire_reste + my_fac_budgetaire WHERE fac_id = my_fac_id;

		-- delete...
	    DELETE FROM RECETTE WHERE rec_id=a_rec_id;

		-- verif si on doit supprimer la recette papier
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = my_rpp_id AND rpp_visible='N';
		IF my_nb = 1 THEN
		   del_recette_papier(my_rpp_id, a_utl_ordre);
		END IF;

		-- mise a jour des restes des ctrl de la facture
		Corriger.maj_restes_ctrl(my_fac_id);

		Budget.del_recette(my_exe_ordre, my_org_id, my_tcd_ordre, my_rec_montant_budgetaire);
   END;

PROCEDURE log_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE
   ) IS
   BEGIN
   		INSERT INTO Z_RECETTE_PAPIER SELECT z_recette_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, r.*
		  FROM RECETTE_PAPIER r WHERE rpp_id=a_rpp_id;
   END;

PROCEDURE log_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      CURSOR liste IS SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
	  my_rpco_id	   	  		   	RECETTE_CTRL_PLANCO.rpco_id%TYPE;
	  my_zrec_id					Z_RECETTE.zrec_id%TYPE;
	  my_zrpco_id					Z_RECETTE_CTRL_PLANCO.zrpco_id%TYPE;
   BEGIN
		SELECT z_recette_seq.NEXTVAL INTO my_zrec_id FROM dual;

		INSERT INTO Z_RECETTE SELECT my_zrec_id, SYSDATE, a_utl_ordre, r.*
		  FROM RECETTE r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_ACTION SELECT z_recette_ctrl_action_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_ACTION r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_ANALYTIQUE SELECT z_recette_ctrl_analytique_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_ANALYTIQUE r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_CONVENTION SELECT z_recette_ctrl_convention_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_CONVENTION r WHERE rec_id=a_rec_id;

      	OPEN liste();
  	  	LOOP
			FETCH liste INTO my_rpco_id;
		 	EXIT WHEN liste%NOTFOUND;

			SELECT z_recette_ctrl_planco_seq.NEXTVAL INTO my_zrpco_id FROM dual;
			INSERT INTO Z_RECETTE_CTRL_PLANCO SELECT my_zrpco_id, r.*, my_zrec_id
		  		   FROM RECETTE_CTRL_PLANCO r WHERE rpco_id = my_rpco_id;

			INSERT INTO Z_RECETTE_CTRL_PLANCO_TVA SELECT z_recette_ctrl_planco_tva_seq.NEXTVAL, r.*, my_zrpco_id
		  		   FROM RECETTE_CTRL_PLANCO_TVA r WHERE rpco_id = my_rpco_id;

			INSERT INTO Z_RECETTE_CTRL_PLANCO_CTP SELECT z_recette_ctrl_planco_ctp_seq.NEXTVAL, r.*, my_zrpco_id
		  		   FROM RECETTE_CTRL_PLANCO_CTP r WHERE rpco_id = my_rpco_id;

		END LOOP;
	  	CLOSE liste;

   END;

PROCEDURE ins_recette_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_ract_id	               RECETTE_CTRL_ACTION.ract_id%TYPE;
       my_lolf_id	  	   		   RECETTE_CTRL_ACTION.lolf_id%TYPE;
       my_ract_montant_budgetaire  RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
	   sum_ract_montant_budgetaire RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
       my_ract_ht_saisie	  	   RECETTE_CTRL_ACTION.ract_ht_saisie%TYPE;
       my_ract_tva_saisie		   RECETTE_CTRL_ACTION.ract_tva_saisie%TYPE;
       my_ract_ttc_saisie		   RECETTE_CTRL_ACTION.ract_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre				   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_action)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

  		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_action)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_lolf_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_ract_ht_saisie)>ABS(my_ract_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_action)');
		    END IF;

		    my_ract_tva_saisie := Recetter_Outils.get_tva(my_ract_ht_saisie, my_ract_ttc_saisie);

			-- on calcule le montant budgetaire.
			my_ract_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_ract_ht_saisie,my_ract_ttc_saisie);

			IF my_ract_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_action)');
			END IF;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1) = '$' OR
			  my_rec_montant_budgetaire <= my_somme + my_ract_montant_budgetaire THEN
			    my_ract_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_action_seq.NEXTVAL INTO my_ract_id FROM dual;

			INSERT INTO RECETTE_CTRL_ACTION VALUES (my_ract_id,
			       a_exe_ordre, a_rec_id, my_lolf_id, my_ract_montant_budgetaire,
				   my_ract_ht_saisie, my_ract_tva_saisie, my_ract_ttc_saisie, SYSDATE);

	   		-- procedure de verification
            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_lolf_id);
			Apres_Recette.ins_recette_ctrl_action(my_ract_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_ract_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rana_id	               RECETTE_CTRL_ANALYTIQUE.rana_id%TYPE;
       my_can_id	  	   		   RECETTE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_rana_montant_budgetaire  RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
	   sum_rana_montant_budgetaire RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
       my_rana_ht_saisie	  	   RECETTE_CTRL_ANALYTIQUE.rana_ht_saisie%TYPE;
       my_rana_tva_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_tva_saisie%TYPE;
       my_rana_ttc_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_analytique)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_analytique)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rana_ht_saisie)>ABS(my_rana_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_analytique)');
		    END IF;

		    my_rana_tva_saisie := Recetter_Outils.get_tva(my_rana_ht_saisie, my_rana_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rana_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rana_ht_saisie,my_rana_ttc_saisie);

			IF my_rana_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_analytique)');
			END IF;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_rec_montant_budgetaire <= my_somme + my_rana_montant_budgetaire THEN
			    my_rana_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a un seul code analytique
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''un seul code analytique pour une recette (ins_recette_ctrl_analytique)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_analytique_seq.NEXTVAL INTO my_rana_id FROM dual;

			INSERT INTO RECETTE_CTRL_ANALYTIQUE VALUES (my_rana_id,
			       a_exe_ordre, a_rec_id, my_can_id, my_rana_montant_budgetaire,
				   my_rana_ht_saisie, my_rana_tva_saisie, my_rana_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
			Apres_Recette.ins_recette_ctrl_analytique(my_rana_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rana_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rcon_id	               RECETTE_CTRL_CONVENTION.rcon_id%TYPE;
       my_con_ordre	  	   		   RECETTE_CTRL_CONVENTION.con_ordre%TYPE;
       my_rcon_montant_budgetaire  RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
	   sum_rcon_montant_budgetaire RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
       my_rcon_ht_saisie	  	   RECETTE_CTRL_CONVENTION.rcon_ht_saisie%TYPE;
       my_rcon_tva_saisie		   RECETTE_CTRL_CONVENTION.rcon_tva_saisie%TYPE;
       my_rcon_ttc_saisie		   RECETTE_CTRL_CONVENTION.rcon_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_convention)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_convention)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_con_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rcon_ht_saisie)>ABS(my_rcon_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_convention)');
		    END IF;

		    my_rcon_tva_saisie := Recetter_Outils.get_tva(my_rcon_ht_saisie, my_rcon_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rcon_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rcon_ht_saisie,my_rcon_ttc_saisie);

			IF my_rcon_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_convention)');
			END IF;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_rec_montant_budgetaire <= my_somme + my_rcon_montant_budgetaire THEN
			    my_rcon_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule convention
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une seule convention pour une recette (ins_recette_ctrl_convention)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_convention_seq.NEXTVAL INTO my_rcon_id FROM dual;

			INSERT INTO RECETTE_CTRL_CONVENTION VALUES (my_rcon_id,
			       a_exe_ordre, a_rec_id, my_con_ordre, my_rcon_montant_budgetaire,
				   my_rcon_ht_saisie, my_rcon_tva_saisie, my_rcon_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_con_ordre);
			Apres_Recette.ins_recette_ctrl_convention(my_rcon_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rcon_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	               RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_rpco_ht_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
       my_rpco_tva_saisie		   RECETTE_CTRL_PLANCO.rpco_tva_saisie%TYPE;
       my_rpco_ttc_saisie		   RECETTE_CTRL_PLANCO.rpco_ttc_saisie%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco)');
		END IF;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpco_ht_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant HT positif (ins_recette_ctrl_planco)');
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_rpco_ht_saisie) > ABS(my_rpco_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_planco)');
		    END IF;

		    my_rpco_tva_saisie := Recetter_Outils.get_tva(my_rpco_ht_saisie, my_rpco_ttc_saisie);

			-- on bloque a une seule imputation
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une recette (ins_recette_ctrl_planco)');
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_seq.NEXTVAL INTO my_rpco_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO VALUES (my_rpco_id,
			       a_exe_ordre, a_rec_id, my_pco_num, NULL, my_rpco_ht_saisie, my_rpco_tva_saisie, my_rpco_ttc_saisie, SYSDATE, Get_Tbo_Ordre(a_rec_id));

   	   		-- procedure de verification
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco(my_rpco_id);
 		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_rpcotva_id	           RECETTE_CTRL_PLANCO_TVA.rpcotva_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_TVA.pco_num%TYPE;
       my_rpcotva_tva_saisie	   RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_default_ges_code		   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_fac_tva_reste            FACTURE_CTRL_PLANCO.fpco_tva_reste%TYPE;
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco_tva)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcotva_tva_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpcotva_tva_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant TVA positif (ins_recette_ctrl_planco_tva)');
			END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_tva_seq.NEXTVAL INTO my_rpcotva_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_TVA VALUES (my_rpcotva_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcotva_tva_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_tva(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_tva(my_rpcotva_id);
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_rpcoctp_id	           RECETTE_CTRL_PLANCO_CTP.rpcoctp_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_CTP.pco_num%TYPE;
       my_rpcoctp_ttc_saisie	   RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;
	   my_default_ges_code		   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_par_value				   maracuja.parametre.par_value%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_tyap_id			   	   FACTURE.tyap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
       my_pvi_ctp                  maracuja.planco_visa.pvi_contrepartie_gestion%type;
       my_is_sacd                  integer;
       my_pco_num_185              maracuja.gestion_exercice.pco_num_185%type;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_ctp)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre, f.tyap_id
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre, my_tyap_id
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre <> a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco_ctp)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;
        my_is_sacd := 0;

        select count(*) into my_nb from maracuja.gestion_exercice where ges_code = my_default_ges_code and exe_ordre = a_exe_ordre;
        if my_nb = 1 then
           select pco_num_185 into my_pco_num_185 from maracuja.gestion_exercice where ges_code = my_default_ges_code and exe_ordre = a_exe_ordre;
           if my_pco_num_185 is not null then my_is_sacd := 1; end if;
        end if;

        my_chaine := a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcoctp_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpcoctp_ttc_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant contrepartie positif (ins_recette_ctrl_planco_ctp)');
			END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation contrepartie doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_ctp)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = '' THEN
				my_ges_code := my_default_ges_code;

                select count(*) into my_nb from maracuja.planco_visa where pco_num_ordonnateur = my_pco_num_pere and exe_ordre = my_exe_ordre;
                if my_nb = 1 and my_is_sacd <> 1 then
                   SELECT pvi_contrepartie_gestion INTO my_pvi_ctp FROM maracuja.planco_visa
                      WHERE pco_num_ordonnateur=my_pco_num_pere AND exe_ordre = my_exe_ordre;

         	       IF UPPER(my_pvi_ctp) <> 'COMPOSANTE' OR my_tyap_id = Type_Application.get_type_prestation_interne THEN
		              SELECT c.ges_code INTO my_ges_code FROM maracuja.GESTION g, maracuja.COMPTABILITE c, maracuja.GESTION_EXERCICE ge
			             WHERE g.ges_code = my_default_ges_code AND g.com_ordre = c.com_ordre AND g.ges_code=ge.ges_code
                         AND ge.exe_ordre=my_exe_ordre;
		           END IF;
                end if;
			ELSE
				-- s'il s'agit d'une SACD, on prend dans tous les cas l'UB.
				IF my_is_sacd = 1 THEN
					my_ges_code := my_default_ges_code;
				END IF;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_ctp_seq.NEXTVAL INTO my_rpcoctp_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_CTP VALUES (my_rpcoctp_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcoctp_ttc_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_ctp(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_ctp(my_rpcoctp_id);
		END LOOP;
   END;

PROCEDURE del_recette_ctrl_action (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_ACTION WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_analytique (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_convention (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_CONVENTION WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_planco (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_planco_tva (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
   END;

PROCEDURE del_recette_ctrl_planco_ctp (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
   END;

PROCEDURE upd_date_recette_restrinct (
      a_exe_ordre         RECETTE.exe_ordre%TYPE,
	  a_rec_id			  RECETTE.REC_ID%TYPE)
      IS
          cpt integer;
      BEGIN

      select count(*) into cpt from V_EXERCICE where V_EXERCICE.EXE_ORDRE = a_exe_ordre and V_EXERCICE.EXE_STAT_FAC = 'R';
      if(cpt=1)
      then
          update  RECETTE
          set REC_DATE_SAISIE =to_date('31/12/'||a_exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS')
          where REC_ID = a_rec_id;
      END IF;
      END;

END;

/

--------------------------------------------------------
--  Mise a jour DB VERSION.
--------------------------------------------------------
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.5.0.0',to_date('30/12/2012','dd/mm/yyyy'),'Gestion plan comptable par exercice.');


COMMIT;
