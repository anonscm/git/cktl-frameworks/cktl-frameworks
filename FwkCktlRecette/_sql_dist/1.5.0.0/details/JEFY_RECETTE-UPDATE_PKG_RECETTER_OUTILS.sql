--------------------------------------------------------
--  DDL for Package RECETTER_OUTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."RECETTER_OUTILS" IS

FUNCTION  get_fac_montant_budgetaire(
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_fac_tap_id	        FACTURE.tap_id%TYPE,
	  a_rec_tap_id	        RECETTE.tap_id%TYPE,
	  a_montant_budgetaire  RECETTE.rec_montant_budgetaire%TYPE,
	  a_org_id	            FACTURE.org_id%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE)
   RETURN FACTURE.fac_montant_budgetaire%TYPE;

FUNCTION  get_tva (
      a_rec_ht_saisie       RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie	    RECETTE.rec_ttc_saisie%TYPE)
   RETURN RECETTE.rec_tva_saisie%TYPE;

-- recupere le plan comptable tva en fonction du pco_num, dans planco_visa
FUNCTION get_pco_num_tva (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE)
   RETURN v_planco_visa.pco_num_tva%TYPE;

-- recupere le plan comptable tva en fonction du pco_num et du mor_ordre,
-- dans mode de recouvrement en priorite, sinon dans planco_visa
FUNCTION get_pco_num_ctp (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE,
	  a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN v_planco_visa.pco_num_ctrepartie%TYPE;

FUNCTION chaine_action_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_analytique_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_convention_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_tva_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_ctp_from_facture (a_fac_id FACTURE.fac_id%TYPE, a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN VARCHAR2;

FUNCTION get_recette_debiteur_adresse(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE) RETURN VARCHAR2;

END;

/

--------------------------------------------------------
--  DDL for Package Body RECETTER_OUTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."RECETTER_OUTILS"
IS

FUNCTION  get_fac_montant_budgetaire(
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_fac_tap_id	        FACTURE.tap_id%TYPE,
	  a_rec_tap_id	        RECETTE.tap_id%TYPE,
	  a_montant_budgetaire  RECETTE.rec_montant_budgetaire%TYPE,
	  a_org_id	            FACTURE.org_id%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE)
   RETURN FACTURE.fac_montant_budgetaire%TYPE
   IS
   	   my_par_value                PARAMETRES.par_value%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
   BEGIN
        my_par_value:=Get_Parametre(a_exe_ordre, 'RECETTE_IDEM_TAP_ID');
	    IF my_par_value <> 'NON' THEN
           IF a_rec_tap_id<>a_fac_tap_id THEN
	          RAISE_APPLICATION_ERROR(-20001,'La recette doit avoir le meme prorata que sa facture');
	       END IF;

	        RETURN a_montant_budgetaire;
	    ELSE
	        RETURN Budget.calculer_budgetaire(a_exe_ordre,a_fac_tap_id,a_org_id,
	          a_rec_ht_saisie,a_rec_ttc_saisie);
	    END IF;
   END;

FUNCTION get_tva (
      a_rec_ht_saisie       RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie	    RECETTE.rec_ttc_saisie%TYPE)
     RETURN RECETTE.rec_tva_saisie%TYPE
   IS
   BEGIN
   	    -- on teste si le ht et le ttc sont du meme signe.
		IF (a_rec_ht_saisie<0 AND a_rec_ttc_saisie>0) OR (a_rec_ht_saisie>0 AND a_rec_ttc_saisie<0) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le HT et le TTC doivent etre du meme signe');
		END IF;

		-- on teste que le ttc est superieur au ht (idem pour les reductions de recette avec la valeur absolue).
		IF ABS(a_rec_ht_saisie)>ABS(a_rec_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC');
		END IF;

		RETURN a_rec_ttc_saisie - a_rec_ht_saisie;

   END;

-- recupere le plan comptable tva en fonction du pco_num, dans planco_visa
FUNCTION get_pco_num_tva (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE)
   RETURN v_planco_visa.pco_num_tva%TYPE
   IS
     my_pco_num_tva   v_planco_visa.pco_num_tva%TYPE;
     my_nb			  INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_planco_visa
		  WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE' AND ROWNUM = 1;
		IF my_nb > 0
		THEN
		   SELECT pco_num_tva INTO my_pco_num_tva FROM v_planco_visa
		   		  WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE' AND ROWNUM = 1;
		ELSE
			my_pco_num_tva := NULL;
		END IF;
		RETURN my_pco_num_tva;
   END;

-- recupere le plan comptable ctp en fonction du pco_num et du mor_ordre,
-- dans mode de recouvrement en priorite, sinon dans planco_visa
FUNCTION get_pco_num_ctp (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE,
	  a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN v_planco_visa.pco_num_ctrepartie%TYPE
   IS
     my_pco_num_ctp   v_planco_visa.pco_num_ctrepartie%TYPE;
     my_nb			  INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre AND pco_num_visa is not null;
		IF my_nb > 0
		THEN
		    SELECT pco_num_visa INTO my_pco_num_ctp FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre;
		ELSE
   			SELECT COUNT(*) INTO my_nb FROM v_planco_visa
		  		WHERE pco_num_ordonnateur = a_pco_num AND exe_ordre = a_exe_ordre AND pvi_etat = 'VALIDE';
			IF my_nb > 0
			THEN
				SELECT pco_num_ctrepartie INTO my_pco_num_ctp FROM v_planco_visa
				   WHERE pco_num_ordonnateur = a_pco_num AND pvi_etat = 'VALIDE' AND exe_ordre = a_exe_ordre AND ROWNUM = 1;
			ELSE
				my_pco_num_ctp := NULL;
			END IF;
		END IF;
		RETURN my_pco_num_ctp;
   END;

FUNCTION chaine_action_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_ACTION WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_ACTION%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.lolf_id || '$' || my_ctrl.fact_ht_saisie || '$' || my_ctrl.fact_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_analytique_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_ANALYTIQUE%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.can_id || '$' || my_ctrl.fana_ht_saisie || '$' || my_ctrl.fana_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_convention_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_CONVENTION WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_CONVENTION%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.con_ordre || '$' || my_ctrl.fcon_ht_saisie || '$' || my_ctrl.fcon_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_ctrl.fpco_ht_saisie || '$' || my_ctrl.fpco_ttc_saisie || '$';

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_tva_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
	 my_pco_num_tva	 v_planco_visa.pco_num_tva%TYPE;
     my_chaine		 VARCHAR2(30000);
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 IF my_ctrl.fpco_tva_saisie <> 0 THEN
			my_pco_num_tva := get_pco_num_tva(my_ctrl.pco_num, my_ctrl.exe_ordre);

			IF my_pco_num_tva IS NULL THEN
			   RAISE_APPLICATION_ERROR(-20001,'INDIQUER UN PLAN COMPTABLE TVA (un plan comptable de la facture n''a pas de plan comptable TVA dans planco visa (pco_num='||my_ctrl.pco_num||'))');
			END IF;

		    my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_pco_num_tva || '$' || my_ctrl.fpco_tva_saisie || '$';
		 END IF;

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;

FUNCTION chaine_planco_ctp_from_facture (a_fac_id FACTURE.fac_id%TYPE, a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN VARCHAR2
   IS
     CURSOR liste IS SELECT * FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	 my_ctrl		 FACTURE_CTRL_PLANCO%ROWTYPE;
	 my_pco_num_ctp	 v_planco_visa.pco_num_ctrepartie%TYPE;
     my_chaine		 VARCHAR2(30000);
   	 my_nb			 INTEGER;
   BEGIN
      my_chaine := '';

      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_ctrl;
		 EXIT WHEN liste%NOTFOUND;

		 IF my_ctrl.fpco_ttc_saisie <> 0 THEN

		    my_pco_num_ctp := get_pco_num_ctp(my_ctrl.pco_num, my_ctrl.exe_ordre, a_mor_ordre);

		    IF my_pco_num_ctp IS NULL THEN
		       RAISE_APPLICATION_ERROR(-20001,'INDIQUER UN PLAN COMPTABLE CONTREPARTIE (un plan comptable de la facture n''a pas de plan comptable contrepartie dans planco visa (pco_num='||my_ctrl.pco_num||'))');
		    END IF;

		    my_chaine := my_chaine || my_ctrl.pco_num || '$' || my_pco_num_ctp || '$' || my_ctrl.fpco_ttc_saisie || '$';
		 END IF;

	  END LOOP;
	  CLOSE liste;

	  my_chaine := my_chaine || '$';
      RETURN my_chaine;
   END;


FUNCTION get_recette_debiteur_adresse(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
	RETURN VARCHAR2
	IS
	    my_adr_adr1      GRHUM.ADRESSE.ADR_ADRESSE1%TYPE;
	    my_adr_adr2      GRHUM.ADRESSE.ADR_ADRESSE2%TYPE;
	    my_adr_cp        GRHUM.ADRESSE.CODE_POSTAL%TYPE;
	    my_adr_ville     GRHUM.ADRESSE.VILLE%TYPE;
	    my_chaine		 VARCHAR2(500) := '';
		my_nb			 INTEGER;
	BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		-- on recupere la recette papier.
		IF my_nb = 1 THEN
			SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER_ADR_CLIENT WHERE rpp_id = a_rpp_id AND date_fin IS NULL;
			IF my_nb = 1 THEN
				-- on recupere l'adresse specifiee sur la recette papier
				SELECT ADR.ADR_ADRESSE1, ADR.ADR_ADRESSE2, ADR.CODE_POSTAL, ADR.VILLE
				INTO my_adr_adr1, my_adr_adr2, my_adr_cp, my_adr_ville
				FROM JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT RPP_ADR JOIN GRHUM.ADRESSE ADR ON RPP_ADR.adr_ordre = ADR.adr_ordre
				WHERE RPP_ADR.rpp_id = a_rpp_id AND RPP_ADR.date_fin IS NULL;
			ELSE
				-- pas d'adresse sur la recette papier, on utilise l'adresse du fournisseur
				SELECT ADR.ADR_ADRESSE1, ADR.ADR_ADRESSE2, ADR.ADR_CP, ADR.ADR_VILLE
				INTO my_adr_adr1, my_adr_adr2, my_adr_cp, my_adr_ville
				FROM JEFY_RECETTE.RECETTE_PAPIER RPP JOIN JEFY_RECETTE.V_FOURNIS_ULR ADR ON RPP.fou_ordre = ADR.fou_ordre
				WHERE RPP.rpp_id = a_rpp_id;
			END IF;
			-- construire l'adresse resultat.
			my_chaine := my_adr_adr1 ||' - '|| my_adr_adr2 ||' '|| my_adr_cp ||' '|| my_adr_ville;
		END IF;

		RETURN my_chaine;
	END;

END;

/

COMMIT;
