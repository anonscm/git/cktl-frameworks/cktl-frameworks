CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_COMMANDES_POUR_PI" ("COMM_ID", "EXE_ORDRE", "COMM_NUMERO", "COMM_REFERENCE", "COMM_LIBELLE", "COMM_DATE_CREATION", "UTL_ORDRE", "ENG_ID", "ENG_NUMERO", "FOU_ORDRE", "ORG_ID", "ENG_HT_SAISIE", "ENG_TVA_SAISIE", "ENG_TTC_SAISIE", "PCO_NUM")
AS
  SELECT c.comm_id,
    c.exe_ordre,
    c.comm_numero,
    c.comm_reference,
    c.comm_libelle,
    c.comm_date_creation,
    c.utl_ordre,
    eng.eng_id,
    eng.eng_numero,
    eng.fou_ordre,
    eng.org_id,
    eng.eng_ht_saisie,
    eng.eng_tva_saisie,
    eng.eng_ttc_saisie,
    epco.pco_num
  FROM jefy_depense.commande c,
    jefy_depense.commande_engagement ce,
    jefy_depense.engage_budget eng,
    jefy_depense.engage_ctrl_planco epco
  WHERE c.comm_id = ce.comm_id
  AND ce.eng_id   = eng.eng_id
  AND eng.eng_id  = epco.eng_id
  AND c.tyet_id   = jefy_depense.etats.get_etat_engagee
  AND c.comm_id  IN
    (SELECT comm_id
    FROM
      (SELECT comm.comm_id,
        COUNT(*)
      FROM jefy_depense.commande comm,
        jefy_depense.commande_engagement come
      WHERE comm.comm_id = come.comm_id
      GROUP BY comm.comm_id
      HAVING COUNT(*) = 1
      UNION
      SELECT come.comm_id,
        COUNT(*)
      FROM jefy_depense.commande_engagement come,
        jefy_depense.engage_ctrl_planco epco
      WHERE come.eng_id = epco.eng_id
      GROUP BY come.comm_id
      HAVING COUNT(*) = 1
      )
    )
  AND eng.TYAP_ID   IN (jefy_recette.type_application.get_type_depense, jefy_recette.type_application.get_type_prestation_interne)
  AND eng.fou_ordre IN
    (SELECT fou_ordre
    FROM jefy_recette.v_fournis_ulr
    WHERE pers_id IN
      (SELECT pers_id
      FROM grhum.REPART_STRUCTURE
      WHERE c_structure =
        (SELECT param_value
        FROM grhum.GRHUM_PARAMETRES
        WHERE param_key='ANNUAIRE_FOU_VALIDE_INTERNE'
        )
      )
    );