SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.7.0.0
-- Date de publication : 16/04/2012
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode;

-- Fonctions
CREATE OR REPLACE
FUNCTION GET_DATE_EMISSION (EXERCICE IN NUMBER, DATE_SAISIE IN DATE) RETURN DATE
IS
  anneeDateSaisie  INTEGER;
  dateEmission     DATE;
BEGIN
  dateEmission := DATE_SAISIE;
  SELECT EXTRACT(YEAR FROM DATE_SAISIE) INTO anneeDateSaisie FROM DUAL;
  IF anneeDateSaisie <> exercice THEN
    dateEmission := TO_DATE('31/12/' || exercice, 'DD/MM/YYYY');
  END IF;
  RETURN dateEmission;
END GET_DATE_EMISSION;
/

-- Mise a jour des vues
CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_COMMANDES_POUR_PI" ("COMM_ID", "EXE_ORDRE", "COMM_NUMERO", "COMM_REFERENCE", "COMM_LIBELLE", "COMM_DATE_CREATION", "UTL_ORDRE", "ENG_ID", "ENG_NUMERO", "FOU_ORDRE", "ORG_ID", "ENG_HT_SAISIE", "ENG_TVA_SAISIE", "ENG_TTC_SAISIE", "PCO_NUM")
AS
  SELECT c.comm_id,
    c.exe_ordre,
    c.comm_numero,
    c.comm_reference,
    c.comm_libelle,
    c.comm_date_creation,
    c.utl_ordre,
    eng.eng_id,
    eng.eng_numero,
    eng.fou_ordre,
    eng.org_id,
    eng.eng_ht_saisie,
    eng.eng_tva_saisie,
    eng.eng_ttc_saisie,
    epco.pco_num
  FROM jefy_depense.commande c
       JOIN jefy_depense.commande_engagement ce  ON c.comm_id = ce.comm_id
       JOIN jefy_depense.engage_budget eng       ON ce.eng_id   = eng.eng_id
       JOIN jefy_depense.engage_ctrl_planco epco ON eng.eng_id  = epco.eng_id
  WHERE c.tyet_id = jefy_depense.etats.get_etat_engagee
  AND EXISTS (
        SELECT subCommande.comm_id, COUNT(*)
        FROM jefy_depense.commande subCommande
             JOIN jefy_depense.commande_engagement subCmdEngagement   ON subCommande.comm_id = subCmdEngagement.comm_id
             JOIN jefy_depense.engage_ctrl_planco subEngagementPlanco ON subCmdEngagement.eng_id = subEngagementPlanco.eng_id
        WHERE c.comm_id = subCommande.comm_id
        GROUP BY subCommande.comm_id
        HAVING COUNT(*) = 1)
  AND eng.tyap_id = (SELECT tyap_id FROM jefy_recette.v_type_application WHERE tyap_strid = 'DEPENSE')
  AND EXISTS (
        SELECT 1
        FROM maracuja.v_fournis_light fournisseur
             JOIN grhum.repart_structure repartStructure ON fournisseur.pers_id = repartStructure.pers_id
        WHERE eng.fou_ordre = fournisseur.fou_ordre
        AND repartStructure.c_structure =
            	(SELECT param_value FROM grhum.grhum_parametres WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE'));
/

BEGIN
   JEFY_ADMIN.API_APPLICATION.creerFonction(317, 'PRGREPRO', 'Prestation', 'Droit de gérer les prestations de reprographie', 'Droit de gérer les prestations de reprographie', 'N', 'N', 6);
   COMMIT;
END;
/

-- DT#3328 (exemple : exclusion classe 1 dans son intégralité et du compte 43722 spécifiquement.
-- insert into jefy_recette.parametres (par_ordre, exe_ordre, par_key, par_value, par_description) values (jefy_recette.PARAMETRES_SEQ.nextVal, 2012, 'PCO_CTP_CLASSE_SAISIE_BLOQUEE', '1,43722', 'Les classes de comptes de contrepartie interdites a la saisie, séparées par des virgules');

-- Maj version
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.7.0.0',to_date('16/04/2012','dd/mm/yyyy'), 'Maj vue commandes, nouvelle fonction date_emission');
commit;
