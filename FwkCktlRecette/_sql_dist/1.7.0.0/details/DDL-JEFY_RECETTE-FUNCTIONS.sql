create or replace
FUNCTION GET_DATE_EMISSION (EXERCICE IN NUMBER, DATE_SAISIE IN DATE) RETURN DATE
IS
  anneeDateSaisie  INTEGER;
  dateEmission     DATE;
BEGIN
  dateEmission := DATE_SAISIE;
  SELECT EXTRACT(YEAR FROM DATE_SAISIE) INTO anneeDateSaisie FROM DUAL;
  IF anneeDateSaisie <> exercice THEN
    dateEmission := TO_DATE('31/12/' || exercice, 'DD/MM/YYYY');
  END IF;
  RETURN dateEmission;
END GET_DATE_EMISSION;
/
