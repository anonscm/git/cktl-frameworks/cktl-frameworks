-- DT#3328 (exemple : exclusion classe 1 dans son intégralité et du compte 43722 spécifiquement.
-- insert into jefy_recette.parametres (par_ordre, exe_ordre, par_key, par_value, par_description) values (jefy_recette.PARAMETRES_SEQ.nextVal, 2012, 'PCO_CTP_CLASSE_SAISIE_BLOQUEE', '1,43722', 'Les classes de comptes de contrepartie interdites a la saisie, séparées par des virgules');

-- Maj version
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.7.0.0',to_date('16/04/2012','dd/mm/yyyy'), 'Maj vue commandes, nouvelle fonction date_emission');

commit;