CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_COMMANDES_POUR_PI" ("COMM_ID", "EXE_ORDRE", "COMM_NUMERO", "COMM_REFERENCE", "COMM_LIBELLE", "COMM_DATE_CREATION", "UTL_ORDRE", "ENG_ID", "ENG_NUMERO", "FOU_ORDRE", "ORG_ID", "ENG_HT_SAISIE", "ENG_TVA_SAISIE", "ENG_TTC_SAISIE", "PCO_NUM")
AS
  SELECT c.comm_id,
    c.exe_ordre,
    c.comm_numero,
    c.comm_reference,
    c.comm_libelle,
    c.comm_date_creation,
    c.utl_ordre,
    eng.eng_id,
    eng.eng_numero,
    eng.fou_ordre,
    eng.org_id,
    eng.eng_ht_saisie,
    eng.eng_tva_saisie,
    eng.eng_ttc_saisie,
    epco.pco_num
  FROM jefy_depense.commande c
       JOIN jefy_depense.commande_engagement ce  ON c.comm_id = ce.comm_id
       JOIN jefy_depense.engage_budget eng       ON ce.eng_id   = eng.eng_id
       JOIN jefy_depense.engage_ctrl_planco epco ON eng.eng_id  = epco.eng_id
  WHERE c.tyet_id = jefy_depense.etats.get_etat_engagee
  AND EXISTS (
        SELECT subCommande.comm_id, COUNT(*)
        FROM jefy_depense.commande subCommande
             JOIN jefy_depense.commande_engagement subCmdEngagement   ON subCommande.comm_id = subCmdEngagement.comm_id
             JOIN jefy_depense.engage_ctrl_planco subEngagementPlanco ON subCmdEngagement.eng_id = subEngagementPlanco.eng_id
        WHERE c.comm_id = subCommande.comm_id
        GROUP BY subCommande.comm_id
        HAVING COUNT(*) = 1)
  AND eng.tyap_id = (SELECT tyap_id FROM jefy_recette.v_type_application WHERE tyap_strid = 'DEPENSE')
  AND EXISTS (
        SELECT 1
        FROM maracuja.v_fournis_light fournisseur
             JOIN grhum.repart_structure repartStructure ON fournisseur.pers_id = repartStructure.pers_id
        WHERE eng.fou_ordre = fournisseur.fou_ordre
        AND repartStructure.c_structure =
            	(SELECT param_value FROM grhum.grhum_parametres WHERE param_key = 'ANNUAIRE_FOU_VALIDE_INTERNE'));
/
