CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_CTP_REAICTP" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER;


CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_TVA" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER WHERE pco_num LIKE '445%' ;


CREATE OR REPLACE FORCE VIEW "JEFY_RECETTE"."V_PLAN_COMPTABLE_TVA_REAITVA" ("PCO_NUM", "PCO_VALIDITE", "EXE_ORDRE")
AS
  SELECT pco_num, pco_validite, exe_ordre FROM maracuja.PLAN_COMPTABLE_EXER ;


create or replace force view jefy_recette.v_planco_credit_rec (pcc_ordre, tcd_ordre, pco_num, pcc_etat, pco_validite)
as
   select v.pcc_ordre,
          v.tcd_ordre,
          v.pco_num,
          v.pcc_etat,
          p.pco_validite
   from   maracuja.v_planco_credit_rec v, jefy_admin.type_credit tc, maracuja.plan_comptable_exer p
   where  v.pco_num = p.pco_num and v.tcd_ordre = tc.tcd_ordre and p.exe_ordre = tc.exe_ordre and v.pco_num not like '18%' and (v.pco_num like '1%' or v.pco_num like '7%');

create or replace force view jefy_recette.v_planco_credit_dep (pcc_ordre, tcd_ordre, pco_num, pcc_etat, pco_validite)
as
   select v.pcc_ordre,
          v.tcd_ordre,
          v.pco_num,
          v.pcc_etat,
          p.pco_validite
   from   maracuja.v_planco_credit_dep v, jefy_admin.type_credit tc, maracuja.plan_comptable_exer p
   where  v.pco_num = p.pco_num and v.tcd_ordre = tc.tcd_ordre and p.exe_ordre = tc.exe_ordre and p.pco_num not like '18%';

COMMIT;
