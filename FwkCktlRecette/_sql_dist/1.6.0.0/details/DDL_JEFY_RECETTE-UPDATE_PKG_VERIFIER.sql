--------------------------------------------------------
--  DDL for Package VERIFIER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."VERIFIER" IS

PROCEDURE verifier_droit_facture (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_utl_ordre      			FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_droit_recette (
      a_exe_ordre               RECETTE.exe_ordre%TYPE,
   	  a_utl_ordre      			RECETTE.utl_ordre%TYPE);


PROCEDURE verifier_budget (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_tap_id                  FACTURE.tap_id%TYPE,
	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE);


PROCEDURE verifier_action(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_lolf_id                 FACTURE_CTRL_ACTION.lolf_id%TYPE);

PROCEDURE verifier_analytique(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_can_id                  FACTURE_CTRL_ANALYTIQUE.can_id%TYPE);

PROCEDURE verifier_convention(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_con_ordre                 FACTURE_CTRL_CONVENTION.con_ordre%TYPE);

PROCEDURE verifier_planco(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_tva(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_ctp(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		FACTURE.fou_ordre%TYPE);

PROCEDURE verifier_personne (
   	  a_pers_id		FACTURE.pers_id%TYPE);

-- ne sert pas pour le moment --
PROCEDURE verifier_monnaie;

PROCEDURE verifier_organ(
   	  a_org_id			FACTURE.org_id%TYPE,
	  a_tcd_ordre		FACTURE.tcd_ordre%TYPE);

PROCEDURE verifier_rib (
   	  a_fou_ordre		v_ribfour_ulr.fou_ordre%TYPE,
   	  a_rib_ordre		v_ribfour_ulr.rib_ordre%TYPE,
   	  a_mor_ordre		RECETTE_PAPIER.mor_ordre%TYPE,
   	  a_exe_ordre		RECETTE_PAPIER.exe_ordre%TYPE);

PROCEDURE verifier_facture_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_recette_pap_coherence(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_recette_coherence(a_rec_id RECETTE.rec_id%TYPE);

PROCEDURE verifier_fac_rec_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_utilisation_facture (a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_util_recette_papier (a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_utilisation_recette (a_rec_id RECETTE.rec_id%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body VERIFIER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."VERIFIER"
IS

PROCEDURE verifier_droit_facture (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_utl_ordre      			FACTURE.utl_ordre%TYPE
   ) IS
       my_nb          INTEGER;
	   my_droit		  INTEGER;
	   my_fon_ordre   jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
		-- verifier que l'on peux facturer sur cet exercice.
		my_droit:=0;

		-- on verifie si l'utilisateur a le droit de facturer hors periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REFAC');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REFAC pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='O';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- on verifie si l'utilisateur a le droit de facturer pendant la periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REFACINV');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REFACINV pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='R';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- si droit=0 alors pas de droit trouve.
		IF my_droit=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de facturer pour cet exercice et cet utilisateur (exercice:'||a_exe_ordre||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

PROCEDURE verifier_droit_recette (
      a_exe_ordre               RECETTE.exe_ordre%TYPE,
   	  a_utl_ordre      			RECETTE.utl_ordre%TYPE
   ) IS
       my_nb          INTEGER;
	   my_droit		  INTEGER;
	   my_fon_ordre   jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
		-- verifier que l'on peux recetter sur cet exercice.
		my_droit:=0;

		-- on verifie si l'utilisateur a le droit de recetter hors periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REREC');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REREC pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='O';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- on verifie si l'utilisateur a le droit de recetter pendant la periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('RERECINV');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction RERECINV pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='R';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- si droit=0 alors pas de droit trouver.
		IF my_droit=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de recetter pour cet exercice et cet utilisateur (exercice:'||a_exe_ordre||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;


   PROCEDURE verifier_budget (
       a_exe_ordre    FACTURE.exe_ordre%TYPE,
   	   a_tap_id       FACTURE.tap_id%TYPE,
	   a_org_id       FACTURE.org_id%TYPE,
	   a_tcd_ordre    FACTURE.tcd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
		-- DT3281 fonction obsolete ; sera supprimée ainsi que ses references dans la prochaine version
	   my_nb := 1;
		-- SELECT COUNT(*) INTO my_nb FROM jefy_admin.organ_prorata WHERE tap_id=a_tap_id AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
		-- IF my_nb<>1 THEN
		--	RAISE_APPLICATION_ERROR(-20001, 'Ce taux de prorata n''est pas autorise pour cette ligne budgetaire et cet exercice (org_id:'||a_org_id||', exercice:'||a_exe_ordre||', prorata:'||a_tap_id||')');
		-- END IF;
   END;

   PROCEDURE verifier_action (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_lolf_id                 FACTURE_CTRL_ACTION.lolf_id%TYPE
   ) IS
      my_nb           INTEGER;
      my_par_value    PARAMETRES.par_value%TYPE;
   BEGIN
		-- si parametre tester suivant les destinations du budget de gestion.
		my_par_value:=Get_Parametre(a_exe_ordre, 'CTRL_ORGAN_DEST');
		IF my_par_value = 'OUI' THEN
           SELECT COUNT(*) INTO my_nb FROM v_organ_action_rec WHERE lolf_id=a_lolf_id AND tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorisee pour cette ligne budgetaire et ce type de credit (action:'||a_lolf_id||', organ:'||a_org_id||', typecredit:'||a_tcd_ordre||')');
           END IF;
		END IF;
   END;

   PROCEDURE verifier_analytique (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_can_id                  FACTURE_CTRL_ANALYTIQUE.can_id%TYPE
   ) IS
       my_nb      INTEGER;
  BEGIN
		-- verifier si ce code analytique est valide et utilisable.
		SELECT COUNT(*) INTO my_nb FROM jefy_admin.v_code_analytique WHERE can_id=a_can_id
		AND tyet_id = Type_Etat.get_etat_canal_valide AND can_utilisable = Type_Etat.get_etat_canal_utilisable AND exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas valide ou utilisable (code analytique:'||a_can_id||')');
		END IF;

		-- verifier si public
		SELECT COUNT(*) INTO my_nb FROM jefy_admin.v_code_analytique WHERE can_id=a_can_id
		AND can_public = Type_Etat.get_etat_canal_public AND exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
			-- si pas public, verifier si droit utilisation code analytique (suivant organ).
			SELECT COUNT(*) INTO my_nb FROM v_code_analytique_organ WHERE can_id=a_can_id AND org_id=a_org_id;
			IF my_nb=0 THEN
		   	    RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique ne peut etre utilise avec cette ligne budgetaire (code analytique:'||a_can_id||', organ:'||a_org_id||')');
			END IF;
		END IF;
   END;

   PROCEDURE verifier_convention (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_con_ordre               FACTURE_CTRL_CONVENTION.con_ordre%TYPE
   ) IS
       my_nb      INTEGER;
  BEGIN
  	   my_nb := 1;
  END;

   PROCEDURE verifier_planco (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    v_plan_comptable.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite <> 'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- verifier si correct avec le type de credit.
		SELECT COUNT(*) INTO my_nb FROM v_planco_credit
		   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE';
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas associe a ce type de credit (pco_num:'||a_pco_num||', type de credit:'||a_tcd_ordre||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAUTIMP', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_planco_credit_rec_reautimp
			   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_planco_credit_rec
			   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_planco_tva (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    maracuja.plan_comptable_exer.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation tva n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite<>'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation tva n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes TVA autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAITVA', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_tva_reaitva
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_tva
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation TVA (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_planco_ctp (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    maracuja.plan_comptable_exer.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation contrepartie n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite<>'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation contrepartie n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes CTP autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAICTP', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_ctp_reaictp
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_ctp
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation contrepartie (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_monnaie IS
       i INTEGER;
   BEGIN
        -- a completer --
		i:=1;
   END;

   PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		FACTURE.fou_ordre%TYPE
   ) IS
   	   my_nb			INTEGER;
       my_fou_valide    v_fournis_ulr.fou_valide%TYPE;
   BEGIN
   		IF a_fou_ordre IS NOT NULL THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_fournis_ulr WHERE fou_ordre = a_fou_ordre;
		   IF my_nb <> 1 THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''existe pas (fou_ordre:'||a_fou_ordre||')');
		   END IF;

		   SELECT fou_valide INTO my_fou_valide FROM v_fournis_ulr WHERE fou_ordre = a_fou_ordre;
		   IF my_fou_valide <> 'O' THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas valide (fou_ordre:'||a_fou_ordre||')');
		   END IF;
		END IF;

   END;

   PROCEDURE verifier_personne (
   	  a_pers_id		FACTURE.pers_id%TYPE
   ) IS
   	   my_nb			INTEGER;
   BEGIN
   		IF a_pers_id IS NOT NULL THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_personne WHERE pers_id = a_pers_id;
		   IF my_nb <> 1 THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'La personne n''existe pas (pers_id:'||a_pers_id||')');
		   END IF;
		END IF;
   END;

   PROCEDURE verifier_rib (
   	  a_fou_ordre		v_ribfour_ulr.fou_ordre%TYPE,
   	  a_rib_ordre		v_ribfour_ulr.rib_ordre%TYPE,
   	  a_mor_ordre		RECETTE_PAPIER.mor_ordre%TYPE,
   	  a_exe_ordre		RECETTE_PAPIER.exe_ordre%TYPE
   ) IS
   	   my_nb       		INTEGER;
	   my_rib_valide	v_ribfour_ulr.rib_valide%TYPE;
	   my_mod_code		v_ribfour_ulr.mod_code%TYPE;
	   my_mod_dom		v_mode_paiement.mod_dom%TYPE;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre AND exe_ordre = a_exe_ordre;
	    IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le mode de recouvrement n''existe pas pour cet exercice  (exercice:'||a_exe_ordre||', moderec :'||a_mor_ordre||')');
		END IF;

 	    SELECT mod_code, mod_dom INTO my_mod_code, my_mod_dom FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre;

        IF a_rib_ordre IS NOT NULL
		THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_ribfour_ulr
		     WHERE fou_ordre = a_fou_ordre AND rib_ordre = a_rib_ordre;

		   IF my_nb<>1 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Le rib n''existe pas pour ce fournisseur (fournisseur:'||a_fou_ordre||', rib:'||a_rib_ordre||')');
		   END IF;

		   SELECT rib_valide INTO my_rib_valide FROM v_ribfour_ulr WHERE rib_ordre = a_rib_ordre;
		   IF my_rib_valide <> 'O' THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Le rib n''est pas valide (rib:'||a_rib_ordre||')');
		   END IF;
		ELSE
		   IF my_mod_dom = 'ECHEANCIER' THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de recouvrement il faut un rib (moderec :'||a_mor_ordre||')');
		   END IF;
		END IF;
   END;

   PROCEDURE verifier_recette_pap_coherence(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
   IS
		my_rpp_ht_saisie				RECETTE_PAPIER.rpp_ht_saisie%TYPE;
		my_rpp_tva_saisie				RECETTE_PAPIER.rpp_tva_saisie%TYPE;
		my_rpp_ttc_saisie				RECETTE_PAPIER.rpp_ttc_saisie%TYPE;

		my_ht_saisie					RECETTE.rec_ht_saisie%TYPE;
		my_tva_saisie					RECETTE.rec_tva_saisie%TYPE;
		my_ttc_saisie					RECETTE.rec_ttc_saisie%TYPE;
   BEGIN
   		-- recupere les montants de recette_papier.
		SELECT rpp_ht_saisie, rpp_tva_saisie, rpp_ttc_saisie
		  INTO my_rpp_ht_saisie, my_rpp_tva_saisie, my_rpp_ttc_saisie
		  FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;

		-- recupere et compare les montants de recette.
		SELECT NVL(SUM(rec_ht_saisie),0), NVL(SUM(rec_tva_saisie),0), NVL(SUM(rec_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE WHERE rpp_id = a_rpp_id;

		IF my_rpp_ht_saisie < my_ht_saisie OR my_rpp_tva_saisie < my_tva_saisie OR
		   my_rpp_ttc_saisie < my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles entre la recette papier et les recettes associees (recette papier:'||a_rpp_id||')');
		END IF;
   END;

   PROCEDURE verifier_facture_coherence (a_fac_id FACTURE.fac_id%TYPE) IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

		my_montant_budgetaire			FACTURE.fac_montant_budgetaire%TYPE;
		my_montant_budgetaire_reste		FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_ht_saisie					FACTURE.fac_ht_saisie%TYPE;
		my_tva_saisie					FACTURE.fac_tva_saisie%TYPE;
		my_ttc_saisie					FACTURE.fac_ttc_saisie%TYPE;
   BEGIN
   		-- recupere les montants de facture.
		SELECT fac_montant_budgetaire, fac_montant_budgetaire_reste,
		       fac_ht_saisie, fac_tva_saisie, fac_ttc_saisie
		  INTO my_fac_montant_budgetaire, my_fac_montant_budgetaire_rest,
		       my_fac_ht_saisie, my_fac_tva_saisie, my_fac_ttc_saisie
		  FROM FACTURE WHERE fac_id=a_fac_id;

		-- recupere et compare les montants de facture_ctrl_action.
		SELECT NVL(SUM(fact_montant_budgetaire),0), NVL(SUM(fact_montant_budgetaire_reste),0),
		       NVL(SUM(fact_ht_saisie),0), NVL(SUM(fact_tva_saisie),0), NVL(SUM(fact_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_ACTION WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<>my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<>my_ht_saisie OR my_fac_tva_saisie<>my_tva_saisie OR
		   my_fac_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants actions incoherentes (facture_ctrl_action - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_analytique.

		-- on ne verifie pas la coherence exacte de facture_ctrl_analytique.
		--   puisque les codes analytiques sont facultatifs .
		-- on verifie juste qu'ils ne depassent pas le montant de la facture.

		SELECT NVL(SUM(fana_montant_budgetaire),0), NVL(SUM(fana_montant_budgetaire_reste),0),
		       NVL(SUM(fana_ht_saisie),0), NVL(SUM(fana_tva_saisie),0), NVL(SUM(fana_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<my_ht_saisie OR my_fac_tva_saisie<my_tva_saisie OR
		   my_fac_ttc_saisie<my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants code analytique incoherentes (facture_ctrl_analytique - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_convention.

		-- on ne verifie pas la coherence exacte de facture_ctrl_convention.
		--   puisque les conventions sont facultatives .
		-- on verifie juste qu'ils ne depassent pas le montant de la facture.

		SELECT NVL(SUM(fcon_montant_budgetaire),0), NVL(SUM(fcon_montant_budgetaire_reste),0),
		       NVL(SUM(fcon_ht_saisie),0), NVL(SUM(fcon_tva_saisie),0), NVL(SUM(fcon_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_CONVENTION WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<my_ht_saisie OR my_fac_tva_saisie<my_tva_saisie OR
		   my_fac_ttc_saisie<my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants convention incoherentes (facture_ctrl_convention - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_planco.
		SELECT NVL(SUM(fpco_ht_saisie),0), NVL(SUM(fpco_tva_saisie),0), NVL(SUM(fpco_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id;

		IF my_fac_ht_saisie<>my_ht_saisie OR my_fac_tva_saisie<>my_tva_saisie OR
		   my_fac_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable incoherentes (facture_ctrl_planco - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

   END;

   PROCEDURE verifier_recette_coherence(a_rec_id RECETTE.rec_id%TYPE)
   IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest  FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

		my_fac_id						RECETTE.fac_id%TYPE;
   		my_rec_montant_budgetaire       RECETTE.rec_montant_budgetaire%TYPE;
		my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
		my_rec_tva_saisie				RECETTE.rec_tva_saisie%TYPE;
		my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;

		my_montant_budgetaire			RECETTE.rec_montant_budgetaire%TYPE;
		my_ht_saisie					RECETTE.rec_ht_saisie%TYPE;
		my_tva_saisie					RECETTE.rec_tva_saisie%TYPE;
		my_ttc_saisie					RECETTE.rec_ttc_saisie%TYPE;

		my_rpco							RECETTE_CTRL_PLANCO%ROWTYPE;
		my_rpcotva_tva_saisie			RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
		my_rpcoctp_ttc_saisie			RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;

		my_nb							INTEGER;

      	CURSOR liste IS SELECT * FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
   BEGIN
   		-- recupere les montants de recette.
		SELECT rec_montant_budgetaire, rec_ht_saisie, rec_tva_saisie, rec_ttc_saisie, fac_id
		  INTO my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_tva_saisie, my_rec_ttc_saisie, my_fac_id
		  FROM RECETTE WHERE rec_id=a_rec_id;

		-- recupere et compare les montants de recette_ctrl_action.
		SELECT NVL(SUM(ract_montant_budgetaire),0), NVL(SUM(ract_ht_saisie),0), NVL(SUM(ract_tva_saisie),0),
		   NVL(SUM(ract_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_ACTION WHERE rec_id=a_rec_id;

		IF my_rec_montant_budgetaire<>my_montant_budgetaire OR
		   my_rec_ht_saisie<>my_ht_saisie OR my_rec_tva_saisie<>my_tva_saisie OR
		   my_rec_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants actions incoherentes (recette_ctrl_action - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_analytique.

		-- on ne verifie pas la coherence exacte de recette_ctrl_analytique.
		--   puisque les codes analytiques sont facultatifs .
		-- on verifie juste qu'ils ne depassent pas le montant de la recette.

	    SELECT NVL(SUM(rana_montant_budgetaire),0), NVL(SUM(rana_ht_saisie),0), NVL(SUM(rana_tva_saisie),0),
		  NVL(SUM(rana_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;

		IF ABS(my_rec_montant_budgetaire) < ABS(my_montant_budgetaire) OR
		   ABS(my_rec_ht_saisie) < ABS(my_ht_saisie) OR ABS(my_rec_tva_saisie) < ABS(my_tva_saisie) OR
		   ABS(my_rec_ttc_saisie) < ABS(my_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants codes analytiques incoherentes (recette_ctrl_analytique - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_convention.

		-- on ne verifie pas la coherence exacte de recette_ctrl_convention.
		--   puisque les conventions sont facultatives .
		-- on verifie juste qu'ils ne depassent pas le montant de la recette.

	    SELECT NVL(SUM(rcon_montant_budgetaire),0), NVL(SUM(rcon_ht_saisie),0), NVL(SUM(rcon_tva_saisie),0),
		  NVL(SUM(rcon_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;

		IF ABS(my_rec_montant_budgetaire) < ABS(my_montant_budgetaire) OR
		   ABS(my_rec_ht_saisie) < ABS(my_ht_saisie) OR ABS(my_rec_tva_saisie) < ABS(my_tva_saisie) OR
		   ABS(my_rec_ttc_saisie) < ABS(my_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants convention incoherentes (recette_ctrl_convention - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_planco.
		SELECT NVL(SUM(rpco_ht_saisie),0), NVL(SUM(rpco_tva_saisie),0),
		   NVL(SUM(rpco_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;

		IF my_rec_ht_saisie<>my_ht_saisie OR my_rec_tva_saisie<>my_tva_saisie OR
		   my_rec_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable incoherentes (recette_ctrl_planco - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

      	OPEN liste();
  	  	LOOP
			FETCH liste INTO my_rpco;
		 	EXIT WHEN liste%NOTFOUND;

			-- recupere et compare les montants de recette_ctrl_planco_tva.
			SELECT NVL(SUM(rpcotva_tva_saisie),0)
		  		   INTO my_rpcotva_tva_saisie
		  		   FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id = my_rpco.rpco_id;

			IF my_rpco.rpco_tva_saisie <> my_rpcotva_tva_saisie THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable TVA incoherentes (entre recette_ctrl_planco et recette_ctrl_planco_tva - recette:'||a_rec_id||') (verifier_recette_coherence)');
			END IF;

			-- recupere et compare les montants de recette_ctrl_planco_ctp.
			-- s'il y en a, parce que on peut autoriser a saisir une recette sans contrepartie
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id = my_rpco.rpco_id;
			IF my_nb > 0 THEN
			  SELECT NVL(SUM(rpcoctp_ttc_saisie),0)
		  		   INTO my_rpcoctp_ttc_saisie
		  		   FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id = my_rpco.rpco_id;

			  IF my_rpco.rpco_ttc_saisie <> my_rpcoctp_ttc_saisie THEN
		   	     RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable contrepartie incoherentes (entre recette_ctrl_planco et recette_ctrl_planco_ctp - recette:'||a_rec_id||') (verifier_recette_coherence)');
			  END IF;
			END IF;

		END LOOP;
	  	CLOSE liste;

		-- TODO
		-- verifier que le total des reductions ne depasse pas le total des recettes correspondantes
		-- (plusieurs reductions eventuellement pour une recette)

	END;

   PROCEDURE verifier_fac_rec_coherence(a_fac_id FACTURE.fac_id%TYPE)
   IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest  FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

   		my_rec_montant_budgetaire       RECETTE.rec_montant_budgetaire%TYPE;
		my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
		my_rec_tva_saisie				RECETTE.rec_tva_saisie%TYPE;
		my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;

	  	my_nb							INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb=0
		THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (verifier_fac_rec_coherence)');
		END IF;

		-- recupere les montants de facture
		SELECT fac_montant_budgetaire, fac_montant_budgetaire_reste,
		       fac_ht_saisie, fac_tva_saisie, fac_ttc_saisie
		  INTO my_fac_montant_budgetaire, my_fac_montant_budgetaire_rest,
		       my_fac_ht_saisie, my_fac_tva_saisie, my_fac_ttc_saisie
		  FROM FACTURE WHERE fac_id = a_fac_id;

		-- les montants des recettes
		SELECT NVL(SUM(rec_montant_budgetaire),0), NVL(SUM(rec_ht_saisie),0), NVL(SUM(rec_tva_saisie),0),
		   NVL(SUM(rec_ttc_saisie),0)
		  INTO my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_tva_saisie, my_rec_ttc_saisie
		  FROM RECETTE WHERE fac_id=a_fac_id;

		-- verif...
		IF my_fac_ht_saisie < my_rec_ht_saisie OR my_fac_ttc_saisie < my_rec_ttc_saisie OR my_fac_tva_saisie < my_rec_tva_saisie
		THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le montant total facture est inferieur au montant recette (facture:'||a_fac_id||') (verifier_fac_rec_coherence)');
		END IF;

   END;

   PROCEDURE verifier_organ (
   	 a_org_id	   FACTURE.org_id%TYPE,
	 a_tcd_ordre   FACTURE.tcd_ordre%TYPE
   ) IS
       my_org_niv		v_organ.org_niv%TYPE;
	   my_nb			INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_organ WHERE org_id = a_org_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La ligne budgetaire n''existe pas (org_id:'||a_org_id||')');
		END IF;

   		SELECT org_niv INTO my_org_niv FROM v_organ WHERE org_id = a_org_id;

		IF my_org_niv < 3 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il faut facturer sur un CR ou une convention (org_id:'||a_org_id||')');
		END IF;
   END;

   PROCEDURE verifier_utilisation_facture (
      a_fac_id              FACTURE.fac_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE fac_id=a_fac_id AND tyet_id=1;
		IF my_nb>0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des recettes sur cette facture (fac_id:'||a_fac_id||')');
		END IF;
   END;

   PROCEDURE verifier_util_recette_papier (
      a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette papier n''existe pas (rpp_id:'||a_rpp_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rpp_id=a_rpp_id;
		IF my_nb > 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des recettes sur cette recette papier (rpp_id:'||a_rpp_id||')');
		END IF;
   END;


   PROCEDURE verifier_utilisation_recette (
      a_rec_id              RECETTE.rec_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette n''existe pas (rec_id:'||a_rec_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id AND tit_id IS NOT NULL;
		IF my_nb > 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette est deja sur un titre de recette (rec_id:'||a_rec_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id_reduction = a_rec_id;
		IF my_nb>0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des reductions sur cette recette (rec_id='||a_rec_id||')');
		END IF;
   END;

END;

/

