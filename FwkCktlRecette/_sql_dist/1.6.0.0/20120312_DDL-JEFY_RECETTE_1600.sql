--------------------------------------------------------
--  DDL for Package VERIFIER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."VERIFIER" IS

PROCEDURE verifier_droit_facture (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_utl_ordre      			FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_droit_recette (
      a_exe_ordre               RECETTE.exe_ordre%TYPE,
   	  a_utl_ordre      			RECETTE.utl_ordre%TYPE);


PROCEDURE verifier_budget (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_tap_id                  FACTURE.tap_id%TYPE,
	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE);


PROCEDURE verifier_action(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_lolf_id                 FACTURE_CTRL_ACTION.lolf_id%TYPE);

PROCEDURE verifier_analytique(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_can_id                  FACTURE_CTRL_ANALYTIQUE.can_id%TYPE);

PROCEDURE verifier_convention(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_con_ordre                 FACTURE_CTRL_CONVENTION.con_ordre%TYPE);

PROCEDURE verifier_planco(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_tva(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_ctp(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		FACTURE.fou_ordre%TYPE);

PROCEDURE verifier_personne (
   	  a_pers_id		FACTURE.pers_id%TYPE);

-- ne sert pas pour le moment --
PROCEDURE verifier_monnaie;

PROCEDURE verifier_organ(
   	  a_org_id			FACTURE.org_id%TYPE,
	  a_tcd_ordre		FACTURE.tcd_ordre%TYPE);

PROCEDURE verifier_rib (
   	  a_fou_ordre		v_ribfour_ulr.fou_ordre%TYPE,
   	  a_rib_ordre		v_ribfour_ulr.rib_ordre%TYPE,
   	  a_mor_ordre		RECETTE_PAPIER.mor_ordre%TYPE,
   	  a_exe_ordre		RECETTE_PAPIER.exe_ordre%TYPE);

PROCEDURE verifier_facture_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_recette_pap_coherence(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_recette_coherence(a_rec_id RECETTE.rec_id%TYPE);

PROCEDURE verifier_fac_rec_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_utilisation_facture (a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_util_recette_papier (a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_utilisation_recette (a_rec_id RECETTE.rec_id%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body VERIFIER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."VERIFIER"
IS

PROCEDURE verifier_droit_facture (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_utl_ordre      			FACTURE.utl_ordre%TYPE
   ) IS
       my_nb          INTEGER;
	   my_droit		  INTEGER;
	   my_fon_ordre   jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
		-- verifier que l'on peux facturer sur cet exercice.
		my_droit:=0;

		-- on verifie si l'utilisateur a le droit de facturer hors periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REFAC');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REFAC pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='O';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- on verifie si l'utilisateur a le droit de facturer pendant la periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REFACINV');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REFACINV pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='R';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- si droit=0 alors pas de droit trouve.
		IF my_droit=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de facturer pour cet exercice et cet utilisateur (exercice:'||a_exe_ordre||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

PROCEDURE verifier_droit_recette (
      a_exe_ordre               RECETTE.exe_ordre%TYPE,
   	  a_utl_ordre      			RECETTE.utl_ordre%TYPE
   ) IS
       my_nb          INTEGER;
	   my_droit		  INTEGER;
	   my_fon_ordre   jefy_admin.fonction.fon_ordre%TYPE;
   BEGIN
		-- verifier que l'on peux recetter sur cet exercice.
		my_droit:=0;

		-- on verifie si l'utilisateur a le droit de recetter hors periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('REREC');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction REREC pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='O';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- on verifie si l'utilisateur a le droit de recetter pendant la periode d'inventaire.
   		my_fon_ordre:=Get_Fonction('RERECINV');
		IF my_fon_ordre IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La fonction RERECINV pour le type d''application RECETTE n''existe pas.');
		END IF;

		SELECT COUNT(*) INTO my_nb
		  FROM jefy_admin.utilisateur_fonct uf, jefy_admin.utilisateur_fonct_exercice ufe, v_exercice e
		 WHERE ufe.uf_ordre=uf.uf_ordre AND ufe.exe_ordre=a_exe_ordre AND uf.utl_ordre=a_utl_ordre AND
		       uf.fon_ordre=my_fon_ordre AND ufe.exe_ordre=e.exe_ordre AND exe_stat_fac='R';

		IF my_nb>0 THEN my_droit:=1; END IF;

		-- si droit=0 alors pas de droit trouver.
		IF my_droit=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pas le droit de recetter pour cet exercice et cet utilisateur (exercice:'||a_exe_ordre||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;


   PROCEDURE verifier_budget (
       a_exe_ordre    FACTURE.exe_ordre%TYPE,
   	   a_tap_id       FACTURE.tap_id%TYPE,
	   a_org_id       FACTURE.org_id%TYPE,
	   a_tcd_ordre    FACTURE.tcd_ordre%TYPE
   ) IS
       my_nb INTEGER;
   BEGIN
		-- DT3281 fonction obsolete ; sera supprimée ainsi que ses references dans la prochaine version
	   my_nb := 1;
		-- SELECT COUNT(*) INTO my_nb FROM jefy_admin.organ_prorata WHERE tap_id=a_tap_id AND exe_ordre=a_exe_ordre AND org_id=a_org_id;
		-- IF my_nb<>1 THEN
		--	RAISE_APPLICATION_ERROR(-20001, 'Ce taux de prorata n''est pas autorise pour cette ligne budgetaire et cet exercice (org_id:'||a_org_id||', exercice:'||a_exe_ordre||', prorata:'||a_tap_id||')');
		-- END IF;
   END;

   PROCEDURE verifier_action (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_lolf_id                 FACTURE_CTRL_ACTION.lolf_id%TYPE
   ) IS
      my_nb           INTEGER;
      my_par_value    PARAMETRES.par_value%TYPE;
   BEGIN
		-- si parametre tester suivant les destinations du budget de gestion.
		my_par_value:=Get_Parametre(a_exe_ordre, 'CTRL_ORGAN_DEST');
		IF my_par_value = 'OUI' THEN
           SELECT COUNT(*) INTO my_nb FROM v_organ_action_rec WHERE lolf_id=a_lolf_id AND tcd_ordre=a_tcd_ordre AND org_id=a_org_id;
           IF my_nb=0 THEN
              RAISE_APPLICATION_ERROR(-20001, 'Cette action n''est pas autorisee pour cette ligne budgetaire et ce type de credit (action:'||a_lolf_id||', organ:'||a_org_id||', typecredit:'||a_tcd_ordre||')');
           END IF;
		END IF;
   END;

   PROCEDURE verifier_analytique (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_can_id                  FACTURE_CTRL_ANALYTIQUE.can_id%TYPE
   ) IS
       my_nb      INTEGER;
  BEGIN
		-- verifier si ce code analytique est valide et utilisable.
		SELECT COUNT(*) INTO my_nb FROM jefy_admin.v_code_analytique WHERE can_id=a_can_id
		AND tyet_id = Type_Etat.get_etat_canal_valide AND can_utilisable = Type_Etat.get_etat_canal_utilisable AND exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique n''est pas valide ou utilisable (code analytique:'||a_can_id||')');
		END IF;

		-- verifier si public
		SELECT COUNT(*) INTO my_nb FROM jefy_admin.v_code_analytique WHERE can_id=a_can_id
		AND can_public = Type_Etat.get_etat_canal_public AND exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
			-- si pas public, verifier si droit utilisation code analytique (suivant organ).
			SELECT COUNT(*) INTO my_nb FROM v_code_analytique_organ WHERE can_id=a_can_id AND org_id=a_org_id;
			IF my_nb=0 THEN
		   	    RAISE_APPLICATION_ERROR(-20001, 'Ce code analytique ne peut etre utilise avec cette ligne budgetaire (code analytique:'||a_can_id||', organ:'||a_org_id||')');
			END IF;
		END IF;
   END;

   PROCEDURE verifier_convention (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_con_ordre               FACTURE_CTRL_CONVENTION.con_ordre%TYPE
   ) IS
       my_nb      INTEGER;
  BEGIN
  	   my_nb := 1;
  END;

   PROCEDURE verifier_planco (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    v_plan_comptable.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM v_plan_comptable WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite <> 'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- verifier si correct avec le type de credit.
		SELECT COUNT(*) INTO my_nb FROM v_planco_credit
		   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE';
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation n''est pas associe a ce type de credit (pco_num:'||a_pco_num||', type de credit:'||a_tcd_ordre||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAUTIMP', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_planco_credit_rec_reautimp
			   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_planco_credit_rec
			   WHERE pco_num=a_pco_num AND tcd_ordre=a_tcd_ordre AND pcc_etat='VALIDE' AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_planco_tva (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    maracuja.plan_comptable_exer.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation tva n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite<>'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation tva n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes TVA autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAITVA', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_tva_reaitva
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_tva
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation TVA (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_planco_ctp (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE
   ) IS
   	   my_nb              INTEGER;
	   my_pco_validite    maracuja.plan_comptable_exer.pco_validite%TYPE;
   BEGIN
		-- verifier la validite du compte.
		SELECT COUNT(*) INTO my_nb FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_nb=0 THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation contrepartie n''existe pas (pco_num:'||a_pco_num||')');
		END IF;

		SELECT pco_validite INTO my_pco_validite FROM maracuja.plan_comptable_exer WHERE pco_num=a_pco_num and exe_ordre = a_exe_ordre;
		IF my_pco_validite<>'VALIDE' THEN
	       RAISE_APPLICATION_ERROR(-20001, 'Le compte d''imputation contrepartie n''est pas valide (pco_num:'||a_pco_num||')');
		END IF;

		-- on verifie si l'utilisateur a le droit d'utiliser des comptes CTP autres
		-- pour savoir dans quelle vue on controle s'il peut utiliser ce compte.
		IF Get_Fonction_Utilisateur('REAICTP', a_utl_ordre, a_exe_ordre) = 1 THEN
		    -- droits elargis
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_ctp_reaictp
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		ELSE
			-- droits standards
			SELECT COUNT(*) INTO my_nb FROM v_plan_comptable_ctp
			   WHERE pco_num=a_pco_num AND pco_validite='VALIDE';
		END IF;

		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cet utilisateur n''est pas autorise a utiliser ce compte d''imputation contrepartie (pco_num:'||a_pco_num||', utilisateur:'||a_utl_ordre||')');
		END IF;
   END;

   PROCEDURE verifier_monnaie IS
       i INTEGER;
   BEGIN
        -- a completer --
		i:=1;
   END;

   PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		FACTURE.fou_ordre%TYPE
   ) IS
   	   my_nb			INTEGER;
       my_fou_valide    v_fournis_ulr.fou_valide%TYPE;
   BEGIN
   		IF a_fou_ordre IS NOT NULL THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_fournis_ulr WHERE fou_ordre = a_fou_ordre;
		   IF my_nb <> 1 THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''existe pas (fou_ordre:'||a_fou_ordre||')');
		   END IF;

		   SELECT fou_valide INTO my_fou_valide FROM v_fournis_ulr WHERE fou_ordre = a_fou_ordre;
		   IF my_fou_valide <> 'O' THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'Le fournisseur n''est pas valide (fou_ordre:'||a_fou_ordre||')');
		   END IF;
		END IF;

   END;

   PROCEDURE verifier_personne (
   	  a_pers_id		FACTURE.pers_id%TYPE
   ) IS
   	   my_nb			INTEGER;
   BEGIN
   		IF a_pers_id IS NOT NULL THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_personne WHERE pers_id = a_pers_id;
		   IF my_nb <> 1 THEN
		   	  RAISE_APPLICATION_ERROR(-20001, 'La personne n''existe pas (pers_id:'||a_pers_id||')');
		   END IF;
		END IF;
   END;

   PROCEDURE verifier_rib (
   	  a_fou_ordre		v_ribfour_ulr.fou_ordre%TYPE,
   	  a_rib_ordre		v_ribfour_ulr.rib_ordre%TYPE,
   	  a_mor_ordre		RECETTE_PAPIER.mor_ordre%TYPE,
   	  a_exe_ordre		RECETTE_PAPIER.exe_ordre%TYPE
   ) IS
   	   my_nb       		INTEGER;
	   my_rib_valide	v_ribfour_ulr.rib_valide%TYPE;
	   my_mod_code		v_ribfour_ulr.mod_code%TYPE;
	   my_mod_dom		v_mode_paiement.mod_dom%TYPE;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre AND exe_ordre = a_exe_ordre;
	    IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le mode de recouvrement n''existe pas pour cet exercice  (exercice:'||a_exe_ordre||', moderec :'||a_mor_ordre||')');
		END IF;

 	    SELECT mod_code, mod_dom INTO my_mod_code, my_mod_dom FROM v_mode_recouvrement WHERE mod_ordre = a_mor_ordre;

        IF a_rib_ordre IS NOT NULL
		THEN
   		   SELECT COUNT(*) INTO my_nb FROM v_ribfour_ulr
		     WHERE fou_ordre = a_fou_ordre AND rib_ordre = a_rib_ordre;

		   IF my_nb<>1 THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Le rib n''existe pas pour ce fournisseur (fournisseur:'||a_fou_ordre||', rib:'||a_rib_ordre||')');
		   END IF;

		   SELECT rib_valide INTO my_rib_valide FROM v_ribfour_ulr WHERE rib_ordre = a_rib_ordre;
		   IF my_rib_valide <> 'O' THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Le rib n''est pas valide (rib:'||a_rib_ordre||')');
		   END IF;
		ELSE
		   IF my_mod_dom = 'ECHEANCIER' THEN
		      RAISE_APPLICATION_ERROR(-20001, 'Pour ce mode de recouvrement il faut un rib (moderec :'||a_mor_ordre||')');
		   END IF;
		END IF;
   END;

   PROCEDURE verifier_recette_pap_coherence(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
   IS
		my_rpp_ht_saisie				RECETTE_PAPIER.rpp_ht_saisie%TYPE;
		my_rpp_tva_saisie				RECETTE_PAPIER.rpp_tva_saisie%TYPE;
		my_rpp_ttc_saisie				RECETTE_PAPIER.rpp_ttc_saisie%TYPE;

		my_ht_saisie					RECETTE.rec_ht_saisie%TYPE;
		my_tva_saisie					RECETTE.rec_tva_saisie%TYPE;
		my_ttc_saisie					RECETTE.rec_ttc_saisie%TYPE;
   BEGIN
   		-- recupere les montants de recette_papier.
		SELECT rpp_ht_saisie, rpp_tva_saisie, rpp_ttc_saisie
		  INTO my_rpp_ht_saisie, my_rpp_tva_saisie, my_rpp_ttc_saisie
		  FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;

		-- recupere et compare les montants de recette.
		SELECT NVL(SUM(rec_ht_saisie),0), NVL(SUM(rec_tva_saisie),0), NVL(SUM(rec_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE WHERE rpp_id = a_rpp_id;

		IF my_rpp_ht_saisie < my_ht_saisie OR my_rpp_tva_saisie < my_tva_saisie OR
		   my_rpp_ttc_saisie < my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Somme des montants incompatibles entre la recette papier et les recettes associees (recette papier:'||a_rpp_id||')');
		END IF;
   END;

   PROCEDURE verifier_facture_coherence (a_fac_id FACTURE.fac_id%TYPE) IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

		my_montant_budgetaire			FACTURE.fac_montant_budgetaire%TYPE;
		my_montant_budgetaire_reste		FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_ht_saisie					FACTURE.fac_ht_saisie%TYPE;
		my_tva_saisie					FACTURE.fac_tva_saisie%TYPE;
		my_ttc_saisie					FACTURE.fac_ttc_saisie%TYPE;
   BEGIN
   		-- recupere les montants de facture.
		SELECT fac_montant_budgetaire, fac_montant_budgetaire_reste,
		       fac_ht_saisie, fac_tva_saisie, fac_ttc_saisie
		  INTO my_fac_montant_budgetaire, my_fac_montant_budgetaire_rest,
		       my_fac_ht_saisie, my_fac_tva_saisie, my_fac_ttc_saisie
		  FROM FACTURE WHERE fac_id=a_fac_id;

		-- recupere et compare les montants de facture_ctrl_action.
		SELECT NVL(SUM(fact_montant_budgetaire),0), NVL(SUM(fact_montant_budgetaire_reste),0),
		       NVL(SUM(fact_ht_saisie),0), NVL(SUM(fact_tva_saisie),0), NVL(SUM(fact_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_ACTION WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<>my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<>my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<>my_ht_saisie OR my_fac_tva_saisie<>my_tva_saisie OR
		   my_fac_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants actions incoherentes (facture_ctrl_action - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_analytique.

		-- on ne verifie pas la coherence exacte de facture_ctrl_analytique.
		--   puisque les codes analytiques sont facultatifs .
		-- on verifie juste qu'ils ne depassent pas le montant de la facture.

		SELECT NVL(SUM(fana_montant_budgetaire),0), NVL(SUM(fana_montant_budgetaire_reste),0),
		       NVL(SUM(fana_ht_saisie),0), NVL(SUM(fana_tva_saisie),0), NVL(SUM(fana_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<my_ht_saisie OR my_fac_tva_saisie<my_tva_saisie OR
		   my_fac_ttc_saisie<my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants code analytique incoherentes (facture_ctrl_analytique - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_convention.

		-- on ne verifie pas la coherence exacte de facture_ctrl_convention.
		--   puisque les conventions sont facultatives .
		-- on verifie juste qu'ils ne depassent pas le montant de la facture.

		SELECT NVL(SUM(fcon_montant_budgetaire),0), NVL(SUM(fcon_montant_budgetaire_reste),0),
		       NVL(SUM(fcon_ht_saisie),0), NVL(SUM(fcon_tva_saisie),0), NVL(SUM(fcon_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_montant_budgetaire_reste,
		       my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_CONVENTION WHERE fac_id=a_fac_id;

		IF my_fac_montant_budgetaire<my_montant_budgetaire OR
		   my_fac_montant_budgetaire_rest<my_montant_budgetaire_reste OR
		   my_fac_ht_saisie<my_ht_saisie OR my_fac_tva_saisie<my_tva_saisie OR
		   my_fac_ttc_saisie<my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants convention incoherentes (facture_ctrl_convention - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

		-- recupere et compare les montants de facture_ctrl_planco.
		SELECT NVL(SUM(fpco_ht_saisie),0), NVL(SUM(fpco_tva_saisie),0), NVL(SUM(fpco_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id;

		IF my_fac_ht_saisie<>my_ht_saisie OR my_fac_tva_saisie<>my_tva_saisie OR
		   my_fac_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable incoherentes (facture_ctrl_planco - facture:'||a_fac_id||') (verifier_facture_coherence)');
		END IF;

   END;

   PROCEDURE verifier_recette_coherence(a_rec_id RECETTE.rec_id%TYPE)
   IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest  FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

		my_fac_id						RECETTE.fac_id%TYPE;
   		my_rec_montant_budgetaire       RECETTE.rec_montant_budgetaire%TYPE;
		my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
		my_rec_tva_saisie				RECETTE.rec_tva_saisie%TYPE;
		my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;

		my_montant_budgetaire			RECETTE.rec_montant_budgetaire%TYPE;
		my_ht_saisie					RECETTE.rec_ht_saisie%TYPE;
		my_tva_saisie					RECETTE.rec_tva_saisie%TYPE;
		my_ttc_saisie					RECETTE.rec_ttc_saisie%TYPE;

		my_rpco							RECETTE_CTRL_PLANCO%ROWTYPE;
		my_rpcotva_tva_saisie			RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
		my_rpcoctp_ttc_saisie			RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;

		my_nb							INTEGER;

      	CURSOR liste IS SELECT * FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
   BEGIN
   		-- recupere les montants de recette.
		SELECT rec_montant_budgetaire, rec_ht_saisie, rec_tva_saisie, rec_ttc_saisie, fac_id
		  INTO my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_tva_saisie, my_rec_ttc_saisie, my_fac_id
		  FROM RECETTE WHERE rec_id=a_rec_id;

		-- recupere et compare les montants de recette_ctrl_action.
		SELECT NVL(SUM(ract_montant_budgetaire),0), NVL(SUM(ract_ht_saisie),0), NVL(SUM(ract_tva_saisie),0),
		   NVL(SUM(ract_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_ACTION WHERE rec_id=a_rec_id;

		IF my_rec_montant_budgetaire<>my_montant_budgetaire OR
		   my_rec_ht_saisie<>my_ht_saisie OR my_rec_tva_saisie<>my_tva_saisie OR
		   my_rec_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants actions incoherentes (recette_ctrl_action - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_analytique.

		-- on ne verifie pas la coherence exacte de recette_ctrl_analytique.
		--   puisque les codes analytiques sont facultatifs .
		-- on verifie juste qu'ils ne depassent pas le montant de la recette.

	    SELECT NVL(SUM(rana_montant_budgetaire),0), NVL(SUM(rana_ht_saisie),0), NVL(SUM(rana_tva_saisie),0),
		  NVL(SUM(rana_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;

		IF ABS(my_rec_montant_budgetaire) < ABS(my_montant_budgetaire) OR
		   ABS(my_rec_ht_saisie) < ABS(my_ht_saisie) OR ABS(my_rec_tva_saisie) < ABS(my_tva_saisie) OR
		   ABS(my_rec_ttc_saisie) < ABS(my_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants codes analytiques incoherentes (recette_ctrl_analytique - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_convention.

		-- on ne verifie pas la coherence exacte de recette_ctrl_convention.
		--   puisque les conventions sont facultatives .
		-- on verifie juste qu'ils ne depassent pas le montant de la recette.

	    SELECT NVL(SUM(rcon_montant_budgetaire),0), NVL(SUM(rcon_ht_saisie),0), NVL(SUM(rcon_tva_saisie),0),
		  NVL(SUM(rcon_ttc_saisie),0)
		  INTO my_montant_budgetaire, my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;

		IF ABS(my_rec_montant_budgetaire) < ABS(my_montant_budgetaire) OR
		   ABS(my_rec_ht_saisie) < ABS(my_ht_saisie) OR ABS(my_rec_tva_saisie) < ABS(my_tva_saisie) OR
		   ABS(my_rec_ttc_saisie) < ABS(my_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants convention incoherentes (recette_ctrl_convention - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

		-- recupere et compare les montants de recette_ctrl_planco.
		SELECT NVL(SUM(rpco_ht_saisie),0), NVL(SUM(rpco_tva_saisie),0),
		   NVL(SUM(rpco_ttc_saisie),0)
		  INTO my_ht_saisie, my_tva_saisie, my_ttc_saisie
		  FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;

		IF my_rec_ht_saisie<>my_ht_saisie OR my_rec_tva_saisie<>my_tva_saisie OR
		   my_rec_ttc_saisie<>my_ttc_saisie THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable incoherentes (recette_ctrl_planco - recette:'||a_rec_id||') (verifier_recette_coherence)');
		END IF;

      	OPEN liste();
  	  	LOOP
			FETCH liste INTO my_rpco;
		 	EXIT WHEN liste%NOTFOUND;

			-- recupere et compare les montants de recette_ctrl_planco_tva.
			SELECT NVL(SUM(rpcotva_tva_saisie),0)
		  		   INTO my_rpcotva_tva_saisie
		  		   FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id = my_rpco.rpco_id;

			IF my_rpco.rpco_tva_saisie <> my_rpcotva_tva_saisie THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable TVA incoherentes (entre recette_ctrl_planco et recette_ctrl_planco_tva - recette:'||a_rec_id||') (verifier_recette_coherence)');
			END IF;

			-- recupere et compare les montants de recette_ctrl_planco_ctp.
			-- s'il y en a, parce que on peut autoriser a saisir une recette sans contrepartie
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id = my_rpco.rpco_id;
			IF my_nb > 0 THEN
			  SELECT NVL(SUM(rpcoctp_ttc_saisie),0)
		  		   INTO my_rpcoctp_ttc_saisie
		  		   FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id = my_rpco.rpco_id;

			  IF my_rpco.rpco_ttc_saisie <> my_rpcoctp_ttc_saisie THEN
		   	     RAISE_APPLICATION_ERROR(-20001, 'Sommes des montants imputation comptable contrepartie incoherentes (entre recette_ctrl_planco et recette_ctrl_planco_ctp - recette:'||a_rec_id||') (verifier_recette_coherence)');
			  END IF;
			END IF;

		END LOOP;
	  	CLOSE liste;

		-- TODO
		-- verifier que le total des reductions ne depasse pas le total des recettes correspondantes
		-- (plusieurs reductions eventuellement pour une recette)

	END;

   PROCEDURE verifier_fac_rec_coherence(a_fac_id FACTURE.fac_id%TYPE)
   IS
   		my_fac_montant_budgetaire       FACTURE.fac_montant_budgetaire%TYPE;
		my_fac_montant_budgetaire_rest  FACTURE.fac_montant_budgetaire_reste%TYPE;
		my_fac_ht_saisie				FACTURE.fac_ht_saisie%TYPE;
		my_fac_tva_saisie				FACTURE.fac_tva_saisie%TYPE;
		my_fac_ttc_saisie				FACTURE.fac_ttc_saisie%TYPE;

   		my_rec_montant_budgetaire       RECETTE.rec_montant_budgetaire%TYPE;
		my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
		my_rec_tva_saisie				RECETTE.rec_tva_saisie%TYPE;
		my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;

	  	my_nb							INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb=0
		THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (verifier_fac_rec_coherence)');
		END IF;

		-- recupere les montants de facture
		SELECT fac_montant_budgetaire, fac_montant_budgetaire_reste,
		       fac_ht_saisie, fac_tva_saisie, fac_ttc_saisie
		  INTO my_fac_montant_budgetaire, my_fac_montant_budgetaire_rest,
		       my_fac_ht_saisie, my_fac_tva_saisie, my_fac_ttc_saisie
		  FROM FACTURE WHERE fac_id = a_fac_id;

		-- les montants des recettes
		SELECT NVL(SUM(rec_montant_budgetaire),0), NVL(SUM(rec_ht_saisie),0), NVL(SUM(rec_tva_saisie),0),
		   NVL(SUM(rec_ttc_saisie),0)
		  INTO my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_tva_saisie, my_rec_ttc_saisie
		  FROM RECETTE WHERE fac_id=a_fac_id;

		-- verif...
		IF my_fac_ht_saisie < my_rec_ht_saisie OR my_fac_ttc_saisie < my_rec_ttc_saisie OR my_fac_tva_saisie < my_rec_tva_saisie
		THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Le montant total facture est inferieur au montant recette (facture:'||a_fac_id||') (verifier_fac_rec_coherence)');
		END IF;

   END;

   PROCEDURE verifier_organ (
   	 a_org_id	   FACTURE.org_id%TYPE,
	 a_tcd_ordre   FACTURE.tcd_ordre%TYPE
   ) IS
       my_org_niv		v_organ.org_niv%TYPE;
	   my_nb			INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM v_organ WHERE org_id = a_org_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La ligne budgetaire n''existe pas (org_id:'||a_org_id||')');
		END IF;

   		SELECT org_niv INTO my_org_niv FROM v_organ WHERE org_id = a_org_id;

		IF my_org_niv < 3 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il faut facturer sur un CR ou une convention (org_id:'||a_org_id||')');
		END IF;
   END;

   PROCEDURE verifier_utilisation_facture (
      a_fac_id              FACTURE.fac_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE fac_id=a_fac_id AND tyet_id=1;
		IF my_nb>0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des recettes sur cette facture (fac_id:'||a_fac_id||')');
		END IF;
   END;

   PROCEDURE verifier_util_recette_papier (
      a_rpp_id RECETTE_PAPIER.rpp_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette papier n''existe pas (rpp_id:'||a_rpp_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rpp_id=a_rpp_id;
		IF my_nb > 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des recettes sur cette recette papier (rpp_id:'||a_rpp_id||')');
		END IF;
   END;


   PROCEDURE verifier_utilisation_recette (
      a_rec_id              RECETTE.rec_id%TYPE)
   IS
     my_nb                  INTEGER;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette n''existe pas (rec_id:'||a_rec_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id AND tit_id IS NOT NULL;
		IF my_nb > 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette est deja sur un titre de recette (rec_id:'||a_rec_id||')');
		END IF;
        SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id_reduction = a_rec_id;
		IF my_nb>0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Il y a des reductions sur cette recette (rec_id='||a_rec_id||')');
		END IF;
   END;

END;

/

--------------------------------------------------------
--  DDL for Package BUDGET
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."BUDGET" IS

--
-- procedures a appeler par les autres packages pour le lien eventuel avec budget.
--

PROCEDURE ins_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE upd_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE del_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE ins_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

PROCEDURE upd_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

PROCEDURE del_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

--
-- fonction de calcul de montant budgetaire.
--

FUNCTION calculer_budgetaire (
		 a_exe_ordre     jefy_admin.exercice.exe_ordre%TYPE,
		 a_tap_id        jefy_recette.FACTURE.tap_id%TYPE,
		 a_org_id	     jefy_recette.FACTURE.org_id%TYPE,
		 a_montant_ht    jefy_recette.FACTURE.fac_ht_saisie%TYPE,
		 a_montant_ttc   jefy_recette.FACTURE.fac_ttc_saisie%TYPE)
  RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;

FUNCTION get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE)
     RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;

PROCEDURE get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
		 a_disponible	OUT jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body BUDGET
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."BUDGET"
IS

PROCEDURE ins_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
   BEGIN
   		-- on teste si le montant est positif.
		IF a_montant<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif');
		END IF;

        -- appel aux procedures de jefy_budget.

   END;

PROCEDURE upd_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
   BEGIN
   		-- on teste si le montant est positif.
		IF a_montant<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif');
		END IF;

        -- appel aux procedures de jefy_budget.

   END;

PROCEDURE del_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE ins_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE upd_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE del_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

FUNCTION calculer_budgetaire (
		 a_exe_ordre     jefy_admin.exercice.exe_ordre%TYPE,
		 a_tap_id        jefy_recette.FACTURE.tap_id%TYPE,
		 a_org_id	     jefy_recette.FACTURE.org_id%TYPE,
		 a_montant_ht    jefy_recette.FACTURE.fac_ht_saisie%TYPE,
		 a_montant_ttc   jefy_recette.FACTURE.fac_ttc_saisie%TYPE)
		 RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   IS
     my_tap_taux           jefy_admin.taux_prorata.tap_taux%TYPE;
	 my_montant_budgetaire jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;
   BEGIN

        -- SELECT tap_taux INTO my_tap_taux FROM jefy_admin.taux_prorata WHERE tap_id=a_tap_id;
		-- my_montant_budgetaire := ((a_montant_ttc - a_montant_ht) * ((100 - my_tap_taux) / 100)) + a_montant_ht;
		my_montant_budgetaire := a_montant_ht;

        RETURN my_montant_budgetaire;
   END;

FUNCTION get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE)
     RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   IS
   BEGIN
        RETURN jefy_budget.budget_utilitaires.get_disponible (a_org_id, a_tcd_ordre, a_exe_ordre);
   END;

PROCEDURE get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
		 a_disponible	OUT jefy_recette.FACTURE.fac_montant_budgetaire%TYPE)
	IS
	BEGIN
		 a_disponible := get_disponible(a_exe_ordre, a_org_id, a_tcd_ordre);
	END;

END;

/

--------------------------------------------------------
--  DDL for Package API
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API" IS

--
--
-- procedures publiques a executer par les applications clientes.
--
--

-- Crée une recette indépendante (recette + adresse recette + facture )
-- Delegue la creation recette + facture a ins_facture_recette
-- puis complete en creant l'adresse sur la recette papier.
PROCEDURE ins_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Crée une recette indépendante (recette + facture)
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Update une recette simple indépendante
-- en ajoutant la gestion de l'adresse de la recette papier.
PROCEDURE upd_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Update une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Delete une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

-- Insère une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Update une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Delete une facture simple
PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE);

-- insère une recette papier simple visible
PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero IN OUT	  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

-- Insère une recette
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
--
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Permet de recetter en automatique une facture (recette totale)
-- en créant aussi la recette papier non visible
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE upd_recette_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE upd_facture_eche_id (
      a_fac_id              FACTURE.fac_id%TYPE,
	  a_eche_id				FACTURE.eche_id%TYPE);

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           RECETTE_PAPIER.utl_ordre%TYPE);

-- Supprime une recette simple
PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           RECETTE.utl_ordre%TYPE);

-- Reduit une recette existante
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Reduit une recette existante en inserant l'adresse client.
PROCEDURE ins_reduction_adresse (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

PROCEDURE upd_reduction_ctrl_planco (
      a_rpco_id              RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

END;

/

--------------------------------------------------------
--  DDL for Package Body API
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."API"
IS

-- Crée une recette indépendante (recette + adresse recette + facture )
-- Delegue la creation recette + facture a ins_facture_recette
-- puis complete en creant l'adresse sur la recette papier.
PROCEDURE ins_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE)
IS
	BEGIN
		ins_facture_recette(
			a_fac_id, a_rec_id, a_rpp_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre, a_pers_id, a_fac_lib,
			a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie,
			a_fac_ttc_saisie, a_tyap_id, a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention,
			a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);
		IF a_rpp_adc_adr_ordre IS NOT NULL AND a_rpp_adc_pers_id IS NOT NULL THEN
			Recetter.ins_recette_papier_adr_client(a_rpp_id, a_rpp_adc_adr_ordre, a_rpp_adc_pers_id);
		END IF;
	END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit de facturer et recetter sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer_Recetter.ins_facture_recette(a_fac_id, a_rec_id, a_rpp_id, a_exe_ordre, a_fac_numero, a_rec_numero, NULL, a_fou_ordre,
			a_pers_id, a_fac_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie, a_fac_ttc_saisie, a_tyap_id,
			a_utl_ordre,a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva,
			a_chaine_planco_ctp);

		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		-- on verifie la coherence des montants entre les differents ctrl_.
		Verifier.verifier_facture_coherence(a_fac_id);
		Verifier.verifier_recette_coherence(a_rec_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
	    Apres_Recette.ins_recette(a_rec_id);
   END;

-- Maj Facture Recette avec Gestion de l'adresse de la recette papier.
PROCEDURE upd_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE)
IS
	my_rpp_id				RECETTE.rpp_id%TYPE;
	my_nb					INTEGER;
	BEGIN
		upd_facture_recette(
			a_fac_id, a_rec_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre, a_pers_id, a_fac_lib,
			a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie,
			a_fac_ttc_saisie, a_tyap_id, a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention,
			a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);
		IF a_rpp_adc_adr_ordre IS NOT NULL AND a_rpp_adc_pers_id IS NOT NULL THEN
			SELECT rpp_id INTO my_rpp_id FROM RECETTE WHERE rec_id = a_rec_id;
			Recetter.ins_recette_papier_adr_client(my_rpp_id, a_rpp_adc_adr_ordre, a_rpp_adc_pers_id);
		END IF;
	END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
        Verifier.verifier_utilisation_recette(a_rec_id);
		-- on verifie que l'utilisateur a le droit de facturer et recetter sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer_Recetter.upd_facture_recette(a_fac_id, a_rec_id, a_exe_ordre, a_fac_numero, a_rec_numero, a_fou_ordre,
			a_pers_id, a_fac_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie, a_fac_ttc_saisie, a_tyap_id,
			a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva,
			a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents ctrl_.
		Verifier.verifier_facture_coherence(a_fac_id);
		Verifier.verifier_recette_coherence(a_rec_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
   	 my_fac_id				FACTURE.fac_id%TYPE;
   BEGIN
        Verifier.verifier_utilisation_recette(a_rec_id);
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id = a_rec_id;
		Facturer_Recetter.del_facture_recette(a_rec_id, a_utl_ordre);
		Apres_Recette.del_recette(a_rec_id);
		Apres_Facture.del_facture(my_fac_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   BEGIN
		-- on verifie que l'utilisateur a le droit de facturer sur cet exercice.
		Verifier.verifier_droit_facture(a_exe_ordre, a_utl_ordre);

		-- insertion.
		Facturer.ins_facture(a_fac_id,a_exe_ordre,a_fac_numero,a_fou_ordre,a_pers_id,a_fac_lib,a_mor_ordre,a_org_id,a_tcd_ordre,a_tap_id,
		     a_fac_ht_saisie,a_fac_ttc_saisie,a_tyap_id,a_utl_ordre,a_chaine_action,a_chaine_analytique,a_chaine_convention,a_chaine_planco);

		-- on verifie la coherence des montants entre les differents facture_.
		Verifier.verifier_facture_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.ins_facture(a_fac_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
   	 my_exe_ordre			FACTURE.exe_ordre%TYPE;
	 my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb=0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (upd_facture)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM FACTURE WHERE fac_id=a_fac_id;

		-- on verifie que l'utilisateur a le droit de facturer sur cet exercice.
		Verifier.verifier_droit_facture(my_exe_ordre, a_utl_ordre);

		-- mise a jour.
		Facturer.upd_facture(a_fac_id,a_fac_lib,a_mor_ordre, a_tyap_id,a_fac_ht_saisie,a_fac_ttc_saisie,a_utl_ordre,
		    a_chaine_action,a_chaine_analytique,a_chaine_convention,a_chaine_planco);

		-- on verifie la coherence des montants entre les differents facture_.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Facture.upd_facture(a_fac_id);
   END;

PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE)
   IS
   BEGIN
        Verifier.verifier_utilisation_facture(a_fac_id);
		Facturer.del_facture(a_fac_id, a_utl_ordre);
		Apres_Facture.del_facture(a_fac_id);
   END;

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero IN OUT	  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
   BEGIN
        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette papier.
		Recetter.ins_recette_papier(a_rpp_id, a_exe_ordre, a_rpp_numero, a_rpp_ht_saisie, a_rpp_ttc_saisie,
		a_pers_id, a_fou_ordre, a_rib_ordre, a_mor_ordre, a_rpp_date_recette, a_rpp_date_reception,
		a_rpp_date_service_fait, a_rpp_nb_piece, a_utl_ordre, 'O');

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		Apres_Recette.ins_recette_papier(a_rpp_id);
   END;

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
	   my_exe_ordre			 RECETTE_PAPIER.exe_ordre%TYPE;
	   my_nb INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette papier n''existe pas (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette papier.
		Recetter.upd_recette_papier(a_rpp_id, a_rpp_numero, a_rpp_ht_saisie, a_rpp_ttc_saisie,
		a_pers_id, a_fou_ordre, a_rib_ordre, a_mor_ordre, a_rpp_date_recette, a_rpp_date_reception,
		a_rpp_date_service_fait, a_rpp_nb_piece, a_utl_ordre);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_pap_coherence(a_rpp_id);

		Apres_Recette.upd_recette_papier(a_rpp_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
   BEGIN
        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette.
		Recetter.ins_recette(a_rec_id, a_exe_ordre, a_rpp_id, a_rec_numero, a_fac_id, a_rec_lib, a_rib_ordre, a_mor_ordre,
			a_rec_ht_saisie, a_rec_ttc_saisie, a_tap_id, a_utl_ordre, a_chaine_action,
			a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);

   END;

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE
   ) IS
   	 my_exe_ordre			FACTURE.exe_ordre%TYPE;
	 my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb = 0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (ins_recette_from_facture)');
		END IF;
		SELECT exe_ordre INTO my_exe_ordre FROM FACTURE WHERE fac_id = a_fac_id;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);

		-- creation
		Recetter.ins_recette_from_facture(a_rec_id, a_rec_numero, a_fac_id, a_utl_ordre, a_rib_ordre, a_mor_ordre,
		   a_tap_id, a_pco_num_tva, a_pco_num_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(a_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(a_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE upd_recette_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE
   ) IS
	  my_rec_id_reduction	RECETTE.rec_id_reduction%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rpco_id = a_rpco_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette planco demandee n''existe pas (rpco_id:'||a_rpco_id||') (upd_recette_ctrl_planco)');
		END IF;
   		SELECT r.rec_id_reduction INTO my_rec_id_reduction FROM RECETTE r, RECETTE_CTRL_PLANCO rpco
		  WHERE rpco.rpco_id = a_rpco_id AND rpco.rec_id = r.rec_id;
		IF my_rec_id_reduction IS NOT NULL THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee est une reduction, utiliser upd_reduction_ctrl_planco (rpco_id:'||a_rpco_id||') (upd_recette_ctrl_planco)');
		END IF;
		UPDATE RECETTE_CTRL_PLANCO SET tit_id = a_tit_id WHERE rpco_id = a_rpco_id;
   END;

PROCEDURE upd_facture_eche_id (
      a_fac_id              FACTURE.fac_id%TYPE,
	  a_eche_id				FACTURE.eche_id%TYPE
   ) IS
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (upd_facture_eche_id)');
		END IF;
		UPDATE FACTURE SET eche_id = a_eche_id WHERE fac_id = a_fac_id;
   END;

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
       my_nb         			INTEGER;
	   my_exe_ordre	 		    RECETTE_PAPIER.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La recette papier n''existe pas ou est deja annule (rpp_id:'||a_rpp_id||')');
	    END IF;
   		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;

		Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		Verifier.verifier_util_recette_papier(a_rpp_id);

		Recetter.del_recette_papier(a_rpp_id, a_utl_ordre);

		Apres_Recette.del_recette_papier(a_rpp_id);
   END;


PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           RECETTE.utl_ordre%TYPE
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id||') (del_recette)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id=a_rec_id;

        Verifier.verifier_utilisation_recette(a_rec_id);

		Recetter.del_recette(a_rec_id, a_utl_ordre);

		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);
		-- on verifie la coherence entre la facture et la(les) recette(s) s'il en reste
		Verifier.verifier_fac_rec_coherence(my_fac_id);

		Apres_Recette.del_recette(a_rec_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id_reduction;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id_reduction||') (ins_reduction)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id = a_rec_id_reduction;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		Verifier.verifier_droit_recette(a_exe_ordre, a_utl_ordre);

		-- insertion dans les tables de recette.
		Reduire.ins_reduction(a_rec_id, a_exe_ordre, a_rec_numero, a_rec_lib, a_rpp_nb_piece,
			a_rec_ht_saisie, a_rec_ttc_saisie, a_utl_ordre, a_rec_id_reduction, a_chaine_action,
			a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);

		-- on verifie la coherence des montants entre les differents recette_.
		Verifier.verifier_recette_coherence(a_rec_id);
		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);

		-- on verifie la coherence entre la facture et la(les) recette(s)
		Verifier.verifier_fac_rec_coherence(my_fac_id);

	    -- post-traitement eventuel
	    Apres_Recette.ins_recette(a_rec_id);
   END;

PROCEDURE ins_reduction_adresse (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE
   ) IS
   		my_rpp_id 				RECETTE_PAPIER.rpp_id%TYPE;
   		my_nb					INTEGER;
   BEGIN
		ins_reduction(a_rec_id, a_exe_ordre, a_rec_numero, a_rec_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece,
			a_rec_ht_saisie, a_rec_ttc_saisie, a_utl_ordre, a_rec_id_reduction, a_chaine_action, a_chaine_analytique,
			a_chaine_convention, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);
		IF a_rpp_adc_adr_ordre IS NOT NULL AND a_rpp_adc_pers_id IS NOT NULL THEN
			SELECT count(*) INTO my_nb FROM recette WHERE rec_id = a_rec_id;
			IF my_nb = 1 THEN
				SELECT rpp_id INTO my_rpp_id FROM recette WHERE rec_id = a_rec_id;
				SELECT count(*) INTO my_nb FROM recette_papier where rpp_id = my_rpp_id;
				IF my_nb = 1 THEN
					Recetter.ins_recette_papier_adr_client(my_rpp_id, a_rpp_adc_adr_ordre, a_rpp_adc_pers_id);
				END IF;
			END IF;
		END IF;
   END;

PROCEDURE upd_reduction_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE
   ) IS
	  my_rec_id_reduction	RECETTE.rec_id_reduction%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rpco_id = a_rpco_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette planco demandee n''existe pas (rpco_id:'||a_rpco_id||') (upd_reduction_ctrl_planco)');
		END IF;
   		SELECT r.rec_id_reduction INTO my_rec_id_reduction FROM RECETTE r, RECETTE_CTRL_PLANCO rpco
		  WHERE rpco.rpco_id = a_rpco_id AND rpco.rec_id = r.rec_id;
		IF my_rec_id_reduction IS NULL THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''est pas une reduction, utiliser upd_recette (rpco_id:'||a_rpco_id||') (upd_reduction_ctrl_planco)');
		END IF;
		UPDATE RECETTE_CTRL_PLANCO SET tit_id = a_tit_id WHERE rpco_id = a_rpco_id;
   END;

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb=0 THEN
			RAISE_APPLICATION_ERROR(-20001, 'La recette demandee n''existe pas (rec_id:'||a_rec_id||') (del_reduction)');
		END IF;
		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id=a_rec_id;

        Verifier.verifier_utilisation_recette(a_rec_id);

		Reduire.del_reduction(a_rec_id, a_utl_ordre);

		-- on verifie si la coherence des montants budgetaires restant est conservee.
		Verifier.verifier_facture_coherence(my_fac_id);
		-- on verifie la coherence entre la facture et la(les) recette(s) s'il en reste
		Verifier.verifier_fac_rec_coherence(my_fac_id);

		Apres_Recette.del_recette(a_rec_id);
   END;

END;

/

-- bugfix : date 1.5.0.0 est incorrecte.
update jefy_recette.db_version set db_date = to_date('30/01/2012','dd/mm/yyyy') where db_version = '1.5.0.0';
insert into jefy_recette.db_version (db_version, db_date,db_comment) values ('1.6.0.0', to_date('12/03/2012','dd/mm/yyyy'), 'version 1.6.0.0');

commit;
