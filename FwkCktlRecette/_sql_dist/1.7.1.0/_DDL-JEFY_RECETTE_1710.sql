SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :
-- Date de publication :
-- Licence : CeCILL version 2
--
-- creation et execution d'une procedure qui ajoute des decimales aux numeros de facture lorsque des doublons de numeros sont detectes.
-- ajout d'une contrainte sur exe_ordre, fap_numero


whenever sqlerror exit sql.sqlcode;


CREATE OR REPLACE PROCEDURE JEFY_RECETTE.corrige_doublons_fap
is
   exeordre    integer;
   fapnumero   number;
   fapid integer;
   fapnumeronew number;
   fapnumerostr varchar2(50);
   nb integer;
    i integer;
   cursor c_doublons
   is
      select   exe_ordre,
               fap_numero,
               count (*)
      from     facture_papier
      group by exe_ordre, fap_numero
      having   count (*) > 1;

   cursor c_fap
   is
      select fap_id,
             fap_numero
      from   facture_papier
      where  exe_ordre = exeordre and fap_numero = fapnumero order by fap_id;
begin
   open c_doublons;

   loop

      fetch c_doublons
      into  exeordre,
            fapnumero,
            nb;

      exit when c_doublons%notfound;
        i := 0;
      open c_fap;

      loop
        i := i+1;
         fetch c_fap
         into  fapid,
               fapnumeronew;

         exit when c_fap%notfound;
         fapnumerostr := to_number(to_char(fapnumeronew,'9999999999') || '.'|| i, '9999999999.99');
         update facture_papier set  fap_numero=to_number(fapnumerostr)  where fap_id=fapid and fap_numero=fapnumeronew;

      end loop;

      close c_fap;
   end loop;

   close c_doublons;
end;
/


exec JEFY_RECETTE.corrige_doublons_fap;
commit;


ALTER TABLE JEFY_RECETTE.FACTURE_PAPIER ADD CONSTRAINT UNQ_FACTURE_PAPIER UNIQUE (FAP_NUMERO, EXE_ORDRE)  DEFERRABLE INITIALLY DEFERRED;


-- Maj version
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.7.1.0',to_date('05/11/2012','dd/mm/yyyy'), 'Ajout contrainte pour controler les doublons sur les num de facture');
commit;
