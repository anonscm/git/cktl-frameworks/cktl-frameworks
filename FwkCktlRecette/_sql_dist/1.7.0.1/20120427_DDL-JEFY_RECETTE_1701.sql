SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version :  1.7.0.1
-- Date de publication : 27/04/2012
-- Licence : CeCILL version 2
--
--

whenever sqlerror exit sql.sqlcode;

-- Fonctions
DROP FUNCTION GET_DATE_EMISSION;

CREATE OR REPLACE
FUNCTION "JEFY_RECETTE"."GET_DATE_EMISSION" (EXERCICE IN NUMBER, DATE_SAISIE IN DATE) RETURN DATE
IS
  anneeDateSaisie  INTEGER;
  dateEmission     DATE;
BEGIN
  dateEmission := DATE_SAISIE;
  SELECT EXTRACT(YEAR FROM DATE_SAISIE) INTO anneeDateSaisie FROM DUAL;
  IF anneeDateSaisie <> exercice THEN
    dateEmission := TO_DATE('31/12/' || exercice, 'DD/MM/YYYY');
  END IF;
  RETURN dateEmission;
END;
/

-- Maj version
insert into jefy_recette.db_version (db_version,db_date,db_comment) values ('1.7.0.1',to_date('27/04/2012','dd/mm/yyyy'), 'Correction fonction GET_DATE_EMISSION');
commit;
