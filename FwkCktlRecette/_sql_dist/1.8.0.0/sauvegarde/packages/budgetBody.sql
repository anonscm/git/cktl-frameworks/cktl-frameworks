--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body BUDGET
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."BUDGET" 
IS

PROCEDURE ins_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
   BEGIN
   		-- on teste si le montant est positif.
		IF a_montant<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif');
		END IF;

        -- appel aux procedures de jefy_budget.

   END;

PROCEDURE upd_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
   BEGIN
   		-- on teste si le montant est positif.
		IF a_montant<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif');
		END IF;

        -- appel aux procedures de jefy_budget.

   END;

PROCEDURE del_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE ins_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE upd_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

PROCEDURE del_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE
   ) IS
       i INTEGER;
   BEGIN
        -- appel aux procedures de jefy_budget.
		i:=1;
   END;

FUNCTION calculer_budgetaire (
		 a_exe_ordre     jefy_admin.exercice.exe_ordre%TYPE,
		 a_tap_id        jefy_recette.FACTURE.tap_id%TYPE,
		 a_org_id	     jefy_recette.FACTURE.org_id%TYPE,
		 a_montant_ht    jefy_recette.FACTURE.fac_ht_saisie%TYPE,
		 a_montant_ttc   jefy_recette.FACTURE.fac_ttc_saisie%TYPE)
		 RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   IS
     my_tap_taux           jefy_admin.taux_prorata.tap_taux%TYPE;
	 my_montant_budgetaire jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;
   BEGIN

        -- SELECT tap_taux INTO my_tap_taux FROM jefy_admin.taux_prorata WHERE tap_id=a_tap_id;
		-- my_montant_budgetaire := ((a_montant_ttc - a_montant_ht) * ((100 - my_tap_taux) / 100)) + a_montant_ht;
		my_montant_budgetaire := a_montant_ht;

        RETURN my_montant_budgetaire;
   END;

FUNCTION get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE)
     RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE
   IS
   BEGIN
        RETURN jefy_budget.budget_utilitaires.get_disponible (a_org_id, a_tcd_ordre, a_exe_ordre);
   END;

PROCEDURE get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
		 a_disponible	OUT jefy_recette.FACTURE.fac_montant_budgetaire%TYPE)
	IS
	BEGIN
		 a_disponible := get_disponible(a_exe_ordre, a_org_id, a_tcd_ordre);
	END;

END;

/

