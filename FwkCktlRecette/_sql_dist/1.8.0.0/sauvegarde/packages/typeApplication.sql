--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package TYPE_APPLICATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."TYPE_APPLICATION" IS

FUNCTION get_type_depense
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_type_recette
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_type_prestation_interne
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_type_prestation_externe
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_type(a_tyap_libelle v_type_application.tyap_libelle%TYPE)
      RETURN v_type_application.tyap_id%TYPE;

END;

/

