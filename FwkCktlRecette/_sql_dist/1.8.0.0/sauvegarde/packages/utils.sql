--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package UTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."UTILS" is


    PROCEDURE annul_del_recette (zrecId z_recette.zrec_id%type);
    PROCEDURE annul_del_recette_papier (zrppId z_recette_papier.zrpp_id%type);
    PROCEDURE annul_del_facture (zfacId z_facture.zfac_id%type);
END utils; 

/

