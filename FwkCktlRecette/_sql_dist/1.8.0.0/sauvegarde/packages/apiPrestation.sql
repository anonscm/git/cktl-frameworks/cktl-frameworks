--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package API_PRESTATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API_PRESTATION" 
IS

-- ETAPE NO 1
-- valide la prestation cote client (date de validation client)
-- si c'est de la prestation interne, va generer en plus l'engagement
PROCEDURE valide_prestation_client (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 1
-- devalide la prestation cote client si possible
-- si c'est de la prestation interne, supprime en meme temps l'engagement si c'etait une PI de Pie
-- si c'etait un PI issue d'une commande Carambole, on ne supprimer pas l'engagement, on change juste son tyap_id
-- pour le remettre ! DEPENSE
PROCEDURE devalide_prestation_client (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 2
-- valide la prestation cote prestataire (date de validation prestataire)
-- pas d'autres incidences
PROCEDURE valide_prestation_prest (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 2
-- valide la prestation cote prestataire (date de validation prestataire)
-- pas d'autres incidences
PROCEDURE devalide_prestation_prest (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 3
-- clos la prestation (date de cloture)
-- pas d'autres incidences
PROCEDURE cloture_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 3
-- "declos" la prestation (date de cloture)
-- pas d'autres incidences
PROCEDURE decloture_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 4
-- genere la (les) facture(s) papier(s) a partir d'une prestation close
PROCEDURE genere_facture_papier (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- annulation etape no 4
-- suppression d'une facture papier non recettee
-- si issue d'une prestation, met a jour les quantites restantes a facturer
PROCEDURE del_facture_papier (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- ETAPE NO 5
-- validation de la facture papier cote client (date validation client)
-- aucune autre incidence
PROCEDURE valide_facture_papier_client (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- annulation etape no 5
-- devalidation de la facture cote client si non validee prestataire (date validation client)
-- aucune autre incidence
PROCEDURE devalide_facture_papier_client (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- ETAPE NO 6
-- valide la facture papier cote prestataire
-- genere la facture (table facture), lie a l'engagement si c'est de l'interne,
-- puis genere la recette (table recette) et liquide l'engagement si c'est de l'interne (puis lie les 2)
-- a_fac_id, a_utl_ordre et a_mor_ordre obligatoires
-- autres facultatifs (determination auto si null)
PROCEDURE valide_facture_papier_prest (
    a_fap_id           jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre        jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE,
    a_tap_id_depense   jefy_depense.depense_budget.tap_id%TYPE,
    a_rib_ordre        jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
    a_mor_ordre        jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
    a_tap_id_recette   jefy_recette.RECETTE.tap_id%TYPE);

-- annulation etape no 6
-- devalidation facture cote prestataire si possible (recette non titree)
-- supprime la facture/recette correspondante
-- si c'est de l'interne, supprime aussi la liquidation si possible
PROCEDURE devalide_facture_papier_prest (
    a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre                jefy_recette.FACTURE_PAPIER.utl_ordre%TYPE);

-- Archivage d'une prestation (= suppression, mais on ne supprime jamais réellement une prestation)
PROCEDURE archive_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- met a jour une reference de facture papier existante
-- a utiliser quand elle a ete creee directement (non issue d'une prestation, donc non generee par genere_facture_papier)
PROCEDURE upd_fap_ref (
    a_fap_id          jefy_recette.FACTURE_PAPIER.fap_id%TYPE);

-- Generation d'une prestation interne ! partir d'une commande
-- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- toutes les autres infos budgetaires recette facultatives
PROCEDURE prestation_from_commande (
    a_comm_id             jefy_depense.commande.comm_id%TYPE,
    a_utl_ordre             jefy_recette.prestation.utl_ordre%TYPE,
    a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
    a_org_id_recette     jefy_recette.prestation.org_id%TYPE,
    a_tap_id_recette     jefy_recette.prestation.tap_id%TYPE,
    a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
    a_lolf_id_recette     jefy_recette.prestation.lolf_id%TYPE,
    a_pco_num_recette     jefy_recette.prestation.pco_num%TYPE,
    a_can_id_recette     jefy_recette.prestation.can_id%TYPE,
    a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
    a_prest_id    IN OUT     jefy_recette.prestation.prest_id%TYPE);

-- Generation d'une facture papier interne ! partir d'une commande
-- a_comm_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- toutes les autres infos budgetaires recette facultatives
PROCEDURE facture_papier_from_commande (
    a_comm_id             jefy_depense.commande.comm_id%TYPE,
    a_utl_ordre             jefy_recette.prestation.utl_ordre%TYPE,
    a_fou_ordre_client   jefy_recette.prestation.fou_ordre%TYPE,
    a_org_id_recette     jefy_recette.prestation.org_id%TYPE,
    a_tap_id_recette     jefy_recette.prestation.tap_id%TYPE,
    a_tcd_ordre_recette     jefy_recette.prestation.tcd_ordre%TYPE,
    a_lolf_id_recette     jefy_recette.prestation.lolf_id%TYPE,
    a_pco_num_recette     jefy_recette.prestation.pco_num%TYPE,
    a_can_id_recette     jefy_recette.prestation.can_id%TYPE,
    a_con_ordre_recette     jefy_recette.prestation.con_ordre%TYPE,
    a_prest_id    IN OUT     jefy_recette.prestation.prest_id%TYPE,
    a_fap_id    IN OUT     jefy_recette.facture_papier.fap_id%TYPE);

 -- Duplication d'une facture papier en specifiant l'adresse du fournisseur.
 -- a_fap_id, a_utl_ordre, a_fou_ordre_client, a_four_adr_ordre et a_prest_id_creation obligatoires
 -- renvoie en OUT le fap_id et le fap_numero de la facture créée
PROCEDURE duplicate_facture_papier_adr (
    a_fap_id                  jefy_recette.facture_papier.fap_id%TYPE,
    a_utl_ordre               jefy_recette.facture_papier.utl_ordre%TYPE,
    a_fou_ordre_client        jefy_recette.facture_papier.fou_ordre%TYPE,
    a_fap_id_out      IN OUT  jefy_recette.facture_papier.fap_id%TYPE,
    a_fap_numero_out  IN OUT  jefy_recette.facture_papier.fap_numero%TYPE,
    a_fou_adr_ordre           jefy_recette.facture_papier_adr_client.adr_ordre%TYPE,
    a_pers_id_creation        jefy_recette.facture_papier_adr_client.pers_id_creation%TYPE);

-- Duplication d'une facture papier
-- a_fap_id, a_utl_ordre et a_fou_ordre_client obligatoires
-- renvoie en OUT le fap_id et le fap_numero de la facture créée
PROCEDURE duplicate_facture_papier (
    a_fap_id                   jefy_recette.facture_papier.fap_id%TYPE,
    a_utl_ordre                   jefy_recette.facture_papier.utl_ordre%TYPE,
    a_fou_ordre_client         jefy_recette.facture_papier.fou_ordre%TYPE,
    a_fap_id_out      IN OUT    jefy_recette.facture_papier.fap_id%TYPE,
    a_fap_numero_out IN OUT    jefy_recette.facture_papier.fap_numero%TYPE);

-- Solde une prestation
PROCEDURE solde_prestation (
    a_prest_id                jefy_recette.PRESTATION.prest_id%TYPE,
    a_utl_ordre                jefy_recette.PRESTATION.utl_ordre%TYPE);

-- Duplication d'une prestation
-- a_prest_id, a_utl_ordre et a_exe_ordre obligatoires
-- renvoie en OUT le prest_id et le prest_numero de la facture créée
PROCEDURE duplicate_prestation (
    a_prest_id                   jefy_recette.prestation.prest_id%TYPE,
    a_utl_ordre                   jefy_recette.prestation.utl_ordre%TYPE,
    a_exe_ordre             jefy_recette.prestation.exe_ordre%TYPE,
    a_prest_id_out      IN OUT    jefy_recette.prestation.prest_id%TYPE,
    a_prest_numero_out IN OUT    jefy_recette.prestation.prest_numero%TYPE);

-- la suite est "private"...

FUNCTION get_fap_ref (
      a_exe_ordre            jefy_recette.FACTURE_PAPIER.exe_ordre%TYPE,
      a_org_id                jefy_recette.FACTURE_PAPIER.org_id%TYPE,
      a_fap_id                jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
      a_old_fap_ref            jefy_recette.FACTURE_PAPIER.fap_ref%TYPE)
   RETURN VARCHAR2;

PROCEDURE ins_engage (
      a_eng_id IN OUT        jefy_depense.engage_budget.eng_id%TYPE,
      a_exe_ordre            jefy_depense.engage_budget.exe_ordre%TYPE,
      a_eng_numero IN OUT    jefy_depense.engage_budget.eng_numero%TYPE,
      a_fou_ordre            jefy_depense.engage_budget.fou_ordre%TYPE,
      a_org_id                jefy_depense.engage_budget.org_id%TYPE,
      a_tcd_ordre            jefy_depense.engage_budget.tcd_ordre%TYPE,
      a_tap_id                jefy_depense.engage_budget.tap_id%TYPE,
      a_eng_libelle            jefy_depense.engage_budget.eng_libelle%TYPE,
      a_eng_ht_saisie        jefy_depense.engage_budget.eng_ht_saisie%TYPE,
      a_eng_ttc_saisie        jefy_depense.engage_budget.eng_ttc_saisie%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE,
      a_action                VARCHAR2,
      a_analytique            VARCHAR2,
      a_convention            VARCHAR2,
      a_planco                VARCHAR2);

PROCEDURE del_engage (
      a_eng_id                  jefy_depense.engage_budget.eng_id%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE);

PROCEDURE ins_facture (
    a_fap_id          jefy_recette.FACTURE_PAPIER.fap_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

PROCEDURE del_facture (
    a_fac_id          jefy_recette.FACTURE.fac_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

-- recette totalement une facture
-- et s'il s'agit d'une prestation interne, liquide l'engagement correspondant
-- a_fac_id et a_utl_ordre obligatoires
-- autres facultatifs (détermination auto si possible, sinon erreur)
PROCEDURE ins_recette (
    a_fac_id          jefy_recette.FACTURE.fac_id%TYPE,
    a_fap_numero       jefy_recette.FACTURE_PAPIER.fap_numero%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE,
    a_tap_id_depense  jefy_depense.depense_budget.tap_id%TYPE,
    a_rib_ordre          jefy_recette.RECETTE_PAPIER.rib_ordre%TYPE,
    a_mor_ordre          jefy_recette.RECETTE_PAPIER.mor_ordre%TYPE,
    a_tap_id_recette  jefy_recette.RECETTE.tap_id%TYPE,
    a_pco_num_tva      jefy_recette.RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
    a_pco_num_ctp      jefy_recette.RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE del_recette (
    a_rec_id          jefy_recette.RECETTE.rec_id%TYPE,
    a_utl_ordre          jefy_recette.FACTURE.utl_ordre%TYPE);

-- a_rib_ordre peut "tre null, en fonction du mod_ordre
-- a_tap_id peut etre null (reprend le tap_id de l'engagement dans ce cas)
-- a_dep_ht_saisie et a_dep_ttc_saisie peuvent etre null (liquide tout l'engagement dans ce cas)
PROCEDURE ins_depense_from_engage (
      a_dep_id IN OUT        jefy_depense.depense_budget.dep_id%TYPE,
      a_eng_id                jefy_depense.engage_budget.eng_id%TYPE,
      a_dpp_numero            jefy_depense.depense_papier.dpp_numero_facture%TYPE,
      a_mod_ordre            jefy_depense.depense_papier.mod_ordre%TYPE,
      a_rib_ordre            jefy_depense.depense_papier.rib_ordre%TYPE,
      a_utl_ordre            jefy_depense.engage_budget.utl_ordre%TYPE,
      a_tap_id                jefy_depense.depense_budget.tap_id%TYPE,
      a_dep_ht_saisie        jefy_depense.depense_budget.dep_ht_saisie%TYPE,
      a_dep_ttc_saisie        jefy_depense.depense_budget.dep_ttc_saisie%TYPE);

FUNCTION chaine_action (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_analytique (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_convention (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_hors_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_marche (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco (a_eng_id jefy_depense.engage_budget.eng_id%TYPE,
         a_ht jefy_depense.depense_budget.dep_ht_saisie%TYPE,
         a_ttc jefy_depense.depense_budget.dep_ttc_saisie%TYPE)
   RETURN VARCHAR2;


procedure ins_facture_papier_adr_client (
    a_fap_id            facture_papier_adr_client.fap_id%type,
    a_adr_ordre         facture_papier_adr_client.adr_ordre%type,
    a_pers_id_creation  facture_papier_adr_client.pers_id_creation%type);

procedure ins_prestation_adr_client (
    a_prest_id        prestation_adr_client.prest_id%type,
    a_adr_ordre     prestation_adr_client.adr_ordre%type,
    a_pers_id_creation  prestation_adr_client.pers_id_creation%type);

--
procedure controle_prestation_bascule ( a_prest_id jefy_recette.PRESTATION.prest_id%TYPE);
procedure controle_facture_bascule (a_fap_id       jefy_recette.FACTURE_papier.fap_id%TYPE);

END;

/

