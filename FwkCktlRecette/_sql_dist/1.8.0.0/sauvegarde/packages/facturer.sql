--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package FACTURER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."FACTURER" IS

--
-- procedures appelees par le package api
--

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE);

PROCEDURE log_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE);

PROCEDURE ins_facture_ctrl_action (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_facture_ctrl_analytique (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_facture_ctrl_convention (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_facture_ctrl_planco (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
	  a_chaine			  VARCHAR2);
          
PROCEDURE upd_date_facture_restrinct (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE);
END;

/

