--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package API
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API" IS

--
--
-- procedures publiques a executer par les applications clientes.
--
--

-- Crée une recette indépendante (recette + adresse recette + facture )
-- Delegue la creation recette + facture a ins_facture_recette
-- puis complete en creant l'adresse sur la recette papier.
PROCEDURE ins_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Crée une recette indépendante (recette + facture)
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Update une recette simple indépendante
-- en ajoutant la gestion de l'adresse de la recette papier.
PROCEDURE upd_facture_recette_adresse (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

-- Update une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Delete une recette simple indépendante
-- Gère le triplet facture-recette-recette_papier
PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

-- Insère une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Update une facture simple
-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2);

-- Delete une facture simple
PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE);

-- insère une recette papier simple visible
PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero IN OUT	  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

-- Insère une recette
-- si rpp_id renseigné, on l'utilise et ignore le rib_ordre et mor_ordre
-- si rpp_id null, crée la recette papier non visible avec le rib et le mor
--
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Permet de recetter en automatique une facture (recette totale)
-- en créant aussi la recette papier non visible
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE upd_recette_ctrl_planco (
      a_rpco_id             RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE upd_facture_eche_id (
      a_fac_id              FACTURE.fac_id%TYPE,
	  a_eche_id				FACTURE.eche_id%TYPE);

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           RECETTE_PAPIER.utl_ordre%TYPE);

-- Supprime une recette simple
PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           RECETTE.utl_ordre%TYPE);

-- Reduit une recette existante
-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Reduit une recette existante en inserant l'adresse client.
PROCEDURE ins_reduction_adresse (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2,
	  a_rpp_adc_adr_ordre   RECETTE_PAPIER_ADR_CLIENT.adr_ordre%TYPE,
	  a_rpp_adc_pers_id     RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%TYPE);

PROCEDURE upd_reduction_ctrl_planco (
      a_rpco_id              RECETTE_CTRL_PLANCO.rpco_id%TYPE,
	  a_tit_id				RECETTE_CTRL_PLANCO.tit_id%TYPE);

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

END;

/

