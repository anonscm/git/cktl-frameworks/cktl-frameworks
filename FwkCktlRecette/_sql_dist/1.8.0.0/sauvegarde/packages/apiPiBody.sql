--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body API_PI
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."API_PI" 
IS

PROCEDURE ins_eng_fac (
    a_pef_id IN OUT	  PI_ENG_FAC.pef_id%TYPE,
	a_eng_id  		  PI_ENG_FAC.eng_id%TYPE,
	a_fac_id  		  PI_ENG_FAC.fac_id%TYPE,
	a_prest_id	  	  PI_ENG_FAC.prest_id%TYPE
   ) IS
     my_eng_exe_ordre		v_engage_budget.exe_ordre%TYPE;
     my_fac_exe_ordre		FACTURE.exe_ordre%TYPE;
     my_eng_tyap_id			v_engage_budget.tyap_id%TYPE;
     my_fac_tyap_id			FACTURE.tyap_id%TYPE;
     my_eng_fou_ordre		v_engage_budget.fou_ordre%TYPE;
     my_fac_fou_ordre		FACTURE.fou_ordre%TYPE;

   	 my_nb INTEGER;
   BEGIN
	  -- verifs
	  SELECT COUNT(*) INTO my_nb FROM v_engage_budget WHERE eng_id = a_eng_id;
	  IF my_nb=0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'L''engagement n''existe pas (eng_id='||a_eng_id||') (api_pi.ins_eng_fac)');
	  END IF;
	  SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = a_fac_id;
	  IF my_nb=0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (api_pi.ins_eng_fac)');
	  END IF;

	  -- autorise n factures pour un engagement, pas l'inverse (une facture n'est utilisee qu'une seule fois en pi)
--	  SELECT COUNT(*) INTO my_nb FROM PI_ENG_FAC WHERE fac_id = a_fac_id;
--	  IF my_nb > 0 THEN
--	  	 RAISE_APPLICATION_ERROR(-20001,'Cette facture est deja utilisee en prestation interne (eng_id='||a_eng_id||', fac_id='||a_fac_id||') (api_pi.ins_eng_fac)');
--	  END IF;

	  SELECT exe_ordre, tyap_id, fou_ordre INTO my_eng_exe_ordre, my_eng_tyap_id, my_eng_fou_ordre FROM v_engage_budget WHERE eng_id = a_eng_id;
	  SELECT exe_ordre, tyap_id, fou_ordre INTO my_fac_exe_ordre, my_fac_tyap_id, my_fac_fou_ordre FROM FACTURE WHERE fac_id = a_fac_id;

	  -- exercices
	  IF my_eng_exe_ordre <> my_fac_exe_ordre THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'L''engagement et la facture sont sur un exercice different (api_pi.ins_eng_fac)');
	  END IF;

	  -- types application ==> autorise pour l'instant uniquement le type prestation interne
	  IF my_eng_tyap_id <> get_type_application_pi THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'L''engagement n''est pas un engagement de type prestation interne (api_pi.ins_eng_fac)');
	  END IF;
	  IF my_fac_tyap_id <> get_type_application_pi THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture n''est pas une facture de type prestation interne (api_pi.ins_eng_fac)');
	  END IF;

	  -- fournisseurs
	  --IF my_eng_fou_ordre = my_fac_fou_ordre THEN
	  --	 RAISE_APPLICATION_ERROR(-20001,'L''engagement et la facture sont sur le meme fournisseur (api_pi.ins_eng_fac)');
	  --END IF;

	  -- tout semble ok ? insertion...
	  IF a_pef_id IS NULL THEN
	  	 SELECT pi_eng_fac_seq.NEXTVAL INTO a_pef_id FROM dual;
	  END IF;
	  INSERT INTO PI_ENG_FAC VALUES (a_pef_id, a_eng_id, a_fac_id, a_prest_id, SYSDATE);

   END;

PROCEDURE del_eng_fac (
    a_pef_id		  PI_ENG_FAC.pef_id%TYPE
   ) IS
   	 my_nb			 INTEGER;
   BEGIN
  	 SELECT COUNT(*) INTO my_nb FROM PI_DEP_REC WHERE pef_id = a_pef_id;
  	 IF my_nb > 0 THEN
	    RAISE_APPLICATION_ERROR(-20001,'Cette prestation interne est deja liquidee/recettee (pef_id='||a_pef_id||') (api_pi.del_eng_fac)');
	 END IF;
     DELETE FROM PI_ENG_FAC WHERE pef_id = a_pef_id;
   END;

PROCEDURE del_eng_fac_facture (
    a_fac_id		  PI_ENG_FAC.fac_id%TYPE
   ) IS
     CURSOR liste IS SELECT pef_id FROM PI_ENG_FAC WHERE fac_id = a_fac_id;
	 my_pef_id		 PI_ENG_FAC.pef_id%TYPE;
   BEGIN
      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_pef_id;
		 EXIT WHEN liste%NOTFOUND;

		 del_eng_fac(my_pef_id);

	  END LOOP;
	  CLOSE liste;
   END;

PROCEDURE ins_dep_rec (
    a_pdr_id IN OUT	  PI_DEP_REC.pdr_id%TYPE,
	a_dep_id		  PI_DEP_REC.dep_id%TYPE,
	a_rec_id		  PI_DEP_REC.rec_id%TYPE
   ) IS
     my_dep_exe_ordre		v_depense_budget.exe_ordre%TYPE;
	 my_eng_id				v_depense_budget.dep_id%TYPE;

     my_rec_exe_ordre		RECETTE.exe_ordre%TYPE;
	 my_fac_id				RECETTE.fac_id%TYPE;

	 my_pef_id				PI_ENG_FAC.pef_id%TYPE;

	 my_dep_ttc_saisie		v_depense_budget.dep_ttc_saisie%TYPE;
	 my_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE;

   	 my_nb INTEGER;
   BEGIN
	  -- verifs
	  SELECT COUNT(*) INTO my_nb FROM v_depense_budget WHERE dep_id = a_dep_id;
	  IF my_nb=0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La depense n''existe pas (dep_id='||a_dep_id||') (api_pi.ins_dep_rec)');
	  END IF;
	  SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
	  IF my_nb=0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (api_pi.ins_dep_rec)');
	  END IF;

	  -- deja existante ?
	  -- limite 1 depense = 1 recette
--	  SELECT COUNT(*) INTO my_nb FROM PI_DEP_REC WHERE dep_id = a_dep_id;
--	  IF my_nb > 0 THEN
--	  	 RAISE_APPLICATION_ERROR(-20001,'Cette depense est deja utilisee en prestation interne (dep_id='||a_dep_id||') (api_pi.ins_dep_rec)');
--	  END IF;
--	  SELECT COUNT(*) INTO my_nb FROM PI_DEP_REC WHERE rec_id = a_rec_id;
--	  IF my_nb > 0 THEN
--	  	 RAISE_APPLICATION_ERROR(-20001,'Cette recette est deja utilisee en prestation interne (rec_id='||a_rec_id||') (api_pi.ins_dep_rec)');
--	  END IF;

	  SELECT exe_ordre, eng_id INTO my_dep_exe_ordre, my_eng_id FROM v_depense_budget WHERE dep_id = a_dep_id;
	  SELECT exe_ordre, fac_id INTO my_rec_exe_ordre, my_fac_id FROM RECETTE WHERE rec_id = a_rec_id;

	  -- exercices
	  IF my_dep_exe_ordre <> my_rec_exe_ordre THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La depense et la recette sont sur un exercice different (api_pi.ins_dep_rec)');
	  END IF;

	  -- pi_eng_fac
	  SELECT COUNT(*) INTO my_nb FROM PI_ENG_FAC WHERE eng_id = my_eng_id AND fac_id = my_fac_id;
	  IF my_nb <> 1 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Cette prestation interne n''existe pas entre l''engagement et la facture (eng_id='||my_eng_id||', fac_id='||my_fac_id||') (api_pi.ins_dep_rec)');
	  END IF;
	  SELECT pef_id INTO my_pef_id FROM PI_ENG_FAC WHERE eng_id = my_eng_id AND fac_id = my_fac_id;

	  -- montants
	  SELECT dep_ttc_saisie INTO my_dep_ttc_saisie FROM v_depense_budget WHERE dep_id = a_dep_id;
	  SELECT rec_ttc_saisie INTO my_rec_ttc_saisie FROM RECETTE WHERE rec_id = a_rec_id;
	  IF my_dep_ttc_saisie > my_rec_ttc_saisie THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Montant depense (liquidation) different du montant recette (api_pi.ins_dep_rec)');
	  END IF;

	  -- tout semble ok ? insertion...
	  IF a_pdr_id IS NULL THEN
	  	 SELECT pi_dep_rec_seq.NEXTVAL INTO a_pdr_id FROM dual;
	  END IF;
	  INSERT INTO PI_DEP_REC VALUES (a_pdr_id, my_pef_id, a_dep_id, a_rec_id, SYSDATE);

   END;

PROCEDURE del_dep_rec (
    a_pdr_id		  PI_DEP_REC.pdr_id%TYPE
   )
   IS
     my_rec_id		  PI_DEP_REC.rec_id%TYPE;
   	 my_nb INTEGER;
   BEGIN
	  SELECT COUNT(*) INTO my_nb FROM PI_DEP_REC WHERE pdr_id = a_pdr_id;
	  IF my_nb > 0 THEN
	     SELECT rec_id INTO my_rec_id FROM PI_DEP_REC WHERE pdr_id = a_pdr_id;
   	     DELETE FROM PI_DEP_REC WHERE pdr_id = a_pdr_id;
	  END IF;
   END;

PROCEDURE del_dep_rec_recette (
    a_rec_id		  PI_DEP_REC.rec_id%TYPE
   ) IS
     CURSOR liste IS SELECT pdr_id FROM PI_DEP_REC WHERE rec_id = a_rec_id;
	 my_pdr_id		 PI_DEP_REC.pdr_id%TYPE;
   BEGIN
      OPEN liste();
  	  LOOP
		 FETCH liste INTO my_pdr_id;
		 EXIT WHEN liste%NOTFOUND;

      	 DELETE FROM PI_DEP_REC WHERE pdr_id = my_pdr_id;

	  END LOOP;
	  CLOSE liste;

   END;

FUNCTION get_type_application_pi
   RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN Type_Application.get_type_prestation_interne;
   END;

FUNCTION get_type_application_pe
   RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN Type_Application.get_type_prestation_externe;
   END;

FUNCTION get_mode_paiement_pi (a_exe_ordre PARAMETRES.exe_ordre%TYPE)
   RETURN v_mode_paiement.mod_ordre%TYPE
   IS
   	 my_nb        INTEGER;
	 my_mod_code  v_mode_paiement.mod_code%TYPE;
	 my_mod_ordre v_mode_paiement.mod_ordre%TYPE;
   BEGIN
      my_mod_code := Get_Parametre(a_exe_ordre, 'MODE_PAIEMENT_PI');
	  IF my_mod_code IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Le mode de paiement des PI n''est pas parametre (parametre MODE_PAIEMENT_PI dans la table PARAMETRES) (api_pi.get_mode_paiement_pi)');
	  END IF;

      SELECT COUNT(*) INTO my_nb FROM v_mode_paiement WHERE mod_code = my_mod_code AND exe_ordre = a_exe_ordre;
	  IF my_nb = 1 THEN
	  	 SELECT mod_ordre INTO my_mod_ordre FROM v_mode_paiement WHERE mod_code = my_mod_code AND exe_ordre = a_exe_ordre;
	  	 RETURN my_mod_ordre;
	  END IF;

	  RETURN NULL;
   END;

FUNCTION get_mode_recouvrement_pi (a_exe_ordre PARAMETRES.exe_ordre%TYPE)
   RETURN v_mode_recouvrement.mod_ordre%TYPE
   IS
   	 my_nb        INTEGER;
	 my_mod_code  v_mode_recouvrement.mod_code%TYPE;
	 my_mod_ordre v_mode_recouvrement.mod_ordre%TYPE;
   BEGIN
      my_mod_code := Get_Parametre(a_exe_ordre, 'MODE_RECOUVREMENT_PI');
	  IF my_mod_code IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Le mode de recouvrement des PI n''est pas parametre (parametre MODE_RECOUVREMENT_PI dans la table PARAMETRES) (api_pi.get_mode_recouvrement_pi)');
	  END IF;

      SELECT COUNT(*) INTO my_nb FROM v_mode_recouvrement WHERE mod_code = my_mod_code AND exe_ordre = a_exe_ordre;
	  IF my_nb = 1 THEN
	  	 SELECT mod_ordre INTO my_mod_ordre FROM v_mode_recouvrement WHERE mod_code = my_mod_code AND exe_ordre = a_exe_ordre;
	  	 RETURN my_mod_ordre;
	  END IF;

	  RETURN NULL;
   END;

END;

/

