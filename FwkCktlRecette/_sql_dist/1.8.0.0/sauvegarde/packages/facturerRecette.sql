--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package FACTURER_RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."FACTURER_RECETTER" IS

--
-- procedures appelees par le package api
--

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT   RECETTE.rec_numero%TYPE,
	  a_rpp_numero 	  		RECETTE_PAPIER.rpp_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT   RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE ins_ctrl_action (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
      a_rec_id            RECETTE.rec_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_ctrl_analytique (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
      a_rec_id            RECETTE.rec_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_ctrl_convention (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
      a_rec_id            RECETTE.rec_id%TYPE,
	  a_chaine			  VARCHAR2);

PROCEDURE ins_ctrl_planco (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
      a_rec_id            RECETTE.rec_id%TYPE,
	  a_chaine			  VARCHAR2,
	  a_chaine_tva		  VARCHAR2,
	  a_chaine_ctp		  VARCHAR2);
END;

/

