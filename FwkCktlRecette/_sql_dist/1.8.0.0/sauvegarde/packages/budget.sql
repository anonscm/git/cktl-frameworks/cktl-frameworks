--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package BUDGET
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."BUDGET" IS

--
-- procedures a appeler par les autres packages pour le lien eventuel avec budget.
--

PROCEDURE ins_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE upd_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE del_facture (
          a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
		  a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

PROCEDURE ins_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

PROCEDURE upd_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

PROCEDURE del_recette (
		  a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
          a_org_id       jefy_admin.organ.org_id%TYPE,
          a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
          a_montant      jefy_recette.RECETTE.rec_montant_budgetaire%TYPE);

--
-- fonction de calcul de montant budgetaire.
--

FUNCTION calculer_budgetaire (
		 a_exe_ordre     jefy_admin.exercice.exe_ordre%TYPE,
		 a_tap_id        jefy_recette.FACTURE.tap_id%TYPE,
		 a_org_id	     jefy_recette.FACTURE.org_id%TYPE,
		 a_montant_ht    jefy_recette.FACTURE.fac_ht_saisie%TYPE,
		 a_montant_ttc   jefy_recette.FACTURE.fac_ttc_saisie%TYPE)
  RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;

FUNCTION get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE)
     RETURN jefy_recette.FACTURE.fac_montant_budgetaire%TYPE;

PROCEDURE get_disponible (
		 a_exe_ordre    jefy_admin.exercice.exe_ordre%TYPE,
         a_org_id       jefy_admin.organ.org_id%TYPE,
         a_tcd_ordre    jefy_admin.type_credit.tcd_ordre%TYPE,
		 a_disponible	OUT jefy_recette.FACTURE.fac_montant_budgetaire%TYPE);

END;

/

