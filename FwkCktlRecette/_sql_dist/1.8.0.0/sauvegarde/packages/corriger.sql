--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package CORRIGER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."CORRIGER" IS

PROCEDURE maj_restes_ctrl (
	  a_fac_id		        FACTURE.fac_id%TYPE);

PROCEDURE maj_restes_ctrl_action (
	  a_fac_id					  FACTURE.fac_id%TYPE);

PROCEDURE maj_restes_ctrl_analytique (
	  a_fac_id					  FACTURE.fac_id%TYPE);

PROCEDURE maj_restes_ctrl_convention (
	  a_fac_id					  FACTURE.fac_id%TYPE);

PROCEDURE maj_restes_ctrl_planco (
	  a_fac_id					  FACTURE.fac_id%TYPE);

END;

/

