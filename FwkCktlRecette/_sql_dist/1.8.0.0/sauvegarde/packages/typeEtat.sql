--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package TYPE_ETAT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."TYPE_ETAT" IS

FUNCTION get_etat_valide
   RETURN v_type_etat.tyet_id%TYPE;

FUNCTION get_etat_annule
   RETURN v_type_etat.tyet_id%TYPE;

FUNCTION get_etat_canal_valide
   RETURN v_type_etat.tyet_id%TYPE;

FUNCTION get_etat_canal_utilisable
   RETURN v_type_etat.tyet_id%TYPE;

FUNCTION get_etat_canal_public
   RETURN v_type_etat.tyet_id%TYPE;

FUNCTION get_etat(a_tyet_libelle v_type_etat.tyet_libelle%TYPE)
      RETURN v_type_etat.tyet_id%TYPE;

END;

/

