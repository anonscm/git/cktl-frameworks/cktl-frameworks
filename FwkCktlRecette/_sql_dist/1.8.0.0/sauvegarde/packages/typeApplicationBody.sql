--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body TYPE_APPLICATION
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."TYPE_APPLICATION" 
IS

   FUNCTION get_type_depense
      RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN get_type('DEPENSE');
   END;

   FUNCTION get_type_recette
      RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN get_type('RECETTE');
   END;

   FUNCTION get_type_prestation_interne
      RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN get_type('PRESTATION INTERNE');
   END;

   FUNCTION get_type_prestation_externe
      RETURN v_type_application.tyap_id%TYPE
   IS
   BEGIN
      RETURN get_type('PRESTATION EXTERNE');
   END;

   FUNCTION get_type(a_tyap_libelle v_type_application.tyap_libelle%TYPE)
      RETURN v_type_application.tyap_id%TYPE
   IS
      my_tyap_id   v_type_application.tyap_id%TYPE;
   BEGIN
      my_tyap_id := Get_Type_Application(a_tyap_libelle);

	  IF my_tyap_id IS NULL THEN
         RAISE_APPLICATION_ERROR(-20001, 'Probleme : Le type application "'||a_tyap_libelle||'" n''existe pas !');
	  END IF;

	  RETURN my_tyap_id;
   END;

END;

/

