--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package REGROUPER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."REGROUPER" IS

  TYPE prest_id_array IS TABLE OF integer index by binary_integer;
  TYPE ref_cursor IS REF CURSOR;

  PROCEDURE regrouper_prestation(a_chaine varchar2, a_utl_ordre prestation.utl_ordre%type);
  function rempli_prest_id_array(a_chaine varchar2) return prest_id_array;
  function select_for_prest_id_array(a_prestation_ref prestation%rowtype, a_array prest_id_array) return varchar2;
  function where_for_prest_id_array(a_array prest_id_array) return varchar2;
  procedure ajouter_a_la_prestation(a_prest_id in out prestation.prest_id%type, a_prestation prestation%rowtype, a_utl_ordre prestation.utl_ordre%type);

END; 

/

