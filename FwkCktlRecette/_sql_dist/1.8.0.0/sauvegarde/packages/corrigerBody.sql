--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body CORRIGER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."CORRIGER" 
IS

PROCEDURE maj_restes_ctrl (
	  a_fac_id		        FACTURE.fac_id%TYPE
   ) IS
	 my_reste		  FACTURE.fac_montant_budgetaire_reste%TYPE;
   BEGIN
        SELECT fac_montant_budgetaire_reste INTO my_reste FROM FACTURE WHERE fac_id = a_fac_id;

		IF my_reste > 0 THEN
   		  maj_restes_ctrl_action(a_fac_id);
   		  maj_restes_ctrl_analytique(a_fac_id);
   		  maj_restes_ctrl_convention(a_fac_id);
   		  maj_restes_ctrl_planco(a_fac_id);
		ELSE
		  UPDATE FACTURE_CTRL_ACTION SET fact_montant_budgetaire_reste = 0 WHERE fac_id = a_fac_id;
		  UPDATE FACTURE_CTRL_ANALYTIQUE SET fana_montant_budgetaire_reste = 0 WHERE fac_id = a_fac_id;
		  UPDATE FACTURE_CTRL_CONVENTION SET fcon_montant_budgetaire_reste = 0 WHERE fac_id = a_fac_id;
		  UPDATE FACTURE_CTRL_PLANCO SET fpco_ht_reste = 0, fpco_tva_reste = 0 WHERE fac_id = a_fac_id;
		END IF;
   END;

PROCEDURE maj_restes_ctrl_action (
	  a_fac_id					  FACTURE.fac_id%TYPE
   ) IS
     CURSOR liste IS
	   SELECT lolf_id, SUM(ract_ht_saisie), SUM(ract_ttc_saisie) FROM RECETTE_CTRL_ACTION
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND lolf_id IN (SELECT lolf_id FROM FACTURE_CTRL_ACTION WHERE fac_id=a_fac_id)
		 GROUP BY lolf_id
		 UNION ALL
	   SELECT lolf_id, SUM(ract_ht_saisie), SUM(ract_ttc_saisie) FROM RECETTE_CTRL_ACTION
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND lolf_id NOT IN (SELECT lolf_id FROM FACTURE_CTRL_ACTION WHERE fac_id=a_fac_id)
		 GROUP BY lolf_id;

	 my_recette_ht		      		  RECETTE_CTRL_ACTION.ract_ht_saisie%TYPE;
	 my_recette_ttc		      		  RECETTE_CTRL_ACTION.ract_ttc_saisie%TYPE;
	 my_recette_lolf_id	      		  RECETTE_CTRL_ACTION.lolf_id%TYPE;
	 my_fact_montant_bud_reste        FACTURE_CTRL_ACTION.fact_montant_budgetaire_reste%TYPE;
	 my_exe_ordre					  FACTURE.exe_ordre%TYPE;
	 my_fact_id						  FACTURE_CTRL_ACTION.fact_id%TYPE;
	 my_org_id						  FACTURE.org_id%TYPE;
	 my_tcd_ordre					  FACTURE.tcd_ordre%TYPE;
	 my_tap_id						  FACTURE.tap_id%TYPE;
	 my_montant_budgetaire            RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
     my_nb INTEGER;

	 -- récupère les lignes dans l'ordre (même lolf_id + lolf_id pas utilisés ds recette_ctrl + lolf_id utilisés)
     CURSOR liste2 IS
	   SELECT fact_id, fact_montant_budgetaire_reste FROM FACTURE_CTRL_ACTION
	     WHERE fac_id=a_fac_id AND fact_montant_budgetaire_reste > 0
	     AND lolf_id = my_recette_lolf_id
	   UNION ALL
	   SELECT fact_id, fact_montant_budgetaire_reste FROM FACTURE_CTRL_ACTION
	     WHERE fac_id=a_fac_id AND fact_montant_budgetaire_reste > 0
	     AND lolf_id <> my_recette_lolf_id
		 AND lolf_id NOT IN (
		 	 SELECT lolf_id FROM RECETTE_CTRL_ACTION WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
	   UNION ALL
	   SELECT fact_id, fact_montant_budgetaire_reste FROM FACTURE_CTRL_ACTION
	     WHERE fac_id=a_fac_id AND fact_montant_budgetaire_reste > 0
	     AND lolf_id <> my_recette_lolf_id
		 AND lolf_id IN (
		 	 SELECT lolf_id FROM RECETTE_CTRL_ACTION WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id));

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (maj_facture_ctrl_action)');
		END IF;

		UPDATE FACTURE_CTRL_ACTION SET fact_montant_budgetaire_reste = fact_montant_budgetaire
		  WHERE fac_id = a_fac_id;

		SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
		  FROM FACTURE WHERE fac_id = a_fac_id;

        OPEN liste();
  		LOOP
		   FETCH liste INTO my_recette_lolf_id, my_recette_ht, my_recette_ttc;
		   EXIT WHEN liste%NOTFOUND;

		   my_montant_budgetaire := Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
             my_recette_ht,my_recette_ttc);

		   -- on modifie tous les controleurs, dans l'ordre ou ca vient, en commencant
		   -- par les codes identiques...
		   IF my_montant_budgetaire > 0 THEN
           	  OPEN liste2();
  			  LOOP
		   	  	  FETCH liste2 INTO my_fact_id, my_fact_montant_bud_reste;
		   		  EXIT WHEN liste2%NOTFOUND;

				  IF my_fact_montant_bud_reste >= my_montant_budgetaire
				  THEN
				  	  UPDATE FACTURE_CTRL_ACTION
					    SET fact_montant_budgetaire_reste = fact_montant_budgetaire_reste - my_montant_budgetaire
					    WHERE fact_id = my_fact_id;
				  ELSE
				  	  UPDATE FACTURE_CTRL_ACTION
					    SET fact_montant_budgetaire_reste = 0 WHERE fact_id = my_fact_id;
				  END IF;
			  	  my_montant_budgetaire := my_montant_budgetaire - my_fact_montant_bud_reste;
			  	  IF my_montant_budgetaire <= 0 THEN
			  	  	 EXIT;
			  	  END IF;

			  END LOOP;
			  CLOSE liste2;
		   END IF;

		END LOOP;
		CLOSE liste;
   END;

PROCEDURE maj_restes_ctrl_analytique (
	  a_fac_id					  FACTURE.fac_id%TYPE
   ) IS
     CURSOR liste IS
	   SELECT can_id, SUM(rana_ht_saisie), SUM(rana_ttc_saisie) FROM RECETTE_CTRL_ANALYTIQUE
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND can_id IN (SELECT can_id FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id=a_fac_id)
		 GROUP BY can_id
		 UNION ALL
	   SELECT can_id, SUM(rana_ht_saisie), SUM(rana_ttc_saisie) FROM RECETTE_CTRL_ANALYTIQUE
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND can_id NOT IN (SELECT can_id FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id=a_fac_id)
		 GROUP BY can_id;

	 my_recette_ht		      		  RECETTE_CTRL_ANALYTIQUE.rana_ht_saisie%TYPE;
	 my_recette_ttc		      		  RECETTE_CTRL_ANALYTIQUE.rana_ttc_saisie%TYPE;
	 my_recette_can_id	      		  RECETTE_CTRL_ANALYTIQUE.can_id%TYPE;
	 my_fana_bud_reste        		  FACTURE_CTRL_ANALYTIQUE.fana_montant_budgetaire_reste%TYPE;
	 my_fac_bud_reste        		  FACTURE.fac_montant_budgetaire_reste%TYPE;
	 my_exe_ordre					  FACTURE.exe_ordre%TYPE;
	 my_fana_id						  FACTURE_CTRL_ANALYTIQUE.fana_id%TYPE;
	 my_org_id						  FACTURE.org_id%TYPE;
	 my_tcd_ordre					  FACTURE.tcd_ordre%TYPE;
	 my_tap_id						  FACTURE.tap_id%TYPE;
	 my_montant_budgetaire            RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
     my_nb INTEGER;

	 -- récupère les lignes dans l'ordre (même can_id + can_id pas utilisés ds recette_ctrl + can_id utilisés)
     CURSOR liste2 IS
	   SELECT fana_id, fana_montant_budgetaire_reste FROM FACTURE_CTRL_ANALYTIQUE
	     WHERE fac_id=a_fac_id AND fana_montant_budgetaire_reste > 0
	     AND can_id = my_recette_can_id
	   UNION ALL
	   SELECT fana_id, fana_montant_budgetaire_reste FROM FACTURE_CTRL_ANALYTIQUE
	     WHERE fac_id=a_fac_id AND fana_montant_budgetaire_reste > 0
	     AND can_id <> my_recette_can_id
		 AND can_id NOT IN (
		 	 SELECT can_id FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
	   UNION ALL
	   SELECT fana_id, fana_montant_budgetaire_reste FROM FACTURE_CTRL_ANALYTIQUE
	     WHERE fac_id=a_fac_id AND fana_montant_budgetaire_reste > 0
	     AND can_id <> my_recette_can_id
		 AND can_id IN (
		 	 SELECT can_id FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id));

     CURSOR liste3 IS
	   SELECT fana_id, fana_montant_budgetaire_reste FROM FACTURE_CTRL_ANALYTIQUE
	     WHERE fac_id=a_fac_id AND fana_montant_budgetaire_reste > 0;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (maj_facture_ctrl_analytique)');
		END IF;

		UPDATE FACTURE_CTRL_ANALYTIQUE SET fana_montant_budgetaire_reste = fana_montant_budgetaire
		  WHERE fac_id = a_fac_id;

		SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
		  FROM FACTURE WHERE fac_id = a_fac_id;

        OPEN liste();
  		LOOP
		   FETCH liste INTO my_recette_can_id, my_recette_ht, my_recette_ttc;
		   EXIT WHEN liste%NOTFOUND;

		   my_montant_budgetaire := Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
             my_recette_ht,my_recette_ttc);

		   -- on modifie tous les controleurs, dans l'ordre ou ca vient, en commencant
		   -- par les codes identiques...
		   IF my_montant_budgetaire > 0 THEN
           	  OPEN liste2();
  			  LOOP
		   	  	  FETCH liste2 INTO my_fana_id, my_fana_bud_reste;
		   		  EXIT WHEN liste2%NOTFOUND;

				  IF my_fana_bud_reste >= my_montant_budgetaire
				  THEN
				  	  UPDATE FACTURE_CTRL_ANALYTIQUE
					    SET fana_montant_budgetaire_reste = fana_montant_budgetaire_reste - my_montant_budgetaire
					    WHERE fana_id = my_fana_id;
				  ELSE
				  	  UPDATE FACTURE_CTRL_ANALYTIQUE
					    SET fana_montant_budgetaire_reste = 0 WHERE fana_id = my_fana_id;
				  END IF;
			  	  my_montant_budgetaire := my_montant_budgetaire - my_fana_bud_reste;
			  	  IF my_montant_budgetaire <= 0 THEN
			  	  	 EXIT;
			  	  END IF;

			  END LOOP;
			  CLOSE liste2;
		   END IF;

		END LOOP;
		CLOSE liste;

		-- si le reste facture sur code analytique superieur au reste facture total
		-- (donc y'a des recettes sans code analytique ou moins que la recette)
		-- on ramene le reste code analytique au reste facture...
		SELECT fac_montant_budgetaire_reste INTO my_fac_bud_reste
		  FROM FACTURE WHERE fac_id = a_fac_id;

		SELECT NVL(SUM(fana_montant_budgetaire_reste),0) INTO my_fana_bud_reste
		  FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id = a_fac_id;

		IF my_fana_bud_reste > my_fac_bud_reste THEN

		   my_montant_budgetaire := my_fana_bud_reste - my_fac_bud_reste;

           OPEN liste3();
  		   LOOP
		   	  FETCH liste3 INTO my_fana_id, my_fana_bud_reste;
		   	  EXIT WHEN liste3%NOTFOUND;

			  IF my_fana_bud_reste >= my_montant_budgetaire
			  THEN
			  	  UPDATE FACTURE_CTRL_ANALYTIQUE
				    SET fana_montant_budgetaire_reste = fana_montant_budgetaire_reste - my_montant_budgetaire
				    WHERE fana_id = my_fana_id;
			  ELSE
			  	  UPDATE FACTURE_CTRL_ANALYTIQUE
				    SET fana_montant_budgetaire_reste = 0 WHERE fana_id = my_fana_id;
			  END IF;
			  my_montant_budgetaire := my_montant_budgetaire - my_fana_bud_reste;
			  IF my_montant_budgetaire <= 0 THEN
			  	 EXIT;
			  END IF;

		   END LOOP;
		   CLOSE liste3;
		END IF;

   END;

PROCEDURE maj_restes_ctrl_convention (
	  a_fac_id					  FACTURE.fac_id%TYPE
   ) IS
     CURSOR liste IS
	   SELECT con_ordre, SUM(rcon_ht_saisie), SUM(rcon_ttc_saisie) FROM RECETTE_CTRL_CONVENTION
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND con_ordre IN (SELECT con_ordre FROM FACTURE_CTRL_CONVENTION WHERE fac_id=a_fac_id)
		 GROUP BY con_ordre
		 UNION ALL
	   SELECT con_ordre, SUM(rcon_ht_saisie), SUM(rcon_ttc_saisie) FROM RECETTE_CTRL_CONVENTION
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND con_ordre NOT IN (SELECT con_ordre FROM FACTURE_CTRL_CONVENTION WHERE fac_id=a_fac_id)
		 GROUP BY con_ordre;

	 my_recette_ht		      		  RECETTE_CTRL_CONVENTION.rcon_ht_saisie%TYPE;
	 my_recette_ttc		      		  RECETTE_CTRL_CONVENTION.rcon_ttc_saisie%TYPE;
	 my_recette_con_ordre      		  RECETTE_CTRL_CONVENTION.con_ordre%TYPE;
	 my_fcon_bud_reste        		  FACTURE_CTRL_CONVENTION.fcon_montant_budgetaire_reste%TYPE;
	 my_fac_bud_reste        		  FACTURE.fac_montant_budgetaire_reste%TYPE;
	 my_exe_ordre					  FACTURE.exe_ordre%TYPE;
	 my_fcon_id						  FACTURE_CTRL_CONVENTION.fcon_id%TYPE;
	 my_org_id						  FACTURE.org_id%TYPE;
	 my_tcd_ordre					  FACTURE.tcd_ordre%TYPE;
	 my_tap_id						  FACTURE.tap_id%TYPE;
	 my_montant_budgetaire            RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
     my_nb INTEGER;

	 -- récupère les lignes dans l'ordre (même con_ordre + con_ordre pas utilisés ds recette_ctrl + con_ordre utilisés)
     CURSOR liste2 IS
	   SELECT fcon_id, fcon_montant_budgetaire_reste FROM FACTURE_CTRL_CONVENTION
	     WHERE fac_id=a_fac_id AND fcon_montant_budgetaire_reste > 0
	     AND con_ordre = my_recette_con_ordre
	   UNION ALL
	   SELECT fcon_id, fcon_montant_budgetaire_reste FROM FACTURE_CTRL_CONVENTION
	     WHERE fac_id=a_fac_id AND fcon_montant_budgetaire_reste > 0
	     AND con_ordre <> my_recette_con_ordre
		 AND con_ordre NOT IN (
		 	 SELECT con_ordre FROM RECETTE_CTRL_CONVENTION WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
	   UNION ALL
	   SELECT fcon_id, fcon_montant_budgetaire_reste FROM FACTURE_CTRL_CONVENTION
	     WHERE fac_id=a_fac_id AND fcon_montant_budgetaire_reste > 0
	     AND con_ordre <> my_recette_con_ordre
		 AND con_ordre IN (
		 	 SELECT con_ordre FROM RECETTE_CTRL_CONVENTION WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id));

     CURSOR liste3 IS
	   SELECT fcon_id, fcon_montant_budgetaire_reste FROM FACTURE_CTRL_CONVENTION
	     WHERE fac_id=a_fac_id AND fcon_montant_budgetaire_reste > 0;

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (maj_facture_ctrl_convention)');
		END IF;

		UPDATE FACTURE_CTRL_CONVENTION SET fcon_montant_budgetaire_reste = fcon_montant_budgetaire
		  WHERE fac_id = a_fac_id;

		SELECT exe_ordre, org_id, tcd_ordre, tap_id INTO my_exe_ordre, my_org_id, my_tcd_ordre, my_tap_id
		  FROM FACTURE WHERE fac_id = a_fac_id;

        OPEN liste();
  		LOOP
		   FETCH liste INTO my_recette_con_ordre, my_recette_ht, my_recette_ttc;
		   EXIT WHEN liste%NOTFOUND;

		   my_montant_budgetaire := Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
             my_recette_ht,my_recette_ttc);

		   -- on modifie tous les controleurs, dans l'ordre ou ca vient, en commencant
		   -- par les codes identiques...
		   IF my_montant_budgetaire > 0 THEN
           	  OPEN liste2();
  			  LOOP
		   	  	  FETCH liste2 INTO my_fcon_id, my_fcon_bud_reste;
		   		  EXIT WHEN liste2%NOTFOUND;

				  IF my_fcon_bud_reste >= my_montant_budgetaire
				  THEN
				  	  UPDATE FACTURE_CTRL_CONVENTION
					    SET fcon_montant_budgetaire_reste = fcon_montant_budgetaire_reste - my_montant_budgetaire
					    WHERE fcon_id = my_fcon_id;
				  ELSE
				  	  UPDATE FACTURE_CTRL_CONVENTION
					    SET fcon_montant_budgetaire_reste = 0 WHERE fcon_id = my_fcon_id;
				  END IF;
			  	  my_montant_budgetaire := my_montant_budgetaire - my_fcon_bud_reste;
			  	  IF my_montant_budgetaire <= 0 THEN
			  	  	 EXIT;
			  	  END IF;

			  END LOOP;
			  CLOSE liste2;
		   END IF;

		END LOOP;
		CLOSE liste;

		-- si le reste facture sur convention superieur au reste facture total
		-- (donc y'a des recettes sans convention ou moins que la recette)
		-- on ramene le reste convention au reste facture...
		SELECT fac_montant_budgetaire_reste INTO my_fac_bud_reste
		  FROM FACTURE WHERE fac_id = a_fac_id;

		SELECT NVL(SUM(fcon_montant_budgetaire_reste),0) INTO my_fcon_bud_reste
		  FROM FACTURE_CTRL_CONVENTION WHERE fac_id = a_fac_id;

		IF my_fcon_bud_reste > my_fac_bud_reste THEN

		   my_montant_budgetaire := my_fcon_bud_reste - my_fac_bud_reste;

           OPEN liste3();
  		   LOOP
		   	  FETCH liste3 INTO my_fcon_id, my_fcon_bud_reste;
		   	  EXIT WHEN liste3%NOTFOUND;

			  IF my_fcon_bud_reste >= my_montant_budgetaire
			  THEN
			  	  UPDATE FACTURE_CTRL_CONVENTION
				    SET fcon_montant_budgetaire_reste = fcon_montant_budgetaire_reste - my_montant_budgetaire
				    WHERE fcon_id = my_fcon_id;
			  ELSE
			  	  UPDATE FACTURE_CTRL_CONVENTION
				    SET fcon_montant_budgetaire_reste = 0 WHERE fcon_id = my_fcon_id;
			  END IF;
			  my_montant_budgetaire := my_montant_budgetaire - my_fcon_bud_reste;
			  IF my_montant_budgetaire <= 0 THEN
			  	 EXIT;
			  END IF;

		   END LOOP;
		   CLOSE liste3;
		END IF;

   END;

PROCEDURE maj_restes_ctrl_planco (
	  a_fac_id					  FACTURE.fac_id%TYPE
   ) IS
     CURSOR liste1_1 IS
	   SELECT pco_num, SUM(rpco_ht_saisie) FROM RECETTE_CTRL_PLANCO
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND pco_num IN (SELECT pco_num FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id)
		 GROUP BY pco_num
	   UNION ALL
	   SELECT pco_num, SUM(rpco_ht_saisie) FROM RECETTE_CTRL_PLANCO
	     WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)
		 AND pco_num NOT IN (SELECT pco_num FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id)
		 GROUP BY pco_num;

     CURSOR liste2_1 IS
	   SELECT pco_num, SUM(rpcotva_tva_saisie) FROM RECETTE_CTRL_PLANCO_TVA
	     WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
		 AND pco_num IN (SELECT pco_num FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id)
		 GROUP BY pco_num
	   UNION ALL
	   SELECT pco_num, SUM(rpcotva_tva_saisie) FROM RECETTE_CTRL_PLANCO_TVA
	     WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
		 AND pco_num NOT IN (SELECT pco_num FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id)
		 GROUP BY pco_num;

	 my_recette_ht		      		  RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
	 my_recette_tva		      		  RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
	 my_recette_pco_num      		  RECETTE_CTRL_PLANCO.pco_num%TYPE;
	 my_recette_tva_pco_num    		  RECETTE_CTRL_PLANCO_TVA.pco_num%TYPE;
	 my_fpco_ht_reste        		  FACTURE_CTRL_PLANCO.fpco_ht_reste%TYPE;
	 my_fpco_tva_reste        		  FACTURE_CTRL_PLANCO.fpco_tva_reste%TYPE;
	 my_fpco_id						  FACTURE_CTRL_PLANCO.fpco_id%TYPE;
     my_nb INTEGER;

	 -- récupère les lignes dans l'ordre (même pco_num + pco_num pas utilisés ds recette_ctrl + pco_num utilisés)
     CURSOR liste1_2 IS
	   SELECT fpco_id, fpco_ht_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_ht_reste > 0
	     AND pco_num = my_recette_pco_num
	   UNION ALL
	   SELECT fpco_id, fpco_ht_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_ht_reste > 0
	     AND pco_num <> my_recette_pco_num
		 AND pco_num NOT IN (
		 	 SELECT pco_num FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id))
	   UNION ALL
	   SELECT fpco_id, fpco_ht_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_ht_reste > 0
	     AND pco_num <> my_recette_pco_num
		 AND pco_num IN (
		 	 SELECT pco_num FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id));

	 -- récupère les lignes dans l'ordre (même pco_num + pco_num pas utilisés ds recette_ctrl + pco_num utilisés)
     CURSOR liste2_2 IS
	   SELECT fpco_id, fpco_tva_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_tva_reste > 0
	     AND pco_num = my_recette_tva_pco_num
	   UNION ALL
	   SELECT fpco_id, fpco_tva_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_tva_reste > 0
	     AND pco_num <> my_recette_tva_pco_num
		 AND pco_num NOT IN (
		 	 SELECT pco_num FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)))
	   UNION ALL
	   SELECT fpco_id, fpco_tva_reste FROM FACTURE_CTRL_PLANCO
	     WHERE fac_id=a_fac_id AND fpco_tva_reste > 0
	     AND pco_num <> my_recette_tva_pco_num
		 AND pco_num IN (
		 	 SELECT pco_num FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id IN (
			 		SELECT rec_id FROM RECETTE WHERE fac_id=a_fac_id)));

   BEGIN
        SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (maj_facture_ctrl_planco)');
		END IF;

		-- le ht
		UPDATE FACTURE_CTRL_PLANCO SET fpco_ht_reste = fpco_ht_saisie
		  WHERE fac_id = a_fac_id;

        OPEN liste1_1();
  		LOOP
		   FETCH liste1_1 INTO my_recette_pco_num, my_recette_ht;
		   EXIT WHEN liste1_1%NOTFOUND;

		   -- on modifie tous les controleurs, dans l'ordre ou ca vient, en commencant
		   -- par les codes identiques...
		   IF my_recette_ht > 0 THEN
           	  OPEN liste1_2();
  			  LOOP
		   	  	  FETCH liste1_2 INTO my_fpco_id, my_fpco_ht_reste;
		   		  EXIT WHEN liste1_2%NOTFOUND;

				  IF my_fpco_ht_reste >= my_recette_ht
				  THEN
				  	  UPDATE FACTURE_CTRL_PLANCO
					    SET fpco_ht_reste = fpco_ht_reste - my_recette_ht
					    WHERE fpco_id = my_fpco_id;
				  ELSE
				  	  UPDATE FACTURE_CTRL_PLANCO
					    SET fpco_ht_reste = 0 WHERE fpco_id = my_fpco_id;
				  END IF;
			  	  my_recette_ht := my_recette_ht - my_fpco_ht_reste;
			  	  IF my_recette_ht <= 0 THEN
			  	  	 EXIT;
			  	  END IF;

			  END LOOP;
			  CLOSE liste1_2;
		   END IF;

		END LOOP;
		CLOSE liste1_1;

		-- la tva
		UPDATE FACTURE_CTRL_PLANCO SET fpco_tva_reste = fpco_tva_saisie
		  WHERE fac_id = a_fac_id;

        OPEN liste2_1();
  		LOOP
		   FETCH liste2_1 INTO my_recette_tva_pco_num, my_recette_tva;
		   EXIT WHEN liste2_1%NOTFOUND;

		   -- on modifie tous les controleurs, dans l'ordre ou ca vient, en commencant
		   -- par les codes identiques...
		   -- sauf qu'en tva y'en n'a pas de codes identiques... donc ça fera sur tous, tant pis...
		   IF my_recette_tva > 0 THEN
           	  OPEN liste2_2();
  			  LOOP
		   	  	  FETCH liste2_2 INTO my_fpco_id, my_fpco_tva_reste;
		   		  EXIT WHEN liste2_2%NOTFOUND;

				  IF my_fpco_tva_reste >= my_recette_tva
				  THEN
				  	  UPDATE FACTURE_CTRL_PLANCO
					    SET fpco_tva_reste = fpco_tva_reste - my_recette_tva
					    WHERE fpco_id = my_fpco_id;
				  ELSE
				  	  UPDATE FACTURE_CTRL_PLANCO
					    SET fpco_tva_reste = 0 WHERE fpco_id = my_fpco_id;
				  END IF;
			  	  my_recette_tva := my_recette_tva - my_fpco_tva_reste;
			  	  IF my_recette_tva <= 0 THEN
			  	  	 EXIT;
			  	  END IF;

			  END LOOP;
			  CLOSE liste2_2;
		   END IF;

		END LOOP;
		CLOSE liste2_1;
   END;

END;

/

