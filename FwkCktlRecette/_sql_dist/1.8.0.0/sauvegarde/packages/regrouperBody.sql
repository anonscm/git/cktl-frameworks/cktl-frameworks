--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body REGROUPER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."REGROUPER" 
IS

  PROCEDURE regrouper_prestation(a_chaine varchar2, a_utl_ordre prestation.utl_ordre%type)
  is
     my_nb               integer;
     my_index            integer;
     my_prestation       prestation%rowtype;
     my_prestation_ref   prestation%rowtype;
	 my_prest_id_array   prest_id_array;
     my_prest_id_new     prestation.prest_id%type;
     
     my_select           varchar2(3000);
     my_cursor           ref_cursor;

  begin
     
     -- on remplit le tableau des prest_id
     my_prest_id_array:=rempli_prest_id_array(a_chaine);

     -- on repasse la liste en prenant a chaque fois la prestation comme modele
     for my_nb in my_prest_id_array.first .. my_prest_id_array.last 
     loop
         if my_prest_id_array(my_nb) is not null then
                       
              select * into my_prestation_ref from prestation where prest_id=my_prest_id_array(my_nb);
              my_prest_id_array(my_nb):=null;
                            
              my_select:=select_for_prest_id_array(my_prestation_ref, my_prest_id_array);
              my_prest_id_new:=null;
            
              dbms_output.put_line('ref : '||my_prestation_ref.prest_id);
              
              if my_select is not null then 
                  OPEN my_cursor FOR my_select;
                  LOOP
                      FETCH my_cursor INTO my_prestation;
                      EXIT WHEN my_cursor%NOTFOUND;

                      dbms_output.put_line('->'||my_prestation.prest_id);
     -- reprendre la methode regroupePrestations de la classe org.cocktail.kava.client.factory.FatoryPrestation


     --  - on stocke les engagements correspondants pour suppression ulterieure
     
     --  - on degage les commandes jefy
     
     
                      -- si 1ere, créer prestation a partir de celle de reference
                      if my_prest_id_new is null then
                         ajouter_a_la_prestation(my_prest_id_new , my_prestation_ref, a_utl_ordre);

                         api_prestation.DECLOTURE_PRESTATION(my_prestation_ref.prest_id, a_utl_ordre);
                         api_prestation.DEVALIDE_PRESTATION_PREST(my_prestation_ref.prest_id, a_utl_ordre);
                         api_prestation.DEVALIDE_PRESTATION_CLIENT(my_prestation_ref.prest_id, a_utl_ordre);
                         update prestation set tyet_id=2 where prest_id=my_prestation_ref.prest_id;
                         change_prestation_dt(my_prestation_ref.prest_id, my_prest_id_new);
                      end if;
                      
                      -- ajouter la prestation du cursor a la nouvelle
                      ajouter_a_la_prestation(my_prest_id_new , my_prestation, a_utl_ordre);

                      api_prestation.DECLOTURE_PRESTATION(my_prestation.prest_id, a_utl_ordre);
                      api_prestation.DEVALIDE_PRESTATION_PREST(my_prestation.prest_id, a_utl_ordre);
                      api_prestation.DEVALIDE_PRESTATION_CLIENT(my_prestation.prest_id, a_utl_ordre);
                      update prestation set tyet_id=2 where prest_id=my_prestation.prest_id;
                      change_prestation_dt(my_prestation.prest_id, my_prest_id_new);
                      
                      -- on la supprime du tableau a traiter
                      for my_index in my_prest_id_array.first .. my_prest_id_array.last 
                      loop
                           if my_prest_id_array(my_index) is not null and my_prest_id_array(my_index)=my_prestation.prest_id then
                              my_prest_id_array(my_index):=null;
                           end if;
                      end loop;
         
                  END LOOP;
                  CLOSE my_cursor;
                  
                  if my_prest_id_new is not null then
                     api_prestation.VALIDE_PRESTATION_CLIENT(my_prest_id_new, a_utl_ordre);
                     api_prestation.VALIDE_PRESTATION_PREST(my_prest_id_new, a_utl_ordre);
                     api_prestation.CLOTURE_PRESTATION(my_prest_id_new, a_utl_ordre);
                  end if;
              end if;
         end if;
     end loop;
  
     
  end;

  function rempli_prest_id_array(a_chaine varchar2) return prest_id_array
  is
     my_nb               integer;
     my_array_count      integer;
     my_chaine	         VARCHAR2(30000);
     my_prest_id         prestation.prest_id%type;
	 my_prest_id_array   prest_id_array;  
  begin
     my_chaine:=a_chaine;
     my_array_count:=1;    
     LOOP
   	     IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

         SELECT grhum.en_nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_prest_id FROM dual;
		 my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

         select count(*) into my_nb from prestation where prest_id=my_prest_id and tyet_id=1 and prest_date_facturation is null and prest_date_valide_client is not null
            and prest_date_valide_prest is not null and prest_date_cloture is not null;
        
         if my_nb>0 then
            my_prest_id_array(my_array_count):=my_prest_id;
            my_array_count:=my_array_count+1;
         end if;
     END LOOP;

     return my_prest_id_array;
  end;

  function select_for_prest_id_array(a_prestation_ref prestation%rowtype, a_array prest_id_array) return varchar2
  is
    my_nb                 integer;
    my_chaine             varchar2(3000);
    my_prestation_bud_client_ref prestation_budget_client%rowtype;
    
    my_depense_org_id     prestation_budget_client.ORG_ID%type;
    my_depense_tcd_ordre  prestation_budget_client.tcd_ordre%type;
    my_depense_tap_id     prestation_budget_client.tap_ID%type;
    my_depense_lolf_id    prestation_budget_client.lolf_ID%type;
    my_depense_pco_num    prestation_budget_client.pco_num%type;
    my_depense_can_id     prestation_budget_client.can_ID%type;
    my_depense_con_ordre  prestation_budget_client.con_ordre%type;
  begin
     -- on limite aux prest_id en parametre
     my_chaine:=where_for_prest_id_array(a_array);
     if my_chaine is null then return my_chaine; end if;
     
     my_chaine:='select p.* from prestation p, prestation_budget_client pbc, jefy_depense.engage_budget e '||
        'where p.prest_id=pbc.prest_id and pbc.eng_id=e.eng_id(+) and p.tyet_id=1 and '||my_chaine;
     
     
     -- reprendre la methode testRegroupementPossible de la classe org.cocktail.kava.client.factory.FatoryPrestation

     -- infos recette
     my_chaine:=my_chaine||' and p.exe_ordre='||a_prestation_ref.exe_ordre;
     my_chaine:=my_chaine||' and p.pers_id='||a_prestation_ref.pers_id;

     if a_prestation_ref.cat_id is not null then 
        my_chaine:=my_chaine||' and p.cat_id='||a_prestation_ref.cat_id;
     else
        my_chaine:=my_chaine||' and p.cat_id is null';
     end if;
     if a_prestation_ref.org_id is not null then
        my_chaine:=my_chaine||' and p.org_id='||a_prestation_ref.org_id;
     else
        my_chaine:=my_chaine||' and p.org_id is null';
     end if;
     if a_prestation_ref.tap_id is not null then
        my_chaine:=my_chaine||' and p.tap_id='||a_prestation_ref.tap_id;
     else
        my_chaine:=my_chaine||' and p.tap_id is null';
     end if;
     if a_prestation_ref.tcd_ordre is not null then
        my_chaine:=my_chaine||' and p.tcd_ordre='||a_prestation_ref.tcd_ordre;
     else
        my_chaine:=my_chaine||' and p.tcd_ordre is null';
     end if;
     if a_prestation_ref.lolf_id is not null then
        my_chaine:=my_chaine||' and p.lolf_id='||a_prestation_ref.lolf_id;
     else
        my_chaine:=my_chaine||' and p.lolf_id is null';
     end if;
     if a_prestation_ref.can_id is not null then
        my_chaine:=my_chaine||' and p.can_id='||a_prestation_ref.can_id;
     else
        my_chaine:=my_chaine||' and p.can_id is null';
     end if;
     if a_prestation_ref.con_ordre is not null then
        my_chaine:=my_chaine||' and p.con_ordre='||a_prestation_ref.con_ordre;
     else
        my_chaine:=my_chaine||' and p.con_ordre is null';
     end if;
     if a_prestation_ref.pco_num is not null then
        my_chaine:=my_chaine||' and p.pco_num='''||a_prestation_ref.pco_num||'''';
     else
        my_chaine:=my_chaine||' and p.pco_num is null';
     end if;
     my_chaine:=my_chaine||' and p.fou_ordre_prest='||a_prestation_ref.fou_ordre_prest;
     my_chaine:=my_chaine||' and p.fou_ordre='||a_prestation_ref.fou_ordre;

     -- infos depense
     my_depense_org_id:=null;
     my_depense_tcd_ordre:=null;
     my_depense_tap_id:=null;
     my_depense_lolf_id:=null;
     my_depense_pco_num:=null;
     my_depense_can_id:=null;
     my_depense_con_ordre:=null;
     
     select count(*) into my_nb from prestation_budget_client where prest_id=a_prestation_ref.prest_id;
     if my_nb>0 then
        select * into my_prestation_bud_client_ref from prestation_budget_client where prest_id=a_prestation_ref.prest_id;
        
        if my_prestation_bud_client_ref.eng_id is not null then 
           select org_id, tcd_ordre, tap_id into my_depense_org_id, my_depense_tcd_ordre, my_depense_tap_id 
              from jefy_depense.engage_budget where eng_id=my_prestation_bud_client_ref.eng_id;
           select max(tyac_id) into my_depense_lolf_id from jefy_depense.engage_ctrl_action where eng_id=my_prestation_bud_client_ref.eng_id;
           select max(pco_num) into my_depense_pco_num from jefy_depense.engage_ctrl_planco where eng_id=my_prestation_bud_client_ref.eng_id;

           select nvl(max(can_id), null) into my_depense_can_id from jefy_depense.engage_ctrl_analytique where eng_id=my_prestation_bud_client_ref.eng_id;
           select nvl(max(conv_ordre), null) into my_depense_con_ordre from jefy_depense.engage_ctrl_convention where eng_id=my_prestation_bud_client_ref.eng_id;
        else
           my_depense_org_id:=my_prestation_bud_client_ref.org_id;
           my_depense_tcd_ordre:=my_prestation_bud_client_ref.tcd_ordre;
           my_depense_tap_id:=my_prestation_bud_client_ref.tap_id;
           my_depense_lolf_id:=my_prestation_bud_client_ref.lolf_id;
           my_depense_pco_num:=my_prestation_bud_client_ref.pco_num;
           my_depense_can_id:=my_prestation_bud_client_ref.can_id;
           my_depense_con_ordre:=my_prestation_bud_client_ref.con_ordre;
        end if;
        
     end if;

     if my_depense_org_id is not null then
        my_chaine:=my_chaine||' and (pbc.org_id='||my_depense_org_id||' or e.org_id='||my_depense_org_id||')';
     else
        my_chaine:=my_chaine||' and (pbc.org_id is null and e.org_id is null)';
     end if;

     if my_depense_tcd_ordre is not null then
        my_chaine:=my_chaine||' and (pbc.tcd_ordre='||my_depense_tcd_ordre||' or e.tcd_ordre='||my_depense_tcd_ordre||')';
     else
        my_chaine:=my_chaine||' and (pbc.tcd_ordre is null and e.tcd_ordre is null)';
     end if;

     if my_depense_tap_id is not null then
        my_chaine:=my_chaine||' and (pbc.tap_id='||my_depense_tap_id||' or e.tap_id='||my_depense_tap_id||')';
     else
        my_chaine:=my_chaine||' and (pbc.tap_id is null and e.tap_id is null)';
     end if;

     if my_depense_lolf_id is not null then
        my_chaine:=my_chaine||' and (pbc.lolf_id='||my_depense_lolf_id||' or e.eng_id in (select eng_id from jefy_depense.engage_ctrl_action where tyac_id='||my_depense_lolf_id||'))';
     else
        my_chaine:=my_chaine||' and (pbc.lolf_id is null and (e.eng_id is null or (select count(*) from jefy_depense.engage_ctrl_action where eng_id=e.eng_id)=0))';
     end if;

     if my_depense_pco_num is not null then
        my_chaine:=my_chaine||' and (pbc.pco_num='''||my_depense_pco_num||''' or e.eng_id in (select eng_id from jefy_depense.engage_ctrl_planco where pco_num='''||my_depense_pco_num||'''))';
     else
        my_chaine:=my_chaine||' and (pbc.pco_num is null and (e.eng_id is null or (select count(*) from jefy_depense.engage_ctrl_planco where eng_id=e.eng_id)=0))';
     end if;

     if my_depense_can_id is not null then
        my_chaine:=my_chaine||' and (pbc.can_id='||my_depense_can_id||' or e.eng_id in (select eng_id from jefy_depense.engage_ctrl_analytique where can_id='||my_depense_can_id||'))';
     else
        my_chaine:=my_chaine||' and (pbc.can_id is null and (e.eng_id is null or (select count(*) from jefy_depense.engage_ctrl_analytique where eng_id=e.eng_id)=0))';
     end if;

     if my_depense_con_ordre is not null then
        my_chaine:=my_chaine||' and (pbc.con_ordre='||my_depense_con_ordre||' or e.eng_id in (select eng_id from jefy_depense.engage_ctrl_convention where conv_ordre='||my_depense_con_ordre||'))';
     else
        my_chaine:=my_chaine||' and (pbc.con_ordre is null and (e.eng_id is null or (select count(*) from jefy_depense.engage_ctrl_convention where eng_id=e.eng_id)=0))';
     end if;

--insert into AAAAAAA values(my_chaine);
--commit;
     -- on renvoie la requete     
     return my_chaine;

  end;
    
  function where_for_prest_id_array(a_array prest_id_array) return varchar2
  is
    my_nb       integer;
    my_chaine   varchar2(3000);
  begin
     my_chaine:=null;
     for my_nb in a_array.first .. a_array.last 
     loop
       if a_array(my_nb) is not null then
         if my_chaine is not null then my_chaine:=my_chaine||' or'; end if;
         my_chaine:=my_chaine||' p.prest_id='||to_char(a_array(my_nb));
       end if;
     end loop;
     
     if my_chaine is not null then my_chaine:=' ('||my_chaine||')'; end if;

     return my_chaine;
  end;
  
  procedure ajouter_a_la_prestation(a_prest_id in out prestation.prest_id%type, a_prestation prestation%rowtype, a_utl_ordre prestation.utl_ordre%type)
  is
    CURSOR curseur      IS SELECT * FROM prestation_ligne WHERE prest_id=a_prestation.prest_id;
    my_prestation_ligne prestation_ligne%rowtype;

    my_prestation_budget_client prestation_budget_client%rowtype;
    
    my_nb                       integer;
    
    my_org_id                   prestation_budget_client.org_id%type;
    my_tap_id                   prestation_budget_client.tap_id%type;
    my_tcd_ordre                prestation_budget_client.tcd_ordre%type;
    my_lolf_id                  prestation_budget_client.lolf_id%type;
    my_pco_num                  prestation_budget_client.pco_num%type;
    my_can_id                   prestation_budget_client.can_id%type;
    my_con_ordre                prestation_budget_client.con_ordre%type;
  
    my_pers_libelle    grhum.personne.pers_libelle%type;
    my_prest_total_ht  prestation.prest_total_ht%type;  
    my_prest_total_tva prestation.prest_total_tva%type;
    my_prest_total_ttc prestation.prest_total_ttc%type;
  begin
       if a_prest_id is null then 
          select prestation_seq.nextval into a_prest_id from dual;
          select p.pers_libelle into my_pers_libelle from grhum.personne p, grhum.fournis_ulr f where p.pers_id=f.pers_id and f.fou_ordre=a_prestation.fou_ordre;
          
          insert into prestation values (a_prest_id, Get_Numerotation(a_prestation.exe_ordre,null,'PRESTATION'), a_prestation.exe_ordre, a_prestation.cat_id,
             a_prestation.pers_id, a_prestation.fou_ordre, a_prestation.no_individu, a_utl_ordre, 'Devis cumulatif pour '||my_pers_libelle,
             sysdate, a_prestation.prest_commentaire_client, a_prestation.prest_commentaire_prest, null, null, null, null, a_prestation.prest_remise_globale,
             a_prestation.prest_apply_tva, a_prestation.mor_ordre, a_prestation.org_id, a_prestation.tap_id, a_prestation.tcd_ordre, a_prestation.lolf_id,
             a_prestation.pco_num, a_prestation.can_id, a_prestation.con_ordre, a_prestation.typu_id, a_prestation.tyet_id, a_prestation.prest_total_ht,
             a_prestation.prest_total_tva, a_prestation.prest_total_ttc, a_prestation.fou_ordre_prest);
             
          select * into my_prestation_budget_client from prestation_budget_client where prest_id=a_prestation.prest_id;
          
          my_org_id:=my_prestation_budget_client.org_id;
          if my_org_id is null and my_prestation_budget_client.eng_id is not null then
             select org_id into my_org_id from jefy_depense.engage_budget where eng_id=my_prestation_budget_client.eng_id;  
          end if;
          
          my_tap_id:=my_prestation_budget_client.tap_id;
          if my_tap_id is null and my_prestation_budget_client.eng_id is not null then
             select tap_id into my_tap_id from jefy_depense.engage_budget where eng_id=my_prestation_budget_client.eng_id;  
          end if;

          my_tcd_ordre:=my_prestation_budget_client.tcd_ordre;
          if my_tcd_ordre is null and my_prestation_budget_client.eng_id is not null then
             select tcd_ordre into my_tcd_ordre from jefy_depense.engage_budget where eng_id=my_prestation_budget_client.eng_id;  
          end if;

          my_lolf_id:=my_prestation_budget_client.lolf_id;
          if my_lolf_id is null and my_prestation_budget_client.eng_id is not null then
             select max(tyac_id) into my_lolf_id from jefy_depense.engage_ctrl_action where eng_id=my_prestation_budget_client.eng_id;  
          end if;

          my_pco_num:=my_prestation_budget_client.pco_num;
          if my_pco_num is null and my_prestation_budget_client.eng_id is not null then
             select max(pco_num) into my_pco_num from jefy_depense.engage_ctrl_planco where eng_id=my_prestation_budget_client.eng_id;  
          end if;

          my_can_id:=my_prestation_budget_client.can_id;
          if my_can_id is null and my_prestation_budget_client.eng_id is not null then
             select count(*) into my_nb from jefy_depense.engage_ctrl_analytique where eng_id=my_prestation_budget_client.eng_id;
             if my_nb>0 then 
                select max(can_id) into my_can_id from jefy_depense.engage_ctrl_analytique where eng_id=my_prestation_budget_client.eng_id;
             end if;  
          end if;

          my_con_ordre:=my_prestation_budget_client.con_ordre; 
          if my_con_ordre is null and my_prestation_budget_client.eng_id is not null then
             select count(*) into my_nb from jefy_depense.engage_ctrl_convention where eng_id=my_prestation_budget_client.eng_id;
             if my_nb>0 then 
                select max(conv_ordre) into my_con_ordre from jefy_depense.engage_ctrl_convention where eng_id=my_prestation_budget_client.eng_id;
             end if;  
          end if;
          
          insert into prestation_budget_client values(a_prest_id, my_org_id, my_tap_id, my_tcd_ordre, my_lolf_id, my_pco_num, my_can_id, my_con_ordre, null);
          
       else
           select prest_total_ht, prest_total_tva, prest_total_ttc into my_prest_total_ht, my_prest_total_tva, my_prest_total_ttc
             from prestation where prest_id=a_prest_id;

           my_prest_total_ht:=my_prest_total_ht+a_prestation.prest_total_ht;
           my_prest_total_tva:=my_prest_total_tva+a_prestation.prest_total_tva;
           my_prest_total_ttc:=my_prest_total_ttc+a_prestation.prest_total_ttc;
           
           update prestation set prest_total_ht=my_prest_total_ht, prest_total_tva=my_prest_total_tva, prest_total_ttc=my_prest_total_ttc where prest_id=a_prest_id;            
       end if;

       open curseur();
       loop
   			fetch curseur into my_prestation_ligne;
		 	exit when curseur%notfound;

            insert into prestation_ligne select prestation_ligne_seq.nextval, a_prest_id, null, my_prestation_ligne.caar_id, sysdate, my_prestation_ligne.prlig_reference,
              my_prestation_ligne.prlig_description, my_prestation_ligne.prlig_art_ht, my_prestation_ligne.prlig_art_ttc, my_prestation_ligne.prlig_art_ttc_initial,
              my_prestation_ligne.prlig_total_ht, my_prestation_ligne.prlig_total_ttc, my_prestation_ligne.prlig_total_reste_ht, my_prestation_ligne.prlig_total_reste_ttc,
              my_prestation_ligne.prlig_quantite, my_prestation_ligne.prlig_quantite_reste, my_prestation_ligne.tva_id, my_prestation_ligne.tva_id_initial,
              my_prestation_ligne.tyar_id, my_prestation_ligne.pco_num from dual;
       end loop;
       close curseur;
  end;
end;

/

