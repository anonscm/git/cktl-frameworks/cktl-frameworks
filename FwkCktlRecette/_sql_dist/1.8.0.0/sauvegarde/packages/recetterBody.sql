--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."RECETTER" 
IS

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE,
	  a_rpp_visible			  RECETTE_PAPIER.rpp_visible%TYPE
   ) IS
	   my_rpp_id			 RECETTE_PAPIER.rpp_id%TYPE;
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;
	   my_rpp_tva_saisie	 RECETTE_PAPIER.rpp_tva_saisie%TYPE;
   BEGIN
		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rpp_tva_saisie := Recetter_Outils.get_tva(a_rpp_ht_saisie, a_rpp_ttc_saisie);

	   	-- enregistrement dans la table.
		IF a_rpp_id IS NULL THEN
	       SELECT recette_papier_seq.NEXTVAL INTO a_rpp_id FROM dual;
        END IF;

		my_rpp_numero := a_rpp_numero;
		IF my_rpp_numero IS NULL THEN
   		   my_rpp_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE_PAPIER');
   		END IF;

		Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mor_ordre, a_exe_ordre);

		INSERT INTO JEFY_RECETTE.RECETTE_PAPIER (
   			   RPP_ID, EXE_ORDRE, RPP_NUMERO, RPP_HT_SAISIE, RPP_TVA_SAISIE, RPP_TTC_SAISIE,
   			   PERS_ID, FOU_ORDRE, RIB_ORDRE,
   			   MOR_ORDRE, RPP_DATE_RECETTE, RPP_DATE_SAISIE, RPP_DATE_RECEPTION,
			   RPP_DATE_SERVICE_FAIT, RPP_NB_PIECE, UTL_ORDRE, RPP_VISIBLE)
		VALUES (
			   a_rpp_id, a_exe_ordre, my_rpp_numero, a_rpp_ht_saisie, my_rpp_tva_saisie, a_rpp_ttc_saisie,
   			   a_pers_id, a_fou_ordre, a_rib_ordre,
   			   a_mor_ordre, a_rpp_date_recette, SYSDATE, a_rpp_date_reception,
   			   a_RPP_DATE_SERVICE_FAIT, a_RPP_NB_PIECE, a_UTL_ORDRE, a_rpp_visible);

   END;

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE
   ) IS
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;
	   my_rpp_tva_saisie	 RECETTE_PAPIER.rpp_tva_saisie%TYPE;
	   my_rpp_visible		 RECETTE_PAPIER.rpp_visible%TYPE;
	   my_exe_ordre			 RECETTE_PAPIER.exe_ordre%TYPE;
	   my_nb INTEGER;
   BEGIN
		IF a_rpp_id IS NULL THEN
		   RAISE_APPLICATION_ERROR(-20001,'Il faut un rpp_id !! (upd_recette_papier)');
        END IF;

   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette papier n''existe pas (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;

		SELECT rpp_visible, exe_ordre INTO my_rpp_visible, my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
		IF my_rpp_visible <> 'O' THEN
		   RAISE_APPLICATION_ERROR(-20001,'Cette recette papier ne doit pas etre modifiee directement, passer par la recette correspondante (rpp_id='||a_rpp_id||') (upd_recette_papier)');
		END IF;

		Verifier.verifier_util_recette_papier(a_rpp_id);

		IF a_rpp_ht_saisie<0 OR a_rpp_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pour une reduction de recette, il faut utiliser le package "reduire" (ins_recette_papier)');
		END IF;

		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rpp_tva_saisie := Recetter_Outils.get_tva(a_rpp_ht_saisie, a_rpp_ttc_saisie);

		my_rpp_numero := a_rpp_numero;
		IF my_rpp_numero IS NULL THEN
   		   my_rpp_numero := Get_Numerotation(my_exe_ordre, NULL, 'RECETTE_PAPIER');
   		END IF;

		Verifier.verifier_rib(a_fou_ordre, a_rib_ordre, a_mor_ordre, my_exe_ordre);

		UPDATE JEFY_RECETTE.RECETTE_PAPIER SET RPP_NUMERO = my_rpp_numero, RPP_HT_SAISIE = a_rpp_ht_saisie,
			   RPP_TVA_SAISIE = my_rpp_tva_saisie, RPP_TTC_SAISIE = a_rpp_ttc_saisie,
   			   PERS_ID = a_pers_id, FOU_ORDRE = a_fou_ordre, RIB_ORDRE = a_rib_ordre,
   			   MOR_ORDRE = a_mor_ordre, RPP_DATE_RECETTE = a_rpp_date_recette, RPP_DATE_SAISIE = SYSDATE, RPP_DATE_RECEPTION = a_rpp_date_reception,
			   RPP_DATE_SERVICE_FAIT = a_rpp_date_service_fait, RPP_NB_PIECE = a_rpp_nb_piece, UTL_ORDRE = a_utl_ordre
			   WHERE rpp_id = a_rpp_id;

   END;

PROCEDURE ins_recette_papier_adr_client (
  a_rpp_id            RECETTE_PAPIER_ADR_CLIENT.rpp_id%type,
  a_adr_ordre         RECETTE_PAPIER_ADR_CLIENT.adr_ordre%type,
  a_pers_id_creation  RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%type)
IS
BEGIN
    INSERT INTO JEFY_RECETTE.RECETTE_PAPIER_ADR_CLIENT (
            RPPADC_ID, RPP_ID, ADR_ORDRE, DATE_CREATION, DATE_FIN, PERS_ID_CREATION)
    VALUES (recette_papier_adr_client_seq.nextval,
            a_rpp_id,
            a_adr_ordre,
            sysdate,
            null,
            a_pers_id_creation);
END;

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
       my_nb                 INTEGER;
	   my_par_value          PARAMETRES.par_value%TYPE;

	   my_org_id			 FACTURE.org_id%TYPE;
	   my_tap_id			 FACTURE.tap_id%TYPE;
	   my_exe_ordre			 FACTURE.exe_ordre%TYPE;
	   my_tcd_ordre			 FACTURE.tcd_ordre%TYPE;
	   my_fou_ordre			 FACTURE.fou_ordre%TYPE;
	   my_pers_id			 FACTURE.pers_id%TYPE;
	   my_fac_id			 FACTURE.fac_id%TYPE;
	   my_rpp_numero		 RECETTE_PAPIER.rpp_numero%TYPE;

	   my_montant_budgetaire RECETTE.rec_montant_budgetaire%TYPE;
	   my_fac_budgetaire     FACTURE.fac_montant_budgetaire%TYPE;
	   my_fac_reste          FACTURE.fac_montant_budgetaire_reste%TYPE;
	   my_fac_init           FACTURE.fac_montant_budgetaire%TYPE;
	   my_rec_fac_budgetaire FACTURE.fac_montant_budgetaire%TYPE;

	   my_rec_tva_saisie	 RECETTE.rec_tva_saisie%TYPE;
   BEGIN
		IF a_rec_ht_saisie<0 OR a_rec_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Pour une reduction de recette, il faut utiliser le package "reduire" (ins_recette)');
		END IF;

		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

		SELECT exe_ordre, org_id, tap_id, tcd_ordre, fac_montant_budgetaire_reste, fac_montant_budgetaire, fou_ordre, pers_id
		  INTO my_exe_ordre, my_org_id, my_tap_id, my_tcd_ordre, my_fac_reste, my_fac_init, my_fou_ordre, my_pers_id
		  FROM FACTURE WHERE fac_id=a_fac_id;

		Verifier.verifier_budget(a_exe_ordre, a_tap_id, my_org_id, my_tcd_ordre);

		-- verification de la coherence de l'exercice.
		IF a_exe_ordre<>my_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette doit etre sur le meme exercice que la facture (ins_recette)');
		END IF;

		-- verification de la coherence du prorata.
		IF Get_Parametre(a_exe_ordre, 'RECETTE_IDEM_TAP_ID')<>'NON' AND
		   my_tap_id<>a_tap_id THEN
		     RAISE_APPLICATION_ERROR(-20001, 'il faut que le taux de prorata de la recette soit le meme que la facture initiale (ins_recette)');
		END IF;

	    -- on verifie la coherence des montants.
 		IF ABS(a_rec_ht_saisie) > ABS(a_rec_ttc_saisie) THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette)');
		END IF;

		-- on teste la coherence entre le ht et le ttc puis on calcule la tva.
		my_rec_tva_saisie := Recetter_Outils.get_tva(a_rec_ht_saisie, a_rec_ttc_saisie);

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,my_org_id,
		      a_rec_ht_saisie,a_rec_ttc_saisie);

		-- on calcule pour diminuer le reste de la facture du montant recetté ou celui par rapport au tap_id.
		--    de la facture (si le tap_id peut etre different de celui de la facture).
		my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(a_exe_ordre, my_tap_id, a_tap_id,
	      my_montant_budgetaire, my_org_id, a_rec_ht_saisie, a_rec_ttc_saisie);

		-- si le reste facture est inferieur a 0, et qu'on passe une recette positive -> erreur.
		IF my_fac_reste<=0 AND my_fac_budgetaire>0 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture est deja soldee (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

		-- on verifie qu'on ne diminue pas plus que ce qu'il y a de reste de facture.
		IF my_fac_reste < my_fac_budgetaire THEN
		   --my_fac_budgetaire := my_fac_reste;
		   RAISE_APPLICATION_ERROR(-20001,'Le reste de la facture est inferieur au montant a recetter (fac_id='||a_fac_id||') (ins_recette)');
		END IF;

  		-- on diminue le reste de la facture.
        UPDATE FACTURE SET fac_montant_budgetaire_reste = fac_montant_budgetaire_reste - my_fac_budgetaire
		   WHERE fac_id=a_fac_id;

		IF a_rpp_id IS NULL
		THEN
	  		Recetter.ins_recette_papier(a_rpp_id, a_exe_ordre, my_rpp_numero, a_rec_ht_saisie, a_rec_ttc_saisie,
		   								my_pers_id, my_fou_ordre, a_rib_ordre, a_mor_ordre, SYSDATE, SYSDATE,
		   								SYSDATE, 1, a_utl_ordre, 'N');
	    END IF;

		-- insertion dans la table.
	    IF a_rec_id IS NULL THEN
	       SELECT recette_seq.NEXTVAL INTO a_rec_id FROM dual;
	    END IF;

		IF a_rec_numero IS NULL THEN
   		   a_rec_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE');
   		END IF;

		INSERT INTO RECETTE (
			   REC_ID, EXE_ORDRE, RPP_ID, REC_NUMERO, FAC_ID, REC_LIB,
   			   REC_DATE_SAISIE, REC_MONTANT_BUDGETAIRE, REC_HT_SAISIE, REC_TVA_SAISIE, REC_TTC_SAISIE,
			   TAP_ID, TYET_ID, UTL_ORDRE, REC_ID_REDUCTION)
		VALUES (a_rec_id, a_exe_ordre, a_rpp_id, a_rec_numero, a_fac_id, a_rec_lib,
			   SYSDATE, my_montant_budgetaire, a_rec_ht_saisie, my_rec_tva_saisie, a_rec_ttc_saisie,
			   a_tap_id, Type_Etat.get_etat_valide, a_utl_ordre, NULL);

		ins_recette_ctrl_action(a_exe_ordre, a_rec_id, a_chaine_action);
		ins_recette_ctrl_analytique(a_exe_ordre, a_rec_id, a_chaine_analytique);
		ins_recette_ctrl_convention(a_exe_ordre, a_rec_id, a_chaine_convention);
		ins_recette_ctrl_planco(a_exe_ordre, a_rec_id, a_chaine_planco);
		ins_recette_ctrl_planco_tva(a_exe_ordre, a_rec_id, a_chaine_planco_tva);
		ins_recette_ctrl_planco_ctp(a_exe_ordre, a_rec_id, a_chaine_planco_ctp);

		-- mise a jour des restes des ctrl de la facture
		Corriger.maj_restes_ctrl(a_fac_id);

		Budget.ins_recette(a_exe_ordre, my_org_id, my_tcd_ordre, my_montant_budgetaire);


          upd_date_recette_restrinct(a_exe_ordre,a_rec_id);
   END;

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqués, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE
   ) IS
   	 my_facture		  		FACTURE%ROWTYPE;
   	 my_fap_id              FACTURE_PAPIER.fap_id%TYPE;
   	 my_fap_adr_client		FACTURE_PAPIER_ADR_CLIENT%ROWTYPE;

	 my_somme_ht			FACTURE.fac_ht_saisie%TYPE;
	 my_somme_tva			FACTURE_CTRL_PLANCO.fpco_tva_saisie%TYPE;
	 my_somme_ttc			FACTURE_CTRL_PLANCO.fpco_ttc_saisie%TYPE;
	 my_pco_num_pere		FACTURE_CTRL_PLANCO.pco_num%TYPE;

	 my_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE;
	 my_tap_id				RECETTE.tap_id%TYPE;

	 my_rpp_id				RECETTE_PAPIER.rpp_id%TYPE;
	 my_rpp_numero			RECETTE_PAPIER.rpp_numero%TYPE;

     my_chaine_action		VARCHAR2(30000);
     my_chaine_analytique	VARCHAR2(30000);
     my_chaine_convention	VARCHAR2(30000);
     my_chaine_planco		VARCHAR2(30000);
     my_chaine_planco_tva	VARCHAR2(30000);
     my_chaine_planco_ctp	VARCHAR2(30000);

   	 my_nb                  INTEGER;
   	 my_nbFapAdrClient      INTEGER;
   BEGIN
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;
	  IF my_nb = 0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.RECETTE WHERE fac_id = a_fac_id;
	  IF my_nb > 0 THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'La facture a deja ete recettee, impossible de recetter automatiquement cette facture (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;

	  -- recup des infos de la facture
	  SELECT * INTO my_facture FROM jefy_recette.FACTURE WHERE fac_id = a_fac_id;

	  -- recuperation de l'adresse de la facture papier (si elles existent)
	  SELECT COUNT(*) INTO my_nb FROM jefy_recette.FACTURE_PAPIER where fac_id = a_fac_id;
	  IF my_nb > 0 THEN
		SELECT fap_id INTO my_fap_id FROM jefy_recette.FACTURE_PAPIER WHERE fac_id = a_fac_id;
	  END IF;
	  my_nbFapAdrClient := 0;
	  SELECT COUNT(*) INTO my_nbFapAdrClient FROM jefy_recette.FACTURE_PAPIER_ADR_CLIENT WHERE fap_id = my_fap_id AND date_fin IS NULL;
      IF my_nbFapAdrClient = 1 THEN
        SELECT * INTO my_fap_adr_client FROM jefy_recette.FACTURE_PAPIER_ADR_CLIENT WHERE fap_id = my_fap_id AND date_fin IS NULL;
      END IF;

	  -- determination du mode de recouvrement
	  my_mor_ordre := a_mor_ordre;
	  IF my_mor_ordre IS NULL THEN
	  	 my_mor_ordre := my_facture.mor_ordre;
	  END IF;
	  IF my_mor_ordre IS NULL THEN
	  	 RAISE_APPLICATION_ERROR(-20001,'Il faut un mode de recouvrement pour la recette, la facture n''en a pas (fac_id='||a_fac_id||') (ins_recette_from_facture)');
	  END IF;

	  -- determination du taux de prorata
	  my_tap_id := a_tap_id;
	  IF my_tap_id IS NULL THEN
	  	 my_tap_id := my_facture.tap_id;
	  END IF;

	  -- construction des chaines
	  my_chaine_action := Recetter_Outils.chaine_action_from_facture(a_fac_id);
	  my_chaine_analytique := Recetter_Outils.chaine_analytique_from_facture(a_fac_id);
	  my_chaine_convention := Recetter_Outils.chaine_convention_from_facture(a_fac_id);
	  my_chaine_planco := Recetter_Outils.chaine_planco_from_facture(a_fac_id);
	  -- construction des chaines planco tva et planco contrepartie
	  SELECT NVL(SUM(fpco_tva_saisie), 0), NVL(SUM(fpco_ttc_saisie), 0)
	     INTO my_somme_tva, my_somme_ttc
		 FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;
	  -- tva : si pas de tva (=0), pas de compte tva
	  IF my_facture.fac_tva_saisie = 0 THEN
	  	 my_chaine_planco_tva := '$';
	  ELSE
	     -- sinon prend celui passé
	     IF a_pco_num_tva IS NOT NULL THEN
		 	SELECT COUNT(*) INTO my_nb FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;
			IF my_nb <> 1 THEN
	  	 	   RAISE_APPLICATION_ERROR(-20001,'Pas de facture_ctrl_planco, pas normal (fac_id='||a_fac_id||') (ins_recette_from_facture)');
			END IF;
		 	SELECT pco_num INTO my_pco_num_pere FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;

		 	my_chaine_planco_tva := my_pco_num_pere || '$' || a_pco_num_tva || '$' || my_somme_tva || '$$$';
		 ELSE
		    -- sinon construit en auto
			my_chaine_planco_tva := Recetter_Outils.chaine_planco_tva_from_facture(a_fac_id);
		 END IF;
	  END IF;
	  -- ctp : si passe en param, prend celui-ci
	  IF a_pco_num_ctp IS NOT NULL THEN
	  		SELECT COUNT(*) INTO my_nb FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;
			IF my_nb <> 1 THEN
	  	 	   RAISE_APPLICATION_ERROR(-20001,'Pas de facture_ctrl_planco, pas normal (fac_id='||a_fac_id||') (ins_recette_from_facture)');
			END IF;
		 	SELECT pco_num INTO my_pco_num_pere FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id AND ROWNUM=1;

	  	 	my_chaine_planco_ctp := my_pco_num_pere || '$' || a_pco_num_ctp || '$' || my_somme_ttc || '$$$';
	  ELSE
	     -- sinon construit en auto
	     my_chaine_planco_ctp := Recetter_Outils.chaine_planco_ctp_from_facture(a_fac_id, my_facture.mor_ordre);
	  END IF;

	  -- inserer la recette papier
	  ins_recette_papier(my_rpp_id, my_facture.exe_ordre, my_rpp_numero, my_facture.fac_ht_saisie, my_facture.fac_ttc_saisie,
		my_facture.pers_id, my_facture.fou_ordre, a_rib_ordre, my_mor_ordre, SYSDATE, SYSDATE,
		SYSDATE, 1, a_utl_ordre, 'N');

	  -- inserer l'adresse liee a la recette papier et provenant de la facture papier.
	  IF my_nbFapAdrClient = 1 THEN
        ins_recette_papier_adr_client(my_rpp_id, my_fap_adr_client.adr_ordre, my_fap_adr_client.pers_id_creation);
      END IF;

	  -- finalement inserer la recette
	  ins_recette(a_rec_id, my_facture.exe_ordre, my_rpp_id, a_rec_numero, a_fac_id, my_facture.fac_lib, a_rib_ordre, my_mor_ordre,
	     my_facture.fac_ht_saisie, my_facture.fac_ttc_saisie, my_tap_id, a_utl_ordre,
		 my_chaine_action, my_chaine_analytique, my_chaine_convention, my_chaine_planco,
		 my_chaine_planco_tva, my_chaine_planco_ctp);

   END;

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE
   ) IS
       my_nb         			INTEGER;
	   my_exe_ordre	 		    RECETTE_PAPIER.exe_ordre%TYPE;
   BEGIN
        SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;
	    IF my_nb=0 THEN
           RAISE_APPLICATION_ERROR(-20001, 'La recette papier n''existe pas ou est deja annule (rpp_id:'||a_rpp_id||')');
	    END IF;
   		SELECT exe_ordre INTO my_exe_ordre FROM RECETTE_PAPIER WHERE rpp_id=a_rpp_id;

		--Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		--Verifier.verifier_util_recette_papier(a_rpp_id);

		log_recette_papier(a_rpp_id, a_utl_ordre);

	    DELETE FROM RECETTE_PAPIER WHERE rpp_id = a_rpp_id;
	    DELETE FROM RECETTE_PAPIER_ADR_CLIENT where rpp_id = a_rpp_id;

   END;

PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      my_fac_id						RECETTE.fac_id%TYPE;
	  my_fac_tap_id					FACTURE.tap_id%TYPE;
   	  my_rec_tap_id					RECETTE.tap_id%TYPE;
   	  my_exe_ordre					RECETTE.exe_ordre%TYPE;
   	  my_org_id						FACTURE.org_id%TYPE;
	  my_tcd_ordre					FACTURE.tcd_ordre%TYPE;
	  my_rec_montant_budgetaire		RECETTE.rec_montant_budgetaire%TYPE;
	  my_rec_id_reduction		    RECETTE.rec_id_reduction%TYPE;
	  my_rec_ht_saisie				RECETTE.rec_ht_saisie%TYPE;
	  my_rec_ttc_saisie				RECETTE.rec_ttc_saisie%TYPE;
	  my_fac_budgetaire				FACTURE.fac_montant_budgetaire%TYPE;
	  my_rpp_id						RECETTE.rpp_id%TYPE;
	  my_nb							INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (del_recette)');
		END IF;

		SELECT fac_id, exe_ordre, tap_id, rec_montant_budgetaire, rec_ht_saisie, rec_ttc_saisie, rec_id_reduction, rpp_id
		  INTO my_fac_id, my_exe_ordre, my_rec_tap_id, my_rec_montant_budgetaire, my_rec_ht_saisie, my_rec_ttc_saisie, my_rec_id_reduction, my_rpp_id
		  FROM RECETTE WHERE rec_id = a_rec_id;

		IF my_rec_id_reduction IS NOT NULL THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Cette recette est une reduction, utiliser le package REDUIRE (del_recette)');
		END IF;

        -- verifier qu'on a le droit de recetter sur cet exercice.
		--Verifier.verifier_droit_recette(my_exe_ordre, a_utl_ordre);
		--Verifier.verifier_utilisation_recette(a_rec_id);

		log_recette(a_rec_id,a_utl_ordre);

	    del_recette_ctrl_action(a_rec_id);
	    del_recette_ctrl_analytique(a_rec_id);
	    del_recette_ctrl_convention(a_rec_id);
	    del_recette_ctrl_planco_tva(a_rec_id);
	    del_recette_ctrl_planco_ctp(a_rec_id);
	    del_recette_ctrl_planco(a_rec_id);

		-- mise a jour de la facture
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id = my_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||my_fac_id||') (del_recette)');
		END IF;

		SELECT org_id, tap_id, tcd_ordre INTO my_org_id, my_fac_tap_id, my_tcd_ordre FROM FACTURE WHERE fac_id = my_fac_id;

	    my_fac_budgetaire := Recetter_Outils.get_fac_montant_budgetaire(my_exe_ordre, my_fac_tap_id, my_rec_tap_id,
	          my_rec_montant_budgetaire, my_org_id, my_rec_ht_saisie, my_rec_ttc_saisie);

		UPDATE FACTURE SET fac_montant_budgetaire_reste = fac_montant_budgetaire_reste + my_fac_budgetaire WHERE fac_id = my_fac_id;

		-- delete...
	    DELETE FROM RECETTE WHERE rec_id=a_rec_id;

		-- verif si on doit supprimer la recette papier
   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = my_rpp_id AND rpp_visible='N';
		IF my_nb = 1 THEN
		   del_recette_papier(my_rpp_id, a_utl_ordre);
		END IF;

		-- mise a jour des restes des ctrl de la facture
		Corriger.maj_restes_ctrl(my_fac_id);

		Budget.del_recette(my_exe_ordre, my_org_id, my_tcd_ordre, my_rec_montant_budgetaire);
   END;

PROCEDURE log_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE
   ) IS
   BEGIN
   		INSERT INTO Z_RECETTE_PAPIER SELECT z_recette_papier_seq.NEXTVAL, SYSDATE, a_utl_ordre, r.*
		  FROM RECETTE_PAPIER r WHERE rpp_id=a_rpp_id;
   END;

PROCEDURE log_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      CURSOR liste IS SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
	  my_rpco_id	   	  		   	RECETTE_CTRL_PLANCO.rpco_id%TYPE;
	  my_zrec_id					Z_RECETTE.zrec_id%TYPE;
	  my_zrpco_id					Z_RECETTE_CTRL_PLANCO.zrpco_id%TYPE;
   BEGIN
		SELECT z_recette_seq.NEXTVAL INTO my_zrec_id FROM dual;

		INSERT INTO Z_RECETTE SELECT my_zrec_id, SYSDATE, a_utl_ordre, r.*
		  FROM RECETTE r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_ACTION SELECT z_recette_ctrl_action_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_ACTION r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_ANALYTIQUE SELECT z_recette_ctrl_analytique_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_ANALYTIQUE r WHERE rec_id=a_rec_id;

		INSERT INTO Z_RECETTE_CTRL_CONVENTION SELECT z_recette_ctrl_convention_seq.NEXTVAL, r.*, my_zrec_id
		  FROM RECETTE_CTRL_CONVENTION r WHERE rec_id=a_rec_id;

      	OPEN liste();
  	  	LOOP
			FETCH liste INTO my_rpco_id;
		 	EXIT WHEN liste%NOTFOUND;

			SELECT z_recette_ctrl_planco_seq.NEXTVAL INTO my_zrpco_id FROM dual;
			INSERT INTO Z_RECETTE_CTRL_PLANCO SELECT my_zrpco_id, r.*, my_zrec_id
		  		   FROM RECETTE_CTRL_PLANCO r WHERE rpco_id = my_rpco_id;

			INSERT INTO Z_RECETTE_CTRL_PLANCO_TVA SELECT z_recette_ctrl_planco_tva_seq.NEXTVAL, r.*, my_zrpco_id
		  		   FROM RECETTE_CTRL_PLANCO_TVA r WHERE rpco_id = my_rpco_id;

			INSERT INTO Z_RECETTE_CTRL_PLANCO_CTP SELECT z_recette_ctrl_planco_ctp_seq.NEXTVAL, r.*, my_zrpco_id
		  		   FROM RECETTE_CTRL_PLANCO_CTP r WHERE rpco_id = my_rpco_id;

		END LOOP;
	  	CLOSE liste;

   END;

PROCEDURE ins_recette_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_ract_id	               RECETTE_CTRL_ACTION.ract_id%TYPE;
       my_lolf_id	  	   		   RECETTE_CTRL_ACTION.lolf_id%TYPE;
       my_ract_montant_budgetaire  RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
	   sum_ract_montant_budgetaire RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
       my_ract_ht_saisie	  	   RECETTE_CTRL_ACTION.ract_ht_saisie%TYPE;
       my_ract_tva_saisie		   RECETTE_CTRL_ACTION.ract_tva_saisie%TYPE;
       my_ract_ttc_saisie		   RECETTE_CTRL_ACTION.ract_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre				   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ACTION.ract_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_action)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

  		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_action)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_lolf_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_ract_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_ract_ht_saisie)>ABS(my_ract_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_action)');
		    END IF;

		    my_ract_tva_saisie := Recetter_Outils.get_tva(my_ract_ht_saisie, my_ract_ttc_saisie);

			-- on calcule le montant budgetaire.
			my_ract_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_ract_ht_saisie,my_ract_ttc_saisie);

			IF my_ract_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_action)');
			END IF;

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1) = '$' OR
			  my_rec_montant_budgetaire <= my_somme + my_ract_montant_budgetaire THEN
			    my_ract_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_action_seq.NEXTVAL INTO my_ract_id FROM dual;

			INSERT INTO RECETTE_CTRL_ACTION VALUES (my_ract_id,
			       a_exe_ordre, a_rec_id, my_lolf_id, my_ract_montant_budgetaire,
				   my_ract_ht_saisie, my_ract_tva_saisie, my_ract_ttc_saisie, SYSDATE);

	   		-- procedure de verification
            Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_lolf_id);
			Apres_Recette.ins_recette_ctrl_action(my_ract_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_ract_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rana_id	               RECETTE_CTRL_ANALYTIQUE.rana_id%TYPE;
       my_can_id	  	   		   RECETTE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_rana_montant_budgetaire  RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
	   sum_rana_montant_budgetaire RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
       my_rana_ht_saisie	  	   RECETTE_CTRL_ANALYTIQUE.rana_ht_saisie%TYPE;
       my_rana_tva_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_tva_saisie%TYPE;
       my_rana_ttc_saisie		   RECETTE_CTRL_ANALYTIQUE.rana_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_ANALYTIQUE.rana_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_analytique)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_analytique)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rana_ht_saisie)>ABS(my_rana_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_analytique)');
		    END IF;

		    my_rana_tva_saisie := Recetter_Outils.get_tva(my_rana_ht_saisie, my_rana_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rana_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rana_ht_saisie,my_rana_ttc_saisie);

			IF my_rana_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_analytique)');
			END IF;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_rec_montant_budgetaire <= my_somme + my_rana_montant_budgetaire THEN
			    my_rana_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a un seul code analytique
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''un seul code analytique pour une recette (ins_recette_ctrl_analytique)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_analytique_seq.NEXTVAL INTO my_rana_id FROM dual;

			INSERT INTO RECETTE_CTRL_ANALYTIQUE VALUES (my_rana_id,
			       a_exe_ordre, a_rec_id, my_can_id, my_rana_montant_budgetaire,
				   my_rana_ht_saisie, my_rana_tva_saisie, my_rana_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
			Apres_Recette.ins_recette_ctrl_analytique(my_rana_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rana_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rcon_id	               RECETTE_CTRL_CONVENTION.rcon_id%TYPE;
       my_con_ordre	  	   		   RECETTE_CTRL_CONVENTION.con_ordre%TYPE;
       my_rcon_montant_budgetaire  RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
	   sum_rcon_montant_budgetaire RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
       my_rcon_ht_saisie	  	   RECETTE_CTRL_CONVENTION.rcon_ht_saisie%TYPE;
       my_rcon_tva_saisie		   RECETTE_CTRL_CONVENTION.rcon_tva_saisie%TYPE;
       my_rcon_ttc_saisie		   RECETTE_CTRL_CONVENTION.rcon_ttc_saisie%TYPE;
       my_rec_montant_budgetaire   RECETTE.rec_montant_budgetaire%TYPE;
	   my_rec_tap_id			   RECETTE.tap_id%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   RECETTE_CTRL_CONVENTION.rcon_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_convention)');
		END IF;

		SELECT r.rec_montant_budgetaire, r.tap_id, f.org_id, f.tcd_ordre, r.exe_ordre
		       INTO my_rec_montant_budgetaire, my_rec_tap_id, my_org_id, my_tcd_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'l''exercice n''est pas coherent (ins_recette_ctrl_convention)');
		END IF;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_con_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rcon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
 		    IF ABS(my_rcon_ht_saisie)>ABS(my_rcon_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_convention)');
		    END IF;

		    my_rcon_tva_saisie := Recetter_Outils.get_tva(my_rcon_ht_saisie, my_rcon_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_rcon_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_rec_tap_id,my_org_id,
		           my_rcon_ht_saisie,my_rcon_ttc_saisie);

			IF my_rcon_montant_budgetaire<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une recette il faut un montant positif (ins_recette_ctrl_convention)');
			END IF;

			-- on teste si il n'y a pas assez de dispo.
      		IF my_rec_montant_budgetaire <= my_somme + my_rcon_montant_budgetaire THEN
			    my_rcon_montant_budgetaire := my_rec_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule convention
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		       --RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une seule convention pour une recette (ins_recette_ctrl_convention)');
			--END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_convention_seq.NEXTVAL INTO my_rcon_id FROM dual;

			INSERT INTO RECETTE_CTRL_CONVENTION VALUES (my_rcon_id,
			       a_exe_ordre, a_rec_id, my_con_ordre, my_rcon_montant_budgetaire,
				   my_rcon_ht_saisie, my_rcon_tva_saisie, my_rcon_ttc_saisie, SYSDATE);

	   		-- procedure de verification
			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_con_ordre);
			Apres_Recette.ins_recette_ctrl_convention(my_rcon_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_rcon_montant_budgetaire;
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	               RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_rpco_ht_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
       my_rpco_tva_saisie		   RECETTE_CTRL_PLANCO.rpco_tva_saisie%TYPE;
       my_rpco_ttc_saisie		   RECETTE_CTRL_PLANCO.rpco_ttc_saisie%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco)');
		END IF;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpco_ht_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant HT positif (ins_recette_ctrl_planco)');
			END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_rpco_ht_saisie) > ABS(my_rpco_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_planco)');
		    END IF;

		    my_rpco_tva_saisie := Recetter_Outils.get_tva(my_rpco_ht_saisie, my_rpco_ttc_saisie);

			-- on bloque a une seule imputation
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une recette (ins_recette_ctrl_planco)');
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_seq.NEXTVAL INTO my_rpco_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO VALUES (my_rpco_id,
			       a_exe_ordre, a_rec_id, my_pco_num, NULL, my_rpco_ht_saisie, my_rpco_tva_saisie, my_rpco_ttc_saisie, SYSDATE, Get_Tbo_Ordre(a_rec_id));

   	   		-- procedure de verification
			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco(my_rpco_id);
 		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_rpcotva_id	           RECETTE_CTRL_PLANCO_TVA.rpcotva_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_TVA.pco_num%TYPE;
       my_rpcotva_tva_saisie	   RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_default_ges_code		   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_fac_tva_reste            FACTURE_CTRL_PLANCO.fpco_tva_reste%TYPE;
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre<>a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco_tva)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcotva_tva_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpcotva_tva_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant TVA positif (ins_recette_ctrl_planco_tva)');
			END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_tva_seq.NEXTVAL INTO my_rpcotva_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_TVA VALUES (my_rpcotva_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcotva_tva_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_tva(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_tva(my_rpcotva_id);
		END LOOP;
   END;

PROCEDURE ins_recette_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2
   ) IS
       my_rpco_id	           	   RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_rpcoctp_id	           RECETTE_CTRL_PLANCO_CTP.rpcoctp_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num   	   		   RECETTE_CTRL_PLANCO_CTP.pco_num%TYPE;
       my_rpcoctp_ttc_saisie	   RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;
	   my_default_ges_code		   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_par_value				   maracuja.parametre.par_value%TYPE;
	   my_exe_ordre			   	   RECETTE.exe_ordre%TYPE;
	   my_tyap_id			   	   FACTURE.tyap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                RECETTE.utl_ordre%TYPE;
       my_pvi_ctp                  maracuja.planco_visa.pvi_contrepartie_gestion%type;
       my_is_sacd                  integer;
       my_pco_num_185              maracuja.gestion_exercice.pco_num_185%type;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_ctp)');
		END IF;

		SELECT f.org_id, f.tcd_ordre, r.utl_ordre, r.exe_ordre, f.tyap_id
		       INTO my_org_id, my_tcd_ordre, my_utl_ordre, my_exe_ordre, my_tyap_id
          FROM RECETTE r, FACTURE f WHERE f.fac_id=r.fac_id AND r.rec_id=a_rec_id;

		IF my_exe_ordre <> a_exe_ordre THEN
		   RAISE_APPLICATION_ERROR(-20001, 'L''exercice n''est pas coherent (ins_recette_ctrl_planco_ctp)');
		END IF;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;
        my_is_sacd := 0;

        select count(*) into my_nb from maracuja.gestion_exercice where ges_code = my_default_ges_code and exe_ordre = a_exe_ordre;
        if my_nb = 1 then
           select pco_num_185 into my_pco_num_185 from maracuja.gestion_exercice where ges_code = my_default_ges_code and exe_ordre = a_exe_ordre;
           if my_pco_num_185 is not null then my_is_sacd := 1; end if;
        end if;

        my_chaine := a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere l'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcoctp_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

			IF my_rpcoctp_ttc_saisie < 0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'Pour une imputation comptable recette il faut un montant contrepartie positif (ins_recette_ctrl_planco_ctp)');
			END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation contrepartie doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_ctp)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = '' THEN
				my_ges_code := my_default_ges_code;

                select count(*) into my_nb from maracuja.planco_visa where pco_num_ordonnateur = my_pco_num_pere and exe_ordre = my_exe_ordre;
                if my_nb = 1 and my_is_sacd <> 1 then
                   SELECT pvi_contrepartie_gestion INTO my_pvi_ctp FROM maracuja.planco_visa
                      WHERE pco_num_ordonnateur=my_pco_num_pere AND exe_ordre = my_exe_ordre;

         	       IF UPPER(my_pvi_ctp) <> 'COMPOSANTE' OR my_tyap_id = Type_Application.get_type_prestation_interne THEN
		              SELECT c.ges_code INTO my_ges_code FROM maracuja.GESTION g, maracuja.COMPTABILITE c, maracuja.GESTION_EXERCICE ge
			             WHERE g.ges_code = my_default_ges_code AND g.com_ordre = c.com_ordre AND g.ges_code=ge.ges_code
                         AND ge.exe_ordre=my_exe_ordre;
		           END IF;
                end if;
			ELSE
				-- s'il s'agit d'une SACD, on prend dans tous les cas l'UB.
				IF my_is_sacd = 1 THEN
					my_ges_code := my_default_ges_code;
				END IF;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_ctp_seq.NEXTVAL INTO my_rpcoctp_id FROM dual;

			INSERT INTO RECETTE_CTRL_PLANCO_CTP VALUES (my_rpcoctp_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcoctp_ttc_saisie, SYSDATE, my_ges_code);

   	   		-- procedure de verification
			Verifier.verifier_planco_ctp(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
			Apres_Recette.ins_recette_ctrl_planco_ctp(my_rpcoctp_id);
		END LOOP;
   END;

PROCEDURE del_recette_ctrl_action (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_ACTION WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_analytique (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_convention (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_CONVENTION WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_planco (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id;
   END;

PROCEDURE del_recette_ctrl_planco_tva (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO_TVA WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
   END;

PROCEDURE del_recette_ctrl_planco_ctp (
	  a_rec_id		        RECETTE.rec_id%TYPE
   ) IS
   BEGIN
	    DELETE FROM RECETTE_CTRL_PLANCO_CTP WHERE rpco_id IN (SELECT rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id);
   END;

PROCEDURE upd_date_recette_restrinct (
      a_exe_ordre         RECETTE.exe_ordre%TYPE,
	  a_rec_id			  RECETTE.REC_ID%TYPE)
      IS
          cpt integer;
      BEGIN

      select count(*) into cpt from V_EXERCICE where V_EXERCICE.EXE_ORDRE = a_exe_ordre and V_EXERCICE.EXE_STAT_FAC = 'R';
      if(cpt=1)
      then
          update  RECETTE
          set REC_DATE_SAISIE =to_date('31/12/'||a_exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS')
          where REC_ID = a_rec_id;
      END IF;
      END;

END;

/

