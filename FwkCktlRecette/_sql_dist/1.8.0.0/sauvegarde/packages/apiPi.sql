--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package API_PI
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."API_PI" 
IS

PROCEDURE ins_eng_fac (
    a_pef_id IN OUT	  PI_ENG_FAC.pef_id%TYPE,
	a_eng_id  		  PI_ENG_FAC.eng_id%TYPE,
	a_fac_id  		  PI_ENG_FAC.fac_id%TYPE,
	a_prest_id	  	  PI_ENG_FAC.prest_id%TYPE);

PROCEDURE del_eng_fac (
    a_pef_id		  PI_ENG_FAC.pef_id%TYPE);

PROCEDURE del_eng_fac_facture (
    a_fac_id		  PI_ENG_FAC.fac_id%TYPE);

PROCEDURE ins_dep_rec (
    a_pdr_id IN OUT	  PI_DEP_REC.pdr_id%TYPE,
	a_dep_id		  PI_DEP_REC.dep_id%TYPE,
	a_rec_id		  PI_DEP_REC.rec_id%TYPE);

PROCEDURE del_dep_rec (
    a_pdr_id		  PI_DEP_REC.pdr_id%TYPE);

PROCEDURE del_dep_rec_recette (
    a_rec_id		  PI_DEP_REC.rec_id%TYPE);

FUNCTION get_type_application_pi
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_type_application_pe
   RETURN v_type_application.tyap_id%TYPE;

FUNCTION get_mode_paiement_pi (a_exe_ordre PARAMETRES.exe_ordre%TYPE)
   RETURN v_mode_paiement.mod_ordre%TYPE;

FUNCTION get_mode_recouvrement_pi (a_exe_ordre PARAMETRES.exe_ordre%TYPE)
   RETURN v_mode_recouvrement.mod_ordre%TYPE;

END;

/

