--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body FACTURER_RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."FACTURER_RECETTER" 
IS

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rpp_id IN OUT		RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_rpp_numero 	  		RECETTE_PAPIER.rpp_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
     my_montant_budgetaire FACTURE.fac_montant_budgetaire%TYPE;
     my_fac_tva_saisie     FACTURE.fac_tva_saisie%TYPE;
   BEGIN
		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
	    Verifier.verifier_fournisseur(a_fou_ordre);
	    Verifier.verifier_personne(a_pers_id);

		IF a_fac_ht_saisie<0 OR a_fac_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant positif (ins_facture_recette)');
		END IF;

   		my_fac_tva_saisie := a_fac_ttc_saisie-a_fac_ht_saisie;

		IF my_fac_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_facture_recette)');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      a_fac_ht_saisie,a_fac_ttc_saisie);

	   	-- enregistrement dans la table FACTURE.
	    IF a_fac_id IS NULL THEN
	       SELECT facture_seq.NEXTVAL INTO a_fac_id FROM dual;
	    END IF;
		IF a_fac_numero IS NULL THEN
   		   a_fac_numero := Get_Numerotation(a_exe_ordre, NULL, 'FACTURE');
   		END IF;

	    INSERT INTO FACTURE VALUES (a_fac_id, a_exe_ordre, a_fac_numero, a_fou_ordre, a_pers_id, SYSDATE,
		  a_fac_lib, my_montant_budgetaire, 0, a_fac_ht_saisie, my_fac_tva_saisie, a_fac_ttc_saisie,
		  a_mor_ordre, a_org_id, a_tap_id, a_tcd_ordre, NULL, a_tyap_id, Type_Etat.get_etat_valide, a_utl_ordre);

		IF a_rpp_id IS NULL THEN
	  		Recetter.ins_recette_papier(a_rpp_id, a_exe_ordre, a_rpp_numero, a_fac_ht_saisie, a_fac_ttc_saisie,
		   								a_pers_id, a_fou_ordre, a_rib_ordre, a_mor_ordre, SYSDATE, SYSDATE,
		   								SYSDATE, a_rpp_nb_piece, a_utl_ordre, 'N');
	    END IF;

		-- insertion dans la table RECETTE.
	    IF a_rec_id IS NULL THEN
	       SELECT recette_seq.NEXTVAL INTO a_rec_id FROM dual;
	    END IF;

		IF a_rec_numero IS NULL THEN
   		   a_rec_numero := Get_Numerotation(a_exe_ordre, NULL, 'RECETTE');
   		END IF;

		INSERT INTO RECETTE (
			   REC_ID, EXE_ORDRE, RPP_ID, REC_NUMERO, FAC_ID, REC_LIB, REC_DATE_SAISIE,
			   REC_MONTANT_BUDGETAIRE, REC_HT_SAISIE, REC_TVA_SAISIE, REC_TTC_SAISIE,
			   TAP_ID, TYET_ID, UTL_ORDRE, REC_ID_REDUCTION)
		VALUES (
			   a_rec_id, a_exe_ordre, a_rpp_id, a_rec_numero, a_fac_id, a_fac_lib, SYSDATE,
		   	   my_montant_budgetaire, a_fac_ht_saisie, my_fac_tva_saisie, a_fac_ttc_saisie,
			   a_tap_id, Type_Etat.get_etat_valide, a_utl_ordre, NULL);

		ins_ctrl_action(a_exe_ordre, a_fac_id, a_rec_id, a_chaine_action);
		ins_ctrl_analytique(a_exe_ordre, a_fac_id, a_rec_id, a_chaine_analytique);
		ins_ctrl_convention(a_exe_ordre, a_fac_id, a_rec_id, a_chaine_convention);
		ins_ctrl_planco(a_exe_ordre, a_fac_id, a_rec_id, a_chaine_planco, a_chaine_planco_tva, a_chaine_planco_ctp);

        Budget.ins_facture(a_exe_ordre, a_org_id, a_tcd_ordre, my_montant_budgetaire);
		Budget.ins_recette(a_exe_ordre, a_org_id, a_tcd_ordre, my_montant_budgetaire);

   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE upd_facture_recette (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_rec_numero IN OUT   RECETTE.rec_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2
   ) IS
      my_rpp_id				RECETTE.rpp_id%TYPE;
	  my_fac_id				RECETTE.fac_id%TYPE;
	  my_eche_id			FACTURE.eche_id%TYPE;
      my_rpp_visible		RECETTE_PAPIER.rpp_visible%TYPE;
      my_rpp_numero			RECETTE_PAPIER.rpp_numero%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture-recette n''existe pas (rec_id='||a_rec_id||') (upd_facture_recette)');
		END IF;
		SELECT rpp_id, fac_id INTO my_rpp_id, my_fac_id FROM RECETTE WHERE rec_id = a_rec_id;

   		SELECT COUNT(*) INTO my_nb FROM RECETTE_PAPIER WHERE rpp_id = my_rpp_id;
		IF my_nb <> 1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette papier n''existe pas (rpp_id='||my_rpp_id||') (upd_facture_recette)');
		END IF;
		SELECT rpp_visible, rpp_numero INTO my_rpp_visible, my_rpp_numero FROM RECETTE_PAPIER WHERE rpp_id = my_rpp_id;
		IF my_rpp_visible = 'N' THEN
		   my_rpp_id := NULL;
		END IF;

   		SELECT COUNT(*) INTO my_nb FROM facture WHERE fac_id = my_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||my_fac_id||') (upd_facture_recette)');
		END IF;
		SELECT eche_id INTO my_eche_id FROM jefy_recette.facture WHERE fac_id = my_fac_id;

   		del_facture_recette(a_rec_id, a_utl_ordre);
		ins_facture_recette(a_fac_id, a_rec_id, my_rpp_id, a_exe_ordre, a_fac_numero, a_rec_numero, my_rpp_numero, a_fou_ordre, a_pers_id,
			a_fac_lib, a_rib_ordre, a_mor_ordre, a_rpp_nb_piece, a_org_id, a_tcd_ordre, a_tap_id, a_fac_ht_saisie, a_fac_ttc_saisie, a_tyap_id,
			a_utl_ordre, a_chaine_action, a_chaine_analytique, a_chaine_convention, a_chaine_planco, a_chaine_planco_tva,
			a_chaine_planco_ctp);

		IF my_eche_id IS NOT NULL THEN
		   UPDATE jefy_recette.facture SET eche_id = my_eche_id WHERE fac_id = a_fac_id;
		END IF;

		Verifier.verifier_recette_pap_coherence(my_rpp_id);

   END;

PROCEDURE del_facture_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE
   ) IS
      my_fac_id				RECETTE.fac_id%TYPE;
	  my_nb					INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id = a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture-recette n''existe pas (rec_id='||a_rec_id||') (del_facture_recette)');
		END IF;

		SELECT fac_id INTO my_fac_id FROM RECETTE WHERE rec_id = a_rec_id;

		Recetter.del_recette(a_rec_id, a_utl_ordre);
		Facturer.del_facture(my_fac_id, a_utl_ordre);
   END;

PROCEDURE ins_ctrl_action (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_rec_id		  RECETTE.rec_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fact_id	               FACTURE_CTRL_ACTION.fact_id%TYPE;
       my_ract_id	               RECETTE_CTRL_ACTION.ract_id%TYPE;
       my_lolf_id	  	   		   FACTURE_CTRL_ACTION.lolf_id%TYPE;
       my_fact_montant_budgetaire  FACTURE_CTRL_ACTION.fact_montant_budgetaire%TYPE;
       my_fact_ht_saisie	  	   FACTURE_CTRL_ACTION.fact_ht_saisie%TYPE;
       my_fact_tva_saisie		   FACTURE_CTRL_ACTION.fact_tva_saisie%TYPE;
       my_fact_ttc_saisie		   FACTURE_CTRL_ACTION.fact_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_ACTION.fact_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_ctrl_action)');
		END IF;
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_ctrl_action)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_lolf_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fact_ht_saisie<0 OR my_fact_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant positif (ins_ctrl_action)');
		    END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_fact_ht_saisie) > ABS(my_fact_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_action)');
		    END IF;

		    my_fact_tva_saisie := Recetter_Outils.get_tva(my_fact_ht_saisie, my_fact_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_fact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fact_ht_saisie,my_fact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1) = '$' OR
			  my_fac_montant_budgetaire <= my_somme + my_fact_montant_budgetaire THEN
			    my_fact_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_action_seq.NEXTVAL INTO my_fact_id FROM dual;
			INSERT INTO FACTURE_CTRL_ACTION VALUES (my_fact_id,
			       a_exe_ordre, a_fac_id, my_lolf_id, my_fact_montant_budgetaire, 0,
				   my_fact_ht_saisie, my_fact_tva_saisie, my_fact_ttc_saisie, SYSDATE);

			SELECT recette_ctrl_action_seq.NEXTVAL INTO my_ract_id FROM dual;
			INSERT INTO RECETTE_CTRL_ACTION VALUES (my_ract_id,
			       a_exe_ordre, a_rec_id, my_lolf_id, my_fact_montant_budgetaire,
				   my_fact_ht_saisie, my_fact_tva_saisie, my_fact_ttc_saisie, SYSDATE);

	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_lolf_id);

			Apres_Facture.ins_facture_ctrl_action(my_fact_id);
			Apres_Recette.ins_recette_ctrl_action(my_ract_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_fact_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_ctrl_analytique (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_rec_id		  RECETTE.rec_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fana_id	               FACTURE_CTRL_ANALYTIQUE.fana_id%TYPE;
       my_rana_id	               RECETTE_CTRL_ANALYTIQUE.rana_id%TYPE;
       my_can_id	  	   		   FACTURE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_fana_montant_budgetaire  FACTURE_CTRL_ANALYTIQUE.fana_montant_budgetaire%TYPE;
       my_fana_ht_saisie	  	   FACTURE_CTRL_ANALYTIQUE.fana_ht_saisie%TYPE;
       my_fana_tva_saisie		   FACTURE_CTRL_ANALYTIQUE.fana_tva_saisie%TYPE;
       my_fana_ttc_saisie		   FACTURE_CTRL_ANALYTIQUE.fana_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_ANALYTIQUE.fana_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_ctrl_analytique)');
		END IF;
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_ctrl_analytique)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fana_ht_saisie<0 OR my_fana_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant positif (ins_ctrl_analytique)');
		    END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_fana_ht_saisie) > ABS(my_fana_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_analytique)');
		    END IF;

		    my_fana_tva_saisie := Recetter_Outils.get_tva(my_fana_ht_saisie, my_fana_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_fana_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fana_ht_saisie,my_fana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_fac_montant_budgetaire <= my_somme + my_fana_montant_budgetaire THEN
			    my_fana_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a un seul code analytique
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_ANALYTIQUE WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		    --   RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''un seul code analytique pour une recette (ins_ctrl_analytique)');
			--END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_analytique_seq.NEXTVAL INTO my_fana_id FROM dual;
			INSERT INTO FACTURE_CTRL_ANALYTIQUE VALUES (my_fana_id,
			       a_exe_ordre, a_fac_id, my_can_id, my_fana_montant_budgetaire, 0,
				   my_fana_ht_saisie, my_fana_tva_saisie, my_fana_ttc_saisie, SYSDATE);

			SELECT recette_ctrl_analytique_seq.NEXTVAL INTO my_rana_id FROM dual;
			INSERT INTO RECETTE_CTRL_ANALYTIQUE VALUES (my_rana_id,
			       a_exe_ordre, a_rec_id, my_can_id, my_fana_montant_budgetaire,
				   my_fana_ht_saisie, my_fana_tva_saisie, my_fana_ttc_saisie, SYSDATE);

	        Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);

			Apres_Facture.ins_facture_ctrl_analytique(my_fana_id);
			Apres_Recette.ins_recette_ctrl_analytique(my_rana_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_fana_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_ctrl_convention (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_rec_id		  RECETTE.rec_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fcon_id	               FACTURE_CTRL_CONVENTION.fcon_id%TYPE;
       my_rcon_id	               RECETTE_CTRL_CONVENTION.rcon_id%TYPE;
       my_con_ordre	  	   		   FACTURE_CTRL_CONVENTION.con_ordre%TYPE;
       my_fcon_montant_budgetaire  FACTURE_CTRL_CONVENTION.fcon_montant_budgetaire%TYPE;
       my_fcon_ht_saisie	  	   FACTURE_CTRL_CONVENTION.fcon_ht_saisie%TYPE;
       my_fcon_tva_saisie		   FACTURE_CTRL_CONVENTION.fcon_tva_saisie%TYPE;
       my_fcon_ttc_saisie		   FACTURE_CTRL_CONVENTION.fcon_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_CONVENTION.fcon_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_ctrl_convention)');
		END IF;
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_ctrl_convention)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_con_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fcon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fcon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fcon_ht_saisie<0 OR my_fcon_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant positif (ins_ctrl_convention)');
		    END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_fcon_ht_saisie) > ABS(my_fcon_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_recette_ctrl_convention)');
		    END IF;

		    my_fcon_tva_saisie := Recetter_Outils.get_tva(my_fcon_ht_saisie, my_fcon_ttc_saisie);

			-- on calcule le montant budgetaire.
  			my_fcon_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fcon_ht_saisie,my_fcon_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_fac_montant_budgetaire <= my_somme + my_fcon_montant_budgetaire THEN
			    my_fcon_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- on bloque a une seule convention
			--SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_CONVENTION WHERE rec_id=a_rec_id;
			--IF my_nb>0 THEN
		    --   RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une seule convention pour une recette (ins_ctrl_convention)');
			--END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_convention_seq.NEXTVAL INTO my_fcon_id FROM dual;
			INSERT INTO FACTURE_CTRL_CONVENTION VALUES (my_fcon_id,
			       a_exe_ordre, a_fac_id, my_con_ordre, my_fcon_montant_budgetaire, 0,
				   my_fcon_ht_saisie, my_fcon_tva_saisie, my_fcon_ttc_saisie, SYSDATE);

			SELECT recette_ctrl_convention_seq.NEXTVAL INTO my_rcon_id FROM dual;
			INSERT INTO RECETTE_CTRL_CONVENTION VALUES (my_rcon_id,
			       a_exe_ordre, a_rec_id, my_con_ordre, my_fcon_montant_budgetaire,
				   my_fcon_ht_saisie, my_fcon_tva_saisie, my_fcon_ttc_saisie, SYSDATE);

	        Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_con_ordre);

			Apres_Facture.ins_facture_ctrl_convention(my_fcon_id);
			Apres_Recette.ins_recette_ctrl_convention(my_rcon_id);

			-- mise a jour de la somme de controle.
	  		my_somme := my_somme + my_fcon_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_ctrl_planco (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE,
      a_rec_id            RECETTE.rec_id%TYPE,
	  a_chaine			  VARCHAR2,
	  a_chaine_tva		  VARCHAR2,
	  a_chaine_ctp		  VARCHAR2
   ) IS
       my_fpco_id	               FACTURE_CTRL_PLANCO.fpco_id%TYPE;
       my_rpco_id	               RECETTE_CTRL_PLANCO.rpco_id%TYPE;
       my_rpcotva_id	           RECETTE_CTRL_PLANCO_TVA.rpcotva_id%TYPE;
       my_rpcoctp_id	           RECETTE_CTRL_PLANCO_CTP.rpcoctp_id%TYPE;
       my_pco_num_pere 	   		   RECETTE_CTRL_PLANCO.pco_num%TYPE;
       my_pco_num	  	   		   FACTURE_CTRL_PLANCO.pco_num%TYPE;
       my_rpco_ht_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ht_saisie%TYPE;
       my_rpco_tva_saisie	  	   RECETTE_CTRL_PLANCO.rpco_tva_saisie%TYPE;
       my_rpco_ttc_saisie	  	   RECETTE_CTRL_PLANCO.rpco_ttc_saisie%TYPE;
       my_rpcotva_tva_saisie	   RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
       my_rpcoctp_ttc_saisie	   RECETTE_CTRL_PLANCO_CTP.rpcoctp_ttc_saisie%TYPE;
	   my_somme_rpcotva_tva_saisie RECETTE_CTRL_PLANCO_TVA.rpcotva_tva_saisie%TYPE;
	   my_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_default_ges_code				   RECETTE_CTRL_PLANCO_TVA.ges_code%TYPE;
	   my_par_value				   maracuja.parametre.par_value%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre				   FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                FACTURE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_ctrl_planco)');
		END IF;
		SELECT COUNT(*) INTO my_nb FROM RECETTE WHERE rec_id=a_rec_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La recette n''existe pas (rec_id='||a_rec_id||') (ins_ctrl_planco)');
		END IF;

		SELECT tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine := a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_rpco_ht_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant positif (ins_ctrl_planco)');
		    END IF;

		    -- on teste la coherence entre le ht et le ttc puis on calcule la tva.
	 		IF ABS(my_rpco_ht_saisie) > ABS(my_rpco_ttc_saisie) THEN
		   	   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_ctrl_planco)');
		    END IF;

		    my_rpco_tva_saisie := Recetter_Outils.get_tva(my_rpco_ht_saisie, my_rpco_ttc_saisie);

			-- on bloque a une seule imputation
			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id=a_rec_id;
			IF my_nb>0 THEN
		       RAISE_APPLICATION_ERROR(-20001,'On ne peut mettre qu''une imputation comptable pour une recette (ins_ctrl_planco)');
			END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_planco_seq.NEXTVAL INTO my_fpco_id FROM dual;
			INSERT INTO FACTURE_CTRL_PLANCO VALUES (my_fpco_id,
			       a_exe_ordre, a_fac_id, my_pco_num, my_rpco_ht_saisie, 0,
				   my_rpco_tva_saisie, 0, my_rpco_ttc_saisie, SYSDATE);

			SELECT recette_ctrl_planco_seq.NEXTVAL INTO my_rpco_id FROM dual;
			INSERT INTO RECETTE_CTRL_PLANCO VALUES (my_rpco_id,
			       a_exe_ordre, a_rec_id, my_pco_num, NULL, my_rpco_ht_saisie, my_rpco_tva_saisie, my_rpco_ttc_saisie, SYSDATE, Get_Tbo_Ordre(a_rec_id));

			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Facture.ins_facture_ctrl_planco(my_fpco_id);
	    	Apres_Recette.ins_recette_ctrl_planco(my_rpco_id);
		END LOOP;

		-- determination du ges_code
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;

        my_chaine := a_chaine_tva;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcotva_tva_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_rpcotva_tva_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant TVA positif (ins_ctrl_planco)');
		    END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_tva_seq.NEXTVAL INTO my_rpcotva_id FROM dual;
			INSERT INTO RECETTE_CTRL_PLANCO_TVA VALUES (my_rpcotva_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcotva_tva_saisie, SYSDATE, my_ges_code);

			Verifier.verifier_planco_tva(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Recette.ins_recette_ctrl_planco_tva(my_rpcotva_id);
		END LOOP;

		-- determination du ges_code CTP
		SELECT org_ub INTO my_default_ges_code FROM v_organ WHERE org_id = my_org_id;
		SELECT par_value INTO my_par_value
           FROM maracuja.PARAMETRE
           WHERE UPPER(par_key) = 'CONTRE PARTIE VISA RECETTE'
           AND exe_ordre = a_exe_ordre;
		IF UPPER(my_par_value) <> 'COMPOSANTE' THEN
		   SELECT c.ges_code INTO my_default_ges_code
			FROM maracuja.GESTION g, maracuja.COMPTABILITE c, maracuja.GESTION_EXERCICE ge
			WHERE g.ges_code = my_default_ges_code
			AND g.com_ordre = c.com_ordre
			AND g.ges_code=ge.ges_code
			AND ge.exe_ordre=a_exe_ordre;
		END IF;
        my_chaine := a_chaine_ctp;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'imputation pere.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num_pere FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere la tva.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_rpcoctp_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ges_code.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_ges_code FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_rpcoctp_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La recette doit se faire pour un montant contrepartie positif (ins_ctrl_planco)');
		    END IF;

			SELECT COUNT(*) INTO my_nb FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;
			IF my_nb<>1 THEN
		   	   RAISE_APPLICATION_ERROR(-20001,'L''imputation tva doit etre rattachee a une imputation recette (rec_id='||a_rec_id||') (ins_recette_ctrl_planco_tva)');
		    END IF;
			SELECT rpco_id INTO my_rpco_id FROM RECETTE_CTRL_PLANCO WHERE rec_id = a_rec_id AND pco_num = my_pco_num_pere AND ROWNUM=1;

			IF my_ges_code IS NULL OR trim(my_ges_code) = ''
			THEN
				my_ges_code := my_default_ges_code;
			END IF;

			-- insertion dans la base.
			SELECT recette_ctrl_planco_ctp_seq.NEXTVAL INTO my_rpcoctp_id FROM dual;
			INSERT INTO RECETTE_CTRL_PLANCO_CTP VALUES (my_rpcoctp_id,
			       a_exe_ordre, my_rpco_id, my_pco_num, my_rpcoctp_ttc_saisie, SYSDATE, my_ges_code);

			Verifier.verifier_planco_ctp(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Recette.ins_recette_ctrl_planco_ctp(my_rpcoctp_id);
		END LOOP;

   END;

END;

/

