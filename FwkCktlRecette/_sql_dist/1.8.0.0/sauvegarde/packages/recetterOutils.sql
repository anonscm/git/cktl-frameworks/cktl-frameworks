--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package RECETTER_OUTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."RECETTER_OUTILS" IS

FUNCTION  get_fac_montant_budgetaire(
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_fac_tap_id	        FACTURE.tap_id%TYPE,
	  a_rec_tap_id	        RECETTE.tap_id%TYPE,
	  a_montant_budgetaire  RECETTE.rec_montant_budgetaire%TYPE,
	  a_org_id	            FACTURE.org_id%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE)
   RETURN FACTURE.fac_montant_budgetaire%TYPE;

FUNCTION  get_tva (
      a_rec_ht_saisie       RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie	    RECETTE.rec_ttc_saisie%TYPE)
   RETURN RECETTE.rec_tva_saisie%TYPE;

-- recupere le plan comptable tva en fonction du pco_num, dans planco_visa
FUNCTION get_pco_num_tva (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE)
   RETURN v_planco_visa.pco_num_tva%TYPE;

-- recupere le plan comptable tva en fonction du pco_num et du mor_ordre,
-- dans mode de recouvrement en priorite, sinon dans planco_visa
FUNCTION get_pco_num_ctp (
	  a_pco_num	    v_planco_visa.pco_num_ordonnateur%TYPE,
	  a_exe_ordre   v_planco_visa.exe_ordre%TYPE,
	  a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN v_planco_visa.pco_num_ctrepartie%TYPE;

FUNCTION chaine_action_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_analytique_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_convention_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_tva_from_facture (a_fac_id FACTURE.fac_id%TYPE)
   RETURN VARCHAR2;

FUNCTION chaine_planco_ctp_from_facture (a_fac_id FACTURE.fac_id%TYPE, a_mor_ordre	v_mode_recouvrement.mod_ordre%TYPE)
   RETURN VARCHAR2;

FUNCTION get_recette_debiteur_adresse(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE) RETURN VARCHAR2;

END;

/

