--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package RECETTER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."RECETTER" IS

--
-- procedures appelees par le package api
--

PROCEDURE ins_recette_papier (
      a_rpp_id IN OUT		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_exe_ordre			  RECETTE_PAPIER.exe_ordre%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE,
	  a_rpp_visible			  RECETTE_PAPIER.rpp_visible%TYPE);

PROCEDURE upd_recette_papier (
      a_rpp_id		  		  RECETTE_PAPIER.rpp_id%TYPE,
	  a_rpp_numero			  RECETTE_PAPIER.rpp_numero%TYPE,
	  a_rpp_ht_saisie		  RECETTE_PAPIER.rpp_ht_saisie%TYPE,
	  a_rpp_ttc_saisie		  RECETTE_PAPIER.rpp_ttc_saisie%TYPE,
	  a_pers_id			  	  RECETTE_PAPIER.pers_id%TYPE,
	  a_fou_ordre			  RECETTE_PAPIER.fou_ordre%TYPE,
	  a_rib_ordre			  RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			  RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rpp_date_recette	  RECETTE_PAPIER.rpp_date_recette%TYPE,
	  a_rpp_date_reception	  RECETTE_PAPIER.rpp_date_reception%TYPE,
	  a_rpp_date_service_fait RECETTE_PAPIER.rpp_date_service_fait%TYPE,
	  a_rpp_nb_piece		  RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_utl_ordre			  RECETTE_PAPIER.utl_ordre%TYPE);

PROCEDURE ins_recette_papier_adr_client (
      a_rpp_id                RECETTE_PAPIER_ADR_CLIENT.rpp_id%type,
      a_adr_ordre             RECETTE_PAPIER_ADR_CLIENT.adr_ordre%type,
      a_pers_id_creation      RECETTE_PAPIER_ADR_CLIENT.pers_id_creation%type);

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_recette (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
	  a_rpp_id IN OUT		RECETTE.rpp_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				RECETTE.fac_id%TYPE,
	  a_rec_lib				RECETTE.rec_lib%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

-- Permet de recetter en automatique une facture (recette totale)
-- donc condition: qu'aucune recette n'existe deja pour cette facture
-- a_fac_id et a_utl_ordre sont obligatoires
-- a_mor_ordre, a_tap_id, a_pco_num_tva et a_pco_num_ctp sont facultatifs :
-- si indiqué, ce seront ceux utilisés, si null, tentative de détermination auto:
--   . mor_ordre recupere de la facture si existe, sinon erreur
--   . tap_id recupere de la facture
--   . pco_num_tva determine a partir du planco_visa, sinon erreur
--   . pco_num_ctp determine a partir du mor_ordre en priorite, sinon planco_visa, sinon erreur
-- j'suis clair ? :-)
PROCEDURE ins_recette_from_facture (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
	  a_fac_id				FACTURE.fac_id%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rib_ordre			RECETTE_PAPIER.rib_ordre%TYPE,
	  a_mor_ordre			RECETTE_PAPIER.mor_ordre%TYPE,
	  a_tap_id				RECETTE.tap_id%TYPE,
	  a_pco_num_tva			RECETTE_CTRL_PLANCO_TVA.PCO_NUM%TYPE,
	  a_pco_num_ctp			RECETTE_CTRL_PLANCO_CTP.PCO_NUM%TYPE);

PROCEDURE del_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE);

PROCEDURE del_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE log_recette_papier (
      a_rpp_id              RECETTE_PAPIER.rpp_id%TYPE,
      a_utl_ordre           Z_RECETTE_PAPIER.zrpp_utl_ordre%TYPE);

PROCEDURE log_recette (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE ins_recette_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_recette_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE del_recette_ctrl_action (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_analytique (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_convention (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco_tva (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE del_recette_ctrl_planco_ctp (
	  a_rec_id		        RECETTE.rec_id%TYPE);

PROCEDURE upd_date_recette_restrinct (
      a_exe_ordre         RECETTE.exe_ordre%TYPE,
	  a_rec_id			  RECETTE.REC_ID%TYPE);

END;

/

