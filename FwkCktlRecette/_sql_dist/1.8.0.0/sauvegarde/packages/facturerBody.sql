--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body FACTURER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."FACTURER" 
IS

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE ins_facture (
      a_fac_id IN OUT		FACTURE.fac_id%TYPE,
	  a_exe_ordre			FACTURE.exe_ordre%TYPE,
	  a_fac_numero IN OUT	FACTURE.fac_numero%TYPE,
	  a_fou_ordre			FACTURE.fou_ordre%TYPE,
	  a_pers_id				FACTURE.pers_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_org_id				FACTURE.org_id%TYPE,
	  a_tcd_ordre			FACTURE.tcd_ordre%TYPE,
	  a_tap_id				FACTURE.tap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_montant_budgetaire FACTURE.fac_montant_budgetaire%TYPE;
     my_fac_tva_saisie     FACTURE.fac_tva_saisie%TYPE;
   BEGIN
		Verifier.verifier_budget(a_exe_ordre, a_tap_id, a_org_id, a_tcd_ordre);
	    Verifier.verifier_fournisseur(a_fou_ordre);
	    Verifier.verifier_personne(a_pers_id);

		IF a_fac_ht_saisie<0 OR a_fac_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (ins_facture)');
		END IF;

   		my_fac_tva_saisie := a_fac_ttc_saisie-a_fac_ht_saisie;

		IF my_fac_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_facture)');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(a_exe_ordre,a_tap_id,a_org_id,
		      a_fac_ht_saisie,a_fac_ttc_saisie);

	   	-- enregistrement dans la table.
	    IF a_fac_id IS NULL THEN
	       SELECT facture_seq.NEXTVAL INTO a_fac_id FROM dual;
	    END IF;

		IF a_fac_numero IS NULL THEN
   		   a_fac_numero := Get_Numerotation(a_exe_ordre, NULL, 'FACTURE');
   		END IF;

	    INSERT INTO FACTURE VALUES (a_fac_id, a_exe_ordre, a_fac_numero, a_fou_ordre, a_pers_id, SYSDATE, a_fac_lib,
		  my_montant_budgetaire, my_montant_budgetaire, a_fac_ht_saisie, my_fac_tva_saisie, a_fac_ttc_saisie,
		  a_mor_ordre, a_org_id, a_tap_id, a_tcd_ordre, NULL, a_tyap_id, Type_Etat.get_etat_valide, a_utl_ordre);

		ins_facture_ctrl_action(a_exe_ordre,a_fac_id,a_chaine_action);
		ins_facture_ctrl_analytique(a_exe_ordre,a_fac_id,a_chaine_analytique);
		ins_facture_ctrl_convention(a_exe_ordre,a_fac_id,a_chaine_convention);
		ins_facture_ctrl_planco(a_exe_ordre,a_fac_id,a_chaine_planco);

        Budget.ins_facture(a_exe_ordre, a_org_id, a_tcd_ordre, my_montant_budgetaire);
        upd_date_facture_restrinct(a_exe_ordre,a_fac_id);
   END;

-- format des chaines
-- a_chaine_action: lolf_id$fact_ht_saisie$fact_ttc_saisie$...$
-- a_chaine_analytique: can_id$fana_ht_saisie$fana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$fcon_ht_saisie$fcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$fpco_ht_saisie$fpco_ttc_saisie$...$
PROCEDURE upd_facture (
      a_fac_id 			  	FACTURE.fac_id%TYPE,
	  a_fac_lib				FACTURE.fac_lib%TYPE,
	  a_mor_ordre			FACTURE.mor_ordre%TYPE,
	  a_tyap_id				FACTURE.tyap_id%TYPE,
	  a_fac_ht_saisie		FACTURE.fac_ht_saisie%TYPE,
	  a_fac_ttc_saisie		FACTURE.fac_ttc_saisie%TYPE,
	  a_utl_ordre			FACTURE.utl_ordre%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2
   ) IS
     my_exe_ordre	             FACTURE.exe_ordre%TYPE;
     my_tap_id			    	 FACTURE.tap_id%TYPE;
     my_org_id			    	 FACTURE.org_id%TYPE;
     my_tcd_ordre		    	 FACTURE.tcd_ordre%TYPE;
     my_budgetaire				 FACTURE.fac_montant_budgetaire%TYPE;
     my_montant_budgetaire 		 FACTURE.fac_montant_budgetaire%TYPE;
     my_fac_tva_saisie     		 FACTURE.fac_tva_saisie%TYPE;
	 my_nb						 INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb=0
		THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (upd_facture)');
		END IF;

   		SELECT exe_ordre, fac_montant_budgetaire-fac_montant_budgetaire_reste, tap_id, org_id, tcd_ordre
		  INTO my_exe_ordre, my_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
		  FROM FACTURE WHERE fac_id=a_fac_id;

		log_facture(a_fac_id,a_utl_ordre);

	    -- on verifie la coherence des montants.
		IF a_fac_ht_saisie<0 OR a_fac_ttc_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (upd_facture)');
		END IF;

   		my_fac_tva_saisie := a_fac_ttc_saisie-a_fac_ht_saisie;

		IF my_fac_tva_saisie<0 THEN
		   RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (upd_facture)');
		END IF;

		-- calcul du montant budgetaire.
		my_montant_budgetaire := Budget.calculer_budgetaire(my_exe_ordre,my_tap_id,my_org_id,
		      a_fac_ht_saisie,a_fac_ttc_saisie);

	    IF my_budgetaire > my_montant_budgetaire THEN
          RAISE_APPLICATION_ERROR(-20001, 'Le montant modifie est inferieur au montant deja recette (fac_id:'||a_fac_id||') (upd_facture)');
		END IF;

		-- on modifie les montants.
		UPDATE FACTURE SET fac_lib=a_fac_lib, mor_ordre=a_mor_ordre, tyap_id=a_tyap_id,
		   fac_montant_budgetaire=my_montant_budgetaire,
		   fac_montant_budgetaire_reste=my_montant_budgetaire-my_budgetaire,
		   fac_ht_saisie=a_fac_ht_saisie, fac_tva_saisie=my_fac_tva_saisie,
		   fac_ttc_saisie=a_fac_ttc_saisie, utl_ordre=a_utl_ordre
		   WHERE fac_id=a_fac_id;

		-- on vire les anciens
		DELETE FROM FACTURE_CTRL_ACTION WHERE fac_id = a_fac_id;
		DELETE FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id = a_fac_id;
		DELETE FROM FACTURE_CTRL_CONVENTION WHERE fac_id = a_fac_id;
		DELETE FROM FACTURE_CTRL_PLANCO WHERE fac_id = a_fac_id;

		ins_facture_ctrl_action(my_exe_ordre,a_fac_id,a_chaine_action);
		ins_facture_ctrl_analytique(my_exe_ordre,a_fac_id,a_chaine_analytique);
		ins_facture_ctrl_convention(my_exe_ordre,a_fac_id,a_chaine_convention);
		ins_facture_ctrl_planco(my_exe_ordre,a_fac_id,a_chaine_planco);

		-- mise a jour des restes des ctrl de la facture
		Corriger.maj_restes_ctrl(a_fac_id);

        Budget.upd_facture(my_exe_ordre, my_org_id, my_tcd_ordre, my_montant_budgetaire);
        upd_date_facture_restrinct(my_exe_ordre,a_fac_id);
   END;

PROCEDURE del_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE
   ) IS
     my_exe_ordre	             FACTURE.exe_ordre%TYPE;
     my_tap_id			    	 FACTURE.tap_id%TYPE;
     my_org_id			    	 FACTURE.org_id%TYPE;
     my_tcd_ordre		    	 FACTURE.tcd_ordre%TYPE;
     my_montant_budgetaire 		 FACTURE.fac_montant_budgetaire%TYPE;
	 my_nb						 INTEGER;
   BEGIN
   		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb=0
		THEN
			RAISE_APPLICATION_ERROR(-20001, 'La facture demandee n''existe pas (fac_id:'||a_fac_id||') (del_facture)');
		END IF;

   		SELECT exe_ordre, fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		  INTO my_exe_ordre, my_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
		  FROM FACTURE WHERE fac_id=a_fac_id;

		log_facture(a_fac_id,a_utl_ordre);

	    DELETE FROM FACTURE_CTRL_ACTION WHERE fac_id=a_fac_id;
	    DELETE FROM FACTURE_CTRL_ANALYTIQUE WHERE fac_id=a_fac_id;
	    DELETE FROM FACTURE_CTRL_CONVENTION WHERE fac_id=a_fac_id;
	    DELETE FROM FACTURE_CTRL_PLANCO WHERE fac_id=a_fac_id;
	    DELETE FROM FACTURE WHERE fac_id=a_fac_id;

        Budget.del_facture(my_exe_ordre, my_org_id, my_tcd_ordre, my_montant_budgetaire);
   END;

PROCEDURE log_facture (
      a_fac_id              FACTURE.fac_id%TYPE,
      a_utl_ordre           Z_FACTURE.zfac_utl_ordre%TYPE)
   IS
      my_zfac_id			Z_FACTURE.zfac_id%TYPE;
   BEGIN
		SELECT z_facture_seq.NEXTVAL INTO my_zfac_id FROM dual;

		-- facture.
		INSERT INTO Z_FACTURE SELECT my_zfac_id, SYSDATE, a_utl_ordre, f.*
		  FROM FACTURE f WHERE fac_id=a_fac_id;

		-- facture_ctrl_action.
		INSERT INTO Z_FACTURE_CTRL_ACTION SELECT z_facture_ctrl_action_seq.NEXTVAL, f.*, my_zfac_id
		  FROM FACTURE_CTRL_ACTION f WHERE fac_id=a_fac_id;

		-- facture_ctrl_analytique.
		INSERT INTO Z_FACTURE_CTRL_ANALYTIQUE SELECT z_facture_ctrl_analytique_seq.NEXTVAL, f.*, my_zfac_id
		  FROM FACTURE_CTRL_ANALYTIQUE f WHERE fac_id=a_fac_id;

		-- facture_ctrl_convention.
		INSERT INTO Z_FACTURE_CTRL_CONVENTION SELECT z_facture_ctrl_convention_seq.NEXTVAL, f.*, my_zfac_id
		  FROM FACTURE_CTRL_CONVENTION f WHERE fac_id=a_fac_id;

		-- facture_ctrl_planco.
		INSERT INTO Z_FACTURE_CTRL_PLANCO SELECT z_facture_ctrl_planco_seq.NEXTVAL, f.*, my_zfac_id
		  FROM FACTURE_CTRL_PLANCO f WHERE fac_id=a_fac_id;
   END;

PROCEDURE ins_facture_ctrl_action (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fact_id	               FACTURE_CTRL_ACTION.fact_id%TYPE;
       my_lolf_id	  	   		   FACTURE_CTRL_ACTION.lolf_id%TYPE;
       my_fact_montant_budgetaire  FACTURE_CTRL_ACTION.fact_montant_budgetaire%TYPE;
       my_fact_ht_saisie	  	   FACTURE_CTRL_ACTION.fact_ht_saisie%TYPE;
       my_fact_tva_saisie		   FACTURE_CTRL_ACTION.fact_tva_saisie%TYPE;
       my_fact_ttc_saisie		   FACTURE_CTRL_ACTION.fact_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_ACTION.fact_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_facture_ctrl_action)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere l'action.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_lolf_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fact_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fact_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fact_ht_saisie<0 OR my_fact_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (ins_facture_ctrl_action)');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_fact_tva_saisie := my_fact_ttc_saisie - my_fact_ht_saisie;
			IF my_fact_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_facture_ctrl_action)');
            END IF;
  			my_fact_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fact_ht_saisie,my_fact_ttc_saisie);

			-- on teste si c'est le dernier ou si il n'y a pas assez de dispo.
      		IF SUBSTR(my_chaine,1,1) = '$' OR
			  my_fac_montant_budgetaire <= my_somme + my_fact_montant_budgetaire THEN
			    my_fact_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_action_seq.NEXTVAL INTO my_fact_id FROM dual;

			INSERT INTO FACTURE_CTRL_ACTION VALUES (my_fact_id,
			       a_exe_ordre, a_fac_id, my_lolf_id, my_fact_montant_budgetaire, my_fact_montant_budgetaire,
				   my_fact_ht_saisie, my_fact_tva_saisie, my_fact_ttc_saisie, SYSDATE);

	        Verifier.verifier_action(a_exe_ordre, my_org_id, my_tcd_ordre, my_lolf_id);
			Apres_Facture.ins_facture_ctrl_action(my_fact_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_fact_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_facture_ctrl_analytique (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fana_id	               FACTURE_CTRL_ANALYTIQUE.fana_id%TYPE;
       my_can_id	  	   		   FACTURE_CTRL_ANALYTIQUE.can_id%TYPE;
       my_fana_montant_budgetaire  FACTURE_CTRL_ANALYTIQUE.fana_montant_budgetaire%TYPE;
       my_fana_ht_saisie	  	   FACTURE_CTRL_ANALYTIQUE.fana_ht_saisie%TYPE;
       my_fana_tva_saisie		   FACTURE_CTRL_ANALYTIQUE.fana_tva_saisie%TYPE;
       my_fana_ttc_saisie		   FACTURE_CTRL_ANALYTIQUE.fana_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_ANALYTIQUE.fana_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_facture_ctrl_analytique)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le code analytique.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_can_id FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fana_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fana_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fana_ht_saisie<0 OR my_fana_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (ins_facture_ctrl_analytique)');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_fana_tva_saisie := my_fana_ttc_saisie - my_fana_ht_saisie;
			IF my_fana_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_facture_ctrl_analytique)');
            END IF;
  			my_fana_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fana_ht_saisie,my_fana_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_fac_montant_budgetaire <= my_somme + my_fana_montant_budgetaire THEN
			    my_fana_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_analytique_seq.NEXTVAL INTO my_fana_id FROM dual;

			INSERT INTO FACTURE_CTRL_ANALYTIQUE VALUES (my_fana_id,
			       a_exe_ordre, a_fac_id, my_can_id, my_fana_montant_budgetaire, my_fana_montant_budgetaire,
				   my_fana_ht_saisie, my_fana_tva_saisie, my_fana_ttc_saisie, SYSDATE);

			Verifier.verifier_analytique(a_exe_ordre, my_org_id, my_tcd_ordre, my_can_id);
	    	Apres_Facture.ins_facture_ctrl_analytique(my_fana_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_fana_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_facture_ctrl_convention (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fcon_id	               FACTURE_CTRL_CONVENTION.fcon_id%TYPE;
       my_con_ordre	  	   		   FACTURE_CTRL_CONVENTION.con_ordre%TYPE;
       my_fcon_montant_budgetaire  FACTURE_CTRL_CONVENTION.fcon_montant_budgetaire%TYPE;
       my_fcon_ht_saisie	  	   FACTURE_CTRL_CONVENTION.fcon_ht_saisie%TYPE;
       my_fcon_tva_saisie		   FACTURE_CTRL_CONVENTION.fcon_tva_saisie%TYPE;
       my_fcon_ttc_saisie		   FACTURE_CTRL_CONVENTION.fcon_ttc_saisie%TYPE;
       my_fac_montant_budgetaire   FACTURE.fac_montant_budgetaire%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre                FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_somme					   FACTURE_CTRL_CONVENTION.fcon_montant_budgetaire%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_facture_ctrl_convention)');
		END IF;

		SELECT fac_montant_budgetaire, tap_id, org_id, tcd_ordre
		       INTO my_fac_montant_budgetaire, my_tap_id, my_org_id, my_tcd_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;
		my_somme:=0;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere la convention.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_con_ordre FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fcon_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fcon_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fcon_ht_saisie<0 OR my_fcon_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (ins_facture_ctrl_convention)');
		    END IF;

			-- on calcule la tva et le montant budgetaire.
			my_fcon_tva_saisie := my_fcon_ttc_saisie - my_fcon_ht_saisie;
			IF my_fcon_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au TTC (ins_facture_ctrl_convention)');
            END IF;
  			my_fcon_montant_budgetaire:=Budget.calculer_budgetaire(a_exe_ordre,my_tap_id,my_org_id,
		           my_fcon_ht_saisie,my_fcon_ttc_saisie);

			-- on teste si il n'y a pas assez de dispo.
      		IF my_fac_montant_budgetaire <= my_somme + my_fcon_montant_budgetaire THEN
			    my_fcon_montant_budgetaire := my_fac_montant_budgetaire - my_somme;
	        END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_convention_seq.NEXTVAL INTO my_fcon_id FROM dual;

			INSERT INTO FACTURE_CTRL_CONVENTION VALUES (my_fcon_id,
			       a_exe_ordre, a_fac_id, my_con_ordre, my_fcon_montant_budgetaire, my_fcon_montant_budgetaire,
				   my_fcon_ht_saisie, my_fcon_tva_saisie, my_fcon_ttc_saisie, SYSDATE);

			Verifier.verifier_convention(a_exe_ordre, my_org_id, my_tcd_ordre, my_con_ordre);
	    	Apres_Facture.ins_facture_ctrl_convention(my_fcon_id);

			-- mise a jour de la somme de controle.
	  		my_somme:=my_somme + my_fcon_montant_budgetaire;
		END LOOP;

   END;

PROCEDURE ins_facture_ctrl_planco (
      a_exe_ordre     FACTURE.exe_ordre%TYPE,
	  a_fac_id		  FACTURE.fac_id%TYPE,
	  a_chaine		  VARCHAR2
   ) IS
       my_fpco_id	               FACTURE_CTRL_PLANCO.fpco_id%TYPE;
       my_pco_num	  	   		   FACTURE_CTRL_PLANCO.pco_num%TYPE;
       my_fpco_ht_saisie	  	   FACTURE_CTRL_PLANCO.fpco_ht_saisie%TYPE;
       my_fpco_tva_saisie		   FACTURE_CTRL_PLANCO.fpco_tva_saisie%TYPE;
       my_fpco_ttc_saisie		   FACTURE_CTRL_PLANCO.fpco_ttc_saisie%TYPE;
	   my_tap_id				   FACTURE.tap_id%TYPE;
   	   my_org_id				   FACTURE.org_id%TYPE;
	   my_tcd_ordre				   FACTURE.tcd_ordre%TYPE;
       my_nb	  				   INTEGER;
       my_chaine				   VARCHAR2(30000);
	   my_utl_ordre                FACTURE.utl_ordre%TYPE;
   BEGIN
		SELECT COUNT(*) INTO my_nb FROM FACTURE WHERE fac_id=a_fac_id;
		IF my_nb<>1 THEN
		   RAISE_APPLICATION_ERROR(-20001,'La facture n''existe pas (fac_id='||a_fac_id||') (ins_facture_ctrl_planco)');
		END IF;

		SELECT tap_id, org_id, tcd_ordre, utl_ordre
		       INTO my_tap_id, my_org_id, my_tcd_ordre, my_utl_ordre
          FROM FACTURE WHERE fac_id=a_fac_id;

        my_chaine:=a_chaine;

		LOOP
      		IF SUBSTR(my_chaine,1,1)='$' THEN EXIT; END IF;

			-- on recupere le compte d'imputation.
			SELECT SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1) INTO my_pco_num FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ht.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fpco_ht_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));
			-- on recupere le ttc.
			SELECT grhum.En_Nombre(SUBSTR(my_chaine,1,INSTR(my_chaine,'$')-1)) INTO my_fpco_ttc_saisie FROM dual;
			my_chaine:=SUBSTR(my_chaine,INSTR(my_chaine,'$')+1,LENGTH(my_chaine));

	   		-- verification que les montants sont bien positifs !!!.
			IF my_fpco_ht_saisie<0 OR my_fpco_ttc_saisie<0 THEN
		       RAISE_APPLICATION_ERROR(-20001, 'La facture doit se faire pour un montant positif (ins_facture_ctrl_planco)');
		    END IF;

			-- on calcule la tva.
			my_fpco_tva_saisie := my_fpco_ttc_saisie - my_fpco_ht_saisie;
			IF my_fpco_tva_saisie<0 THEN
               RAISE_APPLICATION_ERROR(-20001, 'Probleme : le HT est superieur au  (ins_facture_ctrl_planco)');
            END IF;

			-- insertion dans la base.
			SELECT facture_ctrl_planco_seq.NEXTVAL INTO my_fpco_id FROM dual;

			INSERT INTO FACTURE_CTRL_PLANCO VALUES (my_fpco_id,
			       a_exe_ordre, a_fac_id, my_pco_num, my_fpco_ht_saisie, my_fpco_ht_saisie,
				   my_fpco_tva_saisie, my_fpco_tva_saisie, my_fpco_ttc_saisie, SYSDATE);

			Verifier.verifier_planco(a_exe_ordre, my_org_id, my_tcd_ordre, my_pco_num, my_utl_ordre);
	    	Apres_Facture.ins_facture_ctrl_planco(my_fpco_id);
		END LOOP;
   END;
   
   
   
   PROCEDURE upd_date_facture_restrinct (
      a_exe_ordre         FACTURE.exe_ordre%TYPE,
	  a_fac_id			  FACTURE.fac_id%TYPE)
    IS
    cpt integer;
    BEGIN
    
    select count(*) into cpt from V_EXERCICE where V_EXERCICE.EXE_ORDRE = a_exe_ordre and V_EXERCICE.EXE_STAT_FAC = 'R';
    if(cpt=1)
    then
        update  FACTURE
    set FAC_DATE_SAISIE =to_date('31/12/'||a_exe_ordre||' 12:00:00','DD/MM/YYYY HH24:MI:SS')
    where fac_id = a_fac_id; 
    END IF;
    END;

END;

/

