--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package APRES_FACTURE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."APRES_FACTURE" IS

-- procedure de post-traitement eventuel - a faire apres les manips dans les factures

PROCEDURE ins_facture   (a_fac_id       FACTURE.fac_id%TYPE);
PROCEDURE upd_facture   (a_fac_id       FACTURE.fac_id%TYPE);
PROCEDURE del_facture   (a_fac_id       FACTURE.fac_id%TYPE);
PROCEDURE ins_facture_ctrl_action       (a_fact_id      FACTURE_CTRL_ACTION.fact_id%TYPE);
PROCEDURE ins_facture_ctrl_analytique   (a_fana_id      FACTURE_CTRL_ANALYTIQUE.fana_id%TYPE);
PROCEDURE ins_facture_ctrl_convention   (a_fcon_id      FACTURE_CTRL_CONVENTION.fcon_id%TYPE);
PROCEDURE ins_facture_ctrl_planco       (a_fpco_id      FACTURE_CTRL_PLANCO.fpco_id%TYPE);

END;

/

