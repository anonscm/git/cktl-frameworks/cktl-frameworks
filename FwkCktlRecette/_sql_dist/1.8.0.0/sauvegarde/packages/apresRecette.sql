--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package APRES_RECETTE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."APRES_RECETTE" IS

-- procedure de post-traitement - a faire apres les manips dans les recettes

PROCEDURE ins_recette_papier   (a_rpp_id       RECETTE_PAPIER.rpp_id%TYPE);
PROCEDURE upd_recette_papier   (a_rpp_id       RECETTE_PAPIER.rpp_id%TYPE);
PROCEDURE del_recette_papier   (a_rpp_id       RECETTE_PAPIER.rpp_id%TYPE);
PROCEDURE ins_recette   (a_rec_id       RECETTE.rec_id%TYPE);
PROCEDURE del_recette   (a_rec_id       RECETTE.rec_id%TYPE);
PROCEDURE ins_recette_ctrl_action       (a_ract_id      RECETTE_CTRL_ACTION.ract_id%TYPE);
PROCEDURE ins_recette_ctrl_analytique   (a_rana_id      RECETTE_CTRL_ANALYTIQUE.rana_id%TYPE);
PROCEDURE ins_recette_ctrl_convention   (a_rcon_id      RECETTE_CTRL_CONVENTION.rcon_id%TYPE);
PROCEDURE ins_recette_ctrl_planco       (a_rpco_id      RECETTE_CTRL_PLANCO.rpco_id%TYPE);
PROCEDURE ins_recette_ctrl_planco_tva   (a_rpcotva_id   RECETTE_CTRL_PLANCO_TVA.rpcotva_id%TYPE);
PROCEDURE ins_recette_ctrl_planco_ctp   (a_rpcoctp_id   RECETTE_CTRL_PLANCO_CTP.rpcoctp_id%TYPE);

END;

/

