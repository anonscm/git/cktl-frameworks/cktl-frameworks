--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body TYPE_ETAT
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."TYPE_ETAT" 
IS

   FUNCTION get_etat_valide
      RETURN v_type_etat.tyet_id%TYPE
   IS
   BEGIN
      RETURN get_etat('VALIDE');
   END;

   FUNCTION get_etat_annule
      RETURN v_type_etat.tyet_id%TYPE
   IS
   BEGIN
      RETURN get_etat('ANNULE');
   END;

   FUNCTION get_etat_canal_valide
      RETURN v_type_etat.tyet_id%TYPE
   IS
   BEGIN
      RETURN get_etat('VALIDE');
   END;

   FUNCTION get_etat_canal_utilisable
      RETURN v_type_etat.tyet_id%TYPE
   IS
   BEGIN
      RETURN get_etat('OUI');
   END;

   FUNCTION get_etat_canal_public
      RETURN v_type_etat.tyet_id%TYPE
   IS
   BEGIN
      RETURN get_etat('OUI');
   END;

   FUNCTION get_etat(a_tyet_libelle v_type_etat.tyet_libelle%TYPE)
      RETURN v_type_etat.tyet_id%TYPE
   IS
      my_tyet_id   v_type_etat.tyet_id%TYPE;
   BEGIN
      my_tyet_id := Get_Type_Etat(a_tyet_libelle);

	  IF my_tyet_id IS NULL THEN
         RAISE_APPLICATION_ERROR(-20001, 'Probleme : L''etat "'||a_tyet_libelle||'" n''existe pas !');
	  END IF;

	  RETURN my_tyet_id;
   END;

END;

/

