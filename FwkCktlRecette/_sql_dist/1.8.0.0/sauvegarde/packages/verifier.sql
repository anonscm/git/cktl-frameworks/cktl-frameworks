--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package VERIFIER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."VERIFIER" IS

PROCEDURE verifier_droit_facture (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_utl_ordre      			FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_droit_recette (
      a_exe_ordre               RECETTE.exe_ordre%TYPE,
   	  a_utl_ordre      			RECETTE.utl_ordre%TYPE);


PROCEDURE verifier_budget (
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_tap_id                  FACTURE.tap_id%TYPE,
	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE);


PROCEDURE verifier_action(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_lolf_id                 FACTURE_CTRL_ACTION.lolf_id%TYPE);

PROCEDURE verifier_analytique(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_can_id                  FACTURE_CTRL_ANALYTIQUE.can_id%TYPE);

PROCEDURE verifier_convention(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_con_ordre                 FACTURE_CTRL_CONVENTION.con_ordre%TYPE);

PROCEDURE verifier_planco(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_tva(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_planco_ctp(
      a_exe_ordre               FACTURE.exe_ordre%TYPE,
   	  a_org_id                  FACTURE.org_id%TYPE,
   	  a_tcd_ordre               FACTURE.tcd_ordre%TYPE,
	  a_pco_num                 FACTURE_CTRL_PLANCO.pco_num%TYPE,
	  a_utl_ordre               FACTURE.utl_ordre%TYPE);

PROCEDURE verifier_fournisseur (
   	  a_fou_ordre		FACTURE.fou_ordre%TYPE);

PROCEDURE verifier_personne (
   	  a_pers_id		FACTURE.pers_id%TYPE);

-- ne sert pas pour le moment --
PROCEDURE verifier_monnaie;

PROCEDURE verifier_organ(
   	  a_org_id			FACTURE.org_id%TYPE,
	  a_tcd_ordre		FACTURE.tcd_ordre%TYPE);

PROCEDURE verifier_rib (
   	  a_fou_ordre		v_ribfour_ulr.fou_ordre%TYPE,
   	  a_rib_ordre		v_ribfour_ulr.rib_ordre%TYPE,
   	  a_mor_ordre		RECETTE_PAPIER.mor_ordre%TYPE,
   	  a_exe_ordre		RECETTE_PAPIER.exe_ordre%TYPE);

PROCEDURE verifier_facture_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_recette_pap_coherence(a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_recette_coherence(a_rec_id RECETTE.rec_id%TYPE);

PROCEDURE verifier_fac_rec_coherence(a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_utilisation_facture (a_fac_id FACTURE.fac_id%TYPE);

PROCEDURE verifier_util_recette_papier (a_rpp_id RECETTE_PAPIER.rpp_id%TYPE);

PROCEDURE verifier_utilisation_recette (a_rec_id RECETTE.rec_id%TYPE);

END;

/

