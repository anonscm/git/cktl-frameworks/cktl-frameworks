--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package REDUIRE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "JEFY_RECETTE"."REDUIRE" IS

--
-- procedures appelees par le package api
--

-- format des chaines
-- a_chaine_action: lolf_id$ract_ht_saisie$ract_ttc_saisie$...$
-- a_chaine_analytique: can_id$rana_ht_saisie$rana_ttc_saisie$...$
-- a_chaine_convention: con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$
-- a_chaine_planco: pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$
-- a_chaine_planco_tva: pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$
-- a_chaine_planco_ctp: pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$
PROCEDURE ins_reduction (
      a_rec_id IN OUT		RECETTE.rec_id%TYPE,
	  a_exe_ordre			RECETTE.exe_ordre%TYPE,
      a_rec_numero IN OUT	RECETTE.rec_numero%TYPE,
      a_rec_lib	   	  		RECETTE.rec_lib%TYPE,
	  a_rpp_nb_piece		RECETTE_PAPIER.rpp_nb_piece%TYPE,
	  a_rec_ht_saisie		RECETTE.rec_ht_saisie%TYPE,
	  a_rec_ttc_saisie		RECETTE.rec_ttc_saisie%TYPE,
	  a_utl_ordre			RECETTE.utl_ordre%TYPE,
	  a_rec_id_reduction	RECETTE.rec_id_reduction%TYPE,
	  a_chaine_action		VARCHAR2,
	  a_chaine_analytique	VARCHAR2,
	  a_chaine_convention	VARCHAR2,
	  a_chaine_planco		VARCHAR2,
	  a_chaine_planco_tva	VARCHAR2,
	  a_chaine_planco_ctp	VARCHAR2);

PROCEDURE del_reduction (
      a_rec_id              RECETTE.rec_id%TYPE,
      a_utl_ordre           Z_RECETTE.zrec_utl_ordre%TYPE);

PROCEDURE ins_reduction_ctrl_action (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_reduction_ctrl_analytique (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_reduction_ctrl_convention (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_reduction_ctrl_planco (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_reduction_ctrl_planco_tva (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

PROCEDURE ins_reduction_ctrl_planco_ctp (
      a_exe_ordre           RECETTE.exe_ordre%TYPE,
	  a_rec_id		        RECETTE.rec_id%TYPE,
	  a_chaine		        VARCHAR2);

END;

/

