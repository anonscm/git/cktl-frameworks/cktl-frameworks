--------------------------------------------------------
--  Fichier créé - vendredi-mai-11-2012   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body UTILS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY "JEFY_RECETTE"."UTILS" is

    PROCEDURE annul_del_recette (zrecId z_recette.zrec_id%type) IS
        flag integer;
        rec_zrecette jefy_recette.z_recette%rowtype;
        my_rpco_id z_RECETTE_CTRL_PLANCO.rpco_id%type;
        CURSOR liste IS SELECT rpco_id FROM z_RECETTE_CTRL_PLANCO WHERE zrec_id =zrecId;
    BEGIN
        select count(*) into flag from jefy_recette.z_recette where zrec_id=zrecid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR(-20001, 'Aucun z_recette trouve pour '|| zrecid);
        end if;
    
        select * into rec_zrecette from jefy_recette.z_recette where zrec_id=zrecid;
     
     
        -- verifier que la rec_id n'est pas presente dans recette
        select count(*) into flag from jefy_recette.recette where rec_id=rec_zrecette.rec_id;
        if (flag >0) then
            RAISE_APPLICATION_ERROR(-20001, 'Rec_id '|| rec_zrecette.rec_id || ' deja presente dans la table recette.');
        end if;

   
        INSERT INTO RECETTE SELECT
           REC_ID, EXE_ORDRE, RPP_ID, 
           REC_NUMERO, FAC_ID, REC_LIB, 
           REC_DATE_SAISIE, REC_MONTANT_BUDGETAIRE, REC_HT_SAISIE, 
           REC_TVA_SAISIE, REC_TTC_SAISIE, TAP_ID, 
           TYET_ID, UTL_ORDRE, REC_ID_REDUCTION
            FROM Z_RECETTE r WHERE zrec_id=zrecid;

   
        INSERT INTO RECETTE_CTRL_ACTION SELECT
            RACT_ID, EXE_ORDRE, 
               REC_ID, LOLF_ID, RACT_MONTANT_BUDGETAIRE, 
               RACT_HT_SAISIE, RACT_TVA_SAISIE, RACT_TTC_SAISIE, 
               RACT_DATE_SAISIE
            FROM JEFY_RECETTE.Z_RECETTE_CTRL_ACTION
            WHERE zrec_id=zrecid;
   
        INSERT INTO RECETTE_CTRL_ANALYTIQUE SELECT
                    RANA_ID, EXE_ORDRE, 
               REC_ID, CAN_ID, RANA_MONTANT_BUDGETAIRE, 
               RANA_HT_SAISIE, RANA_TVA_SAISIE, RANA_TTC_SAISIE, 
               RANA_DATE_SAISIE
            FROM JEFY_RECETTE.Z_RECETTE_CTRL_ANALYTIQUE
            WHERE zrec_id=zrecid;

        INSERT INTO RECETTE_CTRL_CONVENTION SELECT 
            RCON_ID, EXE_ORDRE, 
               REC_ID, CON_ORDRE, RCON_MONTANT_BUDGETAIRE, 
               RCON_HT_SAISIE, RCON_TVA_SAISIE, RCON_TTC_SAISIE, 
               RCON_DATE_SAISIE
            FROM JEFY_RECETTE.Z_RECETTE_CTRL_CONVENTION
            WHERE zrec_id=zrecid;


           OPEN liste();
          LOOP
           FETCH liste INTO my_rpco_id;
            EXIT WHEN liste%NOTFOUND;

           INSERT INTO RECETTE_CTRL_PLANCO SELECT 
                RPCO_ID, EXE_ORDRE, 
               REC_ID, PCO_NUM, TIT_ID, 
               RPCO_HT_SAISIE, RPCO_TVA_SAISIE, RPCO_TTC_SAISIE, 
               RPCO_DATE_SAISIE, TBO_ORDRE
            FROM JEFY_RECETTE.Z_RECETTE_CTRL_PLANCO  r 
            WHERE rpco_id = my_rpco_id;

           INSERT INTO RECETTE_CTRL_PLANCO_TVA SELECT 
                 RPCOTVA_ID, EXE_ORDRE, 
                   RPCO_ID, PCO_NUM, RPCOTVA_TVA_SAISIE, 
                   RPCOTVA_DATE_SAISIE, GES_CODE
                FROM JEFY_RECETTE.Z_RECETTE_CTRL_PLANCO_TVA 
                 WHERE rpco_id = my_rpco_id;

           INSERT INTO RECETTE_CTRL_PLANCO_CTP SELECT 
                 RPCOCTP_ID, EXE_ORDRE, 
                   RPCO_ID, PCO_NUM, RPCOCTP_TTC_SAISIE, 
                   RPCOCTP_DATE_SAISIE, GES_CODE
                FROM JEFY_RECETTE.Z_RECETTE_CTRL_PLANCO_CTP 
                 WHERE rpco_id = my_rpco_id;

          END LOOP;
        CLOSE liste;

    END annul_del_recette;


    PROCEDURE annul_del_recette_papier (zrppId z_recette_papier.zrpp_id%type) is
        flag integer;
        rec_zrecette_papier jefy_recette.z_recette_papier%rowtype;
    begin
    
        select count(*) into flag from jefy_recette.z_recette_papier where zrpp_id=zrppId;
        if (flag=0) then
            RAISE_APPLICATION_ERROR(-20001, 'Aucun z_recette_papier trouve pour zrppId '|| zrppId);
        end if;
    
        select * into rec_zrecette_papier from jefy_recette.z_recette_papier where zrpp_id=zrppId;
    
            -- verifier que la rec_id n'est pas presente dans recette
        select count(*) into flag from jefy_recette.recette_papier where rpp_id=rec_zrecette_papier.rpp_id;
        if (flag >0) then
            RAISE_APPLICATION_ERROR(-20001, 'Rpp_id '|| rec_zrecette_papier.rpp_id || ' deja presente dans la table recette_papier.');
        end if;
    
        INSERT INTO RECETTE_PAPIER 
        SELECT  RPP_ID, EXE_ORDRE, RPP_NUMERO, 
                   RPP_HT_SAISIE, RPP_TVA_SAISIE, RPP_TTC_SAISIE, 
                   PERS_ID, FOU_ORDRE, RIB_ORDRE, 
                   MOR_ORDRE, RPP_DATE_RECETTE, RPP_DATE_SAISIE, 
                   RPP_DATE_RECEPTION, RPP_DATE_SERVICE_FAIT, RPP_NB_PIECE, 
                   UTL_ORDRE, RPP_VISIBLE
                FROM JEFY_RECETTE.Z_RECETTE_PAPIER r WHERE zrpp_id=zrppId;
          
          
          

    end annul_del_recette_papier;


PROCEDURE annul_del_facture (zfacId z_facture.zfac_id%type) IS
        flag integer;
        rec_zfacture jefy_recette.z_facture%rowtype;

    BEGIN
        select count(*) into flag from jefy_recette.z_facture where zfac_id=zfacid;
        if (flag=0) then
            RAISE_APPLICATION_ERROR(-20001, 'Aucun z_facture trouve pour '|| zfacid);
        end if;
    
        select * into rec_zfacture from jefy_recette.z_facture where zfac_id=zfacid;
     
     
        -- verifier que la fac_id n'est pas presente dans facutre
        select count(*) into flag from jefy_recette.facture where fac_id=rec_zfacture.fac_id;
        if (flag >0) then
            RAISE_APPLICATION_ERROR(-20001, 'fac_id '|| rec_zfacture.fac_id || ' deja present dans la table facture.');
        end if;

   
        -- facture.
		INSERT INTO FACTURE SELECT 
            FAC_ID, EXE_ORDRE, FAC_NUMERO, 
               FOU_ORDRE, PERS_ID, FAC_DATE_SAISIE, 
               FAC_LIB, FAC_MONTANT_BUDGETAIRE, FAC_MONTANT_BUDGETAIRE_RESTE, 
               FAC_HT_SAISIE, FAC_TVA_SAISIE, FAC_TTC_SAISIE, 
               MOR_ORDRE, ORG_ID, TAP_ID, 
               TCD_ORDRE, ECHE_ID, TYAP_ID, 
               TYET_ID, UTL_ORDRE
            FROM JEFY_RECETTE.Z_FACTURE
            WHERE zfac_id=zfacid;

        INSERT INTO FACTURE_CTRL_ACTION SELECT 
                FACT_ID, EXE_ORDRE, FAC_ID, 
                   LOLF_ID, FACT_MONTANT_BUDGETAIRE, FACT_MONTANT_BUDGETAIRE_RESTE, 
                   FACT_HT_SAISIE, FACT_TVA_SAISIE, FACT_TTC_SAISIE, 
                   FACT_DATE_SAISIE
            FROM JEFY_RECETTE.Z_FACTURE_CTRL_ACTION
            WHERE zfac_id=zfacid;

      INSERT INTO FACTURE_CTRL_ANALYTIQUE SELECT 
            FANA_ID, EXE_ORDRE, FAC_ID, 
               CAN_ID, FANA_MONTANT_BUDGETAIRE, FANA_MONTANT_BUDGETAIRE_RESTE, 
               FANA_HT_SAISIE, FANA_TVA_SAISIE, FANA_TTC_SAISIE, 
               FANA_DATE_SAISIE
            FROM JEFY_RECETTE.Z_FACTURE_CTRL_ANALYTIQUE
            WHERE zfac_id=zfacid;


      INSERT INTO FACTURE_CTRL_CONVENTION SELECT 
            FCON_ID, EXE_ORDRE, FAC_ID, 
               CON_ORDRE, FCON_MONTANT_BUDGETAIRE, FCON_MONTANT_BUDGETAIRE_RESTE, 
               FCON_HT_SAISIE, FCON_TVA_SAISIE, FCON_TTC_SAISIE, 
               FCON_DATE_SAISIE
            FROM JEFY_RECETTE.Z_FACTURE_CTRL_CONVENTION
            WHERE zfac_id=zfacid;


      INSERT INTO FACTURE_CTRL_PLANCO SELECT 
        FPCO_ID, EXE_ORDRE, FAC_ID, 
           PCO_NUM, FPCO_HT_SAISIE, FPCO_HT_RESTE, 
           FPCO_TVA_SAISIE, FPCO_TVA_RESTE, FPCO_TTC_SAISIE, 
           FPCO_DATE_SAISIE
            FROM JEFY_RECETTE.Z_FACTURE_CTRL_PLANCO
            WHERE zfac_id=zfacid;

    END annul_del_facture;

END utils; 

/

