SET DEFINE OFF;
--
--
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--
--
--
-- Fichier :  n°1/1
-- Type : DDL
-- Schéma modifié :  JEFY_RECETTE
-- Schéma d'execution du script : GRHUM
-- Numéro de version : 1.7.1.1
-- Date de publication : 31/05/2013
-- Licence : CeCILL version 2
--

-- pre requis--
--	Patch sql Maracuja 1.9.3.5

-- ___________________________________________________________________
-- suppression des factures dont la recette a été rejettée (si pas de facture_papier)
-- ___________________________________________________________________


whenever sqlerror exit sql.sqlcode;

exec JEFY_ADMIN.PATCH_UTIL.check_patch_installed ( 4, '1.9.3.5', 'MARACUJA' );

DECLARE
-- Parcours des factures sans recettes
CURSOR C_FACT_SANS_RECETTE IS
SELECT
     f.exe_ordre as exercice, f.fac_numero as numero, f.fac_id as facid, zr.zrec_utl_ordre as utlordre
 FROM
    JEFY_RECETTE.FACTURE f,
    JEFY_RECETTE.RECETTE r,
    JEFY_RECETTE.Z_RECETTE zr,
    JEFY_ADMIN.TYPE_APPLICATION ta
     -- la facture doit être de type recette
WHERE ta.tyap_strid = 'RECETTE'
     AND f.tyap_id = ta.tyap_id
     -- la facture doit être sans recette
     AND r.fac_id(+) = f.fac_id
     AND r.rec_id IS NULL
     -- on va chercher le dernier utl_ordre
     AND zr.fac_id = f.fac_id
     AND zr.zrec_id = ( SELECT MAX(zr2.zrec_id)
                        FROM JEFY_RECETTE.Z_RECETTE zr2
                        WHERE zr2.fac_id = f.fac_id )
    -- Ce ne doit pas être une réduction de recette
    AND zr.rec_id_reduction IS NULL
    -- Le Z_Recette doit être lié à au moins à Z_RECETTE_CTRL_CONVENTION
    AND (SELECT COUNT(*)  FROM JEFY_RECETTE.Z_RECETTE_CTRL_CONVENTION zrcco WHERE zrcco.rec_id=zr.rec_id)>0
	-- La facture ne doit être lié à aucune autre convention
	  AND (SELECT COUNT(*)  FROM JEFY_RECETTE.RECETTE_CTRL_CONVENTION rcco WHERE rcco.rec_id=zr.rec_id)=0
	-- Option de restriction exercice
    -- AND f.exe_ordre='2012'
ORDER BY  f.fac_id;

BEGIN
 FOR REC IN C_FACT_SANS_RECETTE LOOP
  -- DBMS_OUTPUT.PUT_LINE (REC.facid||' - '|| REC.utlordre);
  jefy_recette.api.del_facture(REC.facid, REC.utlordre);
 END LOOP;

 -- Maj version
 insert into jefy_recette.db_version (db_version, db_date, db_comment) values ('1.7.1.1',to_date('31/05/2013', 'dd/mm/yyyy'), 'Suppression des factures dont la recette a été rejettée');

 commit;

END;
