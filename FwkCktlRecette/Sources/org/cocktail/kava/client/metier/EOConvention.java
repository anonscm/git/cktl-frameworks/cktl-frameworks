
// EOConvention.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSValidation;

public class EOConvention extends _EOConvention {

	public static final String	PRIMARY_KEY_KEY	= "conOrdre";

	public EOConvention() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Cet objet au format texte.
	 */
	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(exercice().exeExercice());
		buf.append("-");
		buf.append(new NSNumberFormatter("0000;-0000").format(conIndex()));
		return buf.toString();
	}
}
