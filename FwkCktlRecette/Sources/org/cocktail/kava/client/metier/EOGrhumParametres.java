

// EOGrhumParametres.java
// 
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOGrhumParametres extends _EOGrhumParametres {

    public static final String PARAM_C_PAYS_DEFAUT="GRHUM_C_PAYS_DEFAUT";
    public static final String PARAM_ANNUAIRE_FOU_VALIDE_INTERNE="ANNUAIRE_FOU_VALIDE_INTERNE";
    public static final String PARAM_GEDFS_SERVICE_HOST="GEDFS_SERVICE_HOST";
    public static final String PARAM_GEDFS_SERVICE_PORT="GEDFS_SERVICE_PORT";
    public static final String PARAM_TIMEZONE="GRHUM_TIMEZONE";
    
    public EOGrhumParametres() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
