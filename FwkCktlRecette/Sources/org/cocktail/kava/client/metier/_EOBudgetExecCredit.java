// _EOBudgetExecCredit.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetExecCredit.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOBudgetExecCredit extends  EOGenericRecord {
	public static final String ENTITY_NAME = "BudgetExecCredit";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_budget_exec_credit";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdxcId";

	public static final String BDXC_CONVENTIONS_KEY = "bdxcConventions";
	public static final String BDXC_CREDITS_KEY = "bdxcCredits";
	public static final String BDXC_DBMS_KEY = "bdxcDbms";
	public static final String BDXC_DEBITS_KEY = "bdxcDebits";
	public static final String BDXC_DISPONIBLE_KEY = "bdxcDisponible";
	public static final String BDXC_DISPO_RESERVE_KEY = "bdxcDispoReserve";
	public static final String BDXC_ENGAGEMENTS_KEY = "bdxcEngagements";
	public static final String BDXC_MANDATS_KEY = "bdxcMandats";
	public static final String BDXC_OUVERTS_KEY = "bdxcOuverts";
	public static final String BDXC_PRIMITIFS_KEY = "bdxcPrimitifs";
	public static final String BDXC_PROVISOIRES_KEY = "bdxcProvisoires";
	public static final String BDXC_RELIQUATS_KEY = "bdxcReliquats";
	public static final String BDXC_RESERVE_KEY = "bdxcReserve";
	public static final String BDXC_VOTES_KEY = "bdxcVotes";

// Attributs non visibles
	public static final String BDXC_ID_KEY = "bdxcId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String BDXC_CONVENTIONS_COLKEY = "BDXC_CONVENTIONS";
	public static final String BDXC_CREDITS_COLKEY = "BDXC_CREDITS";
	public static final String BDXC_DBMS_COLKEY = "BDXC_DBMS";
	public static final String BDXC_DEBITS_COLKEY = "BDXC_DEBITS";
	public static final String BDXC_DISPONIBLE_COLKEY = "BDXC_DISPONIBLE";
	public static final String BDXC_DISPO_RESERVE_COLKEY = "BDXC_DISPO_RESERVE";
	public static final String BDXC_ENGAGEMENTS_COLKEY = "BDXC_ENGAGEMENTS";
	public static final String BDXC_MANDATS_COLKEY = "BDXC_MANDATS";
	public static final String BDXC_OUVERTS_COLKEY = "BDXC_OUVERTS";
	public static final String BDXC_PRIMITIFS_COLKEY = "BDXC_PRIMITIFS";
	public static final String BDXC_PROVISOIRES_COLKEY = "BDXC_PROVISOIRES";
	public static final String BDXC_RELIQUATS_COLKEY = "BDXC_RELIQUATS";
	public static final String BDXC_RESERVE_COLKEY = "BDXC_RESERVE";
	public static final String BDXC_VOTES_COLKEY = "BDXC_VOTES";

	public static final String BDXC_ID_COLKEY = "BDXC_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";



	// Accessors methods
  public java.math.BigDecimal bdxcConventions() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_CONVENTIONS_KEY);
  }

  public void setBdxcConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal bdxcCredits() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_CREDITS_KEY);
  }

  public void setBdxcCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_CREDITS_KEY);
  }

  public java.math.BigDecimal bdxcDbms() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_DBMS_KEY);
  }

  public void setBdxcDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_DBMS_KEY);
  }

  public java.math.BigDecimal bdxcDebits() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_DEBITS_KEY);
  }

  public void setBdxcDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_DEBITS_KEY);
  }

  public java.math.BigDecimal bdxcDisponible() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_DISPONIBLE_KEY);
  }

  public void setBdxcDisponible(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_DISPONIBLE_KEY);
  }

  public Integer bdxcDispoReserve() {
    return (Integer) storedValueForKey(BDXC_DISPO_RESERVE_KEY);
  }

  public void setBdxcDispoReserve(Integer value) {
    takeStoredValueForKey(value, BDXC_DISPO_RESERVE_KEY);
  }

  public java.math.BigDecimal bdxcEngagements() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_ENGAGEMENTS_KEY);
  }

  public void setBdxcEngagements(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_ENGAGEMENTS_KEY);
  }

  public java.math.BigDecimal bdxcMandats() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_MANDATS_KEY);
  }

  public void setBdxcMandats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_MANDATS_KEY);
  }

  public java.math.BigDecimal bdxcOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_OUVERTS_KEY);
  }

  public void setBdxcOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_OUVERTS_KEY);
  }

  public java.math.BigDecimal bdxcPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_PRIMITIFS_KEY);
  }

  public void setBdxcPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal bdxcProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_PROVISOIRES_KEY);
  }

  public void setBdxcProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal bdxcReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_RELIQUATS_KEY);
  }

  public void setBdxcReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_RELIQUATS_KEY);
  }

  public Integer bdxcReserve() {
    return (Integer) storedValueForKey(BDXC_RESERVE_KEY);
  }

  public void setBdxcReserve(Integer value) {
    takeStoredValueForKey(value, BDXC_RESERVE_KEY);
  }

  public java.math.BigDecimal bdxcVotes() {
    return (java.math.BigDecimal) storedValueForKey(BDXC_VOTES_KEY);
  }

  public void setBdxcVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDXC_VOTES_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOOrgan organ() {
    return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
  }

  public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCreditDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
    }
  }


  public static EOBudgetExecCredit createBudgetExecCredit(EOEditingContext editingContext, java.math.BigDecimal bdxcConventions
, java.math.BigDecimal bdxcCredits
, java.math.BigDecimal bdxcDbms
, java.math.BigDecimal bdxcDebits
, java.math.BigDecimal bdxcDisponible
, Integer bdxcDispoReserve
, java.math.BigDecimal bdxcEngagements
, java.math.BigDecimal bdxcMandats
, java.math.BigDecimal bdxcOuverts
, java.math.BigDecimal bdxcPrimitifs
, java.math.BigDecimal bdxcProvisoires
, java.math.BigDecimal bdxcReliquats
, java.math.BigDecimal bdxcVotes
, org.cocktail.application.client.eof.EOExercice exercice, org.cocktail.kava.client.metier.EOOrgan organ, org.cocktail.application.client.eof.EOTypeCredit typeCreditDep) {
    EOBudgetExecCredit eo = (EOBudgetExecCredit) createAndInsertInstance(editingContext, _EOBudgetExecCredit.ENTITY_NAME);
		eo.setBdxcConventions(bdxcConventions);
		eo.setBdxcCredits(bdxcCredits);
		eo.setBdxcDbms(bdxcDbms);
		eo.setBdxcDebits(bdxcDebits);
		eo.setBdxcDisponible(bdxcDisponible);
		eo.setBdxcDispoReserve(bdxcDispoReserve);
		eo.setBdxcEngagements(bdxcEngagements);
		eo.setBdxcMandats(bdxcMandats);
		eo.setBdxcOuverts(bdxcOuverts);
		eo.setBdxcPrimitifs(bdxcPrimitifs);
		eo.setBdxcProvisoires(bdxcProvisoires);
		eo.setBdxcReliquats(bdxcReliquats);
		eo.setBdxcVotes(bdxcVotes);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeCreditDepRelationship(typeCreditDep);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOBudgetExecCredit.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOBudgetExecCredit.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOBudgetExecCredit creerInstance(EOEditingContext editingContext) {
		  		EOBudgetExecCredit object = (EOBudgetExecCredit)createAndInsertInstance(editingContext, _EOBudgetExecCredit.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOBudgetExecCredit localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetExecCredit)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOBudgetExecCredit localInstanceIn(EOEditingContext editingContext, EOBudgetExecCredit eo) {
    EOBudgetExecCredit localInstance = (eo == null) ? null : (EOBudgetExecCredit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOBudgetExecCredit#localInstanceIn a la place.
   */
	public static EOBudgetExecCredit localInstanceOf(EOEditingContext editingContext, EOBudgetExecCredit eo) {
		return EOBudgetExecCredit.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOBudgetExecCredit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOBudgetExecCredit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetExecCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetExecCredit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOBudgetExecCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOBudgetExecCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetExecCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetExecCredit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOBudgetExecCredit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetExecCredit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetExecCredit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOBudgetExecCredit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
