
// EORepartPersonneAdresse.java
//
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EORepartPersonneAdresse extends _EORepartPersonneAdresse {

	/** Type adresse. */
	public static final String KEY_TYPE_ADRESSE = "tadrCode";

	/**Principale ? */
	public static final String KEY_RPA_PRINCIPAL = "rpaPrincipal";

	/** Type adresse : Facturation. */
	public static final String FACTURATION_TYPE_ADRESSE = "FACT";

	/** Adresse principale OUI. */
	public static final String ADRESSE_PRINCIPALE_OUI = "O";

	public EORepartPersonneAdresse() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
