// _EOFactureCtrlConvention.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFactureCtrlConvention.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFactureCtrlConvention extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FactureCtrlConvention";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_ctrl_convention";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fconId";

	public static final String FAC_ID_KEY = "facId";
	public static final String FCON_DATE_SAISIE_KEY = "fconDateSaisie";
	public static final String FCON_HT_SAISIE_KEY = "fconHtSaisie";
	public static final String FCON_MONTANT_BUDGETAIRE_KEY = "fconMontantBudgetaire";
	public static final String FCON_MONTANT_BUDGETAIRE_RESTE_KEY = "fconMontantBudgetaireReste";
	public static final String FCON_TTC_SAISIE_KEY = "fconTtcSaisie";
	public static final String FCON_TVA_SAISIE_KEY = "fconTvaSaisie";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FCON_ID_KEY = "fconId";

//Colonnes dans la base de donnees
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FCON_DATE_SAISIE_COLKEY = "FCON_DATE_SAISIE";
	public static final String FCON_HT_SAISIE_COLKEY = "FCON_HT_SAISIE";
	public static final String FCON_MONTANT_BUDGETAIRE_COLKEY = "FCON_MONTANT_BUDGETAIRE";
	public static final String FCON_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FCON_MONTANT_BUDGETAIRE_RESTE";
	public static final String FCON_TTC_SAISIE_COLKEY = "FCON_TTC_SAISIE";
	public static final String FCON_TVA_SAISIE_COLKEY = "FCON_TVA_SAISIE";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FCON_ID_COLKEY = "FCON_ID";


	// Relationships
	public static final String CONVENTION_KEY = "convention";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public Integer facId() {
    return (Integer) storedValueForKey(FAC_ID_KEY);
  }

  public void setFacId(Integer value) {
    takeStoredValueForKey(value, FAC_ID_KEY);
  }

  public NSTimestamp fconDateSaisie() {
    return (NSTimestamp) storedValueForKey(FCON_DATE_SAISIE_KEY);
  }

  public void setFconDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FCON_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal fconHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FCON_HT_SAISIE_KEY);
  }

  public void setFconHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FCON_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal fconMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(FCON_MONTANT_BUDGETAIRE_KEY);
  }

  public void setFconMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FCON_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal fconMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(FCON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setFconMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FCON_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal fconTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FCON_TTC_SAISIE_KEY);
  }

  public void setFconTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FCON_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal fconTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FCON_TVA_SAISIE_KEY);
  }

  public void setFconTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FCON_TVA_SAISIE_KEY);
  }

  public org.cocktail.kava.client.metier.EOConvention convention() {
    return (org.cocktail.kava.client.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kava.client.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }


  public static EOFactureCtrlConvention createFactureCtrlConvention(EOEditingContext editingContext, Integer facId
, NSTimestamp fconDateSaisie
, java.math.BigDecimal fconHtSaisie
, java.math.BigDecimal fconMontantBudgetaire
, java.math.BigDecimal fconMontantBudgetaireReste
, java.math.BigDecimal fconTtcSaisie
, java.math.BigDecimal fconTvaSaisie
) {
    EOFactureCtrlConvention eo = (EOFactureCtrlConvention) createAndInsertInstance(editingContext, _EOFactureCtrlConvention.ENTITY_NAME);
		eo.setFacId(facId);
		eo.setFconDateSaisie(fconDateSaisie);
		eo.setFconHtSaisie(fconHtSaisie);
		eo.setFconMontantBudgetaire(fconMontantBudgetaire);
		eo.setFconMontantBudgetaireReste(fconMontantBudgetaireReste);
		eo.setFconTtcSaisie(fconTtcSaisie);
		eo.setFconTvaSaisie(fconTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOFactureCtrlConvention.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOFactureCtrlConvention.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOFactureCtrlConvention creerInstance(EOEditingContext editingContext) {
		  		EOFactureCtrlConvention object = (EOFactureCtrlConvention)createAndInsertInstance(editingContext, _EOFactureCtrlConvention.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOFactureCtrlConvention localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFactureCtrlConvention)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOFactureCtrlConvention localInstanceIn(EOEditingContext editingContext, EOFactureCtrlConvention eo) {
    EOFactureCtrlConvention localInstance = (eo == null) ? null : (EOFactureCtrlConvention)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOFactureCtrlConvention#localInstanceIn a la place.
   */
	public static EOFactureCtrlConvention localInstanceOf(EOEditingContext editingContext, EOFactureCtrlConvention eo) {
		return EOFactureCtrlConvention.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOFactureCtrlConvention fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOFactureCtrlConvention fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFactureCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFactureCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOFactureCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOFactureCtrlConvention fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFactureCtrlConvention eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFactureCtrlConvention)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOFactureCtrlConvention fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFactureCtrlConvention eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFactureCtrlConvention ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOFactureCtrlConvention fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
