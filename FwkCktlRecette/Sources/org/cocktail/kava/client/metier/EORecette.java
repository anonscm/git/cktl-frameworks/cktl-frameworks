// EORecette.java
//
package org.cocktail.kava.client.metier;

import org.cocktail.kava.client.finder.FinderTauxProrata;

import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EORecette extends _EORecette {

	public static final String PRIMARY_KEY_KEY = "recId";

	public boolean updateTauxProrata() {
		boolean isDirty = false;
		if (tauxProrata() == null) {
			setTauxProrataRelationship(FinderTauxProrata.tauxProrata100(this.editingContext()));
			isDirty = true;
		}
		return isDirty;
	}

	public boolean hasMultipleCtrls() {
		return hasMultipleCtrlActions() || hasMultipleCtrlAnalytiques() || hasMultipleCtrlConventions() || hasMultipleCtrlPlancos()
				|| hasMultipleCtrlPlancoTvas() || hasMultipleCtrlPlancoCtps();
	}

	public boolean hasMultipleCtrlActions() {
		return recetteCtrlActions() != null && recetteCtrlActions().count() > 1;
	}

	public boolean hasMultipleCtrlAnalytiques() {
		return recetteCtrlAnalytiques() != null && recetteCtrlAnalytiques().count() > 1;
	}

	public boolean hasMultipleCtrlConventions() {
		return recetteCtrlConventions() != null && recetteCtrlConventions().count() > 1;
	}

	public boolean hasMultipleCtrlPlancos() {
		return recetteCtrlPlancos() != null && recetteCtrlPlancos().count() > 1;
	}

	public boolean hasMultipleCtrlPlancoTvas() {
		if (recetteCtrlPlancos() == null || recetteCtrlPlancos().count() == 0) {
			return false;
		}
		for (int i = 0; i < recetteCtrlPlancos().count(); i++) {
			EORecetteCtrlPlanco rpco = (EORecetteCtrlPlanco) recetteCtrlPlancos().objectAtIndex(i);
			if (rpco.recetteCtrlPlancoTvas() != null && rpco.recetteCtrlPlancoTvas().count() > 1) {
				return true;
			}
		}
		return false;
	}

	public boolean hasMultipleCtrlPlancoCtps() {
		if (recetteCtrlPlancos() == null || recetteCtrlPlancos().count() == 0) {
			return false;
		}
		for (int i = 0; i < recetteCtrlPlancos().count(); i++) {
			EORecetteCtrlPlanco rpco = (EORecetteCtrlPlanco) recetteCtrlPlancos().objectAtIndex(i);
			if (rpco.recetteCtrlPlancoCtps() != null && rpco.recetteCtrlPlancoCtps().count() > 1) {
				return true;
			}
		}
		return false;
	}

	public boolean isTitred() {
		// if (recetteCtrlPlancos() != null) {
		// for (int i = 0; i < recetteCtrlPlancos().count(); i++) {
		// if (((EORecetteCtrlPlanco) recetteCtrlPlancos().objectAtIndex(i)).titId() != null) {
		// return true;
		// }
		// }
		// }
		// return false;
		// on peut faire ca pour l'instant parce que on sait qu'il n'y a toujours qu'un seul recetteCtrlPlanco par recette...
		// et que ca risque de durer ;-)
		if (titId() == null) {
			return false;
		}
		return true;
	}

	public boolean isVised() {
		if (isTitred() == false) {
			return false;
		}
		if (recetteCtrlPlancos() != null) {
			for (int i = 0; i < recetteCtrlPlancos().count(); i++) {
				EORecetteCtrlPlanco rpco = (EORecetteCtrlPlanco) recetteCtrlPlancos().objectAtIndex(i);
				if (rpco.titre() != null) {
					if (rpco.titre().titEtat() != null && rpco.titre().titEtat().equalsIgnoreCase("VISE")) {
						return true;
					}
				}
			}
		}
		return false;
	}

	public EORecette() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (recDateSaisie() == null) {
			throw new ValidationException("Il faut une date de creation pour la recette!");
		}
		if (recHtSaisie() == null) {
			throw new ValidationException("Il faut un montant HT pour la recette!");
		}
		if (recTtcSaisie() == null) {
			throw new ValidationException("Il faut un montant TTC pour la recette!");
		}
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la recette!");
		}
		if (facture() == null) {
			throw new ValidationException("Il faut une facture client pour la recette!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un utilisateur (agent) pour la recette!");
		}
		if (recetteCtrlActions() == null || recetteCtrlActions().count() == 0) {
			throw new ValidationException("Il faut au moins une action lolf renseignee pour la recette!!");
		}
		if (recetteCtrlPlancos() == null || recetteCtrlPlancos().count() == 0) {
			throw new ValidationException("Il faut au moins un plan comptable pour la recette!!");
		}
		if (recettePapier() == null) {
			throw new ValidationException("Il faut une recette papier de rattachement pour la recette!");
		}
		if (recettePapier().modeRecouvrement() == null) {
			throw new ValidationException("Il faut un mode de recouvrement pour la recette!");
		}
		if (recettePapier().rppNbPiece() == null) {
			throw new ValidationException("Il faut un nombre de pieces pour la recette!");
		}
	}

	public void validateObjectMetierWithFacture() throws NSValidation.ValidationException {
		validateObjectMetier();
		if (facture().personne() == null) {
			throw new ValidationException("Il faut un client pour la recette!");
		}
		if (facture().organ() == null) {
			throw new ValidationException("Il faut une ligne budgetaire pour la recette!");
		}
		if (facture().typeCreditRec() == null) {
			throw new ValidationException("Il faut un type de credit recette pour la recette!");
		}
		if (facture().typeApplication() == null) {
			throw new ValidationException("Il faut un type d'application pour la recette!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void setRecetteCtrlActions(NSMutableArray nsMutableArray) {
		for (int i = 0; i < nsMutableArray.count(); i++) {
			addToRecetteCtrlActionsRelationship((EORecetteCtrlAction) nsMutableArray.objectAtIndex(i));
		}
	}

	public void setRecetteCtrlAnalytiques(NSMutableArray nsMutableArray) {
		for (int i = 0; i < nsMutableArray.count(); i++) {
			addToRecetteCtrlAnalytiquesRelationship((EORecetteCtrlAnalytique) nsMutableArray.objectAtIndex(i));
		}

	}

	public void setRecetteCtrlConventions(NSMutableArray nsMutableArray) {
		for (int i = 0; i < nsMutableArray.count(); i++) {
			addToRecetteCtrlConventionsRelationship((EORecetteCtrlConvention) nsMutableArray.objectAtIndex(i));
		}
	}

	public void setRecetteCtrlPlancos(NSMutableArray nsMutableArray) {
		for (int i = 0; i < nsMutableArray.count(); i++) {
			addToRecetteCtrlPlancosRelationship((EORecetteCtrlPlanco) nsMutableArray.objectAtIndex(i));
		}

	}

}
