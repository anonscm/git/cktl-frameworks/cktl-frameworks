
// EOModeRecouvrement.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOModeRecouvrement extends _EOModeRecouvrement {

	private static final String	MOD_DOM_ECHEANCIER	= "ECHEANCIER";
	public static final String	PRIMARY_KEY_KEY	= "modOrdre";

	public EOModeRecouvrement() {
		super();
	}

	public boolean isEcheancier() {
		return modDom() != null && modDom().equalsIgnoreCase(MOD_DOM_ECHEANCIER);
	}
	
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
