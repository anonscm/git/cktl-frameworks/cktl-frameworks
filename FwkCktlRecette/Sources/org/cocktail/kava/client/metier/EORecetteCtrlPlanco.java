// EORecetteCtrlPlanco.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EORecetteCtrlPlanco extends _EORecetteCtrlPlanco {

	public EORecetteCtrlPlanco() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (recetteCtrlPlancoCtps() == null || recetteCtrlPlancoCtps().count() == 0) {
			throw new ValidationException("Il faut au moins un plan comptable contrepartie pour la recette!!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
