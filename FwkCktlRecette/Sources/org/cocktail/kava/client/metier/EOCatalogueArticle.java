
// EOCatalogueArticle.java
//
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.metier.Article;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOCatalogueArticle extends _EOCatalogueArticle implements CatalogueArticle {

	private static final EOSortOrdering SORT_LIBELLE =
			EOSortOrdering.sortOrderingWithKey(EOCatalogueArticle.ARTICLE_KEY + "." + EOArticle.ART_LIBELLE_KEY, EOSortOrdering.CompareAscending);

	public static NSArray sortedCatalogueArticlesByLibelle(NSArray catArticles) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(catArticles, new NSArray(SORT_LIBELLE));
	}


	public EOCatalogueArticle() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (caarPrixHt() == null) {
			throw new ValidationException("Il faut un prix HT pour l'article!");
		}
		if (caarPrixTtc() == null) {
			throw new ValidationException("Il faut un prix TTC pour l'article!");
		}
		if (caarReference() == null) {
			throw new ValidationException("Il faut une reference pour l'article!");
		}
		if (catalogue() == null) {
			throw new ValidationException("Il faut un catalogue pour l'article!");
		}
		if (tva() == null) {
			throw new ValidationException("Il faut un taux de tva pour l'article!");
		}
		if (caarPrixHt().abs().compareTo(caarPrixTtc().abs()) == 1) {
			throw new ValidationException("Le prix HT ne peut etre superieur au prix TTC!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}


	public BigDecimal tauxTVA() {
		if (tva() == null) {
			return null;
		}
		return tva().tvaTaux();
	}


	public Article getArticleCommon() {
		return article();
	}


}
