// _EOEngageCtrlAction.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngageCtrlAction.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngageCtrlAction extends  EOGenericRecord {
	public static final String ENTITY_NAME = "EngageCtrlAction";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.engage_ctrl_action";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "eactId";

	public static final String EACT_DATE_SAISIE_KEY = "eactDateSaisie";
	public static final String EACT_HT_SAISIE_KEY = "eactHtSaisie";
	public static final String EACT_MONTANT_BUDGETAIRE_KEY = "eactMontantBudgetaire";
	public static final String EACT_MONTANT_BUDGETAIRE_RESTE_KEY = "eactMontantBudgetaireReste";
	public static final String EACT_TTC_SAISIE_KEY = "eactTtcSaisie";
	public static final String EACT_TVA_SAISIE_KEY = "eactTvaSaisie";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

// Attributs non visibles
	public static final String EACT_ID_KEY = "eactId";
	public static final String ENG_ID_KEY = "engId";
	public static final String TYAC_ID_KEY = "tyacId";

//Colonnes dans la base de donnees
	public static final String EACT_DATE_SAISIE_COLKEY = "EACT_DATE_SAISIE";
	public static final String EACT_HT_SAISIE_COLKEY = "EACT_HT_SAISIE";
	public static final String EACT_MONTANT_BUDGETAIRE_COLKEY = "EACT_MONTANT_BUDGETAIRE";
	public static final String EACT_MONTANT_BUDGETAIRE_RESTE_COLKEY = "EACT_MONTANT_BUDGETAIRE_RESTE";
	public static final String EACT_TTC_SAISIE_COLKEY = "EACT_TTC_SAISIE";
	public static final String EACT_TVA_SAISIE_COLKEY = "EACT_TVA_SAISIE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";

	public static final String EACT_ID_COLKEY = "EACT_ID";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";


	// Relationships
	public static final String LOLF_NOMENCLATURE_DEPENSE_KEY = "lolfNomenclatureDepense";



	// Accessors methods
  public NSTimestamp eactDateSaisie() {
    return (NSTimestamp) storedValueForKey(EACT_DATE_SAISIE_KEY);
  }

  public void setEactDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, EACT_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal eactHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EACT_HT_SAISIE_KEY);
  }

  public void setEactHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EACT_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal eactMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(EACT_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEactMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EACT_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal eactMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(EACT_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEactMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EACT_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public java.math.BigDecimal eactTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EACT_TTC_SAISIE_KEY);
  }

  public void setEactTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EACT_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal eactTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(EACT_TVA_SAISIE_KEY);
  }

  public void setEactTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, EACT_TVA_SAISIE_KEY);
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    takeStoredValueForKey(value, EXE_ORDRE_KEY);
  }

  public org.cocktail.kava.client.metier.EOLolfNomenclatureDepense lolfNomenclatureDepense() {
    return (org.cocktail.kava.client.metier.EOLolfNomenclatureDepense)storedValueForKey(LOLF_NOMENCLATURE_DEPENSE_KEY);
  }

  public void setLolfNomenclatureDepenseRelationship(org.cocktail.kava.client.metier.EOLolfNomenclatureDepense value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOLolfNomenclatureDepense oldValue = lolfNomenclatureDepense();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_NOMENCLATURE_DEPENSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_DEPENSE_KEY);
    }
  }


  public static EOEngageCtrlAction createEngageCtrlAction(EOEditingContext editingContext, NSTimestamp eactDateSaisie
, java.math.BigDecimal eactHtSaisie
, java.math.BigDecimal eactMontantBudgetaire
, java.math.BigDecimal eactMontantBudgetaireReste
, java.math.BigDecimal eactTtcSaisie
, java.math.BigDecimal eactTvaSaisie
, Integer exeOrdre
) {
    EOEngageCtrlAction eo = (EOEngageCtrlAction) createAndInsertInstance(editingContext, _EOEngageCtrlAction.ENTITY_NAME);
		eo.setEactDateSaisie(eactDateSaisie);
		eo.setEactHtSaisie(eactHtSaisie);
		eo.setEactMontantBudgetaire(eactMontantBudgetaire);
		eo.setEactMontantBudgetaireReste(eactMontantBudgetaireReste);
		eo.setEactTtcSaisie(eactTtcSaisie);
		eo.setEactTvaSaisie(eactTvaSaisie);
		eo.setExeOrdre(exeOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOEngageCtrlAction.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOEngageCtrlAction.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOEngageCtrlAction creerInstance(EOEditingContext editingContext) {
		  		EOEngageCtrlAction object = (EOEngageCtrlAction)createAndInsertInstance(editingContext, _EOEngageCtrlAction.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOEngageCtrlAction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngageCtrlAction)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOEngageCtrlAction localInstanceIn(EOEditingContext editingContext, EOEngageCtrlAction eo) {
    EOEngageCtrlAction localInstance = (eo == null) ? null : (EOEngageCtrlAction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOEngageCtrlAction#localInstanceIn a la place.
   */
	public static EOEngageCtrlAction localInstanceOf(EOEditingContext editingContext, EOEngageCtrlAction eo) {
		return EOEngageCtrlAction.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOEngageCtrlAction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOEngageCtrlAction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngageCtrlAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngageCtrlAction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOEngageCtrlAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOEngageCtrlAction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngageCtrlAction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngageCtrlAction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOEngageCtrlAction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngageCtrlAction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngageCtrlAction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOEngageCtrlAction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
