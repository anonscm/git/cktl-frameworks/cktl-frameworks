// EOPersonne.java
//
package org.cocktail.kava.client.metier;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.kava.client.PieFwkUtilities;
import org.cocktail.kava.client.selector.FournisseurEtatControle;
import org.cocktail.kava.client.selector.FournisseurFiltre;
import org.cocktail.pieFwk.selector.IFiltre;
import org.cocktail.pieFwk.selector.IListeFiltre;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPersonne extends _EOPersonne {
	public static final String PERS_NOM_PRENOM_KEY = "persNomPrenom";

	/** Cle Primaire. */
	public static final String PRIMARY_KEY_KEY = "persId";

	/** Fournisseurs Join Key. */
	public static final String FOURNISSEUR_JOIN_KEY = "fournisUlrs";

	/** Filtres fournisseurs par etat. */
	public static final IFiltre<EOFournisUlr> FILTRE_FOURNISSEUR_VALIDE = new FournisseurEtatControle(EOFournisUlr.FOU_ETAT_VALIDE);
	public static final IFiltre<EOFournisUlr> FILTRE_FOURNISSEUR_EN_ATTENTE = new FournisseurEtatControle(EOFournisUlr.FOU_ETAT_ATTENTE);

	/** Constructeur. */
	public EOPersonne() {
		super();
	}

	public boolean isPersonneMorale() {
		return "STR".equalsIgnoreCase(persType());
	}

	public String persNomPrenom() {
		return PieFwkUtilities.capitalizedString(persLibelle().concat((PieFwkUtilities.isEmpty(persLc()) ? "" : " ".concat(persLc()))));
	}

	public List<EOFournisUlr> fournisseurs(IFiltre<EOFournisUlr>... filtres) {
		IListeFiltre<EOFournisUlr> fournisseurFiltre = new FournisseurFiltre();
		List<EOFournisUlr> fournisseurs = fournisUlrAsList();
		return fournisseurFiltre.filtrer(fournisseurs, filtres);
	}

	private List<EOFournisUlr> fournisUlrAsList() {
		List<EOFournisUlr> fournisseurs = new ArrayList<EOFournisUlr>();
		NSArray fournisseursAsArray = fournisUlrs();
		if (fournisseursAsArray == null || fournisseursAsArray.count() == 0) {
			return fournisseurs;
		}
		for (int idx = 0; idx < fournisseursAsArray.count(); idx++) {
			fournisseurs.add((EOFournisUlr) fournisseursAsArray.objectAtIndex(idx));
		}
		return fournisseurs;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}
}
