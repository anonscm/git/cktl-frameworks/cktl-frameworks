
// EOGestion.java
//
package org.cocktail.kava.client.metier;

import org.cocktail.application.client.eof.EOExercice;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOGestion extends _EOGestion {

	public static final String	PRIMARY_KEY_KEY	= "gesCode";

	public EOGestion() {
		super();
	}

	public boolean isSacd(EOExercice exercice) {
		if (gestionExercices() == null || gestionExercices().count() == 0)
			return false;

		NSArray array=EOQualifier.filteredArrayWithQualifier(gestionExercices(),
				EOQualifier.qualifierWithQualifierFormat(EOGestionExercice.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		if (array==null || array.count()==0)
			return false;
		if (((EOGestionExercice)array.objectAtIndex(0)).pcoNum185()==null)
			return false;

		return true;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
