// EOPrestation.java
//
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.finder.FinderTauxProrata;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.Prestation;
import org.cocktail.pieFwk.common.metier.PrestationCompanion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrestation extends _EOPrestation implements Prestation {

	public static final String PRIMARY_KEY_KEY = "prestId";

	private PrestationCompanion companion;

	public EOPrestation() {
		super();
		this.companion = new PrestationCompanion(this);
	}

	public boolean updateTauxProrata() {
		boolean isDirty = false;
		if (tauxProrata() == null) {
			setTauxProrataRelationship(FinderTauxProrata.tauxProrata100(this.editingContext()));
			isDirty = true;
		}
		return isDirty;
	}

	public void setPrestRemiseGlobale(BigDecimal aValue) {
		super.setPrestRemiseGlobale(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i)).companion().updateTotaux();
			}
		}
	}

	/**
	 * @param aValue (O/N) applique ou non la tva, et met a jour les lignes de la prestation, et recalcule les totaux
	 */
	public void setPrestApplyTva(String aValue) {
		super.setPrestApplyTva(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				EOPrestationLigne pl = (EOPrestationLigne) prestationLignes().objectAtIndex(i);
				if ("N".equalsIgnoreCase(prestApplyTva())) {
					pl.setTvaRelationship(null);
					pl.setPrligArtTtc(pl.prligArtHt());
				}
				else {
					pl.setTvaRelationship(pl.tvaInitial());
					pl.setPrligArtTtc(pl.prligArtTtcInitial());
				}
			}
		}
	}

	/**
	 * verifie si les informations obligatoires sont la pour valider cote client (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidableClient() {
		if (!typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext()))) {
			return true;
		}
		if (prestationBudgetClient() == null || prestationBudgetClient().organ() == null
				|| prestationBudgetClient().tauxProrata() == null
				|| prestationBudgetClient().typeCreditDep() == null || prestationBudgetClient().lolfNomenclatureDepense() == null
				|| prestationBudgetClient().pcoNum() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidablePrest() {
		if (organ() == null || tauxProrata() == null || typeCreditRec() == null || lolfNomenclatureRecette() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour cloturer la prestation (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isCloturable() {
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour facturer la prestation (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isFacturable() {
		return true;
	}

	/**
	 * @return prestation adresse en cours.
	 */
	public EOPrestationAdrClient currentPrestationAdresseClient() {
		EOPrestationAdrClient result = null;
		NSArray sortedAdr = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				toPrestationAdrClients(), new NSArray(new Object[] {EOPrestationAdrClient.SORT_DATE_CREATION_DESC}));
		// TODO FLA voir si il faut ajouter un filtre sur date_fin IS NULL
		if (sortedAdr != null && sortedAdr.count() > 0) {
			result = (EOPrestationAdrClient) sortedAdr.objectAtIndex(0);
		}
		return result;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la prestation!");
		}
		if (typePublic() == null) {
			throw new ValidationException("Il faut un type de client pour la prestation!");
		}
		if (typeEtat() == null) {
			throw new ValidationException("Il faut un etat pour la prestation!");
		}
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la prestation!");
		}
		if (personne() == null) {
			throw new ValidationException("Il faut un client pour la prestation!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un agent (utilisateur) pour la prestation!");
		}
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			throw new ValidationException("Il faut au moins une ligne dans le panier pour la prestation!");
		}
		if (prestDate() == null) {
			throw new ValidationException("Il faut une date de creation pour la prestation!");
		}
		if (prestLibelle() == null) {
			throw new ValidationException("Il faut un libelle pour la prestation!");
		}
		if (prestRemiseGlobale() != null) {
			if (prestRemiseGlobale().doubleValue() < 0.0 || prestRemiseGlobale().doubleValue() > 100.0) {
				throw new ValidationException("Le pourcentage de remise globale doit etre entre 0 et 100 :-) !");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (prestNumero() == null) {
			try {
				setPrestNumero(Integer.valueOf(ServerProxy.getNumerotation(editingContext(), exercice(), null, "PRESTATION").intValue()));
			} catch (Exception e) {
				throw new ValidationException("Probleme pour numeroter la prestation : " + e);
			}
		}
		updateTauxProrata();
		companion().updateTotaux();
	}

	public BigDecimal remiseGlobale() {
		return prestRemiseGlobale();
	}

	public PrestationCompanion companion() {
		return this.companion;
	}

	public void setTotalHT(BigDecimal totalHT) {
		setPrestTotalHt(totalHT);
	}

	public void setTotalTVA(BigDecimal totalTVA) {
		setPrestTotalTva(totalTVA);
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		setPrestTotalTtc(totalTTC);
	}

	public NSArray getPrestationLignesCommon() {
		return prestationLignes();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) EOApplication.sharedApplication();
	}

	public Number exerciceAsNumber() {
		if (exercice() != null) {
			return exercice().exeExercice();
		}
		return null;
	}

}
