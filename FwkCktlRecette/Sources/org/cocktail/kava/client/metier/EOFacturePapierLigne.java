// EOFacturePapierLigne.java
//
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.FacturePapierLigne;
import org.cocktail.pieFwk.common.metier.FacturePapierLigneCompanion;
import org.cocktail.pieFwk.common.metier.PrestationLigne;
import org.cocktail.pieFwk.common.metier.TauxTva;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSValidation;

public class EOFacturePapierLigne extends _EOFacturePapierLigne implements FacturePapierLigne {

	public static final String	FLIG_TOTAL_TVA_KEY	= "fligTotalTva";

	private FacturePapierLigneCompanion companion;

	public EOFacturePapierLigne() {
		super();
		this.companion = new FacturePapierLigneCompanion(this);
	}

	public FacturePapierLigneCompanion companion() {
		return this.companion;
	}

	public void setTvaRelationship(EOTva value) {
		super.setTvaRelationship(value);
		companion.updateTotaux();
	}


	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise HT
	 */
	public void setFligArtHt(BigDecimal aValue) {
		super.setFligArtHt(aValue);
		companion.updateTotalHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total remise TTC
	 */
	public void setFligArtTtc(BigDecimal aValue) {
		super.setFligArtTtc(aValue);
		companion.updateTotalTtc();
	}

	public BigDecimal fligTotalTva() {
		if (fligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (fligTotalHt() == null) {
			return fligTotalTtc();
		}
		return fligTotalTtc().subtract(fligTotalHt());
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (typeArticle() == null) {
			throw new ValidationException("Il faut un type article pour les lignes de la facture papier!");
		}
		if (facturePapier() == null) {
			throw new ValidationException("Il faut une facture papier pour les lignes de la facture papier!");
		}
		if (fligDate() == null) {
			throw new ValidationException("Il faut une date de creation pour les lignes de la facture papier!");
		}
		if (fligDescription() == null) {
			throw new ValidationException("Il faut un libelle pour les lignes de la facture papier!");
		}
		if (fligArtHt() == null) {
			throw new ValidationException("Il faut un prix HT pour les lignes de la facture papier!");
		}
		if (fligArtTtc() == null) {
			throw new ValidationException("Il faut un prix TTC pour les lignes de la facture papier!");
		}
		if (fligQuantite() == null) {
			throw new ValidationException("Il faut une quantite pour les lignes de la facture papier!");
		}
		if (fligArtHt().abs().compareTo(fligArtTtc().abs()) == 1) {
			throw new ValidationException("Le prix HT ne peut etre superieur au prix TTC!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Methode actuellement utilisée uniquement dans les Tests par l'implementation Mock de l'interface.
	 * Ici on redirige vers la classe parente en castant.
	 */
	public void addToFacturePapierLignesRelationship(FacturePapierLigne ligne) {
		this.addToFacturePapierLignesRelationship((EOFacturePapierLigne) ligne);
	}

	public FacturePapier getFacturePapierCommon() {
		return facturePapier();
	}

	public PrestationLigne getPrestationLigneCommon() {
		return prestationLigne();
	}

	public TauxTva tauxTva() {
		return tva();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) EOApplication.sharedApplication();
	}

	public Number exerciceAsNumber() {
		if (facturePapier() != null && facturePapier().exercice() != null) {
			return facturePapier().exercice().exeExercice();
		}
		return null;
	}

}
