// EOFacturePapier.java
//
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.client.remotecalls.ServerCallCompta;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineType;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.finder.FinderPlanComptable;
import org.cocktail.kava.client.finder.FinderTauxProrata;
import org.cocktail.kava.client.finder.FinderTypeApplication;
import org.cocktail.kava.client.service.FacturePapierService;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.FacturePapierCompanion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOFacturePapier extends _EOFacturePapier implements FacturePapier {

	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	public static final String FAP_TOTAL_HT_LIVE_KEY = "fapTotalHtLive";
	public static final String FAP_TOTAL_TTC_LIVE_KEY = "fapTotalTtcLive";
	public static final String FAP_TOTAL_TVA_LIVE_KEY = "fapTotalTvaLive";
	public static final String FAP_NUMERO_AS_INT_KEY = "fapNumeroAsInteger";

	public static final String PRIMARY_KEY_KEY = "fapId";

	private EOPlanComptable planComptable;
	private EOPlanComptable planComptableCtp;
	private EOPlanComptable planComptableTva;

	private FacturePapierCompanion companion;

	public EOFacturePapier() {
		super();
		this.companion = new FacturePapierCompanion(this);
	}

	public boolean updateTauxProrata() {
		boolean isDirty = false;
		if (tauxProrata() == null) {
			setTauxProrataRelationship(defaultTauxProrata());
			isDirty = true;
		}
		return isDirty;
	}

	private EOTauxProrata defaultTauxProrata() {
		return FinderTauxProrata.tauxProrata100(this.editingContext());
	}

	/**
	 * Lazily load planComptable using exercice and pcoNum.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptable() {
		if (planComptable == null) {
			planComptable = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNum());
		}
		return planComptable;
	}

	/**
	 * Lazily load planComptableCtp using exercice and pcoNumCtp.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptableCtp() {
		if (planComptableCtp == null) {
			planComptableCtp = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNumCtp());
		}
		return planComptableCtp;
	}

	/**
	 * Lazily load planComptableTva using exercice and pcoNumTva.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptableTva() {
		if (planComptableTva == null) {
			planComptableTva = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNumTva());
		}
		return planComptableTva;
	}

	public void setPlanComptable(EOPlanComptable planComptable) {
		String pcoNum = planComptable == null ? null : planComptable.pcoNum();
		setPcoNum(pcoNum);
	}

	public void setPlanComptableCtp(EOPlanComptable planComptableCtp) {
		String pcoNumCtp = planComptableCtp == null ? null : planComptableCtp.pcoNum();
		setPcoNumCtp(pcoNumCtp);
	}

	public void setPlanComptableTva(EOPlanComptable planComptableTva) {
		String pcoNumTva = planComptableTva == null ? null : planComptableTva.pcoNum();
		setPcoNumTva(pcoNumTva);
	}

	@Override
	public void setPcoNum(String value) {
		super.setPcoNum(value);
		this.planComptable = null;
	}

	@Override
	public void setPcoNumCtp(String value) {
		super.setPcoNumCtp(value);
		this.planComptableCtp = null;
	}

	@Override
	public void setPcoNumTva(String value) {
		super.setPcoNumTva(value);
		this.planComptableTva = null;
	}

	/**
	 * @return adresse de facturation en cours.
	 */
	public EOFacturePapierAdrClient currentFactPapierAdresseClient() {
		EOFacturePapierAdrClient result = null;
		NSArray sortedAdr = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				toFacturePapierAdrClients(),
				new NSArray(new Object[] {EOFacturePapierAdrClient.SORT_DATE_CREATION_DESC}));
		if (sortedAdr != null && sortedAdr.count() > 0) {
			result = (EOFacturePapierAdrClient) sortedAdr.objectAtIndex(0);
		}
		return result;
	}

	public void setFapRemiseGlobale(BigDecimal aValue) {
		if (aValue != null && aValue.equals(fapRemiseGlobale())) {
			return;
		}
		super.setFapRemiseGlobale(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i)).companion().updateTotaux();
			}
		}
	}

	/**
	 * @param aValue (O/N) applique ou non la tva, et met a jour les lignes de la facture papier, et recalcule le total ttc
	 */
	public void setFapApplyTva(String aValue) {
		if (aValue != null && aValue.equals(fapApplyTva())) {
			return;
		}
		super.setFapApplyTva(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				EOFacturePapierLigne flig = (EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i);
				if ("N".equalsIgnoreCase(fapApplyTva())) {
					flig.setTvaRelationship(null);
					flig.setFligArtTtc(flig.fligArtHt());
				}
				else {
					flig.setTvaRelationship(flig.tvaInitial());
					flig.setFligArtTtc(flig.fligArtTtcInitial());
				}
			}
		}
	}

	/**
	 * verifie si les informations obligatoires sont la pour valider cote client (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidableClient() {
		if (!typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext()))) {
			return true;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidablePrest() {
		if (organ() == null || typeCreditRec() == null || lolfNomenclatureRecette() == null || pcoNum() == null
				|| modeRecouvrement() == null) {
			return false;
		}
		return true;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (exercice() == null) {
			throw new ValidationException("Il faut un exercice pour la facture papier!");
		}
		if (typePublic() == null) {
			throw new ValidationException("Il faut un type de client pour la facture papier!");
		}
		if (typeEtat() == null) {
			throw new ValidationException("Il faut un etat pour la facture papier!");
		}
		if (personne() == null) {
			throw new ValidationException("Il faut un client pour la facture papier!");
		}
		if (utilisateur() == null) {
			throw new ValidationException("Il faut un agent (utilisateur) pour la facture papier!");
		}
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			throw new ValidationException("Il faut au moins une ligne dans le panier pour la facture papier!");
		}
		if (fapDate() == null) {
			throw new ValidationException("Il faut une date de creation pour la facture papier!");
		}
		if (fapLib() == null) {
			throw new ValidationException("Il faut un libelle pour la facture papier!");
		}
		if (fapRemiseGlobale() != null) {
			if (fapRemiseGlobale().doubleValue() < 0.0 || fapRemiseGlobale().doubleValue() > 100.0) {
				throw new ValidationException("Le pourcentage de remise globale doit etre entre 0 et 100 :-) !");
			}
		}
		if (echeId() != null) {
			if (modeRecouvrement() == null || !modeRecouvrement().isEcheancier()) {
				throw new ValidationException(
						"Un echeancier existe pour cette facture, supprimer d'abord l'echeancier avant de changer de mode de recouvrement !");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		if (fapNumero() == null) {
			try {
				setFapNumero(Float.valueOf(ServerProxy.getNumerotation(editingContext(), exercice(), null, "FACTURE_PAPIER").intValue()));
			} catch (Exception e) {
				throw new ValidationException("Probleme pour numeroter la facture papier : " + e);
			}
		}
		updateTauxProrata();
		companion().updateTotaux();
	}

	public BigDecimal remiseGlobale() {
		return fapRemiseGlobale();
	}

	public FacturePapierCompanion companion() {
		return this.companion;
	}

	public void setTotalHT(BigDecimal totalHT) {
		setFapTotalHt(totalHT);
	}

	public void setTotalTVA(BigDecimal totalTVA) {
		setFapTotalTva(totalTVA);
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		setFapTotalTtc(totalTTC);
	}

	public NSArray getFacturePapierLignesCommon() {
		return facturePapierLignes();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) EOApplication.sharedApplication();
	}

	public Number exerciceAsNumber() {
		if (exercice() != null) {
			return exercice().exeExercice();
		}
		return null;
	}

	public boolean hasEcheancierNational() {
		return this.echeId() != null;
	}

	public boolean hasEcheancierSepa(EOEditingContext edc) {
		EOEditingContext localEdc = edc != null ? edc : this.editingContext();
		Integer facturePapierPrimaryKey = FacturePapierService.instance().getFacturePapierPrimaryKey(localEdc, this);
		return ServerCallCompta.clientSideRequestHasEcheanciers(
				localEdc, ISepaSddOrigineType.FACTURE, facturePapierPrimaryKey);
	}

	public Integer fapNumeroAsInteger() {
		if (fapNumero() == null) {
			return null;
		}
		return Integer.valueOf(fapNumero().intValue());
	}
}
