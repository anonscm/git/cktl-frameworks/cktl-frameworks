// _EORecetteCtrlPlanco.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecetteCtrlPlanco.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecetteCtrlPlanco extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RecetteCtrlPlanco";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_planco";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rpcoId";

	public static final String RPCO_DATE_SAISIE_KEY = "rpcoDateSaisie";
	public static final String RPCO_HT_SAISIE_KEY = "rpcoHtSaisie";
	public static final String RPCO_TTC_SAISIE_KEY = "rpcoTtcSaisie";
	public static final String RPCO_TVA_SAISIE_KEY = "rpcoTvaSaisie";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String TIT_ID_KEY = "titId";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String REC_ID_KEY = "recId";
	public static final String RPCO_ID_KEY = "rpcoId";

//Colonnes dans la base de donnees
	public static final String RPCO_DATE_SAISIE_COLKEY = "RPCO_DATE_SAISIE";
	public static final String RPCO_HT_SAISIE_COLKEY = "RPCO_HT_SAISIE";
	public static final String RPCO_TTC_SAISIE_COLKEY = "RPCO_TTC_SAISIE";
	public static final String RPCO_TVA_SAISIE_COLKEY = "RPCO_TVA_SAISIE";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String TIT_ID_COLKEY = "TIT_ID";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String REC_ID_COLKEY = "REC_ID";
	public static final String RPCO_ID_COLKEY = "RPCO_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String RECETTE_KEY = "recette";
	public static final String RECETTE_CTRL_PLANCO_CTPS_KEY = "recetteCtrlPlancoCtps";
	public static final String RECETTE_CTRL_PLANCO_TVAS_KEY = "recetteCtrlPlancoTvas";
	public static final String TITRE_KEY = "titre";



	// Accessors methods
  public NSTimestamp rpcoDateSaisie() {
    return (NSTimestamp) storedValueForKey(RPCO_DATE_SAISIE_KEY);
  }

  public void setRpcoDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RPCO_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_HT_SAISIE_KEY);
  }

  public void setRpcoHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_TTC_SAISIE_KEY);
  }

  public void setRpcoTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rpcoTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPCO_TVA_SAISIE_KEY);
  }

  public void setRpcoTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPCO_TVA_SAISIE_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public Integer titId() {
    return (Integer) storedValueForKey(TIT_ID_KEY);
  }

  public void setTitId(Integer value) {
    takeStoredValueForKey(value, TIT_ID_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOPlanComptable planComptable() {
    return (org.cocktail.kava.client.metier.EOPlanComptable)storedValueForKey(PLAN_COMPTABLE_KEY);
  }

  public void setPlanComptableRelationship(org.cocktail.kava.client.metier.EOPlanComptable value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPlanComptable oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EORecette recette() {
    return (org.cocktail.kava.client.metier.EORecette)storedValueForKey(RECETTE_KEY);
  }

  public void setRecetteRelationship(org.cocktail.kava.client.metier.EORecette value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EORecette oldValue = recette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RECETTE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOTitre titre() {
    return (org.cocktail.kava.client.metier.EOTitre)storedValueForKey(TITRE_KEY);
  }

  public void setTitreRelationship(org.cocktail.kava.client.metier.EOTitre value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTitre oldValue = titre();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TITRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TITRE_KEY);
    }
  }

  public NSArray recetteCtrlPlancoCtps() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCO_CTPS_KEY);
  }

  public NSArray recetteCtrlPlancoCtps(EOQualifier qualifier) {
    return recetteCtrlPlancoCtps(qualifier, null);
  }

  public NSArray recetteCtrlPlancoCtps(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = recetteCtrlPlancoCtps();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToRecetteCtrlPlancoCtpsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_CTPS_KEY);
  }

  public void removeFromRecetteCtrlPlancoCtpsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_CTPS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp createRecetteCtrlPlancoCtpsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlPlancoCtp");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_PLANCO_CTPS_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp) eo;
  }

  public void deleteRecetteCtrlPlancoCtpsRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_CTPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlPlancoCtpsRelationships() {
    Enumeration objects = recetteCtrlPlancoCtps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlPlancoCtpsRelationship((org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp)objects.nextElement());
    }
  }

  public NSArray recetteCtrlPlancoTvas() {
    return (NSArray)storedValueForKey(RECETTE_CTRL_PLANCO_TVAS_KEY);
  }

  public NSArray recetteCtrlPlancoTvas(EOQualifier qualifier) {
    return recetteCtrlPlancoTvas(qualifier, null);
  }

  public NSArray recetteCtrlPlancoTvas(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = recetteCtrlPlancoTvas();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToRecetteCtrlPlancoTvasRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_TVAS_KEY);
  }

  public void removeFromRecetteCtrlPlancoTvasRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_TVAS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva createRecetteCtrlPlancoTvasRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecetteCtrlPlancoTva");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTE_CTRL_PLANCO_TVAS_KEY);
    return (org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva) eo;
  }

  public void deleteRecetteCtrlPlancoTvasRelationship(org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTE_CTRL_PLANCO_TVAS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecetteCtrlPlancoTvasRelationships() {
    Enumeration objects = recetteCtrlPlancoTvas().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecetteCtrlPlancoTvasRelationship((org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva)objects.nextElement());
    }
  }


  public static EORecetteCtrlPlanco createRecetteCtrlPlanco(EOEditingContext editingContext, NSTimestamp rpcoDateSaisie
, java.math.BigDecimal rpcoHtSaisie
, java.math.BigDecimal rpcoTtcSaisie
, java.math.BigDecimal rpcoTvaSaisie
, Integer tboOrdre
) {
    EORecetteCtrlPlanco eo = (EORecetteCtrlPlanco) createAndInsertInstance(editingContext, _EORecetteCtrlPlanco.ENTITY_NAME);
		eo.setRpcoDateSaisie(rpcoDateSaisie);
		eo.setRpcoHtSaisie(rpcoHtSaisie);
		eo.setRpcoTtcSaisie(rpcoTtcSaisie);
		eo.setRpcoTvaSaisie(rpcoTvaSaisie);
		eo.setTboOrdre(tboOrdre);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecetteCtrlPlanco.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecetteCtrlPlanco.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecetteCtrlPlanco creerInstance(EOEditingContext editingContext) {
		  		EORecetteCtrlPlanco object = (EORecetteCtrlPlanco)createAndInsertInstance(editingContext, _EORecetteCtrlPlanco.ENTITY_NAME);
		  		return object;
			}


	
  	  public EORecetteCtrlPlanco localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecetteCtrlPlanco)localInstanceOfObject(editingContext, this);
	  }
	
  public static EORecetteCtrlPlanco localInstanceIn(EOEditingContext editingContext, EORecetteCtrlPlanco eo) {
    EORecetteCtrlPlanco localInstance = (eo == null) ? null : (EORecetteCtrlPlanco)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecetteCtrlPlanco#localInstanceIn a la place.
   */
	public static EORecetteCtrlPlanco localInstanceOf(EOEditingContext editingContext, EORecetteCtrlPlanco eo) {
		return EORecetteCtrlPlanco.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EORecetteCtrlPlanco fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecetteCtrlPlanco fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecetteCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecetteCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EORecetteCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EORecetteCtrlPlanco fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecetteCtrlPlanco eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecetteCtrlPlanco)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecetteCtrlPlanco fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecetteCtrlPlanco eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecetteCtrlPlanco ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EORecetteCtrlPlanco fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
