

// EOParametreDepense.java
// 
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOParametreDepense extends _EOParametreDepense {

    public static final String PARAM_CONTROLE_ORGAN_DEST="CTRL_ORGAN_DEST";
    public static final String VALUE_CONTROLE_ORGAN_DEST_OUI="OUI";
    public static final String VALUE_CONTROLE_ORGAN_DEST_NON="NON";

    public EOParametreDepense() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
