// _EOPrestationAdrClient.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrestationAdrClient.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrestationAdrClient extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrestationAdrClient";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_adr_client";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "padcId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_FIN_KEY = "dateFin";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String PADC_ID_KEY = "padcId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PREST_ID_KEY = "prestId";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "Date_CREATION";
	public static final String DATE_FIN_COLKEY = "Date_FIN";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String PADC_ID_COLKEY = "PADC_ID";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PREST_ID_COLKEY = "PREST_ID";


	// Relationships
	public static final String TO_ADRESSE_KEY = "toAdresse";
	public static final String TO_PERSONNE_CREATION_KEY = "toPersonneCreation";
	public static final String TO_PRESTATION_KEY = "toPrestation";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp dateFin() {
    return (NSTimestamp) storedValueForKey(DATE_FIN_KEY);
  }

  public void setDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_FIN_KEY);
  }

  public org.cocktail.kava.client.metier.EOAdresse toAdresse() {
    return (org.cocktail.kava.client.metier.EOAdresse)storedValueForKey(TO_ADRESSE_KEY);
  }

  public void setToAdresseRelationship(org.cocktail.kava.client.metier.EOAdresse value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOAdresse oldValue = toAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ADRESSE_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOPersonne toPersonneCreation() {
    return (org.cocktail.application.client.eof.EOPersonne)storedValueForKey(TO_PERSONNE_CREATION_KEY);
  }

  public void setToPersonneCreationRelationship(org.cocktail.application.client.eof.EOPersonne value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPersonne oldValue = toPersonneCreation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_CREATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_CREATION_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOPrestation toPrestation() {
    return (org.cocktail.kava.client.metier.EOPrestation)storedValueForKey(TO_PRESTATION_KEY);
  }

  public void setToPrestationRelationship(org.cocktail.kava.client.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPrestation oldValue = toPrestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRESTATION_KEY);
    }
  }


  public static EOPrestationAdrClient createPrestationAdrClient(EOEditingContext editingContext, NSTimestamp dateCreation
, org.cocktail.kava.client.metier.EOAdresse toAdresse, org.cocktail.application.client.eof.EOPersonne toPersonneCreation, org.cocktail.kava.client.metier.EOPrestation toPrestation) {
    EOPrestationAdrClient eo = (EOPrestationAdrClient) createAndInsertInstance(editingContext, _EOPrestationAdrClient.ENTITY_NAME);
		eo.setDateCreation(dateCreation);
    eo.setToAdresseRelationship(toAdresse);
    eo.setToPersonneCreationRelationship(toPersonneCreation);
    eo.setToPrestationRelationship(toPrestation);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPrestationAdrClient.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPrestationAdrClient.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPrestationAdrClient creerInstance(EOEditingContext editingContext) {
		  		EOPrestationAdrClient object = (EOPrestationAdrClient)createAndInsertInstance(editingContext, _EOPrestationAdrClient.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOPrestationAdrClient localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrestationAdrClient)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOPrestationAdrClient localInstanceIn(EOEditingContext editingContext, EOPrestationAdrClient eo) {
    EOPrestationAdrClient localInstance = (eo == null) ? null : (EOPrestationAdrClient)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPrestationAdrClient#localInstanceIn a la place.
   */
	public static EOPrestationAdrClient localInstanceOf(EOEditingContext editingContext, EOPrestationAdrClient eo) {
		return EOPrestationAdrClient.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOPrestationAdrClient fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPrestationAdrClient fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrestationAdrClient eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrestationAdrClient)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOPrestationAdrClient fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOPrestationAdrClient fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrestationAdrClient eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrestationAdrClient)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPrestationAdrClient fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrestationAdrClient eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrestationAdrClient ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOPrestationAdrClient fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
