

// EOFonction.java
//
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOFonction extends _EOFonction {

    public static final String DROIT_CONTREPARTIE_NON_STANDARD="REAICTP";
    public static final String DROIT_TVA_NON_STANDARD="REAITVA";
    public static final String DROIT_VOIR_TOUTES_LES_RECETTES="REALLREC";
    public static final String DROIT_IMPUTATION_RECETTE_NON_STANDARD="REAUTIMP";
    public static final String DROIT_INTERDIT_SAISIE_CONTREPARTIE="REPASCTP";
    public static final String DROIT_RECETTER="REREC";
    public static final String DROIT_RECETTER_PERIODE_BLOCAGE="RERECINV";

    public static final String DROIT_FACTURER="REFAC";
    public static final String DROIT_FACTURER_PERIODE_BLOCAGE="REFACINV";
    public static final String DROIT_VOIR_TOUTES_COMMANDES="PRALLCOM";

    public static final String DROIT_GERER_CATALOGUES="PRGCAT";
    public static final String DROIT_VOIR_TOUS_LES_CATALOGUES="PRALLCAT";
    public static final String DROIT_GERER_PRESTATION="PRGPR";
    public static final String DROIT_VOIR_TOUTES_LES_PRESTATIONS="PRALLPR";
    public static final String DROIT_SUPPRIMER_FACTURES="PRDELFAC";
    public static final String DROIT_VOIR_TOUTES_LES_FACTURES_PAPIER="PRALLFAP";

    public static final String DROIT_VOIR_PRESTATIONS="PRESV";
    public static final String DROIT_CREER_PRESTATION="PRESC";
    public static final String DROIT_SUPPRIMER_PRESTATION="PRESS";
    public static final String DROIT_MODIFIER_PRESTATION="PRESM";

    public static final String DROIT_VOIR_PAIEMENT_WEB="CONSPW";
    public static final String DROIT_CREER_PRESTATION_CUMULATIVE="CREFACUM";

    public static final String DROIT_GERER_PRESTATIONS_REPRO = "PRGREPRO";
	public static final String DROIT_GESTION_MANDATS_SEPA = "RESEPA";
	public static final String DROIT_GESTION_MANDATS_SEPA_MARACUJA = "SEPASDD";

    public EOFonction() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    }


}
