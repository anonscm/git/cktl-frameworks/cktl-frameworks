// _EOPreferences.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreferences.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPreferences extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Preferences";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.preferences";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prefId";

	public static final String PREF_DEFAULT_VALUE_KEY = "prefDefaultValue";
	public static final String PREF_DESCRIPTION_KEY = "prefDescription";
	public static final String PREF_KEY_KEY = "prefKey";
	public static final String PREF_PERSONNALISABLE_KEY = "prefPersonnalisable";

// Attributs non visibles
	public static final String PREF_ID_KEY = "prefId";
	public static final String TYAP_ID_KEY = "tyapId";

//Colonnes dans la base de donnees
	public static final String PREF_DEFAULT_VALUE_COLKEY = "PREF_DEFAULT_VALUE";
	public static final String PREF_DESCRIPTION_COLKEY = "pref_description";
	public static final String PREF_KEY_COLKEY = "PREF_KEY";
	public static final String PREF_PERSONNALISABLE_COLKEY = "pref_personnalisable";

	public static final String PREF_ID_COLKEY = "PREF_id";
	public static final String TYAP_ID_COLKEY = "TYap_ID";


	// Relationships
	public static final String TYPE_APPLICATION_KEY = "typeApplication";



	// Accessors methods
  public String prefDefaultValue() {
    return (String) storedValueForKey(PREF_DEFAULT_VALUE_KEY);
  }

  public void setPrefDefaultValue(String value) {
    takeStoredValueForKey(value, PREF_DEFAULT_VALUE_KEY);
  }

  public String prefDescription() {
    return (String) storedValueForKey(PREF_DESCRIPTION_KEY);
  }

  public void setPrefDescription(String value) {
    takeStoredValueForKey(value, PREF_DESCRIPTION_KEY);
  }

  public String prefKey() {
    return (String) storedValueForKey(PREF_KEY_KEY);
  }

  public void setPrefKey(String value) {
    takeStoredValueForKey(value, PREF_KEY_KEY);
  }

  public Integer prefPersonnalisable() {
    return (Integer) storedValueForKey(PREF_PERSONNALISABLE_KEY);
  }

  public void setPrefPersonnalisable(Integer value) {
    takeStoredValueForKey(value, PREF_PERSONNALISABLE_KEY);
  }

  public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }


  public static EOPreferences createPreferences(EOEditingContext editingContext, String prefKey
, Integer prefPersonnalisable
) {
    EOPreferences eo = (EOPreferences) createAndInsertInstance(editingContext, _EOPreferences.ENTITY_NAME);
		eo.setPrefKey(prefKey);
		eo.setPrefPersonnalisable(prefPersonnalisable);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPreferences.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPreferences.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPreferences creerInstance(EOEditingContext editingContext) {
		  		EOPreferences object = (EOPreferences)createAndInsertInstance(editingContext, _EOPreferences.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOPreferences localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreferences)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOPreferences localInstanceIn(EOEditingContext editingContext, EOPreferences eo) {
    EOPreferences localInstance = (eo == null) ? null : (EOPreferences)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPreferences#localInstanceIn a la place.
   */
	public static EOPreferences localInstanceOf(EOEditingContext editingContext, EOPreferences eo) {
		return EOPreferences.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOPreferences fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPreferences fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreferences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreferences)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOPreferences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOPreferences fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreferences eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreferences)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPreferences fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreferences eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreferences ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOPreferences fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
