

// EOOrgan.java
//
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOOrgan extends _EOOrgan {

	public static final String	PRIMARY_KEY_KEY	= "orgId";

    public EOOrgan() {
        super();
    }

    /**
     * Check if the current logged in user has permissions on this organ.
     * @param loggedUser logged user.
     * @return true if the user logged in has permissions on this organ ; false otherwise.
     */
    public boolean hasPermission(EOUtilisateur loggedUser) {
    	boolean hasPermission = false;
    	if (utilisateurOrgans() == null) {
    		return hasPermission;
    	}
    	for (int idx = 0; idx < utilisateurOrgans().count(); idx++) {
    		EOUtilisateurOrgan utilisateurOrgan = (EOUtilisateurOrgan) utilisateurOrgans().objectAtIndex(idx);
    		if (utilisateurOrgan.utilisateur().equals(loggedUser)) {
    			hasPermission = true;
    			break;
    		}
    	}
    	return hasPermission;
    }

    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();

    }

    public void validateObjectMetier() throws NSValidation.ValidationException {


    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

    }


}
