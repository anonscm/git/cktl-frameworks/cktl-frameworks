// _EOConventionLimitative.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOConventionLimitative.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOConventionLimitative extends  EOGenericRecord {
	public static final String ENTITY_NAME = "ConventionLimitative";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_convention_limitative";



	// Attributes


	public static final String CL_MONTANT_KEY = "clMontant";

// Attributs non visibles
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String CL_MONTANT_COLKEY = "CL_MONTANT";

	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";



	// Accessors methods
  public java.math.BigDecimal clMontant() {
    return (java.math.BigDecimal) storedValueForKey(CL_MONTANT_KEY);
  }

  public void setClMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CL_MONTANT_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOOrgan organ() {
    return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOTypeCredit typeCreditDep() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
  }

  public void setTypeCreditDepRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCreditDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }

  public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }


  public static EOConventionLimitative createConventionLimitative(EOEditingContext editingContext, java.math.BigDecimal clMontant
, org.cocktail.application.client.eof.EOExercice exercice) {
    EOConventionLimitative eo = (EOConventionLimitative) createAndInsertInstance(editingContext, _EOConventionLimitative.ENTITY_NAME);
		eo.setClMontant(clMontant);
    eo.setExerciceRelationship(exercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOConventionLimitative.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOConventionLimitative.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOConventionLimitative creerInstance(EOEditingContext editingContext) {
		  		EOConventionLimitative object = (EOConventionLimitative)createAndInsertInstance(editingContext, _EOConventionLimitative.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOConventionLimitative localInstanceIn(EOEditingContext editingContext) {
	  		return (EOConventionLimitative)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOConventionLimitative localInstanceIn(EOEditingContext editingContext, EOConventionLimitative eo) {
    EOConventionLimitative localInstance = (eo == null) ? null : (EOConventionLimitative)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOConventionLimitative#localInstanceIn a la place.
   */
	public static EOConventionLimitative localInstanceOf(EOEditingContext editingContext, EOConventionLimitative eo) {
		return EOConventionLimitative.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOConventionLimitative fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOConventionLimitative fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOConventionLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOConventionLimitative)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOConventionLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOConventionLimitative fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOConventionLimitative eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOConventionLimitative)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOConventionLimitative fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOConventionLimitative eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOConventionLimitative ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOConventionLimitative fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
