// _EORecettePapier.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecettePapier.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecettePapier extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RecettePapier";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_papier";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rppId";

	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
	public static final String RPP_DATE_RECEPTION_KEY = "rppDateReception";
	public static final String RPP_DATE_RECETTE_KEY = "rppDateRecette";
	public static final String RPP_DATE_SAISIE_KEY = "rppDateSaisie";
	public static final String RPP_DATE_SERVICE_FAIT_KEY = "rppDateServiceFait";
	public static final String RPP_HT_SAISIE_KEY = "rppHtSaisie";
	public static final String RPP_NB_PIECE_KEY = "rppNbPiece";
	public static final String RPP_NUMERO_KEY = "rppNumero";
	public static final String RPP_TTC_SAISIE_KEY = "rppTtcSaisie";
	public static final String RPP_TVA_SAISIE_KEY = "rppTvaSaisie";
	public static final String RPP_VISIBLE_KEY = "rppVisible";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String RPP_ID_KEY = "rppId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String RPP_DATE_RECEPTION_COLKEY = "RPP_DATE_RECEPTION";
	public static final String RPP_DATE_RECETTE_COLKEY = "RPP_DATE_RECETTE";
	public static final String RPP_DATE_SAISIE_COLKEY = "RPP_DATE_SAISIE";
	public static final String RPP_DATE_SERVICE_FAIT_COLKEY = "RPP_DATE_SERVICE_FAIT";
	public static final String RPP_HT_SAISIE_COLKEY = "RPP_HT_SAISIE";
	public static final String RPP_NB_PIECE_COLKEY = "RPP_NB_PIECE";
	public static final String RPP_NUMERO_COLKEY = "RPP_NUMERO";
	public static final String RPP_TTC_SAISIE_COLKEY = "RPP_TTC_SAISIE";
	public static final String RPP_TVA_SAISIE_COLKEY = "RPP_TVA_SAISIE";
	public static final String RPP_VISIBLE_COLKEY = "RPP_VISIBLE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String RPP_ID_COLKEY = "RPP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String PERSONNE_KEY = "personne";
	public static final String RECETTES_KEY = "recettes";
	public static final String RIBFOUR_ULR_KEY = "ribfourUlr";
	public static final String TO_RECETTE_PAPIER_ADR_CLIENTS_KEY = "toRecettePapierAdrClients";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String personne_persNomPrenom() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setPersonne_persNomPrenom(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public NSTimestamp rppDateReception() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_RECEPTION_KEY);
  }

  public void setRppDateReception(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_RECEPTION_KEY);
  }

  public NSTimestamp rppDateRecette() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_RECETTE_KEY);
  }

  public void setRppDateRecette(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_RECETTE_KEY);
  }

  public NSTimestamp rppDateSaisie() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_SAISIE_KEY);
  }

  public void setRppDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_SAISIE_KEY);
  }

  public NSTimestamp rppDateServiceFait() {
    return (NSTimestamp) storedValueForKey(RPP_DATE_SERVICE_FAIT_KEY);
  }

  public void setRppDateServiceFait(NSTimestamp value) {
    takeStoredValueForKey(value, RPP_DATE_SERVICE_FAIT_KEY);
  }

  public java.math.BigDecimal rppHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_HT_SAISIE_KEY);
  }

  public void setRppHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_HT_SAISIE_KEY);
  }

  public Integer rppNbPiece() {
    return (Integer) storedValueForKey(RPP_NB_PIECE_KEY);
  }

  public void setRppNbPiece(Integer value) {
    takeStoredValueForKey(value, RPP_NB_PIECE_KEY);
  }

  public String rppNumero() {
    return (String) storedValueForKey(RPP_NUMERO_KEY);
  }

  public void setRppNumero(String value) {
    takeStoredValueForKey(value, RPP_NUMERO_KEY);
  }

  public java.math.BigDecimal rppTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_TTC_SAISIE_KEY);
  }

  public void setRppTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal rppTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RPP_TVA_SAISIE_KEY);
  }

  public void setRppTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RPP_TVA_SAISIE_KEY);
  }

  public String rppVisible() {
    return (String) storedValueForKey(RPP_VISIBLE_KEY);
  }

  public void setRppVisible(String value) {
    takeStoredValueForKey(value, RPP_VISIBLE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }

  public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOPersonne personne() {
    return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EORibfourUlr ribfourUlr() {
    return (org.cocktail.kava.client.metier.EORibfourUlr)storedValueForKey(RIBFOUR_ULR_KEY);
  }

  public void setRibfourUlrRelationship(org.cocktail.kava.client.metier.EORibfourUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EORibfourUlr oldValue = ribfourUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIBFOUR_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_ULR_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }

  public NSArray recettes() {
    return (NSArray)storedValueForKey(RECETTES_KEY);
  }

  public NSArray recettes(EOQualifier qualifier) {
    return recettes(qualifier, null, false);
  }

  public NSArray recettes(EOQualifier qualifier, boolean fetch) {
    return recettes(qualifier, null, fetch);
  }

  public NSArray recettes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORecette.RECETTE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recettes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public void removeFromRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public org.cocktail.kava.client.metier.EORecette createRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Recette");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTES_KEY);
    return (org.cocktail.kava.client.metier.EORecette) eo;
  }

  public void deleteRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecettesRelationships() {
    Enumeration objects = recettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecettesRelationship((org.cocktail.kava.client.metier.EORecette)objects.nextElement());
    }
  }

  public NSArray toRecettePapierAdrClients() {
    return (NSArray)storedValueForKey(TO_RECETTE_PAPIER_ADR_CLIENTS_KEY);
  }

  public NSArray toRecettePapierAdrClients(EOQualifier qualifier) {
    return toRecettePapierAdrClients(qualifier, null, false);
  }

  public NSArray toRecettePapierAdrClients(EOQualifier qualifier, boolean fetch) {
    return toRecettePapierAdrClients(qualifier, null, fetch);
  }

  public NSArray toRecettePapierAdrClients(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORecettePapierAdrClient.TO_RECETTE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORecettePapierAdrClient.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRecettePapierAdrClients();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToToRecettePapierAdrClientsRelationship(org.cocktail.kava.client.metier.EORecettePapierAdrClient object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RECETTE_PAPIER_ADR_CLIENTS_KEY);
  }

  public void removeFromToRecettePapierAdrClientsRelationship(org.cocktail.kava.client.metier.EORecettePapierAdrClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_PAPIER_ADR_CLIENTS_KEY);
  }

  public org.cocktail.kava.client.metier.EORecettePapierAdrClient createToRecettePapierAdrClientsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RecettePapierAdrClient");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RECETTE_PAPIER_ADR_CLIENTS_KEY);
    return (org.cocktail.kava.client.metier.EORecettePapierAdrClient) eo;
  }

  public void deleteToRecettePapierAdrClientsRelationship(org.cocktail.kava.client.metier.EORecettePapierAdrClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RECETTE_PAPIER_ADR_CLIENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRecettePapierAdrClientsRelationships() {
    Enumeration objects = toRecettePapierAdrClients().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRecettePapierAdrClientsRelationship((org.cocktail.kava.client.metier.EORecettePapierAdrClient)objects.nextElement());
    }
  }


  public static EORecettePapier createRecettePapier(EOEditingContext editingContext, NSTimestamp rppDateSaisie
, java.math.BigDecimal rppHtSaisie
, String rppNumero
, java.math.BigDecimal rppTtcSaisie
, java.math.BigDecimal rppTvaSaisie
, String rppVisible
) {
    EORecettePapier eo = (EORecettePapier) createAndInsertInstance(editingContext, _EORecettePapier.ENTITY_NAME);
		eo.setRppDateSaisie(rppDateSaisie);
		eo.setRppHtSaisie(rppHtSaisie);
		eo.setRppNumero(rppNumero);
		eo.setRppTtcSaisie(rppTtcSaisie);
		eo.setRppTvaSaisie(rppTvaSaisie);
		eo.setRppVisible(rppVisible);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecettePapier.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecettePapier.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecettePapier creerInstance(EOEditingContext editingContext) {
		  		EORecettePapier object = (EORecettePapier)createAndInsertInstance(editingContext, _EORecettePapier.ENTITY_NAME);
		  		return object;
			}


	
  	  public EORecettePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecettePapier)localInstanceOfObject(editingContext, this);
	  }
	
  public static EORecettePapier localInstanceIn(EOEditingContext editingContext, EORecettePapier eo) {
    EORecettePapier localInstance = (eo == null) ? null : (EORecettePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecettePapier#localInstanceIn a la place.
   */
	public static EORecettePapier localInstanceOf(EOEditingContext editingContext, EORecettePapier eo) {
		return EORecettePapier.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EORecettePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecettePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecettePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EORecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EORecettePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecettePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecettePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecettePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecettePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecettePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EORecettePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
