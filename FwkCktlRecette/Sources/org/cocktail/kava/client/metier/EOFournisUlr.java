// EOFournisUlr.java
//
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOFournisUlr extends _EOFournisUlr {

	/** Serial version Id. */
	private static final long serialVersionUID = 1L;

	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
	public static final String PRIMARY_KEY_KEY = "fouOrdre";

	public static final String FOU_ETAT_VALIDE = "O";
	public static final String FOU_ETAT_ATTENTE = "N";
	public static final String FOU_ETAT_INVALIDE = "A";
	public static final String FOU_TYPE_FOURNISSEUR = "F";
	public static final String FOU_TYPE_CLIENT = "C";
	public static final String FOU_TYPE_TIERS = "T";

	public EOFournisUlr() {
		super();
	}

	public boolean validateForSelect() {
		return FOU_TYPE_CLIENT.equals(fouType()) || FOU_TYPE_TIERS.equals(fouType());
	}

	public boolean isTypeFournisseurOnly() {
		return FOU_TYPE_FOURNISSEUR.equals(fouType());
	}

	public boolean isEtatValide() {
		return FOU_ETAT_VALIDE.equals(fouValide());
	}

	public boolean isEtatInvalide() {
		return FOU_ETAT_INVALIDE.equals(fouValide());
	}

	public boolean isEtat(String etat) {
		if (etat == null) {
			return false;
		}
		return etat.equals(fouValide());
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public String personne_persNomPrenom() {
		if (personne() != null) {
			return personne().persNomPrenom();
		}
		return "";
	}

}
