// EOPrestationLigne.java
//
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;
import java.util.Collections;

import org.cocktail.kava.client.factory.FactoryPrestationLigne;
import org.cocktail.kava.client.finder.FinderCatalogueArticle;
import org.cocktail.kava.client.finder.FinderTypeArticle;
import org.cocktail.kava.client.finder.FinderTypeEtat;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;
import org.cocktail.pieFwk.common.metier.Prestation;
import org.cocktail.pieFwk.common.metier.PrestationLigne;
import org.cocktail.pieFwk.common.metier.PrestationLigneCompanion;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrestationLigne extends _EOPrestationLigne implements PrestationLigne {

	public static final String PRLIG_TOTAL_TVA_KEY = "prligTotalTva";
	public static final String PRLIG_TOTAL_RESTE_TVA_KEY = "prligTotalResteTva";

	private PrestationLigneCompanion companion;

	public EOPrestationLigne() {
		super();
		this.companion = new PrestationLigneCompanion(this);
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise HT
	 */
	public void setPrligArtHt(BigDecimal aValue) {
		super.setPrligArtHt(aValue);
		companion().updateTotalHt();
		companion().updateTotalResteHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total remise TTC
	 */
	public void setPrligArtTtc(BigDecimal aValue) {
		super.setPrligArtTtc(aValue);
		companion().updateTotalTtc();
		companion().updateTotalResteTtc();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite, la quantite restante, recalcule les
	 *            totaux remises HT et TTC, et met a jour les options/remises,
	 *            et fait le cafe, le menage et les devoirs des enfants...
	 *            pfff....
	 */
	public void setPrligQuantite(BigDecimal aValue) {
		super.setPrligQuantite(aValue);
		super.setPrligQuantiteReste(aValue);
		companion().updateTotaux();

		// mise a jour des remises quantitatives possibles
		if (catalogueArticle() == null
				|| catalogueArticle().article() == null
				|| catalogueArticle().article().articles() == null
				|| catalogueArticle().article().articles().count() == 0) {
			return;
		}
		updateOptionsRemises();
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite restante et recalcule les totaux restants remises HT et TTC
	 */
	public void setPrligQuantiteReste(BigDecimal aValue) {
		super.setPrligQuantiteReste(aValue);
		companion().updateTotalResteHt();
		companion().updateTotalResteTtc();

		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i)).setPrligQuantiteReste(prligQuantiteReste());
			}
		}
	}

	private void updateOptionsRemises() {
		if (catalogueArticle() == null) {
			return;
		}
		// recherche des remises quantitatives valides pour le type de public
		NSArray remises = FinderCatalogueArticle.find(
				editingContext(),
				catalogueArticle(),
				catalogueArticle().article().articlePrestation().typePublic(),
				FinderTypeEtat.typeEtatValide(editingContext()),
				Collections.singleton(FinderTypeArticle.typeArticleRemise(editingContext())),
				prligQuantite());

		// suppression de celles qui ne doivent plus y etre, mise a jour de la
		// quantite des autres
		if (prestationLignes() != null) {
			int i = 0;
			while (prestationLignes().count() > i) {
				EOPrestationLigne pl = (EOPrestationLigne) prestationLignes().objectAtIndex(i);
				if (pl.catalogueArticle() != null && pl.catalogueArticle().article().typeArticle().equals(FinderTypeArticle.typeArticleRemise(editingContext()))) {
					if (remises != null && remises.containsObject(pl.catalogueArticle())) {
						pl.setPrligQuantite(prligQuantite());
						i++;
					} else {
						FactoryPrestationLigne.removeObject(editingContext(), pl, this);
						editingContext().deleteObject(pl);
					}
				} else {
					pl.setPrligQuantite(prligQuantite());
					i++;
				}
			}
		}

		// ajout des nouvelles qui n'y sont pas encore
		if (remises != null && remises.count() > 0) {
			for (int i = 0; i < remises.count(); i++) {
				EOCatalogueArticle catalogueArticle = (EOCatalogueArticle) remises.objectAtIndex(i);
				if (!((NSArray) prestationLignes().valueForKey(EOPrestationLigne.CATALOGUE_ARTICLE_KEY)).containsObject(catalogueArticle)) {
					FactoryPrestationLigne.newObject(editingContext(), this, catalogueArticle);
				}
			}
		}
	}

	public BigDecimal prligTotalTva() {
		if (prligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalHt() == null) {
			return prligTotalTtc();
		}
		return prligTotalTtc().subtract(prligTotalHt());
	}

	public BigDecimal prligTotalResteTva() {
		if (prligTotalResteTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalResteHt() == null) {
			return prligTotalResteTtc();
		}
		return prligTotalResteTtc().subtract(prligTotalResteHt());
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (typeArticle() == null) {
			throw new ValidationException("Il faut un type article pour les lignes du panier!");
		}
		if (prestation() == null) {
			throw new ValidationException("Il faut une prestation pour les lignes du panier!");
		}
		if (prligDate() == null) {
			throw new ValidationException("Il faut une date de creation pour les lignes du panier!");
		}
		if (prligDescription() == null) {
			throw new ValidationException("Il faut un libelle pour les lignes du panier!");
		}
		if (prligArtHt() == null) {
			throw new ValidationException("Il faut un prix HT pour les lignes du panier!");
		}
		if (prligArtTtc() == null) {
			throw new ValidationException("Il faut un prix TTC pour les lignes du panier!");
		}
		if (prligQuantite() == null) {
			throw new ValidationException("Il faut une quantite pour les lignes du panier!");
		}
		if (prligQuantiteReste() == null) {
			throw new ValidationException("Il faut une quantite restante pour les lignes du panier!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public PrestationLigneCompanion companion() {
		return this.companion;
	}

	public Prestation getPrestationCommon() {
		return prestation();
	}

	public CatalogueArticle getCatalogueArticleCommon() {
		return catalogueArticle();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) EOApplication.sharedApplication();
	}

	public Number exerciceAsNumber() {
		if (prestation() != null && prestation().exercice() != null) {
			return prestation().exercice().exeExercice();
		}
		return null;
	}

}
