

// EOParametreJefyAdmin.java
// 
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOParametreJefyAdmin extends _EOParametreJefyAdmin {

	public static final String PARAM_UNIVERSITE="UNIV";
	public static final String PARAM_USE_DECIMAL="FORMAT_USE_DECIMAL";
		
    public EOParametreJefyAdmin() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
