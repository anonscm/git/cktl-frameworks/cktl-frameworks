// _EOUtilisateurFonction.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateurFonction.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOUtilisateurFonction extends  EOGenericRecord {
	public static final String ENTITY_NAME = "UtilisateurFonction";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.utilisateur_fonct";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ufOrdre";

	public static final String FONCTION_FON_ID_INTERNE_KEY = "fonction_fonIdInterne";
	public static final String FONCTION_FON_SPEC_EXERCICE_KEY = "fonction_fonSpecExercice";

// Attributs non visibles
	public static final String FON_ORDRE_KEY = "fonOrdre";
	public static final String UF_ORDRE_KEY = "ufOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String FONCTION_FON_ID_INTERNE_COLKEY = "$attribute.columnName";
	public static final String FONCTION_FON_SPEC_EXERCICE_COLKEY = "$attribute.columnName";

	public static final String FON_ORDRE_COLKEY = "FON_ORDRE";
	public static final String UF_ORDRE_COLKEY = "uf_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String FONCTION_KEY = "fonction";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_FONCTION_EXERCICES_KEY = "utilisateurFonctionExercices";
	public static final String UTILISATEUR_FONCTION_GESTIONS_KEY = "utilisateurFonctionGestions";



	// Accessors methods
  public String fonction_fonIdInterne() {
    return (String) storedValueForKey(FONCTION_FON_ID_INTERNE_KEY);
  }

  public void setFonction_fonIdInterne(String value) {
    takeStoredValueForKey(value, FONCTION_FON_ID_INTERNE_KEY);
  }

  public String fonction_fonSpecExercice() {
    return (String) storedValueForKey(FONCTION_FON_SPEC_EXERCICE_KEY);
  }

  public void setFonction_fonSpecExercice(String value) {
    takeStoredValueForKey(value, FONCTION_FON_SPEC_EXERCICE_KEY);
  }

  public org.cocktail.kava.client.metier.EOFonction fonction() {
    return (org.cocktail.kava.client.metier.EOFonction)storedValueForKey(FONCTION_KEY);
  }

  public void setFonctionRelationship(org.cocktail.kava.client.metier.EOFonction value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFonction oldValue = fonction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FONCTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FONCTION_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }

  public NSArray utilisateurFonctionExercices() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCTION_EXERCICES_KEY);
  }

  public NSArray utilisateurFonctionExercices(EOQualifier qualifier) {
    return utilisateurFonctionExercices(qualifier, null, false);
  }

  public NSArray utilisateurFonctionExercices(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctionExercices(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctionExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice.UTILISATEUR_FONCTION_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctionExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToUtilisateurFonctionExercicesRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_EXERCICES_KEY);
  }

  public void removeFromUtilisateurFonctionExercicesRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_EXERCICES_KEY);
  }

  public org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice createUtilisateurFonctionExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("UtilisateurFonctionExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCTION_EXERCICES_KEY);
    return (org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice) eo;
  }

  public void deleteUtilisateurFonctionExercicesRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctionExercicesRelationships() {
    Enumeration objects = utilisateurFonctionExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctionExercicesRelationship((org.cocktail.kava.client.metier.EOUtilisateurFonctionExercice)objects.nextElement());
    }
  }

  public NSArray utilisateurFonctionGestions() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCTION_GESTIONS_KEY);
  }

  public NSArray utilisateurFonctionGestions(EOQualifier qualifier) {
    return utilisateurFonctionGestions(qualifier, null, false);
  }

  public NSArray utilisateurFonctionGestions(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctionGestions(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctionGestions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion.UTILISATEUR_FONCTION_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctionGestions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToUtilisateurFonctionGestionsRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_GESTIONS_KEY);
  }

  public void removeFromUtilisateurFonctionGestionsRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_GESTIONS_KEY);
  }

  public org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion createUtilisateurFonctionGestionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("UtilisateurFonctionGestion");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCTION_GESTIONS_KEY);
    return (org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion) eo;
  }

  public void deleteUtilisateurFonctionGestionsRelationship(org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTION_GESTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctionGestionsRelationships() {
    Enumeration objects = utilisateurFonctionGestions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctionGestionsRelationship((org.cocktail.kava.client.metier.EOUtilisateurFonctionGestion)objects.nextElement());
    }
  }


  public static EOUtilisateurFonction createUtilisateurFonction(EOEditingContext editingContext, String fonction_fonIdInterne
, String fonction_fonSpecExercice
) {
    EOUtilisateurFonction eo = (EOUtilisateurFonction) createAndInsertInstance(editingContext, _EOUtilisateurFonction.ENTITY_NAME);
		eo.setFonction_fonIdInterne(fonction_fonIdInterne);
		eo.setFonction_fonSpecExercice(fonction_fonSpecExercice);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOUtilisateurFonction.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOUtilisateurFonction.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOUtilisateurFonction creerInstance(EOEditingContext editingContext) {
		  		EOUtilisateurFonction object = (EOUtilisateurFonction)createAndInsertInstance(editingContext, _EOUtilisateurFonction.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOUtilisateurFonction localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateurFonction)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOUtilisateurFonction localInstanceIn(EOEditingContext editingContext, EOUtilisateurFonction eo) {
    EOUtilisateurFonction localInstance = (eo == null) ? null : (EOUtilisateurFonction)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOUtilisateurFonction#localInstanceIn a la place.
   */
	public static EOUtilisateurFonction localInstanceOf(EOEditingContext editingContext, EOUtilisateurFonction eo) {
		return EOUtilisateurFonction.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOUtilisateurFonction fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOUtilisateurFonction fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateurFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateurFonction)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOUtilisateurFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOUtilisateurFonction fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateurFonction eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateurFonction)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOUtilisateurFonction fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateurFonction eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateurFonction ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOUtilisateurFonction fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
