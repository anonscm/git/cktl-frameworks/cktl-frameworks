
// EOArticlePrestation.java
//
package org.cocktail.kava.client.metier;

import org.cocktail.pieFwk.common.metier.ArticlePrestation;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOArticlePrestation extends _EOArticlePrestation implements ArticlePrestation {

	public EOArticlePrestation() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (pcoNumRecette() == null) {
			throw new ValidationException("Il faut une imputation recette pour l'article!");
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public NSTimestamp dateDebut() {
		return artpCfcDateDebut();
	}

	public NSTimestamp dateFin() {
		return artpCfcDateFin();
	}

}
