

// EOParametres.java
// 
package org.cocktail.kava.client.metier;


import com.webobjects.foundation.NSValidation;

public class EOParametres extends _EOParametres {

	public static final String PARAM_AUTORISE_CREATION_CLIENT="AUTORISE_CREATION_CLIENT";
	public static final String PARAM_AUTORISE_CREATION_RIB="AUTORISE_CREATION_RIB";
	public static final String PARAM_AUTORISE_PI_FROM_COMMANDE="AUTORISE_PI_FROM_COMMANDE";
	public static final String PARAM_CRYPT_PASSWORD_COMPTE_CLIENT="CRYPT_PASSWORD_COMPTE_CLIENT";
	public static final String PARAM_CTRL_ORGAN_DEST="CTRL_ORGAN_DEST";
	public static final String PARAM_MODE_PAIEMENT_PI="MODE_PAIEMENT_PI";
	public static final String PARAM_MODE_RECOUVREMENT_PI="MODE_RECOUVREMENT_PI";
	public static final String PARAM_RECETTE_IDEM_TAP_ID="RECETTE_IDEM_TAP_ID";
	public static final String PARAM_SIXID_PIE_DEVIS="SIXID_PIE_DEVIS";
	public static final String PARAM_SIXID_PIE_FACTURE="SIXID_PIE_FACTURE";
	public static final String PARAM_SIXID_PIE_FACTURE_ANGLAIS="SIXID_PIE_FACTURE_ANGLAIS";
	public static final String PARAM_WS_PRESTATION="WS_PRESTATION";
	public static final String PARAM_WS_PRESTATION_PASSWORD="WS_PRESTATION_PASSWORD";
	public static final String PARAM_CONFIG_URL_APP_WEB="APP_URL_WEB";

    public EOParametres() {
        super();
    }


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }

    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }


}
