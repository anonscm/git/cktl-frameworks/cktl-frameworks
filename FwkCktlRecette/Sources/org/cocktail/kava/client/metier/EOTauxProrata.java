// EOTauxProrata.java
// 
package org.cocktail.kava.client.metier;

import java.math.BigDecimal;

import com.webobjects.foundation.NSValidation;

public class EOTauxProrata extends _EOTauxProrata {

	public static final String	PRIMARY_KEY_KEY	= "tapId";

	public BigDecimal getMontantBudgetaire(BigDecimal ht, BigDecimal tva) {
		if (ht == null) {
			ht = new BigDecimal(0.0);
		}
		if (tva == null) {
			tva = new BigDecimal(0.0);
		}
		return ht.add(
				tva.multiply(new BigDecimal(1.0).subtract(tapTaux().divide(new BigDecimal(100.0), 2, BigDecimal.ROUND_HALF_UP)).setScale(2,
						BigDecimal.ROUND_HALF_UP))).setScale(2, BigDecimal.ROUND_HALF_UP);
	}

	public EOTauxProrata() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
