// _EORecetteCtrlAnalytique.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORecetteCtrlAnalytique.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORecetteCtrlAnalytique extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RecetteCtrlAnalytique";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.recette_ctrl_analytique";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ranaId";

	public static final String RANA_DATE_SAISIE_KEY = "ranaDateSaisie";
	public static final String RANA_HT_SAISIE_KEY = "ranaHtSaisie";
	public static final String RANA_MONTANT_BUDGETAIRE_KEY = "ranaMontantBudgetaire";
	public static final String RANA_TTC_SAISIE_KEY = "ranaTtcSaisie";
	public static final String RANA_TVA_SAISIE_KEY = "ranaTvaSaisie";
	public static final String REC_ID_KEY = "recId";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String RANA_ID_KEY = "ranaId";

//Colonnes dans la base de donnees
	public static final String RANA_DATE_SAISIE_COLKEY = "RANA_DATE_SAISIE";
	public static final String RANA_HT_SAISIE_COLKEY = "RANA_HT_SAISIE";
	public static final String RANA_MONTANT_BUDGETAIRE_COLKEY = "RANA_MONTANT_BUDGETAIRE";
	public static final String RANA_TTC_SAISIE_COLKEY = "RANA_TTC_SAISIE";
	public static final String RANA_TVA_SAISIE_COLKEY = "RANA_TVA_SAISIE";
	public static final String REC_ID_COLKEY = "REC_ID";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String RANA_ID_COLKEY = "RANA_ID";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public NSTimestamp ranaDateSaisie() {
    return (NSTimestamp) storedValueForKey(RANA_DATE_SAISIE_KEY);
  }

  public void setRanaDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, RANA_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal ranaHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RANA_HT_SAISIE_KEY);
  }

  public void setRanaHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RANA_HT_SAISIE_KEY);
  }

  public java.math.BigDecimal ranaMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(RANA_MONTANT_BUDGETAIRE_KEY);
  }

  public void setRanaMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RANA_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal ranaTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RANA_TTC_SAISIE_KEY);
  }

  public void setRanaTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RANA_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal ranaTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(RANA_TVA_SAISIE_KEY);
  }

  public void setRanaTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, RANA_TVA_SAISIE_KEY);
  }

  public Integer recId() {
    return (Integer) storedValueForKey(REC_ID_KEY);
  }

  public void setRecId(Integer value) {
    takeStoredValueForKey(value, REC_ID_KEY);
  }

  public org.cocktail.kava.client.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kava.client.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kava.client.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }


  public static EORecetteCtrlAnalytique createRecetteCtrlAnalytique(EOEditingContext editingContext, NSTimestamp ranaDateSaisie
, java.math.BigDecimal ranaHtSaisie
, java.math.BigDecimal ranaMontantBudgetaire
, java.math.BigDecimal ranaTtcSaisie
, java.math.BigDecimal ranaTvaSaisie
, Integer recId
) {
    EORecetteCtrlAnalytique eo = (EORecetteCtrlAnalytique) createAndInsertInstance(editingContext, _EORecetteCtrlAnalytique.ENTITY_NAME);
		eo.setRanaDateSaisie(ranaDateSaisie);
		eo.setRanaHtSaisie(ranaHtSaisie);
		eo.setRanaMontantBudgetaire(ranaMontantBudgetaire);
		eo.setRanaTtcSaisie(ranaTtcSaisie);
		eo.setRanaTvaSaisie(ranaTvaSaisie);
		eo.setRecId(recId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EORecetteCtrlAnalytique.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EORecetteCtrlAnalytique.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EORecetteCtrlAnalytique creerInstance(EOEditingContext editingContext) {
		  		EORecetteCtrlAnalytique object = (EORecetteCtrlAnalytique)createAndInsertInstance(editingContext, _EORecetteCtrlAnalytique.ENTITY_NAME);
		  		return object;
			}


	
  	  public EORecetteCtrlAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EORecetteCtrlAnalytique)localInstanceOfObject(editingContext, this);
	  }
	
  public static EORecetteCtrlAnalytique localInstanceIn(EOEditingContext editingContext, EORecetteCtrlAnalytique eo) {
    EORecetteCtrlAnalytique localInstance = (eo == null) ? null : (EORecetteCtrlAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EORecetteCtrlAnalytique#localInstanceIn a la place.
   */
	public static EORecetteCtrlAnalytique localInstanceOf(EOEditingContext editingContext, EORecetteCtrlAnalytique eo) {
		return EORecetteCtrlAnalytique.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EORecetteCtrlAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EORecetteCtrlAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORecetteCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORecetteCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EORecetteCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EORecetteCtrlAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORecetteCtrlAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORecetteCtrlAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EORecetteCtrlAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORecetteCtrlAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORecetteCtrlAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EORecetteCtrlAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
