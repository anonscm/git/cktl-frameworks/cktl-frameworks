// EOCataloguePublic.java
// 
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOCataloguePublic extends _EOCataloguePublic {

	public EOCataloguePublic() {
		super();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		if (capPourcentage() != null) {
			if (capPourcentage().doubleValue() < 0.0 || capPourcentage().doubleValue() > 100.0) {
				throw new ValidationException("Le pourcentage de remise doit etre entre 0 et 100 :-) !");
			}
		}
	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
