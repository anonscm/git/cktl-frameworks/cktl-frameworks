// _EOFacture.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFacture.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFacture extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Facture";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "facId";

	public static final String ECHE_ID_KEY = "echeId";
	public static final String FAC_DATE_SAISIE_KEY = "facDateSaisie";
	public static final String FAC_HT_SAISIE_KEY = "facHtSaisie";
	public static final String FAC_LIB_KEY = "facLib";
	public static final String FAC_MONTANT_BUDGETAIRE_KEY = "facMontantBudgetaire";
	public static final String FAC_MONTANT_BUDGETAIRE_RESTE_KEY = "facMontantBudgetaireReste";
	public static final String FAC_NUMERO_KEY = "facNumero";
	public static final String FAC_TTC_SAISIE_KEY = "facTtcSaisie";
	public static final String FAC_TVA_SAISIE_KEY = "facTvaSaisie";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PERS_ID_KEY = "persId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECHE_ID_COLKEY = "ECHE_ID";
	public static final String FAC_DATE_SAISIE_COLKEY = "FAC_DATE_SAISIE";
	public static final String FAC_HT_SAISIE_COLKEY = "FAC_HT_SAISIE";
	public static final String FAC_LIB_COLKEY = "FAC_LIB";
	public static final String FAC_MONTANT_BUDGETAIRE_COLKEY = "FAC_MONTANT_BUDGETAIRE";
	public static final String FAC_MONTANT_BUDGETAIRE_RESTE_COLKEY = "FAC_MONTANT_BUDGETAIRE_RESTE";
	public static final String FAC_NUMERO_COLKEY = "FAC_NUMERO";
	public static final String FAC_TTC_SAISIE_COLKEY = "FAC_TTC_SAISIE";
	public static final String FAC_TVA_SAISIE_COLKEY = "FAC_TVA_SAISIE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String FACTURE_CTRL_ACTIONS_KEY = "factureCtrlActions";
	public static final String FACTURE_CTRL_ANALYTIQUES_KEY = "factureCtrlAnalytiques";
	public static final String FACTURE_CTRL_CONVENTIONS_KEY = "factureCtrlConventions";
	public static final String FACTURE_CTRL_PLANCOS_KEY = "factureCtrlPlancos";
	public static final String FACTURE_PAPIER_KEY = "facturePapier";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String ORGAN_KEY = "organ";
	public static final String PERSONNE_KEY = "personne";
	public static final String RECETTES_KEY = "recettes";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public Integer echeId() {
    return (Integer) storedValueForKey(ECHE_ID_KEY);
  }

  public void setEcheId(Integer value) {
    takeStoredValueForKey(value, ECHE_ID_KEY);
  }

  public NSTimestamp facDateSaisie() {
    return (NSTimestamp) storedValueForKey(FAC_DATE_SAISIE_KEY);
  }

  public void setFacDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, FAC_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal facHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_HT_SAISIE_KEY);
  }

  public void setFacHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_HT_SAISIE_KEY);
  }

  public String facLib() {
    return (String) storedValueForKey(FAC_LIB_KEY);
  }

  public void setFacLib(String value) {
    takeStoredValueForKey(value, FAC_LIB_KEY);
  }

  public java.math.BigDecimal facMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(FAC_MONTANT_BUDGETAIRE_KEY);
  }

  public void setFacMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal facMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setFacMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer facNumero() {
    return (Integer) storedValueForKey(FAC_NUMERO_KEY);
  }

  public void setFacNumero(Integer value) {
    takeStoredValueForKey(value, FAC_NUMERO_KEY);
  }

  public java.math.BigDecimal facTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_TTC_SAISIE_KEY);
  }

  public void setFacTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal facTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(FAC_TVA_SAISIE_KEY);
  }

  public void setFacTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAC_TVA_SAISIE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.client.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }

  public void setFournisUlrRelationship(org.cocktail.kava.client.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.kava.client.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.kava.client.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOOrgan organ() {
    return (org.cocktail.kava.client.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.client.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOPersonne personne() {
    return (org.cocktail.kava.client.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.client.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.kava.client.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.kava.client.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.kava.client.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.kava.client.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.client.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }

  public void setTypeCreditRecRelationship(org.cocktail.application.client.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }

  public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.client.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.client.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }

  public NSArray factureCtrlActions() {
    return (NSArray)storedValueForKey(FACTURE_CTRL_ACTIONS_KEY);
  }

  public NSArray factureCtrlActions(EOQualifier qualifier) {
    return factureCtrlActions(qualifier, null);
  }

  public NSArray factureCtrlActions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = factureCtrlActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToFactureCtrlActionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ACTIONS_KEY);
  }

  public void removeFromFactureCtrlActionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ACTIONS_KEY);
  }

  public org.cocktail.kava.client.metier.EOFactureCtrlAction createFactureCtrlActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FactureCtrlAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_CTRL_ACTIONS_KEY);
    return (org.cocktail.kava.client.metier.EOFactureCtrlAction) eo;
  }

  public void deleteFactureCtrlActionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFactureCtrlActionsRelationships() {
    Enumeration objects = factureCtrlActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFactureCtrlActionsRelationship((org.cocktail.kava.client.metier.EOFactureCtrlAction)objects.nextElement());
    }
  }

  public NSArray factureCtrlAnalytiques() {
    return (NSArray)storedValueForKey(FACTURE_CTRL_ANALYTIQUES_KEY);
  }

  public NSArray factureCtrlAnalytiques(EOQualifier qualifier) {
    return factureCtrlAnalytiques(qualifier, null);
  }

  public NSArray factureCtrlAnalytiques(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = factureCtrlAnalytiques();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToFactureCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ANALYTIQUES_KEY);
  }

  public void removeFromFactureCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ANALYTIQUES_KEY);
  }

  public org.cocktail.kava.client.metier.EOFactureCtrlAnalytique createFactureCtrlAnalytiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FactureCtrlAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_CTRL_ANALYTIQUES_KEY);
    return (org.cocktail.kava.client.metier.EOFactureCtrlAnalytique) eo;
  }

  public void deleteFactureCtrlAnalytiquesRelationship(org.cocktail.kava.client.metier.EOFactureCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_ANALYTIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFactureCtrlAnalytiquesRelationships() {
    Enumeration objects = factureCtrlAnalytiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFactureCtrlAnalytiquesRelationship((org.cocktail.kava.client.metier.EOFactureCtrlAnalytique)objects.nextElement());
    }
  }

  public NSArray factureCtrlConventions() {
    return (NSArray)storedValueForKey(FACTURE_CTRL_CONVENTIONS_KEY);
  }

  public NSArray factureCtrlConventions(EOQualifier qualifier) {
    return factureCtrlConventions(qualifier, null);
  }

  public NSArray factureCtrlConventions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = factureCtrlConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToFactureCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_CONVENTIONS_KEY);
  }

  public void removeFromFactureCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_CONVENTIONS_KEY);
  }

  public org.cocktail.kava.client.metier.EOFactureCtrlConvention createFactureCtrlConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FactureCtrlConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_CTRL_CONVENTIONS_KEY);
    return (org.cocktail.kava.client.metier.EOFactureCtrlConvention) eo;
  }

  public void deleteFactureCtrlConventionsRelationship(org.cocktail.kava.client.metier.EOFactureCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFactureCtrlConventionsRelationships() {
    Enumeration objects = factureCtrlConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFactureCtrlConventionsRelationship((org.cocktail.kava.client.metier.EOFactureCtrlConvention)objects.nextElement());
    }
  }

  public NSArray factureCtrlPlancos() {
    return (NSArray)storedValueForKey(FACTURE_CTRL_PLANCOS_KEY);
  }

  public NSArray factureCtrlPlancos(EOQualifier qualifier) {
    return factureCtrlPlancos(qualifier, null);
  }

  public NSArray factureCtrlPlancos(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = factureCtrlPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }

  public void addToFactureCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_PLANCOS_KEY);
  }

  public void removeFromFactureCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_PLANCOS_KEY);
  }

  public org.cocktail.kava.client.metier.EOFactureCtrlPlanco createFactureCtrlPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FactureCtrlPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_CTRL_PLANCOS_KEY);
    return (org.cocktail.kava.client.metier.EOFactureCtrlPlanco) eo;
  }

  public void deleteFactureCtrlPlancosRelationship(org.cocktail.kava.client.metier.EOFactureCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_CTRL_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFactureCtrlPlancosRelationships() {
    Enumeration objects = factureCtrlPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFactureCtrlPlancosRelationship((org.cocktail.kava.client.metier.EOFactureCtrlPlanco)objects.nextElement());
    }
  }

  public NSArray facturePapier() {
    return (NSArray)storedValueForKey(FACTURE_PAPIER_KEY);
  }

  public NSArray facturePapier(EOQualifier qualifier) {
    return facturePapier(qualifier, null, false);
  }

  public NSArray facturePapier(EOQualifier qualifier, boolean fetch) {
    return facturePapier(qualifier, null, fetch);
  }

  public NSArray facturePapier(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EOFacturePapier.FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EOFacturePapier.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = facturePapier();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
  }

  public void removeFromFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
  }

  public org.cocktail.kava.client.metier.EOFacturePapier createFacturePapierRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FacturePapier");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_PAPIER_KEY);
    return (org.cocktail.kava.client.metier.EOFacturePapier) eo;
  }

  public void deleteFacturePapierRelationship(org.cocktail.kava.client.metier.EOFacturePapier object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFacturePapierRelationships() {
    Enumeration objects = facturePapier().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFacturePapierRelationship((org.cocktail.kava.client.metier.EOFacturePapier)objects.nextElement());
    }
  }

  public NSArray recettes() {
    return (NSArray)storedValueForKey(RECETTES_KEY);
  }

  public NSArray recettes(EOQualifier qualifier) {
    return recettes(qualifier, null, false);
  }

  public NSArray recettes(EOQualifier qualifier, boolean fetch) {
    return recettes(qualifier, null, fetch);
  }

  public NSArray recettes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.client.metier.EORecette.FACTURE_KEY, EOQualifier.QualifierOperatorEqual, this);

      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.client.metier.EORecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = recettes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }

  public void addToRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    addObjectToBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public void removeFromRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
  }

  public org.cocktail.kava.client.metier.EORecette createRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Recette");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, RECETTES_KEY);
    return (org.cocktail.kava.client.metier.EORecette) eo;
  }

  public void deleteRecettesRelationship(org.cocktail.kava.client.metier.EORecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRecettesRelationships() {
    Enumeration objects = recettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRecettesRelationship((org.cocktail.kava.client.metier.EORecette)objects.nextElement());
    }
  }


  public static EOFacture createFacture(EOEditingContext editingContext, NSTimestamp facDateSaisie
, java.math.BigDecimal facHtSaisie
, java.math.BigDecimal facMontantBudgetaire
, java.math.BigDecimal facMontantBudgetaireReste
, Integer facNumero
, java.math.BigDecimal facTtcSaisie
, java.math.BigDecimal facTvaSaisie
) {
    EOFacture eo = (EOFacture) createAndInsertInstance(editingContext, _EOFacture.ENTITY_NAME);
		eo.setFacDateSaisie(facDateSaisie);
		eo.setFacHtSaisie(facHtSaisie);
		eo.setFacMontantBudgetaire(facMontantBudgetaire);
		eo.setFacMontantBudgetaireReste(facMontantBudgetaireReste);
		eo.setFacNumero(facNumero);
		eo.setFacTtcSaisie(facTtcSaisie);
		eo.setFacTvaSaisie(facTvaSaisie);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOFacture.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOFacture.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOFacture creerInstance(EOEditingContext editingContext) {
		  		EOFacture object = (EOFacture)createAndInsertInstance(editingContext, _EOFacture.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOFacture localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFacture)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOFacture localInstanceIn(EOEditingContext editingContext, EOFacture eo) {
    EOFacture localInstance = (eo == null) ? null : (EOFacture)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOFacture#localInstanceIn a la place.
   */
	public static EOFacture localInstanceOf(EOEditingContext editingContext, EOFacture eo) {
		return EOFacture.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOFacture fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOFacture fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFacture)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOFacture fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFacture eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFacture)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOFacture fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFacture eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFacture ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOFacture fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
