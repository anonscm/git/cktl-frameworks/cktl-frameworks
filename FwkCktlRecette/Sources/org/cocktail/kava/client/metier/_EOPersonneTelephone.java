// _EOPersonneTelephone.java
/*
 * Copyright Cocktail, 2001-2011
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersonneTelephone.java instead.
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPersonneTelephone extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PersonneTelephone";
	public static final String ENTITY_TABLE_NAME = "grhum.personne_telephone";



	// Attributes


	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICATIF_KEY = "indicatif";
	public static final String LISTE_ROUGE_KEY = "listeRouge";
	public static final String NO_TELEPHONE_KEY = "noTelephone";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

//Colonnes dans la base de donnees
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_DEB_VAL_COLKEY = "D_DEB_VAL";
	public static final String D_FIN_VAL_COLKEY = "D_FIN_VAL";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INDICATIF_COLKEY = "INDICATIF";
	public static final String LISTE_ROUGE_COLKEY = "LISTE_ROUGE";
	public static final String NO_TELEPHONE_COLKEY = "NO_TELEPHONE";

	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYPE_NO_COLKEY = "TYPE_NO";
	public static final String TYPE_TEL_COLKEY = "TYPE_TEL";


	// Relationships
	public static final String TYPE_NO_TEL_KEY = "typeNoTel";
	public static final String TYPE_TEL_RELATIONSHIP_KEY = "typeTelRelationship";



	// Accessors methods
  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey(D_DEB_VAL_KEY);
  }

  public void setDDebVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_DEB_VAL_KEY);
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey(D_FIN_VAL_KEY);
  }

  public void setDFinVal(NSTimestamp value) {
    takeStoredValueForKey(value, D_FIN_VAL_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer indicatif() {
    return (Integer) storedValueForKey(INDICATIF_KEY);
  }

  public void setIndicatif(Integer value) {
    takeStoredValueForKey(value, INDICATIF_KEY);
  }

  public String listeRouge() {
    return (String) storedValueForKey(LISTE_ROUGE_KEY);
  }

  public void setListeRouge(String value) {
    takeStoredValueForKey(value, LISTE_ROUGE_KEY);
  }

  public String noTelephone() {
    return (String) storedValueForKey(NO_TELEPHONE_KEY);
  }

  public void setNoTelephone(String value) {
    takeStoredValueForKey(value, NO_TELEPHONE_KEY);
  }

  public org.cocktail.kava.client.metier.EOTypeNoTel typeNoTel() {
    return (org.cocktail.kava.client.metier.EOTypeNoTel)storedValueForKey(TYPE_NO_TEL_KEY);
  }

  public void setTypeNoTelRelationship(org.cocktail.kava.client.metier.EOTypeNoTel value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeNoTel oldValue = typeNoTel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_NO_TEL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_NO_TEL_KEY);
    }
  }

  public org.cocktail.kava.client.metier.EOTypeTel typeTelRelationship() {
    return (org.cocktail.kava.client.metier.EOTypeTel)storedValueForKey(TYPE_TEL_RELATIONSHIP_KEY);
  }

  public void setTypeTelRelationshipRelationship(org.cocktail.kava.client.metier.EOTypeTel value) {
    if (value == null) {
    	org.cocktail.kava.client.metier.EOTypeTel oldValue = typeTelRelationship();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_TEL_RELATIONSHIP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_TEL_RELATIONSHIP_KEY);
    }
  }


  public static EOPersonneTelephone createPersonneTelephone(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOPersonneTelephone eo = (EOPersonneTelephone) createAndInsertInstance(editingContext, _EOPersonneTelephone.ENTITY_NAME);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOPersonneTelephone.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOPersonneTelephone.fetch(editingContext, null, sortOrderings);
//  }


	
		/**
		 * Cree une instance de l'objet et l'insere dans l'editing context.
		 * @param editingContext
		 *
		 * @return L'objet insere dans l'editing context.
		 */
		  public static EOPersonneTelephone creerInstance(EOEditingContext editingContext) {
		  		EOPersonneTelephone object = (EOPersonneTelephone)createAndInsertInstance(editingContext, _EOPersonneTelephone.ENTITY_NAME);
		  		return object;
			}


	
  	  public EOPersonneTelephone localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersonneTelephone)localInstanceOfObject(editingContext, this);
	  }
	
  public static EOPersonneTelephone localInstanceIn(EOEditingContext editingContext, EOPersonneTelephone eo) {
    EOPersonneTelephone localInstance = (eo == null) ? null : (EOPersonneTelephone)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   *
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOPersonneTelephone#localInstanceIn a la place.
   */
	public static EOPersonneTelephone localInstanceOf(EOEditingContext editingContext, EOPersonneTelephone eo) {
		return EOPersonneTelephone.localInstanceIn(editingContext, eo);
	}




	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}


	public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		if (eoenterpriseobject == null) {
			return null;
		}

		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		else if (eoeditingcontext1.equals(eoeditingcontext)) {
			return eoenterpriseobject;
		}
		com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
		return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

	}

	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		*
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes
		*/
	  public static EOPersonneTelephone fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }


	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   *
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOPersonneTelephone fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersonneTelephone eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersonneTelephone)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }




	  public static EOPersonneTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }

	  public static EOPersonneTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersonneTelephone eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersonneTelephone)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }


	  /**
	   *
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOPersonneTelephone fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersonneTelephone eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersonneTelephone ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }


	public static EOPersonneTelephone fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}



}
