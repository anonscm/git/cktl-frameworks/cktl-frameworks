
// EOAdresse.java
//
package org.cocktail.kava.client.metier;

import com.webobjects.foundation.NSValidation;

public class EOAdresse extends _EOAdresse {

	public static final String	PRIMARY_KEY_KEY	= "adrOrdre";

	public EOAdresse() {
		super();
	}

	public String toInlineString() {
		StringBuilder toStringBuilder = new StringBuilder("(");
		if (adrAdresse1() != null && adrAdresse1().length() > 0) {
			toStringBuilder.append(adrAdresse1());
		}
		if (adrAdresse2() != null && adrAdresse2().length() > 0) {
			toStringBuilder.append(" - ").append(adrAdresse2());
		}
		if (codePostal() != null && codePostal().length() > 0) {
			toStringBuilder.append(" - ").append(codePostal());
		}
		if (ville() != null && ville().length() > 0) {
			toStringBuilder.append(" ").append(ville());
		}
		toStringBuilder.append(")");
		return toStringBuilder.toString();
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
