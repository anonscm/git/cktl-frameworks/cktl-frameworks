package org.cocktail.kava.client;

import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

public class PieFwkUtilities {

	public static String datToString(NSTimestamp date, String format) {
		String aString = null;
		try {
			NSTimestampFormatter formatter = new NSTimestampFormatter(format);
			aString = formatter.format(date);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return aString;
	}

	public static String capitalizedString(String aString) {
		if ("".equals(aString))
			return "";

		String debut = (aString.substring(0, 1)).toUpperCase();
		String fin = (aString.substring(1, aString.length())).toLowerCase();

		return debut.concat(fin);
	}

	public static boolean isEmpty(String str) {
		return ((str == null) || (str.length() == 0) || ("*nil*".equals(str)));
	}
}
