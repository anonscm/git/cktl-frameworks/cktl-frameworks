package org.cocktail.kava.client.service;

import java.io.Serializable;
import java.util.Iterator;

import org.cocktail.kava.client.finder.FinderFournisseurAdresse;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EORepartPersonneAdresse;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class PersonneService implements Serializable {

	/** Serial version id. */
	private static final long serialVersionUID = 1L;

	/** Singleton. */
	private static final PersonneService INSTANCE = new PersonneService();

	/**
	 * Singleton.
	 * @return service singleton.
	 */
	public static final PersonneService instance() {
		return INSTANCE;
	}

	/**
	 * Constructeur prive.
	 */
	private PersonneService() {
	}

	/**
	 * Return the default address.
	 * @return default address.
	 */
	public EORepartPersonneAdresse defaultAdress(EOEditingContext ec, EOPersonne personne) {
		if (personne == null) {
			return null;
		}

		EORepartPersonneAdresse defaultAdress = null;
		NSArray repartPersonneAdresses = personne.repartPersonneAdresses();
		NSArray fournisUlr = personne.fournisUlrs();
		if (fournisUlr != null && fournisUlr.count() == 1) {
			EOAdresse fournisAdress =
					FinderFournisseurAdresse.findAdresse(ec, ((EOFournisUlr) fournisUlr.objectAtIndex(0)));
			defaultAdress = findMatch(repartPersonneAdresses, fournisAdress);
		}
		else if (repartPersonneAdresses != null && repartPersonneAdresses.count() > 0) {
			EOQualifier factQual = Qualifiers.getQualifierFactory(
					QualifierKey.REPART_PRS_ADR_TYPE).build(
					EOQualifier.QualifierOperatorEqual, EORepartPersonneAdresse.FACTURATION_TYPE_ADRESSE);
			NSArray facturationAdresses = EOQualifier.filteredArrayWithQualifier(repartPersonneAdresses, factQual);
			if (facturationAdresses.count() == 1) {
				defaultAdress = (EORepartPersonneAdresse) facturationAdresses.objectAtIndex(0);
			}
			else if (facturationAdresses.count() > 1) {
				NSArray factPrincipaleAdresses = selectAdressePrincipale(facturationAdresses);
				if (factPrincipaleAdresses.count() == 1) {
					defaultAdress = (EORepartPersonneAdresse) facturationAdresses.objectAtIndex(0);
				}
				else {
					NSArray currentAdresses = factPrincipaleAdresses.count() == 0
							? facturationAdresses : factPrincipaleAdresses;
					defaultAdress = selectMostRecent(currentAdresses);
				}
			}
			else {
				// no 'facturation'
				NSArray principaleAdresses = selectAdressePrincipale(repartPersonneAdresses);
				if (principaleAdresses.count() == 1) {
					defaultAdress = (EORepartPersonneAdresse) principaleAdresses.objectAtIndex(0);
				}
				else {
					NSArray currentAdresses = principaleAdresses.count() == 0
							? repartPersonneAdresses : principaleAdresses;
					defaultAdress = selectMostRecent(currentAdresses);
				}
			}
		}
		return defaultAdress;
	}

	/**
	 * Find matching repartPersonneAdress.
	 *
	 * @param rptAdresses list of repartPersonneAdress.
	 * @param adresse eoAdresse to find.
	 * @return repartPersonneAdresse matching the adresse speciefied ; null if no match found.
	 */
	private EORepartPersonneAdresse findMatch(NSArray rptAdresses, EOAdresse adresse) {
		EORepartPersonneAdresse resultRptAdresse = null;
		for (int idx = 0; idx < rptAdresses.count(); idx++) {
			EORepartPersonneAdresse currentRptAdr = (EORepartPersonneAdresse) rptAdresses.objectAtIndex(idx);
			if (currentRptAdr.adresse().equals(adresse)) {
				resultRptAdresse = currentRptAdr;
				break;
			}
		}
		return resultRptAdresse;
	}

	/**
	 * Select 'principale' addresses.
	 *
	 * @param repartPrsAdresses addresses.
	 * @return an array containing only 'principale' addresses.
	 */
	private NSArray selectAdressePrincipale(NSArray repartPrsAdresses) {
		EOQualifier principaleQual = Qualifiers.getQualifierFactory(
				QualifierKey.REPART_PRS_ADR_PRINCIPALE).build(
				EOQualifier.QualifierOperatorEqual, EORepartPersonneAdresse.ADRESSE_PRINCIPALE_OUI);
		return EOQualifier.filteredArrayWithQualifier(repartPrsAdresses, principaleQual);
	}

	/**
	 * Select the most recent created address.
	 *
	 * @param repartPrsAdresses addresses.
	 * @return the most recent address.
	 */
	private EORepartPersonneAdresse selectMostRecent(NSArray repartPrsAdresses) {
		Iterator iteAdr = repartPrsAdresses.iterator();
		EORepartPersonneAdresse mostRecentRepartAdresse = (EORepartPersonneAdresse) iteAdr.next();
		while (iteAdr.hasNext()) {
			EORepartPersonneAdresse currentRepartAdresse = (EORepartPersonneAdresse) iteAdr.next();
			NSTimestamp currentRptAdrCreation = currentRepartAdresse.adresse().dCreation();
			if (currentRptAdrCreation != null
					&& currentRptAdrCreation.after(mostRecentRepartAdresse.adresse().dCreation())) {
				mostRecentRepartAdresse = currentRepartAdresse;
			}
		}
		return mostRecentRepartAdresse;
	}

}
