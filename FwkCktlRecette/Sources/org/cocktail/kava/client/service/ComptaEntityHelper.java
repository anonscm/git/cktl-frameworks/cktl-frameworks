package org.cocktail.kava.client.service;

import org.cocktail.fwkcktlcompta.client.metier.EOGrhumAdresse;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumPersonne;
import org.cocktail.fwkcktlcompta.client.metier.EOGrhumRib;
import org.cocktail.fwkcktlcompta.client.metier.EOJefyRecetteFacturePapier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddOrigineEntity;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFacturePapierAdrClient;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.remoteCalls.ServerCallRecette;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class ComptaEntityHelper {

	public static ISepaSddOrigineEntity toFacturePapier(EOEditingContext edc, EOFacturePapier facturePapier) {
		if (facturePapier == null) {
			return null;
		}

		EOJefyRecetteFacturePapier facturePapierOrigineEntity = null;
		Integer facturePapierId = null;
		NSDictionary remoteCallResults = ServerCallRecette.serverPrimaryKeyForObject(edc, facturePapier);
		if (remoteCallResults != null) {
			facturePapierId = (Integer) remoteCallResults.valueForKey(EOFacturePapier.FAP_ID_KEY);
			facturePapierOrigineEntity = EOJefyRecetteFacturePapier.fetchByKeyValue(edc, EOJefyRecetteFacturePapier.FAP_ID_KEY, facturePapierId);
		}

		return facturePapierOrigineEntity;
	}

	public static EOGrhumPersonne toGrhumPersonne(EOEditingContext edc, EOPersonne personne) {
		if (personne == null) {
			return null;
		}

		EOGrhumPersonne grhumPersonne = null;
		Integer grhumPersonneId = null;
		NSDictionary remoteCallResults = ServerCallRecette.serverPrimaryKeyForObject(edc, personne);
		if (remoteCallResults != null) {
			grhumPersonneId = (Integer) remoteCallResults.valueForKey(EOPersonne.PERS_ID_KEY);
			grhumPersonne = EOGrhumPersonne.fetchByKeyValue(edc, EOGrhumPersonne.PERS_ID_KEY, grhumPersonneId);
		}

		return grhumPersonne;
	}

	public static EOGrhumAdresse toGrhumAdresse(EOEditingContext edc, EOFacturePapierAdrClient facturePapierAdresseClient) {
		if (facturePapierAdresseClient == null || facturePapierAdresseClient.toAdresse() == null) {
			return null;
		}

		EOGrhumAdresse ghrumAdresse = null;
		Integer ghrumAdresseId = null;
		NSDictionary remoteCallResults = ServerCallRecette.serverPrimaryKeyForObject(edc, facturePapierAdresseClient.toAdresse());
		if (remoteCallResults != null) {
			ghrumAdresseId = (Integer) remoteCallResults.valueForKey(EOAdresse.ADR_ORDRE_KEY);
			ghrumAdresse = EOGrhumAdresse.fetchByKeyValue(edc, EOAdresse.ADR_ORDRE_KEY, ghrumAdresseId);
		}

		return ghrumAdresse;
	}

	public static EOGrhumRib toGrhumRib(EOEditingContext edc, EORibfourUlr ribFourUlr) {
		if (ribFourUlr == null) {
			return null;
		}

		EOGrhumRib grhumRib = null;
		Integer grhumRibId = null;
		NSDictionary remoteCallResults = ServerCallRecette.serverPrimaryKeyForObject(edc, ribFourUlr);
		if (remoteCallResults != null) {
			grhumRibId = (Integer) remoteCallResults.valueForKey(EORibfourUlr.RIB_ORDRE_KEY);
			grhumRib = EOGrhumRib.fetchByKeyValue(edc, EORibfourUlr.RIB_ORDRE_KEY, grhumRibId);
		}

		return grhumRib;
	}
}
