/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOCodeAnalytique;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOGestion;
import org.cocktail.kava.client.metier.EOLolfNomenclatureRecette;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOOrgan;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecetteCtrlAction;
import org.cocktail.kava.client.metier.EORecetteCtrlAnalytique;
import org.cocktail.kava.client.metier.EORecetteCtrlConvention;
import org.cocktail.kava.client.metier.EORecetteCtrlPlanco;
import org.cocktail.kava.client.metier.EORecetteCtrlPlancoCtp;
import org.cocktail.kava.client.metier.EORecetteCtrlPlancoTva;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORecettePapierAdrClient;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.metier.EOTauxProrata;
import org.cocktail.kava.client.metier.EOTypeApplication;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class InsFactureRecette {

	/**
	 * Appele la procedure de creation de la facture-recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static NSDictionary save(EOEditingContext ec, EORecette recette) throws Exception {
		if (recette == null) {
			throw new Exception("Facture-Recette a enregistrer null!!");
		}
		recette.validateObjectMetierWithFacture();
		NSDictionary dico = ServerProxy.apiInsFactureRecette(ec, dico(ec, recette));
		return dico;
	}

	/**
	 * Appele la procedure de creation de la facture-recette-adresse<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static NSDictionary saveWithAddress(EOEditingContext ec, EORecette recette) throws Exception {
		if (recette == null) {
			throw new Exception("Facture-Recette-Adresse a enregistrer null!!");
		}
		recette.validateObjectMetierWithFacture();
		NSDictionary dico = ServerProxy.apiInsFactureRecetteAdresse(ec, dico(ec, recette));
		return dico;
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'objet<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	protected static NSDictionary dico(EOEditingContext ec, EORecette recette) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met le numero a null, il sera genere dans la procedure
		dico.takeValueForKey(null, "010aFacId");

		// on met le numero a null, il sera genere dans la procedure
		dico.takeValueForKey(null, "020aRecId");

		if (recette.recettePapier() != null) {
			try {
				dicoForPrimaryKeys = ServerProxy.primaryKeyForGlobalID(ec, recette.recettePapier());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORecettePapier.PRIMARY_KEY_KEY), "025aRppId");
			}
			catch (Exception e) {
				dico.takeValueForKey(null, "025aRppId");
			}
		}
		else {
			dico.takeValueForKey(null, "025aRppId");
		}

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "030aExeOrdre");

		dico.takeValueForKey(recette.facture().facNumero(), "040aFacNumero");

		dico.takeValueForKey(recette.recNumero(), "050aRecNumero");

		if (recette.facture().fournisUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.facture().fournisUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.PRIMARY_KEY_KEY), "060aFouOrdre");
		}
		else {
			dico.takeValueForKey(null, "060aFouOrdre");
		}

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.facture().personne());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY), "070aPersId");

		dico.takeValueForKey(recette.recLib(), "080aFacLib");

		if (recette.recettePapier() != null && recette.recettePapier().ribfourUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.recettePapier().ribfourUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORibfourUlr.PRIMARY_KEY_KEY), "085aRibOrdre");
		}
		else {
			dico.takeValueForKey(null, "085aRibOrdre");
		}

		if (recette.recettePapier() != null && recette.recettePapier().modeRecouvrement() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.recettePapier().modeRecouvrement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModeRecouvrement.PRIMARY_KEY_KEY), "090aMorOrdre");
		}
		else {
			dico.takeValueForKey(null, "090aMorOrdre");
		}

		dico.takeValueForKey(recette.recettePapier().rppNbPiece(), "095aRppNbPiece");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.facture().organ());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.PRIMARY_KEY_KEY), "100aOrgId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.facture().typeCreditRec());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.PRIMARY_KEY_KEY), "110aTcdOrdre");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.tauxProrata());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.PRIMARY_KEY_KEY), "120aTapId");

		dico.takeValueForKey(recette.recHtSaisie(), "130aFacHtSaisie");
		dico.takeValueForKey(recette.recTtcSaisie(), "140aFacTtcSaisie");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.facture().typeApplication());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeApplication.PRIMARY_KEY_KEY), "150aTyapId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "160aUtlOrdre");

		dico.takeValueForKey(chaineCtrlAction(ec, recette.recetteCtrlActions()), "170aChaineAction");
		dico.takeValueForKey(chaineCtrlAnalytique(ec, recette.recetteCtrlAnalytiques()), "180aChaineAnalytique");
		dico.takeValueForKey(chaineCtrlConvention(ec, recette.recetteCtrlConventions()), "190aChaineConvention");
		dico.takeValueForKey(chaineCtrlPlanco(ec, recette.recetteCtrlPlancos()), "200aChainePlanco");
		dico.takeValueForKey(chaineCtrlPlancoTva(ec, recette.recetteCtrlPlancos()), "210aChainePlancoTva");
		dico.takeValueForKey(chaineCtrlPlancoCtp(ec, recette.recetteCtrlPlancos()), "220aChainePlancoCtp");

		dico.addEntriesFromDictionary(createRppAdrClientDictionnary(ec, recette));

		return dico;
	}

	/**
	 *
	 * @param ec       editing context.
	 * @param recette  recette.
	 * @return dico.
	 */
	private static NSDictionary createRppAdrClientDictionnary(EOEditingContext ec, EORecette recette) {
		NSDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;
		Object rppAdcIdAdrOrdre = null;
		Object rppAdcIdPersIdCreation = null;

		if (recette.recettePapier() != null) {
			EORecettePapierAdrClient currentAdresse = recette.recettePapier().currentRecPapierAdresseClient();
			if (currentAdresse != null) {
				try {
					dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, currentAdresse.toAdresse());
					rppAdcIdAdrOrdre = dicoForPrimaryKeys.objectForKey(EOAdresse.PRIMARY_KEY_KEY);

					dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, currentAdresse.toPersonneCreation());
					rppAdcIdPersIdCreation = dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY);
				} catch (Exception e) {
					// TODO add real logs
				}
			}
		}

		dico.takeValueForKey(rppAdcIdAdrOrdre, "230aRppAdcAdrOrdre");
		dico.takeValueForKey(rppAdcIdPersIdCreation, "240aRppAdcPersId");

		return dico;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlAction de la recette<BR>
	 * Format de la chaine : lolf_id$ract_ht_saisie$ract_ttc_saisie$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlAction a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlAction(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlAction recetteCtrlAction = (EORecetteCtrlAction) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlAction.lolfNomenclatureRecette());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOLolfNomenclatureRecette.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + recetteCtrlAction.ractHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + recetteCtrlAction.ractTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlAnalytique de la recette<BR>
	 * Format de la chaine : can_id$rana_ht_saisie$rana_ttc_saisie$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlAnalytique a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlAnalytique(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlAnalytique recetteCtrlAnalytique = (EORecetteCtrlAnalytique) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlAnalytique.codeAnalytique());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOCodeAnalytique.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + recetteCtrlAnalytique.ranaHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + recetteCtrlAnalytique.ranaTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlConvention de la recette<BR>
	 * Format de la chaine : con_ordre$rcon_ht_saisie$rcon_ttc_saisie$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlConvention a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlConvention(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlConvention recetteCtrlConvention = (EORecetteCtrlConvention) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlConvention.convention());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOConvention.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + recetteCtrlConvention.rconHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + recetteCtrlConvention.rconTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlPlanco de la recette<BR>
	 * Format de la chaine : pco_num$rpco_ht_saisie$rpco_ttc_saisie$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlPlanco a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlPlanco(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlPlanco recetteCtrlPlanco = (EORecetteCtrlPlanco) a.objectAtIndex(i);

			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlanco.planComptable());
			chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY) + "$";

			chaine = chaine + recetteCtrlPlanco.rpcoHtSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
			chaine = chaine + recetteCtrlPlanco.rpcoTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlPlancoTva de la recette<BR>
	 * Format de la chaine : pco_num_pere$pco_num$rpcotva_tva_saisie$ges_code$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlPlanco a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlPlancoTva(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";

		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlPlanco recetteCtrlPlanco = (EORecetteCtrlPlanco) a.objectAtIndex(i);
			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlanco.planComptable());
			String pcoNumPere = dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY).toString();
			for (int j = 0; j < recetteCtrlPlanco.recetteCtrlPlancoTvas().count(); j++) {
				EORecetteCtrlPlancoTva recetteCtrlPlancoTva = (EORecetteCtrlPlancoTva) recetteCtrlPlanco.recetteCtrlPlancoTvas()
						.objectAtIndex(j);

				chaine = chaine + pcoNumPere + "$";
				dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlancoTva.planComptable());
				chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY) + "$";
				chaine = chaine + recetteCtrlPlancoTva.rpcotvaTvaSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
				if (recetteCtrlPlancoTva.gestion() != null) {
					dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlancoTva.gestion());
					chaine = chaine + dicoForPrimaryKeys.objectForKey(EOGestion.PRIMARY_KEY_KEY) + "$";
				}
				else {
					chaine = chaine + "$";
				}
			}
		}
		chaine = chaine + "$";
		return chaine;
	}

	/**
	 * Construit la chaine de caracteres relative aux EORecetteCtrlPlancoCtp de la recette<BR>
	 * Format de la chaine : pco_num_pere$pco_num$rpcoctp_ttc_saisie$ges_code$...$ <BR>
	 *
	 * @param a
	 *            EORecetteCtrlPlanco a partir desquels on travaille
	 * @return un String contenant la chaine a passer en argument de la procedure
	 */
	protected static String chaineCtrlPlancoCtp(EOEditingContext ec, NSArray a) {
		if (a == null) {
			return "$";
		}
		String chaine = "";
		for (int i = 0; i < a.count(); i++) {
			EORecetteCtrlPlanco recetteCtrlPlanco = (EORecetteCtrlPlanco) a.objectAtIndex(i);
			NSDictionary dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlanco.planComptable());
			String pcoNumPere = dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY).toString();
			for (int j = 0; j < recetteCtrlPlanco.recetteCtrlPlancoCtps().count(); j++) {
				EORecetteCtrlPlancoCtp recetteCtrlPlancoCtp = (EORecetteCtrlPlancoCtp) recetteCtrlPlanco.recetteCtrlPlancoCtps()
						.objectAtIndex(j);

				chaine = chaine + pcoNumPere + "$";
				dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlancoCtp.planComptable());
				chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPlanComptable.PRIMARY_KEY_KEY) + "$";
				chaine = chaine + recetteCtrlPlancoCtp.rpcoctpTtcSaisie().setScale(2, BigDecimal.ROUND_HALF_UP) + "$";
				if (recetteCtrlPlancoCtp.gestion() != null) {
					dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recetteCtrlPlancoCtp.gestion());
					chaine = chaine + dicoForPrimaryKeys.objectForKey(EOGestion.PRIMARY_KEY_KEY) + "$";
				}
				else {
					chaine = chaine + "$";
				}
			}
		}
		chaine = chaine + "$";
		return chaine;
	}

}