/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOModeRecouvrement;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORibfourUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class InsRecettePapier {

	/**
	 * Appele la procedure de creation de la recette papier<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecettePapier qui sera enregistree
	 */
	public static void save(EOEditingContext ec, EORecettePapier recettePapier) throws Exception {
		if (recettePapier == null) {
			throw new Exception("Recette a enregistrer null!!");
		}
		recettePapier.validateObjectMetier();
		ServerProxy.apiInsRecettePapier(ec, dico(ec, recettePapier));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'objet<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecette pour lequel construire le dictionnaire
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	protected static NSDictionary dico(EOEditingContext ec, EORecettePapier recettePapier) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		// on met la cle de la table a null, elle sera generee dans la procedure
		dico.takeValueForKey(null, "010aRppId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "020aExeOrdre");

		dico.takeValueForKey(recettePapier.rppNumero(), "030aRppNumero");

		dico.takeValueForKey(recettePapier.rppHtSaisie(), "040aRppHtSaisie");
		dico.takeValueForKey(recettePapier.rppTtcSaisie(), "050aRppTtcSaisie");

		if (recettePapier.personne() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.personne());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY), "060aPersId");
		}
		else {
			dico.takeValueForKey(null, "060aPersId");
		}

		if (recettePapier.fournisUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.fournisUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.PRIMARY_KEY_KEY), "070aFouOrdre");
		}
		else {
			dico.takeValueForKey(null, "070aFouOrdre");
		}

		if (recettePapier.ribfourUlr() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.ribfourUlr());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORibfourUlr.PRIMARY_KEY_KEY), "080aRibOrdre");
		}
		else {
			dico.takeValueForKey(null, "080aRibOrdre");
		}

		if (recettePapier.modeRecouvrement() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.modeRecouvrement());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModeRecouvrement.PRIMARY_KEY_KEY), "090aMorOrdre");
		}
		else {
			dico.takeValueForKey(null, "090aMorOrdre");
		}

		dico.takeValueForKey(recettePapier.rppDateRecette(), "100aRppDateRecette");
		dico.takeValueForKey(recettePapier.rppDateReception(), "110aRppDateReception");
		dico.takeValueForKey(recettePapier.rppDateServiceFait(), "120aRppDateServiceFait");
		dico.takeValueForKey(recettePapier.rppNbPiece(), "130aRppNbPiece");

		if (recettePapier.utilisateur() != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recettePapier.utilisateur());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "140aUtlOrdre");
		}
		else {
			dico.takeValueForKey(null, "140aUtlOrdre");
		}
		return dico;
	}

}