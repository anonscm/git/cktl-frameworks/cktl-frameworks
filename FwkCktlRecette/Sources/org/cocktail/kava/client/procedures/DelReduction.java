/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class DelReduction {

	/**
	 * Appele la procedure de suppression de la reduction<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void save(EOEditingContext ec, EORecette recette, EOUtilisateur utilisateur) throws Exception {
		if (recette == null) {
			throw new Exception("Recette a supprimer null!!");
		}
		ServerProxy.apiDelReduction(ec, dico(ec, recette, utilisateur));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure a partir de l'objet<BR>
	 * 
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette pour lequel construire le dictionnaire
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 * @return une instance du NSDictionary pret a l'emploi
	 */
	protected static NSDictionary dico(EOEditingContext ec, EORecette recette, EOUtilisateur utilisateur) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, recette);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORecette.PRIMARY_KEY_KEY), "010aRecId");

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, utilisateur);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "020aUtlOrdre");

		return dico;
	}

}