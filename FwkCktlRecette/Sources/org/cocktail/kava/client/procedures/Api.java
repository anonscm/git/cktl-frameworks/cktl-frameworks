/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class Api {

	/**
	 * Appele la procedure de creation de la facture<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param facture
	 *            EOFacture qui sera enregistre
	 */
	public static void insFacture(EOEditingContext ec, EOFacture facture) throws Exception {
		InsFacture.save(ec, facture);
	}

	/**
	 * Appele la procedure de modification de la facture<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param facture
	 *            EOFacture qui sera enregistre
	 * @param facId
	 *            cle de la facture a modifier
	 */
	public static void updFacture(EOEditingContext ec, EOFacture facture, Number facId) throws Exception {
		UpdFacture.save(ec, facture, facId);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facture)));
	}

	/**
	 * Appele la procedure de suppression de la facture<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param facture
	 *            EOFacture qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void delFacture(EOEditingContext ec, EOFacture facture, EOUtilisateur utilisateur) throws Exception {
		DelFacture.save(ec, facture, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(facture)));
	}

	/**
	 * Appele la procedure de creation de la recette papier<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecettePapier qui sera enregistree
	 */
	public static void insRecettePapier(EOEditingContext ec, EORecettePapier recettePapier) throws Exception {
		InsRecettePapier.save(ec, recettePapier);
	}

	/**
	 * Appele la procedure de modification de la recette papier<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecettePapier qui sera modifiee
	 * @param rppId
	 *            cle de la recette papier a modifier
	 */
	public static void updRecettePapier(EOEditingContext ec, EORecettePapier recettePapier, Number rppId) throws Exception {
		UpdRecettePapier.save(ec, recettePapier, rppId);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recettePapier)));
	}

	/**
	 * Appele la procedure de suppression de la recette papier<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recettePapier
	 *            EORecettePapier qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void delRecettePapier(EOEditingContext ec, EORecettePapier recettePapier, EOUtilisateur utilisateur) throws Exception {
		DelRecettePapier.save(ec, recettePapier, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recettePapier)));
	}

	/**
	 * Appele la procedure de creation de la recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static void insRecette(EOEditingContext ec, EORecette recette) throws Exception {
		InsRecette.save(ec, recette);
	}

	/**
	 * Appele la procedure de suppression de la recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void delRecette(EOEditingContext ec, EORecette recette, EOUtilisateur utilisateur) throws Exception {
		DelRecette.save(ec, recette, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette)));
	}

	/**
	 * Appele la procedure de creation de la facture-recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static NSDictionary insFactureRecette(EOEditingContext ec, EORecette recette) throws Exception {
		NSDictionary dico = InsFactureRecette.save(ec, recette);
		return dico;
	}

	/**
	 * Appele la procedure de creation de la facture-recette<BR>
	 *
	 * @param ec        EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette   EORecette qui sera enregistre
	 */
	public static NSDictionary insFactureRecetteAdresse(EOEditingContext ec, EORecette recette) throws Exception {
		NSDictionary dico = InsFactureRecette.saveWithAddress(ec, recette);
		return dico;
	}

	/**
	 * Appele la procedure de modification de la facture-recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera modifiee
	 * @param recId
	 *            cle de la recette a modifier
	 */
	public static void updFactureRecette(EOEditingContext ec, EORecette recette, Number recId) throws Exception {
		UpdFactureRecette.save(ec, recette, recId);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.recettePapier())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.facture())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette)));
	}

	/**
	 * Appele la procedure de modification de la facture-recette-adresse<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera modifiee
	 * @param recId
	 *            cle de la recette a modifier
	 */
	public static void updFactureRecetteAdresse(EOEditingContext ec, EORecette recette, Number recId) throws Exception {
		UpdFactureRecette.saveWithAddress(ec, recette, recId);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.recettePapier())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.facture())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette)));
	}

	/**
	 * Appele la procedure de suppression de la facture-recette<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void delFactureRecette(EOEditingContext ec, EORecette recette, EOUtilisateur utilisateur) throws Exception {
		DelFactureRecette.save(ec, recette, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.recettePapier())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette.facture())));
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette)));
	}

	/**
	 * Appele la procedure de creation de la reduction<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static NSDictionary insReduction(EOEditingContext ec, EORecette recette) throws Exception {
		NSDictionary dico = InsReduction.save(ec, recette);
		return dico;
	}

	/**
	 * Appele la procedure de creation de la reduction<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera enregistre
	 */
	public static NSDictionary insReductionAdresse(EOEditingContext ec, EORecette recette) throws Exception {
		NSDictionary dico = InsReduction.saveWithAdress(ec, recette);
		return dico;
	}

	/**
	 * Appele la procedure de suppression de la reduction<BR>
	 *
	 * @param ec
	 *            EOEditingContext servant a recuperer les cles primaires des relations
	 * @param recette
	 *            EORecette qui sera supprimee
	 * @param utilisateur
	 *            EOUtilisateur qui supprime
	 */
	public static void delReduction(EOEditingContext ec, EORecette recette, EOUtilisateur utilisateur) throws Exception {
		DelReduction.save(ec, recette, utilisateur);
		ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(recette)));
	}
}