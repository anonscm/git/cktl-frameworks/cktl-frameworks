/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import org.cocktail.application.client.eof.EOPays;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.finder.FinderFournisUlr;
import org.cocktail.kava.client.finder.FinderIndividuUlr;
import org.cocktail.kava.client.finder.FinderPersonne;
import org.cocktail.kava.client.finder.FinderStructureUlr;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOIndividuUlr;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class InsClient {

	private static final String PROCEDURE_NAME = "InsClient";

	/**
	 * Appelle la procedure de creation d'un client juste en mise a jour d'un enregistrement existant (personne existante)
	 * 
	 * @param ec
	 * @param personne
	 *            OBLIGATOIRE
	 * @param fournisUlr
	 *            facultatif
	 * @param typeClient
	 *            OBLIGATOIRE
	 * @param adresse
	 *            facultatif
	 * @param utilisateur
	 *            facultatif
	 * @return
	 * @throws Exception
	 */
	public static NSDictionary saveUpdate(EOEditingContext ec, EOPersonne personne, EOFournisUlr fournisUlr, String typeClient, EOAdresse adresse,
			EOUtilisateur utilisateur) throws Exception {
		return save(ec, personne, null, null, fournisUlr, typeClient, null, null, null, null, null, null, null, null, null, null, null, null, null,
				null, null, null, null, null, null, null, adresse, utilisateur);
	}

	/**
	 * Appele la procedure de creation d'un nouveau client<BR>
	 * 
	 * @param ec
	 * @param personne
	 *            Peut etre null
	 * @param structureUlr
	 *            Peut etre null
	 * @param individuUlr
	 *            Peut etre null
	 * @param fournisUlr
	 *            Peut etre null
	 * @param typeClient
	 *            "1" --> personne morale ; "2" --> personne physique
	 * @param cCivilite
	 *            Utilise si personne physique (voir table grhum.civilite)
	 * @param nom
	 *            Nom usuel si personne physique, raison sociale si personne morale
	 * @param prenom
	 *            Utilise si personne physique
	 * @param adresse1
	 * @param adresse2
	 * @param bp
	 * @param cp
	 * @param cpEtranger
	 * @param ville
	 * @param cPays
	 *            Code pays (voir table grhum.pays)
	 * @param tel
	 * @param fax
	 * @param login
	 * @param password
	 * @param email
	 * @param ribPays
	 * @param ribBanque
	 * @param ribGuichet
	 * @param ribCompte
	 * @param ribCle
	 * @param adresse
	 * @param utilisateur
	 * @return un dictionnaire avec les 4 EO crees/recuperes, cles : personne, structureUlr, individuUlr et fournisUlr personne et
	 *         fournisUlr toujours non null (normalement), et soit structureUlr (si personne morale) soit individuUlr (si personne physique)
	 * @throws Exception
	 */
	public static NSDictionary save(EOEditingContext ec, EOPersonne personne, EOStructureUlr structureUlr, EOIndividuUlr individuUlr,
			EOFournisUlr fournisUlr, String typeClient, String cCivilite, String nom, String prenom, String adresse1, String adresse2, String bp,
			String cp, String cpEtranger, String ville, EOPays pays, String tel, String fax, String login, String password, String email,
			String ribPays, String ribBanque, String ribGuichet, String ribCompte, String ribCle, EOAdresse adresse, EOUtilisateur utilisateur)
			throws Exception {

		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		if (personne != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, personne);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.PRIMARY_KEY_KEY), "010aPersId");
		}
		else {
			dico.takeValueForKey(null, "010aPersId");
		}
		if (structureUlr != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, structureUlr);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOStructureUlr.PRIMARY_KEY_KEY), "020aCStructure");
		}
		else {
			dico.takeValueForKey(null, "020aCStructure");
		}
		if (individuUlr != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, individuUlr);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOIndividuUlr.PRIMARY_KEY_KEY), "030aNoIndividu");
		}
		else {
			dico.takeValueForKey(null, "030aNoIndividu");
		}
		if (fournisUlr != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, fournisUlr);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.PRIMARY_KEY_KEY), "040aFouOrdre");
		}
		else {
			dico.takeValueForKey(null, "040aFouOrdre");
		}

		dico.takeValueForKey(typeClient, "050aTypeClient");
		dico.takeValueForKey(cCivilite, "060aCCivilite");
		dico.takeValueForKey(nom, "070aNom");
		dico.takeValueForKey(prenom, "080aPrenom");
		dico.takeValueForKey(adresse1, "090aAdresse1");
		dico.takeValueForKey(adresse2, "100aAdresse2");
		dico.takeValueForKey(bp, "110aBp");
		dico.takeValueForKey(cp, "120aCp");
		dico.takeValueForKey(cpEtranger, "130aCpEtranger");
		dico.takeValueForKey(ville, "140aVille");
		if (pays != null) {
			dico.takeValueForKey(pays.cPays(), "150aCPays");
		}
		else {
			dico.takeValueForKey(null, "150aCPays");
		}
		dico.takeValueForKey(tel, "160aTel");
		dico.takeValueForKey(fax, "170aFax");
		dico.takeValueForKey(login, "180aLogin");
		dico.takeValueForKey(password, "190aPassword");
		dico.takeValueForKey(email, "200aEmail");
		dico.takeValueForKey(ribPays, "210aRibPays");
		dico.takeValueForKey(ribBanque, "220aRibBanque");
		dico.takeValueForKey(ribGuichet, "230aRibGuichet");
		dico.takeValueForKey(ribCompte, "240aRibCompte");
		dico.takeValueForKey(ribCle, "250aRibCle");
		if (adresse != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, adresse);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOAdresse.PRIMARY_KEY_KEY), "260aAdrOrdre");
		}
		else {
			dico.takeValueForKey(null, "260aAdrOrdre");
		}
		if (utilisateur != null) {
			dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.PRIMARY_KEY_KEY), "270aUtlOrdre");
		}
		else {
			dico.takeValueForKey(null, "270aUtlOrdre");
		}

		NSDictionary returnedDico = ServerProxy.executeStoredProcedureNamed(ec, PROCEDURE_NAME, dico);
		dico.removeAllObjects();

		if (personne == null && returnedDico.valueForKey("010aPersId") != null) {
			dico.takeValueForKey(FinderPersonne.findByPrimaryKey(ec, returnedDico.valueForKey("010aPersId")), "personne");
		}
		else {
			if (personne != null) {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(personne)));
			}
			dico.takeValueForKey(personne, "personne");
		}
		if (structureUlr == null && returnedDico.valueForKey("020aCStructure") != null) {
			dico.takeValueForKey(FinderStructureUlr.findByPrimaryKey(ec, returnedDico.valueForKey("020aCStructure")), "structureUlr");
		}
		else {
			if (structureUlr != null) {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(structureUlr)));
			}
			dico.takeValueForKey(structureUlr, "structureUlr");
		}
		if (individuUlr == null && returnedDico.valueForKey("030aNoIndividu") != null) {
			dico.takeValueForKey(FinderIndividuUlr.findByPrimaryKey(ec, returnedDico.valueForKey("030aNoIndividu")), "individuUlr");
		}
		else {
			if (individuUlr != null) {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(individuUlr)));
			}
			dico.takeValueForKey(individuUlr, "individuUlr");
		}
		if (fournisUlr == null && returnedDico.valueForKey("040aFouOrdre") != null) {
			dico.takeValueForKey(FinderFournisUlr.findByPrimaryKey(ec, returnedDico.valueForKey("040aFouOrdre")), "fournisUlr");
		}
		else {
			if (fournisUlr != null) {
				ec.invalidateObjectsWithGlobalIDs(new NSArray(ec.globalIDForObject(fournisUlr)));
			}
			dico.takeValueForKey(fournisUlr, "fournisUlr");
		}

		return dico;
	}

}