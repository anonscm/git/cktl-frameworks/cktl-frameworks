/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.procedures;

import java.math.BigDecimal;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.ServerProxy;
import org.cocktail.kava.client.metier.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public class GetDisponible {

	private static final String	PROCEDURE_NAME	= "GetDisponible";

	public static BigDecimal get(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditDep) {
		NSMutableDictionary dico = new NSMutableDictionary();
		NSDictionary dicoForPrimaryKeys = null;

		if (exercice == null || organ == null || typeCreditDep == null) {
			return null;
		}

		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, exercice);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "010aExeOrdre");
		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, organ);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.PRIMARY_KEY_KEY), "020aOrgId");
		dicoForPrimaryKeys = ServerProxy.primaryKeyForObject(ec, typeCreditDep);
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.PRIMARY_KEY_KEY), "030aTcdOrdre");

		try {
			NSDictionary returnedDico = ServerProxy.executeStoredProcedureNamed(ec, PROCEDURE_NAME, dico);
			if (returnedDico != null) {
				return (BigDecimal) returnedDico.valueForKey("040aDisponible");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}