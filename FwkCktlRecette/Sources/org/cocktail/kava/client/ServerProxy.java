/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOAdresse;
import org.cocktail.kava.client.metier.EOCommandesPourPi;
import org.cocktail.kava.client.metier.EOFacturePapier;
import org.cocktail.kava.client.metier.EOFournisUlr;
import org.cocktail.kava.client.metier.EOGestion;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public class ServerProxy {

	// Procedures...
	// Prestations
	public static void regrouperPrestation(EOEditingContext ec, NSArray prestations, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestRegrouperPrestation", new Class[] {
						NSArray.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestations, utilisateur
				}, false);
	}

	public static void apiPrestationValidePrestationClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationValidePrestationClient", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationDevalidePrestationClient(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDevalidePrestationClient", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationValidePrestationPrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationValidePrestationPrestataire", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationDevalidePrestationPrestataire(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDevalidePrestationPrestataire",
				new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				}, new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationCloturePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationCloturePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationDecloturePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDecloturePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationChangePrestationDT(EOEditingContext ec, EOPrestation oldPrestation, EOPrestation newPrestation)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationChangePrestationDT", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						oldPrestation, newPrestation
				}, false);
	}

	public static void apiPrestationArchivePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationArchivePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationGenereFacturePapier(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationGenereFacturePapier", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationDelFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDelFacturePapier", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						facturePapier, utilisateur
				}, false);
	}

	public static NSDictionary apiPrestationPrestationFromCommande(EOEditingContext ec, EOCommandesPourPi commande, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationPrestationFromCommande", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class,
						EOEnterpriseObject.class, String.class
				}, new Object[] {
						commande, utilisateur, fournisUlrClient, pcoNum
				}, false);
	}

	public static NSDictionary apiPrestationFacturePapierFromCommande(EOEditingContext ec, EOCommandesPourPi commande, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationFacturePapierFromCommande", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class,
						EOEnterpriseObject.class, String.class
				}, new Object[] {
						commande, utilisateur, fournisUlrClient, pcoNum
				}, false);
	}

	public static void apiPrestationSoldePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationSoldePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur
				}, false);
	}

	public static void apiPrestationDupliquePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDupliquePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur, exerciceCible
				}, false);
	}

	public static void apiPrestationBasculePrestation(EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationBasculePrestation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						prestation, utilisateur, exerciceCible
				}, false);
	}

	public static void apiPrestationUpdFapRef(EOEditingContext ec, EOFacturePapier facturePapier) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationUpdFapRef", new Class[] {
					EOEnterpriseObject.class
				}, new Object[] {
					facturePapier
				}, false);
	}

	public static void apiPrestationInsPrestationAdrClient(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationInsPrestationAdrClient", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiPrestationInsFactureAdrClient(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationInsFactureAdrClient", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	// Factures papier
	public static void apiPrestationValideFacturePapierClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationValideFacturePapierClient", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						facturePapier, utilisateur
				}, false);
	}

	public static void apiPrestationDevalideFacturePapierClient(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDevalideFacturePapierClient", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						facturePapier, utilisateur
				}, false);
	}

	public static void apiPrestationValideFacturePapierPrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationValideFacturePapierPrest", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						facturePapier, utilisateur
				}, false);
	}

	public static void apiPrestationDevalideFacturePapierPrest(EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDevalideFacturePapierPrest", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class
				},
				new Object[] {
						facturePapier, utilisateur
				}, false);
	}

	public static NSDictionary apiPrestationDuplicateFacturePapier(EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur, EOFournisUlr fournisUlrClient) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDuplicateFacturePapier", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class,
						EOEnterpriseObject.class
				}, new Object[] {
						facturePapier, utilisateur, fournisUlrClient
				}, false);
	}

	public static NSDictionary apiPrestationDuplicateFacturePapierAdresse(
			EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur,
			EOFournisUlr fournisUlrClient, EOAdresse fournisUlrAdresse) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiPrestationDuplicateFacturePapierAdresse", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class,
						EOEnterpriseObject.class, EOEnterpriseObject.class
				}, new Object[] {
						facturePapier, utilisateur, fournisUlrClient, fournisUlrAdresse
				}, false);
	}

	// Facture
	public static void apiInsFacture(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiInsFacture",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiUpdFacture(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiUpdFacture",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiDelFacture(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiDelFacture",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	// Recette papier
	public static void apiInsRecettePapier(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiInsRecettePapier",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiUpdRecettePapier(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiUpdRecettePapier",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiDelRecettePapier(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiDelRecettePapier",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	// Recette
	public static void apiInsRecette(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiInsRecette",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiDelRecette(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiDelRecette",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	// FactureRecette
	public static NSDictionary apiInsFactureRecetteAdresse(EOEditingContext ec, NSDictionary dico) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiInsFactureRecetteAdresse", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static NSDictionary apiInsFactureRecette(EOEditingContext ec, NSDictionary dico) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiInsFactureRecette", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiUpdFactureRecette(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiUpdFactureRecette", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiUpdFactureRecetteAdresse(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiUpdFactureRecetteAdresse", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiDelFactureRecette(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiDelFactureRecette", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	// Reduction
	public static NSDictionary apiInsReduction(EOEditingContext ec, NSDictionary dico) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiInsReduction", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static NSDictionary apiInsReductionAdresse(EOEditingContext ec, NSDictionary dico) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestApiInsReductionAdresse", new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static void apiDelReduction(EOEditingContext ec, NSDictionary dico) throws Exception {
		((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk", "clientSideRequestApiDelReduction",
				new Class[] {
					NSDictionary.class
				}, new Object[] {
					dico
				}, false);
	}

	public static NSDictionary primaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestPrimaryKeyForGlobalID", new Class[] {
					EOGlobalID.class
				}, new Object[] {
					ec.globalIDForObject(eo)
				}, false);
	}

	public static NSDictionary primaryKeyForGlobalID(EOEditingContext ec, EOEnterpriseObject eo) {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestPrimaryKeyForGlobalID", new Class[] {
					EOGlobalID.class
				}, new Object[] {
					ec.globalIDForObject(eo)
				}, false);
	}

	public static Number getNumerotation(EOEditingContext ec, EOExercice exercice, EOGestion gestion, String tnuEntite) throws Exception {
		return (Number) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestGetNumerotation", new Class[] {
						EOEnterpriseObject.class, EOEnterpriseObject.class, String.class
				},
				new Object[] {
						exercice, gestion, tnuEntite
				}, false);
	}

	public static NSDictionary executeStoredProcedureNamed(EOEditingContext ec, String proc, NSDictionary dico) throws Exception {
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(ec, "session.sessionPieFwk",
				"clientSideRequestExecuteStoredProcedureNamed", new Class[] {
						String.class, NSDictionary.class
				}, new Object[] {
						proc, dico
				},
				false);
	}
}