/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.factory;

import org.cocktail.kava.client.finder.FinderPlanComptable;
import org.cocktail.kava.client.metier.EOCatalogueArticle;
import org.cocktail.kava.client.metier.EOPlanComptable;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EOPrestationLigne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

public class FactoryPrestationLigne extends Factory {

	private static final String COMPTE_INVALIDE =
			"Le compte d''imputation {0} est invalide pour l''exercice sélectionné.";

	public FactoryPrestationLigne() {
		super();
	}

	public FactoryPrestationLigne(boolean withLog) {
		super(withLog);
	}

	public static EOPrestationLigne newObject(EOEditingContext ec, EOPrestation prestation,
			EOCatalogueArticle catalogueArticle) {
		EOPrestationLigne object = createNewObject(ec, prestation, catalogueArticle);
		object.setPrestationRelationship(prestation);
		if (object.prestation() != null && "N".equalsIgnoreCase(object.prestation().prestApplyTva())) {
			object.setTvaRelationship(null);
			object.setPrligArtTtc(object.prligArtHt());
		}
		return object;
	}

	public static EOPrestationLigne newObject(EOEditingContext ec, EOPrestationLigne prestationLigne,
			EOCatalogueArticle catalogueArticle) {
		EOPrestation prestationParent = null;
		if (prestationLigne != null && prestationLigne.prestation() != null) {
			prestationParent = prestationLigne.prestation();
		}
		EOPrestationLigne object = createNewObject(ec, prestationParent, catalogueArticle);
		if (prestationLigne != null) {
			object.setPrestationLignePereRelationship(prestationLigne);
			object.setPrestationRelationship(prestationLigne.prestation());
			if (object.prestation() != null && "N".equalsIgnoreCase(object.prestation().prestApplyTva())) {
				object.setTvaRelationship(null);
				object.setPrligArtTtc(object.prligArtHt());
			}
			object.setPrligQuantite(prestationLigne.prligQuantite());
			object.setPrligQuantiteReste(prestationLigne.prligQuantiteReste());
		}
		return object;
	}

	private static EOPrestationLigne createNewObject(EOEditingContext ec, EOPrestation prestationParent,
			EOCatalogueArticle catalogueArticle) {
		EOPrestationLigne object = newObject(ec);
		object.setCatalogueArticleRelationship(catalogueArticle);
		if (catalogueArticle != null) {
			object.setPrligArtHt(catalogueArticle.caarPrixHt());
			object.setPrligArtTtc(catalogueArticle.caarPrixTtc());
			object.setPrligArtTtcInitial(catalogueArticle.caarPrixTtc());
			object.setTvaRelationship(catalogueArticle.tva());
			object.setTvaInitialRelationship(catalogueArticle.tva());
			object.setPrligDescription(catalogueArticle.article().artLibelle());
			object.setPrligReference(catalogueArticle.caarReference());
			object.setTypeArticleRelationship(catalogueArticle.article().typeArticle());
			EOPlanComptable compte = FinderPlanComptable.findOnlyValid(
					ec, prestationParent.exercice(), catalogueArticle.article().articlePrestation().pcoNumRecette());
			if (compte != null) {
				object.setPcoNum(compte.pcoNum());
			}
		}
		return object;
	}

	public static EOPrestationLigne newObject(EOEditingContext ec, EOPrestation prestation, EOPrestationLigne article) {
		EOPrestationLigne object = newObject(ec);

		object.setCatalogueArticleRelationship(article.catalogueArticle());
		object.setPrestationRelationship(prestation);
		object.setPrligDate(new NSTimestamp());

		object.setPrligReference(article.prligReference());
		object.setPrligDescription(article.prligDescription());
		object.setPrligArtHt(article.prligArtHt());
		object.setPrligArtTtc(article.prligArtTtc());
		object.setPrligArtTtcInitial(article.prligArtTtcInitial());

		object.setPrligTotalHt(article.prligTotalHt());
		object.setPrligTotalTtc(article.prligTotalTtc());
		object.setPrligTotalResteHt(article.prligTotalResteHt());
		object.setPrligTotalResteTtc(article.prligTotalResteTtc());

		object.setPrligQuantite(article.prligQuantite());
		object.setPrligQuantiteReste(article.prligQuantiteReste());

		object.setTvaRelationship(article.tva());
		object.setTvaInitialRelationship(article.tvaInitial());
		object.setTypeArticleRelationship(article.typeArticle());
		object.setPcoNum(article.pcoNum());

		return object;
	}

	private static EOPrestationLigne newObject(EOEditingContext ec) {
		EOPrestationLigne object = (EOPrestationLigne) Factory.instanceForEntity(ec, EOPrestationLigne.ENTITY_NAME);
		object.setPrligDate(new NSTimestamp());
		ec.insertObject(object);
		return object;
	}

	public static void removeObject(EOEditingContext ec, EOPrestationLigne lignePrestation,
			EOPrestationLigne prestationLignePere) {
		if (lignePrestation == null) {
			return;
		}

		if (ec != null) {
			ec.deleteObject(lignePrestation);
		}
		if (prestationLignePere != null) {
			prestationLignePere.removeFromPrestationLignesRelationship(lignePrestation);
		}
		if (lignePrestation.prestation() != null) {
			lignePrestation.prestation().removeFromPrestationLignesRelationship(lignePrestation);
		}
	}
}
