/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.factory;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eodistribution.client.EODistributedClassDescription;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class Factory {

	private boolean	withLogs;

	public Factory() {
		super();
		this.setWithLogs(false);
	}

	public Factory(boolean withLog) {
		super();
		this.setWithLogs(withLog);
	}

	public static EOEnterpriseObject instanceForEntity(EOEditingContext ec, String entity) {
		try {
			EODistributedClassDescription description = (EODistributedClassDescription) EOClassDescription.classDescriptionForEntityName(entity);
			EOEnterpriseObject object = description.createInstanceWithEditingContext(ec, null);
			return object;
		}
		catch (Exception e) {
			System.err.println("Impossible de recuperer la description pour l'entite " + entity + ". Pas d'objet cree...");
			return null;
		}
	}

	public static NSArray fetchArray(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings) {
		try {
			EOFetchSpecification spec = new EOFetchSpecification(entityName, qualifier, sortOrderings);
			spec.setRefreshesRefetchedObjects(true);
			return ec.objectsWithFetchSpecification(spec);
		}
		catch (Exception e) {
			return new NSArray();
		}
	}

	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray res = fetchArray(ec, entityName, qualifier, sortOrderings);
		if ((res == null) || (res.count() == 0)) {
			return null;
		}
		return (EOEnterpriseObject) res.objectAtIndex(0);
	}

	public void trace(final String prefix, final NSArray a) {
		for (int i = 0; i < a.count(); i++) {
			trace(prefix + "  " + i + "-->", a.objectAtIndex(i));
		}
	}

	public void trace(final Object obj) {
		trace("", obj);
	}

	public void trace(final String prefix, final Object obj) {
		if (withLogs()) {
			if (obj == null) {
				System.out.println(prefix + " null");
			}
			else {
				if (obj instanceof NSArray) {
					trace(prefix, (NSArray) obj);
				}
				else
					if (obj instanceof EOEnterpriseObject) {
						trace(prefix, (EOEnterpriseObject) obj);
					}
					else {
						System.out.println(prefix + obj.toString());
					}
			}
		}
	}

	public void trace(final String prefix, final EOEnterpriseObject object) {
		if (object != null) {
			final Iterator iter = object.attributeKeys().vector().iterator();
			while (iter.hasNext()) {
				String obj = (String) iter.next();
				trace(prefix + "  " + obj + "-->", object.valueForKey(obj));
			}

			Iterator iter2 = object.toOneRelationshipKeys().vector().iterator();
			while (iter2.hasNext()) {
				String obj = (String) iter2.next();
				trace(prefix + "  " + obj + "-->" + object.valueForKey(obj));
			}
			Iterator iter3 = object.toManyRelationshipKeys().vector().iterator();
			while (iter3.hasNext()) {
				String obj = (String) iter3.next();
				if (prefix != null && prefix.length() > 250) {
					trace(prefix + "  " + obj + "-->" + object.valueForKey(obj));
				}
				else {
					trace(prefix + "  " + obj + "-->", object.valueForKey(obj));
				}
			}
		}
	}

	/**
	 * @param withLogs
	 *            The withLogs to set.
	 * @uml.property name="withLogs"
	 */
	public void setWithLogs(boolean b) {
		this.withLogs = b;
	}

	public boolean withLogs() {
		return withLogs;
	}

	/**
	 * @return Un gregorianCalendar initialise a la date du jour (sans les heures/minutes/etc). Utilisez getToday().getTime() pour recuperer la
	 *         date du jour nettoyee des secondes sous forme de Date.
	 */
	private static final GregorianCalendar getToday() {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc;
	}

	/**
	 * @return
	 */
	public static final NSTimestamp getDateJour() {
		return new NSTimestamp(getToday().getTime());
	}

}
