/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.factory;

import org.cocktail.kava.client.metier.EOFacture;
import org.cocktail.kava.client.metier.EORecette;
import org.cocktail.kava.client.metier.EORecettePapier;
import org.cocktail.kava.client.metier.EORecettePapierAdrClient;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryRecettePapier extends Factory {

	public FactoryRecettePapier() {
		super();
	}

	public FactoryRecettePapier(boolean withLog) {
		super(withLog);
	}

	public static EORecettePapier newObject(EOEditingContext ec) {
		EORecettePapier recettePapier = (EORecettePapier) Factory.instanceForEntity(ec, EORecettePapier.ENTITY_NAME);
		recettePapier.setRppDateSaisie(Factory.getDateJour());
		ec.insertObject(recettePapier);
		return recettePapier;
	}

	public static EORecettePapier newObject(EOEditingContext ec, EORecette recette) {
		EORecettePapier recettePapier = newObject(ec);
		recettePapier.setRppVisible("N");
		recettePapier.setRppDateReception(Factory.getDateJour());
		recettePapier.setRppDateRecette(Factory.getDateJour());
		recettePapier.setRppDateServiceFait(Factory.getDateJour());
		recettePapier.setRppNbPiece(new Integer(1));
		recettePapier = buildFromRecette(ec, recettePapier, recette);
		return recettePapier;
	}

	private static EORecettePapier buildFromRecette(
			EOEditingContext ec, EORecettePapier recettePapier, EORecette recetteBase) {
		if (recetteBase == null) {
			return recettePapier;
		}

		recettePapier.setExerciceRelationship(recetteBase.exercice());
		recettePapier.setRppHtSaisie(recetteBase.recHtSaisie());
		recettePapier.setRppTtcSaisie(recetteBase.recTtcSaisie());
		recettePapier.setRppTvaSaisie(recetteBase.recTvaSaisie());

		recettePapier = buildFromFacture(recettePapier, recetteBase.facture());
		recettePapier = buildFromRecettePapier(ec, recettePapier, recetteBase);
		return recettePapier;
	}

	private static EORecettePapier buildFromFacture(EORecettePapier recettePapier, EOFacture factureBase) {
		if (factureBase == null) {
			return recettePapier;
		}

		recettePapier.setFournisUlrRelationship(factureBase.fournisUlr());
		recettePapier.setModeRecouvrementRelationship(factureBase.modeRecouvrement());
		recettePapier.setPersonneRelationship(factureBase.personne());
		return recettePapier;
	}

	private static EORecettePapier buildFromRecettePapier(
			EOEditingContext ec, EORecettePapier recettePapier, EORecette recetteBase) {
		if (recetteBase.recetteReduction() != null && recetteBase.recetteReduction().recettePapier() != null
				&& recetteBase.recetteReduction().recettePapier().currentRecPapierAdresseClient() != null) {
			EORecettePapierAdrClient adrClientBase =
					recetteBase.recetteReduction().recettePapier().currentRecPapierAdresseClient();
			EORecettePapierAdrClient eoNewAdrClient = FactoryRecettePapierAdrClient.newObject(
					ec, adrClientBase.toPersonneCreation(), recettePapier, adrClientBase.toAdresse());
			recettePapier.addToToRecettePapierAdrClientsRelationship(eoNewAdrClient);
		}
		return recettePapier;
	}

}
