/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.kava.client.qualifier;

import java.util.List;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.kava.client.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EONotQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Classe fournissant les qualifiers specifiques aux plans comptables contreparties.
 *
 * @author flagouey
 */
public class PlanComptableCtpQualifiers {

	/** Get singleton. */
	public static final PlanComptableCtpQualifiers instance() {
		return INSTANCE;
	}

	/** Singleton instance. */
	private static final PlanComptableCtpQualifiers INSTANCE = new PlanComptableCtpQualifiers();

	/**
	 * Constructeur privé.
	 */
	private PlanComptableCtpQualifiers() {
	}

	/**
	 * Build le qualifier 'TYPE VALIDE'.
	 * @param planComptablePath  chemin d'acces a l'entite 'planComptable'
	 * @return qualifier limitant les plans comptables Contreparties à ceux valides.
	 */
	public EOQualifier buildTypeEtatValide(String planComptablePath) {
		return EOQualifier.qualifierWithQualifierFormat(
				planComptablePath + "." + EOPlanComptable.PCO_VALIDITE_KEY + " = %@",
				new NSArray("VALIDE"));
	}

	/**
	 * Build le qualifier 'Exercice'.
	 * @param planComptablePath  chemin d'acces a l'entite 'planComptable'
	 * @param exercice           exercice.
	 * @return qualifier Exercice ; null si echec.
	 */
	public EOQualifier buildExercice(String planComptablePath, EOExercice exercice) {
		if (exercice == null) {
			return null;
		}
		return EOQualifier.qualifierWithQualifierFormat(
				planComptablePath + "." + EOPlanComptable.EXERCICE_KEY + " = %@",
				new NSArray(exercice));
	}

	/**
	 * Build le qualifier limitant les résultats à certaines classes du plan comptable.
	 * @param numeroComptePath  chemin d'acces a la cle 'numero de compte'
	 * @param classesInterdites liste des classes a exclure.
	 * @return qualifier limitant les résultats à certains classes du plan comptable ; null si echec.
	 */
	public EOQualifier buildClassesInterdites(String numeroComptePath, List<String> classesInterdites) {
		if (classesInterdites == null) {
			return null;
		}

		EOQualifier classesInterditesQualifier = null;
		NSMutableArray classesInterditesQualifiers = new NSMutableArray();
		for (String classeInterdite : classesInterdites) {
			classesInterditesQualifiers.addObject(new EOKeyValueQualifier(
					numeroComptePath, EOQualifier.QualifierOperatorLike, classeInterdite + "*"));
		}

		if (classesInterditesQualifiers.count() > 0) {
			classesInterditesQualifier = new EONotQualifier(new EOOrQualifier(classesInterditesQualifiers));
		}
		return classesInterditesQualifier;
	}
}
