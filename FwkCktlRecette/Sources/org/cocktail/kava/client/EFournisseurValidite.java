package org.cocktail.kava.client;

import org.cocktail.kava.client.metier.EOFournisUlr;

// TODO FLA : delete me
public enum EFournisseurValidite {

	VALIDE ("Valide", EOFournisUlr.FOU_ETAT_VALIDE),
	EN_ATTENTE ("En attente", EOFournisUlr.FOU_ETAT_ATTENTE);

	private final String label;
	private final String code;

	private EFournisseurValidite(String label, String code) {
		this.label = label;
		this.code = code;
	}

	public String getLabel() {
		return label;
	}

	public String getCode() {
		return code;
	}
}
