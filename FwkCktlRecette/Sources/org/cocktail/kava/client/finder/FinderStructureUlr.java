/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.finder;

import org.cocktail.kava.client.EFournisseurType;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.qualifier.Qualifiers;
import org.cocktail.kava.client.qualifier.Qualifiers.QualifierKey;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderStructureUlr {

	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOStructureUlr.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static EOStructureUlr findByPrimaryKey(EOEditingContext ec, Object pk) {
		if (pk == null) {
			return null;
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.PRIMARY_KEY_KEY + " = %@", new NSArray(pk));
		EOFetchSpecification fs = new EOFetchSpecification(EOStructureUlr.ENTITY_NAME, qual, null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			NSArray a = ec.objectsWithFetchSpecification(fs);
			if (a != null && a.count() > 0) {
				return (EOStructureUlr) a.objectAtIndex(0);
			}
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * Retourne une liste de structures valides matchant les criteres de recherche fournis.
	 * @param ec          editing context courant.
	 * @param llStructure valeur du critere 'llStructure' (facultatif)
	 * @param siret       valeur du critere 'siret' (facultatif)
	 * @param fournisType valeur du critere 'type de fournisseur' (facultatif)
	 * @return une liste de structures valides matchant les criteres de recherche specifies.
	 */
	public static NSArray find(EOEditingContext ec, String llStructure, String siret, EFournisseurType fournisseurType) {
		NSMutableArray quals = new NSMutableArray();
		if (llStructure != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.LL_STRUCTURE_KEY + " caseInsensitiveLike %@", new NSArray(
					"*" + llStructure + "*")));
		}
		if (siret != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.SIRET_KEY + " caseInsensitiveLike %@", new NSArray("*"
					+ siret + "*")));
		}
		if (fournisseurType != null && !EFournisseurType.TOUS.equals(fournisseurType)) {
			quals.addObject(Qualifiers.getQualifierFactory(QualifierKey.PERSONNE_FOURNISSEUR_TYPE).build(
					EOQualifier.QualifierOperatorEqual, fournisseurType.getCode()));
		}
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.TEM_VALIDE_KEY + " = %@", new NSArray("O")));
		EOFetchSpecification fs = new EOFetchSpecification(EOStructureUlr.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static EOStructureUlr findStructure(EOEditingContext ec, String cStructure) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOStructureUlr.C_STRUCTURE_KEY + " = %@", new NSArray(cStructure)));
		EOFetchSpecification fs = new EOFetchSpecification(EOStructureUlr.ENTITY_NAME, new EOAndQualifier(quals), null);
		try {
			return (EOStructureUlr) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
