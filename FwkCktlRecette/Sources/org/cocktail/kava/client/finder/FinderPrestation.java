/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA 
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant 
 donne sa specificite de logiciel libre, qui peut le rendre complexe a 
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement, 
 a l'utiliser et l'exploiter dans les memes conditions de securite. 

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez 
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use, 
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info". 

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability. 

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or 
 data to be ensured and,  more generally, to use and operate it in the 
 same conditions as regards security. 

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.finder;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeEtat;
import org.cocktail.kava.client.metier.EOCatalogue;
import org.cocktail.kava.client.metier.EOCatalogueResponsable;
import org.cocktail.kava.client.metier.EOPersonne;
import org.cocktail.kava.client.metier.EOPrestation;
import org.cocktail.kava.client.metier.EORepartStructure;
import org.cocktail.kava.client.metier.EOSecretariat;
import org.cocktail.kava.client.metier.EOStructureUlr;
import org.cocktail.kava.client.metier.EOTypeApplication;
import org.cocktail.kava.client.metier.EOTypePublic;
import org.cocktail.kava.client.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPrestation {

	public static EOPrestation find(EOEditingContext ec, Integer prestNumero) {
		NSMutableArray quals = new NSMutableArray();
		if (prestNumero != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PREST_NUMERO_KEY + " = %@", new NSArray(prestNumero)));
		}
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestation.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOPrestation) ec.objectsWithFetchSpecification(fs).lastObject();
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public static EOPrestation findByPrimaryKey(EOEditingContext ec, Object pk) {
		if (pk == null) {
			return null;
		}
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOPrestation.PRIMARY_KEY_KEY + " = %@", new NSArray(pk));
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestation.ENTITY_NAME, qual, null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			NSArray a = ec.objectsWithFetchSpecification(fs);
			if (a != null && a.count() > 0) {
				return (EOPrestation) a.lastObject();
			}
			return null;
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static EOQualifier defaultQualifierWithDroit(EOUtilisateur utilisateur,EOExercice exercice){
		NSMutableArray quals = new NSMutableArray();
		if (utilisateur != null) {
			NSMutableArray orQuals = new NSMutableArray();
			// utilisateur
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
			// responsable du catalogue du devis
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CATALOGUE_RESPONSABLES_KEY
					+ "." + EOCatalogueResponsable.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
			// contact
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur
					.personne().individuUlr())));
			// secretariats
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.SECRETARIATS_KEY + "." + EOSecretariat.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur.personne()
					.individuUlr())));
			// ttes personnes du groupe client
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.REPART_STRUCTURES_KEY + "." + EORepartStructure.PERSONNE_KEY + " = %@", new NSArray(utilisateur.personne())));

			// TODO ajouter les membres des sous-groupes du client de type 'PD'

			quals.addObject(new EOOrQualifier(orQuals));
		}
		if (exercice != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		}
		return new EOAndQualifier(quals);
	}

	public static NSArray find(EOEditingContext ec, EOExercice exercice, EOUtilisateur utilisateur, EOTypeApplication typeApplication,
			EOTypeEtat typeEtat, EOTypePublic typePublic) {
		NSMutableArray quals = new NSMutableArray();
		if (utilisateur != null) {
			NSMutableArray orQuals = new NSMutableArray();
			// utilisateur
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
			// responsable du catalogue du devis
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.CATALOGUE_KEY + "." + EOCatalogue.CATALOGUE_RESPONSABLES_KEY
					+ "." + EOCatalogueResponsable.UTILISATEUR_KEY + " = %@", new NSArray(utilisateur)));
			// contact
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur
					.personne().individuUlr())));
			// secretariats
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.SECRETARIATS_KEY + "." + EOSecretariat.INDIVIDU_ULR_KEY + " = %@", new NSArray(utilisateur.personne()
					.individuUlr())));
			// ttes personnes du groupe client
			orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.PERSONNE_KEY + "." + EOPersonne.STRUCTURE_ULR_KEY + "."
					+ EOStructureUlr.REPART_STRUCTURES_KEY + "." + EORepartStructure.PERSONNE_KEY + " = %@", new NSArray(utilisateur.personne())));

			// TODO ajouter les membres des sous-groupes du client de type 'PD'

			quals.addObject(new EOOrQualifier(orQuals));
		}
		if (exercice != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.EXERCICE_KEY + " = %@", new NSArray(exercice)));
		}
		if (typeApplication != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_PUBLIC_KEY + "." + EOTypePublic.TYPE_APPLICATION_KEY
					+ "=%@", new NSArray(typeApplication)));
		}
		if (typeEtat != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_ETAT_KEY + " = %@", new NSArray(typeEtat)));
		}
		if (typePublic != null) {
			quals.addObject(EOQualifier.qualifierWithQualifierFormat(EOPrestation.TYPE_PUBLIC_KEY + " = %@", new NSArray(typePublic)));
		}
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPrestation.PREST_NUMERO_KEY, EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestation.ENTITY_NAME, new EOAndQualifier(quals), sort);
		
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}
	
	public static NSArray find(EOEditingContext ec,EOQualifier quals){
		return find(ec, quals, null);
	}

	public static NSArray find(EOEditingContext ec, EOQualifier quals, Integer fetchLimit) {
		NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOPrestation.PREST_NUMERO_KEY, EOSortOrdering.CompareDescending));
		EOFetchSpecification fs = new EOFetchSpecification(EOPrestation.ENTITY_NAME, quals, sort);
		fs.setRefreshesRefetchedObjects(true);
		fs.setUsesDistinct(true);
		if (fetchLimit != null) {
			fs.setFetchLimit(fetchLimit);
		}
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		} catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

}
