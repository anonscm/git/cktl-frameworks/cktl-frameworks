/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.client.finder;

import org.cocktail.application.client.eof.EOExercice;
import org.cocktail.application.client.eof.EOTypeCredit;
import org.cocktail.kava.client.metier.EOConvention;
import org.cocktail.kava.client.metier.EOConventionLimitative;
import org.cocktail.kava.client.metier.EOConventionNonLimitative;
import org.cocktail.kava.client.metier.EOOrgan;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderConvention {

	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	private static EOQualifier qualValide() {
		return EOQualifier.qualifierWithQualifierFormat(EOConvention.CON_SUPPR_KEY + "=%@", new NSArray("N"));
	}

	public static NSArray findDep(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditDep) {
		if (exercice == null || organ == null || typeCreditDep == null) {
			return new NSArray();
		}

		NSMutableArray andQualsLimitatives = new NSMutableArray();
		andQualsLimitatives.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_LIMITATIVES_KEY + "." + EOConventionLimitative.EXERCICE_KEY + " = %@",
				new NSArray(new Object[] { exercice })));
		andQualsLimitatives.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_LIMITATIVES_KEY + "." + EOConventionLimitative.ORGAN_KEY + " = %@",
				new NSArray(new Object[] { organ })));
		andQualsLimitatives.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_LIMITATIVES_KEY + "." + EOConventionLimitative.TYPE_CREDIT_DEP_KEY + " = %@",
				new NSArray(new Object[] { typeCreditDep })));

		NSMutableArray andQualsNonLimitative = new NSMutableArray();
		andQualsNonLimitative.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "." + EOConventionNonLimitative.EXERCICE_KEY + " = %@",
				new NSArray(new Object[] { exercice })));
		andQualsNonLimitative.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "." + EOConventionNonLimitative.ORGAN_KEY + " = %@",
				new NSArray(new Object[] { organ })));
		andQualsNonLimitative.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "." + EOConventionNonLimitative.TYPE_CREDIT_DEP_KEY
					+ " = %@",
				new NSArray(new Object[] { typeCreditDep })));

		NSMutableArray orQuals = new NSMutableArray();
		orQuals.addObject(new EOAndQualifier(andQualsLimitatives));
		orQuals.addObject(new EOAndQualifier(andQualsNonLimitative));

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualValide());
		quals.addObject(new EOOrQualifier(orQuals));

		EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(quals), null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static NSArray findRec(EOEditingContext ec, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCreditRec) {
		if (exercice == null || organ == null || typeCreditRec == null) {
			return new NSArray();
		}

		NSMutableArray orQuals = new NSMutableArray();

		orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(
			EOConvention.CONVENTION_LIMITATIVES_KEY + "." + EOConventionLimitative.EXERCICE_KEY + " = %@ AND "
			+ EOConvention.CONVENTION_LIMITATIVES_KEY + "."	+ EOConventionLimitative.ORGAN_KEY + " = %@ AND "
			+ EOConvention.CONVENTION_LIMITATIVES_KEY + "."	+ EOConventionLimitative.TYPE_CREDIT_REC_KEY + " = %@",
			new NSArray(new Object[] { exercice, organ, typeCreditRec })));

		orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(
			EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "." + EOConventionNonLimitative.EXERCICE_KEY + " = %@ AND "
			+ EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "."	+ EOConventionNonLimitative.ORGAN_KEY + " = %@ AND "
			+ EOConvention.CONVENTION_NON_LIMITATIVES_KEY + "."	+ EOConventionNonLimitative.TYPE_CREDIT_REC_KEY + " = %@",
			new NSArray(new Object[] { exercice, organ, typeCreditRec })));

		// conventions non encore positionnees
		// orQuals.addObject(EOQualifier.qualifierWithQualifierFormat(EOConvention.CONVENTION_SANS_POSITS_KEY + "."
		// + EOConventionSansPosit.CSP_MODE_GESTION_KEY + " != nil", null));

		NSMutableArray quals = new NSMutableArray();
		quals.addObject(qualValide());
		quals.addObject(new EOOrQualifier(orQuals));

		EOFetchSpecification fs = new EOFetchSpecification(EOConvention.ENTITY_NAME, new EOAndQualifier(quals), null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	/**
	 * Fetch d'une convention selon son identifiant CON_ORDRE.
	 * @param ec Editing context a utiliser.
	 * @param conOrdre Identifiant unique du contrat.
	 * Ajoute par bertrand le 30/05/2007 pour les prestation de formation continue.
	 */
	public static EOConvention find(EOEditingContext ec, Number conOrdre) {

		EOFetchSpecification fs = new EOFetchSpecification(
				EOConvention.ENTITY_NAME,
				EOQualifier.qualifierWithQualifierFormat(EOConvention.PRIMARY_KEY_KEY + " = %@", new NSArray(conOrdre)),
				null);
		try {
			return (EOConvention) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
