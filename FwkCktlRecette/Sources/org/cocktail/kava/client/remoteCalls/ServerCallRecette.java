package org.cocktail.kava.client.remoteCalls;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eodistribution.client.EODistributedObjectStore;
import com.webobjects.foundation.NSDictionary;

public class ServerCallRecette {

	public static final String SESSION_KEY = "session";

	private static final String CLIENT_SIDE_REQUEST_PRIMARY_KEY = "clientSideRequestPrimaryKeyForGlobalId";

	protected static String getRemoteKeyPath() {
		return SESSION_KEY + "." + "remoteDelegateRecette";
	}

	public static NSDictionary serverPrimaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) {
		
		
		return (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
				ec, getRemoteKeyPath(), CLIENT_SIDE_REQUEST_PRIMARY_KEY,
				new Class[] {EOGlobalID.class},
				new Object[] {ec.globalIDForObject(eo)},
				false);
	}
}
