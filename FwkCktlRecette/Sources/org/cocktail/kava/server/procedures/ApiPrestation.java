/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.procedures;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.metier.EOAdresse;
import org.cocktail.kava.server.metier.EOCommandesPourPi;
import org.cocktail.kava.server.metier.EOFacturePapier;
import org.cocktail.kava.server.metier.EOFournisUlr;
import org.cocktail.kava.server.metier.EOModeRecouvrement;
import org.cocktail.kava.server.metier.EOPersonne;
import org.cocktail.kava.server.metier.EOPrestation;
import org.cocktail.kava.server.metier.EORibfourUlr;
import org.cocktail.kava.server.metier.EOTauxProrata;
import org.cocktail.kava.server.metier.EOUtilisateur;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

public class ApiPrestation {

	public static void regrouperPrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, NSArray prestations, EOUtilisateur utilisateur)
			throws Exception {
		if (prestations == null || utilisateur == null) {
			throw new Exception("prestations a regrouper ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			String chaine = "";
			for (int i = 0; i < prestations.count(); i++) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, (EOPrestation) prestations.objectAtIndex(i));
				chaine = chaine + dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY) + "$";
			}

			dico.takeValueForKey(chaine + "$", "010aChaine");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("RegrouperPrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void validePrestationClient(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a valider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationValidePrestationClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void devalidePrestationClient(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a devalider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDevalidePrestationClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void validePrestationPrestataire(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation,
			EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a valider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationValidePrestationPrest", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void soldePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a valider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationSoldePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void dupliquePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		if (prestation == null || utilisateur == null || exerciceCible == null) {
			throw new Exception("prestation a valider, utilisateur ou exercice cible a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, exerciceCible);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "030aExeOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDuplicatePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void basculePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur, EOExercice exerciceCible) throws Exception {
		if (prestation == null || utilisateur == null || exerciceCible == null) {
			throw new Exception("prestation a valider, utilisateur ou exercice cible a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, exerciceCible);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "030aExeOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDuplicatePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			if (!dataBus.executeProcedure("ApiPrestationSoldePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			try {
				decloturePrestation(dataBus, ec, prestation, utilisateur);
				devalidePrestationPrestataire(dataBus, ec, prestation, utilisateur);
				devalidePrestationClient(dataBus, ec, prestation, utilisateur);
			} catch (Throwable e) {
			}
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void devalidePrestationPrestataire(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation,
			EOUtilisateur utilisateur) throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a devalider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDevalidePrestationPrest", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void cloturePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a cloturer ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationCloturePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void decloturePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a decloturer ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDecloturePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void changePrestationDT(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation oldPrestation, EOPrestation newPrestation)
			throws Exception {
		if (oldPrestation == null || newPrestation == null) {
			throw new Exception("prestation a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, oldPrestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aOldPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, newPrestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aNewPrestId");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationChangePrestationDT", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void archivePrestation(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a archiver ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationArchivePrestation", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void genereFacturePapier(_CktlBasicDataBus dataBus, EOEditingContext ec, EOPrestation prestation, EOUtilisateur utilisateur)
			throws Exception {
		if (prestation == null || utilisateur == null) {
			throw new Exception("prestation a facturer ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, prestation);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPrestation.PREST_ID_KEY), "010aPrestId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationGenereFacturePapier", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void delFacturePapier(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier, EOUtilisateur utilisateur)
			throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Facture papier a supprimer ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDelFacturePapier", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static NSDictionary prestationFromCommande(_CktlBasicDataBus dataBus, EOEditingContext ec, EOCommandesPourPi commande,
			EOUtilisateur utilisateur, EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		if (commande == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Commande a generer en PI ou utilisateur ou fournisseur client a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, commande);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommandesPourPi.COMM_ID_KEY), "010aCommId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, fournisUlrClient);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.FOU_ORDRE_KEY), "030aFouOrdreClient");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, FinderTauxProrata.tauxProrata100(ec));
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "050aTapIdRecette");

			dico.takeValueForKey(pcoNum, "080aPcoNumRecette");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationPrestationFromCommande", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static NSDictionary facturePapierFromCommande(_CktlBasicDataBus dataBus, EOEditingContext ec, EOCommandesPourPi commande,
			EOUtilisateur utilisateur, EOFournisUlr fournisUlrClient, String pcoNum) throws Exception {
		if (commande == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Commande a generer en PI/Facture papier ou utilisateur ou fournisseur client a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, commande);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOCommandesPourPi.COMM_ID_KEY), "010aCommId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, fournisUlrClient);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.FOU_ORDRE_KEY), "030aFouOrdreClient");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, FinderTauxProrata.tauxProrata100(ec));
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "050aTapIdRecette");

			dico.takeValueForKey(pcoNum, "080aPcoNumRecette");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationFacturePapierFromCommande", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static NSDictionary duplicateFacturePapier(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur, EOFournisUlr fournisUlrClient) throws Exception {
		if (facturePapier == null || utilisateur == null || fournisUlrClient == null) {
			throw new Exception("Facture papier a dupliquer ou utilisateur ou fournisseur client a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, fournisUlrClient);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.FOU_ORDRE_KEY), "030aFouOrdreClient");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDuplicateFacturePapier", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static NSDictionary duplicateFacturePapierAdresse(_CktlBasicDataBus dataBus, EOEditingContext ec,
			EOFacturePapier facturePapier, EOUtilisateur utilisateur, EOFournisUlr fournisUlrClient,
			EOAdresse fournisUlrAdresse) throws Exception {
		if (facturePapier == null || utilisateur == null || fournisUlrClient == null || fournisUlrAdresse == null) {
			throw new Exception(
					"Facture papier a dupliquer ou utilisateur ou fournisseur client a null ou fourniseur sans adresse.");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, fournisUlrClient);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFournisUlr.FOU_ORDRE_KEY), "030aFouOrdreClient");

			// 030 && 040 utilise en tant que parametres retours.

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, fournisUlrAdresse);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOAdresse.ENTITY_PRIMARY_KEY), "060aFouAdrOrdre");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur.personne());
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPersonne.ENTITY_PRIMARY_KEY), "070aPersIdCreation");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDuplicateFacturePapierAdresse", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
			return dataBus.executedProcResult();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}


	public static void updFapRef(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier) throws Exception {
		if (facturePapier == null) {
			throw new Exception("prestation a referencer (fap_ref) null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationUpdFapRef", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void valideFacturePapierClient(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Facture papier a valider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationValideFacturePapierClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void devalideFacturePapierClient(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Facture papier a devalider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDevalideFacturePapierClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void valideFacturePapierPrest(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Facture papier a valider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			if (facturePapier.engageBudget() != null && facturePapier.engageBudget().tauxProrata() != null) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier.engageBudget().tauxProrata());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "030aTapIdDepense");
			}
			else {
				dico.takeValueForKey(null, "030aTapIdDepense");
			}

			dico.takeValueForKey(null, "040aRibOrdre");
			if (facturePapier.ribfourUlr() != null) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier.ribfourUlr());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EORibfourUlr.RIB_ORDRE_KEY), "040aRibOrdre");
			}
			else {
				dico.takeValueForKey(null, "040aRibOrdre");
			}

			if (facturePapier.modeRecouvrement() != null) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier.modeRecouvrement());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOModeRecouvrement.MOD_ORDRE_KEY), "050aMorOrdre");
			}
			else {
				dico.takeValueForKey(null, "050aMorOrdre");
			}

			if (facturePapier.tauxProrata() != null) {
				dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier.tauxProrata());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTauxProrata.TAP_ID_KEY), "060aTapIdRecette");
			}
			else {
				dico.takeValueForKey(null, "060aTapIdRecette");
			}

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationValideFacturePapierPrest", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void devalideFacturePapierPrest(_CktlBasicDataBus dataBus, EOEditingContext ec, EOFacturePapier facturePapier,
			EOUtilisateur utilisateur) throws Exception {
		if (facturePapier == null || utilisateur == null) {
			throw new Exception("Facture papier a devalider ou utilisateur a null!!");
		}

		try {
			NSMutableDictionary dico = new NSMutableDictionary();
			NSDictionary dicoForPrimaryKeys = null;

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, facturePapier);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOFacturePapier.FAP_ID_KEY), "010aFapId");

			dicoForPrimaryKeys = EOUtilities.primaryKeyForObject(ec, utilisateur);
			dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "020aUtlOrdre");

			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationDevalideFacturePapierPrest", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void insFactureAdrClient(_CktlBasicDataBus dataBus, NSDictionary dico) throws Exception {
		if (dico == null) {
			throw new Exception("Parametre null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationInsFactureAdrClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

	public static void insPrestationAdrClient(_CktlBasicDataBus dataBus, NSDictionary dico) throws Exception {
		if (dico == null) {
			throw new Exception("Parametre null!!");
		}
		try {
			dataBus.beginTransaction();
			if (!dataBus.executeProcedure("ApiPrestationInsPrestationAdrClient", dico)) {
				throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			dataBus.commitTransaction();
		} catch (Exception e) {
			e.printStackTrace();
			dataBus.rollbackTransaction();
			throw e;
		}
	}

}