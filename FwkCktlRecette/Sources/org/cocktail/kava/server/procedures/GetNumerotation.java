/*
 * Copyright Cocktail (Consortium) 1995-2007
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use,
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability.
 *
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and, more generally, to use and operate it in the
 * same conditions as regards security.
 *
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.procedures;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.metier.EOGestion;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSMutableDictionary;

import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

public class GetNumerotation {

	public static Number get(_CktlBasicDataBus dataBus, EOEditingContext ec, EOExercice exercice, EOGestion gestion,
			String tnuEntite) throws Exception {
		if (tnuEntite == null) {
			throw new Exception("Il faut un type de numerotation (tnuEntite) pour numeroter!");
		}
		NSMutableDictionary mutableDico = new NSMutableDictionary(tnuEntite, "030aTnuEntite");
		if (exercice != null) {
			EOEnterpriseObject eo = EOUtilities.localInstanceOfObject(ec, exercice);
			if (eo != null) {
				mutableDico.takeValueForKey(EOUtilities.primaryKeyForObject(ec, eo).valueForKey(EOExercice.EXE_ORDRE_KEY), "010aExeOrdre");
			}
		}
		if (gestion != null) {
			EOEnterpriseObject eo = EOUtilities.localInstanceOfObject(ec, gestion);
			if (eo != null) {
				mutableDico.takeValueForKey(EOUtilities.primaryKeyForObject(ec, eo).valueForKey(EOGestion.GES_CODE_KEY), "020aGesCode");
			}
		}
		if (!dataBus.executeProcedure("GetNumerotationProc", mutableDico)) {
			throw new Exception((String) dataBus.executedProcResult().valueForKey(_CktlBasicDataBus.ERROR_KEY));
		}
		return (Number) dataBus.executedProcResult().valueForKey("040aNumNumero");
	}

}