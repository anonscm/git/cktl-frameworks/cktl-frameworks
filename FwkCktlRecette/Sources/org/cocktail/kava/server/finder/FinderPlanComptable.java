/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.kava.server.finder;

import java.io.Serializable;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.metier.EOPlanComptable;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * PlanComptable server side finder.
 * @author flagouey
 */
public class FinderPlanComptable implements Serializable {

	/** Serial Version ID. */
	private static final long serialVersionUID = -8017204340145385611L;

	/**
	 * Retrieve from DB planComptable object matching the 'exercice' and 'pcoNum' provided.
	 * @param ec       editing context.
	 * @param exercice exercice as Type.
	 * @param pcoNum   planComptable identifier.
	 * @return planComptable object mathing the exercice and identifier specified ; null if not found.
	 */
	public static EOPlanComptable findOnlyValid(EOEditingContext ec, EOExercice exercice, String pcoNum) {
		return find(ec, exercice.exeOrdre(), pcoNum, true);
	}

	/**
	 * Retrieve from DB planComptable object matching the 'exercice' and 'pcoNum' provided.
	 * @param ec       editing context.
	 * @param exercice exercice as Type.
	 * @param pcoNum   planComptable identifier.
	 * @return planComptable object mathing the exercice and identifier specified ; null if not found.
	 */
	public static EOPlanComptable findById(EOEditingContext ec, Number exercice, String pcoNum) {
		return find(ec, exercice, pcoNum, false);
	}

	/**
	 * Retrieve from DB planComptable object matching the 'exercice' and 'pcoNum' provided.
	 * @param ec       editing context.
	 * @param exercice exercice as Type.
	 * @param pcoNum   planComptable identifier.
	 * @return planComptable object mathing the exercice and identifier specified ; null if not found.
	 */
	public static EOPlanComptable findById(EOEditingContext ec, EOExercice exercice, String pcoNum) {
		return find(ec, exercice.exeOrdre(), pcoNum, false);
	}

	/**
	 * Retrieve from DB planComptable object matching the 'exercice' and 'pcoNum' provided.
	 * @param ec       editing context.
	 * @param exercice exercice as Number
	 * @param pcoNum   planComptable identifier.
	 * @return planComptable object mathing the exercice and identifier specified ; null if not found.
	 */
	public static EOPlanComptable find(EOEditingContext ec, Number exercice, String pcoNum, boolean onlyValid) {
		EOPlanComptable resultPco = null;
		if (pcoNum == null || exercice == null) {
			return resultPco;
		}

		NSMutableArray quals = new NSMutableArray();
		if (onlyValid) {
			quals.addObject(qualValide());
		}
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOPlanComptable.PCO_NUM_KEY + " = %@",
				new NSArray(new Object[] {pcoNum})) );
		quals.addObject(EOQualifier.qualifierWithQualifierFormat(
				EOPlanComptable.EXERCICE_KEY + "." + EOExercice.EXE_ORDRE_KEY + " = %@",
				new NSArray(exercice)) );

		EOFetchSpecification fs = new EOFetchSpecification(EOPlanComptable.ENTITY_NAME, new EOAndQualifier(quals), null);
		fs.setUsesDistinct(true);
		fs.setRefreshesRefetchedObjects(true);
		try {
			NSArray results = (NSArray) ec.objectsWithFetchSpecification(fs);
			if (results != null && results.count() > 0) {
				resultPco = (EOPlanComptable) results.objectAtIndex(0);
			}
			return resultPco;
		} catch (Exception e) {
			e.printStackTrace();
			return resultPco;
		}
	}

	/**
	 * Define VALIDE qualifier.
	 * @return VALIDE qualifier.
	 */
	private static EOQualifier qualValide() {
		return EOQualifier.qualifierWithQualifierFormat(
				EOPlanComptable.PCO_VALIDITE_KEY + " = %@", new NSArray("VALIDE"));
	}

}
