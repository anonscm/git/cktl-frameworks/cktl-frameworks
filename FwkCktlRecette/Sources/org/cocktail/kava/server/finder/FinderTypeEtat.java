/*
 Copyright Cocktail (Consortium) 1995-2007

 Ce logiciel est regi par la licence CeCILL soumise au droit francais et
 respectant les principes de diffusion des logiciels libres. Vous pouvez
 utiliser, modifier et/ou redistribuer ce programme sous les conditions
 de la licence CeCILL telle que diffusee par le CEA, le CNRS et l'INRIA
 sur le site "http://www.cecill.info".

 En contrepartie de l'accessibilite au code source et des droits de copie,
 de modification et de redistribution accordes par cette licence, il n'est
 offert aux utilisateurs qu'une garantie limitee.  Pour les memes raisons,
 seule une responsabilite restreinte pese sur l'auteur du programme,  le
 titulaire des droits patrimoniaux et les concedants successifs.

 A cet egard  l'attention de l'utilisateur est attiree sur les risques
 associes au chargement,  a l'utilisation,  a la modification et/ou au
 developpement et a la reproduction du logiciel par l'utilisateur etant
 donne sa specificite de logiciel libre, qui peut le rendre complexe a
 manipuler et qui le reserve donc a des developpeurs et des professionnels
 avertis possedant  des  connaissances  informatiques approfondies.  Les
 utilisateurs sont donc invites a charger  et  tester  l'adequation  du
 logiciel a leurs besoins dans des conditions permettant d'assurer la
 securite de leurs systemes et ou de leurs donnees et, plus generalement,
 a l'utiliser et l'exploiter dans les memes conditions de securite.

 Le fait que vous puissiez acceder a cet en-tete signifie que vous avez
 pris connaissance de la licence CeCILL, et que vous en avez accepte les
 termes.


 This software is governed by the CeCILL  license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 "http://www.cecill.info".

 As a counterpart to the access to the source code and  rights to copy,
 modify and redistribute granted by the license, users are provided only
 with a limited warranty  and the software's author,  the holder of the
 economic rights,  and the successive licensors  have only  limited
 liability.

 In this respect, the user's attention is drawn to the risks associated
 with loading,  using,  modifying and/or developing or reproducing the
 software by the user in light of its specific status of free software,
 that may mean  that it is complicated to manipulate,  and  that  also
 therefore means  that it is reserved for developers  and  experienced
 professionals having in-depth computer knowledge. Users are therefore
 encouraged to load and test the software's suitability as regards their
 requirements in conditions enabling the security of their systems and/or
 data to be ensured and,  more generally, to use and operate it in the
 same conditions as regards security.

 The fact that you are presently reading this means that you have had
 knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.kava.server.finder;


import org.cocktail.application.serveur.eof.EOTypeEtat;
import org.cocktail.pieFwk.common.EditingContextHelper;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class FinderTypeEtat {

	private static final String	ETAT_VALIDE	= "VALIDE";
	private static final String	ETAT_ANNULE	= "ANNULE";
	private static final String	ETAT_OUI	= "OUI";
	private static final String	ETAT_NON	= "NON";

	private static EOTypeEtat	TYPE_ETAT_VALIDE;
	private static EOTypeEtat	TYPE_ETAT_ANNULE;
	private static EOTypeEtat	TYPE_ETAT_OUI;
	private static EOTypeEtat	TYPE_ETAT_NON;

	public static NSArray findAll(EOEditingContext ec) {
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeEtat.ENTITY_NAME, null, null);
		try {
			return (NSArray) ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new NSArray();
		}
	}

	public static EOTypeEtat typeEtatValide(EOEditingContext ec) {
		if (TYPE_ETAT_VALIDE == null || TYPE_ETAT_VALIDE.editingContext() == null) {
			TYPE_ETAT_VALIDE = findTypeEtat(ec, ETAT_VALIDE);
		}
		TYPE_ETAT_VALIDE = (EOTypeEtat) EditingContextHelper.localInstanceOfObject(ec, TYPE_ETAT_VALIDE);
		return TYPE_ETAT_VALIDE;
	}

	public static EOTypeEtat typeEtatAnnule(EOEditingContext ec) {
		if (TYPE_ETAT_ANNULE == null || TYPE_ETAT_ANNULE.editingContext() == null) {
			TYPE_ETAT_ANNULE = findTypeEtat(ec, ETAT_ANNULE);
		}
		TYPE_ETAT_ANNULE = (EOTypeEtat) EditingContextHelper.localInstanceOfObject(ec, TYPE_ETAT_ANNULE);
		return TYPE_ETAT_ANNULE;
	}

	public static EOTypeEtat typeEtatOui(EOEditingContext ec) {
		if (TYPE_ETAT_OUI == null || TYPE_ETAT_OUI.editingContext() == null) {
			TYPE_ETAT_OUI = findTypeEtat(ec, ETAT_OUI);
		}
		TYPE_ETAT_OUI = (EOTypeEtat) EditingContextHelper.localInstanceOfObject(ec, TYPE_ETAT_OUI);
		return TYPE_ETAT_OUI;
	}

	public static EOTypeEtat typeEtatNon(EOEditingContext ec) {
		if (TYPE_ETAT_NON == null || TYPE_ETAT_NON.editingContext() == null) {
			TYPE_ETAT_NON = findTypeEtat(ec, ETAT_NON);
		}
		TYPE_ETAT_NON  = (EOTypeEtat) EditingContextHelper.localInstanceOfObject(ec, TYPE_ETAT_NON);
		return TYPE_ETAT_NON;
	}

	private static EOTypeEtat findTypeEtat(EOEditingContext ec, String typeEtat) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(EOTypeEtat.TYET_LIBELLE_KEY + " = %@", new NSArray(typeEtat));
		EOFetchSpecification fs = new EOFetchSpecification(EOTypeEtat.ENTITY_NAME, qual, null);
		fs.setRefreshesRefetchedObjects(true);
		try {
			return (EOTypeEtat) ec.objectsWithFetchSpecification(fs).objectAtIndex(0);
		}
		catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

}
