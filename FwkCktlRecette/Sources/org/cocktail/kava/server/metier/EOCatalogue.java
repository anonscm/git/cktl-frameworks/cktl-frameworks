
// EOCatalogue.java
// 

package org.cocktail.kava.server.metier;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation.NSValidation;

public class EOCatalogue extends _EOCatalogue {
	
	public static final String PK_CAT_ID  = "pkCatId";
	public static final EOSortOrdering SORT_LIBELLE = EOSortOrdering.sortOrderingWithKey(CAT_LIBELLE_KEY, EOSortOrdering.CompareAscending);
	
	public static final NSTimestampFormatter FORMAT_DATE =new NSTimestampFormatter("%m/%d/%Y");
	
	
	public EOCatalogue() {
		super();
	}

	
	public String chaineDateDebut() {
		NSTimestamp deb = this.catDateDebut();
		if(deb!=null) {
			return FORMAT_DATE.format(deb); 
		}
		else {
			return null;
		}
	}
	
	public String chaineDateFin() {
		NSTimestamp fin = this.catDateFin();
		if(fin!=null) {
			return FORMAT_DATE.format(fin);
		}
		else {
			return null;
		}
	}
	
	
	
	public Number pkCatId() {
		NSDictionary pk = EOUtilities.primaryKeyForObject(editingContext(), this);
		return (Number)pk.valueForKey(CAT_ID_KEY);
	}
	
	

	public Object valueForKey(java.lang.String arg0) {
		Object obj = null;
		try {
			obj = super.valueForKey(arg0);
		}
		catch (Throwable e) {
			try {
				int i = 0;
				for (NSArray list = (com.webobjects.foundation.NSArray) cataloguePrestationWebs().valueForKey(arg0); (obj == null || obj
						.equals(NullValue))
						&& list.count() > i; i++)
					obj = list.objectAtIndex(i);
			}
			catch (Throwable e2) {
				obj = NullValue;
			}
		}
		return obj;
	}

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * 
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

}
