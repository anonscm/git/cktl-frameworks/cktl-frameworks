/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartPersonneAdresse.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EORepartPersonneAdresse extends  EOGenericRecord {
	public static final String ENTITY_NAME = "RepartPersonneAdresse";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_repart_personne_adresse";



	// Attributes


	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_BP_KEY = "adrBp";
	public static final String ADRESSE_SHORT_KEY = "adresseShort";
	public static final String BIS_TER_KEY = "bisTer";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String C_VOIE_KEY = "cVoie";
	public static final String E_MAIL_KEY = "eMail";
	public static final String HABITANT_CHEZ_KEY = "habitantChez";
	public static final String LC_PAYS_KEY = "lcPays";
	public static final String LL_PAYS_KEY = "llPays";
	public static final String LOCALITE_KEY = "localite";
	public static final String NOM_VOIE_KEY = "nomVoie";
	public static final String NO_VOIE_KEY = "noVoie";
	public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
	public static final String TADR_CODE_KEY = "tadrCode";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_BP_COLKEY = "ADR_BP";
	public static final String ADRESSE_SHORT_COLKEY = "$attribute.columnName";
	public static final String BIS_TER_COLKEY = "BIS_TER";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String C_VOIE_COLKEY = "C_VOIE";
	public static final String E_MAIL_COLKEY = "E_MAIL";
	public static final String HABITANT_CHEZ_COLKEY = "HABITANT_CHEZ";
	public static final String LC_PAYS_COLKEY = "LC_PAYS";
	public static final String LL_PAYS_COLKEY = "LL_PAYS";
	public static final String LOCALITE_COLKEY = "LOCALITE";
	public static final String NOM_VOIE_COLKEY = "NOM_VOIE";
	public static final String NO_VOIE_COLKEY = "NO_VOIE";
	public static final String RPA_PRINCIPAL_COLKEY = "RPA_PRINCIPAL";
	public static final String TADR_CODE_COLKEY = "TADR_CODE";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String ADRESSE_KEY = "adresse";
	public static final String PERSONNE_KEY = "personne";
	public static final String TYPE_ADRESSE_KEY = "typeAdresse";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrBp() {
    return (String) storedValueForKey(ADR_BP_KEY);
  }

  public void setAdrBp(String value) {
    takeStoredValueForKey(value, ADR_BP_KEY);
  }

  public String adresseShort() {
    return (String) storedValueForKey(ADRESSE_SHORT_KEY);
  }

  public void setAdresseShort(String value) {
    takeStoredValueForKey(value, ADRESSE_SHORT_KEY);
  }

  public String bisTer() {
    return (String) storedValueForKey(BIS_TER_KEY);
  }

  public void setBisTer(String value) {
    takeStoredValueForKey(value, BIS_TER_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public String cpEtranger() {
    return (String) storedValueForKey(CP_ETRANGER_KEY);
  }

  public void setCpEtranger(String value) {
    takeStoredValueForKey(value, CP_ETRANGER_KEY);
  }

  public String cVoie() {
    return (String) storedValueForKey(C_VOIE_KEY);
  }

  public void setCVoie(String value) {
    takeStoredValueForKey(value, C_VOIE_KEY);
  }

  public String eMail() {
    return (String) storedValueForKey(E_MAIL_KEY);
  }

  public void setEMail(String value) {
    takeStoredValueForKey(value, E_MAIL_KEY);
  }

  public String habitantChez() {
    return (String) storedValueForKey(HABITANT_CHEZ_KEY);
  }

  public void setHabitantChez(String value) {
    takeStoredValueForKey(value, HABITANT_CHEZ_KEY);
  }

  public String lcPays() {
    return (String) storedValueForKey(LC_PAYS_KEY);
  }

  public void setLcPays(String value) {
    takeStoredValueForKey(value, LC_PAYS_KEY);
  }

  public String llPays() {
    return (String) storedValueForKey(LL_PAYS_KEY);
  }

  public void setLlPays(String value) {
    takeStoredValueForKey(value, LL_PAYS_KEY);
  }

  public String localite() {
    return (String) storedValueForKey(LOCALITE_KEY);
  }

  public void setLocalite(String value) {
    takeStoredValueForKey(value, LOCALITE_KEY);
  }

  public String nomVoie() {
    return (String) storedValueForKey(NOM_VOIE_KEY);
  }

  public void setNomVoie(String value) {
    takeStoredValueForKey(value, NOM_VOIE_KEY);
  }

  public String noVoie() {
    return (String) storedValueForKey(NO_VOIE_KEY);
  }

  public void setNoVoie(String value) {
    takeStoredValueForKey(value, NO_VOIE_KEY);
  }

  public String rpaPrincipal() {
    return (String) storedValueForKey(RPA_PRINCIPAL_KEY);
  }

  public void setRpaPrincipal(String value) {
    takeStoredValueForKey(value, RPA_PRINCIPAL_KEY);
  }

  public String tadrCode() {
    return (String) storedValueForKey(TADR_CODE_KEY);
  }

  public void setTadrCode(String value) {
    takeStoredValueForKey(value, TADR_CODE_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.kava.server.metier.EOAdresse adresse() {
    return (org.cocktail.kava.server.metier.EOAdresse)storedValueForKey(ADRESSE_KEY);
  }

  public void setAdresseRelationship(org.cocktail.kava.server.metier.EOAdresse value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOAdresse oldValue = adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ADRESSE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeAdresse typeAdresse() {
    return (org.cocktail.kava.server.metier.EOTypeAdresse)storedValueForKey(TYPE_ADRESSE_KEY);
  }

  public void setTypeAdresseRelationship(org.cocktail.kava.server.metier.EOTypeAdresse value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeAdresse oldValue = typeAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ADRESSE_KEY);
    }
  }
  

/**
 * Créer une instance de EORepartPersonneAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORepartPersonneAdresse createEORepartPersonneAdresse(EOEditingContext editingContext, String rpaPrincipal
, String tadrCode
			) {
    EORepartPersonneAdresse eo = (EORepartPersonneAdresse) createAndInsertInstance(editingContext, _EORepartPersonneAdresse.ENTITY_NAME);    
		eo.setRpaPrincipal(rpaPrincipal);
		eo.setTadrCode(tadrCode);
    return eo;
  }

  
	  public EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext) {
	  		return (EORepartPersonneAdresse)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartPersonneAdresse creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORepartPersonneAdresse creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EORepartPersonneAdresse object = (EORepartPersonneAdresse)createAndInsertInstance(editingContext, _EORepartPersonneAdresse.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext, EORepartPersonneAdresse eo) {
    EORepartPersonneAdresse localInstance = (eo == null) ? null : (EORepartPersonneAdresse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORepartPersonneAdresse#localInstanceIn a la place.
   */
	public static EORepartPersonneAdresse localInstanceOf(EOEditingContext editingContext, EORepartPersonneAdresse eo) {
		return EORepartPersonneAdresse.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORepartPersonneAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartPersonneAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EORepartPersonneAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORepartPersonneAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORepartPersonneAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORepartPersonneAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORepartPersonneAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORepartPersonneAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORepartPersonneAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORepartPersonneAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
