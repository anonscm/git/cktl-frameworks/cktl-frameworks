/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrestationBoutique.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOPrestationBoutique extends  EOGenericRecord {
	public static final String ENTITY_NAME = "PrestationBoutique";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.prestation_boutique";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pbId";

	public static final String BP_CODE_KEY = "bpCode";
	public static final String PB_EMAIL_CONTACT_KEY = "pbEmailContact";
	public static final String PB_LIBELLE_ENG_KEY = "pbLibelleEng";
	public static final String PB_LIBELLE_FRA_KEY = "pbLibelleFra";
	public static final String PB_VAL_CLIENT_KEY = "pbValClient";
	public static final String PB_VAL_CLOTURE_KEY = "pbValCloture";
	public static final String PB_VAL_PRESTATAIRE_KEY = "pbValPrestataire";

// Attributs non visibles
	public static final String PB_ID_KEY = "pbId";

//Colonnes dans la base de donnees
	public static final String BP_CODE_COLKEY = "PB_CODE";
	public static final String PB_EMAIL_CONTACT_COLKEY = "PB_EMAIL_CONTACT";
	public static final String PB_LIBELLE_ENG_COLKEY = "PB_LIBELLE_ENG";
	public static final String PB_LIBELLE_FRA_COLKEY = "PB_LIBELLE_FRA";
	public static final String PB_VAL_CLIENT_COLKEY = "PB_VAL_CLIENT";
	public static final String PB_VAL_CLOTURE_COLKEY = "PB_VAL_CLOTURE";
	public static final String PB_VAL_PRESTATAIRE_COLKEY = "PB_VAL_PRESTATAIRE";

	public static final String PB_ID_COLKEY = "PB_ID";


	// Relationships
	public static final String BOUTIQUE_CLIENTS_KEY = "boutiqueClients";
	public static final String V_EMAIL_RESPONSABLE_CATALOGUES_KEY = "vEmailResponsableCatalogues";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String bpCode() {
    return (String) storedValueForKey(BP_CODE_KEY);
  }

  public void setBpCode(String value) {
    takeStoredValueForKey(value, BP_CODE_KEY);
  }

  public String pbEmailContact() {
    return (String) storedValueForKey(PB_EMAIL_CONTACT_KEY);
  }

  public void setPbEmailContact(String value) {
    takeStoredValueForKey(value, PB_EMAIL_CONTACT_KEY);
  }

  public String pbLibelleEng() {
    return (String) storedValueForKey(PB_LIBELLE_ENG_KEY);
  }

  public void setPbLibelleEng(String value) {
    takeStoredValueForKey(value, PB_LIBELLE_ENG_KEY);
  }

  public String pbLibelleFra() {
    return (String) storedValueForKey(PB_LIBELLE_FRA_KEY);
  }

  public void setPbLibelleFra(String value) {
    takeStoredValueForKey(value, PB_LIBELLE_FRA_KEY);
  }

  public String pbValClient() {
    return (String) storedValueForKey(PB_VAL_CLIENT_KEY);
  }

  public void setPbValClient(String value) {
    takeStoredValueForKey(value, PB_VAL_CLIENT_KEY);
  }

  public String pbValCloture() {
    return (String) storedValueForKey(PB_VAL_CLOTURE_KEY);
  }

  public void setPbValCloture(String value) {
    takeStoredValueForKey(value, PB_VAL_CLOTURE_KEY);
  }

  public String pbValPrestataire() {
    return (String) storedValueForKey(PB_VAL_PRESTATAIRE_KEY);
  }

  public void setPbValPrestataire(String value) {
    takeStoredValueForKey(value, PB_VAL_PRESTATAIRE_KEY);
  }

  public NSArray boutiqueClients() {
    return (NSArray)storedValueForKey(BOUTIQUE_CLIENTS_KEY);
  }

  public NSArray boutiqueClients(EOQualifier qualifier) {
    return boutiqueClients(qualifier, null, false);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, boolean fetch) {
    return boutiqueClients(qualifier, null, fetch);
  }

  public NSArray boutiqueClients(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOBoutiqueClient.PRESTATION_BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOBoutiqueClient.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = boutiqueClients();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public void removeFromBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutiqueClient createBoutiqueClientsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BoutiqueClient");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BOUTIQUE_CLIENTS_KEY);
    return (org.cocktail.kava.server.metier.EOBoutiqueClient) eo;
  }

  public void deleteBoutiqueClientsRelationship(org.cocktail.kava.server.metier.EOBoutiqueClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUE_CLIENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBoutiqueClientsRelationships() {
    Enumeration objects = boutiqueClients().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBoutiqueClientsRelationship((org.cocktail.kava.server.metier.EOBoutiqueClient)objects.nextElement());
    }
  }

  public NSArray vEmailResponsableCatalogues() {
    return (NSArray)storedValueForKey(V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier) {
    return vEmailResponsableCatalogues(qualifier, null, false);
  }

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier, boolean fetch) {
    return vEmailResponsableCatalogues(qualifier, null, fetch);
  }

  public NSArray vEmailResponsableCatalogues(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue.PRESTATION_BOUTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = vEmailResponsableCatalogues();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    addObjectToBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }

  public void removeFromVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
  }

  public org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue createVEmailResponsableCataloguesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("VEmailResponsableCatalogue");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
    return (org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue) eo;
  }

  public void deleteVEmailResponsableCataloguesRelationship(org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, V_EMAIL_RESPONSABLE_CATALOGUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllVEmailResponsableCataloguesRelationships() {
    Enumeration objects = vEmailResponsableCatalogues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteVEmailResponsableCataloguesRelationship((org.cocktail.kava.server.metier.EOVEmailResponsableCatalogue)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrestationBoutique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrestationBoutique createEOPrestationBoutique(EOEditingContext editingContext, String pbEmailContact
, String pbValClient
, String pbValCloture
, String pbValPrestataire
			) {
    EOPrestationBoutique eo = (EOPrestationBoutique) createAndInsertInstance(editingContext, _EOPrestationBoutique.ENTITY_NAME);    
		eo.setPbEmailContact(pbEmailContact);
		eo.setPbValClient(pbValClient);
		eo.setPbValCloture(pbValCloture);
		eo.setPbValPrestataire(pbValPrestataire);
    return eo;
  }

  
	  public EOPrestationBoutique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrestationBoutique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrestationBoutique creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrestationBoutique creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPrestationBoutique object = (EOPrestationBoutique)createAndInsertInstance(editingContext, _EOPrestationBoutique.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrestationBoutique localInstanceIn(EOEditingContext editingContext, EOPrestationBoutique eo) {
    EOPrestationBoutique localInstance = (eo == null) ? null : (EOPrestationBoutique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrestationBoutique#localInstanceIn a la place.
   */
	public static EOPrestationBoutique localInstanceOf(EOEditingContext editingContext, EOPrestationBoutique eo) {
		return EOPrestationBoutique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrestationBoutique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrestationBoutique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrestationBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrestationBoutique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrestationBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrestationBoutique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrestationBoutique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrestationBoutique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrestationBoutique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrestationBoutique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrestationBoutique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrestationBoutique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
