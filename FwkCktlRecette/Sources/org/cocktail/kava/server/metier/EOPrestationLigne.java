// EOPrestationLigne.java
//

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;
import java.util.Collections;

import org.cocktail.kava.server.factory.FactoryPrestationLigne;
import org.cocktail.kava.server.finder.FinderCatalogueArticle;
import org.cocktail.kava.server.finder.FinderTypeArticle;
import org.cocktail.kava.server.finder.FinderTypeEtat;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;
import org.cocktail.pieFwk.common.metier.Prestation;
import org.cocktail.pieFwk.common.metier.PrestationLigne;
import org.cocktail.pieFwk.common.metier.PrestationLigneCompanion;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPrestationLigne extends _EOPrestationLigne implements PrestationLigne {
	public static final String	PRLIG_TOTAL_TVA_KEY			= "prligTotalTva";
	public static final String	PRLIG_TOTAL_RESTE_TVA_KEY	= "prligTotalResteTva";

	private PrestationLigneCompanion companion;

	public EOPrestationLigne() {
		super();
		this.companion = new PrestationLigneCompanion(this);
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise HT
	 */
	public void setPrligArtHt(BigDecimal aValue) {
		super.setPrligArtHt(aValue);
		companion().updateTotalHt();
		companion().updateTotalResteHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total remise TTC
	 */
	public void setPrligArtTtc(BigDecimal aValue) {
		super.setPrligArtTtc(aValue);
		companion().updateTotalTtc();
		companion().updateTotalResteTtc();
	}


	/**
	 * @param aValue
	 *            Met a jour la quantite, la quantite restante, recalcule les totaux remises HT et TTC, et met a jour les options/remises, et
	 *            fait le cafe, le menage et les devoirs des enfants... pfff....
	 */
	public void setPrligQuantite(EOEditingContext ec, BigDecimal aValue) {
		super.setPrligQuantite(aValue);
		super.setPrligQuantiteReste(aValue);
		companion().updateTotaux();

		// mise a jour des remises quantitatives possibles
		if (catalogueArticle() == null
				|| catalogueArticle().article() == null
				|| catalogueArticle().article().articles() == null
				|| catalogueArticle().article().articles().count() == 0) {
			return;
		}
		updateOptionsRemises(ec);
	}

	/**
	 * @param aValue
	 *            Met a jour la quantite restante et recalcule les totaux restants remises HT et TTC
	 */
	public void setPrligQuantiteReste(BigDecimal aValue) {
		super.setPrligQuantiteReste(aValue);
		companion().updateTotalResteHt();
		companion().updateTotalResteTtc();
	}

	public void alignementQuantiteResteDesAvenantsALArticle() {
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i)).setPrligQuantiteReste(prligQuantiteReste());
			}
		}
	}

	private void updateOptionsRemises(EOEditingContext ec) {
		if (catalogueArticle() == null) {
			return;
		}
		// recherche des remises quantitatives valides pour le type de public
		NSArray remises = FinderCatalogueArticle.find(editingContext(), catalogueArticle(),
				catalogueArticle().article().articlePrestation().typePublic(),
				FinderTypeEtat.typeEtatValide(ec),
				Collections.singleton(FinderTypeArticle.typeArticleRemise(ec)), prligQuantite());

		suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(ec, remises);
		ajoutAvenantsDevenusNecessaires(remises);
	}

	private void ajoutAvenantsDevenusNecessaires(NSArray remises) {
		// ajout des nouvelles qui n'y sont pas encore
		if (remises != null && remises.count() > 0) {
			for (int i = 0; i < remises.count(); i++) {
				EOCatalogueArticle catalogueArticle = (EOCatalogueArticle) remises.objectAtIndex(i);
				if (!((NSArray) prestationLignes().valueForKey(EOPrestationLigne.CATALOGUE_ARTICLE_KEY)).containsObject(catalogueArticle)) {
					FactoryPrestationLigne.newObject(editingContext(), this, catalogueArticle);
				}
			}
		}
	}

	/**
	 * suppression de celles qui ne doivent plus y etre, mise a jour de la quantite des autres
	 *
	 * @param ec
	 * @param remises
	 */
	private void suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(EOEditingContext ec,
			NSArray remises) {
		suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(this, ec, remises);
	}

	public static void suppressionRemisesDevenusInutilesEtMiseAJourDesQuantites(EOPrestationLigne ligne, EOEditingContext ec,
			NSArray remises) {
		if (ligne.prestationLignes() == null) { return; }

		int nbRemisesTraitees = 0;
		while (ligne.prestationLignes().count() > nbRemisesTraitees ) {
			EOPrestationLigne prestation = (EOPrestationLigne) ligne.prestationLignes().objectAtIndex(nbRemisesTraitees);
			if (ligne.isPrestationUneRemise(ec, prestation)) {
				if (ligne.isRemiseEncoreValide(remises, prestation)) {
					prestation.setPrligQuantite(ligne.prligQuantite());
					nbRemisesTraitees++;
				}
				else {
					ligne.removeFromPrestationLignesRelationship(prestation);
					ligne.editingContext().deleteObject(prestation);
				}
			}
			else {
				prestation.setPrligQuantite(ligne.prligQuantite());
				nbRemisesTraitees++;
			}
		}
	}


	protected boolean isRemiseEncoreValide(NSArray remises,
			EOPrestationLigne prestation) {
		return remises != null && remises.containsObject(prestation.catalogueArticle());
	}

	protected boolean isPrestationUneRemise(EOEditingContext ec,
			EOPrestationLigne prestation) {
		return prestation.catalogueArticle().article().typeArticle().equals(FinderTypeArticle.typeArticleRemise(ec));
	}

	public BigDecimal prligTotalTva() {
		if (prligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalHt() == null) {
			return prligTotalTtc();
		}
		return prligTotalTtc().subtract(prligTotalHt());
	}

	public BigDecimal prligTotalResteTva() {
		if (prligTotalResteTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (prligTotalResteHt() == null) {
			return prligTotalResteTtc();
		}
		return prligTotalResteTtc().subtract(prligTotalResteHt());
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}

	public PrestationLigneCompanion companion() {
		return this.companion;
	}

	public Prestation getPrestationCommon() {
		return prestation();
	}

	public CatalogueArticle getCatalogueArticleCommon() {
		return catalogueArticle();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) WOApplication.application();
	}

	public Number exerciceAsNumber() {
		if (prestation() != null && prestation().exercice() != null) {
			return prestation().exercice().exeExercice();
		}
		return null;
	}
}
