// EOFacturePapierLigne.java
//

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.FacturePapierLigne;
import org.cocktail.pieFwk.common.metier.FacturePapierLigneCompanion;
import org.cocktail.pieFwk.common.metier.PrestationLigne;
import org.cocktail.pieFwk.common.metier.TauxTva;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSValidation;

public class EOFacturePapierLigne extends _EOFacturePapierLigne implements FacturePapierLigne {

	/** Serial version ID. */
	private static final long serialVersionUID = 1L;

	public static final String	FLIG_TOTAL_TVA_KEY	= "fligTotalTva";

	private FacturePapierLigneCompanion companion;

	public EOFacturePapierLigne() {
		super();
		this.companion = new FacturePapierLigneCompanion(this);
	}

	public FacturePapierLigneCompanion companion() {
		return this.companion;
	}

	public void setTvaRelationship(EOTva value) {
		super.setTvaRelationship(value);
		companion.updateTotaux();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire HT et recalcule le total remise HT
	 */
	public void setFligArtHt(BigDecimal aValue) {
		super.setFligArtHt(aValue);
		companion.updateTotalHt();
	}

	/**
	 * @param aValue
	 *            Met a jour le montant unitaire TTC et recalcule le total remise TTC
	 */
	public void setFligArtTtc(BigDecimal aValue) {
		super.setFligArtTtc(aValue);
		companion.updateTotalTtc();
	}

	public BigDecimal fligTotalTva() {
		if (fligTotalTtc() == null) {
			return new BigDecimal(0.0);
		}
		if (fligTotalHt() == null) {
			return fligTotalTtc();
		}
		return fligTotalTtc().subtract(fligTotalHt());
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Methode actuellement utilisée uniquement dans les Tests par l'implementation Mock de l'interface.
	 * Ici on redirige vers la classe parente en castant.
	 */
	public void addToFacturePapierLignesRelationship(FacturePapierLigne ligne) {
		this.addToFacturePapierLignesRelationship((EOFacturePapierLigne) ligne);
	}

	public FacturePapier getFacturePapierCommon() {
		return facturePapier();
	}

	public PrestationLigne getPrestationLigneCommon() {
		return prestationLigne();
	}

	public TauxTva tauxTva() {
		return tva();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) WOApplication.application();
	}

	public Number exerciceAsNumber() {
		if (facturePapier() != null && facturePapier().exercice() != null) {
			return facturePapier().exercice().exeExercice();
		}
		return null;
	}
}
