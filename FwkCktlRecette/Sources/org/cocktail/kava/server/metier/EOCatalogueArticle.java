
// EOCatalogueArticle.java
//

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.metier.Article;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOCatalogueArticle extends _EOCatalogueArticle implements CatalogueArticle {

	private Number caarId;

	private static final EOSortOrdering SORT_LIBELLE = EOSortOrdering
			.sortOrderingWithKey(EOCatalogueArticle.ARTICLE_KEY + "."
					+ EOArticle.ART_LIBELLE_KEY,
					EOSortOrdering.CompareAscending);

	public static NSArray sortedCatalogueArticlesByLibelle(NSArray catArticles) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(catArticles,
				new NSArray(SORT_LIBELLE));
	}



	/* retourne la cle primaire */
	public Number getPkCaarId() {
		if(caarId==null) {
		NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(),this);
			if(dico!=null) {
				caarId = (Number)dico.objectForKey(EOCatalogueArticle.CAAR_ID_KEY);
			}
		}
		return caarId;
	}



	public EOCatalogueArticle() {
		super();
	}

	/*
	 * // If you add instance variables to store property values you // should
	 * add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the
	 * superclass). private void writeObject(java.io.ObjectOutputStream out)
	 * throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws
	 * java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
	}



	public BigDecimal tauxTVA() {
		if (tva() == null) {
			return null;
		}
		return tva().tvaTaux();
	}

	public Article getArticleCommon() {
		return article();
	}
}
