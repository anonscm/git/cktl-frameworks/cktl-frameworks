

// EOPrestationBoutique.java
// 

package org.cocktail.kava.server.metier;


import java.util.Enumeration;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPrestationBoutique extends _EOPrestationBoutique {
	
	
    public EOPrestationBoutique() {
        super();
    }
    
    public boolean isAutoValPrestaClient() {
    	return pbValClient().equals("N");
    }
    
    public boolean isAutoValPrestaPrestataire() {
    	return pbValPrestataire().equals("N");
    }

	public boolean isAutoCloturePrestation() {
		return pbValCloture().equals("N");
	}
	
	public NSArray responsablesCatalogues() {
		NSMutableArray array = new NSMutableArray();
		NSArray vEmailResp = vEmailResponsableCatalogues();
		System.out.println("avant : "+vEmailResp.count());
		NSArray tmp;
		EOVEmailResponsableCatalogue enumElement;
		for (Enumeration e = vEmailResp.objectEnumerator() ; e.hasMoreElements() ;) {
			enumElement = (EOVEmailResponsableCatalogue)e.nextElement();
			tmp = (NSArray)array.valueForKey( EOVEmailResponsableCatalogue.PERS_ID_KEY );
	       if( !tmp.contains( enumElement.persId() ) ) {
	    	   array.addObject( enumElement );
	       }
		 }
		System.out.println("apres : "+array.count());
		return array;
	}
	
	
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }





}
