/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOEngageBudget.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOEngageBudget extends  EOGenericRecord {
	public static final String ENTITY_NAME = "EngageBudget";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_engage_budget";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "engId";

	public static final String ENG_DATE_SAISIE_KEY = "engDateSaisie";
	public static final String ENG_HT_SAISIE_KEY = "engHtSaisie";
	public static final String ENG_LIBELLE_KEY = "engLibelle";
	public static final String ENG_MONTANT_BUDGETAIRE_KEY = "engMontantBudgetaire";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_KEY = "engMontantBudgetaireReste";
	public static final String ENG_NUMERO_KEY = "engNumero";
	public static final String ENG_TTC_SAISIE_KEY = "engTtcSaisie";
	public static final String ENG_TVA_SAISIE_KEY = "engTvaSaisie";
	public static final String FOU_ORDRE_KEY = "fouOrdre";

// Attributs non visibles
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ENG_DATE_SAISIE_COLKEY = "ENG_DATE_SAISIE";
	public static final String ENG_HT_SAISIE_COLKEY = "ENG_HT_SAISIE";
	public static final String ENG_LIBELLE_COLKEY = "ENG_LIBELLE";
	public static final String ENG_MONTANT_BUDGETAIRE_COLKEY = "ENG_MONTANT_BUDGETAIRE";
	public static final String ENG_MONTANT_BUDGETAIRE_RESTE_COLKEY = "ENG_MONTANT_BUDGETAIRE_RESTE";
	public static final String ENG_NUMERO_COLKEY = "ENG_NUMERO";
	public static final String ENG_TTC_SAISIE_COLKEY = "ENG_TTC_SAISIE";
	public static final String ENG_TVA_SAISIE_COLKEY = "ENG_TVA_SAISIE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";

	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String ENGAGE_CTRL_ACTIONS_KEY = "engageCtrlActions";
	public static final String ENGAGE_CTRL_ANALYTIQUE_KEY = "engageCtrlAnalytique";
	public static final String ENGAGE_CTRL_CONVENTIONS_KEY = "engageCtrlConventions";
	public static final String ENGAGE_CTRL_PLANCOS_KEY = "engageCtrlPlancos";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_CREDIT_DEP_KEY = "typeCreditDep";
	public static final String UTILISATEUR_KEY = "utilisateur";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp engDateSaisie() {
    return (NSTimestamp) storedValueForKey(ENG_DATE_SAISIE_KEY);
  }

  public void setEngDateSaisie(NSTimestamp value) {
    takeStoredValueForKey(value, ENG_DATE_SAISIE_KEY);
  }

  public java.math.BigDecimal engHtSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_HT_SAISIE_KEY);
  }

  public void setEngHtSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_HT_SAISIE_KEY);
  }

  public String engLibelle() {
    return (String) storedValueForKey(ENG_LIBELLE_KEY);
  }

  public void setEngLibelle(String value) {
    takeStoredValueForKey(value, ENG_LIBELLE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public void setEngMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_KEY);
  }

  public java.math.BigDecimal engMontantBudgetaireReste() {
    return (java.math.BigDecimal) storedValueForKey(ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public void setEngMontantBudgetaireReste(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_MONTANT_BUDGETAIRE_RESTE_KEY);
  }

  public Integer engNumero() {
    return (Integer) storedValueForKey(ENG_NUMERO_KEY);
  }

  public void setEngNumero(Integer value) {
    takeStoredValueForKey(value, ENG_NUMERO_KEY);
  }

  public java.math.BigDecimal engTtcSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TTC_SAISIE_KEY);
  }

  public void setEngTtcSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TTC_SAISIE_KEY);
  }

  public java.math.BigDecimal engTvaSaisie() {
    return (java.math.BigDecimal) storedValueForKey(ENG_TVA_SAISIE_KEY);
  }

  public void setEngTvaSaisie(java.math.BigDecimal value) {
    takeStoredValueForKey(value, ENG_TVA_SAISIE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan organ() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.kava.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.kava.server.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.kava.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.kava.server.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCreditDep() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_DEP_KEY);
  }

  public void setTypeCreditDepRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCreditDep();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_DEP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_DEP_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray engageCtrlActions() {
    return (NSArray)storedValueForKey(ENGAGE_CTRL_ACTIONS_KEY);
  }

  public NSArray engageCtrlActions(EOQualifier qualifier) {
    return engageCtrlActions(qualifier, null);
  }

  public NSArray engageCtrlActions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = engageCtrlActions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEngageCtrlActionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ACTIONS_KEY);
  }

  public void removeFromEngageCtrlActionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ACTIONS_KEY);
  }

  public org.cocktail.kava.server.metier.EOEngageCtrlAction createEngageCtrlActionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlAction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGE_CTRL_ACTIONS_KEY);
    return (org.cocktail.kava.server.metier.EOEngageCtrlAction) eo;
  }

  public void deleteEngageCtrlActionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ACTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngageCtrlActionsRelationships() {
    Enumeration objects = engageCtrlActions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngageCtrlActionsRelationship((org.cocktail.kava.server.metier.EOEngageCtrlAction)objects.nextElement());
    }
  }

  public NSArray engageCtrlAnalytique() {
    return (NSArray)storedValueForKey(ENGAGE_CTRL_ANALYTIQUE_KEY);
  }

  public NSArray engageCtrlAnalytique(EOQualifier qualifier) {
    return engageCtrlAnalytique(qualifier, null);
  }

  public NSArray engageCtrlAnalytique(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = engageCtrlAnalytique();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEngageCtrlAnalytiqueRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ANALYTIQUE_KEY);
  }

  public void removeFromEngageCtrlAnalytiqueRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ANALYTIQUE_KEY);
  }

  public org.cocktail.kava.server.metier.EOEngageCtrlAnalytique createEngageCtrlAnalytiqueRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGE_CTRL_ANALYTIQUE_KEY);
    return (org.cocktail.kava.server.metier.EOEngageCtrlAnalytique) eo;
  }

  public void deleteEngageCtrlAnalytiqueRelationship(org.cocktail.kava.server.metier.EOEngageCtrlAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_ANALYTIQUE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngageCtrlAnalytiqueRelationships() {
    Enumeration objects = engageCtrlAnalytique().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngageCtrlAnalytiqueRelationship((org.cocktail.kava.server.metier.EOEngageCtrlAnalytique)objects.nextElement());
    }
  }

  public NSArray engageCtrlConventions() {
    return (NSArray)storedValueForKey(ENGAGE_CTRL_CONVENTIONS_KEY);
  }

  public NSArray engageCtrlConventions(EOQualifier qualifier) {
    return engageCtrlConventions(qualifier, null);
  }

  public NSArray engageCtrlConventions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = engageCtrlConventions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEngageCtrlConventionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlConvention object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_CONVENTIONS_KEY);
  }

  public void removeFromEngageCtrlConventionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_CONVENTIONS_KEY);
  }

  public org.cocktail.kava.server.metier.EOEngageCtrlConvention createEngageCtrlConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlConvention");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGE_CTRL_CONVENTIONS_KEY);
    return (org.cocktail.kava.server.metier.EOEngageCtrlConvention) eo;
  }

  public void deleteEngageCtrlConventionsRelationship(org.cocktail.kava.server.metier.EOEngageCtrlConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngageCtrlConventionsRelationships() {
    Enumeration objects = engageCtrlConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngageCtrlConventionsRelationship((org.cocktail.kava.server.metier.EOEngageCtrlConvention)objects.nextElement());
    }
  }

  public NSArray engageCtrlPlancos() {
    return (NSArray)storedValueForKey(ENGAGE_CTRL_PLANCOS_KEY);
  }

  public NSArray engageCtrlPlancos(EOQualifier qualifier) {
    return engageCtrlPlancos(qualifier, null);
  }

  public NSArray engageCtrlPlancos(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = engageCtrlPlancos();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEngageCtrlPlancosRelationship(org.cocktail.kava.server.metier.EOEngageCtrlPlanco object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_PLANCOS_KEY);
  }

  public void removeFromEngageCtrlPlancosRelationship(org.cocktail.kava.server.metier.EOEngageCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_PLANCOS_KEY);
  }

  public org.cocktail.kava.server.metier.EOEngageCtrlPlanco createEngageCtrlPlancosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EngageCtrlPlanco");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ENGAGE_CTRL_PLANCOS_KEY);
    return (org.cocktail.kava.server.metier.EOEngageCtrlPlanco) eo;
  }

  public void deleteEngageCtrlPlancosRelationship(org.cocktail.kava.server.metier.EOEngageCtrlPlanco object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ENGAGE_CTRL_PLANCOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngageCtrlPlancosRelationships() {
    Enumeration objects = engageCtrlPlancos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngageCtrlPlancosRelationship((org.cocktail.kava.server.metier.EOEngageCtrlPlanco)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOEngageBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOEngageBudget createEOEngageBudget(EOEditingContext editingContext, NSTimestamp engDateSaisie
, java.math.BigDecimal engHtSaisie
, String engLibelle
, java.math.BigDecimal engMontantBudgetaire
, java.math.BigDecimal engMontantBudgetaireReste
, Integer engNumero
, java.math.BigDecimal engTtcSaisie
, java.math.BigDecimal engTvaSaisie
, Integer fouOrdre
			) {
    EOEngageBudget eo = (EOEngageBudget) createAndInsertInstance(editingContext, _EOEngageBudget.ENTITY_NAME);    
		eo.setEngDateSaisie(engDateSaisie);
		eo.setEngHtSaisie(engHtSaisie);
		eo.setEngLibelle(engLibelle);
		eo.setEngMontantBudgetaire(engMontantBudgetaire);
		eo.setEngMontantBudgetaireReste(engMontantBudgetaireReste);
		eo.setEngNumero(engNumero);
		eo.setEngTtcSaisie(engTtcSaisie);
		eo.setEngTvaSaisie(engTvaSaisie);
		eo.setFouOrdre(fouOrdre);
    return eo;
  }

  
	  public EOEngageBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOEngageBudget)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngageBudget creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOEngageBudget creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOEngageBudget object = (EOEngageBudget)createAndInsertInstance(editingContext, _EOEngageBudget.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOEngageBudget localInstanceIn(EOEditingContext editingContext, EOEngageBudget eo) {
    EOEngageBudget localInstance = (eo == null) ? null : (EOEngageBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOEngageBudget#localInstanceIn a la place.
   */
	public static EOEngageBudget localInstanceOf(EOEditingContext editingContext, EOEngageBudget eo) {
		return EOEngageBudget.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOEngageBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOEngageBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOEngageBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOEngageBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOEngageBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOEngageBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOEngageBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOEngageBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOEngageBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOEngageBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOEngageBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOEngageBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
