

// EOAdresse.java
// 

package org.cocktail.kava.server.metier;


import org.cocktail.application.serveur.eof.EOPays;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOAdresse extends _EOAdresse {
	
	public static final String CPAYS_FRANCE = "100";
	
	EOPays paysFrance = null;
	
	private String cpCache,lPaysCache;
	
    public EOAdresse() {
        super();
    }
    
    
    public EOPays getPaysFrance() {
    	if(paysFrance==null && editingContext()!=null) {
	    	EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(EOPays.C_PAYS_KEY + "='" + CPAYS_FRANCE + "'", null);
	    	EOFetchSpecification myFetch = new EOFetchSpecification(EOPays.ENTITY_NAME,qualifier,null);
	        NSArray objets = editingContext().objectsWithFetchSpecification(myFetch);
	        if(objets.count()>0) {
	        	paysFrance = (EOPays)objets.objectAtIndex(0);
	        }
    	}
        return paysFrance;
    }
    
    
    
    public boolean isNotEtranger() {
    	System.out.println("Adresse isNotEtranger: "+pays().cPays().equals( CPAYS_FRANCE ));
    	return pays().cPays().equals( CPAYS_FRANCE );
    }
    
    
    public boolean isEtranger() {
    	return !isNotEtranger();
    }
    
	public void setCPCache(String codePostal) {
		cpCache = codePostal;
		if (codePostal == null) {
			setCodePostal(null);
			setCpEtranger(null);
		}

		parseCPCache();
	}

	/**
	 * @return codePostal ou cpEtranger suivant celui saisi.
	 */
	public String getCPCache() {
		if (cpCache == null) {
			cpCache = codePostal();
		}
		if (cpCache == null) {
			cpCache = cpEtranger();
		}
		return cpCache;
	}

	public void setLPaysCache(String lPays) {
		lPaysCache = lPays;
		if (lPaysCache != null) {
			setPaysRelationship(null);
		}

		//parseCPCache();
	}

	public String getLPaysCache() {
		if (lPaysCache == null && pays() != null) {
			lPaysCache = pays().llPays();
		}
		return lPaysCache;
	}

	/**
	 * Analyse CPCache() et rempli le champ CodePostal ou CPEtranger suivant le pays.
	 */
	protected void parseCPCache() {

	}

	public void setToPaysRelationship(EOPays value) {
		super.setPaysRelationship(value);
		if (value != null) {
			//			setLPaysCache(value.llPays());
			lPaysCache = value.llPays();
		}

		parseCPCache();
	}



	/**
	 * Utilisez plutot {@link EOAdresse#getCPCache()}.
	 */
	@Override
	public String codePostal() {
		return super.codePostal();
	}

	/**
	 * Utilisez plutot {@link EOAdresse#getCPCache()}.
	 */
	@Override
	public String cpEtranger() {
		return super.cpEtranger();
	}

	/**
	 * Utilisez plutot {@link EOAdresse#setCPCache(String)}.
	 */
	@Override
	public void setCodePostal(String value) {
		super.setCodePostal(value);
	}

	/**
	 * Utilisez plutot {@link EOAdresse#setCPCache(String)}.
	 */
	@Override
	public void setCpEtranger(String value) {
		super.setCpEtranger(value);
	}

	/**
	 * Convertie en Majuscules sans accents.
	 */
	public void setVille(String value) {
		super.setVille(value);
	}



	
	
	
	
	public void awakeFromInsertion(EOEditingContext arg0) {
		super.awakeFromInsertion(arg0);
	}


    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelee.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
