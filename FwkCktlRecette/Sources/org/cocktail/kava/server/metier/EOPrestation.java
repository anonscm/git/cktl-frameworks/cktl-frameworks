// EOPrestation.java
//

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.application.serveur.eof.EOExercice;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.Prestation;
import org.cocktail.pieFwk.common.metier.PrestationCompanion;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOPrestation extends _EOPrestation implements Prestation {

	private PrestationCompanion companion;

	public EOPrestation() {
		super();
		this.companion = new PrestationCompanion(this);
	}

	public void updateTauxProrata() {
		if (tauxProrata() == null) {
			setTauxProrataRelationship(FinderTauxProrata.tauxProrata100(this.editingContext()));
		}
	}

	/**
	 * @return prestation adresse en cours.
	 */
	public EOPrestationAdrClient currentPrestationAdresseClient() {
		EOPrestationAdrClient result = null;
		NSArray sortedAdr = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				toPrestationAdrClients(), new NSArray(new Object[] {EOPrestationAdrClient.SORT_DATE_CREATION_DESC}));
		if (sortedAdr != null && sortedAdr.count() > 0) {
			result = (EOPrestationAdrClient) sortedAdr.objectAtIndex(0);
		}
		return result;
	}

	/** Génération d'un numero de prestation en fonction de l'exercice à partir de la procedure stockée. */
	public static Integer generatePrestaNumero(EOExercice exercice,EOEditingContext ec) {

		Integer exeOrdre = (Integer)exercice.valueForKey(EOExercice.EXE_ORDRE_KEY);
		NSMutableDictionary dico = new NSMutableDictionary();
		dico.setObjectForKey(exeOrdre, "010aExeOrdre");
		dico.setObjectForKey(NullValue, "020aGesCode");
		dico.setObjectForKey(EOPrestation.ENTITY_NAME.toUpperCase(), "030aTnuEntite");
		NSDictionary result = null;
		try {
			result = EOUtilities.executeStoredProcedureNamed(ec, "GetNumerotationProc", dico);
		}
		catch (Throwable e) {
			e.printStackTrace();
			ec.revert();
			return null;
		}
		if (result == null) {
			return null;
		}
		else {
			Integer i = (Integer) result.objectForKey("040aNumNumero");
			System.out.println("prestaNumero=" + i);
			return i;
		}
	}


	public void setPrestRemiseGlobale(BigDecimal aValue) {
		super.setPrestRemiseGlobale(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				((EOPrestationLigne) prestationLignes().objectAtIndex(i)).companion().updateTotaux();
			}
		}
	}

	/**
	 * @param aValue
	 *            (O/N) applique ou non la tva, et met a jour les lignes de la prestation, et recalcule les totaux
	 */
	public void setPrestApplyTva(String aValue) {
		super.setPrestApplyTva(aValue);
		if (prestationLignes() != null) {
			for (int i = 0; i < prestationLignes().count(); i++) {
				EOPrestationLigne pl = (EOPrestationLigne) prestationLignes().objectAtIndex(i);
				if ("N".equalsIgnoreCase(prestApplyTva())) {
					pl.setTvaRelationship(null);
					pl.setPrligArtTtc(pl.prligArtHt());
				}
				else {
					pl.setTvaRelationship(pl.tvaInitial());
					pl.setPrligArtTtc(pl.prligArtTtcInitial());
				}
			}
		}
	}

	/**
	 * verifie si les informations obligatoires sont la pour valider cote client (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidableClient(EOEditingContext ec) {
		if (!typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(ec))) {
			return true;
		}
		if (prestationBudgetClient() == null || prestationBudgetClient().organ() == null || prestationBudgetClient().tauxProrata() == null
				|| prestationBudgetClient().typeCreditDep() == null || prestationBudgetClient().lolfNomenclatureDepense() == null
				|| prestationBudgetClient().pcoNum() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isValidablePrest(EOEditingContext ec) {
		if (organ() == null || typeCreditRec() == null || lolfNomenclatureRecette() == null) {
			return false;
		}
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour cloturer la prestation (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isCloturable(EOEditingContext ec) {
		return true;
	}

	/**
	 * verifie si les infos obligatoires sont la pour facturer la prestation (peu importe son etape actuelle de validation)
	 *
	 * @return
	 */
	public boolean isFacturable(EOEditingContext ec) {
		return true;
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		updateTauxProrata();
		companion().updateTotaux();
	}

	public BigDecimal remiseGlobale() {
		return prestRemiseGlobale();
	}

	public PrestationCompanion companion() {
		return this.companion;
	}

	public void setTotalHT(BigDecimal totalHT) {
		setPrestTotalHt(totalHT);
	}

	public void setTotalTVA(BigDecimal totalTVA) {
		setPrestTotalTva(totalTVA);
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		setPrestTotalTtc(totalTTC);
	}

	public NSArray getPrestationLignesCommon() {
		return prestationLignes();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) WOApplication.application();
	}

	public Number exerciceAsNumber() {
		if (exercice() != null) {
			return exercice().exeExercice();
		}
		return null;
	}

}
