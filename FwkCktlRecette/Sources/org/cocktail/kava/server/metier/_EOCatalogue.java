/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCatalogue.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCatalogue extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Catalogue";
	public static final String ENTITY_TABLE_NAME = "jefy_catalogue.catalogue";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "catId";

	public static final String CAT_COMMENTAIRE_KEY = "catCommentaire";
	public static final String CAT_DATE_DEBUT_KEY = "catDateDebut";
	public static final String CAT_DATE_FIN_KEY = "catDateFin";
	public static final String CAT_LIBELLE_KEY = "catLibelle";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String CAT_COMMENTAIRE_COLKEY = "CAT_COMMENTAIRE";
	public static final String CAT_DATE_DEBUT_COLKEY = "CAT_DATE_DEBUT";
	public static final String CAT_DATE_FIN_COLKEY = "CAT_DATE_FIN";
	public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String BOUTIQUES_CATALOGUES_KEY = "boutiquesCatalogues";
	public static final String CATALOGUE_ARTICLES_KEY = "catalogueArticles";
	public static final String CATALOGUE_PRESTATION_KEY = "cataloguePrestation";
	public static final String CATALOGUE_PRESTATION_WEBS_KEY = "cataloguePrestationWebs";
	public static final String CATALOGUE_PUBLICS_KEY = "cataloguePublics";
	public static final String CATALOGUE_RESPONSABLES_KEY = "catalogueResponsables";
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_ETAT_KEY = "typeEtat";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String catCommentaire() {
    return (String) storedValueForKey(CAT_COMMENTAIRE_KEY);
  }

  public void setCatCommentaire(String value) {
    takeStoredValueForKey(value, CAT_COMMENTAIRE_KEY);
  }

  public NSTimestamp catDateDebut() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_DEBUT_KEY);
  }

  public void setCatDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_DEBUT_KEY);
  }

  public NSTimestamp catDateFin() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_FIN_KEY);
  }

  public void setCatDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_FIN_KEY);
  }

  public String catLibelle() {
    return (String) storedValueForKey(CAT_LIBELLE_KEY);
  }

  public void setCatLibelle(String value) {
    takeStoredValueForKey(value, CAT_LIBELLE_KEY);
  }

  public org.cocktail.kava.server.metier.EOCataloguePrestation cataloguePrestation() {
    return (org.cocktail.kava.server.metier.EOCataloguePrestation)storedValueForKey(CATALOGUE_PRESTATION_KEY);
  }

  public void setCataloguePrestationRelationship(org.cocktail.kava.server.metier.EOCataloguePrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCataloguePrestation oldValue = cataloguePrestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CATALOGUE_PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CATALOGUE_PRESTATION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOCodeMarche codeMarche() {
    return (org.cocktail.application.serveur.eof.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
  }

  public void setCodeMarcheRelationship(org.cocktail.application.serveur.eof.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOCodeMarche oldValue = codeMarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.server.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }

  public void setFournisUlrRelationship(org.cocktail.kava.server.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.kava.server.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }

  public void setTypeApplicationRelationship(org.cocktail.kava.server.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray boutiquesCatalogues() {
    return (NSArray)storedValueForKey(BOUTIQUES_CATALOGUES_KEY);
  }

  public NSArray boutiquesCatalogues(EOQualifier qualifier) {
    return boutiquesCatalogues(qualifier, null, false);
  }

  public NSArray boutiquesCatalogues(EOQualifier qualifier, boolean fetch) {
    return boutiquesCatalogues(qualifier, null, fetch);
  }

  public NSArray boutiquesCatalogues(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOBoutiqueCatalogue.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOBoutiqueCatalogue.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = boutiquesCatalogues();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToBoutiquesCataloguesRelationship(org.cocktail.kava.server.metier.EOBoutiqueCatalogue object) {
    addObjectToBothSidesOfRelationshipWithKey(object, BOUTIQUES_CATALOGUES_KEY);
  }

  public void removeFromBoutiquesCataloguesRelationship(org.cocktail.kava.server.metier.EOBoutiqueCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUES_CATALOGUES_KEY);
  }

  public org.cocktail.kava.server.metier.EOBoutiqueCatalogue createBoutiquesCataloguesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("BoutiqueCatalogue");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, BOUTIQUES_CATALOGUES_KEY);
    return (org.cocktail.kava.server.metier.EOBoutiqueCatalogue) eo;
  }

  public void deleteBoutiquesCataloguesRelationship(org.cocktail.kava.server.metier.EOBoutiqueCatalogue object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, BOUTIQUES_CATALOGUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBoutiquesCataloguesRelationships() {
    Enumeration objects = boutiquesCatalogues().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBoutiquesCataloguesRelationship((org.cocktail.kava.server.metier.EOBoutiqueCatalogue)objects.nextElement());
    }
  }

  public NSArray catalogueArticles() {
    return (NSArray)storedValueForKey(CATALOGUE_ARTICLES_KEY);
  }

  public NSArray catalogueArticles(EOQualifier qualifier) {
    return catalogueArticles(qualifier, null, false);
  }

  public NSArray catalogueArticles(EOQualifier qualifier, boolean fetch) {
    return catalogueArticles(qualifier, null, fetch);
  }

  public NSArray catalogueArticles(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOCatalogueArticle.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOCatalogueArticle.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = catalogueArticles();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCatalogueArticlesRelationship(org.cocktail.kava.server.metier.EOCatalogueArticle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLES_KEY);
  }

  public void removeFromCatalogueArticlesRelationship(org.cocktail.kava.server.metier.EOCatalogueArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLES_KEY);
  }

  public org.cocktail.kava.server.metier.EOCatalogueArticle createCatalogueArticlesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CatalogueArticle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CATALOGUE_ARTICLES_KEY);
    return (org.cocktail.kava.server.metier.EOCatalogueArticle) eo;
  }

  public void deleteCatalogueArticlesRelationship(org.cocktail.kava.server.metier.EOCatalogueArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_ARTICLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCatalogueArticlesRelationships() {
    Enumeration objects = catalogueArticles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCatalogueArticlesRelationship((org.cocktail.kava.server.metier.EOCatalogueArticle)objects.nextElement());
    }
  }

  public NSArray cataloguePrestationWebs() {
    return (NSArray)storedValueForKey(CATALOGUE_PRESTATION_WEBS_KEY);
  }

  public NSArray cataloguePrestationWebs(EOQualifier qualifier) {
    return cataloguePrestationWebs(qualifier, null, false);
  }

  public NSArray cataloguePrestationWebs(EOQualifier qualifier, boolean fetch) {
    return cataloguePrestationWebs(qualifier, null, fetch);
  }

  public NSArray cataloguePrestationWebs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOCataloguePrestationWeb.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOCataloguePrestationWeb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = cataloguePrestationWebs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCataloguePrestationWebsRelationship(org.cocktail.kava.server.metier.EOCataloguePrestationWeb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_WEBS_KEY);
  }

  public void removeFromCataloguePrestationWebsRelationship(org.cocktail.kava.server.metier.EOCataloguePrestationWeb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_WEBS_KEY);
  }

  public org.cocktail.kava.server.metier.EOCataloguePrestationWeb createCataloguePrestationWebsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CataloguePrestationWeb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CATALOGUE_PRESTATION_WEBS_KEY);
    return (org.cocktail.kava.server.metier.EOCataloguePrestationWeb) eo;
  }

  public void deleteCataloguePrestationWebsRelationship(org.cocktail.kava.server.metier.EOCataloguePrestationWeb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PRESTATION_WEBS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCataloguePrestationWebsRelationships() {
    Enumeration objects = cataloguePrestationWebs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCataloguePrestationWebsRelationship((org.cocktail.kava.server.metier.EOCataloguePrestationWeb)objects.nextElement());
    }
  }

  public NSArray cataloguePublics() {
    return (NSArray)storedValueForKey(CATALOGUE_PUBLICS_KEY);
  }

  public NSArray cataloguePublics(EOQualifier qualifier) {
    return cataloguePublics(qualifier, null, false);
  }

  public NSArray cataloguePublics(EOQualifier qualifier, boolean fetch) {
    return cataloguePublics(qualifier, null, fetch);
  }

  public NSArray cataloguePublics(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOCataloguePublic.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOCataloguePublic.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = cataloguePublics();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCataloguePublicsRelationship(org.cocktail.kava.server.metier.EOCataloguePublic object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_PUBLICS_KEY);
  }

  public void removeFromCataloguePublicsRelationship(org.cocktail.kava.server.metier.EOCataloguePublic object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PUBLICS_KEY);
  }

  public org.cocktail.kava.server.metier.EOCataloguePublic createCataloguePublicsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CataloguePublic");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CATALOGUE_PUBLICS_KEY);
    return (org.cocktail.kava.server.metier.EOCataloguePublic) eo;
  }

  public void deleteCataloguePublicsRelationship(org.cocktail.kava.server.metier.EOCataloguePublic object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_PUBLICS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCataloguePublicsRelationships() {
    Enumeration objects = cataloguePublics().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCataloguePublicsRelationship((org.cocktail.kava.server.metier.EOCataloguePublic)objects.nextElement());
    }
  }

  public NSArray catalogueResponsables() {
    return (NSArray)storedValueForKey(CATALOGUE_RESPONSABLES_KEY);
  }

  public NSArray catalogueResponsables(EOQualifier qualifier) {
    return catalogueResponsables(qualifier, null, false);
  }

  public NSArray catalogueResponsables(EOQualifier qualifier, boolean fetch) {
    return catalogueResponsables(qualifier, null, fetch);
  }

  public NSArray catalogueResponsables(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOCatalogueResponsable.CATALOGUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOCatalogueResponsable.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = catalogueResponsables();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCatalogueResponsablesRelationship(org.cocktail.kava.server.metier.EOCatalogueResponsable object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CATALOGUE_RESPONSABLES_KEY);
  }

  public void removeFromCatalogueResponsablesRelationship(org.cocktail.kava.server.metier.EOCatalogueResponsable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_RESPONSABLES_KEY);
  }

  public org.cocktail.kava.server.metier.EOCatalogueResponsable createCatalogueResponsablesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("CatalogueResponsable");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CATALOGUE_RESPONSABLES_KEY);
    return (org.cocktail.kava.server.metier.EOCatalogueResponsable) eo;
  }

  public void deleteCatalogueResponsablesRelationship(org.cocktail.kava.server.metier.EOCatalogueResponsable object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CATALOGUE_RESPONSABLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCatalogueResponsablesRelationships() {
    Enumeration objects = catalogueResponsables().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCatalogueResponsablesRelationship((org.cocktail.kava.server.metier.EOCatalogueResponsable)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCatalogue avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCatalogue createEOCatalogue(EOEditingContext editingContext, String catCommentaire
, NSTimestamp catDateDebut
, String catLibelle
, org.cocktail.kava.server.metier.EOFournisUlr fournisUlr, org.cocktail.kava.server.metier.EOTypeApplication typeApplication, org.cocktail.application.serveur.eof.EOTypeEtat typeEtat			) {
    EOCatalogue eo = (EOCatalogue) createAndInsertInstance(editingContext, _EOCatalogue.ENTITY_NAME);    
		eo.setCatCommentaire(catCommentaire);
		eo.setCatDateDebut(catDateDebut);
		eo.setCatLibelle(catLibelle);
    eo.setFournisUlrRelationship(fournisUlr);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOCatalogue localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCatalogue)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCatalogue creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCatalogue creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCatalogue object = (EOCatalogue)createAndInsertInstance(editingContext, _EOCatalogue.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCatalogue localInstanceIn(EOEditingContext editingContext, EOCatalogue eo) {
    EOCatalogue localInstance = (eo == null) ? null : (EOCatalogue)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCatalogue#localInstanceIn a la place.
   */
	public static EOCatalogue localInstanceOf(EOEditingContext editingContext, EOCatalogue eo) {
		return EOCatalogue.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCatalogue fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCatalogue fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCatalogue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCatalogue)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCatalogue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCatalogue fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCatalogue eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCatalogue)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCatalogue fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCatalogue eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCatalogue ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCatalogue fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
