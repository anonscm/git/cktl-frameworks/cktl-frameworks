

// EOCommune.java
// 

package org.cocktail.kava.server.metier;


import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;



public class EOCommune extends _EOCommune {
	
	public static final EOSortOrdering SORT_LC_COM_KEY_ASC = EOSortOrdering.sortOrderingWithKey(EOCommune.LC_COM_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_C_POSTAL_ASC = EOSortOrdering.sortOrderingWithKey(EOCommune.C_POSTAL_KEY, EOSortOrdering.CompareAscending);

	
    public EOCommune() {
        super();
    }

    
    
	public static NSArray rechercherCommunes(EOEditingContext editingContext, String codePostal) {
		NSMutableArray args = new NSMutableArray(codePostal);

		EOQualifier myQualifier = EOQualifier.qualifierWithQualifierFormat(EOCommune.C_POSTAL_KEY + " = %@", args);
		EOFetchSpecification myFetch = new EOFetchSpecification(EOCommune.ENTITY_NAME, myQualifier, null);
		return editingContext.objectsWithFetchSpecification(myFetch);
	}

	public String codePostalEtLlComm() {
		return cPostal() + " " + llCom();
	}

    
/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/



    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Apparemment cette methode n'est pas appelée.
     * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
     */    
    public void validateForSave() throws NSValidation.ValidationException {
        validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForSave();
        
    }

    /**
     * Peut etre appele à partir des factories.
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
      

    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     *
     */
    private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
           
    }

}
