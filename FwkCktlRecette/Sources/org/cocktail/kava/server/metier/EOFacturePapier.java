// EOFacturePapier.java
//

package org.cocktail.kava.server.metier;

import java.math.BigDecimal;

import org.cocktail.kava.server.finder.FinderPlanComptable;
import org.cocktail.kava.server.finder.FinderTauxProrata;
import org.cocktail.kava.server.finder.FinderTypeApplication;
import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.FacturePapierCompanion;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSValidation;

public class EOFacturePapier extends _EOFacturePapier implements FacturePapier {

	public static final String	FAP_ID_KEY				= "fapId";

	public static final String	FAP_TOTAL_HT_LIVE_KEY	= "fapTotalHtLive";
	public static final String	FAP_TOTAL_TTC_LIVE_KEY	= "fapTotalTtcLive";
	public static final String	FAP_TOTAL_TVA_LIVE_KEY	= "fapTotalTvaLive";

	private EOPlanComptable planComptable;
	private EOPlanComptable planComptableCtp;
	private EOPlanComptable planComptableTva;

	private FacturePapierCompanion companion;

	public EOFacturePapier() {
		super();
		this.companion = new FacturePapierCompanion(this);
	}

	public void updateTauxProrata() {
		if (tauxProrata() == null) {
			setTauxProrataRelationship(defaultTauxProrata());
		}
	}

	private EOTauxProrata defaultTauxProrata() {
		return FinderTauxProrata.tauxProrata100(this.editingContext());
	}

	/**
	 * Lazily load planComptable using exercice and pcoNum.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptable() {
		if (planComptable == null) {
			planComptable = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNum());
		}
		return planComptable;
	}

	/**
	 * Lazily load planComptableCtp using exercice and pcoNumCtp.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptableCtp() {
		if (planComptableCtp == null) {
			planComptableCtp = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNumCtp());
		}
		return planComptableCtp;
	}

	/**
	 * Lazily load planComptableTva using exercice and pcoNumTva.
	 * @return planComptable matching exercice and pcoNum ; null if not found.
	 */
	public EOPlanComptable planComptableTva() {
		if (planComptableTva == null) {
			planComptableTva = FinderPlanComptable.findById(editingContext(), exercice().exeOrdre(), pcoNumTva());
		}
		return planComptableTva;
	}

	public void setPlanComptable(EOPlanComptable planComptable) {
		String pcoNum = planComptable == null ? null : planComptable.pcoNum();
		setPcoNum(pcoNum);
	}

	public void setPlanComptableCtp(EOPlanComptable planComptableCtp) {
		String pcoNumCtp = planComptableCtp == null ? null : planComptableCtp.pcoNum();
		setPcoNumCtp(pcoNumCtp);
	}

	public void setPlanComptableTva(EOPlanComptable planComptableTva) {
		String pcoNumTva = planComptableTva == null ? null : planComptableTva.pcoNum();
		setPcoNumTva(pcoNumTva);
	}

	@Override
	public void setPcoNum(String value) {
		super.setPcoNum(value);
		this.planComptable = null;
	}

	@Override
	public void setPcoNumCtp(String value) {
		super.setPcoNumCtp(value);
		this.planComptableCtp = null;
	}

	@Override
	public void setPcoNumTva(String value) {
		super.setPcoNumTva(value);
		this.planComptableTva = null;
	}

	/**
	 * @return adresse de facturation en cours.
	 */
	public EOFacturePapierAdrClient currentAdresseClient() {
		EOFacturePapierAdrClient result = null;
		NSArray sortedAdr = EOSortOrdering.sortedArrayUsingKeyOrderArray(
				toFacturePapierAdrClients(),
				new NSArray(new Object[] {EOFacturePapierAdrClient.SORT_DATE_CREATION_DESC}));
		if (sortedAdr != null && sortedAdr.count() > 0) {
			result = (EOFacturePapierAdrClient) sortedAdr.objectAtIndex(0);
		}
		return result;
	}

	public Integer fapId() {
		try {
			NSDictionary dico = EOUtilities.primaryKeyForObject(editingContext(), this);
			return (Integer) dico.objectForKey(FAP_ID_KEY);
		}
		catch (Exception e) {
			return null;
		}
	}

	/**
	 * verifie si les infos obligatoires sont la pour valider cote prestataire
	 *  (peu importe son etape actuelle de validation)
	 * @return true ou false
	 */
	public boolean isValidablePrest() {
		if (organ() == null || typeCreditRec() == null || lolfNomenclatureRecette() == null
				|| pcoNum() == null	|| modeRecouvrement() == null) {
			return false;
		}
		return true;
	}


	public boolean isValidableClient() {
		if (!typePublic().typeApplication().equals(FinderTypeApplication.typeApplicationPrestationInterne(editingContext()))) {
			return true;
		}
		return true;
	}

	public void setFapRemiseGlobale(BigDecimal aValue) {
		if (aValue != null && aValue.equals(fapRemiseGlobale())) {
			return;
		}
		super.setFapRemiseGlobale(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				((EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i)).companion().updateTotaux();
			}
		}
	}

	/**
	 * @param aValue
	 *            (O/N) applique ou non la tva, et met a jour les lignes de la facture papier, et recalcule le total ttc
	 */
	public void setFapApplyTva(String aValue) {
		if (aValue != null && aValue.equals(fapApplyTva())) {
			return;
		}
		super.setFapApplyTva(aValue);
		if (facturePapierLignes() != null) {
			for (int i = 0; i < facturePapierLignes().count(); i++) {
				EOFacturePapierLigne flig = (EOFacturePapierLigne) facturePapierLignes().objectAtIndex(i);
				if ("N".equalsIgnoreCase(fapApplyTva())) {
					flig.setTvaRelationship(null);
					flig.setFligArtTtc(flig.fligArtHt());
				}
				else {
					flig.setTvaRelationship(flig.tvaInitial());
					flig.setFligArtTtc(flig.fligArtTtcInitial());
				}
			}
		}
	}

	/*
	 * // If you add instance variables to store property values you // should add empty implementions of the Serialization methods // to avoid
	 * unnecessary overhead (the properties will be // serialized for you in the superclass). private void writeObject(java.io.ObjectOutputStream
	 * out) throws java.io.IOException { }
	 *
	 * private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException { }
	 */

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelee.
	 *
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();

	}

	/**
	 * Peut etre appele à partir des factories.
	 *
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 *
	 */
	private final void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		updateTauxProrata();
		companion().updateTotaux();
	}

	public BigDecimal remiseGlobale() {
		return fapRemiseGlobale();
	}

	public FacturePapierCompanion companion() {
		return this.companion;
	}

	public void setTotalHT(BigDecimal totalHT) {
		setFapTotalHt(totalHT);
	}

	public void setTotalTVA(BigDecimal totalTVA) {
		setFapTotalTva(totalTVA);
	}

	public void setTotalTTC(BigDecimal totalTTC) {
		setFapTotalTtc(totalTTC);
	}

	public NSArray getFacturePapierLignesCommon() {
		return facturePapierLignes();
	}

	public ApplicationConfig applicationConfig() {
		return (ApplicationConfig) WOApplication.application();
	}

	public Number exerciceAsNumber() {
		if (exercice() != null) {
			return exercice().exeExercice();
		}
		return null;
	}

}
