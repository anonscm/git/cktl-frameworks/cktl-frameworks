/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFacturePapier.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFacturePapier extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FacturePapier";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_papier";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fapId";

	public static final String ECHE_ID_KEY = "echeId";
	public static final String FAP_APPLY_TVA_KEY = "fapApplyTva";
	public static final String FAP_COMMENTAIRE_CLIENT_KEY = "fapCommentaireClient";
	public static final String FAP_COMMENTAIRE_PREST_KEY = "fapCommentairePrest";
	public static final String FAP_DATE_KEY = "fapDate";
	public static final String FAP_DATE_LIMITE_PAIEMENT_KEY = "fapDateLimitePaiement";
	public static final String FAP_DATE_REGLEMENT_KEY = "fapDateReglement";
	public static final String FAP_DATE_VALIDATION_CLIENT_KEY = "fapDateValidationClient";
	public static final String FAP_DATE_VALIDATION_PREST_KEY = "fapDateValidationPrest";
	public static final String FAP_LIB_KEY = "fapLib";
	public static final String FAP_NUMERO_KEY = "fapNumero";
	public static final String FAP_REF_KEY = "fapRef";
	public static final String FAP_REFERENCE_REGLEMENT_KEY = "fapReferenceReglement";
	public static final String FAP_REMISE_GLOBALE_KEY = "fapRemiseGlobale";
	public static final String FAP_TOTAL_HT_KEY = "fapTotalHt";
	public static final String FAP_TOTAL_TTC_KEY = "fapTotalTtc";
	public static final String FAP_TOTAL_TVA_KEY = "fapTotalTva";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String PCO_NUM_CTP_KEY = "pcoNumCtp";
	public static final String PCO_NUM_TVA_KEY = "pcoNumTva";
	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CON_ORDRE_KEY = "conOrdre";
	public static final String ENG_ID_KEY = "engId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String FAC_ID_KEY = "facId";
	public static final String FAP_ID_KEY = "fapId";
	public static final String FAP_UTL_VALIDATION_CLIENT_KEY = "fapUtlValidationClient";
	public static final String FAP_UTL_VALIDATION_PREST_KEY = "fapUtlValidationPrest";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_ORDRE_PREST_KEY = "fouOrdrePrest";
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String MOR_ORDRE_KEY = "morOrdre";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ORG_ID_KEY = "orgId";
	public static final String PERS_ID_KEY = "persId";
	public static final String PREST_ID_KEY = "prestId";
	public static final String RIB_ORDRE_KEY = "ribOrdre";
	public static final String TAP_ID_KEY = "tapId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String TYPU_ID_KEY = "typuId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ECHE_ID_COLKEY = "ECHE_ID";
	public static final String FAP_APPLY_TVA_COLKEY = "FAP_APPLY_TVA";
	public static final String FAP_COMMENTAIRE_CLIENT_COLKEY = "FAP_COMMENTAIRE_CLIENT";
	public static final String FAP_COMMENTAIRE_PREST_COLKEY = "FAP_COMMENTAIRE_PREST";
	public static final String FAP_DATE_COLKEY = "FAP_DATE";
	public static final String FAP_DATE_LIMITE_PAIEMENT_COLKEY = "FAP_DATE_LIMITE_PAIEMENT";
	public static final String FAP_DATE_REGLEMENT_COLKEY = "FAP_DATE_REGLEMENT";
	public static final String FAP_DATE_VALIDATION_CLIENT_COLKEY = "FAP_DATE_VALIDATION_CLIENT";
	public static final String FAP_DATE_VALIDATION_PREST_COLKEY = "FAP_DATE_VALIDATION_PREST";
	public static final String FAP_LIB_COLKEY = "FAP_LIB";
	public static final String FAP_NUMERO_COLKEY = "FAP_NUMERO";
	public static final String FAP_REF_COLKEY = "FAP_REF";
	public static final String FAP_REFERENCE_REGLEMENT_COLKEY = "FAP_REFERENCE_REGLEMENT";
	public static final String FAP_REMISE_GLOBALE_COLKEY = "FAP_REMISE_GLOBALE";
	public static final String FAP_TOTAL_HT_COLKEY = "FAP_TOTAL_HT";
	public static final String FAP_TOTAL_TTC_COLKEY = "FAP_TOTAL_TTC";
	public static final String FAP_TOTAL_TVA_COLKEY = "FAP_TOTAL_TVA";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String PCO_NUM_CTP_COLKEY = "PCO_NUM_CTP";
	public static final String PCO_NUM_TVA_COLKEY = "PCO_NUM_TVA";
	public static final String PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CON_ORDRE_COLKEY = "CON_ORDRE";
	public static final String ENG_ID_COLKEY = "ENG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String FAC_ID_COLKEY = "FAC_ID";
	public static final String FAP_ID_COLKEY = "FAP_ID";
	public static final String FAP_UTL_VALIDATION_CLIENT_COLKEY = "fap_utl_validation_client";
	public static final String FAP_UTL_VALIDATION_PREST_COLKEY = "fap_utl_validation_prest";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_ORDRE_PREST_COLKEY = "FOU_ORDRE_PREST";
	public static final String LOLF_ID_COLKEY = "LOLF_ID";
	public static final String MOR_ORDRE_COLKEY = "MOR_ORDRE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PREST_ID_COLKEY = "PREST_ID";
	public static final String RIB_ORDRE_COLKEY = "RIB_ORDRE";
	public static final String TAP_ID_COLKEY = "TAP_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String TYPU_ID_COLKEY = "TYPU_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_KEY = "codeAnalytique";
	public static final String CONVENTION_KEY = "convention";
	public static final String ENGAGE_BUDGET_KEY = "engageBudget";
	public static final String EXERCICE_KEY = "exercice";
	public static final String FACTURE_KEY = "facture";
	public static final String FACTURE_PAPIER_LIGNES_KEY = "facturePapierLignes";
	public static final String FOURNIS_ULR_KEY = "fournisUlr";
	public static final String FOURNIS_ULR_PREST_KEY = "fournisUlrPrest";
	public static final String INDIVIDU_ULR_KEY = "individuUlr";
	public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
	public static final String MODE_RECOUVREMENT_KEY = "modeRecouvrement";
	public static final String ORGAN_KEY = "organ";
	public static final String PERSONNE_KEY = "personne";
	public static final String PRESTATION_KEY = "prestation";
	public static final String RIBFOUR_ULR_KEY = "ribfourUlr";
	public static final String TAUX_PRORATA_KEY = "tauxProrata";
	public static final String TO_FACTURE_PAPIER_ADR_CLIENTS_KEY = "toFacturePapierAdrClients";
	public static final String TYPE_CREDIT_REC_KEY = "typeCreditRec";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_PUBLIC_KEY = "typePublic";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_VALIDATION_CLIENT_KEY = "utilisateurValidationClient";
	public static final String UTILISATEUR_VALIDATION_PREST_KEY = "utilisateurValidationPrest";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer echeId() {
    return (Integer) storedValueForKey(ECHE_ID_KEY);
  }

  public void setEcheId(Integer value) {
    takeStoredValueForKey(value, ECHE_ID_KEY);
  }

  public String fapApplyTva() {
    return (String) storedValueForKey(FAP_APPLY_TVA_KEY);
  }

  public void setFapApplyTva(String value) {
    takeStoredValueForKey(value, FAP_APPLY_TVA_KEY);
  }

  public String fapCommentaireClient() {
    return (String) storedValueForKey(FAP_COMMENTAIRE_CLIENT_KEY);
  }

  public void setFapCommentaireClient(String value) {
    takeStoredValueForKey(value, FAP_COMMENTAIRE_CLIENT_KEY);
  }

  public String fapCommentairePrest() {
    return (String) storedValueForKey(FAP_COMMENTAIRE_PREST_KEY);
  }

  public void setFapCommentairePrest(String value) {
    takeStoredValueForKey(value, FAP_COMMENTAIRE_PREST_KEY);
  }

  public NSTimestamp fapDate() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_KEY);
  }

  public void setFapDate(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_KEY);
  }

  public NSTimestamp fapDateLimitePaiement() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_LIMITE_PAIEMENT_KEY);
  }

  public void setFapDateLimitePaiement(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_LIMITE_PAIEMENT_KEY);
  }

  public NSTimestamp fapDateReglement() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_REGLEMENT_KEY);
  }

  public void setFapDateReglement(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_REGLEMENT_KEY);
  }

  public NSTimestamp fapDateValidationClient() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_VALIDATION_CLIENT_KEY);
  }

  public void setFapDateValidationClient(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_VALIDATION_CLIENT_KEY);
  }

  public NSTimestamp fapDateValidationPrest() {
    return (NSTimestamp) storedValueForKey(FAP_DATE_VALIDATION_PREST_KEY);
  }

  public void setFapDateValidationPrest(NSTimestamp value) {
    takeStoredValueForKey(value, FAP_DATE_VALIDATION_PREST_KEY);
  }

  public String fapLib() {
    return (String) storedValueForKey(FAP_LIB_KEY);
  }

  public void setFapLib(String value) {
    takeStoredValueForKey(value, FAP_LIB_KEY);
  }

  public Float fapNumero() {
    return (Float) storedValueForKey(FAP_NUMERO_KEY);
  }

  public void setFapNumero(Float value) {
    takeStoredValueForKey(value, FAP_NUMERO_KEY);
  }

  public String fapRef() {
    return (String) storedValueForKey(FAP_REF_KEY);
  }

  public void setFapRef(String value) {
    takeStoredValueForKey(value, FAP_REF_KEY);
  }

  public String fapReferenceReglement() {
    return (String) storedValueForKey(FAP_REFERENCE_REGLEMENT_KEY);
  }

  public void setFapReferenceReglement(String value) {
    takeStoredValueForKey(value, FAP_REFERENCE_REGLEMENT_KEY);
  }

  public java.math.BigDecimal fapRemiseGlobale() {
    return (java.math.BigDecimal) storedValueForKey(FAP_REMISE_GLOBALE_KEY);
  }

  public void setFapRemiseGlobale(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_REMISE_GLOBALE_KEY);
  }

  public java.math.BigDecimal fapTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_HT_KEY);
  }

  public void setFapTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal fapTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_TTC_KEY);
  }

  public void setFapTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_TTC_KEY);
  }

  public java.math.BigDecimal fapTotalTva() {
    return (java.math.BigDecimal) storedValueForKey(FAP_TOTAL_TVA_KEY);
  }

  public void setFapTotalTva(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FAP_TOTAL_TVA_KEY);
  }

  public String pcoNum() {
    return (String) storedValueForKey(PCO_NUM_KEY);
  }

  public void setPcoNum(String value) {
    takeStoredValueForKey(value, PCO_NUM_KEY);
  }

  public String pcoNumCtp() {
    return (String) storedValueForKey(PCO_NUM_CTP_KEY);
  }

  public void setPcoNumCtp(String value) {
    takeStoredValueForKey(value, PCO_NUM_CTP_KEY);
  }

  public String pcoNumTva() {
    return (String) storedValueForKey(PCO_NUM_TVA_KEY);
  }

  public void setPcoNumTva(String value) {
    takeStoredValueForKey(value, PCO_NUM_TVA_KEY);
  }

  public String personne_persNomPrenom() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setPersonne_persNomPrenom(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public org.cocktail.kava.server.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.kava.server.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.kava.server.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOConvention convention() {
    return (org.cocktail.kava.server.metier.EOConvention)storedValueForKey(CONVENTION_KEY);
  }

  public void setConventionRelationship(org.cocktail.kava.server.metier.EOConvention value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOConvention oldValue = convention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CONVENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CONVENTION_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOEngageBudget engageBudget() {
    return (org.cocktail.kava.server.metier.EOEngageBudget)storedValueForKey(ENGAGE_BUDGET_KEY);
  }

  public void setEngageBudgetRelationship(org.cocktail.kava.server.metier.EOEngageBudget value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOEngageBudget oldValue = engageBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ENGAGE_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ENGAGE_BUDGET_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFacture facture() {
    return (org.cocktail.kava.server.metier.EOFacture)storedValueForKey(FACTURE_KEY);
  }

  public void setFactureRelationship(org.cocktail.kava.server.metier.EOFacture value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFacture oldValue = facture();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FACTURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFournisUlr fournisUlr() {
    return (org.cocktail.kava.server.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_KEY);
  }

  public void setFournisUlrRelationship(org.cocktail.kava.server.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFournisUlr oldValue = fournisUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFournisUlr fournisUlrPrest() {
    return (org.cocktail.kava.server.metier.EOFournisUlr)storedValueForKey(FOURNIS_ULR_PREST_KEY);
  }

  public void setFournisUlrPrestRelationship(org.cocktail.kava.server.metier.EOFournisUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFournisUlr oldValue = fournisUlrPrest();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FOURNIS_ULR_PREST_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FOURNIS_ULR_PREST_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOIndividuUlr individuUlr() {
    return (org.cocktail.kava.server.metier.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
  }

  public void setIndividuUlrRelationship(org.cocktail.kava.server.metier.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOIndividuUlr oldValue = individuUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
    return (org.cocktail.kava.server.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
  }

  public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.server.metier.EOLolfNomenclatureRecette value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOLolfNomenclatureRecette oldValue = lolfNomenclatureRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_NOMENCLATURE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOModeRecouvrement modeRecouvrement() {
    return (org.cocktail.kava.server.metier.EOModeRecouvrement)storedValueForKey(MODE_RECOUVREMENT_KEY);
  }

  public void setModeRecouvrementRelationship(org.cocktail.kava.server.metier.EOModeRecouvrement value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOModeRecouvrement oldValue = modeRecouvrement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, MODE_RECOUVREMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, MODE_RECOUVREMENT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan organ() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPrestation prestation() {
    return (org.cocktail.kava.server.metier.EOPrestation)storedValueForKey(PRESTATION_KEY);
  }

  public void setPrestationRelationship(org.cocktail.kava.server.metier.EOPrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestation oldValue = prestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EORibfourUlr ribfourUlr() {
    return (org.cocktail.kava.server.metier.EORibfourUlr)storedValueForKey(RIBFOUR_ULR_KEY);
  }

  public void setRibfourUlrRelationship(org.cocktail.kava.server.metier.EORibfourUlr value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EORibfourUlr oldValue = ribfourUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, RIBFOUR_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, RIBFOUR_ULR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTauxProrata tauxProrata() {
    return (org.cocktail.kava.server.metier.EOTauxProrata)storedValueForKey(TAUX_PRORATA_KEY);
  }

  public void setTauxProrataRelationship(org.cocktail.kava.server.metier.EOTauxProrata value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTauxProrata oldValue = tauxProrata();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAUX_PRORATA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAUX_PRORATA_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeCredit typeCreditRec() {
    return (org.cocktail.application.serveur.eof.EOTypeCredit)storedValueForKey(TYPE_CREDIT_REC_KEY);
  }

  public void setTypeCreditRecRelationship(org.cocktail.application.serveur.eof.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeCredit oldValue = typeCreditRec();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_REC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_REC_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.serveur.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.serveur.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypePublic typePublic() {
    return (org.cocktail.kava.server.metier.EOTypePublic)storedValueForKey(TYPE_PUBLIC_KEY);
  }

  public void setTypePublicRelationship(org.cocktail.kava.server.metier.EOTypePublic value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypePublic oldValue = typePublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PUBLIC_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateurValidationClient() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDATION_CLIENT_KEY);
  }

  public void setUtilisateurValidationClientRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateurValidationClient();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_VALIDATION_CLIENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDATION_CLIENT_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOUtilisateur utilisateurValidationPrest() {
    return (org.cocktail.kava.server.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDATION_PREST_KEY);
  }

  public void setUtilisateurValidationPrestRelationship(org.cocktail.kava.server.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOUtilisateur oldValue = utilisateurValidationPrest();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_VALIDATION_PREST_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDATION_PREST_KEY);
    }
  }
  
  public NSArray facturePapierLignes() {
    return (NSArray)storedValueForKey(FACTURE_PAPIER_LIGNES_KEY);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier) {
    return facturePapierLignes(qualifier, null, false);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier, boolean fetch) {
    return facturePapierLignes(qualifier, null, fetch);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOFacturePapierLigne.FACTURE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOFacturePapierLigne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = facturePapierLignes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
  }

  public void removeFromFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
  }

  public org.cocktail.kava.server.metier.EOFacturePapierLigne createFacturePapierLignesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FacturePapierLigne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_PAPIER_LIGNES_KEY);
    return (org.cocktail.kava.server.metier.EOFacturePapierLigne) eo;
  }

  public void deleteFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFacturePapierLignesRelationships() {
    Enumeration objects = facturePapierLignes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFacturePapierLignesRelationship((org.cocktail.kava.server.metier.EOFacturePapierLigne)objects.nextElement());
    }
  }

  public NSArray toFacturePapierAdrClients() {
    return (NSArray)storedValueForKey(TO_FACTURE_PAPIER_ADR_CLIENTS_KEY);
  }

  public NSArray toFacturePapierAdrClients(EOQualifier qualifier) {
    return toFacturePapierAdrClients(qualifier, null, false);
  }

  public NSArray toFacturePapierAdrClients(EOQualifier qualifier, boolean fetch) {
    return toFacturePapierAdrClients(qualifier, null, fetch);
  }

  public NSArray toFacturePapierAdrClients(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOFacturePapierAdrClient.TO_FACTURE_PAPIER_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOFacturePapierAdrClient.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toFacturePapierAdrClients();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToFacturePapierAdrClientsRelationship(org.cocktail.kava.server.metier.EOFacturePapierAdrClient object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_ADR_CLIENTS_KEY);
  }

  public void removeFromToFacturePapierAdrClientsRelationship(org.cocktail.kava.server.metier.EOFacturePapierAdrClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_ADR_CLIENTS_KEY);
  }

  public org.cocktail.kava.server.metier.EOFacturePapierAdrClient createToFacturePapierAdrClientsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FacturePapierAdrClient");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_FACTURE_PAPIER_ADR_CLIENTS_KEY);
    return (org.cocktail.kava.server.metier.EOFacturePapierAdrClient) eo;
  }

  public void deleteToFacturePapierAdrClientsRelationship(org.cocktail.kava.server.metier.EOFacturePapierAdrClient object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_FACTURE_PAPIER_ADR_CLIENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToFacturePapierAdrClientsRelationships() {
    Enumeration objects = toFacturePapierAdrClients().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToFacturePapierAdrClientsRelationship((org.cocktail.kava.server.metier.EOFacturePapierAdrClient)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFacturePapier avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFacturePapier createEOFacturePapier(EOEditingContext editingContext, String fapApplyTva
, NSTimestamp fapDate
, String fapLib
, Float fapNumero
, java.math.BigDecimal fapTotalHt
, java.math.BigDecimal fapTotalTtc
, java.math.BigDecimal fapTotalTva
			) {
    EOFacturePapier eo = (EOFacturePapier) createAndInsertInstance(editingContext, _EOFacturePapier.ENTITY_NAME);    
		eo.setFapApplyTva(fapApplyTva);
		eo.setFapDate(fapDate);
		eo.setFapLib(fapLib);
		eo.setFapNumero(fapNumero);
		eo.setFapTotalHt(fapTotalHt);
		eo.setFapTotalTtc(fapTotalTtc);
		eo.setFapTotalTva(fapTotalTva);
    return eo;
  }

  
	  public EOFacturePapier localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFacturePapier)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFacturePapier creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFacturePapier creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOFacturePapier object = (EOFacturePapier)createAndInsertInstance(editingContext, _EOFacturePapier.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFacturePapier localInstanceIn(EOEditingContext editingContext, EOFacturePapier eo) {
    EOFacturePapier localInstance = (eo == null) ? null : (EOFacturePapier)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFacturePapier#localInstanceIn a la place.
   */
	public static EOFacturePapier localInstanceOf(EOEditingContext editingContext, EOFacturePapier eo) {
		return EOFacturePapier.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFacturePapier fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFacturePapier fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFacturePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFacturePapier)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFacturePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFacturePapier fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFacturePapier eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFacturePapier)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFacturePapier fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFacturePapier eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFacturePapier ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFacturePapier fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
