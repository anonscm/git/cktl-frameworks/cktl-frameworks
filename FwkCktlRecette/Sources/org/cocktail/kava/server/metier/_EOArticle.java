/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOArticle.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOArticle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Article";
	public static final String ENTITY_TABLE_NAME = "jefy_catalogue.article";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "artId";

	public static final String ART_LIBELLE_KEY = "artLibelle";

// Attributs non visibles
	public static final String ART_ID_KEY = "artId";
	public static final String ART_ID_PERE_KEY = "artIdPere";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String TYAR_ID_KEY = "tyarId";

//Colonnes dans la base de donnees
	public static final String ART_LIBELLE_COLKEY = "ART_LIBELLE";

	public static final String ART_ID_COLKEY = "ART_ID";
	public static final String ART_ID_PERE_COLKEY = "ART_ID_PERE";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String TYAR_ID_COLKEY = "TYAR_ID";


	// Relationships
	public static final String ARTICLE_PERE_KEY = "articlePere";
	public static final String ARTICLE_PRESTATION_KEY = "articlePrestation";
	public static final String ARTICLE_PRESTATION_WEBS_KEY = "articlePrestationWebs";
	public static final String ARTICLES_KEY = "articles";
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String TYPE_ARTICLE_KEY = "typeArticle";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String artLibelle() {
    return (String) storedValueForKey(ART_LIBELLE_KEY);
  }

  public void setArtLibelle(String value) {
    takeStoredValueForKey(value, ART_LIBELLE_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticle articlePere() {
    return (org.cocktail.kava.server.metier.EOArticle)storedValueForKey(ARTICLE_PERE_KEY);
  }

  public void setArticlePereRelationship(org.cocktail.kava.server.metier.EOArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOArticle oldValue = articlePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_PERE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOArticlePrestation articlePrestation() {
    return (org.cocktail.kava.server.metier.EOArticlePrestation)storedValueForKey(ARTICLE_PRESTATION_KEY);
  }

  public void setArticlePrestationRelationship(org.cocktail.kava.server.metier.EOArticlePrestation value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOArticlePrestation oldValue = articlePrestation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ARTICLE_PRESTATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ARTICLE_PRESTATION_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOCodeMarche codeMarche() {
    return (org.cocktail.application.serveur.eof.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
  }

  public void setCodeMarcheRelationship(org.cocktail.application.serveur.eof.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOCodeMarche oldValue = codeMarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeArticle typeArticle() {
    return (org.cocktail.kava.server.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
  }

  public void setTypeArticleRelationship(org.cocktail.kava.server.metier.EOTypeArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeArticle oldValue = typeArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
    }
  }
  
  public NSArray articlePrestationWebs() {
    return (NSArray)storedValueForKey(ARTICLE_PRESTATION_WEBS_KEY);
  }

  public NSArray articlePrestationWebs(EOQualifier qualifier) {
    return articlePrestationWebs(qualifier, null, false);
  }

  public NSArray articlePrestationWebs(EOQualifier qualifier, boolean fetch) {
    return articlePrestationWebs(qualifier, null, fetch);
  }

  public NSArray articlePrestationWebs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOArticlePrestationWeb.ARTICLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOArticlePrestationWeb.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = articlePrestationWebs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToArticlePrestationWebsRelationship(org.cocktail.kava.server.metier.EOArticlePrestationWeb object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_WEBS_KEY);
  }

  public void removeFromArticlePrestationWebsRelationship(org.cocktail.kava.server.metier.EOArticlePrestationWeb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_WEBS_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticlePrestationWeb createArticlePrestationWebsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ArticlePrestationWeb");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLE_PRESTATION_WEBS_KEY);
    return (org.cocktail.kava.server.metier.EOArticlePrestationWeb) eo;
  }

  public void deleteArticlePrestationWebsRelationship(org.cocktail.kava.server.metier.EOArticlePrestationWeb object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLE_PRESTATION_WEBS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllArticlePrestationWebsRelationships() {
    Enumeration objects = articlePrestationWebs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteArticlePrestationWebsRelationship((org.cocktail.kava.server.metier.EOArticlePrestationWeb)objects.nextElement());
    }
  }

  public NSArray articles() {
    return (NSArray)storedValueForKey(ARTICLES_KEY);
  }

  public NSArray articles(EOQualifier qualifier) {
    return articles(qualifier, null, false);
  }

  public NSArray articles(EOQualifier qualifier, boolean fetch) {
    return articles(qualifier, null, fetch);
  }

  public NSArray articles(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOArticle.ARTICLE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOArticle.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = articles();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToArticlesRelationship(org.cocktail.kava.server.metier.EOArticle object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public void removeFromArticlesRelationship(org.cocktail.kava.server.metier.EOArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
  }

  public org.cocktail.kava.server.metier.EOArticle createArticlesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Article");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ARTICLES_KEY);
    return (org.cocktail.kava.server.metier.EOArticle) eo;
  }

  public void deleteArticlesRelationship(org.cocktail.kava.server.metier.EOArticle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ARTICLES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllArticlesRelationships() {
    Enumeration objects = articles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteArticlesRelationship((org.cocktail.kava.server.metier.EOArticle)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOArticle avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOArticle createEOArticle(EOEditingContext editingContext, String artLibelle
, org.cocktail.kava.server.metier.EOTypeArticle typeArticle			) {
    EOArticle eo = (EOArticle) createAndInsertInstance(editingContext, _EOArticle.ENTITY_NAME);    
		eo.setArtLibelle(artLibelle);
    eo.setTypeArticleRelationship(typeArticle);
    return eo;
  }

  
	  public EOArticle localInstanceIn(EOEditingContext editingContext) {
	  		return (EOArticle)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticle creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOArticle creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOArticle object = (EOArticle)createAndInsertInstance(editingContext, _EOArticle.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOArticle localInstanceIn(EOEditingContext editingContext, EOArticle eo) {
    EOArticle localInstance = (eo == null) ? null : (EOArticle)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOArticle#localInstanceIn a la place.
   */
	public static EOArticle localInstanceOf(EOEditingContext editingContext, EOArticle eo) {
		return EOArticle.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOArticle fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOArticle fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOArticle)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOArticle fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOArticle eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOArticle)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOArticle fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOArticle eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOArticle ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOArticle fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
