/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructureUlr.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOStructureUlr extends  EOGenericRecord {
	public static final String ENTITY_NAME = "StructureUlr";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.v_structure_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_NAF_KEY = "cNaf";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";

//Colonnes dans la base de donnees
	public static final String C_NAF_COLKEY = "C_NAF";
	public static final String C_RNE_COLKEY = "C_RNE";
	public static final String C_STRUCTURE_COLKEY = "c_structure";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "ll_structure";
	public static final String ORG_ORDRE_COLKEY = "ORG_ORDRE";
	public static final String PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String SIREN_COLKEY = "SIREN";
	public static final String SIRET_COLKEY = "SIRET";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";

	public static final String PERS_ID_COLKEY = "PERS_ID";


	// Relationships
	public static final String PERSONNE_KEY = "personne";
	public static final String REPART_STRUCTURES_KEY = "repartStructures";
	public static final String REPART_TYPE_GROUPES_KEY = "repartTypeGroupes";
	public static final String SECRETARIATS_KEY = "secretariats";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String cNaf() {
    return (String) storedValueForKey(C_NAF_KEY);
  }

  public void setCNaf(String value) {
    takeStoredValueForKey(value, C_NAF_KEY);
  }

  public String cRne() {
    return (String) storedValueForKey(C_RNE_KEY);
  }

  public void setCRne(String value) {
    takeStoredValueForKey(value, C_RNE_KEY);
  }

  public String cStructure() {
    return (String) storedValueForKey(C_STRUCTURE_KEY);
  }

  public void setCStructure(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_KEY);
  }

  public String cStructurePere() {
    return (String) storedValueForKey(C_STRUCTURE_PERE_KEY);
  }

  public void setCStructurePere(String value) {
    takeStoredValueForKey(value, C_STRUCTURE_PERE_KEY);
  }

  public String cTypeStructure() {
    return (String) storedValueForKey(C_TYPE_STRUCTURE_KEY);
  }

  public void setCTypeStructure(String value) {
    takeStoredValueForKey(value, C_TYPE_STRUCTURE_KEY);
  }

  public String lcStructure() {
    return (String) storedValueForKey(LC_STRUCTURE_KEY);
  }

  public void setLcStructure(String value) {
    takeStoredValueForKey(value, LC_STRUCTURE_KEY);
  }

  public String llStructure() {
    return (String) storedValueForKey(LL_STRUCTURE_KEY);
  }

  public void setLlStructure(String value) {
    takeStoredValueForKey(value, LL_STRUCTURE_KEY);
  }

  public Integer orgOrdre() {
    return (Integer) storedValueForKey(ORG_ORDRE_KEY);
  }

  public void setOrgOrdre(Integer value) {
    takeStoredValueForKey(value, ORG_ORDRE_KEY);
  }

  public String personne_persNomPrenom() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setPersonne_persNomPrenom(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public String siren() {
    return (String) storedValueForKey(SIREN_KEY);
  }

  public void setSiren(String value) {
    takeStoredValueForKey(value, SIREN_KEY);
  }

  public String siret() {
    return (String) storedValueForKey(SIRET_KEY);
  }

  public void setSiret(String value) {
    takeStoredValueForKey(value, SIRET_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.kava.server.metier.EOPersonne personne() {
    return (org.cocktail.kava.server.metier.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.kava.server.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public NSArray repartStructures() {
    return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
  }

  public NSArray repartStructures(EOQualifier qualifier) {
    return repartStructures(qualifier, null, false);
  }

  public NSArray repartStructures(EOQualifier qualifier, boolean fetch) {
    return repartStructures(qualifier, null, fetch);
  }

  public NSArray repartStructures(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EORepartStructure.STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EORepartStructure.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartStructures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartStructuresRelationship(org.cocktail.kava.server.metier.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public void removeFromRepartStructuresRelationship(org.cocktail.kava.server.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public org.cocktail.kava.server.metier.EORepartStructure createRepartStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURES_KEY);
    return (org.cocktail.kava.server.metier.EORepartStructure) eo;
  }

  public void deleteRepartStructuresRelationship(org.cocktail.kava.server.metier.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartStructuresRelationships() {
    Enumeration objects = repartStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartStructuresRelationship((org.cocktail.kava.server.metier.EORepartStructure)objects.nextElement());
    }
  }

  public NSArray repartTypeGroupes() {
    return (NSArray)storedValueForKey(REPART_TYPE_GROUPES_KEY);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier) {
    return repartTypeGroupes(qualifier, null, false);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier, boolean fetch) {
    return repartTypeGroupes(qualifier, null, fetch);
  }

  public NSArray repartTypeGroupes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EORepartTypeGroupe.STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EORepartTypeGroupe.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartTypeGroupes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartTypeGroupesRelationship(org.cocktail.kava.server.metier.EORepartTypeGroupe object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
  }

  public void removeFromRepartTypeGroupesRelationship(org.cocktail.kava.server.metier.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
  }

  public org.cocktail.kava.server.metier.EORepartTypeGroupe createRepartTypeGroupesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_TYPE_GROUPES_KEY);
    return (org.cocktail.kava.server.metier.EORepartTypeGroupe) eo;
  }

  public void deleteRepartTypeGroupesRelationship(org.cocktail.kava.server.metier.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_TYPE_GROUPES_KEY);
  }

  public void deleteAllRepartTypeGroupesRelationships() {
    Enumeration objects = repartTypeGroupes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartTypeGroupesRelationship((org.cocktail.kava.server.metier.EORepartTypeGroupe)objects.nextElement());
    }
  }

  public NSArray secretariats() {
    return (NSArray)storedValueForKey(SECRETARIATS_KEY);
  }

  public NSArray secretariats(EOQualifier qualifier) {
    return secretariats(qualifier, null, false);
  }

  public NSArray secretariats(EOQualifier qualifier, boolean fetch) {
    return secretariats(qualifier, null, fetch);
  }

  public NSArray secretariats(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOSecretariat.STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOSecretariat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = secretariats();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSecretariatsRelationship(org.cocktail.kava.server.metier.EOSecretariat object) {
    addObjectToBothSidesOfRelationshipWithKey(object, SECRETARIATS_KEY);
  }

  public void removeFromSecretariatsRelationship(org.cocktail.kava.server.metier.EOSecretariat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SECRETARIATS_KEY);
  }

  public org.cocktail.kava.server.metier.EOSecretariat createSecretariatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Secretariat");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, SECRETARIATS_KEY);
    return (org.cocktail.kava.server.metier.EOSecretariat) eo;
  }

  public void deleteSecretariatsRelationship(org.cocktail.kava.server.metier.EOSecretariat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, SECRETARIATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSecretariatsRelationships() {
    Enumeration objects = secretariats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSecretariatsRelationship((org.cocktail.kava.server.metier.EOSecretariat)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOStructureUlr avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOStructureUlr createEOStructureUlr(EOEditingContext editingContext, String cStructure
, String cTypeStructure
, String llStructure
, String temValide
			) {
    EOStructureUlr eo = (EOStructureUlr) createAndInsertInstance(editingContext, _EOStructureUlr.ENTITY_NAME);    
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setLlStructure(llStructure);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOStructureUlr localInstanceIn(EOEditingContext editingContext) {
	  		return (EOStructureUlr)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructureUlr creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOStructureUlr creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOStructureUlr object = (EOStructureUlr)createAndInsertInstance(editingContext, _EOStructureUlr.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOStructureUlr localInstanceIn(EOEditingContext editingContext, EOStructureUlr eo) {
    EOStructureUlr localInstance = (eo == null) ? null : (EOStructureUlr)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOStructureUlr#localInstanceIn a la place.
   */
	public static EOStructureUlr localInstanceOf(EOEditingContext editingContext, EOStructureUlr eo) {
		return EOStructureUlr.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOStructureUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructureUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOStructureUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOStructureUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOStructureUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOStructureUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOStructureUlr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOStructureUlr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
