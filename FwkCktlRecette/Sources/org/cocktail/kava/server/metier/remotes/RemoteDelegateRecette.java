package org.cocktail.kava.server.metier.remotes;

import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSDictionary;

public class RemoteDelegateRecette {

	private CktlWebSession mySession;

	public RemoteDelegateRecette(CktlWebSession session) {
		mySession = session;
	}

	public NSDictionary clientSideRequestPrimaryKeyForObject(EOEnterpriseObject eo) {
		if (eo == null) {
			return null;
		}
		return EOUtilities.primaryKeyForObject(defaultEditingContext(), eo);
	}
	
	public NSDictionary clientSideRequestPrimaryKeyForGlobalId(EOGlobalID gid) {
		if (gid == null) {
			return null;
		}
		EOEnterpriseObject eo = defaultEditingContext().objectForGlobalID(gid);
		return EOUtilities.primaryKeyForObject(defaultEditingContext(), eo);
	}

	public EOEditingContext defaultEditingContext() {
		return getMySession().defaultEditingContext();
	}

	public CktlWebSession getMySession() {
		return mySession;
	}

	public void setMySession(CktlWebSession mySession) {
		this.mySession = mySession;
	}
}
