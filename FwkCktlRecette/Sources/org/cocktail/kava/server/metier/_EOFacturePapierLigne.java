/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOFacturePapierLigne.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOFacturePapierLigne extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FacturePapierLigne";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.facture_papier_ligne";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fligId";

	public static final String FLIG_ART_HT_KEY = "fligArtHt";
	public static final String FLIG_ART_TTC_KEY = "fligArtTtc";
	public static final String FLIG_ART_TTC_INITIAL_KEY = "fligArtTtcInitial";
	public static final String FLIG_DATE_KEY = "fligDate";
	public static final String FLIG_DESCRIPTION_KEY = "fligDescription";
	public static final String FLIG_QUANTITE_KEY = "fligQuantite";
	public static final String FLIG_REFERENCE_KEY = "fligReference";
	public static final String FLIG_TOTAL_HT_KEY = "fligTotalHt";
	public static final String FLIG_TOTAL_TTC_KEY = "fligTotalTtc";

// Attributs non visibles
	public static final String FAP_ID_KEY = "fapId";
	public static final String FLIG_ID_KEY = "fligId";
	public static final String FLIG_ID_PERE_KEY = "fligIdPere";
	public static final String PRLIG_ID_KEY = "prligId";
	public static final String TVA_ID_KEY = "tvaId";
	public static final String TVA_ID_INITIAL_KEY = "tvaIdInitial";
	public static final String TYAR_ID_KEY = "tyarId";

//Colonnes dans la base de donnees
	public static final String FLIG_ART_HT_COLKEY = "FLIG_ART_HT";
	public static final String FLIG_ART_TTC_COLKEY = "FLIG_ART_TTC";
	public static final String FLIG_ART_TTC_INITIAL_COLKEY = "FLIG_ART_TTC_INITIAL";
	public static final String FLIG_DATE_COLKEY = "FLIG_DATE";
	public static final String FLIG_DESCRIPTION_COLKEY = "FLIG_DESCRIPTION";
	public static final String FLIG_QUANTITE_COLKEY = "FLIG_QUANTITE";
	public static final String FLIG_REFERENCE_COLKEY = "FLIG_REFERENCE";
	public static final String FLIG_TOTAL_HT_COLKEY = "FLIG_TOTAL_HT";
	public static final String FLIG_TOTAL_TTC_COLKEY = "FLIG_TOTAL_TTC";

	public static final String FAP_ID_COLKEY = "FAP_ID";
	public static final String FLIG_ID_COLKEY = "FLIG_ID";
	public static final String FLIG_ID_PERE_COLKEY = "FLIG_ID_PERE";
	public static final String PRLIG_ID_COLKEY = "PRLIG_ID";
	public static final String TVA_ID_COLKEY = "TVA_ID";
	public static final String TVA_ID_INITIAL_COLKEY = "TVA_ID_INITIAL";
	public static final String TYAR_ID_COLKEY = "TYAR_ID";


	// Relationships
	public static final String FACTURE_PAPIER_KEY = "facturePapier";
	public static final String FACTURE_PAPIER_LIGNE_PERE_KEY = "facturePapierLignePere";
	public static final String FACTURE_PAPIER_LIGNES_KEY = "facturePapierLignes";
	public static final String PRESTATION_LIGNE_KEY = "prestationLigne";
	public static final String TVA_KEY = "tva";
	public static final String TVA_INITIAL_KEY = "tvaInitial";
	public static final String TYPE_ARTICLE_KEY = "typeArticle";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public java.math.BigDecimal fligArtHt() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_ART_HT_KEY);
  }

  public void setFligArtHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_ART_HT_KEY);
  }

  public java.math.BigDecimal fligArtTtc() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_ART_TTC_KEY);
  }

  public void setFligArtTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_ART_TTC_KEY);
  }

  public java.math.BigDecimal fligArtTtcInitial() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_ART_TTC_INITIAL_KEY);
  }

  public void setFligArtTtcInitial(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_ART_TTC_INITIAL_KEY);
  }

  public NSTimestamp fligDate() {
    return (NSTimestamp) storedValueForKey(FLIG_DATE_KEY);
  }

  public void setFligDate(NSTimestamp value) {
    takeStoredValueForKey(value, FLIG_DATE_KEY);
  }

  public String fligDescription() {
    return (String) storedValueForKey(FLIG_DESCRIPTION_KEY);
  }

  public void setFligDescription(String value) {
    takeStoredValueForKey(value, FLIG_DESCRIPTION_KEY);
  }

  public java.math.BigDecimal fligQuantite() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_QUANTITE_KEY);
  }

  public void setFligQuantite(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_QUANTITE_KEY);
  }

  public String fligReference() {
    return (String) storedValueForKey(FLIG_REFERENCE_KEY);
  }

  public void setFligReference(String value) {
    takeStoredValueForKey(value, FLIG_REFERENCE_KEY);
  }

  public java.math.BigDecimal fligTotalHt() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_TOTAL_HT_KEY);
  }

  public void setFligTotalHt(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_TOTAL_HT_KEY);
  }

  public java.math.BigDecimal fligTotalTtc() {
    return (java.math.BigDecimal) storedValueForKey(FLIG_TOTAL_TTC_KEY);
  }

  public void setFligTotalTtc(java.math.BigDecimal value) {
    takeStoredValueForKey(value, FLIG_TOTAL_TTC_KEY);
  }

  public org.cocktail.kava.server.metier.EOFacturePapier facturePapier() {
    return (org.cocktail.kava.server.metier.EOFacturePapier)storedValueForKey(FACTURE_PAPIER_KEY);
  }

  public void setFacturePapierRelationship(org.cocktail.kava.server.metier.EOFacturePapier value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFacturePapier oldValue = facturePapier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FACTURE_PAPIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_PAPIER_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOFacturePapierLigne facturePapierLignePere() {
    return (org.cocktail.kava.server.metier.EOFacturePapierLigne)storedValueForKey(FACTURE_PAPIER_LIGNE_PERE_KEY);
  }

  public void setFacturePapierLignePereRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOFacturePapierLigne oldValue = facturePapierLignePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FACTURE_PAPIER_LIGNE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FACTURE_PAPIER_LIGNE_PERE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOPrestationLigne prestationLigne() {
    return (org.cocktail.kava.server.metier.EOPrestationLigne)storedValueForKey(PRESTATION_LIGNE_KEY);
  }

  public void setPrestationLigneRelationship(org.cocktail.kava.server.metier.EOPrestationLigne value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOPrestationLigne oldValue = prestationLigne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PRESTATION_LIGNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PRESTATION_LIGNE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTva tva() {
    return (org.cocktail.kava.server.metier.EOTva)storedValueForKey(TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.kava.server.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTva oldValue = tva();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTva tvaInitial() {
    return (org.cocktail.kava.server.metier.EOTva)storedValueForKey(TVA_INITIAL_KEY);
  }

  public void setTvaInitialRelationship(org.cocktail.kava.server.metier.EOTva value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTva oldValue = tvaInitial();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TVA_INITIAL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TVA_INITIAL_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOTypeArticle typeArticle() {
    return (org.cocktail.kava.server.metier.EOTypeArticle)storedValueForKey(TYPE_ARTICLE_KEY);
  }

  public void setTypeArticleRelationship(org.cocktail.kava.server.metier.EOTypeArticle value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOTypeArticle oldValue = typeArticle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ARTICLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ARTICLE_KEY);
    }
  }
  
  public NSArray facturePapierLignes() {
    return (NSArray)storedValueForKey(FACTURE_PAPIER_LIGNES_KEY);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier) {
    return facturePapierLignes(qualifier, null, false);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier, boolean fetch) {
    return facturePapierLignes(qualifier, null, fetch);
  }

  public NSArray facturePapierLignes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.kava.server.metier.EOFacturePapierLigne.FACTURE_PAPIER_LIGNE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.kava.server.metier.EOFacturePapierLigne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = facturePapierLignes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
  }

  public void removeFromFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
  }

  public org.cocktail.kava.server.metier.EOFacturePapierLigne createFacturePapierLignesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FacturePapierLigne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FACTURE_PAPIER_LIGNES_KEY);
    return (org.cocktail.kava.server.metier.EOFacturePapierLigne) eo;
  }

  public void deleteFacturePapierLignesRelationship(org.cocktail.kava.server.metier.EOFacturePapierLigne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FACTURE_PAPIER_LIGNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFacturePapierLignesRelationships() {
    Enumeration objects = facturePapierLignes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFacturePapierLignesRelationship((org.cocktail.kava.server.metier.EOFacturePapierLigne)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOFacturePapierLigne avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOFacturePapierLigne createEOFacturePapierLigne(EOEditingContext editingContext, java.math.BigDecimal fligArtHt
, java.math.BigDecimal fligArtTtc
, java.math.BigDecimal fligArtTtcInitial
, NSTimestamp fligDate
, String fligDescription
, java.math.BigDecimal fligQuantite
, java.math.BigDecimal fligTotalHt
, java.math.BigDecimal fligTotalTtc
, org.cocktail.kava.server.metier.EOTypeArticle typeArticle			) {
    EOFacturePapierLigne eo = (EOFacturePapierLigne) createAndInsertInstance(editingContext, _EOFacturePapierLigne.ENTITY_NAME);    
		eo.setFligArtHt(fligArtHt);
		eo.setFligArtTtc(fligArtTtc);
		eo.setFligArtTtcInitial(fligArtTtcInitial);
		eo.setFligDate(fligDate);
		eo.setFligDescription(fligDescription);
		eo.setFligQuantite(fligQuantite);
		eo.setFligTotalHt(fligTotalHt);
		eo.setFligTotalTtc(fligTotalTtc);
    eo.setTypeArticleRelationship(typeArticle);
    return eo;
  }

  
	  public EOFacturePapierLigne localInstanceIn(EOEditingContext editingContext) {
	  		return (EOFacturePapierLigne)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFacturePapierLigne creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOFacturePapierLigne creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOFacturePapierLigne object = (EOFacturePapierLigne)createAndInsertInstance(editingContext, _EOFacturePapierLigne.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOFacturePapierLigne localInstanceIn(EOEditingContext editingContext, EOFacturePapierLigne eo) {
    EOFacturePapierLigne localInstance = (eo == null) ? null : (EOFacturePapierLigne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOFacturePapierLigne#localInstanceIn a la place.
   */
	public static EOFacturePapierLigne localInstanceOf(EOEditingContext editingContext, EOFacturePapierLigne eo) {
		return EOFacturePapierLigne.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOFacturePapierLigne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOFacturePapierLigne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOFacturePapierLigne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOFacturePapierLigne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOFacturePapierLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOFacturePapierLigne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOFacturePapierLigne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOFacturePapierLigne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOFacturePapierLigne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOFacturePapierLigne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOFacturePapierLigne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOFacturePapierLigne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
