/*
 * Copyright Cocktail, 2001-2011 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCataloguePrestation.java instead.
package org.cocktail.kava.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOCataloguePrestation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "CataloguePrestation";
	public static final String ENTITY_TABLE_NAME = "jefy_recette.catalogue_prestation";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "catId";

	public static final String CAT_DATE_VOTE_KEY = "catDateVote";
	public static final String CAT_NUMERO_KEY = "catNumero";
	public static final String CAT_PUBLIE_WEB_KEY = "catPublieWeb";
	public static final String PCO_NUM_DEPENSE_KEY = "pcoNumDepense";
	public static final String PCO_NUM_RECETTE_KEY = "pcoNumRecette";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String LOLF_ID_RECETTE_KEY = "lolfIdRecette";
	public static final String ORG_ID_RECETTE_KEY = "orgIdRecette";

//Colonnes dans la base de donnees
	public static final String CAT_DATE_VOTE_COLKEY = "CAT_DATE_VOTE";
	public static final String CAT_NUMERO_COLKEY = "CAT_NUMERO";
	public static final String CAT_PUBLIE_WEB_COLKEY = "CAT_PUBLIE_WEB";
	public static final String PCO_NUM_DEPENSE_COLKEY = "PCO_NUM_DEPENSE";
	public static final String PCO_NUM_RECETTE_COLKEY = "PCO_NUM_RECETTE";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String LOLF_ID_RECETTE_COLKEY = "LOLF_ID_RECETTE";
	public static final String ORG_ID_RECETTE_COLKEY = "ORG_ID_RECETTE";


	// Relationships
	public static final String LOLF_NOMENCLATURE_RECETTE_KEY = "lolfNomenclatureRecette";
	public static final String ORGAN_RECETTE_KEY = "organRecette";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public NSTimestamp catDateVote() {
    return (NSTimestamp) storedValueForKey(CAT_DATE_VOTE_KEY);
  }

  public void setCatDateVote(NSTimestamp value) {
    takeStoredValueForKey(value, CAT_DATE_VOTE_KEY);
  }

  public Integer catNumero() {
    return (Integer) storedValueForKey(CAT_NUMERO_KEY);
  }

  public void setCatNumero(Integer value) {
    takeStoredValueForKey(value, CAT_NUMERO_KEY);
  }

  public String catPublieWeb() {
    return (String) storedValueForKey(CAT_PUBLIE_WEB_KEY);
  }

  public void setCatPublieWeb(String value) {
    takeStoredValueForKey(value, CAT_PUBLIE_WEB_KEY);
  }

  public String pcoNumDepense() {
    return (String) storedValueForKey(PCO_NUM_DEPENSE_KEY);
  }

  public void setPcoNumDepense(String value) {
    takeStoredValueForKey(value, PCO_NUM_DEPENSE_KEY);
  }

  public String pcoNumRecette() {
    return (String) storedValueForKey(PCO_NUM_RECETTE_KEY);
  }

  public void setPcoNumRecette(String value) {
    takeStoredValueForKey(value, PCO_NUM_RECETTE_KEY);
  }

  public org.cocktail.kava.server.metier.EOLolfNomenclatureRecette lolfNomenclatureRecette() {
    return (org.cocktail.kava.server.metier.EOLolfNomenclatureRecette)storedValueForKey(LOLF_NOMENCLATURE_RECETTE_KEY);
  }

  public void setLolfNomenclatureRecetteRelationship(org.cocktail.kava.server.metier.EOLolfNomenclatureRecette value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOLolfNomenclatureRecette oldValue = lolfNomenclatureRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, LOLF_NOMENCLATURE_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, LOLF_NOMENCLATURE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.kava.server.metier.EOOrgan organRecette() {
    return (org.cocktail.kava.server.metier.EOOrgan)storedValueForKey(ORGAN_RECETTE_KEY);
  }

  public void setOrganRecetteRelationship(org.cocktail.kava.server.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.kava.server.metier.EOOrgan oldValue = organRecette();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_RECETTE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_RECETTE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCataloguePrestation avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCataloguePrestation createEOCataloguePrestation(EOEditingContext editingContext, Integer catNumero
, String catPublieWeb
			) {
    EOCataloguePrestation eo = (EOCataloguePrestation) createAndInsertInstance(editingContext, _EOCataloguePrestation.ENTITY_NAME);    
		eo.setCatNumero(catNumero);
		eo.setCatPublieWeb(catPublieWeb);
    return eo;
  }

  
	  public EOCataloguePrestation localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCataloguePrestation)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCataloguePrestation creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCataloguePrestation creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOCataloguePrestation object = (EOCataloguePrestation)createAndInsertInstance(editingContext, _EOCataloguePrestation.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCataloguePrestation localInstanceIn(EOEditingContext editingContext, EOCataloguePrestation eo) {
    EOCataloguePrestation localInstance = (eo == null) ? null : (EOCataloguePrestation)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCataloguePrestation#localInstanceIn a la place.
   */
	public static EOCataloguePrestation localInstanceOf(EOEditingContext editingContext, EOCataloguePrestation eo) {
		return EOCataloguePrestation.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCataloguePrestation fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCataloguePrestation fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCataloguePrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCataloguePrestation)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCataloguePrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCataloguePrestation fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCataloguePrestation eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCataloguePrestation)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCataloguePrestation fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCataloguePrestation eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCataloguePrestation ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCataloguePrestation fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
