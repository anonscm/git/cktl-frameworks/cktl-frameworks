package org.cocktail.pieFwk.selector;

public interface IFiltre<K> {

	boolean controler(K element);

}
