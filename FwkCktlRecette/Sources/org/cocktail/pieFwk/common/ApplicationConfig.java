package org.cocktail.pieFwk.common;

import com.webobjects.eocontrol.EOEditingContext;

public interface ApplicationConfig {

	int nbDecimalesImposees(EOEditingContext edc, Number exercice);

}
