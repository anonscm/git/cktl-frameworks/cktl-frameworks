package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.service.PrestationLigneService;

import com.webobjects.foundation.NSArray;

public class PrestationCompanion {

	private Prestation prestation;
	private PrestationLigneService ligneService;

	public PrestationCompanion(Prestation prestation) {
		this.prestation = prestation;
		this.ligneService = PrestationLigneService.instance();
	}

	public BigDecimal totalHTLive() {
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal totalLive = BigDecimal.ZERO;
		for (int idx = 0; idx < prestationLignes().count(); idx++) {
			PrestationLigne ligne = (PrestationLigne) prestationLignes().objectAtIndex(idx);
			BigDecimal ligneTotalHT = ligneService().calculerMontantTotalHTExact(ligne.prligArtHt(), ligne.prligQuantite());
			totalLive = totalLive.add(ligneTotalHT);
		}

		return ligneService().applyRemise(
				totalLive, prestation().remiseGlobale(),
				ligneService().determineNombreDecimalesImposees(prestation()));
	}

	public BigDecimal totalTTCLive() {
		if (prestationLignes() == null || prestationLignes().count() == 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal totalLive = BigDecimal.ZERO;
		for (int idx = 0; idx < prestationLignes().count(); idx++) {
			PrestationLigne ligne = (PrestationLigne) prestationLignes().objectAtIndex(idx);
			BigDecimal ligneTotalTTC = ligneService().calculerMontantTotalTTCExact(
					ligne.prligArtHt(),
					ligne.prligArtTtc(),
					ligne.prligQuantite(),
					ligne.companion().tauxTvaCatalogueArticle());
			totalLive = totalLive.add(ligneTotalTTC);
		}

		return ligneService().applyRemise(
				totalLive, prestation().remiseGlobale(),
				ligneService().determineNombreDecimalesImposees(prestation()));
	}

	public BigDecimal totalTVALive() {
		return totalTTCLive().subtract(totalHTLive());
	}

	public void updateTotaux() {
		BigDecimal totalHTLive = totalHTLive();
		BigDecimal totalTTCLive = totalTTCLive();
		BigDecimal totalTVALive = totalTTCLive.subtract(totalHTLive);

		prestation().setTotalHT(totalHTLive);
		prestation().setTotalTVA(totalTVALive);
		prestation().setTotalTTC(totalTTCLive);
	}

	public Prestation prestation() {
		return this.prestation;
	}

	public NSArray prestationLignes() {
		if (prestation() == null) {
			return new NSArray();
		}

		return prestation().getPrestationLignesCommon();
	}

	public PrestationLigneService ligneService() {
		return this.ligneService;
	}
}
