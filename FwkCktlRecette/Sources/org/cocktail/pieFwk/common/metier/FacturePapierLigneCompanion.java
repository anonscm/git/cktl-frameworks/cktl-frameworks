package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.service.FacturePapierLigneService;

import com.webobjects.foundation.NSArray;

public class FacturePapierLigneCompanion {

	//TODO si possible faire en sorte que les méthodes retourne un résultat (en particulier sur les updateTotalXXX).
	// et voir les impacts. Dans cette premiere version, on garde le fonctionnement deja en place.

	private FacturePapierLigneService ligneService;
	private FacturePapierLigne ligne;

	public FacturePapierLigneCompanion(FacturePapierLigne ligne) {
		this.ligne = ligne;
		this.ligneService = FacturePapierLigneService.instance();
	}

	public void modifierQuantite(BigDecimal quantite) {
		ligne().setFligQuantite(quantite);
		updateTotaux();
		modifierQuantiteEnfants(quantite);
	}

	protected void modifierQuantiteEnfants(BigDecimal quantite) {
		NSArray lignesEnfants = ligne().facturePapierLignes();
		if (lignesEnfants != null) {
			for (int i = 0; i < lignesEnfants.count(); i++) {
				((FacturePapierLigne) lignesEnfants.objectAtIndex(i)).companion().modifierQuantite(quantite);
			}
		}
	}

	public void updateTotaux() {
		updateTotalHt();
		updateTotalTtc();
	}

	/**
	 * met a jour le total ht de la ligne, fonction du prix unitaire, de la quantite et de la remise eventuelle
	 */
	public void updateTotalHt() {
		BigDecimal articleHT = ligne().fligArtHt();
		BigDecimal quantite = ligne().fligQuantite();
		BigDecimal totalHTRemiseIncluse = null;

		if (isCalculPossible(articleHT, quantite)) {
			totalHTRemiseIncluse = getLigneService().calculerMontantTotalHTRemiseIncluse(ligne());
		}

		ligne().setFligTotalHt(totalHTRemiseIncluse);
	}

	/**
	 * met a jour le total ttc de la ligne, fonction du prix unitaire, de la quantite et de la remise eventuelle
	 */
	public void updateTotalTtc() {
		BigDecimal articleTTC = ligne().fligArtTtc();
		BigDecimal quantite = ligne().fligQuantite();
		BigDecimal totalTTCRemiseIncluse = null;

		if (isCalculPossible(articleTTC, quantite)) {
			totalTTCRemiseIncluse = getLigneService().calculerMontantTotalTTCRemiseIncluse(ligne());
		}

		ligne().setFligTotalTtc(totalTTCRemiseIncluse);
	}

	protected boolean isCalculPossible(BigDecimal montant, BigDecimal quantite) {
		return montant != null && quantite != null;
	}

	public FacturePapierLigne ligne() {
		return ligne;
	}

	public FacturePapierLigneService getLigneService() {
		return ligneService;
	}

	public void setLigneService(FacturePapierLigneService ligneService) {
		this.ligneService = ligneService;
	}
}
