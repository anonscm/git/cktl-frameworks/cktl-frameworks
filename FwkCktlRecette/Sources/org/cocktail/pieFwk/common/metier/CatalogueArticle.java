package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;


public interface CatalogueArticle {

	BigDecimal tauxTVA();
	Article getArticleCommon();

}
