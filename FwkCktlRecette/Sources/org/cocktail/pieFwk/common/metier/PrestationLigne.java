package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;

public interface PrestationLigne {

	PrestationLigneCompanion companion();

	BigDecimal prligArtHt();
	BigDecimal prligArtTtc();
	BigDecimal prligQuantite();
	BigDecimal prligQuantiteReste();

	void setPrligQuantite(BigDecimal quantite);
	void setPrligQuantiteReste(BigDecimal quantite);
	void setPrligTotalHt(BigDecimal totalHT);
	void setPrligTotalTtc(BigDecimal totalTTC);
	void setPrligTotalResteHt(BigDecimal totalResteHT);
	void setPrligTotalResteTtc(BigDecimal totalResteTTC);

	Prestation getPrestationCommon();
	CatalogueArticle getCatalogueArticleCommon();

	ApplicationConfig applicationConfig();
	EOEditingContext editingContext();
	Number exerciceAsNumber();
}
