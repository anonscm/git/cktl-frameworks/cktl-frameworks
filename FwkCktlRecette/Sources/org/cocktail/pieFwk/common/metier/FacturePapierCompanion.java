package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.kava.client.finder.FinderTypePublic;
import org.cocktail.kava.client.metier.EOArticlePrestation;
import org.cocktail.kava.client.metier.EOFacturePapierLigne;
import org.cocktail.kava.client.metier.EOPrestationLigne;
import org.cocktail.pieFwk.common.service.FacturePapierLigneService;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class FacturePapierCompanion {

	private FacturePapier facturePapier;
	private FacturePapierLigneService ligneService;

	public FacturePapierCompanion(FacturePapier facturePapier) {
		this.facturePapier = facturePapier;
		this.ligneService = FacturePapierLigneService.instance();
	}

	public BigDecimal totalHTLive() {
		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal totalLive = BigDecimal.ZERO;

		for (int idx = 0; idx < facturePapierLignes().count(); idx++) {
			FacturePapierLigne ligne = (FacturePapierLigne) facturePapierLignes().objectAtIndex(idx);
			BigDecimal articleHT = ligne.fligArtHt();
			BigDecimal quantite = ligne.fligQuantite();
			BigDecimal ligneTotalHT = ligneService().calculerMontantTotalHTExact(articleHT, quantite);
			totalLive = totalLive.add(ligneTotalHT);
		}

		return ligneService().applyRemise(
				totalLive, facturePapier().remiseGlobale(),
				ligneService().determineNombreDecimalesImposees(facturePapier()));
	}

	public BigDecimal totalTTCLive() {

		if (facturePapierLignes() == null || facturePapierLignes().count() == 0) {
			return BigDecimal.ZERO;
		}

		BigDecimal totalLive = BigDecimal.ZERO;
		for (int idx = 0; idx < facturePapierLignes().count(); idx++) {
			FacturePapierLigne ligne = (FacturePapierLigne) facturePapierLignes().objectAtIndex(idx);
			BigDecimal ligneTotalTTC = ligneService().calculerMontantTotalTTCExact(
					ligne.fligArtHt(),
					ligne.fligArtTtc(),
					ligne.fligQuantite(),
					ligneService().getTauxTva(ligne));
			totalLive = totalLive.add(ligneTotalTTC);
		}

		return ligneService().applyRemise(
				totalLive, facturePapier().remiseGlobale(),
				ligneService().determineNombreDecimalesImposees(facturePapier()));
	}

	public BigDecimal totalTVALive() {
		return totalTTCLive().subtract(totalHTLive());
	}

	public void updateTotaux() {
		BigDecimal totalHTLive = totalHTLive();
		BigDecimal totalTTCLive = totalTTCLive();
		BigDecimal totalTVALive = totalTTCLive.subtract(totalHTLive).setScale(totalHTLive.scale());

		facturePapier().setTotalHT(totalHTLive);
		facturePapier().setTotalTVA(totalTVALive);
		facturePapier().setTotalTTC(totalTTCLive);
	}

	public NSTimestamp[] getDateDebutEtFinFormationContinue() {
		NSTimestamp[] datesDebutEtFin = new NSTimestamp[2];
		NSTimestamp contrainteDate1ereEcheance = null;
		NSTimestamp contrainteDateDerniereEcheance = null;

		for (int i = 0; i < facturePapierLignes().count(); i++) {
			PrestationLigne lignePrestation = ((FacturePapierLigne) facturePapierLignes().objectAtIndex(i)).getPrestationLigneCommon();
			if (lignePrestation != null && lignePrestation.getCatalogueArticleCommon() != null) {
				ArticlePrestation prestation = lignePrestation.getCatalogueArticleCommon().getArticleCommon().getArticlePrestationCommon();
				contrainteDate1ereEcheance = conserverDateLaPlusRecente(contrainteDate1ereEcheance, prestation.dateDebut());
				contrainteDateDerniereEcheance = conserverDateLaPlusTard(contrainteDateDerniereEcheance, prestation.dateFin());
			}
		}

		// la date de fin doit etre 1 mois avant la fin de la formation
		if (contrainteDateDerniereEcheance != null) {
			contrainteDateDerniereEcheance = contrainteDateDerniereEcheance.timestampByAddingGregorianUnits(0, -1, 0, 0, 0, 0);
		}

		datesDebutEtFin[0] = contrainteDate1ereEcheance;
		datesDebutEtFin[1] = contrainteDateDerniereEcheance;

		return datesDebutEtFin;
	}

	protected NSTimestamp conserverDateLaPlusRecente(NSTimestamp date, NSTimestamp dateAEvaluer) {
		if (date == null) {
			return dateAEvaluer;
		}

		if (dateAEvaluer != null && dateAEvaluer.before(date)) {
			return dateAEvaluer;
		}

		return date;
	}

	protected NSTimestamp conserverDateLaPlusTard(NSTimestamp date, NSTimestamp dateAEvaluer) {
		if (date == null) {
			return dateAEvaluer;
		}

		if (dateAEvaluer != null && dateAEvaluer.after(date)) {
			return dateAEvaluer;
		}

		return date;
	}

	public FacturePapier facturePapier() {
		return this.facturePapier;
	}

	public NSArray facturePapierLignes() {
		if (facturePapier() == null) {
			return new NSArray();
		}

		return facturePapier().getFacturePapierLignesCommon();
	}

	public FacturePapierLigneService ligneService() {
		return this.ligneService;
	}
}
