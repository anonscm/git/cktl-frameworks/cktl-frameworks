package org.cocktail.pieFwk.common.metier;

import com.webobjects.foundation.NSTimestamp;

public interface ArticlePrestation {

	NSTimestamp dateDebut();
	NSTimestamp dateFin();

}
