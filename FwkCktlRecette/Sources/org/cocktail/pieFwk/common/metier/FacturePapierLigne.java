package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface FacturePapierLigne {

	FacturePapierLigneCompanion companion();

	BigDecimal fligArtHt();
	void setFligArtHt(BigDecimal montantHT);

	BigDecimal fligArtTtc();
	void setFligArtTtc(BigDecimal montantTTC);

	BigDecimal fligTotalHt();
	void setFligTotalHt(BigDecimal totalHT);

	BigDecimal fligTotalTtc();
	void setFligTotalTtc(BigDecimal totalTTC);

	BigDecimal fligQuantite();
	void setFligQuantite(BigDecimal quantite);

	TauxTva tauxTva();

	NSArray facturePapierLignes();
	void addToFacturePapierLignesRelationship(FacturePapierLigne ligne);

	PrestationLigne getPrestationLigneCommon();
	FacturePapier getFacturePapierCommon();

	ApplicationConfig applicationConfig();
	EOEditingContext editingContext();
	Number exerciceAsNumber();
}
