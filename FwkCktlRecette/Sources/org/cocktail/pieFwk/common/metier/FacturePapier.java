package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public interface FacturePapier {

	FacturePapierCompanion companion();

	void setTotalHT(BigDecimal totalHT);
	void setTotalTVA(BigDecimal totalTVA);
	void setTotalTTC(BigDecimal totalTTC);

	BigDecimal remiseGlobale();
	NSArray getFacturePapierLignesCommon();

	ApplicationConfig applicationConfig();
	EOEditingContext editingContext();
	Number exerciceAsNumber();
}
