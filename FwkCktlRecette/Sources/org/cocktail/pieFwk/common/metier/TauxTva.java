package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

public interface TauxTva {

	BigDecimal tauxTVA();

}
