package org.cocktail.pieFwk.common.metier;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.service.PrestationLigneService;

public class PrestationLigneCompanion {

	private PrestationLigne ligne;
	private PrestationLigneService ligneService;

	public PrestationLigneCompanion(PrestationLigne ligne) {
		this.ligne = ligne;
		this.ligneService = PrestationLigneService.instance();
	}

	public PrestationLigneService service() {
		return this.ligneService;
	}

	public void updateTotaux() {
		updateTotalHt();
		updateTotalTtc();
		updateTotalResteHt();
		updateTotalResteTtc();
	}

	/**
	 * met a jour le total ht de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ht a 0 ou plus
	 */
	public void updateTotalHt() {
		BigDecimal articleHT = ligne().prligArtHt();
		BigDecimal quantite = ligne().prligQuantite();
		BigDecimal totalHTRemiseIncluse = null;

		if (isCalculPossible(articleHT, quantite)) {
			totalHTRemiseIncluse = service().calculerMontantTotalHTRemiseIncluse(ligne(), quantite);
		}

		ligne().setPrligTotalHt(totalHTRemiseIncluse);
	}

	/**
	 * met a jour le total reste ht de la ligne, fonction du prix unitaire, de
	 * la quantite restante et de la remise eventuelle, scale 0 ou 2 selon scale
	 * prix unitaire ht a 0 ou plus
	 */
	public void updateTotalResteHt() {
		BigDecimal articleHT = ligne().prligArtHt();
		BigDecimal quantiteReste = ligne().prligQuantiteReste();
		BigDecimal totalResteHTRemiseIncluse = null;

		if (isCalculPossible(articleHT, quantiteReste)) {
		    totalResteHTRemiseIncluse = service().calculerMontantTotalHTRemiseIncluse(ligne(), quantiteReste);
		}

		ligne().setPrligTotalResteHt(totalResteHTRemiseIncluse);
	}

	/**
	 * met a jour le total ttc de la ligne, fonction du prix unitaire, de la
	 * quantite et de la remise eventuelle, scale 0 ou 2 selon scale prix
	 * unitaire ttc a 0 ou plus
	 */
	public void updateTotalTtc() {
		BigDecimal articleHT = ligne().prligArtHt();
		BigDecimal articleTTC = ligne().prligArtTtc();
		BigDecimal quantite = ligne().prligQuantite();
		BigDecimal totalTTCRemiseIncluse = null;

		if (isCalculPossible(articleTTC, quantite)) {
			totalTTCRemiseIncluse = service().calculerMontantTotalTTCRemiseIncluse(
					ligne(), articleHT, articleTTC, quantite);
		}

		ligne().setPrligTotalTtc(totalTTCRemiseIncluse);
	}

	/**
	 * met a jour le total rest ttc de la ligne, fonction du prix unitaire, de
	 * la quantite restante et de la remise eventuelle, scale 0 ou 2 selon scale
	 * prix unitaire ttc a 0 ou plus
	 */
	public void updateTotalResteTtc() {
		BigDecimal articleHT = ligne().prligArtHt();
		BigDecimal articleTTC = ligne().prligArtTtc();
		// BigDecimal quantite = ligne.prligQuantite();
		BigDecimal quantiteReste = ligne().prligQuantiteReste();
		BigDecimal totalResteTTCRemiseIncluse = null;

		// Actuellement impossible de corriger cette anomalie car appel cyclique.
//		if (quantiteReste == null) {
//			ligne().setPrligQuantiteReste(quantite);
//		}

		if (isCalculPossible(articleTTC, quantiteReste)) {
			totalResteTTCRemiseIncluse = service().calculerMontantTotalTTCRemiseIncluse(
					ligne(), articleHT, articleTTC, quantiteReste);
		}

		ligne().setPrligTotalResteTtc(totalResteTTCRemiseIncluse);
	}

	public BigDecimal tauxTvaCatalogueArticle() {
		if (ligne() != null && ligne().getCatalogueArticleCommon() != null) {
			return ligne().getCatalogueArticleCommon().tauxTVA();
		}

		return null;
	}

	protected boolean isCalculPossible(BigDecimal montant, BigDecimal quantite) {
		return montant != null && quantite != null;
	}

	public PrestationLigne ligne() {
		return ligne;
	}
}
