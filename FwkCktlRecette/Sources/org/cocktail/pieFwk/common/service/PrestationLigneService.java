package org.cocktail.pieFwk.common.service;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.metier.Prestation;
import org.cocktail.pieFwk.common.metier.PrestationLigne;

public class PrestationLigneService extends AbstractFacturationService {

	private static final PrestationLigneService INSTANCE  = new PrestationLigneService();

	public static PrestationLigneService instance() {
		return INSTANCE;
	}

	private PrestationLigneService() {
	}

	public BigDecimal getRemiseGlobale(PrestationLigne ligne) {
		if (ligne == null || ligne.getPrestationCommon() == null) {
			return null;
		}

		return ligne.getPrestationCommon().remiseGlobale();
	}

	public BigDecimal calculerMontantTotalHTRemiseIncluse(PrestationLigne ligne, BigDecimal quantite) {
		BigDecimal articleHT = ligne.prligArtHt();
		Integer nbDecimales = determineNombreDecimalesPourPrecision(ligne);

		BigDecimal montantTotalHTExact = calculerMontantTotalHTExact(articleHT, quantite);

		return applyRemise(montantTotalHTExact, getRemiseGlobale(ligne)).setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal calculerMontantTotalTTCRemiseIncluse(PrestationLigne ligne, BigDecimal montantHT, BigDecimal montantTTC, BigDecimal quantite) {
		int nbDecimalesImposees = determineNombreDecimalesPourPrecision(ligne);
		BigDecimal tauxTvaSiPresent = null;

		if (ligne != null) {
			tauxTvaSiPresent = ligne.companion().tauxTvaCatalogueArticle();
		}

		BigDecimal totalTTExact = calculerMontantTotalTTCExact(montantHT, montantTTC, quantite, tauxTvaSiPresent);

		return applyRemise(totalTTExact, getRemiseGlobale(ligne)).setScale(nbDecimalesImposees, BigDecimal.ROUND_HALF_UP);
	}

	public int determineNombreDecimalesImposees(PrestationLigne ligne) {
		return determineNombreDecimalesImposees(ligne.applicationConfig(), ligne.editingContext(), ligne.exerciceAsNumber());
	}

	public int determineNombreDecimalesImposees(Prestation prestation) {
		return determineNombreDecimalesImposees(prestation.applicationConfig(), prestation.editingContext(), prestation.exerciceAsNumber());
	}
	
	public int determineNombreDecimalesPourPrecision(PrestationLigne ligne) {
	    return determineNombreDecimalesPourPrecision(ligne.applicationConfig(), ligne.editingContext(), ligne.exerciceAsNumber());
	}
}
