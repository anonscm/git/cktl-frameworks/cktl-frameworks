package org.cocktail.pieFwk.common.service;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;

import com.webobjects.eocontrol.EOEditingContext;

public interface FacturationService {

	// TODO voir si il est interessant d'avoir une interface Produit avec HT / QTE / TTC
	BigDecimal applyRemise(BigDecimal montant, BigDecimal remiseGlobaleEnPourcent);
	BigDecimal applyRemise(BigDecimal montant, BigDecimal remiseGlobaleEnPourcent, Integer nbDecimales);
	BigDecimal calculerTauxTva(BigDecimal mtHT, BigDecimal mtTTC, int scale, BigDecimal tauxTvaParDefaut);

	int determineNombreDecimalesImposees(ApplicationConfig config, EOEditingContext edc, Number exercice);


	/**
	 * Calcul le montant total HT sans appliquer d'arrondi au résultat final.
	 * @param produitHT montant du produit.
	 * @param quantite  quantite de produit.
	 * @return le montant total HT sans appliquer d'arrondi au résultat final.
	 */
	BigDecimal calculerMontantTotalHTExact(BigDecimal produitHT, BigDecimal quantite);

	/**
	 * Calcul le montant total TTC sans appliquer d'arrondi au résultat final.
	 * @param produitHT  montant du produit HT.
	 * @param produitTTC montant du produit TTC.
	 * @param quantite  quantite de produit.
	 * @param tauxTVAEnPourcent taux TVA (19.6 par exemple) à utiliser. Optionnel. le tx peut etre recalculé depuis TTC / HT
	 * @return le montant total TTC sans appliquer d'arrondi au résultat final.
	 */
	BigDecimal calculerMontantTotalTTCExact(BigDecimal produitHT, BigDecimal produitTTC, BigDecimal quantite, BigDecimal tauxTVAEnPourcent);
}