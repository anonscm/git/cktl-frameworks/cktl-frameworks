package org.cocktail.pieFwk.common.service;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;
import org.cocktail.pieFwk.common.metier.FacturePapier;
import org.cocktail.pieFwk.common.metier.FacturePapierLigne;
import org.cocktail.pieFwk.common.metier.PrestationLigne;

import com.webobjects.eocontrol.EOEditingContext;

public class FacturePapierLigneService extends AbstractFacturationService {

	private static final FacturePapierLigneService INSTANCE  = new FacturePapierLigneService();

	public static FacturePapierLigneService instance() {
		return INSTANCE;
	}

	private FacturePapierLigneService() {
	}

	public BigDecimal getRemiseGlobale(FacturePapierLigne ligne) {
		if (ligne == null || ligne.getFacturePapierCommon() == null) {
			return null;
		}

		return ligne.getFacturePapierCommon().remiseGlobale();
	}

	public BigDecimal calculerMontantTotalHTRemiseIncluse(FacturePapierLigne ligneFacture) {
		BigDecimal articleHT = ligneFacture.fligArtHt();
		BigDecimal quantite = ligneFacture.fligQuantite();
		int nbDecimales = determineNombreDecimalesPourPrecision(ligneFacture);

		BigDecimal montantTotalHTExact = calculerMontantTotalHTExact(articleHT, quantite);

		return applyRemise(montantTotalHTExact, getRemiseGlobale(ligneFacture))
					.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal calculerMontantTotalTTCRemiseIncluse(FacturePapierLigne ligneFacture) {
		BigDecimal articleHT = ligneFacture.fligArtHt();
		BigDecimal articleTTC = ligneFacture.fligArtTtc();
		BigDecimal quantite = ligneFacture.fligQuantite();
		BigDecimal tauxTvaSiPresent = getTauxTva(ligneFacture);
		int nbDecimales = determineNombreDecimalesPourPrecision(ligneFacture);

		BigDecimal totalTTExact = calculerMontantTotalTTCExact(articleHT, articleTTC, quantite, tauxTvaSiPresent);

		return applyRemise(totalTTExact, getRemiseGlobale(ligneFacture))
				.setScale(nbDecimales, BigDecimal.ROUND_HALF_UP);
	}

	public BigDecimal getTauxTva(FacturePapierLigne ligneFacture) {
		if (ligneFacture != null && ligneFacture.tauxTva() != null) {
			return ligneFacture.tauxTva().tauxTVA();
		}

		CatalogueArticle catalogueArticle = catalogueArticle(ligneFacture);
		if (catalogueArticle != null) {
			return catalogueArticle.tauxTVA();
		}

		return null;
	}

	protected CatalogueArticle catalogueArticle(FacturePapierLigne ligne) {
		if (ligne == null || ligne.getPrestationLigneCommon() == null || ligne.getPrestationLigneCommon().getCatalogueArticleCommon() == null) {
			return null;
		}

		return ligne.getPrestationLigneCommon().getCatalogueArticleCommon();
	}

	public int determineNombreDecimalesImposees(FacturePapierLigne ligne) {
		return determineNombreDecimalesImposees(ligne.applicationConfig(), ligne.editingContext(), ligne.exerciceAsNumber());
	}

	public int determineNombreDecimalesImposees(FacturePapier facturePapier) {
		return determineNombreDecimalesImposees(facturePapier.applicationConfig(), facturePapier.editingContext(), facturePapier.exerciceAsNumber());
	}
	
	public int determineNombreDecimalesPourPrecision(FacturePapierLigne ligne) {
	    return determineNombreDecimalesPourPrecision(ligne.applicationConfig(), ligne.editingContext(), ligne.exerciceAsNumber());
	}
}
