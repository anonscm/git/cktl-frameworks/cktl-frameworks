package org.cocktail.pieFwk.common.service;

import java.math.BigDecimal;

import org.cocktail.pieFwk.common.ApplicationConfig;
import org.cocktail.pieFwk.common.metier.CatalogueArticle;

import com.webobjects.eocontrol.EOEditingContext;

public abstract class AbstractFacturationService implements FacturationService {

	public static final int ZERO_DECIMALE = 0;
	public static final int DEUX_DECIMALES = 2;
	public static final int TROIS_DECIMALES = 3;
	public static final int NB_DECIMALES_TAUX_TVA = 3;
	public static final BigDecimal TAUX_TVA_PAR_DEFAUT = BigDecimal.ONE;
	public static final BigDecimal POURCENTAGE_100 = BigDecimal.valueOf(100.0d);

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal calculerMontantTotalHTExact(BigDecimal produitHT, BigDecimal quantite) {
	    if (produitHT == null) {
	        produitHT = BigDecimal.ZERO;
        }
        if (quantite == null) {
            quantite = BigDecimal.ZERO;
        }
		return produitHT.multiply(quantite);
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal calculerMontantTotalTTCExact(BigDecimal produitHT, BigDecimal produitTTC, BigDecimal quantite, BigDecimal tauxTVAEnPourcent) {
		BigDecimal montantTotalTTCExact = null;
		
        if (quantite == null) {
            quantite = BigDecimal.ZERO;
        }
        
        // FLA : loin d'etre parfait encore car on corrige bien le montant TTC mais on laisse l'article avec un TTC qui peut être faux
        // ex : article Ht : 100 / Tx TVA 20% / Ttc : 15.000 (completement faux)
        // on aura bien un TTC a 120 euros avec la méthode de calcul en total de facture mais l'article sera encore a 15.000.
		if (isCalculTotalTTCAvecTVACommeBaseCalcul(produitHT, produitTTC, quantite)) {
			montantTotalTTCExact = calculerMontantTotalTTCRemiseIncluseAvecBaseTVA(produitHT, produitTTC, quantite, tauxTVAEnPourcent);
		} else if (isCalculTotalTTCPossibleAvecHTCommeBaseCalcul(produitHT, produitTTC, quantite)) {
            montantTotalTTCExact = calculerMontantTotalTTCRemiseIncluseAvecBaseHT(produitHT, produitTTC, quantite, tauxTVAEnPourcent);
		}

		return montantTotalTTCExact;
	}

	protected BigDecimal calculerMontantTotalTTCRemiseIncluseAvecBaseHT(BigDecimal produitHT, BigDecimal produitTTC, BigDecimal quantite, BigDecimal tauxTVAEnPourcent) {
		BigDecimal totalHTExact = calculerMontantTotalHTExact(produitHT, quantite);

		BigDecimal tauxTva = null;
		if (tauxTVAEnPourcent != null) {
			tauxTva = calculerTauxTva(tauxTVAEnPourcent, NB_DECIMALES_TAUX_TVA);
		} else {
			tauxTva = calculerTauxTva(produitHT, produitTTC, NB_DECIMALES_TAUX_TVA, TAUX_TVA_PAR_DEFAUT);
		}

		return totalHTExact.multiply(tauxTva);
	}

	protected BigDecimal calculerMontantTotalTTCRemiseIncluseAvecBaseTVA(BigDecimal produitHT, BigDecimal produitTTC, BigDecimal quantite, BigDecimal tauxTVAEnPourcent) {
		return produitTTC.multiply(quantite);
	}

	public BigDecimal applyRemise(BigDecimal montant, BigDecimal remiseGlobaleEnPourcent) {
		return applyRemise(montant, remiseGlobaleEnPourcent, null);
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal applyRemise(BigDecimal montant, BigDecimal remiseGlobaleEnPourcent, Integer nbDecimales) {
		if (montant == null) {
			return BigDecimal.ZERO;
		}

		int nbDecimalesImposees = montant.scale();
		if (nbDecimales != null) {
			nbDecimalesImposees = nbDecimales.intValue();
		}

		if (remiseGlobaleEnPourcent == null) {
			return montant.setScale(nbDecimalesImposees, BigDecimal.ROUND_HALF_UP);
		}

		return montant.subtract(montant.multiply(remiseGlobaleEnPourcent.divide(POURCENTAGE_100, BigDecimal.ROUND_HALF_UP)))
					  .setScale(nbDecimalesImposees, BigDecimal.ROUND_HALF_UP);
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal calculerTauxTva(BigDecimal mtHT, BigDecimal mtTTC, int scale, BigDecimal tauxTvaParDefaut) {
		BigDecimal tauxTva = tauxTvaParDefaut;

		if (estDivisible(mtHT)) {
			tauxTva = mtTTC.divide(mtHT, scale, BigDecimal.ROUND_HALF_UP).setScale(scale, BigDecimal.ROUND_HALF_UP);
		}

		return tauxTva;
	}

	/**
	 * {@inheritDoc}
	 */
	public int determineNombreDecimalesImposees(ApplicationConfig config, EOEditingContext edc, Number exercice) {
		Integer nbDecimales = config.nbDecimalesImposees(edc, exercice);
		if (nbDecimales == null) {
			nbDecimales = AbstractFacturationService.DEUX_DECIMALES;
		}
		return nbDecimales.intValue();
	}
	
	public int determineNombreDecimalesPourPrecision(ApplicationConfig config, EOEditingContext edc, Number exercice) {
        if (determineNombreDecimalesImposees(config, edc, exercice) == 0) {
            return determineNombreDecimalesImposees(config, edc, exercice);
        }
	    return AbstractFacturationService.TROIS_DECIMALES;
    } 

	/**
	 * Transforme 19.6% en 1.196
	 * @param tauxTvaEnPourcent
	 * @param scale
	 * @return
	 */
	protected BigDecimal calculerTauxTva(BigDecimal tauxTvaEnPourcent, int scale) {
		return tauxTvaEnPourcent
					.divide(POURCENTAGE_100, scale, BigDecimal.ROUND_HALF_UP)
					.add(BigDecimal.ONE);
	}

	protected boolean tauxTVACatalogueDisponible(CatalogueArticle catalogueArticle) {
		return catalogueArticle != null && catalogueArticle.tauxTVA() != null;
	}

	protected boolean estDivisible(BigDecimal montant) {
		return montant != null && montant.doubleValue() > 0.0d;
	}

	protected boolean isCalculPossible(BigDecimal montantHT, BigDecimal montantTTC, BigDecimal quantite) {
		return montantHT != null && montantTTC != null && quantite != null;
	}

	/**
	 * Vérifier si le calcul du total TTC peut être réalisé en utilisant la formule :
	 * Total TTC = HT unitaire * quantite * TxTva.
	 * @param montantHT  montant unitaire HT.
	 * @param montantTTC montant unitaire TTC.
	 * @param quantite   quantite produit.
	 * @return true si on peut appliquer la formule basée sur le HT ; false sinon.
	 */
	protected boolean isCalculTotalTTCPossibleAvecHTCommeBaseCalcul(BigDecimal montantHT, BigDecimal montantTTC, BigDecimal quantite) {
		return isCalculPossible(montantHT, montantTTC, quantite);
	}

	/**
	 * Vérifier si le calcul du total TTC peut être réalisé en utilisant uniquement le montant de TVA comme base de calcul.
	 * @param montantHT  montant unitaire HT.
	 * @param montantTTC montant unitaire TTC.
	 * @param quantite   quantite produit.
	 * @return true si on peut appliquer la formule sur le montant de la TVA ; false sinon.
	 */
	protected boolean isCalculTotalTTCAvecTVACommeBaseCalcul(BigDecimal montantHT, BigDecimal montantTTC, BigDecimal quantite) {
		return isCalculPossible(montantHT, montantTTC, quantite)
				&& montantHT.doubleValue() == 0.0d
				&& montantTTC.doubleValue() > 0.0d;
	}
}
