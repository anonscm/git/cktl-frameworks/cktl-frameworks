package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;

public abstract class BaseConventionTest {

    protected Contrat creerContratAvecTranches(Tranche...tranches) {
        Contrat contrat = new Contrat();
        for (Tranche tranche : tranches) {
            contrat.addToTranchesRelationship(tranche);
        }
        return contrat;
    }
    
    protected Tranche creerTranche(int exercice) {
        Tranche tranche = new Tranche();
        EOExerciceCocktail exerciceCktl = new EOExerciceCocktail();
        exerciceCktl.setExeExercice(exercice);
        tranche.setExerciceCocktailRelationship(exerciceCktl);
        return tranche;
    }
    
    protected TrancheBudget creerTrancheBudget(int exercice) {
        TrancheBudget trancheBudget = new TrancheBudget();
        EOExercice exerciceCktl = new EOExercice();
        exerciceCktl.setExeExercice(Long.valueOf(exercice));
        trancheBudget.setExercice(exerciceCktl);
        trancheBudget.setTbSuppr("N");
        return trancheBudget;
    }
    
    protected TrancheBudgetRec creerTrancheBudgetRec(int exercice) {
        TrancheBudgetRec trancheBudgetRec = new TrancheBudgetRec();
        EOExercice exerciceCktl = new EOExercice();
        exerciceCktl.setExeExercice(Long.valueOf(exercice));
        trancheBudgetRec.setExercice(exerciceCktl);
        trancheBudgetRec.setTbrSuppr("N");
        return trancheBudgetRec;
    }
    
}
