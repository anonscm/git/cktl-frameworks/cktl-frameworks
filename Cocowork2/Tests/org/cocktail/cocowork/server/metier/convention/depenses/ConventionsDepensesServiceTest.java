package org.cocktail.cocowork.server.metier.convention.depenses;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOOrgan;
import org.cocktail.fwkcktldepense.server.metier.EOTypeCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

/**
 * Tests de {@link ConventionsDepensesService}
 * A noter dans ces tests que nous n'utilisons jamais le mockEc,
 * c'est nécessaire uniquement pour charger les eomodels.
 * 
 * @author Alexis Tual
 *
 */
public class ConventionsDepensesServiceTest {
    
    @Rule
    public MockEditingContext mockEc = new MockEditingContext("Cocowork", "carambole", "FwkCktlJefyAdmin");
    
    private ConventionsDepensesService conventionsDepensesService;
    

    /**
     * Instanciation des services
     */
    @Before
    public void setUp() {
        conventionsDepensesService = new ConventionsDepensesService();
    }

    /**
     * Une seule convention simple avec types de crédits et organ.
     * 
     */
    @Test
    public void testTotalResteEngagePourConventionSimpleSurExercice() {
        EOTypeCredit typeCredit = new EOTypeCredit();
        typeCredit.setTcdCode("TC1");
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCreditJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit();
        typeCreditJefy.setTcdCode("TC1");
        EOOrgan organ = new EOOrgan();
        organ.setOrgUniv("UNIV");
        organ.setOrgEtab("ETAB");
        organ.setOrgUb("UB_1A");
        organ.setOrgCr("CR_1A");
        organ.setOrgSouscr("SCR_1A1A");
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan();
        organJefy.setOrgUniv("UNIV");
        organJefy.setOrgEtab("ETAB");
        organJefy.setOrgUb("UB_1A");
        organJefy.setOrgCr("CR_1A");
        organJefy.setOrgSouscr("SCR_1A1A");
        EOEngagementBudget engagementBudget = new EOEngagementBudget();
        engagementBudget.setEngMontantBudgetaireReste(BigDecimal.valueOf(600d));
        engagementBudget.setTypeCreditRelationship(typeCredit);
        engagementBudget.setOrganRelationship(organ);
        EOExercice exercice = new EOExercice();
        exercice.setExeExercice(2013);
        EOEngagementControleConvention engCtrlConv1 = new EOEngagementControleConvention();
        engCtrlConv1.setEngagementBudgetRelationship(engagementBudget);
        engCtrlConv1.setExerciceRelationship(exercice);
        engCtrlConv1.setEconMontantBudgetaireReste(BigDecimal.valueOf(600d));
        // On crée deux conventions simples
        Contrat contrat = new Contrat();
        contrat.addToEngagementControleConventions(engCtrlConv1);
        // Obligé de créer un JefyAdmin exercice
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOExercice();
        exerciceJefy.setExeExercice(2013L);
        BigDecimal total = conventionsDepensesService.totalResteEngagePourConventionSimpleSurExercice(
                contrat, exerciceJefy, typeCreditJefy, organJefy);
        assertEquals(600, total.doubleValue(), 0);
    }
    
    /**
     * Une seule convention répartie lors de la liquidation uniquement : les engagements ont pas été fait :
     * ça donne 0.
     */
    @Test
    public void testTotalResteEngagePourConventionSimpleSurExerciceCasLiquidationUniquement() {
        EOEngagementBudget engagementBudget = new EOEngagementBudget();
        engagementBudget.setEngMontantBudgetaireReste(BigDecimal.valueOf(1000d));
        EODepenseBudget depenseBudget = new EODepenseBudget();
        depenseBudget.setEngagementBudgetRelationship(engagementBudget);
        EOExercice exercice = new EOExercice();
        exercice.setExeExercice(2013);
        EODepenseControleConvention depCtrlConv1 = new EODepenseControleConvention();
        depCtrlConv1.setDepenseBudgetRelationship(depenseBudget);
        depCtrlConv1.setExerciceRelationship(exercice);
        depCtrlConv1.setDconMontantBudgetaire(BigDecimal.valueOf(600d));
        Contrat contrat = new Contrat();
        contrat.addToDepenseControleConventions(depCtrlConv1);
        // Obligé de créer un JefyAdmin exercice
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOExercice();
        exerciceJefy.setExeExercice(2013L);
        BigDecimal total = conventionsDepensesService.totalResteEngagePourConventionSimpleSurExercice(contrat, exerciceJefy, null, null);
        assertEquals(0, total.doubleValue(), 0);
    }
    
    /**
     * Plusieurs conventions réparties au moment de l'engagement : on doit récupérer uniquement ceux correspondants à la convention
     * passé en paramètre.
     */
    @Test
    public void testTotalResteEngagePourConventionSimpleSurExerciceCasDePlusieursConventions() {
        EOEngagementBudget engagementBudget = new EOEngagementBudget();
        engagementBudget.setEngMontantBudgetaireReste(BigDecimal.valueOf(1000d));
        EOExercice exercice = new EOExercice();
        exercice.setExeExercice(2013);
        EOEngagementControleConvention engCtrlConv1 = new EOEngagementControleConvention();
        engCtrlConv1.setEngagementBudgetRelationship(engagementBudget);
        engCtrlConv1.setExerciceRelationship(exercice);
        engCtrlConv1.setEconMontantBudgetaireReste(BigDecimal.valueOf(600d));
        EOEngagementControleConvention engCtrlConv2 = new EOEngagementControleConvention();
        engCtrlConv2.setEngagementBudgetRelationship(engagementBudget);
        engCtrlConv2.setExerciceRelationship(exercice);
        engCtrlConv2.setEconMontantBudgetaireReste(BigDecimal.valueOf(400d));
        // On crée deux conventions simples
        Contrat contrat = new Contrat();
        contrat.addToEngagementControleConventions(engCtrlConv1);
        Contrat contrat2 = new Contrat();
        contrat2.addToEngagementControleConventions(engCtrlConv2);
        // Obligé de créer un JefyAdmin exercice
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOExercice();
        exerciceJefy.setExeExercice(2013L);
        BigDecimal total = conventionsDepensesService.totalResteEngagePourConventionSimpleSurExercice(contrat, exerciceJefy, null, null);
        assertEquals(600, total.doubleValue(), 0);
    }
    
    /**
     * Cas nominal pour une convention RA.
     */
    @Test
    public void testTotalResteEngagePourConventionRaSurExercice() {
        EOExercice exercice = new EOExercice();
        exercice.setExeExercice(2013);
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceJefy = 
                new org.cocktail.fwkcktljefyadmin.common.metier.EOExercice();
        exerciceJefy.setExeExercice(2013L);
        EOExerciceCocktail exerciceCocktail = new EOExerciceCocktail();
        exerciceCocktail.setExeExercice(2013);
        Tranche tranche = new Tranche();
        tranche.setExerciceCocktailRelationship(exerciceCocktail);
        Contrat contrat = new Contrat();
        contrat.addToTranchesRelationship(tranche);
        EOEngagementBudget engagementBudget = new EOEngagementBudget();
        engagementBudget.setEngMontantBudgetaireReste(BigDecimal.valueOf(1000d));
        HistoCreditPositionne histo = new HistoCreditPositionne();
        histo.setHcpMontant(BigDecimal.valueOf(1000d));
        histo.addToEngagementBudgetsRelationship(engagementBudget);
        tranche.addToHistoCreditPositionnesRelationship(histo);
        BigDecimal total = conventionsDepensesService.totalResteEngagePourConventionRaSurExercice(contrat, exerciceJefy, null, null);
        assertEquals(1000, total.doubleValue(), 0);
    }
    

}
