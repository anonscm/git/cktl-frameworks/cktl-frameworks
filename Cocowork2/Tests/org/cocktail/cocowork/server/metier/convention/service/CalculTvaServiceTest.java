package org.cocktail.cocowork.server.metier.convention.service;

import java.math.BigDecimal;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class CalculTvaServiceTest {

    private CalculTvaService calculTvaService;
    public static final double MONTANT_HT = 1000;
    public static final double TAUX = 2.10;
    public static final double MONTANT_TTC = 1021;
    
    @Before
    public void setUp() {
        calculTvaService = new CalculTvaServiceImpl();
    }
    
    @Test
    public void testMontantTtc() {
        BigDecimal montantTtc = calculTvaService.montantTtc(MONTANT_HT, TAUX);
        Assert.assertEquals(MONTANT_TTC, montantTtc.doubleValue(), 0);
    }
    
    @Test
    public void testMontantHt() {
        BigDecimal montantHt = calculTvaService.montantHt(MONTANT_TTC, TAUX);
        Assert.assertEquals(MONTANT_HT, montantHt.doubleValue(), 0);
    }
    
}
