package org.cocktail.cocowork.server.metier.convention.service;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class CalculMontantParticipationDisponibleFraisInclusTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("Cocowork");
    private CalculMontantParticipationDisponibleFraisInclus calculMontantParticipationDisponibleFraisInclus;
    private RepartPartenaireTranche repartPartenaireTranche;
    private double montantParticipation = 1000d;
    private double montantFg = 100d;
    
    @Before
    public void setUp() throws Exception {
        calculMontantParticipationDisponibleFraisInclus = new CalculMontantParticipationDisponibleFraisInclus();
        repartPartenaireTranche = new RepartPartenaireTranche();
        ec.insertSavedObject(repartPartenaireTranche);
        repartPartenaireTranche.setRptMontantParticipation(BigDecimal.valueOf(montantParticipation));
        repartPartenaireTranche.fraisGestion().setFgMontant(BigDecimal.valueOf(montantFg));
    }

    @Test
    public void testMontantParticipationDisponibleFraisExclus() {
        BigDecimal montantParticipationDispo = 
                calculMontantParticipationDisponibleFraisInclus.montantParticipationDisponible(repartPartenaireTranche);
        Assert.assertEquals(montantParticipation, montantParticipationDispo.doubleValue(), 0d);
    }

}
