package org.cocktail.cocowork.server.metier.convention;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponible;
import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponibleFraisExclus;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

public class TrancheTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("Cocowork","FwkCktlPersonne","FwkCktlJefyAdmin","FwkCktlDroitsUtils");

	@UnderTest
	Tranche tranche;
	
	
	List<RepartPartenaireTranche> repartPartenaireTranches = new ArrayList<RepartPartenaireTranche>();
	List<FraisGestion> fraisGestions = new ArrayList<FraisGestion>();
	
	@Before
	public void setUp() throws Exception {
		repartPartenaireTranches.add(createDummyRepartPartenaireTranche(1000));
		repartPartenaireTranches.add(createDummyRepartPartenaireTranche(2000));
	}

	private RepartPartenaireTranche createDummyRepartPartenaireTranche(int montant) {
		RepartPartenaireTranche repartPartenaireTranche = ec.createSavedObject(RepartPartenaireTranche.class);
		repartPartenaireTranche.setRptMontantParticipation(new BigDecimal(montant));
		CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
		        new CalculMontantParticipationDisponibleFraisExclus();
		repartPartenaireTranche.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
		return repartPartenaireTranche;
	}

	private FraisGestion createDummyFraisGestion(int montant) {
		FraisGestion fraisGestion = ec.createSavedObject(FraisGestion.class);
		fraisGestion.setFgMontant(new BigDecimal(montant));
		return fraisGestion;
	}
	
	@Test
	public void testTraMontant() {
		// La tranche est juste créée
		assertEquals(0, tranche.traMontant().signum());
		
		// On ajoute des contributions
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
		}
		assertEquals(1, tranche.traMontant().signum());
		assertEquals(0, tranche.traMontant().compareTo(new BigDecimal(3000)));
		tranche.setReportNmoins1(new BigDecimal(200));
		assertEquals(0, tranche.traMontant().compareTo(new BigDecimal(3200)));
		tranche.setReportNplus1(new BigDecimal(500));
		assertEquals(0, tranche.traMontant().compareTo(new BigDecimal(2700)));
		
		
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			repartPartenaireTranche.addToFraisGestionsRelationship(createDummyFraisGestion(100));
		}
		
		assertEquals(0, tranche.traMontant().compareTo(new BigDecimal(2500)));
	}

	@Test
	public void testTraMontantFraisInclus() {
		// On ajoute des contributions
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
			repartPartenaireTranche.addToFraisGestionsRelationship(createDummyFraisGestion(100));
		}
		assertEquals(0, tranche.traMontantFraisInclus().compareTo(new BigDecimal(3000)));
	}


	@Test
	public void testTotalContributionsDisponibles() {
		// On ajoute des contributions
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
		}
		
		assertEquals(0, tranche.totalContributionsDisponibles().compareTo(new BigDecimal(3000)));

		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			repartPartenaireTranche.addToFraisGestionsRelationship(createDummyFraisGestion(100));
		}

		assertEquals(0, tranche.totalContributionsDisponibles().compareTo(new BigDecimal(2800)));

		tranche.setReportNmoins1(new BigDecimal(200));
		tranche.setReportNplus1(new BigDecimal(500));

		assertEquals(0, tranche.totalContributionsDisponibles().compareTo(new BigDecimal(2800)));

	}

	@Test
	public void testTotalContributionsPlusReportNmoins1() {
		// On ajoute des contributions
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
		}
		
		assertEquals(0, tranche.totalContributionsPlusReportNmoins1().compareTo(new BigDecimal(3000)));
		
		tranche.setReportNmoins1(new BigDecimal(200));
		tranche.setReportNplus1(new BigDecimal(500));

		
		assertEquals(0, tranche.totalContributionsPlusReportNmoins1().compareTo(new BigDecimal(3200)));
		
		for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
			repartPartenaireTranche.addToFraisGestionsRelationship(createDummyFraisGestion(100));
		}
		
		assertEquals(0, tranche.totalContributionsPlusReportNmoins1().compareTo(new BigDecimal(3000)));
	}

	@Test
	public void testTotalContributions() {
        for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
            tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
        }
        assertEquals(3000, tranche.totalContributions().doubleValue(), 0);
	}
	
	@Test
	public void testTotalContributionsAvecNull() {
	    for (RepartPartenaireTranche repartPartenaireTranche : repartPartenaireTranches) {
	        repartPartenaireTranche.setRptMontantParticipation(null);
	        tranche.addToToRepartPartenaireTranchesRelationship(repartPartenaireTranche);
	    }
	    assertEquals(0, tranche.totalContributions().doubleValue(), 0);
	}
	
}
