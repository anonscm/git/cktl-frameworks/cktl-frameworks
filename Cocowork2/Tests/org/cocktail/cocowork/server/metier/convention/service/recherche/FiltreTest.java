package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.junit.Assert;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

import er.extensions.eof.ERXQ;

public class FiltreTest {

    @Test
    public void qualifiersWithoutNullValue() {
        Filtre filtre = new Filtre();
        EOQualifier qual1 = ERXQ.equals("test", new Object());
        EOQualifier qual2 = ERXQ.equals("test", null);
        EOQualifier qual3 = ERXQ.equals("test", NSKeyValueCoding.NullValue);
        // On ne gère pas ce cas là car cela produit : (test like *null*) 
        // comment savoir si c'est vraiment null ou la chaine de caractère ?
        EOQualifier qual4 = ERXQ.contains("test", null);
        NSArray<EOQualifier> quals = filtre.qualifiersWithoutNullValue(qual1, qual2, qual3, qual4);
        Assert.assertArrayEquals(new EOQualifier[] {qual1, qual4}, quals.objects());
    }
    
}
