package org.cocktail.cocowork.server.metier.convention;


import static org.junit.Assert.*;

import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.junit.*;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

import er.extensions.foundation.ERXTimestampUtilities;

public class AvenantTest {

//private EOStructure struct = new EOStructure();
	
  @Rule
 public MockEditingContext ec = new MockEditingContext("Cocowork","FwkCktlPersonne","FwkCktlJefyAdmin","FwkCktlDroitsUtils");
 //public MockEditingContext ec = new MockEditingContext("Cocowork");

  @UnderTest
  private Avenant avenant;
  

  @Dummy
  private EODomaineScientifique domaineScientifique;

  @Dummy
  private ModeGestion modeGestionRA;
  

  @Before
  public void setupModeGestionRA() {
    modeGestionRA.setMgLibelleCourt(ModeGestion.MODE_GESTION_RA);
  }
    
  @Test 
  public void testRecuperationDomaineScientifique() {
    avenant.setDomaineScientifique(domaineScientifique);
    assertEquals(domaineScientifique, avenant.domaineScientifique());
  }
  
  @Test
  public void testLibelleAvenantZero() {
    avenant.setAvtIndex(0);
    assertEquals(avenant.libelleCourt(), Avenant.INITIAL);
  }
  
  @Test
  public void testPassageModeGestionRA() {
    avenant.setModeGestionRelationship(modeGestionRA);
    assertTrue("L'avenant doit etre a credit limitatif", avenant.avtLimitatifBoolean());
    assertFalse("L'avenant ne doit pas etre lucratif", avenant.avtLucrativiteBoolean());
  }
  
  @Test
  public void testContratEstSigne() {
    avenant.setAvtDateValidAdm(ERXTimestampUtilities.today());
    assertTrue(avenant.isSigne());
  }

  @Test
  public void testRecuperationAvenantValides() {
      Contrat contrat = new Contrat();
      Avenant avenantValide = new Avenant();
      avenantValide.setAvtDateValidAdm(new NSTimestamp());
      avenantValide.setAvtSuppr("N");
      contrat.addToAvenantsRelationship(avenantValide);
      assertEquals(1, contrat.avenantsValides().count());
  }
  
  @Test
  public void testRecuperationAvenantValidesEtNonSupprimes() {
      Contrat contrat = new Contrat();
      Avenant avenantValide = new Avenant();
      avenantValide.setAvtDateValidAdm(new NSTimestamp());
      avenantValide.setAvtSuppr("O");
      contrat.addToAvenantsRelationship(avenantValide);
      assertEquals("Les avenants supprimés ne doivent pas être présents", 0, contrat.avenantsValides().count());
  }
  
}
