package org.cocktail.cocowork.server.metier.convention;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import junit.framework.TestCase;

import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponible;
import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponibleFraisExclus;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

/**
 * Tests concernant les contrats.
 * 
 * @author Alexis Tual
 *
 */
public class ContratTest extends BaseConventionTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("Cocowork");
    
    private double montantParticip1 = 1000;
    private double montantFg1 = 100;
    private double montantParticip2 = 2000;
    private double montantFg2 = 200;
    private double montantInitial = 3000;
    private double montantAvenant = 2000;
    
    /**
     * Initialize un contrat avec fg pour test(s)
     * @return un contrat
     */
    public Contrat creerContratAvecFraisGestion() {
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        Contrat contrat = new Contrat();
        ec.insertSavedObject(contrat);
        Avenant avenant0 = new Avenant();
        avenant0.setAvtMontantHt(BigDecimal.valueOf(montantInitial));
        avenant0.setAvtIndex(0);
        avenant0.setAvtSuppr("N");
        avenant0.setAvtDateValidAdm(new NSTimestamp());
        Avenant avenant1 = new Avenant();
        avenant1.setAvtDateValidAdm(new NSTimestamp());
        avenant1.setAvtMontantHt(BigDecimal.valueOf(montantAvenant));
        avenant1.setAvtIndex(1);
        avenant1.setAvtSuppr("N");
        contrat.addToAvenants(avenant0);
        contrat.addToAvenants(avenant1);
        Tranche tranche = new Tranche();
        tranche.setTraSuppr("N");
        ec.insertSavedObject(tranche);
        contrat.addToTranches(tranche);
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        ec.insertSavedObject(repart1);
        repart1.setRptMontantParticipation(BigDecimal.valueOf(montantParticip1));
        repart1.fraisGestion().setFgMontant(BigDecimal.valueOf(montantFg1));
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        ec.insertSavedObject(repart2);
        repart2.setRptMontantParticipation(BigDecimal.valueOf(montantParticip2));
        repart2.fraisGestion().setFgMontant(BigDecimal.valueOf(montantFg2));
        tranche.addToToRepartPartenaireTranches(repart1);
        tranche.addToToRepartPartenaireTranches(repart2);
        return contrat;
    }
    
    /**
     * A noter que l'on teste que le cas général ici.
     * Il y a d'autres implémentations possibles :
     * cf {@link CalculMontantParticipationDisponible}
     */
    @Test
    public void testTotalContributionsAPositionner() {
        Contrat contrat = creerContratAvecFraisGestion();
        BigDecimal contribs = contrat.totalContributionsAPositionner();
        assertEquals(montantParticip1 + montantParticip2 - montantFg1 - montantFg2 , contribs.doubleValue(), 0d);
    }

    @Test
    public void testTotalContributions() {
        Contrat contrat = creerContratAvecFraisGestion();
        BigDecimal contribs = contrat.totalContributions();
        assertEquals(montantParticip1 + montantParticip2, contribs.doubleValue(), 0d);
    }

    @Test
    public void testTranchesParExerciceAsc() {
        Tranche tranche2012 = creerTranche(2012);
        Tranche tranche2011 = creerTranche(2011);
        Tranche tranche2010 = creerTranche(2010);
        Tranche[] tranches = new Tranche[] {tranche2012, tranche2010, tranche2011};
        Tranche[] tranchesOrdonnees = new Tranche[] {tranche2010, tranche2011, tranche2012};
        Contrat contrat = creerContratAvecTranches(tranches);
        
        Assert.assertArrayEquals("Les tranches sont dans l'ordre d'insertion", tranches, contrat.tranches().toArray());
        Assert.assertArrayEquals("Les tranches sont dans l'ordre des exercices", tranchesOrdonnees, contrat.tranchesOrdonneesParExerciceAsc().toArray());
    }

    @Test
    public void testMontantDisponibleTranches() {
        Contrat contrat = creerContratAvecFraisGestion();
        double montantTotal = montantInitial + montantAvenant;
        double expected = montantTotal - (montantParticip1 + montantParticip2 - (montantFg1 + montantFg2));
        Assert.assertEquals(expected,  contrat.montantDisponibleTranches().doubleValue(), 0);
    }
}
