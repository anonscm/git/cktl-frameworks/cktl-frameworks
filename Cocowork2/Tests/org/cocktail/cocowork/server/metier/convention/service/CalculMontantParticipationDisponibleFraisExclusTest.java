package org.cocktail.cocowork.server.metier.convention.service;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class CalculMontantParticipationDisponibleFraisExclusTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("Cocowork");
    private CalculMontantParticipationDisponibleFraisExclus calculMontantParticipationDisponibleFraisExclus;
    private RepartPartenaireTranche repartPartenaireTranche;
    private double montantParticipation = 1000d;
    private double montantFg = 100d;
    
    @Before
    public void setUp() throws Exception {
        calculMontantParticipationDisponibleFraisExclus = new CalculMontantParticipationDisponibleFraisExclus();
        repartPartenaireTranche = new RepartPartenaireTranche();
        ec.insertSavedObject(repartPartenaireTranche);
        repartPartenaireTranche.setRptMontantParticipation(BigDecimal.valueOf(montantParticipation));
        repartPartenaireTranche.fraisGestion().setFgMontant(BigDecimal.valueOf(montantFg));
    }

    @Test
    public void testMontantParticipationDisponibleFraisExclus() {
        BigDecimal montantParticipationDispo = 
                calculMontantParticipationDisponibleFraisExclus.montantParticipationDisponible(repartPartenaireTranche);
        Assert.assertEquals(montantParticipation - montantFg, montantParticipationDispo.doubleValue(), 0d);
    }

}
