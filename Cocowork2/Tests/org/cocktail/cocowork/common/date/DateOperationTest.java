package org.cocktail.cocowork.common.date;

import static org.junit.Assert.*;

import org.junit.Test;

public class DateOperationTest {

	@Test
	public void dernierJourDuMois() {
		assertEquals("Le dernier jour du mois de janvier est le 31",
				"2012-01-31 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("01/2012").toString());
		
		assertEquals("Le dernier jour du mois de fevrier est le 29",
				"2012-02-29 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("02/2012").toString());
		
		assertEquals("Le dernier jour du mois de fevrier est le 28",
				"2013-02-28 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("02/2013").toString());
		
		assertEquals("Le dernier jour du mois de juin est le 30",
				"2013-06-30 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("06/2013").toString());

		assertEquals("Si le mois est supérieur à 12 la fonction décalle à l'année suivante",
				"2014-01-31 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("13/2013").toString());

		assertEquals("Si le mois est supérieur à 12 la fonction décalle à l'année suivante",
				"2014-02-28 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("14/2013").toString());

		assertEquals("Si le mois est inférieur à 1 la fonction décalle à l'année précédente",
				"2012-12-31 23:59:59 Etc/GMT", DateOperation.dernierJourDuMois("0/2013").toString());

	}
	
	@Test
	public void dernierJourDuMoisAvecParametreInvalide() {
		assertNull("Si le parametre est invalide : retourne null",
				 DateOperation.dernierJourDuMois(null));
		
		assertNull("Si le parametre est invalide : retourne null",
				 DateOperation.dernierJourDuMois("toto"));

	}
	


}
