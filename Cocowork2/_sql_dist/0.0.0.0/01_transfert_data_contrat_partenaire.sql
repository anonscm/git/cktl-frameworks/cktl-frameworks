DECLARE

CURSOR C1 is select  ap.AP_ORDRE as CP_ORDRE, a.CON_ORDRE, ap.PERS_ID, ap.C_STRUCTURE, ap.AP_MONTANT as CP_MONTANT,
ap.AP_PRINCIPAL as CP_PRINCIPAL, ap.TYPE_PART_ORDRE, ap.AP_DATE_SIGNATURE as CP_DATE_SIGNATURE,
ap.AP_REF_EXTERNE_PARTEN as CP_REF_EXTERNE_PARTEN from accords.AVENANT_PARTENAIRE ap,accords.avenant a where a.avt_ordre=ap.avt_ordre;

CURSOR C2 is select apc.APC_ORDRE as CPC_ORDRE, cp.CP_ORDRE, apc.PERS_ID, apc.C_STRUCTURE, apc.NO_INDIVIDU, apc.TC_ORDRE, apc.PERS_ID_CONTACT 
from accords.avenant_part_contact apc, accords.avenant a, accords.contrat_partenaire cp
where a.AVT_ORDRE=apc.AVT_ORDRE and cp.CON_ORDRE=a.CON_ORDRE and cp.PERS_ID=apc.PERS_ID;

cp ACCORDS.CONTRAT_PARTENAIRE%rowtype;

cpc accords.contrat_part_contact%rowtype;

seq Integer;

begin
  open C1;
  loop
   fetch C1 into cp;
   exit when C1%notfound;

   		INSERT INTO ACCORDS.CONTRAT_PARTENAIRE ( CP_ORDRE, CON_ORDRE, PERS_ID, C_STRUCTURE, CP_MONTANT,
CP_PRINCIPAL, TYPE_PART_ORDRE, CP_DATE_SIGNATURE,
CP_REF_EXTERNE_PARTEN ) VALUES (cp.CP_ORDRE, cp.CON_ORDRE, cp.PERS_ID, cp.C_STRUCTURE, cp.CP_MONTANT,
cp.CP_PRINCIPAL, cp.TYPE_PART_ORDRE, cp.CP_DATE_SIGNATURE,
cp.CP_REF_EXTERNE_PARTEN);
		
  end loop;
  close C1;
  

  
  open C2;
  loop
   fetch C2 into cpc;
   exit when C2%notfound;

   		INSERT INTO accords.contrat_part_contact( CPC_ORDRE, CP_ORDRE, PERS_ID, C_STRUCTURE, NO_INDIVIDU, TC_ORDRE, PERS_ID_CONTACT  ) 
		VALUES (cpc.CPC_ORDRE, cpc.CP_ORDRE, cpc.PERS_ID, cpc.C_STRUCTURE, cpc.NO_INDIVIDU, cpc.TC_ORDRE, cpc.PERS_ID_CONTACT );
		
  end loop;
  close C2;
  
  commit;
    
    select max(cp_ordre)+1 into seq from accords.contrat_partenaire;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE ACCORDS.CONTRAT_PARTENAIRE_SEQ START WITH ' || seq;
  	
    select max(cpc_ordre)+1 into seq from accords.contrat_part_contact;
	EXECUTE IMMEDIATE 'CREATE SEQUENCE ACCORDS.CONTRAT_PART_CONTACT_SEQ START WITH ' || seq;
	
	commit;
end;