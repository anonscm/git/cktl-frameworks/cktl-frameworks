--
-- Patch DDL de ACCORDS du 07/12/2010 à éxecuter depuis le user ACCORDS
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0
-- Date de publication : 02/12/2010
-- Auteur(s) : Emmanuel GEZE
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

-- Ajout des attributs de date de creation et de responsable
ALTER TABLE ACCORDS.PROJET
 ADD (
 	PERS_ID_RESPONSABLE NUMBER,
 	PJT_DATE_CREATION DATE
 );
COMMENT ON COLUMN ACCORDS.PROJET.PERS_ID_RESPONSABLE IS 'Responsable du projet';
COMMENT ON COLUMN ACCORDS.PROJET.PJT_DATE_CREATION IS 'Date de creation du projet';

-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.AVENANT
 ADD (
 	AVT_TVA_ID NUMBER
 );

-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.SB_DEPENSE
 ADD (
 	TVA_ID NUMBER
 );

-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.SB_RECETTE
 ADD (
 	TVA_ID NUMBER
 );

 -- Autoriser les NULL dans org_id
alter table ACCORDS.SB_DEPENSE MODIFY(ORG_ID  NULL);
alter table ACCORDS.SB_RECETTE MODIFY(ORG_ID  NULL);
 
-- Maj de  la version 
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (accords.VERSION_HISTO_SEQ.nextval,'2.1.0',to_date('07/12/2010','DD/MM/YYYY'),'Ajout TVA_ID, modifs Projet');

