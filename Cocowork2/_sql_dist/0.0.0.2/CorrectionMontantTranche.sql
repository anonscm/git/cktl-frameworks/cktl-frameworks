CREATE OR REPLACE PROCEDURE ACCORDS.CORRIGER_TRANCHES_CONTRAT(conExeOrdre IN NUMBER, conIndex IN NUMBER)
IS  
  conOrdre NUMBER;
  montantBudgetaireTranche NUMBER;
  montantPositionneDepTranche NUMBER;
  montantPositionneRecTranche NUMBER;
  montantPositionneTranche NUMBER;
  montantContributionsARepartir NUMBER;
  montantReport NUMBER;
  montantFrais NUMBER;
  montantFraisTranche NUMBER;
  persIdConEtablissement NUMBER;
  persIdPartenaire NUMBER;
  nbRpt NUMBER;
  montantRpt NUMBER;
  exerciceTranche NUMBER;
  exerciceSuivant NUMBER;
  reportNmoins1 NUMBER;
  modeGestion NUMBER;
  CURSOR C_TRANCHES(tConOrdre NUMBER) IS SELECT * FROM ACCORDS.TRANCHE WHERE CON_ORDRE = tConOrdre AND TRA_SUPPR <> 'O' ORDER BY EXE_ORDRE;
  type rpts is table of accords.repart_partenaire_tranche%rowtype;
  rptsTranche rpts;
  rpt accords.repart_partenaire_tranche%rowtype;
BEGIN

  SELECT CON_ORDRE INTO conOrdre FROM ACCORDS.CONTRAT WHERE EXE_ORDRE = conExeOrdre AND CON_INDEX = conIndex;
  SELECT AVT_MODE_GEST INTO modeGestion FROM ACCORDS.AVENANT WHERE CON_ORDRE = conOrdre AND AVT_INDEX =0;          

  SELECT S.PERS_ID INTO persIdConEtablissement FROM ACCORDS.CONTRAT C
    INNER JOIN GRHUM.STRUCTURE_ULR S ON S.C_STRUCTURE = C.CON_ETABLISSEMENT
      WHERE C.CON_ORDRE = conOrdre;
      
  -- Evidement on est sur la premiere tranche ! :)      
  reportNmoins1 := 0;
  FOR tranche IN C_TRANCHES(conOrdre) LOOP
    -- TOTAL POSITIONNE DEP
    SELECT NVL(SUM(TB_MONTANT),0) INTO montantPositionneDepTranche FROM TRANCHE_BUDGET WHERE TB_SUPPR <> 'O' AND TRA_ORDRE = tranche.tra_ordre;
    -- TOTAL POSITIONNE DEP
    SELECT NVL(SUM(TBR_MONTANT),0) INTO montantPositionneRecTranche FROM TRANCHE_BUDGET_REC WHERE TBR_SUPPR <> 'O' AND TRA_ORDRE = tranche.tra_ordre;
    -- ON PREND LE PLUS GRAND
    if(montantPositionneDepTranche > montantPositionneRecTranche) THEN
      montantPositionneTranche := montantPositionneDepTranche;
    else
      montantPositionneTranche := montantPositionneRecTranche;
    end if;
    
    -- la somme des frais de gestion sur la tranche
    --select nvl(sum(fg_montant), 0) into montantFraisTranche from accords.frais_gestion fg join
    --                                                             accords.repart_partenaire_tranche rpt on fg.rpt_id = rpt.rpt_id
    --                                                       where rpt.tra_ordre = tranche.tra_ordre;

    -- les contributions sont ce qui n'a pas été reporté par rapport au positionné
    montantContributionsARepartir := montantPositionneTranche - reportNmoins1;

    -- TOTAL LIQUIDATIONS
    IF(modeGestion = 1) THEN
      select  nvl(sum(montant),0) into montantBudgetaireTranche from (
        select distinct db.dep_id, db.dep_montant_budgetaire montant from accords.tranche t
                inner join ACCORDS.histo_credit_positionne hcp on t.tra_ordre = hcp.tra_ordre
                inner join ACCORDS.v_hist_cred_pod_eng_budget vhcpeb on vhcpeb.hcp_ordre = hcp.hcp_ordre
                inner join JEFY_DEPENSE.engage_budget eb on vhcpeb.eng_id = eb.eng_id
                inner join JEFY_DEPENSE.depense_budget db on eb.eng_id = db.eng_id
                      where t.tra_ordre = tranche.tra_ordre
                        and hcp.hcp_suppr <> 'O'
      );
    ELSE
      SELECT NVL(SUM(DCON_MONTANT_BUDGETAIRE),0) INTO montantBudgetaireTranche FROM JEFY_DEPENSE.DEPENSE_CTRL_CONVENTION WHERE CONV_ORDRE = tranche.con_ordre and EXE_ORDRE = tranche.exe_ordre;
    END IF;
    
    UPDATE ACCORDS.repart_partenaire_tranche SET RPT_MONTANT_PARTICIPATION = 0 WHERE TRA_ORDRE = tranche.tra_ordre;
    -- est-ce qu'il reste des choses a repartir
    if(montantContributionsARepartir > 0) THEN 
      -- Faire un tableau de RPT, avec dedans : tous les RPT sauf ceux correspondants au partenaire établissement si nbRPT > 1
      -- On fetch les RPT sauf ceux correspondant à l'établissement
      SELECT REPT.* bulk collect into rptsTranche FROM ACCORDS.repart_partenaire_tranche REPT
              INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CP_ORDRE = REPT.CP_ORDRE
                WHERE REPT.TRA_ORDRE  = tranche.tra_ordre and CP.PERS_ID <> persIdConEtablissement;
      -- Si nbRpt = 0, on refetch avec tous
      if (rptsTranche.count() = 0) then
        SELECT REPT.* bulk collect into rptsTranche FROM ACCORDS.repart_partenaire_tranche REPT
          INNER JOIN ACCORDS.CONTRAT_PARTENAIRE CP ON CP.CP_ORDRE = REPT.CP_ORDRE
            WHERE REPT.TRA_ORDRE  = tranche.tra_ordre;
      end if;
      nbRpt := rptsTranche.count();
      -- On itère sur les RPTs
      FOR i IN rptsTranche.first()..rptsTranche.last() LOOP
        rpt := rptsTranche(i);
        -- On fait la répartition pour les partenaires non établissement ou avec l'établissement s'il est seul partenaire
          montantRpt := (montantContributionsARepartir - mod(montantContributionsARepartir, nbRpt)) / nbRpt;
          if(i = nbRpt) then
            montantRpt := montantRpt + mod(montantContributionsARepartir, nbRpt);
          end if;
          -- Mais il faut rajouter les frais de gestion !
          select nvl(sum(fg_montant), 0) into montantFrais from accords.frais_gestion fg where fg.rpt_id = rpt.rpt_id;
          UPDATE ACCORDS.repart_partenaire_tranche SET RPT_MONTANT_PARTICIPATION = (montantRpt + montantFrais) WHERE RPT_ID = rpt.rpt_id;
      END LOOP;
    end if;

    -- le report vers la tranche suivante
    -- le premier est classique
    if(montantPositionneTranche > reportNmoins1) then 
      montantReport := montantPositionneTranche - montantBudgetaireTranche;
    else
      montantReport := reportNmoins1 - montantBudgetaireTranche;
    end if;
    
    dbms_output.put_line(
      'exe_ordre : ' || tranche.exe_ordre || ' - ' || 
      'montantContributionsARepartir : ' || montantContributionsARepartir || ' - ' || 
      'montantPositionneDepTranche : ' || montantPositionneDepTranche || ' - ' || 
      'montantPositionneRecTranche : ' || montantPositionneRecTranche || ' - ' || 
      'montantPositionneTranche : ' || montantPositionneTranche || ' - ' || 
      'REPORT_N_MOINS_1 : ' || reportNmoins1 || ' - ' || 
      'REPORT_N_PLUS_1 : ' || montantReport || ' - ' || 
      'montantBudgetaireTranche : ' || montantBudgetaireTranche 
      );

    exerciceTranche := tranche.exe_ordre;
    exerciceSuivant := tranche.exe_ordre + 1;

    if (exerciceTranche < 2013) then
      UPDATE ACCORDS.tranche SET REPORT_N_PLUS_1 = montantReport WHERE CON_ORDRE  = conOrdre AND EXE_ORDRE = exerciceTranche;
      UPDATE ACCORDS.tranche set REPORT_N_MOINS_1 = montantReport WHERE CON_ORDRE  = conOrdre AND EXE_ORDRE = exerciceSuivant;
    end if;
    reportNmoins1 := montantReport;
  END LOOP;
END;
/