--
-- Patch DDL de ACCORDS du 27/08/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.19
-- Date de publication : 27/08/2012
-- Auteur(s) : Julien LAFOURCADE
-- Licence : CeCILL version 2
--
--

CREATE OR REPLACE package ACCORDS.TRANSFERT_EVENEMENTS is

  function pers_id_for_utl(a_utl_ordre number) return number;

  function pers_id_for_indiv(a_indiv number) return number;

  function etat_for_statut(statut varchar2) return number;
  
  function now_if_null(date_modif_init date) return date;

  procedure transferer_evt;
  
  procedure test_transferer_evt;

end TRANSFERT_EVENEMENTS;
/


CREATE OR REPLACE package body ACCORDS.TRANSFERT_EVENEMENTS as
  
  function pers_id_for_utl(a_utl_ordre number) return number is
  pers_id number;
  begin
      dbms_output.put_line('In pers_id_for_utl ' || a_utl_ordre);
      select pers_id into pers_id from JEFY_ADMIN.V_UTILISATEUR where UTL_ORDRE = a_utl_ordre;
      return pers_id;
  end pers_id_for_utl;

  function pers_id_for_indiv(a_indiv number) return number is
  pers_id number;
  begin
      dbms_output.put_line('In pers_id_for_indiv ' || a_indiv);
      select pers_id into pers_id from GRHUM.INDIVIDU_ULR where no_individu = a_indiv;
      return pers_id;
  end pers_id_for_indiv;

  function etat_for_statut(statut varchar2) return number is
   EVT_ETAT_PREVU GRHUM.EVT_EVENEMENT_ETAT%rowtype;
   EVT_ETAT_REA GRHUM.EVT_EVENEMENT_ETAT%rowtype;
  begin
      -- recuperation de l'état
      select * into EVT_ETAT_PREVU from GRHUM.EVT_EVENEMENT_ETAT where EVTE_ID_INTERNE = 'PREVU';
      select * into EVT_ETAT_REA from GRHUM.EVT_EVENEMENT_ETAT where EVTE_ID_INTERNE = 'REALISE';
      if (statut = 'P') then
        return EVT_ETAT_PREVU.EVTE_ID;
      elsif (statut = 'R') then 
        return EVT_ETAT_REA.EVTE_ID;
      end if;
  end etat_for_statut;

  function now_if_null(date_modif_init date) return date is
  begin
    if (date_modif_init is null) then
      return sysdate;
    else
      return date_modif_init;
    end if;
  end now_if_null;

  procedure transferer_evt
  is
   CURSOR C1 is select * from ACCORDS.AVENANT_EVENEMENT;
   CURSOR C2(a_evt_ordre number) is select * from ACCORDS.REPART_INDIVIDU_EVT where EVT_ORDRE = a_evt_ordre;
   AV_EVT ACCORDS.AVENANT_EVENEMENT%rowtype;
   EVT ACCORDS.EVENEMENT%rowtype;
   EVT_NATURE ACCORDS.NATURE_EVENEMENT%rowtype;
   EVT_TYPE GRHUM.EVT_EVENEMENT_TYPE%rowtype;
   REP_IND_EVT ACCORDS.REPART_INDIVIDU_EVT%rowtype;
   T_TACH_ID NUMBER; 
   begin
    dbms_output.put_line('In transferer_evt debut');
    -- recuperation constantes
    select TYTA_ID into T_TACH_ID FROM GRHUM.EVT_TYPE_TACHE where TYTA_ID_INTERNE = 'ALERTE_MAIL';
    open C1;
    loop
     fetch C1 into AV_EVT;
     exit when C1%notfound;
     dbms_output.put_line('In transferer_evt ' || AV_EVT.EVT_ORDRE);
     -- recuperation de l'evenement original
     select * into EVT from ACCORDS.EVENEMENT where EVT_ORDRE = AV_EVT.EVT_ORDRE;
     -- recuperation du type d'evenement initial
     select * into EVT_NATURE from ACCORDS.NATURE_EVENEMENT where NE_ORDRE = EVT.EVT_TYPE;
     -- recuperation du type d'evenement nouveau
     dbms_output.put_line('In transferer_evt, EVT_NATURE : ' || EVT_NATURE.NE_COMMENTAIRE || ' - ' || EVT_NATURE.NE_LIBELLE);
     select * into EVT_TYPE from GRHUM.EVT_EVENEMENT_TYPE where (EVTT_LL = EVT_NATURE.NE_COMMENTAIRE OR EVTT_LC = EVT_NATURE.NE_LIBELLE);
     -- Si 
     -- insertion dans GRHUM.EVT_EVENEMENT
     insert into GRHUM.EVT_EVENEMENT(
                    EVT_APP_NAME, 
                    EVT_DATE_CREATION, 
                    EVT_DATE_MODIF, 
                    EVT_DATE_PREVUE, 
                    EVT_ID, 
                    EVT_OBJET, 
                    evt_observations, 
                    EVT_STATUT, 
                    EVTE_ID, 
                    EVTT_ID, 
                    FREQ_ID, 
                    PERSID_CREATION, 
                    PERSID_MODIF) VALUES  (
                    'COCOLIGHT',
                    EVT.EVT_DATE_CREATION,
                    now_if_null(EVT.EVT_DATE_MODIFICATION),
                    EVT.EVT_DATE,
                    GRHUM.EVT_EVENEMENT_SEQ.nextval,
                    EVT.EVT_MOTIF,
                    EVT.EVT_OBSERVATION,
                    EVT.EVT_STATUT,
                    ACCORDS.TRANSFERT_EVENEMENTS.etat_for_statut(EVT.EVT_STATUT),
                    EVT_TYPE.EVTT_ID,
                    null,
                    ACCORDS.TRANSFERT_EVENEMENTS.pers_id_for_utl(EVT.UTL_ORDRE),
                    ACCORDS.TRANSFERT_EVENEMENTS.pers_id_for_utl(EVT.UTL_ORDRE));
	 -- liaison de l'evt a l'avenant
	 insert into ACCORDS.AVENANT_EVT_EVENEMENT(AVT_ORDRE,EVT_ID) VALUES(AV_EVT.AVT_ORDRE, GRHUM.EVT_EVENEMENT_SEQ.currval);
     -- recuperation des individus associes
     OPEN C2(EVT.EVT_ORDRE);
     loop
        fetch C2 into REP_IND_EVT;
        exit when C2%notfound;
        -- insertion dans la nouvelle table
        insert into GRHUM.EVT_EVENEMENT_PERSONNE(EPERS_ID, EVT_ID, RPER_ID, PERS_ID, EPER_ALERTER)
                    VALUES (GRHUM.EVT_EVENEMENT_PERSONNE_SEQ.nextval, GRHUM.EVT_EVENEMENT_SEQ.currval, null, pers_id_for_indiv(REP_IND_EVT.NO_INDIVIDU), 'OUI');
     end loop;
     close C2;
     -- insertion si besoin d'une tache d'envoie de mail...
     if (EVT.EVT_DELAI_ALERTE is not null) then
        insert into GRHUM.EVT_TACHE(
                    EVT_ID,
                    PERSID_CREATION,
                    PERSID_MODIF,
                    TACH_DATA,
                    TACH_DATE_CREATION,
                    TACH_DATE_MODIF,
                    TACH_DESCRIPTION,
                    TACH_ETAT,
                    TACH_CLASS_NAME,
                    TACH_GROUP,
                    TACH_ID,
                    TACH_NAME,
                    TACH_SEND_MAIL_AFTER_EXE,
                    TACH_TRIGGER_OFFSET_MINUTES,
                    TYTA_ID) VALUES (
                    GRHUM.EVT_EVENEMENT_SEQ.currval,
                    ACCORDS.TRANSFERT_EVENEMENTS.pers_id_for_utl(EVT.UTL_ORDRE),
                    ACCORDS.TRANSFERT_EVENEMENTS.pers_id_for_utl(EVT.UTL_ORDRE),
                    null,
                    EVT.EVT_DATE_CREATION,
                    EVT.EVT_DATE_MODIFICATION,
                    'Alerte par mail des personnes concernés par l''Évènement',
                    'Programmée',
                    'org.cocktail.fwkcktlevenement.serveur.quartz.job.impl.JobAlerteMail',
                    'DEFAULT',
                    GRHUM.EVT_TACHE_SEQ.nextval,
                    'TACHE_ALERTE_MAIL',
                    'NON',
                    EVT.EVT_DELAI_ALERTE,
                    T_TACH_ID);
     end if;
    end loop;
    close C1;
  end transferer_evt;
  
  
  procedure test_transferer_evt
  is
    -- oldz
    AVT_ID NUMBER;
    EVT_ID NUMBER;
    NATURE_ID NUMBER;
    NO_INDIV NUMBER;
    -- newz
    EVT_NEW_ID NUMBER;
    -- test
    countvar NUMBER;
    testexception EXCEPTION;
  begin
    dbms_output.put_line('In test transferer_evt');
    -- On rÃ©cupÃ¨re un avenant existant...
    select AVT_ORDRE into AVT_ID from ACCORDS.AVENANT where rownum = 1 order by AVT_ORDRE desc;
    -- On crÃ©e un ACCORDS.EVENEMENT (old)
    select ACCORDS.EVENEMENT_SEQ.nextval into EVT_ID from dual;
    insert into ACCORDS.EVENEMENT (EVT_ORDRE,EVT_DATE,EVT_DATE_INIT,EVT_ALERTE_EFFECTUEE,EVT_DELAI_ALERTE,EVT_MOTIF,EVT_OBSERVATION,EVT_STATUT,UTL_ORDRE,EVT_DATE_CREATION,EVT_DATE_MODIFICATION,EVT_TYPE) values (EVT_ID,to_date('14/10/12','DD/MM/RR'),to_date('19/03/10','DD/MM/RR'),'N',1440,'POUET_MOTIF','POUET_OBSERVATION','P',57,to_date('19/03/10','DD/MM/RR'),null,4); 
    -- On recupère un ACCORDS.NATURE_EVENEMENT (old)
    select NE_ORDRE into NATURE_ID from ACCORDS.NATURE_EVENEMENT where NE_LIBELLE_COURT = 'COUR';
    -- On recupère un individu existant...
    select NO_INDIVIDU into NO_INDIV from GRHUM.INDIVIDU_ULR where rownum = 1 order by NO_INDIVIDU desc;
    -- On crée un ACCORDS.REPART_INDIVIDU_EVT
    insert into ACCORDS.REPART_INDIVIDU_EVT (EVT_ORDRE,NO_INDIVIDU,RIE_ORDRE) values (EVT_ID, NO_INDIV, ACCORDS.REPART_INDIVIDU_EVT_SEQ.nextval);
    -- Enfin on fait le lien entre avenant et evenement
    insert into ACCORDS.AVENANT_EVENEMENT (AE_ORDRE,AVT_ORDRE,EVT_ORDRE) values (ACCORDS.AVENANT_EVENEMENT_SEQ.nextval, AVT_ID, EVT_ID);
    
    -- On exécute le transfert evt 
    transferer_evt();
    
    -- On teste si un evenement est prÃ©sent
    select EVT_ID into EVT_NEW_ID from GRHUM.EVT_EVENEMENT where evt_observations = 'POUET_OBSERVATION' and evt_objet = 'POUET_MOTIF';
    if (EVT_NEW_ID IS NULL) then
        raise_application_error(9999, 'Aucun GRHUM.EVT_EVENEMENT trouvé');
    end if;
    -- On teste si un GRHUM.evt_evenement_personne est présent
    select count(*) into countvar from GRHUM.EVT_EVENEMENT_PERSONNE where EVT_ID = EVT_NEW_ID;
    if (countvar = 0) then
        raise_application_error(9999, 'Aucun GRHUM.EVT_EVENEMENT_PERSONNE trouvé');
    end if;
    -- On teste si un GRHUM.EVT_TACHE est présent
    select count(*) into countvar from GRHUM.EVT_TACHE where EVT_ID = EVT_NEW_ID;
    if (countvar = 0) then
        raise_application_error(9999, 'Aucun GRHUM.EVT_TACHE trouvé ');
    end if;
    dbms_output.put_line('--------> SUCCESS : Test du transfert d''Évènements effectué avec succès <--------');
  end test_transferer_evt;
  
  end TRANSFERT_EVENEMENTS;
  /