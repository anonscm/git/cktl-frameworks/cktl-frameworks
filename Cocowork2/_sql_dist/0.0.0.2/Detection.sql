-- Détection des conventions ayant une somme des contributions inférieure aux crédits positionnés
declare
  totalContributionsMoinsFG number;
  totalContributions number;
  totalFg number;
  totalReports number;
  totalCreditsPos number;
  cursor c_conv is select * from accords.contrat order by exe_ordre asc;
begin
	for uneConv in c_conv loop
		---- 1) Calcule totalContributionsMoinsFraisGestion
		---------- Somme des repart partenaire tranche dont les tranches sont non suppr
		select sum(rpt.rpt_montant_participation) into totalContributions
		                                          from ACCORDS.repart_partenaire_tranche rpt
		                                          join ACCORDS.tranche t on rpt.tra_ordre = t.tra_ordre
		                                         where t.con_ordre = uneConv.con_ordre and t.tra_suppr = 'N';
		----------  moins
		---------- Somme des frais de gestion correspondant aux repart partenaire tranches
		select sum(fg.fg_montant) into totalFg from accords.frais_gestion fg
		                                       join accords.repart_partenaire_tranche rpt on fg.rpt_id = rpt.rpt_id
		                                       join accords.tranche t on rpt.tra_ordre = t.tra_ordre
		                                      where t.con_ordre = uneConv.con_ordre and t.tra_suppr = 'N';
		totalContributionsMoinsFG := totalContributions - totalFg;
		---- 2) Calcul du total des crédits positionnés
		---------- Somme des tranchebudget non supprimés correspondant aux tranches non supprimées
		select nvl(sum(tb_montant),0) into totalCreditsPos
			          from accords.tranche_budget tb
			          join accords.tranche t on tb.tra_ordre = t.tra_ordre
			          join accords.contrat c on t.con_ordre = c.con_ordre
			         where tb.tb_suppr <> 'O' and 
			         	   t.tra_suppr <> 'O' and c.con_ordre = uneConv.con_ordre;
		        
		----------  moins
		---------- Somme des reports nplus1 correspondant aux tranches non supprimées
		select nvl(sum(t.report_n_plus_1),0) into totalReports
			          from accords.tranche t 
			          join accords.contrat c on t.con_ordre = c.con_ordre
			         where t.tra_suppr <> 'O' and c.con_ordre = uneConv.con_ordre;
		---- différence entre 1) et 2), si 1 < 2 => erreur
		if ((totalCreditsPos - totalReports) > totalContributionsMoinsFG) then
		  dbms_output.put_line ('Convention ' || uneConv.con_ordre || ' ' || uneConv.exe_ordre || ' ' || uneConv.con_index || ' | '  || totalContributions || ' ' || totalCreditsPos || ' ' || totalReports);
		end if;
	end loop;
end;



-- Détection des conventions ayant une somme des contributions supérieure au montant de la conv
declare
  totalContributions number;
  montantConv number;
  cursor c_conv is select * from accords.contrat order by exe_ordre asc;
begin
	for uneConv in c_conv loop
		---- 1) Calcule montant de la tranche
		---------- Somme des repart partenaire tranche dont les tranches sont non suppr = montant de la tranche)
		select sum(rpt.rpt_montant_participation) into totalContributions
		                                          from ACCORDS.repart_partenaire_tranche rpt
		                                          join ACCORDS.tranche t on rpt.tra_ordre = t.tra_ordre
		                                         where t.con_ordre = uneConv.con_ordre and t.tra_suppr = 'N';

		---- 2) Calcul du montant du contrat
    select sum(avt_montant_ht) into montantConv from accords.avenant avt where avt_suppr='N' and avt_date_valid_adm is not null and con_ordre = uneConv.con_ordre;

		if (totalContributions > montantConv) then
		  dbms_output.put_line ('>>> Convention ' || uneConv.con_ordre || ' ' || totalContributions || ' ' || montantConv);
      --ACCORDS.CORRIGER_TRANCHES_CONTRAT(
      --  CONEXEORDRE => uneConv.exe_ordre,
      --  CONINDEX => uneConv.con_index
      --);
		end if;
	end loop;
end;