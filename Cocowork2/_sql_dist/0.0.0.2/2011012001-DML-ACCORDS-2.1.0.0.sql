SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

execute ACCORDS.TRANSFERT_REPARTS.transferer_reparts;
commit;
-- en principe après ceci, on peut dropper les colonnes... :
-- alter table ACCORDS.SB_RECETTE drop column CAN_ID;
-- alter table ACCORDS.SB_RECETTE drop column PCO_NUM;