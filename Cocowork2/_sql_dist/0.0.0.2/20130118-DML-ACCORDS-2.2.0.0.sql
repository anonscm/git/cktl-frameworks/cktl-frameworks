--
-- Patch DDL de ACCORDS du 18/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.2.0.0
-- Date de publication : 18/01/2013
-- Auteur(s) : Julien LAFOURCADE
-- Licence : CeCILL version 2
--
--

-- Nouveau qui permet de voir tous les contrats (sans restriction dans Cocolight)
INSERT INTO JEFY_ADMIN.FONCTION ( FON_ORDRE, FON_ID_INTERNE, FON_CATEGORIE, FON_DESCRIPTION, FON_LIBELLE, FON_SPEC_GESTION, FON_SPEC_ORGAN, FON_SPEC_EXERCICE, TYAP_ID ) 
VALUES ( 1037, 'CONVTOUS', 'Gestion/Contrats', 'Consultation de tous les contrats',  'Consultation de tous les contrats', 'N', 'N', 'N', 11);

UPDATE ACCORDS.CONTRAT SET UTL_ORDRE_MODIF = UTL_ORDRE_CREATION WHERE UTL_ORDRE_MODIF IS NULL;

Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.2.0.0',to_date('18/01/2013','DD/MM/YYYY'),
    'AJOUT DES CHAMPS POUR LE REPORT SUR LA TABLE TRANCHE ET AXES STRATEGIQUES');

commit;