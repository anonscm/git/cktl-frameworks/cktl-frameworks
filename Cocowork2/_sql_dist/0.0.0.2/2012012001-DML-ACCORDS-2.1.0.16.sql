--
-- Patch DDL de ACCORDS du 20/01/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.16
-- Date de publication : 20/01/2012
-- Auteur(s) : Julien Lafourcade
-- Licence : CeCILL version 2
--
--


--
-- Patch DDL de ACCORDS du 19/12/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;


-- Migration des montant des contributions
CREATE OR REPLACE package ACCORDS.UTILITAIRE_TRA is

  function get_tra_montant(a_tra_ordre number) return number;
  procedure migrate_tra_montant;
  procedure check_tra_montant;
  procedure migrate_frais_gestion;
  
end UTILITAIRE_TRA;
/


CREATE OR REPLACE package body ACCORDS.UTILITAIRE_TRA as

  function get_tra_montant(a_tra_ordre in number) return number is
    somme number;
  begin
    select sum(rpt_montant_participation) into somme from ACCORDS.REPART_PARTENAIRE_TRANCHE where tra_ordre = a_tra_ordre;
    return somme;
  end get_tra_montant;
  
  procedure migrate_tra_montant is
    CURSOR C_TRA(a_con_ordre number) is select * from ACCORDS.TRANCHE where con_ordre = a_con_ordre and tra_suppr = 'N' order by exe_ordre desc;
    CURSOR C_CONTRAT is select * from ACCORDS.CONTRAT where date_migration is null order by con_date_creation desc; 
    CURSOR C_CP_ALL is select * from ACCORDS.CONTRAT_PARTENAIRE; 
    CURSOR C_CP(a_con_ordre number) is select * from ACCORDS.CONTRAT_PARTENAIRE cp where cp.con_ordre = a_con_ordre; 
    currentTra ACCORDS.TRANCHE%rowtype;
    currentContrat ACCORDS.CONTRAT%rowtype;
    cpGestionnaire ACCORDS.CONTRAT_PARTENAIRE%rowtype;
    cpAutre ACCORDS.CONTRAT_PARTENAIRE%rowtype;
    currentCp ACCORDS.CONTRAT_PARTENAIRE%rowtype;
    rptCount number;
    cpCount number;
    cpGestCount number;
    cpSommeMontant number;
    contratMontant number;
    sommeTraMontantInit number;
    montantParticipation number;
    migrCount number;
  begin
    -- On marque comme migrées les conventions qui ont un montant_tranche = 0 et des repart_partenaire_tranche
    update accords.contrat set date_migration = sysdate where con_ordre in (select c.con_ordre from ACCORDS.CONTRAT c, ACCORDS.TRANCHE t, ACCORDS.REPART_PARTENAIRE_TRANCHE rpt 
                                                                                    where c.con_ordre = t.con_ordre(+) and t.tra_ordre = rpt.tra_ordre(+) and (t.tra_montant is null or t.tra_montant = 0) group by c.con_ordre having count(rpt.rpt_id) > 0);  
    -- avant toute chose, remettons d'équerre les tra_montant avec les tra_montant_init, ie si tra_montant_init = 0, on met tra_montant_init = tra_montant
    update ACCORDS.TRANCHE set tra_montant_init = tra_montant where tra_montant_init = 0;
    -- on itère sur les contrat_partenaire pour y pré-insérer les RPT
    for currentCp in C_CP_ALL
    loop
        -- S'il n'y a pas encore de REPART_PARTENAIRE_TRANCHE, on les crées
        select count(*) into rptCount from ACCORDS.REPART_PARTENAIRE_TRANCHE where cp_ordre = currentCp.cp_ordre;
        if (rptCount = 0) then
          -- dbms_output.put_line('--- Insertion des REPART_PARTENAIRE_TRANCHE');
          insert into ACCORDS.REPART_PARTENAIRE_TRANCHE(CP_ORDRE, RPT_ID, RPT_MONTANT_PARTICIPATION, RPT_TAUX_PARTICIPATION, TRA_ORDRE) (select currentCp.cp_ordre, ACCORDS.REPART_PARTENAIRE_TRANCHE_SEQ.nextval, 0, 0, t.tra_ordre from ACCORDS.TRANCHE t where t.con_ordre = currentCP.con_ordre);
        end if;
    end loop;
    -- Ensuite on traîte le cas le plus simple :
    ---- S'il n'y a que deux partenariats et que l'un des partenaires et le service gestionnaire, alors on met le montant de la tranche
    ---- comme contribution de l'autre partenaire.
    migrCount := 0;
    for currentContrat in C_CONTRAT
    loop
        select count(*) into cpCount from ACCORDS.CONTRAT_PARTENAIRE cp where cp.con_ordre = currentContrat.con_ordre;
        select count(*) into cpGestCount from ACCORDS.CONTRAT_PARTENAIRE cp, GRHUM.STRUCTURE_ULR s where 
                                              cp.con_ordre = currentContrat.con_ordre and currentContrat.con_cr = s.c_structure and cp.pers_id = s.pers_id;
        select sum(cp.cp_montant) into cpSommeMontant from ACCORDS.CONTRAT_PARTENAIRE cp where cp.con_ordre = currentContrat.con_ordre;
        select avt_montant_ht into contratMontant from ACCORDS.AVENANT where avt_index = 0 and con_ordre = currentContrat.con_ordre;
        select sum(t.tra_montant_init) into sommeTraMontantInit from ACCORDS.TRANCHE t where t.con_ordre = currentContrat.con_ordre and t.tra_suppr = 'N';

        -- Ensuite on traîte le cas le plus simple A :
        ---- S'il n'y a que deux partenariats et que l'un des partenaires et le service gestionnaire, alors on met le montant de la tranche
        ---- comme contribution de l'autre partenaire.                                      
        if (cpCount = 2 and cpGestCount = 1 and currentContrat.date_migration is null) then
            dbms_output.put_line('--- Migration de  la convention ' || currentContrat.con_ordre);
            -- Service gestionnaire
            select cp.* into cpGestionnaire from ACCORDS.CONTRAT_PARTENAIRE cp, GRHUM.STRUCTURE_ULR s where 
                                         cp.con_ordre = currentContrat.con_ordre and currentContrat.con_cr = s.c_structure and cp.pers_id = s.pers_id;
            -- L'autre
            select * into cpAutre from ACCORDS.CONTRAT_PARTENAIRE cp where cp.con_ordre = currentContrat.con_ordre and cp.pers_id <> cpGestionnaire.pers_id;
            -- Ok on itère sur les tranches maintenant
            for currentTra in C_TRA(currentContrat.con_ordre)
            loop
                dbms_output.put_line('------ A) Update de repart_partenaire_tranche (cp_ordre, tra_ordre, tra_montant) ' || cpAutre.cp_ordre || ',' || currentTra.tra_ordre || ',' || currentTra.tra_montant_init);
                update accords.repart_partenaire_tranche set rpt_montant_participation = currentTra.tra_montant_init where cp_ordre = cpAutre.cp_ordre and tra_ordre = currentTra.tra_ordre;
            end loop;
            -- On marque la convention comme migrée
            update accords.contrat set date_migration = sysdate where con_ordre = currentContrat.con_ordre;
            migrCount := migrCount + 1;
        -- Ensuite on traite le cas B où les CONTRAT_PARTENAIRE sont renseignés et que leur somme correspond au montant de la convention ET que la somme des tranches est égale
        -- au montant de la convention...
        elsif (contratMontant > 0 and cpSommeMontant = contratMontant and sommeTraMontantInit = contratMontant and currentContrat.date_migration is null) then
            dbms_output.put_line('--- Migration de  la convention ' || currentContrat.con_ordre);
            -- Pour chaque partenaire de la conv on crée ou met à jour le RPT avec un montant = montant_contribution_conv x montant_tranche / montant_conv
            for currentCp in C_CP(currentContrat.con_ordre)
            loop
                -- Ok on itère sur les tranches maintenant
                for currentTra in C_TRA(currentContrat.con_ordre)
                loop
                    montantParticipation := ROUND((currentCp.cp_montant * currentTra.tra_montant_init) / contratMontant, 2);
                    dbms_output.put_line('------ B) Update de repart_partenaire_tranche (cp_ordre, tra_ordre, tra_montant) ' || currentCp.cp_ordre || ',' || currentTra.tra_ordre || ',' || montantParticipation);
                    update accords.repart_partenaire_tranche set rpt_montant_participation = montantParticipation where cp_ordre = currentCp.cp_ordre and tra_ordre = currentTra.tra_ordre;
                end loop;
            end loop;
            -- On marque la convention comme migrée
            update accords.contrat set date_migration = sysdate where con_ordre = currentContrat.con_ordre;
            migrCount := migrCount + 1;
        -- Ensuite on traite le dernier cas résiduel (en principe), on met toute la contribution sur le partenaire principal
        else
            dbms_output.put_line('--- Migration de  la convention ' || currentContrat.con_ordre);
            for currentTra in C_TRA(currentContrat.con_ordre)
            loop
                select cp.* into cpAutre from ACCORDS.CONTRAT_PARTENAIRE cp where cp.con_ordre = currentContrat.con_ordre and cp.cp_principal = 'O' and rownum <= 1;
                dbms_output.put_line('------ C) Update de repart_partenaire_tranche (cp_ordre, tra_ordre, tra_montant) ' || cpAutre.cp_ordre || ',' || currentTra.tra_ordre || ',' || currentTra.tra_montant_init);
                update accords.repart_partenaire_tranche set rpt_montant_participation = currentTra.tra_montant_init where cp_ordre = cpAutre.cp_ordre and tra_ordre = currentTra.tra_ordre;
            end loop;
            -- On marque la convention comme migrée
            update accords.contrat set date_migration = sysdate where con_ordre = currentContrat.con_ordre;
            migrCount := migrCount + 1;
        end if;
    end loop;
    dbms_output.put_line('--------- Migration de ' || migrCount || ' convention(s)');
    dbms_output.put_line('--------- Vérification de la migration ---------');
    ACCORDS.UTILITAIRE_TRA.check_tra_montant;    
  end migrate_tra_montant;
  
  procedure check_tra_montant is
      CURSOR C_CONTRAT is select * from ACCORDS.CONTRAT order by con_date_creation desc;
      CURSOR C_TRA(a_con_ordre number) is select * from ACCORDS.TRANCHE where con_ordre = a_con_ordre and tra_suppr = 'N' order by exe_ordre desc;
      currentContrat ACCORDS.CONTRAT%rowtype; 
  begin
      -- Parcours de toutes les conventions et vérif pour chaque tranche que la somme des RPT = tra_montant_init
      for currentContrat in C_CONTRAT
      loop
          for currentTra in C_TRA(currentContrat.con_ordre)
          loop
              -- Vérification que la somme des RPT = tra_montant_init
              if (currentTra.tra_montant_init != get_tra_montant(currentTra.tra_ordre)) then
                  dbms_output.put_line('------ WARN Conv ' || currentContrat.con_ordre || ' : sum RPT = (' || get_tra_montant(currentTra.tra_ordre) || ') != tra_montant (' || currentTra.tra_montant_init || ') with tra_ordre ' || currentTra.tra_ordre);
              end if;
          end loop;
      end loop;
  end check_tra_montant;
  
  
   procedure migrate_frais_gestion is
    CURSOR C_FRAIS_TOTAUX is select tra_ordre, sum(fg_montant) as total_frais from ACCORDS.frais_gestion where tra_ordre is not null GROUP BY tra_ordre;
    CURSOR C_RPT_POUR_TRANCHE(a_tra_ordre number) is select rpt_id, rpt_montant_participation, (select sum(rpt_montant_participation) from ACCORDS.repart_partenaire_tranche where tra_ordre = a_tra_ordre) from ACCORDS.repart_partenaire_tranche where tra_ordre = a_tra_ordre GROUP BY rpt_id, rpt_montant_participation;
    currentTraOrdre number;
    currentTotalFrais number;
    currentRptId number;
    currentRptMontant number;
    currentTotalTranche number;
    currentMontantFrais number;
  begin
    OPEN C_FRAIS_TOTAUX;
    LOOP
      FETCH C_FRAIS_TOTAUX into currentTraOrdre, currentTotalFrais;
      EXIT WHEN C_FRAIS_TOTAUX%NOTFOUND;
      
      OPEN C_RPT_POUR_TRANCHE(currentTraOrdre);
      LOOP
        FETCH C_RPT_POUR_TRANCHE INTO currentRptId, currentRptMontant, currentTotalTranche; 
        EXIT WHEN C_RPT_POUR_TRANCHE%NOTFOUND;
        
        if(currentTotalTranche != 0) then
          currentMontantFrais := ROUND(currentTotalFrais * currentRptMontant / currentTotalTranche, 2);
          insert into ACCORDS.frais_gestion (FG_LIBELLE, FG_MONTANT, FG_ORDRE, FG_PCT_TRA, TRA_ORDRE, RPT_ID) 
          values('Frais', currentMontantFrais, accords.frais_gestion_seq.nextval, 0, null, currentRptId);
        end if;
      END LOOP;
      
      CLOSE C_RPT_POUR_TRANCHE;
    END LOOP;
    CLOSE C_FRAIS_TOTAUX;
    
    update ACCORDS.frais_gestion upfg set FG_PCT_TRA = (
      select  decode(rpt.rpt_montant_participation, 0, 0,round(fg.fg_montant/rpt.rpt_montant_participation*100, 2)) from accords.frais_gestion fg
        inner join ACCORDS.repart_partenaire_tranche rpt on fg.rpt_id = rpt.rpt_id where rpt.rpt_id = upfg.rpt_id) where tra_ordre is null;

    
  end migrate_frais_gestion;

  
end UTILITAIRE_TRA;
/

-- Insert into EVT_EVENEMENT_TYPE (EVTT_ID,EVTT_ID_INTERNE,EVTT_ORDRE_AFFICHAGE,EVTT_LC,EVTT_LL,EVTT_CAT_ID) values 
--	(GRHUM.EVT_EVENEMENT_TYPE_SEQ.NEXTVAL,'NOTE',1,'Note','Note',1);

Insert into JEFY_ADMIN.FONCTION (FON_ORDRE,FON_ID_INTERNE,FON_CATEGORIE,FON_DESCRIPTION,FON_LIBELLE,FON_SPEC_GESTION,FON_SPEC_ORGAN,FON_SPEC_EXERCICE,TYAP_ID) 
values (1036,'SUPADMIN','Gestion/Contrat','Super Utilisateur','Le droit de tout faire','N','N','N',11);



------------------------------------------------------------------------------------------------------------------------
--  Ajout de la clef étrangères DOC_ID
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.16',to_date('20/01/2012','DD/MM/YYYY'),
    'Procédures migration Coconuts');
commit;
