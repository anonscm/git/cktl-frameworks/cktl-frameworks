select
  ded.*,
  tap_taux,
  sysdate,
  con.EXE_ORDRE||'-'||to_char(con.CON_INDEX,'FM0000') as CON_NUMERO, CON_OBJET, CON_OBSERVATIONS, CON_REFERENCE_EXTERNE,
  mg.mg_libelle, con_date_cloture,
  AVT_MONTANT_HT, AVT_MONTANT_TTC,
  avt_lucrativite, avt_recup_tva,
  AVT_DATE_DEB, AVT_DATE_FIN, AVT_DATE_DEB_EXEC, AVT_DATE_FIN_EXEC, AVT_DATE_SIGNATURE, AVT_DATE_SIGNATURE_DEFINITIVE, AVT_DATE_SOUMISSION_CA,
  ACCORDS.UTILITAIRE_TRA.get_tra_montant(tra.tra_ordre) as tra_montant,
  cr.ll_structure as cr_gest, etab.ll_structure as etab_gest,
  parten.pers_libelle_lc as parten_princ,
  org.ORG_UNIV as ORG_UNIV_propo, org.ORG_ETAB as ORG_ETAB_propo, org.ORG_UB as ORG_UB_propo, org.ORG_CR as ORG_CR_propo,
  org.ORG_SOUSCR as ORG_SOUSCR_propo, org.ORG_LIB as ORG_LIB_propo,
  org2.ORG_UNIV as ORG_UNIV_posit, org2.ORG_ETAB as ORG_ETAB_posit, org2.ORG_UB as ORG_UB_posit, org2.ORG_CR as ORG_CR_posit,
  org2.ORG_SOUSCR as ORG_SOUSCR_posit, org2.ORG_LIB as ORG_LIB_posit,
  TCD_CODE, TCD_ABREGE, TCD_LIBELLE
from
  accords.v_suivi_detail ded,
  accords.contrat con,
  accords.avenant avt,
  accords.tranche tra,
  accords.V_PART_PRINC_BIS parten,
  accords.MODE_GESTION mg,
  grhum.structure_ulr cr,
  grhum.structure_ulr etab,
  jefy_admin.ORGAN org,
  jefy_admin.ORGAN org2,
  jefy_admin.TAUX_PRORATA tap,
  jefy_admin.TYPE_CREDIT tcd,
  jefy_admin.UTILISATEUR_ORGAN uo
where
  ded.con_ordre = con.con_ordre and
  ded.org_id_propo = org.org_id and
  ded.org_id_posit = org2.org_id(+) and
  ded.tap_id = tap.tap_id(+) and
  ded.tcd_ordre_posit = tcd.tcd_ordre(+) and
  con.con_ordre = avt.con_ordre and avt.avt_index = 0 and
  con.con_ordre = tra.con_ordre and tra_suppr = 'N' and tra.exe_ordre = ded.exe_ordre and
  avt.avt_mode_gest = mg.mg_ordre and
  avt.avt_ordre = parten.avt_ordre(+) and
  con.con_etablissement = etab.c_structure(+) and con.con_cr = cr.c_structure(+) and

  ded.con_ordre in ($P!{CONORDRES}) and
  (ded.exe_ordre = $P{EXEORDRE} or ded.exe_ordre = $P{EXEORDRE}-1) and
  uo.utl_ordre = $P{UTLORDRE} and uo.org_id = ded.org_id_propo
order by
  ded.con_ordre, ded.nature, ded.org_id_propo, ded.org_id_posit, tcd.tcd_code,
  econ_fcon_date_saisie desc, dpp_rpp_date_saisie