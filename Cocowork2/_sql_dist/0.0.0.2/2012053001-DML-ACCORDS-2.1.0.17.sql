-- migration des domaines scientifiques
execute accords.migrer_domaines_scientifiques;



------------------------------------------------------------------------------------------------------------------------
--  Ajout de la clef étrangères DOC_ID
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.17',to_date('30/05/2012','DD/MM/YYYY'),
    'Procédures migration Coconuts');
commit;