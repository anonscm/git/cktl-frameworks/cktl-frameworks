
SET DEFINE OFF;

--
-- 
-- ___________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ___________________________________________________________________
--
--

--
-- 
-- Fichier:  n°1
-- Type: DDL
-- Schéma:  ACCORDS
-- Numéro de version :  2.0.0.0
-- Date de publication :  25/12/2010
-- Auteur(s) : Michael Haller 
-- Licence : CeCILL version 2
--
--


/* **********************************************************************************
Evolution du schéma pour Cocolight, ajouts pour le support des conventions
 dans le cadre  de la recherche ou des stages, des contacts 
************************************************************************************* */


whenever sqlerror exit sql.sqlcode ;

Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (accords.VERSION_HISTO_SEQ.nextval,'2.0.0',to_date('26/10/2010','DD/MM/YYYY'),'Debut Cocolight');

GRANT UPDATE,INSERT,SELECT on GRHUM.REPART_ASSOCIATION TO ACCORDS;

CREATE TABLE ACCORDS.CONTRAT_PARTENAIRE
(
  CP_ORDRE               NUMBER(38)             NOT NULL,
  CON_ORDRE              NUMBER(38)             NOT NULL,
  PERS_ID                NUMBER                 NOT NULL,
  C_STRUCTURE            VARCHAR2(10 BYTE),
  CP_MONTANT             NUMBER(20,2),
  CP_PRINCIPAL           VARCHAR2(1 BYTE)       NOT NULL,
  TYPE_PART_ORDRE        NUMBER(38),
  CP_DATE_SIGNATURE      DATE,
  CP_REF_EXTERNE_PARTEN  VARCHAR2(100 BYTE)
)
TABLESPACE GFC;

COMMENT ON TABLE ACCORDS.CONTRAT_PARTENAIRE IS 'Entreprises partenaires pour les conventions';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.CP_ORDRE IS 'Cle sequentielle';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.CON_ORDRE IS 'Cle du contrat';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.PERS_ID IS 'Cle de l''entite partenaire (cf. GRHUM.PERSONNE.PERS_ID)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.C_STRUCTURE IS 'Cle de l''entite partenaire (cf. GRHUM.STRUCTURE_ULR.C_STRUCTURE) : doit disparaitre ppour PERS_ID';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.CP_MONTANT IS 'Montant du partenariat (TTC)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.CP_PRINCIPAL IS 'Indicateur partenaire principal (O / N)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.TYPE_PART_ORDRE IS 'Type de partenaire (cf. TYPE_PARTENAIRE)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PARTENAIRE.CP_REF_EXTERNE_PARTEN IS 'Reference de la convention chez le partenaire';


ALTER TABLE ACCORDS.CONTRAT_PARTENAIRE ADD (
  PRIMARY KEY
 (CP_ORDRE)
    USING INDEX 
    TABLESPACE GFC
);

GRANT SELECT ON  ACCORDS.CONTRAT_PARTENAIRE TO JEFY_PAYE;

  
CREATE TABLE ACCORDS.CONTRAT_PART_CONTACT
(
  CPC_ORDRE        NUMBER(38)                   NOT NULL,
  CP_ORDRE        NUMBER(38)                   NOT NULL,
  PERS_ID          NUMBER                       NOT NULL,
  C_STRUCTURE      VARCHAR2(10 BYTE),
  NO_INDIVIDU      NUMBER(38),
  TC_ORDRE         NUMBER(38),
  PERS_ID_CONTACT  NUMBER
)
TABLESPACE GFC;

COMMENT ON TABLE ACCORDS.CONTRAT_PART_CONTACT IS 'Contacts des differents partenaires pour les conventions';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.CPC_ORDRE IS 'Cle sequentielle';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.CP_ORDRE IS 'Cle du contrat_partenaire';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.PERS_ID IS 'Cle de l''entite partenaire (cf. GRHUM.PERSONNE.PERS_ID)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.C_STRUCTURE IS 'Cle de l''entite partenaire (cf. GRHUM.STRUCTURE_ULR.C_STRUCTURE) : doit disparaitre pour PERS_ID';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.NO_INDIVIDU IS 'Cle de l''individu (cf. GRHUM.INDIVIDU_ULR)';

COMMENT ON COLUMN ACCORDS.CONTRAT_PART_CONTACT.TC_ORDRE IS 'Type de contact (cf. TYPE_CONTACT)';


CREATE INDEX ACCORDS.CONTRAT_PART_CONT_AVT_ORDRE ON ACCORDS.CONTRAT_PART_CONTACT
(CP_ORDRE)
TABLESPACE GFC;


CREATE INDEX ACCORDS.CONTRAT_PART_CONT_PERS_ID ON ACCORDS.CONTRAT_PART_CONTACT
(PERS_ID)
TABLESPACE GFC;


CREATE INDEX ACCORDS.CONTRAT_PART_CONT_C_STRUCTURE ON ACCORDS.CONTRAT_PART_CONTACT
(C_STRUCTURE)
TABLESPACE GFC;


CREATE INDEX ACCORDS.CONTRAT_PART_CONT_NO_INDIVIDU ON ACCORDS.CONTRAT_PART_CONTACT
(NO_INDIVIDU)
TABLESPACE GFC;


ALTER TABLE ACCORDS.CONTRAT_PART_CONTACT ADD (
  PRIMARY KEY
 (CPC_ORDRE)
    USING INDEX 
    TABLESPACE GFC
);

GRANT SELECT ON  ACCORDS.CONTRAT_PART_CONTACT TO JEFY_PAYE;

CREATE TABLE ACCORDS.TYPE_CLASSIFICATION_CONTRAT (TCC_CODE VARCHAR2(10) , TCC_ID NUMBER NOT NULL, TCC_LIBELLE VARCHAR2(256) )TABLESPACE GFC;

ALTER TABLE ACCORDS.TYPE_CLASSIFICATION_CONTRAT ADD PRIMARY KEY (TCC_ID);

ALTER TABLE ACCORDS.CONTRAT
 ADD (
 TCC_ID  NUMBER DEFAULT 1 NOT NULL,
 AVIS_FAVORABLE VARCHAR2(1),
 AVIS_DEFAVORABLE VARCHAR2(1),
 CONTEXTE VARCHAR2(512),
 REMARQUES VARCHAR2(512),
 MOTIFS_AVIS VARCHAR2(512),
 C_NAF VARCHAR2(5), 
 CON_GROUPE_PARTENAIRE VARCHAR2(10),
 CON_DUREE NUMBER
 );
 
-- alter table ACCORDS.CONTRAT add CON_GROUPE_PARTENAIRE VARCHAR2(10);
COMMENT ON COLUMN "ACCORDS"."CONTRAT"."CON_GROUPE_PARTENAIRE" IS 'Groupe auquel seront rattaches tous les partenaires et contacts';

CREATE TABLE ACCORDS.INDICATEURS_CONTRAT (IC_CODE VARCHAR2(10) , IC_ID NUMBER NOT NULL, IC_LIBELLE VARCHAR2(256), IC_ORDRE NUMBER NOT NULL );

ALTER TABLE ACCORDS.INDICATEURS_CONTRAT ADD PRIMARY KEY (IC_ID);

create sequence ACCORDS.INDICATEURS_CONTRAT_SEQ START WITH 1;

CREATE TABLE ACCORDS.REPART_INDICATEURS_CONTRAT (CON_ORDRE NUMBER , IC_ID NUMBER , RIC_ID NUMBER NOT NULL);

ALTER TABLE ACCORDS.REPART_INDICATEURS_CONTRAT ADD PRIMARY KEY (RIC_ID);

create sequence ACCORDS.REPART_INDICATEURS_CONTRAT_SEQ START WITH 1;

CREATE SEQUENCE ACCORDS.CONTRAT_PARTENAIRE_SEQ START WITH 1;
    
CREATE SEQUENCE ACCORDS.CONTRAT_PART_CONTACT_SEQ START WITH 1;

CREATE TABLE ACCORDS.CORRESP_TYPE_CONTACT_ASSO(TC_ORDRE NUMBER NOT NULL, ASS_ID NUMBER NOT NULL);

CREATE TABLE accords.PROJET
(
  EXE_ORDRE      NUMBER,
  PJT_LIBELLE     VARCHAR2(250 BYTE),
  PJT_ID       NUMBER                         NOT NULL,
  PJT_ID_PERE  NUMBER,
  TPJT_ID        NUMBER
)
TABLESPACE GFC;


ALTER TABLE accords.PROJET ADD (
  PRIMARY KEY
 (PJT_ID)
    USING INDEX 
    TABLESPACE GFC
);


CREATE TABLE accords.PROJET_CONTRAT
(
  CON_ORDRE       NUMBER,
  PRCO_ID  NUMBER                        NOT NULL,
  PJT_ID        NUMBER
)
TABLESPACE GFC;


ALTER TABLE accords.PROJET_CONTRAT ADD (
  PRIMARY KEY
 (PRCO_ID)
    USING INDEX 
    TABLESPACE GFC
);


CREATE TABLE accords.TYPE_PROJET
(
  TPJT_ID    NUMBER                             NOT NULL,
  TPJT_CODE  VARCHAR2(255 BYTE)
)
TABLESPACE GFC;

CREATE TABLE ACCORDS.REPART_PARTENAIRE_TRANCHE (
    CP_ORDRE NUMBER , 
    RPT_ID NUMBER NOT NULL, 
    RPT_MONTANT_PARTICIPATION NUMBER(12,2) , 
    RPT_TAUX_PARTICIPATION NUMBER(3,2) , 
    TRA_ORDRE NUMBER NOT NULL);

ALTER TABLE ACCORDS.REPART_PARTENAIRE_TRANCHE ADD PRIMARY KEY (RPT_ID);

create sequence ACCORDS.REPART_PARTENAIRE_TRANCHE_SEQ START WITH 1 INCREMENT BY 1;


ALTER TABLE ACCORDS.SB_DEPENSE
 ADD (
    TCD_ORDRE  NUMBER,
    CAN_ID  NUMBER,
    CE_ORDRE NUMBER
 );

ALTER TABLE ACCORDS.SB_RECETTE
 ADD (
    TCD_ORDRE  NUMBER,
    CAN_ID  NUMBER
 );
 
ALTER TABLE accords.TYPE_PROJET ADD (
  PRIMARY KEY
 (TPJT_ID)
    USING INDEX 
    TABLESPACE GFC
 );
  
ALTER TABLE accords.PROJET ADD (
  CONSTRAINT FK_PROJET_PERE 
 FOREIGN KEY (PJT_ID_PERE) 
 REFERENCES ACCORDS.PROJET (PJT_ID));
 
 CREATE SEQUENCE ACCORDS.PROJET_CONTRAT_SEQ START WITH 1;
 
 CREATE SEQUENCE ACCORDS.PROJET_SEQ START WITH 1;

CREATE OR REPLACE VIEW ACCORDS.V_COMPTES(CPT_ORDRE,CPT_UID_GID,CPT_LOGIN,CPT_PASSWD,CPT_CONNEXION,CPT_VLAN,CPT_EMAIL,CPT_DOMAINE,CPT_CHARTE,D_CREATION,D_MODIFICATION)
as
select cn.CPT_ORDRE,cn.CPT_UID_GID,cn.CPT_LOGIN,cn.CPT_PASSWD,cn.CPT_CONNEXION,cn.CPT_VLAN,ce.CEM_EMAIL as cpt_email,ce.CEM_DOMAINE as CPT_DOMAINE,cn.CPT_CHARTE,cn.D_CREATION,cn.D_MODIFICATION from grhum.COMPTE_NEW cn, grhum.compte_email ce
where ce.CPT_ORDRE=cn.CPT_ORDRE
/

CREATE OR REPLACE VIEW accords.V_PART_PRINC_UNIQ_BIS
(AP_ORDRE, AVT_ORDRE)
AS 
select max(CP_ORDRE) as ap_ordre, avt.avt_ordre
  from accords.contrat_partenaire ap, accords.avenant avt
  where ap.CON_ORDRE = avt.con_ordre and ap.cp_principal='O' and avt.avt_index=0
  group by avt.avt_ordre
/

CREATE OR REPLACE VIEW accords.V_PART_PRINC_BIS
(AP_ORDRE, PERS_LIBELLE_LC, AVT_ORDRE, AP_MONTANT, AP_PRINCIPAL, 
 TYPE_PART_ORDRE)
AS 
SELECT
   CONTRAT_PARTENAIRE.CP_ORDRE as ap_ordre,
   PERS_LIBELLE||' '||PERS_LC as PERS_LIBELLE_LC,
   V_PART_PRINC_UNIQ_BIS.AVT_ORDRE,
   CP_MONTANT as AP_MONTANT,
   CP_PRINCIPAL as AP_PRINCIPAL,
   TYPE_PART_ORDRE
FROM
   accords.V_PART_PRINC_UNIQ_BIS,
   accords.CONTRAT_PARTENAIRE,
   GRHUM.PERSONNE
WHERE
   CONTRAT_PARTENAIRE.CP_ORDRE = V_PART_PRINC_UNIQ_BIS.AP_ORDRE AND
   CONTRAT_PARTENAIRE.PERS_ID = GRHUM.PERSONNE.PERS_ID AND
   CP_PRINCIPAL = 'O'
/

CREATE OR REPLACE package accords.TRANSFERT_PARTENAIRE is

  function create_groupe_partenaire(
       conOrdre number
  ) return varchar2; 
  ------------------------------------------------------------------------------------------------
  -- CREE l'appartenance au groupe de partenaire de la convention a partir d'un enregistrement contrat_partenaire.
  ------------------------------------------------------------------------------------------------
  procedure create_association_partenaire(
       cpOrdre number
  );
  
  procedure create_association_etab_gest(
       cpOrdre number
  );
  
  ------------------------------------------------------------------------------------------------
  -- CREE l'appartenance au groupe de contacts de la convention a partir d'un enregistrement contrat_part_contact.
  ------------------------------------------------------------------------------------------------
  procedure create_association_contact(
       cpcOrdre number
  );

  procedure create_association_centre(
       cpcOrdre number
  );
  
  

  procedure transfert_contact(
           cpOrdre Number
 );

    procedure transfert_partenaires(
      conOrdre number
  );
  
  procedure transfert_centre( 
  conOrdre Number,
  cpOrdre Number,
  cpPersId Number,
  cpCStructure Number,
  centregest varchar2
  );

  
  procedure transfert_gestionnaires(
     conOrdre Number
  );
 
  
  procedure transfert_contrat;
  
  

  
end TRANSFERT_PARTENAIRE;
/

CREATE OR REPLACE package body accords.TRANSFERT_PARTENAIRE is

  ------------------------------------------------------------------------------------------------
  -- CREE le groupe ou seront associes les partenaires de la convention.
  -- Renvoit un C_STRUCTURE.
  ------------------------------------------------------------------------------------------------
  function create_groupe_partenaire(
       conOrdre number
  ) return varchar2
  is
   nb number;
   conv accords.contrat%rowtype;
   cstructure varchar2(10);
   cstructurepere varchar2(10);
   persid number;
   begin
    cstructure:=null;
    persid:=null;
    select count(*) into nb from grhum.grhum_parametres where param_key='ANNUAIRE_PARTENARIAT';
 if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_groupe_partenaire : Le groupe racine de partenariat est inexistant!!!');
 end if;
 select param_value into cstructurepere from grhum.grhum_parametres where param_key='ANNUAIRE_PARTENARIAT';
    select count(*) into nb from accords.contrat where con_ordre = conOrdre and con_groupe_partenaire is null;
    if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_groupe_partenaire : La convention existe pas ou possede deja un groupe pour ces partenaires!!!');
 end if;
    select * into conv from  accords.contrat where con_ordre = conOrdre;
 grhum.Ins_Groupe(cstructure,
         persid,
        'PN',
        'Partenaires de l acte CONV-'||conv.exe_ordre||'-'||conv.con_index,
         'Part CONV-'||conv.exe_ordre||'-'||conv.con_index,
        cstructurepere,
          null,null,null,null,null,null,null,null,null,null,null,null);
  update accords.contrat set con_groupe_partenaire=cstructure where con_ordre=conOrdre;
  return cstructure;
  end create_groupe_partenaire;
 
  ------------------------------------------------------------------------------------------------
  -- CREE l'appartenance au groupe de partenaire de la convention a partir d'un enregistrement contrat_partenaire.
  ------------------------------------------------------------------------------------------------
  procedure create_association_partenaire(
       cpOrdre number
  )
  is
   nb number;
   part accords.contrat_partenaire%rowtype;
   cgroupe varchar2(10);
   assid number;
   rasid number;
   douverture accords.avenant.AVT_DATE_DEB%type;
   dfermeture accords.avenant.AVT_DATE_FIN%type;
   -- cstructurepere varchar2(10);
   -- persid number;
   begin
    
    select count(*) into nb from accords.contrat_partenaire where cp_ordre=cpOrdre;
 if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_partenaire : Le partenariat est inexistant!!!');
 end if;
 select * into part from accords.contrat_partenaire where cp_ordre=cpOrdre;
    
 select count(*) into nb from accords.contrat where con_ordre = part.con_ordre and con_groupe_partenaire is not null;
    if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_partenaire : Impossible de determiner un groupe partenarial pour la convention de ce partenariat!!!');
 end if;
    select con_groupe_partenaire into cgroupe from  accords.contrat where con_ordre = part.con_ordre;
 
 select MIN(a.AVT_DATE_DEB) into douverture from avenant a where a.CON_ORDRE=part.con_ordre;
 select MAX(a.AVT_DATE_FIN) into dfermeture from avenant a where a.CON_ORDRE=part.con_ordre;
 
-- if(part.cp_principal = 'O') then
  -- select ass_id into assid from grhum.association where ass_code='ETABINTPRINC';
   --assid:=145;
 --else
   select ass_id into assid from grhum.association where ass_code='PARTCONTRACT';
 --end if;
 
 select grhum.REPART_ASSOCIATION_SEQ.nextval into rasid from dual;
 
 GRHUM.INS_REPART_STRUCTURE ( part.pers_id, cgroupe );
 
 INSERT INTO GRHUM.REPART_ASSOCIATION ( RAS_ID, PERS_ID, C_STRUCTURE, ASS_ID, RAS_RANG, RAS_COMMENTAIRE,
 RAS_D_OUVERTURE, RAS_D_FERMETURE, D_CREATION, D_MODIFICATION, RAS_QUOTITE, PERS_ID_CREATION,
 PERS_ID_MODIFICATION ) 
 VALUES ( rasid, part.pers_id, cgroupe, assid, 1, NULL,  douverture, dfermeture,  SYSDATE,  SYSDATE, NULL, NULL, NULL); 

  end create_association_partenaire;
  
  procedure create_association_etab_gest(
       cpOrdre number
  )
  is
   nb number;
   part accords.contrat_partenaire%rowtype;
   cgroupe varchar2(10);
   assid number;
   rasid number;
   douverture accords.avenant.AVT_DATE_DEB%type;
   dfermeture accords.avenant.AVT_DATE_FIN%type;
   -- cstructurepere varchar2(10);
   -- persid number;
   begin
    
    select count(*) into nb from accords.contrat_partenaire where cp_ordre=cpOrdre;
 if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_partenaire : Le partenariat est inexistant!!!');
 end if;
 select * into part from accords.contrat_partenaire where cp_ordre=cpOrdre;
    
 select count(*) into nb from accords.contrat where con_ordre = part.con_ordre and con_groupe_partenaire is not null;
    if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_partenaire : Impossible de determiner un groupe partenarial pour la convention de ce partenariat!!!');
 end if;
    select con_groupe_partenaire into cgroupe from  accords.contrat where con_ordre = part.con_ordre;
 
 select MIN(a.AVT_DATE_DEB) into douverture from avenant a where a.CON_ORDRE=part.con_ordre;
 select MAX(a.AVT_DATE_FIN) into dfermeture from avenant a where a.CON_ORDRE=part.con_ordre;
 
-- if(part.cp_principal = 'O') then
  -- select ass_id into assid from grhum.association where ass_code='ETABINTPRINC';
   --assid:=145;
 --else
   select ass_id into assid from grhum.association where ass_code='ETABINTPRINC';
 --end if;
 
 select grhum.REPART_ASSOCIATION_SEQ.nextval into rasid from dual;
 
 GRHUM.INS_REPART_STRUCTURE ( part.pers_id, cgroupe );
 
 INSERT INTO GRHUM.REPART_ASSOCIATION ( RAS_ID, PERS_ID, C_STRUCTURE, ASS_ID, RAS_RANG, RAS_COMMENTAIRE,
 RAS_D_OUVERTURE, RAS_D_FERMETURE, D_CREATION, D_MODIFICATION, RAS_QUOTITE, PERS_ID_CREATION,
 PERS_ID_MODIFICATION ) 
 VALUES ( rasid, part.pers_id, cgroupe, assid, 1, NULL,  douverture, dfermeture,  SYSDATE,  SYSDATE, NULL, NULL, NULL); 

  end create_association_etab_gest;
  
  ------------------------------------------------------------------------------------------------
  -- CREE l'appartenance au groupe de contacts de la convention a partir d'un enregistrement contrat_part_contact.
  ------------------------------------------------------------------------------------------------
  procedure create_association_contact(
       cpcOrdre number
  )
  is
   nb number;
   cont accords.contrat_part_contact%rowtype;
   cgroupe varchar2(10);
   assid number;
   rasid number;
   douverture accords.avenant.AVT_DATE_DEB%type;
   dfermeture accords.avenant.AVT_DATE_FIN%type;
   count_repart number;

   -- cstructurepere varchar2(10);
   -- persid number;
   begin
    
    select count(*) into nb from accords.contrat_part_contact where cpc_ordre=cpcOrdre;
 if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_contact : Le partenariat est inexistant!!!');
 end if;
 select * into cont from accords.contrat_part_contact where cpc_ordre=cpcOrdre;
    
 select count(*) into nb from accords.contrat c, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and c.con_ordre = part.con_ordre and con_groupe_partenaire is not null;
    if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_contact : Impossible de determiner un groupe partenarial pour la convention de ce contact!!!');
 end if;
    select con_groupe_partenaire into cgroupe from  accords.contrat c, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and c.con_ordre = part.con_ordre;
 
 select MIN(a.AVT_DATE_DEB) into douverture from avenant a, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and a.con_ordre = part.con_ordre;
 select MAX(a.AVT_DATE_FIN) into dfermeture from avenant a, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and a.con_ordre = part.con_ordre;

 if(cont.tc_ordre is not null) then
    select ass_id into assid from ACCORDS.CORRESP_TYPE_CONTACT_ASSO where tc_ordre=cont.tc_ordre;
    if(assid is null) then
             select a.ass_id into assid from grhum.association a, grhum.type_association ta where ta.TAS_CODE='CONV' and a.TAS_ID=ta.TAS_ID and a.ASS_CODE='AUTRE';
    end if;
 else
     select a.ass_id into assid from grhum.association a, grhum.type_association ta where ta.TAS_CODE='CONV' and a.TAS_ID=ta.TAS_ID and a.ASS_CODE='AUTRE';
    
 
 end if;
 
 select grhum.REPART_ASSOCIATION_SEQ.nextval into rasid from dual;
 --en cas de problèmec'est  vérifier que: select * from accords.contrat_part_contact where pers_id_contact not in (select pers_id FROM grhum.v_personne); ne retourne rien
 GRHUM.INS_REPART_STRUCTURE ( cont.pers_id_contact, cgroupe );
 
 -- test si l'association n'existe pas déja (cas d'une personne contact de plusieurs partenaires dans le même contrat)
 select count(*) into count_repart from grhum.repart_association 
 where pers_id = cont.pers_id_contact and c_structure = cgroupe and ass_id = assid and ras_d_ouverture = douverture;

 if(count_repart = 0) then
	 INSERT INTO GRHUM.REPART_ASSOCIATION ( RAS_ID, PERS_ID, C_STRUCTURE, ASS_ID, RAS_RANG, RAS_COMMENTAIRE,
	 RAS_D_OUVERTURE, RAS_D_FERMETURE, D_CREATION, D_MODIFICATION, RAS_QUOTITE, PERS_ID_CREATION,
	 PERS_ID_MODIFICATION ) 
	 VALUES ( rasid, cont.pers_id_contact, cgroupe, assid, 2, NULL,  douverture, dfermeture,  SYSDATE,  SYSDATE, NULL, NULL, NULL); 
 end if;
  
  end create_association_contact;

  procedure create_association_centre(
       cpcOrdre number
  )
  is
   nb number;
   cont accords.contrat_part_contact%rowtype;
   cgroupe varchar2(10);
   assid number;
   rasid number;
   douverture accords.avenant.AVT_DATE_DEB%type;
   dfermeture accords.avenant.AVT_DATE_FIN%type;
   
   -- cstructurepere varchar2(10);
   -- persid number;
   begin
    
    select count(*) into nb from accords.contrat_part_contact where cpc_ordre=cpcOrdre;
 if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_contact : Le partenariat est inexistant!!!');
 end if;
 select * into cont from accords.contrat_part_contact where cpc_ordre=cpcOrdre;
    
 select count(*) into nb from accords.contrat c, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and c.con_ordre = part.con_ordre and con_groupe_partenaire is not null;
    if(nb!=1) then
   RAISE_APPLICATION_ERROR(-20000,'create_association_contact : Impossible de determiner un groupe partenarial pour la convention de ce contact!!!');
 end if;
    select con_groupe_partenaire into cgroupe from  accords.contrat c, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and c.con_ordre = part.con_ordre;
 
 select MIN(a.AVT_DATE_DEB) into douverture from avenant a, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and a.con_ordre = part.con_ordre;
 select MAX(a.AVT_DATE_FIN) into dfermeture from avenant a, accords.contrat_partenaire part where part.CP_ORDRE=cont.cp_ordre and a.con_ordre = part.con_ordre;


     select a.ass_id into assid from grhum.association a, grhum.type_association ta where ta.TAS_CODE='CONV' and a.TAS_ID=ta.TAS_ID and a.ASS_CODE='REPRGESTPART';
    
 
 
 select grhum.REPART_ASSOCIATION_SEQ.nextval into rasid from dual;
 
 GRHUM.INS_REPART_STRUCTURE ( cont.pers_id_contact, cgroupe );
 
 INSERT INTO GRHUM.REPART_ASSOCIATION ( RAS_ID, PERS_ID, C_STRUCTURE, ASS_ID, RAS_RANG, RAS_COMMENTAIRE,
 RAS_D_OUVERTURE, RAS_D_FERMETURE, D_CREATION, D_MODIFICATION, RAS_QUOTITE, PERS_ID_CREATION,
 PERS_ID_MODIFICATION ) 
 VALUES ( rasid, cont.pers_id_contact, cgroupe, assid, 2, NULL,  douverture, dfermeture,  SYSDATE,  SYSDATE, NULL, NULL, NULL); 

 
  
  end create_association_centre;
  
  

  procedure transfert_contact(
           cpOrdre Number
 )
  is
   CURSOR C1 is select * from accords.contrat_part_contact c where c.CP_ORDRE=cpOrdre ;
   cont accords.contrat_part_contact%rowtype;
   nb varchar2(10);
   begin
    
    open C1;
  loop
   fetch C1 into cont;
   exit when C1%notfound;

     create_association_contact(cont.cpc_ordre);
  
  end loop;
  close C1;
  
  end transfert_contact;

    procedure transfert_partenaires(
      conOrdre number
  )
  is
   CURSOR C1 is select * from accords.contrat_partenaire where con_ordre=conOrdre;
   contp accords.contrat_partenaire%rowtype;
   begin
    
    open C1;
  loop
   fetch C1 into contp;
   exit when C1%notfound;

     create_association_partenaire(contp.cp_ordre);
  
     transfert_contact(contp.cp_ordre);
  
  end loop;
  close C1;
  
  end transfert_partenaires;
  
  procedure transfert_centre( 
  conOrdre Number,
  cpOrdre Number,
  cpPersId Number,
  cpCStructure Number,
  centregest varchar2
  )
  is
   cpcOrdre Number;
   persIdCont Number;
   nb varchar2(10);
   begin
    
    select count(*) into nb from contrat;
    
    select accords.contrat_part_contact_seq.nextval into cpcOrdre from dual;
    select pers_id into persIdCont from grhum.structure_ulr where c_structure=centregest;
    INSERT INTO accords.contrat_part_contact( CPC_ORDRE, CP_ORDRE, PERS_ID, C_STRUCTURE, NO_INDIVIDU, TC_ORDRE, PERS_ID_CONTACT  ) 
    VALUES (cpcOrdre, cpOrdre, cpPersId, cpCStructure, null, null, persIdCont );
    create_association_centre(cpcOrdre);
   end transfert_centre;

  
  procedure transfert_gestionnaires(
     conOrdre Number
  )
  is
   persid Number;
   cpOrdre Number;
   cStructure varchar2(10);
   cGroupe varchar2(10);
   assid Number;
   assidpart Number;
   centregest varchar2(10);
 nb Integer;
  begin
      select count(*) into nb from contrat c where c.con_ordre=conOrdre and c.CON_ETABLISSEMENT is not null;
      
    if(nb=1) then
        
      select count(*) into nb from grhum.structure_ulr s, accords.contrat c where c.CON_ORDRE=conOrdre and s.C_STRUCTURE=c.CON_ETABLISSEMENT;
      select c.CON_CR into centregest from contrat c where c.con_ordre=conOrdre and c.CON_ETABLISSEMENT is not null;
      select c.CON_ETABLISSEMENT into cStructure from accords.contrat c where c.CON_ORDRE=conOrdre;
           
      if(nb!=1) then
        RAISE_APPLICATION_ERROR(-20000,'Transfert gestionnaire : Impossible de determiner l etablissement gestionnaire');  
      end if;
  
      select s.pers_id into persid from grhum.structure_ulr s, accords.contrat c where c.CON_ORDRE=conOrdre and s.C_STRUCTURE=c.CON_ETABLISSEMENT;
      
      select count(*) into nb from accords.contrat_partenaire cp where CON_ORDRE=conOrdre and pers_id=persid; 
      if(nb=1) then
          update accords.contrat_partenaire cp set cp.CP_PRINCIPAL='O' where CON_ORDRE=conOrdre and pers_id=persid; 
          select cp_ordre into cpOrdre from accords.contrat_partenaire where CON_ORDRE=conOrdre and pers_id=persid;
          select count(*) into nb from accords.contrat where con_ordre = conOrdre and con_groupe_partenaire is not null;
          if(nb!=1) then
                    RAISE_APPLICATION_ERROR(-20000,'transfert_gestionnaires : Impossible de determiner un groupe partenarial pour la convention de ce partenariat!!!');
          end if;
          select con_groupe_partenaire into cgroupe from  accords.contrat where con_ordre = conOrdre;
          
          select ass_id into assidpart from grhum.association where ass_code='PARTCONTRACT';
          select ass_id into assid from grhum.association where ass_code='ETABINTPRINC';
          --end if;
 
          select count(*) into nb from GRHUM.REPART_ASSOCIATION where C_STRUCTURE=cgroupe and pers_id=persid and ass_id=assidpart;
          if(nb!=1) then
                    RAISE_APPLICATION_ERROR(-20000,'transfert_gestionnaires : erreur lors de la recuperation du partenaire');
          end if;
          
          update GRHUM.REPART_ASSOCIATION set ass_id=assid where C_STRUCTURE=cgroupe and pers_id=persid and ass_id=assidpart;
 
 
 ---------------------------- 
      else
          select accords.contrat_partenaire_seq.nextval into cpOrdre from dual;
          INSERT INTO ACCORDS.CONTRAT_PARTENAIRE ( CP_ORDRE, CON_ORDRE, PERS_ID, C_STRUCTURE, CP_MONTANT,
          CP_PRINCIPAL, TYPE_PART_ORDRE, CP_DATE_SIGNATURE,CP_REF_EXTERNE_PARTEN ) 
          VALUES (cpOrdre, conOrdre, persid, cStructure, 0,'O', null, null,null);
          
          create_association_etab_gest(cpOrdre);
      end if;
      if(centregest is not null) then
        transfert_centre(conOrdre,cpOrdre,persid,cStructure,centregest);
      end if;
    
  
    end if;
    
 end transfert_gestionnaires;
 
  
  procedure transfert_contrat
  is
   CURSOR C1 is select * from accords.contrat where con_groupe_partenaire is null;
   cont accords.contrat%rowtype;
   nb varchar2(10);
   begin
    
    open C1;
  loop
   fetch C1 into cont;
   exit when C1%notfound;

     nb:=create_groupe_partenaire(cont.con_ordre);
  
     transfert_partenaires(cont.con_ordre);
  
     transfert_gestionnaires(cont.con_ordre);
  
  end loop;
  close C1;
  
  end transfert_contrat;
  
  

  
end TRANSFERT_PARTENAIRE;
/



create or replace PROCEDURE accords.update_data_v200
is
CURSOR C1 is select  ap.AP_ORDRE as CP_ORDRE, a.CON_ORDRE, ap.PERS_ID, ap.C_STRUCTURE, ap.AP_MONTANT as CP_MONTANT,
ap.AP_PRINCIPAL as CP_PRINCIPAL, ap.TYPE_PART_ORDRE, ap.AP_DATE_SIGNATURE as CP_DATE_SIGNATURE,
ap.AP_REF_EXTERNE_PARTEN as CP_REF_EXTERNE_PARTEN from accords.AVENANT_PARTENAIRE ap,accords.avenant a where a.avt_ordre=ap.avt_ordre;

CURSOR C2 is select apc.APC_ORDRE as CPC_ORDRE, cp.CP_ORDRE, apc.PERS_ID, apc.C_STRUCTURE, apc.NO_INDIVIDU, apc.TC_ORDRE, apc.PERS_ID_CONTACT 
from accords.avenant_part_contact apc, accords.avenant a, accords.contrat_partenaire cp
where a.AVT_ORDRE=apc.AVT_ORDRE and cp.CON_ORDRE=a.CON_ORDRE and cp.PERS_ID=apc.PERS_ID;

cp ACCORDS.CONTRAT_PARTENAIRE%rowtype;

cpc accords.contrat_part_contact%rowtype;

seq Integer;
nb Integer;

begin
  open C1;
  loop
   fetch C1 into cp;
   exit when C1%notfound;

        INSERT INTO ACCORDS.CONTRAT_PARTENAIRE ( CP_ORDRE, CON_ORDRE, PERS_ID, C_STRUCTURE, CP_MONTANT,
CP_PRINCIPAL, TYPE_PART_ORDRE, CP_DATE_SIGNATURE,
CP_REF_EXTERNE_PARTEN ) VALUES (cp.CP_ORDRE, cp.CON_ORDRE, cp.PERS_ID, cp.C_STRUCTURE, cp.CP_MONTANT,
cp.CP_PRINCIPAL, cp.TYPE_PART_ORDRE, cp.CP_DATE_SIGNATURE,
cp.CP_REF_EXTERNE_PARTEN);
        
  end loop;
  close C1;
  

  
  open C2;
  loop
   fetch C2 into cpc;
   exit when C2%notfound;

        INSERT INTO accords.contrat_part_contact( CPC_ORDRE, CP_ORDRE, PERS_ID, C_STRUCTURE, NO_INDIVIDU, TC_ORDRE, PERS_ID_CONTACT  ) 
        VALUES (ACCORDS.CONTRAT_PART_CONTACT_SEQ.nextval, cpc.CP_ORDRE, cpc.PERS_ID, cpc.C_STRUCTURE, cpc.NO_INDIVIDU, cpc.TC_ORDRE, cpc.PERS_ID_CONTACT );
        --VALUES (cpc.CPC_ORDRE, cpc.CP_ORDRE, cpc.PERS_ID, cpc.C_STRUCTURE, cpc.NO_INDIVIDU, cpc.TC_ORDRE, cpc.PERS_ID_CONTACT );
  end loop;
  close C2;
  
    

select count(*) into nb from accords.contrat_partenaire;
    
    if(nb>0)
    then
        select max(cp_ordre)+1 into seq from accords.contrat_partenaire;
      EXECUTE IMMEDIATE 'ALTER SEQUENCE ACCORDS.CONTRAT_PARTENAIRE_SEQ INCREMENT BY ' || nb;
      EXECUTE IMMEDIATE 'SELECT ACCORDS.CONTRAT_PARTENAIRE_SEQ.NEXTVAL FROM dual' INTO nb;
      EXECUTE IMMEDIATE 'ALTER SEQUENCE ACCORDS.CONTRAT_PARTENAIRE_SEQ INCREMENT BY 1';
    end if;
    
    select count(*) into nb from accords.contrat_part_contact;
    
    if(nb>0)
    then    
        select max(cpc_ordre)+1 into seq from accords.contrat_part_contact;
      EXECUTE IMMEDIATE 'ALTER SEQUENCE ACCORDS.CONTRAT_PART_CONTACT_SEQ INCREMENT BY ' || nb;
      EXECUTE IMMEDIATE 'SELECT ACCORDS.CONTRAT_PART_CONTACT_SEQ.NEXTVAL FROM dual' INTO nb;
      EXECUTE IMMEDIATE 'ALTER SEQUENCE ACCORDS.CONTRAT_PART_CONTACT_SEQ INCREMENT BY 1';
    end if;
    

    -- Cocowork2 a besoin d'avoir l'arborescence ANNUAIRE_PARTENARIAT de remplie =  1 groupe pour chaque convention qui contient tout les partenaires 
    TRANSFERT_PARTENAIRE.transfert_contrat;

end;
/

-- Script Vers version 2.1.0.0
-- Recréation de la table ACCORDS.AVENANT_EVT_EVENEMENT
CREATE TABLE ACCORDS.AVENANT_EVT_EVENEMENT
  (
    AVT_ORDRE NUMBER(38,0) NOT NULL,
    EVT_ID NUMBER(22,0) NOT NULL,
    PRIMARY KEY (AVT_ORDRE, EVT_ID) USING INDEX TABLESPACE GFC
  )
  TABLESPACE GFC ;
  COMMENT ON COLUMN ACCORDS.AVENANT_EVT_EVENEMENT.AVT_ORDRE
IS
  'Cle de l''avenant';
  COMMENT ON COLUMN ACCORDS.AVENANT_EVT_EVENEMENT.EVT_ID
IS
  'Cle de l''evenement';
  COMMENT ON TABLE ACCORDS.AVENANT_EVT_EVENEMENT
IS
  'Evenements lies aux conventions';

GRANT REFERENCES ON GRHUM.EVT_EVENEMENT TO ACCORDS;
GRANT SELECT ON GRHUM.EVT_EVENEMENT TO ACCORDS;
  
ALTER TABLE ACCORDS.AVENANT_EVT_EVENEMENT ADD (
  CONSTRAINT FK_AVENANT FOREIGN KEY (AVT_ORDRE) REFERENCES ACCORDS.AVENANT (AVT_ORDRE) DEFERRABLE INITIALLY DEFERRED) ;

ALTER TABLE ACCORDS.AVENANT_EVT_EVENEMENT ADD (
  CONSTRAINT FK_EVENEMENT FOREIGN KEY (EVT_ID) REFERENCES GRHUM.EVT_EVENEMENT (EVT_ID) DEFERRABLE INITIALLY DEFERRED);  
 
  
-- TODO : migrer les tables existantes : EVENEMENT, AVENANT_EVENEMENT, NATURE_EVENEMENT, REPART_INDIVIDU_EVT 
-- vers les nouvelles tables de grhum...

-- Puis drop des objets
--drop table ACCORDS.EVENEMENT;
--drop sequence ACCORDS.EVENEMENT_SEQ;
--drop table ACCORDS.AVENANT_EVENEMENT;
--drop sequence ACCORDS.AVENANT_EVENEMENT_SEQ;
--drop table ACCORDS.NATURE_EVENEMENT;
--drop table ACCORDS.REPART_INDIVIDU_EVT;
--drop sequence ACCORDS.REPART_INDIVIDU_EVT_SEQ;


-- TABLE JOINTURE SB_RECETTE / CODE_ANALYTIQUE
CREATE TABLE ACCORDS.REPART_SB_RECETTE_C_ANALYTIQUE
(
  SR_ORDRE NUMBER NOT NULL,
  MONTANT_HT NUMBER(12,2) NOT NULL,
  CAN_ID NUMBER NOT NULL 
)
TABLESPACE GFC;
ALTER TABLE ACCORDS.REPART_SB_RECETTE_C_ANALYTIQUE ADD (
  PRIMARY KEY
 (SR_ORDRE, CAN_ID)
    USING INDEX 
    TABLESPACE GFC
 );
ALTER TABLE ACCORDS.REPART_SB_RECETTE_C_ANALYTIQUE ADD (
  CONSTRAINT FK_REP_C_ANALYTIQUE_SB_RECETTE 
 FOREIGN KEY (SR_ORDRE) 
 REFERENCES ACCORDS.SB_RECETTE (SR_ORDRE));
ALTER TABLE ACCORDS.REPART_SB_RECETTE_C_ANALYTIQUE ADD (
  CONSTRAINT FK_REP_CODE_ANALYTIQUE 
 FOREIGN KEY (CAN_ID) 
 REFERENCES JEFY_ADMIN.CODE_ANALYTIQUE (CAN_ID));
 
 -- TABLE JOINTURE SB_RECETTE / PLAN_CO
CREATE TABLE ACCORDS.REPART_SB_RECETTE_PLANCO
(
  SR_ORDRE NUMBER NOT NULL,
  MONTANT_HT NUMBER(12,2) NOT NULL,
  PCO_NUM VARCHAR2(20) NOT NULL 
)
TABLESPACE GFC;
ALTER TABLE ACCORDS.REPART_SB_RECETTE_PLANCO ADD (
  PRIMARY KEY
 (SR_ORDRE, PCO_NUM)
    USING INDEX 
    TABLESPACE GFC
 );
ALTER TABLE ACCORDS.REPART_SB_RECETTE_PLANCO ADD (
  CONSTRAINT FK_REP_PLANCO_SB_RECETTE 
 FOREIGN KEY (SR_ORDRE) 
 REFERENCES ACCORDS.SB_RECETTE (SR_ORDRE));
ALTER TABLE ACCORDS.REPART_SB_RECETTE_PLANCO ADD (
  CONSTRAINT FK_REP_PLANCO 
 FOREIGN KEY (PCO_NUM) 
 REFERENCES MARACUJA.PLAN_COMPTABLE (PCO_NUM));
 
 -- TABLE JOINTURE SB_DEPENSE / CODE_ANALYTIQUE
CREATE TABLE ACCORDS.REPART_SB_DEPENSE_C_ANALYTIQUE
(
  SD_ORDRE NUMBER NOT NULL,
  MONTANT_HT NUMBER(12,2) NOT NULL,
  CAN_ID NUMBER NOT NULL 
)
TABLESPACE GFC;
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_C_ANALYTIQUE ADD (
  PRIMARY KEY
 (SD_ORDRE, CAN_ID)
    USING INDEX 
    TABLESPACE GFC
 );
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_C_ANALYTIQUE ADD (
  CONSTRAINT FK_REP_C_ANALYTIQUE_SB_DEPENSE 
 FOREIGN KEY (SD_ORDRE) 
 REFERENCES ACCORDS.SB_DEPENSE (SD_ORDRE));
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_C_ANALYTIQUE ADD (
  CONSTRAINT FK_REP_SD_CODE_ANALYTIQUE 
 FOREIGN KEY (CAN_ID) 
 REFERENCES JEFY_ADMIN.CODE_ANALYTIQUE (CAN_ID));
 
 -- TABLE JOINTURE SB_DEPENSE / PLAN_CO
CREATE TABLE ACCORDS.REPART_SB_DEPENSE_PLANCO
(
  SD_ORDRE NUMBER NOT NULL,
  MONTANT_HT NUMBER(12,2) NOT NULL,
  PCO_NUM VARCHAR2(20) NOT NULL 
)
TABLESPACE GFC;
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_PLANCO ADD (
  PRIMARY KEY
 (SD_ORDRE, PCO_NUM)
    USING INDEX 
    TABLESPACE GFC
 );
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_PLANCO ADD (
  CONSTRAINT FK_REP_PLANCO_SB_DEPENSE 
 FOREIGN KEY (SD_ORDRE) 
 REFERENCES ACCORDS.SB_DEPENSE (SD_ORDRE));
ALTER TABLE ACCORDS.REPART_SB_DEPENSE_PLANCO ADD (
  CONSTRAINT FK_REP_SD_PLANCO 
 FOREIGN KEY (PCO_NUM) 
 REFERENCES MARACUJA.PLAN_COMPTABLE (PCO_NUM));

-- FK SB_RECETTE -> TYPE_CREDIT
ALTER TABLE ACCORDS.SB_RECETTE ADD (
  CONSTRAINT FK_RECETTE_TC 
 FOREIGN KEY (TCD_ORDRE) 
 REFERENCES JEFY_ADMIN.TYPE_CREDIT (TCD_ORDRE));
 
-- FK SB_DEPENSE / TYPE_CREDIT
ALTER TABLE ACCORDS.SB_DEPENSE ADD (
  CONSTRAINT FK_DEPENSE_TC 
 FOREIGN KEY (TCD_ORDRE) 
 REFERENCES JEFY_ADMIN.TYPE_CREDIT (TCD_ORDRE));
 
-- FK SB_RECETTE / LOLF
ALTER TABLE ACCORDS.SB_RECETTE ADD (
  CONSTRAINT FK_REC_LOLF 
 FOREIGN KEY (LOLF_ID) 
 REFERENCES JEFY_ADMIN.LOLF_NOMENCLATURE_RECETTE (LOLF_ID));

-- FK SB_DEPENSE / LOLF
ALTER TABLE ACCORDS.SB_DEPENSE ADD (
  CONSTRAINT FK_DEP_LOLF 
 FOREIGN KEY (LOLF_ID) 
 REFERENCES JEFY_ADMIN.LOLF_NOMENCLATURE_DEPENSE (LOLF_ID));
 
----------------------------------------------------------
-- Procédure de migration des reparts depense et recette
----------------------------------------------------------
CREATE OR REPLACE package ACCORDS.TRANSFERT_REPARTS is

  procedure transferer_reparts;

end TRANSFERT_REPARTS;
/


CREATE OR REPLACE package body ACCORDS.TRANSFERT_REPARTS as
  
  procedure transferer_reparts
  is
   CURSOR C_REC is select * from ACCORDS.SB_RECETTE;
   CURSOR C_DEP is select * from ACCORDS.SB_DEPENSE;
   REC ACCORDS.SB_RECETTE%rowtype;
   DEP ACCORDS.SB_DEPENSE%rowtype;
   begin
    open C_REC;
    loop
     fetch C_REC into REC;
     exit when C_REC%notfound;
     -- pareil pour PCO_NUM
     if (REC.PCO_NUM is not null) then
      insert into ACCORDS.REPART_SB_RECETTE_PLANCO (SR_ORDRE,MONTANT_HT,PCO_NUM) values (REC.SR_ORDRE,REC.SR_MONTANT_HT,REC.PCO_NUM);
     end if;
     -- pareil pour CAN_ID
     if (REC.CAN_ID is not null) then
      insert into ACCORDS.REPART_SB_RECETTE_C_ANALYTIQUE (SR_ORDRE,MONTANT_HT,CAN_ID) values (REC.SR_ORDRE,REC.SR_MONTANT_HT,REC.CAN_ID);
     end if;
    end loop;
    close C_REC;
    open C_DEP;
    loop
     fetch C_DEP into DEP;
     exit when C_DEP%notfound;
     -- pareil pour PCO_NUM
     if (DEP.PCO_NUM is not null) then
        insert into ACCORDS.REPART_SB_DEPENSE_PLANCO (SD_ORDRE,MONTANT_HT,PCO_NUM) values (DEP.SD_ORDRE,DEP.SD_MONTANT_HT,DEP.PCO_NUM);
     end if;
     -- pareil pour CAN_ID
     if (DEP.CAN_ID is not null) then
        insert into ACCORDS.REPART_SB_DEPENSE_C_ANALYTIQUE (SD_ORDRE,MONTANT_HT,CAN_ID) values (DEP.SD_ORDRE,DEP.SD_MONTANT_HT,DEP.CAN_ID);
     end if;
    end loop;
    close C_DEP;
  end transferer_reparts;
  
  end TRANSFERT_REPARTS;
  /


-- Ajout des attributs de date de creation et de responsable
ALTER TABLE ACCORDS.PROJET
 ADD (
    PERS_ID_RESPONSABLE NUMBER,
    PJT_DATE_CREATION DATE
 );
COMMENT ON COLUMN ACCORDS.PROJET.PERS_ID_RESPONSABLE IS 'Responsable du projet';
COMMENT ON COLUMN ACCORDS.PROJET.PJT_DATE_CREATION IS 'Date de creation du projet';

-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.AVENANT
 ADD (
    AVT_TVA_ID NUMBER
 );
ALTER TABLE ACCORDS.AVENANT ADD (
  CONSTRAINT FK_AVT_TVA 
 FOREIGN KEY (AVT_TVA_ID) 
 REFERENCES JEFY_ADMIN.TVA (TVA_ID));
 
-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.SB_DEPENSE
 ADD (
    TVA_ID NUMBER
 );
ALTER TABLE ACCORDS.SB_DEPENSE ADD (
  CONSTRAINT FK_SB_DEP_TVA 
 FOREIGN KEY (TVA_ID) 
 REFERENCES JEFY_ADMIN.TVA (TVA_ID));
 
-- Ajout d'un attribut TVA_ID cle primaire de JEFY_ADMIN.TVA
ALTER TABLE ACCORDS.SB_RECETTE
 ADD (
    TVA_ID NUMBER
 );
 ALTER TABLE ACCORDS.SB_RECETTE ADD (
  CONSTRAINT FK_SB_REC_TVA 
 FOREIGN KEY (TVA_ID) 
 REFERENCES JEFY_ADMIN.TVA (TVA_ID));
 
 -- Autoriser les NULL dans org_id
alter table ACCORDS.SB_DEPENSE MODIFY(ORG_ID  NULL);
alter table ACCORDS.SB_RECETTE MODIFY(ORG_ID  NULL);


-- Maj de  la version 
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (accords.VERSION_HISTO_SEQ.nextval,'2.1.0',to_date('20/01/2011','DD/MM/YYYY'),'Ajout TVA_ID, modifs Projet, ajout de reparts depense et recette');
commit;