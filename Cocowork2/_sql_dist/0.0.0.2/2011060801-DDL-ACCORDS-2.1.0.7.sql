SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

-- Correction vue convention_limitative en se basant sur les crédits bibasse
-- (vue v_suivi_depense_posit)
CREATE OR REPLACE FORCE VIEW "ACCORDS"."CONVENTION_LIMITATIVE" ("CON_ORDRE", "CL_CON_REFERENCE", "CON_REFERENCE_EXTERNE", "CON_OBJET_COURT", "EXE_ORDRE", "TCD_ORDRE", "ORG_ID", "CL_TYPE_MONTANT", "CL_MONTANT")
AS
  SELECT
    con.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000') AS cl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'DEPENSE'            AS CL_TYPE_MONTANT,
    SUM(vsdp.total_posit) AS cl_montant
  FROM ACCORDS.v_suivi_depense_posit vsdp,
    accords.contrat con,
    accords.avenant avt
  WHERE
  avt.con_ordre = con.con_ordre
  AND avt.avt_index           = 0
  AND avt.avt_mode_gest       = 1 -- ress. aff.
  HAVING SUM(vsdp.total_posit) <> 0
  GROUP BY con.con_ordre, con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000'), con.con_reference_externe, con.con_objet_court, vsdp.exe_ordre, vsdp.tcd_ordre, vsdp.org_id, 'DEPENSE'
  UNION
  SELECT
    con.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000') AS cl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'RECETTE'             AS CL_TYPE_MONTANT,
    SUM(vsdp.total_posit) AS cl_montant
  FROM ACCORDS.v_suivi_depense_posit vsdp,
    accords.contrat con,
    accords.avenant avt
  WHERE 
  avt.con_ordre = con.con_ordre
  AND avt.avt_index            = 0
  AND avt.avt_mode_gest        = 1 -- ress. aff.
  HAVING SUM(vsdp.total_posit) <> 0
  GROUP BY
    con.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000'),
    con.con_reference_externe,
    con.con_objet_court,
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'RECETTE';

-- Correction vue convention_non_limitative en se basant sur les crédits bibasse
-- (vue v_suivi_depense_posit)
CREATE OR REPLACE FORCE VIEW "ACCORDS"."CONVENTION_NON_LIMITATIVE" ("CON_ORDRE", "CNL_CON_REFERENCE", "CON_REFERENCE_EXTERNE", "CON_OBJET_COURT", "CNL_MODE_GESTION", "CNL_LIMITATIF", "EXE_ORDRE", "TCD_ORDRE", "ORG_ID", "CNL_TYPE_MONTANT", "CNL_MONTANT")
AS
  SELECT
    con.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000') AS cnl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court           AS cnl_mode_gestion,
    NVL(avt.avt_limitatif, 'NON') AS cnl_limitatif,
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'DEPENSE'            AS cnl_type_montant,
    SUM(vsdp.total_posit) AS cnl_montant
    /*,
    suivi_exec_depense.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo */
  FROM ACCORDS.v_suivi_depense_posit vsdp,
    accords.contrat con,
    accords.avenant avt,
    accords.mode_gestion mg
  WHERE 
  avt.con_ordre = con.con_ordre
  AND avt.avt_index      = 0
  AND avt.avt_mode_gest <> 1
  AND -- non RA
    avt.avt_mode_gest          = mg.mg_ordre
  HAVING SUM(vsdp.total_posit) <> 0
  group by con.con_ordre, con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000'), con.con_reference_externe, con.con_objet_court, mg.mg_libelle_court, NVL(avt.avt_limitatif, 'NON'), vsdp.exe_ordre, vsdp.tcd_ordre, vsdp.org_id, 'DEPENSE'
  UNION
  -- recette
  SELECT
    con.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000') AS cnl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court           AS cnl_mode_gestion,
    NVL(avt.avt_limitatif, 'NON') AS cnl_limitatif,
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'RECETTE'             AS cnl_type_montant,
    SUM(vsdp.total_posit) AS cnl_montant
    /*,
    suivi_exec_recette.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo */
  FROM accords.v_suivi_depense_posit vsdp,
    accords.tranche tra,
    accords.contrat con,
    accords.avenant avt,
    accords.mode_gestion mg
  WHERE
  avt.con_ordre = con.con_ordre
  AND avt.avt_index      = 0
  AND avt.avt_mode_gest <> 1
  AND -- autres modes gestion
    avt.avt_mode_gest           = mg.mg_ordre
  HAVING SUM(vsdp.total_posit) <> 0
  GROUP BY
    con.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000'),
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court,
    NVL(avt.avt_limitatif, 'NON'),
    vsdp.exe_ordre,
    vsdp.tcd_ordre,
    vsdp.org_id,
    'RECETTE';

-- Correction vue pour prise en compte des budgets validés uniquement
CREATE OR REPLACE FORCE VIEW "ACCORDS"."V_SUIVI_DEPENSE_POSIT" ("EXE_ORDRE", "CON_ORDRE", "ORG_ID", "ORG_PERE", "TCD_ORDRE", "TOTAL_POSIT")
AS
  SELECT pb.exe_ordre,
    tra.con_ordre,
    pb.org_id,
    org.org_pere,
    pbnl.tcd_ordre,
    SUM(pbnl.pvbnl_a_ouvrir)
  FROM accords.tranche tra,
    accords.tranche_prevision_budg tpb,
    jefy_budget.prevision_budg pb,
    jefy_admin.type_etat te,
    jefy_budget.prevision_budg_nat_lolf pbnl,
    jefy_admin.organ org
  WHERE tra.tra_ordre=tpb.tra_ordre
  AND tpb.pvbu_id    =pb.pvbu_id
  AND pb.pvbu_id     =pbnl.pvbu_id
  AND pb.org_id      =org.org_id
  AND pb.tyet_id = te.tyet_id
  AND te.tyet_libelle = 'VALIDE'
  GROUP BY pb.exe_ordre,
    tra.con_ordre,
    pb.org_id,
    org.org_pere,
    pbnl.tcd_ordre
  HAVING SUM(pbnl.pvbnl_a_ouvrir)>0;
    
-- Maj de  la version 
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (accords.VERSION_HISTO_SEQ.nextval,'2.1.0.7',to_date('08/06/2011','DD/MM/YYYY'),'Corrections vues CONVENTION_NON_LIMITATIVE, CONVENTION_LIMITATIVE et V_SUIVI_DEPENSE_POSIT');
commit;