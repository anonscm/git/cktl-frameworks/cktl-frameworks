--
-- Patch DDL de ACCORDS du 09/11/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.12
-- Date de publication : 09/11/2011
-- Auteur(s) : Alexis TUAL
-- Licence : CeCILL version 2
--
--

-- Suppression des enregistrements existants
-- delete from ACCORDS.AVENANT_DOCUMENT;
ALTER TABLE ACCORDS.AVENANT_DOCUMENT ADD DOC_ID NUMBER NOT NULL;
ALTER TABLE ACCORDS.AVENANT_DOCUMENT MODIFY(COU_NUMERO NUMBER NULL);
GRANT REFERENCES ON CKTL_GED.DOCUMENT TO ACCORDS;
ALTER TABLE ACCORDS.AVENANT_DOCUMENT ADD (CONSTRAINT FK_DOCUMENT FOREIGN KEY (DOC_ID) REFERENCES CKTL_GED.DOCUMENT (DOC_ID)  DEFERRABLE INITIALLY DEFERRED);

------------------------------------------------------------------------------------------------------------------------
--  Ajout de la clef étrangères DOC_ID
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.12',to_date('09/11/2011','DD/MM/YYYY'),
    'Ajout de la clef étrangère DOC_ID');
commit;
