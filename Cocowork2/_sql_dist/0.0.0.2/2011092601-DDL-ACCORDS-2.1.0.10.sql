--
-- Patch DDL de ACCORDS du 26/09/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.10
-- Date de publication : 26/09/2011
-- Auteur(s) : Alexis TUAL
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

CREATE OR REPLACE FORCE VIEW "ACCORDS"."V_SUIVI_DEPENSE_POSIT" ("EXE_ORDRE", "CON_ORDRE", "ORG_ID", "ORG_PERE", "TCD_ORDRE", "TOTAL_POSIT")
AS
  SELECT hcp.EXE_ORDRE,
    tra.CON_ORDRE,
    hcp.ORG_ID,
    org.ORG_PERE,
    hcp.TCD_ORDRE,
    SUM(hcp.HCP_MONTANT) AS TOTAL_POSIT
  FROM HISTO_CREDIT_POSITIONNE hcp,
    TRANCHE tra,
    jefy_admin.organ org
  WHERE hcp.TRA_ORDRE = tra.TRA_ORDRE
  AND tra.TRA_SUPPR   = 'N'
  AND hcp.org_id      = org.org_id
  GROUP BY hcp.EXE_ORDRE,
    tra.CON_ORDRE,
    hcp.ORG_ID,
    org.ORG_PERE,
    hcp.TCD_ORDRE
  HAVING SUM(hcp.HCP_MONTANT) > 0;

CREATE OR REPLACE FORCE VIEW "ACCORDS"."CONVENTION_LIMITATIVE" ("CON_ORDRE", "CL_CON_REFERENCE", "CON_REFERENCE_EXTERNE", "CON_OBJET_COURT", "EXE_ORDRE", "TCD_ORDRE", "ORG_ID", "CL_TYPE_MONTANT", "CL_MONTANT")
AS
  SELECT tra.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000') AS cl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    hcp.exe_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'DEPENSE'            AS CL_TYPE_MONTANT,
    SUM(hcp.hcp_montant) AS cl_montant
    /*,
    suivi_exec_depense.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo*/
  FROM accords.histo_credit_positionne hcp,
    accords.contrat con,
    accords.tranche tra,
    accords.avenant avt
  WHERE hcp.hcp_suppr         = 'N'
  AND hcp.tra_ordre           = tra.tra_ordre
  AND tra.tra_suppr           = 'N'
  AND tra.tra_valide          = 'O'
  AND tra.con_ordre           = con.con_ordre
  AND tra.con_ordre           = avt.con_ordre
  AND avt.avt_index           = 0
  AND avt.avt_mode_gest       = 1 -- ress. aff.
  HAVING SUM(hcp.hcp_montant) > 0
  GROUP BY tra.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000'),
    con.con_reference_externe,
    con.con_objet_court,
    hcp.exe_ordre,
    hcp.tra_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'DEPENSE'
  UNION
  SELECT tra.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000') AS cl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    hcp.exe_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'RECETTE'             AS CL_TYPE_MONTANT,
    SUM(hcp.hcpr_montant) AS cl_montant
    /*,
    suivi_exec_recette.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo */
  FROM accords.histo_credit_posit_rec hcp,
    accords.contrat con,
    accords.tranche tra,
    accords.avenant avt
  WHERE hcp.hcpr_suppr         = 'N'
  AND hcp.tra_ordre            = tra.tra_ordre
  AND tra.tra_suppr            = 'N'
  AND tra.tra_valide           = 'O'
  AND tra.con_ordre            = con.con_ordre
  AND tra.con_ordre            = avt.con_ordre
  AND avt.avt_index            = 0
  AND avt.avt_mode_gest        = 1 -- ress. aff.
  HAVING SUM(hcp.hcpr_montant) > 0
  GROUP BY tra.con_ordre,
    con.exe_ordre
    ||'-'
    ||TO_CHAR(con.con_index,'FM0000'),
    con.con_reference_externe,
    con.con_objet_court,
    hcp.exe_ordre,
    hcp.tra_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'RECETTE';

CREATE OR REPLACE FORCE VIEW "ACCORDS"."CONVENTION_NON_LIMITATIVE" ("CON_ORDRE", "CNL_CON_REFERENCE", "CON_REFERENCE_EXTERNE", "CON_OBJET_COURT", "CNL_MODE_GESTION", "CNL_LIMITATIF", "EXE_ORDRE", "TCD_ORDRE", "ORG_ID", "CNL_TYPE_MONTANT", "CNL_MONTANT")
AS
  SELECT tra.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000') AS cnl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court           AS cnl_mode_gestion,
    NVL(avt.avt_limitatif, 'NON') AS cnl_limitatif,
    hcp.exe_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'DEPENSE'            AS cnl_type_montant,
    SUM(hcp.hcp_montant) AS cnl_montant
    /*,
    suivi_exec_depense.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo */
  FROM accords.histo_credit_positionne hcp,
    accords.tranche tra,
    accords.contrat con,
    accords.avenant avt,
    accords.mode_gestion mg
  WHERE hcp.hcp_suppr    = 'N'
  AND hcp.tra_ordre      = tra.tra_ordre
  AND tra.tra_suppr      = 'N'
  AND tra.tra_valide     = 'O'
  AND tra.con_ordre      = con.con_ordre
  AND tra.con_ordre      = avt.con_ordre
  AND avt.avt_index      = 0
  AND avt.avt_mode_gest <> 1
  AND -- non RA
    avt.avt_mode_gest          = mg.mg_ordre
  HAVING SUM(hcp.hcp_montant) <> 0
  GROUP BY tra.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000'),
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court,
    NVL(avt.avt_limitatif, 'NON'),
    hcp.exe_ordre,
    hcp.tra_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'DEPENSE'
  UNION
  -- recette
  SELECT tra.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000') AS cnl_con_reference,
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court           AS cnl_mode_gestion,
    NVL(avt.avt_limitatif, 'NON') AS cnl_limitatif,
    hcp.exe_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'RECETTE'             AS cnl_type_montant,
    SUM(hcp.hcpr_montant) AS cnl_montant
    /*,
    suivi_exec_recette.get_total_dispo(hcp.exe_ordre, tra.con_ordre, hcp.tcd_ordre, hcp.org_id) as dispo */
  FROM accords.histo_credit_posit_rec hcp,
    accords.tranche tra,
    accords.contrat con,
    accords.avenant avt,
    accords.mode_gestion mg
  WHERE hcp.hcpr_suppr   = 'N'
  AND hcp.tra_ordre      = tra.tra_ordre
  AND tra.tra_suppr      = 'N'
  AND tra.tra_valide     = 'O'
  AND tra.con_ordre      = con.con_ordre
  AND tra.con_ordre      = avt.con_ordre
  AND avt.avt_index      = 0
  AND avt.avt_mode_gest <> 1
  AND -- autres modes gestion
    avt.avt_mode_gest           = mg.mg_ordre
  HAVING SUM(hcp.hcpr_montant) <> 0
  GROUP BY tra.con_ordre,
    con.exe_ordre
    ||' -'
    ||TO_CHAR(con.con_index,'0000'),
    con.con_reference_externe,
    con.con_objet_court,
    mg.mg_libelle_court,
    NVL(avt.avt_limitatif, 'NON'),
    hcp.exe_ordre,
    hcp.tra_ordre,
    hcp.tcd_ordre,
    hcp.org_id,
    'RECETTE';

------------------------------------------------------------------------------------------------------------------------
--  Maj de la version null sur la colonne ACCORDS.HISTO_CREDIT_POSITIONNE.PCO_NUM
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.10',to_date('26/09/2011','DD/MM/YYYY'),
    'Reprise des vues et procedures de suivi budgetaire coconuts');
commit;