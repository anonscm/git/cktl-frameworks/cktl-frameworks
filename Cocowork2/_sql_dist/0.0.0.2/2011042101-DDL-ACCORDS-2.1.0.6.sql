SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

CREATE OR REPLACE FORCE VIEW accords.v_suivi_depense_posit(exe_ordre, con_ordre, org_id, org_pere, tcd_ordre, total_posit)
AS
  select pb.exe_ordre, tra.con_ordre, pb.org_id, org.org_pere, pbnl.tcd_ordre, sum(pbnl.pvbnl_a_ouvrir)
  from accords.tranche tra, accords.tranche_prevision_budg tpb, jefy_budget.prevision_budg pb, jefy_budget.prevision_budg_nat_lolf pbnl, jefy_admin.organ org
  where tra.tra_ordre=tpb.tra_ordre and tpb.pvbu_id=pb.pvbu_id and pb.pvbu_id=pbnl.pvbu_id and pb.org_id=org.org_id
  group by pb.exe_ordre, tra.con_ordre, pb.org_id, org.org_pere, pbnl.tcd_ordre
  having sum(pbnl.pvbnl_a_ouvrir)>0;

    
-- Maj de  la version 
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (accords.VERSION_HISTO_SEQ.nextval,'2.1.0.6',to_date('21/04/2011','DD/MM/YYYY'),'Ajout de la vue accords.v_suivi_depense_posit');
commit;