--
-- Patch DDL de ACCORDS du 18/01/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.2.0.0
-- Date de publication : 18/01/2013
-- Auteur(s) : Julien LAFOURCADE
-- Licence : CeCILL version 2
--
--

ALTER TABLE ACCORDS.TRANCHE ADD (
	REPORT_N_MOINS_1 NUMBER(20,2) DEFAULT 0 NOT NULL,
	REPORT_N_PLUS_1 NUMBER(20,2) DEFAULT 0 NOT NULL
);


ALTER TABLE ACCORDS.CONTRAT MODIFY (CON_OBJET VARCHAR2(500));

create or replace view accords.v_hist_cred_pod_eng_budget as
select hcp.hcp_ordre, eb.eng_id from ACCORDS.histo_credit_positionne hcp
  inner join JEFY_DEPENSE.engage_budget eb 
    on hcp.org_id = eb.org_id 
    and hcp.exe_ordre = eb.exe_ordre
    and hcp.tcd_ordre = eb.tcd_ordre
    and hcp.hcp_suppr <> 'O';
/


create or replace TRIGGER accords.v_hist_cred_pod_eng_budget_del
INSTEAD OF DELETE ON accords.v_hist_cred_pod_eng_budget
REFERENCING old as o
FOR EACH ROW
BEGIN

  null;

END;
/

declare
   c int;
begin
   select count(*) into c from user_tables where table_name = upper('AXES_STRATEGIQUES');
   if c = 0 then
      execute immediate '
        CREATE TABLE GRHUM.AXES_STRATEGIQUES (
          ID NUMBER NOT NULL,
          LIBELLE VARCHAR(100) NOT NULL,
          PERS_ID_CREATION NUMBER NOT NULL, 
          PERS_ID_MODIFICATION NUMBER,
          D_CREATION DATE NOT NULL, 
          D_MODIFICATION DATE,
          CONSTRAINT PK_AXE_STRATEGIQUE PRIMARY KEY (ID) USING INDEX TABLESPACE INDX_GRHUM
        )
      ';
      execute immediate '
        CREATE SEQUENCE GRHUM.AXES_STRATEGIQUES_SEQ INCREMENT BY 1 START WITH 1 NOCACHE  NOORDER  NOCYCLE
      ';
   end if;
end;
/

COMMENT ON TABLE GRHUM.AXES_STRATEGIQUES IS 'Table des axes strategiques de l''etablissement';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.ID IS 'Identifiant de l''axe';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.LIBELLE IS 'Libelle de l''axe';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.PERS_ID_CREATION IS 'persid du createur';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.PERS_ID_MODIFICATION IS 'persid du modificateur.';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.D_CREATION IS 'date de creation';
COMMENT ON COLUMN GRHUM.AXES_STRATEGIQUES.D_MODIFICATION IS 'date de modification';

 
 CREATE TABLE ACCORDS.CONTRAT_AXE_STRATEGIQUES (
 	CON_ORDRE NUMBER NOT NULL,
 	AS_ID NUMBER NOT NULL
 );
 
 
COMMENT ON TABLE ACCORDS.CONTRAT_AXE_STRATEGIQUES IS 'Table répoartition entre les contrats et les axes stratégiques';
COMMENT ON COLUMN ACCORDS.CONTRAT_AXE_STRATEGIQUES.CON_ORDRE IS 'Identifiant du contrat';
COMMENT ON COLUMN ACCORDS.CONTRAT_AXE_STRATEGIQUES.AS_ID IS 'Identifiant de l''axe';


ALTER TABLE ACCORDS.CONTRAT_AXE_STRATEGIQUES ADD (
 CONSTRAINT PK_CONTRAT_AXE_STRATEGIQUES PRIMARY KEY (CON_ORDRE, AS_ID)
 USING INDEX TABLESPACE GFC );

 ALTER TABLE ACCORDS.CONTRAT_AXE_STRATEGIQUES ADD (
	CONSTRAINT FK_CONT_AXE_STRAT_CONTRAT 
	FOREIGN KEY (CON_ORDRE) 
	REFERENCES ACCORDS.CONTRAT (CON_ORDRE)
); 
 
GRANT REFERENCES ON GRHUM.AXES_STRATEGIQUES TO ACCORDS;
GRANT SELECT ON GRHUM.AXES_STRATEGIQUES TO ACCORDS;
 
ALTER TABLE ACCORDS.CONTRAT_AXE_STRATEGIQUES ADD (
	CONSTRAINT FK_CONT_AXE_STRAT_AXE 
	FOREIGN KEY (AS_ID) 
	REFERENCES GRHUM.AXES_STRATEGIQUES (ID)
);  
 

create or replace
package accords.SUIVI_EXEC_DEPENSE is 
  
  --------------------------------------------------------------------------------------------
  -- Fournit le total depense sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  procedure get_total_depense( 
    EXEORDRE   IN    integer, 
    CONORDRE   IN    integer, 
    TCDORDRE   IN    integer, 
    ORGID      IN    integer, 
    TOTAL      OUT   number) ;

  --------------------------------------------------------------------------------------------
  -- Fournit le total liquide sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  procedure get_total_liquide( EXEORDRE   IN    integer, 
                               CONORDRE   IN    integer, 
                               TCDORDRE   IN    integer, 
                               ORGID      IN    integer,
                               TOTAL      OUT   number) ;

  --------------------------------------------------------------------------------------------
  -- Fournit le total restant engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  procedure get_total_reste_engage( EXEORDRE   IN    integer, 
                                    CONORDRE   IN    integer, 
                                    TCDORDRE   IN    integer, 
                                    ORGID      IN    integer,
                                    TOTAL      OUT   number) ;

  --------------------------------------------------------------------------------------------
  -- Fournit le total positionne pour la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe si la tranche correspondante a ete validee ou non.
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  procedure get_total_posit( 
    EXEORDRE   IN    integer, 
    CONORDRE   IN    integer, 
    TCDORDRE   IN    integer, 
    ORGID      IN    integer,
    TOTAL      OUT   number) ;

  --------------------------------------------------------------------------------------------
  -- Fournit le total disponible sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (reste engage + total liquide).
  --------------------------------------------------------------------------------------------
  procedure get_total_dispo( 
    EXEORDRE   IN    integer, 
    CONORDRE   IN    integer, 
    TCDORDRE   IN    integer, 
    ORGID      IN    integer,
    TOTAL      OUT   number) ;

  --------------------------------------------------------------------------------------------
  -- Fournit le total disponible reel sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  procedure get_total_dispo_reel(  
    EXEORDRE   IN    integer, 
    CONORDRE   IN    integer, 
    TCDORDRE   IN    integer, 
    ORGID      IN    integer,
    TOTAL      OUT   number) ;
  
  ------------------------------------------------------------------------------------
  -- Procedure qui verifie le montant disponible pour une convention, un exercice, 
  -- un type de credit et une LB. Cf. get_total_dispo().
  -- 
  -- La tranche correspondante doit avoir ete validee.
  -- 
  -- Pour une convention limitative (ressources affectées) ou une convention geree 
  -- en "crédits limitatifs", cette procedure lance un RAISE_APPLICATION_ERROR pour 
  -- annuler la transaction en cours si le disponible est negatif. 
  -- Sinon, la procedure va jusqu'a son terme normalement.
  ------------------------------------------------------------------------------------
  procedure verif_disponible( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) ;
  
  
  
  

  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention, l'exercice et la ligne budgetaire
  -- specifies (pour tous les types de credit).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number ;
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention et l'exercice 
  -- specifies (tous types de credit et LB confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;
    
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;
    
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention, l'exercice et la ligne budgetaire
  -- specifie (tous types de credit confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number ;
    
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total restant engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total restant engage sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer,
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total restant engage sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer,
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total propose pour la convention et l'exercice specifies
  -- (toutes LB confondues).
  --
  -- Peu importe si la tranche correspondante a ete validee ou non.
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_propo( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe si la tranche correspondante a ete validee ou non.
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_posit( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- Peu importe si la tranche correspondante a ete validee ou non.
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_posit( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- Peu importe si la tranche correspondante a ete validee ou non.
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_posit( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total budgetaire disponible sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (càd total restant engage + total liquide).
  --------------------------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total budgetaire disponible sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (càd total restant engage + total liquide).
  --------------------------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total budgetaire disponible sur la convention et l'exercice 
  -- specifies (tous types de credit et LB confondus).
  --
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (càd total restant engage + total liquide).
  --------------------------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total disponible reel sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  -- 
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  function get_total_dispo_reel(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total disponible reel sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  -- 
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  function get_total_dispo_reel(
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number ;

  --------------------------------------------------------------------------------------------
  -- Calcule le total disponible reel sur la convention et l'exercice specifies
  -- (tous types de credit confondus).
  -- 
  -- La tranche correspondante doit avoir ete validee.
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  function get_total_dispo_reel(
    EXEORDRE    integer, 
    CONORDRE    integer) return number ;
    
    
end SUIVI_EXEC_DEPENSE;
/





create or replace
package body accords.SUIVI_EXEC_DEPENSE is
    
  --------------------------------------------------------------------------------------------
  -- Fournit le total depense sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  procedure get_total_depense( EXEORDRE   IN    integer, 
                               CONORDRE   IN    integer, 
                               TCDORDRE   IN    integer, 
                               ORGID      IN    integer,
                               TOTAL      OUT   number) 
  is
  begin
    
    if (TCDORDRE is null and ORGID is null) then
      TOTAL := get_total_depense(EXEORDRE, CONORDRE);
    elsif (TCDORDRE is null) then
      TOTAL := get_total_depense(EXEORDRE, CONORDRE, ORGID);
    else
      TOTAL := get_total_depense(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    end if;
    
  end get_total_depense;

  --------------------------------------------------------------------------------------------
  -- Fournit le total liquide sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  procedure get_total_liquide( EXEORDRE   IN    integer, 
                               CONORDRE   IN    integer, 
                               TCDORDRE   IN    integer, 
                               ORGID      IN    integer,
                               TOTAL      OUT   number) 
  is
  begin
    
    if (TCDORDRE is null and ORGID is null) then
      TOTAL := get_total_liquide(EXEORDRE, CONORDRE);
    elsif (TCDORDRE is null) then
      TOTAL := get_total_liquide(EXEORDRE, CONORDRE, ORGID);
    else
      TOTAL := get_total_liquide(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    end if;
    
  end get_total_liquide;

  --------------------------------------------------------------------------------------------
  -- Fournit le total restant engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  procedure get_total_reste_engage( EXEORDRE   IN    integer, 
                                    CONORDRE   IN    integer, 
                                    TCDORDRE   IN    integer, 
                                    ORGID      IN    integer,
                                    TOTAL      OUT   number) 
  is
  begin
    
    if (TCDORDRE is null and ORGID is null) then
      TOTAL := get_total_reste_engage(EXEORDRE, CONORDRE);
    elsif (TCDORDRE is null) then
      TOTAL := get_total_reste_engage(EXEORDRE, CONORDRE, ORGID);
    else
      TOTAL := get_total_reste_engage(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    end if;
    
  end get_total_reste_engage;


  --------------------------------------------------------------------------------------------
  -- Fournit le total positionne pour la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  procedure get_total_posit( EXEORDRE   IN    integer, 
                             CONORDRE   IN    integer, 
                             TCDORDRE   IN    integer, 
                             ORGID      IN    integer, 
                             TOTAL      OUT   number) 
  is
  begin
  
    TOTAL := get_total_posit(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    
  end get_total_posit;


  --------------------------------------------------------------------------------------------
  -- Fournit le total disponible sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (reste engage + total liquide).
  --------------------------------------------------------------------------------------------
  procedure get_total_dispo( EXEORDRE   IN    integer, 
                             CONORDRE   IN    integer, 
                             TCDORDRE   IN    integer, 
                             ORGID      IN    integer,
                             TOTAL      OUT   number) 
  is
  begin
  
    TOTAL := get_total_dispo(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    
  end get_total_dispo;


  --------------------------------------------------------------------------------------------
  -- Fournit le total disponible reel sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  procedure get_total_dispo_reel(  EXEORDRE   IN    integer, 
                                   CONORDRE   IN    integer, 
                                   TCDORDRE   IN    integer, 
                                   ORGID      IN    integer,
                                   TOTAL      OUT   number) 
  is
  begin
  
    TOTAL := get_total_dispo_reel(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    
  end get_total_dispo_reel;
  
  
  ------------------------------------------------------------------------------------
  -- Procedure qui verifie le montant disponible pour une convention, un exercice, 
  -- un type de credit et une LB. Cf. get_total_dispo().
  -- 
  -- Pour une convention limitative (ressources affectées) ou une convention geree 
  -- en "crédits limitatifs", cette procedure lance un RAISE_APPLICATION_ERROR pour 
  -- annuler la transaction en cours si le disponible est negatif. 
  -- Sinon, la procedure va jusqu'a son terme normalement.
  ------------------------------------------------------------------------------------
  procedure verif_disponible(  EXEORDRE    integer, 
                               CONORDRE    integer, 
                               TCDORDRE    integer, 
                               ORGID       integer)   
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    LIMITATIF_OUI accords.avenant.AVT_LIMITATIF%type := 'OUI';
    
    nb integer;
    mgordre integer;
    avtlimitatif accords.avenant.AVT_LIMITATIF%type;
    conv varchar2(50);
    lbstr varchar2(200);
    
    sumPositionne number := 0;
    sumDepense number := 0;
    dispo number := 0;
  
	REPORTNPLUS1 number := 0;

  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (TCDORDRE is null) then
      RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : TCDORDRE');
    end if;
    if (ORGID is null) then
      RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
    
    
    -- recup du numero de la convention (du type: 2007-98)
    select exe_ordre||'-'||con_index into conv 
    from accords.contrat 
    where con_ordre = CONORDRE;
    -- recup du nom de la LB (du type: 900 DSI FONCT)
    select replace(trim(org_ub||' '||org_cr||' '||org_souscr),' ','-') into lbstr 
    from JEFY_ADMIN.ORGAN 
    where org_id = ORGID;
    
    
    sumPositionne :=  get_total_posit(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    sumDepense :=     get_total_depense(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
    dispo := sumPositionne - sumDepense;
    
    dbms_output.put_line('sumPositionne = '||sumPositionne);
    dbms_output.put_line('sumDepense = '||sumDepense);
    dbms_output.put_line('dispo = '||dispo);
    
    
    -- mode de gestion de la convention
    select avt_mode_gest into mgordre
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    -- limitativite des credits
    select avt_limitatif into avtlimitatif
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
  	select report_n_plus_1 into REPORTNPLUS1 from accords.tranche where con_ordre = CONORDRE and exe_ordre = EXEORDRE;
    if(REPORTNPLUS1 > 0) then 
    	RAISE_APPLICATION_ERROR(
	      -20001, 
	      'Les crédits de la tranche '|| EXEORDRE || ' ont été reportés' ||
	      ' pour la convention '||conv);
	end if;
    -- verification disponible
    -- pour les RA et CL, pas de depassement possible
    if (mgordre = RESSOURCE_AFFECTEE OR avtlimitatif = LIMITATIF_OUI) then
    
      if (dispo < 0) then 
        RAISE_APPLICATION_ERROR(
          -20001, 
          'Le montant restant engagé + montant liquidé ('||
          trim(to_char(sumDepense,'999999999999999999d00'))||
          ') dépasse de '||
          trim(to_char(abs(dispo),'999999999999999999d00'))||
          ' le total des crédits positionnés ('||
          trim(to_char(sumPositionne,'999999999999999999d00'))||
          ') pour la convention '||conv);
      end if;
      
    end if;
    
  end verif_disponible;
  
  
  
  
  
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention et l'exercice 
  -- specifies (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumLiquide number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans DEPENSE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
    
      select nvl(sum(DEP_MONTANT_BUDGETAIRE),0) into sumLiquide
      from JEFY_DEPENSE.DEPENSE_BUDGET dep, JEFY_DEPENSE.ENGAGE_BUDGET eng
      where dep.exe_ordre = EXEORDRE and dep.eng_id = eng.eng_id
      and eng.org_id in (
        select distinct org_id from accords.v_suivi_depense_posit 
        where exe_ordre = EXEORDRE and con_ordre = CONORDRE
      );
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(dcon_montant_budgetaire),0) into sumLiquide 
      from accords.V_SUIVI_DEP 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE;
  
    --end if;
        
    return sumLiquide;
  
  end get_total_liquide;
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention, l'exercice et la ligne budgetaire
  -- specifies (pour tous les types de credit).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumLiquide number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans DEPENSE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
    
      select nvl(sum(DEP_MONTANT_BUDGETAIRE),0) into sumLiquide
      from JEFY_DEPENSE.DEPENSE_BUDGET dep, JEFY_DEPENSE.ENGAGE_BUDGET eng
      where dep.exe_ordre = EXEORDRE and org_id = ORGID and dep.eng_id = eng.eng_id;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(dcon_montant_budgetaire),0) into sumLiquide 
      from accords.V_SUIVI_DEP 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and ORG_ID = ORGID;
  
    --end if;
        
    return sumLiquide;
  
  end get_total_liquide;
  
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total liquide sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_liquide( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
    taux_prorata integer;
    sumLiquide number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (TCDORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : TCDORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans DEPENSE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
    
      select nvl(sum(DEP_MONTANT_BUDGETAIRE),0) into sumLiquide
      from JEFY_DEPENSE.DEPENSE_BUDGET dep, JEFY_DEPENSE.ENGAGE_BUDGET eng
      where dep.exe_ordre = EXEORDRE and org_id = ORGID and dep.eng_id = eng.eng_id and tcd_ordre = TCDORDRE;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
    --  select nvl(sum(dcon_montant_budgetaire),0) into sumLiquide 
    --  from accords.V_SUIVI_DEP 
    --  where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID;
  

	select count(*) into nb from jefy_admin.organ_prorata op
      inner join jefy_admin.taux_prorata tp on op.tap_id = tp.tap_id where op.org_id = ORGID and exe_ordre = exeordre and orp_priorite = 0;
	if(nb > 0) then 
    	select tp.tap_taux into taux_prorata from jefy_admin.organ_prorata op
      		inner join jefy_admin.taux_prorata tp on op.tap_id = tp.tap_id where op.org_id = ORGID and exe_ordre = exeordre and orp_priorite = 0;
    else 
		RAISE_APPLICATION_ERROR(-20001, 'Impossible de trouver le taux de prorata pour l''org_id ' || ORGID || ' et l''exercice ' || exeordre);
	end if;
    
    if(taux_prorata = 100) then 
      -- si taux prorata 100 alors CR HT
      select nvl(sum(DCON_HT_SAISIE),0) into sumLiquide 
        from accords.V_SUIVI_DEP 
        where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID;
    
    else
      -- si taux prorata 0 alors CR TTC
      select nvl(sum(DCON_TTC_SAISIE),0) into sumLiquide 
        from accords.V_SUIVI_DEP 
        where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID;

      
    end if;
        
    return sumLiquide;
  
  end get_total_liquide;
  
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des sommes restant engagees.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumRestantEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (TCDORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : TCDORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if; 
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then 
    
      select nvl(sum(ENG_MONTANT_BUDGETAIRE_RESTE),0) into sumRestantEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id = ORGID and tcd_ordre = TCDORDRE;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(econ_montant_budgetaire_reste),0) into sumRestantEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID; 
  
    --end if;
        
    return sumRestantEngage;
    
  end get_total_reste_engage;

  --------------------------------------------------------------------------------------------
  -- Calcule le total restant engage sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer,
    ORGID       integer) return number
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumRestantEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if; 
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then 
    
      select nvl(sum(ENG_MONTANT_BUDGETAIRE_RESTE),0) into sumRestantEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id = ORGID;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(econ_montant_budgetaire_reste),0) into sumRestantEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and ORG_ID = ORGID; 
  
    --end if;
        
    return sumRestantEngage;
    
  end get_total_reste_engage;

  --------------------------------------------------------------------------------------------
  -- Calcule le total restant engage sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des restes engages.
  --------------------------------------------------------------------------------------------
  function get_total_reste_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer) return number
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumRestantEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then 
    
      select nvl(sum(ENG_MONTANT_BUDGETAIRE_RESTE),0) into sumRestantEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id in (
        select distinct org_id from accords.v_suivi_depense_posit 
        where exe_ordre = EXEORDRE and con_ordre = CONORDRE
      );
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(econ_montant_budgetaire_reste),0) into sumRestantEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE; 
  
    --end if;
        
    return sumRestantEngage;
    
  end get_total_reste_engage;
  
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (TCDORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : TCDORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
      
      select nvl(sum(ENG_MONTANT_BUDGETAIRE),0) into sumEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id = ORGID and tcd_ordre = TCDORDRE;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(econ_montant_budgetaire),0) into sumEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID; 
  
    --end if;
    
    return sumEngage;

  end get_total_engage;
    
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention, l'exercice et la ligne budgetaire
  -- specifie (tous types de credit confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
      
      select nvl(sum(ENG_MONTANT_BUDGETAIRE),0) into sumEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id = ORGID;
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
    
      select nvl(sum(econ_montant_budgetaire),0) into sumEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and ORG_ID = ORGID;
  
    --end if;
    
    return sumEngage;

  end get_total_engage;
    
  --------------------------------------------------------------------------------------------
  -- Calcule le total engage sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme des engagements.
  --------------------------------------------------------------------------------------------
  function get_total_engage(  
    EXEORDRE    integer, 
    CONORDRE    integer) return number 
  is
  
    RESSOURCE_AFFECTEE integer := 1;
    nb integer;
  
    sumEngage number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    
    /*
    -- recup mode de gestion de la convention
    select avt_mode_gest into nb
    from accords.avenant avt
    where avt.con_ordre = CONORDRE and avt_index = 0;
    
    ---------------------------------------------------------------------------------------------------
    -- pour les RA : rien dans ENGAGE_CTRL_CONVENTION !
    -- le lien est l'ORG_ID
    ---------------------------------------------------------------------------------------------------
    if (nb = RESSOURCE_AFFECTEE) then
      
      select nvl(sum(ENG_MONTANT_BUDGETAIRE),0) into sumEngage
      from JEFY_DEPENSE.ENGAGE_BUDGET
      where exe_ordre = EXEORDRE and org_id in (
        select distinct org_id from accords.v_suivi_depense_posit 
        where exe_ordre = EXEORDRE and con_ordre = CONORDRE
      );
      
    ---------------------------------------------------------------------------------------------------
    -- pour les non RA, on peut utiliser ENGAGE_CTRL_CONVENTION et DEPENSE_CTRL_CONVENTION
    ---------------------------------------------------------------------------------------------------
    else */
      
      select nvl(sum(econ_montant_budgetaire),0) into sumEngage 
      from accords.V_SUIVI_ENG 
      where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE;
      
    --end if;
    
    return sumEngage;
    
  end get_total_engage;
  

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number 
  is
    
    sumDepense number := 0;
    
  begin
    
    sumDepense :=
      get_total_reste_engage(EXEORDRE, CONORDRE, TCDORDRE, ORGID) +
      get_total_liquide(EXEORDRE, CONORDRE, TCDORDRE, ORGID);
        
    return sumDepense;
    
  end get_total_depense;

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer,
    ORGID       integer) return number
  is
    
    sumDepense number := 0;
    
  begin
    
    sumDepense :=
      get_total_reste_engage(EXEORDRE, CONORDRE, ORGID) +
      get_total_liquide(EXEORDRE, CONORDRE, ORGID);
        
    return sumDepense;
    
  end get_total_depense;

  --------------------------------------------------------------------------------------------
  -- Calcule le total depense sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  -- C'est la somme du restant engage et des liquidations.
  --------------------------------------------------------------------------------------------
  function get_total_depense( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number
  is
    
    sumDepense number := 0;
    
  begin
    
    sumDepense :=
      get_total_reste_engage(EXEORDRE, CONORDRE) +
      get_total_liquide(EXEORDRE, CONORDRE);
        
    return sumDepense;
    
  end get_total_depense;
  
  

  --------------------------------------------------------------------------------------------
  -- Calcule le total propose pour la convention et l'exercice specifies
  -- (toutes LB confondues).
  -- 
  -- Peu importe le mode de gestion de la convention. 
  -- 
  --------------------------------------------------------------------------------------------
  function get_total_propo( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number 
  is
  
    nb integer;
    sumPropo number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
  
  
    select nvl(sum(TOTAL_PROPO),0) into sumPropo
    from accords.V_SUIVI_DEPENSE_PROPO
    where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE;
    
    return sumPropo;
    
  end get_total_propo;
  
  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention, l'exercice, la ligne budgetaire
  -- et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  --------------------------------------------------------------------------------------------
  function get_total_posit(   
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number 
  is
  
    nb integer;
    sumPositionne number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (TCDORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : TCDORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
  
  
    select nvl(sum(TOTAL_POSIT),0) into sumPositionne
    from accords.V_SUIVI_DEPENSE_POSIT
    where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and TCD_ORDRE = TCDORDRE and ORG_ID = ORGID;
    
    return sumPositionne;
    
  end get_total_posit;

  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_posit( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number 
  is
  
    nb integer;
    sumPositionne number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
    if (ORGID is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : ORGID');
    end if;
  
  
    select nvl(sum(TOTAL_POSIT),0) into sumPositionne
    from accords.V_SUIVI_DEPENSE_POSIT
    where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE and ORG_ID = ORGID;
    
    return sumPositionne;
    
  end get_total_posit;

  --------------------------------------------------------------------------------------------
  -- Calcule le total positionne pour la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  --
  --------------------------------------------------------------------------------------------
  function get_total_posit( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number 
  is
  
    nb integer;
    sumPositionne number := 0;
  
  begin
  
    -- verif presence des arguments obligatoires
    if (EXEORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : EXEORDRE');
    end if;
    if (CONORDRE is null) then
      return null;--RAISE_APPLICATION_ERROR(-20001, 'Argument manquant : CONORDRE');
    end if;
  
  
    select nvl(sum(TOTAL_POSIT),0) into sumPositionne
    from accords.V_SUIVI_DEPENSE_POSIT
    where CON_ORDRE = CONORDRE and EXE_ORDRE = EXEORDRE;
    
    return sumPositionne;
    
  end get_total_posit;
  
  
  
  -------------------------------------------------------------------------
  -- Calcule le total disponible sur la convention, l'exercice, la ligne 
  -- budgetaire et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (reste engage + total liquide).
  ----------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    TCDORDRE    integer, 
    ORGID       integer) return number 
  is
    
    dispo number;
    
  begin 
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE, TCDORDRE, ORGID) - 
      get_total_depense(EXEORDRE, CONORDRE, TCDORDRE, ORGID); 
    
    return dispo;
    
  end get_total_dispo;

  --------------------------------------------------------------------------------------------
  -- Calcule le total budgetaire disponible sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (càd total restant engage + total liquide).
  --------------------------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number
  is
    
    dispo number;
    
  begin 
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE, ORGID) - 
      get_total_depense(EXEORDRE, CONORDRE, ORGID); 
    
    return dispo;
    
  end get_total_dispo;

  --------------------------------------------------------------------------------------------
  -- Calcule le total budgetaire disponible sur la convention et l'exercice 
  -- specifies (tous types de credit et LB confondus).
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (càd total restant engage + total liquide).
  --------------------------------------------------------------------------------------------
  function get_total_dispo( 
    EXEORDRE    integer, 
    CONORDRE    integer) return number
  is
    
    dispo number;
    
  begin 
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE) - 
      get_total_depense(EXEORDRE, CONORDRE); 
    
    return dispo;
    
  end get_total_dispo;
  
  
  
  -------------------------------------------------------------------------
  -- Calcule le total disponible sur la convention, l'exercice, la ligne 
  -- budgetaire et le type de credit specifies.
  --
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total depense (reste engage + total liquide).
  ----------------------------------------------------------------------------
  function get_total_dispo_reel(  EXEORDRE    integer, 
                                  CONORDRE    integer, 
                                  TCDORDRE    integer, 
                                  ORGID       integer) return number 
  is
    
    dispo number;
  
  begin  
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE, TCDORDRE, ORGID) -
      get_total_liquide(EXEORDRE, CONORDRE, TCDORDRE, ORGID);    
    
    return dispo;
    
  end get_total_dispo_reel;

  --------------------------------------------------------------------------------------------
  -- Calcule le total disponible reel sur la convention, l'exercice et la ligne budgetaire
  -- specifies (tous types de credit confondus).
  -- 
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  function get_total_dispo_reel(
    EXEORDRE    integer, 
    CONORDRE    integer, 
    ORGID       integer) return number 
  is
    
    dispo number;
  
  begin  
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE, ORGID) -
      get_total_liquide(EXEORDRE, CONORDRE, ORGID);    
    
    return dispo;
    
  end get_total_dispo_reel;

  --------------------------------------------------------------------------------------------
  -- Calcule le total disponible reel sur la convention et l'exercice specifies
  -- (tous types de credit et LB confondus).
  -- 
  -- Peu importe le mode de gestion de la convention.
  -- 
  -- C'est la difference entre le total positionne dans Coconut et le 
  -- total liquide.
  --------------------------------------------------------------------------------------------
  function get_total_dispo_reel(
    EXEORDRE    integer, 
    CONORDRE    integer) return number
  is
    
    dispo number;
  
  begin  
    
    dispo := 
      get_total_posit(EXEORDRE, CONORDRE) -
      nvl(get_total_liquide(EXEORDRE, CONORDRE), 0);    
    
    return dispo;
    
  end get_total_dispo_reel;
  
  
/*
  begin 
    suivi_exec.VERIF_disponible(2007, 3905, 103, 5000102); 
  exception
    when others then
      dbms_output.put_line('Erreur : '||SQLERRM);
  end;
*/
  
end SUIVI_EXEC_DEPENSE;
/


