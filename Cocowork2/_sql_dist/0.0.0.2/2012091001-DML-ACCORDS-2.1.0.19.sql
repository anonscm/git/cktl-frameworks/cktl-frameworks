--
-- Patch DML de ACCORDS du 27/08/2012 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.19
-- Date de publication : 27/08/2012
-- Auteur(s) : Julien LAFOURCADE
-- Licence : CeCILL version 2
--
--
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.19',to_date('29/06/2012','DD/MM/YYYY'),
    'MAJ DU PACKAGE ACCORDS.TRANSFERT_EVENEMENT');
commit;