CREATE TABLE ACCORDS.AVENANT_DOM_SCIENT
(
	AVT_ORDRE NUMBER NOT NULL,
	DS_ORDRE NUMBER NOT NULL,
	ADS_ID NUMBER NOT NULL, 
	CONSTRAINT DOMAINE_SCIENTIFIQUE_PK PRIMARY KEY (ADS_ID) ENABLE,
	CONSTRAINT FK_ADS_AVENANT FOREIGN KEY (AVT_ORDRE) REFERENCES ACCORDS.AVENANT (AVT_ORDRE) DEFERRABLE INITIALLY DEFERRED ENABLE, 
	CONSTRAINT FK_ADS_DS FOREIGN KEY (DS_ORDRE) REFERENCES GRHUM.DOMAINE_SCIENTIFIQUE (DS_ORDRE) DEFERRABLE INITIALLY DEFERRED ENABLE
);

CREATE SEQUENCE ACCORDS.AVENANT_DOM_SCIENT_SEQ INCREMENT BY 1 START WITH 1 MINVALUE 1 NOCACHE;

ALTER TABLE ACCORDS.CONTRAT
ADD (CON_DUREE_MOIS NUMBER );

ALTER TABLE ACCORDS.AVENANT
ADD (AVT_MONTANT_GLOBAL NUMBER );


CREATE OR REPLACE PROCEDURE accords.migrer_domaines_scientifiques
is
TYPE avenant_type IS RECORD (avt_ordre accords.avenant.avt_ordre%type, ds_ordre accords.avenant.ds_ordre%type);
avenant avenant_type;
CURSOR C1 is select avt_ordre, ds_ordre from accords.avenant where ds_ordre is not null;
begin
 open C1;
 loop
  fetch C1 into avenant;
  exit when C1%notfound;

  insert into accords.avenant_dom_scient(ads_id, avt_ordre, ds_ordre) values (accords.avenant_dom_scient_seq.nextval, avenant.avt_ordre, avenant.ds_ordre);

 end loop;
 close C1;
end;
/