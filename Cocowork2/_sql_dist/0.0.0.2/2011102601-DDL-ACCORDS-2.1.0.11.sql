--
-- Patch DDL de ACCORDS du 26/10/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.11
-- Date de publication : 26/10/2011
-- Auteur(s) : Alexis TUAL
-- Licence : CeCILL version 2
--
--

CREATE OR REPLACE package ACCORDS.UTILITAIRE_TRA is

  function get_tra_montant(a_tra_ordre number) return number;

end UTILITAIRE_TRA;
/

CREATE OR REPLACE package body ACCORDS.UTILITAIRE_TRA as

  function get_tra_montant(a_tra_ordre in number) return number is
    somme number;
  begin
    select sum(rpt_montant_participation) into somme from ACCORDS.REPART_PARTENAIRE_TRANCHE where tra_ordre = a_tra_ordre;
    return somme;
  end get_tra_montant;

end UTILITAIRE_TRA;
/

------------------------------------------------------------------------------------------------------------------------
--  Rajout d'un utilitaire pour calculer le montant d'une tranche
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.11',to_date('26/10/2011','DD/MM/YYYY'),
    'Rajout d''un utilitaire pour calculer le montant d''une tranche');
commit;
