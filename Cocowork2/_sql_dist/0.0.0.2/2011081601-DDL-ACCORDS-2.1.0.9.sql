--
-- Patch DDL de ACCORDS du 16/08/2011 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;

--
--
-- Fichier : 1
-- Type : DDL
-- Schema : ACCORDS
-- Numero de version : 2.1.0.9
-- Date de publication : 16/08/2011
-- Auteur(s) : Alexis TUAL
-- Licence : CeCILL version 2
--
--

WHENEVER SQLERROR EXIT SQL.SQLCODE;

------------------------------------------------------------------------------------------------------------------------
--  Drop de la contrainte null sur la colonne ACCORDS.HISTO_CREDIT_POSITIONNE.PCO_NUM et
--                                            ACCORDS.HISTO_CREDIT_POSIT_REC.PCO_NUM
------------------------------------------------------------------------------------------------------------------------
ALTER TABLE ACCORDS.HISTO_CREDIT_POSITIONNE MODIFY (PCO_NUM NULL);
ALTER TABLE ACCORDS.HISTO_CREDIT_POSIT_REC MODIFY (PCO_NUM NULL);

------------------------------------------------------------------------------------------------------------------------
--  Maj de la version null sur la colonne ACCORDS.HISTO_CREDIT_POSITIONNE.PCO_NUM
------------------------------------------------------------------------------------------------------------------------
Insert into ACCORDS.VERSION_HISTO (VH_ORDRE,VH_NUM,VH_DATE_EXEC,VH_LIBELLE) values (
    accords.VERSION_HISTO_SEQ.nextval,'2.1.0.9',to_date('16/08/2011','DD/MM/YYYY'),
    'Drop de la contrainte null sur la colonne ACCORDS.HISTO_CREDIT_POSITIONNE.PCO_NUM et 
    ACCORDS.HISTO_CREDIT_POSIT_REC.PCO_NUM');
commit;