package org.cocktail.cocowork.server;

import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class CocoworkApplicationUser extends PersonneApplicationUser {

	public static final String TYAP_STR_ID_COCONUT = "COCONUT";
	public static NSArray fonctions = null;
	
	/** Super admin */
	public static final String FON_ID_SUPER_ADMIN = "SUPADMIN";
	private Boolean hasDroitSuperAdmin = null;
	
	
	/** Gestion / Contrat */
	public static final String FON_ID_CREATION_CONTRAT = "CONC";
	private Boolean hasDroitCreationContratsEtAvenants = null;	
	public static final String FON_ID_CONSULTATION_CONTRAT = "CONV";
	private Boolean hasDroitConsultationContratsEtAvenants = null;
	public static final String FON_ID_CONSULTATION_TOUS_CONTRATS = "CONVTOUS";
	private Boolean hasDroitConsultationTousLesContratsEtAvenants = null;
	public static final String FON_ID_RECHERCHE_DETAILLEE_CONTRAT = "CONR";
	private Boolean hasDroitRechercheDetailleeContratsEtAvenants = null;
	public static final String FON_ID_MODIFICATION_CONTRAT = "CONM";
	private Boolean hasDroitModificationContratsEtAvenants = null;
	public static final String FON_ID_SUPPRESSION_CONTRAT = "CONS";
	private Boolean hasDroitSuppressionContratsEtAvenants = null;
	public static final String FON_ID_VALIDATION_CONTRAT = "CONVAL";
	private Boolean hasDroitValidationContratEtAvenants = null;

	/** Gestion / Generalites */
	public static final String FON_ID_MODIFICATION_GENERALITES = "GENM";
	private Boolean hasDroitModificationGeneralites = null;
	
	/** Gestion / Partenaires */
	public static final String FON_ID_CONSULTATION_PARTENAIRES = "PARTV";
	private Boolean hasDroitConsultationPartenaires = null;
	public static final String FON_ID_MODIFICATION_PARTENAIRES = "PARTM";
	private Boolean hasDroitModificationPartenaires = null;
	
	/** Gestion / Document */
	public static final String FON_ID_CONSULTATION_DOCUMENT = "DOCV";
	private Boolean hasDroitConsultationDocuments = null;
	public static final String FON_ID_MODIFICATION_DOCUMENT = "DOCM";
	private Boolean hasDroitModificationDocuments = null;
	
	/** Gestion / Evenements */
	public static final String FON_ID_CONSULTATION_EVENEMENTS = "EVTV";
	private Boolean hasDroitConsultationEvenements = null;
	public static final String FON_ID_MODIFICATION_EVENEMENTS = "EVTM";
	private Boolean hasDroitModificationEvenements = null;
	
	/** Gestion / Tranches */
	public static final String FON_ID_CREATION_TRANCHES = "TRAC";
	private Boolean hasDroitCreationTranches = null;	
	public static final String FON_ID_SUPPRESSION_TRANCHES = "TRAS";
	private Boolean hasDroitSuppressionTranches = null;
	public static final String FON_ID_MODIFICATION_TRANCHES = "TRAM";
	private Boolean hasDroitModificationTranches = null;
	
	/** Impressions */
	private static final String FON_ID_IMPRESSION_GENERALITES = "IMPGEN";
    private Boolean hasDroitImpressionGeneralites = null;

	/** Gestion budget */
	private static final String FON_ID_BUDPROP = "BUDPROP";
    private Boolean hasDroitPropositionBudget = null;
    
    /** Validation tranche */
    private static final String FON_ID_VALIDATION_TRANCHE = "TRAVAL";
    private Boolean hasDroitValidationTranche = null;
    
    /** Report des crédits **/
    private static final String FON_ID_REPORT_CREDITS = "REPC";
    private Boolean hasDroitReportCredits = null;


	public CocoworkApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		super(ec, tyapStrId, persId);
	}

	public CocoworkApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
	}

	public CocoworkApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
	}

	public CocoworkApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	public boolean hasDroitSuperAdmin() {
		if (hasDroitSuperAdmin == null) {
			hasDroitSuperAdmin = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_SUPER_ADMIN, null);
		}
		return hasDroitSuperAdmin;
	}
	
	
	public boolean hasDroitCreationContratsEtAvenants() {
		if (hasDroitCreationContratsEtAvenants == null) {
			hasDroitCreationContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CREATION_CONTRAT, null);
		}
		return hasDroitCreationContratsEtAvenants;
	}
	
	public boolean hasDroitConsultationContratsEtAvenants() {
		if (hasDroitConsultationContratsEtAvenants == null) {
			hasDroitConsultationContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CONSULTATION_CONTRAT, null);
		}
		return hasDroitConsultationContratsEtAvenants;
	}
	
	public boolean hasDroitConsultationTousLesContratsEtAvenants() {
		if (hasDroitConsultationTousLesContratsEtAvenants == null) {
			hasDroitConsultationTousLesContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CONSULTATION_TOUS_CONTRATS, null);
		}
		return hasDroitConsultationTousLesContratsEtAvenants;
	}
	
	
	public boolean hasDroitValidationContratsEtAvenants() {
	    if (hasDroitValidationContratEtAvenants == null) {
	        hasDroitValidationContratEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_VALIDATION_CONTRAT, null);
	    }
	    return hasDroitValidationContratEtAvenants;
	}
	
	public boolean hasDroitSuppressionContratsEtAvenants() {
		if (hasDroitSuppressionContratsEtAvenants == null) {
			hasDroitSuppressionContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_SUPPRESSION_CONTRAT, null);
		}
		return hasDroitSuppressionContratsEtAvenants;
	}
	
	public boolean hasDroitRechercheDetailleeContratsEtAvenants() {
		if (hasDroitRechercheDetailleeContratsEtAvenants == null) {
			hasDroitRechercheDetailleeContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_RECHERCHE_DETAILLEE_CONTRAT, null);
		}
		return hasDroitRechercheDetailleeContratsEtAvenants;
	}
	
	public boolean hasDroitModificationContratsEtAvenants() {
		if (hasDroitModificationContratsEtAvenants == null) {
			hasDroitModificationContratsEtAvenants = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_CONTRAT, null);
		}
		return hasDroitModificationContratsEtAvenants;
	}
	
	public boolean hasDroitModificationGeneralites() {
		if (hasDroitModificationGeneralites == null) {
			hasDroitModificationGeneralites = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_GENERALITES, null);
		}
		return hasDroitModificationGeneralites;
	}
	
	public boolean hasDroitConsultationPartenaires() {
		if (hasDroitConsultationPartenaires == null) {
			hasDroitConsultationPartenaires = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CONSULTATION_PARTENAIRES, null);
		}
		return hasDroitConsultationPartenaires;
	}
	
	public boolean hasDroitModificationPartenaires() {
	    if (hasDroitModificationPartenaires == null) {
	        hasDroitModificationPartenaires = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_PARTENAIRES, null);
	    }
		return hasDroitModificationPartenaires; 
	}
	
	public boolean hasDroitConsultationDocuments() {
		if (hasDroitConsultationDocuments == null) {
			hasDroitConsultationDocuments = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CONSULTATION_DOCUMENT, null);
		}
		return hasDroitConsultationDocuments;
	}
	
	public boolean hasDroitModificationDocuments() {
	    if (hasDroitModificationDocuments == null)
	        hasDroitModificationDocuments = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_DOCUMENT, null);
	    return hasDroitModificationDocuments;
	}
	
	public boolean hasDroitConsultationEvenements() {
		if (hasDroitConsultationEvenements == null) {
			hasDroitConsultationEvenements = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CONSULTATION_EVENEMENTS, null);
		}
		return hasDroitConsultationEvenements;
	}
	
	public boolean hasDroitModificationEvenements() {
		if (hasDroitModificationEvenements == null) {
			hasDroitModificationEvenements = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_EVENEMENTS, null);
		}
		return hasDroitModificationEvenements;
	}
	
	public boolean hasDroitCreationTranches() {
		if (hasDroitCreationTranches == null) {
			hasDroitCreationTranches = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_CREATION_TRANCHES, null);
		}
		return hasDroitCreationTranches;
	}
	
	public boolean hasDroitModificationTranches() {
		if (hasDroitModificationTranches == null) {
			hasDroitModificationTranches = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_MODIFICATION_TRANCHES, null);
		}
		return hasDroitModificationTranches;
	}
	
	public boolean hasDroitSuppressionTranches() {
		if (hasDroitSuppressionTranches == null) {
			hasDroitSuppressionTranches = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_SUPPRESSION_TRANCHES, null);
		}
		return hasDroitSuppressionTranches;
	}
	
	public boolean hasDroitImpressionGeneralites() {
	    if (hasDroitImpressionGeneralites == null) {
	        hasDroitImpressionGeneralites = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_IMPRESSION_GENERALITES, null);
	    }
	    return hasDroitImpressionGeneralites;
	}

	public boolean hasDroitValidationPrevisionBudget() {
	    if (hasDroitPropositionBudget == null) {
	        hasDroitPropositionBudget = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_BUDPROP, null);
	    }
	    return hasDroitPropositionBudget;
	}
	
	public boolean hasDroitPropositionBudget() {
        if (hasDroitPropositionBudget == null) {
            hasDroitPropositionBudget = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_BUDPROP, null);
        }
        return hasDroitPropositionBudget;
	}
	
	public boolean hasDroitValidationTranche() {
	    if (hasDroitValidationTranche == null) {
	        hasDroitValidationTranche = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_VALIDATION_TRANCHE, null);
	    }
	    return hasDroitValidationTranche;
	}
	
	public boolean hasDroitReportCredits() {
	    if (hasDroitReportCredits == null) {
	    	hasDroitReportCredits = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_COCONUT, FON_ID_REPORT_CREDITS, null);
	    }
	    return hasDroitReportCredits;
	}	
	
}
