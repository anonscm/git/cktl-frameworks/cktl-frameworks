package org.cocktail.cocowork.server;

import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;

public class CocoworkAutorisationCache extends AutorisationsCache {

    public CocoworkAutorisationCache(String appStrId, Integer persId) {
        super(appStrId, persId);
    }
    
    public CocoworkAutorisationCache(Integer persId) {
        this("COCONUT", persId);
    }

    public boolean hasDroitConsultationContrat() {
        return hasDroitUtilisationOnFonction("CONV");
    }
    
}
