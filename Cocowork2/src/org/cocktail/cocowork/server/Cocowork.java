package org.cocktail.cocowork.server;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

/**
 * Framework principal de Cocowork2.
 * 
 * @see ERXFrameworkPrincipal
 * @author Alexis Tual
 *
 */
public class Cocowork extends ERXFrameworkPrincipal {
	public static CocoworkParamManager paramManager = new CocoworkParamManager();

    static {
        setUpFrameworkPrincipalClass(Cocowork.class);
    }
    
    @Override
    public void finishInitialization() {
        ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
        moduleRegister.addModule(new CocoworkModule());
    }
    
    @Override
    public void didFinishInitialization() {
    	super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();
    }
    
}
