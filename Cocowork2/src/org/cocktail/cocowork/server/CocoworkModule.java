package org.cocktail.cocowork.server;

import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponible;
import org.cocktail.cocowork.server.metier.convention.service.CalculTvaService;
import org.cocktail.cocowork.server.metier.convention.service.CalculTvaServiceImpl;
import org.cocktail.cocowork.server.metier.convention.service.recherche.RechercheContratService;
import org.cocktail.cocowork.server.metier.convention.service.recherche.RechercheContratServiceImpl;

import com.google.inject.AbstractModule;

/**
 * 
 * Module Guice.
 * Si on a besoin de définir un lien dans le fichier Properties : {@link #getImplementationClassFor(Class)}
 * 
 * @author Alexis Tual
 *
 */
public class CocoworkModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RechercheContratService.class).to(RechercheContratServiceImpl.class);
        bind(CalculTvaService.class).to(CalculTvaServiceImpl.class);
        bind(CalculMontantParticipationDisponible.class).to(
                getImplementationClassFor(CalculMontantParticipationDisponible.class));
    }

    @SuppressWarnings("unchecked")
    private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
        try {
            String implementationClassName = System.getProperty(baseClass.getSimpleName());
            Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
            return clazz;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
        }
    }
    
}
