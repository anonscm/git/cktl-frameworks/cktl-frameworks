// DO NOT EDIT.  Make changes to ModeGestion.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOModeGestion extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWModeGestion";

  // Attribute Keys
  public static final ERXKey<String> MG_COMMENTAIRE = new ERXKey<String>("mgCommentaire");
  public static final ERXKey<String> MG_LIBELLE = new ERXKey<String>("mgLibelle");
  public static final ERXKey<String> MG_LIBELLE_COURT = new ERXKey<String>("mgLibelleCourt");
  // Relationship Keys

  // Attributes
  public static final String MG_COMMENTAIRE_KEY = MG_COMMENTAIRE.key();
  public static final String MG_LIBELLE_KEY = MG_LIBELLE.key();
  public static final String MG_LIBELLE_COURT_KEY = MG_LIBELLE_COURT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOModeGestion.class);

  public ModeGestion localInstanceIn(EOEditingContext editingContext) {
    ModeGestion localInstance = (ModeGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String mgCommentaire() {
    return (String) storedValueForKey(EOModeGestion.MG_COMMENTAIRE_KEY);
  }

  public void setMgCommentaire(String value) {
    if (EOModeGestion.LOG.isDebugEnabled()) {
        EOModeGestion.LOG.debug( "updating mgCommentaire from " + mgCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOModeGestion.MG_COMMENTAIRE_KEY);
  }

  public String mgLibelle() {
    return (String) storedValueForKey(EOModeGestion.MG_LIBELLE_KEY);
  }

  public void setMgLibelle(String value) {
    if (EOModeGestion.LOG.isDebugEnabled()) {
        EOModeGestion.LOG.debug( "updating mgLibelle from " + mgLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOModeGestion.MG_LIBELLE_KEY);
  }

  public String mgLibelleCourt() {
    return (String) storedValueForKey(EOModeGestion.MG_LIBELLE_COURT_KEY);
  }

  public void setMgLibelleCourt(String value) {
    if (EOModeGestion.LOG.isDebugEnabled()) {
        EOModeGestion.LOG.debug( "updating mgLibelleCourt from " + mgLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOModeGestion.MG_LIBELLE_COURT_KEY);
  }


  public static ModeGestion create(EOEditingContext editingContext, String mgLibelle
, String mgLibelleCourt
) {
    ModeGestion eo = (ModeGestion) EOUtilities.createAndInsertInstance(editingContext, EOModeGestion.ENTITY_NAME);    
        eo.setMgLibelle(mgLibelle);
        eo.setMgLibelleCourt(mgLibelleCourt);
    return eo;
  }

  public static ERXFetchSpecification<ModeGestion> fetchSpec() {
    return new ERXFetchSpecification<ModeGestion>(EOModeGestion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ModeGestion> fetchAll(EOEditingContext editingContext) {
    return EOModeGestion.fetchAll(editingContext, null);
  }

  public static NSArray<ModeGestion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOModeGestion.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<ModeGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ModeGestion> fetchSpec = new ERXFetchSpecification<ModeGestion>(EOModeGestion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ModeGestion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ModeGestion fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOModeGestion.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ModeGestion fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ModeGestion> eoObjects = EOModeGestion.fetchAll(editingContext, qualifier, null);
    ModeGestion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWModeGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ModeGestion fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOModeGestion.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ModeGestion fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    ModeGestion eoObject = EOModeGestion.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWModeGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ModeGestion localInstanceIn(EOEditingContext editingContext, ModeGestion eo) {
    ModeGestion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.cocowork.server.metier.convention.ModeGestion> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOModeGestion.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.ModeGestion>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.cocowork.server.metier.convention.ModeGestion> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOModeGestion.ENTITY_NAME);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.ModeGestion>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}