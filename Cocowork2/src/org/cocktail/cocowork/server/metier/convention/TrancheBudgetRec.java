/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.common.exception.ExceptionApplication;
import org.cocktail.cocowork.common.exception.ExceptionProcedure;
import org.cocktail.cocowork.server.Cocowork;
import org.cocktail.cocowork.server.CocoworkParamManager;
import org.cocktail.cocowork.server.metier.convention.procedure.suivirecette.ProcedureGetTotalRecette;
import org.cocktail.fwkcktljefyadmin.common.exception.IllegalArgumentException;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationFactory;

public class TrancheBudgetRec extends EOTrancheBudgetRec {

    public static final EOQualifier QUAL_NON_SUPPR = ERXQ.equals(TrancheBudgetRec.TBR_SUPPR_KEY, "N");
    private static final Logger LOG = Logger.getLogger(TrancheBudgetRec.class);
    
    public TrancheBudgetRec() {
        super();
    }
    
    public TrancheBudgetRec mergeAvecExistant() {
        TrancheBudgetRec result = this;
        EOQualifier qual = TRANCHE.eq(tranche()).and(TYPE_CREDIT.eq(typeCredit())).and(ORGAN.eq(organ())).and(QUAL_NON_SUPPR);
        TrancheBudgetRec existant = TrancheBudgetRec.fetch(editingContext(), qual);
        if (existant != null) {
            LOG.info("Va merger une TrancheBudgetRec avec celle existante");
            existant.setTbrMontant(existant.tbrMontant().add(this.tbrMontant()));
            // On supprime l'objet courant
            this.editingContext().deleteObject(this);
            result = existant;
        }
        return result;
    }
    
    public static TrancheBudgetRec creerTrancheBudget(Tranche tranche, EOExercice exericeOuvertCourant, Integer utlOrdre) {
        if (tranche == null)
            throw new IllegalArgumentException("La tranche est obligatoire");
        // Tout un tas de vérif sur l'exercice avant d'aller plus loin...
        DepenseRecetteCommon.verificationExercice(tranche.exerciceCocktail(), exericeOuvertCourant);
        TrancheBudgetRec trancheBudgetRec = EOTrancheBudgetRec.create(tranche.editingContext(), null, null, null, null);
        trancheBudgetRec.setTbrSuppr("N");
        trancheBudgetRec.setTbrDateCreation(new NSTimestamp());
        trancheBudgetRec.setTrancheRelationship(tranche);
        trancheBudgetRec.setExerciceRelationship(tranche.exerciceCocktail().exercice());
        trancheBudgetRec.setTbrMontant(BigDecimal.ZERO);
        trancheBudgetRec.setTbrDateModif(new NSTimestamp());
        trancheBudgetRec.setUtlOrdreCreation(utlOrdre);
        return trancheBudgetRec;
    }
    
    public static void supprimerLigneCreditRecette(EOEditingContext ec, EOExercice exericeOuvertCourant, VCreditsPositionnesRec creditsRecettesPos) {
        EOExerciceCocktail exerciceSelectionne = creditsRecettesPos.tranche().exerciceCocktail();
        // On vérifie l'exercice surlequel on se trouve
        DepenseRecetteCommon.verificationExercice(exerciceSelectionne, exericeOuvertCourant);
        // On va chercher la tranche budget correspondante
        EOQualifier qual = TrancheBudgetRec.ORGAN.eq(creditsRecettesPos.organ()).and(
                           TrancheBudgetRec.TYPE_CREDIT.eq(creditsRecettesPos.typeCredit())).and(
                           TrancheBudgetRec.TRANCHE.eq(creditsRecettesPos.tranche())).and(
                                           TrancheBudgetRec.QUAL_NON_SUPPR);
        
        NSArray<TrancheBudgetRec> tranchesBudgetRec = TrancheBudgetRec.fetchAll(ec, qual, null);
        if (tranchesBudgetRec.count() < 1)
            throw new IllegalStateException("Aucune TrancheBudgetRec correspondant au cr\u00E9dit positionn\u00E9 " +
                                            "n'a \u00E9t\u00E9 trouv\u00E9.");
        else if (tranchesBudgetRec.count() > 1)
            throw new IllegalStateException("Plusieurs TrancheBudgetRec correspondants au cr\u00E9dit positionn\u00E9 " +
                                            "ont \u00E9t\u00E9 trouv\u00E9s.");
        TrancheBudgetRec trancheBudgetRec = tranchesBudgetRec.lastObject();
        supprimerTrancheBudgetRec(ec, trancheBudgetRec, creditsRecettesPos);
    }

    public static void supprimerTrancheBudgetRec(EOEditingContext ec, TrancheBudgetRec trancheBudgetRec, VCreditsPositionnesRec creditsRecettesPosRec) {
        if (creditsRecettesPosRec != null) {
            HistoCreditPositionneRec.creerHistoCreditPositionne(ec, creditsRecettesPosRec.tranche(), creditsRecettesPosRec.planco(), 
                            creditsRecettesPosRec.typeCredit(), creditsRecettesPosRec.organ(), creditsRecettesPosRec.montant().negate());
            trancheBudgetRec.setTbrMontant(trancheBudgetRec.tbrMontant().subtract(creditsRecettesPosRec.montant()));
        } else {
            HistoCreditPositionneRec.creerHistoCreditPositionne(ec, trancheBudgetRec.tranche(), null, 
                            trancheBudgetRec.typeCredit(), trancheBudgetRec.organ(), trancheBudgetRec.tbrMontant().negate());
            trancheBudgetRec.setTbrMontant(BigDecimal.valueOf(0));
        }
        if (trancheBudgetRec.tbrMontant().compareTo(BigDecimal.ZERO) <= 0) {
            trancheBudgetRec.setTbrSuppr("O");
            trancheBudgetRec.setTbrDateModif(new NSTimestamp());
        }
    }
    

    /**
     * <p>Fournit le total des depenses existantes sur les memes exercice, convention, type de credit et LB que cet objet. 
     * <p>Ce total des depenses est : total restant engage + total liquide.
     * <p>Fait appel a une procedure stoquee.
     * @return Le total des depenses ou <code>null</code> si probleme.
     * @throws ExceptionApplication 
     */
    public BigDecimal totalRecettes() {
        BigDecimal totalRecette=null;
        if (!isNewObject()) {
            try {
                Number exeOrdre = (Number)ERXEOControlUtilities.primaryKeyObjectForObject(exercice());
                Number conOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(tranche().contrat());
                Number tcdOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(typeCredit());
                Number orgId = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(organ());

                totalRecette = new ProcedureGetTotalRecette(editingContext()).execute(
                                exeOrdre, 
                                conOrdre, 
                                tcdOrdre, 
                                orgId);
            } 
            catch (ExceptionProcedure e) {
                throw new RuntimeException(
                                "Une erreur est survenue lors de l'appel \u00E0 la procedure stoqu\u00E9e :\n" +
                                                e.getMessage(), e);
            }
            catch (Exception e) {
                throw new RuntimeException(
                                "Impossible d'obtenir la cl\u00E9 primaire d'un objet :\n" +
                                                e.getMessage(), e);
            }
        }
        return totalRecette;
    }

    /**
     * Fournit le "reste a recetter" par rapport au total positionne, 
     * cad la difference entre le total positionne et le total des recettes existantes.
     * @see #totalRecettes()
     * @return Le montant restant a recetter ou <code>null</code> si probleme lors de l'obtention du total des recettes.
     * @throws ExceptionApplication 
     */
    public BigDecimal resteARecetter() {
        BigDecimal totalRecettes = totalRecettes();
        if (totalRecettes != null)
            return tbrMontant().subtract(totalRecettes);
        else
            return null;
    }
    
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        // Si une convention RA 
        // Si le crédit positionné dépasse le montant de la tranche
        if (tranche().contrat().isModeRA()) {
          if (tbrMontant().compareTo(tranche().traMontant()) == 1) {
              throw ERXValidationFactory.defaultFactory().createCustomException(this, TBR_MONTANT_KEY, tbrMontant(), "MontantTooBig");
          }
        } else {
            ContratFinancementRecettesValidation financementValidation = new ContratFinancementRecettesValidation();
			if (!financementValidation.isSatisfiedBy(tranche().contrat())) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "MontantContributionsTropBasPourTotalPositionne");
			}
        }
        // Si on est dans le cadre d'une convention RA, on vérifie qu'il n'y ait pas déjà une trancheBudget sur un autre
        // organ
        // Si multi-ligne par conv. RA non autorisé
        String isMultiLBAutorise = Cocowork.paramManager.getParam(CocoworkParamManager.MULTI_LB_POUR_CONV_RA_AUTORISE);
		if (!CktlParamManager.booleanForValue(isMultiLBAutorise)) {
	        if (tranche().contrat().isModeRA()) {
	            NSArray<EOOrgan> allOrgansRec = (NSArray<EOOrgan>) tranche().trancheBudgetsRecNonSupprimes().valueForKey(ORGAN_KEY);
	            allOrgansRec = ERXArrayUtilities.arrayWithoutDuplicates(allOrgansRec);
	            if (allOrgansRec.count() > 1) {
	                throw ERXValidationFactory.defaultFactory().createCustomException(this, "MultipleOrgansRA");
	            }
	        }
        }
    }
    
}
