// DO NOT EDIT.  Make changes to TranchePrevision.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTranchePrevision extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTranchePrevision";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> PREVISION_BUDGET = new ERXKey<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget>("previsionBudget");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");

  // Attributes
  // Relationships
  public static final String PREVISION_BUDGET_KEY = PREVISION_BUDGET.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOTranchePrevision.class);

  public TranchePrevision localInstanceIn(EOEditingContext editingContext) {
    TranchePrevision localInstance = (TranchePrevision)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget previsionBudget() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget)storedValueForKey(EOTranchePrevision.PREVISION_BUDGET_KEY);
  }
  
  public void setPrevisionBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
    takeStoredValueForKey(value, EOTranchePrevision.PREVISION_BUDGET_KEY);
  }

  public void setPrevisionBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
    if (EOTranchePrevision.LOG.isDebugEnabled()) {
      EOTranchePrevision.LOG.debug("updating previsionBudget from " + previsionBudget() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPrevisionBudget(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget oldValue = previsionBudget();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranchePrevision.PREVISION_BUDGET_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranchePrevision.PREVISION_BUDGET_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOTranchePrevision.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOTranchePrevision.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOTranchePrevision.LOG.isDebugEnabled()) {
      EOTranchePrevision.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranchePrevision.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranchePrevision.TRANCHE_KEY);
    }
  }
  

  public static TranchePrevision create(EOEditingContext editingContext, org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget previsionBudget, org.cocktail.cocowork.server.metier.convention.Tranche tranche) {
    TranchePrevision eo = (TranchePrevision) EOUtilities.createAndInsertInstance(editingContext, EOTranchePrevision.ENTITY_NAME);    
    eo.setPrevisionBudgetRelationship(previsionBudget);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<TranchePrevision> fetchSpec() {
    return new ERXFetchSpecification<TranchePrevision>(EOTranchePrevision.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TranchePrevision> fetchAll(EOEditingContext editingContext) {
    return EOTranchePrevision.fetchAll(editingContext, null);
  }

  public static NSArray<TranchePrevision> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTranchePrevision.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TranchePrevision> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TranchePrevision> fetchSpec = new ERXFetchSpecification<TranchePrevision>(EOTranchePrevision.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TranchePrevision> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TranchePrevision fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranchePrevision.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TranchePrevision fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TranchePrevision> eoObjects = EOTranchePrevision.fetchAll(editingContext, qualifier, null);
    TranchePrevision eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTranchePrevision that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TranchePrevision fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranchePrevision.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TranchePrevision fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TranchePrevision eoObject = EOTranchePrevision.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTranchePrevision that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TranchePrevision localInstanceIn(EOEditingContext editingContext, TranchePrevision eo) {
    TranchePrevision localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}