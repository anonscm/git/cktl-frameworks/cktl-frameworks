package org.cocktail.cocowork.server.metier.convention.service;

import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.foundation.ERXSelectorUtilities;

/**
 * 
 * Classe intermédiaire pour centraliser les notifications de Cocolight.
 * Masque aussi l'API WO pour l'extérieur.<br/>
 * (Un bémol qd même, il faut quand même que les méthode appelée ait en paramètre
 * un objet NSNotification).
 * 
 * @author Alexis Tual
 *
 */
public final class NotificationCenter {

    private NotificationCenter() {
        
    }
    
    /**
     * @return une instance de {@link NotificationCenter}
     */
    public static NotificationCenter instance() {
        return new NotificationCenter();
    }
    
    /**
     * Les différentes notifications employées dans Cocolight.
     *
     */
    public static enum Notification {
        REFRESH_DEPENSES, REFRESH_RECETTES, REFRESH_TRANCHES, WILL_OPEN_AJOUT_PREVISION
    }
    
    /**
     * @param objet l'objet à abonner à la notification
     * @param methode la méthode à exécuter lorsque l'objet sera notifié.
     *          Attention la méthode doit pour l'instant prendre en argument un
     *          {@link NSNotification}
     * @param notification la {@link Notification} à laquelle on veut s'abonner.
     */
    public void abonner(Object objet, String methode, Notification notification) {
        abonner(objet, methode, notification, null);
    }
    
    /**
     * @param objet l'objet à abonner à la notification
     * @param methode la méthode à exécuter lorsque l'objet sera notifié.
     *          Attention la méthode doit pour l'instant prendre en argument un
     *          {@link NSNotification}
     * @param notification la {@link Notification} à laquelle on veut s'abonner.
     * @param origine on veut être notifié uniquement si l'objet d'origine est égal à <code>origine</code>
     */
    public void abonner(Object objet, String methode, Notification notification, Object origine) {
        NSNotificationCenter.defaultCenter().addObserver(
                objet, 
                ERXSelectorUtilities.notificationSelector(methode), 
                notification.name(), origine);
    }
    
    
    /**
     * @param notification la {@link Notification} que l'on veut envoyer.
     */
    public void notifier(Notification notification) {
        NSNotificationCenter.defaultCenter().postNotification(notification.name(), null);
    }
    
    /**
     * @param notification la {@link Notification} que l'on veut envoyer.
     * @param infos : un objet d'informations / origine
     */
    public void notifier(Notification notification, Object infos) {
        NSNotificationCenter.defaultCenter().postNotification(notification.name(), infos);
    }
    
}
