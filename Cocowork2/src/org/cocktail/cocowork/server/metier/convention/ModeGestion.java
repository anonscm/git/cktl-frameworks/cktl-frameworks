

// ModeGestion.java
// 
package org.cocktail.cocowork.server.metier.convention;

import er.extensions.eof.ERXKey;



public class ModeGestion extends EOModeGestion
{

    public static final ERXKey<Integer> MG_ORDRE = new ERXKey<Integer>("mgOrdre");
    
    public static final String MODE_GESTION_RA = "RA";
    public static final String MODE_GESTION_SIF = "SIF";
    
    public ModeGestion() {
        super();
    }

    public boolean isModeRA() {
        return MODE_GESTION_RA.equals(mgLibelleCourt());
    }
    
}
