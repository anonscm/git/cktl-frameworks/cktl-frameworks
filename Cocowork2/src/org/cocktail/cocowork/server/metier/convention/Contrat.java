package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.GregorianCalendar;

import org.cocktail.cocowork.common.exception.ExceptionCompositePrimaryKey;
import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionNullPrimaryKey;
import org.cocktail.cocowork.common.exception.ExceptionProcedure;
import org.cocktail.cocowork.common.exception.ExceptionTemporaryPrimaryKey;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.Constantes;
import org.cocktail.cocowork.common.tools.ModelUtilities;
import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryProjet;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderConvention;
import org.cocktail.cocowork.server.metier.convention.procedure.utilitaire.ProcedureGetConOrdresPropoPourLb;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget;
import org.cocktail.fwkcktlcompta.server.metier.EOBrouillard;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateurOrgan;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;

//import fr.univlr.cri.eoutilities.CRIFetchUtilities;

public class Contrat extends EOContrat {

    public static final ERXKey<Integer> UTL_ORDRE_CREATION = new ERXKey<Integer>("utlOrdreCreation");
    public static final ERXKey<Integer> CON_CR = new ERXKey<Integer>("conCr");
    
	public static final String CON_SUPPR_OUI = "O";
	public static final String CON_SUPPR_NON = "N";
	public static final String CON_GROUPE_BUD_OUI = "O";
	public static final String CON_GROUPE_BUD_NON = "N";

	public final static NSNumberFormatter indexFormatter = new NSNumberFormatter("0000;-0000");

	public final static int OBJET_LONGUEUR_MAX = 250;
	public final static int OBJET_COURT_LONGUEUR_MAX = 50;

	public final static EOQualifier QUALIFIER_NON_SUPPR = EOQualifier.qualifierWithQualifierFormat("conSuppr='N'", null);

	public static final EOSortOrdering SORT_EXERCICE_DESC = EOSortOrdering.sortOrderingWithKey("exerciceCocktail.exeExercice", EOSortOrdering.CompareDescending);
	public static final EOSortOrdering SORT_INDEX_DESC = EOSortOrdering.sortOrderingWithKey("conIndex", EOSortOrdering.CompareDescending);
	public static final NSArray SORTS_EXERCICE_ET_INDEX_DESC = new NSArray(new Object[] { SORT_EXERCICE_DESC, SORT_INDEX_DESC });

	public static final String NUMERO_CONTRAT_KEY = "numeroContrat";

	private boolean shouldNotValidate;

	private Avenant avenantZero;

	public static final String ASS_CODE_RESPADMIN = "RESPADMIN";
	public static final String ASS_CODE_RESPSCIENTIF = "RESPSCIENTIF";
	
	static public class StrategieDate {
		public final static String STRATEGIE_LIBELLE_FIN_EXEC = "Mettre la date de fin d'ex\u00E9cution au 01/01/";
		public final static String STRATEGIE_LIBELLE_CLOTURE = "Mettre la date de cl\u00F4ture au 31/12/";
		public final static String STRATEGIE_LIBELLE_RIEN_FAIRE = "Ne rien faire";
		public final static int FIN_EXEC = 0;
		public final static int CLOTURE = 1;
		public final static int RIEN_FAIRE = 2;
		int code;
		String libelle;

		public StrategieDate(int code, String libelle) {
			this.code = code;
			this.libelle = libelle;
		}

		public int getCode() {
			return this.code;
		}

		public String getLibelle() {
			return this.libelle;
		}
	}

	private String sansAvis;

	public Contrat() {
		super();
	}

	public org.cocktail.cocowork.server.metier.convention.Avenant avenantZero() {
		if (avenantZero == null) {
			NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> avenants = avenants(Avenant.AVT_INDEX.eq(0), null, false);
			if (avenants != null && avenants.count() > 0) {
				avenantZero = avenants.objectAtIndex(0);
			}
		}
		return avenantZero;
	}

	public void setAvenantZeroRelationship(org.cocktail.cocowork.server.metier.convention.Avenant value) {
		if (value == null) {
			removeFromAvenantsRelationship(avenantZero());
		} else {
			addToAvenantsRelationship(value);
		}
	}

	/**
	 * Fournit une instance de cette classe.
	 * 
	 * @param ec
	 *            Editing context a utliser.
	 * @return L'instance creee.
	 * @throws Exception
	 *             Probleme d'instanciation.
	 */
	static public Contrat instanciate(EOEditingContext ec) throws Exception {
		Contrat unContrat = Contrat.create(ec, null, null, null, null);
		if (unContrat != null) {
			TypeReconduction trnp = (TypeReconduction) EOUtilities.objectMatchingKeyAndValue(ec, TypeReconduction.ENTITY_NAME, TypeReconduction.TR_ID_INTERNE_KEY, "NON_PREVUE");
			unContrat.setTypeReconductionRelationship(trnp);
		}
		return unContrat;
	}

	public NSArray<Contrat> autresContratsUtilisantOrgan(EOExercice exercice, EOOrgan organ) {
		// En RA, il faut que la LB RA selectionnee ne soit utilisee que par
		// cette convention
		NSArray<Contrat> contratsUtilisantCetteLB = new NSArray<Contrat>();
		Number exeOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(exercice);
		Number orgId = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(organ);
		String resultat;
		try {
			resultat = new ProcedureGetConOrdresPropoPourLb(editingContext()).execute(exeOrdre, orgId);
		} catch (ExceptionProcedure e) {
			throw NSForwardException._runtimeExceptionForThrowable(e);
		}
		NSMutableArray<String> tmp = NSArray.componentsSeparatedByString(resultat, "|").mutableClone();
		NSMutableArray<Integer> distinctsConOrdres = new NSMutableArray<Integer>();
		for (int i = 0; i < tmp.count(); i++) {
			distinctsConOrdres.addObject(new Integer((String) tmp.objectAtIndex(i)));
		}
		distinctsConOrdres.removeObject(Integer.valueOf(this.primaryKey()));
		if (!distinctsConOrdres.isEmpty()) {
			contratsUtilisantCetteLB = Contrat.fetchAll(editingContext(), Contrat.CON_ORDRE.in(distinctsConOrdres), null);
		}
		return contratsUtilisantCetteLB;
	}

	/**
	 * Tente de determine la cle primaire de ce contrat.
	 * 
	 * @return Cle primaire de ce contrat.
	 * @throws ExceptionNullPrimaryKey
	 * @throws ExceptionTemporaryPrimaryKey
	 * @throws ExceptionCompositePrimaryKey
	 */
	public Number getPrimaryKey() throws ExceptionNullPrimaryKey, ExceptionTemporaryPrimaryKey, ExceptionCompositePrimaryKey {
		return (Number) new ModelUtilities().primaryKeyForObject(this);
	}

	/**
	 * Reference lisible du contrat
	 * 
	 * @return Exemple : "2006-0012"
	 */
	public String exerciceEtIndex() {
		StringBuffer buf = new StringBuffer();
		buf.append(exerciceCocktail() != null ? exerciceCocktail().exeExercice().toString() : "????");
		buf.append("-");
		buf.append(conIndex() != null ? indexFormatter.format(conIndex()) : "????");
		return buf.toString();
	}

	public String numeroContrat() {
		StringBuffer buf = new StringBuffer();
		buf.append(typeClassificationContrat() != null ? typeClassificationContrat().tccCode() : "????");
		buf.append("-");
		buf.append(exerciceEtIndex());
		return buf.toString();
	}
	
	/**
	 * Avenant non supprimes dans l'ordre croissant de leur numero d'index.
	 * 
	 * @return Liste des avenants tries de ce contrat
	 */
	public NSArray avenantsDontInitialNonSupprimes() {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(avenants(), Avenant.QUALIFIER_NON_SUPPR), new NSArray(
				Avenant.SORT_INDEX_ASC));
	}

	/**
	 * Avenant non supprimes et valides dans l'ordre croissant de leur numero
	 * d'index.
	 * 
	 * @return Liste des avenants tries de ce contrat
	 */
	public NSArray avenantsDontInitialNonSupprimesEtValides() {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(
				EOQualifier.filteredArrayWithQualifier(avenants(), ERXQ.and(Avenant.QUALIFIER_VALIDE, Avenant.QUALIFIER_NON_SUPPR)), new NSArray(Avenant.SORT_INDEX_ASC));
	}

	/**
	 * Avenant non supprimes, sans prendre en compte l'avenant initial, dans
	 * l'ordre croissant de leur numero d'index.
	 * 
	 * @return Liste des avenants tries de ce contrat
	 */
	public NSArray avenantsNonSupprimes() {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(avenants(), Avenant.QUALIFIER_NON_SUPPR_NON_INITIAL), new NSArray(
				Avenant.SORT_INDEX_ASC));
	}

	/**
	 * Avenant non supprimes et valides sans prendre en compte l'avenant
	 * initial, dans l'ordre croissant de leur numero d'index.
	 * 
	 * @return Liste des avenants tries de ce contrat
	 */
	public NSArray<Avenant> avenantsNonSupprimesEtValides() {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(EOQualifier.filteredArrayWithQualifier(avenants(),
				ERXQ.and(Avenant.QUALIFIER_VALIDE, Avenant.QUALIFIER_NON_SUPPR_NON_INITIAL)), new NSArray(Avenant.SORT_INDEX_ASC));
	}

	/**
	 * Avenants valides, avenant initial inclus, dans l'ordre
	 * croissant de leur numero d'index.
	 * 
	 * @return Liste des avenants valides tries de ce contrat
	 */
	public NSArray avenantsValides() {
		return avenantsDontInitialNonSupprimesEtValides();
	}

	/**
	 * Avenants valides, sans prendre en compte l'avenant initial, dans l'ordre
	 * croissant de leur numero d'index.
	 * 
	 * @return Liste des avenants valides tries de ce contrat
	 */
	public NSArray avenantsValidesSansAvenantZero() {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(
				EOQualifier.filteredArrayWithQualifier(avenants(), ERXQ.and(Avenant.QUALIFIER_VALIDE, Avenant.QUALIFIER_NON_INITIAL, Avenant.QUALIFIER_NON_SUPPR)), new NSArray(Avenant.SORT_INDEX_ASC));
	}

	/**
	 * @param typeAvenant
	 *            Le code du type de l'avenant {@link TypeAvenant#taCode()}
	 * @return Liste des avenants valides, filtrés par type d'avenant (administratif ou financier), avec l'avenant
	 *         initial, trié par ordre croissant d'index
	 */
	public NSArray<Avenant> avenantsValidesParTypeAvecInitial(String typeAvenant) {
		// Les avenants valides avec l'avenant initial, triés
		NSArray<Avenant> avenants = avenantsValides();
		if (typeAvenant != null) {
			// Filre sur le type d'avenant
			EOQualifier qualTA = ERXQ.inObjects(Avenant.TYPE_AVENANT.dot(TypeAvenant.TA_CODE_KEY).key(), TypeAvenant.CODE_TYPE_AVENANT_INITIAL, typeAvenant);
			avenants = ERXQ.filtered(avenants, qualTA);
			// On re-trie au cas où...
			avenants = ERXS.sorted(avenants, Avenant.SORT_INDEX_ASC);
		}
		return avenants;
	}

	/**
	 * Nombre d'avenant non supprimes, sans compter l'avenant initial.
	 * 
	 * @return Le nombre d'avenant non supprimes, sans compter l'avenant
	 *         initial.
	 */
	public int avenantsCount() {
		return avenantsNonSupprimes().count();
	}

	/**
	 * Recherche en base d'un contrat selon son exercice de creation et son
	 * numero sequentiel (ex: 2006-0123).
	 * 
	 * @param ec
	 *            Editing context a utilser.
	 * @param typeClassification
	 *            Type de classification du contrat
	 * @param anneeExercice
	 *            Annee correspondant a l'exercice de creation (ex: 2006).
	 * @param index
	 *            Numero sequentiel (ex: 123).
	 * @return Le contrat correspondant a la convention cherchee.
	 */
	public static Contrat fetchContratSelonTypeClassificationEtExerciceEtIndex(final EOEditingContext ec, final TypeClassificationContrat typeClassification,
			final int anneeExercice, final int index) {
		/*
		 * EOQualifier qualif = EOQualifier.qualifierWithQualifierFormat(
		 * "exerciceCocktail.exeExercice = "
		 * +anneeExercice+" and conIndex = "+index, null);
		 */

		FinderConvention finder = new FinderConvention(ec);

		finder.setContratIndex(index);
		finder.setExerciceCreation(anneeExercice);
		finder.setTypeClassificationContrat(typeClassification);

		Contrat contrat;

		try {
			contrat = (Contrat) ERXArrayUtilities.firstObject(finder.find());
		} catch (ExceptionFinder e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			contrat = null;
		}

		return contrat;
	}

	/**
	 * Recherche en base des contrats appartenants à un projet/groupe de
	 * conventions.
	 * 
	 * @param projet
	 * @return La liste des contrats du projet/groupe.
	 */
	public static NSArray listeContratsPourProjet(final EOEditingContext ec, Projet projet) {

		FinderConvention finder = new FinderConvention(ec);

		finder.setProjet(projet);

		NSArray contrats;

		try {
			contrats = finder.find();
		} catch (ExceptionFinder e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			contrats = new NSArray();
		}

		return contrats;

	}

	public static NSArray<NSDictionary<String, Object>> centresResponsabiliteAsRawRows(EOEditingContext ec, NSArray<Integer> conOrdres) {
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		if (!conOrdres.isEmpty()) {
			String conOrdresAsString = conOrdres.componentsJoinedByString(",");
			String sql = "select distinct s.c_structure, s.ll_structure from GRHUM.STRUCTURE_ULR s, ACCORDS.CONTRAT c " + "where c.con_cr = s.c_structure and c.con_ordre in ("
					+ conOrdresAsString + ") order by s.ll_structure asc";
			EOModel model = EOUtilities.entityForClass(ec, Contrat.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	public static NSArray<NSDictionary<String, Object>> disciplinesAsRawRows(EOEditingContext ec, NSArray<Integer> conOrdres) {
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		if (!conOrdres.isEmpty()) {
			String conOrdresAsString = conOrdres.componentsJoinedByString(",");
			String sql = "select distinct d.disc_ordre, d.disc_libelle_long from GRHUM.DISCIPLINE d, ACCORDS.AVENANT a, ACCORDS.CONTRAT c "
					+ "where a.disc_ordre = d.disc_ordre and a.con_ordre = c.con_ordre and c.con_ordre in (" + conOrdresAsString + ") order by d.disc_libelle_long asc";
			EOModel model = EOUtilities.entityForClass(ec, Contrat.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	public static NSArray<NSDictionary<String, Object>> typesContratAsRawRows(EOEditingContext ec, NSArray<Integer> conOrdres) {
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		if (!conOrdres.isEmpty()) {
			String conOrdresAsString = conOrdres.componentsJoinedByString(",");
			String sql = "select distinct t.tycon_id, t.tycon_libelle from ACCORDS.TYPE_CONTRAT t, ACCORDS.CONTRAT c " + "where c.con_nature = t.tycon_id and c.con_ordre in ("
					+ conOrdresAsString + ") order by t.tycon_libelle asc";
			EOModel model = EOUtilities.entityForClass(ec, Contrat.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	public static NSArray<NSDictionary<String, Object>> modesGestionAsRawRows(EOEditingContext ec, NSArray<Integer> conOrdres) {
		NSArray<NSDictionary<String, Object>> results = NSArray.emptyArray();
		if (!conOrdres.isEmpty()) {
			String conOrdresAsString = conOrdres.componentsJoinedByString(",");
			String sql = "select distinct m.mg_ordre, m.mg_libelle from ACCORDS.MODE_GESTION m, ACCORDS.AVENANT a, ACCORDS.CONTRAT c "
					+ "where m.mg_ordre = a.avt_mode_gest and a.con_ordre = c.con_ordre and c.con_ordre in (" + conOrdresAsString + ") order by m.mg_libelle asc";
			EOModel model = EOUtilities.entityForClass(ec, Contrat.class).model();
			results = EOUtilities.rawRowsForSQL(ec, model.name(), sql, null);
		}
		return results;
	}

	/**
	 * Indique si ce contrat est du type specifie
	 * 
	 * @param typeContratIdInterne
	 *            Exemple: TypeContrat.TYCON_ID_INTERNE_CONVENTION_FORMATION
	 * @return <code>true</code> si cette convention est du type specifie,
	 *         <code>false</code> sinon.
	 */
	public boolean estDeType(final String typeContratIdInterne) {
		return typeContrat() != null && typeContratIdInterne != null && typeContratIdInterne.equals(typeContrat().tyconIdInterne());
	}

	/**
	 * Indique si une convention est du type specifie.
	 * 
	 * @param conOrdre
	 *            Identifiant de la convention
	 * @param typeContratIdInterne
	 *            Exemple: TypeContrat.TYCON_ID_INTERNE_CONVENTION_FORMATION
	 * @return <code>true</code> si la convention est du type specifi!e,
	 *         <code>false</code> sinon.
	 * @throws ExceptionFinder
	 */
	public static boolean conventionEstDeType(final Number conOrdre, final String typeContratIdInterne) throws ExceptionFinder {

		Contrat contrat = new FinderConvention(new EOEditingContext()).findWithConOrdre(conOrdre);
		return contrat.estDeType(typeContratIdInterne);
	}

	/**
	 * @return la liste des tranches ordonnées par exercice croissant
	 */
	public NSArray<Tranche> tranchesOrdonneesParExerciceAsc() {
	    return tranches(null, EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).ascs(), false);
	}
	
	public NSArray<Tranche> tranches(String... exeStat) {
		// fetch exercices
		EOQualifier qual = ERXQ.in(EOExercice.EXE_STAT_KEY, new NSArray<String>(exeStat));
		NSArray<EOExercice> exercices = EOExercice.fetchAll(editingContext(), qual);
		NSArray<Integer> exeOrdres = (NSArray<Integer>) exercices.valueForKey(EOExercice.EXE_EXERCICE_KEY);
		EOQualifier qualExo = ERXQ.in(Tranche.EXERCICE_COCKTAIL_KEY + "." + EOExerciceCocktail.EXE_EXERCICE_KEY, exeOrdres);
		return tranches(qualExo);
	}

	/**
	 * Retourne la tranche (eventuelle) du contrat pour un exercice donne.
	 * 
	 * @param exercice
	 *            Exercice a prendre en compte.
	 * @param yComprisLesSupprimees
	 *            Mettre a <code>false</code> pour ne pas prendre en compte les
	 *            tranches supprimees
	 * @return La Tranche, ou null si aucune trouvee
	 */
	public Tranche tranchePourExercice(final EOExerciceCocktail exercice, final boolean yComprisLesSupprimees) {
		if (exercice == null)
			return null;

		String str = yComprisLesSupprimees ? "exerciceCocktail = %@" : "traSuppr = 'N' and exerciceCocktail = %@";
		EOQualifier q = EOQualifier.qualifierWithQualifierFormat(str, new NSArray(exercice));

		NSArray tranches = EOQualifier.filteredArrayWithQualifier(tranches(), q);
		if (tranches.count() > 0)
			return (Tranche) tranches.objectAtIndex(0);
		else
			return null;
	}

	/**
	 * Calcule la somme des montants de tous les avenants de la convention (hors
	 * contrat initial).
	 * 
	 * @return
	 */
	public BigDecimal montantAvenantsHt() {
		return (BigDecimal) avenantsNonSupprimes().valueForKey("@sum.avtMontantHt");
	}

	/**
	 * Calcule la somme des montants des avenants de la convention (hors contrat
	 * initial) qui ont ete valides.
	 * 
	 * @return
	 */
	public BigDecimal montantAvenantsValidesHt() {
		EOQualifier qualifier = Avenant.QUALIFIER_VALIDE;
		NSArray array = EOQualifier.filteredArrayWithQualifier(avenantsNonSupprimes(), qualifier);
		return (BigDecimal) array.valueForKey("@sum.avtMontantHt");
	}

	/**
	 * Calcule la somme des montants ttc des avenants de la convention (hors
	 * contrat initial) qui ont ete valides.
	 * 
	 * @return
	 */
	public BigDecimal montantAvenantsValidesTtc() {
		EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat("avtDateValidAdm <> nil", null);
		NSArray array = EOQualifier.filteredArrayWithQualifier(avenantsNonSupprimes(), qualifier);
		return (BigDecimal) array.valueForKey("@sum.avtMontantTtc");
	}

	/**
	 * @return la somme du montant HT initial et des montants des avenants
	 *         valides HT
	 */
	public BigDecimal montantTotalHt() {
		if (avenantZero().avtMontantHt() != null)
			return avenantZero().avtMontantHt().add(montantAvenantsValidesHt());
		return null;
	}

	/**
	 * @return la somme du montant TTC initial et des montants des avenants
	 *         valides TTC
	 */
	public BigDecimal montantTotalTtc() {
		if (avenantZero().avtMontantTtc() != null)
			return avenantZero().avtMontantTtc().add(montantAvenantsValidesTtc());
		return null;
	}

	/**
	 * 
	 * @return La somme des credits positionnes en depenses sur l'intégralite des tranches. 
	 * Cela correspond a la somme des montants positionnés sur les tranches en dépenses de laquelle on soustrait 
	 * les montants des tranches reportes sur N + 1 pour ne pas le compter deux fois. 
	 */
	public BigDecimal totalPositionneContrat() {
		NSArray<TrancheBudget> trancheBudgets = ERXArrayUtilities
				.flatten((NSArray<TrancheBudget>) tranches(Tranche.QUALIFIER_NON_SUPPR).valueForKey(EOTranche.TRANCHE_BUDGETS_KEY));
		NSArray<TrancheBudget> trancheBudgetsNonSupprimes = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(trancheBudgets, TrancheBudget.QUAL_NON_SUPPR);
		BigDecimal totalReporte = (BigDecimal) tranches(Tranche.QUALIFIER_NON_SUPPR).valueForKey("@sum." + Tranche.REPORT_NPLUS1.key());
		BigDecimal totalPositionne = (BigDecimal) trancheBudgetsNonSupprimes.valueForKey("@sum." + TrancheBudget.TB_MONTANT_KEY);
		return totalPositionne.subtract(totalReporte);
	}
	
	public BigDecimal totalResteAPositionner() {
		return (BigDecimal) tranches(Tranche.QUALIFIER_NON_SUPPR).valueForKey("@sum." + Tranche.RESTE_A_POSITIONNE.key());
	}

	/**
	 * 
	 * @return La somme des credits positionnes en recette sur l'intégralite des
	 *         tranches
	 */
	public BigDecimal totalPositionneRecContrat() {
		NSArray<TrancheBudgetRec> trancheBudgets = ERXArrayUtilities.flatten((NSArray<TrancheBudgetRec>) tranches(Tranche.QUALIFIER_NON_SUPPR).valueForKey(
				Tranche.TRANCHE_BUDGET_RECS_KEY));
		NSArray<TrancheBudgetRec> trancheBudgetsNonSupprimes = ERXArrayUtilities.filteredArrayWithQualifierEvaluation(trancheBudgets, TrancheBudgetRec.QUAL_NON_SUPPR);
		return (BigDecimal) trancheBudgetsNonSupprimes.valueForKey("@sum." + TrancheBudgetRec.TBR_MONTANT_KEY);
	}

	private NSArray<Tranche> tranchesNonSupprimees() {
	    return tranches(Tranche.QUALIFIER_NON_SUPPR);
	}
	
	/**
	 * @return le total des contributions disponibles à positionner sur l'ensemble des tranches.
	 * 
	 */
	public BigDecimal totalContributionsAPositionner() {
	    return Tranche.TOTAL_CONTRIBUTIONS_DISPONIBLES.atSum().valueInObject(tranchesNonSupprimees());
	}
	
	/**
	 * @return le total des contributions disponibles à positionner sur l'ensemble des tranches, avec les reports
	 * 
	 */
	public BigDecimal totalContributionsAPositionnerAvecReports() {
		return Tranche.TOTAL_CONTRIBUTIONS_DISPONIBLES_PLUS_REPORT.atSum().valueInObject(tranchesNonSupprimees());
	}

	/**
	 * @return le total des contributions 
	 */
	public BigDecimal totalContributions() {
	    return Tranche.TOTAL_CONTRIBUTIONS.atSum().valueInObject(tranchesNonSupprimees());
	}
	
	/**
	 * @return le total des contributions avec les reports
	 */
	public BigDecimal totalContributionsAvecReports() {
		return Tranche.TOTAL_CONTRIBUTIONS_PLUS_REPORTS.atSum().valueInObject(tranchesNonSupprimees());
	}

	/**
	 * Retourne la fraction du montant global de la convention non utilisee dans
	 * des tranches.
	 * 
	 * @return Montant global de la convention - Somme des montants des tranches
	 *         existantes
	 */
	public BigDecimal montantDisponibleTranches() {
		BigDecimal sommeTranches = totalContributionsAPositionner();
		BigDecimal montantValide = montantTotalHt();
		BigDecimal reste = montantValide.subtract(sommeTranches);
		BigDecimal montantDisponibleTranches = reste.setScale(2, BigDecimal.ROUND_HALF_DOWN);
		return montantDisponibleTranches;
	}

	/**
	 * Retourne la fraction du montant global previsionnel des depenses de la
	 * convention non utilisee dans des tranches.
	 * 
	 * @return Montant global previsionnel des depense de la convention - Somme
	 *         des montants previsionnels en depense des tranches existantes
	 * @deprecated
	 */
	public BigDecimal montantDepenseDisponibleTranches() {
		BigDecimal sommeTranches = (BigDecimal) tranches().valueForKey("@sum.traMontantDepense");
		// Montant global = ht avenant zero + ht avenants
		BigDecimal montantValide = avenantZero().avtMontantHt().add(montantAvenantsValidesHt());

		double reste = montantValide.doubleValue() - sommeTranches.doubleValue();
		return new BigDecimal(reste).setScale(2, BigDecimal.ROUND_HALF_DOWN);
	}

	/**
	 * Retourne la fraction du montant global previsionnel des recettes de la
	 * convention non utilisee dans des tranches.
	 * 
	 * @return Montant global previsionnel des recette de la convention - Somme
	 *         des montants previsionnels en recette des tranches existantes
	 * @deprecated
	 */
	public BigDecimal montantRecetteDisponibleTranches() {
		BigDecimal sommeTranches = (BigDecimal) tranches().valueForKey("@sum.traMontantRecette");
		// Montant global = ht contrat initial + ht avenants valides
		BigDecimal montantValide = avenantZero().avtMontantHt().add(montantAvenantsValidesHt());

		double reste = montantValide.doubleValue() - sommeTranches.doubleValue();
		return new BigDecimal(reste).setScale(2, BigDecimal.ROUND_HALF_DOWN);
	}

	public void reporterCredits(NSArray<ReportInfo> reportInfos) {
		for (ReportInfo reportInfo : reportInfos) {
			reportInfo.reporter();
		}
	}

	/**
	 * Calcul du montant total apporte par les partenaires de la convention.
	 * 
	 * @return Montant total apporte par les partenaires de la convention
	 */
	public BigDecimal sommeMontantsPartenariats() {
		BigDecimal sommeMontants = (BigDecimal) contratPartenaires().valueForKey("@sum.cpMontant");
		return sommeMontants != null ? sommeMontants : new BigDecimal(0);
	}

	/**
	 * <p>
	 * Calcul du montant restant a apporter par des partenariats =
	 * Montant_Avenant_Zero - Somme_Montants_Partenariats.
	 * <p>
	 * ATTENTION : Il faut etre sur l'avenant 0 pour que le calcul s'applique
	 * sur le montant global du contrat initial.
	 */
	public BigDecimal montantDisponiblePourPartenariat() {
		if (montantTotalTtc() != null) {
			return montantTotalTtc().subtract(sommeMontantsPartenariats());
		}
		return new BigDecimal(0);
	}

	public BigDecimal montantDepenses() {
		BigDecimal total = new BigDecimal(0);
		NSArray<Tranche> tranches = tranches();
		for (Tranche tranche : tranches)
			total = total.add(tranche.sommeDepenses());
		return total;
	}

	public BigDecimal montantRecettes() {
		BigDecimal total = new BigDecimal(0);
		NSArray<Tranche> tranches = tranches();
		for (Tranche tranche : tranches)
			total = total.add(tranche.sommeRecettes());
		return total;
	}

	public BigDecimal montantBalance() {
		return montantRecettes().subtract(montantDepenses());
	}

	/**
	 * Retourne la date de debut de la convention, en prenant en compte ou non
	 * les avenants existants eventuels.
	 * 
	 * @return La date de debut de la convention.
	 */
	public NSTimestamp dateDebut(boolean avecAvenants) {
		if (avecAvenants) {
			NSMutableArray array = avenantsNonSupprimesEtValides().mutableClone();
			array.addObject(avenantZero());
			return (NSTimestamp) array.valueForKey("@min.avtDateDeb");
		} else
			return avenantZero().avtDateDeb();
	}

	/**
	 * @param avecAvenants
	 *            <code>true</code> Si les avenants sont compris
	 * @return La date de fin du contrat, en prenant éventuellement en compte le dernier avenant administratif ou financier valide
	 */
	public NSTimestamp dateFin(boolean avecAvenants) {
		NSTimestamp ret = avenantZero().avtDateFin(); //si pas d'avenants on retourne la date de la convention
		if (avecAvenants) {
			// On recherche le dernier avenant
			NSArray<Avenant> avenants = this.avenantsNonSupprimesEtValides();
			if (avenants.count() > 0) {
				NSTimestamp dateDernierAvenant = avenants.lastObject().avtDateFin();
				ret = dateDernierAvenant;
			}
		}
		return ret;
	}

	/**
	 * @see com.webobjects.eocontrol.EOCustomObject#validateForSave()
	 */
	public void validateForSave() throws ValidationException {
		if (!shouldNotValidate) {
			String validationExceptionTemplate = null;
            NSMutableDictionary<String, String> replacements = new NSMutableDictionary<String, String>();

            ContratFinancementDepensesValidation financementValidation = new ContratFinancementDepensesValidation();
            
			if (modeDeGestion() == null) {
				validationExceptionTemplate = "ModeGestionRequis";
			} else if (typeContrat() == null) {
			    validationExceptionTemplate = "TypeContratRequis";
			} else if (conObjet() == null || conObjet().length() == 0) {
			    validationExceptionTemplate = "ObjetContratRequis";
			} else if (dateDebut() == null) {
			    validationExceptionTemplate = "DatesRequises";
			} else if (etablissement() == null) {
			    validationExceptionTemplate = "EtablissementRequis";
			} else if (centreResponsabilite() == null) {
			    validationExceptionTemplate = "CentreResponsabiliteRequis";
			} else if (partenairePrincipal() == null) {
			    validationExceptionTemplate = "PartenaireRequis";
			} else if (contratPartenaires().count() <= 1) {
			    validationExceptionTemplate = "NbPartenairesInsuffisants";
			} else if (avenants().count() < 1) {
			    validationExceptionTemplate = "AvenantRequis";
			} else if (montantDisponibleTranches().doubleValue() < 0) {
			    validationExceptionTemplate = "MontantTranchesDepasseMontantGlobalContrat";
			} else if (!financementValidation.isSatisfiedBy(this)) {
			    validationExceptionTemplate = "MontantContributionsMoinsFgTropBasPourTotalPositionne";
			    replacements.takeValueForKey(totalContributionsAPositionner(), "contrib");
			    replacements.takeValueForKey(totalPositionneContrat(), "creditpos");
			} else {
				NSArray avenants = avenantsDontInitialNonSupprimes();
				Enumeration<Avenant> enumAvenants = avenants.objectEnumerator();
				while (enumAvenants.hasMoreElements()) {
					Avenant avenant = (Avenant) enumAvenants.nextElement();
					if (avenant.avtObjet() == null || avenant.avtObjet().length() == 0) {
					    validationExceptionTemplate = "AvenantObjetRequis";
					}
				}
			}
			if (validationExceptionTemplate != null) {
			    ERXValidationException ex = ERXValidationFactory.defaultFactory().createCustomException(this, validationExceptionTemplate);
			    ex.setContext(replacements);
				throw ex;
			}
		}
		super.validateForSave();
	}
	
	/**
	 * Calcul du numero d'index suivant.
	 */
	public void validateForInsert() throws ValidationException {
		// System.out.println("validateForInsert CONTRAT serveur");

		if (exerciceCocktail() == null) {
			throw new ValidationException("L'exercice doit etre renseigne avant enregistrement.");
		}

		if (projetContrat() == null || projetContrat().count() == 0) {

			Projet defaultProject = Projet.defaultProjet(editingContext());
			FactoryProjet projectFactory = new FactoryProjet(editingContext());

			try {
				projectFactory.ajouterContrat(defaultProject, this);
			} catch (ExceptionUtilisateur e) {
				// TODO Auto-generated catch block

				throw new ValidationException(e.getMessage());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				throw new ValidationException(e.getMessage());
			}

		}

		Integer index;
		try {
			index = getNewIndexContrat(editingContext(), exerciceCocktail().exeExercice(), typeClassificationContrat().getPrimaryKey());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new ValidationException("La recherche de l'indice du contrat a echoue.");
		}

		if (index == null) {
			throw new ValidationException("La recherche de l'indice du contrat a echoue.");
		}
		// System.out.println("validateContratForInsert CONTRAT index = "+index);

		this.setConIndex(index);

		super.validateForInsert();
	}

	/**
	 * Determine la prochaine valeur de CON_INDEX.
	 * 
	 * @param ec
	 *            Editing context de travail.
	 * @param exercice
	 *            Exercice a prendre en compte.
	 * @param typeClassification
	 *            type de la convention.
	 * @return La prochaine valeur de CON_INDEX.
	 */
	public static Integer getNewIndexContrat(EOEditingContext ec, Number exercice, Number typeClassification) {
		// System.out.println("getNewIndexContrat");
		NSArray array = EOUtilities.rawRowsForSQL(ec, "Cocowork", "select max(con_index) from ACCORDS.contrat where exe_ordre = " + exercice + " and tcc_id = "
				+ typeClassification, null);

		Integer index = null;
		if (((NSDictionary) array.objectAtIndex(0)).valueForKey("MAX_CON_INDEX_") != NSKeyValueCoding.NullValue) {
			Number value = (Number) ((NSDictionary) array.objectAtIndex(0)).valueForKey("MAX_CON_INDEX_");
			int intIndex = value.intValue();
			index = new Integer(intIndex);
		}
		if (index == null) {
			index = new Integer(1);
		} else {
			index = new Integer(index.intValue() + 1);
		}
		// System.out.println("dictionnaire = "+(NSDictionary)array.objectAtIndex(0));

		return (Integer) index;
	}

	public ModeGestion modeDeGestion() {
		return avenantZero() != null ? avenantZero().modeGestionRelationship() : null;
	}

	public void setEtablissementRelationship(EOStructure etablissement, Integer persIdUtilisateur) {
		EOStructure oldEtablissement = etablissement();

		if (oldEtablissement != null && !oldEtablissement.equals(etablissement)) {
			// Changement d'etablissement
			FactoryContratPartenaire fcp = new FactoryContratPartenaire(editingContext(), Boolean.TRUE);
			ContratPartenaire ancienPartenairePrincipal = partenaireForPersId(oldEtablissement.persId());
			try {
				fcp.supprimerContratPartenaire(ancienPartenairePrincipal, persIdUtilisateur);
			} catch (ExceptionUtilisateur e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (Exception e) {	
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		// Chgt d'etablissement responsable
		// setCentreResponsabilite(null);

		if (etablissement != null) {
			// Ajout du nouvel etablissement en tant que partenaire si il ne
			// l'est pas encore
			NSArray<ContratPartenaire> partenariats = (NSArray<ContratPartenaire>) EOQualifier.filteredArrayWithQualifier(contratPartenaires(),
					EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(etablissement.persId())));
			if (partenariats != null && partenariats.count() == 0) {
				FactoryConvention fc = new FactoryConvention(editingContext(), Boolean.TRUE);
				try {
					ContratPartenaire contratPartenaire = fc.ajouterPartenaireContractant(this, etablissement, Boolean.TRUE, BigDecimal.valueOf(0));
					if (centreResponsabilite() != null) {
						// Si le service gestionnaire existe, il devient contact
						// de l'etablissement gestionnaire
						FactoryContratPartenaire fcp = new FactoryContratPartenaire(editingContext(), Boolean.TRUE);
						fcp.ajouterContact(contratPartenaire, centreResponsabilite(), null);
					}
				} catch (ExceptionUtilisateur e) {
					e.printStackTrace();
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			} else {
				if(partenairesPrincipaux().isEmpty()) {
					ContratPartenaire contratPartenaire = ERXArrayUtilities.firstObject(partenariats);
					contratPartenaire.setCpPrincipalBoolean(true);
				}
			}
		}
		super.setEtablissementRelationship(etablissement);
	}

	public void setCentreResponsabiliteRelationship(EOStructure centreGestionnaire, Integer persIdUtilisateur) {
		EOStructure oldCentre = super.centreResponsabilite();
		super.setCentreResponsabiliteRelationship(centreGestionnaire);
		if (etablissement() != null) {
			// Recherche du partenaire principal interne (etablissement
			// gestionnaire)
			ContratPartenaire partenairePrincipal = partenaireForPersId(etablissement().persId());
			FactoryContratPartenaire fcp = new FactoryContratPartenaire(editingContext(), Boolean.TRUE);
			if (oldCentre != null && !oldCentre.equals(centreGestionnaire)) {
				// RAZ du contact correspondant au centre gestionnaire
				// Recherche parmi les contacts
				NSArray contacts = partenairePrincipal.contratPartContacts();
				EOKeyValueQualifier qual = new EOKeyValueQualifier(ContratPartContact.PERS_ID_CONTACT_KEY, EOQualifier.QualifierOperatorEqual, oldCentre.persId());
				contacts = EOQualifier.filteredArrayWithQualifier(contacts, qual);
				if (contacts != null && contacts.count() > 0) {
					try {
						fcp.supprimerContact((ContratPartContact) contacts.lastObject(), partenairePrincipal, persIdUtilisateur);
						if (oldCentre.globalID().isTemporary()) {
							editingContext().forgetObject(oldCentre);
						}
						// editingContext().insertObject(oldCentre);
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			if (centreGestionnaire != null && partenairePrincipal.contactForPersId(centreGestionnaire.persId()) == null) {
				// setEtablissementRelationship(centreGestionnaire.etablissement());
				EOQualifier qualifier = new EOKeyValueQualifier(EOAssociation.ASS_CODE_KEY, EOKeyValueQualifier.QualifierOperatorLike, "REPRGESTPART");
				EOAssociation association = EOAssociation.fetchByQualifier(editingContext(), qualifier);
				try {
					fcp.ajouterContact(partenairePrincipal, centreGestionnaire, new NSArray(association));
				} catch (ExceptionUtilisateur e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		// MAJ de la relation centreResponsabilite sur tous les avenants
		NSArray avenants = avenantsDontInitialNonSupprimes();
		Enumeration enumAvenants = avenants.objectEnumerator();
		while (enumAvenants.hasMoreElements()) {
			Avenant avenant = (Avenant) enumAvenants.nextElement();
			avenant.setCentreResponsabiliteRelationship(centreGestionnaire);
		}
	}

	public ContratPartenaire partenaireForPersonne(IPersonne personne) {
		return partenaireForPersId(personne.persId());
	}
	
	public ContratPartenaire partenaireForPersId(Integer persId) {
		ContratPartenaire partenaire = null;
		if (persId != null) {
			NSArray partenaires = EOQualifier.filteredArrayWithQualifier(contratPartenaires(), EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(persId)));
			if (partenaires != null && partenaires.count() == 1) {
				partenaire = (ContratPartenaire) partenaires.lastObject();
			}
		}
		return partenaire;
	}

	public ContratPartenaire partenaireCentreResponsabilite() {
		ContratPartenaire partenaireCentreResponsabilite = null;
		EOStructure centreResponsabilite = centreResponsabilite();
		if (centreResponsabilite != null) {
			NSArray partenaires = EOQualifier.filteredArrayWithQualifier(contratPartenaires(),
					EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(centreResponsabilite.persId())));
			if (partenaires != null && partenaires.count() == 1) {
				partenaireCentreResponsabilite = (ContratPartenaire) partenaires.lastObject();
			}
		}
		return partenaireCentreResponsabilite;
	}

	public ContratPartenaire partenairePrincipal() {
		ContratPartenaire partenairePrincipal = null;
		NSArray partenaires = partenairesPrincipaux();
		if (partenaires != null && partenaires.count() >= 1) {
			partenairePrincipal = (ContratPartenaire) partenaires.lastObject();
		}
		return partenairePrincipal;
	}

	public NSArray<ContratPartenaire> partenairesPrincipaux() {
		return contratPartenaires(ContratPartenaire.CP_PRINCIPAL.eq(ContratPartenaire.PARTENAIRE_PRINCIPAL_OUI));
	}

	public NSArray<ContratPartenaire> partenairesNonPrincipaux() {
		return contratPartenaires(ContratPartenaire.CP_PRINCIPAL.ne(ContratPartenaire.PARTENAIRE_PRINCIPAL_OUI));
	}

	public NSArray<EOStructure> structuresPartenairesForAssociation(EOAssociation association) {
		NSArray<EOStructure> lesStructures = (NSArray<EOStructure>) repartAssociationPartenairesForAssociation(association).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
		return lesStructures;
	}

	@Override
	public void setConDateValidAdm(NSTimestamp value) {
		super.setConDateValidAdm(value);
		if (avenantZero() != null) {
			avenantZero().setAvtDateValidAdm(value);
		}
	}

	public NSTimestamp dateDebut() {
		return dateDebut(true);
	}

	public void setDateDebut(NSTimestamp date) {
		// Si le contrat n'est pas signé, on renseigne la date de debut de
		// l'avtZero
		if (!isSigne())
			avenantZero().setAvtDateDeb(date);
	}

	public NSTimestamp dateFin() {
		return dateFin(true);
	}

	public void setDateFin(NSTimestamp date) {
		// Si le contrat n'est pas signé, on renseigne la date de fin de
		// l'avtZero
		if (!isSigne())
			avenantZero().setAvtDateFin(date);
	}

	public BigDecimal montantHt() {
		// FIXME montant ht = Somme(montant ht de chaque avenant)
		if (avenantZero() == null) {
			return null;
		}
		if(avenantZero().avtMontantHt() == null) {
			return null;
		}
		return avenantZero().avtMontantHt().add(montantAvenantsValidesHt());
	}

	public BigDecimal montantTtc() {
		// FIXME montant ttc = Somme(montant ttc de chaque avenant)
		if (avenantZero() == null)
			return null;
		return avenantZero().avtMontantTtc().add(montantAvenantsValidesTtc());
	}

	public boolean isSigne() {
		return conDateValidAdm() != null && avenantZero() != null && avenantZero().isSigne();
	}

	public boolean isConsultablePar(EOUtilisateur user) {
		CocoworkApplicationUser cocoworkApplicationUser = new CocoworkApplicationUser(editingContext(), user);
		return isCreateur(user) 
				|| isAdministrateurCentreGestionnaire(user) 
				|| isPartenaireOuContact(user) 
				|| hasDroitsComposanteBudgetaire(user) 
				|| isDestinataireDocument(user)
				// TODO Pas tres propre. A refactorer 
				|| cocoworkApplicationUser.hasDroitSuperAdmin()
				|| cocoworkApplicationUser.hasDroitConsultationTousLesContratsEtAvenants();
	}

	public boolean isModifiablePar(EOUtilisateur user) {
		CocoworkApplicationUser cocoworkApplicationUser = new CocoworkApplicationUser(editingContext(), user);
		// TODO Pas tres propre. A refactorer 
		return isCreateur(user) || isAdministrateurCentreGestionnaire(user) || cocoworkApplicationUser.hasDroitSuperAdmin();
	}

	public boolean isBudgetModifiablePar(EOUtilisateur user) {
		return hasDroitsComposanteBudgetaire(user);
	}

	private boolean isCreateur(EOUtilisateur user) {
		return this.utilisateurCreation() != null && ERXEOControlUtilities.eoEquals(this.utilisateurCreation(), user);
	}

	private boolean isAdministrateurCentreGestionnaire(EOUtilisateur user) {
		NSArray individusSecretaires = (NSArray) centreResponsabilite().toSecretariats().valueForKey(EOSecretariat.NO_INDIVIDU_KEY);
		return individusSecretaires.containsObject(user.noIndividu());
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private boolean hasDroitsComposanteBudgetaire(EOUtilisateur user) {
		// On récupère les organs du user
		org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur userJefyAdmin = FinderUtilisateur.getUtilisateur(editingContext(), user.utlOrdre());
		NSArray organs = (NSArray) userJefyAdmin.utilisateurOrgans().valueForKey(EOUtilisateurOrgan.ORGAN_KEY);
		// On récupère les TrancheBudget
		NSArray<TrancheBudget> trancheBudgets = ERXArrayUtilities.flatten((NSArray) tranches().valueForKey(Tranche.TRANCHE_BUDGETS_NON_SUPP_KEY));
		NSArray<TrancheBudget> trancheBudgetRecs = ERXArrayUtilities.flatten((NSArray) tranches().valueForKey(Tranche.TRANCHE_BUDGETS_REC_NON_SUPP_KEY));
		// On récupère les organs
		NSMutableArray<EOOrgan> organsBudget = ((NSArray<EOOrgan>) trancheBudgets.valueForKey(TrancheBudget.ORGAN_KEY)).mutableClone();
		organsBudget.addObjectsFromArray((NSArray<EOOrgan>) trancheBudgetRecs.valueForKey(TrancheBudgetRec.ORGAN_KEY));
		// true si l'un des organ du user correspond à l'un des organs du budget
		return ERXArrayUtilities.arrayContainsAnyObjectFromArray(organs, organsBudget);
	}

	private boolean isPartenaireOuContact(EOUtilisateur user) {
		NSArray part = (NSArray) this.contratPartenaires().valueForKey(ContratPartenaire.PERS_ID_KEY);

		return part.containsObject(user.toPersonne().persId());
	}

	private boolean isDestinataireDocument(EOUtilisateur user) {
		// TODO A coder
		return false;
	}

	public void setConObjet(String aValue) {
		super.setConObjet(aValue);
		if (avenantZero() != null) {
			avenantZero().setAvtObjet(aValue);
		}
	}

	public void setConObservations(String aValue) {
		super.setConObservations(aValue);
		if (avenantZero() != null) {
			avenantZero().setAvtObservations(aValue);
		}
	}

	public void setConReferenceExterne(String aValue) {
		super.setConReferenceExterne(aValue);
		if (avenantZero() != null) {
			avenantZero().setAvtRefExterne(aValue);
		}
	}

	public void setReconductible(Boolean value) {
		if (value != null && value.booleanValue()) {
			TypeReconduction tre = (TypeReconduction) EOUtilities.objectMatchingKeyAndValue(editingContext(), TypeReconduction.ENTITY_NAME, TypeReconduction.TR_ID_INTERNE_KEY,
					"EXPRESSE");
			setTypeReconductionRelationship(tre);
		} else {
			TypeReconduction trnp = (TypeReconduction) EOUtilities.objectMatchingKeyAndValue(editingContext(), TypeReconduction.ENTITY_NAME, TypeReconduction.TR_ID_INTERNE_KEY,
					"NON_PREVUE");
			setTypeReconductionRelationship(trnp);
		}
	}

	public Boolean reconductible() {
		return (typeReconduction() != null && typeReconduction().trIdInterne().equals("EXPRESSE") ? Boolean.TRUE : Boolean.FALSE);
	}

	public void updateDuree() {
		if (dateDebut() == null || dateFin() == null)
			setConDuree(null);
		else {
			GregorianCalendar deb = new GregorianCalendar();
			deb.setTime(dateDebut());

			GregorianCalendar fin = new GregorianCalendar();
			fin.setTime(dateFin());

			long diffEnJour = (fin.getTimeInMillis() - deb.getTimeInMillis()) / (24 * 60 * 60 * 1000);
			int duree = (new Long(diffEnJour)).intValue();

			if (duree >= 0)
				setConDuree(new Integer(duree + 1));
			else
				setConDuree(null);
		}
	}

	public void updateDateFin() throws ExceptionUtilisateur, Exception {
		initDatesEtDuree(dateDebut(), null, conDuree());
	}

	public void initDatesEtDuree(NSTimestamp dateDebut, NSTimestamp dateFin, Integer duree) throws ExceptionUtilisateur {

		if (avenantZero() == null || avenantsCount() > 0)
			throw new ExceptionUtilisateur("Cette operation n'est possible que si la convention comporte un unique avenant qui est initial");

		if (dateDebut != null && dateFin != null && dateFin.before(dateDebut))
			throw new ExceptionUtilisateur("La date de fin ne peut etre anterieur à la date de debut");

		NSTimestamp finCalcule;
		NSTimestamp debutCalcule;

		if (dateDebut == null) {
			debutCalcule = new NSTimestamp();
			if (dateFin == null) {
				if (duree == null) {
					// - Initialisation date fin : today + 1 an
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(debutCalcule);
					calendar.add(GregorianCalendar.YEAR, 1);
					calendar.add(GregorianCalendar.DATE, -1);
					calendar.setTime(calendar.getTime());
					finCalcule = new NSTimestamp(calendar.getTime());

				} else {
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(debutCalcule);
					calendar.add(GregorianCalendar.DATE, duree.intValue() - 1);
					calendar.setTime(calendar.getTime());
					finCalcule = new NSTimestamp(calendar.getTime());
				}
			} else {
				finCalcule = dateFin;
				if (duree == null) {
					// - Initialisation date fin : today + 1 an
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(finCalcule);
					calendar.add(GregorianCalendar.YEAR, -1);
					calendar.add(GregorianCalendar.DATE, +1);
					calendar.setTime(calendar.getTime());
					debutCalcule = new NSTimestamp(calendar.getTime());

				} else {
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(finCalcule);
					calendar.add(GregorianCalendar.DATE, -(duree.intValue() - 1));
					calendar.setTime(calendar.getTime());
					debutCalcule = new NSTimestamp(calendar.getTime());
				}
			}
		} else {
			debutCalcule = dateDebut;
			if (dateFin == null) {
				if (duree == null) {
					// - Initialisation date fin : today + 1 an
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(debutCalcule);
					calendar.add(GregorianCalendar.YEAR, 1);
					calendar.add(GregorianCalendar.DATE, -1);
					calendar.setTime(calendar.getTime());
					finCalcule = new NSTimestamp(calendar.getTime());

				} else {
					GregorianCalendar calendar = new GregorianCalendar();
					calendar.setTime(debutCalcule);
					calendar.add(GregorianCalendar.DATE, duree.intValue() - 1);
					calendar.setTime(calendar.getTime());
					finCalcule = new NSTimestamp(calendar.getTime());
				}
			} else {
				finCalcule = dateFin;
			}
		}

		avenantZero().setAvtDateDeb(debutCalcule);
		avenantZero().setAvtDateFin(finCalcule);

	}

	/**
	 * @return the sansAvis
	 */
	public String sansAvis() {
		String sansAvis = "N";
		if ((avisFavorable() == null || !avisFavorable().equals("O")) && (avisDefavorable() == null || !avisDefavorable().equals("O"))) {
			sansAvis = "O";
		}
		return sansAvis;
	}

	/**
	 * @param sansAvis
	 *            the sansAvis to set
	 */
	public void setSansAvis(String sansAvis) {
		this.sansAvis = sansAvis;
		if (sansAvis != null && sansAvis.equals("O")) {
			setAvisFavorable(null);
			setAvisDefavorable(null);
		}
	}

	public void setShouldNotValidate(boolean shouldNotValidate) {
		this.shouldNotValidate = shouldNotValidate;
		if (avenantZero() != null) {
			avenantZero().setShouldNotValidate(shouldNotValidate);
		}
	}

	public boolean estSupprime() {
		return CON_SUPPR_OUI.equals(conSuppr());
	}

	@SuppressWarnings("unchecked")
	private NSArray<IPersonne> responsables(String typeResp) {
    NSArray<ContratPartContact> contacts = ERXArrayUtilities.flatten((NSArray) contratPartenaires().valueForKey(ContratPartenaire.CONTRAT_PART_CONTACTS_KEY), true);
    NSArray<Integer> persIds = (NSArray<Integer>) contacts.valueForKey(ContratPartContact.PERS_ID_CONTACT.key());
    EOQualifier qualifier = EORepartAssociation.PERS_ID.in(persIds)
      .and(EORepartAssociation.TO_ASSOCIATION.dot(EOAssociation.ASS_CODE).eq(typeResp))
      .and(EORepartAssociation.TO_STRUCTURE.eq(groupePartenaire()));
    NSArray<IPersonne> responsables = (NSArray<IPersonne>) EORepartAssociation.fetchAll(editingContext(), qualifier).valueForKey(EORepartAssociation.TO_PERSONNE_ELT_KEY);
    return ERXS.sorted(responsables, ERXS.ascInsensitives(IPersonne.NOM_PRENOM_AFFICHAGE_KEY));
	}

	public NSArray<IPersonne> responsablesAdministratifs() {
	  return responsables(ASS_CODE_RESPADMIN);
	}

	public NSArray<IPersonne> responsablesScientifiques() {
		return responsables(ASS_CODE_RESPSCIENTIF);
	}

	public boolean hasTrancheWithPrevisionEnAttente() {
		boolean result = false;
		NSArray<EOPrevisionBudget> prevs = ERXArrayUtilities.flatten((NSArray<EOPrevisionBudget>) tranches().valueForKey(Tranche.PREVISIONS_BUDGET_KEY));
		for (EOPrevisionBudget prev : prevs) {
			if (prev.isEnAttente()) {
				result = true;
				break;
			}
		}
		return result;
	}

	public boolean isModeRA() {
		return avenantZero().modeGestionRelationship().isModeRA();
	}

	public boolean isModeBudgetSimple() {
		return !isModeBudgetAvance();
	}

	public boolean isModeBudgetAvance() {
		// S'il y a des depenses saisis sur les convention simple migrees
		NSArray<SbDepense> _depenses = SbDepense.fetchAll(editingContext(), SbDepense.TRANCHE.dot(Tranche.CONTRAT).eq(this), null);
		return !ERXProperties.booleanForKey(Constantes.BUDGET_LIGHT_CONV_SIMPLE_PARAM) || isModeRA() || !_depenses.isEmpty();
	}

	public NSArray<EOBrouillard> brouillardsValides() {
		return brouillards(ERXQ.equals(EOBrouillard.BRO_ETAT_KEY, EOBrouillard.BRO_ETAT_VISE));
	}

	public NSArray<EOBrouillard> brouillardsEnAttente() {
		return brouillards(ERXQ.equals(EOBrouillard.BRO_ETAT_KEY, EOBrouillard.BRO_ETAT_ATTENTE));
	}

	public BigDecimal sommeBrouillardsValides() {
		return (BigDecimal) brouillardsValides().valueForKey("@sum." + EOBrouillard.MONTANT_KEY);
	}

	private void verificationMontantRA(BigDecimal montant) {
		// On vérifie avant tout que le montant demandé est inférieur ou égal à
		// 30% du montant de la convention
		// et que l'ensemble des demandes ne dépasse pas le montant de la
		// convention bien sûr
		if (montant != null) {
			BigDecimal pct = montantTotalHt().multiply(BigDecimal.valueOf(0.30));
			if (montant.compareTo(pct) == 1)
				throw new ValidationException("Le montant de la demande ne doit pas dépasser 30% du montant global de la convention");
			if (montant.add(sommeBrouillardsValides()).compareTo(montantTotalHt()) == 1)
				throw new ValidationException("Les montants déjà pris en charge et le montant demandé dépassent le montant global de la convention");
		} else {
			throw new ValidationException("Un montant valide doit être saisie");
		}
	}

	public void creerCrForRA(EOOrgan parentOrgan) throws Exception {
		if (this.organComposante() == null && parentOrgan != null) {
			EOOrgan organCr = EOOrgan.creerNewEOOrgan(editingContext(), parentOrgan, this.numeroContrat());
			EOTypeOrgan typeOrgan = EOTypeOrgan.getTypeOrganForLibelle(editingContext(), EOTypeOrgan.TYPE_CONVENTION_RA);
			organCr.setTypeOrganRelationship(typeOrgan);
			this.setOrganComposanteRelationship(organCr);
		}
	}

	public Integer niveauOrganMin() {
		if (isModeRA())
			return EOOrgan.ORG_NIV_4;
		else
			return EOOrgan.ORG_NIV_3;
	}

	public boolean isValidAdm() {
		return this.conDateValidAdm() != null;
	}

	public void avantModification(EOUtilisateur utilisateur) {
		setUtilisateurModifRelationship(utilisateur);
		String libelleGroupe = "Partenaires de l'acte " + numeroContrat();
		EOStructure groupePartenaires = groupePartenaire();
		// Renommer le groupe partenaire
		groupePartenaires.setStrAffichage(libelleGroupe);
		// Maj des dates des differents roles des differents partenaires et
		// contacts
		NSArray<EORepartStructure> partenairesEtContacts = groupePartenaires.toRepartStructuresElts();
		Enumeration<EORepartStructure> enumPartenairesEtContacts = partenairesEtContacts.objectEnumerator();
		while (enumPartenairesEtContacts.hasMoreElements()) {
			EORepartStructure eoRepartStructure = (EORepartStructure) enumPartenairesEtContacts.nextElement();
			NSArray<EORepartAssociation> lesRoles = eoRepartStructure.toRepartAssociations(null);
			Enumeration<EORepartAssociation> enumLesRoles = lesRoles.objectEnumerator();
			while (enumLesRoles.hasMoreElements()) {
				EORepartAssociation eoRepartAssociation = (EORepartAssociation) enumLesRoles.nextElement();
				eoRepartAssociation.setRasDOuverture(dateDebut());
				eoRepartAssociation.setRasDFermeture(dateFin());
			}
		}
	}

	public static NSArray<EOOrgan> autresOrgansBudget(EOEditingContext edc) {
		NSArray<String> orgIds = ERXProperties.componentsSeparatedByString(Constantes.AUTRES_ORGANS_BUDGET_PARAM, ",");

		NSArray<EOOrgan> organs = new NSMutableArray<EOOrgan>();
		for (String orgId : orgIds) {
			Integer id = ERXStringUtilities.integerWithString(orgId);
			if (id != null) {
				EOOrgan organ = (EOOrgan) EOUtilities.objectWithPrimaryKeyValue(edc, EOOrgan.ENTITY_NAME, id);
				organs.add(organ);
			}
		}
		return organs;
	}

	public NSArray<ContratPartenaire> partenairesForAssociation(EOAssociation association) {
		return partenairesForAssociations(new NSArray<EOAssociation>(association));
	}

	public NSArray<ContratPartenaire> partenairesForAssociations(NSArray<EOAssociation> lesAssociations) {

		NSArray<Integer> lesPersIds = (NSArray<Integer>) repartAssociationPartenairesForAssociations(lesAssociations).valueForKey(EORepartAssociation.PERS_ID_KEY);
		EOQualifier qualPartenaire = ContratPartenaire.PERS_ID.in(lesPersIds);
		NSArray<ContratPartenaire> lesPartenaires = EOQualifier.filteredArrayWithQualifier(contratPartenaires(), qualPartenaire);
		return lesPartenaires;

	}

	public NSArray<EORepartAssociation> repartAssociationPartenairesForAssociationAuxDates(EOAssociation association, NSTimestamp aLaDateDebut, NSTimestamp aLaDateFin) {
		NSArray<EORepartAssociation> lesRepartAsso = repartAssociationPartenairesForAssociationsAuxDates(new NSArray<EOAssociation>(association), aLaDateDebut, aLaDateFin);
		return lesRepartAsso;
	}

	public NSArray<EORepartAssociation> repartAssociationPartenairesForAssociation(EOAssociation association) {

		NSArray<EORepartAssociation> lesRepartAsso = repartAssociationPartenairesForAssociations(new NSArray<EOAssociation>(association));
		return lesRepartAsso;
	}

	public NSArray<EORepartAssociation> repartAssociationPartenairesForAssociations(NSArray<EOAssociation> lesAssociations) {
		NSArray<EORepartAssociation> lesRepartAsso = repartAssociationPartenairesForAssociationsAuxDates(lesAssociations, null, null);
		if (lesRepartAsso == null) {
			return null;
		} else {
			return lesRepartAsso;
		}
	}

	public NSArray<EORepartAssociation> repartAssociationPartenairesForAssociationsAuxDates(NSArray<EOAssociation> lesAssociations, NSTimestamp aLaDateDebut, NSTimestamp aLaDateFin) {
		EOQualifier qualRepartAssoDateDebut = null;
		EOQualifier qualRepartAssoDateFin = null;
		EOQualifier qualRepartAssoDate = null;
		EOQualifier qualRepartAsso = null;

		NSArray<Integer> lesPersId = (NSArray<Integer>) contratPartenaires().valueForKey(ContratPartenaire.PERS_ID_KEY);

		EOQualifier qualRepartAsso1 = ERXQ.and(ERXQ.equals(EORepartAssociation.TO_STRUCTURE_KEY, groupePartenaire()),
				ERXQ.inObjects(EORepartAssociation.PERS_ID_KEY, lesPersId.objects()), ERXQ.inObjects(EORepartAssociation.TO_ASSOCIATION_KEY, lesAssociations.objects()));

		if (aLaDateDebut != null)
			qualRepartAssoDateDebut = ERXQ.and(ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, aLaDateDebut),
					ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, aLaDateDebut));
		if (aLaDateFin != null)
			qualRepartAssoDateFin = ERXQ.and(ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, aLaDateFin),
					ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, aLaDateFin));
		if (qualRepartAssoDateDebut != null && qualRepartAssoDateFin != null)
			qualRepartAssoDate = ERXQ.or(qualRepartAssoDateDebut, qualRepartAssoDateFin);
		else {
			if (qualRepartAssoDateDebut != null)
				qualRepartAssoDate = qualRepartAssoDateDebut;
			if (qualRepartAssoDateFin != null)
				qualRepartAssoDate = qualRepartAssoDateFin;
		}

		if (qualRepartAssoDate == null)
			qualRepartAsso = qualRepartAsso1;
		else
			qualRepartAsso = ERXQ.and(qualRepartAsso1, qualRepartAssoDate);

		NSArray<EORepartAssociation> lesRepartAsso = EORepartAssociation.fetchAll(this.editingContext(), qualRepartAsso);
		return lesRepartAsso;
	}

}