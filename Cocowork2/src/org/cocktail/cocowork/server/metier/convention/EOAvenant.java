// DO NOT EDIT.  Make changes to Avenant.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAvenant extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWAvenant";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> AVT_DATE_CREATION = new ERXKey<NSTimestamp>("avtDateCreation");
  public static final ERXKey<NSTimestamp> AVT_DATE_DEB = new ERXKey<NSTimestamp>("avtDateDeb");
  public static final ERXKey<NSTimestamp> AVT_DATE_DEB_EXEC = new ERXKey<NSTimestamp>("avtDateDebExec");
  public static final ERXKey<NSTimestamp> AVT_DATE_FIN = new ERXKey<NSTimestamp>("avtDateFin");
  public static final ERXKey<NSTimestamp> AVT_DATE_FIN_EXEC = new ERXKey<NSTimestamp>("avtDateFinExec");
  public static final ERXKey<NSTimestamp> AVT_DATE_MODIF = new ERXKey<NSTimestamp>("avtDateModif");
  public static final ERXKey<NSTimestamp> AVT_DATE_SIGNATURE = new ERXKey<NSTimestamp>("avtDateSignature");
  public static final ERXKey<NSTimestamp> AVT_DATE_VALID_ADM = new ERXKey<NSTimestamp>("avtDateValidAdm");
  public static final ERXKey<java.math.BigDecimal> AVT_DEP_ANTERIEURE = new ERXKey<java.math.BigDecimal>("avtDepAnterieure");
  public static final ERXKey<Integer> AVT_INDEX = new ERXKey<Integer>("avtIndex");
  public static final ERXKey<String> AVT_LIMITATIF = new ERXKey<String>("avtLimitatif");
  public static final ERXKey<String> AVT_LUCRATIVITE = new ERXKey<String>("avtLucrativite");
  public static final ERXKey<java.math.BigDecimal> AVT_MNT_RELIQUAT = new ERXKey<java.math.BigDecimal>("avtMntReliquat");
  public static final ERXKey<String> AVT_MONNAIE = new ERXKey<String>("avtMonnaie");
  public static final ERXKey<java.math.BigDecimal> AVT_MONTANT_GLOBAL = new ERXKey<java.math.BigDecimal>("avtMontantGlobal");
  public static final ERXKey<java.math.BigDecimal> AVT_MONTANT_HT = new ERXKey<java.math.BigDecimal>("avtMontantHt");
  public static final ERXKey<java.math.BigDecimal> AVT_MONTANT_TTC = new ERXKey<java.math.BigDecimal>("avtMontantTtc");
  public static final ERXKey<String> AVT_OBJET = new ERXKey<String>("avtObjet");
  public static final ERXKey<String> AVT_OBJET_COURT = new ERXKey<String>("avtObjetCourt");
  public static final ERXKey<String> AVT_OBSERVATIONS = new ERXKey<String>("avtObservations");
  public static final ERXKey<java.math.BigDecimal> AVT_PCT_AVANCE = new ERXKey<java.math.BigDecimal>("avtPctAvance");
  public static final ERXKey<java.math.BigDecimal> AVT_REC_ANTERIEURE = new ERXKey<java.math.BigDecimal>("avtRecAnterieure");
  public static final ERXKey<String> AVT_RECUP_TVA = new ERXKey<String>("avtRecupTva");
  public static final ERXKey<String> AVT_REF_EXTERNE = new ERXKey<String>("avtRefExterne");
  public static final ERXKey<Integer> AVT_STAT_RELIQUAT = new ERXKey<Integer>("avtStatReliquat");
  public static final ERXKey<String> AVT_SUPPR = new ERXKey<String>("avtSuppr");
  public static final ERXKey<Integer> TA_ORDRE = new ERXKey<Integer>("taOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantDocument> AVENANT_DOCUMENTS = new ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantDocument>("avenantDocuments");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> AVENANT_DOMAINE_SCIENTIFIQUES = new ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique>("avenantDomaineScientifiques");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> AVENANT_EVENEMENTS = new ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantEvenement>("avenantEvenements");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> AVENANT_TYPE_STATS = new ERXKey<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat>("avenantTypeStats");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> CENTRE_RESPONSABILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("centreResponsabilite");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.grhum.Discipline> DISCIPLINE = new ERXKey<org.cocktail.cocowork.server.metier.grhum.Discipline>("discipline");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.ModeGestion> MODE_GESTION = new ERXKey<org.cocktail.cocowork.server.metier.convention.ModeGestion>("modeGestion");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTva> TVA = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTva>("tva");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeAvenant> TYPE_AVENANT = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeAvenant>("typeAvenant");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_MODIF = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurModif");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_VALID_ADM = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurValidAdm");
  public static final ERXKey<EOGenericRecord> V_PART_PRINC = new ERXKey<EOGenericRecord>("vPartPrinc");

  // Attributes
  public static final String AVT_DATE_CREATION_KEY = AVT_DATE_CREATION.key();
  public static final String AVT_DATE_DEB_KEY = AVT_DATE_DEB.key();
  public static final String AVT_DATE_DEB_EXEC_KEY = AVT_DATE_DEB_EXEC.key();
  public static final String AVT_DATE_FIN_KEY = AVT_DATE_FIN.key();
  public static final String AVT_DATE_FIN_EXEC_KEY = AVT_DATE_FIN_EXEC.key();
  public static final String AVT_DATE_MODIF_KEY = AVT_DATE_MODIF.key();
  public static final String AVT_DATE_SIGNATURE_KEY = AVT_DATE_SIGNATURE.key();
  public static final String AVT_DATE_VALID_ADM_KEY = AVT_DATE_VALID_ADM.key();
  public static final String AVT_DEP_ANTERIEURE_KEY = AVT_DEP_ANTERIEURE.key();
  public static final String AVT_INDEX_KEY = AVT_INDEX.key();
  public static final String AVT_LIMITATIF_KEY = AVT_LIMITATIF.key();
  public static final String AVT_LUCRATIVITE_KEY = AVT_LUCRATIVITE.key();
  public static final String AVT_MNT_RELIQUAT_KEY = AVT_MNT_RELIQUAT.key();
  public static final String AVT_MONNAIE_KEY = AVT_MONNAIE.key();
  public static final String AVT_MONTANT_GLOBAL_KEY = AVT_MONTANT_GLOBAL.key();
  public static final String AVT_MONTANT_HT_KEY = AVT_MONTANT_HT.key();
  public static final String AVT_MONTANT_TTC_KEY = AVT_MONTANT_TTC.key();
  public static final String AVT_OBJET_KEY = AVT_OBJET.key();
  public static final String AVT_OBJET_COURT_KEY = AVT_OBJET_COURT.key();
  public static final String AVT_OBSERVATIONS_KEY = AVT_OBSERVATIONS.key();
  public static final String AVT_PCT_AVANCE_KEY = AVT_PCT_AVANCE.key();
  public static final String AVT_REC_ANTERIEURE_KEY = AVT_REC_ANTERIEURE.key();
  public static final String AVT_RECUP_TVA_KEY = AVT_RECUP_TVA.key();
  public static final String AVT_REF_EXTERNE_KEY = AVT_REF_EXTERNE.key();
  public static final String AVT_STAT_RELIQUAT_KEY = AVT_STAT_RELIQUAT.key();
  public static final String AVT_SUPPR_KEY = AVT_SUPPR.key();
  public static final String TA_ORDRE_KEY = TA_ORDRE.key();
  // Relationships
  public static final String AVENANT_DOCUMENTS_KEY = AVENANT_DOCUMENTS.key();
  public static final String AVENANT_DOMAINE_SCIENTIFIQUES_KEY = AVENANT_DOMAINE_SCIENTIFIQUES.key();
  public static final String AVENANT_EVENEMENTS_KEY = AVENANT_EVENEMENTS.key();
  public static final String AVENANT_TYPE_STATS_KEY = AVENANT_TYPE_STATS.key();
  public static final String CENTRE_RESPONSABILITE_KEY = CENTRE_RESPONSABILITE.key();
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String DISCIPLINE_KEY = DISCIPLINE.key();
  public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();
  public static final String MODE_GESTION_KEY = MODE_GESTION.key();
  public static final String TVA_KEY = TVA.key();
  public static final String TYPE_AVENANT_KEY = TYPE_AVENANT.key();
  public static final String UTILISATEUR_CREATION_KEY = UTILISATEUR_CREATION.key();
  public static final String UTILISATEUR_MODIF_KEY = UTILISATEUR_MODIF.key();
  public static final String UTILISATEUR_VALID_ADM_KEY = UTILISATEUR_VALID_ADM.key();
  public static final String V_PART_PRINC_KEY = V_PART_PRINC.key();

  private static Logger LOG = Logger.getLogger(EOAvenant.class);

  public Avenant localInstanceIn(EOEditingContext editingContext) {
    Avenant localInstance = (Avenant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp avtDateCreation() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_CREATION_KEY);
  }

  public void setAvtDateCreation(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateCreation from " + avtDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_CREATION_KEY);
  }

  public NSTimestamp avtDateDeb() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_DEB_KEY);
  }

  public void setAvtDateDeb(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateDeb from " + avtDateDeb() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_DEB_KEY);
  }

  public NSTimestamp avtDateDebExec() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_DEB_EXEC_KEY);
  }

  public void setAvtDateDebExec(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateDebExec from " + avtDateDebExec() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_DEB_EXEC_KEY);
  }

  public NSTimestamp avtDateFin() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_FIN_KEY);
  }

  public void setAvtDateFin(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateFin from " + avtDateFin() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_FIN_KEY);
  }

  public NSTimestamp avtDateFinExec() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_FIN_EXEC_KEY);
  }

  public void setAvtDateFinExec(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateFinExec from " + avtDateFinExec() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_FIN_EXEC_KEY);
  }

  public NSTimestamp avtDateModif() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_MODIF_KEY);
  }

  public void setAvtDateModif(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateModif from " + avtDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_MODIF_KEY);
  }

  public NSTimestamp avtDateSignature() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_SIGNATURE_KEY);
  }

  public void setAvtDateSignature(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateSignature from " + avtDateSignature() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_SIGNATURE_KEY);
  }

  public NSTimestamp avtDateValidAdm() {
    return (NSTimestamp) storedValueForKey(EOAvenant.AVT_DATE_VALID_ADM_KEY);
  }

  public void setAvtDateValidAdm(NSTimestamp value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDateValidAdm from " + avtDateValidAdm() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DATE_VALID_ADM_KEY);
  }

  public java.math.BigDecimal avtDepAnterieure() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_DEP_ANTERIEURE_KEY);
  }

  public void setAvtDepAnterieure(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtDepAnterieure from " + avtDepAnterieure() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_DEP_ANTERIEURE_KEY);
  }

  public Integer avtIndex() {
    return (Integer) storedValueForKey(EOAvenant.AVT_INDEX_KEY);
  }

  public void setAvtIndex(Integer value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtIndex from " + avtIndex() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_INDEX_KEY);
  }

  public String avtLimitatif() {
    return (String) storedValueForKey(EOAvenant.AVT_LIMITATIF_KEY);
  }

  public void setAvtLimitatif(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtLimitatif from " + avtLimitatif() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_LIMITATIF_KEY);
  }

  public String avtLucrativite() {
    return (String) storedValueForKey(EOAvenant.AVT_LUCRATIVITE_KEY);
  }

  public void setAvtLucrativite(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtLucrativite from " + avtLucrativite() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_LUCRATIVITE_KEY);
  }

  public java.math.BigDecimal avtMntReliquat() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_MNT_RELIQUAT_KEY);
  }

  public void setAvtMntReliquat(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtMntReliquat from " + avtMntReliquat() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_MNT_RELIQUAT_KEY);
  }

  public String avtMonnaie() {
    return (String) storedValueForKey(EOAvenant.AVT_MONNAIE_KEY);
  }

  public void setAvtMonnaie(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtMonnaie from " + avtMonnaie() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_MONNAIE_KEY);
  }

  public java.math.BigDecimal avtMontantGlobal() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_MONTANT_GLOBAL_KEY);
  }

  public void setAvtMontantGlobal(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtMontantGlobal from " + avtMontantGlobal() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_MONTANT_GLOBAL_KEY);
  }

  public java.math.BigDecimal avtMontantHt() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_MONTANT_HT_KEY);
  }

  public void setAvtMontantHt(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtMontantHt from " + avtMontantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_MONTANT_HT_KEY);
  }

  public java.math.BigDecimal avtMontantTtc() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_MONTANT_TTC_KEY);
  }

  public void setAvtMontantTtc(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtMontantTtc from " + avtMontantTtc() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_MONTANT_TTC_KEY);
  }

  public String avtObjet() {
    return (String) storedValueForKey(EOAvenant.AVT_OBJET_KEY);
  }

  public void setAvtObjet(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtObjet from " + avtObjet() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_OBJET_KEY);
  }

  public String avtObjetCourt() {
    return (String) storedValueForKey(EOAvenant.AVT_OBJET_COURT_KEY);
  }

  public void setAvtObjetCourt(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtObjetCourt from " + avtObjetCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_OBJET_COURT_KEY);
  }

  public String avtObservations() {
    return (String) storedValueForKey(EOAvenant.AVT_OBSERVATIONS_KEY);
  }

  public void setAvtObservations(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtObservations from " + avtObservations() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_OBSERVATIONS_KEY);
  }

  public java.math.BigDecimal avtPctAvance() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_PCT_AVANCE_KEY);
  }

  public void setAvtPctAvance(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtPctAvance from " + avtPctAvance() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_PCT_AVANCE_KEY);
  }

  public java.math.BigDecimal avtRecAnterieure() {
    return (java.math.BigDecimal) storedValueForKey(EOAvenant.AVT_REC_ANTERIEURE_KEY);
  }

  public void setAvtRecAnterieure(java.math.BigDecimal value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtRecAnterieure from " + avtRecAnterieure() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_REC_ANTERIEURE_KEY);
  }

  public String avtRecupTva() {
    return (String) storedValueForKey(EOAvenant.AVT_RECUP_TVA_KEY);
  }

  public void setAvtRecupTva(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtRecupTva from " + avtRecupTva() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_RECUP_TVA_KEY);
  }

  public String avtRefExterne() {
    return (String) storedValueForKey(EOAvenant.AVT_REF_EXTERNE_KEY);
  }

  public void setAvtRefExterne(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtRefExterne from " + avtRefExterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_REF_EXTERNE_KEY);
  }

  public Integer avtStatReliquat() {
    return (Integer) storedValueForKey(EOAvenant.AVT_STAT_RELIQUAT_KEY);
  }

  public void setAvtStatReliquat(Integer value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtStatReliquat from " + avtStatReliquat() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_STAT_RELIQUAT_KEY);
  }

  public String avtSuppr() {
    return (String) storedValueForKey(EOAvenant.AVT_SUPPR_KEY);
  }

  public void setAvtSuppr(String value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating avtSuppr from " + avtSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.AVT_SUPPR_KEY);
  }

  public Integer taOrdre() {
    return (Integer) storedValueForKey(EOAvenant.TA_ORDRE_KEY);
  }

  public void setTaOrdre(Integer value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
        EOAvenant.LOG.debug( "updating taOrdre from " + taOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenant.TA_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure centreResponsabilite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOAvenant.CENTRE_RESPONSABILITE_KEY);
  }
  
  public void setCentreResponsabilite(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOAvenant.CENTRE_RESPONSABILITE_KEY);
  }

  public void setCentreResponsabiliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating centreResponsabilite from " + centreResponsabilite() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCentreResponsabilite(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = centreResponsabilite();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.CENTRE_RESPONSABILITE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.CENTRE_RESPONSABILITE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EOAvenant.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EOAvenant.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.grhum.Discipline discipline() {
    return (org.cocktail.cocowork.server.metier.grhum.Discipline)storedValueForKey(EOAvenant.DISCIPLINE_KEY);
  }
  
  public void setDiscipline(org.cocktail.cocowork.server.metier.grhum.Discipline value) {
    takeStoredValueForKey(value, EOAvenant.DISCIPLINE_KEY);
  }

  public void setDisciplineRelationship(org.cocktail.cocowork.server.metier.grhum.Discipline value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating discipline from " + discipline() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDiscipline(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.grhum.Discipline oldValue = discipline();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.DISCIPLINE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.DISCIPLINE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique)storedValueForKey(EOAvenant.DOMAINE_SCIENTIFIQUE_KEY);
  }
  
  public void setDomaineScientifique(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
    takeStoredValueForKey(value, EOAvenant.DOMAINE_SCIENTIFIQUE_KEY);
  }

  public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating domaineScientifique from " + domaineScientifique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDomaineScientifique(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.DOMAINE_SCIENTIFIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.DOMAINE_SCIENTIFIQUE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.ModeGestion modeGestion() {
    return (org.cocktail.cocowork.server.metier.convention.ModeGestion)storedValueForKey(EOAvenant.MODE_GESTION_KEY);
  }
  
  public void setModeGestion(org.cocktail.cocowork.server.metier.convention.ModeGestion value) {
    takeStoredValueForKey(value, EOAvenant.MODE_GESTION_KEY);
  }

  public void setModeGestionRelationship(org.cocktail.cocowork.server.metier.convention.ModeGestion value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating modeGestion from " + modeGestion() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setModeGestion(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.ModeGestion oldValue = modeGestion();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.MODE_GESTION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.MODE_GESTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTva tva() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTva)storedValueForKey(EOAvenant.TVA_KEY);
  }
  
  public void setTva(org.cocktail.fwkcktljefyadmin.common.metier.EOTva value) {
    takeStoredValueForKey(value, EOAvenant.TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTva value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
    	 EOAvenant.LOG.debug("updating tva from " + (org.cocktail.fwkcktljefyadmin.common.metier.EOTva)storedValueForKey(EOAvenant.TVA_KEY) + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTva(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTva oldValue = tva();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.TVA_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.TVA_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypeAvenant typeAvenant() {
    return (org.cocktail.cocowork.server.metier.convention.TypeAvenant)storedValueForKey(EOAvenant.TYPE_AVENANT_KEY);
  }
  
  public void setTypeAvenant(org.cocktail.cocowork.server.metier.convention.TypeAvenant value) {
    takeStoredValueForKey(value, EOAvenant.TYPE_AVENANT_KEY);
  }

  public void setTypeAvenantRelationship(org.cocktail.cocowork.server.metier.convention.TypeAvenant value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating typeAvenant from " + typeAvenant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeAvenant(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeAvenant oldValue = typeAvenant();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.TYPE_AVENANT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.TYPE_AVENANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOAvenant.UTILISATEUR_CREATION_KEY);
  }
  
  public void setUtilisateurCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOAvenant.UTILISATEUR_CREATION_KEY);
  }

  public void setUtilisateurCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating utilisateurCreation from " + utilisateurCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurCreation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurCreation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.UTILISATEUR_CREATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.UTILISATEUR_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurModif() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOAvenant.UTILISATEUR_MODIF_KEY);
  }
  
  public void setUtilisateurModif(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOAvenant.UTILISATEUR_MODIF_KEY);
  }

  public void setUtilisateurModifRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating utilisateurModif from " + utilisateurModif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurModif(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurModif();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.UTILISATEUR_MODIF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.UTILISATEUR_MODIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurValidAdm() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOAvenant.UTILISATEUR_VALID_ADM_KEY);
  }
  
  public void setUtilisateurValidAdm(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOAvenant.UTILISATEUR_VALID_ADM_KEY);
  }

  public void setUtilisateurValidAdmRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating utilisateurValidAdm from " + utilisateurValidAdm() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurValidAdm(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurValidAdm();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.UTILISATEUR_VALID_ADM_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.UTILISATEUR_VALID_ADM_KEY);
    }
  }
  
  public EOGenericRecord vPartPrinc() {
    return (EOGenericRecord)storedValueForKey(EOAvenant.V_PART_PRINC_KEY);
  }
  
  public void setVPartPrinc(EOGenericRecord value) {
    takeStoredValueForKey(value, EOAvenant.V_PART_PRINC_KEY);
  }

  public void setVPartPrincRelationship(EOGenericRecord value) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("updating vPartPrinc from " + vPartPrinc() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setVPartPrinc(value);
    }
    else if (value == null) {
        EOGenericRecord oldValue = vPartPrinc();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenant.V_PART_PRINC_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenant.V_PART_PRINC_KEY);
    }
  }
  
  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument> avenantDocuments() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument>)storedValueForKey(EOAvenant.AVENANT_DOCUMENTS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument> avenantDocuments(EOQualifier qualifier) {
    return avenantDocuments(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument> avenantDocuments(EOQualifier qualifier, boolean fetch) {
    return avenantDocuments(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument> avenantDocuments(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.AvenantDocument.AVENANT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.AvenantDocument.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenantDocuments();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDocument>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenantDocuments(org.cocktail.cocowork.server.metier.convention.AvenantDocument object) {
    includeObjectIntoPropertyWithKey(object, EOAvenant.AVENANT_DOCUMENTS_KEY);
  }

  public void removeFromAvenantDocuments(org.cocktail.cocowork.server.metier.convention.AvenantDocument object) {
    excludeObjectFromPropertyWithKey(object, EOAvenant.AVENANT_DOCUMENTS_KEY);
  }

  public void addToAvenantDocumentsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDocument object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("adding " + object + " to avenantDocuments relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenantDocuments(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOCUMENTS_KEY);
    }
  }

  public void removeFromAvenantDocumentsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDocument object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("removing " + object + " from avenantDocuments relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenantDocuments(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOCUMENTS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.AvenantDocument createAvenantDocumentsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.AvenantDocument.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOAvenant.AVENANT_DOCUMENTS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.AvenantDocument) eo;
  }

  public void deleteAvenantDocumentsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDocument object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOCUMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantDocumentsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.AvenantDocument> objects = avenantDocuments().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantDocumentsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> avenantDomaineScientifiques() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique>)storedValueForKey(EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> avenantDomaineScientifiques(EOQualifier qualifier) {
    return avenantDomaineScientifiques(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> avenantDomaineScientifiques(EOQualifier qualifier, boolean fetch) {
    return avenantDomaineScientifiques(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> avenantDomaineScientifiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique.AVENANT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenantDomaineScientifiques();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenantDomaineScientifiques(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique object) {
    includeObjectIntoPropertyWithKey(object, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
  }

  public void removeFromAvenantDomaineScientifiques(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique object) {
    excludeObjectFromPropertyWithKey(object, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
  }

  public void addToAvenantDomaineScientifiquesRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("adding " + object + " to avenantDomaineScientifiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenantDomaineScientifiques(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
    }
  }

  public void removeFromAvenantDomaineScientifiquesRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("removing " + object + " from avenantDomaineScientifiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenantDomaineScientifiques(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique createAvenantDomaineScientifiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique) eo;
  }

  public void deleteAvenantDomaineScientifiquesRelationship(org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_DOMAINE_SCIENTIFIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantDomaineScientifiquesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.AvenantDomaineScientifique> objects = avenantDomaineScientifiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantDomaineScientifiquesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> avenantEvenements() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement>)storedValueForKey(EOAvenant.AVENANT_EVENEMENTS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> avenantEvenements(EOQualifier qualifier) {
    return avenantEvenements(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> avenantEvenements(EOQualifier qualifier, boolean fetch) {
    return avenantEvenements(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> avenantEvenements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.AvenantEvenement.AVENANT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.AvenantEvenement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenantEvenements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantEvenement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenantEvenements(org.cocktail.cocowork.server.metier.convention.AvenantEvenement object) {
    includeObjectIntoPropertyWithKey(object, EOAvenant.AVENANT_EVENEMENTS_KEY);
  }

  public void removeFromAvenantEvenements(org.cocktail.cocowork.server.metier.convention.AvenantEvenement object) {
    excludeObjectFromPropertyWithKey(object, EOAvenant.AVENANT_EVENEMENTS_KEY);
  }

  public void addToAvenantEvenementsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantEvenement object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("adding " + object + " to avenantEvenements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenantEvenements(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_EVENEMENTS_KEY);
    }
  }

  public void removeFromAvenantEvenementsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantEvenement object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("removing " + object + " from avenantEvenements relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenantEvenements(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_EVENEMENTS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.AvenantEvenement createAvenantEvenementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.AvenantEvenement.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOAvenant.AVENANT_EVENEMENTS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.AvenantEvenement) eo;
  }

  public void deleteAvenantEvenementsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantEvenement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_EVENEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantEvenementsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.AvenantEvenement> objects = avenantEvenements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantEvenementsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> avenantTypeStats() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat>)storedValueForKey(EOAvenant.AVENANT_TYPE_STATS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> avenantTypeStats(EOQualifier qualifier) {
    return avenantTypeStats(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> avenantTypeStats(EOQualifier qualifier, boolean fetch) {
    return avenantTypeStats(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> avenantTypeStats(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat.AVENANT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.AvenantTypeStat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenantTypeStats();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenantTypeStats(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat object) {
    includeObjectIntoPropertyWithKey(object, EOAvenant.AVENANT_TYPE_STATS_KEY);
  }

  public void removeFromAvenantTypeStats(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat object) {
    excludeObjectFromPropertyWithKey(object, EOAvenant.AVENANT_TYPE_STATS_KEY);
  }

  public void addToAvenantTypeStatsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("adding " + object + " to avenantTypeStats relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenantTypeStats(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_TYPE_STATS_KEY);
    }
  }

  public void removeFromAvenantTypeStatsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat object) {
    if (EOAvenant.LOG.isDebugEnabled()) {
      EOAvenant.LOG.debug("removing " + object + " from avenantTypeStats relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenantTypeStats(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_TYPE_STATS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.AvenantTypeStat createAvenantTypeStatsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.AvenantTypeStat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOAvenant.AVENANT_TYPE_STATS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.AvenantTypeStat) eo;
  }

  public void deleteAvenantTypeStatsRelationship(org.cocktail.cocowork.server.metier.convention.AvenantTypeStat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOAvenant.AVENANT_TYPE_STATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantTypeStatsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.AvenantTypeStat> objects = avenantTypeStats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantTypeStatsRelationship(objects.nextElement());
    }
  }


  public static Avenant create(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.Contrat contrat, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation) {
    Avenant eo = (Avenant) EOUtilities.createAndInsertInstance(editingContext, EOAvenant.ENTITY_NAME);    
    eo.setContratRelationship(contrat);
    eo.setUtilisateurCreationRelationship(utilisateurCreation);
    return eo;
  }

  public static ERXFetchSpecification<Avenant> fetchSpec() {
    return new ERXFetchSpecification<Avenant>(EOAvenant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Avenant> fetchAll(EOEditingContext editingContext) {
    return EOAvenant.fetchAll(editingContext, null);
  }

  public static NSArray<Avenant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAvenant.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Avenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Avenant> fetchSpec = new ERXFetchSpecification<Avenant>(EOAvenant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Avenant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Avenant fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenant.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Avenant fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Avenant> eoObjects = EOAvenant.fetchAll(editingContext, qualifier, null);
    Avenant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Avenant fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenant.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Avenant fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Avenant eoObject = EOAvenant.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Avenant localInstanceIn(EOEditingContext editingContext, Avenant eo) {
    Avenant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}