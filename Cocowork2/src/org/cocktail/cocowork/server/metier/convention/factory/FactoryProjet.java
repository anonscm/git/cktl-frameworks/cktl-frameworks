package org.cocktail.cocowork.server.metier.convention.factory;

import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.Projet;
import org.cocktail.cocowork.server.metier.convention.ProjetContrat;
import org.cocktail.cocowork.server.metier.convention.TypeProjet;
import org.cocktail.cocowork.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryProjet extends Factory {
	
	/**
	 * Constructeur.
	 * @param ec
	 */
	public FactoryProjet(EOEditingContext ec) {
		super(ec);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param ec
	 * @param withlog
	 */
	public FactoryProjet(EOEditingContext ec, Boolean withlog) {
		super(ec, withlog);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Creation d'un projet de contrats.
	 */
	public Projet creerProjet(Projet projetPere, TypeProjet typeProjet, String libelleProjet)  throws Exception, ExceptionUtilisateur {
		
		Projet newProject=Projet.instanciate(ec);
		ec.insertObject(newProject);
		
		newProject.setTypeProjetRelationship(typeProjet);
		newProject.setProjetPereRelationship(projetPere);
		newProject.setExerciceCocktailRelationship(new FinderExerciceCocktail(ec).findExerciceCocktailCourant());
		newProject.setPjtLibelle(libelleProjet);
		
		return newProject;
		
	}
	
	public ProjetContrat ajouterContrat(Projet projet, Contrat contrat)  throws Exception, ExceptionUtilisateur {
		
		ProjetContrat newProjetContrat = ProjetContrat.instanciate(ec);
		ec.insertObject(newProjetContrat);
		newProjetContrat.setContratRelationship(contrat);
		contrat.addToProjetContratRelationship(newProjetContrat);
		newProjetContrat.setProjetRelationship(projet);
		projet.addToProjetContratsRelationship(newProjetContrat);
		
		return newProjetContrat;
		
	}
	
	
	
	

}
