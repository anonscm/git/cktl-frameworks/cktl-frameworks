// DO NOT EDIT.  Make changes to HistoCreditPositionneEngageBudget.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOHistoCreditPositionneEngageBudget extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWHistoCreditPositionneEngageBudget";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget> ENGAGEMENT_BUDGET = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget>("engagementBudget");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> HISTO_CREDIT_POSITIONNE = new ERXKey<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne>("histoCreditPositionne");

  // Attributes
  // Relationships
  public static final String ENGAGEMENT_BUDGET_KEY = ENGAGEMENT_BUDGET.key();
  public static final String HISTO_CREDIT_POSITIONNE_KEY = HISTO_CREDIT_POSITIONNE.key();

  private static Logger LOG = Logger.getLogger(EOHistoCreditPositionneEngageBudget.class);

  public HistoCreditPositionneEngageBudget localInstanceIn(EOEditingContext editingContext) {
    HistoCreditPositionneEngageBudget localInstance = (HistoCreditPositionneEngageBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget() {
    return (org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget)storedValueForKey(EOHistoCreditPositionneEngageBudget.ENGAGEMENT_BUDGET_KEY);
  }
  
  public void setEngagementBudget(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneEngageBudget.ENGAGEMENT_BUDGET_KEY);
  }

  public void setEngagementBudgetRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget value) {
    if (EOHistoCreditPositionneEngageBudget.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneEngageBudget.LOG.debug("updating engagementBudget from " + engagementBudget() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEngagementBudget(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget oldValue = engagementBudget();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneEngageBudget.ENGAGEMENT_BUDGET_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneEngageBudget.ENGAGEMENT_BUDGET_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne histoCreditPositionne() {
    return (org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne)storedValueForKey(EOHistoCreditPositionneEngageBudget.HISTO_CREDIT_POSITIONNE_KEY);
  }
  
  public void setHistoCreditPositionne(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneEngageBudget.HISTO_CREDIT_POSITIONNE_KEY);
  }

  public void setHistoCreditPositionneRelationship(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne value) {
    if (EOHistoCreditPositionneEngageBudget.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneEngageBudget.LOG.debug("updating histoCreditPositionne from " + histoCreditPositionne() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setHistoCreditPositionne(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne oldValue = histoCreditPositionne();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneEngageBudget.HISTO_CREDIT_POSITIONNE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneEngageBudget.HISTO_CREDIT_POSITIONNE_KEY);
    }
  }
  

  public static HistoCreditPositionneEngageBudget create(EOEditingContext editingContext, org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget engagementBudget, org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne histoCreditPositionne) {
    HistoCreditPositionneEngageBudget eo = (HistoCreditPositionneEngageBudget) EOUtilities.createAndInsertInstance(editingContext, EOHistoCreditPositionneEngageBudget.ENTITY_NAME);    
    eo.setEngagementBudgetRelationship(engagementBudget);
    eo.setHistoCreditPositionneRelationship(histoCreditPositionne);
    return eo;
  }

  public static ERXFetchSpecification<HistoCreditPositionneEngageBudget> fetchSpec() {
    return new ERXFetchSpecification<HistoCreditPositionneEngageBudget>(EOHistoCreditPositionneEngageBudget.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<HistoCreditPositionneEngageBudget> fetchAll(EOEditingContext editingContext) {
    return EOHistoCreditPositionneEngageBudget.fetchAll(editingContext, null);
  }

  public static NSArray<HistoCreditPositionneEngageBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOHistoCreditPositionneEngageBudget.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<HistoCreditPositionneEngageBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<HistoCreditPositionneEngageBudget> fetchSpec = new ERXFetchSpecification<HistoCreditPositionneEngageBudget>(EOHistoCreditPositionneEngageBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<HistoCreditPositionneEngageBudget> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static HistoCreditPositionneEngageBudget fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionneEngageBudget.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionneEngageBudget fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<HistoCreditPositionneEngageBudget> eoObjects = EOHistoCreditPositionneEngageBudget.fetchAll(editingContext, qualifier, null);
    HistoCreditPositionneEngageBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWHistoCreditPositionneEngageBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionneEngageBudget fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionneEngageBudget.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionneEngageBudget fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    HistoCreditPositionneEngageBudget eoObject = EOHistoCreditPositionneEngageBudget.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWHistoCreditPositionneEngageBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionneEngageBudget localInstanceIn(EOEditingContext editingContext, HistoCreditPositionneEngageBudget eo) {
    HistoCreditPositionneEngageBudget localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}