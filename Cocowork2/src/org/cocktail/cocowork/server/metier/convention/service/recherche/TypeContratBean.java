package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * Bean représentant le type de contrat.
 * 
 * @author Alexis Tual
 *
 */
public class TypeContratBean {

    public static final String TYPE_CON_LIBELLE = "typeConLibelle";
    
    private String typeConIdInterne;
    private String typeConLibelle;
    
    /**
     * @param typeConIdInterne l'id
     * @param typeConLibelle le libellé
     */
    public TypeContratBean(String typeConIdInterne, String typeConLibelle) {
        super();
        this.typeConIdInterne = typeConIdInterne;
        this.typeConLibelle = typeConLibelle;
    }

    public String getTypeConIdInterne() {
        return typeConIdInterne;
    }
    
    public String getTypeConLibelle() {
        return typeConLibelle;
    }
 
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(typeConIdInterne)
                .append(typeConLibelle)
                .toHashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof TypeContratBean) {
            TypeContratBean other = (TypeContratBean)obj;
            result = new EqualsBuilder()
                        .append(typeConIdInterne, other.typeConIdInterne)
                        .append(typeConLibelle, other.typeConLibelle)
                        .isEquals();
        }
        return result;
    }
    
}
