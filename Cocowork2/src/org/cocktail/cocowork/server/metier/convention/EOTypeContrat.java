// DO NOT EDIT.  Make changes to TypeContrat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeContrat";

  // Attribute Keys
  public static final ERXKey<String> TYCON_ID_INTERNE = new ERXKey<String>("tyconIdInterne");
  public static final ERXKey<String> TYCON_LIBELLE = new ERXKey<String>("tyconLibelle");
  public static final ERXKey<String> TYCON_NATURE = new ERXKey<String>("tyconNature");
  public static final ERXKey<Integer> TYCON_NIVEAU = new ERXKey<Integer>("tyconNiveau");
  public static final ERXKey<String> TYCON_RECHERCHE = new ERXKey<String>("tyconRecherche");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat> FILS = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat>("fils");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat> PERE = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat>("pere");

  // Attributes
  public static final String TYCON_ID_INTERNE_KEY = TYCON_ID_INTERNE.key();
  public static final String TYCON_LIBELLE_KEY = TYCON_LIBELLE.key();
  public static final String TYCON_NATURE_KEY = TYCON_NATURE.key();
  public static final String TYCON_NIVEAU_KEY = TYCON_NIVEAU.key();
  public static final String TYCON_RECHERCHE_KEY = TYCON_RECHERCHE.key();
  // Relationships
  public static final String FILS_KEY = FILS.key();
  public static final String PERE_KEY = PERE.key();

  private static Logger LOG = Logger.getLogger(EOTypeContrat.class);

  public TypeContrat localInstanceIn(EOEditingContext editingContext) {
    TypeContrat localInstance = (TypeContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tyconIdInterne() {
    return (String) storedValueForKey(EOTypeContrat.TYCON_ID_INTERNE_KEY);
  }

  public void setTyconIdInterne(String value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
        EOTypeContrat.LOG.debug( "updating tyconIdInterne from " + tyconIdInterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContrat.TYCON_ID_INTERNE_KEY);
  }

  public String tyconLibelle() {
    return (String) storedValueForKey(EOTypeContrat.TYCON_LIBELLE_KEY);
  }

  public void setTyconLibelle(String value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
        EOTypeContrat.LOG.debug( "updating tyconLibelle from " + tyconLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContrat.TYCON_LIBELLE_KEY);
  }

  public String tyconNature() {
    return (String) storedValueForKey(EOTypeContrat.TYCON_NATURE_KEY);
  }

  public void setTyconNature(String value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
        EOTypeContrat.LOG.debug( "updating tyconNature from " + tyconNature() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContrat.TYCON_NATURE_KEY);
  }

  public Integer tyconNiveau() {
    return (Integer) storedValueForKey(EOTypeContrat.TYCON_NIVEAU_KEY);
  }

  public void setTyconNiveau(Integer value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
        EOTypeContrat.LOG.debug( "updating tyconNiveau from " + tyconNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContrat.TYCON_NIVEAU_KEY);
  }

  public String tyconRecherche() {
    return (String) storedValueForKey(EOTypeContrat.TYCON_RECHERCHE_KEY);
  }

  public void setTyconRecherche(String value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
        EOTypeContrat.LOG.debug( "updating tyconRecherche from " + tyconRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContrat.TYCON_RECHERCHE_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.TypeContrat pere() {
    return (org.cocktail.cocowork.server.metier.convention.TypeContrat)storedValueForKey(EOTypeContrat.PERE_KEY);
  }
  
  public void setPere(org.cocktail.cocowork.server.metier.convention.TypeContrat value) {
    takeStoredValueForKey(value, EOTypeContrat.PERE_KEY);
  }

  public void setPereRelationship(org.cocktail.cocowork.server.metier.convention.TypeContrat value) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
      EOTypeContrat.LOG.debug("updating pere from " + pere() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPere(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeContrat oldValue = pere();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTypeContrat.PERE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTypeContrat.PERE_KEY);
    }
  }
  
  public NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fils() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat>)storedValueForKey(EOTypeContrat.FILS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fils(EOQualifier qualifier) {
    return fils(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fils(EOQualifier qualifier, boolean fetch) {
    return fils(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.TypeContrat.PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.TypeContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = fils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFils(org.cocktail.cocowork.server.metier.convention.TypeContrat object) {
    includeObjectIntoPropertyWithKey(object, EOTypeContrat.FILS_KEY);
  }

  public void removeFromFils(org.cocktail.cocowork.server.metier.convention.TypeContrat object) {
    excludeObjectFromPropertyWithKey(object, EOTypeContrat.FILS_KEY);
  }

  public void addToFilsRelationship(org.cocktail.cocowork.server.metier.convention.TypeContrat object) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
      EOTypeContrat.LOG.debug("adding " + object + " to fils relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToFils(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTypeContrat.FILS_KEY);
    }
  }

  public void removeFromFilsRelationship(org.cocktail.cocowork.server.metier.convention.TypeContrat object) {
    if (EOTypeContrat.LOG.isDebugEnabled()) {
      EOTypeContrat.LOG.debug("removing " + object + " from fils relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromFils(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTypeContrat.FILS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.TypeContrat createFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.TypeContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTypeContrat.FILS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.TypeContrat) eo;
  }

  public void deleteFilsRelationship(org.cocktail.cocowork.server.metier.convention.TypeContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTypeContrat.FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFilsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.TypeContrat> objects = fils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFilsRelationship(objects.nextElement());
    }
  }


  public static TypeContrat create(EOEditingContext editingContext, String tyconIdInterne
, String tyconNature
) {
    TypeContrat eo = (TypeContrat) EOUtilities.createAndInsertInstance(editingContext, EOTypeContrat.ENTITY_NAME);    
        eo.setTyconIdInterne(tyconIdInterne);
        eo.setTyconNature(tyconNature);
    return eo;
  }

  public static ERXFetchSpecification<TypeContrat> fetchSpec() {
    return new ERXFetchSpecification<TypeContrat>(EOTypeContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeContrat> fetchAll(EOEditingContext editingContext) {
    return EOTypeContrat.fetchAll(editingContext, null);
  }

  public static NSArray<TypeContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeContrat> fetchSpec = new ERXFetchSpecification<TypeContrat>(EOTypeContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeContrat> eoObjects = EOTypeContrat.fetchAll(editingContext, qualifier, null);
    TypeContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeContrat eoObject = EOTypeContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeContrat localInstanceIn(EOEditingContext editingContext, TypeContrat eo) {
    TypeContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeContrat.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeContrat.ENTITY_NAME);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}