// Avenant.java
// 
package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.cocktail.cocowork.common.tools.Constantes;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryConvention;
import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTva;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;
import er.extensions.validation.ERXValidationFactory;

public class Avenant extends EOAvenant {
	public static final String INITIAL = "Initial";

	/**
	 * <p>
	 * Nom de l'entite correspondante a cette classe metier.
	 * <p>
	 * C'est indispensable de "cacher" <code>{@link EOAvenant#ENTITY_NAME}</code> ici car il y a 2 entites qui pointent
	 * sur cette classe metier dans le modele : "CWAvenant" et "CWAvenantZero", donc le template utilise par EOGenerator
	 * fait que <code>{@link EOAvenant#ENTITY_NAME}</code> vaut <code>CWAvenantZero</code> au lieu de
	 * <code>CWAvenant</code>.
	 */
	public static final String ENTITY_NAME = "CWAvenant";

	public static final String AVT_MONNAIE = "E";

	public static final String AVT_LUCRATIVITE_OUI = "O";
	public static final String AVT_LUCRATIVITE_NON = "N";

	public static final String AVT_RECUP_TVA_OUI = "O";
	public static final String AVT_RECUP_TVA_NON = "N";

	public static final String AVT_SUPPR_OUI = "O";
	public static final String AVT_SUPPR_NON = "N";

	public static final int LONGUEUR_MAX_REF_EXTERNE = 100;
	public static final int LONGUEUR_MAX_OBJET = 250;
	public static final int LONGUEUR_MAX_OBSERVATIONS = 500;

	public static final String AVT_LIMITATIF_OUI = "OUI";

	public static final EOSortOrdering SORT_INDEX_ASC = EOSortOrdering.sortOrderingWithKey("avtIndex", EOSortOrdering.CompareAscending);

	public static final EOQualifier QUALIFIER_NON_SUPPR = EOQualifier.qualifierWithQualifierFormat("avtSuppr='N'", null);
	public static final EOQualifier QUALIFIER_NON_INITIAL = EOQualifier.qualifierWithQualifierFormat("avtIndex > 0", null);
	public static final EOQualifier QUALIFIER_NON_SUPPR_NON_INITIAL = new EOAndQualifier(new NSArray(new EOQualifier[] {QUALIFIER_NON_SUPPR,
			QUALIFIER_NON_INITIAL}));
	public static final EOQualifier QUALIFIER_VALIDE = EOQualifier.qualifierWithQualifierFormat("avtDateValidAdm <> nil", null);

	public NSMutableArray documents = null;

	private boolean shouldNotValidate;

	public Avenant() {
		super();
	}

	public void setShouldNotValidate(boolean shouldNotValidate) {
		this.shouldNotValidate = shouldNotValidate;
	}

	public Boolean avtLucrativiteBoolean() {
		return new Boolean(AVT_LUCRATIVITE_OUI.equals(avtLucrativite()));
	}

	public void setAvtLucrativiteBoolean(Boolean aValue) {
		setAvtLucrativite(aValue != null && aValue.booleanValue() ? AVT_LUCRATIVITE_OUI : AVT_LUCRATIVITE_NON);
	}

	public Boolean avtRecupTvaBoolean() {
		return new Boolean(AVT_RECUP_TVA_OUI.equals(avtRecupTva()));
	}

	public void setAvtRecupTvaBoolean(Boolean aValue) {
		setAvtRecupTva(aValue != null && aValue.booleanValue() ? AVT_RECUP_TVA_OUI : AVT_RECUP_TVA_NON);
	}

	public Boolean avtLimitatifBoolean() {
		return new Boolean(AVT_LIMITATIF_OUI.equals(avtLimitatif()));
	}

	public void setAvtLimitatifBoolean(Boolean aValue) {
		setAvtLimitatif(aValue != null && aValue.booleanValue() ? AVT_LIMITATIF_OUI : null);
	}

	public void setAvtDateDeb(NSTimestamp aValue) {
		super.setAvtDateDeb(aValue);
		if (isAvenantZero()) {
			contrat().updateDuree();
			recalculerTranches();
		}
	}

	public void setAvtDateFin(NSTimestamp aValue) {
		super.setAvtDateFin(aValue);
		if (isAvenantZero()) {
			contrat().updateDuree();
			recalculerTranches();
		}
	}

	public void setAvtMontantHt(BigDecimal aValue) {
		if (aValue != null && avtMontantHt() != null && avtDateValidAdm() != null) {
			if (contrat().montantDisponibleTranches().doubleValue() - avtMontantHt().doubleValue() + aValue.doubleValue() < 0) {
				throw new ValidationException(
						"Le Montant Ht ne peut etre modifie car le montant de la convention devient inferieur aux tranches deja positionnee");
			}
			if (contrat().montantDisponiblePourPartenariat().doubleValue() - avtMontantHt().doubleValue() + aValue.doubleValue() < 0) {
				throw new ValidationException(
						"Le Montant Ht ne peut etre modifie car le montant de la convention devient inferieur aux contributions de partenariat deja apportees");
			}
		}

		super.setAvtMontantHt(aValue != null ? aValue.setScale(2, BigDecimal.ROUND_HALF_UP) : null);
		// Recalcul du montant TTC
		setAvtMontantTtc(htToTtc(avtMontantHt(), tva()));
		if (isAvenantZero()) {
			recalculerTranches();
		}
	}

	private void recalculerTranches() {
		if (ERXProperties.booleanForKeyWithDefault(Constantes.GESTION_TRANCHE_AUTO_PARAM, true)) {
			if (contrat() != null && contrat().dateDebut() != null && contrat().dateFin() != null && contrat().montantHt() != null
					&& contrat().montantHt().doubleValue() > 0) {
				FactoryConvention fc = new FactoryConvention(editingContext(), true);
				try {
					fc.genererTranches(contrat(), contrat().utilisateurCreation());
				} catch (ValidationException e) {
					throw e;
				} catch (Exception e) {
					throw new NSForwardException(e);
				}
			}
		}
	}

	public void setAvtMontantTtc(BigDecimal aValue) {
		super.setAvtMontantTtc(aValue != null ? aValue.setScale(2, BigDecimal.ROUND_HALF_UP) : null);
	}

	public boolean isAvenantZero() {
		return contrat() != null && contrat().avenantZero() != null && this.equals(contrat().avenantZero());
	}

	public boolean isSigne() {
		return avtDateValidAdm() != null;
	}

	/**
	 * @see com.webobjects.eocontrol.EOCustomObject#validateForSave()
	 */
	public void validateForSave() throws ValidationException {
		super.validateForSave();

		if (!shouldNotValidate) {
			// On vérifie que la date de début et la date de fin soient bien renseignées
			// dans le cas d'un contrat de type convention
			if (contrat() != null && contrat().typeClassificationContrat().isTypeConv()) {
				if (avtDateDeb() == null || avtDateFin() == null) {
					throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionNoDate");
				}
			}

			// verif date fin >= date debut
			if (avtDateFin() != null && avtDateFin().before(avtDateDeb())) {
				String message;
				if (avtIndex().intValue() > 0) {
					message = "La date de fin de l'avenant n\u00BA" + avtIndex() + " ne peut \u00EAtre ant\u00E9rieure \u00E0 la date de d\u00E9but.";
				} else {
					message = "La date de fin du contrat initial ne peut \u00EAtre ant\u00E9rieure \u00E0 la date de d\u00E9but.";
				}
				throw new ValidationException(message);
			}
			// Si date de signature est null mais que dateValidAdm renseignée, pas normal !
			if (avtDateSignature() == null && avtDateValidAdm() != null) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionPasSigne");
			}
			// Si date de validation != null & contrat date valid adm == null pas bon !
			if (avtDateValidAdm() != null && contrat().conDateValidAdm() == null) {
				setAvtDateValidAdm(null);
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionPasValide");
			}
			// On vérifie que le type est bien spécifié
			if (typeAvenant() == null) {
				throw new ValidationException("Le type d'avenant doit \u00EAtre indiqu\u00E9.");
			}
		}
		// Enfin, on recalcule les tranches et la durée de la convention
		contrat().updateDuree();
		recalculerTranches();
	}

	/**
	 * Cet objet au format texte.
	 */
	public String toString() {
		if (avtIndex().intValue() > 0) {
			return "Avenant n\u00BA" + avtIndex() + " de la convention " + contrat().toString();
		} else {
			return "Contrat initial de la convention " + contrat().toString();
		}
	}

	@Override
	public void setModeGestionRelationship(org.cocktail.cocowork.server.metier.convention.ModeGestion aValue) {
		super.setModeGestionRelationship(aValue);
		if (aValue != null && aValue.isModeRA()) {
			setAvtLimitatifBoolean(Boolean.TRUE);
			setAvtLucrativiteBoolean(Boolean.FALSE);
		}
	}

	// pour le kvc
	public ModeGestion modeGestionRelationship() {
		return modeGestion();
	}

	/**
	 * @return the documents
	 */
	public NSMutableArray documents() {
		if (documents == null && contrat().conIndex() != null) {
			// TODO Recuperer tous les documents associes a l'avenant a partir du numero de contrat
			// et du numero de l'avenant
		}
		return documents;
	}

	/**
	 * @param documents
	 *            the documents to set
	 */
	public void setDocuments(NSMutableArray documents) {
		this.documents = documents;
	}

	public TypeStat typeOptionnel() {
		TypeStat typeOptionnel = null;
		NSArray avenantTypeStats = avenantTypeStats();
		if (avenantTypeStats != null && avenantTypeStats.count() > 0 && avenantTypeStats.lastObject() != null) {
			typeOptionnel = ((AvenantTypeStat) avenantTypeStats.lastObject()).typeStat();
		}
		return typeOptionnel;
	}

	public void setTypeOptionnel(TypeStat value) {
		if (value != null) {
			AvenantTypeStat avtTypeStat;
			try {
				avtTypeStat = AvenantTypeStat.instanciate(editingContext());
				editingContext().insertObject(avtTypeStat);
				avtTypeStat.setTypeStatRelationship(value);
				avtTypeStat.setAvenantRelationship(this);
				addToAvenantTypeStatsRelationship(avtTypeStat);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			AvenantTypeStat avtTypeStat = (AvenantTypeStat) avenantTypeStats().lastObject();
			if (avtTypeStat != null) {
				removeFromAvenantTypeStatsRelationship(avtTypeStat);
				editingContext().deleteObject(avtTypeStat);
			}
		}
	}

	private BigDecimal htToTtc(BigDecimal montantHt, EOTva tva) {
		if (montantHt == null || tva == null) {
			return montantHt;
		}
		// HT x (1 + TVA / 100)
		return montantHt.multiply(new BigDecimal("1").add(tva.tvaTaux().divide(new BigDecimal("100"))));
	}

	@Override
	public void setTvaRelationship(EOTva value) {
		super.setTvaRelationship(value);
		BigDecimal montantHT = avtMontantHt();
		if (montantHT != null) {
			setAvtMontantTtc(htToTtc(montantHT, value));
		} else {
			setAvtMontantTtc(null);
		}

	}

	// Pour le kvc
	public EOTva tvaRelationship() {
		return tva();
	}

	@Override
	public EOTva tva() {
		EOTva tva = super.tva();
		if (tva == null && avtMontantHt() != null && avtMontantTtc() != null) {
			BigDecimal taux = avtMontantTtc().subtract(avtMontantHt());
			if (taux.doubleValue() > 0) {
				taux = taux.multiply(BigDecimal.valueOf(100));
				taux = taux.divide(avtMontantHt(), RoundingMode.HALF_UP);
				taux.setScale(2, RoundingMode.HALF_UP);
			} else {
				taux = BigDecimal.ZERO;
			}
			tva = (EOTva) EOUtilities.objectMatchingKeyAndValue(editingContext(), EOTva.ENTITY_NAME, EOTva.TVA_TAUX_KEY, taux);
			setTvaRelationship(tva);
		}
		return tva;
	}

	public NSArray<EOEvenement> evenements() {
		NSArray<EOEvenement> evts = (NSArray<EOEvenement>) avenantEvenements().valueForKey(EOAvenantEvenement.EVENEMENT_KEY);
		evts = ERXS.sorted(evts, ERXS.desc(EOEvenement.DATE_PREVUE_KEY).array());
		return evts;
	}

	public void removeFromEvenementsRelationship(EOEvenement evenement) {
		NSArray<AvenantEvenement> avEvts = avenantEvenements(EOAvenantEvenement.EVENEMENT.eq(evenement));
		for (AvenantEvenement avEvt : avEvts) {
			avEvt.setEvenementRelationship(null);
			EOEvenement.supprimerEvenement(evenement, editingContext());
			this.deleteAvenantEvenementsRelationship(avEvt);
		}
	}

	public void removeFromDomaineScientifiqueRelationship(EODomaineScientifique domaine) {
		NSArray<AvenantDomaineScientifique> domaines = avenantDomaineScientifiques(EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(domaine));

		for (AvenantDomaineScientifique ads : domaines) {
			ads.setDomaineScientifiqueRelationship(null);
			this.deleteAvenantDomaineScientifiquesRelationship(ads);
		}
	}

	public String libelleCourt() {
		String libelleCourt = null;

		if (avtIndex().intValue() > 0) {
			libelleCourt = String.valueOf(avtIndex());
		} else {
			libelleCourt = Avenant.INITIAL;
		}
		return libelleCourt;
	}

	@Override
	public EODomaineScientifique domaineScientifique() {
		if (avenantDomaineScientifiques().isEmpty()) {
			return null;
		} else {
			AvenantDomaineScientifique avenantDomaineScientifique = ERXArrayUtilities.firstObject(avenantDomaineScientifiques());
			return avenantDomaineScientifique.domaineScientifique();
		}
	}

	@Override
	public void setDomaineScientifique(EODomaineScientifique domaineScientifique) {
		if (domaineScientifique == null) {
			return;
		}

		if (avenantDomaineScientifiques().isEmpty()) {
			AvenantDomaineScientifique.create(editingContext(), this, domaineScientifique);
		} else {
			AvenantDomaineScientifique avenantDomaineScientifique = ERXArrayUtilities.firstObject(avenantDomaineScientifiques());
			avenantDomaineScientifique.setDomaineScientifiqueRelationship(domaineScientifique);
		}
	}
}
