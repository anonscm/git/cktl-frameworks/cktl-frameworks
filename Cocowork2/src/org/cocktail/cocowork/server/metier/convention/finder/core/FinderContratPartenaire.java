package org.cocktail.cocowork.server.metier.convention.finder.core;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.TypePartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Composant permettant de rechercher des structures.
 * 
 * @author Michael Haller, Consortium Cocktail, 2009
 */
public class FinderContratPartenaire extends Finder {
	protected Contrat contrat;
	protected IPersonne partenaire;
	protected TypePartenaire typePartenaire;
	protected Boolean partenairePrincipal;

	protected EOQualifier qualifierContrat;
	protected EOQualifier qualifierPartenaire;
	protected EOQualifier qualifierTypePartenaire;
	protected EOQualifier qualifierPartenairePrincipal;

	/**
	 * 
	 * @param ec
	 */
	public FinderContratPartenaire(EOEditingContext ec) {
		super(ec, ContratPartenaire.ENTITY_NAME);

		// this.addMandatoryQualifier(EOQualifier.qualifierWithQualifierFormat("conSuppr = %@",
		// new NSArray("N")));
	}

	/**
	 * Change le critere
	 */
	public void setContrat(final Contrat contrat) {
		this.qualifierContrat = createQualifier("contrat = %@", contrat);

		this.contrat = contrat;
	}

	/**
	 * Change le critere
	 */
	public void setPartenaire(final IPersonne partenaire) {
		this.qualifierPartenaire = createQualifier("persId = %@", partenaire.persId());

		this.partenaire = partenaire;
	}

	/**
	 * Change le critere
	 */
	public void setPartenairePrincipal(final Boolean partenairePrincipal) {
		if (partenairePrincipal != null) {
			if (partenairePrincipal.booleanValue())
				this.qualifierPartenairePrincipal = ContratPartenaire.QUALIFIER_PARTENAIRE_PRINCIPAL;
			else
				this.qualifierPartenairePrincipal = ContratPartenaire.QUALIFIER_PARTENAIRE_NON_PRINCIPAL;
		}
		this.partenairePrincipal = partenairePrincipal;
	}

	/**
	 * Change le critere
	 */
	public void setTypePartenaire(final TypePartenaire typePartenaire) {
		this.qualifierTypePartenaire = createQualifier("typePartenaire = %@", typePartenaire);

		this.typePartenaire = typePartenaire;
	}

	/**
	 * Supprime tous les criteres de recherche courant.
	 */
	public void clearAllCriteria() {
		this.setContrat((Contrat) null);
		this.setPartenaire((IPersonne) null);
		this.setPartenairePrincipal(null);
		this.setTypePartenaire(null);
	}

	/**
	 * Recherche des partenariats.
	 * 
	 * @return Liste des partenariats trouves.
	 * @throws ExceptionFinder
	 */
	public NSArray find() throws ExceptionFinder {

		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();

		// nouveaux criteres
		this.addOptionalQualifier(this.qualifierContrat);
		this.addOptionalQualifier(this.qualifierPartenaire);
		this.addOptionalQualifier(this.qualifierPartenairePrincipal);
		this.addOptionalQualifier(this.qualifierTypePartenaire);

		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder("Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher une convention.");

		// fetch
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
