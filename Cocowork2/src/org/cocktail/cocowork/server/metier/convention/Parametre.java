package org.cocktail.cocowork.server.metier.convention;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOSharedEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.foundation.ERXArrayUtilities;

public class Parametre extends EOParametre {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(Parametre.class);
    public static final String GENERALITES_MODIFIABLES_APRES_VALIDATION = "GENERALITES_MODIFIABLES_APRES_VALIDATION";
    private static NSDictionary<String, Parametre> _paramCache;

    public static NSDictionary<String, Parametre> allParametresByKey() {
        if (_paramCache == null) {
            EOSharedEditingContext sharedEc =  EOSharedEditingContext.defaultSharedEditingContext();
            NSArray<Parametre> allParams = (NSArray<Parametre>)sharedEc.objectsByEntityName().objectForKey(Parametre.ENTITY_NAME);
            _paramCache = ERXArrayUtilities.dictionaryOfObjectsIndexedByKeyPath(allParams, Parametre.PARAM_KEY_KEY);
        }
        return _paramCache;
    }
    
    public static String paramStringForKey(String key) {
        Parametre param = allParametresByKey().objectForKey(key);
        return param != null ? param.paramValue() : null;
    }
    
    public static boolean paramBooleanForKey(String key) {
        String stringParam = paramStringForKey(key);
        return "OUI".equalsIgnoreCase(stringParam) || "O".equalsIgnoreCase(stringParam);
    }
    
}
