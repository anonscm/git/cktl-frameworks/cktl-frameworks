

// HistoCreditPositionne.java
// 
package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.validation.ERXValidationFactory;



public class HistoCreditPositionne extends EOHistoCreditPositionne
{

    public HistoCreditPositionne() {
        super();
    }
    
    public static HistoCreditPositionne creerHistoCreditPositionne(EOEditingContext editingContext, 
                                              Tranche tranche,
                                              EOPlanComptableExer planco,
                                              EOTypeCredit typeCredit,
                                              EOOrgan organ,
                                              BigDecimal montant
                                              ) {
        HistoCreditPositionne historique = HistoCreditPositionne.create(editingContext, null, null, null);
        historique.setHcpDate(new NSTimestamp());
        historique.setTrancheRelationship(tranche);
        historique.setTypeCreditRelationship(typeCredit);
        historique.setExerciceRelationship(tranche.exerciceCocktail().exercice());
        historique.setPlancoRelationship(planco);
        historique.setOrganRelationship(organ);
        historique.setHcpMontant(montant);
        return historique;
    }
    
    public static void checkPreCreation(VDepensesTranche depenses, BigDecimal montant) {
        // On vérifie que le montant des crédits positionnés ne dépassera pas le montant des dépenses.
        if (depenses != null && montant != null && depenses.tranche().contrat().isModeBudgetAvance()) {
            if (depenses.montantResteAPositionne().compareTo(montant) < 0) {
                throw ERXValidationFactory.defaultFactory().createCustomException(depenses, "MontantCreditDepasseDepenses");
            }
        }
    }
    
    private void fixHcpSuppr() {
        if(hcpSuppr() == null) {
            setHcpSuppr("N");
        }
    }
    
    @Override
    public void validateForInsert() throws ValidationException {
        super.validateForInsert();
        fixHcpSuppr();
    }
    
    @Override
    public void validateForUpdate() throws ValidationException {
        super.validateForUpdate();
        fixHcpSuppr();
    }
    
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (tranche().contrat().isModeBudgetAvance() && planco() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, PLANCO_KEY, null, "PlancoMissing");
    }
    
}
