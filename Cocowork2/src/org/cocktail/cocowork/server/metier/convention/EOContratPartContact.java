// DO NOT EDIT.  Make changes to ContratPartContact.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOContratPartContact extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWContratPartContact";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CONTACT = new ERXKey<Integer>("persIdContact");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> CONTRAT_PARTENAIRE = new ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("contratPartenaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> PARTENAIRE_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("partenaireStructure");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContact> TYPE_CONTACT = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContact>("typeContact");

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PERS_ID_CONTACT_KEY = PERS_ID_CONTACT.key();
  // Relationships
  public static final String CONTRAT_PARTENAIRE_KEY = CONTRAT_PARTENAIRE.key();
  public static final String INDIVIDU_KEY = INDIVIDU.key();
  public static final String PARTENAIRE_STRUCTURE_KEY = PARTENAIRE_STRUCTURE.key();
  public static final String TYPE_CONTACT_KEY = TYPE_CONTACT.key();

  private static Logger LOG = Logger.getLogger(EOContratPartContact.class);

  public ContratPartContact localInstanceIn(EOEditingContext editingContext) {
    ContratPartContact localInstance = (ContratPartContact)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(EOContratPartContact.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
        EOContratPartContact.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartContact.PERS_ID_KEY);
  }

  public Integer persIdContact() {
    return (Integer) storedValueForKey(EOContratPartContact.PERS_ID_CONTACT_KEY);
  }

  public void setPersIdContact(Integer value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
        EOContratPartContact.LOG.debug( "updating persIdContact from " + persIdContact() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartContact.PERS_ID_CONTACT_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.ContratPartenaire contratPartenaire() {
    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire)storedValueForKey(EOContratPartContact.CONTRAT_PARTENAIRE_KEY);
  }
  
  public void setContratPartenaire(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
    takeStoredValueForKey(value, EOContratPartContact.CONTRAT_PARTENAIRE_KEY);
  }

  public void setContratPartenaireRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
      EOContratPartContact.LOG.debug("updating contratPartenaire from " + contratPartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContratPartenaire(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.ContratPartenaire oldValue = contratPartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartContact.CONTRAT_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartContact.CONTRAT_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(EOContratPartContact.INDIVIDU_KEY);
  }
  
  public void setIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, EOContratPartContact.INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
      EOContratPartContact.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setIndividu(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartContact.INDIVIDU_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartContact.INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure partenaireStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOContratPartContact.PARTENAIRE_STRUCTURE_KEY);
  }
  
  public void setPartenaireStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOContratPartContact.PARTENAIRE_STRUCTURE_KEY);
  }

  public void setPartenaireStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
      EOContratPartContact.LOG.debug("updating partenaireStructure from " + partenaireStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPartenaireStructure(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = partenaireStructure();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartContact.PARTENAIRE_STRUCTURE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartContact.PARTENAIRE_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypeContact typeContact() {
    return (org.cocktail.cocowork.server.metier.convention.TypeContact)storedValueForKey(EOContratPartContact.TYPE_CONTACT_KEY);
  }
  
  public void setTypeContact(org.cocktail.cocowork.server.metier.convention.TypeContact value) {
    takeStoredValueForKey(value, EOContratPartContact.TYPE_CONTACT_KEY);
  }

  public void setTypeContactRelationship(org.cocktail.cocowork.server.metier.convention.TypeContact value) {
    if (EOContratPartContact.LOG.isDebugEnabled()) {
      EOContratPartContact.LOG.debug("updating typeContact from " + typeContact() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeContact(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeContact oldValue = typeContact();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartContact.TYPE_CONTACT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartContact.TYPE_CONTACT_KEY);
    }
  }
  

  public static ContratPartContact create(EOEditingContext editingContext, Integer persId
, Integer persIdContact
, org.cocktail.cocowork.server.metier.convention.ContratPartenaire contratPartenaire) {
    ContratPartContact eo = (ContratPartContact) EOUtilities.createAndInsertInstance(editingContext, EOContratPartContact.ENTITY_NAME);    
        eo.setPersId(persId);
        eo.setPersIdContact(persIdContact);
    eo.setContratPartenaireRelationship(contratPartenaire);
    return eo;
  }

  public static ERXFetchSpecification<ContratPartContact> fetchSpec() {
    return new ERXFetchSpecification<ContratPartContact>(EOContratPartContact.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ContratPartContact> fetchAll(EOEditingContext editingContext) {
    return EOContratPartContact.fetchAll(editingContext, null);
  }

  public static NSArray<ContratPartContact> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOContratPartContact.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<ContratPartContact> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ContratPartContact> fetchSpec = new ERXFetchSpecification<ContratPartContact>(EOContratPartContact.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ContratPartContact> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ContratPartContact fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOContratPartContact.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ContratPartContact fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ContratPartContact> eoObjects = EOContratPartContact.fetchAll(editingContext, qualifier, null);
    ContratPartContact eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWContratPartContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ContratPartContact fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOContratPartContact.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ContratPartContact fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    ContratPartContact eoObject = EOContratPartContact.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWContratPartContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ContratPartContact localInstanceIn(EOEditingContext editingContext, ContratPartContact eo) {
    ContratPartContact localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}