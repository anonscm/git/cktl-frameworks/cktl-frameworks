package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.TrancheBudget;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateurOrgan;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.eof.qualifiers.ERXQualifierInSubquery;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * 
 * Service pour la recherche avancée de contrats.
 * 
 * 
 * @author Alexis Tual
 *
 */
public class RechercheContratServiceImpl implements RechercheContratService {

    /**
     * @param utlOrdre le utlOdre (table JEFY_ADMIN.UTILISATEUR)
     * @return le qualifier des contrats dont le créateur correspond à <code>utlOrdre</code>
     */
    protected EOQualifier qualifierCreateurContrat(int utlOrdre) {
        return Contrat.UTL_ORDRE_CREATION.eq(utlOrdre);
    }

    /**
     * @param noIndividu le noIndividu 
     * @return le qualifier des contrats dont les secrétaires du centre de responsabilité 
     *         correspond à <code>noIndividu</code>
     */
    protected EOQualifier qualifierSecretaireContrat(int noIndividu) {
        EOQualifier qualSecretariats = EOStructure.TO_SECRETARIATS.dot(EOSecretariat.NO_INDIVIDU_KEY).containsObject(noIndividu);
        ERXQualifierInSubquery qual = new ERXQualifierInSubquery(
                qualSecretariats, EOStructure.ENTITY_NAME, Contrat.CON_CR.key(), EOStructure.C_STRUCTURE.key());
        return qual;
    }
    
    /**
     * @param noIndividu le noIndividu
     * @return le qualifier des contrats dont les crédits positionnés ont une ligne budgétaire
     *         sur laquelle l'utilisateur avec <code>noIndividu</code> a des droits
     */
    protected EOQualifier qualifierOrganContrat(int noIndividu) {
        EOQualifier qualTranches = Tranche.TRANCHE_BUDGETS
                                    .dot(TrancheBudget.ORGAN)
                                    .dot(EOOrgan.UTILISATEUR_ORGANS_KEY)
                                    .dot(EOUtilisateurOrgan.UTILISATEUR_KEY)
                                    .dot(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur.NO_INDIVIDU_KEY)
                                    .containsObject(noIndividu);
        ERXQualifierInSubquery qual = new ERXQualifierInSubquery(
                qualTranches, Tranche.ENTITY_NAME,Contrat.CON_ORDRE.key(),Tranche.CON_ORDRE.key());
        return qual;
    }
    
    /**
     * @param persId le persId
     * @return le qualifier des contrat dont le ou les partenaires correspond à <code>persId</code>
     */
    protected EOQualifier qualifierPartenaireContrat(int persId) {
        EOQualifier qualPartenaire = ContratPartenaire.PERS_ID.containsObject(persId);
        ERXQualifierInSubquery qual = new ERXQualifierInSubquery(
                qualPartenaire, ContratPartenaire.ENTITY_NAME, Contrat.CON_ORDRE.key(), ContratPartenaire.CON_ORDRE.key());
        return qual;
    }
    
    /**
     * @param utlOrdre l'utlOrdre
     * @param noIndividu le noIndividu
     * @param persId le persId
     * @param tous true si on veut afficher toutes les conventions, false si on veut afficher que celles de
     *        l'utilisateur
     * @return un qualifier correspondant aux contrats
     *          de l'utilisateur ayant les <code>utlOrdre</code>, <code>noIndividu</code>, <code>persId</code>
     *          donnés
     */
    protected EOQualifier qualifierForUtilisateurContrat(int utlOrdre, int noIndividu, int persId, boolean tous) {
        EOQualifier qual = null;
        if (!tous) {
            qual = ERXQ.or(
                    qualifierCreateurContrat(utlOrdre),
                    qualifierOrganContrat(noIndividu),
                    qualifierPartenaireContrat(persId),
                    qualifierSecretaireContrat(noIndividu)
                    );
        }
        return qual;
    }
   
    /**
     * @return un qualifier pour les contrat de type conventions non supprimées
     */
    protected EOQualifier qualifierForConventions() {
        EOQualifier qual = ERXQ.and(
                Contrat.TYPE_CLASSIFICATION_CONTRAT
                        .dot(TypeClassificationContrat.TCC_CODE_KEY)
                            .eq(TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_CONV),
                Contrat.QUALIFIER_NON_SUPPR,
                Contrat.AVENANTS.dot(Avenant.AVT_INDEX).eq(0));
        return qual;
    }
    
    /** 
     * {@inheritDoc}
     */
    public NSArray<ResultatRechercheBean> conventionsForUtilisateur(
            EOEditingContext ec, int utlOrdre, int noIndividu, int persId, boolean voirToutes) {
        ERXFetchSpecification<EOEnterpriseObject> fetchSpec = new ERXFetchSpecification<EOEnterpriseObject>(Contrat.ENTITY_NAME);
        fetchSpec.setFetchesRawRows(true);
        EOQualifier qual = ERXQ.and(
                qualifierForConventions(),
                qualifierForUtilisateurContrat(utlOrdre, noIndividu, persId, voirToutes));
        fetchSpec.setQualifier(qual);
        fetchSpec.setUsesDistinct(true);
        fetchSpec.setRawRowKeyPaths(ResultatRechercheBean.ROW_KEYS);
        NSArray<NSDictionary<String, Object>> resultats = fetchSpec.fetchRawRows(ec);
        NSMutableArray<ResultatRechercheBean> resultatsRecherche = new NSMutableArray<ResultatRechercheBean>();
        for (NSDictionary<String, Object> row : resultats) {
            resultatsRecherche.addObject(new ResultatRechercheBean(row));
        }
        return resultatsRecherche.immutableClone();
    }

    /** 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public NSArray<DisciplineBean> disciplinesFromResultats(NSArray<ResultatRechercheBean> resultats) {
        NSArray<DisciplineBean> disciplines = (NSArray<DisciplineBean>) resultats.valueForKey(ResultatRechercheBean.DISCIPLINE_KEY);
        disciplines = ERXArrayUtilities.removeNullValues(ERXArrayUtilities.distinct(disciplines));
        disciplines = ERXS.sorted(disciplines, ERXS.asc(DisciplineBean.DISC_LIBELLE));
        return disciplines;
    }
    
    /** 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public NSArray<TypeContratBean> typesContratFromResultats(NSArray<ResultatRechercheBean> resultats) {
        NSArray<TypeContratBean> typesContrat = (NSArray<TypeContratBean>) resultats.valueForKey(ResultatRechercheBean.TYPE_CONTRAT_KEY);
        typesContrat = ERXArrayUtilities.removeNullValues(ERXArrayUtilities.distinct(typesContrat));
        typesContrat = ERXS.sorted(typesContrat, ERXS.asc(TypeContratBean.TYPE_CON_LIBELLE));
        return typesContrat;
    }

    /** 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public NSArray<ModeGestionBean> modesGestionFromResultats(NSArray<ResultatRechercheBean> resultats) {
        NSArray<ModeGestionBean> modesGestion = (NSArray<ModeGestionBean>) resultats.valueForKey(ResultatRechercheBean.MODE_GESTION_KEY);
        modesGestion = ERXArrayUtilities.removeNullValues(ERXArrayUtilities.distinct(modesGestion));
        modesGestion = ERXS.sorted(modesGestion, ERXS.asc(ModeGestionBean.MG_LIBELLE));
        return ERXArrayUtilities.removeNullValues(ERXArrayUtilities.distinct(modesGestion));
    }
    
    /** 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public NSArray<ServiceGestionnaireBean> servicesGestionnairesFromResultats(NSArray<ResultatRechercheBean> resultats) {
        NSArray<ServiceGestionnaireBean> centresResp = (NSArray<ServiceGestionnaireBean>) resultats.valueForKey(ResultatRechercheBean.SERVICE_GEST_KEY);
        centresResp = ERXArrayUtilities.removeNullValues(ERXArrayUtilities.distinct(centresResp));
        centresResp = ERXS.sorted(centresResp, ERXS.asc(ServiceGestionnaireBean.LIBELLE));
        return centresResp;
    }
    
    /** 
     * {@inheritDoc}
     */
    public NSArray<ResultatRechercheBean> filtrerConventions(NSArray<ResultatRechercheBean> resultats, Filtre filtre) {
        EOQualifier qualifier = filtre.qualifier();
        return ERXQ.filtered(resultats, qualifier);
    }
    
}
