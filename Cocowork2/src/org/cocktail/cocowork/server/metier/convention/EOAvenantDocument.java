// DO NOT EDIT.  Make changes to AvenantDocument.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAvenantDocument extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWAvenantDocument";

  // Attribute Keys
  public static final ERXKey<Integer> COU_NUMERO = new ERXKey<Integer>("couNumero");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant> AVENANT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant>("avenant");
  public static final ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument> DOCUMENT = new ERXKey<org.cocktail.fwkcktlged.serveur.metier.EODocument>("document");

  // Attributes
  public static final String COU_NUMERO_KEY = COU_NUMERO.key();
  // Relationships
  public static final String AVENANT_KEY = AVENANT.key();
  public static final String DOCUMENT_KEY = DOCUMENT.key();

  private static Logger LOG = Logger.getLogger(EOAvenantDocument.class);

  public AvenantDocument localInstanceIn(EOEditingContext editingContext) {
    AvenantDocument localInstance = (AvenantDocument)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer couNumero() {
    return (Integer) storedValueForKey(EOAvenantDocument.COU_NUMERO_KEY);
  }

  public void setCouNumero(Integer value) {
    if (EOAvenantDocument.LOG.isDebugEnabled()) {
        EOAvenantDocument.LOG.debug( "updating couNumero from " + couNumero() + " to " + value);
    }
    takeStoredValueForKey(value, EOAvenantDocument.COU_NUMERO_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.Avenant avenant() {
    return (org.cocktail.cocowork.server.metier.convention.Avenant)storedValueForKey(EOAvenantDocument.AVENANT_KEY);
  }
  
  public void setAvenant(org.cocktail.cocowork.server.metier.convention.Avenant value) {
    takeStoredValueForKey(value, EOAvenantDocument.AVENANT_KEY);
  }

  public void setAvenantRelationship(org.cocktail.cocowork.server.metier.convention.Avenant value) {
    if (EOAvenantDocument.LOG.isDebugEnabled()) {
      EOAvenantDocument.LOG.debug("updating avenant from " + avenant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAvenant(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Avenant oldValue = avenant();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantDocument.AVENANT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantDocument.AVENANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlged.serveur.metier.EODocument document() {
    return (org.cocktail.fwkcktlged.serveur.metier.EODocument)storedValueForKey(EOAvenantDocument.DOCUMENT_KEY);
  }
  
  public void setDocument(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
    takeStoredValueForKey(value, EOAvenantDocument.DOCUMENT_KEY);
  }

  public void setDocumentRelationship(org.cocktail.fwkcktlged.serveur.metier.EODocument value) {
    if (EOAvenantDocument.LOG.isDebugEnabled()) {
      EOAvenantDocument.LOG.debug("updating document from " + document() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDocument(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlged.serveur.metier.EODocument oldValue = document();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantDocument.DOCUMENT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantDocument.DOCUMENT_KEY);
    }
  }
  

  public static AvenantDocument create(EOEditingContext editingContext, org.cocktail.fwkcktlged.serveur.metier.EODocument document) {
    AvenantDocument eo = (AvenantDocument) EOUtilities.createAndInsertInstance(editingContext, EOAvenantDocument.ENTITY_NAME);    
    eo.setDocumentRelationship(document);
    return eo;
  }

  public static ERXFetchSpecification<AvenantDocument> fetchSpec() {
    return new ERXFetchSpecification<AvenantDocument>(EOAvenantDocument.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AvenantDocument> fetchAll(EOEditingContext editingContext) {
    return EOAvenantDocument.fetchAll(editingContext, null);
  }

  public static NSArray<AvenantDocument> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAvenantDocument.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<AvenantDocument> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AvenantDocument> fetchSpec = new ERXFetchSpecification<AvenantDocument>(EOAvenantDocument.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AvenantDocument> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AvenantDocument fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantDocument.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantDocument fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AvenantDocument> eoObjects = EOAvenantDocument.fetchAll(editingContext, qualifier, null);
    AvenantDocument eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWAvenantDocument that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantDocument fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantDocument.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantDocument fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    AvenantDocument eoObject = EOAvenantDocument.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWAvenantDocument that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantDocument localInstanceIn(EOEditingContext editingContext, AvenantDocument eo) {
    AvenantDocument localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}