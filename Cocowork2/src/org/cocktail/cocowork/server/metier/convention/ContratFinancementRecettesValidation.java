package org.cocktail.cocowork.server.metier.convention;

import java.io.Serializable;
import java.math.BigDecimal;

public class ContratFinancementRecettesValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Verifie que le total des contributions des partenaires est superieur ou égal 
     * à la somme des budgets de recettes positionnés.
     * 
     * @param contrat le contrat a valider.
     * @return true si le financement du contrat au niveau des recettes est valide, false sinon.
     */
    public boolean isSatisfiedBy(Contrat contrat) {
        if (contrat == null) {
            return false;
        }
        
        BigDecimal totalContributions = contrat.totalContributions();
        BigDecimal totalPositionneRecContrat = contrat.totalPositionneRecContrat();
        return totalContributions.subtract(totalPositionneRecContrat).doubleValue() >= 0;
    } 
    
}
