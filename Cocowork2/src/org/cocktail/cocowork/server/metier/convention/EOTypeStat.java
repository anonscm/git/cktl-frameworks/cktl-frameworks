// DO NOT EDIT.  Make changes to TypeStat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeStat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeStat";

  // Attribute Keys
  public static final ERXKey<String> TS_COMMENTAIRE = new ERXKey<String>("tsCommentaire");
  public static final ERXKey<String> TS_LIBELLE = new ERXKey<String>("tsLibelle");
  public static final ERXKey<String> TS_LIBELLE_COURT = new ERXKey<String>("tsLibelleCourt");
  // Relationship Keys

  // Attributes
  public static final String TS_COMMENTAIRE_KEY = TS_COMMENTAIRE.key();
  public static final String TS_LIBELLE_KEY = TS_LIBELLE.key();
  public static final String TS_LIBELLE_COURT_KEY = TS_LIBELLE_COURT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeStat.class);

  public TypeStat localInstanceIn(EOEditingContext editingContext) {
    TypeStat localInstance = (TypeStat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tsCommentaire() {
    return (String) storedValueForKey(EOTypeStat.TS_COMMENTAIRE_KEY);
  }

  public void setTsCommentaire(String value) {
    if (EOTypeStat.LOG.isDebugEnabled()) {
        EOTypeStat.LOG.debug( "updating tsCommentaire from " + tsCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeStat.TS_COMMENTAIRE_KEY);
  }

  public String tsLibelle() {
    return (String) storedValueForKey(EOTypeStat.TS_LIBELLE_KEY);
  }

  public void setTsLibelle(String value) {
    if (EOTypeStat.LOG.isDebugEnabled()) {
        EOTypeStat.LOG.debug( "updating tsLibelle from " + tsLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeStat.TS_LIBELLE_KEY);
  }

  public String tsLibelleCourt() {
    return (String) storedValueForKey(EOTypeStat.TS_LIBELLE_COURT_KEY);
  }

  public void setTsLibelleCourt(String value) {
    if (EOTypeStat.LOG.isDebugEnabled()) {
        EOTypeStat.LOG.debug( "updating tsLibelleCourt from " + tsLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeStat.TS_LIBELLE_COURT_KEY);
  }


  public static TypeStat create(EOEditingContext editingContext) {
    TypeStat eo = (TypeStat) EOUtilities.createAndInsertInstance(editingContext, EOTypeStat.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<TypeStat> fetchSpec() {
    return new ERXFetchSpecification<TypeStat>(EOTypeStat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeStat> fetchAll(EOEditingContext editingContext) {
    return EOTypeStat.fetchAll(editingContext, null);
  }

  public static NSArray<TypeStat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeStat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeStat> fetchSpec = new ERXFetchSpecification<TypeStat>(EOTypeStat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeStat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeStat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeStat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeStat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeStat> eoObjects = EOTypeStat.fetchAll(editingContext, qualifier, null);
    TypeStat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeStat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeStat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeStat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeStat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeStat eoObject = EOTypeStat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeStat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeStat localInstanceIn(EOEditingContext editingContext, TypeStat eo) {
    TypeStat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeStat> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeStat.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeStat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeStat> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeStat.ENTITY_NAME);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeStat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}