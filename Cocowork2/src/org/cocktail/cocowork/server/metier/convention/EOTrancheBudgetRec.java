// DO NOT EDIT.  Make changes to TrancheBudgetRec.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTrancheBudgetRec extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTrancheBudgetRec";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> TBR_DATE_BUDGET = new ERXKey<NSTimestamp>("tbrDateBudget");
  public static final ERXKey<NSTimestamp> TBR_DATE_CREATION = new ERXKey<NSTimestamp>("tbrDateCreation");
  public static final ERXKey<NSTimestamp> TBR_DATE_MODIF = new ERXKey<NSTimestamp>("tbrDateModif");
  public static final ERXKey<NSTimestamp> TBR_DATE_VALID = new ERXKey<NSTimestamp>("tbrDateValid");
  public static final ERXKey<java.math.BigDecimal> TBR_MNT_BUDGET = new ERXKey<java.math.BigDecimal>("tbrMntBudget");
  public static final ERXKey<java.math.BigDecimal> TBR_MNT_DISPO = new ERXKey<java.math.BigDecimal>("tbrMntDispo");
  public static final ERXKey<java.math.BigDecimal> TBR_MNT_VALID = new ERXKey<java.math.BigDecimal>("tbrMntValid");
  public static final ERXKey<java.math.BigDecimal> TBR_MONTANT = new ERXKey<java.math.BigDecimal>("tbrMontant");
  public static final ERXKey<String> TBR_SUPPR = new ERXKey<String>("tbrSuppr");
  public static final ERXKey<String> TBR_VALID = new ERXKey<String>("tbrValid");
  public static final ERXKey<Integer> UTL_ORDRE_CREATION = new ERXKey<Integer>("utlOrdreCreation");
  public static final ERXKey<Integer> UTL_ORDRE_MODIF = new ERXKey<Integer>("utlOrdreModif");
  public static final ERXKey<Integer> UTL_ORDRE_VALID = new ERXKey<Integer>("utlOrdreValid");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organ");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String TBR_DATE_BUDGET_KEY = TBR_DATE_BUDGET.key();
  public static final String TBR_DATE_CREATION_KEY = TBR_DATE_CREATION.key();
  public static final String TBR_DATE_MODIF_KEY = TBR_DATE_MODIF.key();
  public static final String TBR_DATE_VALID_KEY = TBR_DATE_VALID.key();
  public static final String TBR_MNT_BUDGET_KEY = TBR_MNT_BUDGET.key();
  public static final String TBR_MNT_DISPO_KEY = TBR_MNT_DISPO.key();
  public static final String TBR_MNT_VALID_KEY = TBR_MNT_VALID.key();
  public static final String TBR_MONTANT_KEY = TBR_MONTANT.key();
  public static final String TBR_SUPPR_KEY = TBR_SUPPR.key();
  public static final String TBR_VALID_KEY = TBR_VALID.key();
  public static final String UTL_ORDRE_CREATION_KEY = UTL_ORDRE_CREATION.key();
  public static final String UTL_ORDRE_MODIF_KEY = UTL_ORDRE_MODIF.key();
  public static final String UTL_ORDRE_VALID_KEY = UTL_ORDRE_VALID.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOTrancheBudgetRec.class);

  public TrancheBudgetRec localInstanceIn(EOEditingContext editingContext) {
    TrancheBudgetRec localInstance = (TrancheBudgetRec)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp tbrDateBudget() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_BUDGET_KEY);
  }

  public void setTbrDateBudget(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateBudget from " + tbrDateBudget() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_BUDGET_KEY);
  }

  public NSTimestamp tbrDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_CREATION_KEY);
  }

  public void setTbrDateCreation(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateCreation from " + tbrDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_CREATION_KEY);
  }

  public NSTimestamp tbrDateModif() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_MODIF_KEY);
  }

  public void setTbrDateModif(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateModif from " + tbrDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_MODIF_KEY);
  }

  public NSTimestamp tbrDateValid() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_VALID_KEY);
  }

  public void setTbrDateValid(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateValid from " + tbrDateValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_VALID_KEY);
  }

  public java.math.BigDecimal tbrMntBudget() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MNT_BUDGET_KEY);
  }

  public void setTbrMntBudget(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMntBudget from " + tbrMntBudget() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MNT_BUDGET_KEY);
  }

  public java.math.BigDecimal tbrMntDispo() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MNT_DISPO_KEY);
  }

  public void setTbrMntDispo(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMntDispo from " + tbrMntDispo() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MNT_DISPO_KEY);
  }

  public java.math.BigDecimal tbrMntValid() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MNT_VALID_KEY);
  }

  public void setTbrMntValid(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMntValid from " + tbrMntValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MNT_VALID_KEY);
  }

  public java.math.BigDecimal tbrMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MONTANT_KEY);
  }

  public void setTbrMontant(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMontant from " + tbrMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MONTANT_KEY);
  }

  public String tbrSuppr() {
    return (String) storedValueForKey(EOTrancheBudgetRec.TBR_SUPPR_KEY);
  }

  public void setTbrSuppr(String value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrSuppr from " + tbrSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_SUPPR_KEY);
  }

  public String tbrValid() {
    return (String) storedValueForKey(EOTrancheBudgetRec.TBR_VALID_KEY);
  }

  public void setTbrValid(String value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrValid from " + tbrValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_VALID_KEY);
  }

  public Integer utlOrdreCreation() {
    return (Integer) storedValueForKey(EOTrancheBudgetRec.UTL_ORDRE_CREATION_KEY);
  }

  public void setUtlOrdreCreation(Integer value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating utlOrdreCreation from " + utlOrdreCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.UTL_ORDRE_CREATION_KEY);
  }

  public Integer utlOrdreModif() {
    return (Integer) storedValueForKey(EOTrancheBudgetRec.UTL_ORDRE_MODIF_KEY);
  }

  public void setUtlOrdreModif(Integer value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating utlOrdreModif from " + utlOrdreModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.UTL_ORDRE_MODIF_KEY);
  }

  public Integer utlOrdreValid() {
    return (Integer) storedValueForKey(EOTrancheBudgetRec.UTL_ORDRE_VALID_KEY);
  }

  public void setUtlOrdreValid(Integer value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating utlOrdreValid from " + utlOrdreValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.UTL_ORDRE_VALID_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOTrancheBudgetRec.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOTrancheBudgetRec.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.ORGAN_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOTrancheBudgetRec.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(EOTrancheBudgetRec.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.TYPE_CREDIT_KEY);
    }
  }
  

  public static TrancheBudgetRec create(EOEditingContext editingContext, Integer utlOrdreCreation
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit) {
    TrancheBudgetRec eo = (TrancheBudgetRec) EOUtilities.createAndInsertInstance(editingContext, EOTrancheBudgetRec.ENTITY_NAME);    
        eo.setUtlOrdreCreation(utlOrdreCreation);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  public static ERXFetchSpecification<TrancheBudgetRec> fetchSpec() {
    return new ERXFetchSpecification<TrancheBudgetRec>(EOTrancheBudgetRec.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext) {
    return EOTrancheBudgetRec.fetchAll(editingContext, null);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTrancheBudgetRec.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TrancheBudgetRec> fetchSpec = new ERXFetchSpecification<TrancheBudgetRec>(EOTrancheBudgetRec.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TrancheBudgetRec> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TrancheBudgetRec fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetRec.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetRec fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TrancheBudgetRec> eoObjects = EOTrancheBudgetRec.fetchAll(editingContext, qualifier, null);
    TrancheBudgetRec eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTrancheBudgetRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetRec fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetRec.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetRec fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TrancheBudgetRec eoObject = EOTrancheBudgetRec.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTrancheBudgetRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetRec localInstanceIn(EOEditingContext editingContext, TrancheBudgetRec eo) {
    TrancheBudgetRec localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}