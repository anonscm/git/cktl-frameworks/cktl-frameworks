/**
 * Cocowork
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.server.metier.convention.procedure.utilitaire;

import org.cocktail.cocowork.common.procedure.StoredProcedure;
import org.cocktail.cocowork.common.tools.ModelUtilities;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ProcedureUtilitaire extends StoredProcedure 
{
	protected static final ModelUtilities modelUtilities = new ModelUtilities();
	

	/**
	 * Constructeur.
	 * @param ec
	 * @param name
	 * @param orderedArgumentsNames
	 */
	protected ProcedureUtilitaire(EOEditingContext ec, String name, String[] orderedArgumentsNames) {
		super(ec, name, orderedArgumentsNames);
	}
	
}
