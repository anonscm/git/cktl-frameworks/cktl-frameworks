package org.cocktail.cocowork.server.metier.convention.procedure.suivirecette;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Procedure qui calcule le total des recettes au titre d'une convention sur un exercice, 
 * pour un type de credit et une ligne de l'organigramme budgetaire.
 * 
 */
public class ProcedureGetTotalRecette extends ProcedureSuiviRecette
{
	protected final static String PROCEDURE_NAME = "GetTotalRecette";
	
	
	/**
	 * Constructeur protected.
	 * @param ec Editing context a utiliser.
	 */
	public ProcedureGetTotalRecette(final EOEditingContext ec) { 
		super(ec, PROCEDURE_NAME);
	}
	
}
