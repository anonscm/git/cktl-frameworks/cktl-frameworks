package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureAbstract;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * Caracteristiques d'un item servant à calculer un budget previsionnel.
 * Implementee actuellement par {@link SbRecette} et {@link SbDepense}.
 * 
 * @author Alexis TUAL <alexis.tual at cocktail.org>
 *
 */
public interface Budgetable extends EOEnterpriseObject {

    /**
     * Clef correspondant à l'organ attendue par bibasse (niveau 3 ou 4)
     */
    public static final String ORGAN_FOR_BIBASSE_KEY = "organForBibasse";
    /**
     * Clef correspondant au budget previsionnel
     */
    public static final String PREVISION_BUDGET_KEY = "previsionBudget";
    /**
     * Clef correspondant à l'organ surlequel se fait l'acte (peut être < 3)
     */
    public static final String ORGAN_KEY = "organ";

    /**
     * @return la repartition du montant selon la nature 
     */
    public NSArray<IRepartMontant> repartPlanComptables();
    
    /**
     * @return le type de credit
     */
    public EOTypeCredit typeCredit();
    
    /**
     * @return l'action lolf
     */
    public EOLolfNomenclatureAbstract lolf();
    
    /**
     * @return le budget previsionnel correspondant
     */
    public EOPrevisionBudget previsionBudget();
    
    /**
     * Associe le budget previsionnel
     * @param value le budget prev
     */
    public void setPrevisionBudgetRelationship(EOPrevisionBudget value);
    
    /**
     * @return la date de l'acte 
     */
    public NSTimestamp datePrevisionnelle();
    
    /**
     * @return le montant total 
     */
    public BigDecimal montantHt();
    
    /**
     * @return le libelle
     */
    public String libelle();

}
