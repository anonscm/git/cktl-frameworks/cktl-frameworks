package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSKeyValueCoding;

public interface IRepartMontant extends EOEnterpriseObject {

    public BigDecimal montantHt();
    public void setMontantHt(BigDecimal value);
    public EOEnterpriseObject nomenclature();
    public void setNomenclature(EOEnterpriseObject value);
    
}
