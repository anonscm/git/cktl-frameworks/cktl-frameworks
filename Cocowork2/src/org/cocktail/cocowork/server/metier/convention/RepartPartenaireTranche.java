package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.Cocowork;
import org.cocktail.cocowork.server.metier.convention.service.CalculMontantParticipationDisponible;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXKey;

public class RepartPartenaireTranche extends EORepartPartenaireTranche {

    @Inject
    private CalculMontantParticipationDisponible calculMontantParticipationDisponible;
    
    public RepartPartenaireTranche() {
        super();
    }
    
	/**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee
	 * @throws InstantiationException Probleme d'instanciation.
	 */
	static public RepartPartenaireTranche instanciate(EOEditingContext ec) throws Exception {
		return (RepartPartenaireTranche) Factory.instanceForEntity(ec, ENTITY_NAME);
	}
	
	@Override
	public void validateForSave() throws ValidationException {
	    super.validateForSave();
	    tranche().checkMontantTranche();
	}
	
	public FraisGestion getOrCreateFraisGestion() {
	    FraisGestion frais = fraisGestions() != null ? fraisGestions().lastObject() : null;
	    if (frais == null) {
	        frais = FraisGestion.create(editingContext(), "Frais", BigDecimal.ZERO, BigDecimal.ZERO, this);
	    }
	    return frais;
	}
	
	public FraisGestion fraisGestion() {
	    FraisGestion frais = fraisGestions() != null ? fraisGestions().lastObject() : null;
	    return frais;
	}

	@Override
	public void setRptMontantParticipation(BigDecimal value) {
	    super.setRptMontantParticipation(value);
	    FraisGestion fraisGestion = getOrCreateFraisGestion();
	    if (fraisGestion != null) {
	        fraisGestion.recalculerFrais();
	    }
	}
	
	public BigDecimal fraisGestionAsDecimal() {
	    FraisGestion fraisGestion = fraisGestion();
	    BigDecimal result = BigDecimal.ZERO;
	    if (fraisGestion != null) {
	        result = fraisGestion.fgMontant();
	    }
	    return result;
	}
	
	public static final ERXKey<BigDecimal> MONTANT_PARTICIPATION_DISPONIBLE = 
	        new ERXKey<BigDecimal>("montantParticipationDisponible");
	
	public void setCalculMontantParticipationDisponible(CalculMontantParticipationDisponible calculMontantParticipationDisponible) {
        this.calculMontantParticipationDisponible = calculMontantParticipationDisponible;
    }
	
	/**
	 * @return le montant de la participation disponible
	 */
	public BigDecimal montantParticipationDisponible() {
	   return calculMontantParticipationDisponible.montantParticipationDisponible(this);
	}
	
}
