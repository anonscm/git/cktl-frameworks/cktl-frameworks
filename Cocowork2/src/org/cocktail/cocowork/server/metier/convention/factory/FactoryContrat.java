package org.cocktail.cocowork.server.metier.convention.factory;

import org.cocktail.cocowork.common.exception.ExceptionArgument;
import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.IndicateursContrat;
import org.cocktail.cocowork.server.metier.convention.Projet;
import org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.TypeReconduction;
import org.cocktail.cocowork.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * 
 * @author Michael HALLER, Consortium Cocktail, 2008
 *
 */
public class FactoryContrat extends Factory 
{
	protected FactoryAvenant avenantFactory;
	
	
	/**
	 * 
	 * @param ec
	 * @param withtrace
	 */
	public FactoryContrat(final EOEditingContext ec, final Boolean withtrace) {
		super(ec, withtrace);
		
		this.avenantFactory = new FactoryAvenant(ec, withtrace);
	}
	
	/**
	 * Creation d'un objet Contrat.
	 * @param typeClassificationContrat
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'objet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat cree
	 * @throws InstantiationException 
	 * @throws ExceptionFinder 
	 */
	public Contrat creerContrat(
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur) throws ExceptionUtilisateur, Exception {
		
		trace("creerContrat()");
		//trace(etablissementGestionnaire);
		//trace(centreResponsabiliteGestionnaire);
		trace(typeContrat);
		trace(referenceExterne);
		trace(objet);
		trace(observations);
		trace(dateCloture);
		trace(typeReconduction);
		trace(createur);
		
//		if (dateDebut == null)
//			throw new ArgumentException("La date de d\u00E9but doit \u00EAtre fournie.");
//		if (dateFin == null)
//			throw new ArgumentException("La date de fin doit \u00EAtre fournie.");
//		if (montantGlobalHT!=null && montantGlobalHT.signum()<0)
//			throw new ArgumentException("Le montant global ne doit pas \u00EAtre n\u00E9gatif.");
//		if (tauxTva!=null && tauxTva.signum()<0)
//			throw new ArgumentException("Le taux de TVA ne doit pas \u00EAtre n\u00E9gatif.");
		
		
		Contrat contrat = creerContratVierge(typeClassificationContrat, createur);
		
		
		modifierContrat(
				contrat,
				typeClassificationContrat,
				referenceExterne, 
				etablissementGestionnaire, 
				centreResponsabiliteGestionnaire, 
				typeContrat, 
				objet, 
				observations, 
				dateCloture,
				typeReconduction, 
				naf,
				null, 
				null);
		
		return contrat;
	}
	
	/**
	 * Creation d'un objet Contrat.
	 * @param projet
	 * @param typeClassificationContrat
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'objet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat cree
	 * @throws InstantiationException 
	 * @throws ExceptionFinder 
	 */
	public Contrat creerContrat(
			final Projet projet,
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur) throws ExceptionUtilisateur, Exception {
		
		Contrat newContrat=creerContrat(typeClassificationContrat, etablissementGestionnaire, centreResponsabiliteGestionnaire, typeContrat, referenceExterne, objet, observations, dateCloture, typeReconduction, naf, createur);
		
		FactoryProjet fp = new FactoryProjet(ec);
		fp.ajouterContrat(projet, newContrat);
		
		return newContrat;
	}
		

	/**
	 * Creation d'un objet Contrat minimum.
	 * @param createur Utilisateur createur de l'objet (obligatoire)
	 * @return Le contrat vierge cree
	 * @throws InstantiationException
	 * @throws ExceptionFinder 
	 */
	public Contrat creerContratVierge(final TypeClassificationContrat typeClassificationContrat,final EOUtilisateur createur) throws Exception, ExceptionUtilisateur {
		
		Contrat contrat = Contrat.instanciate(ec);
		
		contrat.setConGroupeBud(Contrat.CON_GROUPE_BUD_NON);
		contrat.setConSuppr(Contrat.CON_SUPPR_NON);
		contrat.setConDateCreation(new NSTimestamp());
		contrat.setUtilisateurCreationRelationship(createur);
		contrat.setUtilisateurModifRelationship(createur);
		// TODO Determiner le service de l'utilisateur afin d'initialiser le centre gestionnaire et donc l'etablissement
		contrat.setTypeClassificationContratRelationship(typeClassificationContrat);
		
		contrat.setExerciceCocktailRelationship(new FinderExerciceCocktail(ec).findExerciceCocktailCourant());
		
		return contrat;
	}
	
	/**
	 * Modification d'un contrat
	 * @param contrat Contrat correspondant a la convention a modifier.
	 * @param referenceExterne Reference externe facultative.
	 * @param etablissementGestionnaire Etablissement gestionnaire du contrat.
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite du contrat
	 * @param typeContrat Type de contrat (ex: Convention de formation)
	 * @param objet Objet du contrat
	 * @param observations Remarques eventuelles
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction (ex: Expresse)
 	 * @param naf code naf (domaine fonctionnel)
	 * @param utilisateur Utilisateur modificateur du contrat.
	 * @param dateModification Pour forcer une date de modification de l'avenant.
	 * @throws ExceptionUtilisateur, Exception 
	 */
	public void modifierContrat(
			final Contrat contrat,
			final TypeClassificationContrat typeClassificationContrat,
			final String referenceExterne,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String objet,
			final String observations,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur utilisateur,
			final NSTimestamp dateModification) throws ExceptionUtilisateur, Exception {

		trace("modifierContrat()");
		trace(contrat);
		trace(referenceExterne);
		//trace(etablissementGestionnaire);
		//trace(centreResponsabiliteGestionnaire);
		trace(typeContrat);
		trace(objet);
		trace(observations);
		trace(dateCloture);
		trace(typeReconduction);
		trace(utilisateur);
		trace(dateModification);
		
		if (contrat == null)
			throw new ExceptionUtilisateur("Le contrat doit \u00EAtre fourni.");
		if (typeClassificationContrat == null)
			throw new ExceptionUtilisateur("Le type de classification doit \u00EAtre fourni.");
			
		if (etablissementGestionnaire == null)
			throw new ExceptionUtilisateur("L'\u00E9tablissement gestionnaire doit \u00EAtre fourni.");
		if (centreResponsabiliteGestionnaire == null)
			throw new ExceptionUtilisateur("Le centre de responsabilit\u00E9 gestionnaire doit \u00EAtre fourni.");
		if (typeContrat == null)
			throw new ExceptionUtilisateur("Le type de contrat doit \u00EAtre fourni.");
		if (typeReconduction == null)
			throw new ExceptionUtilisateur("Le type de reconduction doit \u00EAtre fourni.");
		if (objet == null)
			throw new ExceptionUtilisateur("L'objet doit \u00EAtre fourni.");
		if (objet.length() > Contrat.OBJET_LONGUEUR_MAX)
			throw new ExceptionUtilisateur("L'objet ne doit pas d\u00E9passer "+Contrat.OBJET_LONGUEUR_MAX+" caract\u00E8res.");
		
		contrat.setTypeClassificationContratRelationship(typeClassificationContrat);
		contrat.setConReferenceExterne(referenceExterne);
		contrat.setEtablissementRelationship(etablissementGestionnaire, utilisateur.toPersonne().persId());
		contrat.setCentreResponsabiliteRelationship(centreResponsabiliteGestionnaire, utilisateur.toPersonne().persId());
		contrat.setTypeContratRelationship(typeContrat);
		contrat.setConObjet(objet);
		contrat.setConObjetCourt(
				objet.substring(0, objet.length()<Contrat.OBJET_COURT_LONGUEUR_MAX ? objet.length() : Contrat.OBJET_COURT_LONGUEUR_MAX));
		contrat.setConObservations(observations);
		contrat.setConDateCloture(dateCloture);
		contrat.setTypeReconductionRelationship(typeReconduction);
		contrat.setCodeNafRelationship(naf);
		contrat.setUtilisateurModifRelationship(utilisateur);
		contrat.setConDateModif(dateModification);
	}
	
	
	/**
	 * Suppression d'un contrat. Il n'est pas supprime mais archive. 
	 * Les avenants associes (zero et autres) ne sont pas supprimes.
	 * @param contrat Contrat a supprimer.
	 * @throws ExceptionArgument 
	 */
	public void supprimerContrat(
			final Contrat contrat) throws ExceptionUtilisateur {
		_supprimerContrat(contrat, false);
	}

	/**
	 * Suppression d'un contrat par un ADMIN. Il n'est pas supprime mais archive. 
	 * Les avenants associes (zero et autres) ne sont pas supprimes.
	 * Un contrat validé peut être supprimée.
	 * @param contrat Contrat a supprimer.
	 * @throws ExceptionArgument 
	 */
	public void supprimerContratAdmin(
			final Contrat contrat) throws ExceptionUtilisateur {
		_supprimerContrat(contrat, true);
	}

	/**
	 * Suppression d'un contrat. Il n'est pas supprime mais archive. Les avenants associes (zero et autres) ne sont pas
	 * supprimes.
	 * 
	 * @param contrat
	 *            Contrat a supprimer.
	 * @param isAdmin
	 *            <code>true</code> si l'utilisateur est un admin
	 * @throws ExceptionArgument
	 */
	private void _supprimerContrat(final Contrat contrat, final boolean isAdmin) throws ExceptionUtilisateur {
		trace("supprimerContrat() :: isAdmin=", new Boolean(isAdmin));
		trace(contrat);

		// Verifier que la convention n'est pas validee
		if (!isAdmin && contrat.conDateValidAdm() != null) {
			throw new ExceptionUtilisateur("La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n"
					+ "Il n'est donc plus possible de la supprimer.");
		}
		// Vérifier qu'il n'y a pas de crédits positionnées
		for (Tranche uneTranche : contrat.tranches(Tranche.QUALIFIER_NON_SUPPR)) {
			if ((uneTranche.trancheBudgetsNonSupprimes() != null && !uneTranche.trancheBudgetsNonSupprimes().isEmpty())
					|| (uneTranche.trancheBudgetsRecNonSupprimes() != null && !uneTranche.trancheBudgetsRecNonSupprimes().isEmpty())) {
				throw new ExceptionUtilisateur("Suppression impossible !\nLa convention s\u00E9lectionn\u00E9e a des cr\u00E9dits positionn\u00E9s en d\u00E9pense ou recette.\n"
						+ "Vous devez les supprimer avant de pouvoir supprimer la convention.");
			}
		}
		contrat.setConSuppr("O");
	}

	/**
	 * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais archives.
	 * @param contrat Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @throws ExceptionArgument 
	 */
	public void supprimerAvenants(
			final Contrat contrat,
			final boolean avenantZeroAussi) throws ExceptionUtilisateur {
		_supprimerAvenant(contrat, avenantZeroAussi, false);
	}
	
	/**
	 * Suppression des avenants d'un contrat par un ADMIN. Il ne sont pas supprimes mais archives.
	 * @param contrat Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @throws ExceptionArgument 
	 */
	public void supprimerAvenantsAdmin(
			final Contrat contrat,
			final boolean avenantZeroAussi) throws ExceptionUtilisateur {
		_supprimerAvenant(contrat, avenantZeroAussi, true);
	}

	/**
	 * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais archives.
	 * 
	 * @param contrat
	 *            Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi
	 *            Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @param isAdmin
	 *            <code>true</code> si l'utilisateur est un admin
	 * @throws ExceptionUtilisateur
	 */
	private void _supprimerAvenant(final Contrat contrat, final boolean avenantZeroAussi, final boolean isAdmin) throws ExceptionUtilisateur {
		trace("supprimerAvenants() :: isAdmin=", new Boolean(isAdmin));
		trace(contrat);
		trace(new Boolean(avenantZeroAussi));

		// Verifier que la convention n'est pas validee
		if (!isAdmin && contrat.conDateValidAdm() != null) {
			throw new ExceptionUtilisateur("La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n"
					+ "Il n'est donc plus possible de la supprimer.");
		}

		for (int i = 0; i < contrat.avenantsDontInitialNonSupprimes().count(); i++) {
			Avenant avenant = (Avenant) contrat.avenantsDontInitialNonSupprimes().objectAtIndex(i);
			if (avenantZeroAussi || avenant.avtIndex().intValue() != 0) {
				this.avenantFactory.supprimerAvenant(avenant);
			}
		}
	}
	
	public static boolean isValidContratCreation(Contrat contrat) {
		
		if (contrat == null 
			|| contrat.etablissement() == null 
			|| contrat.centreResponsabilite() == null 
			|| contrat.typeContrat() == null 
			|| contrat.typeReconduction() == null 
			|| contrat.conObjet() == null 
			|| contrat.conObjet().length() > Contrat.OBJET_LONGUEUR_MAX
		) return false;
		
		return true;
	}
	
	public void activerIndicateur(final Contrat contrat, final IndicateursContrat indicateur) {
		if (!contrat.indicateursContrat().contains(indicateur)) {
			try {
				RepartIndicateursContrat ric = RepartIndicateursContrat.instanciate(ec);
				ric.setContratRelationship(contrat);
				ric.setIndicateursContratRelationship(indicateur);
				ec.insertObject(ric);
				contrat.addToRepartIndicateursContratRelationship(ric);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public void desactiverIndicateur(final Contrat contrat, final IndicateursContrat indicateur) {
		NSArray rics = contrat.repartIndicateursContrat();
		NSArray indicateurs = (NSArray)rics.valueForKeyPath("IndicateursContrat");
		if (indicateurs != null && indicateurs.contains(indicateur)) {
			EOKeyValueQualifier qual = new EOKeyValueQualifier("IndicateursContrat",EOKeyValueQualifier.QualifierOperatorEqual,indicateur);
			RepartIndicateursContrat ric = (RepartIndicateursContrat)EOQualifier.filteredArrayWithQualifier(rics, qual).lastObject();
			if (ric != null) {
				contrat.removeFromRepartIndicateursContratRelationship(ric);
				ec.deleteObject(ric);
			}
		}
	}
}
