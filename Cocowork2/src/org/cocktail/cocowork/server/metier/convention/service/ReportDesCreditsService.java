package org.cocktail.cocowork.server.metier.convention.service;

import java.text.SimpleDateFormat;

import org.cocktail.cocowork.common.tools.Constantes;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;

public class ReportDesCreditsService {

	public static final Integer STATUT_REPORT_OUI = 0;
	public static final Integer STATUT_REPORT_OUI_MAIS = 1;
	public static final Integer STATUT_REPORT_NON = 2;
	
	private EOEditingContext edc;

	private ReportDesCreditsService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOEditingContext edc() {
		return edc;
	}

	public static ReportDesCreditsService creerNouvelleInstance(EOEditingContext edc) {
		return new ReportDesCreditsService(edc);
	}
	

	public Boolean gestionTrancheAuto() {
		return ERXProperties.booleanForKeyWithDefault(
				Constantes.GESTION_TRANCHE_AUTO_PARAM, true);
	}
	
	public Integer anneeFinDeConvention(Contrat convention) {
		if(convention.dateFin() != null) {
			return Integer.valueOf(new SimpleDateFormat("yyyy").format(convention.dateFin()));
		} else {
			return null;
		}
	}
	
	public Integer anneeDebutDeConvention(Contrat convention) {
		if(convention.dateDebut() != null) {
			return Integer.valueOf(new SimpleDateFormat("yyyy").format(convention.dateDebut()));
		} else {
			return null;
		}
	}
	
	public Tranche trancheSuivantePourConventionEtExercice(Contrat convention, Integer exercice) {
		if(convention != null) {
			return ERXArrayUtilities.firstObject(convention.tranches(Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice), true));
		} else {
			return null;
		}
	}
	
	public Integer creditsReportablesPourConventionSurExercice(Contrat convention, EOExercice exercice) {
		if(anneeFinDeConvention(convention) >= exercice.exeExercice().intValue()) {
			if(trancheSuivantePourConventionEtExercice(convention, exercice.exeExercice().intValue()) == null) {
				if(gestionTrancheAuto() == false) {
					return STATUT_REPORT_OUI_MAIS;
				} else {
					return STATUT_REPORT_NON;
				}
			} else {
				return STATUT_REPORT_OUI;
			}
		} 
		return STATUT_REPORT_NON;
	}

    public EOExercice exerciceSourcePourLeReport() {
    	EOExercice exerciceSourcePourLeReport = null;

    	EOExercice exerciceCourant = EOExercice.fetchFirstByQualifier(edc(), ERXQ.equals(EOExercice.EXE_EXERCICE_KEY, DateCtrl.getCurrentYear()));
    	EOExercice exercicePrecedent = EOExercice.fetchFirstByQualifier(edc(), ERXQ.equals(EOExercice.EXE_EXERCICE_KEY, DateCtrl.getCurrentYear()-1));
    	EOExercice exerciceSuivant = EOExercice.fetchFirstByQualifier(edc(), ERXQ.equals(EOExercice.EXE_EXERCICE_KEY, DateCtrl.getCurrentYear()+1));
    	
    	if(exerciceCourant != null && exerciceSuivant != null /*&& exerciceCourant.estOuvert() && exerciceSuivant.estPreparation()*/) {
    		exerciceSourcePourLeReport = exerciceCourant;
    	}
    	
    	else if(exerciceCourant != null && exercicePrecedent != null && exercicePrecedent.estOuvert() && exerciceCourant.estPreparation()) {
    		exerciceSourcePourLeReport = exercicePrecedent;
    	}
    	
    	else if(exerciceCourant != null && exerciceSuivant != null && exerciceCourant.estRestreint() && exerciceSuivant.estOuvert()) {
    		exerciceSourcePourLeReport = exerciceCourant;
    	}
    	
    	else if(exerciceCourant != null && exercicePrecedent != null && exercicePrecedent.estRestreint() && exerciceCourant.estOuvert()) {
    		exerciceSourcePourLeReport = exercicePrecedent;
    	}
    	
    	else if(exerciceCourant != null && exerciceSuivant != null && exerciceCourant.estClos() && exerciceSuivant.estOuvert()) {
    		exerciceSourcePourLeReport = exerciceCourant;
    	}
    	
    	else if(exerciceCourant != null && exercicePrecedent != null && exercicePrecedent.estClos() && exerciceCourant.estOuvert()) {
    		exerciceSourcePourLeReport = exercicePrecedent;
    	}
    	
    	return exerciceSourcePourLeReport;
    	
    }
    
    public EOExercice exerciceDestinationPourLeReport() {
        EOExercice exerciceDestinationPourLeReport = null;
        
        exerciceDestinationPourLeReport = EOExercice.fetchFirstByQualifier(
        		edc(),
        		ERXQ.equals(EOExercice.EXE_EXERCICE_KEY, exerciceSourcePourLeReport().exeExercice()+1)
        	);
        
        return exerciceDestinationPourLeReport;
      } 


}
