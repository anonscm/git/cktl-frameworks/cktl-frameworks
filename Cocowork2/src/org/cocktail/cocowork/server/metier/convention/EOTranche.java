// DO NOT EDIT.  Make changes to Tranche.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTranche extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTranche";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> REPORT_NMOINS1 = new ERXKey<java.math.BigDecimal>("reportNmoins1");
  public static final ERXKey<java.math.BigDecimal> REPORT_NPLUS1 = new ERXKey<java.math.BigDecimal>("reportNplus1");
  public static final ERXKey<NSTimestamp> TRA_DATE_CREATION = new ERXKey<NSTimestamp>("traDateCreation");
  public static final ERXKey<NSTimestamp> TRA_DATE_MODIF = new ERXKey<NSTimestamp>("traDateModif");
  public static final ERXKey<NSTimestamp> TRA_DATE_VALID = new ERXKey<NSTimestamp>("traDateValid");
  public static final ERXKey<java.math.BigDecimal> TRA_MONTANT = new ERXKey<java.math.BigDecimal>("traMontant");
  public static final ERXKey<java.math.BigDecimal> TRA_MONTANT_INIT = new ERXKey<java.math.BigDecimal>("traMontantInit");
  public static final ERXKey<String> TRA_NATURE_MONTANT = new ERXKey<String>("traNatureMontant");
  public static final ERXKey<String> TRA_SUPPR = new ERXKey<String>("traSuppr");
  public static final ERXKey<String> TRA_VALIDE = new ERXKey<String>("traValide");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> HISTO_CREDIT_POSITIONNES = new ERXKey<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne>("histoCreditPositionnes");
  public static final ERXKey<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> PREVISIONS_BUDGET = new ERXKey<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget>("previsionsBudget");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.SbDepense> SB_DEPENSES = new ERXKey<org.cocktail.cocowork.server.metier.convention.SbDepense>("sbDepenses");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.SbRecette> SB_RECETTES = new ERXKey<org.cocktail.cocowork.server.metier.convention.SbRecette>("sbRecettes");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> TO_REPART_PARTENAIRE_TRANCHES = new ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>("toRepartPartenaireTranches");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> TRANCHE_BUDGET_RECS = new ERXKey<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec>("trancheBudgetRecs");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TrancheBudget> TRANCHE_BUDGETS = new ERXKey<org.cocktail.cocowork.server.metier.convention.TrancheBudget>("trancheBudgets");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TranchePrevision> TRANCHE_PREVISIONS = new ERXKey<org.cocktail.cocowork.server.metier.convention.TranchePrevision>("tranchePrevisions");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_MODIF = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurModif");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_VALID = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurValid");

  // Attributes
  public static final String REPORT_NMOINS1_KEY = REPORT_NMOINS1.key();
  public static final String REPORT_NPLUS1_KEY = REPORT_NPLUS1.key();
  public static final String TRA_DATE_CREATION_KEY = TRA_DATE_CREATION.key();
  public static final String TRA_DATE_MODIF_KEY = TRA_DATE_MODIF.key();
  public static final String TRA_DATE_VALID_KEY = TRA_DATE_VALID.key();
  public static final String TRA_MONTANT_KEY = TRA_MONTANT.key();
  public static final String TRA_MONTANT_INIT_KEY = TRA_MONTANT_INIT.key();
  public static final String TRA_NATURE_MONTANT_KEY = TRA_NATURE_MONTANT.key();
  public static final String TRA_SUPPR_KEY = TRA_SUPPR.key();
  public static final String TRA_VALIDE_KEY = TRA_VALIDE.key();
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String HISTO_CREDIT_POSITIONNES_KEY = HISTO_CREDIT_POSITIONNES.key();
  public static final String PREVISIONS_BUDGET_KEY = PREVISIONS_BUDGET.key();
  public static final String SB_DEPENSES_KEY = SB_DEPENSES.key();
  public static final String SB_RECETTES_KEY = SB_RECETTES.key();
  public static final String TO_REPART_PARTENAIRE_TRANCHES_KEY = TO_REPART_PARTENAIRE_TRANCHES.key();
  public static final String TRANCHE_BUDGET_RECS_KEY = TRANCHE_BUDGET_RECS.key();
  public static final String TRANCHE_BUDGETS_KEY = TRANCHE_BUDGETS.key();
  public static final String TRANCHE_PREVISIONS_KEY = TRANCHE_PREVISIONS.key();
  public static final String UTILISATEUR_CREATION_KEY = UTILISATEUR_CREATION.key();
  public static final String UTILISATEUR_MODIF_KEY = UTILISATEUR_MODIF.key();
  public static final String UTILISATEUR_VALID_KEY = UTILISATEUR_VALID.key();

  private static Logger LOG = Logger.getLogger(EOTranche.class);

  public Tranche localInstanceIn(EOEditingContext editingContext) {
    Tranche localInstance = (Tranche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal reportNmoins1() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.REPORT_NMOINS1_KEY);
  }

  public void setReportNmoins1(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating reportNmoins1 from " + reportNmoins1() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.REPORT_NMOINS1_KEY);
  }

  public java.math.BigDecimal reportNplus1() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.REPORT_NPLUS1_KEY);
  }

  public void setReportNplus1(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating reportNplus1 from " + reportNplus1() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.REPORT_NPLUS1_KEY);
  }

  public NSTimestamp traDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_CREATION_KEY);
  }

  public void setTraDateCreation(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateCreation from " + traDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_CREATION_KEY);
  }

  public NSTimestamp traDateModif() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_MODIF_KEY);
  }

  public void setTraDateModif(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateModif from " + traDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_MODIF_KEY);
  }

  public NSTimestamp traDateValid() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_VALID_KEY);
  }

  public void setTraDateValid(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateValid from " + traDateValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_VALID_KEY);
  }

  public java.math.BigDecimal traMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.TRA_MONTANT_KEY);
  }

  public void setTraMontant(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traMontant from " + traMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_MONTANT_KEY);
  }

  public java.math.BigDecimal traMontantInit() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.TRA_MONTANT_INIT_KEY);
  }

  public void setTraMontantInit(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traMontantInit from " + traMontantInit() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_MONTANT_INIT_KEY);
  }

  public String traNatureMontant() {
    return (String) storedValueForKey(EOTranche.TRA_NATURE_MONTANT_KEY);
  }

  public void setTraNatureMontant(String value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traNatureMontant from " + traNatureMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_NATURE_MONTANT_KEY);
  }

  public String traSuppr() {
    return (String) storedValueForKey(EOTranche.TRA_SUPPR_KEY);
  }

  public void setTraSuppr(String value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traSuppr from " + traSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_SUPPR_KEY);
  }

  public String traValide() {
    return (String) storedValueForKey(EOTranche.TRA_VALIDE_KEY);
  }

  public void setTraValide(String value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traValide from " + traValide() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_VALIDE_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EOTranche.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EOTranche.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail)storedValueForKey(EOTranche.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOTranche.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOTranche.UTILISATEUR_CREATION_KEY);
  }
  
  public void setUtilisateurCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOTranche.UTILISATEUR_CREATION_KEY);
  }

  public void setUtilisateurCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating utilisateurCreation from " + utilisateurCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurCreation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurCreation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.UTILISATEUR_CREATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.UTILISATEUR_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurModif() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOTranche.UTILISATEUR_MODIF_KEY);
  }
  
  public void setUtilisateurModif(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOTranche.UTILISATEUR_MODIF_KEY);
  }

  public void setUtilisateurModifRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating utilisateurModif from " + utilisateurModif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurModif(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurModif();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.UTILISATEUR_MODIF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.UTILISATEUR_MODIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurValid() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOTranche.UTILISATEUR_VALID_KEY);
  }
  
  public void setUtilisateurValid(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOTranche.UTILISATEUR_VALID_KEY);
  }

  public void setUtilisateurValidRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating utilisateurValid from " + utilisateurValid() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurValid(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurValid();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.UTILISATEUR_VALID_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.UTILISATEUR_VALID_KEY);
    }
  }
  
  public NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> histoCreditPositionnes() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne>)storedValueForKey(EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier) {
    return histoCreditPositionnes(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier, boolean fetch) {
    return histoCreditPositionnes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = histoCreditPositionnes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToHistoCreditPositionnes(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public void removeFromHistoCreditPositionnes(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public void addToHistoCreditPositionnesRelationship(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to histoCreditPositionnes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToHistoCreditPositionnes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    }
  }

  public void removeFromHistoCreditPositionnesRelationship(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from histoCreditPositionnes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromHistoCreditPositionnes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne createHistoCreditPositionnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne) eo;
  }

  public void deleteHistoCreditPositionnesRelationship(org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllHistoCreditPositionnesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne> objects = histoCreditPositionnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteHistoCreditPositionnesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> previsionsBudget() {
    return (NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget>)storedValueForKey(EOTranche.PREVISIONS_BUDGET_KEY);
  }

  public NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> previsionsBudget(EOQualifier qualifier) {
    return previsionsBudget(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> previsionsBudget(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> results;
      results = previsionsBudget();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPrevisionsBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.PREVISIONS_BUDGET_KEY);
  }

  public void removeFromPrevisionsBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.PREVISIONS_BUDGET_KEY);
  }

  public void addToPrevisionsBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to previsionsBudget relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToPrevisionsBudget(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.PREVISIONS_BUDGET_KEY);
    }
  }

  public void removeFromPrevisionsBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from previsionsBudget relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromPrevisionsBudget(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.PREVISIONS_BUDGET_KEY);
    }
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget createPrevisionsBudgetRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.PREVISIONS_BUDGET_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget) eo;
  }

  public void deletePrevisionsBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.PREVISIONS_BUDGET_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrevisionsBudgetRelationships() {
    Enumeration<org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget> objects = previsionsBudget().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrevisionsBudgetRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense> sbDepenses() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense>)storedValueForKey(EOTranche.SB_DEPENSES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense> sbDepenses(EOQualifier qualifier) {
    return sbDepenses(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense> sbDepenses(EOQualifier qualifier, boolean fetch) {
    return sbDepenses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense> sbDepenses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.SbDepense.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.SbDepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sbDepenses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.SbDepense>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSbDepenses(org.cocktail.cocowork.server.metier.convention.SbDepense object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.SB_DEPENSES_KEY);
  }

  public void removeFromSbDepenses(org.cocktail.cocowork.server.metier.convention.SbDepense object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.SB_DEPENSES_KEY);
  }

  public void addToSbDepensesRelationship(org.cocktail.cocowork.server.metier.convention.SbDepense object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToSbDepenses(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    }
  }

  public void removeFromSbDepensesRelationship(org.cocktail.cocowork.server.metier.convention.SbDepense object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromSbDepenses(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.SbDepense createSbDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.SbDepense.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.SB_DEPENSES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.SbDepense) eo;
  }

  public void deleteSbDepensesRelationship(org.cocktail.cocowork.server.metier.convention.SbDepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSbDepensesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.SbDepense> objects = sbDepenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSbDepensesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette> sbRecettes() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette>)storedValueForKey(EOTranche.SB_RECETTES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette> sbRecettes(EOQualifier qualifier) {
    return sbRecettes(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette> sbRecettes(EOQualifier qualifier, boolean fetch) {
    return sbRecettes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette> sbRecettes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.SbRecette.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.SbRecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sbRecettes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.SbRecette>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSbRecettes(org.cocktail.cocowork.server.metier.convention.SbRecette object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.SB_RECETTES_KEY);
  }

  public void removeFromSbRecettes(org.cocktail.cocowork.server.metier.convention.SbRecette object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.SB_RECETTES_KEY);
  }

  public void addToSbRecettesRelationship(org.cocktail.cocowork.server.metier.convention.SbRecette object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to sbRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToSbRecettes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    }
  }

  public void removeFromSbRecettesRelationship(org.cocktail.cocowork.server.metier.convention.SbRecette object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from sbRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromSbRecettes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.SbRecette createSbRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.SbRecette.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.SB_RECETTES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.SbRecette) eo;
  }

  public void deleteSbRecettesRelationship(org.cocktail.cocowork.server.metier.convention.SbRecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSbRecettesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.SbRecette> objects = sbRecettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSbRecettesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> toRepartPartenaireTranches() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)storedValueForKey(EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier) {
    return toRepartPartenaireTranches(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier, boolean fetch) {
    return toRepartPartenaireTranches(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartPartenaireTranches();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartPartenaireTranches(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public void removeFromToRepartPartenaireTranches(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public void addToToRepartPartenaireTranchesRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to toRepartPartenaireTranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToToRepartPartenaireTranches(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    }
  }

  public void removeFromToRepartPartenaireTranchesRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from toRepartPartenaireTranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromToRepartPartenaireTranches(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche createToRepartPartenaireTranchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche) eo;
  }

  public void deleteToRepartPartenaireTranchesRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartPartenaireTranchesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> objects = toRepartPartenaireTranches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartPartenaireTranchesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> trancheBudgetRecs() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec>)storedValueForKey(EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier) {
    return trancheBudgetRecs(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier, boolean fetch) {
    return trancheBudgetRecs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = trancheBudgetRecs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTrancheBudgetRecs(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public void removeFromTrancheBudgetRecs(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public void addToTrancheBudgetRecsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to trancheBudgetRecs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTrancheBudgetRecs(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    }
  }

  public void removeFromTrancheBudgetRecsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from trancheBudgetRecs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTrancheBudgetRecs(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec createTrancheBudgetRecsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec) eo;
  }

  public void deleteTrancheBudgetRecsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTrancheBudgetRecsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.TrancheBudgetRec> objects = trancheBudgetRecs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTrancheBudgetRecsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget> trancheBudgets() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget>)storedValueForKey(EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget> trancheBudgets(EOQualifier qualifier) {
    return trancheBudgets(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget> trancheBudgets(EOQualifier qualifier, boolean fetch) {
    return trancheBudgets(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget> trancheBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.TrancheBudget.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.TrancheBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = trancheBudgets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TrancheBudget>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTrancheBudgets(org.cocktail.cocowork.server.metier.convention.TrancheBudget object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public void removeFromTrancheBudgets(org.cocktail.cocowork.server.metier.convention.TrancheBudget object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public void addToTrancheBudgetsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudget object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to trancheBudgets relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTrancheBudgets(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    }
  }

  public void removeFromTrancheBudgetsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudget object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from trancheBudgets relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTrancheBudgets(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.TrancheBudget createTrancheBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.TrancheBudget.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_BUDGETS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.TrancheBudget) eo;
  }

  public void deleteTrancheBudgetsRelationship(org.cocktail.cocowork.server.metier.convention.TrancheBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTrancheBudgetsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.TrancheBudget> objects = trancheBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTrancheBudgetsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision> tranchePrevisions() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision>)storedValueForKey(EOTranche.TRANCHE_PREVISIONS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision> tranchePrevisions(EOQualifier qualifier) {
    return tranchePrevisions(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision> tranchePrevisions(EOQualifier qualifier, boolean fetch) {
    return tranchePrevisions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision> tranchePrevisions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.TranchePrevision.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.TranchePrevision.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tranchePrevisions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.TranchePrevision>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTranchePrevisions(org.cocktail.cocowork.server.metier.convention.TranchePrevision object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_PREVISIONS_KEY);
  }

  public void removeFromTranchePrevisions(org.cocktail.cocowork.server.metier.convention.TranchePrevision object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_PREVISIONS_KEY);
  }

  public void addToTranchePrevisionsRelationship(org.cocktail.cocowork.server.metier.convention.TranchePrevision object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to tranchePrevisions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTranchePrevisions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_PREVISIONS_KEY);
    }
  }

  public void removeFromTranchePrevisionsRelationship(org.cocktail.cocowork.server.metier.convention.TranchePrevision object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from tranchePrevisions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTranchePrevisions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_PREVISIONS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.TranchePrevision createTranchePrevisionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.TranchePrevision.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_PREVISIONS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.TranchePrevision) eo;
  }

  public void deleteTranchePrevisionsRelationship(org.cocktail.cocowork.server.metier.convention.TranchePrevision object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_PREVISIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTranchePrevisionsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.TranchePrevision> objects = tranchePrevisions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTranchePrevisionsRelationship(objects.nextElement());
    }
  }


  public static Tranche create(EOEditingContext editingContext, java.math.BigDecimal reportNmoins1
, java.math.BigDecimal reportNplus1
, NSTimestamp traDateCreation
, String traValide
, org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail exerciceCocktail, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation) {
    Tranche eo = (Tranche) EOUtilities.createAndInsertInstance(editingContext, EOTranche.ENTITY_NAME);    
        eo.setReportNmoins1(reportNmoins1);
        eo.setReportNplus1(reportNplus1);
        eo.setTraDateCreation(traDateCreation);
        eo.setTraValide(traValide);
    eo.setExerciceCocktailRelationship(exerciceCocktail);
    eo.setUtilisateurCreationRelationship(utilisateurCreation);
    return eo;
  }

  public static ERXFetchSpecification<Tranche> fetchSpec() {
    return new ERXFetchSpecification<Tranche>(EOTranche.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext) {
    return EOTranche.fetchAll(editingContext, null);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTranche.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Tranche> fetchSpec = new ERXFetchSpecification<Tranche>(EOTranche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Tranche> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Tranche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Tranche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Tranche> eoObjects = EOTranche.fetchAll(editingContext, qualifier, null);
    Tranche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Tranche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Tranche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Tranche eoObject = EOTranche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Tranche localInstanceIn(EOEditingContext editingContext, Tranche eo) {
    Tranche localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}