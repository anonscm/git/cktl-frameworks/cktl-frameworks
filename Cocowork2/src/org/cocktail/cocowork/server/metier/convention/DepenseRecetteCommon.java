package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;

import com.webobjects.foundation.NSValidation.ValidationException;

public class DepenseRecetteCommon {

    public static void verificationExercice(EOExerciceCocktail exerciceSelectionne, EOExercice exericeOuvertCourant) {
        if (exerciceSelectionne.exercice()==null || 
                        !exerciceSelectionne.exercice().estPreparation() && 
                        !exerciceSelectionne.exercice().estOuvert() && 
                        exerciceSelectionne.exeExercice() > exericeOuvertCourant.exeExercice()) { 
            throw new ValidationException(
                            "Le positionnement de cr\u00E9dit n'est possible que sur une tranche correspondant \n" + 
                            "\u00E0 un exercice comptable 'ouvert', en 'pr\u00E9paration' ou 'clos'."); 
        }
        if (exerciceSelectionne.exeExercice().intValue() < 2007) {
            throw new ValidationException(
                            "Cette op\u00E9ration n'est possible que sur une tranche correspondant \n" +
                            "\u00E0 un exercice post\u00E9rieur ou \u00E9gal \u00E0 2007.");
        }
    }
    
    
}
