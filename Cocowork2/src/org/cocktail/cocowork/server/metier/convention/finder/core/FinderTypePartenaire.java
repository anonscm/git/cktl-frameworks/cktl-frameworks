/**
 * Cocowork
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.server.metier.convention.finder.core;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.cocowork.server.metier.convention.TypePartenaire;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderTypePartenaire extends Finder 
{
	protected EOQualifier typePartIdInterneQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context de travail.
	 */
	public FinderTypePartenaire(EOEditingContext ec) {
		super(ec, TypePartenaire.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du critere.
	 * @param typePartIdInterne Id interne du type de contrat.
	 */
	public void setTypePartIdInterne(final String typePartIdInterne) {
		this.typePartIdInterneQualifier = createQualifier(
				"typePartIdInterne = %@", 
				typePartIdInterne);
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#clearAllCriteria()
	 */
	public void clearAllCriteria() {
		this.typePartIdInterneQualifier = null;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray find() throws ExceptionFinder {
		clearAllCriteria();
		addOptionalQualifier(this.typePartIdInterneQualifier);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray findWithIdInterne(final String idInterne) throws ExceptionFinder {
		removeOptionalQualifiers();
		setTypePartIdInterne(idInterne);
		addOptionalQualifier(typePartIdInterneQualifier);
		
		return super.find();
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
