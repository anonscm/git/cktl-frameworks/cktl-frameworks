// DO NOT EDIT.  Make changes to TypeAvenant.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeAvenant extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeAvenant";

  // Attribute Keys
  public static final ERXKey<String> TA_CODE = new ERXKey<String>("taCode");
  public static final ERXKey<String> TA_COMMENTAIRE = new ERXKey<String>("taCommentaire");
  public static final ERXKey<String> TA_LIBELLE = new ERXKey<String>("taLibelle");
  // Relationship Keys

  // Attributes
  public static final String TA_CODE_KEY = TA_CODE.key();
  public static final String TA_COMMENTAIRE_KEY = TA_COMMENTAIRE.key();
  public static final String TA_LIBELLE_KEY = TA_LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeAvenant.class);

  public TypeAvenant localInstanceIn(EOEditingContext editingContext) {
    TypeAvenant localInstance = (TypeAvenant)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String taCode() {
    return (String) storedValueForKey(EOTypeAvenant.TA_CODE_KEY);
  }

  public void setTaCode(String value) {
    if (EOTypeAvenant.LOG.isDebugEnabled()) {
        EOTypeAvenant.LOG.debug( "updating taCode from " + taCode() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeAvenant.TA_CODE_KEY);
  }

  public String taCommentaire() {
    return (String) storedValueForKey(EOTypeAvenant.TA_COMMENTAIRE_KEY);
  }

  public void setTaCommentaire(String value) {
    if (EOTypeAvenant.LOG.isDebugEnabled()) {
        EOTypeAvenant.LOG.debug( "updating taCommentaire from " + taCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeAvenant.TA_COMMENTAIRE_KEY);
  }

  public String taLibelle() {
    return (String) storedValueForKey(EOTypeAvenant.TA_LIBELLE_KEY);
  }

  public void setTaLibelle(String value) {
    if (EOTypeAvenant.LOG.isDebugEnabled()) {
        EOTypeAvenant.LOG.debug( "updating taLibelle from " + taLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeAvenant.TA_LIBELLE_KEY);
  }


  public static TypeAvenant create(EOEditingContext editingContext, String taCode
, String taLibelle
) {
    TypeAvenant eo = (TypeAvenant) EOUtilities.createAndInsertInstance(editingContext, EOTypeAvenant.ENTITY_NAME);    
        eo.setTaCode(taCode);
        eo.setTaLibelle(taLibelle);
    return eo;
  }

  public static ERXFetchSpecification<TypeAvenant> fetchSpec() {
    return new ERXFetchSpecification<TypeAvenant>(EOTypeAvenant.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeAvenant> fetchAll(EOEditingContext editingContext) {
    return EOTypeAvenant.fetchAll(editingContext, null);
  }

  public static NSArray<TypeAvenant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeAvenant.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeAvenant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeAvenant> fetchSpec = new ERXFetchSpecification<TypeAvenant>(EOTypeAvenant.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeAvenant> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeAvenant fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeAvenant.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeAvenant fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeAvenant> eoObjects = EOTypeAvenant.fetchAll(editingContext, qualifier, null);
    TypeAvenant eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeAvenant fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeAvenant.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeAvenant fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeAvenant eoObject = EOTypeAvenant.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeAvenant that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeAvenant localInstanceIn(EOEditingContext editingContext, TypeAvenant eo) {
    TypeAvenant localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}