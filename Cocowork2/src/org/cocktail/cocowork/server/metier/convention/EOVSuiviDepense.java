// DO NOT EDIT.  Make changes to VSuiviDepense.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVSuiviDepense extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWVSuiviDepense";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> DISPO_REEL = new ERXKey<java.math.BigDecimal>("dispoReel");
  public static final ERXKey<java.math.BigDecimal> DISPO_REEL_POSIT = new ERXKey<java.math.BigDecimal>("dispoReelPosit");
  public static final ERXKey<java.math.BigDecimal> DISPO_VIRTUEL = new ERXKey<java.math.BigDecimal>("dispoVirtuel");
  public static final ERXKey<java.math.BigDecimal> TOTAL_ENGAGE = new ERXKey<java.math.BigDecimal>("totalEngage");
  public static final ERXKey<java.math.BigDecimal> TOTAL_LIQUIDE = new ERXKey<java.math.BigDecimal>("totalLiquide");
  public static final ERXKey<java.math.BigDecimal> TOTAL_MANDATE = new ERXKey<java.math.BigDecimal>("totalMandate");
  public static final ERXKey<java.math.BigDecimal> TOTAL_POSITIONNE = new ERXKey<java.math.BigDecimal>("totalPositionne");
  public static final ERXKey<java.math.BigDecimal> TOTAL_PROPOSE = new ERXKey<java.math.BigDecimal>("totalPropose");
  public static final ERXKey<java.math.BigDecimal> TOTAL_RESTE_A_POSIT = new ERXKey<java.math.BigDecimal>("totalResteAPosit");
  public static final ERXKey<java.math.BigDecimal> TOTAL_REVERSE = new ERXKey<java.math.BigDecimal>("totalReverse");
  public static final ERXKey<Integer> TRA_ORDRE = new ERXKey<Integer>("traOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE_COMPTABLE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exerciceComptable");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable> PLANCO = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable>("planco");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");

  // Attributes
  public static final String DISPO_REEL_KEY = DISPO_REEL.key();
  public static final String DISPO_REEL_POSIT_KEY = DISPO_REEL_POSIT.key();
  public static final String DISPO_VIRTUEL_KEY = DISPO_VIRTUEL.key();
  public static final String TOTAL_ENGAGE_KEY = TOTAL_ENGAGE.key();
  public static final String TOTAL_LIQUIDE_KEY = TOTAL_LIQUIDE.key();
  public static final String TOTAL_MANDATE_KEY = TOTAL_MANDATE.key();
  public static final String TOTAL_POSITIONNE_KEY = TOTAL_POSITIONNE.key();
  public static final String TOTAL_PROPOSE_KEY = TOTAL_PROPOSE.key();
  public static final String TOTAL_RESTE_A_POSIT_KEY = TOTAL_RESTE_A_POSIT.key();
  public static final String TOTAL_REVERSE_KEY = TOTAL_REVERSE.key();
  public static final String TRA_ORDRE_KEY = TRA_ORDRE.key();
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String EXERCICE_COMPTABLE_KEY = EXERCICE_COMPTABLE.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOVSuiviDepense.class);

  public VSuiviDepense localInstanceIn(EOEditingContext editingContext) {
    VSuiviDepense localInstance = (VSuiviDepense)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal dispoReel() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.DISPO_REEL_KEY);
  }

  public void setDispoReel(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating dispoReel from " + dispoReel() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.DISPO_REEL_KEY);
  }

  public java.math.BigDecimal dispoReelPosit() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.DISPO_REEL_POSIT_KEY);
  }

  public void setDispoReelPosit(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating dispoReelPosit from " + dispoReelPosit() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.DISPO_REEL_POSIT_KEY);
  }

  public java.math.BigDecimal dispoVirtuel() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.DISPO_VIRTUEL_KEY);
  }

  public void setDispoVirtuel(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating dispoVirtuel from " + dispoVirtuel() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.DISPO_VIRTUEL_KEY);
  }

  public java.math.BigDecimal totalEngage() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_ENGAGE_KEY);
  }

  public void setTotalEngage(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalEngage from " + totalEngage() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_ENGAGE_KEY);
  }

  public java.math.BigDecimal totalLiquide() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_LIQUIDE_KEY);
  }

  public void setTotalLiquide(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalLiquide from " + totalLiquide() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_LIQUIDE_KEY);
  }

  public java.math.BigDecimal totalMandate() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_MANDATE_KEY);
  }

  public void setTotalMandate(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalMandate from " + totalMandate() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_MANDATE_KEY);
  }

  public java.math.BigDecimal totalPositionne() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_POSITIONNE_KEY);
  }

  public void setTotalPositionne(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalPositionne from " + totalPositionne() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_POSITIONNE_KEY);
  }

  public java.math.BigDecimal totalPropose() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_PROPOSE_KEY);
  }

  public void setTotalPropose(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalPropose from " + totalPropose() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_PROPOSE_KEY);
  }

  public java.math.BigDecimal totalResteAPosit() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_RESTE_A_POSIT_KEY);
  }

  public void setTotalResteAPosit(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalResteAPosit from " + totalResteAPosit() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_RESTE_A_POSIT_KEY);
  }

  public java.math.BigDecimal totalReverse() {
    return (java.math.BigDecimal) storedValueForKey(EOVSuiviDepense.TOTAL_REVERSE_KEY);
  }

  public void setTotalReverse(java.math.BigDecimal value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating totalReverse from " + totalReverse() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TOTAL_REVERSE_KEY);
  }

  public Integer traOrdre() {
    return (Integer) storedValueForKey(EOVSuiviDepense.TRA_ORDRE_KEY);
  }

  public void setTraOrdre(Integer value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
        EOVSuiviDepense.LOG.debug( "updating traOrdre from " + traOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOVSuiviDepense.TRA_ORDRE_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EOVSuiviDepense.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EOVSuiviDepense.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
      EOVSuiviDepense.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVSuiviDepense.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVSuiviDepense.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceComptable() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOVSuiviDepense.EXERCICE_COMPTABLE_KEY);
  }
  
  public void setExerciceComptable(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOVSuiviDepense.EXERCICE_COMPTABLE_KEY);
  }

  public void setExerciceComptableRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
      EOVSuiviDepense.LOG.debug("updating exerciceComptable from " + exerciceComptable() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceComptable(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exerciceComptable();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVSuiviDepense.EXERCICE_COMPTABLE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVSuiviDepense.EXERCICE_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable planco() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable)storedValueForKey(EOVSuiviDepense.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, EOVSuiviDepense.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
      EOVSuiviDepense.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVSuiviDepense.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVSuiviDepense.PLANCO_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOVSuiviDepense.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOVSuiviDepense.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOVSuiviDepense.LOG.isDebugEnabled()) {
      EOVSuiviDepense.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVSuiviDepense.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVSuiviDepense.TRANCHE_KEY);
    }
  }
  

  public static VSuiviDepense create(EOEditingContext editingContext, Integer traOrdre
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exerciceComptable) {
    VSuiviDepense eo = (VSuiviDepense) EOUtilities.createAndInsertInstance(editingContext, EOVSuiviDepense.ENTITY_NAME);    
        eo.setTraOrdre(traOrdre);
    eo.setExerciceComptableRelationship(exerciceComptable);
    return eo;
  }

  public static ERXFetchSpecification<VSuiviDepense> fetchSpec() {
    return new ERXFetchSpecification<VSuiviDepense>(EOVSuiviDepense.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VSuiviDepense> fetchAll(EOEditingContext editingContext) {
    return EOVSuiviDepense.fetchAll(editingContext, null);
  }

  public static NSArray<VSuiviDepense> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVSuiviDepense.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VSuiviDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VSuiviDepense> fetchSpec = new ERXFetchSpecification<VSuiviDepense>(EOVSuiviDepense.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VSuiviDepense> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VSuiviDepense fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVSuiviDepense.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VSuiviDepense fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VSuiviDepense> eoObjects = EOVSuiviDepense.fetchAll(editingContext, qualifier, null);
    VSuiviDepense eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWVSuiviDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VSuiviDepense fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVSuiviDepense.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VSuiviDepense fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VSuiviDepense eoObject = EOVSuiviDepense.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWVSuiviDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VSuiviDepense localInstanceIn(EOEditingContext editingContext, VSuiviDepense eo) {
    VSuiviDepense localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}