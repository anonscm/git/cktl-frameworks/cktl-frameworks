package org.cocktail.cocowork.server.metier.convention.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.cocowork.common.exception.ExceptionArgument;
import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionOperationImpossible;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.nombre.NombreOperation;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.cocowork.server.metier.convention.Projet;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.TypeAvenant;
import org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.convention.TypeReconduction;
import org.cocktail.cocowork.server.metier.convention.service.NotificationCenter;
import org.cocktail.cocowork.server.metier.convention.service.NotificationCenter.Notification;
import org.cocktail.cocowork.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTva;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;


/**
 * 
 * @author Michael Haller, Consortium Cocktail, 2008
 *
 */
public class FactoryConvention extends Factory 
{
	protected FactoryContrat contratFactory;
	protected FactoryAvenant avenantFactory;
	protected FactoryContratPartenaire contratPartenaireFactory;
	protected FactoryTranche trancheFactory;	
	protected NombreOperation nombreOperation;
	
	private static String TYPE_ASSOCIATION_CONVENTION = "Convention";
			
	/**
	 * 
	 * @param ec
	 * @param withlog
	 */
	public FactoryConvention(final EOEditingContext ec, final Boolean withlog) {
		super(ec, withlog);
		
		contratFactory = new FactoryContrat(ec, withlog);
		avenantFactory = new FactoryAvenant(ec, withlog);
		contratPartenaireFactory = new FactoryContratPartenaire(ec, withlog);
		trancheFactory = new FactoryTranche(ec, withlog);
		
		nombreOperation = new NombreOperation();
	}
	
	/**
	 * Creation d'un objet Contrat, l'avenant 0 (contrat initial) est aussi cree.
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param modeGestion Mode de gestion de la convention, ex: Ressource affectee (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'bjet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateDebut Date de debut de la convention (obligatoire)
	 * @param dateFin Date de fin de la convention (obligatoire)
	 * @param dateDebutExecution Date de debut d'execution de la convention
	 * @param dateFinExecution Date de fin d'execution de la convention
	 * @param dateSignature Date de signature en CA de la convention
	 * @param dateCloture Date de cloture de la convention
	 * @param montantGlobalHT Montant global HT du cotrat initial (obligatoire)
	 * @param tauxTva Taux de TVA pour le montant global.
	 * @param creditsLimitatifs Indique si le montant des credits positionnes est indepassable au moment d'une depense.
	 * @param lucratif Indicateur de lucrativite de la convention
	 * @param tvaRecuperable Indicateur de recuperation (collecte) de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @param creerPartenaireAuto si true, ajoute le centre gestionnaire au partenaire
	 * @return Le contrat correspondant a la convention creee. Ce contrat possede un avenant zero implicite.
	 * @throws Exception
	 */
	public Contrat creerConvention(
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final ModeGestion modeGestion,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateDebut,
			final NSTimestamp dateFin,
			final Integer duree,
			final NSTimestamp dateDebutExecution,
			final NSTimestamp dateFinExecution,
			final NSTimestamp dateSignature,
			final NSTimestamp dateCloture,
			final BigDecimal montantGlobalHT,
			final EOTva tauxTva,
			final Boolean creditsLimitatifs,
			final Boolean lucratif,
			final Boolean tvaRecuperable,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur,
			final Boolean creerPartenaireAuto)
	throws Exception, ExceptionUtilisateur {
		
		// contrat
		Contrat contrat = contratFactory.creerContrat(
				typeClassificationContrat,
				etablissementGestionnaire, 
				centreResponsabiliteGestionnaire, 
				typeContrat, 
				referenceExterne, 
				objet, 
				observations, 
				dateCloture,
				typeReconduction, 
				naf,
				createur);
		
		// avenant zero
		avenantFactory.creerAvenant( 
				contrat, 
				new Integer(0),
				TypeAvenant.TypeAvenantContratInitial(ec),
				modeGestion, 
				referenceExterne, 
				objet, 
				observations, 
				dateDebut, 
				dateFin,
				duree,
				dateDebutExecution,
				dateFinExecution,
				dateSignature,
				montantGlobalHT, 
				tauxTva, 
				creditsLimitatifs,
				lucratif, 
				tvaRecuperable,
				createur);
		
		if(creerPartenaireAuto.booleanValue()) {
//			System.out.println("FACTORYcentreResponsabiliteGestionnaire:"+centreResponsabiliteGestionnaire);
			ajouterPartenaire(contrat, (IPersonne) centreResponsabiliteGestionnaire, null, Boolean.FALSE, new BigDecimal(0));
		}
		return contrat;
	}
	
	/**
	 * Creation d'un objet Contrat, l'avenant 0 (contrat initial) est aussi cree.
	 * @param projet
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param modeGestion Mode de gestion de la convention, ex: Ressource affectee (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'bjet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateDebut Date de debut de la convention (obligatoire)
	 * @param dateFin Date de fin de la convention (obligatoire)
	 * @param dateDebutExecution Date de debut d'execution de la convention
	 * @param dateFinExecution Date de fin d'execution de la convention
	 * @param dateSignature Date de signature en CA de la convention
	 * @param dateCloture Date de cloture de la convention
	 * @param montantGlobalHT Montant global HT du cotrat initial (obligatoire)
	 * @param tauxTva Taux de TVA pour le montant global.
	 * @param creditsLimitatifs Indique si le montant des credits positionnes est indepassable au moment d'une depense.
	 * @param lucratif Indicateur de lucrativite de la convention
	 * @param tvaRecuperable Indicateur de recuperation (collecte) de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @param creerPartenaireAuto si true, ajoute le centre gestionnaire au partenaire
	 * @return Le contrat correspondant a la convention creee. Ce contrat possede un avenant zero implicite.
	 * @throws Exception
	 */
	public Contrat creerConvention(
			final Projet projet,
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final ModeGestion modeGestion,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateDebut,
			final NSTimestamp dateFin,
			final Integer duree,
			final NSTimestamp dateDebutExecution,
			final NSTimestamp dateFinExecution,
			final NSTimestamp dateSignature,
			final NSTimestamp dateCloture,
			final BigDecimal montantGlobalHT,
			final EOTva tauxTva,
			final Boolean creditsLimitatifs,
			final Boolean lucratif,
			final Boolean tvaRecuperable,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur,
			final Boolean creerPartenaireAuto)
	throws Exception, ExceptionUtilisateur {
		
		// contrat
		Contrat contrat = contratFactory.creerContrat(
				projet,
				typeClassificationContrat,
				etablissementGestionnaire, 
				centreResponsabiliteGestionnaire, 
				typeContrat, 
				referenceExterne, 
				objet, 
				observations, 
				dateCloture,
				typeReconduction, 
				naf,
				createur);
		
		// avenant zero
		avenantFactory.creerAvenant( 
				contrat, 
				new Integer(0),
				TypeAvenant.TypeAvenantContratInitial(ec),
				modeGestion, 
				referenceExterne, 
				objet, 
				observations, 
				dateDebut, 
				dateFin,
				duree,
				dateDebutExecution,
				dateFinExecution,
				dateSignature,
				montantGlobalHT, 
				tauxTva, 
				creditsLimitatifs,
				lucratif, 
				tvaRecuperable,
				createur);
		
		if(creerPartenaireAuto.booleanValue())
			ajouterPartenaire(contrat, (IPersonne) centreResponsabiliteGestionnaire, null, Boolean.FALSE, new BigDecimal(0));
		
		return contrat;
	}

	/**
	 * Creation d'une convention minimum, l'avenant 0 (contrat initial) est aussi cree
	 * ainsi que le partenaire principal a partir de l'etablissement de l'utilisateur
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat correspondant a la convention creee. Ce contrat possede un avenant zero implicite.
	 * @throws InstantiationException 
	 * @throws Exception 
	 */
	public Contrat creerConventionVierge(final EOUtilisateur createur) throws Exception, ExceptionUtilisateur {
		return creerConventionVierge(createur, null);
	}
	

	/**
	 * Creation d'une convention minimum, l'avenant 0 (contrat initial) est aussi cree
	 * ainsi que le partenaire principal et le service gestionnaire
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @param etablissementGestionnaire EOStructure service gestionnaire de la convention 
	 * @param serviceGestionnaire Utilisateur etablissement gestionnaire de la convention 
	 * @return Le contrat correspondant a la convention creee. Ce contrat possede un avenant zero implicite.
	 * @throws InstantiationException 
	 * @throws Exception 
	 */
	public Contrat creerConventionVierge(EOUtilisateur createur, EOStructure etablissementGestionnaire, EOStructure serviceGestionnaire) throws Exception, ExceptionUtilisateur {

		TypeClassificationContrat typeClassificationContrat = (TypeClassificationContrat)EOUtilities.objectMatchingKeyAndValue(ec, TypeClassificationContrat.ENTITY_NAME, TypeClassificationContrat.TCC_CODE_KEY, "CONV");
		Contrat contrat = contratFactory.creerContratVierge(typeClassificationContrat, createur);
		Avenant avenant=avenantFactory.creerAvenantVierge(
				contrat, 
				new Integer(0), 
				TypeAvenant.TypeAvenantContratInitial(ec), 
				createur);
		String groupeLibelle = "Partenaires de l'acte en cours de creation par "+createur.getLogin()+" le "+DateCtrl.now();
		EOStructure groupePere = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_PARTENARIAT");
		Integer persId = createur.toPersonne().persId();
		EOTypeGroupe typeGroupePartenariat = (EOTypeGroupe)EOUtilities.objectMatchingKeyAndValue(ec, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_PN);
		EOStructure groupe = EOStructureForGroupeSpec.creerGroupe(ec, persId, groupeLibelle, null, new NSArray(typeGroupePartenariat), groupePere);
		contrat.setGroupePartenaireRelationship(groupe);
		if (etablissementGestionnaire != null) {
			// Ajout en tant que partenaire principal de la structure passee en parametre
			ContratPartenaire partenaireInternePrincipal = ajouterPartenaire(contrat, etablissementGestionnaire, null, Boolean.TRUE, BigDecimal.valueOf(0));
			NSMutableDictionary values = new NSMutableDictionary();
			values.setObjectForKey("ETABINTPRINC", EOAssociation.ASS_CODE_KEY);
			EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
			EOAssociation associationEtabInternePrincipal = EOAssociation.fetchByQualifier(ec, qualifier);
			EOStructureForGroupeSpec.definitUnRole(ec, etablissementGestionnaire, associationEtabInternePrincipal, groupe, persId, null, null, null, null, null, true);
			contrat.setEtablissementRelationship(etablissementGestionnaire, createur.toPersonne().persId());
			if (serviceGestionnaire != null) {
				// Ajout du service gestionnaire en tant que contact de l'etablissement
				FactoryContratPartenaire fcp = new FactoryContratPartenaire(ec, Boolean.TRUE);
				values = new NSMutableDictionary();
				values.setObjectForKey("REPRGESTPART", EOAssociation.ASS_CODE_KEY);
				qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
				EOAssociation associationRepresentantGestionnairePartenariat = EOAssociation.fetchByQualifier(ec, qualifier);

				fcp.ajouterContact(partenaireInternePrincipal, serviceGestionnaire, new NSArray(associationRepresentantGestionnairePartenariat));

				// EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec, etablissementGestionnaire, groupe, persId, associationEtabInternePrincipal);
				contrat.setCentreResponsabiliteRelationship(serviceGestionnaire, createur.toPersonne().persId());
			}
		}
		return contrat;
	}
	/**
	 * Creation d'une convention minimum, l'avenant 0 (contrat initial) est aussi cree
	 * ainsi que le partenaire principal a partir de l'etablissement de l'utilisateur
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat correspondant a la convention creee. Ce contrat possede un avenant zero implicite.
	 * @throws InstantiationException 
	 * @throws Exception 
	 */
	public Contrat creerConventionVierge(EOUtilisateur createur, IPersonne etablissementPrincipal) throws Exception, ExceptionUtilisateur {
		//System.out.println("Creation 01");
		TypeClassificationContrat typeClassificationContrat = (TypeClassificationContrat)EOUtilities.objectMatchingKeyAndValue(ec, TypeClassificationContrat.ENTITY_NAME, TypeClassificationContrat.TCC_CODE_KEY, "CONV");
		Contrat contrat = contratFactory.creerContratVierge(typeClassificationContrat, createur);
		//System.out.println("Creation 02 "+contrat);
		Avenant avenant=avenantFactory.creerAvenantVierge(
				contrat, 
				new Integer(0), 
				TypeAvenant.TypeAvenantContratInitial(ec), 
				createur);
		// On teste que l'utilisateur est valide
		if(createur == null || createur.isValide() == false) {
			throw new Exception("Vous n'êtes pas un utlisateur valide (JEFY_ADMIN)");
		}
		String groupeLibelle = "Partenaires de l'acte en cours de creation par "+createur.getLogin()+" le "+DateCtrl.now();
		EOStructure groupePere = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_PARTENARIAT");
		Integer persId = createur.toPersonne().persId();
		EOTypeGroupe typeGroupePartenariat = (EOTypeGroupe)EOUtilities.objectMatchingKeyAndValue(ec, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_PN);
		EOStructure groupe = EOStructureForGroupeSpec.creerGroupe(ec, persId, groupeLibelle, null, new NSArray(typeGroupePartenariat), groupePere);
		contrat.setGroupePartenaireRelationship(groupe);
		if (etablissementPrincipal != null) {
			// Ajout en tant que partenaire principal de la structure passee en parametre
			ContratPartenaire partenaireInternePrincipal = ajouterPartenaire(contrat, etablissementPrincipal, null, Boolean.TRUE, BigDecimal.valueOf(0));
			NSMutableDictionary values = new NSMutableDictionary();
			values.setObjectForKey(EOTypeAssociation.TAS_CODE_CONV, EOAssociation.TO_TYPE_ASSOCIATION_KEY+"."+EOTypeAssociation.TAS_CODE_KEY);
			values.setObjectForKey("PARTENAIRES", EOAssociation.ASS_CODE_KEY);
			EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
			EOAssociation associationPartenaires = EOAssociation.fetchByQualifier(ec, qualifier);
			values.setObjectForKey("ETABINTPRINC", EOAssociation.ASS_CODE_KEY);
			EOAssociation associationEtabInternePrincipal = EOAssociation.fetchByQualifier(ec, qualifier);
            EOStructureForGroupeSpec.definitUnRole(ec, etablissementPrincipal, associationEtabInternePrincipal, groupe, persId, null, null, null, null, null, true);
			contrat.setEtablissementRelationship((EOStructure)etablissementPrincipal, createur.toPersonne().persId());
		}
		// contrat.setAvisFavorable("O");
		//System.out.println("Creation 03");
		return contrat;
	}
	
	/**
	 * Modification d'une convention
	 * @param contrat Contrat correspondant a la convention a modifier (obligatoire)
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param modeGestion Mode de gestion de la convention, ex: Ressource affectee (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'bjet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateDebut Date de debut de la convention (obligatoire)
	 * @param dateFin Date de fin de la convention (obligatoire)
	 * @param dateDebutExecution Date de debut d'execution de la convention
	 * @param dateFinExecution Date de fin d'execution de la convention
	 * @param dateSignature Date de signature en CA de la convention
	 * @param dateCloture Date de cloture de la convention
	 * @param montantGlobalHT Montant global HT de la convention
	 * @param tauxTva Taux de TVA pour le montant global.
	 * @param creditsLimitatifs Indique si le montant des credits positionnes est indepassable au moment d'une depense.
	 * @param lucratif Indicateur de lucrativite de la convention
	 * @param tvaRecuperable Indicateur de recuperation (collecte) de la convention
	 * @param typeReconduction Type de reconduction (ex: Expresse)
	 * @param naf code naf (domaine fonctionnel)
	 * @param utilisateur Utilisateur createur de la convention (obligatoire)
	 * @throws ExceptionArgument 
	 */
	public void modifierConvention(
			final Contrat contrat,
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final ModeGestion modeGestion,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateDebut,
			final NSTimestamp dateFin,
			final Integer duree,
			final NSTimestamp dateDebutExecution,
			final NSTimestamp dateFinExecution,
			final NSTimestamp dateSignature,
			final NSTimestamp dateCloture,
			final BigDecimal montantGlobalHT,
			final EOTva tauxTva,
			final Boolean creditsLimitatifs,
			final Boolean lucratif,
			final Boolean tvaRecuperable,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur utilisateur,
			final NSTimestamp dateModification) throws ExceptionArgument,Exception, ExceptionUtilisateur {
		
		// contrat
		contratFactory.modifierContrat(
				contrat,
				typeClassificationContrat,
				referenceExterne, 
				etablissementGestionnaire, 
				centreResponsabiliteGestionnaire, 
				typeContrat, 
				objet, 
				observations, 
				dateCloture, 
				typeReconduction, 
				naf,
				utilisateur, 
				dateModification);
		
		// avenant zero
		avenantFactory.modifierAvenant(
				contrat.avenantZero(), 
				modeGestion,
				centreResponsabiliteGestionnaire, 
				referenceExterne, 
				objet, 
				observations, 
				dateDebut, 
				dateFin, 
				duree,
				dateDebutExecution,
				dateFinExecution,
				dateSignature,
				montantGlobalHT, 
				tauxTva, 
				creditsLimitatifs, 
				lucratif, 
				tvaRecuperable, 
				utilisateur, 
				dateModification);
	}
	
	
	/**
	 * Suppression d'une convention. Elle n'est pas supprimee mais archivee (ainsi que ses avenants).
	 * @param contrat Contrat correspondant a la convention a supprimer.
	 * @throws Exception 
	 */
	public void supprimerConvention(
			final Contrat contrat) throws Exception, ExceptionUtilisateur {

		contratFactory.supprimerContrat(contrat);
		contratFactory.supprimerAvenants(contrat, true);
	}
	
	/**
	 * Suppression d'une convention par un ADMIN. Elle n'est pas supprimee mais archivee (ainsi que ses avenants).
	 * @param contrat Contrat correspondant a la convention a supprimer.
	 * @throws Exception 
	 */
	public void supprimerConventionAdmin(
			final Contrat contrat) throws Exception, ExceptionUtilisateur {
		
		contratFactory.supprimerContratAdmin(contrat);
		contratFactory.supprimerAvenantsAdmin(contrat, true);
	}

	/**
	 * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais archives.
	 * @param contrat Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @throws ExceptionArgument 
	 */
	public void supprimerAvenants(
			final Contrat contrat,
			final boolean avenantZeroAussi) throws ExceptionUtilisateur {

		trace("supprimerAvenants()");
		trace(contrat);
		trace(new Boolean(avenantZeroAussi));
		
		// Verifier que la convention n'est pas validee
		if (contrat.conDateValidAdm() != null) {
			throw new ExceptionUtilisateur(
				"La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n" +
				"Il n'est donc plus possible de la supprimer.");
		}

		for (int i=0; i<contrat.avenantsDontInitialNonSupprimes().count(); i++) {
			Avenant avenant = (Avenant) contrat.avenantsDontInitialNonSupprimes().objectAtIndex(i);
			if (avenantZeroAussi || avenant.avtIndex().intValue()!=0) {
				this.avenantFactory.supprimerAvenant(avenant);
				//System.out.println("Avenant "+avenant.avtIndex()+" archiv\u00E9.");
			}
		}
	}
	/**
	 * Invalide (refetch) la convention : le contrat et l'avenant zero.
	 * @param contrat Contrat correspondant a la convention.
	 */
	public void invalidateConvention(final Contrat contrat) {
		NSMutableArray gids = new NSMutableArray();
		gids.addObject(ec.globalIDForObject(contrat));
		gids.addObject(ec.globalIDForObject(contrat.avenantZero()));
		ec.invalidateObjectsWithGlobalIDs(gids);
	}
	
	
	/**
	 * Ajout d'un partenaire a une convention.
	 * @param contrat Contrat correspondant a la convention a laquelle on veut ajouter un partenaire.
	 * @param personne Personne physique ou morale partenaire 
	 * @param typePartenaire Type du partenariat.
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @return La partenariat cree.
	 * @throws Exception
	 */
	public ContratPartenaire ajouterPartenaire(
			final Contrat contrat,
			final IPersonne personne,
			final NSArray roles,
			final Boolean partenairePrincipal,
			final BigDecimal montantApporte) throws Exception, ExceptionUtilisateur {
		
		return ajouterPartenaireSurPeriode(
				contrat,
				personne,
				roles,
				null,//dateDebut
				null,//dateFin
				partenairePrincipal,
				montantApporte,
				null); //rasCommentaire
	}
	
	/**
	 * Ajout d'un partenaire a une convention.
	 * @param contrat Contrat correspondant a la convention a laquelle on veut ajouter un partenaire.
	 * @param personne Personne physique ou morale partenaire 
	 * @param typePartenaire Type du partenariat.
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @return La partenariat cree.
	 * @throws Exception
	 */
	public ContratPartenaire ajouterPartenaireSurPeriode(
			final Contrat contrat,
			final IPersonne personne,
			final NSArray roles,
			final NSTimestamp dateDebut,
			final NSTimestamp dateFin,
			final Boolean partenairePrincipal,
			final BigDecimal montantApporte,
			final String rasCommentaire) throws Exception, ExceptionUtilisateur {
		
		if (contrat == null)
			throw new ExceptionUtilisateur("La convention doit \u00EAtre fournie pour lui ajouter un partenaire.");
		
//		Debug.printArray("FactoryConvention.ajouterPartenaire()>>  contrat.avenantZero().avenantPartenaires()", contrat.avenantZero().avenantPartenaires());
		
		// On verifie que le partenariat n'existe pas deja
		System.out.println("ajouterPartenaireSurPeriode : "+personne.persId());

		NSArray partenariats = EOQualifier.filteredArrayWithQualifier(
				contrat.contratPartenaires(),
				EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(personne.persId())));
		if (partenariats!=null && partenariats.count()>0)
			throw new ExceptionUtilisateur("Ce partenariat existe d\u00E9j\u00E0.");
		
//		// On verifie que le partenariat n'existe pas deja, uniquement dans le cas ou le contrat ne vient pas d'etre insere, 
//		// cad present dans la liste des insertedObjects de l'ec,
//		// Si le contrat vient d'etre insere, la recherche de partenariat existant provoque logiquement l'erreur :
//		// "java.lang.IllegalStateException: attempt to refault an object that was inserted into this context"
//		// (et de toute facon il ne peut avoir encore de partenariat)
//		if (!ec.insertedObjects().containsObject(contrat)) {
//			
//			AvenantPartenaireFinder apf = new AvenantPartenaireFinder(ec);
//			apf.setContrat(contrat);
//			apf.setPartenaire(personne);
//			try {
//				NSArray partenariats = apf.find();
//				if (partenariats!=null && partenariats.count()>0)
//					throw new ArgumentException("Ce partenariat exsite d\u00E9j\u00E0.");
//			} 
//			catch (FinderException e) {
//				
//			}
//		}
//		Debug.printArray("partenariats", (NSArray) contrat.avenantZero().avenantPartenaires().valueForKey("cpMontant"));
//		System.out.println("contrat.montantDisponiblePourPartenariat() = "+contrat.montantDisponiblePourPartenariat());
		
		BigDecimal montant = montantApporte;
		if (montantApporte!=null && contrat.montantDisponiblePourPartenariat().compareTo(montantApporte)<0) {
//			montant = contrat.montantDisponiblePourPartenariat();
			throw new ExceptionUtilisateur(
					"La somme des montants apport\u00E9s par les partenaires ne peut d\u00E9passer le montant global de la " +
					"convention (avenants compris).\n\n" +
					"Le montant apport\u00E9 par le partenaire '" +	
					personne + 
					"' (" + 
					nombreOperation.getFormatterMoney().format(montantApporte) + 
					") d\u00E9passe le montant non encore affect\u00E9 de cette convention qui est de " + 
					nombreOperation.getFormatterMoney().format(contrat.montantDisponiblePourPartenariat()) + 
					".\n");
		}

		ContratPartenaire contPartenaire = contratPartenaireFactory.creerContratPartenaire(
				contrat, 
				personne, 
				roles,
				dateDebut,
				dateFin,
				partenairePrincipal,
				montant,
				rasCommentaire);
		if (contPartenaire != null) {
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(ec, "edc");
			NSNotificationCenter.defaultCenter().postNotification("refreshPartenairesNotification", contPartenaire, userInfo);
		}
		return contPartenaire;
	}

	
	
	
	
	
	
	
	public static boolean isValidConventionCreation(Contrat contrat) {
		return FactoryContrat.isValidContratCreation(contrat) && contrat.avenantZero()!=null && FactoryAvenant.isValidAvenantCreation(contrat.avenantZero());
	}

//	/**
//	 * Ajout d'un partenaire a une convention.
//	 * @param conOrdre Identifiant du contrat correspondant a la convention a laquelle on veut ajouter un partenaire.
//	 * @param persId Identifiant de la personne physique ou morale partenaire 
//	 * @param typePartenaire Type du partenariat.
//	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
//	 * @param montantApporte Montant apporte par ce partenaire.
//	 * @return La partenariat cree.
//	 * @throws ArgumentException
//	 * @throws InstantiationException
//	 * @throws FinderException 
//	 */
//	public AvenantPartenaire ajouterPartenaire(
//			final Number conOrdre,
//			final Number persId,
//			final TypePartenaire typePartenaire, 
//			final boolean partenairePrincipal,
//			final BigDecimal montantApporte) throws ArgumentException, InstantiationException, FinderException {
//		
//		if (conOrdre == null)
//			throw new ArgumentException("L'identifiant de la convention doit \u00EAtre fourni pour lui ajouter un partenaire.");
//		
//		Contrat contrat = (Contrat) new ConventionFinder(ec).findWithConOrdre(conOrdre).objectAtIndex(0);
//		STPersonne personne = (STPersonne) new PersonneFinder(ec).findWithPersId(persId).objectAtIndex(0);
//		
//		return ajouterPartenaire(
//				contrat, 
//				personne, 
//				typePartenaire, 
//				partenairePrincipal, 
//				montantApporte);
//	}
	
	/**
	 * Cree et ajoute une tranche a une convention.
	 * @param contrat Contrat correspondant a la convention a laquelle on veut ajouter une tranche.
	 * @param exercice Exercice de la nouvelle tranche.
	 * @param montantDepenses Montant previsionnel des depenses sur cette tranche.
	 * @param montantRecettes Montant previsionnel des recettes sur cette tranche. <b>Pas encore utilise!!!</b>
	 * @param natureMontant Nature des flux financiers sur cette tranche. Ex: {@link Tranche#NATURE_DEPENSE_LIBELLE}.
	 * @param utilisateur Utilisateur qui demande la creation de cette tranche.
	 * @return La nouvelle tranche creee.
	 * @throws Exception
	 */
	public Tranche ajouterTranche(
			final Contrat contrat, 
			final EOExerciceCocktail exercice, 
			final BigDecimal montantDepenses, 
			final BigDecimal montantRecettes, 
			final String natureMontant, 
			final EOUtilisateur utilisateur) throws Exception,ExceptionUtilisateur {

		if (contrat == null)
			throw new ExceptionUtilisateur("La convention doit \u00EAtre fournie pour lui ajouter une tranche.");
		if (contrat.tranchePourExercice(exercice, false) != null)
			throw new ExceptionUtilisateur("La convention poss\u00E8de d\u00E9ja une tranche sur l'exercice "+exercice);
		
		Tranche tranche = trancheFactory.creerTranche(contrat, exercice, montantDepenses, montantRecettes, natureMontant, utilisateur);
		
		return tranche;
	}
	
	/**
	 * Supprime une tranche. Ele est seulement marquee comme telle.
	 * @param tranche Tranche a supprimer.
	 * @throws ExceptionUtilisateur 
	 */
	public void supprimerTranche(
			final Tranche tranche) throws ExceptionUtilisateur {

		if (tranche == null)
			throw new ExceptionUtilisateur("La tranche \u00E0 supprimer est requise.");
		
		tranche.setTraSuppr(Tranche.TRA_SUPPR_OUI);
	}


	/**
	 * Instancie autant de tranches que necessaire pour une convention. Cad pour les exercices non deja affectes.
	 * @param convention Convention concernee.
	 * @param utilisateur Utilisateur enregistre comme createur des tranches.
	 * @return Liste des tranches creees.
	 * @throws ExceptionFinder 
	 * @throws InstantiationException 
	 * @throws ExceptionOperationImpossible 
	 */
	public NSArray genererTranches(
			 Contrat contrat, 
			final EOUtilisateur createur) throws ExceptionFinder, Exception, ExceptionOperationImpossible {

		NSMutableArray tranches = null;

		if (contrat == null)
			throw new NullPointerException("Un contrat est necessaire.");
		if (createur == null)
			throw new NullPointerException("L\\\'utilisateur cr\u00E9ateur est requis.");
		if (contrat.dateDebut() == null) 
			throw new NullPointerException("Une date de d\u00E9but est requise.");
		if (contrat.montantHt() == null || contrat.montantHt().doubleValue()==0 ) 
			throw new NullPointerException("Un montant HT sup\u00E9rieur à 0 est requis.");
			
		if (contrat.montantHt() != null && contrat.montantHt().doubleValue()>0 ) {
			FinderExerciceCocktail fe=new FinderExerciceCocktail(ec);
			NSArray exercicesTous;
			if(contrat.dateFin()==null){
				exercicesTous = fe.findWithDatesDebutFin(contrat.dateDebut(), contrat.dateDebut());
			}
			else{
				// Recherche les exercices non deja utilises dans les tranches existantes
				exercicesTous = fe.findWithDatesDebutFin(contrat.dateDebut(), contrat.dateFin());
			}
			
			NSArray tranchesContrat = contrat.tranches();
			NSArray exercicesTranches = (NSArray) tranchesContrat.valueForKey("exerciceCocktail");
			
			NSMutableArray exercicesManquants = new NSMutableArray();
	//		DateOperation.print(contrat.dateDebut(true), false);
	//		DateOperation.print(contrat.dateFin(true), false);
			for (int i=0; i<exercicesTous.count(); i++) {
				if (!exercicesTranches.containsObject(exercicesTous.objectAtIndex(i)))
					exercicesManquants.addObject(exercicesTous.objectAtIndex(i));
			}
	//		Debug.printArray("exercicesTous", exercicesTous);
	//		Debug.printArray("exercicesTranches", exercicesTranches);
	//		Debug.printArray("exercicesManquants", exercicesManquants);
			
			NSMutableArray tranchesASupprimer = new NSMutableArray();
			for (int i=0; i<tranchesContrat.count(); i++) {
				Tranche uneTranche = (Tranche)tranchesContrat.objectAtIndex(i);
				if (!exercicesTous.containsObject(uneTranche.exerciceCocktail())) {
					tranchesASupprimer.addObject(uneTranche);
				}
			}
			
			tranches = new NSMutableArray();
			Tranche currentTranche;
			// des tranches a eliminer ?
			if (tranchesASupprimer.count() > 0) {
				Enumeration enumTranchesASupprimer = tranchesASupprimer.objectEnumerator();
				while (enumTranchesASupprimer.hasMoreElements()) {
					Tranche uneTranche = (Tranche) enumTranchesASupprimer.nextElement();
					contrat.removeFromTranchesRelationship(uneTranche);
					contrat.editingContext().deleteObject(uneTranche);
				}
				NotificationCenter.instance().notifier(Notification.REFRESH_TRANCHES, contrat.editingContext());
			}
			// des tranches manquantes a creer ?
			if (exercicesManquants.count() > 0) {
				FactoryTranche ft = new FactoryTranche(contrat.editingContext());
				for (int i=0; i<exercicesManquants.count(); i++) {
					currentTranche = ft.creerTranche(
							contrat, 
							(EOExerciceCocktail) exercicesManquants.objectAtIndex(i), 
							new BigDecimal(0), 
							new BigDecimal(0), 
							Tranche .NATURE_DEPENSE_RECETTE, 
							createur);
					
					tranches.addObject(currentTranche);
		            NotificationCenter.instance().notifier(Notification.REFRESH_TRANCHES, contrat.editingContext());
				}
			}
		}
		return tranches;
	}

	public ContratPartenaire ajouterPartenaireContractant(Contrat contrat,IPersonne personneAAjouter, Boolean partenairePrincipal, BigDecimal montantApporte) throws ExceptionUtilisateur, Exception {
		// Tous les partenaires ont un role par defaut de partenaire contractant dans le groupe Partenaire correspondant au contrat
		NSMutableDictionary values = new NSMutableDictionary();
		values.setObjectForKey(EOTypeAssociation.TAS_CODE_CONV, EOAssociation.TO_TYPE_ASSOCIATION_KEY+"."+EOTypeAssociation.TAS_CODE_KEY);
		values.setObjectForKey("PARTCONTRACT", EOAssociation.ASS_CODE_KEY);
		EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
		EOAssociation association = EOAssociation.fetchByQualifier(personneAAjouter.editingContext(), qualifier);
		return ajouterPartenaire(contrat, personneAAjouter, new NSArray(association), partenairePrincipal, montantApporte);		
	}
	
	public void supprimerPartenaire(Contrat contrat, ContratPartenaire partenaireASupprimer, Integer persIdUtilisateur) {
		// TODO Verifier avant de lancer la suppression du partenaire que le contrat n'est pas deja engage
		if (contrat != null && partenaireASupprimer != null) {
			// suppression du partenaire du groupe associe au contrat
			EOStructureForGroupeSpec.supprimerPersonneDuGroupe(contrat.editingContext(), partenaireASupprimer.partenaire(), contrat.groupePartenaire(), persIdUtilisateur);
			contrat.removeFromContratPartenairesRelationship(partenaireASupprimer);
			// Suppression des repartPartenaireTranches
			NSArray tranches = contrat.tranches();
			Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
			while (enumTranches.hasMoreElements()) {
				Tranche tranche = (Tranche) enumTranches.nextElement();
				NSArray rpt = tranche.toRepartPartenaireTranches();
				EOKeyValueQualifier qualifier = new EOKeyValueQualifier(RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY,EOQualifier.QualifierOperatorEqual,partenaireASupprimer);
				rpt = EOQualifier.filteredArrayWithQualifier(rpt, qualifier);
				Enumeration<RepartPartenaireTranche> enumRpt = rpt.immutableClone().objectEnumerator();
				while (enumRpt.hasMoreElements()) {
					RepartPartenaireTranche repartPartenaireTranche = (RepartPartenaireTranche) enumRpt.nextElement();
					tranche.removeObjectFromBothSidesOfRelationshipWithKey(repartPartenaireTranche, RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY);
					ec.deleteObject(repartPartenaireTranche);
				}
			}
			
		}
	}
	
}
