package org.cocktail.cocowork.server.metier.convention;

import java.io.Serializable;
import java.math.BigDecimal;

public class ContratFinancementDepensesValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Verifie que le total des contributions des partenaires est superieur ou égal 
     * à la somme des budgets de depenses positionnés.
     * 
     * @param contrat le contrat a valider.
     * @return true si le financement du contrat est valide, false sinon.
     */
    public boolean isSatisfiedBy(Contrat contrat) {
        if (contrat == null) {
            return false;
        }
        
        BigDecimal totalContributionsAPositionner = contrat.totalContributionsAPositionner();
        BigDecimal totalPositionneContrat = contrat.totalPositionneContrat();
        return totalContributionsAPositionner.subtract(totalPositionneContrat).doubleValue() >= 0;
    } 
}
