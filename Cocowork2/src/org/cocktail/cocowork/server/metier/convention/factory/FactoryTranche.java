/**
 * Cocowork
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.server.metier.convention.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.cocowork.common.date.DateOperation;
import org.cocktail.cocowork.common.exception.ExceptionArgument;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.cocowork.server.metier.convention.service.NotificationCenter;
import org.cocktail.cocowork.server.metier.convention.service.NotificationCenter.Notification;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * 
 * @author Michael HALLER, Consortium Cocktail, 2008
 * 
 */
public class FactoryTranche extends Factory 
{

	/**
	 * Constructeur.
	 * @param ec
	 */
	public FactoryTranche(EOEditingContext ec) {
		super(ec);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param ec
	 * @param withlog
	 */
	public FactoryTranche(EOEditingContext ec, Boolean withlog) {
		super(ec, withlog);
		// TODO Auto-generated constructor stub
	}

	
	/**
	 * Creation d'une tranche annuelle.
	 * @param contrat Contrat de rattachement.
	 * @param exercice Exercice correspondant. Une tranche annuelle <--> un exercice.
	 * @param montantDepenses Montant total des depenses prevues pour l'exercice correspondant. 
	 * @param montantRecettes Montant total des recettes prevues pour l'exercice correspondant. PAS ENCORE UTILISE : DEP = REC.
	 * @param natureMontant Nature du montant de cette tranche (ex: {@link Tranche#NATURE_DEPENSE_RECETTE}).
	 * @param createur Utilisateur createur de la tranche. PAS ENCORE UTILISE.
	 * @return La tranche creee.
	 * @throws Exception
	 */
	public Tranche creerTranche(
			 Contrat contrat,
			final EOExerciceCocktail exercice,
			final BigDecimal montantDepenses,
			final BigDecimal montantRecettes,
			final String natureMontant,
			final EOUtilisateur createur) throws Exception,ExceptionUtilisateur {

		if (contrat == null)
			throw new ExceptionUtilisateur("Un contrat est necessaire pour créer une tranche.");
		if (exercice == null)
			throw new ExceptionUtilisateur("Un exercice est necessaire pour créer une tranche.");
		if (createur == null)
			throw new ExceptionUtilisateur("L'utilisateur cr\u00E9ateur de la tranche est requis.");
		if (montantDepenses == null)
			throw new ExceptionUtilisateur("Un montant sup\u00E9rieur ou \u00E9gal \u00E0 z\u00E9ro est n\u00E9cessaire concernant les d\u00E9penses.");
//		if (montantRecettes == null)
//			throw new ArgumentException("Un montant sup\u00E9rieur ou \u00E9gal \u00E0 z\u00E9ro est n\u00E9cessaire concernant les recettes.");
		if (!Tranche.NATURE_DEPENSE.equals(natureMontant) && 
				!Tranche.NATURE_RECETTE.equals(natureMontant) &&
				!Tranche.NATURE_DEPENSE_RECETTE.equals(natureMontant))
			throw new ExceptionUtilisateur("Nature du montant de la tranche inconnu.");
		
		
		Tranche tranche = Tranche.instanciate(ec);
		
		ec.insertObject(tranche);

		tranche.setExerciceCocktailRelationship(exercice);
		tranche.setUtilisateurCreationRelationship(createur);
		tranche.setUtilisateurModifRelationship(createur);
		// tranche.setContratRelationship(contrat);
		
		tranche.setTraSuppr(Tranche.TRA_SUPPR_NON);
		tranche.setTraMontant(montantDepenses);
		tranche.setTraMontantInit(montantDepenses);
		tranche.setTraNatureMontant(natureMontant);
		tranche.setTraValide(Tranche.TRA_VALIDE_NON);
		tranche.setTraDateCreation(new NSTimestamp());
		
		// Creation des repartPartenaireTranche
		NSArray partenaires = contrat.contratPartenaires();
		Enumeration<ContratPartenaire>enumPartenaires = partenaires.objectEnumerator();
		while (enumPartenaires.hasMoreElements()) {
			ContratPartenaire contratPartenaire = (ContratPartenaire) enumPartenaires.nextElement();
			RepartPartenaireTranche rpt = RepartPartenaireTranche.instanciate(ec);
			ec.insertObject(rpt);
			rpt.setRptMontantParticipation(BigDecimal.ZERO);
			rpt.setRptTauxParticipation(BigDecimal.ZERO);
			contratPartenaire.addToContributionsRelationship(rpt);
			tranche.addToToRepartPartenaireTranchesRelationship(rpt);
		}
		contrat.addToTranchesRelationship(tranche);
        NotificationCenter.instance().notifier(Notification.REFRESH_TRANCHES, contrat.editingContext());
		return tranche;
	}

		
	
	/**
	 * Supprime une tranche. Ele est seulement marquee comme telle.
	 * @param tranche Tranche a supprimer.
	 */
	public void supprimerTranche(
			final Tranche tranche) throws ExceptionUtilisateur {

		if (tranche == null)
			throw new ExceptionUtilisateur("La tranche \u00E0 supprimer est requise.");
		
		tranche.setTraSuppr(Tranche.TRA_SUPPR_OUI);
	}

	/**
	 * Modification d'une tranche existante.
	 * @param contrat Contrat de rattachement.
	 * @param exercice Exercice correspondant. Une tranche annuelle <--> un exercice.
	 * @param montantDepenses Montant total des depenses prevues pour l'exercice correspondant. 
	 * @param montantRecettes Montant total des recettes prevues pour l'exercice correspondant. PAS ENCORE UTILISE : DEP = REC.
	 * @param natureMontant Nature du montant de cette tranche (ex: {@link Tranche#NATURE_DEPENSE_RECETTE}).
	 * @param utilisateur Utilisateur modificateur de la tranche. PAS ENCORE UTILISE.
	 * @throws ExceptionArgument
	 */
	public void modifierTranche(
			final Tranche tranche,
			final Contrat contrat,
			final EOExerciceCocktail exercice,
			final BigDecimal montantDepenses,
			final BigDecimal montantRecettes,
			final String natureMontant,
			final EOUtilisateur utilisateur) throws Exception, ExceptionUtilisateur {

		if (contrat == null)
			throw new ExceptionUtilisateur("Un contrat est necessaire pour modifier la tranche.");
		if (exercice == null)
			throw new ExceptionUtilisateur("Un exercice est necessaire pour modifier la tranche.");
		if (utilisateur == null)
			throw new ExceptionUtilisateur("L'utilisateur modificateur de la tranche est requis pour modifier la tranche.");
		if (montantDepenses == null)
			throw new ExceptionUtilisateur("Un montant sup\u00E9rieur ou \u00E9gal \u00E0 z\u00E9ro est n\u00E9cessaire concernant les d\u00E9penses.");
//		if (montantRecettes == null)
//			throw new ArgumentException("Un montant sup\u00E9rieur ou \u00E9gal \u00E0 z\u00E9ro est n\u00E9cessaire concernant les recettes.");
		if (!Tranche.NATURE_DEPENSE.equals(natureMontant) && 
				!Tranche.NATURE_RECETTE.equals(natureMontant) &&
				!Tranche.NATURE_DEPENSE_RECETTE.equals(natureMontant))
			throw new ExceptionUtilisateur("Nature du montant de la tranche inconnu.");
		
		if (tranche.traValide()!=null && Tranche.TRA_VALIDE_OUI.equals(tranche.traValide()))
			throw new ExceptionUtilisateur(
					"Impossible de modifier cette tranche, elle a \u00E9t\u00E9 valid\u00E9e le "+
					DateOperation.print(tranche.traDateValid(), false)+
					".");
		
		tranche.setContratRelationship(contrat);
		tranche.setExerciceCocktailRelationship(exercice);
		tranche.setTraMontant(montantDepenses);
		tranche.setTraNatureMontant(natureMontant);
		tranche.setTraDateModif(new NSTimestamp());
	}
}
