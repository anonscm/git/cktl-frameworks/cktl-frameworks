

// AvenantPartContact.java
// 

package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXKey;



public class ContratPartContact extends EOContratPartContact {

    public static final String PERSONNE_KEY = "personne";
    public static final ERXKey<IPersonne> PERSONNE = new ERXKey<IPersonne>(PERSONNE_KEY);

    private IPersonne personne, partenaire;
    
    public ContratPartContact() {
        super();
    }

    public IPersonne partenaire() {
    	
    	if (partenaire == null) {
	    	EOStructure struct = EOStructure.fetchByKeyValue(this.editingContext(), EOStructure.PERS_ID_KEY, persId());
	    	if(struct != null)
	    		partenaire = struct;
	    	
	    	EOIndividu indiv = EOIndividu.fetchByKeyValue(this.editingContext(), EOIndividu.PERS_ID_KEY, persId());
	    	if(indiv!=null)
	    		partenaire = indiv;
	    	}
    	return partenaire;
    }
    public void setPartenaire(IPersonne partenaire) {    	
    	setPersId(partenaire.persId());
    	this.partenaire = partenaire;
    }

    public void setPersonne(IPersonne personne) {    	
    	setPersIdContact(personne.persId());  
    	this.personne = personne;
    }
    
    public IPersonne personne() {
    	
    	if (personne == null) {
	    	EOStructure struct = EOStructure.fetchByKeyValue(this.editingContext(), EOStructure.PERS_ID_KEY, persIdContact());
	    	if(struct != null)
	    		personne = struct;
	    	
	    	EOIndividu indiv = EOIndividu.fetchByKeyValue(this.editingContext(), EOIndividu.PERS_ID_KEY, persIdContact());
	    	if(indiv!=null)
	    		personne =  indiv;
    	}
    	
    	return personne;
    }

    
//    public IPersonne personne() {
//    	IPersonne personne = null;
//    	
//    	personne = individu();
//    	if (personne == null) {
//    		personne = partenaireStructure();
//    	}
//    	
//    	return personne;
//    }
    public String responsabilite() {
    	// TODO A coder
    	String responsabilite = "";
    	
    	return responsabilite;
    }
    public String qualite() {
    	String qualite = "";
    	
    	if (personne() != null) {
    		qualite = personne().persType();
    	}
    	return qualite;
    }
    
    /**
     * @return NSArray de EOAssociation
     */
    public static NSArray tousLesRolesContact(EOEditingContext ec) {
    	NSArray roles = null;
		NSMutableDictionary values = new NSMutableDictionary();
		values.setObjectForKey("CONTACT", EOAssociation.ASS_CODE_KEY);
		EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
		EOAssociation associationContact = EOAssociation.fetchByQualifier(ec, qualifier);
		if (associationContact != null) {
			roles = associationContact.getFils(ec);
		}
    	return roles;
    }

    public NSArray roles() {
    	NSArray roles = null;
    	IPersonne personne = personne();
    	EOQualifier qualifier = new EOKeyValueQualifier(EORepartAssociation.TO_ASSOCIATION_KEY+"."+EOAssociation.ASS_LIBELLE_KEY, 
    			EOKeyValueQualifier.QualifierOperatorNotEqual, "ADMINISTRATEUR");
    	roles = personne.getRepartAssociationsInGroupe(contratPartenaire().contrat().groupePartenaire(), qualifier);
    	return roles;
    }
}
