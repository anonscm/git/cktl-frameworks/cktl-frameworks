package org.cocktail.cocowork.server.metier.convention.procedure.suividepense;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Procedure qui calcule le total des engagements au titre d'une convention sur un exercice, 
 * pour un type de credit et une ligne de l'organigramme budgetaire.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public class ProcedureGetTotalEngage extends ProcedureSuiviDepense
{
	protected final static String PROCEDURE_NAME = "GetTotalEngage";
	
	
	/**
	 * Constructeur protected.
	 * @param ec Editing context a utiliser.
	 */
	public ProcedureGetTotalEngage(final EOEditingContext ec) { 
		super(ec, PROCEDURE_NAME);
	}
	
}
