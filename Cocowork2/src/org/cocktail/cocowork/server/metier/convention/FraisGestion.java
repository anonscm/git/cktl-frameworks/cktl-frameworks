package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;

public class FraisGestion extends EOFraisGestion {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(FraisGestion.class);
    

    public void recalculerFrais() {
        setFgPctIntermediaire(fgPctTra());
    }
    
    public BigDecimal getFgMontantIntermediaire() {
        return fgMontant();
    }
    
    public void setFgMontantIntermediaire(BigDecimal value) {
    	if(value == null) {
    		value = BigDecimal.ZERO;
    	}
        super.setFgMontant(value);
        // On recalcule le pourcentage
//        BigDecimal pct = 
        BigDecimal montantParticip = repartPartenaireTranche().rptMontantParticipation();
        if (montantParticip != null && montantParticip.compareTo(BigDecimal.ZERO) != 0) {
            BigDecimal pct = value.divide(montantParticip, 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            setFgPctTra(pct);
        } else {
            setFgMontant(BigDecimal.ZERO);
            setFgPctTra(BigDecimal.ZERO);
        }
    }
    
    public BigDecimal getFgPctIntermediaire() {
        return fgPctTra();
    }
    
    public void setFgPctIntermediaire(BigDecimal value) {
    	if(value == null) {
    		value = BigDecimal.ZERO;
    	}
        super.setFgPctTra(value);
        // On recalcule le montant 
        BigDecimal montantParticip = repartPartenaireTranche().rptMontantParticipation();
        if (montantParticip != null) {
            BigDecimal montant = montantParticip.multiply(value).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
            setFgMontant(montant);
        }
    }
    
}
