// DO NOT EDIT.  Make changes to IndicateursContrat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOIndicateursContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWIndicateursContrat";

  // Attribute Keys
  public static final ERXKey<String> IC_CODE = new ERXKey<String>("icCode");
  public static final ERXKey<Integer> IC_ID = new ERXKey<Integer>("icId");
  public static final ERXKey<String> IC_LIBELLE = new ERXKey<String>("icLibelle");
  // Relationship Keys

  // Attributes
  public static final String IC_CODE_KEY = IC_CODE.key();
  public static final String IC_ID_KEY = IC_ID.key();
  public static final String IC_LIBELLE_KEY = IC_LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOIndicateursContrat.class);

  public IndicateursContrat localInstanceIn(EOEditingContext editingContext) {
    IndicateursContrat localInstance = (IndicateursContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String icCode() {
    return (String) storedValueForKey(EOIndicateursContrat.IC_CODE_KEY);
  }

  public void setIcCode(String value) {
    if (EOIndicateursContrat.LOG.isDebugEnabled()) {
        EOIndicateursContrat.LOG.debug( "updating icCode from " + icCode() + " to " + value);
    }
    takeStoredValueForKey(value, EOIndicateursContrat.IC_CODE_KEY);
  }

  public Integer icId() {
    return (Integer) storedValueForKey(EOIndicateursContrat.IC_ID_KEY);
  }

  public void setIcId(Integer value) {
    if (EOIndicateursContrat.LOG.isDebugEnabled()) {
        EOIndicateursContrat.LOG.debug( "updating icId from " + icId() + " to " + value);
    }
    takeStoredValueForKey(value, EOIndicateursContrat.IC_ID_KEY);
  }

  public String icLibelle() {
    return (String) storedValueForKey(EOIndicateursContrat.IC_LIBELLE_KEY);
  }

  public void setIcLibelle(String value) {
    if (EOIndicateursContrat.LOG.isDebugEnabled()) {
        EOIndicateursContrat.LOG.debug( "updating icLibelle from " + icLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOIndicateursContrat.IC_LIBELLE_KEY);
  }


  public static IndicateursContrat create(EOEditingContext editingContext, Integer icId
) {
    IndicateursContrat eo = (IndicateursContrat) EOUtilities.createAndInsertInstance(editingContext, EOIndicateursContrat.ENTITY_NAME);    
        eo.setIcId(icId);
    return eo;
  }

  public static ERXFetchSpecification<IndicateursContrat> fetchSpec() {
    return new ERXFetchSpecification<IndicateursContrat>(EOIndicateursContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<IndicateursContrat> fetchAll(EOEditingContext editingContext) {
    return EOIndicateursContrat.fetchAll(editingContext, null);
  }

  public static NSArray<IndicateursContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOIndicateursContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<IndicateursContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<IndicateursContrat> fetchSpec = new ERXFetchSpecification<IndicateursContrat>(EOIndicateursContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<IndicateursContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static IndicateursContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOIndicateursContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static IndicateursContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<IndicateursContrat> eoObjects = EOIndicateursContrat.fetchAll(editingContext, qualifier, null);
    IndicateursContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWIndicateursContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static IndicateursContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOIndicateursContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static IndicateursContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    IndicateursContrat eoObject = EOIndicateursContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWIndicateursContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static IndicateursContrat localInstanceIn(EOEditingContext editingContext, IndicateursContrat eo) {
    IndicateursContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}