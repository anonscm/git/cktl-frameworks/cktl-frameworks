// DO NOT EDIT.  Make changes to Contrat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWContrat";

  // Attribute Keys
  public static final ERXKey<String> AVIS_DEFAVORABLE = new ERXKey<String>("avisDefavorable");
  public static final ERXKey<String> AVIS_FAVORABLE = new ERXKey<String>("avisFavorable");
  public static final ERXKey<NSTimestamp> CON_DATE_APUREMENT = new ERXKey<NSTimestamp>("conDateApurement");
  public static final ERXKey<NSTimestamp> CON_DATE_CLOTURE = new ERXKey<NSTimestamp>("conDateCloture");
  public static final ERXKey<NSTimestamp> CON_DATE_CREATION = new ERXKey<NSTimestamp>("conDateCreation");
  public static final ERXKey<NSTimestamp> CON_DATE_FIN_PAIEMENT = new ERXKey<NSTimestamp>("conDateFinPaiement");
  public static final ERXKey<NSTimestamp> CON_DATE_MODIF = new ERXKey<NSTimestamp>("conDateModif");
  public static final ERXKey<NSTimestamp> CON_DATE_VALID_ADM = new ERXKey<NSTimestamp>("conDateValidAdm");
  public static final ERXKey<Integer> CON_DUREE = new ERXKey<Integer>("conDuree");
  public static final ERXKey<Integer> CON_DUREE_MOIS = new ERXKey<Integer>("conDureeMois");
  public static final ERXKey<String> CON_GROUPE_BUD = new ERXKey<String>("conGroupeBud");
  public static final ERXKey<Integer> CON_INDEX = new ERXKey<Integer>("conIndex");
  public static final ERXKey<Integer> CON_NATURE = new ERXKey<Integer>("conNature");
  public static final ERXKey<String> CON_OBJET = new ERXKey<String>("conObjet");
  public static final ERXKey<String> CON_OBJET_COURT = new ERXKey<String>("conObjetCourt");
  public static final ERXKey<String> CON_OBSERVATIONS = new ERXKey<String>("conObservations");
  public static final ERXKey<Integer> CON_ORDRE = new ERXKey<Integer>("conOrdre");
  public static final ERXKey<String> CON_REFERENCE_EXTERNE = new ERXKey<String>("conReferenceExterne");
  public static final ERXKey<String> CON_SUPPR = new ERXKey<String>("conSuppr");
  public static final ERXKey<String> CONTEXTE = new ERXKey<String>("contexte");
  public static final ERXKey<String> MOTIFS_AVIS = new ERXKey<String>("motifsAvis");
  public static final ERXKey<String> REMARQUES = new ERXKey<String>("remarques");
  public static final ERXKey<Integer> TR_ORDRE = new ERXKey<Integer>("trOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant> AVENANTS = new ERXKey<org.cocktail.cocowork.server.metier.convention.Avenant>("avenants");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AxeStrategique> AXE_STRATEGIQUES = new ERXKey<org.cocktail.cocowork.server.metier.convention.AxeStrategique>("axeStrategiques");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> BROUILLARDS = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>("brouillards");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> CENTRE_RESPONSABILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("centreResponsabilite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf> CODE_NAF = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf>("codeNaf");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> CONTRAT_PARTENAIRES = new ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>("contratPartenaires");
  public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> DEPENSE_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>("depenseControleConventions");
  public static final ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> ENGAGEMENT_CONTROLE_CONVENTIONS = new ERXKey<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>("engagementControleConventions");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> ETABLISSEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("etablissement");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> GROUPE_PARTENAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("groupePartenaire");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> INDICATEURS_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.IndicateursContrat>("indicateursContrat");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN_COMPOSANTE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organComposante");
  public static final ERXKey<org.cocktail.kava.server.metier.EOPrestation> PRESTATIONS = new ERXKey<org.cocktail.kava.server.metier.EOPrestation>("prestations");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.ProjetContrat> PROJET_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.ProjetContrat>("projetContrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> REPART_INDICATEURS_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat>("repartIndicateursContrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHES = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranches");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat> TYPE_CLASSIFICATION_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat>("typeClassificationContrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat> TYPE_CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeContrat>("typeContrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypeReconduction> TYPE_RECONDUCTION = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypeReconduction>("typeReconduction");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_CREATION = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurCreation");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_MODIF = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurModif");
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur> UTILISATEUR_VALID_ADM = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur>("utilisateurValidAdm");

  // Attributes
  public static final String AVIS_DEFAVORABLE_KEY = AVIS_DEFAVORABLE.key();
  public static final String AVIS_FAVORABLE_KEY = AVIS_FAVORABLE.key();
  public static final String CON_DATE_APUREMENT_KEY = CON_DATE_APUREMENT.key();
  public static final String CON_DATE_CLOTURE_KEY = CON_DATE_CLOTURE.key();
  public static final String CON_DATE_CREATION_KEY = CON_DATE_CREATION.key();
  public static final String CON_DATE_FIN_PAIEMENT_KEY = CON_DATE_FIN_PAIEMENT.key();
  public static final String CON_DATE_MODIF_KEY = CON_DATE_MODIF.key();
  public static final String CON_DATE_VALID_ADM_KEY = CON_DATE_VALID_ADM.key();
  public static final String CON_DUREE_KEY = CON_DUREE.key();
  public static final String CON_DUREE_MOIS_KEY = CON_DUREE_MOIS.key();
  public static final String CON_GROUPE_BUD_KEY = CON_GROUPE_BUD.key();
  public static final String CON_INDEX_KEY = CON_INDEX.key();
  public static final String CON_NATURE_KEY = CON_NATURE.key();
  public static final String CON_OBJET_KEY = CON_OBJET.key();
  public static final String CON_OBJET_COURT_KEY = CON_OBJET_COURT.key();
  public static final String CON_OBSERVATIONS_KEY = CON_OBSERVATIONS.key();
  public static final String CON_ORDRE_KEY = CON_ORDRE.key();
  public static final String CON_REFERENCE_EXTERNE_KEY = CON_REFERENCE_EXTERNE.key();
  public static final String CON_SUPPR_KEY = CON_SUPPR.key();
  public static final String CONTEXTE_KEY = CONTEXTE.key();
  public static final String MOTIFS_AVIS_KEY = MOTIFS_AVIS.key();
  public static final String REMARQUES_KEY = REMARQUES.key();
  public static final String TR_ORDRE_KEY = TR_ORDRE.key();
  // Relationships
  public static final String AVENANTS_KEY = AVENANTS.key();
  public static final String AXE_STRATEGIQUES_KEY = AXE_STRATEGIQUES.key();
  public static final String BROUILLARDS_KEY = BROUILLARDS.key();
  public static final String CENTRE_RESPONSABILITE_KEY = CENTRE_RESPONSABILITE.key();
  public static final String CODE_NAF_KEY = CODE_NAF.key();
  public static final String CONTRAT_PARTENAIRES_KEY = CONTRAT_PARTENAIRES.key();
  public static final String DEPENSE_CONTROLE_CONVENTIONS_KEY = DEPENSE_CONTROLE_CONVENTIONS.key();
  public static final String ENGAGEMENT_CONTROLE_CONVENTIONS_KEY = ENGAGEMENT_CONTROLE_CONVENTIONS.key();
  public static final String ETABLISSEMENT_KEY = ETABLISSEMENT.key();
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String GROUPE_PARTENAIRE_KEY = GROUPE_PARTENAIRE.key();
  public static final String INDICATEURS_CONTRAT_KEY = INDICATEURS_CONTRAT.key();
  public static final String ORGAN_COMPOSANTE_KEY = ORGAN_COMPOSANTE.key();
  public static final String PRESTATIONS_KEY = PRESTATIONS.key();
  public static final String PROJET_CONTRAT_KEY = PROJET_CONTRAT.key();
  public static final String REPART_INDICATEURS_CONTRAT_KEY = REPART_INDICATEURS_CONTRAT.key();
  public static final String TRANCHES_KEY = TRANCHES.key();
  public static final String TYPE_CLASSIFICATION_CONTRAT_KEY = TYPE_CLASSIFICATION_CONTRAT.key();
  public static final String TYPE_CONTRAT_KEY = TYPE_CONTRAT.key();
  public static final String TYPE_RECONDUCTION_KEY = TYPE_RECONDUCTION.key();
  public static final String UTILISATEUR_CREATION_KEY = UTILISATEUR_CREATION.key();
  public static final String UTILISATEUR_MODIF_KEY = UTILISATEUR_MODIF.key();
  public static final String UTILISATEUR_VALID_ADM_KEY = UTILISATEUR_VALID_ADM.key();

  private static Logger LOG = Logger.getLogger(EOContrat.class);

  public Contrat localInstanceIn(EOEditingContext editingContext) {
    Contrat localInstance = (Contrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String avisDefavorable() {
    return (String) storedValueForKey(EOContrat.AVIS_DEFAVORABLE_KEY);
  }

  public void setAvisDefavorable(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating avisDefavorable from " + avisDefavorable() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.AVIS_DEFAVORABLE_KEY);
  }

  public String avisFavorable() {
    return (String) storedValueForKey(EOContrat.AVIS_FAVORABLE_KEY);
  }

  public void setAvisFavorable(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating avisFavorable from " + avisFavorable() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.AVIS_FAVORABLE_KEY);
  }

  public NSTimestamp conDateApurement() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_APUREMENT_KEY);
  }

  public void setConDateApurement(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateApurement from " + conDateApurement() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_APUREMENT_KEY);
  }

  public NSTimestamp conDateCloture() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_CLOTURE_KEY);
  }

  public void setConDateCloture(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateCloture from " + conDateCloture() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_CLOTURE_KEY);
  }

  public NSTimestamp conDateCreation() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_CREATION_KEY);
  }

  public void setConDateCreation(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateCreation from " + conDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_CREATION_KEY);
  }

  public NSTimestamp conDateFinPaiement() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_FIN_PAIEMENT_KEY);
  }

  public void setConDateFinPaiement(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateFinPaiement from " + conDateFinPaiement() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_FIN_PAIEMENT_KEY);
  }

  public NSTimestamp conDateModif() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_MODIF_KEY);
  }

  public void setConDateModif(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateModif from " + conDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_MODIF_KEY);
  }

  public NSTimestamp conDateValidAdm() {
    return (NSTimestamp) storedValueForKey(EOContrat.CON_DATE_VALID_ADM_KEY);
  }

  public void setConDateValidAdm(NSTimestamp value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDateValidAdm from " + conDateValidAdm() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DATE_VALID_ADM_KEY);
  }

  public Integer conDuree() {
    return (Integer) storedValueForKey(EOContrat.CON_DUREE_KEY);
  }

  public void setConDuree(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDuree from " + conDuree() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DUREE_KEY);
  }

  public Integer conDureeMois() {
    return (Integer) storedValueForKey(EOContrat.CON_DUREE_MOIS_KEY);
  }

  public void setConDureeMois(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conDureeMois from " + conDureeMois() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_DUREE_MOIS_KEY);
  }

  public String conGroupeBud() {
    return (String) storedValueForKey(EOContrat.CON_GROUPE_BUD_KEY);
  }

  public void setConGroupeBud(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conGroupeBud from " + conGroupeBud() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_GROUPE_BUD_KEY);
  }

  public Integer conIndex() {
    return (Integer) storedValueForKey(EOContrat.CON_INDEX_KEY);
  }

  public void setConIndex(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conIndex from " + conIndex() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_INDEX_KEY);
  }

  public Integer conNature() {
    return (Integer) storedValueForKey(EOContrat.CON_NATURE_KEY);
  }

  public void setConNature(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conNature from " + conNature() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_NATURE_KEY);
  }

  public String conObjet() {
    return (String) storedValueForKey(EOContrat.CON_OBJET_KEY);
  }

  public void setConObjet(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conObjet from " + conObjet() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_OBJET_KEY);
  }

  public String conObjetCourt() {
    return (String) storedValueForKey(EOContrat.CON_OBJET_COURT_KEY);
  }

  public void setConObjetCourt(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conObjetCourt from " + conObjetCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_OBJET_COURT_KEY);
  }

  public String conObservations() {
    return (String) storedValueForKey(EOContrat.CON_OBSERVATIONS_KEY);
  }

  public void setConObservations(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conObservations from " + conObservations() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_OBSERVATIONS_KEY);
  }

  public Integer conOrdre() {
    return (Integer) storedValueForKey(EOContrat.CON_ORDRE_KEY);
  }

  public void setConOrdre(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conOrdre from " + conOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_ORDRE_KEY);
  }

  public String conReferenceExterne() {
    return (String) storedValueForKey(EOContrat.CON_REFERENCE_EXTERNE_KEY);
  }

  public void setConReferenceExterne(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conReferenceExterne from " + conReferenceExterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_REFERENCE_EXTERNE_KEY);
  }

  public String conSuppr() {
    return (String) storedValueForKey(EOContrat.CON_SUPPR_KEY);
  }

  public void setConSuppr(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating conSuppr from " + conSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CON_SUPPR_KEY);
  }

  public String contexte() {
    return (String) storedValueForKey(EOContrat.CONTEXTE_KEY);
  }

  public void setContexte(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating contexte from " + contexte() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.CONTEXTE_KEY);
  }

  public String motifsAvis() {
    return (String) storedValueForKey(EOContrat.MOTIFS_AVIS_KEY);
  }

  public void setMotifsAvis(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating motifsAvis from " + motifsAvis() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.MOTIFS_AVIS_KEY);
  }

  public String remarques() {
    return (String) storedValueForKey(EOContrat.REMARQUES_KEY);
  }

  public void setRemarques(String value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating remarques from " + remarques() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.REMARQUES_KEY);
  }

  public Integer trOrdre() {
    return (Integer) storedValueForKey(EOContrat.TR_ORDRE_KEY);
  }

  public void setTrOrdre(Integer value) {
    if (EOContrat.LOG.isDebugEnabled()) {
        EOContrat.LOG.debug( "updating trOrdre from " + trOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOContrat.TR_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure centreResponsabilite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOContrat.CENTRE_RESPONSABILITE_KEY);
  }
  
  public void setCentreResponsabilite(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOContrat.CENTRE_RESPONSABILITE_KEY);
  }

  public void setCentreResponsabiliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating centreResponsabilite from " + centreResponsabilite() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCentreResponsabilite(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = centreResponsabilite();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.CENTRE_RESPONSABILITE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.CENTRE_RESPONSABILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONaf codeNaf() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONaf)storedValueForKey(EOContrat.CODE_NAF_KEY);
  }
  
  public void setCodeNaf(org.cocktail.fwkcktlpersonne.common.metier.EONaf value) {
    takeStoredValueForKey(value, EOContrat.CODE_NAF_KEY);
  }

  public void setCodeNafRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONaf value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating codeNaf from " + codeNaf() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCodeNaf(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EONaf oldValue = codeNaf();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.CODE_NAF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.CODE_NAF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure etablissement() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOContrat.ETABLISSEMENT_KEY);
  }
  
  public void setEtablissement(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOContrat.ETABLISSEMENT_KEY);
  }

  public void setEtablissementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating etablissement from " + etablissement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEtablissement(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = etablissement();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.ETABLISSEMENT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.ETABLISSEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail)storedValueForKey(EOContrat.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOContrat.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure groupePartenaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOContrat.GROUPE_PARTENAIRE_KEY);
  }
  
  public void setGroupePartenaire(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOContrat.GROUPE_PARTENAIRE_KEY);
  }

  public void setGroupePartenaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating groupePartenaire from " + groupePartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setGroupePartenaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = groupePartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.GROUPE_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.GROUPE_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organComposante() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOContrat.ORGAN_COMPOSANTE_KEY);
  }
  
  public void setOrganComposante(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOContrat.ORGAN_COMPOSANTE_KEY);
  }

  public void setOrganComposanteRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating organComposante from " + organComposante() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrganComposante(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organComposante();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.ORGAN_COMPOSANTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.ORGAN_COMPOSANTE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat typeClassificationContrat() {
    return (org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat)storedValueForKey(EOContrat.TYPE_CLASSIFICATION_CONTRAT_KEY);
  }
  
  public void setTypeClassificationContrat(org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat value) {
    takeStoredValueForKey(value, EOContrat.TYPE_CLASSIFICATION_CONTRAT_KEY);
  }

  public void setTypeClassificationContratRelationship(org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating typeClassificationContrat from " + typeClassificationContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeClassificationContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat oldValue = typeClassificationContrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.TYPE_CLASSIFICATION_CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.TYPE_CLASSIFICATION_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypeContrat typeContrat() {
    return (org.cocktail.cocowork.server.metier.convention.TypeContrat)storedValueForKey(EOContrat.TYPE_CONTRAT_KEY);
  }
  
  public void setTypeContrat(org.cocktail.cocowork.server.metier.convention.TypeContrat value) {
    takeStoredValueForKey(value, EOContrat.TYPE_CONTRAT_KEY);
  }

  public void setTypeContratRelationship(org.cocktail.cocowork.server.metier.convention.TypeContrat value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating typeContrat from " + typeContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeContrat oldValue = typeContrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.TYPE_CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.TYPE_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypeReconduction typeReconduction() {
    return (org.cocktail.cocowork.server.metier.convention.TypeReconduction)storedValueForKey(EOContrat.TYPE_RECONDUCTION_KEY);
  }
  
  public void setTypeReconduction(org.cocktail.cocowork.server.metier.convention.TypeReconduction value) {
    takeStoredValueForKey(value, EOContrat.TYPE_RECONDUCTION_KEY);
  }

  public void setTypeReconductionRelationship(org.cocktail.cocowork.server.metier.convention.TypeReconduction value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating typeReconduction from " + typeReconduction() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeReconduction(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypeReconduction oldValue = typeReconduction();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.TYPE_RECONDUCTION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.TYPE_RECONDUCTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOContrat.UTILISATEUR_CREATION_KEY);
  }
  
  public void setUtilisateurCreation(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOContrat.UTILISATEUR_CREATION_KEY);
  }

  public void setUtilisateurCreationRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating utilisateurCreation from " + utilisateurCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurCreation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurCreation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.UTILISATEUR_CREATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.UTILISATEUR_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurModif() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOContrat.UTILISATEUR_MODIF_KEY);
  }
  
  public void setUtilisateurModif(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOContrat.UTILISATEUR_MODIF_KEY);
  }

  public void setUtilisateurModifRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating utilisateurModif from " + utilisateurModif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurModif(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurModif();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.UTILISATEUR_MODIF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.UTILISATEUR_MODIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurValidAdm() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(EOContrat.UTILISATEUR_VALID_ADM_KEY);
  }
  
  public void setUtilisateurValidAdm(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOContrat.UTILISATEUR_VALID_ADM_KEY);
  }

  public void setUtilisateurValidAdmRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("updating utilisateurValidAdm from " + utilisateurValidAdm() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurValidAdm(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateurValidAdm();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContrat.UTILISATEUR_VALID_ADM_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContrat.UTILISATEUR_VALID_ADM_KEY);
    }
  }
  
  public NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> avenants() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.Avenant>)storedValueForKey(EOContrat.AVENANTS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> avenants(EOQualifier qualifier) {
    return avenants(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> avenants(EOQualifier qualifier, boolean fetch) {
    return avenants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> avenants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.Avenant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.Avenant.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.Avenant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.Avenant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.Avenant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenants(org.cocktail.cocowork.server.metier.convention.Avenant object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.AVENANTS_KEY);
  }

  public void removeFromAvenants(org.cocktail.cocowork.server.metier.convention.Avenant object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.AVENANTS_KEY);
  }

  public void addToAvenantsRelationship(org.cocktail.cocowork.server.metier.convention.Avenant object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to avenants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenants(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.AVENANTS_KEY);
    }
  }

  public void removeFromAvenantsRelationship(org.cocktail.cocowork.server.metier.convention.Avenant object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from avenants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenants(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.AVENANTS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.Avenant createAvenantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.Avenant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.AVENANTS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.Avenant) eo;
  }

  public void deleteAvenantsRelationship(org.cocktail.cocowork.server.metier.convention.Avenant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.AVENANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.Avenant> objects = avenants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique> axeStrategiques() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique>)storedValueForKey(EOContrat.AXE_STRATEGIQUES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique> axeStrategiques(EOQualifier qualifier) {
    return axeStrategiques(qualifier, null);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique> axeStrategiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique> results;
      results = axeStrategiques();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.AxeStrategique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToAxeStrategiques(org.cocktail.cocowork.server.metier.convention.AxeStrategique object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.AXE_STRATEGIQUES_KEY);
  }

  public void removeFromAxeStrategiques(org.cocktail.cocowork.server.metier.convention.AxeStrategique object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.AXE_STRATEGIQUES_KEY);
  }

  public void addToAxeStrategiquesRelationship(org.cocktail.cocowork.server.metier.convention.AxeStrategique object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to axeStrategiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAxeStrategiques(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.AXE_STRATEGIQUES_KEY);
    }
  }

  public void removeFromAxeStrategiquesRelationship(org.cocktail.cocowork.server.metier.convention.AxeStrategique object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from axeStrategiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAxeStrategiques(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.AXE_STRATEGIQUES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.AxeStrategique createAxeStrategiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.AxeStrategique.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.AXE_STRATEGIQUES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.AxeStrategique) eo;
  }

  public void deleteAxeStrategiquesRelationship(org.cocktail.cocowork.server.metier.convention.AxeStrategique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.AXE_STRATEGIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAxeStrategiquesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.AxeStrategique> objects = axeStrategiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAxeStrategiquesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> brouillards() {
    return (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)storedValueForKey(EOContrat.BROUILLARDS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> brouillards(EOQualifier qualifier) {
    return brouillards(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> brouillards(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> results;
      results = brouillards();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToBrouillards(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.BROUILLARDS_KEY);
  }

  public void removeFromBrouillards(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.BROUILLARDS_KEY);
  }

  public void addToBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to brouillards relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToBrouillards(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.BROUILLARDS_KEY);
    }
  }

  public void removeFromBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from brouillards relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromBrouillards(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.BROUILLARDS_KEY);
    }
  }

  public org.cocktail.fwkcktlcompta.server.metier.EOBrouillard createBrouillardsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlcompta.server.metier.EOBrouillard.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.BROUILLARDS_KEY);
    return (org.cocktail.fwkcktlcompta.server.metier.EOBrouillard) eo;
  }

  public void deleteBrouillardsRelationship(org.cocktail.fwkcktlcompta.server.metier.EOBrouillard object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.BROUILLARDS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBrouillardsRelationships() {
    Enumeration<org.cocktail.fwkcktlcompta.server.metier.EOBrouillard> objects = brouillards().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBrouillardsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> contratPartenaires() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>)storedValueForKey(EOContrat.CONTRAT_PARTENAIRES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> contratPartenaires(EOQualifier qualifier) {
    return contratPartenaires(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> contratPartenaires(EOQualifier qualifier, boolean fetch) {
    return contratPartenaires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> contratPartenaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.ContratPartenaire.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.ContratPartenaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contratPartenaires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartenaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContratPartenaires(org.cocktail.cocowork.server.metier.convention.ContratPartenaire object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.CONTRAT_PARTENAIRES_KEY);
  }

  public void removeFromContratPartenaires(org.cocktail.cocowork.server.metier.convention.ContratPartenaire object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.CONTRAT_PARTENAIRES_KEY);
  }

  public void addToContratPartenairesRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to contratPartenaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToContratPartenaires(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.CONTRAT_PARTENAIRES_KEY);
    }
  }

  public void removeFromContratPartenairesRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from contratPartenaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromContratPartenaires(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.CONTRAT_PARTENAIRES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.ContratPartenaire createContratPartenairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.ContratPartenaire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.CONTRAT_PARTENAIRES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.ContratPartenaire) eo;
  }

  public void deleteContratPartenairesRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartenaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.CONTRAT_PARTENAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContratPartenairesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.ContratPartenaire> objects = contratPartenaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContratPartenairesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions() {
    return (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)storedValueForKey(EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions(EOQualifier qualifier) {
    return depenseControleConventions(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> depenseControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> results;
      results = depenseControleConventions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDepenseControleConventions(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromDepenseControleConventions(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
  }

  public void addToDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to depenseControleConventions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToDepenseControleConventions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
    }
  }

  public void removeFromDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from depenseControleConventions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromDepenseControleConventions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention createDepenseControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention) eo;
  }

  public void deleteDepenseControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.DEPENSE_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDepenseControleConventionsRelationships() {
    Enumeration<org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention> objects = depenseControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDepenseControleConventionsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions() {
    return (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)storedValueForKey(EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions(EOQualifier qualifier) {
    return engagementControleConventions(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> engagementControleConventions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> results;
      results = engagementControleConventions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEngagementControleConventions(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public void removeFromEngagementControleConventions(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
  }

  public void addToEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to engagementControleConventions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToEngagementControleConventions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    }
  }

  public void removeFromEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from engagementControleConventions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromEngagementControleConventions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention createEngagementControleConventionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    return (org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention) eo;
  }

  public void deleteEngagementControleConventionsRelationship(org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.ENGAGEMENT_CONTROLE_CONVENTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEngagementControleConventionsRelationships() {
    Enumeration<org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention> objects = engagementControleConventions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEngagementControleConventionsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> indicateursContrat() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat>)storedValueForKey(EOContrat.INDICATEURS_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> indicateursContrat(EOQualifier qualifier) {
    return indicateursContrat(qualifier, null);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> indicateursContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> results;
      results = indicateursContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.IndicateursContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToIndicateursContrat(org.cocktail.cocowork.server.metier.convention.IndicateursContrat object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.INDICATEURS_CONTRAT_KEY);
  }

  public void removeFromIndicateursContrat(org.cocktail.cocowork.server.metier.convention.IndicateursContrat object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.INDICATEURS_CONTRAT_KEY);
  }

  public void addToIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.IndicateursContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to indicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToIndicateursContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.INDICATEURS_CONTRAT_KEY);
    }
  }

  public void removeFromIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.IndicateursContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from indicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromIndicateursContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.INDICATEURS_CONTRAT_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.IndicateursContrat createIndicateursContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.IndicateursContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.INDICATEURS_CONTRAT_KEY);
    return (org.cocktail.cocowork.server.metier.convention.IndicateursContrat) eo;
  }

  public void deleteIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.IndicateursContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.INDICATEURS_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllIndicateursContratRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.IndicateursContrat> objects = indicateursContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteIndicateursContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.kava.server.metier.EOPrestation> prestations() {
    return (NSArray<org.cocktail.kava.server.metier.EOPrestation>)storedValueForKey(EOContrat.PRESTATIONS_KEY);
  }

  public NSArray<org.cocktail.kava.server.metier.EOPrestation> prestations(EOQualifier qualifier) {
    return prestations(qualifier, null);
  }

  public NSArray<org.cocktail.kava.server.metier.EOPrestation> prestations(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.kava.server.metier.EOPrestation> results;
      results = prestations();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.kava.server.metier.EOPrestation>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.kava.server.metier.EOPrestation>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPrestations(org.cocktail.kava.server.metier.EOPrestation object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.PRESTATIONS_KEY);
  }

  public void removeFromPrestations(org.cocktail.kava.server.metier.EOPrestation object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.PRESTATIONS_KEY);
  }

  public void addToPrestationsRelationship(org.cocktail.kava.server.metier.EOPrestation object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to prestations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToPrestations(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.PRESTATIONS_KEY);
    }
  }

  public void removeFromPrestationsRelationship(org.cocktail.kava.server.metier.EOPrestation object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from prestations relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromPrestations(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.PRESTATIONS_KEY);
    }
  }

  public org.cocktail.kava.server.metier.EOPrestation createPrestationsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.kava.server.metier.EOPrestation.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.PRESTATIONS_KEY);
    return (org.cocktail.kava.server.metier.EOPrestation) eo;
  }

  public void deletePrestationsRelationship(org.cocktail.kava.server.metier.EOPrestation object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.PRESTATIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrestationsRelationships() {
    Enumeration<org.cocktail.kava.server.metier.EOPrestation> objects = prestations().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrestationsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat> projetContrat() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat>)storedValueForKey(EOContrat.PROJET_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat> projetContrat(EOQualifier qualifier) {
    return projetContrat(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat> projetContrat(EOQualifier qualifier, boolean fetch) {
    return projetContrat(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat> projetContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.ProjetContrat.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.ProjetContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = projetContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ProjetContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToProjetContrat(org.cocktail.cocowork.server.metier.convention.ProjetContrat object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.PROJET_CONTRAT_KEY);
  }

  public void removeFromProjetContrat(org.cocktail.cocowork.server.metier.convention.ProjetContrat object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.PROJET_CONTRAT_KEY);
  }

  public void addToProjetContratRelationship(org.cocktail.cocowork.server.metier.convention.ProjetContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to projetContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToProjetContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.PROJET_CONTRAT_KEY);
    }
  }

  public void removeFromProjetContratRelationship(org.cocktail.cocowork.server.metier.convention.ProjetContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from projetContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromProjetContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.PROJET_CONTRAT_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.ProjetContrat createProjetContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.ProjetContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.PROJET_CONTRAT_KEY);
    return (org.cocktail.cocowork.server.metier.convention.ProjetContrat) eo;
  }

  public void deleteProjetContratRelationship(org.cocktail.cocowork.server.metier.convention.ProjetContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.PROJET_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllProjetContratRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.ProjetContrat> objects = projetContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteProjetContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> repartIndicateursContrat() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat>)storedValueForKey(EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier) {
    return repartIndicateursContrat(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier, boolean fetch) {
    return repartIndicateursContrat(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartIndicateursContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartIndicateursContrat(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public void removeFromRepartIndicateursContrat(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public void addToRepartIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to repartIndicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToRepartIndicateursContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
    }
  }

  public void removeFromRepartIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from repartIndicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromRepartIndicateursContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat createRepartIndicateursContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
    return (org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat) eo;
  }

  public void deleteRepartIndicateursContratRelationship(org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.REPART_INDICATEURS_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartIndicateursContratRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.RepartIndicateursContrat> objects = repartIndicateursContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartIndicateursContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Tranche> tranches() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.Tranche>)storedValueForKey(EOContrat.TRANCHES_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Tranche> tranches(EOQualifier qualifier) {
    return tranches(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Tranche> tranches(EOQualifier qualifier, boolean fetch) {
    return tranches(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.Tranche> tranches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.Tranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.Tranche.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.Tranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tranches();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.Tranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.Tranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTranches(org.cocktail.cocowork.server.metier.convention.Tranche object) {
    includeObjectIntoPropertyWithKey(object, EOContrat.TRANCHES_KEY);
  }

  public void removeFromTranches(org.cocktail.cocowork.server.metier.convention.Tranche object) {
    excludeObjectFromPropertyWithKey(object, EOContrat.TRANCHES_KEY);
  }

  public void addToTranchesRelationship(org.cocktail.cocowork.server.metier.convention.Tranche object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("adding " + object + " to tranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTranches(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContrat.TRANCHES_KEY);
    }
  }

  public void removeFromTranchesRelationship(org.cocktail.cocowork.server.metier.convention.Tranche object) {
    if (EOContrat.LOG.isDebugEnabled()) {
      EOContrat.LOG.debug("removing " + object + " from tranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTranches(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.TRANCHES_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.Tranche createTranchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.Tranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContrat.TRANCHES_KEY);
    return (org.cocktail.cocowork.server.metier.convention.Tranche) eo;
  }

  public void deleteTranchesRelationship(org.cocktail.cocowork.server.metier.convention.Tranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContrat.TRANCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTranchesRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.Tranche> objects = tranches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTranchesRelationship(objects.nextElement());
    }
  }


  public static Contrat create(EOEditingContext editingContext, String conObjet
, Integer conOrdre
, org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail exerciceCocktail, org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateurCreation) {
    Contrat eo = (Contrat) EOUtilities.createAndInsertInstance(editingContext, EOContrat.ENTITY_NAME);    
        eo.setConObjet(conObjet);
        eo.setConOrdre(conOrdre);
    eo.setExerciceCocktailRelationship(exerciceCocktail);
    eo.setUtilisateurCreationRelationship(utilisateurCreation);
    return eo;
  }

  public static ERXFetchSpecification<Contrat> fetchSpec() {
    return new ERXFetchSpecification<Contrat>(EOContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Contrat> fetchAll(EOEditingContext editingContext) {
    return EOContrat.fetchAll(editingContext, null);
  }

  public static NSArray<Contrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Contrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Contrat> fetchSpec = new ERXFetchSpecification<Contrat>(EOContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Contrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Contrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Contrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Contrat> eoObjects = EOContrat.fetchAll(editingContext, qualifier, null);
    Contrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Contrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Contrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Contrat eoObject = EOContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Contrat localInstanceIn(EOEditingContext editingContext, Contrat eo) {
    Contrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}