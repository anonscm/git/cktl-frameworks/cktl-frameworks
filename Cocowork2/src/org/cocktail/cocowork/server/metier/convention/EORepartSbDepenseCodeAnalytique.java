// DO NOT EDIT.  Make changes to RepartSbDepenseCodeAnalytique.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartSbDepenseCodeAnalytique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWRepartSbDepenseCodeAnalytique";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT_HT = new ERXKey<java.math.BigDecimal>("montantHt");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique> CODE_ANALYTIQUE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique>("codeAnalytique");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.SbDepense> SB_DEPENSE = new ERXKey<org.cocktail.cocowork.server.metier.convention.SbDepense>("sbDepense");

  // Attributes
  public static final String MONTANT_HT_KEY = MONTANT_HT.key();
  // Relationships
  public static final String CODE_ANALYTIQUE_KEY = CODE_ANALYTIQUE.key();
  public static final String SB_DEPENSE_KEY = SB_DEPENSE.key();

  private static Logger LOG = Logger.getLogger(EORepartSbDepenseCodeAnalytique.class);

  public RepartSbDepenseCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
    RepartSbDepenseCodeAnalytique localInstance = (RepartSbDepenseCodeAnalytique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montantHt() {
    return (java.math.BigDecimal) storedValueForKey(EORepartSbDepenseCodeAnalytique.MONTANT_HT_KEY);
  }

  public void setMontantHt(java.math.BigDecimal value) {
    if (EORepartSbDepenseCodeAnalytique.LOG.isDebugEnabled()) {
        EORepartSbDepenseCodeAnalytique.LOG.debug( "updating montantHt from " + montantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartSbDepenseCodeAnalytique.MONTANT_HT_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique)storedValueForKey(EORepartSbDepenseCodeAnalytique.CODE_ANALYTIQUE_KEY);
  }
  
  public void setCodeAnalytique(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique value) {
    takeStoredValueForKey(value, EORepartSbDepenseCodeAnalytique.CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique value) {
    if (EORepartSbDepenseCodeAnalytique.LOG.isDebugEnabled()) {
      EORepartSbDepenseCodeAnalytique.LOG.debug("updating codeAnalytique from " + codeAnalytique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCodeAnalytique(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique oldValue = codeAnalytique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbDepenseCodeAnalytique.CODE_ANALYTIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbDepenseCodeAnalytique.CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.SbDepense sbDepense() {
    return (org.cocktail.cocowork.server.metier.convention.SbDepense)storedValueForKey(EORepartSbDepenseCodeAnalytique.SB_DEPENSE_KEY);
  }
  
  public void setSbDepense(org.cocktail.cocowork.server.metier.convention.SbDepense value) {
    takeStoredValueForKey(value, EORepartSbDepenseCodeAnalytique.SB_DEPENSE_KEY);
  }

  public void setSbDepenseRelationship(org.cocktail.cocowork.server.metier.convention.SbDepense value) {
    if (EORepartSbDepenseCodeAnalytique.LOG.isDebugEnabled()) {
      EORepartSbDepenseCodeAnalytique.LOG.debug("updating sbDepense from " + sbDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSbDepense(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.SbDepense oldValue = sbDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbDepenseCodeAnalytique.SB_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbDepenseCodeAnalytique.SB_DEPENSE_KEY);
    }
  }
  

  public static RepartSbDepenseCodeAnalytique create(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique codeAnalytique, org.cocktail.cocowork.server.metier.convention.SbDepense sbDepense) {
    RepartSbDepenseCodeAnalytique eo = (RepartSbDepenseCodeAnalytique) EOUtilities.createAndInsertInstance(editingContext, EORepartSbDepenseCodeAnalytique.ENTITY_NAME);    
    eo.setCodeAnalytiqueRelationship(codeAnalytique);
    eo.setSbDepenseRelationship(sbDepense);
    return eo;
  }

  public static ERXFetchSpecification<RepartSbDepenseCodeAnalytique> fetchSpec() {
    return new ERXFetchSpecification<RepartSbDepenseCodeAnalytique>(EORepartSbDepenseCodeAnalytique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartSbDepenseCodeAnalytique> fetchAll(EOEditingContext editingContext) {
    return EORepartSbDepenseCodeAnalytique.fetchAll(editingContext, null);
  }

  public static NSArray<RepartSbDepenseCodeAnalytique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartSbDepenseCodeAnalytique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartSbDepenseCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartSbDepenseCodeAnalytique> fetchSpec = new ERXFetchSpecification<RepartSbDepenseCodeAnalytique>(EORepartSbDepenseCodeAnalytique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartSbDepenseCodeAnalytique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartSbDepenseCodeAnalytique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbDepenseCodeAnalytique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbDepenseCodeAnalytique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartSbDepenseCodeAnalytique> eoObjects = EORepartSbDepenseCodeAnalytique.fetchAll(editingContext, qualifier, null);
    RepartSbDepenseCodeAnalytique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWRepartSbDepenseCodeAnalytique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbDepenseCodeAnalytique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbDepenseCodeAnalytique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbDepenseCodeAnalytique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartSbDepenseCodeAnalytique eoObject = EORepartSbDepenseCodeAnalytique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWRepartSbDepenseCodeAnalytique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbDepenseCodeAnalytique localInstanceIn(EOEditingContext editingContext, RepartSbDepenseCodeAnalytique eo) {
    RepartSbDepenseCodeAnalytique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}