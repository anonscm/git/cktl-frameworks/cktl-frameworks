/**
 * Cocowork
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.server.metier.convention.finder.core;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.cocowork.server.metier.convention.TypeContact;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderTypeContact extends Finder 
{
	protected EOQualifier tcLibelleCourtQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec
	 * @param entityName
	 */
	public FinderTypeContact(EOEditingContext ec) {
		super(ec, TypeContact.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du critere.
	 * @param tcLibelleCourt Id interne du type de contact.
	 */
	public void setTcLibelleCourt(final String tcLibelleCourt) {
		this.tcLibelleCourtQualifier = createQualifier(
				"tcLibelleCourt = %@", 
				tcLibelleCourt);
	}
	
	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#clearAllCriteria()
	 */
	public void clearAllCriteria() {
		this.tcLibelleCourtQualifier = null;
	}

	
	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray find() throws ExceptionFinder {
		addOptionalQualifier(this.tcLibelleCourtQualifier);
		clearAllCriteria();
		
		return super.find();
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
