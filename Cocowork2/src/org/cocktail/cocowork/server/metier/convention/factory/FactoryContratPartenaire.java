/**
 * 
 */
package org.cocktail.cocowork.server.metier.convention.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.cocowork.common.exception.ExceptionArgument;
import org.cocktail.cocowork.common.exception.ExceptionUtilisateur;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.EOContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Michael HALLER, Consortium Cocktail, 2008
 * 
 */
public class FactoryContratPartenaire extends Factory {
	final static public Integer STRATEGIE_MONTANT_APPORTE_REMPLACER = new Integer(0);
	final static public Integer STRATEGIE_MONTANT_APPORTE_AJOUTER = new Integer(1);

	/**
	 * @param ec
	 * @param withtrace
	 */
	public FactoryContratPartenaire(EOEditingContext ec, Boolean withtrace) {
		super(ec, withtrace.booleanValue());

	}

	/**
	 * Creation d'un nouveau partenariat (objet ContratPartenaire)
	 * 
	 * @param contrat
	 *            Contrat concerne.
	 * @param personne
	 *            Personne physique ou morale partenaire
	 * @param typePartenaire
	 *            Type du partenariat
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public ContratPartenaire creerContratPartenaire(final Contrat contrat, final IPersonne personne, final NSArray roles, final Boolean partenairePrincipal,
			final BigDecimal montantApporte) throws ExceptionUtilisateur, Exception {
		return creerContratPartenaire(contrat, personne, roles, null,// dateDebut,
				null,// dateFin,
				partenairePrincipal, montantApporte, null); // rasCommentaire
	}
	
	/**
	 * Creation d'un nouveau partenariat (objet ContratPartenaire)
	 * 
	 * @param contrat
	 *            Contrat concerne.
	 * @param personne
	 *            Personne physique ou morale partenaire
	 * @param typePartenaire
	 *            Type du partenariat
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public ContratPartenaire creerContratPartenaire(final Contrat contrat, final IPersonne personne, final NSArray roles, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final String rasCommentaire) throws ExceptionUtilisateur, Exception {
		return creerContratPartenaire(contrat, personne, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, rasCommentaire, null);
	}
	
	/**
	 * Creation d'un nouveau partenariat (objet ContratPartenaire)
	 * 
	 * @param contrat
	 *            Contrat concerne.
	 * @param personne
	 *            Personne physique ou morale partenaire
	 * @param typePartenaire
	 *            Type du partenariat
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @param rasQuotite la quotite du repart association
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public ContratPartenaire creerContratPartenaire(final Contrat contrat, final IPersonne personne, final NSArray roles, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final String rasCommentaire, final BigDecimal rasQuotite) throws ExceptionUtilisateur, Exception {

		trace("creerContratPartenaire()");
		// trace(contrat);
		// trace(personne);
		trace(roles);
		trace(partenairePrincipal);
		trace(montantApporte);

		if (contrat == null)
			throw new ExceptionUtilisateur("Le contrat doit \u00EAtre fourni pour cr\u00E9er un partenariat.");
		if (personne == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour cr\u00E9er un partenariat.");

		
		NSArray<ContratPartenaire> contratPartenaires = contrat.contratPartenaires(ContratPartenaire.CONTRAT.eq(contrat).and(ContratPartenaire.PERS_ID.eq(personne.persId())));
		ContratPartenaire contratPartenaire = null;
		if(contratPartenaires.isEmpty()) {
			contratPartenaire = (ContratPartenaire) Factory.instanceForEntity(ec, EOContratPartenaire.ENTITY_NAME);
		} else {
			contratPartenaire = contratPartenaires.get(0);
		}

		ec.insertObject(contratPartenaire);

		modifierContratPartenaire(contratPartenaire, contrat, personne, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, STRATEGIE_MONTANT_APPORTE_REMPLACER,
				rasCommentaire, rasQuotite);

		NSArray tranches = contrat.tranches();
		Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
		while (enumTranches.hasMoreElements()) {
			Tranche tranche = (Tranche) enumTranches.nextElement();
			RepartPartenaireTranche rpt = RepartPartenaireTranche.instanciate(ec);
			ec.insertObject(rpt);
			rpt.setRptMontantParticipation(BigDecimal.ZERO);
			rpt.setRptTauxParticipation(BigDecimal.ZERO);
			contratPartenaire.addToContributionsRelationship(rpt);
			tranche.addToToRepartPartenaireTranchesRelationship(rpt);
		}

		return contratPartenaire;
	}

	/**
	 * Modif d'un partenariat.
	 * 
	 * @param contratPartenaire
	 *            Partenariat a modifier.
	 * @param contrat
	 *            Contrat concerne.
	 * @param partenaire
	 *            Personne physique ou morale partenaire
	 * @param roles
	 *            Liste des roles du partenaire
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte
	 *            Strategie a appliquer concernant le montant apporte par le
	 *            partenaire : remplacer l'ancien montant par celui specifie
	 *            (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER}
	 *            ) ; augmenter le monatnt existant de celui sepcifie (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER}
	 *            ).
	 * @throws ExceptionUtilisateur
	 *             ,Exception
	 * @throws ExceptionArgument
	 *             Strategie
	 */
	public void modifierContratPartenaire(final ContratPartenaire contratPartenaire, final Contrat contrat, final IPersonne partenaire, final NSArray roles,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte) throws ExceptionUtilisateur, Exception {

		modifierContratPartenaire(contratPartenaire, contrat, partenaire, roles, null, null, partenairePrincipal, montantApporte, strategiePourMontantApporte, null);
	}
	
	/**
	 * Modif d'un partenariat.
	 * 
	 * @param contratPartenaire
	 *            Partenariat a modifier.
	 * @param contrat
	 *            Contrat concerne.
	 * @param partenaire
	 *            Personne physique ou morale partenaire
	 * @param roles
	 *            Liste des roles du partenaire
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte
	 *            Strategie a appliquer concernant le montant apporte par le
	 *            partenaire : remplacer l'ancien montant par celui specifie
	 *            (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER}
	 *            ) ; augmenter le monatnt existant de celui sepcifie (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER}
	 *            ).
	 * @throws ExceptionUtilisateur
	 *             ,Exception
	 * @throws ExceptionArgument
	 *             Strategie
	 */
	public void modifierContratPartenaire(final ContratPartenaire contratPartenaire, final Contrat contrat, final IPersonne partenaire, final NSArray roles,
			final NSTimestamp dateDebut, final NSTimestamp dateFin, final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte,
			final String rasCommentaire) throws ExceptionUtilisateur, Exception {
		modifierContratPartenaire(contratPartenaire, contrat, partenaire, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, strategiePourMontantApporte, rasCommentaire, null);
	}
	
	/**
	 * Modif d'un partenariat.
	 * 
	 * @param contratPartenaire
	 *            Partenariat a modifier.
	 * @param contrat
	 *            Contrat concerne.
	 * @param partenaire
	 *            Personne physique ou morale partenaire
	 * @param roles
	 *            Liste des roles du partenaire
	 * @param partenairePrincipal
	 *            <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte
	 *            Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte
	 *            Strategie a appliquer concernant le montant apporte par le
	 *            partenaire : remplacer l'ancien montant par celui specifie
	 *            (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER}
	 *            ) ; augmenter le monatnt existant de celui sepcifie (utiliser
	 *            {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER}
	 *            ).
	 * @param rasQuotite
	 * 			  La quotite de la repart association
	 * @throws ExceptionUtilisateur
	 *             ,Exception
	 * @throws ExceptionArgument
	 *             Strategie
	 */
	public void modifierContratPartenaire(final ContratPartenaire contratPartenaire, final Contrat contrat, final IPersonne partenaire, final NSArray roles,
			final NSTimestamp dateDebut, final NSTimestamp dateFin, final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte,
			final String rasCommentaire, final BigDecimal rasQuotite) throws ExceptionUtilisateur, Exception {

		trace("modifierContratPartenaire()");
		// trace(contratPartenaire);
		// trace(contrat);
		// trace(partenaire);
		// trace(roles);
		// trace(partenairePrincipal);
		// trace(montantApporte);
		// trace(strategiePourMontantApporte);
		partenaire.persId();
		if (contratPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat doit \u00EAtre fourni pour modifier un partenariat.");
		if (contrat == null)
			throw new ExceptionUtilisateur("Le contrat doit \u00EAtre fourni pour modifier un partenariat.");
		if (partenaire == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour modifier un partenariat.");
		if (strategiePourMontantApporte == null)
			throw new ExceptionUtilisateur("La strat\u00E9gie pour le montant apport\u00E9 doit \u00EAtre fournie.");
		if (!strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_REMPLACER) && !strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_AJOUTER))
			throw new ExceptionUtilisateur("Strat\u00E9gie inconnue pour le montant apport\u00E9.");

		contratPartenaire.setContratRelationship(contrat);
		contratPartenaire.setPartenaire(partenaire);
		// contratPartenaire.setTypePartenaireRelationship(typePartenaire);

		contratPartenaire.setCpPrincipalBoolean(partenairePrincipal != null ? partenairePrincipal.booleanValue() : false);
		Integer persid = null;
		if (contrat.utilisateurModif() != null)
			persid = contrat.utilisateurModif().toPersonne().persId();
		else
			persid = contrat.utilisateurCreation().toPersonne().persId();
		EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec, partenaire, contrat.groupePartenaire(), persid, true);
		if (roles != null) {
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation association = (EOAssociation) enumRoles.nextElement();

				NSTimestamp aDebut = dateDebut;
				NSTimestamp aFin = dateFin;
				if (aDebut == null || aFin == null) {
					aDebut = contrat.dateDebut();
					aFin = contrat.dateFin();
				} else {
					if (dateDebut.before(contrat.dateDebut())) {
						aDebut = contrat.dateDebut();
					}
					if (aFin.after(contrat.dateFin())) {
						aFin = contrat.dateFin();
					}
				}

				partenaire.definitUnRole(ec, association, contrat.groupePartenaire(), persid, aDebut, aFin, rasCommentaire, rasQuotite, null, true);
			}
		}
		BigDecimal nouveauMontant = null;

		if (strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_REMPLACER))
			nouveauMontant = montantApporte;
		else if (strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_AJOUTER))
			nouveauMontant = contratPartenaire.cpMontant().add(montantApporte);

		contratPartenaire.setCpMontant(nouveauMontant);
	}

	/**
	 * Suppression d'un partenariat.
	 * 
	 * @param contratPartenaire  Partenariat a supprimer.
	 * @param persIdUtilisateur persId de la personne qui supprime
	 * @throws Exception exception
	 * @throws ExceptionUtilisateur exception
	 */
	public void supprimerContratPartenaire(final ContratPartenaire contratPartenaire, Integer persIdUtilisateur) throws Exception, ExceptionUtilisateur {

		if (contratPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat à supprimer n'est pas renseigne");

		trace("supprimerContratPartenaire()");
		trace(contratPartenaire);

		EOEditingContext edc = contratPartenaire.editingContext();
		NSMutableArray persIds = new NSMutableArray();
		// Eliminer les contacts du partenaires
		NSArray contacts = contratPartenaire.contratPartContacts().immutableClone();
		if (contacts != null && contacts.count() > 0) {
			try {
				Enumeration enumContacts = contacts.objectEnumerator();
				while (enumContacts.hasMoreElements()) {
					ContratPartContact unContact = (ContratPartContact) enumContacts.nextElement();
					persIds.addObject(unContact.persIdContact());
					contratPartenaire.removeObjectFromBothSidesOfRelationshipWithKey(unContact, "contratPartContacts");
					edc.deleteObject(unContact);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Suppression des repartPartenaireTranches 
		NSArray contributions = contratPartenaire.contributions().immutableClone();
		Enumeration<RepartPartenaireTranche> enumContributions = contributions.objectEnumerator();
		while (enumContributions.hasMoreElements()) {
			RepartPartenaireTranche repartPartenaireTranche = (RepartPartenaireTranche) enumContributions.nextElement();
			contratPartenaire.removeFromContributionsRelationship(repartPartenaireTranche);
			ec.deleteObject(repartPartenaireTranche);
		}
		// NSArray tranches = contratPartenaire.contrat().tranches();
		// Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
		// while (enumTranches.hasMoreElements()) {
		// Tranche tranche = (Tranche) enumTranches.nextElement();
		// NSArray rpt = tranche.toRepartPartenaireTranches();
		// EOKeyValueQualifier qualifier = new
		// EOKeyValueQualifier(RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY,EOQualifier.QualifierOperatorEqual,contratPartenaire);
		// rpt = EOQualifier.filteredArrayWithQualifier(rpt, qualifier);
		// Enumeration<RepartPartenaireTranche> enumRpt =
		// rpt.immutableClone().objectEnumerator();
		// while (enumRpt.hasMoreElements()) {
		// RepartPartenaireTranche repartPartenaireTranche =
		// (RepartPartenaireTranche) enumRpt.nextElement();
		// tranche.removeObjectFromBothSidesOfRelationshipWithKey(repartPartenaireTranche,
		// Tranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
		// ec.deleteObject(repartPartenaireTranche);
		// }
		// }

		// Eliminer le partenaire du groupe des partenaires
		Contrat contrat = contratPartenaire.contrat();
		// if (contratPartenaire.cpPrincipalBoolean()) {
		// contrat.setEtablissementRelationship(null);
		// }
		EOStructure groupe = contrat.groupePartenaire();
		EOStructureForGroupeSpec.supprimerPersonneDuGroupe(contrat.editingContext(), contratPartenaire.partenaire(), groupe, persIdUtilisateur);

		Integer persid = null;
		if (contrat.utilisateurModif() != null)
			persid = contrat.utilisateurModif().toPersonne().persId();
		else
			persid = contrat.utilisateurCreation().toPersonne().persId();

		persIds.addObject(contratPartenaire.persId());
		contrat.removeObjectFromBothSidesOfRelationshipWithKey(contratPartenaire, "contratPartenaires");
		edc.deleteObject(contratPartenaire);

		if (contratPartenaire.partenaire().globalID().isTemporary()) {
			edc.forgetObject(contratPartenaire.partenaire());
		}

	}

	public void ajouterContact(ContratPartenaire contratPartenaire, IPersonne personne, NSArray roles) throws ExceptionUtilisateur, Exception {

		if (contratPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat auquel vous tentez d'ajouter un partenaire n'est pas renseigne");
		if (personne == null)
			throw new ExceptionUtilisateur("Le contact n'est pas renseigne");
		// if(typeContact==null)
		// throw new Exception("Il faut un type de contact");

		IPersonne partenaire = contratPartenaire.partenaire();
//		JB : un stagiaire de FC est contact d'un contratPartenaire qui peut etre une personne physique
//		if (partenaire.isStructure()) {
			Contrat contrat = contratPartenaire.contrat();

			ContratPartContact cpc = (ContratPartContact) Factory.instanceForEntity(ec, ContratPartContact.ENTITY_NAME);

			ec.insertObject(cpc);

			cpc.setContratPartenaireRelationship(contratPartenaire);
			cpc.setPersonne(personne);
			cpc.setPartenaire(partenaire);
			// cpc.setTypeContact(typeContact);

			Integer persid = null;
			if (contrat.utilisateurModif() != null)
				persid = contrat.utilisateurModif().toPersonne().persId();
			else
				persid = contrat.utilisateurCreation().toPersonne().persId();

			contratPartenaire.addToContratPartContactsRelationship(cpc);
			EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec, personne, contrat.groupePartenaire(), persid, true);

			if (roles != null) {
				Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
				while (enumRoles.hasMoreElements()) {
					EOAssociation association = (EOAssociation) enumRoles.nextElement();
					personne.definitUnRole(ec, association, contrat.groupePartenaire(), persid, contrat.dateDebut(), contrat.dateFin(), null, null, null, true);
				}
			}
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(ec, "edc");
			NSNotificationCenter.defaultCenter().postNotification("refreshContactsNotification", cpc, userInfo);

//		} else
//			throw new ExceptionUtilisateur("Seules les personnes morales peuvent avoir des contacts");

	}

	/**
	 * Suppression d'un contact.
	 * 
	 * @param contact ContratPartContact a supprimer.
	 * @param partenaire ContratPartenaire a supprimer.
	 * @param persIdUtilisateur persId de la personne qui supprime le contact
	 * @throws Exception exception
	 * @throws ExceptionUtilisateur exception
	 */
	public void supprimerContact(final ContratPartContact contact, ContratPartenaire partenaire, Integer persIdUtilisateur) throws Exception, ExceptionUtilisateur {

		if (contact == null)
			throw new ExceptionUtilisateur("Le contact doit \u00EAtre fourni pour le supprimer.");
		if (partenaire == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour supprimer le contact.");

		trace("supprimerContratPartContact()");
		trace(contact);

		// Eliminer le Contact du groupe des partenaires
		EOStructureForGroupeSpec.supprimerPersonneDuGroupe(ec, contact.personne(), partenaire.contrat().groupePartenaire(), persIdUtilisateur);
		if (contact.personne().equals(partenaire.contrat().centreResponsabilite())) {
			partenaire.contrat().setCentreResponsabiliteRelationship(null, persIdUtilisateur);
		}
		partenaire.removeFromContratPartContactsRelationship(contact);

		ec.deleteObject(contact);

		NSMutableDictionary userInfo = new NSMutableDictionary();
		userInfo.setObjectForKey(ec, "edc");
		NSNotificationCenter.defaultCenter().postNotification("refreshContactsNotification", null, userInfo);
	}

	/**
	 * Renvoie un Boolean pour indiquer si un partenaire joue un role dans le
	 * groupe des partenaires associe au contrat
	 * 
	 * @param partenaire
	 * @param role
	 * @param contrat
	 * @return
	 */
	public Boolean partenaireARoleDansContrat(IPersonne partenaire, EOAssociation role, Contrat contrat) {
		NSArray repartAssociations = partenaire.getRepartAssociationsInGroupe(contrat.groupePartenaire(), null);
		if (repartAssociations == null)
			return Boolean.FALSE;
		if (repartAssociations.isEmpty())
			return Boolean.FALSE;
		Enumeration<EORepartAssociation> enumRepartAssociations = repartAssociations.objectEnumerator();
		while (enumRepartAssociations.hasMoreElements()) {
			EORepartAssociation repartAssociation = enumRepartAssociations.nextElement();
			if (repartAssociation.toAssociation().equals(role))
				return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	protected boolean isAssociationsInRepartAssociation(EOAssociation role, EORepartAssociation repartAssociation) {
		return repartAssociation.toAssociation().equals(role);
	}

	/**
	 * Renvoie un Boolean pour indiquer si un partenaire joue un des roles dans
	 * le groupe des partenaires associe au contrat
	 * 
	 * @param partenaire
	 * @param role
	 * @param contrat
	 * @return
	 */
	public Boolean partenaireARolesDansContrat(IPersonne partenaire, NSArray<EOAssociation> roles, Contrat contrat) {
		NSArray repartAssociations = partenaire.getRepartAssociationsInGroupe(contrat.groupePartenaire(), null);
		if (repartAssociations == null)
			return Boolean.FALSE;
		if (repartAssociations.isEmpty())
			return Boolean.FALSE;
		Enumeration<EORepartAssociation> enumRepartAssociations = repartAssociations.objectEnumerator();
		while (enumRepartAssociations.hasMoreElements()) {
			EORepartAssociation repartAssociation = enumRepartAssociations.nextElement();
			Enumeration<EOAssociation> lesRoles = roles.objectEnumerator();
			while (lesRoles.hasMoreElements()) {
				if (isAssociationsInRepartAssociation(lesRoles.nextElement(), repartAssociation))
					return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Ajout d'un role a un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void ajouterLeRole(ContratPartenaire partenaire, EOAssociation role) {
		if (partenaire != null && role != null) {
			ajouterLesRoles(partenaire, new NSArray(role));
		}
	}

	public void ajouterLeRolePourLesDates(ContratPartenaire partenaire, EOAssociation role, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (partenaire != null && role != null) {
			ajouterLesRolesPourLesDates(partenaire, new NSArray(role), dateDebut, dateFin);
		}
	}

	public void ajouterLeRolePourLesDatesEtQuotite(ContratPartenaire partenaire, EOAssociation role, NSTimestamp dateDebut, NSTimestamp dateFin, BigDecimal quotite) {
		if (partenaire != null && role != null) {
			ajouterLesRolesPourLesDatesEtQuotite(partenaire, new NSArray(role), dateDebut, dateFin, quotite);
		}
	}

	/**
	 * Ajout de roles a un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void ajouterLesRoles(ContratPartenaire partenaire, NSArray roles) {
		if (partenaire != null && roles != null) {
			Contrat contrat = partenaire.contrat();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, contrat.groupePartenaire(), contrat.utilisateurModif().toPersonne().persId(), contrat.dateDebut(),
						contrat.dateFin(), null, null, null, true);
			}
		}
	}

	public void ajouterLesRolesPourLesDates(ContratPartenaire partenaire, NSArray roles, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (partenaire != null && roles != null) {
			Contrat contrat = partenaire.contrat();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, contrat.groupePartenaire(), contrat.utilisateurModif().toPersonne().persId(), dateDebut, dateFin, null,
						null, null, true);
			}
		}
	}

	public void ajouterLesRolesPourLesDatesEtQuotite(ContratPartenaire partenaire, NSArray roles, NSTimestamp dateDebut, NSTimestamp dateFin, BigDecimal quotite) {
		if (partenaire != null && roles != null) {
			Contrat contrat = partenaire.contrat();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, contrat.groupePartenaire(), contrat.utilisateurModif().toPersonne().persId(), dateDebut, dateFin, null,
						quotite, null, true);
			}
		}
	}

	/**
	 * Suppression du role d'un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param unRole
	 */
	public void supprimerLeRole(ContratPartenaire partenaire, EOAssociation unRole) {
		supprimerLesRoles(partenaire, new NSArray(unRole));
	}

	/**
	 * Suppression des roles d'un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void supprimerLesRoles(ContratPartenaire partenaire, NSArray roles) {
		if (partenaire != null && roles != null) {
			Contrat contrat = partenaire.contrat();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.supprimerLesRoles(ec, unRole, contrat.groupePartenaire(), contrat.utilisateurModif().toPersonne().persId());
			}
		}
	}

	/**
	 * Suppression du roles d'un contact d'un partenaire
	 * 
	 * @param partenaire
	 * @param unRole
	 */
	public void supprimerLeRoleContact(ContratPartenaire partenaire, ContratPartContact cpc, EOAssociation unRole) {
		supprimerLesRolesContact(partenaire, cpc, new NSArray(unRole));
	}

	/**
	 * Suppression des roles d'un contact d'un partenaire
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void supprimerLesRolesContact(ContratPartenaire partenaire, ContratPartContact cpc, NSArray roles) {
		if (partenaire != null && cpc != null && roles != null) {
			Contrat contrat = partenaire.contrat();
			IPersonne personne = cpc.personne();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.supprimerLesRoles(ec, unRole, contrat.groupePartenaire(), contrat.utilisateurModif().toPersonne().persId());
			}
		}
	}

}
