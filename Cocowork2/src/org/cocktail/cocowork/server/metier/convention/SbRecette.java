

// SbRecette.java
// 
package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.util.Calendar;

import org.cocktail.cocowork.common.date.DateOperation;
import org.cocktail.cocowork.common.nombre.NombreOperation;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetParametre;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;



public class SbRecette extends EOSbRecette implements Budgetable
{

    public SbRecette() {
        super();
    }

	public static SbRecette instanciate(EOEditingContext ec) throws Exception {
		SbRecette recette = (SbRecette) Factory.instanceForEntity(ec, ENTITY_NAME);
		return recette;
	}
	
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setSrMontantHt(BigDecimal.ZERO);
	}
	
	@Override
	public void setSrLibelle(String aValue) {
		if (aValue != null && aValue.length()>50) {
			aValue = aValue.substring(0,50);
		}
		super.setSrLibelle(aValue);
	}
	
	@Override
	public String srLibelle() {
		return super.srLibelle();
	}
	
	@Override
	public void setTypeCreditRelationship(EOTypeCredit value) {
	    if(value == null || !value.equals(typeCredit())) {
	        // Nettoyage des relations lolf et reparts
	        setLolfRelationship(null);
	        super.setTypeCreditRelationship(value);
	    }
	}
	
	@Override
	public void validateForSave() throws ValidationException {
	    super.validateForSave();
	       // Vérifier le CR sélectionné en fonction du mode de gestion de la conv
        if (!tranche().contrat().isModeRA()) {
            if (organ().isTypeRA())
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Budgetable.ORGAN_KEY, organ(), "ConvSimpleWithBadOrgan"); 
        } else if (tranche().contrat().isModeRA()) {
            if (!organ().isTypeRA())
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Budgetable.ORGAN_KEY, organ(), "ConvRaWithBadOrgan");
            // On check que cet organ ne soit pas utilisé par d'autres conventions
            NSArray<Contrat> contratsUtilisantOrgan = tranche().contrat().autresContratsUtilisantOrgan(tranche().exerciceCocktail().exercice(), organ());
            if (!contratsUtilisantOrgan.isEmpty()) {
                   final NSArray<String> libellesContrat = (NSArray<String>)contratsUtilisantOrgan.valueForKey(Contrat.NUMERO_CONTRAT_KEY);
                   NSDictionary<String, String> stringContainer = new NSDictionary<String, String>(libellesContrat.componentsJoinedByString(", "), "libellesContrat");
                   String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("OrganDejaUtilise", stringContainer);
                   throw new ValidationException(msg);
            }
        }
        // Si la somme est supérieur au montant de la tranche (avec frais de gestion inclus car on rajoute ici les recettes
        // avec frais de gestion inclus...)

		if (lolf() != null) { // Destination
			if (tranche().isMontantTrancheLtSommeRecettesDest()) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, SR_MONTANT_HT_KEY, srMontantHt(), "MontantRecDestTooMuch");
			}
		} else if (planco() != null) { // Nature
			if (tranche().isMontantTrancheLtSommeRecettesNature()) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, SR_MONTANT_HT_KEY, srMontantHt(), "MontantRecNatureTooMuch");
			}
		}

        // Dans le cas d'une recette par nature et que la somme des recette avec ce planco est inférieure au positionne
        // TODO : A Decommenter lorsque le positionne en recette sera fait
        checkMontantAvecCreditsPositionnes(null);
	    if (srDatePrev() != null) {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(srDatePrev());
	        int year = cal.get(Calendar.YEAR);
	        if (year > tranche().exerciceCocktail().exeExercice())
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, SR_DATE_PREV_KEY, srDatePrev(), "DatePrevAfterExo");
	    }
	    if (lolf() == null && planco() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, LOLF_KEY, null, "lolf.ou.nature");
        if (lolf() != null && planco() != null)
               throw ERXValidationFactory.defaultFactory().createCustomException(this, LOLF_KEY, null, "lolf.ou.nature");
	}
	
	public void checkMontantAvecCreditsPositionnes(BigDecimal montantASoustraire) throws ValidationException {
	    VRecettesTranche recettesTranche = recettesTranche();
	    if (planco() != null && recettesTranche != null) {
            BigDecimal totalPositionneCorrespondant = recettesTranche.montantCreditsPositionnes();
            BigDecimal sommeRecettes = sommeRecettesAvecMemePlanco();
            if (montantASoustraire != null) {
                sommeRecettes = sommeRecettes.subtract(montantASoustraire);
            }
            if (sommeRecettes.compareTo(totalPositionneCorrespondant) < 0) {
                String total = new NombreOperation().getFormatterMoney().format(totalPositionneCorrespondant);
                NSDictionary<String, String> stringContainer = new NSDictionary<String, String>(total, "totalPositionne");
                String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("MontantInferieurPositionneRecette", stringContainer);
                throw new ValidationException(msg);
            }
        }
	}
	
	public BigDecimal sommeRecettesAvecMemePlanco() {
	    NSArray<SbRecette> recettes = tranche().sbRecettes(PLANCO.eq(planco()));
	    return (BigDecimal)recettes.valueForKey("@sum."+SR_MONTANT_HT_KEY);
	}
	
	public VRecettesTranche recettesTranche() {
	    EOQualifier qual = TRANCHE.eq(tranche()).and(PLANCO.eq(planco()));
	    return VRecettesTranche.fetch(editingContext(), qual);
	}
	
	public void setDefaultValues() {
	    if (tranche() != null) {
	        String objet = tranche().contrat().conObjet();
	        String srLibelle = objet != null && objet.length() > 50 ? objet.substring(0, 50) : objet;
	        setSrLibelle(srLibelle);
	        // date de la dépense par défaut = date de fin de la tranche
	        int annee = tranche().exerciceCocktail().exeExercice();
	        // fin d'année
	        setSrDatePrev(DateOperation.lastTimestampOfYear(annee));
            // Si mode RA, on rajoute
            if (tranche().contrat().isModeRA()) {
                EOOrgan organ = tranche().contrat().organComposante();
                setOrganRelationship(organ);
            }
	    }
	}
	
	@Override
	public void setClientPersId(Integer value) {
	    super.setClientPersId(value);
	    // Si le montant n'est pas encore renseigné, on le renseigne
	    if (srMontantHt() == null || BigDecimal.ZERO.compareTo(srMontantHt()) == 0) {
	        NSArray<RepartPartenaireTranche> reparts = tranche().toRepartPartenaireTranches(
	                EORepartPartenaireTranche.CONTRAT_PARTENAIRE.dot(EOContratPartenaire.PERS_ID).eq(value));
	        if (!reparts.isEmpty())
	            setSrMontantHt(reparts.lastObject().rptMontantParticipation());
	    }
	}

    public NSArray<IRepartMontant> repartPlanComptables() {
        return null;
    }
	
    public EOOrgan organForBibasse() {
        if (organ() != null) {
            EOExercice exo = tranche().exerciceCocktail().exercice();
            // TODO :  optimiser, mettre ça en cache
            int niveau = FinderBudgetParametre.getParametreOrganNiveauSaisie(editingContext(), exo);
            // En principe niveau = CR ou UB
            if (organ().orgNiveau().intValue() == niveau)
                return organ();
            // Si le niveau est supérieur (plus profond) on prend le père
            else if (organ().orgNiveau().intValue() > niveau) {
                EOOrgan currentOrgan = organ().organPere();
                while (currentOrgan != null && currentOrgan.orgNiveau().intValue() > niveau) {
                    currentOrgan = currentOrgan.organPere();
                }
                return currentOrgan;
            } else if (organ().orgNiveau().intValue() < niveau) {
                return null;
            }
        }
        return null;
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#datePrevisionnelle()
     */
    public NSTimestamp datePrevisionnelle() {
        return srDatePrev();
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#montantHt()
     */
    public BigDecimal montantHt() {
        return srMontantHt();
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#libelle()
     */
    public String libelle() {
        return srLibelle();
    }
    
//    AT : utile ?
//    public boolean translateOrgan() {
//        long exoTranche = tranche().exerciceCocktail().exeExercice();
//        EOOrgan organRecette = this.organ();
//        boolean hasTranslated = false;
//        NSArray<EOExercice> exosRecette = (NSArray<EOExercice>) organRecette.organExercice().valueForKey(EOOrganExercice.EXERCICE_KEY);
//        if (!exosRecette.isEmpty()) {
//            EOExercice exoOrganRecette = exosRecette.lastObject();
//            // Si l'exercice de l'organ de la depense n'est pas celui de la tranche, on cherche le même organ 
//            // pour l'exercice de la tranche
//            if (!(exoOrganRecette.exeExercice() == exoTranche)) {
//                EOQualifier qual = ERXQ.equals(EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXE_ORDRE_KEY, exoTranche).and(
//                                               EOOrgan.qualFromOrgan(organRecette));
//                EOOrgan organExo = EOOrgan.fetchFirstByQualifier(editingContext(), qual);
//                if (organExo == null) {
//                    NSMutableDictionary<String, String> stringContainer = new NSMutableDictionary<String, String>(libelle(), "recette");
//                    stringContainer.setObjectForKey(Long.toString(exoTranche), "exo");
//                    String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("OrganSurNouvelExoIntrouvableRec", stringContainer);
//                    throw new ValidationException(msg);
//                } else {
//                    setOrganRelationship(organExo);
//                    hasTranslated = true;
//                }
//            }
//        }
//        return hasTranslated;
//    }
    
}
