

// TrancheBudget.java
// 
package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.cocowork.common.exception.ExceptionApplication;
import org.cocktail.cocowork.common.nombre.NombreOperation;
import org.cocktail.cocowork.server.Cocowork;
import org.cocktail.cocowork.server.CocoworkParamManager;
import org.cocktail.cocowork.server.metier.convention.depenses.ConventionsDepensesService;
import org.cocktail.fwkcktljefyadmin.common.exception.IllegalArgumentException;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;



public class TrancheBudget extends EOTrancheBudget {

    public static final EOQualifier QUAL_NON_SUPPR = ERXQ.equals(TrancheBudget.TB_SUPPR_KEY, "N");
    private static final Logger LOG = Logger.getLogger(TrancheBudget.class);
    public static final ERXKey<BigDecimal> TOTAL_DEPENSES = new ERXKey<BigDecimal>("totalDepenses");

    public static final String MARQUEUR_SUPPRESSION_OUI = "O";
    
    public TrancheBudget() {
        super();
    }

    public TrancheBudget mergeAvecExistant() {
        TrancheBudget result = this;
        
        EOQualifier qual = TRANCHE.eq(tranche()).and(TYPE_CREDIT.eq(typeCredit())).and(ORGAN.eq(organ())).and(QUAL_NON_SUPPR);
        TrancheBudget existant = TrancheBudget.fetch(editingContext(), qual);
        if (existant != null) {
            LOG.info("Va merger une TrancheBudget avec celle existante");
            existant.setTbMontant(existant.tbMontant().add(this.tbMontant()));
            // On supprime l'objet courant
            this.tranche().removeFromTrancheBudgets(this);
            this.setTrancheRelationship(null);
            this.editingContext().deleteObject(this);
            
            result = existant;
        }    
        
        return result;
    }
    
    /** Reprise du mécanisme de budget présent dans Coconuts */
    public static TrancheBudget creerTrancheBudget(Tranche tranche, EOExercice exericeOuvertCourant, Integer utlOrdre) {
        if (tranche == null) {
            throw new IllegalArgumentException("La tranche est obligatoire");
        }
        
        // Tout un tas de vérif sur l'exercice avant d'aller plus loin...
        DepenseRecetteCommon.verificationExercice(tranche.exerciceCocktail(), exericeOuvertCourant);
        TrancheBudget trancheBudget = EOTrancheBudget.create(tranche.editingContext(), null, null, null, null);
        trancheBudget.setTbSuppr("N");
        trancheBudget.setTbDateCreation(new NSTimestamp());
        trancheBudget.setTrancheRelationship(tranche);
        trancheBudget.setExerciceRelationship(tranche.exerciceCocktail().exercice());
        trancheBudget.setTbMontant(BigDecimal.ZERO);
        trancheBudget.setTbDateModif(new NSTimestamp());
        trancheBudget.setUtlOrdreCreation(utlOrdre);
        return trancheBudget;
    }
    
    public static void supprimerTrancheBudget(EOEditingContext ec, TrancheBudget trancheBudget, VCreditsPositionnes creditsDepensesPos) {
        // On vérifie avant que le montant dépensé soit égal à 0
        if (trancheBudget.totalDepenses().signum() > 0) {
            NSNumberFormatter formatter = new NombreOperation().getFormatterMoney();
            String msg =    "Op\u00E9ration impossible : \n" 
                            + "Des d\u00E9penses ont \u00E9t\u00E9 r\u00E9alis\u00E9es \u00E0 hauteur de "
                            + formatter.format(trancheBudget.totalDepenses()) + " sur ces cr\u00E9dits positionn\u00E9s.\n"
                            + "Il n'est donc pas permis de supprimer ces cr\u00E9dits.";
            throw new ValidationException(msg);
        }
        // Enfin là c'est safe, on historise et on marque si besoin la TrancheBudget à supprimer
        if (creditsDepensesPos != null) {
            HistoCreditPositionne.creerHistoCreditPositionne(ec, creditsDepensesPos.tranche(), creditsDepensesPos.planco(), 
                            creditsDepensesPos.typeCredit(), creditsDepensesPos.organ(), creditsDepensesPos.montant().negate());
            trancheBudget.setTbMontant(trancheBudget.tbMontant().subtract(creditsDepensesPos.montant()));
        } else {
            HistoCreditPositionne.creerHistoCreditPositionne(ec, trancheBudget.tranche(), null, 
                            trancheBudget.typeCredit(), trancheBudget.organ(), trancheBudget.tbMontant().negate());
            trancheBudget.setTbMontant(BigDecimal.valueOf(0));
        }
        
        if (trancheBudget.tbMontant().compareTo(BigDecimal.ZERO) <= 0) {
            trancheBudget.supprimer(null);
        }
    }

    public static void supprimerLigneCreditDepense(EOEditingContext ec, EOExercice exericeOuvertCourant, VCreditsPositionnes creditsDepensesPos, TrancheBudget trancheBudget) {
        EOExerciceCocktail exerciceSelectionne = creditsDepensesPos.tranche().exerciceCocktail();
        DepenseRecetteCommon.verificationExercice(exerciceSelectionne, exericeOuvertCourant);
        supprimerTrancheBudget(ec, trancheBudget, creditsDepensesPos);
    }
    
    public static EOQualifier qualifierTranchesBudgetsActives(Tranche tranche, EOOrgan organ, EOTypeCredit typeCredit) {
        return TrancheBudget.ORGAN.eq(organ).
                and(TrancheBudget.TYPE_CREDIT.eq(typeCredit)).
                and(TrancheBudget.TRANCHE.eq(tranche)).
                and(TrancheBudget.QUAL_NON_SUPPR);
    }
    
    public static boolean existeAbsenceTrancheBudget(List<TrancheBudget> tranchesBudgets) {
        return tranchesBudgets.size() < 1;
    }

    public static boolean existeDoublonsTranchesBudgets(List<TrancheBudget> tranchesBudgets) {       
        return tranchesBudgets.size() > 1;    
    }
    
    /**
     * Traitement des doublons de TrancheBudget : 
     * On supprime toutes les tranches existantes 
     * et on recree une nouvelle tranche dont le montant est égal au montant positionné dans l'histo de la tranche.
     * @param tranchesBudgets les tranches budgets a supprimer.
     * @param montantCreditsPositionnes le montant de la nouvelle tranche budget remplacant les autres supprimées.
     * @param utlOrdre l'utilisateur a l'origine de l'operation.
     */
     public static void corrigerDoublonsTranchesBudgets(List<TrancheBudget> tranchesBudgets, BigDecimal montantCreditsPositionnes, Integer utlOrdre) {
        TrancheBudget derniereTrancheBudget = tranchesBudgets.get(tranchesBudgets.size() - 1);
        
        TrancheBudget newTrancheBudget = TrancheBudget.creerTrancheBudget(
            derniereTrancheBudget.tranche(),
            derniereTrancheBudget.exercice(), 
            utlOrdre);
        
        newTrancheBudget.setTbMontant(montantCreditsPositionnes);
        newTrancheBudget.setOrgan(derniereTrancheBudget.organ());
        newTrancheBudget.setTypeCredit(derniereTrancheBudget.typeCredit());
        
        for (TrancheBudget currentTrancheBudget : tranchesBudgets) {
            currentTrancheBudget.supprimer(utlOrdre);
        }
    }
    
    /**
     * Supprime une tranche budget en poitionnant son montant a 0 et le flag 'Suppr' a Oui.
     * @param utlOrdre l'utilisateur a l'origine de l'operation.
     */
    public void supprimer(Integer utlOrdre) {
        setTbMontant(BigDecimal.ZERO);
        setTbSuppr(MARQUEUR_SUPPRESSION_OUI);
        setTbDateModif(new NSTimestamp());
        if (utlOrdre != null) {
            setUtlOrdreModif(utlOrdre);
        }
    }
    
    /**
     * <p>Fournit le total des depenses existantes sur les memes exercice, convention, type de credit et LB que cet objet. 
     * <p>Ce total des depenses est : total restant engage + total liquide.
     * <p>Fait appel a une procedure stoquee.
     * @return Le total des depenses ou <code>null</code> si probleme.
     * @throws ExceptionApplication 
     */
    public BigDecimal totalDepenses() {
        BigDecimal totalDepense=null;
        if (!isNewObject()) {
        	totalDepense = ConventionsDepensesService.creerNouvelleInstance().totalDepensesPourConventionSurExerciceTypeCreditEtOrgan(tranche().contrat(), exercice(), typeCredit(), organ());
        	
//            try {
//                Number exeOrdre = (Number)ERXEOControlUtilities.primaryKeyObjectForObject(exercice());
//                Number conOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(tranche().contrat());
//                Number tcdOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(typeCredit());
//                Number orgId = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(organ());
//
//                totalDepense = new ProcedureGetTotalDepense(editingContext()).execute(
//                                exeOrdre, 
//                                conOrdre, 
//                                tcdOrdre, 
//                                orgId);
//            } 
//            catch (ExceptionProcedure e) {
//                throw new RuntimeException(
//                                "Une erreur est survenue lors de l'appel \u00E0 la procedure stoqu\u00E9e :\n" +
//                                                e.getMessage(), e);
//            }
//            catch (Exception e) {
//                throw new RuntimeException(
//                                "Impossible d'obtenir la cl\u00E9 primaire d'un objet :\n" +
//                                                e.getMessage(), e);
//            }
        }
        return totalDepense;
    }
    
    public BigDecimal totalResteEngage() {
        BigDecimal totalResteEngage=null;
        if (!isNewObject()) {
        	totalResteEngage = ConventionsDepensesService.creerNouvelleInstance().totalResteEngagePourConventionSurExerciceTypeCreditEtOrgan(tranche().contrat(), exercice(), typeCredit(), organ());
        }
        return totalResteEngage;
    }

    /**
     * Fournit le "reste a depenser" par rapport au total positionne, 
     * cad la difference entre le total positionne et le total des depenses existantes.
     * @see #totalDepenses()
     * @return Le montant restant a depenser ou <code>null</code> si probleme lors de l'obtention du total des depenses.
     * @throws ExceptionApplication 
     */
    public BigDecimal resteADepenser() {
        BigDecimal totalDepenses = ConventionsDepensesService.creerNouvelleInstance().totalDepensesPlusResteEngagePourConventionSurExerciceTypeCreditEtOrgan(tranche().contrat(), exercice(), typeCredit(), organ()); 
        if (totalDepenses != null)
            return tbMontant().subtract(totalDepenses);
        else
            return null;
    }
 
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (tranche().contrat().isModeRA()) {

	        // Si le crédit positionné dépasse le montant de la tranche
	        if (tbMontant().compareTo(tranche().traMontant()) == 1) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, TB_MONTANT_KEY, tbMontant(), "MontantTooBig");
	        }
            // Si on est dans le cadre d'une convention RA, on vérifie qu'il n'y ait pas déjà une trancheBudget sur un autre
            // organ
	        // Si multi-ligne par conv. RA non activé
	        String isMultiLBAutorise = Cocowork.paramManager.getParam(CocoworkParamManager.MULTI_LB_POUR_CONV_RA_AUTORISE);
			if (!CktlParamManager.booleanForValue(isMultiLBAutorise)) {
	            NSArray<EOOrgan> allOrgansDep = (NSArray<EOOrgan>) tranche().trancheBudgetsNonSupprimes().valueForKey(ORGAN_KEY);
	            allOrgansDep = ERXArrayUtilities.arrayWithoutDuplicates(allOrgansDep);
	            if (allOrgansDep.count() > 1) {
	                throw ERXValidationFactory.defaultFactory().createCustomException(this, "MultipleOrgansRA");
	            }
	        }
        } else {
            ContratFinancementDepensesValidation financementValidation = new ContratFinancementDepensesValidation();
    		if (!financementValidation.isSatisfiedBy(tranche().contrat())) {
    		    BigDecimal totalContributionsAPositionner = tranche().contrat().totalContributionsAPositionner();
                BigDecimal totalPositionneContrat = tranche().contrat().totalPositionneContrat();
                
                ERXValidationException ex = ERXValidationFactory.defaultFactory().createCustomException(this, "MontantContributionsMoinsFgTropBasPourTotalPositionne");
                NSMutableDictionary<String, String> replacements = new NSMutableDictionary<String, String>();
                replacements.takeValueForKey(totalContributionsAPositionner, "contrib");
                replacements.takeValueForKey(totalPositionneContrat, "creditpos");
                ex.setContext(replacements);
                throw ex;
        	}
        }
    }
    
}
