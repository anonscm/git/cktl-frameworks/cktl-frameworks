// DO NOT EDIT.  Make changes to TypeContact.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeContact extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeContact";

  // Attribute Keys
  public static final ERXKey<Integer> TC_CLASSE = new ERXKey<Integer>("tcClasse");
  public static final ERXKey<String> TC_LIBELLE = new ERXKey<String>("tcLibelle");
  public static final ERXKey<String> TC_LIBELLE_COURT = new ERXKey<String>("tcLibelleCourt");
  // Relationship Keys

  // Attributes
  public static final String TC_CLASSE_KEY = TC_CLASSE.key();
  public static final String TC_LIBELLE_KEY = TC_LIBELLE.key();
  public static final String TC_LIBELLE_COURT_KEY = TC_LIBELLE_COURT.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeContact.class);

  public TypeContact localInstanceIn(EOEditingContext editingContext) {
    TypeContact localInstance = (TypeContact)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer tcClasse() {
    return (Integer) storedValueForKey(EOTypeContact.TC_CLASSE_KEY);
  }

  public void setTcClasse(Integer value) {
    if (EOTypeContact.LOG.isDebugEnabled()) {
        EOTypeContact.LOG.debug( "updating tcClasse from " + tcClasse() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContact.TC_CLASSE_KEY);
  }

  public String tcLibelle() {
    return (String) storedValueForKey(EOTypeContact.TC_LIBELLE_KEY);
  }

  public void setTcLibelle(String value) {
    if (EOTypeContact.LOG.isDebugEnabled()) {
        EOTypeContact.LOG.debug( "updating tcLibelle from " + tcLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContact.TC_LIBELLE_KEY);
  }

  public String tcLibelleCourt() {
    return (String) storedValueForKey(EOTypeContact.TC_LIBELLE_COURT_KEY);
  }

  public void setTcLibelleCourt(String value) {
    if (EOTypeContact.LOG.isDebugEnabled()) {
        EOTypeContact.LOG.debug( "updating tcLibelleCourt from " + tcLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeContact.TC_LIBELLE_COURT_KEY);
  }


  public static TypeContact create(EOEditingContext editingContext, String tcLibelle
) {
    TypeContact eo = (TypeContact) EOUtilities.createAndInsertInstance(editingContext, EOTypeContact.ENTITY_NAME);    
        eo.setTcLibelle(tcLibelle);
    return eo;
  }

  public static ERXFetchSpecification<TypeContact> fetchSpec() {
    return new ERXFetchSpecification<TypeContact>(EOTypeContact.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeContact> fetchAll(EOEditingContext editingContext) {
    return EOTypeContact.fetchAll(editingContext, null);
  }

  public static NSArray<TypeContact> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeContact.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeContact> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeContact> fetchSpec = new ERXFetchSpecification<TypeContact>(EOTypeContact.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeContact> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeContact fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeContact.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeContact fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeContact> eoObjects = EOTypeContact.fetchAll(editingContext, qualifier, null);
    TypeContact eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeContact fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeContact.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeContact fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeContact eoObject = EOTypeContact.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeContact localInstanceIn(EOEditingContext editingContext, TypeContact eo) {
    TypeContact localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}