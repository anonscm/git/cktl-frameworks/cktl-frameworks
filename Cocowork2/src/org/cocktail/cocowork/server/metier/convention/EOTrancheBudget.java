// DO NOT EDIT.  Make changes to TrancheBudget.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTrancheBudget extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTrancheBudget";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> TB_DATE_BUDGET = new ERXKey<NSTimestamp>("tbDateBudget");
  public static final ERXKey<NSTimestamp> TB_DATE_CREATION = new ERXKey<NSTimestamp>("tbDateCreation");
  public static final ERXKey<NSTimestamp> TB_DATE_MODIF = new ERXKey<NSTimestamp>("tbDateModif");
  public static final ERXKey<NSTimestamp> TB_DATE_VALID = new ERXKey<NSTimestamp>("tbDateValid");
  public static final ERXKey<java.math.BigDecimal> TB_MNT_BUDGET = new ERXKey<java.math.BigDecimal>("tbMntBudget");
  public static final ERXKey<java.math.BigDecimal> TB_MNT_DISPO = new ERXKey<java.math.BigDecimal>("tbMntDispo");
  public static final ERXKey<java.math.BigDecimal> TB_MNT_VALID = new ERXKey<java.math.BigDecimal>("tbMntValid");
  public static final ERXKey<java.math.BigDecimal> TB_MONTANT = new ERXKey<java.math.BigDecimal>("tbMontant");
  public static final ERXKey<String> TB_SUPPR = new ERXKey<String>("tbSuppr");
  public static final ERXKey<String> TB_VALID = new ERXKey<String>("tbValid");
  public static final ERXKey<Integer> UTL_ORDRE_CREATION = new ERXKey<Integer>("utlOrdreCreation");
  public static final ERXKey<Integer> UTL_ORDRE_MODIF = new ERXKey<Integer>("utlOrdreModif");
  public static final ERXKey<Integer> UTL_ORDRE_VALID = new ERXKey<Integer>("utlOrdreValid");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organ");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String TB_DATE_BUDGET_KEY = TB_DATE_BUDGET.key();
  public static final String TB_DATE_CREATION_KEY = TB_DATE_CREATION.key();
  public static final String TB_DATE_MODIF_KEY = TB_DATE_MODIF.key();
  public static final String TB_DATE_VALID_KEY = TB_DATE_VALID.key();
  public static final String TB_MNT_BUDGET_KEY = TB_MNT_BUDGET.key();
  public static final String TB_MNT_DISPO_KEY = TB_MNT_DISPO.key();
  public static final String TB_MNT_VALID_KEY = TB_MNT_VALID.key();
  public static final String TB_MONTANT_KEY = TB_MONTANT.key();
  public static final String TB_SUPPR_KEY = TB_SUPPR.key();
  public static final String TB_VALID_KEY = TB_VALID.key();
  public static final String UTL_ORDRE_CREATION_KEY = UTL_ORDRE_CREATION.key();
  public static final String UTL_ORDRE_MODIF_KEY = UTL_ORDRE_MODIF.key();
  public static final String UTL_ORDRE_VALID_KEY = UTL_ORDRE_VALID.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOTrancheBudget.class);

  public TrancheBudget localInstanceIn(EOEditingContext editingContext) {
    TrancheBudget localInstance = (TrancheBudget)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp tbDateBudget() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudget.TB_DATE_BUDGET_KEY);
  }

  public void setTbDateBudget(NSTimestamp value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbDateBudget from " + tbDateBudget() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_DATE_BUDGET_KEY);
  }

  public NSTimestamp tbDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudget.TB_DATE_CREATION_KEY);
  }

  public void setTbDateCreation(NSTimestamp value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbDateCreation from " + tbDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_DATE_CREATION_KEY);
  }

  public NSTimestamp tbDateModif() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudget.TB_DATE_MODIF_KEY);
  }

  public void setTbDateModif(NSTimestamp value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbDateModif from " + tbDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_DATE_MODIF_KEY);
  }

  public NSTimestamp tbDateValid() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudget.TB_DATE_VALID_KEY);
  }

  public void setTbDateValid(NSTimestamp value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbDateValid from " + tbDateValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_DATE_VALID_KEY);
  }

  public java.math.BigDecimal tbMntBudget() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudget.TB_MNT_BUDGET_KEY);
  }

  public void setTbMntBudget(java.math.BigDecimal value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbMntBudget from " + tbMntBudget() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_MNT_BUDGET_KEY);
  }

  public java.math.BigDecimal tbMntDispo() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudget.TB_MNT_DISPO_KEY);
  }

  public void setTbMntDispo(java.math.BigDecimal value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbMntDispo from " + tbMntDispo() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_MNT_DISPO_KEY);
  }

  public java.math.BigDecimal tbMntValid() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudget.TB_MNT_VALID_KEY);
  }

  public void setTbMntValid(java.math.BigDecimal value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbMntValid from " + tbMntValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_MNT_VALID_KEY);
  }

  public java.math.BigDecimal tbMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudget.TB_MONTANT_KEY);
  }

  public void setTbMontant(java.math.BigDecimal value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbMontant from " + tbMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_MONTANT_KEY);
  }

  public String tbSuppr() {
    return (String) storedValueForKey(EOTrancheBudget.TB_SUPPR_KEY);
  }

  public void setTbSuppr(String value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbSuppr from " + tbSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_SUPPR_KEY);
  }

  public String tbValid() {
    return (String) storedValueForKey(EOTrancheBudget.TB_VALID_KEY);
  }

  public void setTbValid(String value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating tbValid from " + tbValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.TB_VALID_KEY);
  }

  public Integer utlOrdreCreation() {
    return (Integer) storedValueForKey(EOTrancheBudget.UTL_ORDRE_CREATION_KEY);
  }

  public void setUtlOrdreCreation(Integer value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating utlOrdreCreation from " + utlOrdreCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.UTL_ORDRE_CREATION_KEY);
  }

  public Integer utlOrdreModif() {
    return (Integer) storedValueForKey(EOTrancheBudget.UTL_ORDRE_MODIF_KEY);
  }

  public void setUtlOrdreModif(Integer value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating utlOrdreModif from " + utlOrdreModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.UTL_ORDRE_MODIF_KEY);
  }

  public Integer utlOrdreValid() {
    return (Integer) storedValueForKey(EOTrancheBudget.UTL_ORDRE_VALID_KEY);
  }

  public void setUtlOrdreValid(Integer value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
        EOTrancheBudget.LOG.debug( "updating utlOrdreValid from " + utlOrdreValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudget.UTL_ORDRE_VALID_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOTrancheBudget.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOTrancheBudget.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
      EOTrancheBudget.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudget.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudget.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOTrancheBudget.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOTrancheBudget.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
      EOTrancheBudget.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudget.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudget.ORGAN_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOTrancheBudget.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOTrancheBudget.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
      EOTrancheBudget.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudget.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudget.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(EOTrancheBudget.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOTrancheBudget.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (EOTrancheBudget.LOG.isDebugEnabled()) {
      EOTrancheBudget.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudget.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudget.TYPE_CREDIT_KEY);
    }
  }
  

  public static TrancheBudget create(EOEditingContext editingContext, Integer utlOrdreCreation
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit) {
    TrancheBudget eo = (TrancheBudget) EOUtilities.createAndInsertInstance(editingContext, EOTrancheBudget.ENTITY_NAME);    
        eo.setUtlOrdreCreation(utlOrdreCreation);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  public static ERXFetchSpecification<TrancheBudget> fetchSpec() {
    return new ERXFetchSpecification<TrancheBudget>(EOTrancheBudget.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TrancheBudget> fetchAll(EOEditingContext editingContext) {
    return EOTrancheBudget.fetchAll(editingContext, null);
  }

  public static NSArray<TrancheBudget> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTrancheBudget.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TrancheBudget> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TrancheBudget> fetchSpec = new ERXFetchSpecification<TrancheBudget>(EOTrancheBudget.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TrancheBudget> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TrancheBudget fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudget.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudget fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TrancheBudget> eoObjects = EOTrancheBudget.fetchAll(editingContext, qualifier, null);
    TrancheBudget eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTrancheBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudget fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudget.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudget fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TrancheBudget eoObject = EOTrancheBudget.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTrancheBudget that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudget localInstanceIn(EOEditingContext editingContext, TrancheBudget eo) {
    TrancheBudget localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}