// DO NOT EDIT.  Make changes to RepartContratAxeStrategique.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartContratAxeStrategique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWRepartContratAxeStrategique";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.AxeStrategique> AXE_STRATEGIQUE = new ERXKey<org.cocktail.cocowork.server.metier.convention.AxeStrategique>("axeStrategique");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");

  // Attributes
  // Relationships
  public static final String AXE_STRATEGIQUE_KEY = AXE_STRATEGIQUE.key();
  public static final String CONTRAT_KEY = CONTRAT.key();

  private static Logger LOG = Logger.getLogger(EORepartContratAxeStrategique.class);

  public RepartContratAxeStrategique localInstanceIn(EOEditingContext editingContext) {
    RepartContratAxeStrategique localInstance = (RepartContratAxeStrategique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.cocowork.server.metier.convention.AxeStrategique axeStrategique() {
    return (org.cocktail.cocowork.server.metier.convention.AxeStrategique)storedValueForKey(EORepartContratAxeStrategique.AXE_STRATEGIQUE_KEY);
  }
  
  public void setAxeStrategique(org.cocktail.cocowork.server.metier.convention.AxeStrategique value) {
    takeStoredValueForKey(value, EORepartContratAxeStrategique.AXE_STRATEGIQUE_KEY);
  }

  public void setAxeStrategiqueRelationship(org.cocktail.cocowork.server.metier.convention.AxeStrategique value) {
    if (EORepartContratAxeStrategique.LOG.isDebugEnabled()) {
      EORepartContratAxeStrategique.LOG.debug("updating axeStrategique from " + axeStrategique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAxeStrategique(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.AxeStrategique oldValue = axeStrategique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartContratAxeStrategique.AXE_STRATEGIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartContratAxeStrategique.AXE_STRATEGIQUE_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EORepartContratAxeStrategique.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EORepartContratAxeStrategique.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EORepartContratAxeStrategique.LOG.isDebugEnabled()) {
      EORepartContratAxeStrategique.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartContratAxeStrategique.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartContratAxeStrategique.CONTRAT_KEY);
    }
  }
  

  public static RepartContratAxeStrategique create(EOEditingContext editingContext, org.cocktail.cocowork.server.metier.convention.AxeStrategique axeStrategique, org.cocktail.cocowork.server.metier.convention.Contrat contrat) {
    RepartContratAxeStrategique eo = (RepartContratAxeStrategique) EOUtilities.createAndInsertInstance(editingContext, EORepartContratAxeStrategique.ENTITY_NAME);    
    eo.setAxeStrategiqueRelationship(axeStrategique);
    eo.setContratRelationship(contrat);
    return eo;
  }

  public static ERXFetchSpecification<RepartContratAxeStrategique> fetchSpec() {
    return new ERXFetchSpecification<RepartContratAxeStrategique>(EORepartContratAxeStrategique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartContratAxeStrategique> fetchAll(EOEditingContext editingContext) {
    return EORepartContratAxeStrategique.fetchAll(editingContext, null);
  }

  public static NSArray<RepartContratAxeStrategique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartContratAxeStrategique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartContratAxeStrategique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartContratAxeStrategique> fetchSpec = new ERXFetchSpecification<RepartContratAxeStrategique>(EORepartContratAxeStrategique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartContratAxeStrategique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartContratAxeStrategique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartContratAxeStrategique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartContratAxeStrategique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartContratAxeStrategique> eoObjects = EORepartContratAxeStrategique.fetchAll(editingContext, qualifier, null);
    RepartContratAxeStrategique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWRepartContratAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartContratAxeStrategique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartContratAxeStrategique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartContratAxeStrategique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartContratAxeStrategique eoObject = EORepartContratAxeStrategique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWRepartContratAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartContratAxeStrategique localInstanceIn(EOEditingContext editingContext, RepartContratAxeStrategique eo) {
    RepartContratAxeStrategique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}