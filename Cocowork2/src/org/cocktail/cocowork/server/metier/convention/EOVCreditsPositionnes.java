// DO NOT EDIT.  Make changes to VCreditsPositionnes.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVCreditsPositionnes extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWVCreditsPositionnes";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organ");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String MONTANT_KEY = MONTANT.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOVCreditsPositionnes.class);

  public VCreditsPositionnes localInstanceIn(EOEditingContext editingContext) {
    VCreditsPositionnes localInstance = (VCreditsPositionnes)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(EOVCreditsPositionnes.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
        EOVCreditsPositionnes.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, EOVCreditsPositionnes.MONTANT_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOVCreditsPositionnes.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOVCreditsPositionnes.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
      EOVCreditsPositionnes.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnes.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnes.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOVCreditsPositionnes.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOVCreditsPositionnes.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
      EOVCreditsPositionnes.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnes.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnes.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(EOVCreditsPositionnes.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOVCreditsPositionnes.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
      EOVCreditsPositionnes.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnes.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnes.PLANCO_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOVCreditsPositionnes.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOVCreditsPositionnes.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
      EOVCreditsPositionnes.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnes.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnes.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(EOVCreditsPositionnes.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOVCreditsPositionnes.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (EOVCreditsPositionnes.LOG.isDebugEnabled()) {
      EOVCreditsPositionnes.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnes.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnes.TYPE_CREDIT_KEY);
    }
  }
  

  public static VCreditsPositionnes create(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco, org.cocktail.cocowork.server.metier.convention.Tranche tranche) {
    VCreditsPositionnes eo = (VCreditsPositionnes) EOUtilities.createAndInsertInstance(editingContext, EOVCreditsPositionnes.ENTITY_NAME);    
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlancoRelationship(planco);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VCreditsPositionnes> fetchSpec() {
    return new ERXFetchSpecification<VCreditsPositionnes>(EOVCreditsPositionnes.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VCreditsPositionnes> fetchAll(EOEditingContext editingContext) {
    return EOVCreditsPositionnes.fetchAll(editingContext, null);
  }

  public static NSArray<VCreditsPositionnes> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVCreditsPositionnes.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VCreditsPositionnes> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VCreditsPositionnes> fetchSpec = new ERXFetchSpecification<VCreditsPositionnes>(EOVCreditsPositionnes.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VCreditsPositionnes> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VCreditsPositionnes fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVCreditsPositionnes.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VCreditsPositionnes fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VCreditsPositionnes> eoObjects = EOVCreditsPositionnes.fetchAll(editingContext, qualifier, null);
    VCreditsPositionnes eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWVCreditsPositionnes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VCreditsPositionnes fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVCreditsPositionnes.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VCreditsPositionnes fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VCreditsPositionnes eoObject = EOVCreditsPositionnes.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWVCreditsPositionnes that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VCreditsPositionnes localInstanceIn(EOEditingContext editingContext, VCreditsPositionnes eo) {
    VCreditsPositionnes localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}