// AvenantDocument.java
// Created on Tue Dec 09 13:56:50 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.cocowork.common.tools.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;

public class AvenantDocument extends EOAvenantDocument {

    public AvenantDocument() {
        super();
    }
    
	public static AvenantDocument instanciate(EOEditingContext ec) throws Exception {
		return (AvenantDocument) Factory.instanceForEntity(ec, ENTITY_NAME);
	}

}
