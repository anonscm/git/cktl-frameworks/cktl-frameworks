// DO NOT EDIT.  Make changes to FraisGestion.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOFraisGestion extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWFraisGestion";

  // Attribute Keys
  public static final ERXKey<String> FG_LIBELLE = new ERXKey<String>("fgLibelle");
  public static final ERXKey<java.math.BigDecimal> FG_MONTANT = new ERXKey<java.math.BigDecimal>("fgMontant");
  public static final ERXKey<java.math.BigDecimal> FG_PCT_TRA = new ERXKey<java.math.BigDecimal>("fgPctTra");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> REPART_PARTENAIRE_TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>("repartPartenaireTranche");

  // Attributes
  public static final String FG_LIBELLE_KEY = FG_LIBELLE.key();
  public static final String FG_MONTANT_KEY = FG_MONTANT.key();
  public static final String FG_PCT_TRA_KEY = FG_PCT_TRA.key();
  // Relationships
  public static final String REPART_PARTENAIRE_TRANCHE_KEY = REPART_PARTENAIRE_TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOFraisGestion.class);

  public FraisGestion localInstanceIn(EOEditingContext editingContext) {
    FraisGestion localInstance = (FraisGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String fgLibelle() {
    return (String) storedValueForKey(EOFraisGestion.FG_LIBELLE_KEY);
  }

  public void setFgLibelle(String value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating fgLibelle from " + fgLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.FG_LIBELLE_KEY);
  }

  public java.math.BigDecimal fgMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.FG_MONTANT_KEY);
  }

  public void setFgMontant(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating fgMontant from " + fgMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.FG_MONTANT_KEY);
  }

  public java.math.BigDecimal fgPctTra() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.FG_PCT_TRA_KEY);
  }

  public void setFgPctTra(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating fgPctTra from " + fgPctTra() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.FG_PCT_TRA_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche repartPartenaireTranche() {
    return (org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche)storedValueForKey(EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
  }
  
  public void setRepartPartenaireTranche(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche value) {
    takeStoredValueForKey(value, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
  }

  public void setRepartPartenaireTrancheRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
      EOFraisGestion.LOG.debug("updating repartPartenaireTranche from " + repartPartenaireTranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setRepartPartenaireTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche oldValue = repartPartenaireTranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
    }
  }
  

  public static FraisGestion create(EOEditingContext editingContext, String fgLibelle
, java.math.BigDecimal fgMontant
, java.math.BigDecimal fgPctTra
, org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche repartPartenaireTranche) {
    FraisGestion eo = (FraisGestion) EOUtilities.createAndInsertInstance(editingContext, EOFraisGestion.ENTITY_NAME);    
        eo.setFgLibelle(fgLibelle);
        eo.setFgMontant(fgMontant);
        eo.setFgPctTra(fgPctTra);
    eo.setRepartPartenaireTrancheRelationship(repartPartenaireTranche);
    return eo;
  }

  public static ERXFetchSpecification<FraisGestion> fetchSpec() {
    return new ERXFetchSpecification<FraisGestion>(EOFraisGestion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext) {
    return EOFraisGestion.fetchAll(editingContext, null);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOFraisGestion.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<FraisGestion> fetchSpec = new ERXFetchSpecification<FraisGestion>(EOFraisGestion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<FraisGestion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static FraisGestion fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOFraisGestion.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FraisGestion fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<FraisGestion> eoObjects = EOFraisGestion.fetchAll(editingContext, qualifier, null);
    FraisGestion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWFraisGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FraisGestion fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOFraisGestion.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FraisGestion fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    FraisGestion eoObject = EOFraisGestion.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWFraisGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FraisGestion localInstanceIn(EOEditingContext editingContext, FraisGestion eo) {
    FraisGestion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}