// DO NOT EDIT.  Make changes to VCreditsPositionnesRec.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVCreditsPositionnesRec extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWVCreditsPositionnesRec";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organ");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String MONTANT_KEY = MONTANT.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOVCreditsPositionnesRec.class);

  public VCreditsPositionnesRec localInstanceIn(EOEditingContext editingContext) {
    VCreditsPositionnesRec localInstance = (VCreditsPositionnesRec)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(EOVCreditsPositionnesRec.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
        EOVCreditsPositionnesRec.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.MONTANT_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOVCreditsPositionnesRec.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
      EOVCreditsPositionnesRec.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnesRec.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnesRec.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOVCreditsPositionnesRec.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
      EOVCreditsPositionnesRec.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnesRec.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnesRec.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(EOVCreditsPositionnesRec.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
      EOVCreditsPositionnesRec.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnesRec.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnesRec.PLANCO_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOVCreditsPositionnesRec.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
      EOVCreditsPositionnesRec.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnesRec.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnesRec.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(EOVCreditsPositionnesRec.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOVCreditsPositionnesRec.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (EOVCreditsPositionnesRec.LOG.isDebugEnabled()) {
      EOVCreditsPositionnesRec.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVCreditsPositionnesRec.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVCreditsPositionnesRec.TYPE_CREDIT_KEY);
    }
  }
  

  public static VCreditsPositionnesRec create(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco, org.cocktail.cocowork.server.metier.convention.Tranche tranche) {
    VCreditsPositionnesRec eo = (VCreditsPositionnesRec) EOUtilities.createAndInsertInstance(editingContext, EOVCreditsPositionnesRec.ENTITY_NAME);    
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlancoRelationship(planco);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VCreditsPositionnesRec> fetchSpec() {
    return new ERXFetchSpecification<VCreditsPositionnesRec>(EOVCreditsPositionnesRec.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VCreditsPositionnesRec> fetchAll(EOEditingContext editingContext) {
    return EOVCreditsPositionnesRec.fetchAll(editingContext, null);
  }

  public static NSArray<VCreditsPositionnesRec> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVCreditsPositionnesRec.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VCreditsPositionnesRec> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VCreditsPositionnesRec> fetchSpec = new ERXFetchSpecification<VCreditsPositionnesRec>(EOVCreditsPositionnesRec.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VCreditsPositionnesRec> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VCreditsPositionnesRec fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVCreditsPositionnesRec.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VCreditsPositionnesRec fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VCreditsPositionnesRec> eoObjects = EOVCreditsPositionnesRec.fetchAll(editingContext, qualifier, null);
    VCreditsPositionnesRec eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWVCreditsPositionnesRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VCreditsPositionnesRec fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVCreditsPositionnesRec.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VCreditsPositionnesRec fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VCreditsPositionnesRec eoObject = EOVCreditsPositionnesRec.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWVCreditsPositionnesRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VCreditsPositionnesRec localInstanceIn(EOEditingContext editingContext, VCreditsPositionnesRec eo) {
    VCreditsPositionnesRec localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}