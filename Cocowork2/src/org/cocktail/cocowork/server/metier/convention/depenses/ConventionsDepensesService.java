package org.cocktail.cocowork.server.metier.convention.depenses;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.FraisGestion;
import org.cocktail.cocowork.server.metier.convention.HistoCreditPositionne;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.Tranche;
import org.cocktail.fwkcktldepense.server.metier.EODepenseBudget;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EODepenseControlePlanComptable;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementBudget;
import org.cocktail.fwkcktldepense.server.metier.EOEngagementControleConvention;
import org.cocktail.fwkcktldepense.server.metier.EOExercice;
import org.cocktail.fwkcktldepense.server.metier.EOMandat;
import org.cocktail.fwkcktldepense.server.metier._IDepenseBudget;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

public class ConventionsDepensesService {

	public static ConventionsDepensesService creerNouvelleInstance() {
		return new ConventionsDepensesService();
	}

	public BigDecimal totalDepensesPlusResteEngagePourConventionSurExerciceTypeCreditEtOrgan(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		return totalDepensesPourConventionSurExerciceTypeCreditEtOrgan(convention, exercice, typeCredit, organ).add(totalResteEngagePourConventionSurExerciceTypeCreditEtOrgan(convention, exercice, typeCredit, organ));
	}

	public BigDecimal totalDepensesPourConventionSurExerciceTypeCreditEtOrgan(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		Boolean modeRa = convention.isModeRA();
		BigDecimal totalDepenses;

		if (modeRa) {
			totalDepenses = totalDepensesPourConventionRaSurExercice(convention, exercice, typeCredit, organ);

		} else {
			totalDepenses = totalDepensesPourConventionSimpleSurExercice(convention, exercice, typeCredit, organ);
		}

		return totalDepenses;
	}

	public BigDecimal totalDepensesPourConventionSimpleSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		BigDecimal totalDepenses = BigDecimal.ZERO;
		EOQualifier depensesQualifier = EODepenseControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue());
		if (typeCredit != null) {
			depensesQualifier =
					ERXQ.and(
							depensesQualifier,
							EODepenseControleConvention.DEPENSE_BUDGET
									.dot(EODepenseBudget.ENGAGEMENT_BUDGET)
									.dot(EOEngagementBudget.TYPE_CREDIT)
									.dot(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit.TCD_CODE_KEY).eq(typeCredit.tcdCode()));
		}
		if (organ != null) {
			Number orgId = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(organ);
			ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organKey = EODepenseControleConvention.DEPENSE_BUDGET.dot(EODepenseBudget.ENGAGEMENT_BUDGET).dot(EOEngagementBudget.ORGAN);
			depensesQualifier =
					ERXQ.and(
							depensesQualifier,
							organKey.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_ETAB).eq(organ.orgEtab()),
							organKey.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_UB).eq(organ.orgUb()),
							organKey.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_CR).eq(organ.orgCr()),
							organKey.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_SOUSCR).eq(organ.orgSouscr())
							);
		}

		NSArray<EODepenseControleConvention> depenses =
				ERXQ.filtered(
						convention.depenseControleConventions(),
						depensesQualifier
						);
		NSArray<BigDecimal> montantsDepenses = (NSArray<BigDecimal>) depenses.valueForKey(EODepenseControleConvention.DCON_MONTANT_BUDGETAIRE.key());
		totalDepenses = (BigDecimal) montantsDepenses.valueForKey(ERXKey.sum().key());
		return totalDepenses;
	}

	public BigDecimal totalDepensesPourConventionRaSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		BigDecimal totalDepenses = BigDecimal.ZERO;
		Tranche trancheExercice = ERXQ.first(convention.tranches(), Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));
		EOQualifier qualifier = null;
		if (typeCredit != null) {
			qualifier = ERXQ.and(
					qualifier,
					HistoCreditPositionne.TYPE_CREDIT.eq(typeCredit)
					);
		}

		if (organ != null) {
			qualifier = ERXQ.and(
					qualifier,
					HistoCreditPositionne.ORGAN.eq(organ)
					);
		}

		NSArray<HistoCreditPositionne> histosPourContrat = trancheExercice.histoCreditPositionnes(qualifier);
		NSArray<BigDecimal> montants = (NSArray<BigDecimal>) histosPourContrat.valueForKey(HistoCreditPositionne.HCP_MONTANT.key());
		BigDecimal totalMontants = (BigDecimal) montants.valueForKey(ERXKey.sum().key());
		if (totalMontants.signum() == 1) {
			NSArray<EODepenseBudget> depensesPourContrat =
					ERXArrayUtilities.flatten(
							(NSArray<EODepenseBudget>) histosPourContrat.valueForKeyPath(HistoCreditPositionne.ENGAGEMENT_BUDGETS.dot(EOEngagementBudget.DEPENSE_BUDGETS).key()),
							true
							);
			NSArray<BigDecimal> montantsDepenses = (NSArray<BigDecimal>) depensesPourContrat.valueForKey(EODepenseBudget.DEP_MONTANT_BUDGETAIRE.key());
			totalDepenses = (BigDecimal) montantsDepenses.valueForKey(ERXKey.sum().key());
		}
		return totalDepenses;
	}

	public BigDecimal totalResteEngagePourConventionSurExerciceTypeCreditEtOrgan(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		Boolean modeRa = convention.isModeRA();
		BigDecimal totalResteEngage;

		if (modeRa) {
			totalResteEngage = totalResteEngagePourConventionRaSurExercice(convention, exercice, typeCredit, organ);

		} else {
			totalResteEngage = totalResteEngagePourConventionSimpleSurExercice(convention, exercice, typeCredit, organ);
		}

		return totalResteEngage;
	}

	public BigDecimal totalResteEngagePourConventionSimpleSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		BigDecimal totalResteEngage = BigDecimal.ZERO;
		EOQualifier engagementsQualifier = EOEngagementControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue());
		if (typeCredit != null) {
			engagementsQualifier =
					ERXQ.and(
							engagementsQualifier,
							EOEngagementControleConvention.ENGAGEMENT_BUDGET
									.dot(EOEngagementBudget.TYPE_CREDIT)
									.dot(org.cocktail.fwkcktldepense.server.metier.EOTypeCredit.TCD_CODE_KEY).eq(typeCredit.tcdCode()));
		}
		if (organ != null) {
			ERXKey<org.cocktail.fwkcktldepense.server.metier.EOOrgan> organKeyEngagements = EOEngagementControleConvention.ENGAGEMENT_BUDGET.dot(EOEngagementBudget.ORGAN);
			engagementsQualifier =
					ERXQ.and(
							engagementsQualifier,
							organKeyEngagements.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_ETAB).eq(organ.orgEtab()),
							organKeyEngagements.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_UB).eq(organ.orgUb()),
							organKeyEngagements.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_CR).eq(organ.orgCr()),
							organKeyEngagements.dot(org.cocktail.fwkcktldepense.server.metier.EOOrgan.ORG_SOUSCR).eq(organ.orgSouscr())
							);
		}
		NSArray<EOEngagementControleConvention> engagements =
				ERXQ.filtered(
						convention.engagementControleConventions(),
						engagementsQualifier
						);
		totalResteEngage = (BigDecimal) engagements.valueForKey(ERXKey.sum(EOEngagementControleConvention.ECON_MONTANT_BUDGETAIRE_RESTE).key());
		return totalResteEngage;
	}

	public BigDecimal totalResteEngagePourConventionRaSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, EOTypeCredit typeCredit, EOOrgan organ) {
		BigDecimal totalResteEngage = BigDecimal.ZERO;
		Tranche trancheExercice = ERXQ.first(convention.tranches(), Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));
		EOQualifier qualifier = null;
		if (typeCredit != null) {
			qualifier = ERXQ.and(
					qualifier,
					HistoCreditPositionne.TYPE_CREDIT.eq(typeCredit)
					);
		}

		if (organ != null) {
			qualifier = ERXQ.and(
					qualifier,
					HistoCreditPositionne.ORGAN.eq(organ)
					);
		}

		NSArray<HistoCreditPositionne> histosPourContrat = trancheExercice.histoCreditPositionnes(qualifier);
		NSArray<BigDecimal> montants = (NSArray<BigDecimal>) histosPourContrat.valueForKey(HistoCreditPositionne.HCP_MONTANT.key());
		BigDecimal totalMontants = (BigDecimal) montants.valueForKey(ERXKey.sum().key());
		if (totalMontants.signum() == 1) {
			NSArray<EOEngagementBudget> engagementBudgets =
					ERXArrayUtilities.flatten(
							(NSArray<EOEngagementBudget>) histosPourContrat.valueForKey(HistoCreditPositionne.ENGAGEMENT_BUDGETS.key()),
							true
							);
			engagementBudgets = ERXArrayUtilities.arrayWithoutDuplicates(engagementBudgets);
			totalResteEngage = (BigDecimal) engagementBudgets.valueForKey(ERXKey.sum(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE).key());
		}
		return totalResteEngage;
	}

	public BigDecimal totalReportablePourConventionSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {
		Tranche tranche = ERXQ.first(convention.tranches(), Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));
		BigDecimal totalMontants = tranche.totalContributionsPlusReportNmoins1();
		BigDecimal totalDepenses = totalDepensesPourConventionSurExerciceTypeCreditEtOrgan(convention, exercice, null, null);
		return totalMontants.subtract(totalDepenses);
	}

	public Boolean tousLesEngagementsDeLaConventionSimpleSontSoldesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		// On regarde d'abord les engagements liés directement à la convention
		NSArray<EOEngagementControleConvention> engagementControleConventions =
				ERXQ.filtered(
						convention.engagementControleConventions(),
						EOEngagementControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue())
						);

		NSArray<BigDecimal> engagementControleConventionMontantRestes =
				(NSArray<BigDecimal>) engagementControleConventions.valueForKey(EOEngagementControleConvention.ECON_MONTANT_BUDGETAIRE_RESTE.key());

		BigDecimal totalEngagementControleConventionMontantRestes = (BigDecimal) engagementControleConventionMontantRestes.valueForKey(ERXKey.sum().key());

		if (totalEngagementControleConventionMontantRestes.signum() == 1) {
			return false;
		}

		// On regarde ensuite les engagement ne passant par les dépenses liées à la convention

		NSArray<EODepenseControleConvention> depenseControleConventions =
				ERXQ.filtered(
						convention.depenseControleConventions(),
						EODepenseControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue())
						);

		NSArray<BigDecimal> depenseControleConventionMontantRestes =
				(NSArray<BigDecimal>) depenseControleConventions.valueForKeyPath(
						EODepenseControleConvention.DEPENSE_BUDGET
								.dot(EODepenseBudget.ENGAGEMENT_BUDGET)
								.dot(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE).key());
		BigDecimal totalDepenseControleConventionMontantRestes = (BigDecimal) depenseControleConventionMontantRestes.valueForKey(ERXKey.sum().key());

		if (totalDepenseControleConventionMontantRestes.signum() == 1) {
			return false;
		}

		return true;

	}

	public Boolean tousLesEngagementsDeLaConventionRaSontSoldesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		NSArray<Tranche> tranches = convention.tranches(Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));
		NSArray<BigDecimal> motantRestes = ERXArrayUtilities.flatten(
				(NSArray<BigDecimal>) tranches.valueForKeyPath(
						Tranche.HISTO_CREDIT_POSITIONNES
								.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
								.dot(EOEngagementBudget.ENG_MONTANT_BUDGETAIRE_RESTE).key()
						)
				);

		BigDecimal totalMontantRestes = (BigDecimal) motantRestes.valueForKey(ERXKey.sum().key());
		if (totalMontantRestes.signum() == 1) {
			return false;
		} else {
			return true;
		}
	}

	public Boolean tousLesEngagementsDeLaConventionSontSoldesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {
		Boolean modeRa = convention.isModeRA();
		Boolean tousSoldes;

		if (modeRa) {
			tousSoldes = tousLesEngagementsDeLaConventionRaSontSoldesSurExercice(convention, exercice);

		} else {
			tousSoldes = tousLesEngagementsDeLaConventionSimpleSontSoldesSurExercice(convention, exercice);
		}

		return tousSoldes;
	}

	public Boolean toutesLesDepensesDeLaConventionSimpleSontViseesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		NSArray<EODepenseControleConvention> depenseControleConventions =
				ERXQ.filtered(
						convention.depenseControleConventions(),
						EODepenseControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue())
						);

		NSArray<String> manEtats =
				ERXArrayUtilities.flatten(
						(NSArray<EOMandat>) depenseControleConventions.valueForKeyPath(
								EODepenseControleConvention.DEPENSE_BUDGET
										.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
										.dot(EODepenseControlePlanComptable.MANDAT)
										.dot(EOMandat.MAN_ETAT).key())
						);

		manEtats = ERXArrayUtilities.removeNullValues(manEtats);
		manEtats = ERXArrayUtilities.arrayWithoutDuplicates(manEtats);

		manEtats.remove(EOMandat.ETAT_VISE);
		manEtats.remove(EOMandat.ETAT_PAYE);

		return manEtats.isEmpty();

	}

	public Boolean toutesLesDepensesDeLaConventionRaSontViseesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		NSArray<Tranche> tranches = convention.tranches(Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));

		NSArray<String> manEtats =
				ERXArrayUtilities.flatten(
						(NSArray<EOMandat>) tranches.valueForKeyPath(
								Tranche.HISTO_CREDIT_POSITIONNES
										.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS)
										.dot(EOEngagementBudget.DEPENSE_BUDGETS)
										.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
										.dot(EODepenseControlePlanComptable.MANDAT)
										.dot(EOMandat.MAN_ETAT).key())
						);

		manEtats = ERXArrayUtilities.removeNullValues(manEtats);
		manEtats = ERXArrayUtilities.arrayWithoutDuplicates(manEtats);

		manEtats.remove(EOMandat.ETAT_VISE);
		manEtats.remove(EOMandat.ETAT_PAYE);

		return manEtats.isEmpty();

	}

	public Boolean toutesLesDepensesDeLaConventionOntUnBordereau(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {
		Boolean modeRa = convention.isModeRA();
		Boolean toutesVisees;

		if (modeRa) {
			toutesVisees = toutesLesDepensesDeLaConventionRaOntUnBordereau(convention, exercice);

		} else {
			toutesVisees = toutesLesDepensesDeLaConventionSimpleOntUnBordereau(convention, exercice);
		}

		return toutesVisees;
	}

	public Boolean toutesLesDepensesDeLaConventionSimpleOntUnBordereau(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		NSArray<EODepenseControleConvention> depenseControleConventions =
				ERXQ.filtered(
						convention.depenseControleConventions(),
						EODepenseControleConvention.EXERCICE.dot(EOExercice.EXE_EXERCICE).eq(exercice.exeExercice().intValue())
						);

		NSArray<EOMandat> mandats =
				ERXArrayUtilities.flatten(
						(NSArray<EOMandat>) depenseControleConventions.valueForKeyPath(
								EODepenseControleConvention.DEPENSE_BUDGET
										.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
										.dot(EODepenseControlePlanComptable.MANDAT).key())
						);

		if (mandats.contains(NSKeyValueCoding.NullValue)) {
			return false;
		}

		return true;

	}

	public Boolean toutesLesDepensesDeLaConventionRaOntUnBordereau(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {

		NSArray<Tranche> tranches = convention.tranches(Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exercice.exeExercice()));

		//Extraire du controle les engagements sans depense associée
		@SuppressWarnings("unchecked")
		NSArray<EOEngagementBudget> engagementsAvecDepense = ERXArrayUtilities.flatten(
				(NSArray<EOEngagementBudget>) tranches.valueForKeyPath(Tranche.HISTO_CREDIT_POSITIONNES.dot(HistoCreditPositionne.ENGAGEMENT_BUDGETS).key())
				);
		engagementsAvecDepense = EOQualifier.filteredArrayWithQualifier(engagementsAvecDepense, ERXQ.isNotNull(EOEngagementBudget.DEPENSE_BUDGETS_KEY + "." + _IDepenseBudget.DEP_MONTANT_BUDGETAIRE_KEY));

		@SuppressWarnings("unchecked")
		NSArray<EOMandat> mandats =
				ERXArrayUtilities.flatten(
						(NSArray<EOMandat>) engagementsAvecDepense.valueForKeyPath(
								EOEngagementBudget.DEPENSE_BUDGETS
										.dot(EODepenseBudget.DEPENSE_CONTROLE_PLAN_COMPTABLES)
										.dot(EODepenseControlePlanComptable.MANDAT).key())
						);

		if (mandats.contains(NSKeyValueCoding.NullValue)) {
			return false;
		}
		return true;
	}

	public Boolean toutesLesDepensesDeLaConventionSontViseesSurExercice(Contrat convention, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice) {
		Boolean modeRa = convention.isModeRA();
		Boolean toutesVisees;

		if (modeRa) {
			toutesVisees = toutesLesDepensesDeLaConventionRaSontViseesSurExercice(convention, exercice);

		} else {
			toutesVisees = toutesLesDepensesDeLaConventionSimpleSontViseesSurExercice(convention, exercice);
		}

		return toutesVisees;
	}

}
