/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXS;
import er.extensions.eof.ERXSortOrdering;
import er.extensions.foundation.ERXArrayUtilities;

public class VDepensesTranche extends EOVDepensesTranche {

    public VDepensesTranche() {
        super();
    }
    
    public static NSArray<VCreditsPositionnes> creditsPositionnes(EOEditingContext ec, Tranche tranche, EOPlanComptableExer planco) {
        EOQualifier qual = VCreditsPositionnes.TRANCHE.eq(tranche).and(VCreditsPositionnes.PLANCO.eq(planco));
        ERXSortOrdering ordering = ERXS.desc(VCreditsPositionnes.TYPE_CREDIT_KEY + "." + EOTypeCredit.TCD_ORDRE_KEY);
        EOFetchSpecification fspec = new EOFetchSpecification(VCreditsPositionnes.ENTITY_NAME, qual, ordering.array());
        fspec.setRefreshesRefetchedObjects(true);
        return ec.objectsWithFetchSpecification(fspec);
    }
    
    public NSArray<VCreditsPositionnes> creditsPositionnes() {
        return creditsPositionnes(editingContext(), tranche(), planco());
    }
    
    public BigDecimal montantCreditsPositionnes() {
        return (BigDecimal)creditsPositionnes().valueForKey("@sum." + VCreditsPositionnes.MONTANT_KEY);
    }

    public BigDecimal montantResteAPositionne() {
        return montant().subtract(montantCreditsPositionnes());
    }
    
    public NSArray<EOOrgan> organsForDepenses() {
        EOQualifier qual = SbDepense.TRANCHE.eq(tranche()).and(SbDepense.PLANCO.eq(planco()));
        NSArray<SbDepense> depenses = SbDepense.fetchAll(editingContext(), qual, null);
        return ERXArrayUtilities.arrayWithoutDuplicates((NSArray<EOOrgan>)depenses.valueForKey(Budgetable.ORGAN_KEY));
    }
    
}
