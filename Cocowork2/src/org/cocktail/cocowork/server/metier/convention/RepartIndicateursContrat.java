// RepartIndicateursContrat.java
// Created on Thu Mar 12 16:18:14 Europe/Paris 2009 by Apple EOModeler Version 5.2

package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.cocowork.common.tools.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;



public class RepartIndicateursContrat extends EORepartIndicateursContrat {

    public static final String ENTITY_NAME = "CWRepartIndicateursContrat";

    public static final String ENTITY_TABLE_NAME = "ACCORDS.REPART_INDICATEURS_CONTRAT";

    public RepartIndicateursContrat() {
        super();
    }

	/**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee.
	 * @throws InstantiationException Probleme d'instanciation.
	 */
	static public RepartIndicateursContrat instanciate(EOEditingContext ec) throws Exception {
		RepartIndicateursContrat uneRic = (RepartIndicateursContrat) Factory.instanceForEntity(ec, ENTITY_NAME);
		return uneRic;
	}
}
