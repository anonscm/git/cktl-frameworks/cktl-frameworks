// DO NOT EDIT.  Make changes to HistoCreditPositionneRec.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOHistoCreditPositionneRec extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWHistoCreditPositionneRec";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> HCPR_DATE = new ERXKey<NSTimestamp>("hcprDate");
  public static final ERXKey<java.math.BigDecimal> HCPR_MONTANT = new ERXKey<java.math.BigDecimal>("hcprMontant");
  public static final ERXKey<String> HCPR_SUPPR = new ERXKey<String>("hcprSuppr");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan> ORGAN = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan>("organ");
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String HCPR_DATE_KEY = HCPR_DATE.key();
  public static final String HCPR_MONTANT_KEY = HCPR_MONTANT.key();
  public static final String HCPR_SUPPR_KEY = HCPR_SUPPR.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOHistoCreditPositionneRec.class);

  public HistoCreditPositionneRec localInstanceIn(EOEditingContext editingContext) {
    HistoCreditPositionneRec localInstance = (HistoCreditPositionneRec)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp hcprDate() {
    return (NSTimestamp) storedValueForKey(EOHistoCreditPositionneRec.HCPR_DATE_KEY);
  }

  public void setHcprDate(NSTimestamp value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
        EOHistoCreditPositionneRec.LOG.debug( "updating hcprDate from " + hcprDate() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.HCPR_DATE_KEY);
  }

  public java.math.BigDecimal hcprMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOHistoCreditPositionneRec.HCPR_MONTANT_KEY);
  }

  public void setHcprMontant(java.math.BigDecimal value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
        EOHistoCreditPositionneRec.LOG.debug( "updating hcprMontant from " + hcprMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.HCPR_MONTANT_KEY);
  }

  public String hcprSuppr() {
    return (String) storedValueForKey(EOHistoCreditPositionneRec.HCPR_SUPPR_KEY);
  }

  public void setHcprSuppr(String value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
        EOHistoCreditPositionneRec.LOG.debug( "updating hcprSuppr from " + hcprSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.HCPR_SUPPR_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EOHistoCreditPositionneRec.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneRec.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneRec.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneRec.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(EOHistoCreditPositionneRec.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneRec.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneRec.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneRec.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(EOHistoCreditPositionneRec.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneRec.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneRec.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneRec.PLANCO_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOHistoCreditPositionneRec.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneRec.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneRec.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneRec.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(EOHistoCreditPositionneRec.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOHistoCreditPositionneRec.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (EOHistoCreditPositionneRec.LOG.isDebugEnabled()) {
      EOHistoCreditPositionneRec.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionneRec.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionneRec.TYPE_CREDIT_KEY);
    }
  }
  

  public static HistoCreditPositionneRec create(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.cocowork.server.metier.convention.Tranche tranche, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit) {
    HistoCreditPositionneRec eo = (HistoCreditPositionneRec) EOUtilities.createAndInsertInstance(editingContext, EOHistoCreditPositionneRec.ENTITY_NAME);    
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  public static ERXFetchSpecification<HistoCreditPositionneRec> fetchSpec() {
    return new ERXFetchSpecification<HistoCreditPositionneRec>(EOHistoCreditPositionneRec.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<HistoCreditPositionneRec> fetchAll(EOEditingContext editingContext) {
    return EOHistoCreditPositionneRec.fetchAll(editingContext, null);
  }

  public static NSArray<HistoCreditPositionneRec> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOHistoCreditPositionneRec.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<HistoCreditPositionneRec> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<HistoCreditPositionneRec> fetchSpec = new ERXFetchSpecification<HistoCreditPositionneRec>(EOHistoCreditPositionneRec.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<HistoCreditPositionneRec> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static HistoCreditPositionneRec fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionneRec.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionneRec fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<HistoCreditPositionneRec> eoObjects = EOHistoCreditPositionneRec.fetchAll(editingContext, qualifier, null);
    HistoCreditPositionneRec eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWHistoCreditPositionneRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionneRec fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionneRec.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionneRec fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    HistoCreditPositionneRec eoObject = EOHistoCreditPositionneRec.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWHistoCreditPositionneRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionneRec localInstanceIn(EOEditingContext editingContext, HistoCreditPositionneRec eo) {
    HistoCreditPositionneRec localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}