package org.cocktail.cocowork.server.metier.convention.service;

import java.math.BigDecimal;

import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;

import com.google.inject.Singleton;

/**
 * Calcul du montant de participation disponible en incluant les frais de gestion.
 * 
 * Exemple :
 * <pre>
 *        1 contribution de 1000 et 10 frais de gestion 
 *        => Montant participation = 1000
 * </pre>
 * Ce mode de calcul n'est pas celui par défaut, il doit être paramétré explicitement 
 * 
 * @see CalculMontantParticipationDisponible
 * 
 * @author Alexis Tual
 *
 */
@Singleton
public class CalculMontantParticipationDisponibleFraisInclus implements CalculMontantParticipationDisponible {

    /**
     * {@inheritDoc}
     */
    public BigDecimal montantParticipationDisponible(RepartPartenaireTranche repartPartenaireTranche) {
        BigDecimal montantContrib = BigDecimal.ZERO;
        if (repartPartenaireTranche != null) {
            montantContrib = repartPartenaireTranche.rptMontantParticipation();
        }
        return montantContrib;
    }

}
