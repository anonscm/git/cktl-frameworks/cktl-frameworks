// DO NOT EDIT.  Make changes to ContratPartenaire.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOContratPartenaire extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWContratPartenaire";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> CP_DATE_SIGNATURE = new ERXKey<NSTimestamp>("cpDateSignature");
  public static final ERXKey<java.math.BigDecimal> CP_MONTANT = new ERXKey<java.math.BigDecimal>("cpMontant");
  public static final ERXKey<String> CP_PRINCIPAL = new ERXKey<String>("cpPrincipal");
  public static final ERXKey<String> CP_REFERENCE_EXTERNE = new ERXKey<String>("cpReferenceExterne");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartContact> CONTRAT_PART_CONTACTS = new ERXKey<org.cocktail.cocowork.server.metier.convention.ContratPartContact>("contratPartContacts");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> CONTRIBUTIONS = new ERXKey<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>("contributions");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.TypePartenaire> TYPE_PARTENAIRE = new ERXKey<org.cocktail.cocowork.server.metier.convention.TypePartenaire>("typePartenaire");

  // Attributes
  public static final String CP_DATE_SIGNATURE_KEY = CP_DATE_SIGNATURE.key();
  public static final String CP_MONTANT_KEY = CP_MONTANT.key();
  public static final String CP_PRINCIPAL_KEY = CP_PRINCIPAL.key();
  public static final String CP_REFERENCE_EXTERNE_KEY = CP_REFERENCE_EXTERNE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String CONTRAT_PART_CONTACTS_KEY = CONTRAT_PART_CONTACTS.key();
  public static final String CONTRIBUTIONS_KEY = CONTRIBUTIONS.key();
  public static final String TYPE_PARTENAIRE_KEY = TYPE_PARTENAIRE.key();

  private static Logger LOG = Logger.getLogger(EOContratPartenaire.class);

  public ContratPartenaire localInstanceIn(EOEditingContext editingContext) {
    ContratPartenaire localInstance = (ContratPartenaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp cpDateSignature() {
    return (NSTimestamp) storedValueForKey(EOContratPartenaire.CP_DATE_SIGNATURE_KEY);
  }

  public void setCpDateSignature(NSTimestamp value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
        EOContratPartenaire.LOG.debug( "updating cpDateSignature from " + cpDateSignature() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartenaire.CP_DATE_SIGNATURE_KEY);
  }

  public java.math.BigDecimal cpMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOContratPartenaire.CP_MONTANT_KEY);
  }

  public void setCpMontant(java.math.BigDecimal value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
        EOContratPartenaire.LOG.debug( "updating cpMontant from " + cpMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartenaire.CP_MONTANT_KEY);
  }

  public String cpPrincipal() {
    return (String) storedValueForKey(EOContratPartenaire.CP_PRINCIPAL_KEY);
  }

  public void setCpPrincipal(String value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
        EOContratPartenaire.LOG.debug( "updating cpPrincipal from " + cpPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartenaire.CP_PRINCIPAL_KEY);
  }

  public String cpReferenceExterne() {
    return (String) storedValueForKey(EOContratPartenaire.CP_REFERENCE_EXTERNE_KEY);
  }

  public void setCpReferenceExterne(String value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
        EOContratPartenaire.LOG.debug( "updating cpReferenceExterne from " + cpReferenceExterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartenaire.CP_REFERENCE_EXTERNE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(EOContratPartenaire.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
        EOContratPartenaire.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, EOContratPartenaire.PERS_ID_KEY);
  }

  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EOContratPartenaire.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EOContratPartenaire.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartenaire.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartenaire.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.TypePartenaire typePartenaire() {
    return (org.cocktail.cocowork.server.metier.convention.TypePartenaire)storedValueForKey(EOContratPartenaire.TYPE_PARTENAIRE_KEY);
  }
  
  public void setTypePartenaire(org.cocktail.cocowork.server.metier.convention.TypePartenaire value) {
    takeStoredValueForKey(value, EOContratPartenaire.TYPE_PARTENAIRE_KEY);
  }

  public void setTypePartenaireRelationship(org.cocktail.cocowork.server.metier.convention.TypePartenaire value) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("updating typePartenaire from " + typePartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypePartenaire(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.TypePartenaire oldValue = typePartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOContratPartenaire.TYPE_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOContratPartenaire.TYPE_PARTENAIRE_KEY);
    }
  }
  
  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact> contratPartContacts() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact>)storedValueForKey(EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact> contratPartContacts(EOQualifier qualifier) {
    return contratPartContacts(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact> contratPartContacts(EOQualifier qualifier, boolean fetch) {
    return contratPartContacts(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact> contratPartContacts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.ContratPartContact.CONTRAT_PARTENAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.ContratPartContact.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contratPartContacts();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.ContratPartContact>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContratPartContacts(org.cocktail.cocowork.server.metier.convention.ContratPartContact object) {
    includeObjectIntoPropertyWithKey(object, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
  }

  public void removeFromContratPartContacts(org.cocktail.cocowork.server.metier.convention.ContratPartContact object) {
    excludeObjectFromPropertyWithKey(object, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
  }

  public void addToContratPartContactsRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartContact object) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("adding " + object + " to contratPartContacts relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToContratPartContacts(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
    }
  }

  public void removeFromContratPartContactsRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartContact object) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("removing " + object + " from contratPartContacts relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromContratPartContacts(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.ContratPartContact createContratPartContactsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.ContratPartContact.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.ContratPartContact) eo;
  }

  public void deleteContratPartContactsRelationship(org.cocktail.cocowork.server.metier.convention.ContratPartContact object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRAT_PART_CONTACTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContratPartContactsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.ContratPartContact> objects = contratPartContacts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContratPartContactsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> contributions() {
    return (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)storedValueForKey(EOContratPartenaire.CONTRIBUTIONS_KEY);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> contributions(EOQualifier qualifier) {
    return contributions(qualifier, null, false);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> contributions(EOQualifier qualifier, boolean fetch) {
    return contributions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> contributions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.CONTRAT_PARTENAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contributions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContributions(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    includeObjectIntoPropertyWithKey(object, EOContratPartenaire.CONTRIBUTIONS_KEY);
  }

  public void removeFromContributions(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    excludeObjectFromPropertyWithKey(object, EOContratPartenaire.CONTRIBUTIONS_KEY);
  }

  public void addToContributionsRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("adding " + object + " to contributions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToContributions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRIBUTIONS_KEY);
    }
  }

  public void removeFromContributionsRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    if (EOContratPartenaire.LOG.isDebugEnabled()) {
      EOContratPartenaire.LOG.debug("removing " + object + " from contributions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromContributions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRIBUTIONS_KEY);
    }
  }

  public org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche createContributionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOContratPartenaire.CONTRIBUTIONS_KEY);
    return (org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche) eo;
  }

  public void deleteContributionsRelationship(org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOContratPartenaire.CONTRIBUTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContributionsRelationships() {
    Enumeration<org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche> objects = contributions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContributionsRelationship(objects.nextElement());
    }
  }


  public static ContratPartenaire create(EOEditingContext editingContext, String cpPrincipal
, Integer persId
, org.cocktail.cocowork.server.metier.convention.Contrat contrat) {
    ContratPartenaire eo = (ContratPartenaire) EOUtilities.createAndInsertInstance(editingContext, EOContratPartenaire.ENTITY_NAME);    
        eo.setCpPrincipal(cpPrincipal);
        eo.setPersId(persId);
    eo.setContratRelationship(contrat);
    return eo;
  }

  public static ERXFetchSpecification<ContratPartenaire> fetchSpec() {
    return new ERXFetchSpecification<ContratPartenaire>(EOContratPartenaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ContratPartenaire> fetchAll(EOEditingContext editingContext) {
    return EOContratPartenaire.fetchAll(editingContext, null);
  }

  public static NSArray<ContratPartenaire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOContratPartenaire.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<ContratPartenaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ContratPartenaire> fetchSpec = new ERXFetchSpecification<ContratPartenaire>(EOContratPartenaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ContratPartenaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ContratPartenaire fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOContratPartenaire.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ContratPartenaire fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ContratPartenaire> eoObjects = EOContratPartenaire.fetchAll(editingContext, qualifier, null);
    ContratPartenaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWContratPartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ContratPartenaire fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOContratPartenaire.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ContratPartenaire fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    ContratPartenaire eoObject = EOContratPartenaire.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWContratPartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ContratPartenaire localInstanceIn(EOEditingContext editingContext, ContratPartenaire eo) {
    ContratPartenaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}