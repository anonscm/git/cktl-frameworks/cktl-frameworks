// DO NOT EDIT.  Make changes to ProjetContrat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOProjetContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWProjetContrat";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat> CONTRAT = new ERXKey<org.cocktail.cocowork.server.metier.convention.Contrat>("contrat");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Projet> PROJET = new ERXKey<org.cocktail.cocowork.server.metier.convention.Projet>("projet");

  // Attributes
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String PROJET_KEY = PROJET.key();

  private static Logger LOG = Logger.getLogger(EOProjetContrat.class);

  public ProjetContrat localInstanceIn(EOEditingContext editingContext) {
    ProjetContrat localInstance = (ProjetContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.cocowork.server.metier.convention.Contrat contrat() {
    return (org.cocktail.cocowork.server.metier.convention.Contrat)storedValueForKey(EOProjetContrat.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    takeStoredValueForKey(value, EOProjetContrat.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.cocowork.server.metier.convention.Contrat value) {
    if (EOProjetContrat.LOG.isDebugEnabled()) {
      EOProjetContrat.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Contrat oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOProjetContrat.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOProjetContrat.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Projet projet() {
    return (org.cocktail.cocowork.server.metier.convention.Projet)storedValueForKey(EOProjetContrat.PROJET_KEY);
  }
  
  public void setProjet(org.cocktail.cocowork.server.metier.convention.Projet value) {
    takeStoredValueForKey(value, EOProjetContrat.PROJET_KEY);
  }

  public void setProjetRelationship(org.cocktail.cocowork.server.metier.convention.Projet value) {
    if (EOProjetContrat.LOG.isDebugEnabled()) {
      EOProjetContrat.LOG.debug("updating projet from " + projet() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setProjet(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Projet oldValue = projet();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOProjetContrat.PROJET_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOProjetContrat.PROJET_KEY);
    }
  }
  

  public static ProjetContrat create(EOEditingContext editingContext) {
    ProjetContrat eo = (ProjetContrat) EOUtilities.createAndInsertInstance(editingContext, EOProjetContrat.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<ProjetContrat> fetchSpec() {
    return new ERXFetchSpecification<ProjetContrat>(EOProjetContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ProjetContrat> fetchAll(EOEditingContext editingContext) {
    return EOProjetContrat.fetchAll(editingContext, null);
  }

  public static NSArray<ProjetContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOProjetContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<ProjetContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ProjetContrat> fetchSpec = new ERXFetchSpecification<ProjetContrat>(EOProjetContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ProjetContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ProjetContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOProjetContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ProjetContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ProjetContrat> eoObjects = EOProjetContrat.fetchAll(editingContext, qualifier, null);
    ProjetContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWProjetContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ProjetContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOProjetContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ProjetContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    ProjetContrat eoObject = EOProjetContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWProjetContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ProjetContrat localInstanceIn(EOEditingContext editingContext, ProjetContrat eo) {
    ProjetContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}