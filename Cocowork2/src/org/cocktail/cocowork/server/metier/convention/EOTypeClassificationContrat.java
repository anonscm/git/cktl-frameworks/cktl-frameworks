// DO NOT EDIT.  Make changes to TypeClassificationContrat.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeClassificationContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeClassificationContrat";

  // Attribute Keys
  public static final ERXKey<String> TCC_CODE = new ERXKey<String>("tccCode");
  public static final ERXKey<Integer> TCC_ID = new ERXKey<Integer>("tccId");
  public static final ERXKey<String> TCC_LIBELLE = new ERXKey<String>("tccLibelle");
  // Relationship Keys

  // Attributes
  public static final String TCC_CODE_KEY = TCC_CODE.key();
  public static final String TCC_ID_KEY = TCC_ID.key();
  public static final String TCC_LIBELLE_KEY = TCC_LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeClassificationContrat.class);

  public TypeClassificationContrat localInstanceIn(EOEditingContext editingContext) {
    TypeClassificationContrat localInstance = (TypeClassificationContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tccCode() {
    return (String) storedValueForKey(EOTypeClassificationContrat.TCC_CODE_KEY);
  }

  public void setTccCode(String value) {
    if (EOTypeClassificationContrat.LOG.isDebugEnabled()) {
        EOTypeClassificationContrat.LOG.debug( "updating tccCode from " + tccCode() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeClassificationContrat.TCC_CODE_KEY);
  }

  public Integer tccId() {
    return (Integer) storedValueForKey(EOTypeClassificationContrat.TCC_ID_KEY);
  }

  public void setTccId(Integer value) {
    if (EOTypeClassificationContrat.LOG.isDebugEnabled()) {
        EOTypeClassificationContrat.LOG.debug( "updating tccId from " + tccId() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeClassificationContrat.TCC_ID_KEY);
  }

  public String tccLibelle() {
    return (String) storedValueForKey(EOTypeClassificationContrat.TCC_LIBELLE_KEY);
  }

  public void setTccLibelle(String value) {
    if (EOTypeClassificationContrat.LOG.isDebugEnabled()) {
        EOTypeClassificationContrat.LOG.debug( "updating tccLibelle from " + tccLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeClassificationContrat.TCC_LIBELLE_KEY);
  }


  public static TypeClassificationContrat create(EOEditingContext editingContext, Integer tccId
) {
    TypeClassificationContrat eo = (TypeClassificationContrat) EOUtilities.createAndInsertInstance(editingContext, EOTypeClassificationContrat.ENTITY_NAME);    
        eo.setTccId(tccId);
    return eo;
  }

  public static ERXFetchSpecification<TypeClassificationContrat> fetchSpec() {
    return new ERXFetchSpecification<TypeClassificationContrat>(EOTypeClassificationContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeClassificationContrat> fetchAll(EOEditingContext editingContext) {
    return EOTypeClassificationContrat.fetchAll(editingContext, null);
  }

  public static NSArray<TypeClassificationContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeClassificationContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeClassificationContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeClassificationContrat> fetchSpec = new ERXFetchSpecification<TypeClassificationContrat>(EOTypeClassificationContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeClassificationContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeClassificationContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeClassificationContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeClassificationContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeClassificationContrat> eoObjects = EOTypeClassificationContrat.fetchAll(editingContext, qualifier, null);
    TypeClassificationContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeClassificationContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeClassificationContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeClassificationContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeClassificationContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeClassificationContrat eoObject = EOTypeClassificationContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeClassificationContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeClassificationContrat localInstanceIn(EOEditingContext editingContext, TypeClassificationContrat eo) {
    TypeClassificationContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeClassificationContrat.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOTypeClassificationContrat.ENTITY_NAME);
    return (NSArray<org.cocktail.cocowork.server.metier.convention.TypeClassificationContrat>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}