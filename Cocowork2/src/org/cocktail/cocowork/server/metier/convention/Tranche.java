package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.util.Map.Entry;

import org.cocktail.cocowork.common.tools.Constantes;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryTranche;
import org.cocktail.cocowork.server.metier.convention.procedure.suividepense.ProcedureGetTotalDepense;
import org.cocktail.cocowork.server.metier.convention.procedure.suividepense.ProcedureGetTotalLiquide;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeApplication;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXProperties;
import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;

public class Tranche extends EOTranche
{
    public static final ERXKey<Integer> CON_ORDRE = new ERXKey<Integer>("conOrdre");
    
    public static final String NATURE_DEPENSE = "DEPENSE";
	public static final String NATURE_RECETTE = "RECETTE";
	public static final String NATURE_DEPENSE_RECETTE = "DEPENSE_RECETTE";

	public static final String NATURE_DEPENSE_LIBELLE = "D\u00E9pense";
	public static final String NATURE_RECETTE_LIBELLE = "Recette";
	public static final String NATURE_DEPENSE_RECETTE_LIBELLE = "Recette & D\u00E9pense";
	
	public static final String TRA_SUPPR_OUI = "O";
	public static final String TRA_SUPPR_NON = "N";
	
	public static final String TRA_VALIDE_OUI = "O";
	public static final String TRA_VALIDE_NON = "N";

	public final static EOQualifier QUALIFIER_NON_SUPPR = 
		EOQualifier.qualifierWithQualifierFormat("traSuppr='N'", null);

	public final static EOSortOrdering SORT_EXERCICE_ASC = 
		EOSortOrdering.sortOrderingWithKey("exerciceCocktail.exeExercice", EOSortOrdering.CompareAscending);
	
	public static final ERXKey<BigDecimal> RESTE_A_POSITIONNE = new ERXKey<BigDecimal>("resteAPositionne");
	
	/**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee
	 * @throws InstantiationException Probleme d'instanciation.
	 */
	static public Tranche instanciate(EOEditingContext ec) throws Exception {
		return (Tranche) Factory.instanceForEntity(ec, ENTITY_NAME);
	}
	
    public void setTraMontantInit(BigDecimal aValue) {
        super.setTraMontantInit(aValue);
        if (aValue != null && ( traMontant() == null || traMontant().doubleValue()==0 )) {
        	setTraMontant(aValue);
        }
    }

    @Override
    public BigDecimal traMontant() {
    	BigDecimal traMontant = super.traMontant();
    	traMontant = totalContributionsPlusReportNmoins1().subtract(reportNplus1());
        return traMontant;
    }
    
    public BigDecimal traMontantFraisInclus() {
        BigDecimal traMontant = totalContributions();
        // On ajoute le report de l'annee precedente et on soustrait celui de l'année suivante
        traMontant = traMontant.add(reportNmoins1()).subtract(reportNplus1());
        return traMontant;
    }
    
    @Override
    public BigDecimal reportNmoins1() {
      // Verification car si on a 0 dans Oracle ca retourne null au lieu de 0
      BigDecimal reportNmoins1 = super.reportNmoins1();
      if(reportNmoins1 == null) {
        setReportNmoins1(BigDecimal.ZERO);
        return BigDecimal.ZERO;
      }
      return reportNmoins1;
    }
    
    @Override
    public BigDecimal reportNplus1() {
      // Verification car si on a 0 dans Oracle ca retourne null au lieu de 0
      BigDecimal reportNplus1 = super.reportNplus1();
      if(reportNplus1 == null) {
        setReportNplus1(BigDecimal.ZERO);
        return BigDecimal.ZERO;
      }
      return reportNplus1;
    }
    
    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_DISPONIBLES = new ERXKey<BigDecimal>("totalContributionsDisponibles");
    public BigDecimal totalContributionsDisponibles() {
      BigDecimal montant = super.traMontant();
      montant = valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.MONTANT_PARTICIPATION_DISPONIBLE));
      return montant;
    }
    
    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS = new ERXKey<BigDecimal>("totalContributions");
    public BigDecimal totalContributions() {
        BigDecimal montant = super.traMontant();
        montant = valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.RPT_MONTANT_PARTICIPATION));
        return montant;
    }
    
    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_PLUS_REPORTS = new ERXKey<BigDecimal>("totalContributionsPlusReports");
    public BigDecimal totalContributionsPlusReports() {
    	BigDecimal montant = totalContributions().add(reportNmoins1());
    	return montant;
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_DISPONIBLES_PLUS_REPORT = new ERXKey<BigDecimal>("totalContributionsPlusReportNmoins1");
    public BigDecimal totalContributionsPlusReportNmoins1() {
        BigDecimal montant = totalContributionsDisponibles();
        return montant.add(reportNmoins1());
      }

    public BigDecimal fraisGestion() {
        return (BigDecimal)valueForKeyPath("toRepartPartenaireTranches.@sum.fraisGestionAsDecimal");
    }
    
    
    @Override
    public void setTraNatureMontant(String aValue) {
        super.setTraNatureMontant(aValue);
        // On remet à 0 le montant de la tranche par la suppression des repart
        // Si aucune nature (depense, recette ou depence_recette), 
        NSArray<RepartPartenaireTranche> reparts = toRepartPartenaireTranches().immutableClone();
        if (aValue == null) {
            for (RepartPartenaireTranche repart : reparts) {
                removeFromToRepartPartenaireTranchesRelationship(repart);
                editingContext().deleteObject(repart);
            }
            // on supprime les dépenses et recettes de la tranche
            NSArray<SbDepense> depenses = sbDepenses().immutableClone();
            for (SbDepense depense : depenses) {
                removeFromSbDepensesRelationship(depense);
                editingContext().deleteObject(depense);
            }
            // on supprime les dépenses et recettes de la tranche
            NSArray<SbRecette> recettes = sbRecettes().immutableClone();
            for (SbRecette recette : recettes) {
                removeFromSbRecettesRelationship(recette);
                editingContext().deleteObject(recette);
            }
        } else if (NATURE_DEPENSE.equals(aValue) || NATURE_RECETTE.equals(aValue) ||
                NATURE_DEPENSE_RECETTE.equals(aValue)) {
            // S'il n'y a pas de repartPartenaireTranche
            if (toRepartPartenaireTranches().isEmpty() && contrat() != null) {
                // Creation des repartPartenaireTranche
                NSArray<ContratPartenaire> partenaires = contrat().contratPartenaires();
                for (ContratPartenaire contratPartenaire : partenaires) {
                    RepartPartenaireTranche rpt;
                        rpt = RepartPartenaireTranche.create(editingContext(), null, null);
                        rpt.setRptMontantParticipation(BigDecimal.ZERO);
                        rpt.setRptTauxParticipation(BigDecimal.ZERO);
                        contratPartenaire.addToContributionsRelationship(rpt);
                        addToToRepartPartenaireTranchesRelationship(rpt);
                }
            }
        }
    }

    /**
     * @return
     */
    public BigDecimal sommeDepenses() {
        BigDecimal total = new BigDecimal(0);
        for (Object depense : sbDepenses())
            total = total.add(((SbDepense)depense).sdMontantHt());
        return total;
    }
    
    /**
     * @return
     */
    public BigDecimal sommeRecettes() {
        BigDecimal total = new BigDecimal(0);
        for (Object recette : sbRecettes())
            total = total.add(((SbRecette)recette).srMontantHt());
        return total;
    }
    
    /**
     * @return
     */
    public BigDecimal balance() {
        return sommeRecettes().subtract(sommeDepenses());
    }
    
    /**
     * @return
     */
    public BigDecimal sommeDepensesNature() {
        NSArray<SbDepense> depenses = sbDepenses(SbDepense.PLANCO.isNotNull());
        return (BigDecimal)depenses.valueForKey("@sum.sdMontantHt");
    }
    
    /**
     * @return
     */
    public BigDecimal sommeDepensesDestination() {
        NSArray<SbDepense> depenses = sbDepenses(SbDepense.LOLF.isNotNull());
        return (BigDecimal)depenses.valueForKey("@sum.sdMontantHt");
    }
    
    /**
     * @return
     */
    public BigDecimal sommeRecettesNature() {
        NSArray<SbRecette> recettes = sbRecettes(SbDepense.PLANCO.isNotNull());
        return (BigDecimal)recettes.valueForKey("@sum.srMontantHt");
    }
    
    /**
     * @return
     */
    public BigDecimal sommeRecettesDestination() {
        NSArray<SbRecette> recettes = sbRecettes(SbDepense.LOLF.isNotNull());
        return (BigDecimal)recettes.valueForKey("@sum.srMontantHt");
    }
    
    /**
     * @return
     */
    public BigDecimal balanceRecettesNatureDestination() {
        return sommeRecettesNature().subtract(sommeRecettesDestination());
    }
    
    /**
     * @return
     */
    public BigDecimal balanceDepensesNatureDestination() {
        return sommeDepensesNature().subtract(sommeDepensesDestination());
    }
    
    /**
     * @return
     */
    public BigDecimal balanceRecettesDepensesNature() {
        return sommeRecettesNature().subtract(sommeDepensesNature());
    }
    
    /**
     * @return
     */
    public BigDecimal balanceRecettesDepensesDestination() {
        return sommeRecettesDestination().subtract(sommeDepensesDestination());
    }
    
    /**
     * @return
     */
    public NSArray<VDepensesTranche> sommeDepensesByNature() {
        ERXFetchSpecification<VDepensesTranche> fspec = VDepensesTranche.fetchSpec();
        fspec.setRefreshesRefetchedObjects(true);
        fspec.setQualifier(VDepensesTranche.TRANCHE.eq(this));
        return fspec.fetchObjects(editingContext());
    }
    
    public NSArray<VRecettesTranche> sommeRecettesByNature() {
        ERXFetchSpecification<VRecettesTranche> fspec = VRecettesTranche.fetchSpec();
        fspec.setRefreshesRefetchedObjects(true);
        fspec.setQualifier(VRecettesTranche.TRANCHE.eq(this));
        return fspec.fetchObjects(editingContext());
    }

	// -- Contrôles DEPENSES
	public boolean isMontantTrancheLtSommeDepensesDestOuNature() {
		return isMontantTrancheLtSommeDepensesDest() || isMontantTrancheLtSommeDepensesNature();
	}

	public boolean isMontantTrancheLtSommeDepensesDest() {
		BigDecimal sommeDepensesDest = sommeDepensesDestination();
		return traMontant().compareTo(sommeDepensesDest) < 0;
	}

	public boolean isMontantTrancheLtSommeDepensesNature() {
		BigDecimal sommeDepensesNature = sommeDepensesNature();
		return traMontant().compareTo(sommeDepensesNature) < 0;
	}

	// -- Contrôles RECETTES
	public boolean isMontantTrancheLtSommeRecettesDestOuNature() {
		return isMontantTrancheLtSommeRecettesNature() || isMontantTrancheLtSommeRecettesDest();
	}

	public boolean isMontantTrancheLtSommeRecettesNature() {
		BigDecimal sommeRecettesNature = sommeRecettesNature();
		return traMontantFraisInclus().compareTo(sommeRecettesNature) < 0;
	}

	public boolean isMontantTrancheLtSommeRecettesDest() {
		BigDecimal sommeRecettesDestination = sommeRecettesDestination();
		return traMontantFraisInclus().compareTo(sommeRecettesDestination) < 0;
	}

    // -- Contrôles pour avenants 
    
    public boolean isMontantTrancheLtOrEqSommeDepensesNature() {
        BigDecimal sommeDepensesNature = sommeDepensesNature();
        return traMontant().compareTo(sommeDepensesNature) <= 0;
    }
    
    public boolean isMontantTrancheLtOrEqSommeDepensesDest() {
        BigDecimal sommeDepensesDest = sommeDepensesDestination();
        return traMontant().compareTo(sommeDepensesDest) <= 0;
    }
        
    public boolean isMontantTrancheLtOrEqSommeRecettesNature() {
        BigDecimal sommeRecettesNature = sommeRecettesNature();
        return traMontantFraisInclus().compareTo(sommeRecettesNature) <= 0;
    }
    
    public boolean isMontantTrancheLtOrEqSommeRecettesDest() {
        BigDecimal sommeRecettesDestination = sommeRecettesDestination();
        return traMontantFraisInclus().compareTo(sommeRecettesDestination) <= 0;
    }
    
    public boolean isMontantTrancheLtOrEqSommeRecettesDestEtNature() {
        BigDecimal sommeRecettesDestination = sommeRecettesDestination();
        BigDecimal sommeRecettesNature = sommeRecettesNature();
        return traMontant().compareTo(sommeRecettesDestination) <= 0 && traMontant().compareTo(sommeRecettesNature) <= 0;
    }
    
    public EOExercice exercice() {
        EOExercice exo = exerciceCocktail().exercice();
        return exo;
    }
    
//    public EOExercice exerciceValide(EOExercice dernierExerciceOuvert) {
//        if (exerciceCocktail() != null) {
//            EOExercice exo = exerciceCocktail().exercice();
//            if (exo != null && (!exo.estClos()))
//                return exo;
//        }
//        return dernierExerciceOuvert;
//    }

    public boolean isNatureDepense() {
        return NATURE_DEPENSE.equals(traNatureMontant());
    }

    public boolean isNatureRecette() {
        return NATURE_RECETTE.equals(traNatureMontant());
    }
    
    public boolean isNatureDepenseRecette() {
        return NATURE_DEPENSE_RECETTE.equals(traNatureMontant());
    }
    
    /** [BEGIN] MODE PREVISION BUDGETAIRE TOUTE INTEGREE, EN SUSPENS DEPUIS DEPART JMC  **/
    public NSArray<SbDepense> depensesSansPrevision() {
        return sbDepenses(ERXQ.isNull(Budgetable.PREVISION_BUDGET_KEY));
    }
    
    public NSArray<SbRecette> recettesSansPrevision() {
        return sbRecettes(ERXQ.isNull(Budgetable.PREVISION_BUDGET_KEY));
    }
    
    public NSArray<Budgetable> budgetablesSansPrevisionAvecOrgan() {
        EOQualifier qual = ERXQ.isNull(Budgetable.PREVISION_BUDGET_KEY).and(ERXQ.isNotNull(Budgetable.ORGAN_KEY));
        NSMutableArray<Budgetable> budgetables = new NSMutableArray<Budgetable>();
        budgetables.addObjectsFromArray(sbDepenses(qual));
        budgetables.addObjectsFromArray(sbRecettes(qual));
        return budgetables.immutableClone();
    }
    
    public void createPrevisionBudgetNatureLolfs(EOPrevisionBudget prev, NSArray<Budgetable> budgetables) {
        EOEditingContext ec = editingContext();
        for (Budgetable budgetable : budgetables) {
            Budgetable budgetableLocal = ERXEOControlUtilities.localInstanceOfObject(ec, budgetable);
            if (budgetableLocal.lolf() != null && budgetableLocal.typeCredit() != null) {
                // Fetch de nomenclatures
                EOTypeAction lolf = EOTypeAction.findWithId(ec, (Integer)ERXEOControlUtilities.primaryKeyObjectForObject(budgetableLocal.lolf()));
                EOTypeCredit typeCredit = budgetableLocal.typeCredit();
                // Aggregation par croisé complet nature, tc, lolf
                if (budgetableLocal.repartPlanComptables() != null && !budgetableLocal.repartPlanComptables().isEmpty()) {
                    for (IRepartMontant repart : budgetableLocal.repartPlanComptables()) {
                        EOPlanComptable planCo = (EOPlanComptable)repart.nomenclature();
                        // on récupère ou crée un PropBudgetNature si n'existe pas déjà pour cette proposition et ce tc/nature
                        EOPlanComptableExer planCoExer = 
                            EOPlanComptableExer.fetchWithPcoNumAndExercice(ec, planCo.pcoNum(), prev.exercice().exeExercice().intValue());
                        // on récupère ou crée un PropBudgetNatureLolf 
                        EOPrevisionBudgetNatureLolf prevNatureLolf = prev.getOrCreatePrevisionBudgetNatureLolf(planCoExer, lolf, typeCredit);
                        prevNatureLolf.addToMontant(repart.montantHt());
                    }
                    // on ajoute la depense à la prev
                    budgetableLocal.setPrevisionBudgetRelationship(prev);
                }
            }
        }
    }
    
    public EOPrevisionBudget getOrCreatePrevisionBudget(
            EOExercice exo,
            EOTypeApplication typeApp, 
            EOTypeEtat typeEtat, 
            EOOrgan organ,
            org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utl,
            NSArray<Budgetable> budgetables) {
        EOEditingContext ec = editingContext();
        // On cherche parmi les previsions existantes qui ne sont pas rejetees
        EOQualifier qual =
            ERXQ.equals(EOPrevisionBudget.EXERCICE_KEY, exo)
                .and(ERXQ.equals(EOPrevisionBudget.TYPE_APPLICATION_KEY, typeApp))
                .and(ERXQ.equals(EOPrevisionBudget.ORGAN_KEY, organ))
                .and(ERXQ.notEquals(EOPrevisionBudget.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOTypeEtat.ETAT_BLOCAGE));
        NSArray<EOPrevisionBudget> prevs = previsionsBudget(qual);
        EOPrevisionBudget prev = null;
        if (prevs.isEmpty()) {
            prev = EOPrevisionBudget.creerInstance(ec);
            prev.setExerciceRelationship(exo.localInstanceIn(ec));
            prev.setOrganRelationship(organ.localInstanceIn(ec));
            prev.setTypeApplicationRelationship(typeApp.localInstanceIn(ec));
            prev.setTypeEtat(typeEtat.localInstanceIn(ec));
            prev.setUtilisateurRelationship(utl.localInstanceIn(ec));
            prev.setPvbuDate(new NSTimestamp());
            addToPrevisionsBudgetRelationship(prev);
        } else {
            prev = prevs.lastObject();
        }
        createPrevisionBudgetNatureLolfs(prev, budgetables);
        return prev;
    }
    
    public void createPrevisionsBudgetAuto(
            EOExercice exo, 
            org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utl) {
        // Le resultat
        NSMutableArray<EOPrevisionBudget> prevs = new NSMutableArray<EOPrevisionBudget>();
        // On trie les depenses par organ UB ou CR
        NSDictionary<EOOrgan, NSArray<Budgetable>> budgetablesForOrgans = 
            ERXArrayUtilities.arrayGroupedByKeyPath(budgetablesSansPrevisionAvecOrgan(), Budgetable.ORGAN_FOR_BIBASSE_KEY);
        // Fetch de constantes
        EOTypeApplication typeApp = FinderTypeApplication.getTypeApplication(
                editingContext(), CocoworkApplicationUser.TYAP_STR_ID_COCONUT);
        EOTypeEtat typeEtat = EOTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ETAT_EN_ATTENTE);
        // Pour chaque organ on crée une proposition
        for (Entry<EOOrgan, NSArray<Budgetable>> entry : budgetablesForOrgans.entrySet()) {
            Object key = entry.getKey();
            if (!ERXArrayUtilities.NULL_GROUPING_KEY.equals(key)) {
                EOPrevisionBudget prev = getOrCreatePrevisionBudget(exo, typeApp, typeEtat, (EOOrgan)key, utl, entry.getValue());
                prevs.addObject(prev);
            }
        }
    }
    
    public NSArray<? extends Budgetable> budgetables() {
        NSMutableArray<SbDepense> depenses = sbDepenses().mutableClone();
        NSMutableArray<SbRecette> recettes = sbRecettes().mutableClone();
        NSMutableArray<Budgetable> budgetables = new NSMutableArray<Budgetable>(depenses);
        budgetables.addObjectsFromArray(recettes);
        return budgetables.immutableClone();
    }
    
    public void rejeterPrevision(EOPrevisionBudget prevision, boolean sendMail) {
        EOTypeEtat valide = EOTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ETAT_BLOCAGE);
        prevision.setTypeEtatRelationship(valide);
        for (Budgetable budgetable : budgetables()) {
            budgetable.setPrevisionBudgetRelationship(null);
        }
    }
    
    public NSArray<EOPrevisionBudget> previsionsBudgetForEtats(String...etats) {
        NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
        for (String etat : etats) {
            quals.addObject(ERXQ.equals(EOPrevisionBudget.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, etat));
        }
        EOQualifier qual = ERXQ.or(quals);
        return previsionsBudget(qual);
    }
    
    public void supprimerPrevision(EOPrevisionBudget prev) {
        removeFromPrevisionsBudgetRelationship(prev);
        NSArray<SbRecette> recettes = sbRecettes(SbRecette.PREVISION_BUDGET.eq(prev));
        NSArray<SbDepense> depenses = sbDepenses(SbDepense.PREVISION_BUDGET.eq(prev));
        for (SbRecette recette : recettes)
            recette.setPrevisionBudgetRelationship(null);
        for (SbDepense depense : depenses)
            depense.setPrevisionBudgetRelationship(null);
        editingContext().deleteObject(prev);
    }
    /** [END] MODE PREVISION BUDGETAIRE TOUTE INTEGREE, EN SUSPENS DEPUIS DEPART JMC  **/

    public final static String TRANCHE_BUDGETS_NON_SUPP_KEY = "trancheBudgetsNonSupprimes";
    
    public NSArray<TrancheBudget> trancheBudgetsNonSupprimes() {
        return trancheBudgets(TrancheBudget.QUAL_NON_SUPPR);
    }
    
    public final static String TRANCHE_BUDGETS_REC_NON_SUPP_KEY = "trancheBudgetsRecNonSupprimes";
    
    public NSArray<TrancheBudgetRec> trancheBudgetsRecNonSupprimes() {
        return trancheBudgetRecs(TrancheBudgetRec.QUAL_NON_SUPPR);
    }
    
    public BigDecimal totalPositionne() {
        return (BigDecimal)trancheBudgetsNonSupprimes().valueForKey("@sum." + TrancheBudget.TB_MONTANT_KEY);
    }
    
    public BigDecimal totalPositionneRec() {
        return (BigDecimal)trancheBudgetsRecNonSupprimes().valueForKey("@sum." + TrancheBudgetRec.TBR_MONTANT_KEY);
    }
    
    public BigDecimal resteAPositionne() {
    	if(reportNplus1().signum() == 1) {
    		return BigDecimal.ZERO;
    	} else {
    		return traMontant().subtract(totalPositionne());
    	}
//      if(contrat().isModeRA()) {
//        
//      } else {
//        return contrat().totalContributionsMoinsFraisGestion().subtract(contrat().totalPositionneContrat());
//      }

    }
    
    public BigDecimal resteAPositionneRec() {
        if(contrat().isModeRA()) {
          return traMontant().subtract(totalPositionneRec());
        } else {
          return contrat().totalContributions().subtract(contrat().totalPositionneRecContrat());
        }
    }
    
    public void checkMontantTranche() {
    	if (contrat().isModeRA()) {
	        // Si le montant de la tranche est inférieur au total des crédits positionnés avant le report
	        if (reportNplus1().signum() == 0 && traMontant().compareTo(totalPositionne()) == -1) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, traMontant(), "TotalTrancheTropBasPourCredits");
	        }
	        // Si le montant de la tranche est inférieur au total des dépenses avant le report
	        if (reportNplus1().signum() == 0 && isMontantTrancheLtSommeDepensesDestOuNature()) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, traMontant(), "TotalTrancheTropBasPourDepenses");
	        }
	        // Si le montant de la tranche est inférieur au total des crédits recettes positionnés
	        if (reportNplus1().signum() == 0 && traMontant().compareTo(totalPositionneRec()) == -1) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, traMontant(), "TotalTrancheTropBasPourCreditsRec");
	        }
	        // Si le montant de la tranche est inférieur au total des recettes 
	        if (reportNplus1().signum() == 0 && isMontantTrancheLtSommeRecettesDestOuNature()) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, traMontant(), "TotalTrancheTropBasPourRecettes");
	        }
	        // FIXME Ajouter la vérification lorsque reportNplus1().signum() != 0
    	}
    }
    
    public BigDecimal totalConsomme(boolean inclureResteEngage) {
        BigDecimal montantConsomme = null;
        try {
            if (inclureResteEngage) {
                montantConsomme = new ProcedureGetTotalDepense(editingContext()).execute(exerciceCocktail().exercice(), contrat(), null, null);
            }
            else {
                montantConsomme = new ProcedureGetTotalLiquide(editingContext()).execute(exerciceCocktail().exercice(), contrat(), null, null);
            }
        }
        catch (Exception e) {
            throw NSForwardException._runtimeExceptionForThrowable(e);
        }
        return montantConsomme;
    }
    
    public BigDecimal montantReportable(BigDecimal montantConsomme) {
        BigDecimal montantReportable = BigDecimal.ZERO;
        if (traMontant() != null)
            montantReportable = traMontant().subtract(montantConsomme);
        return montantReportable;
    }
    
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        checkMontantTranche();
    }

    @Override
    public void validateForDelete() throws ValidationException {
        super.validateForDelete();
        if (!sbDepenses().isEmpty()) {
            NSMutableDictionary<String, String> stringContainer = new NSMutableDictionary<String, String>(exerciceCocktail().exeExercice().toString(), "tranche");
            String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("DepensesExistantes", stringContainer);
            throw new ValidationException(msg);
        }
    }
// AT : Utile ??    
//    public boolean translateOrgansDepenses() {
//        boolean hasTranslated = false;
//        for (Object obj : sbDepenses()) {
//            SbDepense depense = (SbDepense)obj;
//            boolean result = depense.translateOrgan();
//            if (result) hasTranslated = true;
//        }
//        return hasTranslated;
//    }
    
//    public boolean translateOrgansRecettes() {
//        boolean hasTranslated = false;
//        for (Object obj : sbRecettes()) {
//            SbRecette recette = (SbRecette)obj;
//            boolean result = recette.translateOrgan();
//            if (result) hasTranslated = true;
//        }
//        return hasTranslated;
//    }
    
    public void valider(EOUtilisateur validateur) {
        // On effectue le controle de preconditons :
        if (!contrat().isValidAdm())
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ValiderContratAvantTranche");
        if (traMontant() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ValiderTrancheNoMontant");
        this.setUtilisateurValidRelationship(validateur);
        this.setTraDateValid(new NSTimestamp());
        this.setTraValide(TRA_VALIDE_OUI);
    }
    
    public boolean isValide() {
        return TRA_VALIDE_OUI.equals(this.traValide()) && traDateValid() != null && utilisateurValid() != null;
    }
    
    public String libelleTranche() {
        return exerciceCocktail().exeExercice().toString();
    }

    public boolean isTrancheSourceReportCredit() {
        NSArray<Tranche> tranchesSources = contrat().tranches(EOExercice.EXE_ETAT_RESTREINT, EOExercice.EXE_ETAT_OUVERT);
        return tranchesSources.containsObject(this);
    }
    
    public static void effectuerReportDeCredits(Tranche tranche, BigDecimal montant, EOEditingContext edc, EOUtilisateur utilisateur) throws Exception {
      Tranche suivante = tranche.trancheExerciceSuivant();
      Boolean gestionAutomatiqueDesTranches  = ERXProperties.booleanForKeyWithDefault(Constantes.GESTION_TRANCHE_AUTO_PARAM, true);
      if(suivante == null) {
        if(gestionAutomatiqueDesTranches) {
          throw new Exception("La tranche " + (tranche.exerciceCocktail().exeExercice() + 1) + " n'existe pas. Veuillez créer" +
          		" un avenant allant jusqu'à l'année " + (tranche.exerciceCocktail().exeExercice() + 1) + " avant d'effectuer le report des crédits.");
        } else {
          FactoryTranche factory = new FactoryTranche(edc);
          EOExerciceCocktail exerciceSuivant = EOExerciceCocktail.fetchFirstByQualifier(edc, ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, tranche.exerciceCocktail().exeExercice()+1));
          suivante = factory.creerTranche(tranche.contrat(), exerciceSuivant, BigDecimal.ZERO, BigDecimal.ZERO, Tranche.NATURE_DEPENSE_RECETTE, utilisateur);
        }
      }
      
      BigDecimal montantDuReport;
      
      if(montant == null) {
    	  montantDuReport = tranche.traMontant().subtract(tranche.totalConsomme(true));
      } else {
    	  montantDuReport = montant;
      }
      tranche.setReportNplus1(montantDuReport);
      suivante.setReportNmoins1(montantDuReport);
      
    }
    
    
    public Tranche trancheExerciceSuivant() {
      EOQualifier qualifier = Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exerciceCocktail().exeExercice()+1)
          .and(Tranche.QUALIFIER_NON_SUPPR)
          .and(Tranche.CONTRAT.eq(contrat()));
      return Tranche.fetch(editingContext(), qualifier);
    }
    
    public Tranche trancheExercicePrecedent() {
        EOQualifier qualifier = Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exerciceCocktail().exeExercice()-1)
            .and(Tranche.QUALIFIER_NON_SUPPR)
            .and(Tranche.CONTRAT.eq(contrat()));
        return Tranche.fetch(editingContext(), qualifier);
      }
      
    
    public void checkSuppressionPossible() throws ValidationException {
    	if(sbRecettes().isEmpty() == false) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesRecettes");
    	}
    	if(sbDepenses().isEmpty() == false) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesDepenses");
    	}
    	if(trancheBudgets(TrancheBudget.QUAL_NON_SUPPR).isEmpty() == false) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesCreditPositionnesDepenses");
    	}
    	if(trancheBudgetRecs(TrancheBudgetRec.QUAL_NON_SUPPR).isEmpty() == false) {
    		throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesCreditPositionnesRecettes");
    	}
    }
    
    @Override
    public void delete() {
    	Tranche precedente = trancheExercicePrecedent();
    	if(precedente != null) {
    		precedente.setReportNplus1(BigDecimal.ZERO);
    	}
    	super.delete();
    }
    
    
    
}
