package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * Bean utilisé dans les filtres de recherche pour représenter les services
 * gestionnaires.
 * 
 * @author Alexis Tual
 *
 */
public class ServiceGestionnaireBean {

    public static final String LIBELLE = "libelle";
    
    private String cStructure;
    private String libelle;
    
    /**
     * @param cStructure un cstructure
     * @param libelle le libellé du service
     */
    public ServiceGestionnaireBean(String cStructure, String libelle) {
        super();
        this.cStructure = cStructure;
        this.libelle = libelle;
    }
    
    public String getCStructure() {
        return cStructure;
    }
    
    public String getLibelle() {
        return libelle;
    }
   
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(cStructure)
                .append(libelle)
                .toHashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof ServiceGestionnaireBean) {
            ServiceGestionnaireBean other = (ServiceGestionnaireBean) obj;
            result = new EqualsBuilder()
                        .append(cStructure, other.cStructure)
                        .append(libelle, other.libelle)
                        .isEquals();
        }
        return result;
    }
    
}
