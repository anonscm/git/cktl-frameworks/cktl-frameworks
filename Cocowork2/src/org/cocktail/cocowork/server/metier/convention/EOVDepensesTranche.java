// DO NOT EDIT.  Make changes to VDepensesTranche.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVDepensesTranche extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWVDepensesTranche";

  // Attribute Keys
  public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche> TRANCHE = new ERXKey<org.cocktail.cocowork.server.metier.convention.Tranche>("tranche");

  // Attributes
  public static final String EXE_ORDRE_KEY = EXE_ORDRE.key();
  public static final String MONTANT_KEY = MONTANT.key();
  // Relationships
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOVDepensesTranche.class);

  public VDepensesTranche localInstanceIn(EOEditingContext editingContext) {
    VDepensesTranche localInstance = (VDepensesTranche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EOVDepensesTranche.EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    if (EOVDepensesTranche.LOG.isDebugEnabled()) {
        EOVDepensesTranche.LOG.debug( "updating exeOrdre from " + exeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOVDepensesTranche.EXE_ORDRE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(EOVDepensesTranche.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (EOVDepensesTranche.LOG.isDebugEnabled()) {
        EOVDepensesTranche.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, EOVDepensesTranche.MONTANT_KEY);
  }

  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(EOVDepensesTranche.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOVDepensesTranche.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (EOVDepensesTranche.LOG.isDebugEnabled()) {
      EOVDepensesTranche.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVDepensesTranche.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVDepensesTranche.PLANCO_KEY);
    }
  }
  
  public org.cocktail.cocowork.server.metier.convention.Tranche tranche() {
    return (org.cocktail.cocowork.server.metier.convention.Tranche)storedValueForKey(EOVDepensesTranche.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    takeStoredValueForKey(value, EOVDepensesTranche.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.cocowork.server.metier.convention.Tranche value) {
    if (EOVDepensesTranche.LOG.isDebugEnabled()) {
      EOVDepensesTranche.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.cocowork.server.metier.convention.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVDepensesTranche.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVDepensesTranche.TRANCHE_KEY);
    }
  }
  

  public static VDepensesTranche create(EOEditingContext editingContext, Integer exeOrdre
, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planco, org.cocktail.cocowork.server.metier.convention.Tranche tranche) {
    VDepensesTranche eo = (VDepensesTranche) EOUtilities.createAndInsertInstance(editingContext, EOVDepensesTranche.ENTITY_NAME);    
        eo.setExeOrdre(exeOrdre);
    eo.setPlancoRelationship(planco);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VDepensesTranche> fetchSpec() {
    return new ERXFetchSpecification<VDepensesTranche>(EOVDepensesTranche.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VDepensesTranche> fetchAll(EOEditingContext editingContext) {
    return EOVDepensesTranche.fetchAll(editingContext, null);
  }

  public static NSArray<VDepensesTranche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVDepensesTranche.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VDepensesTranche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VDepensesTranche> fetchSpec = new ERXFetchSpecification<VDepensesTranche>(EOVDepensesTranche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VDepensesTranche> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VDepensesTranche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVDepensesTranche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDepensesTranche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VDepensesTranche> eoObjects = EOVDepensesTranche.fetchAll(editingContext, qualifier, null);
    VDepensesTranche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWVDepensesTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDepensesTranche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVDepensesTranche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VDepensesTranche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VDepensesTranche eoObject = EOVDepensesTranche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWVDepensesTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VDepensesTranche localInstanceIn(EOEditingContext editingContext, VDepensesTranche eo) {
    VDepensesTranche localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}