package org.cocktail.cocowork.server.metier.convention.procedure.suividepense;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * <p>Procedure qui calcule le total disponible en depense
 * pour une convention, un exercice, un type de credit et une ligne de l'organigramme 
 * budgetaire.
 * <p>Le disponible est :
 * 		<pre>total_credits_positionnes_dans_Coconut - ( total_restant_engage + total_liquide )</pre>
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public class ProcedureGetTotalDispo extends ProcedureSuiviDepense
{
	protected final static String PROCEDURE_NAME = "GetTotalDispo";
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 */
	public ProcedureGetTotalDispo(final EOEditingContext ec) { 
		super(ec, PROCEDURE_NAME); 
	}
	
}
