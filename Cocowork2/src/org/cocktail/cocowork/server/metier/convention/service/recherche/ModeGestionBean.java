package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * Bean utilisé dans les filtres de recherche pour représenter les modes de gestion
 * 
 * @author Alexis Tual
 *
 */
public class ModeGestionBean {

    public static final String MG_LIBELLE = "mgLibelle";
    
    private Integer mgOrdre;
    private String mgLibelle;
    
    /**
     * @param mgOrdre un id de mode de gestion
     * @param mgLibelle un libellé de mode de gestion
     */
    public ModeGestionBean(Integer mgOrdre, String mgLibelle) {
        super();
        this.mgOrdre = mgOrdre;
        this.mgLibelle = mgLibelle;
    }
    
    public String getMgLibelle() {
        return mgLibelle;
    }
    
    public Integer getMgOrdre() {
        return mgOrdre;
    }
    
    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(mgOrdre)
                .append(mgLibelle)
                .toHashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof ModeGestionBean) {
            ModeGestionBean other = (ModeGestionBean) obj;
            result = new EqualsBuilder()
                        .append(mgOrdre, other.mgOrdre)
                        .append(mgLibelle, other.mgLibelle)
                        .isEquals();
        }
        return result;
    }
    
}
