package org.cocktail.cocowork.server.metier.convention.service.recherche;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ModeGestion;
import org.cocktail.cocowork.server.metier.convention.TypeContrat;
import org.cocktail.cocowork.server.metier.grhum.Discipline;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * Bean destiné à représenter un résultat de recherche.
 * Il aggrège les informations nécessaires au filtrage :
 * 
 * @author Alexis Tual
 *
 */
public class ResultatRechercheBean {

    private static final String CON_ORDRE_ROW_KEY = Contrat.CON_ORDRE_KEY;
    private static final String EXE_ORDRE_ROW_KEY = Contrat.EXERCICE_COCKTAIL_KEY + "." + EOExerciceCocktail.EXE_ORDRE_KEY;
    private static final String CON_INDEX_ROW_KEY = Contrat.CON_INDEX_KEY;
    private static final String CON_OBJET_ROW_KEY = Contrat.CON_OBJET_KEY;
    private static final String CON_REF_EXTERNE = Contrat.CON_REFERENCE_EXTERNE_KEY;
    private static final String CENTRE_RESP_C_STRUCTURE_ROW_KEY = Contrat.CENTRE_RESPONSABILITE.dot(EOStructure.C_STRUCTURE).key();
    private static final String CENTRE_RESP_LL_STRUCTURE_ROW_KEY = Contrat.CENTRE_RESPONSABILITE.dot(EOStructure.LL_STRUCTURE).key();
    private static final String CON_DATE_VALID_ADM_ROW_KEY = Contrat.CON_DATE_VALID_ADM_KEY;
    private static final String MG_ORDRE_ROW_KEY = Contrat.AVENANTS.dot(Avenant.MODE_GESTION).dot(ModeGestion.MG_ORDRE).key();
    private static final String MG_LIBELLE_COURT_ROW_KEY = Contrat.AVENANTS.dot(Avenant.MODE_GESTION).dot(ModeGestion.MG_LIBELLE_KEY).key();
    private static final String DISC_ORDRE_ROW_KEY = Contrat.AVENANTS.dot(Avenant.DISCIPLINE).dot(Discipline.DISC_ORDRE).key();
    private static final String DISC_LIBELLE_LONG_ROW_KEY = Contrat.AVENANTS.dot(Avenant.DISCIPLINE).dot(Discipline.DISC_LIBELLE_LONG).key();
    private static final String TYCON_ID_INTERNE_ROW_KEY = Contrat.TYPE_CONTRAT.dot(TypeContrat.TYCON_ID_INTERNE).key();
    private static final String TYCON_LIBELLE_ROW_KEY = Contrat.TYPE_CONTRAT.dot(TypeContrat.TYCON_LIBELLE).key();
    private static final String CON_DATE_DEBUT_ROW_KEY = Contrat.AVENANTS.dot(Avenant.AVT_DATE_DEB).key();
    private static final String CON_DATE_FIN_ROW_KEY = Contrat.AVENANTS.dot(Avenant.AVT_DATE_FIN).key();
    private static final String CON_DATE_SIGN_ROW_KEY = Contrat.AVENANTS.dot(Avenant.AVT_DATE_SIGNATURE).key();

    public static final NSArray<String> ROW_KEYS = new NSArray<String>(
            CON_ORDRE_ROW_KEY, EXE_ORDRE_ROW_KEY, CON_INDEX_ROW_KEY, CON_OBJET_ROW_KEY, CON_REF_EXTERNE, CENTRE_RESP_C_STRUCTURE_ROW_KEY,
            CENTRE_RESP_LL_STRUCTURE_ROW_KEY, CON_DATE_VALID_ADM_ROW_KEY, MG_ORDRE_ROW_KEY, MG_LIBELLE_COURT_ROW_KEY,
            DISC_ORDRE_ROW_KEY, DISC_LIBELLE_LONG_ROW_KEY, TYCON_ID_INTERNE_ROW_KEY, TYCON_LIBELLE_ROW_KEY,
            CON_DATE_DEBUT_ROW_KEY, CON_DATE_FIN_ROW_KEY, CON_DATE_SIGN_ROW_KEY
            );

    public static final String DISCIPLINE_KEY = "discipline";
    public static final String TYPE_CONTRAT_KEY = "typeContrat";
    public static final String MODE_GESTION_KEY = "modeGestion";
    public static final String SERVICE_GEST_KEY = "serviceGestionnaire";
    public static final String EXE_ORDRE_KEY = "exeOrdre";
    public static final String INDEX_KEY = "index";
    public static final String OBJET_KEY = "objet";
    public static final String REF_EXTERNE_KEY = "refExterne";
    public static final String DATE_DEBUT_KEY = "dateDebut";
    public static final String DATE_FIN_KEY = "dateFin";
    public static final String DATE_SIGNATURE_KEY = "dateSignature";
    public static final String NUMERO_KEY = "numero";
    public static final String NUMERO_POUR_TRI_KEY = "numeroPourTri";

    private Integer conOrdre;
    private Integer exeOrdre;
    private Integer index;
    private String objet;
    private String refExterne;
    private ServiceGestionnaireBean serviceGestionnaire;
    private NSTimestamp dateValidite;
    private DisciplineBean discipline;
    private ModeGestionBean modeGestion;
    private TypeContratBean typeContrat;
    private NSTimestamp dateDebut;
    private NSTimestamp dateFin;
    private NSTimestamp dateSignature;
    
    private static final int MULTIPLICATEUR_EXERCICE_INDEX = 10000;

    /**
     * @param row les données à partir desquelles on crée l'objet
     */
    public ResultatRechercheBean(NSDictionary<String, Object> row) {
        this.conOrdre = (Integer) convertField(row, CON_ORDRE_ROW_KEY);
        this.exeOrdre = (Integer) convertField(row, EXE_ORDRE_ROW_KEY);
        this.index = (Integer) convertField(row, CON_INDEX_ROW_KEY);
        this.objet = (String) convertField(row, CON_OBJET_ROW_KEY);
        this.refExterne = (String) convertField(row, CON_REF_EXTERNE);
        this.serviceGestionnaire = convertServiceGestionnaire(row);
        this.dateValidite = (NSTimestamp) convertField(row, CON_DATE_VALID_ADM_ROW_KEY);
        this.discipline = convertDiscipline(row);
        this.modeGestion = convertModeGestion(row);
        this.typeContrat = convertTypeContrat(row);
        this.dateDebut = (NSTimestamp) convertField(row, CON_DATE_DEBUT_ROW_KEY);
        this.dateFin = (NSTimestamp) convertField(row, CON_DATE_FIN_ROW_KEY);
        this.dateSignature = (NSTimestamp) convertField(row, CON_DATE_SIGN_ROW_KEY);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.MULTI_LINE_STYLE);
    }
    
    private TypeContratBean convertTypeContrat(NSDictionary<String, Object> row) {
        String id = (String) convertField(row, TYCON_ID_INTERNE_ROW_KEY);
        TypeContratBean obj = null;
        if (id != null) {
            obj = new TypeContratBean(id, (String) convertField(row, TYCON_LIBELLE_ROW_KEY));
        }
        return obj;
    }

    private ServiceGestionnaireBean convertServiceGestionnaire(NSDictionary<String, Object> row) {
        ServiceGestionnaireBean obj = null;
        String id = (String) convertField(row, CENTRE_RESP_C_STRUCTURE_ROW_KEY);
        if (id != null) {
            obj = new ServiceGestionnaireBean(id, (String) convertField(row, CENTRE_RESP_LL_STRUCTURE_ROW_KEY));
        }
        return obj;
    }

    private ModeGestionBean convertModeGestion(NSDictionary<String, Object> row) {
        ModeGestionBean obj = null;
        Integer id = (Integer) convertField(row, MG_ORDRE_ROW_KEY);
        if (id != null) {
            obj = new ModeGestionBean(id, (String) convertField(row, MG_LIBELLE_COURT_ROW_KEY));
        }
        return obj;
    }

    private DisciplineBean convertDiscipline(NSDictionary<String, Object> row) {
        DisciplineBean obj = null;
        Integer id = (Integer) convertField(row, DISC_ORDRE_ROW_KEY);
        if (id != null) {
            obj = new DisciplineBean(id, (String) convertField(row, DISC_LIBELLE_LONG_ROW_KEY));
        }
        return obj;
    }

    private Object convertField(NSDictionary<String, Object> row, String key) {
        Object obj = row.objectForKey(key);
        if (obj instanceof NSKeyValueCoding.Null) {
            obj = null;
        }
        return obj;
    }

    public String getNumero() {
        return getExeOrdre() + " - " + getIndex();
    }

    public Integer getNumeroPourTri() {
    	return getExeOrdre() * MULTIPLICATEUR_EXERCICE_INDEX + getIndex();
    }

    public Integer getIndex() {
        return index;
    }

    public Integer getConOrdre() {
        return conOrdre;
    }

    public Integer getExeOrdre() {
        return exeOrdre;
    }

    public String getObjet() {
        return objet;
    }

    public String getRefExterne() {
        return refExterne;
    }
    
    public ServiceGestionnaireBean getServiceGestionnaire() {
        return serviceGestionnaire;
    }

    public NSTimestamp getDateValidite() {
        return dateValidite;
    }

    public DisciplineBean getDiscipline() {
        return discipline;
    }

    public ModeGestionBean getModeGestion() {
        return modeGestion;
    }

    public TypeContratBean getTypeContrat() {
        return typeContrat;
    }

    public NSTimestamp getDateDebut() {
        return dateDebut;
    }
    
    public NSTimestamp getDateFin() {
        return dateFin;
    }
    
    public NSTimestamp getDateSignature() {
        return dateSignature;
    }
    
}