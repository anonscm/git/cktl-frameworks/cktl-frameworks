// ProjetContrat.java
// Created on Thu Feb 21 14:11:38 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.cocowork.common.tools.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;

public class ProjetContrat extends EOProjetContrat {

    public ProjetContrat() {
        super();
    }
    
    /**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee.
	 * @throws Exception Probleme d'instanciation.
	 */
	static public ProjetContrat instanciate(EOEditingContext ec) throws Exception {
		ProjetContrat unProjetContrat = (ProjetContrat) Factory.instanceForEntity(ec, ENTITY_NAME);
		
		return unProjetContrat;
	}

}
