
// AvenantPartenaire.java
// 

package org.cocktail.cocowork.server.metier.convention;

//import org.cocktail.cocowork.client.metier.convention.AvenantPartenaire;
import java.math.BigDecimal;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;

public class ContratPartenaire extends EOContratPartenaire {
    
    public static final ERXKey<Integer> CON_ORDRE = new ERXKey<Integer>("conOrdre");
    
	public static final String PARTENAIRE_PRINCIPAL_OUI = "O";
	public static final String PARTENAIRE_PRINCIPAL_NON = "N";
	public static final String PARTENAIRE_KEY = "partenaire";

	public static final EOQualifier QUALIFIER_PARTENAIRE_PRINCIPAL = EOQualifier.qualifierWithQualifierFormat("cpPrincipal = %@", new NSArray(PARTENAIRE_PRINCIPAL_OUI));
	public static final EOQualifier QUALIFIER_PARTENAIRE_NON_PRINCIPAL = EOQualifier.qualifierWithQualifierFormat("cpPrincipal = %@", new NSArray(PARTENAIRE_PRINCIPAL_NON));

	public static final ERXKey<IPersonne> PARTENAIRE = new ERXKey<IPersonne>(PARTENAIRE_KEY);
	
	private NSMutableArray contratPartContacts;
	private IPersonne partenaire;

	public ContratPartenaire() {
		super();
	}

	public void setCpPrincipalBoolean(final boolean value) {
		setCpPrincipal(value ? PARTENAIRE_PRINCIPAL_OUI : PARTENAIRE_PRINCIPAL_NON);
	}

	public boolean cpPrincipalBoolean() {
		return PARTENAIRE_PRINCIPAL_OUI.equals(cpPrincipal());
	}

	public BigDecimal cpMontant() {
		BigDecimal cpMontant = super.cpMontant();
		cpMontant = (BigDecimal) contributions().valueForKeyPath("@sum.rptMontantParticipation");
		return cpMontant;
	}

	public void setPartenaire(IPersonne partenaire) {
		if (partenaire != null) {
			setPersId(partenaire.persId());
		}
		this.partenaire = partenaire;
	}

	public IPersonne partenaire() {

		if (partenaire == null) {
			EOStructure struct = EOStructure.fetchByKeyValue(this.editingContext(), EOStructure.PERS_ID_KEY, persId());
			if (struct != null)
				partenaire = struct;

			EOIndividu indiv = EOIndividu.fetchByKeyValue(this.editingContext(), EOIndividu.PERS_ID_KEY, persId());
			if (indiv != null)
				partenaire = indiv;
		}
		return partenaire;
	}

	public ContratPartContact contactForPersId(Integer persId) {
		ContratPartContact contact = null;
		if (persId != null) {
			EOKeyValueQualifier qualifier = new EOKeyValueQualifier(ContratPartContact.PERS_ID_CONTACT_KEY, EOQualifier.QualifierOperatorEqual, persId);
			NSArray contacts = EOQualifier.filteredArrayWithQualifier(contratPartContacts(), qualifier);
			if (contacts != null && contacts.count() == 1) {
				contact = (ContratPartContact) contacts.lastObject();
			}
		}
		return contact;
	}

	/**
	 * Renvoie un NSArray des roles du partenaire dans le contrat
	 * 
	 * @param partenaire
	 * @param contrat
	 * @return
	 */

	public NSArray<EOAssociation> rolesPartenaireDansContrat(IPersonne partenaire, Contrat contrat) {
		NSArray<EORepartAssociation> repartAssociations = partenaire.getRepartAssociationsInGroupe(contrat.groupePartenaire(), null);

		NSArray<EOAssociation> lesAssociations = null;
		if (repartAssociations != null && !repartAssociations.isEmpty()) {
			lesAssociations = (NSArray<EOAssociation>) repartAssociations.valueForKey(EORepartAssociation.TO_ASSOCIATION_KEY);
		}
		return lesAssociations;
	}

	public NSArray<EORepartAssociation> repartAssociationForAssociation(EOAssociation association) {

		NSArray<EORepartAssociation> repartAssociations = repartAssociationForAssociationsAuxDates(new NSArray<EOAssociation>(association), null, null);
		if (repartAssociations == null) {
			return null;
		} else {
			return repartAssociations;
		}
	}

	public NSArray<EORepartAssociation> repartAssociationForAssociations(NSArray<EOAssociation> lesAssociations) {

		NSArray<EORepartAssociation> repartAssociations = repartAssociationForAssociationsAuxDates(lesAssociations, null, null);
		if (repartAssociations == null) {
			return null;
		} else {
			return repartAssociations;
		}
	}

	public NSArray<EORepartAssociation> repartAssociationForAssociationAuxDates(EOAssociation association, NSTimestamp aLaDateDebut, NSTimestamp aLaDateFin) {
		NSArray<EORepartAssociation> repartAssociations = repartAssociationForAssociationsAuxDates(new NSArray<EOAssociation>(association), aLaDateDebut, aLaDateFin);
		if (repartAssociations == null) {
			return null;
		} else {
			return repartAssociations;
		}
	}

	public NSArray<EORepartAssociation> repartAssociationForAssociationsAuxDates(NSArray<EOAssociation> lesAssociations, NSTimestamp aLaDateDebut, NSTimestamp aLaDateFin) {
		EOQualifier qualRepartAssoDateDebut = null;
		EOQualifier qualRepartAssoDateFin = null;
		EOQualifier qualRepartAssoDate = null;
		EOQualifier qualRepartAsso = null;

		EOQualifier qualRepartAsso1 = ERXQ.inObjects(EORepartAssociation.TO_ASSOCIATION_KEY, lesAssociations.objects());
		if (aLaDateDebut != null)
			qualRepartAssoDateDebut = ERXQ.and(ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, aLaDateDebut),
					ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, aLaDateDebut));
		if (aLaDateFin != null)
			qualRepartAssoDateFin = ERXQ.and(ERXQ.greaterThanOrEqualTo(EORepartAssociation.RAS_D_FERMETURE_KEY, aLaDateFin),
					ERXQ.lessThanOrEqualTo(EORepartAssociation.RAS_D_OUVERTURE_KEY, aLaDateFin));
		if (qualRepartAssoDateDebut != null && qualRepartAssoDateFin != null)
			qualRepartAssoDate = ERXQ.or(qualRepartAssoDateDebut, qualRepartAssoDateFin);
		else {
			if (qualRepartAssoDateDebut != null)
				qualRepartAssoDate = qualRepartAssoDateDebut;
			if (qualRepartAssoDateFin != null)
				qualRepartAssoDate = qualRepartAssoDateFin;
		}

		if (qualRepartAssoDate == null)
			qualRepartAsso = qualRepartAsso1;
		else
			qualRepartAsso = ERXQ.and(qualRepartAsso1, qualRepartAssoDate);

		NSArray<EORepartAssociation> lesRepartAsso = partenaire().getRepartAssociationsInGroupe(contrat().groupePartenaire(), qualRepartAsso1);

		return lesRepartAsso;
	}
}
