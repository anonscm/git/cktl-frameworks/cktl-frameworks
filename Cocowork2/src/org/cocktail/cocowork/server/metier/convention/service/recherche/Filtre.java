package org.cocktail.cocowork.server.metier.convention.service.recherche;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * 
 * Représente les critères de la recherche avancée.
 * 
 * @author Alexis Tual
 *
 */
public class Filtre {

    private ServiceGestionnaireBean selectedService;
    private DisciplineBean selectedDiscipline;
    private ModeGestionBean selectedModeDeGestion;
    private TypeContratBean selectedTypeContrat;
    
    private Integer filtreExeordre;
    private Integer filtreIndex;
    private String filtreRefExterne;
    private String filtreObjet;
    private NSTimestamp filtreDateDebutMin;
    private NSTimestamp filtreDateDebutMax;
    private NSTimestamp filtreDateFinMin;
    private NSTimestamp filtreDateFinMax;
    private NSTimestamp filtreDateSignatureMin;
    private NSTimestamp filtreDateSignatureMax;

    /**
     * @return le qualifier correspondant aux critères.
     *         A noter que les critères null sont exclus du qualifier.
     * 
     */
    public EOQualifier qualifier() {
        NSArray<EOQualifier> qualifiers = qualifiersWithoutNullValue(
                ERXQ.equals(ResultatRechercheBean.DISCIPLINE_KEY, selectedDiscipline),
                ERXQ.equals(ResultatRechercheBean.MODE_GESTION_KEY, selectedModeDeGestion),
                ERXQ.equals(ResultatRechercheBean.TYPE_CONTRAT_KEY, selectedTypeContrat),
                ERXQ.equals(ResultatRechercheBean.SERVICE_GEST_KEY, selectedService),
                ERXQ.equals(ResultatRechercheBean.EXE_ORDRE_KEY, filtreExeordre),
                ERXQ.equals(ResultatRechercheBean.INDEX_KEY, filtreIndex),
                ERXQ.between(ResultatRechercheBean.DATE_DEBUT_KEY, filtreDateDebutMin, filtreDateDebutMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_FIN_KEY, filtreDateFinMin, filtreDateFinMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_DEBUT_KEY, filtreDateDebutMin, filtreDateDebutMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_SIGNATURE_KEY, filtreDateSignatureMin, filtreDateSignatureMax, true)
                );
        if (filtreObjet != null) {
            qualifiers = qualifiers.arrayByAddingObject(ERXQ.contains(ResultatRechercheBean.OBJET_KEY, filtreObjet));
        }
        if (filtreRefExterne != null) {
            qualifiers = qualifiers.arrayByAddingObject(ERXQ.contains(ResultatRechercheBean.REF_EXTERNE_KEY, filtreRefExterne));
        }
        EOAndQualifier andQual = new EOAndQualifier(qualifiers);
        return andQual;
    }
    
    protected NSArray<EOQualifier> qualifiersWithoutNullValue(EOQualifier... quals) {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        for (EOQualifier qual : quals) {
            if (qual != null) {
                if (qual instanceof EOKeyValueQualifier) {
                    Object object = ((EOKeyValueQualifier) qual).value();
                    if (object != null && !(object instanceof NSKeyValueCoding.Null)) {
                        qualifiers.addObject(qual);
                    }
                } else {
                    qualifiers.addObject(qual);
                }
            }
        }
        return qualifiers.immutableClone();
    }
    
    public void setFiltreExeordre(Integer filtreExeordre) {
        this.filtreExeordre = filtreExeordre;
    }
    
    public Integer getFiltreExeordre() {
        return filtreExeordre;
    }
    
    public Integer getFiltreIndex() {
        return filtreIndex;
    }
    
    public void setFiltreIndex(Integer filtreIndex) {
        this.filtreIndex = filtreIndex;
    }
    
    public String getFiltreRefExterne() {
        return filtreRefExterne;
    }
    
    public void setFiltreRefExterne(String filtreRefExterne) {
        this.filtreRefExterne = filtreRefExterne;
    }
    
    public ServiceGestionnaireBean getSelectedService() {
        return selectedService;
    }
    
    public void setSelectedService(ServiceGestionnaireBean selectedService) {
        this.selectedService = selectedService;
    }
    
    public String getFiltreObjet() {
        return filtreObjet;
    }
    
    public void setFiltreObjet(String filtreObjet) {
        this.filtreObjet = filtreObjet;
    }
    
    public NSTimestamp getFiltreDateDebutMax() {
        return filtreDateDebutMax;
    }

    public NSTimestamp getFiltreDateDebutMin() {
        return filtreDateDebutMin;
    }

    public void setFiltreDateDebutMin(NSTimestamp filtreDateDebutMin) {
        this.filtreDateDebutMin = filtreDateDebutMin;
    }

    public NSTimestamp getFiltreDateSignatureMin() {
        return filtreDateSignatureMin;
    }

    public void setFiltreDateSignatureMin(NSTimestamp filtreDateSignatureMin) {
        this.filtreDateSignatureMin = filtreDateSignatureMin;
    }

    public NSTimestamp getFiltreDateSignatureMax() {
        return filtreDateSignatureMax;
    }

    public void setFiltreDateSignatureMax(NSTimestamp filtreDateSignatureMax) {
        this.filtreDateSignatureMax = filtreDateSignatureMax;
    }

    public void setFiltreDateDebutMax(NSTimestamp filtreDateDebutMax) {
        this.filtreDateDebutMax = filtreDateDebutMax;
    }

    public NSTimestamp getFiltreDateFinMin() {
        return filtreDateFinMin;
    }

    public void setFiltreDateFinMin(NSTimestamp filtreDateFinMin) {
        this.filtreDateFinMin = filtreDateFinMin;
    }

    public NSTimestamp getFiltreDateFinMax() {
        return filtreDateFinMax;
    }

    public void setFiltreDateFinMax(NSTimestamp filtreDateFinMax) {
        this.filtreDateFinMax = filtreDateFinMax;
    }
    
    public DisciplineBean getSelectedDiscipline() {
        return selectedDiscipline;
    }
    
    public void setSelectedDiscipline(DisciplineBean selectedDiscipline) {
        this.selectedDiscipline = selectedDiscipline;
    }
    
    public ModeGestionBean getSelectedModeDeGestion() {
        return selectedModeDeGestion;
    }
    
    public void setSelectedModeDeGestion(ModeGestionBean selectedModeDeGestion) {
        this.selectedModeDeGestion = selectedModeDeGestion;
    }
    
    public TypeContratBean getSelectedTypeContrat() {
        return selectedTypeContrat;
    }
    
    public void setSelectedTypeContrat(TypeContratBean selectedTypeContrat) {
        this.selectedTypeContrat = selectedTypeContrat;
    }
    
}
