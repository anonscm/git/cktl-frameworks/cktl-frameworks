package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class ReportInfo {

    public static final String RETRAIT_REPART_SOURCE_KEY = "retraitRepartSource";
    public static final String AJOUT_REPART_DESTINATION_KEY = "ajoutRepartDestination";
    private RepartPartenaireTranche repartSource;
    private RepartPartenaireTranche repartDestination;
    private BigDecimal montantRepartSource;
    private BigDecimal montantRepartDestination;

    public ReportInfo(RepartPartenaireTranche repartSource, Tranche trancheDest) {
        super();
        this.repartSource = repartSource;
        this.montantRepartSource = repartSource.rptMontantParticipation();
        this.repartDestination = montantRepartDestinationFromSource(repartSource, trancheDest);
        if (this.repartDestination != null) {
            this.montantRepartDestination = repartDestination.rptMontantParticipation();
        } else {
            this.montantRepartDestination = BigDecimal.ZERO;
        }
    }

    private RepartPartenaireTranche montantRepartDestinationFromSource(RepartPartenaireTranche repartSource, Tranche trancheDestination) {
        Integer contribPersId = repartSource.contratPartenaire().persId();
        // On va chercher le repart partenaire sur la tranche destination
        EOQualifier qual = RepartPartenaireTranche.CONTRAT_PARTENAIRE.dot(ContratPartenaire.PERS_ID).eq(contribPersId);
        NSArray<RepartPartenaireTranche> repartsDest = trancheDestination.toRepartPartenaireTranches(qual);
        return repartsDest.isEmpty() ? null : repartsDest.lastObject();
    }

    public void reporter() {
        repartDestination.setRptMontantParticipation(getMontantRepartDestination());
        repartSource.setRptMontantParticipation(getMontantRepartSource());
    }

    public String libelleContributeur() {
        return repartSource.contratPartenaire().partenaire().libelleEtId();
    }
    
    public BigDecimal retraitRepartSource() {
        return repartSource.rptMontantParticipation().subtract(getMontantRepartSource()).abs();
    }
    
    public BigDecimal ajoutRepartDestination() {
        return repartDestination.rptMontantParticipation().subtract(getMontantRepartDestination()).abs();
    }
    
    public BigDecimal getMontantRepartDestination() {
        return montantRepartDestination;
    }

    public void setMontantRepartDestination(BigDecimal montantRepartDestination) {
        this.montantRepartDestination = montantRepartDestination;
    }

    public BigDecimal getMontantRepartSource() {
        return montantRepartSource;
    }

    public void setMontantRepartSource(BigDecimal montantRepartSource) {
        if (montantRepartSource.compareTo(repartSource.rptMontantParticipation()) == -1) {
            this.montantRepartSource = montantRepartSource;
        }  
    }

}