package org.cocktail.cocowork.server.metier.convention;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

import com.webobjects.eoaccess.EOUtilities;

public class AvenantDomaineScientifique extends EOAvenantDomaineScientifique {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(AvenantDomaineScientifique.class);
    
    public void remove(EODomaineScientifique domaine) {
    	removeObjectFromBothSidesOfRelationshipWithKey(domaine, EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
    }
    
}
