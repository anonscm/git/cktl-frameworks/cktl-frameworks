package org.cocktail.cocowork.server.metier.convention.procedure.suividepense;

import java.math.BigDecimal;

import org.cocktail.cocowork.common.exception.ExceptionProcedure;
import org.cocktail.cocowork.common.procedure.StoredProcedure;
import org.cocktail.cocowork.common.tools.ModelUtilities;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.gfc.Typcred;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;


/**
 * Classe abstraite representant une procedure de suivi d'execution des depenses
 * au titre d'une convention sur un exercice, pour un type de credit et une ligne de l'organigramme budgetaire.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public abstract class ProcedureSuiviDepense extends StoredProcedure
{
	private static final ModelUtilities modelUtilities = new ModelUtilities();
	
	protected String procedureName;
	
	protected final static String EXE_ORDRE_ARG_NAME = 	"010exeOrdre";
	protected final static String CON_ORDRE_ARG_NAME = 	"020conOrdre";
	protected final static String TCD_ORDRE_ARG_NAME = 	"030tcdOrdre";
	protected final static String ORG_ID_ARG_NAME = 	"040orgId";
	
	protected final static String RETURN_VALUE_NAME = 	"050total";
	

	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 * @param procedureName Nom de la procedure stoquee.
	 */
	public ProcedureSuiviDepense(final EOEditingContext ec, final String procedureName) { 
		super(
				ec, 
				procedureName,
				new String[] { EXE_ORDRE_ARG_NAME, CON_ORDRE_ARG_NAME, TCD_ORDRE_ARG_NAME, ORG_ID_ARG_NAME });
	}
	
	/**
	 * Lance la procedure pour la convention, sur l'exercice, le type de credit et la ligne budgetaire specifies.
	 * @param exeOrdre Identifiant de l'exercice. Obligatoire.
	 * @param conOrdre Identifiant du contrat. Obligatoire.
	 * @param tcdOrdre Identifiant du type de credit. 
	 * Peut etre <code>null</code> pour totaliser qqsoit le type de credit.
	 * @param orgId Identifiant de la ligne budgetaire. 
	 * Peut etre <code>null</code> (SAUF pour les conventions gerees en Ressoures Affectees) pour totaliser qqsoit la LB.
	 * @return Le resultat renvoye par la procedure.
	 * @throws ExceptionProcedure Erreur lors de l'appel a la procedure stoquee.
	 */
	public BigDecimal execute(
			final Number exeOrdre, 
			final Number conOrdre,
			final Number tcdOrdre, 
			final Number orgId) throws ExceptionProcedure {
		
		Object[] argumentsValues = new Object[] { 
				exeOrdre, 
				conOrdre, 
				tcdOrdre!=null ? tcdOrdre : (Object)NSKeyValueCoding.NullValue, 
				orgId!=null ? orgId : (Object)NSKeyValueCoding.NullValue 
		};
		
//		System.out.println("ProcedureSuiviDepense.execute() arguments = ");
//		System.out.println("exeOrdre = "+exeOrdre);
//		System.out.println("conOrdre = "+conOrdre);
//		System.out.println("tcdOrdre = "+tcdOrdre);
//		System.out.println("orgId = "+orgId);
		
		try {
			NSDictionary dico = super.execute(argumentsValues);
			Object resultat = dico.valueForKey(RETURN_VALUE_NAME);
			
			if (resultat == NSKeyValueCoding.NullValue)
				return new BigDecimal(0);
			else
				return (BigDecimal) resultat;
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ExceptionProcedure(e.getMessage());
		}
	}	
	/**
	 * Lance la procedure pour la convention, sur l'exercice, le type de credit et la ligne de l'organigramme budgetaire specifies.
	 * @param exercice L'exercice. Obligatoire. 
	 * @param contrat Le contrat. Obligatoire. 
	 * @param typcred Le type de credit. 
	 * Peut etre <code>null</code> pour totaliser qqsoit le type de credit.
	 * @param organ La ligne budgetaire. 
	 * Peut etre <code>null</code> (SAUF pour les conventions gerees en Ressoures Affectees) pour totaliser qqsoit la LB.
	 * @return Le resultat renvoye par la procedure.
	 * @throws ExceptionProcedure Erreur lors de l'appel a la procedure stoquee.
	 */
	public BigDecimal execute(
			final EOExercice exercice, 
			final Contrat contrat, 
			final Typcred typcred, 
			final EOOrgan organ) throws Exception {

		Number exeOrdre=null;
		Number conOrdre=null;
		Number tcdOrdre=null;
		Number orgId=null;
		String info=null;
		try {
			info = "de l'exercice";
			exeOrdre = (Number) modelUtilities.primaryKeyForObject(exercice);
			
			info = "du contrat";
			conOrdre = (Number) modelUtilities.primaryKeyForObject(contrat);
			
			if (typcred != null) {
				info = "du type de cr\u00E9dit";
				tcdOrdre = typcred!=null ? (Number) modelUtilities.primaryKeyForObject(typcred) : null;
			}
			
			if (organ != null) {
				info = "de la ligne budg\u00E9taire";
				orgId = organ!=null ? (Number) modelUtilities.primaryKeyForObject(organ) : null;
			}
		} 
		catch (Exception e) {
			throw new Exception("Impossible d'obtenir l'identifiant "+info);
		}

		return execute(exeOrdre, conOrdre, tcdOrdre, orgId);
	}	
	
}
