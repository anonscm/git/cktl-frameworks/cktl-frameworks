// DO NOT EDIT.  Make changes to AxeStrategique.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAxeStrategique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWAxeStrategique";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys

  // Attributes
  public static final String D_CREATION_KEY = D_CREATION.key();
  public static final String D_MODIFICATION_KEY = D_MODIFICATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOAxeStrategique.class);

  public AxeStrategique localInstanceIn(EOEditingContext editingContext) {
    AxeStrategique localInstance = (AxeStrategique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(EOAxeStrategique.D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    if (EOAxeStrategique.LOG.isDebugEnabled()) {
        EOAxeStrategique.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOAxeStrategique.D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(EOAxeStrategique.D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    if (EOAxeStrategique.LOG.isDebugEnabled()) {
        EOAxeStrategique.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOAxeStrategique.D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(EOAxeStrategique.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (EOAxeStrategique.LOG.isDebugEnabled()) {
        EOAxeStrategique.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOAxeStrategique.LIBELLE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOAxeStrategique.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOAxeStrategique.LOG.isDebugEnabled()) {
        EOAxeStrategique.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOAxeStrategique.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(EOAxeStrategique.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (EOAxeStrategique.LOG.isDebugEnabled()) {
        EOAxeStrategique.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOAxeStrategique.PERS_ID_MODIFICATION_KEY);
  }


  public static AxeStrategique create(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, Integer persIdCreation
, Integer persIdModification
) {
    AxeStrategique eo = (AxeStrategique) EOUtilities.createAndInsertInstance(editingContext, EOAxeStrategique.ENTITY_NAME);    
        eo.setDCreation(dCreation);
        eo.setDModification(dModification);
        eo.setLibelle(libelle);
        eo.setPersIdCreation(persIdCreation);
        eo.setPersIdModification(persIdModification);
    return eo;
  }

  public static ERXFetchSpecification<AxeStrategique> fetchSpec() {
    return new ERXFetchSpecification<AxeStrategique>(EOAxeStrategique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AxeStrategique> fetchAll(EOEditingContext editingContext) {
    return EOAxeStrategique.fetchAll(editingContext, null);
  }

  public static NSArray<AxeStrategique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAxeStrategique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<AxeStrategique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AxeStrategique> fetchSpec = new ERXFetchSpecification<AxeStrategique>(EOAxeStrategique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AxeStrategique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AxeStrategique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAxeStrategique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AxeStrategique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AxeStrategique> eoObjects = EOAxeStrategique.fetchAll(editingContext, qualifier, null);
    AxeStrategique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AxeStrategique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAxeStrategique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AxeStrategique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    AxeStrategique eoObject = EOAxeStrategique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AxeStrategique localInstanceIn(EOEditingContext editingContext, AxeStrategique eo) {
    AxeStrategique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}