// DO NOT EDIT.  Make changes to TypeReconduction.java instead.
package org.cocktail.cocowork.server.metier.convention;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeReconduction extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWTypeReconduction";

  // Attribute Keys
  public static final ERXKey<String> TR_COMMENTAIRE = new ERXKey<String>("trCommentaire");
  public static final ERXKey<String> TR_ID_INTERNE = new ERXKey<String>("trIdInterne");
  public static final ERXKey<String> TR_LIBELLE = new ERXKey<String>("trLibelle");
  // Relationship Keys

  // Attributes
  public static final String TR_COMMENTAIRE_KEY = TR_COMMENTAIRE.key();
  public static final String TR_ID_INTERNE_KEY = TR_ID_INTERNE.key();
  public static final String TR_LIBELLE_KEY = TR_LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeReconduction.class);

  public TypeReconduction localInstanceIn(EOEditingContext editingContext) {
    TypeReconduction localInstance = (TypeReconduction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String trCommentaire() {
    return (String) storedValueForKey(EOTypeReconduction.TR_COMMENTAIRE_KEY);
  }

  public void setTrCommentaire(String value) {
    if (EOTypeReconduction.LOG.isDebugEnabled()) {
        EOTypeReconduction.LOG.debug( "updating trCommentaire from " + trCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeReconduction.TR_COMMENTAIRE_KEY);
  }

  public String trIdInterne() {
    return (String) storedValueForKey(EOTypeReconduction.TR_ID_INTERNE_KEY);
  }

  public void setTrIdInterne(String value) {
    if (EOTypeReconduction.LOG.isDebugEnabled()) {
        EOTypeReconduction.LOG.debug( "updating trIdInterne from " + trIdInterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeReconduction.TR_ID_INTERNE_KEY);
  }

  public String trLibelle() {
    return (String) storedValueForKey(EOTypeReconduction.TR_LIBELLE_KEY);
  }

  public void setTrLibelle(String value) {
    if (EOTypeReconduction.LOG.isDebugEnabled()) {
        EOTypeReconduction.LOG.debug( "updating trLibelle from " + trLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeReconduction.TR_LIBELLE_KEY);
  }


  public static TypeReconduction create(EOEditingContext editingContext, String trIdInterne
, String trLibelle
) {
    TypeReconduction eo = (TypeReconduction) EOUtilities.createAndInsertInstance(editingContext, EOTypeReconduction.ENTITY_NAME);    
        eo.setTrIdInterne(trIdInterne);
        eo.setTrLibelle(trLibelle);
    return eo;
  }

  public static ERXFetchSpecification<TypeReconduction> fetchSpec() {
    return new ERXFetchSpecification<TypeReconduction>(EOTypeReconduction.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeReconduction> fetchAll(EOEditingContext editingContext) {
    return EOTypeReconduction.fetchAll(editingContext, null);
  }

  public static NSArray<TypeReconduction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeReconduction.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeReconduction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeReconduction> fetchSpec = new ERXFetchSpecification<TypeReconduction>(EOTypeReconduction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeReconduction> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeReconduction fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeReconduction.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeReconduction fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeReconduction> eoObjects = EOTypeReconduction.fetchAll(editingContext, qualifier, null);
    TypeReconduction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWTypeReconduction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeReconduction fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeReconduction.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeReconduction fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeReconduction eoObject = EOTypeReconduction.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWTypeReconduction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeReconduction localInstanceIn(EOEditingContext editingContext, TypeReconduction eo) {
    TypeReconduction localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}