package org.cocktail.cocowork.server.metier.convention;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypeAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class TypeAvenant extends EOTypeAvenant 
{
	public static final String CODE_TYPE_AVENANT_INITIAL 		= "INIT";
	public static final String CODE_TYPE_AVENANT_FINANCIER 		= "FIN";
	public static final String CODE_TYPE_AVENANT_ADMINISTRATIF 	= "ADM";
	
	
	/**
	 * Fetch le type d'avenant a partir de son code interne.
	 * @param ec Editing context a utiliser
	 * @param taCode Code interne du type d'avenant (TA_CODE)
	 * @return Le type d'avenant trouve ou null.
	 * @throws ExceptionFinder 
	 */
	public static TypeAvenant TypeAvenantWithCode(
			final EOEditingContext ec, 
			final String taCode) throws ExceptionFinder {
		
		return (TypeAvenant) new FinderTypeAvenant(ec).findWithCode(taCode).lastObject();
	}

	/**
	 * Renvoit le type d'avenant correspondant au conrat initial.
	 * @param ec Editing context a utiliser.
	 * @return Le type de contrat correspondant au conrat initial.
	 * @throws ExceptionFinder
	 */
	static public TypeAvenant TypeAvenantContratInitial(EOEditingContext ec) throws ExceptionFinder {
		
		return (TypeAvenant) new FinderTypeAvenant(ec).findWithCode(TypeAvenant.CODE_TYPE_AVENANT_INITIAL).lastObject();
	}
	
	public static NSArray<TypeAvenant> fetchAllWithoutInitial(EOEditingContext ec) {
	    return fetchAll(ec, TA_CODE.ne(CODE_TYPE_AVENANT_INITIAL), TA_LIBELLE.ascs());
	}
	

}
