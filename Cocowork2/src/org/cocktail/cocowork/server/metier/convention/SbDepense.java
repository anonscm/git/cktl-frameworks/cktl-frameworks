

// SbDepense.java
// 
package org.cocktail.cocowork.server.metier.convention;

import java.math.BigDecimal;
import java.util.Calendar;

import org.cocktail.cocowork.common.date.DateOperation;
import org.cocktail.cocowork.common.nombre.NombreOperation;
import org.cocktail.cocowork.common.tools.factory.Factory;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetParametre;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTva;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;



public class SbDepense extends EOSbDepense implements Budgetable
{

    public SbDepense() {
        super();
    }

	public static SbDepense instanciate(EOEditingContext ec) throws Exception {
		SbDepense depense = (SbDepense) Factory.instanceForEntity(ec, ENTITY_NAME);
		return depense;
	}
	
	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setSdDatePrev(DateCtrl.now());
		setSdMontantHt(BigDecimal.ZERO);
	}
	@Override
	public void setSdLibelle(String aValue) {
		if (aValue != null && aValue.length()>50) {
			aValue = aValue.substring(0,50);
		}
		super.setSdLibelle(aValue);
	}
	
	@Override
	public String sdLibelle() {
		// TODO Auto-generated method stub
		return super.sdLibelle();
	}

	@Override
	public void setTypeCreditRelationship(EOTypeCredit value) {
	    if(value == null || !value.equals(typeCredit())) {
	        // Nettoyage des relations lolf et reparts
	        setLolfRelationship(null);
//	        deleteAllRepartSbDepensePlanComptablesRelationships();
	        super.setTypeCreditRelationship(value);
	    }
	    // Si typeDeCredit est de section 3, on raz la TVA
	    if (typeCredit() != null && typeCredit().isSection3())
	        setTvaRelationship(EOTva.fetchTvaZero(editingContext()));
	        
	}
	
	@Override
	public void validateForSave() throws ValidationException {
	    super.validateForSave();
	    // Vérifier le CR sélectionné en fonction du mode de gestion de la conv
	    if (!tranche().contrat().isModeRA()) {
	        if (organ().isTypeRA())
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Budgetable.ORGAN_KEY, organ(), "ConvSimpleWithBadOrgan"); 
	    } else if (tranche().contrat().isModeRA()) {
	        if (!organ().isTypeRA())
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, Budgetable.ORGAN_KEY, organ(), "ConvRaWithBadOrgan");
	        // On check que cet organ ne soit pas utilisé par d'autres conventions
	        NSArray<Contrat> contratsUtilisantOrgan = tranche().contrat().autresContratsUtilisantOrgan(tranche().exerciceCocktail().exercice(), organ());
	        if (!contratsUtilisantOrgan.isEmpty()) {
	               final NSArray<String> libellesContrat = (NSArray<String>)contratsUtilisantOrgan.valueForKey(Contrat.NUMERO_CONTRAT_KEY);
	               NSDictionary<String, String> stringContainer = new NSDictionary<String, String>(libellesContrat.componentsJoinedByString(", "), "libellesContrat");
	               String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("OrganDejaUtilise", stringContainer);
	               throw new ValidationException(msg);
	        }
	    }
	    // Si la somme est supérieur au montant de la tranche
		if (lolf() != null) { // Destination
			if (tranche().isMontantTrancheLtSommeDepensesDest()) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, SD_MONTANT_HT_KEY, sdMontantHt(), "MontantDepDestTooMuch");
			}
		} else if (planco() != null) { // Nature
			if (tranche().isMontantTrancheLtSommeDepensesNature()) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, SD_MONTANT_HT_KEY, sdMontantHt(), "MontantDepNatureTooMuch");
			}
		}
	    checkMontantAvecCreditsPositionnes(null);
	    if (sdDatePrev() != null) {
	        Calendar cal = Calendar.getInstance();
	        cal.setTime(sdDatePrev());
	        int year = cal.get(Calendar.YEAR);
	        if (year > tranche().exerciceCocktail().exeExercice())
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, SD_DATE_PREV_KEY, sdDatePrev(), "DatePrevAfterExo");
	    }
	    if (lolf() == null && planco() == null)
	        throw ERXValidationFactory.defaultFactory().createCustomException(this, LOLF_KEY, null, "lolf.ou.nature");
	    if (lolf() != null && planco() != null)
	           throw ERXValidationFactory.defaultFactory().createCustomException(this, LOLF_KEY, null, "lolf.ou.nature");
	    
	}
//	AT : utile ??
//	public boolean translateOrgan() {
//        long exoTranche = tranche().exerciceCocktail().exeExercice();
//	    EOOrgan organDepense = this.organ();
//	    boolean hasTranslated = false;
//        NSArray<EOExercice> exosDepense = (NSArray<EOExercice>) organDepense.organExercice().valueForKey(EOOrganExercice.EXERCICE_KEY);
//        if (!exosDepense.isEmpty()) {
//            EOExercice exoOrganDepense = exosDepense.lastObject();
//            // Si l'exercice de l'organ de la depense n'est pas celui de la tranche, on cherche le même organ 
//            // pour l'exercice de la tranche
//            if (!(exoOrganDepense.exeExercice() == exoTranche)) {
//                EOQualifier qual = ERXQ.equals(EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXE_ORDRE_KEY, exoTranche).and(
//                                               EOOrgan.qualFromOrgan(organDepense));
//                EOOrgan organExo = EOOrgan.fetchFirstByQualifier(editingContext(), qual);
//                if (organExo == null) {
//                    NSMutableDictionary<String, String> stringContainer = new NSMutableDictionary<String, String>(libelle(), "depense");
//                    stringContainer.setObjectForKey(Long.toString(exoTranche), "exo");
//                    String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("OrganSurNouvelExoIntrouvable", stringContainer);
//                    throw new ValidationException(msg);
//                } else {
//                    setOrganRelationship(organExo);
//                    hasTranslated = true;
//                }
//            }
//        }
//        return hasTranslated;
//	}
	
	public void checkMontantAvecCreditsPositionnes(BigDecimal montantASoustraire) throws ValidationException {
	       // Dans le cas d'une dépense par nature et que la somme des depenses avec ce planco est inférieure au positionne
	    VDepensesTranche depenses = depensesTranche();
        if (planco() != null && depenses != null) {
            BigDecimal totalPositionneCorrespondant = depenses.montantCreditsPositionnes();
            BigDecimal sommeDepenses = sommeDepensesAvecMemePlanco();
            if (montantASoustraire != null) {
                sommeDepenses = sommeDepenses.subtract(montantASoustraire);
            }
            if (sommeDepenses.compareTo(totalPositionneCorrespondant) < 0) {
                String total = new NombreOperation().getFormatterMoney().format(totalPositionneCorrespondant);
                NSDictionary<String, String> stringContainer = new NSDictionary<String, String>(total, "totalPositionne");
                String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("MontantInferieurPositionneDepense", stringContainer);
                throw new ValidationException(msg);
            }
        }
	}
	
	public BigDecimal sommeDepensesAvecMemePlanco() {
	    NSArray<SbDepense> depenses = tranche().sbDepenses(PLANCO.eq(planco()));
	    return (BigDecimal)depenses.valueForKey("@sum."+SD_MONTANT_HT_KEY);
	}
	
	public void setDefaultValues() {
        if (tranche() != null) {
            // date de la dépense par défaut = date de fin de la tranche
            int annee = tranche().exerciceCocktail().exeExercice();
            setSdDatePrev(DateOperation.lastTimestampOfYear(annee));
            // Si mode RA, on rajoute
            if (tranche().contrat().isModeRA()) {
                EOOrgan organ = tranche().contrat().organComposante();
                setOrganRelationship(organ);
            }
        }
    }
	
	public EOOrgan organForBibasse() {
	    if (organ() != null) {
	        EOExercice exo = tranche().exerciceCocktail().exercice();
	        // TODO :  optimiser, mettre ça en cache
	        int niveau = FinderBudgetParametre.getParametreOrganNiveauSaisie(editingContext(), exo);
	        // En principe niveau = CR ou UB
	        if (organ().orgNiveau().intValue() == niveau)
	            return organ();
	        // Si le niveau est supérieur (plus profond) on prend le père
	        else if (organ().orgNiveau().intValue() > niveau) {
	            EOOrgan currentOrgan = organ().organPere();
	            while (currentOrgan != null && currentOrgan.orgNiveau().intValue() > niveau) {
	                currentOrgan = currentOrgan.organPere();
	            }
	            return currentOrgan;
	        } else if (organ().orgNiveau().intValue() < niveau) {
	            return null;
	        }
	    }
	    return null;
	}

	public VDepensesTranche depensesTranche() {
	    EOQualifier qual = TRANCHE.eq(tranche()).and(PLANCO.eq(planco()));
	    return VDepensesTranche.fetch(editingContext(), qual);
	}
	
    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#repartPlanComptables()
     */
    public NSArray<IRepartMontant> repartPlanComptables() {
//        return repartSbDepensePlanComptables();
        return null;
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#datePrevisionnelle()
     */
    public NSTimestamp datePrevisionnelle() {
        return sdDatePrev();
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#montantHt()
     */
    public BigDecimal montantHt() {
        return sdMontantHt();
    }

    /* 
     * @see org.cocktail.cocowork.server.metier.convention.Budgetable#libelle()
     */
    public String libelle() {
        return sdLibelle();
    }

}
