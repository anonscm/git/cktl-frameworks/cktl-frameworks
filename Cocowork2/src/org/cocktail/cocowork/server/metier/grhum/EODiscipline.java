// DO NOT EDIT.  Make changes to Discipline.java instead.
package org.cocktail.cocowork.server.metier.grhum;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EODiscipline extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWDiscipline";

  // Attribute Keys
  public static final ERXKey<String> DISC_LIBELLE_COURT = new ERXKey<String>("discLibelleCourt");
  public static final ERXKey<String> DISC_LIBELLE_EDITION = new ERXKey<String>("discLibelleEdition");
  public static final ERXKey<String> DISC_LIBELLE_LONG = new ERXKey<String>("discLibelleLong");
  // Relationship Keys

  // Attributes
  public static final String DISC_LIBELLE_COURT_KEY = DISC_LIBELLE_COURT.key();
  public static final String DISC_LIBELLE_EDITION_KEY = DISC_LIBELLE_EDITION.key();
  public static final String DISC_LIBELLE_LONG_KEY = DISC_LIBELLE_LONG.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EODiscipline.class);

  public Discipline localInstanceIn(EOEditingContext editingContext) {
    Discipline localInstance = (Discipline)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String discLibelleCourt() {
    return (String) storedValueForKey(EODiscipline.DISC_LIBELLE_COURT_KEY);
  }

  public void setDiscLibelleCourt(String value) {
    if (EODiscipline.LOG.isDebugEnabled()) {
        EODiscipline.LOG.debug( "updating discLibelleCourt from " + discLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EODiscipline.DISC_LIBELLE_COURT_KEY);
  }

  public String discLibelleEdition() {
    return (String) storedValueForKey(EODiscipline.DISC_LIBELLE_EDITION_KEY);
  }

  public void setDiscLibelleEdition(String value) {
    if (EODiscipline.LOG.isDebugEnabled()) {
        EODiscipline.LOG.debug( "updating discLibelleEdition from " + discLibelleEdition() + " to " + value);
    }
    takeStoredValueForKey(value, EODiscipline.DISC_LIBELLE_EDITION_KEY);
  }

  public String discLibelleLong() {
    return (String) storedValueForKey(EODiscipline.DISC_LIBELLE_LONG_KEY);
  }

  public void setDiscLibelleLong(String value) {
    if (EODiscipline.LOG.isDebugEnabled()) {
        EODiscipline.LOG.debug( "updating discLibelleLong from " + discLibelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EODiscipline.DISC_LIBELLE_LONG_KEY);
  }


  public static Discipline create(EOEditingContext editingContext, String discLibelleCourt
, String discLibelleLong
) {
    Discipline eo = (Discipline) EOUtilities.createAndInsertInstance(editingContext, EODiscipline.ENTITY_NAME);    
        eo.setDiscLibelleCourt(discLibelleCourt);
        eo.setDiscLibelleLong(discLibelleLong);
    return eo;
  }

  public static ERXFetchSpecification<Discipline> fetchSpec() {
    return new ERXFetchSpecification<Discipline>(EODiscipline.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Discipline> fetchAll(EOEditingContext editingContext) {
    return EODiscipline.fetchAll(editingContext, null);
  }

  public static NSArray<Discipline> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EODiscipline.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Discipline> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Discipline> fetchSpec = new ERXFetchSpecification<Discipline>(EODiscipline.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Discipline> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Discipline fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EODiscipline.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Discipline fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Discipline> eoObjects = EODiscipline.fetchAll(editingContext, qualifier, null);
    Discipline eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Discipline fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EODiscipline.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Discipline fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Discipline eoObject = EODiscipline.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Discipline localInstanceIn(EOEditingContext editingContext, Discipline eo) {
    Discipline localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.cocowork.server.metier.grhum.Discipline> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EODiscipline.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.cocowork.server.metier.grhum.Discipline>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.cocowork.server.metier.grhum.Discipline> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EODiscipline.ENTITY_NAME);
    return (NSArray<org.cocktail.cocowork.server.metier.grhum.Discipline>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}