package org.cocktail.cocowork.server.metier.grhum.finder.core;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.cocowork.common.text.StringOperation;
import org.cocktail.cocowork.common.tools.ModelUtilities;
import org.cocktail.cocowork.server.metier.grhum.Discipline;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * Composant permettant de rechercher des disciplines.
 * 
 * @author Michael HALLER, Consortium Cocktail, 2008
 */
public class FinderDiscipline extends Finder
{
	protected Integer discOrdre;
	protected String discLibelleCourt;
	protected String discLibelleLong;
	protected String discLibelleEdition;
	
	
	
	protected EOQualifier qualifierDiscOrdre;
	protected EOQualifier qualifierDiscLibelleLong;
	protected EOQualifier qualifierDiscLibelleCourt;
	protected EOQualifier qualifierDiscLibelleEdition;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 * @param entityName Nom de l'entite du model correspondant aux objets metier recherches.
	 */
	protected FinderDiscipline(EOEditingContext ec, final String entityName) {
		super(ec, entityName);
		
	}
	
	/**
	 * 
	 * @param ec
	 */
	public FinderDiscipline(final EOEditingContext ec) {
		super(ec, Discipline.ENTITY_NAME);
		
		//this.addMandatoryQualifier(EOQualifier.qualifierWithQualifierFormat("temValide = %@", new NSArray("O")));
	}
	

	public void setLibelleCourt(final String libelleCourt, final boolean wholeWord) {
		this.qualifierDiscLibelleCourt = createQualifier(
				"discLibelleCourt caseinsensitivelike %@",
				wholeWord ? libelleCourt : StringOperation.makePatternForSearching(libelleCourt));
		
		this.discLibelleCourt = libelleCourt;
	}
	
	public void setLibelleLong(final String libelleLong, final boolean wholeWord) {
		this.qualifierDiscLibelleLong = createQualifier(
				"discLibelleLong caseinsensitivelike %@",
				wholeWord ? libelleLong : StringOperation.makePatternForSearching(libelleLong));
		
		this.discLibelleLong = libelleLong;
	}
	
	public void setLibelleEdition(final String libelleEdition, final boolean wholeWord) {
		this.qualifierDiscLibelleEdition = createQualifier(
				"discLibelleEdition = %@",
				wholeWord ? libelleEdition : StringOperation.makePatternForSearching(libelleEdition));
		
		this.discLibelleEdition = libelleEdition;
	}
	
	protected void setDiscOrdre(final Integer discOrdre) {
		this.qualifierDiscOrdre = createQualifier(
				"discOrdre = %@",
				discOrdre);
		
		this.discOrdre = discOrdre;
	}

	/**
	 * Supprime tous les criteres de recherche courant.
	 */
	public void clearAllCriteria() {
		this.setDiscOrdre(null);
		this.setLibelleCourt(null, false);
		this.setLibelleLong(null, false);
		this.setLibelleEdition(null, false);
	}
	
	/**
	 * Recherche une structure par son identifiant C_STRUCTURE.
	 * @param cStructure Identifiant de la structure cherchee
	 * @return Structure trouvee ou null.
	 * @throws ExceptionFinder 
	 */
	public Discipline findWithDiscOrdre(final Integer discOrdre) throws ExceptionFinder {

		this.removeOptionalQualifiers();
		setDiscOrdre(discOrdre);
		this.addOptionalQualifier(this.qualifierDiscOrdre);
		
		return (Discipline) super.find().lastObject();
	}
	
	public Discipline findFromGenericRecord(final EOGenericRecord discipline) throws ExceptionFinder {
		
		if (discipline == null)
			throw new NullPointerException("L'objet metier representant la discipline est requis.");
		
		Integer id;
		try {
			id = (Integer) new ModelUtilities().primaryKeyForObject(discipline);
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ExceptionFinder(e.getMessage(), e);
		} 
		
		return findWithDiscOrdre(id);
	}

	public NSArray find() throws ExceptionFinder {
		
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.qualifierDiscLibelleCourt);
		this.addOptionalQualifier(this.qualifierDiscLibelleLong);
		this.addOptionalQualifier(this.qualifierDiscLibelleEdition);
		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Un libell\u00E9 long , un libell\u00E9 court ou un libell\u00E9 edition doit \u00EAtre fourni pour rechercher une discipline.");

		
		
		
		// fetch
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
