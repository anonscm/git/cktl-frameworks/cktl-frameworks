// EOTypcred.java
// Created on Mon May 22 09:39:49 Europe/Paris 2006 by Apple EOModeler Version 5.2

package org.cocktail.cocowork.server.metier.gfc;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOGenericRecord;

public class EOTypcred extends EOGenericRecord {

    public EOTypcred() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public EOTypcred(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }
*/


    public String tcdSect() {
        return (String)storedValueForKey("tcdSect");
    }

    public void setTcdSect(String value) {
        takeStoredValueForKey(value, "tcdSect");
    }

    public BigDecimal tcdPresident() {
        return (BigDecimal)storedValueForKey("tcdPresident");
    }

    public void setTcdPresident(BigDecimal value) {
        takeStoredValueForKey(value, "tcdPresident");
    }

    public String tcdLib() {
        return (String)storedValueForKey("tcdLib");
    }

    public void setTcdLib(String value) {
        takeStoredValueForKey(value, "tcdLib");
    }

    public String tcdAbrege() {
        return (String)storedValueForKey("tcdAbrege");
    }

    public void setTcdAbrege(String value) {
        takeStoredValueForKey(value, "tcdAbrege");
    }

//    public NSArray plancoCredits() {
//        return (NSArray)storedValueForKey("plancoCredits");
//    }
//
//    public void setPlancoCredits(NSArray value) {
//        takeStoredValueForKey(value, "plancoCredits");
//    }
//
//    public void addToPlancoCredits(cocowork.eof.client.gfc.EOPlancoCredit object) {
//        includeObjectIntoPropertyWithKey(object, "plancoCredits");
//    }
//
//    public void removeFromPlancoCredits(cocowork.eof.client.gfc.EOPlancoCredit object) {
//        excludeObjectFromPropertyWithKey(object, "plancoCredits");
//    }
}
