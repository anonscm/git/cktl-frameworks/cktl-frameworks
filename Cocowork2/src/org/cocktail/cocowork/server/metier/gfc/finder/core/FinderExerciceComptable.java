package org.cocktail.cocowork.server.metier.gfc.finder.core;

import org.cocktail.cocowork.common.exception.ExceptionFinder;
import org.cocktail.cocowork.common.exception.ExceptionNotFound;
import org.cocktail.cocowork.common.finder.Finder;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class FinderExerciceComptable extends Finder
{
	protected Number exeExercice;
	protected Number exeOrdre;
	protected String exeStat;
	
	protected EOQualifier qualifierExeExercice;
	protected EOQualifier qualifierExeOrdre;
	protected EOQualifier qualifierExeStat;
	
	
	public FinderExerciceComptable(final EOEditingContext ec) {
		super(ec, EOExercice.ENTITY_NAME);
		
	}

	/**
	 * Change le critere.
	 * @param exeStat Etat de l'exercice (ex: Exercice.EXERCICE_ETAT_OUVERT)
	 */
	public void setExeStat(final String exeStat) {
		this.qualifierExeStat = createQualifier(
				"exeStat = %@", 
				exeStat);
		this.exeStat = exeStat;
	}
	/**
	 * Change le critere.
	 * @param exeExercice Annee de l'exercice (ex: 2006)
	 */
	protected void setExeExercice(final Number exeExercice) {
		this.qualifierExeExercice = createQualifier(
				"exeExercice = %@",
				exeExercice);
		this.exeExercice = exeExercice;
	}
	/**
	 * Change le critere.
	 * @param exeOrdre Identifiant de l'exercice
	 */
	protected void setExeOrdre(final Number exeOrdre) {
		this.qualifierExeOrdre = createQualifier(
				"exeOrdre = %@", 
				exeOrdre);
		this.exeOrdre = exeOrdre;
	}

	
	/**
	 * Supprime tous les criteres de recherche courants
	 */
	public void clearAllCriteria() {
		setExeStat(null);
		setExeExercice(null);
		setExeOrdre(null);
	}

	/**
	 * Recherche un exercice dans JEFY_ADMIN.EXERCICE selon son etat.
	 * @param exeStat Etat de l'exercice recherche. Ex: {@link ExerciceComptable#EXERCICE_ETAT_OUVERT}.
	 * @return L'exercice trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExercice findExerciceComptable(final String exeStat) throws ExceptionFinder {
		
		if (exeStat == null)
			throw new NullPointerException("Etat de l'exercice requis.");
		
		clearAllCriteria();
		this.removeOptionalQualifiers();
		setExeStat(EOExercice.EXE_ETAT_OUVERT);
		this.addOptionalQualifier(this.qualifierExeStat);
		
		return (EOExercice) super.find().lastObject();
	}
	/**
	 * Recherche un exercice dans JEFY_ADMIN.EXERCICE selon son etat, avec erreur si rien n'est trouve.
	 * @param exeStat Etat de l'exercice recherche. Ex: {@link ExerciceComptable#EXERCICE_ETAT_OUVERT}.
	 * @return L'exercice trouve.
	 * @throws ExceptionNotFound Pas trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExercice findMandatoryExerciceComptable(final String exeStat) throws ExceptionFinder, ExceptionNotFound {

	    EOExercice eo = findExerciceComptable(exeStat);
		if (eo == null)
			throw new ExceptionNotFound("L'exercice courant (ouvert) n'a pas \u00E9t\u00E9 trouv\u00E9.");
		
		return eo;
	}

	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public EOExercice findWithExeExercice(final Number exeExercice) throws ExceptionFinder { 
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeExercice(exeExercice);
		addOptionalQualifier(this.qualifierExeExercice);
		
		return (EOExercice) super.find().lastObject();
	}
	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public EOExercice findWithExeOrdre(final Number exeOrdre) throws ExceptionFinder { 
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeOrdre(exeOrdre);
		addOptionalQualifier(this.qualifierExeOrdre);
		
		return (EOExercice) super.find().lastObject();
	}

	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public NSArray find() throws ExceptionFinder { 
		
		this.removeOptionalQualifiers();
		this.addOptionalQualifier(this.qualifierExeExercice);
		this.addOptionalQualifier(this.qualifierExeStat);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
