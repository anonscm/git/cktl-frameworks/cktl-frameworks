// DO NOT EDIT.  Make changes to Fonction.java instead.
package org.cocktail.cocowork.server.metier.gfc;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOFonction extends  CktlServerRecord {
  public static final String ENTITY_NAME = "CWFonction";

  // Attribute Keys
  public static final ERXKey<String> FON_CATEGORIE = new ERXKey<String>("fonCategorie");
  public static final ERXKey<String> FON_DESCRIPTION = new ERXKey<String>("fonDescription");
  public static final ERXKey<String> FON_ID_INTERNE = new ERXKey<String>("fonIdInterne");
  public static final ERXKey<String> FON_LIBELLE = new ERXKey<String>("fonLibelle");
  public static final ERXKey<String> FON_SPEC_EXERCICE = new ERXKey<String>("fonSpecExercice");
  public static final ERXKey<String> FON_SPEC_GESTION = new ERXKey<String>("fonSpecGestion");
  public static final ERXKey<String> FON_SPEC_ORGAN = new ERXKey<String>("fonSpecOrgan");
  // Relationship Keys

  // Attributes
  public static final String FON_CATEGORIE_KEY = FON_CATEGORIE.key();
  public static final String FON_DESCRIPTION_KEY = FON_DESCRIPTION.key();
  public static final String FON_ID_INTERNE_KEY = FON_ID_INTERNE.key();
  public static final String FON_LIBELLE_KEY = FON_LIBELLE.key();
  public static final String FON_SPEC_EXERCICE_KEY = FON_SPEC_EXERCICE.key();
  public static final String FON_SPEC_GESTION_KEY = FON_SPEC_GESTION.key();
  public static final String FON_SPEC_ORGAN_KEY = FON_SPEC_ORGAN.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOFonction.class);

  public Fonction localInstanceIn(EOEditingContext editingContext) {
    Fonction localInstance = (Fonction)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String fonCategorie() {
    return (String) storedValueForKey(EOFonction.FON_CATEGORIE_KEY);
  }

  public void setFonCategorie(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonCategorie from " + fonCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_CATEGORIE_KEY);
  }

  public String fonDescription() {
    return (String) storedValueForKey(EOFonction.FON_DESCRIPTION_KEY);
  }

  public void setFonDescription(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonDescription from " + fonDescription() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_DESCRIPTION_KEY);
  }

  public String fonIdInterne() {
    return (String) storedValueForKey(EOFonction.FON_ID_INTERNE_KEY);
  }

  public void setFonIdInterne(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonIdInterne from " + fonIdInterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_ID_INTERNE_KEY);
  }

  public String fonLibelle() {
    return (String) storedValueForKey(EOFonction.FON_LIBELLE_KEY);
  }

  public void setFonLibelle(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonLibelle from " + fonLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_LIBELLE_KEY);
  }

  public String fonSpecExercice() {
    return (String) storedValueForKey(EOFonction.FON_SPEC_EXERCICE_KEY);
  }

  public void setFonSpecExercice(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonSpecExercice from " + fonSpecExercice() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_SPEC_EXERCICE_KEY);
  }

  public String fonSpecGestion() {
    return (String) storedValueForKey(EOFonction.FON_SPEC_GESTION_KEY);
  }

  public void setFonSpecGestion(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonSpecGestion from " + fonSpecGestion() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_SPEC_GESTION_KEY);
  }

  public String fonSpecOrgan() {
    return (String) storedValueForKey(EOFonction.FON_SPEC_ORGAN_KEY);
  }

  public void setFonSpecOrgan(String value) {
    if (EOFonction.LOG.isDebugEnabled()) {
        EOFonction.LOG.debug( "updating fonSpecOrgan from " + fonSpecOrgan() + " to " + value);
    }
    takeStoredValueForKey(value, EOFonction.FON_SPEC_ORGAN_KEY);
  }


  public static Fonction create(EOEditingContext editingContext, String fonCategorie
, String fonIdInterne
, String fonLibelle
, String fonSpecExercice
, String fonSpecGestion
, String fonSpecOrgan
) {
    Fonction eo = (Fonction) EOUtilities.createAndInsertInstance(editingContext, EOFonction.ENTITY_NAME);    
        eo.setFonCategorie(fonCategorie);
        eo.setFonIdInterne(fonIdInterne);
        eo.setFonLibelle(fonLibelle);
        eo.setFonSpecExercice(fonSpecExercice);
        eo.setFonSpecGestion(fonSpecGestion);
        eo.setFonSpecOrgan(fonSpecOrgan);
    return eo;
  }

  public static ERXFetchSpecification<Fonction> fetchSpec() {
    return new ERXFetchSpecification<Fonction>(EOFonction.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Fonction> fetchAll(EOEditingContext editingContext) {
    return EOFonction.fetchAll(editingContext, null);
  }

  public static NSArray<Fonction> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOFonction.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Fonction> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Fonction> fetchSpec = new ERXFetchSpecification<Fonction>(EOFonction.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Fonction> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Fonction fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOFonction.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Fonction fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Fonction> eoObjects = EOFonction.fetchAll(editingContext, qualifier, null);
    Fonction eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one CWFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Fonction fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOFonction.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Fonction fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Fonction eoObject = EOFonction.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no CWFonction that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Fonction localInstanceIn(EOEditingContext editingContext, Fonction eo) {
    Fonction localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}