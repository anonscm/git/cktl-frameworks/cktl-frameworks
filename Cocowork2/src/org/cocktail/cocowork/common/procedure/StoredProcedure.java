/**
 * Cocowork2
 * Michael Haller
 * 2008
 *
 */
package org.cocktail.cocowork.common.procedure;


//import org.cocktail.cocowork.common.exception.ExceptionClientSideRequest;

import org.cocktail.cocowork.common.exception.ExceptionClientSideRequest;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;


/**
 * 
 * 
 * @author Michael HALLER, Consortium Cocktail, 2008
 * 
 */
public abstract class StoredProcedure 
{
	protected EOEditingContext ec;
	protected String name;
	protected String[] orderedArgumentsNames;
	
	
	/**
	 * 
	 * Constructeur.
	 * @param ec
	 * @param name
	 */
	protected StoredProcedure(final EOEditingContext ec, final String name, final String[] orderedArgumentsNames) {
		this.ec = ec;
		this.name = name;
		this.orderedArgumentsNames = orderedArgumentsNames;
	}
	
	/**
	 * Execute la procedure stoquee avec les arguments specifies.
	 * @param orderedArgumentsValues Liste ordonnee des arguments a passer a la procedure stoquee.
	 * @return Un dictionnaire resultat.
	 * @throws ExceptionClientSideRequest Erreur concernant l'appel de la methode distante.
	 */
	public NSDictionary execute(final Object[] orderedArgumentsValues) throws Exception {

		NSMutableDictionary argumentsDico = new NSMutableDictionary();
		for (int i=0; i<this.orderedArgumentsNames.length; i++) {
			argumentsDico.takeValueForKey(orderedArgumentsValues[i], this.orderedArgumentsNames[i]);
		}
		
		NSDictionary returnedDico;
		try {
			returnedDico=executeStoredProcedureNamed(this.ec, this.name, argumentsDico);
			
		//	returnedDico = (NSDictionary) ((EODistributedObjectStore) ec.parentObjectStore()).invokeRemoteMethodWithKeyPath(
		//			ec, 
		//			"session", 
		//			"clientSideRequestExecuteStoredProcedureNamed", 
		//			new Class[] { String.class, NSDictionary.class }, 
		//			new Object[] { this.name, argumentsDico/*.immutableClone()*/ }, 
		//			true);
					
			
		} 
		catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
		return returnedDico;
	}
	
    public static NSDictionary executeStoredProcedureNamed(EOEditingContext lEC, String leNom, NSDictionary lesArguments) {
        NSDictionary leDico = null;
        leDico = EOUtilities.executeStoredProcedureNamed(lEC, leNom, lesArguments);
        return leDico;
    }
}
