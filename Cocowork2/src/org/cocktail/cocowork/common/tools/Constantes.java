package org.cocktail.cocowork.common.tools;

public class Constantes {

    public static final String BUDGET_LIGHT_CONV_SIMPLE_PARAM = "BUDGET_LIGHT_CONV_SIMPLE";
    public static final String GESTION_TRANCHE_AUTO_PARAM = "GESTION_TRANCHE_AUTO";
    public static final String AUTRES_ORGANS_BUDGET_PARAM = "AUTRES_ORGANS_BUDGET";
    
}
