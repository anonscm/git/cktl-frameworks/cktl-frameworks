package org.cocktail.cocowork.common.nombre;

import java.math.BigDecimal;

import org.cocktail.cocowork.common.nombre.number.translate.FrenchDef;
import org.cocktail.cocowork.common.nombre.number.translate.Nombre;

import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestampFormatter;


/**
 * Utilitaire autour des nombres.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2006
 */
public class NombreOperation 
{
	public final static int SCALE_MONTANT 		= 2;

	/** Formatter pour les champs monetaires */
	protected NSNumberFormatter FORMATTER_MONEY = new NSNumberFormatter("#,##0.00 \u20AC;0.00 \u20AC;-#,##0.00 \u20AC");
	/** Formatter pour l'affichage des numeros d'index (sur 3 caracteres) */
	protected NSNumberFormatter FORMATTER_INDEX = new NSNumberFormatter("000;-000");
	/** Formatter de dates utilise par toute l'application */
	protected NSTimestampFormatter FORMATTER_DATE = new NSTimestampFormatter("%d/%m/%Y");
	/** Formatter de dates utilise par toute l'application */
	protected NSTimestampFormatter FORMATTER_TIME = new NSTimestampFormatter("%H:%M");
	/** Formatter de dates utilise par toute l'application */
	protected NSTimestampFormatter FORMATTER_DATE_TIME = new NSTimestampFormatter("%d/%m/%Y %H:%M");
	
	

	/**
	 * Constructeur.
	 */
	public NombreOperation() {
		
		FORMATTER_MONEY.setLocalizesPattern(true);
		
	}

	/**
	 * Formateur pour la date sans l'heure.
	 * @return Le formateur
	 * @see NSNumberFormatter#format(Object)
	 */
	public NSTimestampFormatter getFormatterDate() {
		return FORMATTER_DATE;
	}
	/**
	 * Formateur pour la date avec l'heure.
	 * @return Le formateur
	 * @see NSNumberFormatter#format(Object)
	 */
	public NSTimestampFormatter getFormatterDateTime() {
		return FORMATTER_DATE_TIME;
	}
	/**
	 * Formateur d'entiers sur 4 digits (ex: "0193" pour les numeros d'index des conventions).
	 * @return Le formateur
	 * @see NSNumberFormatter#format(Object)
	 */
	public NSNumberFormatter getFormatterIndex() {
		return FORMATTER_INDEX;
	}
	/**
	 * Formateur pour les montants financiers
	 * @return Le formateur
	 * @see NSNumberFormatter#format(Object)
	 */
	public NSNumberFormatter getFormatterMoney() {
		return FORMATTER_MONEY;
	}
	/**
	 * Formateur pour l'heure.
	 * @return Le formateur
	 * @see NSNumberFormatter#format(Object)
	 */
	public NSTimestampFormatter getFormatterTime() {
		return FORMATTER_TIME;
	}
	
	
	/**
	 * Traduction d'un BigDecimal en toutes lettres, partie decimale comprise.
	 * @param value Valeur a traduire. La partie decimale est arrondie a 2 chiffres.
	 * @return La chaine de caractere representant le BigDecimal.
	 */
	public String traduireEnLettres(final BigDecimal value) {
		if (value.signum() == 0) 
			return "0 euros";
			
		BigDecimal montant = (BigDecimal) value;
		String partieEntiereEnLettres="", partieDecimaleEnLettres="";
		StringBuffer montantEnLettre = new StringBuffer();
			
		montant.setScale(2, BigDecimal.ROUND_HALF_UP);
		long partieEntiere = (long) montant.intValue();
		long partieDecimale = (long) ((montant.doubleValue() - (double) montant.intValue())*100.0 + 0.5);
		
		Nombre.declareLanguage(new FrenchDef());
		Nombre nombrePartieEntiere = new Nombre(0, 0, 0, partieEntiere);
		Nombre nombrePartieDecimale = new Nombre(0, 0, 0, partieDecimale);
		if (partieEntiere!=0 && nombrePartieEntiere.isValid()) 
			partieEntiereEnLettres = nombrePartieEntiere.toLetters();
		if (partieDecimale!=0 && nombrePartieDecimale.isValid()) 
			partieDecimaleEnLettres = nombrePartieDecimale.toLetters();
		
		if (partieEntiereEnLettres.length() > 0) {
			partieEntiereEnLettres = partieEntiereEnLettres.substring(0,1).toUpperCase() + partieEntiereEnLettres.substring(1);//majuscule
			montantEnLettre.append(partieEntiereEnLettres);
			montantEnLettre.append(" euros");
			
			if (partieDecimaleEnLettres.length() > 0)
				montantEnLettre.append(" et ");
		}
		if (partieDecimale!=0 && partieDecimaleEnLettres.length()>0)  {
			if (partieEntiereEnLettres.length() < 1)
				partieDecimaleEnLettres = partieDecimaleEnLettres.substring(0,1).toUpperCase() + partieDecimaleEnLettres.substring(1);//maj
			montantEnLettre.append(partieDecimaleEnLettres);
			montantEnLettre.append(" cts");
		}
		
		return montantEnLettre.toString();
	}
	

	/**
	 * <p>Calcul des montants des N prelevements a partir d'un montant global d'echeancier 
	 * <p>Strategie : Division du montant global en N montants, avec ajustement du dernier montant car chaque 
	 * montant est arrondi sur 2 decimales. Cela assure qu'on a bien 
	 * Somme(montants_echeances) = montant_global_echeancier.
	 * @param montantGlobal Montant global de l'echeancier
	 * @param nombreEcheances Nombre de prelevements
	 * @return La liste ordonnee des montants des prelevements
	 */
	public BigDecimal[] calculerMontantsEcheances(
			final BigDecimal 	montantGlobal, 
			final int 			nombreEcheances) {
		
		BigDecimal montant, reste, nb, nb_1;
		BigDecimal[] montants = new BigDecimal[nombreEcheances];
		
		nb = new BigDecimal(nombreEcheances);
		nb_1 = new BigDecimal(nombreEcheances - 1);
		
		montant = montantGlobal.divide(
				nb, SCALE_MONTANT, BigDecimal.ROUND_HALF_UP);
		reste = montantGlobal.subtract(
				montant.multiply(nb_1)).setScale(SCALE_MONTANT, BigDecimal.ROUND_HALF_UP);
		
		int i;
		for (i = 0; i < nombreEcheances - 1; i++) 
			montants[i] = montant;
		
		montants[i] = reste;
		
		return montants;
	}
	
	/**
	 * <p>Calcul des montants des N prelevements a partir d'un montant global d'echeancier, sachant que le montant
	 * du premier prelevement est contraint.
	 * @param montantGlobal Montant global de l'echeancier.
	 * @param nombreEcheances Nombre de prelevements.
	 * @param premierMontant Montant demande pour le premier prelevement.
	 * @return La liste ordonnee des montants des prelevements.
	 * @see #calculerMontantsPrelevementsPourMontantGlobalEcheancier(BigDecimal, int)
	 */
	public BigDecimal[] calculerMontantsEcheances(
			final BigDecimal 	montantGlobal, 
			final int 			nombreEcheances, 
			final BigDecimal 	premierMontant) {
		
		// montant 1er echeance null, nul ou negatif interdit
		if (premierMontant!=null && premierMontant.signum()>0) {
			BigDecimal[] montants = new BigDecimal[nombreEcheances];
			montants[0] = premierMontant;
			
			BigDecimal[] montantsSuivants = calculerMontantsEcheances(
					montantGlobal.subtract(premierMontant), 
					nombreEcheances - 1);
			
			for (int i=1; i<nombreEcheances; i++)
				montants[i] = montantsSuivants[i - 1];
				
			return montants;
		}
		else
			return calculerMontantsEcheances(
					montantGlobal, 
					nombreEcheances);
	}
}
