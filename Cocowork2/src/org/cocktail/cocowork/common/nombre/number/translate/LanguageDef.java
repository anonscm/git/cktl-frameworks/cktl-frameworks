package org.cocktail.cocowork.common.nombre.number.translate;


// Referenced classes of package nombre:
//            Vocab, Tree

public interface LanguageDef
{

    public abstract int getLanguage();

    public abstract int[] getCountries();

    public abstract Vocab getVocab();

    public abstract Tree ofLong(int i, int j, long l);

    public abstract Tree ofLetters(String s);

    public abstract String toLetters(int i, int j, Tree tree);
}
