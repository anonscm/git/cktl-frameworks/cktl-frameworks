package org.cocktail.cocowork.common.nombre.number.translate;


// Referenced classes of package nombre:
//            Vocab, LanguageDef, Tree, IntList

public class FrenchDef
    implements LanguageDef
{

    private int language;
    private int countries[];
    private Vocab vocab;

    private void init()
    {
        language = 0;
        countries = new int[3];
        countries[0] = 1;
        countries[1] = 0;
        countries[2] = 2;
        vocab = new Vocab();
        vocab.addWord(1002, "vingts", 20);
        vocab.addWord(1002, "cents", 100);
        vocab.addWord(1002, "millions", 0xf4240);
        vocab.addWord(1002, "milliards", 0x3b9aca00);
        vocab.addWord(1001, "z\351ro", 0);
        vocab.addWord(1001, "un", 1);
        vocab.addWord(1001, "deux", 2);
        vocab.addWord(1001, "trois", 3);
        vocab.addWord(1001, "quatre", 4);
        vocab.addWord(1001, "cinq", 5);
        vocab.addWord(1001, "six", 6);
        vocab.addWord(1001, "sept", 7);
        vocab.addWord(1001, "huit", 8);
        vocab.addWord(1001, "neuf", 9);
        vocab.addWord(1001, "dix", 10);
        vocab.addWord(1001, "onze", 11);
        vocab.addWord(1001, "douze", 12);
        vocab.addWord(1001, "treize", 13);
        vocab.addWord(1001, "quatorze", 14);
        vocab.addWord(1001, "quinze", 15);
        vocab.addWord(1001, "seize", 16);
        vocab.addWord(1001, "vingt", 20);
        vocab.addWord(1001, "trente", 30);
        vocab.addWord(1001, "quarante", 40);
        vocab.addWord(1001, "cinquante", 50);
        vocab.addWord(1001, "soixante", 60);
        vocab.addWord(1001, "septante", 70);
        vocab.addWord(1001, "huitante", 80);
        vocab.addWord(1001, "nonante", 90);
        vocab.addWord(1001, "cent", 100);
        vocab.addWord(1001, "mille", 1000);
        vocab.addWord(1001, "million", 0xf4240);
        vocab.addWord(1001, "milliard", 0x3b9aca00);
        vocab.addWord(0, "une", 1);
        vocab.addWord(1003, "uni\350me", 1);
        vocab.addWord(1003, "deuxi\350me", 2);
        vocab.addWord(1003, "troisi\350me", 3);
        vocab.addWord(1003, "quatri\350me", 4);
        vocab.addWord(1003, "cinqui\350me", 5);
        vocab.addWord(1003, "sixi\350me", 6);
        vocab.addWord(1003, "septi\350me", 7);
        vocab.addWord(1003, "huiti\350me", 8);
        vocab.addWord(1003, "neuvi\350me", 9);
        vocab.addWord(1003, "dixi\350me", 10);
        vocab.addWord(1003, "onzi\350me", 11);
        vocab.addWord(1003, "douzi\350me", 12);
        vocab.addWord(1003, "treizi\350me", 13);
        vocab.addWord(1003, "quatorzi\350me", 14);
        vocab.addWord(1003, "quinzi\350me", 15);
        vocab.addWord(1003, "seizi\350me", 16);
        vocab.addWord(1003, "vingti\350me", 20);
        vocab.addWord(1003, "trenti\350me", 30);
        vocab.addWord(1003, "quaranti\350me", 40);
        vocab.addWord(1003, "cinquanti\350me", 50);
        vocab.addWord(1003, "soixanti\350me", 60);
        vocab.addWord(1003, "septanti\350me", 70);
        vocab.addWord(1003, "huitanti\350me", 80);
        vocab.addWord(1003, "nonanti\350me", 90);
        vocab.addWord(1003, "centi\350me", 100);
        vocab.addWord(1003, "milli\350me", 1000);
        vocab.addWord(1003, "millioni\350me", 0xf4240);
        vocab.addWord(1003, "milliardi\350me", 0x3b9aca00);
        vocab.addWord(0, "premi\350re", 1);
        vocab.addWord(0, "second", 2);
        vocab.addWord(0, "seconde", 2);
        vocab.addWord(1004, "premier", 1);
        vocab.addLanguage(0, "Fran\347ais");
        vocab.addLanguage(1, "Anglais");
        vocab.addLanguage(2, "Espagnol");
        vocab.addCountry(1, "Belgique");
        vocab.addCountry(0, "France");
        vocab.addCountry(2, "Suisse");
        vocab.addCountry(3, "UK");
        vocab.addCountry(4, "Espagne");
        vocab.addMode(0, "Cardinal");
        vocab.addMode(1, "Ordinal");
        vocab.addMode(2, "Date");
        vocab.addLink(1, "et");
        vocab.addLink(2, "de");
        vocab.addLink(3, "-");
        vocab.addLink(4, " ");
        vocab.addLink(5, " et ");
        vocab.addLink(6, "-et-");
        vocab.addLink(7, " de ");
        vocab.addMisc("translate", "Traduire");
        vocab.addMisc("reset", "Effacer");
        vocab.addMisc("import", "Importer");
    }

    public int getLanguage()
    {
        return language;
    }

    public int[] getCountries()
    {
        return countries;
    }

    public Vocab getVocab()
    {
        return vocab;
    }

    private Tree decompPlus(int i, int j, long l, int k)
    {
        Tree tree = decomp(i, j, (l / (long)k) * (long)k);
        if(l % (long)k != 0L)
        {
            tree = Tree.newPlus(tree, decomp(i, j, l % (long)k));
        }
        return tree;
    }

    private Tree decompMult(int i, int j, long l, int k)
    {
        Tree tree = Tree.newNum(k);
        if(l / (long)k != 1L || k != 100 && k != 1000)
        {
            tree = Tree.newMult(decomp(i, j, l / (long)k), tree);
        }
        if(l % (long)k != 0L)
        {
            tree = Tree.newPlus(tree, decomp(i, j, l % (long)k));
        }
        return tree;
    }

    private Tree decomp(int i, int j, long l)
    {
        if(l <= 16L)
        {
            return Tree.newNum(l);
        }
        if(i == 0 && (l == 70L || l == 90L))
        {
            return decompPlus(i, j, l, 20);
        }
        if((i == 0 || i == 1) && l == 80L)
        {
            return decompMult(i, j, l, 20);
        }
        if(i == 0 && l > 60L && l <= 99L)
        {
            return decompPlus(i, j, l, 20);
        }
        if(l < 100L && l % 10L == 0L)
        {
            return Tree.newNum(l);
        }
        if(l < 100L)
        {
            return decompPlus(i, j, l, 10);
        }
        if(l < 1000L)
        {
            return decompMult(i, j, l, 100);
        }
        if(l < 0xf4240L)
        {
            return decompMult(i, j, l, 1000);
        }
        if(l < 0x3b9aca00L)
        {
            return decompMult(i, j, l, 0xf4240);
        } else
        {
            return decompMult(i, j, l, 0x3b9aca00);
        }
    }

    public Tree ofLong(int i, int j, long l)
    {
        Tree tree;
        if(j == 2 && l >= 1000L && l < 2000L)
        {
            tree = decompMult(i, j, l, 100);
        } else
        {
            tree = decomp(i, j, l);
        }
        doLink(i, j, tree);
        return tree;
    }

    private Tree insertNum(Tree tree, long l)
    {
        switch(tree.oper)
        {
        case 1: // '\001'
            if(tree.left.pivot() > l)
            {
                return Tree.newPlus(tree.left, insertNum(tree.right, l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }

        case 0: // '\0'
            if(tree.pivot() > l)
            {
                return Tree.newPlus(tree, Tree.newNum(l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }

        case 2: // '\002'
            if(tree.pivot() > l)
            {
                return Tree.newPlus(tree, Tree.newNum(l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }
        }
        return null;
    }

    private Tree ofIntList(Tree tree, IntList intlist)
    {
        if(intlist == null)
        {
            return tree;
        } else
        {
            return ofIntList(insertNum(tree, intlist.hd), intlist.tl);
        }
    }

    private Tree ofIntList(IntList intlist)
    {
        return ofIntList(Tree.newNum(intlist.hd), intlist.tl);
    }

    public Tree ofLetters(String s)
    {
        IntList intlist = IntList.ofLetters(vocab, s);
        if(intlist == null)
        {
            return null;
        } else
        {
            return ofIntList(intlist);
        }
    }

    private void doLink(int i, int j, Tree tree)
    {
        link(tree, true, false);
        if(j == 1)
        {
            if(tree.matchNum(1L))
            {
                tree.link = 1004;
            } else
            {
                linkOrdinal(tree);
            }
        }
    }

    private void link(Tree tree, boolean flag, boolean flag1)
    {
        switch(tree.oper)
        {
        default:
            break;

        case 1: // '\001'
            link(tree.left, false, false);
            link(tree.right, flag, false);
            if(tree.left.value >= 100L)
            {
                tree.link = 4;
                break;
            }
            if(tree.left.matchNum(60L) && tree.right.matchNum(11L))
            {
                tree.link = 6;
                break;
            }
            if(tree.right.matchNum(1L) && !tree.left.matchNumMultNum(4L, 20L))
            {
                tree.link = 6;
            } else
            {
                tree.link = 3;
            }
            break;

        case 0: // '\0'
            if((tree.left.matchMultNum(0xf4240L) || tree.left.matchMultNum(0x3b9aca00L)) && (tree.right.matchNum(0xf4240L) || tree.right.matchNum(0x3b9aca00L)))
            {
                link(tree.left, true, false);
                tree.link = 7;
            } else
            {
                link(tree.left, false, false);
                if(tree.right.matchNum(20L))
                {
                    tree.link = 3;
                } else
                {
                    tree.link = 4;
                }
            }
            if(tree.left.value > 1L)
            {
                link(tree.right, flag, true);
            } else
            {
                link(tree.right, flag, false);
            }
            break;

        case 2: // '\002'
            if(flag && flag1 && (tree.value == 20L || tree.value == 100L || tree.value == 0xf4240L || tree.value == 0x3b9aca00L))
            {
                tree.link = 1002;
            } else
            {
                tree.link = 1001;
            }
            break;
        }
    }

    private void linkOrdinal(Tree tree)
    {
        switch(tree.oper)
        {
        case 0: // '\0'
        case 1: // '\001'
            linkOrdinal(tree.right);
            break;

        case 2: // '\002'
            tree.link = 1003;
            break;
        }
    }

    public String toLetters(int i, int j, Tree tree)
    {
        return toLetters(tree);
    }

    private String toLetters(Tree tree)
    {
        switch(tree.oper)
        {
        case 0: // '\0'
        case 1: // '\001'
            return toLetters(tree.left) + vocab.wordOfLink(tree.link) + toLetters(tree.right);

        case 2: // '\002'
            if(tree.value > 0x3b9aca00L)
            {
                return Long.toString(tree.value);
            }
            String s = vocab.wordOfInt(tree.link, (int)tree.value);
            if(s == null)
            {
                return Long.toString(tree.value);
            } else
            {
                return s;
            }
        }
        return null;
    }

    public FrenchDef()
    {
        init();
    }
}
