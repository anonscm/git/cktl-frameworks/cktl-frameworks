package org.cocktail.cocowork.common.nombre.number.translate;


public class Country
{

    public static final int FRANCE = 0;
    public static final int BELGIUM = 1;
    public static final int SWISS = 2;
    public static final int UK = 3;
    public static final int SPAIN = 4;
    public static final int NBR = 5;

    public Country()
    {
    }
}
