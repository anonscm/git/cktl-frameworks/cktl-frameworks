package org.cocktail.cocowork.common.nombre.number.translate;


// Referenced classes of package nombre:
//            LanguageDef, Tree

public class Nombre
{

    private static LanguageDef languages_defs[];
    private int language;
    private int country;
    private int mode;
    private Tree tree;

    public static LanguageDef getLanguageDef(int i)
    {
        return languages_defs[i];
    }

    public static void declareLanguage(LanguageDef languagedef)
    {
        languages_defs[languagedef.getLanguage()] = languagedef;
    }

    public int getLanguage()
    {
        return language;
    }

    public int getCountry()
    {
        return country;
    }

    public int getMode()
    {
        return mode;
    }

    public boolean isValid()
    {
        return tree != null;
    }

    public long toLong()
    {
        return tree.toLong();
    }

    public String toLetters()
    {
        return languages_defs[language].toLetters(country, mode, tree);
    }

    public String toArith()
    {
        return tree.toArith();
    }

    public String toDigits()
    {
        long l = tree.value;
        if(l == 0L)
        {
            return "0";
        }
        int i = 0;
        String s = "";
        while(l > 0L) 
        {
            s = String.valueOf((char)(int)(l % 10L + 48L)) + s;
            l /= 10L;
            if(++i % 3 == 0 && l > 0L)
            {
                s = " " + s;
            }
        }
        return s;
    }

    public Nombre(int i, int j, int k, long l)
    {
        language = i;
        country = j;
        mode = k;
        tree = languages_defs[i].ofLong(j, k, l);
    }

    public Nombre(int i, int j, int k, String s)
    {
        language = i;
        country = j;
        mode = k;
        tree = null;
        int l = 0;
        long l1 = 0L;
        for(int i1 = 0; i1 < s.length(); i1++)
        {
//            int j1 = s.charAt(i1);
            char j1 = s.charAt(i1);
            if(Character.isWhitespace(j1))
            {
                continue;
            }
            if(Character.isDigit(j1))
            {
                l1 = (l1 * 10L + (long)j1) - 48L;
                l++;
                continue;
            }
            l1 = -1L;
            break;
        }

        if(l == 0)
        {
            l1 = -1L;
        }
        for(int k1 = 0; k1 < 3 && l1 < 0L; k1++)
        {
            tree = languages_defs[k1].ofLetters(s);
            if(tree != null)
            {
                l1 = tree.toLong();
            }
        }

        if(l1 >= 0L)
        {
            tree = languages_defs[i].ofLong(j, k, l1);
        } else
        {
            tree = null;
        }
    }

    static 
    {
        languages_defs = new LanguageDef[3];
        for(int i = 0; i < 3; i++)
        {
            languages_defs[i] = null;
        }

    }
}
