/**
 * Cocowork
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionApplication extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionApplication() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionApplication(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionApplication(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionApplication(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
