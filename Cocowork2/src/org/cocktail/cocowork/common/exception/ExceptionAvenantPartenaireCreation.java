package org.cocktail.cocowork.common.exception;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class ExceptionAvenantPartenaireCreation extends Exception 
{
	
	public ExceptionAvenantPartenaireCreation(final String message) {
		super(message);
	}

	public ExceptionAvenantPartenaireCreation(final String message, final Throwable cause) {
		super(message, cause);
	}


}
