package org.cocktail.cocowork.common.exception;

/**
 * 
 * 
 * @author Michael Haller, Consortium Cocktail, 2009
 * 
 */
public class ExceptionUtilisateur extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionUtilisateur() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionUtilisateur(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionUtilisateur(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionUtilisateur(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
