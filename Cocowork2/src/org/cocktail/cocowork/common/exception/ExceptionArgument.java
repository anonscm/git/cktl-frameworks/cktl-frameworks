package org.cocktail.cocowork.common.exception;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class ExceptionArgument extends Exception 
{
	public ExceptionArgument(final String message) {
		super(message);
	}

	public ExceptionArgument(final String message, final Throwable cause) {
		super(message, cause);
	}
}
