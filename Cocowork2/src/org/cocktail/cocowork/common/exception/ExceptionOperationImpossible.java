/**
 * Coconut
 * bgauthie
 * 2007
 *
 */
package org.cocktail.cocowork.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionOperationImpossible extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionOperationImpossible() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionOperationImpossible(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionOperationImpossible(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionOperationImpossible(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
