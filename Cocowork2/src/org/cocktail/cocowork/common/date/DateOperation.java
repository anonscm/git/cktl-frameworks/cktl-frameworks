package org.cocktail.cocowork.common.date;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * <p>Classe utilitaire autour des dates...
 * 
 * <p>ATTENTION: Les NSTimestamp n'ont aucun NSTimezone associe, ils travaillent toujours en UTC (cad GMT).
 * Instancier un NSTimestamp avec en parametre un time zone ne fait que "decaler" la valeur initiale (consideree comme
 * etant en GMT) selon ce time zone. Meme si aucun time zone n'est specifie a la creation d'un NSTimestamp, c'est 
 * NSTimezone.defaultTimeZone() qui est utilise.<br>
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2006.
 */
public class DateOperation 
{
	/**
	 * <p>Cree un {@link NSTimestamp} a partir d'une expression du type "dd/mm/yyyy" ou "ddmmyyyy".
	 * <p>Le {@link NSTimeZone#defaultTimeZone()} est utilise.
	 * @param ddmmyyyy Exemple "11/03/1979" ou "11031979".
	 * @return Un {@link NSTimestamp} correspondant a cette date.
	 */
	static public NSTimestamp createTimestamp(String ddmmyyyy) {
		if (ddmmyyyy==null || "".equals(ddmmyyyy)) return null;
		NSTimestamp ts = null;
		try {
			ts = (NSTimestamp) new NSTimestampFormatter("dd/MM/yyyy").parseObject(ddmmyyyy);
		} catch (ParseException pe1) {
			try {
				ts = (NSTimestamp) new NSTimestampFormatter("ddMMyyyy").parseObject(ddmmyyyy);
			} catch (ParseException pe2) {
				
			}
		}
		return ts;
	}
	

	/**
	 * Retourne la representation litterale lisible d'une date.
	 * @param ts La date.
	 * @param includeTime true, pour inclure l'heure sous la forme "HH/mm/ss" dans la representation.
	 * @return La representation litterale lisible de la date specifie. Ex: "09/11/2006 - 11:34:53".
	 */
	static public String print(final Date ts, final boolean includeTime) {
		if (ts == null) return "NULL";
		
		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(ts);

		StringBuffer buf = new StringBuffer();
		buf.append(new SimpleDateFormat("dd/MM/yyyy").format(myCalendar.getTime()));
		if (includeTime) {
			buf.append(" - ");
			buf.append(new SimpleDateFormat("HH:mm:ss").format(myCalendar.getTime()));
		}
		return buf.toString();
	}
	/**
	 * Retourne la representation litterale lisible d'une date.
	 * @param ts La date.
	 * @param includeTime true, pour inclure l'heure sous la forme "HH/mm/ss" dans la representation.
	 * @return La representation litterale lisible de la date specifie. Ex: "09/11/2006 - 11:34:53".
	 */
	static public String print(final Date ts, final boolean includeTime, final boolean includeDayOfWeek) {
		if (ts == null) return "NULL";
		
		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(ts);

		StringBuffer buf = new StringBuffer();
		if (includeDayOfWeek)
			buf.append(new SimpleDateFormat("E dd/MM/yyyy").format(myCalendar.getTime()));
		else
			buf.append(new SimpleDateFormat("dd/MM/yyyy").format(myCalendar.getTime()));
		if (includeTime) {
			buf.append(" - ");
			buf.append(new SimpleDateFormat("HH:mm:ss").format(myCalendar.getTime()));
		}
		return buf.toString();
	}


	/**
	 * <p>Ajouter un laps de temps a une date.
	 * 
	 * @param date Date a laquelle ajouter le laps de temps
	 * @param valeur Valeur du laps de temps (ex: 1, -3)
	 * @param unite Unite du laps de temps (ex: Calendar.MONTH, Calendar.DATE)
	 * 
	 * @return La date passee en parametre + le laps de temps specifie. (NB:
	 *         31/01/2006 + 1 mois = 28/02/2006)
	 */
	static public Date decaler(final Date date, final int valeur, final int unite) {
		if (date == null) return null;
		
		Calendar startDate = GregorianCalendar.getInstance();
		startDate.setTime(date);
		startDate.add(unite, valeur);
		Date ts = new Date(startDate.getTimeInMillis());
		
		return ts;
	}

	/**
	 * <p>Calcul l'ecart de temps qu'il existe entre deux dates, dans l'unite precisee.
	 * <p>Exemples :
	 * <ul>
	 * <li><code>ecartEntreDates(05/09/2006, 12/05/2007, Calendar.MONTH)</code> renvoie <code>-8</code></li>
	 * <li><code>ecartEntreDates(05/09/2006, 01/05/2007, Calendar.MONTH)</code> renvoie <code>-7</code></li>
	 * <li><code>ecartEntreDates(10/09/2006, 12/09/2006, Calendar.MONTH)</code> renvoie <code>-8</code></li>
	 * <li><code>ecartEntreDates(03/10/2006, 12/09/2006, Calendar.DATE)</code> renvoie <code>21</code></li>
	 * </ul>
	 * 
	 * @param date Date pour laquelle on veut calculer l'ecart.
	 * @param dateDeReference Date par rapport a laquelle est calcule l'ecart de temps.
	 * @param unite Unite souhaitee : Soit Calendar.YEAR, soit Calendar.MONTH soit Calendar.DATE.
	 * 
	 * @return L'ecart trouve entre les deux dates, positif ou negatif.
	 */
	static public int ecart(
			final NSTimestamp date, 
			final NSTimestamp dateDeReference, 
			final int unite) 
	throws IllegalArgumentException {
		if (date==null || dateDeReference==null)
			throw new IllegalArgumentException("Arguments null interdits.");
		if (unite!=Calendar.YEAR && unite!=Calendar.MONTH && unite!=Calendar.DATE)
			throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
			
		// date inferieure
		Calendar d = GregorianCalendar.getInstance();
		d.setTime(dateDeReference.after(date) ? date : dateDeReference);

		// date superieure
		Calendar ref = GregorianCalendar.getInstance();
		ref.setTime(dateDeReference.after(date) ? dateDeReference : date);
		
		// calcul difference
		DateDiff diff = new DateDiff(d, ref);
		diff.calculateDifference();
		int ecart = diff.getFieldOnly(unite);
		
		// signe de la difference
		int ecartSigne = dateDeReference.after(date) ? ecart : ecart*-1;

		System.out.println("ecart()>>  DateDiff.diff = "+diff.toString());
		System.out.println("ecart()>>  "+print(date,true)
				+" - "+print(dateDeReference,true)+" = "+ecartSigne);
		
		return ecartSigne;
	}
	
	/**
	 * Determine le premier jour du mois specifie.
	 * 
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * 
	 * @return Un NSTimestamp egal au premier jour du mois, 0:00:00
	 */
	static public NSTimestamp premierJourDuMois(final String mois) {
		NSTimestampFormatter formatterPeriode = new NSTimestampFormatter("%m/%Y");
		NSTimestamp periode = (NSTimestamp) formatterPeriode.parseObjectInUTC(mois, new ParsePosition(0));
		
		if (periode != null) {
			Calendar dateDeb = GregorianCalendar.getInstance();
			dateDeb.setTime(periode);
			NSTimestamp debut = new NSTimestamp(dateDeb.getTime());
			
			System.out.println("getPremierJourDuMois()>>  resultat = "+print(debut,true));
			return debut;
		}
		return null;
	}
	
	/**
	 * Determine le dernier jour du mois specifie.
	 *
	 * @param mois Mois sous la forme "mm/yyyy". Ex: 03/2006 ou 3/2006 pour mars 2006
	 * 
	 * @return Un NSTimestamp egal au dernier jour du mois, 23:59:59
	 */
	static public NSTimestamp dernierJourDuMois(final String mois) {
		NSTimestampFormatter formatterPeriode = new NSTimestampFormatter("%m/%Y");
		NSTimestamp periode = (NSTimestamp) formatterPeriode.parseObjectInUTC(mois, new ParsePosition(0));
		
		if (periode != null) {
			Calendar dateFin = GregorianCalendar.getInstance();
			dateFin.setTime(periode);
			dateFin.add(Calendar.MONTH, 1);
			dateFin.add(Calendar.SECOND, -1);
			NSTimestamp fin = new NSTimestamp(dateFin.getTime());
			return fin;
		}
		return null;
	}
	
	/**
	 * Fournit une nouvelle date a partir de celle specifiee, mais dont l'heure est mise a 0h0m0s0ms.
	 * NB: Le time zone de la date retournee est le meme que celui de la date passee en parametre.
	 * @param date Date de reference.
	 * @return La nouvelle date.
	 */
	static public NSTimestamp premiereHeure(final Date date) {
		if (date==null) return null;

		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(date); 
		myCalendar.set(GregorianCalendar.HOUR_OF_DAY, 0);
		myCalendar.set(GregorianCalendar.MINUTE, 0);
		myCalendar.set(GregorianCalendar.SECOND, 0);
		myCalendar.set(GregorianCalendar.MILLISECOND, 0);

		return new NSTimestamp(myCalendar.getTime());
	}
	

	/**
	 * Fournit une nouvelle date a partir de celle specifiee, mais dont l'heure est mise a 23h59m59s999ms.
	 * NB: Le time zone de la date retournee est le meme que celui de la date passee en parametre.
	 * @param date Date de reference.
	 * @return La nouvelle date.
	 */
	static public NSTimestamp derniereHeure(final Date date) {
		if (date==null) return null;
		
		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(date); // le tz du ts est pris en compte
		myCalendar.set(GregorianCalendar.HOUR_OF_DAY, 23);
		myCalendar.set(GregorianCalendar.MINUTE, 59);
		myCalendar.set(GregorianCalendar.SECOND, 59);
		myCalendar.set(GregorianCalendar.MILLISECOND, 999);
		
		return new NSTimestamp(myCalendar.getTime());  
	}
	

	/** 
	 * This class determines the difference between 2 dates not including the dates themselves, i.e. inclusive 
	 * passed as java.utilCalendar.
	 * <p>Owner : Niraj Agarwal
	 * 
	 * <p>Modif : Bertrand Gauthier. Duree comprise entre 2 dates, en incluant ces deux dates.
	 * <p>Exemple: 
	 * <ul> <li>[01/01/2005 --> 31/12/2005] 	<--> 	1 an,		0 mois,		0 jour</li>
	 * 		<li>[01/02/2005 --> 31/03/2005] 	<--> 	0 an,		2 mois,		0 jour</li>
	 * 		<li>[01/01/2005 --> 01/04/2005]		<-->	0 an,		3 mois,		1 jour</li>
	 * 		<li>[11/03/2006 --> 11/03/2006] 	<--> 	0 an,		0 mois,		1 jour</li>
	 * 		<li>[11/03/2005 --> 11/03/2006] 	<--> 	1 an,		0 mois,		1 jour</li>	</ul>
	 */
	static public class DateDiff  
	{
		// The year difference between passed dates
		private int yearDiff = 0;
		// The month difference between passed dates, excluding years
		private int monthDiff = 0;
		// The day difference between passed dates, excluding years and months
		private int dayDiff = 0;
		// Total day difference between passed dates, including years and months 
		private int dayOnly = 0;
		
		// Flag pour affichage
		private boolean nice = false;
		
		private Calendar startDate = null;
		private Calendar endDate = null;
		
		// 1 jour en millisecondes = 24 x 60 x 60 x 1000
		private static final long DAY = 86400000;
		
		/**
		 * Constructeur
		 * @param pStartDate Date de debut sous forme "dd/MM/yyyy"
		 * @param pEndDate Date de fin sous forme "dd/MM/yyyy"
		 * @throws NumberFormatException Si le parseInt echoue
		 */
		public DateDiff(String pStartDate, String pEndDate) throws NumberFormatException {
			startDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			endDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			
			startDate.clear();
			endDate.clear();
			
			startDate.set(
				Integer.parseInt(pStartDate.substring(6)), Integer.parseInt(pStartDate.substring(3,5)), Integer.parseInt(pStartDate.substring(0,2)));
			endDate.set(
				Integer.parseInt(pEndDate.substring(6)), Integer.parseInt(pEndDate.substring(3,5)), Integer.parseInt(pEndDate.substring(0,2)));
			
			//to include the date themselves
			startDate.add(Calendar.DATE, -1);
		}
		
		public DateDiff(Calendar pStartDate, Calendar pEndDate)
		{
			startDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			endDate = GregorianCalendar.getInstance(NSTimeZone.defaultTimeZone());
			
			startDate.clear();
			endDate.clear();
			
			startDate.set(pStartDate.get(Calendar.YEAR), pStartDate.get(Calendar.MONTH), pStartDate.get(Calendar.DATE));
			endDate.set(pEndDate.get(Calendar.YEAR), pEndDate.get(Calendar.MONTH), pEndDate.get(Calendar.DATE));

			//to include the date themselves
			startDate.add(Calendar.DAY_OF_YEAR, -1);
		}
		
		/**
		 * Calule la difference entre les deux dates (incluses dans la difference)
		 */
		public void calculateDifference()
		{
			if( startDate == null || endDate == null || startDate.after(endDate) )
				return;
			
			dayOnly = (int) ((endDate.getTimeInMillis() - startDate.getTimeInMillis()) / DAY);
			
			yearDiff = endDate.get(Calendar.YEAR) - startDate.get(Calendar.YEAR);
			
			boolean bYearAdjusted = false;
			startDate.add(Calendar.YEAR, yearDiff);
			if( startDate.after(endDate) )
			{
				bYearAdjusted = true;
				startDate.add(Calendar.YEAR, -1 );
				yearDiff--;
			}
			
			monthDiff = endDate.get(Calendar.MONTH) - startDate.get(Calendar.MONTH);
			if( bYearAdjusted && monthDiff <= 0 )
				monthDiff = 12 + monthDiff;
			
			startDate.add(Calendar.MONTH, monthDiff);
			if( startDate.after(endDate) )
			{
				startDate.add(Calendar.MONTH, -1 );
				monthDiff--;
			}
			
			dayDiff = endDate.get(Calendar.DAY_OF_YEAR) - startDate.get(Calendar.DAY_OF_YEAR);
			if( dayDiff < 0 )
				dayDiff = 365 + dayDiff;
			
			startDate.add(Calendar.DAY_OF_YEAR, dayDiff);
		}
		
		/**
		 * Accesseur des resultats du calcul de la difference
		 * @param field Calendar.DATE, Calendar.MONTH ou Calendar.YEAR
		 */
		public int getField(final int field) throws IllegalArgumentException {
			if (field!=Calendar.YEAR && field!=Calendar.MONTH && field!=Calendar.DATE)
				throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
		
			switch (field) {
				case Calendar.DATE:
					return dayDiff;
				case Calendar.MONTH:
					return monthDiff;
				case Calendar.YEAR:
					return yearDiff;
				default:
					return 0;
			}
		}
		
		/**
		 * Accesseur des resultats du calcul de la difference
		 * @param field Calendar.DATE, Calendar.MONTH ou Calendar.YEAR
		 */
		public int getFieldOnly(final int field) throws IllegalArgumentException {
			if (field!=Calendar.YEAR && field!=Calendar.MONTH && field!=Calendar.DATE)
				throw new IllegalArgumentException("Arguments autoris\u00E9s : Calendar.YEAR, Calendar.MONTH ou Calendar.DATE");
		
			switch (field) {
				case Calendar.DATE:
					return getDayOnly();
				case Calendar.MONTH:
					return getMonthOnly();
				case Calendar.YEAR:
					return getYearOnly();
				default:
					return 0;
			}
		}
		
		/**
		 * Retourne le nombre d'annee. Cad 1 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getYear() {
			return yearDiff;
		}
		/**
		 * Retourne le nombre de mois. Cad 7 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getMonth() {
			return monthDiff;
		}
		/**
		 * Retourne le nombre de jours. Cad 12 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getDay() {
			return dayDiff;
		}

		/**
		 * Retourne la difference en nombre de jours.
		 */
		public int getDayOnly() {
			return dayOnly;
		}
		/**
		 * Retourne la difference en nombre de mois entiers.
		 * Cad 19 par exemple dans le cas ou la difference calculee est de "1an 7mois 12j".
		 */
		public int getMonthOnly() {
			return yearDiff*12 + monthDiff;
		}
		/**
		 * Retourne la difference en nombre d'annees entieres.
		 * C'est la meme chose que getYear().
		 */
		public int getYearOnly() {
			return getYear();
		}
		
		
		
		//--
		/**
		 * La duree sous forme affichable complete.
		 * <p>Exemples :	
		 * <ul>
		 * 	<li>12ans 4mois 12j</li>
		 * 	<li>6mois</li>
		 * 	<li>1an 15j</li>
		 * 	<li>1j</li>
		 * </ul>
		 */
		public String toString() {
			StringBuffer buf = new StringBuffer();
			if (getYear() > 0) {
				buf.append(new Integer(getYear()).toString());
				if (getYear()==1)
					buf.append(nice ? " an" : "an");
				else
					buf.append(nice ? " ans" : "ans");
			}
			if (getMonth() > 0) {
				buf.append(getYear()>0 ? " " : "");
				buf.append(new Integer(getMonth()).toString());
				buf.append(nice ? " mois" : "mois");
			}
			if (getDay() > 0) {
				buf.append(getYear()>0 || getMonth()>0 ? " " : "");
				buf.append(new Integer(getDay()).toString());
				if (getDay() == 1)
					buf.append(nice ? " jour" : "j");
				else
					buf.append(nice ? " jours" : "j");
			}
			return buf.length()>0 ? buf.toString() : (nice ? "0 jour" : "0j");
		}
		

	}
	
	/**
	 * Point d'entree pour tests.
	 * @param arguments
	 */
	static public void main(String[] arguments) {
		try {
			System.out.println("NSTimeZone.defaultTimeZone() : "+NSTimeZone.defaultTimeZone());
			NSTimestamp myNSTimestamp = new NSTimestamp();
			System.out.println("new NSTimestamp() : "+myNSTimestamp);
			GregorianCalendar myCalendar = new GregorianCalendar();
			System.out.println("myCalendar.getTimeZone() : "+myCalendar.getTimeZone().getID());
			myCalendar.setTime(myNSTimestamp);


			System.out.println("date : "+print(myNSTimestamp, true));
			
			NSTimestamp d = new NSTimestamp(myCalendar.getTime());
			System.out.println("d : "+d);
			
			myCalendar = new GregorianCalendar();
			myCalendar.setTime(d);


			NSTimestamp date1 = new NSTimestamp(DateFormat.getDateInstance(DateFormat.SHORT).parse("1/12/2006"));
			NSTimestamp date2 = new NSTimestamp(DateFormat.getDateInstance(DateFormat.SHORT).parse("1/7/2007"));
			System.out.println("date1 : "+date1);
			System.out.println("date2 : "+date2);
			derniereHeure(date1);
			derniereHeure(date2);
			System.out.println("date1.after(date2) : "+derniereHeure(date1).before(premiereHeure(date2)));
			System.out.println("date1.before(date2) : "+date1.before(date2));
			System.out.println("date2.after(date1) : "+date2.after(date1));
			System.out.println("date2.before(date1) : "+date2.before(date1));

			ecart(date2, date1, Calendar.MONTH);


		} catch (ParseException e) {
			e.printStackTrace();
		}

	}
	

	
	public static NSTimestamp calculDateFinWithDateDebutAndDureeDescription(NSTimestamp dateDebut,Integer nbAnnee, Integer nbMois, Integer nbSemaines, Integer nbJours){
		
		NSTimestamp finCalcule=null;
		//NSTimestamp debutCalcule=null;
		if(nbAnnee!=null || nbMois!=null || nbSemaines!=null || nbJours!=null)
		{
			GregorianCalendar calendar = new GregorianCalendar();
			if(dateDebut==null) 
				calendar.setTime(new NSTimestamp());
			
			else
				calendar.setTime(dateDebut);
	
			if(nbAnnee!=null)
				calendar.add(GregorianCalendar.YEAR, nbAnnee.intValue());
			if(nbMois!=null)
				calendar.add(GregorianCalendar.MONTH, nbMois.intValue());
			if(nbSemaines!=null)
				calendar.add(GregorianCalendar.DATE, nbSemaines.intValue()*7);
			if(nbJours!=null)
				calendar.add(GregorianCalendar.DATE, nbJours.intValue());
					
			calendar.add(GregorianCalendar.DATE, -1);	
			finCalcule = new NSTimestamp(calendar.getTime());
		}
		return finCalcule;
	}

	/**
	 * @param year annee dont on veut recuperer le dernier timestamp
	 * @return le dernier timestamp (hormis les ms), soit 31/12/yyyy 23h59 59sec
	 */
	public static NSTimestamp lastTimestampOfYear(int year) {
	    Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, Calendar.DECEMBER);
        int lastDay = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        int lastHour = cal.getActualMaximum(Calendar.HOUR_OF_DAY);
        cal.set(Calendar.DAY_OF_MONTH, lastDay);
        cal.set(Calendar.HOUR_OF_DAY, lastHour);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        return new NSTimestamp(cal.getTime());
	}
	
}
