/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cocktail.cocowork.common.finder;

import org.cocktail.cocowork.common.exception.ExceptionFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

//import fr.univlr.cri.eoutilities.CRIFetchUtilities;

/**
 *
 * @author Michael Haller
 */


public abstract class Finder {

	protected static final int DEFAULT_MINIMUM_LENGTH_FOR_STRING_CRITERIA = 3;
	
	protected static final String INFO_RECHERCHE_POSSIBLE = 
		"La recherche peut \u00EAtre lanc\u00E9e.";
	protected static final String INFO_EDITING_CONTEXT_REQUIS = "Editing context requis pour lancer la recherche";
	
	
	protected EOEditingContext ec;
	protected String entityName;
	
//	protected ArrayList warningMessages = new ArrayList();
//	protected HashMap warningMessages = new HashMap();
//	protected String[] criteriaNamesForWarningMessages = new String[] { };
	
	protected NSMutableArray freeQualifiers;
	protected NSMutableArray optionalQualifiers;
	protected NSMutableArray mandatoryQualifiers;
//	protected EOQualifier globalQualifier;
	
	protected NSArray objects;
	
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 * @param entityName Nom de l'entite du model correspondant aux objets metier recherches.
	 */
	protected Finder(EOEditingContext ec, final String entityName) {
		
		this.ec = ec;
		this.entityName = entityName;

		this.freeQualifiers = new NSMutableArray();
		this.optionalQualifiers = new NSMutableArray();
		this.mandatoryQualifiers = new NSMutableArray();
		
		this.objects = new NSMutableArray();
	}
	
	/**
	 * Change l'editing context de travail.
	 * @param ec Nouvel editing contexte a utiliser.
	 */
	public void setEditingContext(final EOEditingContext ec) {
		this.ec = ec;
	}
	
	
	/**
	 * Supprime tous les criteres de recherches courants.
	 */
	public abstract void clearAllCriteria();
	
	/**
	 * Indique si les criteres necessaires ont ete fournis pour chercher.
	 * @return <code>true</code> si la recherche est possible, <code>false</code> sinon.
	 */
	public abstract boolean canFind();
	
	/**
	 * 
	 * @return
	 */
	public abstract String getCurrentWarningMessage();
	
	
//	/**
//	 * 
//	 * @param criteriaNamesForWarningMessages
//	 */
//	public void setOrderedCriteriaNamesForWarningMessages(final String[] criteriaNamesForWarningMessages) {
//		if (criteriaNamesForWarningMessages != null)
//			this.criteriaNamesForWarningMessages = criteriaNamesForWarningMessages;
//	}
	
//	/**
//	 * Ajout d'un message d'avertissement dans la pile des messages.
//	 * @param warningMessage Message a ajouter.
//	 */
//	protected void addUserWarningMessage(final String warningMessage) {
//		if (warningMessage!=null && !this.warningMessages.contains(warningMessage));
//			this.warningMessages.add(warningMessage);
//	}
//	/**
//	 * Suppression d'un message d'avertissement de la pile des messages.
//	 * @param warningMessage Message a supprimer.
//	 */
//	protected void removeUserWarningMessage(final String warningMessage) {
//		if (warningMessage != null)
//			this.warningMessages.remove(warningMessage);
//	}
//	/**
//	 * Ajout d'un message d'avertissement dans la pile des messages.
//	 * @param warningMessage Message a ajouter.
//	 */
//	protected void addUserWarningMessage(final String criterionName, final String warningMessage) {
//		System.out.println("\nFinder.addUserWarningMessage():  ");
//		System.out.println("criterionName :  "+criterionName);
//		System.out.println("warningMessage:  "+warningMessage);
//		
//		if (criterionName!=null && warningMessage!=null)
//			this.warningMessages.put(criterionName, warningMessage);
//	}
//	/**
//	 * Suppression d'un message d'avertissement de la pile des messages.
//	 * @param warningMessage Message a supprimer.
//	 */
//	protected void removeUserWarningMessage(final String criterionName) {
//		System.out.println("\nFinder.removeUserWarningMessage():  ");
//		System.out.println("criterionName :  "+criterionName);
//		
//		if (criterionName != null)
//			this.warningMessages.remove(criterionName);
//	}
	
//	/**
//	 * Fournit un message d'avertissement sur l'etat des criteres requis pour la recherche.
//	 * @return Un message correspondant au probleme concernant les criteres de recherche.
//	 */
//	public String getUserWarningMessage() {
//		if (warningMessages.size() > 0)
//			return (String) warningMessages.get(warningMessages.size() - 1);
////		if (!canFind())
////			return this.warningMessage;
//		else
//			return "Vous pouvez lancer la recherche.";
//	}
//	/**
//	 * Fournit un message d'avertissement sur l'etat des criteres requis pour la recherche.
//	 * @return Un message correspondant au probleme concernant les criteres de recherche.
//	 */
//	public String getUserWarningMessages() {
//		StringBuffer buf = new StringBuffer();
//		buf.append("\nAvertissements :");
//		for (int i=0; i<warningMessages.size(); i++) {
//			buf.append("\n["+i+"]\t");
//			buf.append((String) warningMessages.get(i));
//		}
//		buf.append("\n");
//		return buf.toString();
//	}
//	/**
//	 * Fournit un message d'avertissement sur l'etat des criteres requis pour la recherche.
//	 * @return Un message correspondant au probleme concernant les criteres de recherche.
//	 */
//	public String getCurrentWarningMessage() { 
//		String message=null;
//		// des messages d'avertissement doivent etre presents
//		if (this.warningMessages.size() > 0) {
//			// premier message d'avertissement de la liste
//			message = (String) this.warningMessages.get(this.warningMessages.keySet().iterator().next());
//			
//			// si la liste des criteres intervenant dans les messages n'est pas vide
//			if (this.criteriaNamesForWarningMessages.length > 0) {
//				int i=0;
//				do {
//					message = (String) this.warningMessages.get(this.criteriaNamesForWarningMessages[i++]);
//				} 
//				while (i<this.criteriaNamesForWarningMessages.length && message==null);
//			}
//		}
//		if (message != null)
//			return message;
//		else
//			return "Vous pouvez lancer la recherche.";
//	}
//	/**
//	 * Fournit un message d'avertissement sur l'etat des criteres requis pour la recherche.
//	 * @return Un message correspondant au probleme concernant les criteres de recherche.
//	 */
//	public String getCurrentWarningMessages() {
//		Iterator iter = this.warningMessages.keySet().iterator();
//		
//		StringBuffer buf = new StringBuffer();
//		buf.append("\nAvertissements :");
//		while (iter.hasNext()) {
//			Object key = iter.next();
//			buf.append("\n["+key+"]\t");
//			buf.append((String) warningMessages.get(key));
//		}
//		buf.append("\n");
//		return buf.toString();
//	}

	
	/**
	 * Ajout d'un qualifier libre.
	 * @param qualifier Qualifer supplementaire, un AND est fait avec ceux existants.
	 */
	public void addFreeQualifier(final EOQualifier qualifier) {
		if (qualifier != null) 
			this.freeQualifiers.addObject(qualifier);
	}
	/**
	 * Ajout d'un qualifier "inevitable".
	 * @param qualifier Le qualifer a ajouter. Un AND sera fait avec les qualifiers existants.
	 */
	protected void addMandatoryQualifier(EOQualifier qualifier) {
		if (qualifier != null) 
			this.mandatoryQualifiers.addObject(qualifier);
	}
	/**
	 * Ajout d'un qualifier optionnel au finder.
	 * @param qualifier Le qualifer a ajouter. Un AND sera fait avec les qualifiers existants.
	 */
	protected void addOptionalQualifier(EOQualifier qualifier) {
		if (qualifier != null) 
			this.optionalQualifiers.addObject(qualifier);
	}
	
//	/**
//	 * Verifie qu'un critere litteral est acceptable : non <code>null</code>, non vide et de longueur suffisante.
//	 * @param criterion Critere litteral qu'on veut verifier.
//	 * @param warningMessageIfEmpty Message d'avertissement a positionner si le critere est <code>null</code> ou vide.
//	 * @param warningMessageIfTooShort Message d'avertissement a positionner si la longueur du critere (non <code>null</code> et non vide) est insuffisante.
//	 * @return Le critere specifie, non modifie.
//	 */
//	protected String checkedCriterion(
//			final String criterion, 
//			final String warningMessageIfEmpty,
//			final String warningMessageIfTooShort) {
//		
//		System.out.println("\nFinder.checkedCriterion(): ");
//		System.out.println("\tcriterion: "+criterion);
//		System.out.println("\tmsg 1: "+warningMessageIfEmpty);
//		System.out.println("\tmsg 2: "+warningMessageIfTooShort);
//		
//		// critere null 
//		if (criterion == null) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(warningMessageIfEmpty);
//			System.out.println(getUserWarningMessages());
//			return null;
//		}
//		// critere vide
//		if (criterion.length() == 0) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(warningMessageIfEmpty);
//			System.out.println(getUserWarningMessages());
//			return null;
//		}
//		
//		// si on arrive jusqu'ici, le critere n'est ni null ni vide : on retire le message d'avertissement
//		removeUserWarningMessage(warningMessageIfEmpty);
//		
//		// critere trop court
//		if (criterion.length() < DEFAULT_MINIMUM_LENGTH_FOR_STRING_CRITERIA) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(warningMessageIfTooShort);
//			System.out.println(getUserWarningMessages());
//			return criterion;
//		}
//		
//		// si on arrive jusqu'ici, le critere n'est pas trop court : on retire le message d'avertissement
//		removeUserWarningMessage(warningMessageIfTooShort);
//
//		System.out.println(getUserWarningMessages());
//		return criterion;
//	}
//	/**
//	 * Verifie qu'un critere litteral est acceptable : non <code>null</code>, non vide et de longueur suffisante.
//	 * @param criterion Critere litteral qu'on veut verifier.
//	 * @param warningMessageIfEmpty Message d'avertissement a positionner si le critere est <code>null</code> ou vide.
//	 * @param warningMessageIfTooShort Message d'avertissement a positionner si la longueur du critere (non <code>null</code> et non vide) est insuffisante.
//	 * @return Le critere specifie, non modifie.
//	 */
//	protected String checkedCriterion(
//			final String criterionName, 
//			final String criterionValue, 
//			final String warningMessageIfEmpty,
//			final String warningMessageIfTooShort) {
//		
//		System.out.println("\nFinder.checkedCriterion(): ");
//		System.out.println("\tcriterion name : "+criterionName);
//		System.out.println("\tcriterion value: "+criterionValue);
//		
//		// critere null 
//		if (criterionValue == null) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(criterionName, warningMessageIfEmpty);
//			System.out.println(getCurrentWarningMessages());
//			return null;
//		}
//		// critere vide
//		if (criterionValue.length() == 0) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(criterionName, warningMessageIfEmpty);
//			System.out.println(getCurrentWarningMessages());
//			return null;
//		}
//		
//		// si on arrive jusqu'ici, le critere n'est ni null ni vide : on retire le message d'avertissement
//		removeUserWarningMessage(criterionName);
//		
//		// critere trop court
//		if (criterionValue.length() < DEFAULT_MINIMUM_LENGTH_FOR_STRING_CRITERIA) {
//			// ajout du message d'avertissement a la pile
//			addUserWarningMessage(criterionName, warningMessageIfTooShort);
//			System.out.println(getCurrentWarningMessages());
//			return criterionValue;
//		}
//		
//		// si on arrive jusqu'ici, le critere n'est pas trop court : on retire le message d'avertissement
//		removeUserWarningMessage(criterionName);
//
//		System.out.println(getCurrentWarningMessages());
//		return criterionValue;
//	}
	
	/**
	 * Cree un qualifier a partir d'un format et d'un unique argument, si cet argument est non null.
	 * @param qualifierFormat Format du qualifier.
	 * @param qualifierArgument Argument du nouveau qualifier. Si cet argument est null, le qualifier est mis a null.
	 */
	protected EOQualifier createQualifier(
			final String qualifierFormat, 
			final Object qualifierArgument) {
		
		if (qualifierArgument != null) 
			return EOQualifier.qualifierWithQualifierFormat(qualifierFormat, new NSArray(qualifierArgument));
		else
			return null;
	}
	/**
	 * Cree un qualifier a partir d'un format et d'une liste de plusieurs arguments (si cette liste est non null et non vide).
	 * @param qualifierFormat Format du qualifier.
	 * @param qualifierArgument Argument du nouveau qualifier. Si cet argument est null, le qualifier est mis a null.
	 */
	protected EOQualifier createQualifier(
			final String qualifierFormat, 
			final NSArray qualifierArguments) {
		
		if (qualifierArguments != null && qualifierArguments.count()>0) 
			return EOQualifier.qualifierWithQualifierFormat(qualifierFormat, qualifierArguments);
		else
			return null;
	}

	/**
	 * Supprimer tous les qualifiers (sauf ceux obligatoires).
	 */
	public void removeQualifiers() {
		removeOptionalQualifiers();
		removeFreeQualifiers();
	}
	/**
	 * Supprimer les qualifiers libres.
	 */
	protected void removeFreeQualifiers() {
		this.optionalQualifiers.removeAllObjects();
	}
	/**
	 * Supprimer les qualifiers (sauf ceux obligatoires).
	 */
	protected void removeOptionalQualifiers() {
		this.optionalQualifiers.removeAllObjects();
	}
	/**
	 * Fournit le qualifier global courant de recherche (y compris ceux obligatoires).
	 * @return Le qualifier global, ou <code>null</code> si aucun critere present.
	 */
	public EOQualifier getGlobalQualifier() {
		NSMutableArray array = new NSMutableArray();
		if(this.mandatoryQualifiers!=null)
			array.addObjectsFromArray(this.mandatoryQualifiers);
		if(this.optionalQualifiers!=null)
			array.addObjectsFromArray(this.optionalQualifiers);
		if(this.freeQualifiers!=null)
			array.addObjectsFromArray(this.freeQualifiers);
		
		// System.out.println("count qualifier : "+array.count());
		if (array.count() > 0)
			return new EOAndQualifier(array); 
		else
			return null;
	}
	/**
	 * Fournit le nombre courant de qualifiers (sauf ceux obligatoires).
	 * @return Un entier, eventuellement nul.
	 */
	protected int getQualifiersCount() {
		return this.optionalQualifiers.count() + this.freeQualifiers.count();
	}
	
	
	/**
	 * Lance la recherche en base, avec les criteres courants..
	 * @return Liste des objets trouves.
	 * @throws ExceptionFinder Si criteres insuffisants ou si aucun objet trouve.
	 */
	public NSArray find() throws ExceptionFinder {
		
		if (!this.canFind()) {
			throw new ExceptionFinder(this.getCurrentWarningMessage());
		}
		objects = fetch();
		
		return objects;
	}
	/**
	 * Lance la recherche sans criteres, sauf ceux obligatoires.
	 * @return Liste des objets trouves.
	 * @throws ExceptionFinder 
	 */
	public NSArray findAll() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		// fetch
		objects = fetch();
		return objects;
	}
	
//	/**
//	 * Lance la recherche en base, s'attend a ne trouver qu'un seul objet.
//	 * @return L'objets trouve.
//	 * @throws ArgumentException 
//	 * @throws FetchException Si aucun ou plusieurs objets trouves.
//	 */
//	protected EOEnterpriseObject findUnique() throws FinderException {
//		
//		if (!this.canFind()) {
//			throw new FinderException(this.getCurrentWarningMessage());
//		}
//		objects = this.fetch();
//
////		if (objects==null || objects.count()==0)
////			throw new FinderException("Aucun objet trouv\u00E9 avec ces crit\u00E8res");
//		if (objects.count() != 1)
//			throw new FinderException("Plus d'un objet trouv\u00E9 avec ces crit\u00E8res");
//
//		return (EOEnterpriseObject) objects.objectAtIndex(0);
//	}
	
	/**
	 * Effectue le fetch en base.
	 * @throws ExceptionFinder Une erreur, ou rien trouve.
	 */
	private NSArray fetch() throws ExceptionFinder {

		// verif ec present
		if (ec == null)
			throw new NullPointerException(INFO_EDITING_CONTEXT_REQUIS);
		
		EOQualifier globalQualifier = getGlobalQualifier();
//		Debug.printQualifier("\nFinder.fetch():  qualifier global pour "+this.entityName, globalQualifier);
		
		// fetch
		try {
			objects = find(
					ec, 
					entityName, 
					globalQualifier,
					null);
		} 
		catch (Throwable e) {
			e.printStackTrace();
			throw new ExceptionFinder("Une erreur s'est produite lors du fetch : "+e.getMessage(), e);
		}
//		Debug.printArray("Objets "+this.entityName+" trouves", objects);
		
		return objects;
	}
	
	/**
	 * Retourne le nombre d'objets trouves.
	 * @return Entier eventuellement nul.
	 */
	public int getFoundObjectCount() {
		return this.objects.count();
	}
	
	/*
	 * Michael Haller
	 */
         // Recherche dans une table
    static public NSArray find(EOEditingContext ec,String leNomTable, EOQualifier leQualifier, NSArray leSort
    //        boolean refresh,
    //        boolean distinct
    ) {
        EOFetchSpecification myFetch;
        myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
        // myFetch.setRefreshesRefetchedObjects(true);
        // System.out.println("fetch : "+myFetch);
        //myFetch.setRefreshesRefetchedObjects(refresh);
        //myFetch.setUsesDistinct(distinct);
        return new NSArray(ec.objectsWithFetchSpecification(myFetch));
    }

}
