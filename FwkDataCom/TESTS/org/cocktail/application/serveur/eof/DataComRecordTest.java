package org.cocktail.application.serveur.eof;

import static org.junit.Assert.*;

import org.junit.Test;

public class DataComRecordTest {

    
    @Test
    public void verifieLongueurMax() {
        assertTrue(DataComRecord.verifieLongueurMax(null, -1));
        assertTrue(DataComRecord.verifieLongueurMax(null, 0));
        assertTrue(DataComRecord.verifieLongueurMax(null, 1));
        
        assertTrue(DataComRecord.verifieLongueurMax("", -1));
        assertTrue(DataComRecord.verifieLongueurMax("", 0));
        assertTrue(DataComRecord.verifieLongueurMax("", 1));

        assertTrue(DataComRecord.verifieLongueurMax("A", -1));
        assertFalse(DataComRecord.verifieLongueurMax("A", 0));
        assertTrue(DataComRecord.verifieLongueurMax("A", 1));

        assertTrue(DataComRecord.verifieLongueurMax("AB", -1));
        assertFalse(DataComRecord.verifieLongueurMax("AB", 0));
        assertFalse(DataComRecord.verifieLongueurMax("AB", 1));
        assertTrue(DataComRecord.verifieLongueurMax("AB", 2));
    }

    @Test
    public void verifieLongueurMin() {
        assertTrue(DataComRecord.verifieLongueurMin(null, -1));
        assertTrue(DataComRecord.verifieLongueurMin(null, 0));
        assertTrue(DataComRecord.verifieLongueurMin(null, 1));
        
        assertTrue(DataComRecord.verifieLongueurMin("", -1));
        assertTrue(DataComRecord.verifieLongueurMin("", 0));
        assertFalse(DataComRecord.verifieLongueurMin("", 1));

        assertTrue(DataComRecord.verifieLongueurMin("A", -1));
        assertTrue(DataComRecord.verifieLongueurMin("A", 0));
        assertTrue(DataComRecord.verifieLongueurMin("A", 1));

        assertTrue(DataComRecord.verifieLongueurMin("AB", 0));
        assertTrue(DataComRecord.verifieLongueurMin("AB", 1));
        assertTrue(DataComRecord.verifieLongueurMin("AB", 2));
        assertFalse(DataComRecord.verifieLongueurMin("AB", 3));
    }
}
