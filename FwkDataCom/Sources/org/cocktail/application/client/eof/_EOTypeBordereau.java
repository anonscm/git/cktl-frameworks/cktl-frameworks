/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeBordereau.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeBordereau extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_TypeBordereau";
	public static final String ENTITY_TABLE_NAME = "maracuja.Type_Bordereau";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tboOrdre";

	public static final String TBO_LIBELLE_KEY = "tboLibelle";
	public static final String TBO_ORDRE_KEY = "tboOrdre";
	public static final String TBO_SOUS_TYPE_KEY = "tboSousType";
	public static final String TBO_TYPE_KEY = "tboType";
	public static final String TBO_TYPE_CREATION_KEY = "tboTypeCreation";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String TBO_LIBELLE_COLKEY = "TBO_LIBELLE";
	public static final String TBO_ORDRE_COLKEY = "TBO_ORDRE";
	public static final String TBO_SOUS_TYPE_COLKEY = "TBO_SOUS_TYPE";
	public static final String TBO_TYPE_COLKEY = "TBO_TYPE";
	public static final String TBO_TYPE_CREATION_COLKEY = "TBO_TYPE_CREATION";



	// Relationships



	// Accessors methods
  public String tboLibelle() {
    return (String) storedValueForKey(TBO_LIBELLE_KEY);
  }

  public void setTboLibelle(String value) {
    takeStoredValueForKey(value, TBO_LIBELLE_KEY);
  }

  public Integer tboOrdre() {
    return (Integer) storedValueForKey(TBO_ORDRE_KEY);
  }

  public void setTboOrdre(Integer value) {
    takeStoredValueForKey(value, TBO_ORDRE_KEY);
  }

  public String tboSousType() {
    return (String) storedValueForKey(TBO_SOUS_TYPE_KEY);
  }

  public void setTboSousType(String value) {
    takeStoredValueForKey(value, TBO_SOUS_TYPE_KEY);
  }

  public String tboType() {
    return (String) storedValueForKey(TBO_TYPE_KEY);
  }

  public void setTboType(String value) {
    takeStoredValueForKey(value, TBO_TYPE_KEY);
  }

  public String tboTypeCreation() {
    return (String) storedValueForKey(TBO_TYPE_CREATION_KEY);
  }

  public void setTboTypeCreation(String value) {
    takeStoredValueForKey(value, TBO_TYPE_CREATION_KEY);
  }


  public static EOTypeBordereau createca_TypeBordereau(EOEditingContext editingContext, String tboLibelle
, Integer tboOrdre
, String tboSousType
, String tboType
, String tboTypeCreation
) {
    EOTypeBordereau eo = (EOTypeBordereau) createAndInsertInstance(editingContext, _EOTypeBordereau.ENTITY_NAME);    
		eo.setTboLibelle(tboLibelle);
		eo.setTboOrdre(tboOrdre);
		eo.setTboSousType(tboSousType);
		eo.setTboType(tboType);
		eo.setTboTypeCreation(tboTypeCreation);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTypeBordereau.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTypeBordereau.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOTypeBordereau localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeBordereau)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTypeBordereau localInstanceIn(EOEditingContext editingContext, EOTypeBordereau eo) {
    EOTypeBordereau localInstance = (eo == null) ? null : (EOTypeBordereau)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTypeBordereau#localInstanceIn a la place.
   */
	public static EOTypeBordereau localInstanceOf(EOEditingContext editingContext, EOTypeBordereau eo) {
		return EOTypeBordereau.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTypeBordereau fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTypeBordereau fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeBordereau)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeBordereau fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeBordereau eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeBordereau)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTypeBordereau fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeBordereau eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeBordereau ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeBordereau fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
