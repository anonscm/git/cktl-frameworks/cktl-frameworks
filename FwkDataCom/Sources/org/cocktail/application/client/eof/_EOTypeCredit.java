/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeCredit.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOTypeCredit extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_TypeCredit";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.TYPE_CREDIT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "tcdOrdre";

	public static final String LIB_KEY = "lib";
	public static final String TCD_ABREGE_KEY = "tcdAbrege";
	public static final String TCD_BUDGET_KEY = "tcdBudget";
	public static final String TCD_CODE_KEY = "tcdCode";
	public static final String TCD_LIBELLE_KEY = "tcdLibelle";
	public static final String TCD_PRESIDENT_KEY = "tcdPresident";
	public static final String TCD_SECT_KEY = "tcdSect";
	public static final String TCD_TYPE_KEY = "tcdType";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String LIB_COLKEY = "$attribute.columnName";
	public static final String TCD_ABREGE_COLKEY = "TCD_ABREGE";
	public static final String TCD_BUDGET_COLKEY = "TCD_budget";
	public static final String TCD_CODE_COLKEY = "TCD_CODE";
	public static final String TCD_LIBELLE_COLKEY = "TCD_LIBELLE";
	public static final String TCD_PRESIDENT_COLKEY = "TCD_PRESIDENT";
	public static final String TCD_SECT_COLKEY = "tcd_sect";
	public static final String TCD_TYPE_COLKEY = "TCD_type";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public String lib() {
    return (String) storedValueForKey(LIB_KEY);
  }

  public void setLib(String value) {
    takeStoredValueForKey(value, LIB_KEY);
  }

  public String tcdAbrege() {
    return (String) storedValueForKey(TCD_ABREGE_KEY);
  }

  public void setTcdAbrege(String value) {
    takeStoredValueForKey(value, TCD_ABREGE_KEY);
  }

  public String tcdBudget() {
    return (String) storedValueForKey(TCD_BUDGET_KEY);
  }

  public void setTcdBudget(String value) {
    takeStoredValueForKey(value, TCD_BUDGET_KEY);
  }

  public String tcdCode() {
    return (String) storedValueForKey(TCD_CODE_KEY);
  }

  public void setTcdCode(String value) {
    takeStoredValueForKey(value, TCD_CODE_KEY);
  }

  public String tcdLibelle() {
    return (String) storedValueForKey(TCD_LIBELLE_KEY);
  }

  public void setTcdLibelle(String value) {
    takeStoredValueForKey(value, TCD_LIBELLE_KEY);
  }

  public java.math.BigDecimal tcdPresident() {
    return (java.math.BigDecimal) storedValueForKey(TCD_PRESIDENT_KEY);
  }

  public void setTcdPresident(java.math.BigDecimal value) {
    takeStoredValueForKey(value, TCD_PRESIDENT_KEY);
  }

  public String tcdSect() {
    return (String) storedValueForKey(TCD_SECT_KEY);
  }

  public void setTcdSect(String value) {
    takeStoredValueForKey(value, TCD_SECT_KEY);
  }

  public String tcdType() {
    return (String) storedValueForKey(TCD_TYPE_KEY);
  }

  public void setTcdType(String value) {
    takeStoredValueForKey(value, TCD_TYPE_KEY);
  }

  public org.cocktail.application.client.eof.EOExercice exercice() {
    return (org.cocktail.application.client.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.application.client.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  

  public static EOTypeCredit createca_TypeCredit(EOEditingContext editingContext, String tcdAbrege
, String tcdBudget
, String tcdCode
, String tcdLibelle
, java.math.BigDecimal tcdPresident
, String tcdSect
, String tcdType
) {
    EOTypeCredit eo = (EOTypeCredit) createAndInsertInstance(editingContext, _EOTypeCredit.ENTITY_NAME);    
		eo.setTcdAbrege(tcdAbrege);
		eo.setTcdBudget(tcdBudget);
		eo.setTcdCode(tcdCode);
		eo.setTcdLibelle(tcdLibelle);
		eo.setTcdPresident(tcdPresident);
		eo.setTcdSect(tcdSect);
		eo.setTcdType(tcdType);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOTypeCredit.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOTypeCredit.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOTypeCredit localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeCredit)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOTypeCredit localInstanceIn(EOEditingContext editingContext, EOTypeCredit eo) {
    EOTypeCredit localInstance = (eo == null) ? null : (EOTypeCredit)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOTypeCredit#localInstanceIn a la place.
   */
	public static EOTypeCredit localInstanceOf(EOEditingContext editingContext, EOTypeCredit eo) {
		return EOTypeCredit.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOTypeCredit fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOTypeCredit fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeCredit)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeCredit fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeCredit eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeCredit)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOTypeCredit fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeCredit eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeCredit ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeCredit fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
