/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOOrganSignataire.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOOrganSignataire extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_OrganSignataire";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.ORGAN_SIGNATAIRE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "orsiId";

	public static final String ORSI_DATE_CLOTURE_KEY = "orsiDateCloture";
	public static final String ORSI_DATE_OUVERTURE_KEY = "orsiDateOuverture";
	public static final String ORSI_LIBELLE_SIGNATAIRE_KEY = "orsiLibelleSignataire";
	public static final String ORSI_REFERENCE_DELEGATION_KEY = "orsiReferenceDelegation";

// Attributs non visibles
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String ORG_ID_KEY = "orgId";
	public static final String ORSI_ID_KEY = "orsiId";
	public static final String TYSI_ID_KEY = "tysiId";

//Colonnes dans la base de donnees
	public static final String ORSI_DATE_CLOTURE_COLKEY = "ORSI_DATE_CLOTURE";
	public static final String ORSI_DATE_OUVERTURE_COLKEY = "ORSI_DATE_OUVERTURE";
	public static final String ORSI_LIBELLE_SIGNATAIRE_COLKEY = "ORSI_LIBELLE_SIGNATAIRE";
	public static final String ORSI_REFERENCE_DELEGATION_COLKEY = "ORSI_REFERENCE_DELEGATION";

	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String ORSI_ID_COLKEY = "ORSI_ID";
	public static final String TYSI_ID_COLKEY = "TYSI_ID";


	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String ORGAN_KEY = "organ";
	public static final String ORGAN_SIGNATAIRE_TCS_KEY = "organSignataireTcs";
	public static final String TYPE_SIGNATURE_KEY = "typeSignature";



	// Accessors methods
  public NSTimestamp orsiDateCloture() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_CLOTURE_KEY);
  }

  public void setOrsiDateCloture(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_CLOTURE_KEY);
  }

  public NSTimestamp orsiDateOuverture() {
    return (NSTimestamp) storedValueForKey(ORSI_DATE_OUVERTURE_KEY);
  }

  public void setOrsiDateOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, ORSI_DATE_OUVERTURE_KEY);
  }

  public String orsiLibelleSignataire() {
    return (String) storedValueForKey(ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public void setOrsiLibelleSignataire(String value) {
    takeStoredValueForKey(value, ORSI_LIBELLE_SIGNATAIRE_KEY);
  }

  public String orsiReferenceDelegation() {
    return (String) storedValueForKey(ORSI_REFERENCE_DELEGATION_KEY);
  }

  public void setOrsiReferenceDelegation(String value) {
    takeStoredValueForKey(value, ORSI_REFERENCE_DELEGATION_KEY);
  }

  public org.cocktail.application.client.eof.EOIndividuUlr individu() {
    return (org.cocktail.application.client.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.application.client.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOIndividuUlr oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOOrgan organ() {
    return (org.cocktail.application.client.eof.EOOrgan)storedValueForKey(ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.application.client.eof.EOOrgan value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeSignature typeSignature() {
    return (org.cocktail.application.client.eof.EOTypeSignature)storedValueForKey(TYPE_SIGNATURE_KEY);
  }

  public void setTypeSignatureRelationship(org.cocktail.application.client.eof.EOTypeSignature value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeSignature oldValue = typeSignature();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SIGNATURE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SIGNATURE_KEY);
    }
  }
  
  public NSArray organSignataireTcs() {
    return (NSArray)storedValueForKey(ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public NSArray organSignataireTcs(EOQualifier qualifier) {
    return organSignataireTcs(qualifier, null, false);
  }

  public NSArray organSignataireTcs(EOQualifier qualifier, boolean fetch) {
    return organSignataireTcs(qualifier, null, fetch);
  }

  public NSArray organSignataireTcs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.application.client.eof.EOOrganSignataireTc.ORGAN_SIGNATAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.application.client.eof.EOOrganSignataireTc.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = organSignataireTcs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOrganSignataireTcsRelationship(org.cocktail.application.client.eof.EOOrganSignataireTc object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public void removeFromOrganSignataireTcsRelationship(org.cocktail.application.client.eof.EOOrganSignataireTc object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
  }

  public org.cocktail.application.client.eof.EOOrganSignataireTc createOrganSignataireTcsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_OrganSignataireTc");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ORGAN_SIGNATAIRE_TCS_KEY);
    return (org.cocktail.application.client.eof.EOOrganSignataireTc) eo;
  }

  public void deleteOrganSignataireTcsRelationship(org.cocktail.application.client.eof.EOOrganSignataireTc object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ORGAN_SIGNATAIRE_TCS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOrganSignataireTcsRelationships() {
    Enumeration objects = organSignataireTcs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOrganSignataireTcsRelationship((org.cocktail.application.client.eof.EOOrganSignataireTc)objects.nextElement());
    }
  }


  public static EOOrganSignataire createca_OrganSignataire(EOEditingContext editingContext, org.cocktail.application.client.eof.EOIndividuUlr individu, org.cocktail.application.client.eof.EOOrgan organ, org.cocktail.application.client.eof.EOTypeSignature typeSignature) {
    EOOrganSignataire eo = (EOOrganSignataire) createAndInsertInstance(editingContext, _EOOrganSignataire.ENTITY_NAME);    
    eo.setIndividuRelationship(individu);
    eo.setOrganRelationship(organ);
    eo.setTypeSignatureRelationship(typeSignature);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOOrganSignataire.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOOrganSignataire.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOOrganSignataire localInstanceIn(EOEditingContext editingContext) {
	  		return (EOOrganSignataire)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOOrganSignataire localInstanceIn(EOEditingContext editingContext, EOOrganSignataire eo) {
    EOOrganSignataire localInstance = (eo == null) ? null : (EOOrganSignataire)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOOrganSignataire#localInstanceIn a la place.
   */
	public static EOOrganSignataire localInstanceOf(EOEditingContext editingContext, EOOrganSignataire eo) {
		return EOOrganSignataire.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOOrganSignataire fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOOrganSignataire fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOOrganSignataire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOOrganSignataire)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOOrganSignataire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOOrganSignataire fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOOrganSignataire eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOOrganSignataire)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOOrganSignataire fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOOrganSignataire eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOOrganSignataire ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOOrganSignataire fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
