/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to VFournisseur.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _VFournisseur extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_VFournisseur";
	public static final String ENTITY_TABLE_NAME = "maracuja.V_FOURNISSEUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "fouOrdre";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String ADR_CIVILITE_KEY = "adrCivilite";
	public static final String ADR_CP_KEY = "adrCp";
	public static final String ADR_NOM_KEY = "adrNom";
	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String ADR_PRENOM_KEY = "adrPrenom";
	public static final String ADR_VILLE_KEY = "adrVille";
	public static final String AGT_ORDRE_KEY = "agtOrdre";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_DATE_KEY = "fouDate";
	public static final String FOU_ETRANGER_KEY = "fouEtranger";
	public static final String FOU_MARCHE_KEY = "fouMarche";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String FOU_TYPE_KEY = "fouType";
	public static final String FOU_VALIDE_KEY = "fouValide";
	public static final String LC_PAYS_KEY = "lcPays";
	public static final String PERS_ID_KEY = "persId";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String ADR_CIVILITE_COLKEY = "ADR_CIVILITE";
	public static final String ADR_CP_COLKEY = "ADR_CP";
	public static final String ADR_NOM_COLKEY = "ADR_NOM";
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String ADR_PRENOM_COLKEY = "ADR_PRENOM";
	public static final String ADR_VILLE_COLKEY = "ADR_VILLE";
	public static final String AGT_ORDRE_COLKEY = "AGT_ORDRE";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_DATE_COLKEY = "FOU_DATE";
	public static final String FOU_ETRANGER_COLKEY = "FOU_ETRANGER";
	public static final String FOU_MARCHE_COLKEY = "FOU_MARCHE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String FOU_TYPE_COLKEY = "FOU_TYPE";
	public static final String FOU_VALIDE_COLKEY = "FOU_VALIDE";
	public static final String LC_PAYS_COLKEY = "LC_PAYS";
	public static final String PERS_ID_COLKEY = "PERS_ID";



	// Relationships



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String adrCivilite() {
    return (String) storedValueForKey(ADR_CIVILITE_KEY);
  }

  public void setAdrCivilite(String value) {
    takeStoredValueForKey(value, ADR_CIVILITE_KEY);
  }

  public String adrCp() {
    return (String) storedValueForKey(ADR_CP_KEY);
  }

  public void setAdrCp(String value) {
    takeStoredValueForKey(value, ADR_CP_KEY);
  }

  public String adrNom() {
    return (String) storedValueForKey(ADR_NOM_KEY);
  }

  public void setAdrNom(String value) {
    takeStoredValueForKey(value, ADR_NOM_KEY);
  }

  public Double adrOrdre() {
    return (Double) storedValueForKey(ADR_ORDRE_KEY);
  }

  public void setAdrOrdre(Double value) {
    takeStoredValueForKey(value, ADR_ORDRE_KEY);
  }

  public String adrPrenom() {
    return (String) storedValueForKey(ADR_PRENOM_KEY);
  }

  public void setAdrPrenom(String value) {
    takeStoredValueForKey(value, ADR_PRENOM_KEY);
  }

  public String adrVille() {
    return (String) storedValueForKey(ADR_VILLE_KEY);
  }

  public void setAdrVille(String value) {
    takeStoredValueForKey(value, ADR_VILLE_KEY);
  }

  public Double agtOrdre() {
    return (Double) storedValueForKey(AGT_ORDRE_KEY);
  }

  public void setAgtOrdre(Double value) {
    takeStoredValueForKey(value, AGT_ORDRE_KEY);
  }

  public String cpEtranger() {
    return (String) storedValueForKey(CP_ETRANGER_KEY);
  }

  public void setCpEtranger(String value) {
    takeStoredValueForKey(value, CP_ETRANGER_KEY);
  }

  public Double cptOrdre() {
    return (Double) storedValueForKey(CPT_ORDRE_KEY);
  }

  public void setCptOrdre(Double value) {
    takeStoredValueForKey(value, CPT_ORDRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String fouCode() {
    return (String) storedValueForKey(FOU_CODE_KEY);
  }

  public void setFouCode(String value) {
    takeStoredValueForKey(value, FOU_CODE_KEY);
  }

  public NSTimestamp fouDate() {
    return (NSTimestamp) storedValueForKey(FOU_DATE_KEY);
  }

  public void setFouDate(NSTimestamp value) {
    takeStoredValueForKey(value, FOU_DATE_KEY);
  }

  public String fouEtranger() {
    return (String) storedValueForKey(FOU_ETRANGER_KEY);
  }

  public void setFouEtranger(String value) {
    takeStoredValueForKey(value, FOU_ETRANGER_KEY);
  }

  public String fouMarche() {
    return (String) storedValueForKey(FOU_MARCHE_KEY);
  }

  public void setFouMarche(String value) {
    takeStoredValueForKey(value, FOU_MARCHE_KEY);
  }

  public Integer fouOrdre() {
    return (Integer) storedValueForKey(FOU_ORDRE_KEY);
  }

  public void setFouOrdre(Integer value) {
    takeStoredValueForKey(value, FOU_ORDRE_KEY);
  }

  public String fouType() {
    return (String) storedValueForKey(FOU_TYPE_KEY);
  }

  public void setFouType(String value) {
    takeStoredValueForKey(value, FOU_TYPE_KEY);
  }

  public String fouValide() {
    return (String) storedValueForKey(FOU_VALIDE_KEY);
  }

  public void setFouValide(String value) {
    takeStoredValueForKey(value, FOU_VALIDE_KEY);
  }

  public String lcPays() {
    return (String) storedValueForKey(LC_PAYS_KEY);
  }

  public void setLcPays(String value) {
    takeStoredValueForKey(value, LC_PAYS_KEY);
  }

  public Double persId() {
    return (Double) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Double value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }


  public static VFournisseur createca_VFournisseur(EOEditingContext editingContext, String adrCivilite
, String adrNom
, Double adrOrdre
, NSTimestamp dCreation
, NSTimestamp dModification
, String fouCode
, String fouMarche
, Integer fouOrdre
, Double persId
) {
    VFournisseur eo = (VFournisseur) createAndInsertInstance(editingContext, _VFournisseur.ENTITY_NAME);    
		eo.setAdrCivilite(adrCivilite);
		eo.setAdrNom(adrNom);
		eo.setAdrOrdre(adrOrdre);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setFouCode(fouCode);
		eo.setFouMarche(fouMarche);
		eo.setFouOrdre(fouOrdre);
		eo.setPersId(persId);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _VFournisseur.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _VFournisseur.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public VFournisseur localInstanceIn(EOEditingContext editingContext) {
	  		return (VFournisseur)localInstanceOfObject(editingContext, this);
	  }
	  
  public static VFournisseur localInstanceIn(EOEditingContext editingContext, VFournisseur eo) {
    VFournisseur localInstance = (eo == null) ? null : (VFournisseur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez VFournisseur#localInstanceIn a la place.
   */
	public static VFournisseur localInstanceOf(EOEditingContext editingContext, VFournisseur eo) {
		return VFournisseur.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static VFournisseur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static VFournisseur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    VFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (VFournisseur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static VFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static VFournisseur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    VFournisseur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (VFournisseur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static VFournisseur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  VFournisseur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet VFournisseur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static VFournisseur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
