/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOUtilisateur.java instead.
package org.cocktail.application.client.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;


public abstract class _EOUtilisateur extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_Utilisateur";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.UTILISATEUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "utlOrdre";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PRENOM_KEY = "nomPrenom";
	public static final String PERS_ID_KEY = "persId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_FERMETURE_KEY = "utlFermeture";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String UTL_OUVERTURE_KEY = "utlOuverture";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String TYET_ID_COLKEY = "tyet_id";
	public static final String UTL_FERMETURE_COLKEY = "UTL_FERMETURE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String UTL_OUVERTURE_COLKEY = "UTL_OUVERTURE";



	// Relationships
	public static final String INDIVIDU_KEY = "individu";
	public static final String PERSONNE_KEY = "personne";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_FONCTIONS_KEY = "utilisateurFonctions";
	public static final String UTILISATEUR_ORGANS_KEY = "utilisateurOrgans";



	// Accessors methods
  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomPrenom() {
    return (String) storedValueForKey(NOM_PRENOM_KEY);
  }

  public void setNomPrenom(String value) {
    takeStoredValueForKey(value, NOM_PRENOM_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public Integer tyetId() {
    return (Integer) storedValueForKey(TYET_ID_KEY);
  }

  public void setTyetId(Integer value) {
    takeStoredValueForKey(value, TYET_ID_KEY);
  }

  public NSTimestamp utlFermeture() {
    return (NSTimestamp) storedValueForKey(UTL_FERMETURE_KEY);
  }

  public void setUtlFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_FERMETURE_KEY);
  }

  public Integer utlOrdre() {
    return (Integer) storedValueForKey(UTL_ORDRE_KEY);
  }

  public void setUtlOrdre(Integer value) {
    takeStoredValueForKey(value, UTL_ORDRE_KEY);
  }

  public NSTimestamp utlOuverture() {
    return (NSTimestamp) storedValueForKey(UTL_OUVERTURE_KEY);
  }

  public void setUtlOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, UTL_OUVERTURE_KEY);
  }

  public org.cocktail.application.client.eof.EOIndividuUlr individu() {
    return (org.cocktail.application.client.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.application.client.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOIndividuUlr oldValue = individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOPersonne personne() {
    return (org.cocktail.application.client.eof.EOPersonne)storedValueForKey(PERSONNE_KEY);
  }

  public void setPersonneRelationship(org.cocktail.application.client.eof.EOPersonne value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOPersonne oldValue = personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSONNE_KEY);
    }
  }
  
  public org.cocktail.application.client.eof.EOTypeEtat typeEtat() {
    return (org.cocktail.application.client.eof.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.application.client.eof.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.application.client.eof.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public NSArray utilisateurFonctions() {
    return (NSArray)storedValueForKey(UTILISATEUR_FONCTIONS_KEY);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier) {
    return utilisateurFonctions(qualifier, null, false);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier, boolean fetch) {
    return utilisateurFonctions(qualifier, null, fetch);
  }

  public NSArray utilisateurFonctions(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.application.client.eof.EOUtilisateurFonction.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.application.client.eof.EOUtilisateurFonction.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurFonctions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurFonctionsRelationship(org.cocktail.application.client.eof.EOUtilisateurFonction object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public void removeFromUtilisateurFonctionsRelationship(org.cocktail.application.client.eof.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
  }

  public org.cocktail.application.client.eof.EOUtilisateurFonction createUtilisateurFonctionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_UtilisateurFonction");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_FONCTIONS_KEY);
    return (org.cocktail.application.client.eof.EOUtilisateurFonction) eo;
  }

  public void deleteUtilisateurFonctionsRelationship(org.cocktail.application.client.eof.EOUtilisateurFonction object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_FONCTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurFonctionsRelationships() {
    Enumeration objects = utilisateurFonctions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurFonctionsRelationship((org.cocktail.application.client.eof.EOUtilisateurFonction)objects.nextElement());
    }
  }

  public NSArray utilisateurOrgans() {
    return (NSArray)storedValueForKey(UTILISATEUR_ORGANS_KEY);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier) {
    return utilisateurOrgans(qualifier, null, false);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, boolean fetch) {
    return utilisateurOrgans(qualifier, null, fetch);
  }

  public NSArray utilisateurOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.application.client.eof.EOUtilisateurOrgan.UTILISATEUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.application.client.eof.EOUtilisateurOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = utilisateurOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToUtilisateurOrgansRelationship(org.cocktail.application.client.eof.EOUtilisateurOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public void removeFromUtilisateurOrgansRelationship(org.cocktail.application.client.eof.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
  }

  public org.cocktail.application.client.eof.EOUtilisateurOrgan createUtilisateurOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_UtilisateurOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, UTILISATEUR_ORGANS_KEY);
    return (org.cocktail.application.client.eof.EOUtilisateurOrgan) eo;
  }

  public void deleteUtilisateurOrgansRelationship(org.cocktail.application.client.eof.EOUtilisateurOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, UTILISATEUR_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUtilisateurOrgansRelationships() {
    Enumeration objects = utilisateurOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUtilisateurOrgansRelationship((org.cocktail.application.client.eof.EOUtilisateurOrgan)objects.nextElement());
    }
  }


  public static EOUtilisateur createca_Utilisateur(EOEditingContext editingContext, Integer noIndividu
, Integer persId
, Integer tyetId
, Integer utlOrdre
, NSTimestamp utlOuverture
, org.cocktail.application.client.eof.EOPersonne personne) {
    EOUtilisateur eo = (EOUtilisateur) createAndInsertInstance(editingContext, _EOUtilisateur.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
		eo.setPersId(persId);
		eo.setTyetId(tyetId);
		eo.setUtlOrdre(utlOrdre);
		eo.setUtlOuverture(utlOuverture);
    eo.setPersonneRelationship(personne);
    return eo;
  }

//  public static NSArray fetchAll(EOEditingContext editingContext) {
//    return _EOUtilisateur.fetchAll(editingContext, null);
//  }
//
//  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
//    return _EOUtilisateur.fetch(editingContext, null, sortOrderings);
//  }

  
  
  	  public EOUtilisateur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOUtilisateur)localInstanceOfObject(editingContext, this);
	  }
	  
  public static EOUtilisateur localInstanceIn(EOEditingContext editingContext, EOUtilisateur eo) {
    EOUtilisateur localInstance = (eo == null) ? null : (EOUtilisateur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return
   * @deprecated Utilisez EOUtilisateur#localInstanceIn a la place.
   */
	public static EOUtilisateur localInstanceOf(EOEditingContext editingContext, EOUtilisateur eo) {
		return EOUtilisateur.localInstanceIn(editingContext, eo);
	}
  
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException Si plusieurs objets sont retrournes 
		*/
	  public static EOUtilisateur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException Si plusieurs objets sont trouves.
	 */
	public static EOUtilisateur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOUtilisateur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOUtilisateur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOUtilisateur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException Si aucun objet est trouvé.
	   */
	  public static EOUtilisateur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOUtilisateur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOUtilisateur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOUtilisateur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
  
}
