/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeExer.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOCodeExer extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_CodeExer";
	public static final String ENTITY_TABLE_NAME = "jefy_marches.CODE_EXER";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "ceOrdre";

	public static final String CE3CMP_KEY = "ce3cmp";
	public static final String CE_AUTRES_KEY = "ceAutres";
	public static final String CE_CONTROLE_KEY = "ceControle";
	public static final String CE_MONOPOLE_KEY = "ceMonopole";
	public static final String CE_PERE_KEY = "cePere";
	public static final String CE_RECH_KEY = "ceRech";
	public static final String CE_SUPPR_KEY = "ceSuppr";
	public static final String CE_TYPE_KEY = "ceType";
	public static final String TCN_ID_KEY = "tcnId";

// Attributs non visibles
	public static final String CE_ORDRE_KEY = "ceOrdre";
	public static final String CM_ORDRE_KEY = "cmOrdre";
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String CE3CMP_COLKEY = "CE_3CMP";
	public static final String CE_AUTRES_COLKEY = "CE_AUTRES";
	public static final String CE_CONTROLE_COLKEY = "CE_CONTROLE";
	public static final String CE_MONOPOLE_COLKEY = "CE_MONOPOLE";
	public static final String CE_PERE_COLKEY = "CE_PERE";
	public static final String CE_RECH_COLKEY = "CE_RECH";
	public static final String CE_SUPPR_COLKEY = "CE_SUPPR";
	public static final String CE_TYPE_COLKEY = "CE_TYPE";
	public static final String TCN_ID_COLKEY = "TCN_ID";

	public static final String CE_ORDRE_COLKEY = "CE_ORDRE";
	public static final String CM_ORDRE_COLKEY = "CM_ORDRE";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String CODE_MARCHE_KEY = "codeMarche";
	public static final String EXERCICE_KEY = "exercice";



	// Accessors methods
  public Double ce3cmp() {
    return (Double) storedValueForKey(CE3CMP_KEY);
  }

  public void setCe3cmp(Double value) {
    takeStoredValueForKey(value, CE3CMP_KEY);
  }

  public Double ceAutres() {
    return (Double) storedValueForKey(CE_AUTRES_KEY);
  }

  public void setCeAutres(Double value) {
    takeStoredValueForKey(value, CE_AUTRES_KEY);
  }

  public java.math.BigDecimal ceControle() {
    return (java.math.BigDecimal) storedValueForKey(CE_CONTROLE_KEY);
  }

  public void setCeControle(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CE_CONTROLE_KEY);
  }

  public Double ceMonopole() {
    return (Double) storedValueForKey(CE_MONOPOLE_KEY);
  }

  public void setCeMonopole(Double value) {
    takeStoredValueForKey(value, CE_MONOPOLE_KEY);
  }

  public Double cePere() {
    return (Double) storedValueForKey(CE_PERE_KEY);
  }

  public void setCePere(Double value) {
    takeStoredValueForKey(value, CE_PERE_KEY);
  }

  public String ceRech() {
    return (String) storedValueForKey(CE_RECH_KEY);
  }

  public void setCeRech(String value) {
    takeStoredValueForKey(value, CE_RECH_KEY);
  }

  public String ceSuppr() {
    return (String) storedValueForKey(CE_SUPPR_KEY);
  }

  public void setCeSuppr(String value) {
    takeStoredValueForKey(value, CE_SUPPR_KEY);
  }

  public String ceType() {
    return (String) storedValueForKey(CE_TYPE_KEY);
  }

  public void setCeType(String value) {
    takeStoredValueForKey(value, CE_TYPE_KEY);
  }

  public Double tcnId() {
    return (Double) storedValueForKey(TCN_ID_KEY);
  }

  public void setTcnId(Double value) {
    takeStoredValueForKey(value, TCN_ID_KEY);
  }

  public org.cocktail.application.serveur.eof.EOCodeMarche codeMarche() {
    return (org.cocktail.application.serveur.eof.EOCodeMarche)storedValueForKey(CODE_MARCHE_KEY);
  }
  
	public void setCodeMarche(org.cocktail.application.serveur.eof.EOCodeMarche value) {
		takeStoredValueForKey(value,CODE_MARCHE_KEY);
	}


  public void setCodeMarcheRelationship(org.cocktail.application.serveur.eof.EOCodeMarche value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOCodeMarche oldValue = codeMarche();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_MARCHE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_MARCHE_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOExercice exercice() {
    return (org.cocktail.application.serveur.eof.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.application.serveur.eof.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.application.serveur.eof.EOExercice value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCodeExer avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeExer createEOCodeExer(EOEditingContext editingContext			) {
    EOCodeExer eo = (EOCodeExer) createAndInsertInstance(editingContext, _EOCodeExer.ENTITY_NAME);    
    return eo;
  }

  
	  public EOCodeExer localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeExer)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeExer creerInstance(EOEditingContext editingContext) {
	  		EOCodeExer object = (EOCodeExer)createAndInsertInstance(editingContext, _EOCodeExer.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOCodeExer localInstanceIn(EOEditingContext editingContext, EOCodeExer eo) {
    EOCodeExer localInstance = (eo == null) ? null : (EOCodeExer)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeExer#localInstanceIn a la place.
   */
	public static EOCodeExer localInstanceOf(EOEditingContext editingContext, EOCodeExer eo) {
		return EOCodeExer.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeExer fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeExer fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeExer)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeExer fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeExer eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeExer)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeExer fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeExer eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeExer ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeExer fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
