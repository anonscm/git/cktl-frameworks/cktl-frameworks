/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOIndividuUlr.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOIndividuUlr extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_IndividuUlr";
	public static final String ENTITY_TABLE_NAME = "grhum.INDIVidU_ulr";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String C_CIVILITE_KEY = "cCivilite";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String IND_ACTIVITE_KEY = "indActivite";
	public static final String IND_CLE_INSEE_KEY = "indCleInsee";
	public static final String IND_C_SITUATION_FAMILLE_KEY = "indCSituationFamille";
	public static final String IND_NO_INSEE_KEY = "indNoInsee";
	public static final String IND_ORIGINE_KEY = "indOrigine";
	public static final String IND_QUALITE_KEY = "indQualite";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERSONNE_PERS_NOM_PRENOM_KEY = "personne_persNomPrenom";
	public static final String PRENOM_KEY = "prenom";
	public static final String TEM_VALIDE_KEY = "temValide";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String C_CIVILITE_COLKEY = "C_CIVILITE";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String IND_ACTIVITE_COLKEY = "IND_ACTIVITE";
	public static final String IND_CLE_INSEE_COLKEY = "IND_CLE_INSEE";
	public static final String IND_C_SITUATION_FAMILLE_COLKEY = "IND_C_SITUATION_FAMILLE";
	public static final String IND_NO_INSEE_COLKEY = "IND_NO_INSEE";
	public static final String IND_ORIGINE_COLKEY = "IND_ORIGINE";
	public static final String IND_QUALITE_COLKEY = "IND_QUALITE";
	public static final String NO_INDIVIDU_COLKEY = "no_individu";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERSONNE_PERS_NOM_PRENOM_COLKEY = "$attribute.columnName";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String TEM_VALIDE_COLKEY = "TEM_VALIDE";



	// Relationships
	public static final String CA__PERSONNE_KEY = "ca_Personne";
	public static final String REPART_STRUCTURES_KEY = "repartStructures";
	public static final String TOS_V_PERSONNE_TELEPHONES_KEY = "tosVPersonneTelephones";



	// Accessors methods
  public String cCivilite() {
    return (String) storedValueForKey(C_CIVILITE_KEY);
  }

  public void setCCivilite(String value) {
    takeStoredValueForKey(value, C_CIVILITE_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public String indActivite() {
    return (String) storedValueForKey(IND_ACTIVITE_KEY);
  }

  public void setIndActivite(String value) {
    takeStoredValueForKey(value, IND_ACTIVITE_KEY);
  }

  public Integer indCleInsee() {
    return (Integer) storedValueForKey(IND_CLE_INSEE_KEY);
  }

  public void setIndCleInsee(Integer value) {
    takeStoredValueForKey(value, IND_CLE_INSEE_KEY);
  }

  public String indCSituationFamille() {
    return (String) storedValueForKey(IND_C_SITUATION_FAMILLE_KEY);
  }

  public void setIndCSituationFamille(String value) {
    takeStoredValueForKey(value, IND_C_SITUATION_FAMILLE_KEY);
  }

  public String indNoInsee() {
    return (String) storedValueForKey(IND_NO_INSEE_KEY);
  }

  public void setIndNoInsee(String value) {
    takeStoredValueForKey(value, IND_NO_INSEE_KEY);
  }

  public String indOrigine() {
    return (String) storedValueForKey(IND_ORIGINE_KEY);
  }

  public void setIndOrigine(String value) {
    takeStoredValueForKey(value, IND_ORIGINE_KEY);
  }

  public String indQualite() {
    return (String) storedValueForKey(IND_QUALITE_KEY);
  }

  public void setIndQualite(String value) {
    takeStoredValueForKey(value, IND_QUALITE_KEY);
  }

  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public String personne_persNomPrenom() {
    return (String) storedValueForKey(PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public void setPersonne_persNomPrenom(String value) {
    takeStoredValueForKey(value, PERSONNE_PERS_NOM_PRENOM_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String temValide() {
    return (String) storedValueForKey(TEM_VALIDE_KEY);
  }

  public void setTemValide(String value) {
    takeStoredValueForKey(value, TEM_VALIDE_KEY);
  }

  public org.cocktail.application.serveur.eof.EOPersonne ca_Personne() {
    return (org.cocktail.application.serveur.eof.EOPersonne)storedValueForKey(CA__PERSONNE_KEY);
  }
  
	public void setCa_Personne(org.cocktail.application.serveur.eof.EOPersonne value) {
		takeStoredValueForKey(value,CA__PERSONNE_KEY);
	}


  public void setCa_PersonneRelationship(org.cocktail.application.serveur.eof.EOPersonne value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOPersonne oldValue = ca_Personne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CA__PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CA__PERSONNE_KEY);
    }
  }
  
  public NSArray repartStructures() {
    return (NSArray)storedValueForKey(REPART_STRUCTURES_KEY);
  }
  
  //ICI
  public void setRepartStructures(NSArray values) {
	  takeStoredValueForKey(values,REPART_STRUCTURES_KEY);
  }
  

  public NSArray repartStructures(EOQualifier qualifier) {
    return repartStructures(qualifier, null);
  }

  public NSArray repartStructures(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = repartStructures();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToRepartStructuresRelationship(org.cocktail.application.serveur.eof.EORepartStructure object) {
    addObjectToBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public void removeFromRepartStructuresRelationship(org.cocktail.application.serveur.eof.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
  }

  public org.cocktail.application.serveur.eof.EORepartStructure createRepartStructuresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, REPART_STRUCTURES_KEY);
    return (org.cocktail.application.serveur.eof.EORepartStructure) eo;
  }

  public void deleteRepartStructuresRelationship(org.cocktail.application.serveur.eof.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, REPART_STRUCTURES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartStructuresRelationships() {
    Enumeration objects = repartStructures().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartStructuresRelationship((org.cocktail.application.serveur.eof.EORepartStructure)objects.nextElement());
    }
  }

  public NSArray tosVPersonneTelephones() {
    return (NSArray)storedValueForKey(TOS_V_PERSONNE_TELEPHONES_KEY);
  }
  
  //ICI
  public void setTosVPersonneTelephones(NSArray values) {
	  takeStoredValueForKey(values,TOS_V_PERSONNE_TELEPHONES_KEY);
  }
  

  public NSArray tosVPersonneTelephones(EOQualifier qualifier) {
    return tosVPersonneTelephones(qualifier, null);
  }

  public NSArray tosVPersonneTelephones(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = tosVPersonneTelephones();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
  }

  public void removeFromTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
  }

  public org.cocktail.application.serveur.eof.VPersonneTelephone createTosVPersonneTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ca_VPersonneTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TOS_V_PERSONNE_TELEPHONES_KEY);
    return (org.cocktail.application.serveur.eof.VPersonneTelephone) eo;
  }

  public void deleteTosVPersonneTelephonesRelationship(org.cocktail.application.serveur.eof.VPersonneTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TOS_V_PERSONNE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTosVPersonneTelephonesRelationships() {
    Enumeration objects = tosVPersonneTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTosVPersonneTelephonesRelationship((org.cocktail.application.serveur.eof.VPersonneTelephone)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOIndividuUlr avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOIndividuUlr createEOIndividuUlr(EOEditingContext editingContext, String cCivilite
, Integer noIndividu
, Integer persId
, String temValide
			) {
    EOIndividuUlr eo = (EOIndividuUlr) createAndInsertInstance(editingContext, _EOIndividuUlr.ENTITY_NAME);    
		eo.setCCivilite(cCivilite);
		eo.setNoIndividu(noIndividu);
		eo.setPersId(persId);
		eo.setTemValide(temValide);
    return eo;
  }

  
	  public EOIndividuUlr localInstanceIn(EOEditingContext editingContext) {
	  		return (EOIndividuUlr)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOIndividuUlr creerInstance(EOEditingContext editingContext) {
	  		EOIndividuUlr object = (EOIndividuUlr)createAndInsertInstance(editingContext, _EOIndividuUlr.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOIndividuUlr localInstanceIn(EOEditingContext editingContext, EOIndividuUlr eo) {
    EOIndividuUlr localInstance = (eo == null) ? null : (EOIndividuUlr)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOIndividuUlr#localInstanceIn a la place.
   */
	public static EOIndividuUlr localInstanceOf(EOEditingContext editingContext, EOIndividuUlr eo) {
		return EOIndividuUlr.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOIndividuUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOIndividuUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOIndividuUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOIndividuUlr)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOIndividuUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOIndividuUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOIndividuUlr eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOIndividuUlr)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOIndividuUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOIndividuUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOIndividuUlr ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOIndividuUlr fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
