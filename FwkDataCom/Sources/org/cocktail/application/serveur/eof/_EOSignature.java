/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOSignature.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOSignature extends  DataComRecord {
	public static final String ENTITY_NAME = "ca_Signature";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.signature";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "noIndividu";

	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String SIGN_DATE_MODIFICATION_KEY = "signDateModification";
	public static final String SIGN_IMG_KEY = "signImg";

// Attributs non visibles
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String NO_INDIVIDU_COLKEY = "no_individu";
	public static final String SIGN_DATE_MODIFICATION_COLKEY = "SIGN_DATE_MODIFICATION";
	public static final String SIGN_IMG_COLKEY = "sign_img";

	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String INDIVIDU_ULR_KEY = "individuUlr";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public Integer noIndividu() {
    return (Integer) storedValueForKey(NO_INDIVIDU_KEY);
  }

  public void setNoIndividu(Integer value) {
    takeStoredValueForKey(value, NO_INDIVIDU_KEY);
  }

  public NSTimestamp signDateModification() {
    return (NSTimestamp) storedValueForKey(SIGN_DATE_MODIFICATION_KEY);
  }

  public void setSignDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, SIGN_DATE_MODIFICATION_KEY);
  }

  public NSData signImg() {
    return (NSData) storedValueForKey(SIGN_IMG_KEY);
  }

  public void setSignImg(NSData value) {
    takeStoredValueForKey(value, SIGN_IMG_KEY);
  }

  public org.cocktail.application.serveur.eof.EOIndividuUlr individuUlr() {
    return (org.cocktail.application.serveur.eof.EOIndividuUlr)storedValueForKey(INDIVIDU_ULR_KEY);
  }
  
	public void setIndividuUlr(org.cocktail.application.serveur.eof.EOIndividuUlr value) {
		takeStoredValueForKey(value,INDIVIDU_ULR_KEY);
	}


  public void setIndividuUlrRelationship(org.cocktail.application.serveur.eof.EOIndividuUlr value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOIndividuUlr oldValue = individuUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, INDIVIDU_ULR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, INDIVIDU_ULR_KEY);
    }
  }
  
  public org.cocktail.application.serveur.eof.EOUtilisateur utilisateur() {
    return (org.cocktail.application.serveur.eof.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.application.serveur.eof.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.application.serveur.eof.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.application.serveur.eof.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOSignature avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOSignature createEOSignature(EOEditingContext editingContext, Integer noIndividu
, NSTimestamp signDateModification
, NSData signImg
			) {
    EOSignature eo = (EOSignature) createAndInsertInstance(editingContext, _EOSignature.ENTITY_NAME);    
		eo.setNoIndividu(noIndividu);
		eo.setSignDateModification(signDateModification);
		eo.setSignImg(signImg);
    return eo;
  }

  
	  public EOSignature localInstanceIn(EOEditingContext editingContext) {
	  		return (EOSignature)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOSignature creerInstance(EOEditingContext editingContext) {
	  		EOSignature object = (EOSignature)createAndInsertInstance(editingContext, _EOSignature.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOSignature localInstanceIn(EOEditingContext editingContext, EOSignature eo) {
    EOSignature localInstance = (eo == null) ? null : (EOSignature)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOSignature#localInstanceIn a la place.
   */
	public static EOSignature localInstanceOf(EOEditingContext editingContext, EOSignature eo) {
		return EOSignature.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOSignature fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOSignature fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOSignature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOSignature)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOSignature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOSignature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOSignature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOSignature)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOSignature fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOSignature eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOSignature ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOSignature fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
