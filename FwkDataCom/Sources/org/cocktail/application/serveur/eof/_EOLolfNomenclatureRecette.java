/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOLolfNomenclatureRecette.java instead.
package org.cocktail.application.serveur.eof;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOLolfNomenclatureRecette extends org.cocktail.application.serveur.eof.EOLolfNomenclatureAbstract {
	public static final String ENTITY_NAME = "ca_LolfNomenclatureRecette";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.Lolf_Nomenclature_Recette";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "lolfId";

	public static final String LOLF_ABREVIATION_KEY = "lolfAbreviation";
	public static final String LOLF_CODE_KEY = "lolfCode";
	public static final String LOLF_FERMETURE_KEY = "lolfFermeture";
	public static final String LOLF_LIBELLE_KEY = "lolfLibelle";
	public static final String LOLF_NIVEAU_KEY = "lolfNiveau";
	public static final String LOLF_ORDRE_AFFICHAGE_KEY = "lolfOrdreAffichage";
	public static final String LOLF_OUVERTURE_KEY = "lolfOuverture";

// Attributs non visibles
	public static final String LOLF_ID_KEY = "lolfId";
	public static final String LOLF_PERE_KEY = "lolfPere";
	public static final String LOLF_TYPE_KEY = "lolfType";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String LOLF_ABREVIATION_COLKEY = "lolf_abreviation";
	public static final String LOLF_CODE_COLKEY = "lolf_CODE";
	public static final String LOLF_FERMETURE_COLKEY = "lolf_fermeture";
	public static final String LOLF_LIBELLE_COLKEY = "lolf_LIBELLE";
	public static final String LOLF_NIVEAU_COLKEY = "lolf_niveau";
	public static final String LOLF_ORDRE_AFFICHAGE_COLKEY = "lolf_ordre_affichage";
	public static final String LOLF_OUVERTURE_COLKEY = "lolf_OUVERTURE";

	public static final String LOLF_ID_COLKEY = "lolf_id";
	public static final String LOLF_PERE_COLKEY = "lolf_pere";
	public static final String LOLF_TYPE_COLKEY = "lolf_type";
	public static final String TYET_ID_COLKEY = "tyet_id";


	// Relationships
	public static final String LOLF_NOMENCLATURE_FILS_KEY = "lolfNomenclatureFils";
	public static final String LOLF_NOMENCLATURE_PERE_KEY = "lolfNomenclaturePere";
	public static final String LOLF_NOMENCLATURE_TYPE_KEY = "lolfNomenclatureType";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods

/**
 * Créer une instance de EOLolfNomenclatureRecette avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOLolfNomenclatureRecette createEOLolfNomenclatureRecette(EOEditingContext editingContext, String lolfAbreviation
, String lolfCode
, String lolfLibelle
, Integer lolfNiveau
, Integer lolfOrdreAffichage
, NSTimestamp lolfOuverture
			) {
    EOLolfNomenclatureRecette eo = (EOLolfNomenclatureRecette) createAndInsertInstance(editingContext, _EOLolfNomenclatureRecette.ENTITY_NAME);    
		eo.setLolfAbreviation(lolfAbreviation);
		eo.setLolfCode(lolfCode);
		eo.setLolfLibelle(lolfLibelle);
		eo.setLolfNiveau(lolfNiveau);
		eo.setLolfOrdreAffichage(lolfOrdreAffichage);
		eo.setLolfOuverture(lolfOuverture);
    return eo;
  }

  
	  public EOLolfNomenclatureRecette localInstanceIn(EOEditingContext editingContext) {
	  		return (EOLolfNomenclatureRecette)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOLolfNomenclatureRecette creerInstance(EOEditingContext editingContext) {
	  		EOLolfNomenclatureRecette object = (EOLolfNomenclatureRecette)createAndInsertInstance(editingContext, _EOLolfNomenclatureRecette.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOLolfNomenclatureRecette localInstanceIn(EOEditingContext editingContext, EOLolfNomenclatureRecette eo) {
    EOLolfNomenclatureRecette localInstance = (eo == null) ? null : (EOLolfNomenclatureRecette)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOLolfNomenclatureRecette#localInstanceIn a la place.
   */
	public static EOLolfNomenclatureRecette localInstanceOf(EOEditingContext editingContext, EOLolfNomenclatureRecette eo) {
		return EOLolfNomenclatureRecette.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOLolfNomenclatureRecette fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOLolfNomenclatureRecette fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOLolfNomenclatureRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOLolfNomenclatureRecette)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOLolfNomenclatureRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOLolfNomenclatureRecette fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOLolfNomenclatureRecette eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOLolfNomenclatureRecette)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOLolfNomenclatureRecette fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOLolfNomenclatureRecette eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOLolfNomenclatureRecette ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOLolfNomenclatureRecette fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
