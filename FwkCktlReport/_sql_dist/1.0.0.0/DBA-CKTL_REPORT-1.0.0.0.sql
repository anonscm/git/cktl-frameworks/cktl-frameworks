
SET DEFINE OFF;

--
-- 
-- ____________________________________________________________________________________________
--  /!\ ATTENTION /!\  fichier encodé en UTF-8   (  il peut  contenir des é è ç à î ê ô ... )
-- ____________________________________________________________________________________________
--
--


whenever sqlerror exit sql.sqlcode ;


CREATE USER CKTL_REPORT IDENTIFIED BY cktl_report
DEFAULT TABLESPACE DATA_GRHUM
TEMPORARY TABLESPACE TEMP
QUOTA UNLIMITED ON DATA_GRHUM
QUOTA UNLIMITED ON INDX_GRHUM
;

GRANT CREATE SESSION TO CKTL_REPORT;
GRANT CREATE TABLE TO CKTL_REPORT;
GRANT CREATE SYNONYM TO CKTL_REPORT;
GRANT CREATE VIEW TO CKTL_REPORT;
GRANT CREATE PROCEDURE TO CKTL_REPORT;
GRANT CREATE SEQUENCE TO CKTL_REPORT;
GRANT CREATE TRIGGER TO CKTL_REPORT;
GRANT UNLIMITED TABLESPACE TO CKTL_REPORT;
grant create type to cktl_report;

grant select, references on grhum.personne to cktl_report;

grant select on jefy_report.situation_categorie to CKTL_REPORT;
grant select on jefy_report.situation to CKTL_REPORT;
grant select on jefy_report.situation_critere to CKTL_REPORT;
grant select on jefy_report.situation_critere_popup to CKTL_REPORT;
grant select on jefy_report.situation_critere_repart to CKTL_REPORT;
grant select on jefy_report.situation_exercice to CKTL_REPORT;

grant select on jefy_admin.exercice to CKTL_REPORT with grant option;
grant select on jefy_admin.exercice_cocktail to CKTL_REPORT with grant option;
grant select on maracuja.plan_comptable_exer to CKTL_REPORT with grant option;


grant select on jefy_admin.utilisateur to cktl_report;
grant select, delete on jefy_admin.utilisateur_fonct to cktl_report;
grant select, delete on jefy_admin.utilisateur_fonct_exercice to cktl_report;
grant select, delete on jefy_admin.utilisateur_fonct_gestion to cktl_report;
grant select, delete on jefy_admin.fonction to cktl_report;
grant select on jefy_admin.type_credit to cktl_report;

grant select on accords.contrat to cktl_report;

grant execute on jefy_admin.api_application to cktl_report;
grant execute on jefy_admin.api_utilisateur to cktl_report;

PROMPT Le mot de passe du user est cktl_report
PROMPT il est vivement recommandé de le changer
PROMPT Alter user CKTL_REPORT identified by "unvraimotdepasse";
