declare
   catid    number;
   rapid    number;
   rjrid    number;
   critid   number;
   rcrid    number;
begin
   cktl_report.api_report.prv_add_categorie ('org.cocktail.cktlreport.categorie.scol', 'org.cocktail.cktlreport.categorie', 'Scolarité', 'Commentaire', 'N', 1, catid);
   cktl_report.api_report.prv_add_categorie ('org.cocktail.cktlreport.categorie.scol.administratif', 'org.cocktail.cktlreport.categorie.scol', 'Administratif', 'Scolarité Administrative - scola', 'N', 2, catid);
   cktl_report.api_report.prv_add_critere
      ('org.cocktail.cktlreport.critere.ca_rne',
       'CA_RNE',
       'Code Etablissement',
       'STRING',
       'POPUP',
       'N',
       'org.cocktail.cktlreport.categorie.scol',
       'Code RNE etablissement',
       'select s.c_rne cle, s.c_rne valeur,decode(s.c_type_structure,''E'',1,''C'',2,''CS'',3) ,S.LL_STRUCTURE from grhum.structure_ulr s  where c_type_structure in (''C'',''CS'',''E'') and c_rne is not null
order by  decode(s.c_type_structure,''E'',1,''C'',2,''CS'',3) asc',
       'N',
       1,
       critid
      );
   cktl_report.api_report.prv_add_critere
      ('org.cocktail.cktlreport.critere.scol.cycle',
       'SCOL_CYCLE',
       'Diplome Cycle',
       'STRING',
       'POPUP',
       'N',
       'org.cocktail.cktlreport.categorie.scol',
       'commentaire',
       'select distinct concat(SFD.FDIP_ABREVIATION,sfd.fhab_niveau) as cle,concat(sfd.fgra_code,concat('' - ''||SFD.FDIP_ABREVIATION,sfd.fhab_niveau)) as valeur
from scolarite.v_scol_formation_detail sfd
where SFD.FANN_KEY = $P{SCOL_EXE}
order by cle',
       'N',
       1,
       critid
      );
   cktl_report.api_report.prv_add_critere ('org.cocktail.cktlreport.critere.scol.exe',
                                           'SCOL_EXE',
                                           'Année Universitaire',
                                           'INTEGER',
                                           'POPUP',
                                           'N',
                                           'org.cocktail.cktlreport.categorie.scol',
                                           'commentaire',
                                           'select distinct cons_annee_scol as cle, to_char(cons_annee_scol) ||''-''||to_char(cons_annee_scol+1) as valeur
from garnuche.constantes order by cons_annee_scol desc',
                                           'N',
                                           1,
                                           critid
                                          );
   cktl_report.api_report.prv_add_critere ('org.cocktail.cktlreport.critere.scol.date',
                                           'SCOL_DATE',
                                           'Date Validation',
                                           'DATE',
                                           'POPUP',
                                           'N',
                                           'org.cocktail.cktlreport.categorie.scol',
                                           'commentaire',
                                           'select distinct to_date(bp.paie_date,''dd/mm/yy'') as cle, to_char(bp.paie_date,''dd/mm/yyyy'') as valeur
from garnuche.bordereau_paiement bp order by valeur desc',
                                           'N',
                                           1,
                                           critid
                                          );
   cktl_report.api_report.add_rapport_jrxml ('scola_admresult',
                                             'Admission : résultats',
                                             'PUBLIE',
                                             'N',
                                             'liste résultat des admissions des Etudiants (Alpha et cycle)',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('30/03/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_admresult_admis.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_admlist',
                                             'Admission : liste alpha des inscrits',
                                             'PUBLIE',
                                             'N',
                                             'liste des admissions des Etudiants (Alpha)',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('30/03/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_admlist.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_bourse_globale',
                                             'Boursiers : Liste alpha',
                                             'PUBLIE',
                                             'N',
                                             'liste des Etudiants Boursiers(Alpha)',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('05/04/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_bourse_globale.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_bourse_domaine',
                                             'Boursiers : par Département',
                                             'PUBLIE',
                                             'N',
                                             'liste des Etudiants Boursiers (Département)',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('30/03/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_bourse_domaine.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_bourse_filiere',
                                             'Boursiers : par Filière',
                                             'PUBLIE',
                                             'N',
                                             'liste des Etudiants Boursiers (Filière)',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('30/03/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_bourse_filiere.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_ins_mouvement',
                                             'Inscription : Mouvements comptables',
                                             'PUBLIE',
                                             'N',
                                             'Inscription : récapitulatif Mouvements comptables',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('11/04/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_ins_mouvement.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_ins_journal',
                                             'Inscription : Journal des Encaissements',
                                             'PUBLIE',
                                             'N',
                                             'Inscription : Journal des Encaissements des Droits d''Inscription',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('11/04/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_ins_journal.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_rapport_jrxml ('scola_ins_bordereau',
                                             'Inscription : Liste des Règlements',
                                             'PUBLIE',
                                             'N',
                                             'Inscription : Liste des Règlements des Inscriptions',
                                             null,
                                             null,
                                             'org.cocktail.cktlreport.categorie.scol.administratif',
                                             '1.0.0.0',
                                             to_date ('11/04/2012', 'dd/mm/yyyy'),
                                             'EV',
                                             'scola_ins_bordereau.jasper',
                                             '1.0.0.0',
                                             'PDF',
                                             rapid,
                                             rjrid
                                            );
   cktl_report.api_report.add_critere_to_rapport ('scola_admresult', 'org.cocktail.cktlreport.critere.scol.exe', 'null1', 1, 'O', 'Année Univ.', 'Année Universitaire', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_admresult', 'org.cocktail.cktlreport.critere.scol.cycle', null, 2, 'O', null, null, rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_admlist', 'org.cocktail.cktlreport.critere.scol.exe', 'null1', 1, 'O', 'Année Univ.', 'Année Universitaire', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_admlist', 'org.cocktail.cktlreport.critere.scol.cycle', null, 2, 'O', null, null, rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_globale', 'org.cocktail.cktlreport.critere.scol.exe', 'null1', 1, 'O', 'Année Univ.', 'Année Universitaire', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_globale', 'org.cocktail.cktlreport.critere.ca_rne', null, 2, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_globale', 'org.cocktail.cktlreport.critere.gen.datedebut', null, 3, 'O', 'Choisissez la date de début', 'date de début', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_globale', 'org.cocktail.cktlreport.critere.gen.datefin', 'null1', 4, 'O', 'Choisissez la date de fin', 'date de fin', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_domaine', 'org.cocktail.cktlreport.critere.scol.exe', 'null1', 1, 'O', 'Année Univ.', 'Année Universitaire', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_domaine', 'org.cocktail.cktlreport.critere.ca_rne', null, 2, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_domaine', 'org.cocktail.cktlreport.critere.gen.datedebut', null, 3, 'O', 'Choisissez la date de début', 'date de début', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_domaine', 'org.cocktail.cktlreport.critere.gen.datefin', 'null1', 4, 'O', 'Choisissez la date de fin', 'date de fin', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_filiere', 'org.cocktail.cktlreport.critere.scol.exe', 'null1', 1, 'O', 'Année Univ.', 'Année Universitaire', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_bourse_filiere', 'org.cocktail.cktlreport.critere.ca_rne', null, 2, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_mouvement', 'org.cocktail.cktlreport.critere.scol.date', 'null1', 2, 'O', 'Date Validation.', 'Date de Validation', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_mouvement', 'org.cocktail.cktlreport.critere.ca_rne', null, 1, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_journal', 'org.cocktail.cktlreport.critere.scol.date', 'null1', 1, 'O', 'Date Validation.', 'Date de Validation', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_journal', 'org.cocktail.cktlreport.critere.ca_rne', null, 2, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_bordereau', 'org.cocktail.cktlreport.critere.scol.date', 'null1', 1, 'O', 'Date Validation.', 'Date de Validation', rcrid);
   cktl_report.api_report.add_critere_to_rapport ('scola_ins_bordereau', 'org.cocktail.cktlreport.critere.ca_rne', null, 2, 'O', 'Code RNE Etab', 'RNE Etab', rcrid);
   commit;
end;