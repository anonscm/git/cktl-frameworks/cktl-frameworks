CREATE OR REPLACE PACKAGE CKTL_REPORT.API_REPORT IS 


  FUNCTION SplitAtPos ( PC$Chaine IN VARCHAR2, PN$Pos IN PLS_INTEGER, PC$Sep IN VARCHAR2 DEFAULT ',' )
    RETURN VARCHAR2;
    
  FUNCTION  getLowerStrSansAccents(texte VARCHAR2) RETURN VARCHAR2; 
  
  function formatesSur8Car(nb integer) return varchar2;
  
       
  procedure prv_add_categorie(
    strId categorie.cat_str_id%type,
    strIdPere categorie.cat_str_id%type,
    libelle categorie.cat_libelle%type,
    commentaires categorie.cat_commentaires%type,
    catLocal categorie.cat_local%type,
    silent number,
    catId out categorie.cat_id%type
  );
  
  procedure add_categorie(
    strId categorie.cat_str_id%type,
    strIdPere categorie.cat_str_id%type,
    libelle categorie.cat_libelle%type,
    commentaires categorie.cat_commentaires%type,
    catId out categorie.cat_id%type
  );


  procedure prv_add_critere(
          strId critere.crit_str_id%type, 
          cle critere.crit_cle%type,
          libelle critere.crit_libelle%type,           
          typeValeur critere.crit_type_valeur%type, 
          typeSAisie critere.crit_type_saisie%type, 
          isSrchDispo critere.crit_srch_dispo%type, 
          catStrId categorie.cat_str_id%type, 
          commentaire critere.crit_commentaire%type, 
          metadata critere.crit_metadata%type,
          isLocal critere.crit_local%type, 
          silent number,
          critId out critere.crit_id%type
  );
  

  
  procedure add_critere(
          strId critere.crit_str_id%type, 
          cle critere.crit_cle%type,
          libelle critere.crit_libelle%type,           
          typeValeur critere.crit_type_valeur%type, 
          typeSAisie critere.crit_type_saisie%type, 
          isSrchDispo critere.crit_srch_dispo%type, 
          catStrId categorie.cat_str_id%type, 
          commentaire critere.crit_commentaire%type, 
          metadata critere.crit_metadata%type,
          critId out critere.crit_id%type
  );



  procedure prv_add_rapport(
          strId rapport.rap_str_id%type, 
          libelle rapport.rap_libelle%type,            
          teStrId type_etat.te_str_id%type,
          isLocal rapport.rap_local%type,
          commentaire rapport.rap_commentaire%type, 
          persIdCreation rapport.pers_id_creation%type,
          url rapport.rap_url%type,
          catStrId categorie.cat_str_id%type,
          laVersion rapport.rap_version%type,
          laVersionDate rapport.rap_version_date%type,
          auteur rapport.rap_auteur%type,
          rapId out rapport.rap_id%type
  );
  
  

  procedure add_rapport_jrxml (
          strId rapport.rap_str_id%type, 
          libelle rapport.rap_libelle%type,           
          teStrId type_etat.te_str_id%type,
          isLocal rapport.rap_local%type,
          commentaire rapport.rap_commentaire%type, 
          persIdCreation rapport.pers_id_creation%type,
          url rapport.rap_url%type,
          catStrId categorie.cat_str_id%type,
          laVersion rapport.rap_version%type,
          laVersionDate rapport.rap_version_date%type,
          auteur rapport.rap_auteur%type,          
          fileName rapport_jrxml.rjr_file%type,
          versionMin rapport_jrxml.rjr_version_jr_min%type, 
          chainetexStrId varchar2,
          rapId out rapport.rap_id%type,
          rjrId out rapport_jrxml.rjr_id%type  );
  
  
  procedure add_rapport_jxls (
          strId rapport.rap_str_id%type, 
          libelle rapport.rap_libelle%type,                    
          teStrId type_etat.te_str_id%type,
          isLocal rapport.rap_local%type,
          commentaire rapport.rap_commentaire%type, 
          persIdCreation rapport.pers_id_creation%type,
          url rapport.rap_url%type,
          catStrId categorie.cat_str_id%type,
          laVersion rapport.rap_version%type,
          laVersionDate rapport.rap_version_date%type,
          auteur rapport.rap_auteur%type,          
          fileName rapport_jxls.rjl_file%type,
          sqlSelect rapport_jxls.rjl_sql%type,
          chainetexStrId varchar2,
          rapId out rapport.rap_id%type,
          rjlId out rapport_jxls.rjl_id%type  );  
  

  procedure add_categorie_to_rapport(
          rapStrId rapport.rap_str_id%type, 
          catStrid categorie.cat_str_id%type
  );
  
  procedure remove_categorie_from_rapport(
          rapStrId rapport.rap_str_id%type, 
          catStrid categorie.cat_str_id%type
  );  
  
  procedure add_critere_to_rapport(
          rapStrId rapport.rap_str_id%type, 
          critStrid critere.crit_str_id%type,
          rcrStrId rapport_critere.rcr_str_id%type,
          position rapport_critere.rcr_position%type,
          obligatoire rapport_critere.rcr_obligatoire%type,
          libelle rapport_critere.rcr_libelle%type,
          commentaire rapport_critere.rcr_commentaire%type,
          rcrId out rapport_critere.rcr_id%type
  );
  
    procedure remove_critere_from_rapport(
          rapStrId rapport.rap_str_id%type, 
          critStrid critere.crit_str_id%type
  );
  

  function isExists_RapStrid(rapStrId rapport.rap_str_id%type) return integer;
  procedure checkExists_RapStrid(rapStrId rapport.rap_str_id%type);
  procedure checkExists_catStrid(catStrId categorie.cat_str_id%type);
  procedure checkExists_critStrId(critStrId critere.crit_str_id%type);
  procedure checkExists_tfrStrId(tfrStrId type_format_rapport.tfr_str_id%type);
  
  function getRapId(rapStrId rapport.rap_str_id%type) 
    RETURN  rapport.rap_id%type;
    
  function getCatId(catStrId categorie.cat_str_id%type) 
    RETURN  categorie.cat_id%type;
    
  function getCritId(critStrId critere.crit_str_id%type) 
    RETURN  critere.crit_id%type;
    
  function getTexId(texStrId type_export.tex_str_id%type) 
    RETURN  type_export.tex_id%type;    
    
  function getTfrId(tfrStrId type_format_rapport.tfr_str_id%type) 
    RETURN  type_format_rapport.tfr_id%type;     
    
  function getNextRapCritPosition(rapId rapport.rap_id%type)
    return rapport_critere.rcr_position%type;
    
END API_REPORT;
/



CREATE OR REPLACE PACKAGE BODY CKTL_REPORT.api_report
is
   function splitatpos (pc$chaine in varchar2, pn$pos in pls_integer, pc$sep in varchar2 default ',')
      return varchar2
   is
      lc$chaine   varchar2 (32767) := pc$sep || pc$chaine;
      li$i        pls_integer;
      li$i2       pls_integer;
   begin
      li$i := instr (lc$chaine, pc$sep, 1, pn$pos);

      if li$i > 0 then
         li$i2 := instr (lc$chaine, pc$sep, 1, pn$pos + 1);

         if li$i2 = 0 then
            li$i2 := length (lc$chaine) + 1;
         end if;

         return (substr (lc$chaine, li$i + 1, li$i2 - li$i - 1));
      else
         return null;
      end if;
   end;

   function formatessur8car (nb integer)
      return varchar2
   is
   begin
      return substr (to_char (nb, 'fm00000000000000000000000000000000000000'), 31, 8);
   end;

   function getlowerstrsansaccents (texte varchar2)
      return varchar2
   is
      txt      varchar2 (4000);
      taille   number;
      car      varchar2 (4000);
      i        integer;
   begin
      txt := trim (lower (texte));
      -- Le caractere * est remplace par un souligne
      txt := replace (txt, '*', ' ');
      taille := length (txt);

      if (taille > 0) then
         for i in 1 .. taille
         loop
            car := substr (txt, i, 1);

            if ((car >= chr (224)) and (car <= chr (229))) then
               txt := replace (txt, car, 'a');
            end if;

            if (car = chr (231)) then
               txt := replace (txt, car, 'c');
            end if;

            if ((car >= chr (232)) and (car <= chr (235))) then
               txt := replace (txt, car, 'e');
            end if;

            if ((car >= chr (236)) and (car <= chr (239))) then
               txt := replace (txt, car, 'i');
            end if;

            if ((car = chr (241))) then
               txt := replace (txt, car, 'n');
            end if;

            if ((car >= chr (242)) and (car <= chr (246))) then
               txt := replace (txt, car, 'o');
            end if;

            if ((car >= chr (249)) and (car <= chr (252))) then
               txt := replace (txt, car, 'u');
            end if;

            --prise en compte du nouveau caractere
            car := substr (txt, i, 1);
         end loop;
      end if;

      --DBMS_OUTPUT.PUT_LINE('Chaine sans accents : '||txt);
      return trim (txt);
   end;

   /* Renvoie la prochaine position possible pour un critere  */
   function getnextrapcritposition (rapid rapport.rap_id%type)
      return rapport_critere.rcr_position%type
   is
      res    rapport_critere.rcr_position%type;
      flag   integer;
   begin
      select count (*)
      into   flag
      from   rapport_critere
      where  rap_id = rapid;

      if (flag = 0) then
         res := 0;
      else
         select max (rcr_position)
         into   res
         from   rapport_critere
         where  rap_id = rapid;
      end if;

      res := res + 1;
      return res;
   end;

   function getrapid (rapstrid rapport.rap_str_id%type)
      return rapport.rap_id%type
   is
      rapid   rapport.rap_id%type;
   begin
      select rap_id
      into   rapid
      from   rapport
      where  rap_str_id = rapstrid;

      return rapid;
   end getrapid;

   function getcatid (catstrid categorie.cat_str_id%type)
      return categorie.cat_id%type
   is
      catid   categorie.cat_id%type;
   begin
      select cat_id
      into   catid
      from   categorie
      where  cat_str_id = catstrid;

      return catid;
   end getcatid;

   function getcritid (critstrid critere.crit_str_id%type)
      return critere.crit_id%type
   is
      critid   critere.crit_id%type;
   begin
      select crit_id
      into   critid
      from   critere
      where  crit_str_id = critstrid;

      return critid;
   end getcritid;

   function gettexid (texstrid type_export.tex_str_id%type)
      return type_export.tex_id%type
   is
      texid   type_export.tex_id%type;
   begin
      select tex_id
      into   texid
      from   type_export
      where  tex_str_id = texstrid;

      return texid;
   end gettexid;

   function gettfrid (tfrstrid type_format_rapport.tfr_str_id%type)
      return type_format_rapport.tfr_id%type
   is
      tfrid   type_format_rapport.tfr_id%type;
   begin
      select tfr_id
      into   tfrid
      from   type_format_rapport
      where  tfr_str_id = tfrstrid;

      return tfrid;
   end gettfrid;

   function isexists_rapstrid (rapstrid rapport.rap_str_id%type)
      return integer
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   rapport
      where  rap_str_id = rapstrid;

      return flag;
   end isexists_rapstrid;

   procedure checkexists_rapstrid (rapstrid rapport.rap_str_id%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   rapport
      where  rap_str_id = rapstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre rapStrId = ' || rapstrid || ' ne correspond pas a un rapport existant');
      end if;
   end checkexists_rapstrid;

   procedure checkexists_catstrid (catstrid categorie.cat_str_id%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   categorie
      where  cat_str_id = catstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre catStrId = ' || catstrid || ' ne correspond pas a une catégorie existante');
      end if;
   end checkexists_catstrid;

   procedure checkexists_critstrid (critstrid critere.crit_str_id%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   critere
      where  crit_str_id = critstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre critStrId = ' || critstrid || ' ne correspond pas a un critère existant');
      end if;
   end checkexists_critstrid;

   procedure checkexists_tfrstrid (tfrstrid type_format_rapport.tfr_str_id%type)
   is
      flag   integer;
   begin
      select count (*)
      into   flag
      from   type_format_rapport
      where  tfr_str_id = tfrstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre tfrStrId = ' || tfrstrid || ' ne correspond pas a un tyep de format de rapport existant');
      end if;
   end checkexists_tfrstrid;

   procedure prv_add_categorie (
      strid                categorie.cat_str_id%type,
      stridpere            categorie.cat_str_id%type,
      libelle              categorie.cat_libelle%type,
      commentaires         categorie.cat_commentaires%type,
      catlocal             categorie.cat_local%type,
      silent               number,
      catid          out   categorie.cat_id%type
   )
   as
      flag         integer;
      catidpere    categorie.cat_id%type;
      niveaupere   categorie.cat_niveau%type;
   begin
      --verifier les champs obligatoires
      if (strid is null) then
         raise_application_error (-20001, 'Le parametre strid est obligatoire');
      end if;

      if (stridpere is null) then
         raise_application_error (-20001, 'Le parametre stridPere est obligatoire');
      end if;

      if (libelle is null) then
         raise_application_error (-20001, 'Le parametre libelle est obligatoire');
      end if;

      if (catlocal is null) then
         raise_application_error (-20001, 'Le parametre catLocal est obligatoire');
      end if;

      -- verifier si stridpere existe
      select count (*)
      into   flag
      from   categorie
      where  cat_str_id = stridpere;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre stridpere = ' || stridpere || ' ne correspond pas a une categorie');
      end if;

      -- verifier si str_id existe deja
      select count (*)
      into   flag
      from   categorie
      where  cat_str_id = strid;

      if (flag = 1) then
         if (silent > 0) then
            dbms_output.put_line ('>> categorie trouvee, return: ' || strid);
            return;
         else
            raise_application_error (-20001, 'La categorie = ' || strid || ' existe deja');
         end if;
      end if;

      select cat_id
      into   catidpere
      from   categorie
      where  cat_str_id = stridpere;

      select cat_niveau
      into   niveaupere
      from   categorie
      where  cat_id = catidpere;

      select categorie_seq.nextval
      into   catid
      from   dual;

      dbms_output.put_line ('>> categorie va etre inseree : ' || strid);

      insert into categorie
                  (cat_id,
                   cat_id_pere,
                   cat_niveau,
                   cat_str_id,
                   cat_libelle,
                   cat_commentaires,
                   cat_local
                  )
      values      (catid,   --CAT_ID,
                   catidpere,   --CAT_ID_PERE,
                   niveaupere + 1,   --CAT_NIVEAU,
                   strid,   --CAT_STR_ID,
                   libelle,   --CAT_LIBELLE,
                   commentaires,   --CAT_COMMENTAIRES,
                   catlocal   --CAT_LOCAL
                  );
   end prv_add_categorie;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_categorie (strid categorie.cat_str_id%type, stridpere categorie.cat_str_id%type, libelle categorie.cat_libelle%type, commentaires categorie.cat_commentaires%type, catid out categorie.cat_id%type)
   as
   begin
      prv_add_categorie (strid, stridpere, libelle, commentaires, 'O', 0, catid);
      null;
   end add_categorie;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure prv_add_critere (
      strid               critere.crit_str_id%type,
      cle                 critere.crit_cle%type,
      libelle             critere.crit_libelle%type,
      typevaleur          critere.crit_type_valeur%type,
      typesaisie          critere.crit_type_saisie%type,
      issrchdispo         critere.crit_srch_dispo%type,
      catstrid            categorie.cat_str_id%type,
      commentaire         critere.crit_commentaire%type,
      metadata            critere.crit_metadata%type,
      islocal             critere.crit_local%type,
      silent              number,
      critid        out   critere.crit_id%type
   )
   as
      flag    integer;
      catid   critere.cat_id%type;
   begin
      --verifier les champs obligatoires
      if (strid is null) then
         raise_application_error (-20001, 'Le parametre strid est obligatoire');
      end if;

      if (libelle is null) then
         raise_application_error (-20001, 'Le parametre libelle est obligatoire');
      end if;

      if (typevaleur is null) then
         raise_application_error (-20001, 'Le parametre typeValeur est obligatoire');
      end if;

      if (typesaisie is null) then
         raise_application_error (-20001, 'Le parametre typeSAisie est obligatoire');
      end if;

      if (issrchdispo is null) then
         raise_application_error (-20001, 'Le parametre isSrchDispo est obligatoire');
      end if;

      if (islocal is null) then
         raise_application_error (-20001, 'Le parametre isLocal est obligatoire');
      end if;

      catid := null;

      if (catstrid is not null) then
         -- verifier si catStrId existe
         select count (*)
         into   flag
         from   categorie
         where  cat_str_id = catstrid;

         if (flag = 0) then
            raise_application_error (-20001, 'Le parametre catStrId = ' || catstrid || ' ne correspond pas a une categorie');
         end if;

         select cat_id
         into   catid
         from   categorie
         where  cat_str_id = catstrid;
      end if;

      -- verifier si str_id existe deja
      select count (*)
      into   flag
      from   critere
      where  crit_str_id = strid;

      if (flag = 1) then
         if (silent > 0) then
            -- on met a jour avec les nouvelles valeurs
            update critere
               set crit_cle = cle,
                   crit_libelle = libelle,
                   crit_local = islocal,
                   crit_type_valeur = typevaleur,
                   crit_type_saisie = typesaisie,
                   crit_srch_dispo = issrchdispo,
                   cat_id = catid,
                   crit_commentaire = commentaire,
                   crit_metadata = metadata
             where crit_str_id = strid;

            return;
         else
            raise_application_error (-20001, 'Le critere = ' || strid || ' existe deja');
         end if;
      end if;

      select critere_seq.nextval
      into   critid
      from   dual;

      insert into critere
                  (crit_id,
                   crit_str_id,
                   crit_cle,
                   crit_libelle,
                   crit_local,
                   crit_type_valeur,
                   crit_type_saisie,
                   crit_srch_dispo,
                   cat_id,
                   crit_commentaire,
                   crit_metadata
                  )
      values      (critid,   --CRIT_ID,
                   strid,
                   cle,
                   libelle,
                   islocal,
                   typevaleur,
                   typesaisie,
                   issrchdispo,
                   catid,
                   commentaire,
                   metadata
                  );
   end prv_add_critere;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_critere (
      strid               critere.crit_str_id%type,
      cle                 critere.crit_cle%type,
      libelle             critere.crit_libelle%type,
      typevaleur          critere.crit_type_valeur%type,
      typesaisie          critere.crit_type_saisie%type,
      issrchdispo         critere.crit_srch_dispo%type,
      catstrid            categorie.cat_str_id%type,
      commentaire         critere.crit_commentaire%type,
      metadata            critere.crit_metadata%type,
      critid        out   critere.crit_id%type
   )
   as
   begin
      prv_add_critere (strid, cle, libelle, typevaleur, typesaisie, issrchdispo, catstrid, commentaire, metadata, 'O', 0, critid);
      null;
   end add_critere;

/*----------------------------------------------------------------------------*/
/* Ajoute ou met a jour le rapport */
/*----------------------------------------------------------------------------*/
   procedure prv_add_rapport (
      strid                  rapport.rap_str_id%type,
      libelle                rapport.rap_libelle%type,
      testrid                type_etat.te_str_id%type,
      islocal                rapport.rap_local%type,
      commentaire            rapport.rap_commentaire%type,
      persidcreation         rapport.pers_id_creation%type,
      url                    rapport.rap_url%type,
      catstrid               categorie.cat_str_id%type,
      laversion              rapport.rap_version%type,
      laversiondate          rapport.rap_version_date%type,
      auteur                 rapport.rap_auteur%type,
      rapid            out   rapport.rap_id%type
   )
   as
      flag    integer;
      teid    rapport.te_id%type;
      catid   categorie.cat_id%type;
   begin
      --verifier les champs obligatoires
      if (strid is null) then
         raise_application_error (-20001, 'Le parametre strid est obligatoire');
      end if;

      if (libelle is null) then
         raise_application_error (-20001, 'Le parametre libelle est obligatoire');
      end if;

      if (testrid is null) then
         raise_application_error (-20001, 'Le parametre teStrId est obligatoire');
      end if;

      if (islocal is null) then
         raise_application_error (-20001, 'Le parametre isLocal est obligatoire');
      end if;

      select count (*)
      into   flag
      from   type_etat
      where  te_str_id = testrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le type etat ' || testrid || ' n''existe pas dans la table type_etat');
      end if;

      if (catstrid is not null) then
         select count (*)
         into   flag
         from   categorie
         where  cat_str_id = catstrid;

         if (flag = 0) then
            raise_application_error (-20001, 'La categorie ' || catstrid || ' n''est pas reconnue');
         end if;
      end if;

      -- verifier si str_id existe deja
      select count (*)
      into   flag
      from   rapport
      where  rap_str_id = strid;

      if (flag = 1) then
         select rap_id
         into   rapid
         from   rapport
         where  rap_str_id = strid;

         update rapport
            set rap_libelle = libelle,
                rap_local = islocal,
                rap_commentaire = commentaire,
                pers_id_modification = persidcreation,
                rap_url = url,
                rap_version = laversion,
                rap_version_date = laversiondate,
                rap_auteur = auteur
          where rap_id = rapid;
      else
         select te_id
         into   teid
         from   type_etat
         where  te_str_id = testrid;

         select rapport_seq.nextval
         into   rapid
         from   dual;

         insert into rapport
                     (rap_id,
                      --TFR_ID,
                      te_id,
                      rap_str_id,
                      rap_libelle,
                      rap_local,
                      rap_commentaire,
                      pers_id_creation,
                      pers_id_modification,
                      pers_id_publication,
                      date_creation,
                      date_publication,
                      rap_url,
                      rap_version,
                      rap_version_date,
                      rap_auteur
                     )
         values      (rapid,
                      --tfrId,
                      teid,
                      strid,
                      libelle,
                      islocal,
                      commentaire,
                      persidcreation,
                      persidcreation,
                      null,
                      sysdate,
                      null,
                      url,
                      laversion,
                      laversiondate,
                      auteur
                     );
      --raise_application_error (-20001,'Le rapport = '|| strid||' existe deja');
      end if;

      if (catstrid is not null) then
         add_categorie_to_rapport (strid, catstrid);
      end if;
   end prv_add_rapport;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_rapport_jrxml (
      strid                  rapport.rap_str_id%type,
      libelle                rapport.rap_libelle%type,
      testrid                type_etat.te_str_id%type,
      islocal                rapport.rap_local%type,
      commentaire            rapport.rap_commentaire%type,
      persidcreation         rapport.pers_id_creation%type,
      url                    rapport.rap_url%type,
      catstrid               categorie.cat_str_id%type,
      laversion              rapport.rap_version%type,
      laversiondate          rapport.rap_version_date%type,
      auteur                 rapport.rap_auteur%type,
      filename               rapport_jrxml.rjr_file%type,
      versionmin             rapport_jrxml.rjr_version_jr_min%type,
      chainetexstrid         varchar2,
      rapid            out   rapport.rap_id%type,
      rjrid            out   rapport_jrxml.rjr_id%type
   )
   as
      i               integer;
      flag            integer;
      rapportexists   integer;
      texstrid        varchar2 (100);
      texid           type_export.tex_id%type;
      tfrid           type_format_rapport.tfr_id%type;
      tfrstrid        type_format_rapport.tfr_str_id%type;
   begin
      tfrstrid := 'JRXML';
      checkexists_tfrstrid (tfrstrid);
      tfrid := gettfrid (tfrstrid);
      --rapportExists := 0;
      -- ajoute ou met a jour le rapport
      prv_add_rapport (strid, libelle, testrid, islocal, commentaire, persidcreation, url, catstrid, laversion, laversiondate, auteur, rapid);

      --select rap_id into rapId from  rapport where rap_str_id=strId;
      select count (*)
      into   flag
      from   rapport_jrxml
      where  rap_id = rapid;

      if (flag = 0) then
         --raise_application_error (-20001,'Le rapport JRXML = '|| strid||' existe deja');
         select rapport_jrxml_seq.nextval
         into   rjrid
         from   dual;

         insert into rapport_jrxml
                     (rjr_id,
                      tfr_id,
                      rap_id,
                      rjr_file,
                      rjr_version_jr_min
                     )
         values      (rjrid,
                      tfrid,
                      rapid,
                      filename,
                      versionmin
                     );
      else
         select max (rjr_id)
         into   rjrid
         from   rapport_jrxml
         where  rap_id = rapid;

         -- mise a jour
         update rapport_jrxml
            set rjr_file = filename,
                rjr_version_jr_min = versionmin
          where rjr_id = rjrid;
      end if;

      i := 1;
      texstrid := splitatpos (chainetexstrid, i, ',');

      while (texstrid is not null)
      loop
         -- verifier que export autorise pour format
         texid := gettexid (trim (texstrid));

         select count (*)
         into   flag
         from   type_format_export
         where  tex_id = texid and tfr_id = tfrid;

         if (flag > 0) then
            select count (*)
            into   flag
            from   rapport_jrxml_tex
            where  rjr_id = rjrid and tex_id = texid;

            if (flag = 0) then
               insert into rapport_jrxml_tex
               values      (rapport_jrxml_tex_seq.nextval,
                            rjrid,
                            texid
                           );
            end if;
         else
            dbms_output.put_line (texstrid || ' non ajoute');
         end if;

         i := i + 1;
         texstrid := splitatpos (chainetexstrid, i, ',');
      end loop;
   end;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_rapport_jxls (
      strid                  rapport.rap_str_id%type,
      libelle                rapport.rap_libelle%type,
      testrid                type_etat.te_str_id%type,
      islocal                rapport.rap_local%type,
      commentaire            rapport.rap_commentaire%type,
      persidcreation         rapport.pers_id_creation%type,
      url                    rapport.rap_url%type,
      catstrid               categorie.cat_str_id%type,
      laversion              rapport.rap_version%type,
      laversiondate          rapport.rap_version_date%type,
      auteur                 rapport.rap_auteur%type,
      filename               rapport_jxls.rjl_file%type,
      sqlselect              rapport_jxls.rjl_sql%type,
      chainetexstrid         varchar2,
      rapid            out   rapport.rap_id%type,
      rjlid            out   rapport_jxls.rjl_id%type
   )
   as
      i               integer;
      flag            integer;
      texstrid        varchar2 (100);
      texid           type_export.tex_id%type;
      tfrstrid        type_format_rapport.tfr_str_id%type;
      tfrid           type_format_rapport.tfr_id%type;
      rapportexists   integer;
   begin
      tfrstrid := 'JXLS';
      checkexists_tfrstrid (tfrstrid);
      tfrid := gettfrid (tfrstrid);
      --rapportExists := 0;
      prv_add_rapport (strid, libelle, testrid, islocal, commentaire, persidcreation, url, catstrid, laversion, laversiondate, auteur, rapid);

      select count (*)
      into   flag
      from   rapport_jxls
      where  rap_id = rapid;

      if (flag = 0) then
         select rapport_jxls_seq.nextval
         into   rjlid
         from   dual;

         insert into rapport_jxls
                     (rjl_id,
                      tfr_id,
                      rap_id,
                      rjl_file,
                      rjl_sql
                     )
         values      (rjlid,
                      tfrid,
                      rapid,
                      filename,
                      sqlselect
                     );
      --raise_application_error (-20001,'Le rapport JXLS = '|| strid||' existe deja');
      else
         select max (rjl_id)
         into   rjlid
         from   rapport_jxls
         where  rap_id = rapid;

         -- mise a jour
         update rapport_jxls
            set rjl_file = filename,
                rjl_sql = sqlselect
          where rjl_id = rjlid;
      end if;

      i := 1;
      texstrid := splitatpos (chainetexstrid, i, ',');

      while (texstrid is not null)
      loop
         -- verifier que export autorise pour format
         texid := gettexid (trim (texstrid));

         select count (*)
         into   flag
         from   type_format_export
         where  tex_id = texid and tfr_id = gettfrid (tfrstrid);

         if (flag > 0) then
            select count (*)
            into   flag
            from   rapport_jxls_tex
            where  rjl_id = rjlid and tex_id = texid;

            if (flag = 0) then
               insert into rapport_jxls_tex
               values      (rapport_jxls_tex_seq.nextval,
                            rjlid,
                            texid
                           );
            end if;
         else
            dbms_output.put_line (texstrid || ' non ajoute');
         end if;

         i := i + 1;
         texstrid := splitatpos (chainetexstrid, i, ',');
      end loop;
   end;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_categorie_to_rapport (rapstrid rapport.rap_str_id%type, catstrid categorie.cat_str_id%type)
   as
      rapid   rapport.rap_id%type;
      catid   categorie.cat_id%type;
      flag    integer;
      racid   rapport_categorie.rac_id%type;
   begin
      select count (*)
      into   flag
      from   categorie
      where  cat_str_id = catstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre catStrId = ' || catstrid || ' ne correspond pas a une categorie');
      end if;

      select count (*)
      into   flag
      from   rapport
      where  rap_str_id = rapstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre rapStrId = ' || rapstrid || ' ne correspond pas a un rapport existant');
      end if;

      select rap_id
      into   rapid
      from   rapport
      where  rap_str_id = rapstrid;

      select cat_id
      into   catid
      from   categorie
      where  cat_str_id = catstrid;

      select count (*)
      into   flag
      from   rapport_categorie
      where  cat_id = catid and rap_id = rapid;

      if (flag = 0) then
         select rapport_categorie_seq.nextval
         into   racid
         from   dual;

         insert into rapport_categorie
                     (rac_id,
                      rap_id,
                      cat_id
                     )
         values      (racid,
                      rapid,
                      catid
                     );
      end if;
   end add_categorie_to_rapport;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure remove_categorie_from_rapport (rapstrid rapport.rap_str_id%type, catstrid categorie.cat_str_id%type)
   as
      rapid   rapport.rap_id%type;
      catid   categorie.cat_id%type;
      flag    integer;
      racid   rapport_categorie.rac_id%type;
   begin
      select count (*)
      into   flag
      from   categorie
      where  cat_str_id = catstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre catStrId = ' || catstrid || ' ne correspond pas a une categorie');
      end if;

      select count (*)
      into   flag
      from   rapport
      where  rap_str_id = rapstrid;

      if (flag = 0) then
         raise_application_error (-20001, 'Le parametre rapStrId = ' || rapstrid || ' ne correspond pas a un rapport existant');
      end if;

      select rap_id
      into   rapid
      from   rapport
      where  rap_str_id = rapstrid;

      select cat_id
      into   catid
      from   categorie
      where  cat_str_id = catstrid;

      select count (*)
      into   flag
      from   rapport_categorie
      where  cat_id = catid and rap_id = rapid;

      if (flag = 1) then
         delete from rapport_categorie
               where cat_id = catid and rap_id = rapid;
      end if;
   end remove_categorie_from_rapport;

/*----------------------------------------------------------------------------*/
/*----------------------------------------------------------------------------*/
   procedure add_critere_to_rapport (
      rapstrid            rapport.rap_str_id%type,
      critstrid           critere.crit_str_id%type,
      rcrstrid            rapport_critere.rcr_str_id%type,
      position            rapport_critere.rcr_position%type,
      obligatoire         rapport_critere.rcr_obligatoire%type,
      libelle             rapport_critere.rcr_libelle%type,
      commentaire         rapport_critere.rcr_commentaire%type,
      rcrid         out   rapport_critere.rcr_id%type
   )
   as
      rapid    rapport.rap_id%type;
      critid   critere.crit_id%type;
      flag     integer;
   begin
      checkexists_rapstrid (rapstrid);
      checkexists_critstrid (critstrid);
      rapid := getrapid (rapstrid);
      critid := getcritid (critstrid);

      select count (*)
      into   flag
      from   rapport_critere
      where  rap_id = rapid and crit_id = critid;

      if (flag > 0) then
         return;
      --raise_application_error (-20001,'Le critere critStrid='|| critStrId||' est déjà associé au rapport rapStrId = '|| rapStrId||'.');
      end if;

      if (position is null) then
         raise_application_error (-20001, 'La position est obligatoire.');
      end if;

      if (obligatoire is null or obligatoire not in ('O', 'N')) then
         raise_application_error (-20001, 'Le parametre obligatoire doit etre O ou N.');
      end if;

      select count (*)
      into   flag
      from   rapport_critere
      where  rap_id = rapid and rcr_position = position;

      if (flag > 0) then
         raise_application_error (-20001, 'La position ' || position || ' est déjà utilisée.');
      end if;

      select rapport_critere_seq.nextval
      into   rcrid
      from   dual;

      insert into rapport_critere
                  (rcr_id,
                   rap_id,
                   crit_id,
                   rcr_position,
                   rcr_obligatoire,
                   rcr_str_id,
                   rcr_libelle,
                   rcr_commentaire
                  )
      values      (rcrid,
                   rapid,
                   critid,
                   position,
                   obligatoire,
                   rcrstrid,
                   libelle,
                   commentaire
                  );
   end add_critere_to_rapport;
   
   
   
   
     
    procedure remove_critere_from_rapport(
          rapStrId rapport.rap_str_id%type, 
          critStrid critere.crit_str_id%type
  ) is
        rapid    rapport.rap_id%type;
      critid   critere.crit_id%type;
  begin
        rapid := getrapid (rapstrid);
      critid := getcritid (critstrid);
      delete from pers_rapport_pret_crit where rcr_id in (select rcr_id from rapport_critere where rap_id=rapid and crit_id=critid);
      delete from rapport_critere where rap_id=rapid and crit_id=critid;
  end remove_critere_from_rapport;
   
  
  
  
  
  
end api_report;
/


