/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server;

import org.cocktail.fwkcktlreport.server.files.CktlReportFileAction;
import org.cocktail.fwkcktlreport.server.metier.EORapport;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class CktlReportRapportInstallCtrl {
	public static final String ACTION_IF_PRESENT_RENAME = "Renommer automatiquement le nouveau rapport";
	public static final String ACTION_IF_PRESENT_REPLACE = "Remplacer le rapport existant par le nouveau (et renommer l'ancien)";
	public static final String ACTION_IF_PRESENT_NOTHING = "Ne rien faire";

	/**
	 * Enregistre un rapport (gère notamment les doublons ainsi que les fichiers/répertoires à modifier).
	 * 
	 * @param rap
	 * @param actionIfPresent
	 * @param appUser
	 * @param fileActions Les actions sur le systeme de fichirr à executer apres enregistrement des données
	 * @param touchedRapports Si renseigné, est mis à jour avec les rapports modifiés
	 * @throws Exception
	 */
	public static void saveRapport(EOEditingContext edc, EORapport rap, String actionIfPresent, FwkCktlReportApplicationUser appUser, NSMutableArray<CktlReportFileAction> fileActions, NSMutableArray<EORapport> touchedRapports) throws Exception {

		//verifier si rapport avec meme strId présent	
		NSArray<EORapport> doublons = EORapport.getDoublonsStrId(edc, rap);
		if (doublons.count() > 0) {
			rap.setAutoPerformDirectoryAction(false);

			//suivant les options, on gere le truc
			EORapport doublon = doublons.objectAtIndex(0);
			if (CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_NOTHING.equals(actionIfPresent)) {
				throw new Exception("Rapport identifié par " + rap.rapStrId() + " déjà existant.");
			}
			if (CktlReportRapportInstallCtrl.ACTION_IF_PRESENT_REPLACE.equals(actionIfPresent)) {

				//archiver l'ancien
				EORapport oldRapport = EORapport.duplicate(doublon, doublon.getNewArchiveName(), appUser);
				oldRapport.setAutoPerformDirectoryAction(false);
				oldRapport.setIsDesactive(Boolean.TRUE);
				oldRapport.setRapLibelle(doublon.rapLibelle() + " (ancien)");
				fileActions.addObject(CktlReportFileAction.renameAllAction(appUser, doublon.getActualReportDirectoryName(), oldRapport.getNewAttachedDirectoryName()));

				oldRapport.validateObjectMetier();
				oldRapport.validateForInsert();

				//remplacer l'ancien par le nouveau
				doublon.updateFromAnotherRapport(rap);
				doublon.setAutoPerformDirectoryAction(false);
				fileActions.addObject(CktlReportFileAction.createAction(appUser, doublon.getActualReportDirectoryName()));
				EORapport.supprimer(rap);
				doublon.editingContext().deleteObject(rap);
				doublon.validateObjectMetier();
				touchedRapports.addObject(oldRapport);
				touchedRapports.addObject(doublon);
			}
			else {
				rap.setRapStrId(rap.createNewRapStrId());
				rap.setRapLibelle(rap.createNewRapLibelle());
				fileActions.addObject(CktlReportFileAction.createAction(appUser, rap.getNewAttachedDirectoryName()));
				touchedRapports.addObject(rap);
			}
		}

	}
}
