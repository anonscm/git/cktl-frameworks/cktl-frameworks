/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCritere.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOCritere extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOCritere.class);

	public static final String ENTITY_NAME = "FwkCktlReport_Critere";
	public static final String ENTITY_TABLE_NAME = "cktl_report.CRITERE";


// Attribute Keys
  public static final ERXKey<String> CRIT_CLE = new ERXKey<String>("critCle");
  public static final ERXKey<String> CRIT_COMMENTAIRE = new ERXKey<String>("critCommentaire");
  public static final ERXKey<String> CRIT_LIBELLE = new ERXKey<String>("critLibelle");
  public static final ERXKey<String> CRIT_LOCAL = new ERXKey<String>("critLocal");
  public static final ERXKey<String> CRIT_METADATA = new ERXKey<String>("critMetadata");
  public static final ERXKey<String> CRIT_SRCH_DISPO = new ERXKey<String>("critSrchDispo");
  public static final ERXKey<String> CRIT_STR_ID = new ERXKey<String>("critStrId");
  public static final ERXKey<String> CRIT_TYPE_SAISIE = new ERXKey<String>("critTypeSaisie");
  public static final ERXKey<String> CRIT_TYPE_VALEUR = new ERXKey<String>("critTypeValeur");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie> TO_CATEGORIE = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie>("toCategorie");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "critId";

	public static final String CRIT_CLE_KEY = "critCle";
	public static final String CRIT_COMMENTAIRE_KEY = "critCommentaire";
	public static final String CRIT_LIBELLE_KEY = "critLibelle";
	public static final String CRIT_LOCAL_KEY = "critLocal";
	public static final String CRIT_METADATA_KEY = "critMetadata";
	public static final String CRIT_SRCH_DISPO_KEY = "critSrchDispo";
	public static final String CRIT_STR_ID_KEY = "critStrId";
	public static final String CRIT_TYPE_SAISIE_KEY = "critTypeSaisie";
	public static final String CRIT_TYPE_VALEUR_KEY = "critTypeValeur";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String CRIT_ID_KEY = "critId";

//Colonnes dans la base de donnees
	public static final String CRIT_CLE_COLKEY = "CRIT_CLE";
	public static final String CRIT_COMMENTAIRE_COLKEY = "CRIT_COMMENTAIRE";
	public static final String CRIT_LIBELLE_COLKEY = "CRIT_LIBELLE";
	public static final String CRIT_LOCAL_COLKEY = "CRIT_LOCAL";
	public static final String CRIT_METADATA_COLKEY = "CRIT_METADATA";
	public static final String CRIT_SRCH_DISPO_COLKEY = "CRIT_SRCH_DISPO";
	public static final String CRIT_STR_ID_COLKEY = "CRIT_STR_ID";
	public static final String CRIT_TYPE_SAISIE_COLKEY = "CRIT_TYPE_SAISIE";
	public static final String CRIT_TYPE_VALEUR_COLKEY = "CRIT_TYPE_VALEUR";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CRIT_ID_COLKEY = "CRIT_ID";


	// Relationships
	public static final String TO_CATEGORIE_KEY = "toCategorie";



	// Accessors methods
  public String critCle() {
    return (String) storedValueForKey(CRIT_CLE_KEY);
  }

  public void setCritCle(String value) {
    takeStoredValueForKey(value, CRIT_CLE_KEY);
  }

  public String critCommentaire() {
    return (String) storedValueForKey(CRIT_COMMENTAIRE_KEY);
  }

  public void setCritCommentaire(String value) {
    takeStoredValueForKey(value, CRIT_COMMENTAIRE_KEY);
  }

  public String critLibelle() {
    return (String) storedValueForKey(CRIT_LIBELLE_KEY);
  }

  public void setCritLibelle(String value) {
    takeStoredValueForKey(value, CRIT_LIBELLE_KEY);
  }

  public String critLocal() {
    return (String) storedValueForKey(CRIT_LOCAL_KEY);
  }

  public void setCritLocal(String value) {
    takeStoredValueForKey(value, CRIT_LOCAL_KEY);
  }

  public String critMetadata() {
    return (String) storedValueForKey(CRIT_METADATA_KEY);
  }

  public void setCritMetadata(String value) {
    takeStoredValueForKey(value, CRIT_METADATA_KEY);
  }

  public String critSrchDispo() {
    return (String) storedValueForKey(CRIT_SRCH_DISPO_KEY);
  }

  public void setCritSrchDispo(String value) {
    takeStoredValueForKey(value, CRIT_SRCH_DISPO_KEY);
  }

  public String critStrId() {
    return (String) storedValueForKey(CRIT_STR_ID_KEY);
  }

  public void setCritStrId(String value) {
    takeStoredValueForKey(value, CRIT_STR_ID_KEY);
  }

  public String critTypeSaisie() {
    return (String) storedValueForKey(CRIT_TYPE_SAISIE_KEY);
  }

  public void setCritTypeSaisie(String value) {
    takeStoredValueForKey(value, CRIT_TYPE_SAISIE_KEY);
  }

  public String critTypeValeur() {
    return (String) storedValueForKey(CRIT_TYPE_VALEUR_KEY);
  }

  public void setCritTypeValeur(String value) {
    takeStoredValueForKey(value, CRIT_TYPE_VALEUR_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOCategorie toCategorie() {
    return (org.cocktail.fwkcktlreport.server.metier.EOCategorie)storedValueForKey(TO_CATEGORIE_KEY);
  }

  public void setToCategorieRelationship(org.cocktail.fwkcktlreport.server.metier.EOCategorie value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOCategorie oldValue = toCategorie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCritere avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCritere createEOCritere(EOEditingContext editingContext, String critCle
, String critLibelle
, String critLocal
, String critSrchDispo
, String critStrId
, String critTypeSaisie
, String critTypeValeur
			) {
    EOCritere eo = (EOCritere) createAndInsertInstance(editingContext, _EOCritere.ENTITY_NAME);    
		eo.setCritCle(critCle);
		eo.setCritLibelle(critLibelle);
		eo.setCritLocal(critLocal);
		eo.setCritSrchDispo(critSrchDispo);
		eo.setCritStrId(critStrId);
		eo.setCritTypeSaisie(critTypeSaisie);
		eo.setCritTypeValeur(critTypeValeur);
    return eo;
  }

  
	  public EOCritere localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCritere)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCritere creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCritere creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCritere object = (EOCritere)createAndInsertInstance(editingContext, _EOCritere.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCritere localInstanceIn(EOEditingContext editingContext, EOCritere eo) {
    EOCritere localInstance = (eo == null) ? null : (EOCritere)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCritere#localInstanceIn a la place.
   */
	public static EOCritere localInstanceOf(EOEditingContext editingContext, EOCritere eo) {
		return EOCritere.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCritere fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCritere fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCritere> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCritere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCritere)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCritere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCritere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCritere> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCritere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCritere)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCritere fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCritere eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCritere ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCritere fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
