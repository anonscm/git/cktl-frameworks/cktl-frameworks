/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.util.Date;

import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOPersRapportHisto extends _EOPersRapportHisto {
	public static final EOSortOrdering SORT_PRH_START_TIME_DESC = EOSortOrdering.sortOrderingWithKey(EOPersRapportHisto.PRH_START_TIME_KEY, EOSortOrdering.CompareDescending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_PRH_START_TIME_DESC = new NSArray<EOSortOrdering>(new EOSortOrdering[] {
			SORT_PRH_START_TIME_DESC
	});
	public static final String STATUS_OK = "OK";
	public static final String STATUS_ANNULE = "ANNULE";
	public static final String STATUS_ERREUR = "ERREUR";

	public EOPersRapportHisto() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public static NSArray<EOPersRapportHisto> fetchAllRapportHistoPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, int fetchLimit) {
		EOQualifier qual = ERXQ.equals(EOPersRapportHisto.PERS_ID_KEY, appUser.getPersId());
		return fetchAll(edc, qual, SORT_ARRAY_PRH_START_TIME_DESC, fetchLimit);
	}

	public static NSArray<EOPersRapportHisto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int fetchLimit) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setFetchLimit(fetchLimit);
		//fetchSpec.setUsesDistinct(distinct);
		NSArray<EOPersRapportHisto> eoObjects = (NSArray<EOPersRapportHisto>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	public void setPrhEndTime_now() {
		setPrhEndTime(new NSTimestamp(new Date()));
	}

	public void setPrhStartTime_now() {
		setPrhStartTime(new NSTimestamp(new Date()));
	}

}
