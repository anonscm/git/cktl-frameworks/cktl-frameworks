/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORapport.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EORapport extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EORapport.class);

	public static final String ENTITY_NAME = "FwkCktlReport_Rapport";
	public static final String ENTITY_TABLE_NAME = "cktl_report.RAPPORT";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_PUBLICATION = new ERXKey<NSTimestamp>("datePublication");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> PERS_ID_PUBLICATION = new ERXKey<Integer>("persIdPublication");
  public static final ERXKey<String> RAP_AUTEUR = new ERXKey<String>("rapAuteur");
  public static final ERXKey<String> RAP_COMMENTAIRE = new ERXKey<String>("rapCommentaire");
  public static final ERXKey<String> RAP_LIBELLE = new ERXKey<String>("rapLibelle");
  public static final ERXKey<String> RAP_LOCAL = new ERXKey<String>("rapLocal");
  public static final ERXKey<String> RAP_STR_ID = new ERXKey<String>("rapStrId");
  public static final ERXKey<String> RAP_URL = new ERXKey<String>("rapUrl");
  public static final ERXKey<String> RAP_VERSION = new ERXKey<String>("rapVersion");
  public static final ERXKey<NSTimestamp> RAP_VERSION_DATE = new ERXKey<NSTimestamp>("rapVersionDate");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> TO_PERS_RAPPORT_FAVORISS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris>("toPersRapportFavoriss");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> TO_PERS_RAPPORT_HISTOS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto>("toPersRapportHistos");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> TO_PERS_RAPPORT_PRETS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>("toPersRapportPrets");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> TO_RAPPORT_CATEGORIES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>("toRapportCategories");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> TO_RAPPORT_CRITERES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCritere>("toRapportCriteres");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> TO_RAPPORT_JRXMLS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml>("toRapportJrxmls");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> TO_RAPPORT_JXLSS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJxls>("toRapportJxlss");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeEtat> TO_TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeEtat>("toTypeEtat");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rapId";

	public static final String DATE_CREATION_KEY = "dateCreation";
	public static final String DATE_PUBLICATION_KEY = "datePublication";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PERS_ID_PUBLICATION_KEY = "persIdPublication";
	public static final String RAP_AUTEUR_KEY = "rapAuteur";
	public static final String RAP_COMMENTAIRE_KEY = "rapCommentaire";
	public static final String RAP_LIBELLE_KEY = "rapLibelle";
	public static final String RAP_LOCAL_KEY = "rapLocal";
	public static final String RAP_STR_ID_KEY = "rapStrId";
	public static final String RAP_URL_KEY = "rapUrl";
	public static final String RAP_VERSION_KEY = "rapVersion";
	public static final String RAP_VERSION_DATE_KEY = "rapVersionDate";

// Attributs non visibles
	public static final String RAP_ID_KEY = "rapId";
	public static final String TE_ID_KEY = "teId";

//Colonnes dans la base de donnees
	public static final String DATE_CREATION_COLKEY = "DATE_CREATION";
	public static final String DATE_PUBLICATION_COLKEY = "DATE_PUBLICATION";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PERS_ID_PUBLICATION_COLKEY = "PERS_ID_PUBLICATION";
	public static final String RAP_AUTEUR_COLKEY = "RAP_AUTEUR";
	public static final String RAP_COMMENTAIRE_COLKEY = "RAP_COMMENTAIRE";
	public static final String RAP_LIBELLE_COLKEY = "RAP_LIBELLE";
	public static final String RAP_LOCAL_COLKEY = "RAP_LOCAL";
	public static final String RAP_STR_ID_COLKEY = "RAP_STR_ID";
	public static final String RAP_URL_COLKEY = "RAP_URL";
	public static final String RAP_VERSION_COLKEY = "RAP_VERSION";
	public static final String RAP_VERSION_DATE_COLKEY = "RAP_VERSION_DATE";

	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String TE_ID_COLKEY = "TE_ID";


	// Relationships
	public static final String TO_PERS_RAPPORT_FAVORISS_KEY = "toPersRapportFavoriss";
	public static final String TO_PERS_RAPPORT_HISTOS_KEY = "toPersRapportHistos";
	public static final String TO_PERS_RAPPORT_PRETS_KEY = "toPersRapportPrets";
	public static final String TO_RAPPORT_CATEGORIES_KEY = "toRapportCategories";
	public static final String TO_RAPPORT_CRITERES_KEY = "toRapportCriteres";
	public static final String TO_RAPPORT_JRXMLS_KEY = "toRapportJrxmls";
	public static final String TO_RAPPORT_JXLSS_KEY = "toRapportJxlss";
	public static final String TO_TYPE_ETAT_KEY = "toTypeEtat";



	// Accessors methods
  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_CREATION_KEY);
  }

  public NSTimestamp datePublication() {
    return (NSTimestamp) storedValueForKey(DATE_PUBLICATION_KEY);
  }

  public void setDatePublication(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PUBLICATION_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Integer persIdPublication() {
    return (Integer) storedValueForKey(PERS_ID_PUBLICATION_KEY);
  }

  public void setPersIdPublication(Integer value) {
    takeStoredValueForKey(value, PERS_ID_PUBLICATION_KEY);
  }

  public String rapAuteur() {
    return (String) storedValueForKey(RAP_AUTEUR_KEY);
  }

  public void setRapAuteur(String value) {
    takeStoredValueForKey(value, RAP_AUTEUR_KEY);
  }

  public String rapCommentaire() {
    return (String) storedValueForKey(RAP_COMMENTAIRE_KEY);
  }

  public void setRapCommentaire(String value) {
    takeStoredValueForKey(value, RAP_COMMENTAIRE_KEY);
  }

  public String rapLibelle() {
    return (String) storedValueForKey(RAP_LIBELLE_KEY);
  }

  public void setRapLibelle(String value) {
    takeStoredValueForKey(value, RAP_LIBELLE_KEY);
  }

  public String rapLocal() {
    return (String) storedValueForKey(RAP_LOCAL_KEY);
  }

  public void setRapLocal(String value) {
    takeStoredValueForKey(value, RAP_LOCAL_KEY);
  }

  public String rapStrId() {
    return (String) storedValueForKey(RAP_STR_ID_KEY);
  }

  public void setRapStrId(String value) {
    takeStoredValueForKey(value, RAP_STR_ID_KEY);
  }

  public String rapUrl() {
    return (String) storedValueForKey(RAP_URL_KEY);
  }

  public void setRapUrl(String value) {
    takeStoredValueForKey(value, RAP_URL_KEY);
  }

  public String rapVersion() {
    return (String) storedValueForKey(RAP_VERSION_KEY);
  }

  public void setRapVersion(String value) {
    takeStoredValueForKey(value, RAP_VERSION_KEY);
  }

  public NSTimestamp rapVersionDate() {
    return (NSTimestamp) storedValueForKey(RAP_VERSION_DATE_KEY);
  }

  public void setRapVersionDate(NSTimestamp value) {
    takeStoredValueForKey(value, RAP_VERSION_DATE_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOTypeEtat toTypeEtat() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeEtat)storedValueForKey(TO_TYPE_ETAT_KEY);
  }

  public void setToTypeEtatRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeEtat oldValue = toTypeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ETAT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> toPersRapportFavoriss() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris>)storedValueForKey(TO_PERS_RAPPORT_FAVORISS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> toPersRapportFavoriss(EOQualifier qualifier) {
    return toPersRapportFavoriss(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> toPersRapportFavoriss(EOQualifier qualifier, boolean fetch) {
    return toPersRapportFavoriss(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> toPersRapportFavoriss(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportFavoriss();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportFavorissRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_FAVORISS_KEY);
  }

  public void removeFromToPersRapportFavorissRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_FAVORISS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris createToPersRapportFavorissRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportFavoris");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_FAVORISS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris) eo;
  }

  public void deleteToPersRapportFavorissRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_FAVORISS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportFavorissRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportFavoris> objects = toPersRapportFavoriss().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportFavorissRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> toPersRapportHistos() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto>)storedValueForKey(TO_PERS_RAPPORT_HISTOS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> toPersRapportHistos(EOQualifier qualifier) {
    return toPersRapportHistos(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> toPersRapportHistos(EOQualifier qualifier, boolean fetch) {
    return toPersRapportHistos(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> toPersRapportHistos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportHistos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportHistosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_HISTOS_KEY);
  }

  public void removeFromToPersRapportHistosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_HISTOS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto createToPersRapportHistosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportHisto");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_HISTOS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto) eo;
  }

  public void deleteToPersRapportHistosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_HISTOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportHistosRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> objects = toPersRapportHistos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportHistosRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> toPersRapportPrets() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>)storedValueForKey(TO_PERS_RAPPORT_PRETS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> toPersRapportPrets(EOQualifier qualifier) {
    return toPersRapportPrets(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> toPersRapportPrets(EOQualifier qualifier, boolean fetch) {
    return toPersRapportPrets(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> toPersRapportPrets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportPrets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportPretsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRETS_KEY);
  }

  public void removeFromToPersRapportPretsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRETS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret createToPersRapportPretsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportPret");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_PRETS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret) eo;
  }

  public void deleteToPersRapportPretsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportPretsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> objects = toPersRapportPrets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportPretsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)storedValueForKey(TO_RAPPORT_CATEGORIES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier) {
    return toRapportCategories(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier, boolean fetch) {
    return toRapportCategories(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportCategorie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportCategories();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
  }

  public void removeFromToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportCategorie createToRapportCategoriesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportCategorie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_CATEGORIES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportCategorie) eo;
  }

  public void deleteToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportCategoriesRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> objects = toRapportCategories().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportCategoriesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> toRapportCriteres() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere>)storedValueForKey(TO_RAPPORT_CRITERES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> toRapportCriteres(EOQualifier qualifier) {
    return toRapportCriteres(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> toRapportCriteres(EOQualifier qualifier, boolean fetch) {
    return toRapportCriteres(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> toRapportCriteres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportCritere.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportCritere.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportCriteres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCritere object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CRITERES_KEY);
  }

  public void removeFromToRapportCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCritere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CRITERES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportCritere createToRapportCriteresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportCritere");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_CRITERES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportCritere) eo;
  }

  public void deleteToRapportCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCritere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CRITERES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportCriteresRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> objects = toRapportCriteres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportCriteresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> toRapportJrxmls() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml>)storedValueForKey(TO_RAPPORT_JRXMLS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> toRapportJrxmls(EOQualifier qualifier) {
    return toRapportJrxmls(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> toRapportJrxmls(EOQualifier qualifier, boolean fetch) {
    return toRapportJrxmls(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> toRapportJrxmls(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportJrxml.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportJrxml.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportJrxmls();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportJrxmlsRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxml object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXMLS_KEY);
  }

  public void removeFromToRapportJrxmlsRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxml object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXMLS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportJrxml createToRapportJrxmlsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportJrxml");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_JRXMLS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportJrxml) eo;
  }

  public void deleteToRapportJrxmlsRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxml object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXMLS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportJrxmlsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> objects = toRapportJrxmls().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportJrxmlsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> toRapportJxlss() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls>)storedValueForKey(TO_RAPPORT_JXLSS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> toRapportJxlss(EOQualifier qualifier) {
    return toRapportJxlss(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> toRapportJxlss(EOQualifier qualifier, boolean fetch) {
    return toRapportJxlss(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> toRapportJxlss(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportJxls.TO_RAPPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportJxls.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportJxlss();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportJxlssRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxls object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLSS_KEY);
  }

  public void removeFromToRapportJxlssRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxls object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLSS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportJxls createToRapportJxlssRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportJxls");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_JXLSS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportJxls) eo;
  }

  public void deleteToRapportJxlssRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxls object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLSS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportJxlssRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> objects = toRapportJxlss().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportJxlssRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORapport avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORapport createEORapport(EOEditingContext editingContext, NSTimestamp dateCreation
, String rapLibelle
, String rapLocal
, String rapStrId
, String rapVersion
, org.cocktail.fwkcktlreport.server.metier.EOTypeEtat toTypeEtat			) {
    EORapport eo = (EORapport) createAndInsertInstance(editingContext, _EORapport.ENTITY_NAME);    
		eo.setDateCreation(dateCreation);
		eo.setRapLibelle(rapLibelle);
		eo.setRapLocal(rapLocal);
		eo.setRapStrId(rapStrId);
		eo.setRapVersion(rapVersion);
    eo.setToTypeEtatRelationship(toTypeEtat);
    return eo;
  }

  
	  public EORapport localInstanceIn(EOEditingContext editingContext) {
	  		return (EORapport)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapport creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapport creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORapport object = (EORapport)createAndInsertInstance(editingContext, _EORapport.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORapport localInstanceIn(EOEditingContext editingContext, EORapport eo) {
    EORapport localInstance = (eo == null) ? null : (EORapport)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORapport#localInstanceIn a la place.
   */
	public static EORapport localInstanceOf(EOEditingContext editingContext, EORapport eo) {
		return EORapport.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapport>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORapport fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORapport fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORapport> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORapport eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORapport)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORapport fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORapport fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORapport> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORapport eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORapport)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORapport fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORapport eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORapport ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORapport fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
