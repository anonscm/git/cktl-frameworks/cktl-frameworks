/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOTypeExport.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOTypeExport extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOTypeExport.class);

	public static final String ENTITY_NAME = "FwkCktlReport_TypeExport";
	public static final String ENTITY_TABLE_NAME = "cktl_report.TYPE_EXPORT";


// Attribute Keys
  public static final ERXKey<String> TEX_EXTENSION = new ERXKey<String>("texExtension");
  public static final ERXKey<String> TEX_LIBELLE = new ERXKey<String>("texLibelle");
  public static final ERXKey<String> TEX_STR_ID = new ERXKey<String>("texStrId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> TO_TYPE_FORMAT_EXPORTS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport>("toTypeFormatExports");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "texId";

	public static final String TEX_EXTENSION_KEY = "texExtension";
	public static final String TEX_LIBELLE_KEY = "texLibelle";
	public static final String TEX_STR_ID_KEY = "texStrId";

// Attributs non visibles
	public static final String TEX_ID_KEY = "texId";

//Colonnes dans la base de donnees
	public static final String TEX_EXTENSION_COLKEY = "TEX_EXTENSION";
	public static final String TEX_LIBELLE_COLKEY = "TEX_LIBELLE";
	public static final String TEX_STR_ID_COLKEY = "TEX_STR_ID";

	public static final String TEX_ID_COLKEY = "TEX_ID";


	// Relationships
	public static final String TO_TYPE_FORMAT_EXPORTS_KEY = "toTypeFormatExports";



	// Accessors methods
  public String texExtension() {
    return (String) storedValueForKey(TEX_EXTENSION_KEY);
  }

  public void setTexExtension(String value) {
    takeStoredValueForKey(value, TEX_EXTENSION_KEY);
  }

  public String texLibelle() {
    return (String) storedValueForKey(TEX_LIBELLE_KEY);
  }

  public void setTexLibelle(String value) {
    takeStoredValueForKey(value, TEX_LIBELLE_KEY);
  }

  public String texStrId() {
    return (String) storedValueForKey(TEX_STR_ID_KEY);
  }

  public void setTexStrId(String value) {
    takeStoredValueForKey(value, TEX_STR_ID_KEY);
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> toTypeFormatExports() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport>)storedValueForKey(TO_TYPE_FORMAT_EXPORTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> toTypeFormatExports(EOQualifier qualifier) {
    return toTypeFormatExports(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> toTypeFormatExports(EOQualifier qualifier, boolean fetch) {
    return toTypeFormatExports(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> toTypeFormatExports(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport.TO_TYPE_EXPORT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toTypeFormatExports();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToTypeFormatExportsRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_TYPE_FORMAT_EXPORTS_KEY);
  }

  public void removeFromToTypeFormatExportsRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TYPE_FORMAT_EXPORTS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport createToTypeFormatExportsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_TypeFormatExport");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_TYPE_FORMAT_EXPORTS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport) eo;
  }

  public void deleteToTypeFormatExportsRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_TYPE_FORMAT_EXPORTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToTypeFormatExportsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatExport> objects = toTypeFormatExports().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToTypeFormatExportsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOTypeExport avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOTypeExport createEOTypeExport(EOEditingContext editingContext, String texLibelle
, String texStrId
			) {
    EOTypeExport eo = (EOTypeExport) createAndInsertInstance(editingContext, _EOTypeExport.ENTITY_NAME);    
		eo.setTexLibelle(texLibelle);
		eo.setTexStrId(texStrId);
    return eo;
  }

  
	  public EOTypeExport localInstanceIn(EOEditingContext editingContext) {
	  		return (EOTypeExport)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeExport creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOTypeExport creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOTypeExport object = (EOTypeExport)createAndInsertInstance(editingContext, _EOTypeExport.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOTypeExport localInstanceIn(EOEditingContext editingContext, EOTypeExport eo) {
    EOTypeExport localInstance = (eo == null) ? null : (EOTypeExport)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOTypeExport#localInstanceIn a la place.
   */
	public static EOTypeExport localInstanceOf(EOEditingContext editingContext, EOTypeExport eo) {
		return EOTypeExport.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOTypeExport>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOTypeExport fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOTypeExport fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOTypeExport> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOTypeExport eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOTypeExport)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOTypeExport fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOTypeExport fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOTypeExport> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOTypeExport eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOTypeExport)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOTypeExport fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOTypeExport eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOTypeExport ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOTypeExport fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
