/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.io.File;

import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * Les extensions de rapport prises en charge (Jrxml, Jxls, etc.)
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public interface IRapportExt extends NSKeyValueCoding {
	public static final String TYPE_EXPORTS_KEY = "typeExports";
	public final static String REGLE_CHECK_fichierAssocieDefini = "Il est obligatoire d'associer un fichier";
	public final static String REGLE_CHECK_typeExportDefini = "Il est obligatoire d'associer un type d'export";

	public NSArray<EOTypeExport> getTypeExports();

	public EOTypeFormatRapport toTypeFormatRapport();

	public String getSrcFile();

	public void setSrcFile(String filePath);

	/**
	 * Indique si le fichier est utilisable comme modele.
	 * 
	 * @param f
	 */
	public boolean isFileOfType(File f) throws Exception;

	public NSArray<String> getExtensionsValids();

	public EORapport toRapport();

	public EOGlobalID globalID();

	public boolean hasTemporaryGlobalID();

}
