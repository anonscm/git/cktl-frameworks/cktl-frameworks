/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSValidation;

public class EORapportCritere extends _EORapportCritere {
	public final static String REGLE_CHECK_positionObligatoire = "La position est obligatoire";
	private static final String O = "O";
	private static final String N = "N";
	public static final String CLE_KEY = "cle";

	public static final EOSortOrdering SORT_RCR_POSITION_ASC = EOSortOrdering.sortOrderingWithKey(EORapportCritere.RCR_POSITION_KEY, EOSortOrdering.CompareAscending);
	public static final EOQualifier QUAL_OBLIGATOIRE = new EOKeyValueQualifier(EORapportCritere.RCR_OBLIGATOIRE_KEY, EOQualifier.QualifierOperatorEqual, O);

	public EORapportCritere() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		check_positionObligatoire();

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	/**
	 * @return Le libellé affecté au rapportCritere si pas vide, sinon le libellé par défaut du critère.
	 */
	public String rcrLibelleToDisplay() {
		return (StringCtrl.normalize(rcrLibelle(), (toCritere() == null ? null : toCritere().critLibelle())));
	}

	/**
	 * Affecte un libellé si celui-ci est différent du libellé du critère.
	 * 
	 * @param val
	 */
	public void setRcrLibelleToDisplay(String val) {
		if (val == null || val.equals(toCritere().critLibelle())) {
			setRcrLibelle(null);
		}
		else {
			setRcrLibelle(val);
		}
	}

	/**
	 * @return Le commentaire affecté au rapportCritere si pas vide, sinon le commentaire par défaut du critère.
	 */
	public String rcrCommentaireToDisplay() {
		return (StringCtrl.normalize(rcrCommentaire(), (toCritere() == null ? null : toCritere().critCommentaire())));
	}

	public void setRcrCommentaireToDisplay(String val) {
		if (val == null || val.equals(toCritere().critCommentaire())) {
			setRcrCommentaire(null);
		}
		else {
			setRcrCommentaire(val);
		}
	}

	public Boolean isComposant() {
		return toCritere().isComposant();
	}

	public Boolean isObligatoire() {
		return Boolean.valueOf(O.equals(rcrObligatoire()));
	}

	public void setObligatoire(Boolean val) {
		if (Boolean.TRUE.equals(val)) {
			setRcrObligatoire(O);
		}
		else {
			setRcrObligatoire(N);
		}
	}

	/**
	 * @return La clé utilisée par le moteur de generation de rapport pour récupérer la valeur du critère.
	 */
	public String getCle() {
		String res = StringCtrl.normalize(rcrCle(), (toCritere() == null ? null : toCritere().critCle()));
		return res;
	}

	public void setCle(String val) {
		if (val == null || val.equals(toCritere().critCle())) {
			setRcrCle(null);
		}
		else {
			setRcrCle(val);
		}
	}

	/**
	 * Remonte le critere dans la liste.
	 */
	public void goUp() {
		if (rcrPosition().intValue() > 1) {
			//recuperer le critere à la position n-1 et inverser
			Integer newPos = Integer.valueOf(rcrPosition().intValue() - 1);
			EORapportCritere previous = toRapport().getRapportCritereAtPosition(newPos);
			toRapport().switchRapportCriteresPositions(previous, this);
		}
	}

	public void goDown() {
		if (rcrPosition().intValue() < toRapport().toRapportCriteres().count()) {
			//recuperer le critere à la position n+1 et inverser
			Integer newPos = Integer.valueOf(rcrPosition().intValue() + 1);
			EORapportCritere previous = toRapport().getRapportCritereAtPosition(newPos);
			toRapport().switchRapportCriteresPositions(previous, this);
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setRcrObligatoire(N);
	}

	public void check_positionObligatoire() throws NSValidation.ValidationException {
		if (rcrPosition() == null) {
			throw new NSValidation.ValidationException(REGLE_CHECK_positionObligatoire);
		}
	}

}
