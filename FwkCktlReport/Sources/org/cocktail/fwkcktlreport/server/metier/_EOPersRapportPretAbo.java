/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersRapportPretAbo.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOPersRapportPretAbo extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOPersRapportPretAbo.class);

	public static final String ENTITY_NAME = "FwkCktlReport_PersRapportPretAbo";
	public static final String ENTITY_TABLE_NAME = "cktl_report.PERS_RAPPORT_PRET_ABO";


// Attribute Keys
  public static final ERXKey<NSTimestamp> PRPA_DATE = new ERXKey<NSTimestamp>("prpaDate");
  public static final ERXKey<String> PRPA_DEST = new ERXKey<String>("prpaDest");
  public static final ERXKey<String> PRPA_FREQUENCE = new ERXKey<String>("prpaFrequence");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> TO_PERS_RAPPORT_PRET = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>("toPersRapportPret");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeAbo> TO_TYPE_ABO = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeAbo>("toTypeAbo");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prpaId";

	public static final String PRPA_DATE_KEY = "prpaDate";
	public static final String PRPA_DEST_KEY = "prpaDest";
	public static final String PRPA_FREQUENCE_KEY = "prpaFrequence";

// Attributs non visibles
	public static final String PRPA_ID_KEY = "prpaId";
	public static final String PRP_ID_KEY = "prpId";
	public static final String TA_ID_KEY = "taId";

//Colonnes dans la base de donnees
	public static final String PRPA_DATE_COLKEY = "PRPA_DATE";
	public static final String PRPA_DEST_COLKEY = "PRPA_DEST";
	public static final String PRPA_FREQUENCE_COLKEY = "PRPA_FREQUENCE";

	public static final String PRPA_ID_COLKEY = "PRPA_ID";
	public static final String PRP_ID_COLKEY = "PRP_ID";
	public static final String TA_ID_COLKEY = "TA_ID";


	// Relationships
	public static final String TO_PERS_RAPPORT_PRET_KEY = "toPersRapportPret";
	public static final String TO_TYPE_ABO_KEY = "toTypeAbo";



	// Accessors methods
  public NSTimestamp prpaDate() {
    return (NSTimestamp) storedValueForKey(PRPA_DATE_KEY);
  }

  public void setPrpaDate(NSTimestamp value) {
    takeStoredValueForKey(value, PRPA_DATE_KEY);
  }

  public String prpaDest() {
    return (String) storedValueForKey(PRPA_DEST_KEY);
  }

  public void setPrpaDest(String value) {
    takeStoredValueForKey(value, PRPA_DEST_KEY);
  }

  public String prpaFrequence() {
    return (String) storedValueForKey(PRPA_FREQUENCE_KEY);
  }

  public void setPrpaFrequence(String value) {
    takeStoredValueForKey(value, PRPA_FREQUENCE_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret toPersRapportPret() {
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret)storedValueForKey(TO_PERS_RAPPORT_PRET_KEY);
  }

  public void setToPersRapportPretRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret oldValue = toPersRapportPret();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERS_RAPPORT_PRET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERS_RAPPORT_PRET_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeAbo toTypeAbo() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeAbo)storedValueForKey(TO_TYPE_ABO_KEY);
  }

  public void setToTypeAboRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeAbo value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeAbo oldValue = toTypeAbo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ABO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ABO_KEY);
    }
  }
  

/**
 * Créer une instance de EOPersRapportPretAbo avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersRapportPretAbo createEOPersRapportPretAbo(EOEditingContext editingContext, org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret toPersRapportPret, org.cocktail.fwkcktlreport.server.metier.EOTypeAbo toTypeAbo			) {
    EOPersRapportPretAbo eo = (EOPersRapportPretAbo) createAndInsertInstance(editingContext, _EOPersRapportPretAbo.ENTITY_NAME);    
    eo.setToPersRapportPretRelationship(toPersRapportPret);
    eo.setToTypeAboRelationship(toTypeAbo);
    return eo;
  }

  
	  public EOPersRapportPretAbo localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersRapportPretAbo)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportPretAbo creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportPretAbo creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPersRapportPretAbo object = (EOPersRapportPretAbo)createAndInsertInstance(editingContext, _EOPersRapportPretAbo.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersRapportPretAbo localInstanceIn(EOEditingContext editingContext, EOPersRapportPretAbo eo) {
    EOPersRapportPretAbo localInstance = (eo == null) ? null : (EOPersRapportPretAbo)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersRapportPretAbo#localInstanceIn a la place.
   */
	public static EOPersRapportPretAbo localInstanceOf(EOEditingContext editingContext, EOPersRapportPretAbo eo) {
		return EOPersRapportPretAbo.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersRapportPretAbo fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersRapportPretAbo fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersRapportPretAbo> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersRapportPretAbo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersRapportPretAbo)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersRapportPretAbo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersRapportPretAbo fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersRapportPretAbo> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersRapportPretAbo eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersRapportPretAbo)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersRapportPretAbo fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersRapportPretAbo eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersRapportPretAbo ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersRapportPretAbo fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
