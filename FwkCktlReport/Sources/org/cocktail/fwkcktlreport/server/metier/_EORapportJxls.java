/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORapportJxls.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EORapportJxls extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EORapportJxls.class);

	public static final String ENTITY_NAME = "FwkCktlReport_RapportJxls";
	public static final String ENTITY_TABLE_NAME = "cktl_report.RAPPORT_JXLS";


// Attribute Keys
  public static final ERXKey<String> RJL_FILE = new ERXKey<String>("rjlFile");
  public static final ERXKey<String> RJL_SQL = new ERXKey<String>("rjlSql");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport> TO_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport>("toRapport");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> TO_RAPPORT_JXLS_TEXES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex>("toRapportJxlsTexes");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport> TO_TYPE_FORMAT_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport>("toTypeFormatRapport");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rjlId";

	public static final String RJL_FILE_KEY = "rjlFile";
	public static final String RJL_SQL_KEY = "rjlSql";

// Attributs non visibles
	public static final String RAP_ID_KEY = "rapId";
	public static final String RJL_ID_KEY = "rjlId";
	public static final String TFR_ID_KEY = "tfrId";

//Colonnes dans la base de donnees
	public static final String RJL_FILE_COLKEY = "RJL_FILE";
	public static final String RJL_SQL_COLKEY = "RJL_SQL";

	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String RJL_ID_COLKEY = "RJL_ID";
	public static final String TFR_ID_COLKEY = "TFR_ID";


	// Relationships
	public static final String TO_RAPPORT_KEY = "toRapport";
	public static final String TO_RAPPORT_JXLS_TEXES_KEY = "toRapportJxlsTexes";
	public static final String TO_TYPE_FORMAT_RAPPORT_KEY = "toTypeFormatRapport";



	// Accessors methods
  public String rjlFile() {
    return (String) storedValueForKey(RJL_FILE_KEY);
  }

  public void setRjlFile(String value) {
    takeStoredValueForKey(value, RJL_FILE_KEY);
  }

  public String rjlSql() {
    return (String) storedValueForKey(RJL_SQL_KEY);
  }

  public void setRjlSql(String value) {
    takeStoredValueForKey(value, RJL_SQL_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapport toRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EORapport)storedValueForKey(TO_RAPPORT_KEY);
  }

  public void setToRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EORapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EORapport oldValue = toRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RAPPORT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport)storedValueForKey(TO_TYPE_FORMAT_RAPPORT_KEY);
  }

  public void setToTypeFormatRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport oldValue = toTypeFormatRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FORMAT_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FORMAT_RAPPORT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> toRapportJxlsTexes() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex>)storedValueForKey(TO_RAPPORT_JXLS_TEXES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> toRapportJxlsTexes(EOQualifier qualifier) {
    return toRapportJxlsTexes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> toRapportJxlsTexes(EOQualifier qualifier, boolean fetch) {
    return toRapportJxlsTexes(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> toRapportJxlsTexes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex.TO_RAPPORT_JXLS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportJxlsTexes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportJxlsTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLS_TEXES_KEY);
  }

  public void removeFromToRapportJxlsTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLS_TEXES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex createToRapportJxlsTexesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportJxlsTex");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_JXLS_TEXES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex) eo;
  }

  public void deleteToRapportJxlsTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JXLS_TEXES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportJxlsTexesRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex> objects = toRapportJxlsTexes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportJxlsTexesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORapportJxls avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORapportJxls createEORapportJxls(EOEditingContext editingContext, String rjlFile
, org.cocktail.fwkcktlreport.server.metier.EORapport toRapport, org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport			) {
    EORapportJxls eo = (EORapportJxls) createAndInsertInstance(editingContext, _EORapportJxls.ENTITY_NAME);    
		eo.setRjlFile(rjlFile);
    eo.setToRapportRelationship(toRapport);
    eo.setToTypeFormatRapportRelationship(toTypeFormatRapport);
    return eo;
  }

  
	  public EORapportJxls localInstanceIn(EOEditingContext editingContext) {
	  		return (EORapportJxls)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportJxls creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportJxls creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORapportJxls object = (EORapportJxls)createAndInsertInstance(editingContext, _EORapportJxls.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORapportJxls localInstanceIn(EOEditingContext editingContext, EORapportJxls eo) {
    EORapportJxls localInstance = (eo == null) ? null : (EORapportJxls)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORapportJxls#localInstanceIn a la place.
   */
	public static EORapportJxls localInstanceOf(EOEditingContext editingContext, EORapportJxls eo) {
		return EORapportJxls.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJxls>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORapportJxls fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORapportJxls fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORapportJxls> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORapportJxls eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORapportJxls)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORapportJxls fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORapportJxls fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORapportJxls> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORapportJxls eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORapportJxls)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORapportJxls fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORapportJxls eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORapportJxls ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORapportJxls fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
