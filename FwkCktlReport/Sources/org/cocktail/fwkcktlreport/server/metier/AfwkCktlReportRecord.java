/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlreport.server.metier;

import java.lang.reflect.Method;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOTemporaryGlobalID;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXGenericRecord;

/**
 * Classe abstraite pour les entités métier du framework FwkCktlDroitUtils.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class AfwkCktlReportRecord extends ERXGenericRecord {

	public static final String OUI = "O";
	public static final String NON = "N";

	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PERS_ID_SUPPRESSION_KEY = "persIdSuppression";
	private NSMutableArray specificites = new NSMutableArray();
	private NSArray attributes = null;
	private Map attributesTaillesMax;
	private NSArray attributesObligatoires;

	private FwkCktlReportApplicationUser createur;
	private FwkCktlReportApplicationUser modificateur;
	private FwkCktlReportApplicationUser suppresseur;
	private Integer persIdSuppression;

	public static NSTimestampFormatter DATE_FORMATTER = new NSTimestampFormatter("%y/%d/%d %H:%M:%S");

	/**
	 * @return La date actuelle.
	 */
	public NSTimestamp now() {
		return new NSTimestamp(new Date());
	}

	/**
	 * Verifie si la longueur d'une chaine est bien inferieure a un maximum.
	 * 
	 * @param s
	 * @param l -1 si illimite.
	 * @return true si l=-1 ou si le nombre de caracteres de s est inferieur ou egal a l.
	 */
	protected boolean verifieLongueurMax(String s, int l) {
		return (l == -1 || s == null || s.trim().length() <= l);
	}

	/**
	 * Verifie si la longeur d'une chaine est bien superieure a un maximum.
	 * 
	 * @param s
	 * @param l
	 * @return true si l=0 ou si le nombre de caracteres de s est superieur ou egal a l.
	 */
	protected boolean verifieLongueurMin(String s, int l) {
		return (s == null || s.trim().length() >= l);
	}

	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim). A appeler en debut de ValidateObjectMetier.
	 */
	protected void trimAllString() {
		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String array_element = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(array_element) != null && obj.valueForKey(array_element) instanceof String) {
				obj.takeValueForKey(((String) obj.valueForKey(array_element)).trim(), array_element);
			}
		}
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		callOnAwakeFromInsertionForSpecificites();
	}

	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		callOnValidateForInsertOnSpecificites();
		super.validateForInsert();
	}

	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		callOnValidateForUpdateOnSpecificites();
		super.validateForUpdate();
	}

	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
		callOnValidateForDeleteOnSpecificites();
	}

	@Override
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForSave();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		callOnValidateObjectMetierOnSpecificites();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		callOnValidateBeforeTransactionSaveOnSpecificites();
	}

	public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
		EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
		if (eoeditingcontext1 == null) {
			throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
		}
		if (eoenterpriseobject == null) {
			return null;
		}
		else {
			com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
			return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);
		}
	}

	public static AfwkCktlReportRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
		return createAndInsertInstance(eoeditingcontext, s, null);
	}

	/**
	 * @param eoeditingcontext
	 * @param s
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau est affecte a l'objet juste apres sa creation, avant l'insertion dans
	 *            l'editing context.
	 * @return
	 */
	public static AfwkCktlReportRecord createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
		EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
		if (eoclassdescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
		}
		else {
			AfwkCktlReportRecord eoenterpriseobject = (AfwkCktlReportRecord) eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
			if (specificites != null) {
				Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
				while (enumeration.hasMoreElements()) {
					AfwkCktlReportRecord.ISpecificite specificite = enumeration.nextElement();
					eoenterpriseobject.registerSpecificite(specificite);
				}
			}
			eoeditingcontext.insertObject(eoenterpriseobject);
			return eoenterpriseobject;
		}
	}

	/**
	 * Interface a respecter pour ajouter des specificites métier à l'objet. Pour chaque objet déclaré comme spécificité, les méthodes seront appelées
	 * lors des étapes de la vie de l'objet, que ce soit lors de la création ou lors des étapes de validation. Ceci permet par exemple d'ajouter du
	 * code pour définir les règles métier d'un partenaire (qui est une spécificité d'un objet {@link EOStructure}). <br/>
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public interface ISpecificite {

		/**
		 * Appele lors de l'insertion dans l'editing context.<br/>
		 * NB :
		 * 
		 * @param afwkPersRecord
		 */
		public void onAwakeFromInsertion(AfwkCktlReportRecord afwkPersRecord);

		/**
		 * Appele par le saveChanges lors de l'insertion d'un objet (par validateForUpdate)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForInsert(AfwkCktlReportRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele par le saveChanges lors de la mise a jour d'un objet (par validateForUpdate)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForUpdate(AfwkCktlReportRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele par le saveChanges lors lors de la suppression d'un objet (par validateForDelete)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateForDelete(AfwkCktlReportRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele avant l'enregistrement d'un objet (par validateObjectMetier)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateObjectMetier(AfwkCktlReportRecord afwkPersRecord) throws NSValidation.ValidationException;

		/**
		 * Appele avant l'enregistrement d'un objet (par ValidateBeforeTransactionSave)
		 * 
		 * @param afwkPersRecord L'objet sur lequel porte la methode.
		 */
		public void onValidateBeforeTransactionSave(AfwkCktlReportRecord afwkDroitUtilsRecord) throws NSValidation.ValidationException;

	}

	/**
	 * Ajoute un specificite pour les operations de validation de l'objet metier.
	 * 
	 * @param specificite
	 */
	public void registerSpecificite(ISpecificite specificite) {
		if (specificites == null) {
			specificites = new NSMutableArray();
		}
		if (specificites.indexOfObject(specificite) == NSArray.NotFound) {
			specificites.addObject(specificite);
		}
	}

	/**
	 * Supprime une specificite la liste des specificites.
	 * 
	 * @param specificite
	 */
	public void unregisterSpecificite(ISpecificite specificite) {
		if (specificites != null) {
			if (specificites.indexOfObject(specificite) == NSArray.NotFound) {
				specificites.removeObject(specificite);
			}
		}
	}

	private void callOnAwakeFromInsertionForSpecificites() {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onAwakeFromInsertion(this);
			}
		}
	}

	private void callOnValidateForDeleteOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForDelete(this);
			}
		}
	}

	private void callOnValidateForInsertOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForInsert(this);
			}
		}
	}

	private void callOnValidateForUpdateOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateForUpdate(this);
			}
		}
	}

	private void callOnValidateObjectMetierOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateObjectMetier(this);
			}
		}
	}

	private void callOnValidateBeforeTransactionSaveOnSpecificites() throws NSValidation.ValidationException {
		if (specificites != null) {
			Enumeration<ISpecificite> enumeration = specificites.objectEnumerator();
			while (enumeration.hasMoreElements()) {
				ISpecificite specificite = enumeration.nextElement();
				specificite.onValidateBeforeTransactionSave(this);
			}
		}
	}

	public NSMutableArray getSpecificites() {
		return specificites;
	}

	public void setSpecificites(NSMutableArray specificites) {
		this.specificites = specificites;
	}

	//	/**
	//	 * @return Les attributs de l'entité.
	//	 */
	//	public NSArray attributes() {
	//		if (attributes == null) {
	//			attributes = EOUtilities.entityForObject(this.editingContext(), this).classProperties();
	//		}
	//		return attributes;
	//	}

	/**
	 * @return Une map avec en clé le nom de l'attribut et en valeur un integer contenant la taille max de l'attribut.
	 */
	public Map attributesTaillesMax() {
		if (attributesTaillesMax == null) {
			attributesTaillesMax = attributeMaxSizesForEntityName(this.editingContext(), entityName());
		}
		return attributesTaillesMax;
	}

	//	public Map attributesTaillesMax() {		
	//		if (attributesTaillesMax == null) {
	//			attributesTaillesMax = new HashMap();
	//			Enumeration enumeration = attributes().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				EOProperty object = (EOProperty) enumeration.nextElement();
	//				if (object instanceof EOAttribute) {
	//					if (((EOAttribute) object).width() > 0) {
	//						attributesTaillesMax.put(object.name(), Integer.valueOf(((EOAttribute) object).width()));
	//					}
	//				}
	//				
	//			}
	//		}
	//		return attributesTaillesMax;
	//	}

	public NSArray attributesObligatoires() {
		if (attributesObligatoires == null) {
			attributesObligatoires = requiredAttributeForEntityName(this.editingContext(), entityName());
		}
		return attributesObligatoires;
	}

	//	public NSArray attributesObligatoires() {
	//		if (attributesObligatoires == null) {
	//			NSMutableArray _attributesObligatoires = new NSMutableArray();
	//			Enumeration enumeration = attributes().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				EOProperty object = (EOProperty) enumeration.nextElement();
	//				if (object instanceof EOAttribute) {
	//					if (!((EOAttribute) object).allowsNull()) {
	//						_attributesObligatoires.addObject(object.name());
	//					}
	//				}
	//			}
	//			attributesObligatoires = _attributesObligatoires.immutableClone();
	//		}
	//		return attributesObligatoires;
	//	}

	/**
	 * Vérife si les champs obligatoires sont bien saisis (en fonction de ce qui est indiqué dans le modèle)
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesObligatoires() throws NSValidation.ValidationException {
		Iterator iterator = attributesObligatoires().iterator();
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			if (valueForKey(key) == null) {
				throw new NSValidation.ValidationException("Le champ " + getDisplayName(key) + " est obligatoire.");
			}
		}
	}

	/**
	 * Renvoie le nom d'affichage de la propriété. Utilise {@link #displayNames()}.
	 * 
	 * @param propertyName nom de la propriété (attribut ou relation)
	 * @return
	 */
	public String getDisplayName(String propertyName) {
		if (displayNames().get(propertyName) != null) {
			return (String) displayNames().get(propertyName);
		}
		return propertyName;
	}

	/**
	 * Renvoie une Map contenant en clé le nom de la propriété et en valeur le nom d'affichage (parlant) de cette propriété. Par exemple <CP, Code
	 * Postal>. Par défaut la Map est vide, il faut surcharger la méthode.
	 */
	public Map displayNames() {
		return new HashMap();
	}

	/**
	 * Verifie la longueur maximale des champs saisis (a partir de la taille indiquée dans le modèle).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void checkContraintesLongueursMax() throws NSValidation.ValidationException {
		Iterator iterator = attributesTaillesMax().keySet().iterator();
		System.out.println(attributesTaillesMax().keySet());
		while (iterator.hasNext()) {
			String key = (String) iterator.next();
			//System.out.println("verif :" + getDisplayName(key) + " : " + ((Integer) attributesTaillesMax().get(key)).intValue());
			if (valueForKey(key) != null && ((String) valueForKey(key)).length() > ((Integer) attributesTaillesMax().get(key)).intValue()) {
				throw new NSValidation.ValidationException("La taille du champ " + getDisplayName(key) + " ne doit pas dépasser " + ((Integer) attributesTaillesMax().get(key)).intValue() + " caractères.");
			}
		}
	}

	/**
	 * @return le createur de l'enregistrement (si le champ persIdCreation est renseigne).
	 */
	public FwkCktlReportApplicationUser getCreateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_CREATION_KEY);
		if ((createur == null && persId != null) || (createur != null && !createur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				createur = null;
			}
			else {
				createur = new FwkCktlReportApplicationUser(editingContext(), persId);

			}
		}
		return createur;
	}

	/**
	 * @return le modificateur de l'enregistrement (si le champ persIdModification est renseigne).
	 */
	public FwkCktlReportApplicationUser getModificateur() {
		Integer persId = (Integer) valueForKey(PERS_ID_MODIFICATION_KEY);
		if ((modificateur == null && persId != null) || (modificateur != null && !modificateur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				modificateur = null;
			}
			else {
				modificateur = new FwkCktlReportApplicationUser(editingContext(), persId);
			}
		}
		return modificateur;
	}

	/**
	 * @return le modificateur de l'enregistrement (si le champ persIdModification est renseigne).
	 */
	public FwkCktlReportApplicationUser getSuppresseur() {
		Integer persId = (Integer) valueForKey(PERS_ID_SUPPRESSION_KEY);
		if ((suppresseur == null && persId != null) || (suppresseur != null && !suppresseur.getPersonne().persId().equals(persId))) {
			if (persId == null) {
				suppresseur = null;
			}
			else {
				suppresseur = new FwkCktlReportApplicationUser(editingContext(), persId);
			}
		}
		return suppresseur;
	}

	/**
	 * @return true si l'objet possede un globalID temporaire (n'existe pas encore dans la base de données).
	 */
	public boolean hasTemporaryGlobalID() {
		return (globalID() != null && globalID() instanceof EOTemporaryGlobalID);
	}

	/**
	 * @return le globalID de l'objet à partir de l'editingContext associé. Null si pas d'editingContext.
	 */
	public EOGlobalID globalID() {
		if (editingContext() == null) {
			return null;
		}
		return globalID(this.editingContext());
	}

	/**
	 * @param ec
	 * @return le globalID de l'objet à partir d'ec.
	 */
	public EOGlobalID globalID(EOEditingContext ec) {
		return ec.globalIDForObject(this);
	}

	public Integer persIdSuppression() {
		return persIdSuppression;
	}

	public void setPersIdModification(Integer value) {
		persIdSuppression = value;
	}

	public String dressExceptionMsg(Exception e) {
		return this.getClass().getSimpleName() + " : " + toDisplayString() + ":" + e.getLocalizedMessage() + "(ID = " + globalID().toString() + ")";
	}

	public String toDisplayString() {
		return toString();
	}

	/**
	 * Chemin d'acces en keyValueCoding pour acceder a la variable representant le callDelegate (l'instance de l'objet qui va effectivement effectuer
	 * le traitement)
	 */
	//public static final String REMOTE_PATH = "fwkCktlJavaClientSupport_" + "CktlEOUtilities";

	//	private static String EODISTRIBUTEDOBJECTSTORE_CLASS_NAME = "com.webobjects.eodistribution.client.EODistributedObjectStore";
	//	private static String CLIENT_INVOKE_CLASSNAME = "org.cocktail.fwkcktljavaclientsupport.client.remotecall.ServerCallCktlEOUtilities";
	//	private static String SERVER_INVOKE_CLASSNAME = "com.webobjects.eoaccess.EOUtilities";

	public static final String executeStoredProcedureNamed = "executeStoredProcedureNamed";
	public static final String primaryKeyForObject = "primaryKeyForObject";
	public static final String rawRowsForSQL = "rawRowsForSQL";
	public static final String objectsWithFetchSpecificationAndBindings = "objectsWithFetchSpecificationAndBindings";
	public static final String requiredAttributesForObject = "requiredAttributesForObject";
	public static final String attributesMaxSizesForObject = "attributesMaxSizesForObject";

	//
	//	public static boolean isClientEditingContext(EOEditingContext editingContext) {
	//		EOObjectStore objectStore = editingContext.parentObjectStore();
	//		boolean isClient = true;
	//		try {
	//			Class myEODistributedObjectStore = Class.forName(EODISTRIBUTEDOBJECTSTORE_CLASS_NAME);
	//			isClient = myEODistributedObjectStore.isInstance(objectStore);
	//		} catch (Exception e) {
	//			isClient = false;
	//		}
	//		return isClient;
	//	}

	public static NSDictionary primaryKeyForObject(EOEditingContext ec, EOEnterpriseObject eo) throws Exception {
		return EOUtilities.primaryKeyForObject(ec, eo);
	}

	public static NSArray rawRowsForSQL(EOEditingContext ec, String modelName, String sqlString, NSArray keys) throws Throwable {
		return EOUtilities.rawRowsForSQL(ec, modelName, sqlString, keys);
	}

	public static NSDictionary executeStoredProcedureNamed(EOEditingContext ec, String name, NSDictionary args) throws Throwable {
		return EOUtilities.executeStoredProcedureNamed(ec, name, args);
	}

	//
	//	public static NSDictionary attributeMaxSizesForEntityName(EOEditingContext ec, String name) {
	//
	//		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
	//		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
	//
	//		if (isClientEditingContext(ec)) {
	//			//classEODescriptionName = "com.webobjects.eodistribution.client.EODistributedClassDescription";
	//			classEOAttributeName = "com.webobjects.eodistribution.client.EOAttribute";
	//		}
	//
	//		try {
	//			Class myEOClassDescription = Class.forName(classEODescriptionName);
	//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
	//				String.class
	//			});
	//			Method myEOClassDescription_attributeNamed = myEOClassDescription.getDeclaredMethod("attributeNamed", new Class[] {
	//				String.class
	//			});
	//
	//			EOClassDescription eoclassdescription = (EOClassDescription) myEOClassDescription_classDescriptionForEntityName.invoke(null, new Object[] {
	//				name
	//			});
	//			if (eoclassdescription == null) {
	//				throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + name + "' !");
	//			}
	//
	//			Class myeoAttribute = Class.forName(classEOAttributeName);
	//			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width", new Class[] {});
	//
	//			NSMutableDictionary res = new NSMutableDictionary();
	//			Enumeration enumeration = eoclassdescription.attributeKeys().objectEnumerator();
	//			while (enumeration.hasMoreElements()) {
	//				String attName = (String) enumeration.nextElement();
	//				Object object = myEOClassDescription_attributeNamed.invoke(eoclassdescription, new Object[] {
	//					attName
	//				});
	//				Number width = (Number) myeoAttribute_width.invoke(object, new Object[] {});
	//				if (width != null && width.intValue() > 0) {
	//					res.takeValueForKey(new Integer(width.intValue()), attName);
	//				}
	//			}
	//			return res.immutableClone();
	//
	//		} catch (Exception e) {
	//			e.printStackTrace();
	//			return null;
	//		}
	//
	//	}

	public static Map attributeMaxSizesForEntityName(EOEditingContext ec, String name) {

		HashMap res = new HashMap();

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
		//String attributeNamedName = "attributeNamed";

		try {
			//Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_width = myeoAttribute.getDeclaredMethod("width", new Class[] {});
			//			
			//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
			//				String.class
			//			});
			////			

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

					});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
					String.class
			});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			//NSMutableDictionary res = new NSMutableDictionary();
			//				Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
						attName
				});

				Number width = (Number) myeoAttribute_width.invoke(object, new Object[] {});
				if (width != null && width.intValue() > 0) {
					//res.takeValueForKey(new Integer(width.intValue()), attName);
					res.put(attName, new Integer(width.intValue()));
				}
			}

			return res;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray requiredAttributeForEntityName(EOEditingContext ec, String name) {

		EOClassDescription _classDescription = EOClassDescription.classDescriptionForEntityName(name);

		String classEODescriptionName = "com.webobjects.eoaccess.EOEntityClassDescription";
		String classEOAttributeName = "com.webobjects.eoaccess.EOAttribute";
		//String attributeNamedName = "attributeNamed";

		try {
			//Classe serveur ou client
			Class myEOClassDescription = Class.forName(classEODescriptionName);
			Class myeoAttribute = Class.forName(classEOAttributeName);

			Method myeoAttribute_allowsNull = myeoAttribute.getDeclaredMethod("allowsNull", new Class[] {});
			//			
			//			Method myEOClassDescription_classDescriptionForEntityName = myEOClassDescription.getDeclaredMethod("classDescriptionForEntityName", new Class[] {
			//				String.class
			//			});
			////			

			Class myEntityClass = Class.forName("com.webobjects.eoaccess.EOEntity");
			Method myEOClassDescription_entity = myEOClassDescription.getDeclaredMethod("entity", new Class[] {

					});

			Method myEntityClass_attributeNamed = myEntityClass.getDeclaredMethod("attributeNamed", new Class[] {
					String.class
			});

			Object entity = myEOClassDescription_entity.invoke(_classDescription, new Object[] {});

			NSMutableArray res = new NSMutableArray();
			Enumeration enumeration = _classDescription.attributeKeys().objectEnumerator();
			while (enumeration.hasMoreElements()) {
				String attName = (String) enumeration.nextElement();

				Object object = myEntityClass_attributeNamed.invoke(entity, new Object[] {
						attName
				});
				Boolean xxx = (Boolean) myeoAttribute_allowsNull.invoke(object, new Object[] {});
				if (!xxx.booleanValue()) {
					res.addObject(attName);
				}
			}
			return res.immutableClone();

		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

	}

	public static NSArray objectsWithFetchSpecificationAndBindings(EOEditingContext ec, String entityName, String fetchSpecName, NSDictionary bindings) throws Throwable {
		return EOUtilities.objectsWithFetchSpecificationAndBindings(ec, entityName, fetchSpecName, bindings);
	}

	public static NSArray objectsMatchingKeyAndValue(EOEditingContext ec, String entityName, String key, Object value) {
		NSDictionary dict = new NSDictionary(value, key);
		NSArray results = objectsMatchingValues(ec, entityName, dict);
		return results;
	}

	public static NSArray objectsMatchingValues(EOEditingContext ec, String name, NSDictionary values) {
		EOQualifier qualifier = EOQualifier.qualifierToMatchAllValues(values);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(name, qualifier, null);
		NSArray results = ec.objectsWithFetchSpecification(fetchSpec);
		return results;
	}

	/**
	 * @param editingContext
	 * @return L'editingContext de plus haut niveau (avant enregistrement dans la base ). Renvoi null si editingContext est nul.
	 */
	public static EOEditingContext getTopLevelEditingContext(EOEditingContext editingContext) {
		if (editingContext == null) {
			return null;
		}
		EOEditingContext edc = editingContext;
		while (!(edc.parentObjectStore() instanceof EOObjectStoreCoordinator)) {
			edc = (EOEditingContext) edc.parentObjectStore();
		}
		return edc;
	}

}
