/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORapportJrxml.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EORapportJrxml extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EORapportJrxml.class);

	public static final String ENTITY_NAME = "FwkCktlReport_RapportJrxml";
	public static final String ENTITY_TABLE_NAME = "cktl_report.RAPPORT_JRXML";


// Attribute Keys
  public static final ERXKey<String> RJR_FILE = new ERXKey<String>("rjrFile");
  public static final ERXKey<String> RJR_VERSION_JR_MIN = new ERXKey<String>("rjrVersionJrMin");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport> TO_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport>("toRapport");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> TO_RAPPORT_JRXML_TEXES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex>("toRapportJrxmlTexes");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport> TO_TYPE_FORMAT_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport>("toTypeFormatRapport");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rjrId";

	public static final String RJR_FILE_KEY = "rjrFile";
	public static final String RJR_VERSION_JR_MIN_KEY = "rjrVersionJrMin";

// Attributs non visibles
	public static final String RAP_ID_KEY = "rapId";
	public static final String RJR_ID_KEY = "rjrId";
	public static final String TFR_ID_KEY = "tfrId";

//Colonnes dans la base de donnees
	public static final String RJR_FILE_COLKEY = "RJR_FILE";
	public static final String RJR_VERSION_JR_MIN_COLKEY = "RJR_VERSION_JR_MIN";

	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String RJR_ID_COLKEY = "RJR_ID";
	public static final String TFR_ID_COLKEY = "TFR_ID";


	// Relationships
	public static final String TO_RAPPORT_KEY = "toRapport";
	public static final String TO_RAPPORT_JRXML_TEXES_KEY = "toRapportJrxmlTexes";
	public static final String TO_TYPE_FORMAT_RAPPORT_KEY = "toTypeFormatRapport";



	// Accessors methods
  public String rjrFile() {
    return (String) storedValueForKey(RJR_FILE_KEY);
  }

  public void setRjrFile(String value) {
    takeStoredValueForKey(value, RJR_FILE_KEY);
  }

  public String rjrVersionJrMin() {
    return (String) storedValueForKey(RJR_VERSION_JR_MIN_KEY);
  }

  public void setRjrVersionJrMin(String value) {
    takeStoredValueForKey(value, RJR_VERSION_JR_MIN_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapport toRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EORapport)storedValueForKey(TO_RAPPORT_KEY);
  }

  public void setToRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EORapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EORapport oldValue = toRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RAPPORT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport)storedValueForKey(TO_TYPE_FORMAT_RAPPORT_KEY);
  }

  public void setToTypeFormatRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport oldValue = toTypeFormatRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FORMAT_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FORMAT_RAPPORT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> toRapportJrxmlTexes() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex>)storedValueForKey(TO_RAPPORT_JRXML_TEXES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> toRapportJrxmlTexes(EOQualifier qualifier) {
    return toRapportJrxmlTexes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> toRapportJrxmlTexes(EOQualifier qualifier, boolean fetch) {
    return toRapportJrxmlTexes(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> toRapportJrxmlTexes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex.TO_RAPPORT_JRXML_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportJrxmlTexes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportJrxmlTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXML_TEXES_KEY);
  }

  public void removeFromToRapportJrxmlTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXML_TEXES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex createToRapportJrxmlTexesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportJrxmlTex");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_JRXML_TEXES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex) eo;
  }

  public void deleteToRapportJrxmlTexesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_JRXML_TEXES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportJrxmlTexesRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex> objects = toRapportJrxmlTexes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportJrxmlTexesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORapportJrxml avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORapportJrxml createEORapportJrxml(EOEditingContext editingContext, String rjrFile
, String rjrVersionJrMin
, org.cocktail.fwkcktlreport.server.metier.EORapport toRapport, org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport			) {
    EORapportJrxml eo = (EORapportJrxml) createAndInsertInstance(editingContext, _EORapportJrxml.ENTITY_NAME);    
		eo.setRjrFile(rjrFile);
		eo.setRjrVersionJrMin(rjrVersionJrMin);
    eo.setToRapportRelationship(toRapport);
    eo.setToTypeFormatRapportRelationship(toTypeFormatRapport);
    return eo;
  }

  
	  public EORapportJrxml localInstanceIn(EOEditingContext editingContext) {
	  		return (EORapportJrxml)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportJrxml creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportJrxml creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORapportJrxml object = (EORapportJrxml)createAndInsertInstance(editingContext, _EORapportJrxml.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORapportJrxml localInstanceIn(EOEditingContext editingContext, EORapportJrxml eo) {
    EORapportJrxml localInstance = (eo == null) ? null : (EORapportJrxml)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORapportJrxml#localInstanceIn a la place.
   */
	public static EORapportJrxml localInstanceOf(EOEditingContext editingContext, EORapportJrxml eo) {
		return EORapportJrxml.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportJrxml>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORapportJrxml fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORapportJrxml fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORapportJrxml> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORapportJrxml eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORapportJrxml)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORapportJrxml fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORapportJrxml fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORapportJrxml> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORapportJrxml eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORapportJrxml)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORapportJrxml fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORapportJrxml eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORapportJrxml ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORapportJrxml fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
