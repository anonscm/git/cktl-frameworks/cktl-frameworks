/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersRapportPret.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOPersRapportPret extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOPersRapportPret.class);

	public static final String ENTITY_NAME = "FwkCktlReport_PersRapportPret";
	public static final String ENTITY_TABLE_NAME = "cktl_report.PERS_RAPPORT_PRET";


// Attribute Keys
  public static final ERXKey<NSTimestamp> PRP_DATE_CREATION = new ERXKey<NSTimestamp>("prpDateCreation");
  public static final ERXKey<NSTimestamp> PRP_DATE_MODIFICATION = new ERXKey<NSTimestamp>("prpDateModification");
  public static final ERXKey<String> PRP_LIBELLE = new ERXKey<String>("prpLibelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonne");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> TO_PERS_RAPPORT_PRET_ABOS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo>("toPersRapportPretAbos");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> TO_PERS_RAPPORT_PRET_CRITS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>("toPersRapportPretCrits");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport> TO_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport>("toRapport");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> TO_TYPE_EXPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeExport>("toTypeExport");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport> TO_TYPE_FORMAT_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport>("toTypeFormatRapport");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prpId";

	public static final String PRP_DATE_CREATION_KEY = "prpDateCreation";
	public static final String PRP_DATE_MODIFICATION_KEY = "prpDateModification";
	public static final String PRP_LIBELLE_KEY = "prpLibelle";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";
	public static final String PRP_ID_KEY = "prpId";
	public static final String RAP_ID_KEY = "rapId";
	public static final String TEX_ID_KEY = "texId";
	public static final String TFR_ID_KEY = "tfrId";

//Colonnes dans la base de donnees
	public static final String PRP_DATE_CREATION_COLKEY = "PRP_DATE_CREATION";
	public static final String PRP_DATE_MODIFICATION_COLKEY = "PRP_DATE_MODIFICATION";
	public static final String PRP_LIBELLE_COLKEY = "PRP_LIBELLE";

	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRP_ID_COLKEY = "PRP_ID";
	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String TEX_ID_COLKEY = "TEX_ID";
	public static final String TFR_ID_COLKEY = "TFR_ID";


	// Relationships
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_PERS_RAPPORT_PRET_ABOS_KEY = "toPersRapportPretAbos";
	public static final String TO_PERS_RAPPORT_PRET_CRITS_KEY = "toPersRapportPretCrits";
	public static final String TO_RAPPORT_KEY = "toRapport";
	public static final String TO_TYPE_EXPORT_KEY = "toTypeExport";
	public static final String TO_TYPE_FORMAT_RAPPORT_KEY = "toTypeFormatRapport";



	// Accessors methods
  public NSTimestamp prpDateCreation() {
    return (NSTimestamp) storedValueForKey(PRP_DATE_CREATION_KEY);
  }

  public void setPrpDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, PRP_DATE_CREATION_KEY);
  }

  public NSTimestamp prpDateModification() {
    return (NSTimestamp) storedValueForKey(PRP_DATE_MODIFICATION_KEY);
  }

  public void setPrpDateModification(NSTimestamp value) {
    takeStoredValueForKey(value, PRP_DATE_MODIFICATION_KEY);
  }

  public String prpLibelle() {
    return (String) storedValueForKey(PRP_LIBELLE_KEY);
  }

  public void setPrpLibelle(String value) {
    takeStoredValueForKey(value, PRP_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EORapport toRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EORapport)storedValueForKey(TO_RAPPORT_KEY);
  }

  public void setToRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EORapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EORapport oldValue = toRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RAPPORT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeExport toTypeExport() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeExport)storedValueForKey(TO_TYPE_EXPORT_KEY);
  }

  public void setToTypeExportRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeExport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeExport oldValue = toTypeExport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_EXPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_EXPORT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport)storedValueForKey(TO_TYPE_FORMAT_RAPPORT_KEY);
  }

  public void setToTypeFormatRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport oldValue = toTypeFormatRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FORMAT_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FORMAT_RAPPORT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> toPersRapportPretAbos() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo>)storedValueForKey(TO_PERS_RAPPORT_PRET_ABOS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> toPersRapportPretAbos(EOQualifier qualifier) {
    return toPersRapportPretAbos(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> toPersRapportPretAbos(EOQualifier qualifier, boolean fetch) {
    return toPersRapportPretAbos(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> toPersRapportPretAbos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo.TO_PERS_RAPPORT_PRET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportPretAbos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportPretAbosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_ABOS_KEY);
  }

  public void removeFromToPersRapportPretAbosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_ABOS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo createToPersRapportPretAbosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportPretAbo");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_PRET_ABOS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo) eo;
  }

  public void deleteToPersRapportPretAbosRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_ABOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportPretAbosRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretAbo> objects = toPersRapportPretAbos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportPretAbosRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)storedValueForKey(TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier) {
    return toPersRapportPretCrits(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier, boolean fetch) {
    return toPersRapportPretCrits(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit.TO_PERS_RAPPORT_PRET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportPretCrits();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public void removeFromToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit createToPersRapportPretCritsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportPretCrit");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_PRET_CRITS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit) eo;
  }

  public void deleteToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportPretCritsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> objects = toPersRapportPretCrits().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportPretCritsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPersRapportPret avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersRapportPret createEOPersRapportPret(EOEditingContext editingContext, NSTimestamp prpDateCreation
, NSTimestamp prpDateModification
, String prpLibelle
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne, org.cocktail.fwkcktlreport.server.metier.EORapport toRapport, org.cocktail.fwkcktlreport.server.metier.EOTypeExport toTypeExport, org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport toTypeFormatRapport			) {
    EOPersRapportPret eo = (EOPersRapportPret) createAndInsertInstance(editingContext, _EOPersRapportPret.ENTITY_NAME);    
		eo.setPrpDateCreation(prpDateCreation);
		eo.setPrpDateModification(prpDateModification);
		eo.setPrpLibelle(prpLibelle);
    eo.setToPersonneRelationship(toPersonne);
    eo.setToRapportRelationship(toRapport);
    eo.setToTypeExportRelationship(toTypeExport);
    eo.setToTypeFormatRapportRelationship(toTypeFormatRapport);
    return eo;
  }

  
	  public EOPersRapportPret localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersRapportPret)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportPret creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportPret creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPersRapportPret object = (EOPersRapportPret)createAndInsertInstance(editingContext, _EOPersRapportPret.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersRapportPret localInstanceIn(EOEditingContext editingContext, EOPersRapportPret eo) {
    EOPersRapportPret localInstance = (eo == null) ? null : (EOPersRapportPret)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersRapportPret#localInstanceIn a la place.
   */
	public static EOPersRapportPret localInstanceOf(EOEditingContext editingContext, EOPersRapportPret eo) {
		return EOPersRapportPret.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersRapportPret fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersRapportPret fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersRapportPret> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersRapportPret eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersRapportPret)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersRapportPret fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersRapportPret fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersRapportPret> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersRapportPret eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersRapportPret)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersRapportPret fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersRapportPret eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersRapportPret ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersRapportPret fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
