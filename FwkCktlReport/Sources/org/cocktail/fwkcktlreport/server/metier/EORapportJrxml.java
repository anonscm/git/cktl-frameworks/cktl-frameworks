/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.io.File;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXFileUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class EORapportJrxml extends _EORapportJrxml implements IRapportExt {

	private static final NSArray<String> EXTENSIONS_VALIDES = new NSArray<String>(new String[] {
			"jasper"
	});

	public EORapportJrxml() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Règles de validation<br>
	 * <ul>
	 * <li>Il doit y avoir un fichier associé</li>
	 * <li>Il doit y avoir un fichier associé</li>
	 * </ul>
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		try {
			check_fichierAssocieDefini();
			check_typeExportDefini();
		} catch (NSValidation.ValidationException e) {
			ValidationException e1 = new ValidationException(toDisplayString() + " : " + e.getLocalizedMessage());
			e1.initCause(e);
			throw e1;
		}
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	/**
	 * Les types d'export possible pour ce rapport.
	 */
	public NSArray<EOTypeExport> getTypeExports() {
		NSArray tes = toRapportJrxmlTexes();
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < tes.count(); i++) {
			EORapportJrxmlTex array_element = (EORapportJrxmlTex) tes.objectAtIndex(i);
			res.addObject(array_element.toTypeExport());
		}

		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(new Object[] {
					EOTypeExport.SORT_TEX_LIBELLE_ASC
			}));
	}

	public String getSrcFile() {
		return rjrFile();
	}

	public boolean isFileOfType(File f) throws Exception {
		if (!f.exists()) {
			throw new Exception("Le fichier " + f.getAbsolutePath() + " n'a pas été trouvé.");
		}

		String ext = ERXFileUtilities.fileExtension(f.getAbsolutePath());
		if (ext != null && EXTENSIONS_VALIDES.contains(ext.toLowerCase())) {
			//FIXME ameliorer le controle sur le contenu du fichier
			return true;
		}
		return false;
	}

	public NSArray<String> getExtensionsValids() {
		return EXTENSIONS_VALIDES;
	}

	public void setSrcFile(String filePath) {
		setRjrFile(filePath);
	}

	public void check_fichierAssocieDefini() throws ValidationException {
		if (ERXStringUtilities.stringIsNullOrEmpty(getSrcFile())) {
			throw new NSValidation.ValidationException(REGLE_CHECK_fichierAssocieDefini);
		}
	}

	public void check_typeExportDefini() throws ValidationException {
		if (toRapportJrxmlTexes().count() == 0) {
			throw new NSValidation.ValidationException(REGLE_CHECK_typeExportDefini);
		}
	}

	@Override
	public String toDisplayString() {
		return EOTypeFormatRapport.STR_ID_JRXML + (getSrcFile() != null ? " (" + getSrcFile() + ")" : "") + (toRapport() != null ? toRapport().toDisplayString() : "");
	}
}
