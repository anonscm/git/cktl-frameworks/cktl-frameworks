/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersRapportHisto.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOPersRapportHisto extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOPersRapportHisto.class);

	public static final String ENTITY_NAME = "FwkCktlReport_PersRapportHisto";
	public static final String ENTITY_TABLE_NAME = "cktl_report.PERS_RAPPORT_HISTO";


// Attribute Keys
  public static final ERXKey<NSTimestamp> PRH_END_TIME = new ERXKey<NSTimestamp>("prhEndTime");
  public static final ERXKey<String> PRH_MESSAGE = new ERXKey<String>("prhMessage");
  public static final ERXKey<NSTimestamp> PRH_START_TIME = new ERXKey<NSTimestamp>("prhStartTime");
  public static final ERXKey<String> PRH_STATUS = new ERXKey<String>("prhStatus");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne> TO_PERSONNE = new ERXKey<org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne>("toPersonne");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport> TO_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport>("toRapport");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeExport> TO_TYPE_EXPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOTypeExport>("toTypeExport");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prhId";

	public static final String PRH_END_TIME_KEY = "prhEndTime";
	public static final String PRH_MESSAGE_KEY = "prhMessage";
	public static final String PRH_START_TIME_KEY = "prhStartTime";
	public static final String PRH_STATUS_KEY = "prhStatus";

// Attributs non visibles
	public static final String PERS_ID_KEY = "persId";
	public static final String PRH_ID_KEY = "prhId";
	public static final String RAP_ID_KEY = "rapId";
	public static final String TEX_ID_KEY = "texId";

//Colonnes dans la base de donnees
	public static final String PRH_END_TIME_COLKEY = "PRH_END_TIME";
	public static final String PRH_MESSAGE_COLKEY = "PRH_MESSAGE";
	public static final String PRH_START_TIME_COLKEY = "PRH_START_TIME";
	public static final String PRH_STATUS_COLKEY = "PRH_STATUS";

	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRH_ID_COLKEY = "PRH_ID";
	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String TEX_ID_COLKEY = "TEX_ID";


	// Relationships
	public static final String TO_PERSONNE_KEY = "toPersonne";
	public static final String TO_RAPPORT_KEY = "toRapport";
	public static final String TO_TYPE_EXPORT_KEY = "toTypeExport";



	// Accessors methods
  public NSTimestamp prhEndTime() {
    return (NSTimestamp) storedValueForKey(PRH_END_TIME_KEY);
  }

  public void setPrhEndTime(NSTimestamp value) {
    takeStoredValueForKey(value, PRH_END_TIME_KEY);
  }

  public String prhMessage() {
    return (String) storedValueForKey(PRH_MESSAGE_KEY);
  }

  public void setPrhMessage(String value) {
    takeStoredValueForKey(value, PRH_MESSAGE_KEY);
  }

  public NSTimestamp prhStartTime() {
    return (NSTimestamp) storedValueForKey(PRH_START_TIME_KEY);
  }

  public void setPrhStartTime(NSTimestamp value) {
    takeStoredValueForKey(value, PRH_START_TIME_KEY);
  }

  public String prhStatus() {
    return (String) storedValueForKey(PRH_STATUS_KEY);
  }

  public void setPrhStatus(String value) {
    takeStoredValueForKey(value, PRH_STATUS_KEY);
  }

  public org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne)storedValueForKey(TO_PERSONNE_KEY);
  }

  public void setToPersonneRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne oldValue = toPersonne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PERSONNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PERSONNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EORapport toRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EORapport)storedValueForKey(TO_RAPPORT_KEY);
  }

  public void setToRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EORapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EORapport oldValue = toRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RAPPORT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EOTypeExport toTypeExport() {
    return (org.cocktail.fwkcktlreport.server.metier.EOTypeExport)storedValueForKey(TO_TYPE_EXPORT_KEY);
  }

  public void setToTypeExportRelationship(org.cocktail.fwkcktlreport.server.metier.EOTypeExport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOTypeExport oldValue = toTypeExport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_EXPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_EXPORT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPersRapportHisto avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersRapportHisto createEOPersRapportHisto(EOEditingContext editingContext, NSTimestamp prhStartTime
, org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne toPersonne, org.cocktail.fwkcktlreport.server.metier.EORapport toRapport, org.cocktail.fwkcktlreport.server.metier.EOTypeExport toTypeExport			) {
    EOPersRapportHisto eo = (EOPersRapportHisto) createAndInsertInstance(editingContext, _EOPersRapportHisto.ENTITY_NAME);    
		eo.setPrhStartTime(prhStartTime);
    eo.setToPersonneRelationship(toPersonne);
    eo.setToRapportRelationship(toRapport);
    eo.setToTypeExportRelationship(toTypeExport);
    return eo;
  }

  
	  public EOPersRapportHisto localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersRapportHisto)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportHisto creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersRapportHisto creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPersRapportHisto object = (EOPersRapportHisto)createAndInsertInstance(editingContext, _EOPersRapportHisto.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPersRapportHisto localInstanceIn(EOEditingContext editingContext, EOPersRapportHisto eo) {
    EOPersRapportHisto localInstance = (eo == null) ? null : (EOPersRapportHisto)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersRapportHisto#localInstanceIn a la place.
   */
	public static EOPersRapportHisto localInstanceOf(EOEditingContext editingContext, EOPersRapportHisto eo) {
		return EOPersRapportHisto.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersRapportHisto fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersRapportHisto fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPersRapportHisto> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersRapportHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersRapportHisto)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersRapportHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersRapportHisto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPersRapportHisto> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersRapportHisto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersRapportHisto)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersRapportHisto fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersRapportHisto eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersRapportHisto ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersRapportHisto fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
