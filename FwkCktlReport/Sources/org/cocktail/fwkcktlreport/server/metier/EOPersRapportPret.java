/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlreport.server.CktlReportRapportCriteres;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOPersRapportPret extends _EOPersRapportPret {
	private static final long serialVersionUID = 470965550678155106L;
	public static final EOSortOrdering SORT_RAPPORT_RAP_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EOPersRapportPret.TO_RAPPORT_KEY + "." + EORapport.RAP_LIBELLE_KEY, EOSortOrdering.CompareAscending);

	public EOPersRapportPret() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		setPrpDateModification(DateCtrl.now());
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setPrpDateCreation(DateCtrl.now());
	}

	public static NSArray<EOPersRapportPret> fetchRapportsPretsPourUtilisateur(EOEditingContext editingContext, FwkCktlReportApplicationUser appUser, NSArray<EOSortOrdering> sortOrderings) {
		EOQualifier qual = ERXQ.equals(EOPersRapportPret.PERS_ID_KEY, appUser.getPersId());
		NSArray<EOPersRapportPret> res = fetchAll(editingContext, qual, null);

		return res;
	}

	/**
	 * Crée un rapportPret à partir d'un rapport.
	 * 
	 * @param editingContext
	 * @param personne
	 * @param rapport
	 * @param typeExport Si null, un par défaut est affecté
	 * @param typeFormat Si null, un par défaut est affecté
	 * @param libelle
	 * @param criteres
	 * @return
	 */
	public static EOPersRapportPret create(EOEditingContext editingContext, EOPersonne personne, EORapport rapport, EOTypeExport typeExport, EOTypeFormatRapport typeFormat, String libelle, CktlReportRapportCriteres criteres) {
		EOPersRapportPret res = EOPersRapportPret.creerInstance(editingContext);
		res.setPrpLibelle(libelle);
		res.setToPersonneRelationship(personne);
		res.setToRapportRelationship(rapport);

		if (typeExport == null) {
			NSArray<EOTypeExport> typeExports = rapport.getTypeExports();
			if (typeExports.count() > 0) {
				res.setToTypeExportRelationship(typeExports.objectAtIndex(0));
			}
		}
		else {
			res.setToTypeExportRelationship(typeExport);
		}
		if (typeFormat == null) {
			NSArray<EOTypeFormatRapport> typeFormats = rapport.getTypeFormatRapports();
			if (typeFormats.count() > 0) {
				res.setToTypeFormatRapportRelationship(typeFormats.objectAtIndex(0));
			}
		}
		else {
			res.setToTypeFormatRapportRelationship(typeFormat);
		}

		if (criteres != null) {
			res.addToToPersRapportPretCritsRelationship(criteres);
		}

		//
		//		EOPersRapportPretAbo abo = EOPersRapportPretAbo.creerInstance(editingContext);
		//		abo.setToPersRapportPretRelationship(res);
		//		res.addToToPersRapportPretAbosRelationship(abo);
		return res;
	}

	/**
	 * Affecte des critères en créant les eos correspondant.
	 * 
	 * @param criteres Map de critères, seuls les critères affectés au rapport seront récupérés.
	 */
	public void addToToPersRapportPretCritsRelationship(CktlReportRapportCriteres criteres) {
		//		Map<String, String> preparedCriteres = criteres.preparedMapForSave();
		Map<String, Object> preparedCriteres = criteres;
		NSArray<EORapportCritere> rcs = toRapport().toRapportCriteres();
		Iterator<EORapportCritere> it = rcs.iterator();
		while (it.hasNext()) {
			EORapportCritere rc = (EORapportCritere) it.next();
			Object val = preparedCriteres.get(rc.getCle());
			if (val != null) {
				EOPersRapportPretCrit crit = EOPersRapportPretCrit.createEOPersRapportPretCrit(editingContext(), this, rc);
				crit.setPrpcCritValeur(crit.toRapportCritere().toCritere().convertValeurToString(val));
				addToToPersRapportPretCritsRelationship(crit);
			}
		}
	}

	public EOPersRapportPretAbo toPersRapportPretAbo() {
		if (toPersRapportPretAbos().count() == 0) {
			return null;
		}
		return (EOPersRapportPretAbo) toPersRapportPretAbos().objectAtIndex(0);
	}

	public static void supprimer(EOPersRapportPret prp) {
		if (prp == null) {
			return;
		}
		prp.deleteAllToPersRapportPretAbosRelationships();
		prp.deleteAllToPersRapportPretCritsRelationships();
		prp.editingContext().deleteObject(prp);
	}

	/**
	 * @return Une chaine avec les critères représentés sous forme cle=valeur séparés par des virgules.
	 */
	public String getLesCriteresToDisplay() {
		String res = "";
		NSArray<EOPersRapportPretCrit> crits = toPersRapportPretCrits().immutableClone();
		NSArray<EORapportCritere> crits2 = toRapport().getAllRapportCritereSortedByPosition();

		Iterator<EORapportCritere> it = crits2.iterator();
		while (it.hasNext()) {
			EORapportCritere eoCrit = (EORapportCritere) it.next();
			EOQualifier qual = new EOKeyValueQualifier(EOPersRapportPretCrit.TO_RAPPORT_CRITERE_KEY, EOQualifier.QualifierOperatorEqual, eoCrit);
			NSArray<EOPersRapportPretCrit> prcs = EOQualifier.filteredArrayWithQualifier(crits, qual);
			if (prcs.count() > 0) {
				if (res.length() > 0) {
					res += ", ";
				}
				res += eoCrit.rcrLibelleToDisplay() + "=<b>" + prcs.objectAtIndex(0).prpcCritValeur() + "</b>";
			}
		}
		return res;
	}

}
