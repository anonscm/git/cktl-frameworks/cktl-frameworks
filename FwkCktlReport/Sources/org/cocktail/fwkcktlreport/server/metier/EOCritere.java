/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOCritere extends _EOCritere {

	public static final String STRID_PREFIX = "org.cocktail.cktlreport.critere.";

	public static final String PATTERN_FORMAT_DATE = "dd/MM/yyyy";
	public static final String PATTERN_FORMAT_INTEGER = "# ###";
	public static final String PATTERN_FORMAT_DECIMAL = "# ###.00";
	public static final String PATTERN_FORMAT_NUMBER = PATTERN_FORMAT_DECIMAL;

	public static final EOSortOrdering SORT_CRIT_STR_ID = EOSortOrdering.sortOrderingWithKey(EOCritere.CRIT_STR_ID_KEY, EOSortOrdering.CompareAscending);

	public static final String CRIT_TYPE_SAISIE_COMPOSANT = "COMPOSANT";
	public static final String CRIT_TYPE_SAISIE_POPUP = "POPUP";
	public static final String CRIT_TYPE_SAISIE_LIBRE = "LIBRE";
	public static final String CRIT_TYPE_SAISIE_LIBREETDIALOG = "LIBRE+DIALOG";
	public static final String CRIT_TYPE_SAISIE_DIALOG = "DIALOG";

	public static final String CRIT_TYPE_VALEUR_STRING = "STRING";
	public static final String CRIT_TYPE_VALEUR_NUMBER = "NUMBER";
	public static final String CRIT_TYPE_VALEUR_INTEGER = "INTEGER";
	public static final String CRIT_TYPE_VALEUR_DECIMAL = "DECIMAL";
	public static final String CRIT_TYPE_VALEUR_DATE = "DATE";

	public static final String POPUP_LIST_CLE = "CLE";
	public static final String POPUP_LIST_VALEUR = "VALEUR";

	/**
	 * Les clés pour les listes de popup.
	 */
	public static final NSArray POPUP_KEYS = new NSArray(new Object[] {
			POPUP_LIST_CLE, POPUP_LIST_VALEUR
	});

	public EOCritere() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public Boolean isComposant() {
		return CRIT_TYPE_SAISIE_COMPOSANT.equals(critTypeSaisie());
	}

	public Boolean isPopup() {
		return CRIT_TYPE_SAISIE_POPUP.equals(critTypeSaisie());
	}

	public Boolean isDialog() {
		return CRIT_TYPE_SAISIE_DIALOG.equals(critTypeSaisie());
	}

	public Boolean isLibre() {
		return CRIT_TYPE_SAISIE_LIBRE.equals(critTypeSaisie());
	}

	public Boolean isLibreEtDialog() {
		return CRIT_TYPE_SAISIE_LIBREETDIALOG.equals(critTypeSaisie());
	}

	public Boolean isLibreOuLibreEtDialog() {
		return isLibre() || isLibreEtDialog();
	}

	public Boolean isValeurString() {
		return CRIT_TYPE_VALEUR_STRING.equals(critTypeValeur());
	}

	public Boolean isValeurDate() {
		return CRIT_TYPE_VALEUR_DATE.equals(critTypeValeur());
	}

	public Boolean isValeurInteger() {
		return CRIT_TYPE_VALEUR_INTEGER.equals(critTypeValeur());
	}

	public Boolean isValeurDecimal() {
		return CRIT_TYPE_VALEUR_DECIMAL.equals(critTypeValeur());
	}

	public Boolean isValeurNumber() {
		return CRIT_TYPE_VALEUR_NUMBER.equals(critTypeValeur());
	}

	public Boolean isMetadataSql() {
		String sql = StringCtrl.normalize(critMetadata());
		sql = sql.toUpperCase();
		if (sql.startsWith("SELECT ")) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public String strIdAndLibelle() {
		return critStrId() + " (" + critLibelle() + ")";
	}

	/**
	 * @return Une version "nettoyée" de strId (sans le
	 */
	public String cleanStrIdAndLibelle() {
		return cleanStrId() + " (" + critLibelle() + ")";
	}

	public String libelleAndCleanString() {
		return critLibelle() + " (" + cleanStrId() + ")";
	}

	/**
	 * @return Une version "nettoyée" de strId (sans le debuut si celui-ci commence par org.cocktail.)
	 */
	public String cleanStrId() {
		if (critStrId() != null && critStrId().startsWith(STRID_PREFIX)) {
			return critStrId().substring(STRID_PREFIX.length());
		}
		return critStrId();
	}

	public Object convertStringToValeur(String s) {
		Object res = null;
		if (s != null) {
			if (isValeurDate()) {
				try {
					Date d = new SimpleDateFormat(PATTERN_FORMAT_DATE).parse(s);
					res = new NSTimestamp(d);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (isValeurDecimal() || isValeurNumber()) {
				try {
					Number d = new DecimalFormat(PATTERN_FORMAT_DECIMAL).parse(s);
					res = d;
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (isValeurInteger()) {
				try {
					Number d = NumberFormat.getIntegerInstance().parse(s);
					res = Integer.valueOf(d.intValue());
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			else if (isValeurString()) {
				res = s;
			}
			else {
				res = s;
			}
		}
		return res;

	}

	public String convertValeurToString(Object val) {
		String res = null;
		if (val != null) {
			if (isValeurDate()) {
				res = new SimpleDateFormat(PATTERN_FORMAT_DATE).format(val);
			}
			else if (isValeurDecimal() || isValeurNumber()) {
				res = new DecimalFormat(PATTERN_FORMAT_DECIMAL).format(val);
			}
			else if (isValeurInteger()) {
				res = new DecimalFormat(PATTERN_FORMAT_INTEGER).format(val);
			}
			else {
				res = val.toString();
			}
		}
		return res;
	}

}
