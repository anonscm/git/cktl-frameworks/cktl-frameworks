/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORapportCritere.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EORapportCritere extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EORapportCritere.class);

	public static final String ENTITY_NAME = "FwkCktlReport_RapportCritere";
	public static final String ENTITY_TABLE_NAME = "cktl_report.RAPPORT_CRITERE";


// Attribute Keys
  public static final ERXKey<String> RCR_CLE = new ERXKey<String>("rcrCle");
  public static final ERXKey<String> RCR_COMMENTAIRE = new ERXKey<String>("rcrCommentaire");
  public static final ERXKey<String> RCR_LIBELLE = new ERXKey<String>("rcrLibelle");
  public static final ERXKey<String> RCR_OBLIGATOIRE = new ERXKey<String>("rcrObligatoire");
  public static final ERXKey<Integer> RCR_POSITION = new ERXKey<Integer>("rcrPosition");
  public static final ERXKey<String> RCR_STR_ID = new ERXKey<String>("rcrStrId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCritere> TO_CRITERE = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCritere>("toCritere");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> TO_PERS_RAPPORT_PRET_CRITS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>("toPersRapportPretCrits");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport> TO_RAPPORT = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapport>("toRapport");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "rcrId";

	public static final String RCR_CLE_KEY = "rcrCle";
	public static final String RCR_COMMENTAIRE_KEY = "rcrCommentaire";
	public static final String RCR_LIBELLE_KEY = "rcrLibelle";
	public static final String RCR_OBLIGATOIRE_KEY = "rcrObligatoire";
	public static final String RCR_POSITION_KEY = "rcrPosition";
	public static final String RCR_STR_ID_KEY = "rcrStrId";

// Attributs non visibles
	public static final String CRIT_ID_KEY = "critId";
	public static final String RAP_ID_KEY = "rapId";
	public static final String RCR_ID_KEY = "rcrId";

//Colonnes dans la base de donnees
	public static final String RCR_CLE_COLKEY = "RCR_CLE";
	public static final String RCR_COMMENTAIRE_COLKEY = "RCR_COMMENTAIRE";
	public static final String RCR_LIBELLE_COLKEY = "RCR_LIBELLE";
	public static final String RCR_OBLIGATOIRE_COLKEY = "RCR_OBLIGATOIRE";
	public static final String RCR_POSITION_COLKEY = "RCR_POSITION";
	public static final String RCR_STR_ID_COLKEY = "RCR_STR_ID";

	public static final String CRIT_ID_COLKEY = "CRIT_ID";
	public static final String RAP_ID_COLKEY = "RAP_ID";
	public static final String RCR_ID_COLKEY = "RCR_ID";


	// Relationships
	public static final String TO_CRITERE_KEY = "toCritere";
	public static final String TO_PERS_RAPPORT_PRET_CRITS_KEY = "toPersRapportPretCrits";
	public static final String TO_RAPPORT_KEY = "toRapport";



	// Accessors methods
  public String rcrCle() {
    return (String) storedValueForKey(RCR_CLE_KEY);
  }

  public void setRcrCle(String value) {
    takeStoredValueForKey(value, RCR_CLE_KEY);
  }

  public String rcrCommentaire() {
    return (String) storedValueForKey(RCR_COMMENTAIRE_KEY);
  }

  public void setRcrCommentaire(String value) {
    takeStoredValueForKey(value, RCR_COMMENTAIRE_KEY);
  }

  public String rcrLibelle() {
    return (String) storedValueForKey(RCR_LIBELLE_KEY);
  }

  public void setRcrLibelle(String value) {
    takeStoredValueForKey(value, RCR_LIBELLE_KEY);
  }

  public String rcrObligatoire() {
    return (String) storedValueForKey(RCR_OBLIGATOIRE_KEY);
  }

  public void setRcrObligatoire(String value) {
    takeStoredValueForKey(value, RCR_OBLIGATOIRE_KEY);
  }

  public Integer rcrPosition() {
    return (Integer) storedValueForKey(RCR_POSITION_KEY);
  }

  public void setRcrPosition(Integer value) {
    takeStoredValueForKey(value, RCR_POSITION_KEY);
  }

  public String rcrStrId() {
    return (String) storedValueForKey(RCR_STR_ID_KEY);
  }

  public void setRcrStrId(String value) {
    takeStoredValueForKey(value, RCR_STR_ID_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOCritere toCritere() {
    return (org.cocktail.fwkcktlreport.server.metier.EOCritere)storedValueForKey(TO_CRITERE_KEY);
  }

  public void setToCritereRelationship(org.cocktail.fwkcktlreport.server.metier.EOCritere value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOCritere oldValue = toCritere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CRITERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CRITERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlreport.server.metier.EORapport toRapport() {
    return (org.cocktail.fwkcktlreport.server.metier.EORapport)storedValueForKey(TO_RAPPORT_KEY);
  }

  public void setToRapportRelationship(org.cocktail.fwkcktlreport.server.metier.EORapport value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EORapport oldValue = toRapport();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RAPPORT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RAPPORT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)storedValueForKey(TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier) {
    return toPersRapportPretCrits(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier, boolean fetch) {
    return toPersRapportPretCrits(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> toPersRapportPretCrits(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit.TO_RAPPORT_CRITERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPersRapportPretCrits();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public void removeFromToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit createToPersRapportPretCritsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_PersRapportPretCrit");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PERS_RAPPORT_PRET_CRITS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit) eo;
  }

  public void deleteToPersRapportPretCritsRelationship(org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PERS_RAPPORT_PRET_CRITS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPersRapportPretCritsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit> objects = toPersRapportPretCrits().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPersRapportPretCritsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EORapportCritere avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EORapportCritere createEORapportCritere(EOEditingContext editingContext, String rcrObligatoire
, Integer rcrPosition
, org.cocktail.fwkcktlreport.server.metier.EOCritere toCritere, org.cocktail.fwkcktlreport.server.metier.EORapport toRapport			) {
    EORapportCritere eo = (EORapportCritere) createAndInsertInstance(editingContext, _EORapportCritere.ENTITY_NAME);    
		eo.setRcrObligatoire(rcrObligatoire);
		eo.setRcrPosition(rcrPosition);
    eo.setToCritereRelationship(toCritere);
    eo.setToRapportRelationship(toRapport);
    return eo;
  }

  
	  public EORapportCritere localInstanceIn(EOEditingContext editingContext) {
	  		return (EORapportCritere)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportCritere creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EORapportCritere creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EORapportCritere object = (EORapportCritere)createAndInsertInstance(editingContext, _EORapportCritere.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EORapportCritere localInstanceIn(EOEditingContext editingContext, EORapportCritere eo) {
    EORapportCritere localInstance = (eo == null) ? null : (EORapportCritere)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EORapportCritere#localInstanceIn a la place.
   */
	public static EORapportCritere localInstanceOf(EOEditingContext editingContext, EORapportCritere eo) {
		return EORapportCritere.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCritere>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EORapportCritere fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORapportCritere fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EORapportCritere> eoObjects = fetchAll(editingContext, qualifier, null);
	    EORapportCritere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EORapportCritere)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EORapportCritere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EORapportCritere fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EORapportCritere> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EORapportCritere eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EORapportCritere)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EORapportCritere fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EORapportCritere eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EORapportCritere ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EORapportCritere fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
