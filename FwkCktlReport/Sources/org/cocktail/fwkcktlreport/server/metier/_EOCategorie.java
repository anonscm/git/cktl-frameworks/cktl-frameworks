/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCategorie.java instead.
package org.cocktail.fwkcktlreport.server.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlreport.server.serialize.ICktlReportXmlCodingAdditions;;

public abstract class _EOCategorie extends  AfwkCktlReportRecord implements ICktlReportXmlCodingAdditions {
//	private static Logger logger = Logger.getLogger(_EOCategorie.class);

	public static final String ENTITY_NAME = "FwkCktlReport_Categorie";
	public static final String ENTITY_TABLE_NAME = "cktl_report.CATEGORIE";


// Attribute Keys
  public static final ERXKey<String> CAT_COMMENTAIRES = new ERXKey<String>("catCommentaires");
  public static final ERXKey<String> CAT_LIBELLE = new ERXKey<String>("catLibelle");
  public static final ERXKey<String> CAT_LOCAL = new ERXKey<String>("catLocal");
  public static final ERXKey<Integer> CAT_NIVEAU = new ERXKey<Integer>("catNiveau");
  public static final ERXKey<String> CAT_STR_ID = new ERXKey<String>("catStrId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCritere> CRITERES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCritere>("criteres");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie> TO_CATEGORIE_ENFANTS = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie>("toCategorieEnfants");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie> TO_CATEGORIE_PERE = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EOCategorie>("toCategoriePere");
  public static final ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> TO_RAPPORT_CATEGORIES = new ERXKey<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>("toRapportCategories");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "catId";

	public static final String CAT_COMMENTAIRES_KEY = "catCommentaires";
	public static final String CAT_LIBELLE_KEY = "catLibelle";
	public static final String CAT_LOCAL_KEY = "catLocal";
	public static final String CAT_NIVEAU_KEY = "catNiveau";
	public static final String CAT_STR_ID_KEY = "catStrId";

// Attributs non visibles
	public static final String CAT_ID_KEY = "catId";
	public static final String CAT_ID_PERE_KEY = "catIdPere";

//Colonnes dans la base de donnees
	public static final String CAT_COMMENTAIRES_COLKEY = "CAT_COMMENTAIRES";
	public static final String CAT_LIBELLE_COLKEY = "CAT_LIBELLE";
	public static final String CAT_LOCAL_COLKEY = "CAT_LOCAL";
	public static final String CAT_NIVEAU_COLKEY = "CAT_NIVEAU";
	public static final String CAT_STR_ID_COLKEY = "CAT_STR_ID";

	public static final String CAT_ID_COLKEY = "CAT_ID";
	public static final String CAT_ID_PERE_COLKEY = "CAT_ID_PERE";


	// Relationships
	public static final String CRITERES_KEY = "criteres";
	public static final String TO_CATEGORIE_ENFANTS_KEY = "toCategorieEnfants";
	public static final String TO_CATEGORIE_PERE_KEY = "toCategoriePere";
	public static final String TO_RAPPORT_CATEGORIES_KEY = "toRapportCategories";



	// Accessors methods
  public String catCommentaires() {
    return (String) storedValueForKey(CAT_COMMENTAIRES_KEY);
  }

  public void setCatCommentaires(String value) {
    takeStoredValueForKey(value, CAT_COMMENTAIRES_KEY);
  }

  public String catLibelle() {
    return (String) storedValueForKey(CAT_LIBELLE_KEY);
  }

  public void setCatLibelle(String value) {
    takeStoredValueForKey(value, CAT_LIBELLE_KEY);
  }

  public String catLocal() {
    return (String) storedValueForKey(CAT_LOCAL_KEY);
  }

  public void setCatLocal(String value) {
    takeStoredValueForKey(value, CAT_LOCAL_KEY);
  }

  public Integer catNiveau() {
    return (Integer) storedValueForKey(CAT_NIVEAU_KEY);
  }

  public void setCatNiveau(Integer value) {
    takeStoredValueForKey(value, CAT_NIVEAU_KEY);
  }

  public String catStrId() {
    return (String) storedValueForKey(CAT_STR_ID_KEY);
  }

  public void setCatStrId(String value) {
    takeStoredValueForKey(value, CAT_STR_ID_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOCategorie toCategoriePere() {
    return (org.cocktail.fwkcktlreport.server.metier.EOCategorie)storedValueForKey(TO_CATEGORIE_PERE_KEY);
  }

  public void setToCategoriePereRelationship(org.cocktail.fwkcktlreport.server.metier.EOCategorie value) {
    if (value == null) {
    	org.cocktail.fwkcktlreport.server.metier.EOCategorie oldValue = toCategoriePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CATEGORIE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CATEGORIE_PERE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> criteres() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere>)storedValueForKey(CRITERES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> criteres(EOQualifier qualifier) {
    return criteres(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> criteres(EOQualifier qualifier, boolean fetch) {
    return criteres(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> criteres(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOCritere.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOCritere.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = criteres();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCritere>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EOCritere object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CRITERES_KEY);
  }

  public void removeFromCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EOCritere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CRITERES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOCritere createCriteresRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_Critere");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CRITERES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOCritere) eo;
  }

  public void deleteCriteresRelationship(org.cocktail.fwkcktlreport.server.metier.EOCritere object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CRITERES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCriteresRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOCritere> objects = criteres().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCriteresRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> toCategorieEnfants() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie>)storedValueForKey(TO_CATEGORIE_ENFANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> toCategorieEnfants(EOQualifier qualifier) {
    return toCategorieEnfants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> toCategorieEnfants(EOQualifier qualifier, boolean fetch) {
    return toCategorieEnfants(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> toCategorieEnfants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EOCategorie.TO_CATEGORIE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EOCategorie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toCategorieEnfants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToCategorieEnfantsRelationship(org.cocktail.fwkcktlreport.server.metier.EOCategorie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_CATEGORIE_ENFANTS_KEY);
  }

  public void removeFromToCategorieEnfantsRelationship(org.cocktail.fwkcktlreport.server.metier.EOCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CATEGORIE_ENFANTS_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EOCategorie createToCategorieEnfantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_Categorie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_CATEGORIE_ENFANTS_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EOCategorie) eo;
  }

  public void deleteToCategorieEnfantsRelationship(org.cocktail.fwkcktlreport.server.metier.EOCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_CATEGORIE_ENFANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToCategorieEnfantsRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EOCategorie> objects = toCategorieEnfants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToCategorieEnfantsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories() {
    return (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)storedValueForKey(TO_RAPPORT_CATEGORIES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier) {
    return toRapportCategories(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier, boolean fetch) {
    return toRapportCategories(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> toRapportCategories(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlreport.server.metier.EORapportCategorie.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRapportCategories();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
  }

  public void removeFromToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
  }

  public org.cocktail.fwkcktlreport.server.metier.EORapportCategorie createToRapportCategoriesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlReport_RapportCategorie");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_RAPPORT_CATEGORIES_KEY);
    return (org.cocktail.fwkcktlreport.server.metier.EORapportCategorie) eo;
  }

  public void deleteToRapportCategoriesRelationship(org.cocktail.fwkcktlreport.server.metier.EORapportCategorie object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_RAPPORT_CATEGORIES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRapportCategoriesRelationships() {
    Enumeration<org.cocktail.fwkcktlreport.server.metier.EORapportCategorie> objects = toRapportCategories().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRapportCategoriesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCategorie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCategorie createEOCategorie(EOEditingContext editingContext, String catLibelle
, String catLocal
, Integer catNiveau
, String catStrId
			) {
    EOCategorie eo = (EOCategorie) createAndInsertInstance(editingContext, _EOCategorie.ENTITY_NAME);    
		eo.setCatLibelle(catLibelle);
		eo.setCatLocal(catLocal);
		eo.setCatNiveau(catNiveau);
		eo.setCatStrId(catStrId);
    return eo;
  }

  
	  public EOCategorie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCategorie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCategorie creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCategorie creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOCategorie object = (EOCategorie)createAndInsertInstance(editingContext, _EOCategorie.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOCategorie localInstanceIn(EOEditingContext editingContext, EOCategorie eo) {
    EOCategorie localInstance = (eo == null) ? null : (EOCategorie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCategorie#localInstanceIn a la place.
   */
	public static EOCategorie localInstanceOf(EOEditingContext editingContext, EOCategorie eo) {
		return EOCategorie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie> eoObjects = (NSArray<org.cocktail.fwkcktlreport.server.metier.EOCategorie>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCategorie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCategorie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOCategorie> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCategorie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCategorie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCategorie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCategorie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOCategorie> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCategorie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCategorie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCategorie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCategorie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCategorie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCategorie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
	
	
  
}
