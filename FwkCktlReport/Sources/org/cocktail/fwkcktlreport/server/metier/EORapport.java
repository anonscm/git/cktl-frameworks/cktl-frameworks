/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.metier;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.FwkCktlReport;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlreport.server.files.CktlReportFileAction;
import org.cocktail.fwkcktlreport.server.files.CktlReportFilesUtils;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXStringUtilities;
import er.extensions.foundation.ERXThreadStorage;

/**
 * Initialisez appUser avant d'effectuer des modifications sur un objet EORapport.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class EORapport extends _EORapport {

	public final static String REGLE_CHECK_modeleDefini = "Vous devez définir au moins un modèle pour le rapport.";
	public final static String REGLE_CHECK_libelleDefini = "Le libellé est obligatoire.";
	public final static String REGLE_CHECK_strIdDefini = "L'identifiant est obligatoire.";
	public final static String REGLE_CHECK_categorieAssociee = "Vous devez associer au moins une catégorie au rapport.";

	public static final EOSortOrdering SORT_RAP_LIBELLE_ASC = EOSortOrdering.sortOrderingWithKey(EORapport.RAP_LIBELLE_KEY, EOSortOrdering.CompareAscending);
	public static final NSArray<EOSortOrdering> SORT_ARRAY_RAP_LIBELLE_ASC = new NSArray<EOSortOrdering>(new EOSortOrdering[] {
			EORapport.SORT_RAP_LIBELLE_ASC
	});

	//	private static NSArray<String> PREFETCH_KEYPATHS = new NSArray<String>(new String[] {
	//			EORapport.TO_RAPPORT_CRITERES_KEY, EORapport.TO_RAPPORT_JRXMLS_KEY, EORapport.TO_RAPPORT_JXLSS_KEY, EORapport.TO_TYPE_ETAT_KEY
	//	});

	public static String GETALLRAPPORTCRITERESORTEDBYPOSITION_KEY = "getAllRapportCritereSortedByPosition";

	//private NSMutableArray rapportCritereTemp = new NSMutableArray();
	private NSArray<String> rapportTypeExportsExtensionsCache;

	private NSArray<EOTypeExport> rapportTypeExportsCache;

	private Boolean isFavorisCache = Boolean.FALSE;
	private String newAttachedDirectoryName = null;
	private String actualAttachedDirectoryName;
	private boolean autoPerformDirectoryAction = true;
	private FwkCktlReportApplicationUser appUser;

	private NSArray<String> erreurs = new NSArray<String>();

	public EORapport() {
		super();
	}

	@Override
	/**
	 * Appelez plutot setAppUser.
	 */
	public void setPersIdModification(Integer value) {
		super.setPersIdModification(value);
		if (persIdCreation() == null) {
			setPersIdCreation(value);
		}
	}

	//	public NSArray rapportCriterePretsTemp() {
	//		return rapportCritereTemp.immutableClone();
	//	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
		setPersIdModification(getAppUser().getPersId());
		if (dateCreation() == null) {
			setDateCreation(new NSTimestamp());
		}
		if (rapLocal() == null) {
			setRapLocal("O");
		}
		try {
			check_modeleDefini();
			check_libelleDefini();
			check_strIdDefini();
			check_libelleDoublon();
			check_strIdDoublon();
			check_categorieAssociee();
		} catch (Exception e) {
			throw new NSValidation.ValidationException(dressExceptionMsg(e));
		}

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
		rapportTypeExportsCache = null;
		rapportTypeExportsExtensionsCache = null;
	}

	/**
	 * @param edc
	 * @param appUser
	 * @param sortOrderings
	 * @return Les rapports autorisés pour un utilisateur en fonction des ses droits sur les categories.
	 */
	public static NSArray<EORapport> fetchAllRapportsAutorisesPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAllRapportsAutorisesPourUtilisateur(edc, appUser, null, sortOrderings);
	}

	public static NSArray<EORapport> fetchAllRapportsAutorisesPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOQualifier qual, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAllRapportsAutorisesPourUtilisateur(edc, appUser, qual, sortOrderings, -1);
	}

	public static NSArray<EORapport> fetchAllRapportsAutorisesPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOQualifier qual, NSArray<EOSortOrdering> sortOrderings, int fetchLimit) {
		NSArray<EORapport> res = NSArray.emptyArray();
		NSMutableArray<EOQualifier> qualsRes = new NSMutableArray<EOQualifier>();
		NSArray<EOQualifier> quals = buildQualifierForCategoriesAutorisees(appUser);
		if (quals.count() > 0) {
			qualsRes.add(new EOOrQualifier(quals));
			if (qual != null) {
				qualsRes.add(qual);
			}
			res = EORapport.fetchAll(edc, new EOAndQualifier(qualsRes), sortOrderings, fetchLimit);
		}

		return res;
	}

	public static NSArray<EORapport> fetchAllRapportsAutorisesPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, String rapLibelle, Integer fetchLimit) {
		NSMutableArray<EORapport> res = new NSMutableArray<EORapport>();
		if (fetchLimit == null) {
			fetchLimit = Integer.valueOf(-1);
		}
		if (!MyStringCtrl.isEmpty(rapLibelle)) {
			res.addObjectsFromArray(fetchAllRapportsAutorisesPourUtilisateur(edc, appUser, ERXQ.startsWithInsensitive(EORapport.RAP_LIBELLE_KEY, rapLibelle), SORT_ARRAY_RAP_LIBELLE_ASC, fetchLimit));
			if (res.count() < fetchLimit) {
				res.addObjectsFromArray(fetchAllRapportsAutorisesPourUtilisateur(edc, appUser, ERXQ.contains(EORapport.RAP_LIBELLE_KEY, rapLibelle), SORT_ARRAY_RAP_LIBELLE_ASC, fetchLimit - res.count()));
			}
		}
		return NSArrayCtrl.getDistinctsOfNSArray(res);
	}

	public static NSArray<EORapport> fetchAllRapportsAdministresPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAllRapportsAdministresPourUtilisateur(edc, appUser, null, sortOrderings);
	}

	public static NSArray<EORapport> fetchAllRapportsAdministresPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOQualifier qual, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAllRapportsAdministresPourUtilisateur(edc, appUser, qual, sortOrderings, -1);
	}

	public static NSArray<EORapport> fetchAllRapportsAdministresPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOQualifier qual, NSArray<EOSortOrdering> sortOrderings, int fetchLimit) {
		NSArray<EORapport> res = NSArray.emptyArray();
		NSMutableArray<EOQualifier> qualsRes = new NSMutableArray<EOQualifier>();
		NSArray<EOQualifier> quals = buildQualifierForCategoriesAdministrees(appUser);
		if (quals.count() > 0) {
			qualsRes.add(new EOOrQualifier(quals));
			if (qual != null) {
				qualsRes.add(qual);
			}
			res = EORapport.fetchAll(edc, new EOAndQualifier(qualsRes), sortOrderings, fetchLimit);
		}

		return res;
	}

	public static NSArray<EORapport> fetchAllRapportsAdministresPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, String rapLibelle, Integer fetchLimit) {
		NSMutableArray<EORapport> res = new NSMutableArray<EORapport>();
		if (fetchLimit == null) {
			fetchLimit = Integer.valueOf(-1);
		}
		if (!MyStringCtrl.isEmpty(rapLibelle)) {
			res.addObjectsFromArray(fetchAllRapportsAdministresPourUtilisateur(edc, appUser, ERXQ.startsWithInsensitive(EORapport.RAP_LIBELLE_KEY, rapLibelle), SORT_ARRAY_RAP_LIBELLE_ASC, fetchLimit));
			if (res.count() < fetchLimit) {
				res.addObjectsFromArray(fetchAllRapportsAdministresPourUtilisateur(edc, appUser, ERXQ.contains(EORapport.RAP_LIBELLE_KEY, rapLibelle), SORT_ARRAY_RAP_LIBELLE_ASC, fetchLimit - res.count()));
			}
		}
		return NSArrayCtrl.getDistinctsOfNSArray(res);
	}

	private static NSArray<EOQualifier> buildQualifierForCategoriesAutorisees(FwkCktlReportApplicationUser appUser) {
		NSArray<EOCategorie> catAutorisees = appUser.getCategoriesAutorisees();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

		for (int i = 0; i < catAutorisees.count(); i++) {
			EOCategorie cat = (EOCategorie) catAutorisees.objectAtIndex(i);
			quals.addObject(new EOKeyValueQualifier(EORapport.TO_RAPPORT_CATEGORIES_KEY + "." + EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, cat));
		}
		return quals.immutableClone();
	}

	private static NSArray<EOQualifier> buildQualifierForCategoriesAdministrees(FwkCktlReportApplicationUser appUser) {
		NSArray<EOCategorie> catAutorisees = appUser.getCategoriesAdministrees();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();

		for (int i = 0; i < catAutorisees.count(); i++) {
			EOCategorie cat = (EOCategorie) catAutorisees.objectAtIndex(i);
			quals.addObject(new EOKeyValueQualifier(EORapport.TO_RAPPORT_CATEGORIES_KEY + "." + EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, cat));
		}
		return quals.immutableClone();
	}

	/**
	 * @param edc
	 * @param appUser
	 * @param sortOrderings
	 * @return Les rapport favoris d'un utilisateur en tenant compte de ses droits sur les categories.
	 */
	public static NSArray<EORapport> fetchAllRapportsFavorisPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EORapport> res = NSArray.emptyArray();
		NSMutableArray<EOQualifier> quals = new NSMutableArray<EOQualifier>();
		quals.add(new EOOrQualifier(buildQualifierForCategoriesAutorisees(appUser)));
		quals.add(new EOKeyValueQualifier(EORapport.TO_PERS_RAPPORT_FAVORISS_KEY + "." + EOPersRapportFavoris.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, appUser.getPersId()));
		EOQualifier qual = new EOOrQualifier(quals);
		res = EORapport.fetchAll(edc, qual, sortOrderings);
		Iterator<EORapport> it = res.iterator();
		while (it.hasNext()) {
			EORapport rap = it.next();
			rap.setIsFavorisCache(Boolean.TRUE);
		}
		return res;
	}

	/**
	 * Tous les rapports executables par l'utilisateur pour une catégorie en fonction de ses droits.
	 * 
	 * @param edc
	 * @param utilisateurPersId
	 * @param categorie
	 * @param sortOrderings
	 * @return
	 */
	public static NSArray<EORapport> fetchRapportsAutorisesPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOCategorie categorie, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EORapport> res = NSArray.emptyArray();
		//recup utilisateur et categories autorisées
		if (categorie != null) {
			NSArray<EOCategorie> catAutorisees = appUser.getCategoriesAutorisees();
			if (catAutorisees.containsObject(categorie)) {
				EOQualifier qual = new EOKeyValueQualifier(EORapport.TO_RAPPORT_CATEGORIES_KEY + "." + EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, categorie);
				res = EORapport.fetchAll(edc, qual, sortOrderings, true);
			}
		}
		return res;
	}

	public static NSArray<EORapport> fetchRapportsAdministresPourUtilisateur(EOEditingContext edc, FwkCktlReportApplicationUser appUser, EOCategorie categorie, NSArray<EOSortOrdering> sortOrderings) {
		NSArray<EORapport> res = NSArray.emptyArray();
		//recup utilisateur et categories administrees
		if (categorie != null) {
			NSArray<EOCategorie> catAutorisees = appUser.getCategoriesAdministrees();
			if (catAutorisees.containsObject(categorie)) {
				EOQualifier qual = new EOKeyValueQualifier(EORapport.TO_RAPPORT_CATEGORIES_KEY + "." + EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, categorie);
				res = EORapport.fetchAll(edc, qual, sortOrderings);
			}
		}
		return res;
	}

	public NSArray<EOTypeExport> getTypeExportsCache() {
		if (rapportTypeExportsCache == null) {
			rapportTypeExportsCache = getTypeExports();
		}
		return rapportTypeExportsCache;
	}

	public NSArray<EOTypeExport> getTypeExports() {
		NSMutableArray<EOTypeExport> res = new NSMutableArray<EOTypeExport>();
		Iterator<IRapportExt> it = getRapportExts().iterator();
		while (it.hasNext()) {
			IRapportExt iRapportExt = (IRapportExt) it.next();
			res.addObjectsFromArray(iRapportExt.getTypeExports());
		}
		return NSArrayCtrl.getDistinctsOfNSArray(res);
	}

	@SuppressWarnings("unchecked")
	public NSArray<IRapportExt> getRapportExts() {
		@SuppressWarnings("rawtypes")
		ArrayList res = new ArrayList<NSArray<? extends IRapportExt>>();
		res.add(toRapportJrxmls());
		res.add(toRapportJxlss());

		return NSArrayCtrl.unionOfNSArrays(res);

		//		return NSArrayCtrl.unionOfNSArrays(new NSArray<NSArray<? extends IRapportExt>>[] {
		//				toRapportJrxmls(), toRapportJxlss()
		//		});
	}

	/**
	 * @return Un tableau contenant les extensions possibles du rapport.
	 */
	@SuppressWarnings("unchecked")
	public NSArray<String> getExtensionsFortypeExport() {
		if (rapportTypeExportsExtensionsCache == null) {
			rapportTypeExportsExtensionsCache = (NSArray<String>) getTypeExports().valueForKey(EOTypeExport.TEX_EXTENSION_KEY);
		}
		return rapportTypeExportsExtensionsCache;
	}

	public void setIsFavorisCache(Boolean val) {
		isFavorisCache = val;
	}

	public Boolean isFavorisCache() {
		return isFavorisCache;
	}

	/**
	 * Supprime le rapport des favoris pour un utilisateur
	 * 
	 * @param utilisateurPersId
	 */
	public void deleteFromFavoris(Integer utilisateurPersId) {
		EOPersonne pers = EOPersonne.fetchRequiredByKeyValue(editingContext(), EOPersonne.PERS_ID_KEY, utilisateurPersId);

		NSArray<EOPersRapportFavoris> res = toPersRapportFavoriss(new EOKeyValueQualifier(EOPersRapportFavoris.TO_PERSONNE_KEY, EOQualifier.QualifierOperatorEqual, pers));

		for (int i = res.count() - 1; i >= 0; i--) {
			EOPersRapportFavoris obj = (EOPersRapportFavoris) res.objectAtIndex(i);
			obj.setToPersonneRelationship(null);
			obj.setToRapportRelationship(null);
			editingContext().deleteObject(obj);
		}
		this.setIsFavorisCache(Boolean.FALSE);
	}

	/**
	 * Ajoute le rapport aux fovoris pour l'utilisateur
	 * 
	 * @param utilisateurPersId
	 */
	public void addToFavoris(Integer utilisateurPersId) {
		NSArray<EOPersRapportFavoris> res = toPersRapportFavoriss(new EOKeyValueQualifier(EOPersRapportFavoris.PERS_ID_KEY, EOQualifier.QualifierOperatorEqual, utilisateurPersId));
		if (res.count() == 0) {
			EOPersonne pers = EOPersonne.fetchRequiredByKeyValue(editingContext(), EOPersonne.PERS_ID_KEY, utilisateurPersId);
			if (pers != null) {
				EOPersRapportFavoris obj = EOPersRapportFavoris.createEOPersRapportFavoris(editingContext(), pers, this);
			}
			this.setIsFavorisCache(Boolean.TRUE);
		}
	}

	/**
	 * @return une version "nettoyee" du str_id.
	 */
	public String rapStrIdBasic() {
		return StringCtrl.toBasicString(rapStrId(), null, '_');
	}

	public EORapportJrxml toRapportJrxml() {
		if (toRapportJrxmls().count() > 0) {
			return (EORapportJrxml) toRapportJrxmls().objectAtIndex(0);
		}
		return null;
	}

	public EORapportJxls toRapportJxls() {
		if (toRapportJxlss().count() > 0) {
			return (EORapportJxls) toRapportJxlss().objectAtIndex(0);
		}
		return null;
	}

	public NSArray<EORapportCritere> getRapportCritereObligatoires() {
		return toRapportCriteres(EORapportCritere.QUAL_OBLIGATOIRE);
	}

	public NSArray<EORapportCritere> getAllRapportCritereSortedByPosition() {
		return toRapportCriteres(null, new NSArray<EOSortOrdering>(new EOSortOrdering[] {
				EORapportCritere.SORT_RCR_POSITION_ASC
		}), false);
	}

	@SuppressWarnings("unchecked")
	public static NSArray<EORapport> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, int fetchLimit) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		if (fetchLimit > 0) {
			fetchSpec.setFetchLimit(fetchLimit);
		}
		//fetchSpec.setPrefetchingRelationshipKeyPaths(PREFETCH_KEYPATHS);
		NSArray<EORapport> eoObjects = (NSArray<EORapport>) editingContext.objectsWithFetchSpecification(fetchSpec);
		return eoObjects;
	}

	public NSArray<EOTypeFormatRapport> getTypeFormatRapports() {
		NSMutableArray<EOTypeFormatRapport> res = new NSMutableArray<EOTypeFormatRapport>();
		Iterator<IRapportExt> it = getRapportExts().iterator();
		while (it.hasNext()) {
			IRapportExt iRapportExt = (IRapportExt) it.next();
			res.addObject(iRapportExt.toTypeFormatRapport());
		}
		return res.immutableClone();
	}

	public EORapportCritere getRapportCritereAtPosition(Integer pos) {
		if (pos != null) {
			EOQualifier qual = new EOKeyValueQualifier(EORapportCritere.RCR_POSITION_KEY, EOQualifier.QualifierOperatorEqual, pos);
			NSArray<EORapportCritere> res = toRapportCriteres(qual);
			if (res.count() > 0) {
				return res.objectAtIndex(0);
			}
		}
		return null;
	}

	public void switchRapportCriteresPositions(EORapportCritere rc1, EORapportCritere rc2) {
		Integer oldPos = rc1.rcrPosition();
		Integer newPos = rc2.rcrPosition();
		rc1.setRcrPosition(newPos);
		rc2.setRcrPosition(oldPos);
	}

	@Override
	public EORapportCritere createToRapportCriteresRelationship() {
		EORapportCritere res = super.createToRapportCriteresRelationship();
		res.setRcrPosition(toRapportCriteres().count());
		return res;
	}

	public Boolean hasCriteres() {
		return Boolean.valueOf(toRapportCriteres().count() > 0);
	}

	public Boolean hasRapportExts() {
		return Boolean.valueOf(getRapportExts().count() > 0);
	}

	public NSArray<EOCategorie> categoriesAssociees() {
		NSMutableArray<EOCategorie> res = new NSMutableArray<EOCategorie>();
		for (int i = 0; i < toRapportCategories().count(); i++) {
			EORapportCategorie array_element = (EORapportCategorie) toRapportCategories().objectAtIndex(i);
			res.addObject(array_element.toCategorie());
		}
		return res.immutableClone();
	}

	/**
	 * Ajoute une categorie si elle n'est pas deja associee au rapport.
	 * 
	 * @param selectedObject
	 * @return
	 */
	public EORapportCategorie ajouteCategorie(EOCategorie selectedObject) {
		EORapportCategorie rc = null;
		if (selectedObject != null) {
			EOQualifier qual = new EOKeyValueQualifier(EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, selectedObject);
			if (toRapportCategories(qual).count() == 0) {
				rc = createToRapportCategoriesRelationship();
				rc.setToCategorieRelationship(selectedObject);
			}
		}
		return rc;
	}

	public EORapportCategorie enleveCategorie(EOCategorie selectedObject) {
		EORapportCategorie rc = null;
		if (selectedObject != null) {
			EOQualifier qual = new EOKeyValueQualifier(EORapportCategorie.TO_CATEGORIE_KEY, EOQualifier.QualifierOperatorEqual, selectedObject);
			NSArray<EORapportCategorie> res = toRapportCategories(qual);
			if (res.count() != 0) {
				deleteToRapportCategoriesRelationship((EORapportCategorie) res.objectAtIndex(0));
			}
		}
		return rc;
	}

	public boolean hasRapportJRXML() {
		Iterator<IRapportExt> iter = getRapportExts().iterator();
		while (iter.hasNext()) {
			if (iter.next() instanceof EORapportJrxml) {
				return true;
			}
		}
		return false;
	}

	public boolean hasRapportJXLS() {
		Iterator<IRapportExt> iter = getRapportExts().iterator();
		while (iter.hasNext()) {
			if (iter.next() instanceof EORapportJxls) {
				return true;
			}
		}
		return false;
	}

	@Override
	public EORapportJrxml createToRapportJrxmlsRelationship() {
		EORapportJrxml res = super.createToRapportJrxmlsRelationship();
		res.setToTypeFormatRapportRelationship(EOTypeFormatRapport.fetchByKeyValue(editingContext(), EOTypeFormatRapport.TFR_STR_ID_KEY, EOTypeFormatRapport.STR_ID_JRXML));
		return res;
	}

	@Override
	public EORapportJxls createToRapportJxlssRelationship() {
		EORapportJxls res = super.createToRapportJxlssRelationship();
		res.setToTypeFormatRapportRelationship(EOTypeFormatRapport.fetchByKeyValue(editingContext(), EOTypeFormatRapport.TFR_STR_ID_KEY, EOTypeFormatRapport.STR_ID_JXLS));
		return res;
	}

	@Override
	public String toDisplayString() {
		return "Rapport " + (rapLibelle() != null ? "'" + rapLibelle() + "'" : (rapStrId() != null ? rapStrId() : ""));
	}

	public void check_modeleDefini() throws NSValidation.ValidationException {
		if (getRapportExts().count() == 0) {
			throw new ValidationException(REGLE_CHECK_modeleDefini);
		}
	}

	public void check_libelleDefini() throws NSValidation.ValidationException {
		if (ERXStringUtilities.stringIsNullOrEmpty(rapLibelle())) {
			throw new ValidationException(REGLE_CHECK_libelleDefini);
		}
	}

	public void check_strIdDefini() throws NSValidation.ValidationException {
		if (ERXStringUtilities.stringIsNullOrEmpty(rapStrId())) {
			throw new ValidationException(REGLE_CHECK_strIdDefini);
		}
	}

	public static NSArray<EORapport> getDoublonsStrId(EOEditingContext edc, EORapport rapport) {
		NSMutableArray<EORapport> res = new NSMutableArray<EORapport>();
		NSArray<EORapport> doublons = fetchAll(edc, new EOKeyValueQualifier(EORapport.RAP_STR_ID_KEY, EOQualifier.QualifierOperatorEqual, rapport.rapStrId()), null);
		for (int i = 0; i < doublons.count(); i++) {
			if (!(doublons.objectAtIndex(i)).globalID().equals(rapport.globalID())) {
				res.addObject(doublons.objectAtIndex(i));
			}
		}
		return res.immutableClone();
	}

	public void check_strIdDoublon() throws NSValidation.ValidationException {
		NSArray<EORapport> doublons = getDoublonsStrId(editingContext(), this);
		if (doublons.count() > 0) {
			throw new NSValidation.ValidationException("Le rapport '" + rapLibelle() + "' possède déjà l'identifiant : " + rapStrId() + ".");
		}
	}

	public void check_libelleDoublon() throws NSValidation.ValidationException {
		//pour l'instant on ne bloque pas
		//		NSArray doublons = fetchAll(this.editingContext(), new EOKeyValueQualifier(EORapport.RAP_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, rapLibelle()), null);
		//		if (doublons.count() > 1 || (doublons.count() == 1 && !((EORapport) doublons.objectAtIndex(0)).globalID().equals(globalID()))) {
		//			EORapport doublon = (EORapport) doublons.objectAtIndex(0);
		//			throw new NSValidation.ValidationException("La rapport id='" + rapStrId() + "' possède déjà le libellé : " + rapLibelle() + ".");
		//		}
	}

	public void check_categorieAssociee() throws NSValidation.ValidationException {
		if (toRapportCategories().count() == 0) {
			throw new NSValidation.ValidationException(REGLE_CHECK_categorieAssociee);
		}
	}

	public void setIsBrouillon(Boolean value) {
		if (value.booleanValue()) {
			setToTypeEtatRelationship(EOTypeEtat.getTypeEtatBrouillon(editingContext()));
			setDatePublication(null);
		}
	}

	public Boolean isBrouillon() {
		return Boolean.valueOf(toTypeEtat() != null && EOTypeEtat.STR_ID_BROUILLON.equals(toTypeEtat().teStrId()));
	}

	public void setIsDesactive(Boolean value) {
		if (value.booleanValue()) {
			setToTypeEtatRelationship(EOTypeEtat.getTypeEtatDesactive(editingContext()));
			setDatePublication(null);
		}
	}

	public Boolean isDesactive() {
		return Boolean.valueOf(toTypeEtat() != null && EOTypeEtat.STR_ID_DESACTIVE.equals(toTypeEtat().teStrId()));
	}

	public void setIsPublie(Boolean value) {
		if (value.booleanValue()) {
			setToTypeEtatRelationship(EOTypeEtat.getTypeEtatPublie(editingContext()));
			setDatePublication(new NSTimestamp());
		}
	}

	public Boolean isPublie() {
		return Boolean.valueOf(toTypeEtat() != null && EOTypeEtat.STR_ID_PUBLIE.equals(toTypeEtat().teStrId()));
	}

	@Override
	public void awakeFromInsertion(EOEditingContext ec) {
		super.awakeFromInsertion(ec);
		setIsBrouillon(Boolean.TRUE);
		setDateCreation(new NSTimestamp());
		setPersIdCreation(getAppUser().getPersId());
		actualAttachedDirectoryName = null;

	}

	@Override
	public void awakeFromFetch(EOEditingContext editingContext) {
		super.awakeFromFetch(editingContext);
		actualAttachedDirectoryName = rapStrId();
		setAutoPerformDirectoryAction(true);
		checkErreurs();
	}

	/**
	 * Supprime un rapport et tout ce qui est associé
	 */
	public static void supprimer(EORapport rapport) {
		//FIXME supprimer les fichiers modeles ?
		rapport.deleteAllToPersRapportFavorissRelationships();
		rapport.deleteAllToPersRapportHistosRelationships();
		rapport.deleteAllToPersRapportPretsRelationships();
		rapport.deleteAllToRapportCategoriesRelationships();
		rapport.deleteAllToRapportCriteresRelationships();
		rapport.deleteAllToRapportJrxmlsRelationships();
		rapport.deleteAllToRapportJxlssRelationships();
		rapport.delete();
	}

	public String rapVersionDateStr() {
		if (super.rapVersionDate() != null) {
			return DATE_FORMATTER.format(super.rapVersionDate());
		}
		return null;

	}

	public void setRapVersionDateStr(String s) throws ParseException {
		NSTimestamp res = (NSTimestamp) DATE_FORMATTER.parseObject(s);
		setRapVersionDate(res);
	}

	@Override
	public void setRapStrId(String value) {
		//String oldValue = rapStrId();
		super.setRapStrId(value);
		newAttachedDirectoryName = value;
	}

	/**
	 * Recopie les parametres d'un rapport dans un autre (+ criteres nouveaux). Conserve les historiques, favoris etc.
	 * 
	 * @param rap
	 */
	public void updateFromAnotherRapport(EORapport rap) {
		setRapVersion(rap.rapVersion());
		setRapAuteur(rap.rapAuteur());
		setRapCommentaire(rap.rapCommentaire());
		setRapLibelle(rap.rapLibelle());
		setRapUrl(rap.rapUrl());
		setRapVersionDate(rap.rapVersionDate());
		setRapLocal(rap.rapLocal());

		NSArray<EORapportCritere> oldCrits = toRapportCriteres();
		NSArray<EORapportCritere> newCrits = rap.toRapportCriteres();
		//on supprime les criteres qui ne sont plus affectés
		for (int i = 0; oldCrits != null && i < oldCrits.count(); i++) {
			EORapportCritere rc = oldCrits.objectAtIndex(i);
			boolean found = false;
			for (int j = 0; j < newCrits.count(); j++) {
				EORapportCritere rco = newCrits.objectAtIndex(j);
				if (rco.toCritere().critStrId().equals(rc.toCritere().critStrId())) {
					found = true;
				}
			}
			if (!found) {
				rc.deleteAllToPersRapportPretCritsRelationships();
				rap.deleteToRapportCriteresRelationship(rc);
				//				rc.setToCritereRelationship(null);
				//				rc.setToRapportRelationship(null);
				//On supprime l'ancien critere
				//			editingContext().deleteObject(rc);
			}

		}
		//on ajoute les nouveaux criteres
		for (int i = 0; newCrits != null && i < newCrits.count(); i++) {
			EORapportCritere rc = newCrits.objectAtIndex(i);
			boolean found = false;
			for (int j = 0; j < oldCrits.count(); j++) {
				EORapportCritere rco = oldCrits.objectAtIndex(j);
				if (rco.toCritere().critStrId().equals(rc.toCritere().critStrId())) {
					found = true;
				}
			}
			if (!found) {
				//On cree le nouveau critere
				EORapportCritere newRc = EORapportCritere.creerInstance(editingContext());
				newRc.setRcrCle(rc.rcrCle());
				newRc.setRcrCommentaire(rc.rcrCommentaire());
				newRc.setRcrLibelle(rc.rcrLibelle());
				newRc.setRcrObligatoire(rc.rcrObligatoire());
				newRc.setRcrPosition(newRc.rcrPosition());
				newRc.setToRapportRelationship(this);
				newRc.setToCritereRelationship(rc.toCritere());
			}
		}

		//On supprime les ref aux fichiers
		deleteAllToRapportJrxmlsRelationships();
		for (int i = 0; rap.toRapportJrxmls() != null && i < rap.toRapportJrxmls().count(); i++) {
			EORapportJrxml array_element = (EORapportJrxml) rap.toRapportJrxmls().objectAtIndex(i);
			EORapportJrxml newR = EORapportJrxml.creerInstance(editingContext());
			newR.setPersIdModification(rap.persIdModification());
			newR.setRjrFile(array_element.rjrFile());
			newR.setRjrVersionJrMin(array_element.rjrVersionJrMin());
			newR.setToTypeFormatRapportRelationship(array_element.toTypeFormatRapport());
			newR.setToRapportRelationship(this);
			for (int j = 0; j < array_element.toRapportJrxmlTexes().count(); j++) {
				EORapportJrxmlTex oldTex = (EORapportJrxmlTex) array_element.toRapportJrxmlTexes().objectAtIndex(j);
				EORapportJrxmlTex newTex = newR.createToRapportJrxmlTexesRelationship();
				newTex.setToTypeExportRelationship(oldTex.toTypeExport());
			}
		}

		deleteAllToRapportJxlssRelationships();
		for (int i = 0; rap.toRapportJxlss() != null && i < rap.toRapportJxlss().count(); i++) {
			EORapportJxls array_element = (EORapportJxls) rap.toRapportJxlss().objectAtIndex(i);
			EORapportJxls newR = EORapportJxls.creerInstance(editingContext());
			newR.setPersIdModification(rap.persIdModification());
			newR.setRjlFile(array_element.rjlFile());
			newR.setRjlSql(array_element.rjlSql());
			newR.setToTypeFormatRapportRelationship(array_element.toTypeFormatRapport());
			newR.setToRapportRelationship(this);
			for (int j = 0; j < array_element.toRapportJxlsTexes().count(); j++) {
				EORapportJxlsTex oldTex = (EORapportJxlsTex) array_element.toRapportJxlsTexes().objectAtIndex(j);
				EORapportJxlsTex newTex = newR.createToRapportJxlsTexesRelationship();
				newTex.setToTypeExportRelationship(oldTex.toTypeExport());
			}
		}

	}

	@Override
	public void deleteToPersRapportPretsRelationship(EOPersRapportPret object) {
		object.deleteAllToPersRapportPretAbosRelationships();
		object.deleteAllToPersRapportPretCritsRelationships();
		super.deleteToPersRapportPretsRelationship(object);
	}

	@Override
	public void deleteToRapportCriteresRelationship(EORapportCritere object) {
		object.deleteAllToPersRapportPretCritsRelationships();
		super.deleteToRapportCriteresRelationship(object);
	}

	@Override
	public void deleteToRapportJrxmlsRelationship(EORapportJrxml object) {
		object.deleteAllToRapportJrxmlTexesRelationships();
		super.deleteToRapportJrxmlsRelationship(object);
	}

	@Override
	public void deleteToRapportJxlssRelationship(EORapportJxls object) {
		object.deleteAllToRapportJxlsTexesRelationships();
		super.deleteToRapportJxlssRelationship(object);
	}

	/**
	 * @return Un nom basé sur le rap_strId + numero ordre
	 */
	public String getNewArchiveName() {
		//		String UIDstr = new NSTimestampFormatter("%y%m%d%H%M%F").format(new NSTimestamp());
		String UIDstr = new SimpleDateFormat("yyyyMMddHHmmSSS").format(new Date());
		return rapStrId() + "_" + UIDstr;
	}

	public String createNewRapStrId() {
		String res = null;
		int count = 1;
		int i = 0;
		while (count > 0) {
			i++;
			res = rapStrId() + "_" + i;
			String sql = "select * from " + EORapport.ENTITY_TABLE_NAME + " where " + EORapport.RAP_STR_ID_COLKEY + "='" + res + "'";
			NSArray resultats = EOUtilities.rawRowsForSQL(editingContext(), FwkCktlReport.class.getSimpleName(), sql, null);
			count = resultats.count();
		}
		return res;
	}

	public String createNewRapLibelle() {
		String res = null;
		int count = 1;
		int i = 0;
		while (count > 0) {
			i++;
			res = rapLibelle() + "_" + i;
			String sql = "select * from " + EORapport.ENTITY_TABLE_NAME + " where " + EORapport.RAP_LIBELLE_COLKEY + "='" + res + "'";
			NSArray resultats = EOUtilities.rawRowsForSQL(editingContext(), FwkCktlReport.class.getSimpleName(), sql, null);
			count = resultats.count();
		}
		return res;
	}

	/**
	 * Duplique un rapport (incluant criteres et categories).
	 * 
	 * @param rapSource
	 * @param newName
	 * @param appUser
	 * @return
	 */
	public static EORapport duplicate(EORapport rapSource, String newName, FwkCktlReportApplicationUser appUser) {
		EORapport rapNew = creerInstance(rapSource.editingContext());
		rapNew.setRapStrId(newName);
		rapNew.setRapVersion(rapSource.rapVersion());
		rapNew.setRapAuteur(rapSource.rapAuteur());
		rapNew.setRapCommentaire(rapSource.rapCommentaire());
		rapNew.setRapLibelle(rapSource.rapLibelle());
		rapNew.setRapUrl(rapSource.rapUrl());
		rapNew.setRapVersionDate(rapSource.rapVersionDate());
		rapNew.setPersIdCreation(appUser.getPersId());
		//rapNew.setAppUser(appUser);
		rapNew.setRapLocal(rapSource.rapLocal());
		rapNew.actualAttachedDirectoryName = rapSource.getActualReportDirectoryName();

		NSArray<EORapportCritere> oldCrits = rapSource.toRapportCriteres();
		for (int i = 0; i < oldCrits.count(); i++) {
			EORapportCritere rc = oldCrits.objectAtIndex(i);
			EORapportCritere newRc = rapNew.createToRapportCriteresRelationship();
			newRc.setRcrCle(rc.rcrCle());
			newRc.setRcrCommentaire(rc.rcrCommentaire());
			newRc.setRcrLibelle(rc.rcrLibelle());
			newRc.setRcrObligatoire(rc.rcrObligatoire());
			newRc.setToCritereRelationship(rc.toCritere());
			newRc.setToRapportRelationship(rapNew);
		}

		NSArray<EORapportCategorie> oldCats = rapSource.toRapportCategories();
		for (int i = 0; i < oldCats.count(); i++) {
			EORapportCategorie rc = oldCats.objectAtIndex(i);
			EORapportCategorie newRc = rapNew.createToRapportCategoriesRelationship();
			newRc.setPersIdModification(rapNew.persIdModification());
			newRc.setToCategorieRelationship(rc.toCategorie());
		}

		NSArray<EORapportJrxml> oldJrxmls = rapSource.toRapportJrxmls();
		for (int i = 0; i < oldJrxmls.count(); i++) {
			EORapportJrxml array_element = (EORapportJrxml) oldJrxmls.objectAtIndex(i);
			EORapportJrxml newR = rapNew.createToRapportJrxmlsRelationship();
			newR.setPersIdModification(rapNew.persIdModification());
			newR.setRjrFile(array_element.rjrFile());
			newR.setRjrVersionJrMin(array_element.rjrVersionJrMin());
			newR.setToTypeFormatRapportRelationship(array_element.toTypeFormatRapport());
			for (int j = 0; j < array_element.toRapportJrxmlTexes().count(); j++) {
				EORapportJrxmlTex oldTex = (EORapportJrxmlTex) array_element.toRapportJrxmlTexes().objectAtIndex(j);
				EORapportJrxmlTex newTex = newR.createToRapportJrxmlTexesRelationship();
				newTex.setToTypeExportRelationship(oldTex.toTypeExport());
			}

		}

		NSArray<EORapportJxls> oldJxls = rapSource.toRapportJxlss();
		for (int i = 0; i < oldJxls.count(); i++) {
			EORapportJxls array_element = (EORapportJxls) oldJxls.objectAtIndex(i);
			EORapportJxls newR = rapNew.createToRapportJxlssRelationship();
			newR.setPersIdModification(rapNew.persIdModification());
			newR.setRjlFile(array_element.rjlFile());
			newR.setRjlSql(array_element.rjlSql());
			newR.setToTypeFormatRapportRelationship(array_element.toTypeFormatRapport());
			for (int j = 0; j < array_element.toRapportJxlsTexes().count(); j++) {
				EORapportJxlsTex oldTex = (EORapportJxlsTex) array_element.toRapportJxlsTexes().objectAtIndex(j);
				EORapportJxlsTex newTex = newR.createToRapportJxlsTexesRelationship();
				newTex.setToTypeExportRelationship(oldTex.toTypeExport());
			}
		}

		return rapNew;
	}

	@Override
	public void didInsert() {
		super.didInsert();
		if (autoPerformDirectoryAction) {
			if (getNewAttachedDirectoryName() != null) {
				try {
					CktlReportFileAction.createAction(getAppUser(), getNewAttachedDirectoryName()).performAction();
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
		checkErreurs();
	}

	@Override
	public void didUpdate() {
		super.didUpdate();
		if (autoPerformDirectoryAction) {
			if (actualAttachedDirectoryName != null && getNewAttachedDirectoryName() != null && !actualAttachedDirectoryName.equals(getNewAttachedDirectoryName())) {
				try {
					CktlReportFileAction.renameLocalAction(getAppUser(), actualAttachedDirectoryName, getNewAttachedDirectoryName()).performAction();
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
		checkErreurs();
	}

	@Override
	public void didDelete(EOEditingContext ec) {
		super.didDelete(ec);
		if (autoPerformDirectoryAction) {
			if (actualAttachedDirectoryName != null) {
				try {
					CktlReportFileAction.deleteAction(getAppUser(), actualAttachedDirectoryName).performAction();
				} catch (Exception e) {
					e.printStackTrace();
					throw new RuntimeException(e);
				}
			}
		}
	}

	public void deleteAttachedDirectory(FwkCktlReportApplicationUser appUser) throws Exception {
		if (getActualReportDirectoryName() != null) {
			CktlReportFilesUtils.deleteReportDirectory(appUser, getActualReportDirectoryName());
		}
	}

	public String getActualReportDirectoryName() {
		return actualAttachedDirectoryName;
	}

	public String getNewAttachedDirectoryName() {
		return newAttachedDirectoryName;
	}

	public boolean isAutoPerformDirectoryAction() {
		return autoPerformDirectoryAction;
	}

	public void setAutoPerformDirectoryAction(boolean silent) {
		this.autoPerformDirectoryAction = silent;
	}

	public FwkCktlReportApplicationUser getAppUser() {
		return (FwkCktlReportApplicationUser) ERXThreadStorage.valueForKey(FwkCktlReportApplicationUser.THREAD_STORAGE_KEY);
		//return (FwkCktlReportApplicationUser) CktlWebSession.session().objectForKey(FwkCktlReportApplicationUser.THREAD_STORAGE_KEY);
	}

	//
	//	public void setAppUser(FwkCktlReportApplicationUser appUser) {
	//		this.appUser = appUser;
	//		if (appUser != null) {
	//			setPersIdModification(appUser.getPersId());
	//		}
	//
	//	}

	public void setNewAttachedDirectoryName(String newAttachedDirectoryName) {
		this.newAttachedDirectoryName = newAttachedDirectoryName;
	}

	/**
	 * Verifie si les fichiers associés sont trouvés
	 * 
	 * @param rapportExt
	 * @return
	 * @throws Exception
	 */
	public Boolean checkFiles(IRapportExt rapportExt) throws Exception {
		Boolean res = CktlReportFilesUtils.checkReportDirectoryFound(rapportExt.toRapport(), getAppUser());
		if (res) {
			res = res && CktlReportFilesUtils.checkRapportExtFileFound(rapportExt, getAppUser());
		}
		return res;
	}

	/**
	 * Met à jour les erreurs dectectees.
	 */
	public void checkErreurs() {
		NSMutableArray<String> res = new NSMutableArray<String>();
		NSArray<IRapportExt> rapportexts = getRapportExts();
		for (int i = 0; i < rapportexts.count(); i++) {
			try {
				if (!checkFiles(rapportexts.objectAtIndex(i))) {
					throw new Exception("Le fichier " + ((IRapportExt) rapportexts.objectAtIndex(i)).getSrcFile() + " n'a pas été trouvé.");
				}
			} catch (NullPointerException e1) {
				e1.printStackTrace();
				res.add("NullPointerException");
			} catch (Exception e) {
				res.add(e.getLocalizedMessage());
			}
		}
		erreurs = res.immutableClone();
	}

	public NSArray<String> getErreurs() {
		return erreurs;
	}

	@Override
	public void didRevert(EOEditingContext ec) {
		super.didRevert(ec);
		checkErreurs();
	}

}
