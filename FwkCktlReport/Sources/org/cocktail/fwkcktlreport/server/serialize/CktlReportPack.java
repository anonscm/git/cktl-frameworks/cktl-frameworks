/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.serialize;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Iterator;
import java.util.zip.CRC32;
import java.util.zip.CheckedInputStream;
import java.util.zip.CheckedOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import org.cocktail.fwkcktlreport.server.CktlReportRapportInstallCtrl;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlreport.server.files.CktlReportFileAction;
import org.cocktail.fwkcktlreport.server.files.CktlReportFilesUtils;
import org.cocktail.fwkcktlreport.server.metier.EORapport;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation._NSStringUtilities;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXFileUtilities;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Représente un objet rapport pret pour import/export.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportPack {
	private static final String XML_ENCODING_UTF_8 = _NSStringUtilities.UTF8_ENCODING;
	private static final Charset XML_ENCODING_UTF_8_CHARSET = Charset.forName(XML_ENCODING_UTF_8);
	public static final String PACK_FILE_EXTENSION = "cktlreport";
	public static final String INDEX_FILENAME = "index.xml";
	public static final String FILES_DIRECTORY_NAME = "files";
	private String id;
	private String xmlRapport;
	private NSMutableDictionary<String, NSData> files = new NSMutableDictionary<String, NSData>();

	private CktlReportPack() {
	}

	public String getXmlRapport() {
		return xmlRapport;
	}

	public void setXmlRapport(String xmlRapport) {
		this.xmlRapport = xmlRapport;
	}

	public NSData xmlToNSData() {
		return new NSData(xmlRapport, XML_ENCODING_UTF_8);
	}

	public void addFile(String fileName, NSData fileContent) {
		files.setObjectForKey(fileContent, fileName);
	}

	public void addFile(File file) throws IOException {
		addFile(file.getName(), new NSData(ERXFileUtilities.bytesFromFile(file)));
	}

	/**
	 * Crée et renvoie un pack import/export à partir d'un rapport.
	 * 
	 * @param appUser
	 * @param rapport
	 * @return
	 * @throws Exception
	 */

	public static CktlReportPack createPack(FwkCktlReportApplicationUser appUser, EORapport rapport) throws Exception {
		CktlReportPack pack = new CktlReportPack();
		pack.setId(rapport.rapStrId());
		pack.setXmlRapport(CktlReportXmlFactory.toXml(rapport));
		//Lister les fichiers et les ajouter
		NSArray<File> allFiles = CktlReportFilesUtils.getAllFilesForRapport(appUser, rapport, null);
		Iterator<File> iter = allFiles.iterator();
		while (iter.hasNext()) {
			File file = (File) iter.next();
			pack.addFile(file);
		}
		return pack;
	}

	/**
	 * Charge un pack pour pouvoir ensuite importer le rapport dans la base et installer les fichiers.
	 * 
	 * @param appUser
	 * @param zip
	 * @return
	 * @throws Exception
	 */
	public static CktlReportPack loadPack(FwkCktlReportApplicationUser appUser, String fileName, NSData zip) throws Exception {
		try {
			CktlReportPack pack = new CktlReportPack();
			File f = unZip(appUser, fileName, zip);

			//recupérer le xml
			File indexFile = new File(f.getAbsolutePath() + File.separator + INDEX_FILENAME);
			if (!indexFile.exists()) {
				throw new Exception("Fichier " + INDEX_FILENAME + " manquant");
			}
			String xml = ERXFileUtilities.stringFromInputStream(new FileInputStream(indexFile));
			pack.setXmlRapport(xml);
			if (ERXStringUtilities.stringIsNullOrEmpty(xml)) {
				throw new Exception("xml vide");
			}

			//recuperer les fichiers
			File dir = new File(f.getAbsolutePath() + File.separator + FILES_DIRECTORY_NAME);
			if (!dir.exists()) {
				throw new Exception("Répertoire des template manquant. ");
			}
			NSArray<File> allFiles = new NSArray<File>(ERXFileUtilities.listFiles(dir, false, null));
			if (allFiles.count() == 0) {
				throw new Exception("Aucun fichier de template trouvé.");
			}
			Iterator<File> iter = allFiles.iterator();
			while (iter.hasNext()) {
				File file = (File) iter.next();
				pack.addFile(file);
			}

			//tout est ok, on supprime le repertoire temp
			ERXFileUtilities.deleteDirectory(f);
			return pack;

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Fichier d'import non valide : " + e.getLocalizedMessage());
		}

	}

	public EORapport install(ERXEC erxec, String actionPresentOption, FwkCktlReportApplicationUser fwkCktlReportApplicationUser, NSMutableArray<CktlReportFileAction> fileActions, NSMutableArray<EORapport> touchedRapports) throws Exception {
		EORapport rap = CktlReportXmlFactory.load(getXmlRapport(), erxec, fwkCktlReportApplicationUser);
		CktlReportRapportInstallCtrl.saveRapport(erxec, rap, actionPresentOption, fwkCktlReportApplicationUser, fileActions, touchedRapports);
		return rap;
	}

	@Override
	public String toString() {
		return xmlRapport + "\n" + files.toString();
	}

	private static File unZip(FwkCktlReportApplicationUser appUser, String fileName, NSData zip) throws Exception {
		//on decompresse l'archive dans un répertoire temporaire 

		String tempFilePath = CktlReportFilesUtils.getTemporaryFilePath(CktlReportFilesUtils.getUniqueFileName(appUser, ERXFileUtilities.removeFileExtension(fileName)));
		File f = new File(tempFilePath);
		if (f.exists()) {
			if (f.isDirectory()) {
				ERXFileUtilities.deleteDirectory(f);
			}
			else {
				f.delete();
			}
		}

		//FileInputStream fi = new FileInputStream("test.zip");
		ByteArrayInputStream fi = new ByteArrayInputStream(zip.bytes());
		CheckedInputStream csumi = new CheckedInputStream(fi, new CRC32());
		ZipInputStream archive = new ZipInputStream(new BufferedInputStream(csumi));

		ZipEntry entry;
		while ((entry = archive.getNextEntry()) != null) {
			String path = f.getAbsolutePath() + File.separator + entry.getName();
			File f1 = new File(path);
			if (!entry.isDirectory()) {
				f1.getParentFile().mkdirs();
				if (!f1.createNewFile()) {
					throw new Exception("Impossible de créer le fichier " + f1.getAbsolutePath());
				}
				BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream(f1));
				int x;
				while ((x = archive.read()) != -1) {
					out.write(x);
				}
				out.close();
			}

		}
		archive.close();
		return f;
	}

	/**
	 * Crée un ZIP du pack
	 * 
	 * @return
	 * @throws Exception
	 */
	public NSData toZip() throws Exception {
		try {
			ByteArrayOutputStream f = new ByteArrayOutputStream();
			CheckedOutputStream csum = new CheckedOutputStream(f, new CRC32());
			ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(csum));
			out.setComment("Export de " + getId());

			//Ajouter le xml
			out.putNextEntry(new ZipEntry(INDEX_FILENAME));
			out.write(xmlToNSData().bytes());
			out.closeEntry();

			//Ajouter le repertoire
			out.putNextEntry(new ZipEntry(FILES_DIRECTORY_NAME + "/"));
			out.closeEntry();

			//Ajouter les fichiers du répertoire
			Iterator<String> fileNames = files.allKeys().iterator();
			while (fileNames.hasNext()) {
				String fileName = (String) fileNames.next();
				out.putNextEntry(new ZipEntry(FILES_DIRECTORY_NAME + "/" + fileName));
				out.write(files.objectForKey(fileName).bytes());
				out.closeEntry();
			}
			out.close();
			return new NSData(f.toByteArray());
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

}
