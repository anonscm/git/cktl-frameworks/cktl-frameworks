/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.serialize;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlreport.server.VersionMe;
import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreport.server.metier.EOCritere;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EORapportCategorie;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;
import org.cocktail.fwkcktlreport.server.metier.EORapportJrxml;
import org.cocktail.fwkcktlreport.server.metier.EORapportJrxmlTex;
import org.cocktail.fwkcktlreport.server.metier.EORapportJxls;
import org.cocktail.fwkcktlreport.server.metier.EORapportJxlsTex;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeFormatRapport;
import org.xml.sax.InputSource;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.xml.WOXMLDecoder;
import com.webobjects.eocontrol.EOEditingContext;

public class CktlReportXmlFactory {
	public final static Logger logger = Logger.getLogger(CktlReportXmlFactory.class);

	/** Nom du fichier de mapping pour convertir des objets xml en entites EOF et vice-versa. */
	public static final String MAPPING_FILE_NAME = "mapping/CktlReportMapping.1.0.0.0.xml";

	public final static String NC = "nc";

	private CktlReportXmlFactory() {

	}

	/**
	 * Cree et renvoie un objet a partir d'un contenu xml.
	 * 
	 * @param mappingURL
	 * @param xmlContent
	 * @return
	 * @throws Exception
	 */
	public static EORapport load(String xmlContent) throws Exception {
		InputSource is = new InputSource(new StringReader(xmlContent));
		return load(is);
	}

	public static EORapport load(InputStreamReader inputStreamReader) throws Exception {
		InputSource is = new InputSource(inputStreamReader);
		return load(is);
	}

	//
	//	public static EORapport load(InputStream inputStream) throws Exception {
	//		InputSource is = new InputSource(inputStream);
	//		return load(is);
	//	}

	public static EORapport load(InputSource is) throws Exception {
		try {
			URL mappingUrl = WOApplication.application().resourceManager().pathURLForResourceNamed(MAPPING_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null);
			if (logger.isDebugEnabled()) {
				logger.debug("mapping file = " + mappingUrl.toExternalForm());
			}
			WOXMLDecoder decoder = WOXMLDecoder.decoderWithMapping(mappingUrl.getPath());
			if (logger.isDebugEnabled()) {
				logger.debug("decoder.parserClassName() = " + decoder.parserClassName());
			}
			EORapport newObject = (EORapport) decoder.decodeRootObject(is);
			return newObject;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Cree un objet EORapport à partir du xml.
	 * 
	 * @param xmlContent
	 * @param edc
	 * @param appUser
	 * @return
	 * @throws Exception
	 */
	public static EORapport load(String xmlContent, EOEditingContext edc, FwkCktlReportApplicationUser appUser) throws Exception {
		try {
			EORapport rap = load(xmlContent);
			edc.insertObject(rap);
			//Gerer les relations inverses et la création eventuelle des categories et criteres
			for (int i = 0; rap.toRapportCategories() != null && i < rap.toRapportCategories().count(); i++) {
				EORapportCategorie rc = (EORapportCategorie) rap.toRapportCategories().objectAtIndex(i);
				edc.insertObject(rc);
				rc.setPersIdModification(appUser.getPersId());
				rc.setToRapportRelationship(rap);
				//Chercher la categorie correspondante, la creer le cas echeant
				EOCategorie cat = EOCategorie.fetchByKeyValue(edc, EOCategorie.CAT_STR_ID_KEY, rc.toCategorie().catStrId());
				if (cat == null) {
					edc.insertObject(rc.toCategorie());
					rc.toCategorie().setPersIdModification(appUser.getPersId());
				}
				else {
					//System.out.println(cat);
					rc.takeValueForKey(null, EORapportCategorie.TO_CATEGORIE_KEY);
					rc.setToCategorieRelationship(cat);
				}
			}
			for (int i = 0; rap.toRapportCriteres() != null && i < rap.toRapportCriteres().count(); i++) {
				EORapportCritere rc = (EORapportCritere) rap.toRapportCriteres().objectAtIndex(i);
				edc.insertObject(rc);
				rc.setToRapportRelationship(rap);
				EOCritere cat = EOCritere.fetchByKeyValue(edc, EOCritere.CRIT_STR_ID_KEY, rc.toCritere().critStrId());
				if (cat == null) {
					edc.insertObject(rc.toCritere());
					rc.toCritere().setPersIdModification(appUser.getPersId());
				}
				else {
					rc.takeValueForKey(null, EORapportCritere.TO_CRITERE_KEY);
					rc.setToCritereRelationship(cat);
				}
			}
			for (int i = 0; rap.toRapportJrxmls() != null && i < rap.toRapportJrxmls().count(); i++) {
				EORapportJrxml rc = (EORapportJrxml) rap.toRapportJrxmls().objectAtIndex(i);
				edc.insertObject(rc);
				rc.setPersIdModification(appUser.getPersId());
				rc.setToRapportRelationship(rap);

				EOTypeFormatRapport tfr = EOTypeFormatRapport.fetchByKeyValue(edc, EOTypeFormatRapport.TFR_STR_ID_KEY, rc.toTypeFormatRapport().tfrStrId());
				rc.takeValueForKey(null, EORapportJrxml.TO_TYPE_FORMAT_RAPPORT_KEY);
				rc.setToTypeFormatRapportRelationship(tfr);

				for (int j = 0; j < rc.toRapportJrxmlTexes().count(); j++) {
					EORapportJrxmlTex array_element = (EORapportJrxmlTex) rc.toRapportJrxmlTexes().objectAtIndex(j);
					EOTypeExport tex = EOTypeExport.fetchByKeyValue(edc, EOTypeExport.TEX_STR_ID_KEY, array_element.toTypeExport().texStrId());
					if (tex == null) {
						throw new Exception("Type d'export " + array_element.toTypeExport().texStrId() + " non pris en charge par l'application.");
					}
					else {
						edc.insertObject(array_element);
						array_element.takeValueForKey(null, EORapportJrxmlTex.TO_TYPE_EXPORT_KEY);
						array_element.setToTypeExportRelationship(tex);
					}
					array_element.setPersIdModification(appUser.getPersId());
					array_element.setToRapportJrxmlRelationship(rc);
				}

			}
			for (int i = 0; rap.toRapportJxlss() != null && i < rap.toRapportJxlss().count(); i++) {
				EORapportJxls rc = (EORapportJxls) rap.toRapportJxlss().objectAtIndex(i);
				edc.insertObject(rc);
				rc.setPersIdModification(appUser.getPersId());
				rc.setToRapportRelationship(rap);
				EOTypeFormatRapport tfr = EOTypeFormatRapport.fetchByKeyValue(edc, EOTypeFormatRapport.TFR_STR_ID_KEY, rc.toTypeFormatRapport().tfrStrId());
				rc.takeValueForKey(null, EORapportJxls.TO_TYPE_FORMAT_RAPPORT_KEY);
				rc.setToTypeFormatRapportRelationship(tfr);

				for (int j = 0; j < rc.toRapportJxlsTexes().count(); j++) {
					EORapportJxlsTex array_element = (EORapportJxlsTex) rc.toRapportJxlsTexes().objectAtIndex(j);
					edc.insertObject(array_element);
					EOTypeExport tex = EOTypeExport.fetchByKeyValue(edc, EOTypeExport.TEX_STR_ID_KEY, array_element.toTypeExport().texStrId());
					if (tex == null) {
						throw new Exception("Type d'export " + array_element.toTypeExport().texStrId() + " non pris en charge par l'application.");
					}
					else {
						array_element.takeValueForKey(null, EORapportJxlsTex.TO_TYPE_EXPORT_KEY);
						array_element.setToTypeExportRelationship(tex);
					}
					array_element.setPersIdModification(appUser.getPersId());
					array_element.setToRapportJxlsRelationship(rc);
				}

			}

			return rap;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Transforme une entitee EOF EORapport en xml.
	 * 
	 * @throws Exception
	 */
	public static String toXml(EORapport cxml) throws Exception {
		if (cxml == null) {
			return null;
		}
		URL mappingUrl = WOApplication.application().resourceManager().pathURLForResourceNamed(MAPPING_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null);
		if (mappingUrl == null) {
			throw new Exception("Fichier de mapping " + MAPPING_FILE_NAME + " non trouvé.");
		}
		return toXml(cxml, mappingUrl.getPath());
	}

	public static String toXml(EORapport cxml, String mappingFile) {
		if (cxml == null) {
			return null;
		}
		String res = "";
		CktlReportXmlCoder coder = CktlReportXmlCoder.coderWithMapping(mappingFile);
		coder.setXmlDeclaration("1.0", "UTF-8", null);
		//coder.setDocTypeDeclaration("<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.1.010/cXML.dtd\">");
		res = coder.encodeRootObjectForKey(cxml, null);
		if (logger.isDebugEnabled()) {
			logger.debug("XML généré pour le rapport :" + res);
		}
		return res;
	}

	//
	//	/**
	//	 * @param editingContext
	//	 * @param parameters
	//	 * @param buyerCookie
	//	 * @param punchOutBrowserFormPostURL
	//	 * @param items Tableau d'EOItemIn ou d'EOItemOut
	//	 * @param operation Operation a effectuer (create, edit).
	//	 * @return Un objet PunchOutSetupRequest
	//	 * @throws Exception
	//	 */
	//	public static EORapport createPunchOutSetupRequest(EOEditingContext editingContext, CXMLParameters parameters, CXMLBuyerCookie buyerCookie, String punchOutBrowserFormPostURL, String operation, NSArray items) throws Exception {
	//		File f = new File(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_PUNCH_OUT_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());
	//		if (!f.exists()) {
	//			throw new Exception("Le fichier de template " + f.getPath() + " n'a pas ete trouve.");
	//		}
	//		else {
	//			LRLogger.debug("Fichier template", f.getPath());
	//		}
	//		if (!f.canRead()) {
	//			throw new Exception("Le fichier de template " + f.getPath() + " ne peut etre lu.");
	//		}
	//		FileInputStream fs = new FileInputStream(f);
	//		try {
	//			EORapport res = createCXMLFromTemplate(editingContext, parameters, fs);
	//			res.request().setDeploymentMode(parameters.getDeploymentMode());
	//			res.request().punchOutSetupRequest().setBuyerCookie(buyerCookie.toString());
	//			res.request().punchOutSetupRequest().supplierSetup().url().setVal(parameters.getSupplierSetupURL());
	//			res.request().punchOutSetupRequest().browserFormPost().url().setVal(punchOutBrowserFormPostURL);
	//			res.request().punchOutSetupRequest().setOperation(operation);
	//			if (items != null && EOPunchOutSetupRequest.OPERATION_EDIT.equals(operation)) {
	//				for (int i = 0; i < items.count(); i++) {
	//					EOItemOut itemOut = null;
	//					Object item = items.objectAtIndex(i);
	//					if (item instanceof EOItemIn) {
	//						itemOut = ((EOItemIn) item).convertToEOItemOut();
	//					}
	//					else if (item instanceof EOItemOut) {
	//						itemOut = (EOItemOut) item;
	//					}
	//					//EOItemIn itemIn = ((EOItemIn) items.objectAtIndex(i));
	//					//EOItemOut itemOut = itemIn.convertToEOItemOut();
	//					res.request().punchOutSetupRequest().addToItemOutsRelationship(itemOut);
	//				}
	//			}
	//			return res;
	//		} catch (Exception e) {
	//			fs.close();
	//			File f1 = new File(f.getPath());
	//			FileInputStream fs1 = new FileInputStream(f1);
	//			String ligne;
	//			BufferedReader fichier = new BufferedReader(new InputStreamReader(fs1));
	//			while ((ligne = fichier.readLine()) != null) {
	//				System.err.println(ligne);
	//			}
	//			throw e;
	//		}
	//	}
	//
	//	/**
	//	 * Cree un objet entete pour la commande. Utilise par
	//	 * {@link CXMLMessageFactory#createOrderRequest(EOEditingContext, CXMLParameters, EOAddress, NSArray, EOAddress, NSArray)}
	//	 * 
	//	 * @param editingContext
	//	 * @param billTo Adresse de facturation
	//	 * @param shipTo Adresse de livraison
	//	 * @param contacts Tableau d'objets EOContact
	//	 * @param orderDate
	//	 * @param orderID
	//	 * @param orderType
	//	 * @return
	//	 * @throws Exception
	//	 */
	//	protected static EOOrderRequestHeader createOrderRequestHeader(EOEditingContext editingContext, NSTimestamp orderDate, Long orderID, String orderType, EOAddress billTo, EOAddress shipTo, NSArray contacts) throws Exception {
	//		if (orderID == null) {
	//			throw new Exception("L'ID de la commande (orderID) est obligatoire.");
	//		}
	//		if (orderDate == null) {
	//			orderDate = new NSTimestamp();
	//		}
	//		if (orderType == null) {
	//			orderType = EOOrderRequestHeader.TYPE_NEW;
	//		}
	//
	//		EOOrderRequestHeader orderRequestHeader = EOOrderRequestHeader.creerInstance(editingContext);
	//		orderRequestHeader.setOrderDate(CXMLUtilities.formatTimestamp(orderDate));
	//		orderRequestHeader.setOrderID(orderID);
	//		orderRequestHeader.setType(orderType);
	//		if (billTo != null) {
	//			orderRequestHeader.setBillToRelationship(EOBillTo.createEOBillTo(editingContext, billTo));
	//		}
	//		orderRequestHeader.setShipToRelationship(EOShipTo.createEOShipTo(editingContext, shipTo));
	//		for (int i = 0; contacts != null && i < contacts.count(); i++) {
	//			orderRequestHeader.addToContactsRelationship((EOContact) contacts.objectAtIndex(i));
	//		}
	//
	//		return orderRequestHeader;
	//	}
	//
	//	/**
	//	 * cree un message OrderRequest (pour passer un ordre de commande définitive).
	//	 * 
	//	 * @param editingContext
	//	 * @param parameters
	//	 * @param billTo Adresse de facturation
	//	 * @param contacts Tableau d'objets EOContact
	//	 * @param shipTo Adresse de livraison
	//	 * @param itemOuts Tableau d'objet EOItemOuts
	//	 * @param orderDate
	//	 * @param orderID
	//	 * @param orderType
	//	 * @return
	//	 * @throws Exception
	//	 */
	//	public static EORapport createOrderRequest(EOEditingContext editingContext, CXMLParameters parameters, NSTimestamp orderDate, Long orderID, Long requisitionID, String orderType, EOAddress billTo, NSArray contacts, EOAddress shipTo, NSArray itemOuts) throws Exception {
	//		LRLogger.debug(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_ORDER_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());
	//		FileInputStream fs = new FileInputStream(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_ORDER_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());
	//
	//		EORapport res = createCXMLFromTemplate(editingContext, parameters, fs);
	//
	//		res.setPayloadID(CXMLUtilities.newPayloadId(parameters.getLocalDomain()));
	//
	//		EOOrderRequestHeader orh = createOrderRequestHeader(editingContext, orderDate, orderID, orderType, billTo, shipTo, contacts);
	//		if (requisitionID != null) {
	//			orh.setRequisitionID(requisitionID.toString());
	//		}
	//		orh.setTotalRelationship(EOTotal.createEOTotal(editingContext, EOMoney.creerInstance(editingContext)));
	//		res.request().orderRequest().setOrderRequestHeaderRelationship(orh);
	//
	//		res.request().setDeploymentMode(parameters.getDeploymentMode());
	//
	//		BigDecimal total = new BigDecimal(0).setScale(2);
	//		for (int i = 0; itemOuts != null && i < itemOuts.count(); i++) {
	//			EOItemOut itemOut = (EOItemOut) itemOuts.objectAtIndex(i);
	//			res.request().orderRequest().addToItemOutsRelationship(itemOut);
	//			total = total.add(itemOut.itemDetail().unitPrice().money().getVal().multiply(new BigDecimal(itemOut.quantity().intValue()).setScale(2)));
	//		}
	//		res.request().orderRequest().orderRequestHeader().total().money().setVal(total);
	//		return res;
	//	}

	/**
	 * Cree et initialise un EOrapport à partie d'un xml
	 * 
	 * @param editingContext
	 * @param templateInputStream
	 * @return
	 * @throws Exception
	 */
	protected static EORapport createCXMLFromTemplate(EOEditingContext editingContext, InputStream templateInputStream) throws Exception {
		InputStreamReader isr = new InputStreamReader(templateInputStream, CktlReportXmlUtilities.CHARSET_UTF_8);

		//EORapport res = load(templateInputStream);
		EORapport res = load(isr);

		return res;
	}

}
