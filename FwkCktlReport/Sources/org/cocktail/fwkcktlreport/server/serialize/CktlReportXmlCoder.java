/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.serialize;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Enumeration;

import com.webobjects.appserver.xml.WOXMLCoder;
import com.webobjects.appserver.xml.WOXMLDecoder;
import com.webobjects.appserver.xml.WOXMLException;
import com.webobjects.appserver.xml._private._MappingModel;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXWOXMLCoder;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.foundation.ERXStringUtilities;

public class CktlReportXmlCoder extends WOXMLCoder {
	private String docTypeDeclaration;
	private boolean codeEmptyValues = true;

	private _MappingModel _mappingModel;

	/**
	 * Quick and dirty class to en- and decode the generic xml data to full-flegded objects that can be bound in the edit interface.
	 * 
	 * @author ak
	 */
	public static class XMLData extends NSMutableDictionary {

		public XMLData() {
		}

		public void completeDecoding() {
		}

		public void prepareForCoding() {
		}

		protected void takeValueForKeyPathIfNotPresent(Object object, String key) {
			if (valueForKeyPath(key) == null) {
				takeValueForKeyPath(object, key);
			}
		}

		protected void clearEmptyValueForKeyPath(String key) {
			if (valueForKeyPath(key) != null && ((String) valueForKeyPath(key)).length() == 0) {
				takeValueForKeyPath(null, key);
			}
		}

		protected void clearParentOnEmptyValueForKeyPath(String key) {
			if (valueForKeyPath(key) != null && ((String) valueForKeyPath(key)).length() == 0) {
				takeValueForKeyPath(null, ERXStringUtilities.keyPathWithoutLastProperty(key));
			}
		}

		/**
		 * This works around a bug when the decoder reaches an emtpy tag an tries to create a dictionary from it.
		 */
		public void takeValueForKey(Object aValue, String aKey) {
			if (aValue instanceof NSDictionary && ((NSDictionary) aValue).count() == 0) {
				if (aValue.getClass() == NSMutableDictionary.class) {
					aValue = null;
				}
			}
			super.takeValueForKey(aValue, aKey);
		}

		/**
		 * Serializes to an XML string for the given data object conforming to the supplied model.
		 */
		public static String stringForData(XMLData data, String rootTag, String mappingUrl) {
			data.prepareForCoding();
			WOXMLCoder coder = new ERXWOXMLCoder(mappingUrl);
			String result = coder.encodeRootObjectForKey(data, rootTag);
			data.completeDecoding();
			return result;
		}

		/**
		 * Deserializes the given string to an instance of XMLData.
		 * 
		 * @param string
		 * @param mappingUrl
		 */
		public static XMLData dataForString(String string, String mappingUrl) {
			WOXMLDecoder decoder = WOXMLDecoder.decoderWithMapping(mappingUrl);
			XMLData data;
			try {
				data = (XMLData) decoder.decodeRootObject(new NSData(string.getBytes("UTF-8")));
			} catch (UnsupportedEncodingException e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
			data.completeDecoding();
			return data;
		}
	}

	public String xmlTagForClassNamed(String className) {

		return _mappingModel.xmlTagForClassNamed(className);
	}

	public String xmlTagForPropertyKey(String key, String className) {
		return _mappingModel.xmlTagForPropertyKey(key, className);
	}

	protected void _encodeEO(EOEnterpriseObject eoenterpriseobject) {
		NSArray arr = null;
		arr = sortedArray(eoenterpriseobject.attributeKeys());

		for (Enumeration e = arr.objectEnumerator(); e.hasMoreElements();) {
			String s = (String) e.nextElement();
			encodeObjectForKey(eoenterpriseobject.valueForKey(s), s);
			if (e.hasMoreElements())
				cr();
		}
	}

	protected Enumeration sortedEnumeration(Enumeration e) {
		if (e != null) {
			NSMutableArray arr = new NSMutableArray();
			for (; e.hasMoreElements();) {
				String element = (String) e.nextElement();
				arr.addObject(element);
			}
			e = sortedArray(arr).objectEnumerator();
		}
		return e;
	}

	protected NSArray sortedArray(NSArray arr) {

		if (arr != null) {
			arr = ERXArrayUtilities.sortedArraySortedWithKey(arr, "toString");
		}
		return arr;
	}

	protected void encodeDictionaryWithXMLTag(NSDictionary dict, String tag) {
		NSArray nsarray = sortedArray(dict.allKeys());
		for (Enumeration e = nsarray.objectEnumerator(); e.hasMoreElements();) {
			String key = (String) e.nextElement();
			String tagForKey = _mappingModel.xmlTagForPropertyKeyInXMLTag(key, tag);
			encodeObjectWithXMLTag(dict.objectForKey(key), tagForKey, false, _MappingModel.OUTPUT_PROPERTY_TAG, false);
			if (e.hasMoreElements())
				cr();
		}
	}

	protected void encodeArrayWithXMLTag(NSArray arr, String tag, boolean codeBasedOnClass, int outputTags) {
		for (Enumeration e = arr.objectEnumerator(); e.hasMoreElements();) {
			encodeObjectWithXMLTag(e.nextElement(), tag, codeBasedOnClass, outputTags, false);
			if (e.hasMoreElements())
				cr();
		}
	}

	public void encodeObjectForKey(Object obj, String key) {
		String tag = _mappingModel.xmlTagForPropertyKey(key, encodedClassName());
		encodeObjectWithXMLTag(obj, tag, false, _MappingModel.OUTPUT_PROPERTY_TAG, false);
	}

	public void encodeObjectWithXMLTag(Object obj, String baseTag, boolean codeBasedOnClass, int outputTags, boolean codeTagIfEmpty) {
		if (obj instanceof NSArray) {
			encodeArrayWithXMLTag((NSArray) obj, baseTag, codeBasedOnClass, outputTags);
		}
		else {
			if (((isCodeEmptyValues() || codeTagIfEmpty) && obj == null) || (obj != null)) {
				boolean isBaseType = (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Date) || (obj instanceof Number);
				String className = obj == null ? null : obj.getClass().getName();
				String tagForClassName = className == null ? "" : xmlTagForClassNamed(className);
				String tag = null;
				if (outputTags != _MappingModel.OUTPUT_NEITHER_TAG && (!isBaseType || outputTags != _MappingModel.OUTPUT_CLASS_TAG)) {
					tag = (outputTags & _MappingModel.OUTPUT_CLASS_TAG) == 0 || isBaseType ? baseTag : tagForClassName;
					if (outputTags == _MappingModel.OUTPUT_BOTH_TAGS) {
						_buffer.append('<');
						_buffer.append(baseTag);
						_buffer.append('>');
					}
					_buffer.append('<');
					_buffer.append(tag);
					if (obj != null) {
						//						Enumeration attributes = codeBasedOnClass ? _mappingModel.attributeKeysForClassNamed(className) : _mappingModel.attributeKeysForXMLTag(baseTag);
						Enumeration attributes = sortedAttributeKeysForObject(obj);
						if (attributes == null) {
							attributes = codeBasedOnClass ? _mappingModel.attributeKeysForClassNamed(className) : _mappingModel.attributeKeysForXMLTag(baseTag);
						}
						//attributes = sortedEnumeration(attributes);
						if (attributes != null)
							for (; attributes.hasMoreElements();) {
								String key = (String) attributes.nextElement();
								//								_buffer.append(' ');
								//								_buffer.append(codeBasedOnClass ? xmlTagForPropertyKey(key, className) : _mappingModel.xmlTagForPropertyKeyInXMLTag(key, baseTag));
								//								_buffer.append('=');
								//								_buffer.append('"');
								//								Object value = NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, key);
								//								_buffer.append(value == null ? "" : escapeString(value.toString()));
								//								_buffer.append('"');

								Object value = com.webobjects.foundation.NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, key);
								if ((isCodeEmptyValues() && value == null) || (value != null)) {
									_buffer.append(' ');
									_buffer.append(codeBasedOnClass ? xmlTagForPropertyKey(key, className) : _mappingModel.xmlTagForPropertyKeyInXMLTag(key, baseTag));
									_buffer.append('=');
									_buffer.append('"');
									_buffer.append(value == null ? "" : escapeString(value.toString()));
									_buffer.append('"');
								}

							}

					}
					_buffer.append('>');
				}
				if (obj != null) {
					if (obj instanceof String) {
						_buffer.append(escapeString((String) obj));
					}
					else if (obj instanceof NSTimestamp) {
						_buffer.append((NSTimestamp) obj);
					}
					else if (obj instanceof Boolean) {
						_buffer.append((Boolean) obj);
					}
					else if (obj instanceof Number) {
						// FIXME AK: this will break when using BigDecimals and JDK 1.5
						_buffer.append((Number) obj);
					}
					else if (codeBasedOnClass || _mappingModel.hasMappingForXMLTag(baseTag)) {
						//						Enumeration contentKeys = codeBasedOnClass ? _mappingModel.contentsKeysForClassNamed(className) : _mappingModel.contentsKeysForXMLTag(baseTag);
						Enumeration contentKeys = sortedContentKeysForObject(obj);
						if (contentKeys == null) {
							contentKeys = codeBasedOnClass ? _mappingModel.contentsKeysForClassNamed(className) : _mappingModel.contentsKeysForXMLTag(baseTag);
						}
						//contentKeys = sortedEnumeration(contentKeys);
						if (contentKeys != null) {
							_encodedClasses.push(className);
							boolean hadContent = false;
							for (; contentKeys.hasMoreElements();) {
								cr();
								hadContent = true;
								String key = (String) contentKeys.nextElement();
								System.out.println(key);
								Object value = NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, key);
								String propertyTag = codeBasedOnClass ? xmlTagForPropertyKey(key, className) : _mappingModel.xmlTagForPropertyKeyInXMLTag(key, baseTag);
								boolean codeBasedOnClassForPropertyKey = _mappingModel.codeBasedOnClassForPropertyKey(key, className);
								int outputTagsForPropertyKey = _mappingModel.outputTagsForPropertyKey(key, className);
								encodeObjectWithXMLTag(value, propertyTag, codeBasedOnClassForPropertyKey, outputTagsForPropertyKey, isKeyToCode(obj, key));
							}

							_encodedClasses.pop();
							if (hadContent)
								cr();
						}
					}
					else if (obj instanceof NSDictionary) {
						encodeDictionaryWithXMLTag((NSDictionary) obj, baseTag);
					}
					else if (obj instanceof EOEnterpriseObject) {
						_encodeEO((EOEnterpriseObject) obj);
					}
					else {
						throw new WOXMLException("Unable to encode in XML objects of class " + className);
					}
				}
				if (outputTags != _MappingModel.OUTPUT_NEITHER_TAG && (!isBaseType || outputTags != _MappingModel.OUTPUT_CLASS_TAG)) {
					_buffer.append('<');
					_buffer.append('/');
					_buffer.append(tag);
					_buffer.append('>');
					if (outputTags == _MappingModel.OUTPUT_BOTH_TAGS && !isBaseType) {
						_buffer.append('<');
						_buffer.append('/');
						_buffer.append(baseTag);
						_buffer.append('>');
					}
				}
			}
		}
	}

	protected void _encodeNullForKey(String s) {
		encodeStringInTag("null", s, "?");
	}

	public CktlReportXmlCoder(String s) {
		_mappingModel = _MappingModel.mappingModelWithXMLFile(s);
	}

	@Override
	public synchronized String encodeRootObjectForKey(Object obj, String s) {
		//		if (obj != null) {
		//			_buffer = new StringBuffer(1024);
		//			_buffer.append(xmlDeclaration);
		//			if (docTypeDeclaration != null) {
		//				_buffer.append(docTypeDeclaration);
		//				cr();
		//			}
		//			encodeObjectWithXMLTag(obj, "ignored", true, 1);
		//			return _buffer.toString();
		//		}
		//		else {
		//			return null;
		//		}
		String res = null;
		if (obj != null) {
			_buffer = new StringBuffer(1024);
			_buffer.append(xmlDeclaration);
			if (docTypeDeclaration != null) {
				_buffer.append(docTypeDeclaration);
				cr();
			}
			encodeObjectWithXMLTag(obj, "ignored", true, _MappingModel.OUTPUT_CLASS_TAG, false);
			res = _buffer.toString();
		}
		res = compactXML(res);
		return res;
	}

	public static CktlReportXmlCoder coderWithMapping(String s) {
		return coderWithMapping(s, false);
	}

	/**
	 * @param s Chemin d'acces au fichier de mapping xml
	 * @param codeEmptyValues Si false, les valeurs vides ne seront pas inscrites dans le xml.
	 * @return
	 */
	public static CktlReportXmlCoder coderWithMapping(String s, boolean codeEmptyValues) {
		CktlReportXmlCoder cxmlCoder = new CktlReportXmlCoder(s);
		cxmlCoder.setCodeEmptyValues(codeEmptyValues);
		return cxmlCoder;
	}

	public String getDocTypeDeclaration() {
		return docTypeDeclaration;
	}

	public void setDocTypeDeclaration(String docTypeDeclaration) {
		this.docTypeDeclaration = docTypeDeclaration;
	}

	//	@Override
	//	public String xmlTagForClassNamed(String s) {
	//		return _mappingModel.xmlTagForClassNamed(s);
	//	}
	//
	//	@Override
	//	public String xmlTagForPropertyKey(String s, String s1) {
	//		return _mappingModel.xmlTagForPropertyKey(s, s1);
	//	}

	//	protected void _encodeEO(EOEnterpriseObject eoenterpriseobject) {
	//		NSArray nsarray = eoenterpriseobject.attributeKeys();
	//		String s;
	//		for (Enumeration enumeration = nsarray.objectEnumerator(); enumeration.hasMoreElements(); encodeObjectForKey(eoenterpriseobject.valueForKey(s), s)) {
	//			cr();
	//			s = (String) enumeration.nextElement();
	//		}
	//
	//		
	//		
	//	}

	//	protected void encodeDictionaryWithXMLTag(NSDictionary nsdictionary, String s) {
	//		Enumeration enumeration = nsdictionary.keyEnumerator();
	//		do {
	//			if (!enumeration.hasMoreElements()) {
	//				break;
	//			}
	//			String s1 = (String) enumeration.nextElement();
	//			String s2 = _mappingModel.xmlTagForPropertyKeyInXMLTag(s1, s);
	//			encodeObjectWithXMLTag(nsdictionary.objectForKey(s1), s2, false, 2);
	//			if (enumeration.hasMoreElements()) {
	//				cr();
	//			}
	//		} while (true);
	//	}

	//	protected void encodeArrayWithXMLTag(NSArray nsarray, String s, boolean flag, int i) {
	//		Enumeration enumeration = nsarray.objectEnumerator();
	//		do {
	//			if (!enumeration.hasMoreElements()) {
	//				break;
	//			}
	//			encodeObjectWithXMLTag(enumeration.nextElement(), s, flag, i);
	//			if (enumeration.hasMoreElements()) {
	//				cr();
	//			}
	//		} while (true);
	//	}
	//
	//	public void encodeObjectWithXMLTag(Object obj, String baseTag, boolean codeBasedOnClass, int outputTags) {
	//		if (obj instanceof NSArray) {
	//			encodeArrayWithXMLTag((NSArray) obj, baseTag, codeBasedOnClass, outputTags);
	//		}
	//		else {
	//			if ((isCodeEmptyValues() && obj == null) || (obj != null)) {
	//				boolean flag1 = (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Date) || (obj instanceof Number);
	//				String objClass = obj == null ? null : obj.getClass().getName();
	//				String objTag = objClass == null ? "" : xmlTagForClassNamed(objClass);
	//				String s3 = null;
	//				if (outputTags != 0 && (!flag1 || outputTags != 1)) {
	//					s3 = (outputTags & 1) == 0 || flag1 ? baseTag : objTag;
	//					if (outputTags == 3) {
	//						_buffer.append('<');
	//						_buffer.append(baseTag);
	//						_buffer.append('>');
	//					}
	//					_buffer.append('<');
	//					_buffer.append(s3);
	//					if (obj != null) {
	//						Enumeration enumeration = codeBasedOnClass ? _mappingModel.attributeKeysForClassNamed(objClass) : _mappingModel.attributeKeysForXMLTag(baseTag);
	//						if (enumeration != null) {
	//							while (enumeration.hasMoreElements()) {
	//								String s4 = (String) enumeration.nextElement();
	//								Object obj1 = com.webobjects.foundation.NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, s4);
	//								if ((isCodeEmptyValues() && obj1 == null) || (obj1 != null)) {
	//									_buffer.append(' ');
	//									_buffer.append(codeBasedOnClass ? xmlTagForPropertyKey(s4, objClass) : _mappingModel.xmlTagForPropertyKeyInXMLTag(s4, baseTag));
	//									_buffer.append('=');
	//									_buffer.append('"');
	//									_buffer.append(obj1 == null ? "" : escapeString(obj1.toString()));
	//									_buffer.append('"');
	//								}
	//							}
	//						}
	//
	//					}
	//					_buffer.append('>');
	//				}
	//
	//				if (obj != null) {
	//					if (obj instanceof String) {
	//						_buffer.append(escapeString((String) obj));
	//					}
	//					else if (obj instanceof NSTimestamp) {
	//						_buffer.append(obj);
	//					}
	//					else if (obj instanceof Boolean) {
	//						_buffer.append(obj);
	//					}
	//					else if (obj instanceof Number) {
	//						_buffer.append(obj);
	//					}
	//					else if (codeBasedOnClass || _mappingModel.hasMappingForXMLTag(baseTag)) {
	//						Enumeration enumeration1 = codeBasedOnClass ? _mappingModel.contentsKeysForClassNamed(objClass) : _mappingModel.contentsKeysForXMLTag(baseTag);
	//						if (enumeration1 != null) {
	//							_encodedClasses.push(objClass);
	//							boolean flag2 = false;
	//							Object obj2;
	//							String s6;
	//							boolean flag3;
	//							int j;
	//							for (; enumeration1.hasMoreElements(); encodeObjectWithXMLTag(obj2, s6, flag3, j)) {
	//								cr();
	//								flag2 = true;
	//								String s5 = (String) enumeration1.nextElement();
	//								obj2 = com.webobjects.foundation.NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, s5);
	//								s6 = codeBasedOnClass ? xmlTagForPropertyKey(s5, objClass) : _mappingModel.xmlTagForPropertyKeyInXMLTag(s5, baseTag);
	//								flag3 = _mappingModel.codeBasedOnClassForPropertyKey(s5, objClass);
	//								j = _mappingModel.outputTagsForPropertyKey(s5, objClass);
	//							}
	//
	//							_encodedClasses.pop();
	//							if (flag2) {
	//								cr();
	//							}
	//						}
	//					}
	//					else if (obj instanceof NSDictionary) {
	//						encodeDictionaryWithXMLTag((NSDictionary) obj, baseTag);
	//					}
	//					else if (obj instanceof EOEnterpriseObject) {
	//						_encodeEO((EOEnterpriseObject) obj);
	//					}
	//					else {
	//						throw new WOXMLException("Unable to encode in XML objects of class " + objClass);
	//					}
	//				}
	//				if (outputTags != 0 && (!flag1 || outputTags != 1)) {
	//					_buffer.append('<');
	//					_buffer.append('/');
	//					_buffer.append(s3);
	//					_buffer.append('>');
	//					if (outputTags == 3 && !flag1) {
	//						_buffer.append('<');
	//						_buffer.append('/');
	//						_buffer.append(baseTag);
	//						_buffer.append('>');
	//					}
	//				}
	//			}
	//		}
	//	}

	//	public void encodeObjectWithXMLTag(Object obj, String s, boolean flag, int i) {
	//		if (obj instanceof NSArray) {
	//			encodeArrayWithXMLTag((NSArray) obj, s, flag, i);
	//		}
	//		else {
	//			boolean flag1 = (obj instanceof String) || (obj instanceof Boolean) || (obj instanceof Date) || (obj instanceof Number);
	//			String s1 = obj == null ? null : obj.getClass().getName();
	//			String s2 = s1 == null ? "" : xmlTagForClassNamed(s1);
	//			String s3 = null;
	//			if (i != 0 && (!flag1 || i != 1)) {
	//				s3 = (i & 1) == 0 || flag1 ? s : s2;
	//				if (i == 3) {
	//					_buffer.append('<');
	//					_buffer.append(s);
	//					_buffer.append('>');
	//				}
	//				_buffer.append('<');
	//				_buffer.append(s3);
	//				if (obj != null) {
	//					Enumeration enumeration = flag ? _mappingModel.attributeKeysForClassNamed(s1) : _mappingModel.attributeKeysForXMLTag(s);
	//					if (enumeration != null) {
	//						for (; enumeration.hasMoreElements(); _buffer.append('"')) {
	//							String s4 = (String) enumeration.nextElement();
	//							_buffer.append(' ');
	//							_buffer.append(flag ? xmlTagForPropertyKey(s4, s1) : _mappingModel.xmlTagForPropertyKeyInXMLTag(s4, s));
	//							_buffer.append('=');
	//							_buffer.append('"');
	//							Object obj1 = com.webobjects.foundation.NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, s4);
	//							_buffer.append(obj1 == null ? "" : escapeString(obj1.toString()));
	//						}
	//					}
	//					
	//				}
	//				_buffer.append('>');
	//			}
	//			if (obj != null) {
	//				if (obj instanceof String) {
	//					_buffer.append(escapeString((String) obj));
	//				}
	//				else if (obj instanceof NSTimestamp) {
	//					_buffer.append(obj);
	//				}
	//				else if (obj instanceof Boolean) {
	//					_buffer.append(obj);
	//				}
	//				else if (obj instanceof Number) {
	//					_buffer.append(obj);
	//				}
	//				else if (flag || _mappingModel.hasMappingForXMLTag(s)) {
	//					Enumeration enumeration1 = flag ? _mappingModel.contentsKeysForClassNamed(s1) : _mappingModel.contentsKeysForXMLTag(s);
	//					if (enumeration1 != null) {
	//						_encodedClasses.push(s1);
	//						boolean flag2 = false;
	//						Object obj2;
	//						String s6;
	//						boolean flag3;
	//						int j;
	//						for (; enumeration1.hasMoreElements(); encodeObjectWithXMLTag(obj2, s6, flag3, j)) {
	//							cr();
	//							flag2 = true;
	//							String s5 = (String) enumeration1.nextElement();
	//							obj2 = com.webobjects.foundation.NSKeyValueCodingAdditions.Utility.valueForKeyPath(obj, s5);
	//							s6 = flag ? xmlTagForPropertyKey(s5, s1) : _mappingModel.xmlTagForPropertyKeyInXMLTag(s5, s);
	//							flag3 = _mappingModel.codeBasedOnClassForPropertyKey(s5, s1);
	//							j = _mappingModel.outputTagsForPropertyKey(s5, s1);
	//						}
	//						
	//						_encodedClasses.pop();
	//						if (flag2) {
	//							cr();
	//						}
	//					}
	//				}
	//				else if (obj instanceof NSDictionary) {
	//					encodeDictionaryWithXMLTag((NSDictionary) obj, s);
	//				}
	//				else if (obj instanceof EOEnterpriseObject) {
	//					_encodeEO((EOEnterpriseObject) obj);
	//				}
	//				else {
	//					throw new WOXMLException("Unable to encode in XML objects of class " + s1);
	//				}
	//			}
	//			if (i != 0 && (!flag1 || i != 1)) {
	//				_buffer.append('<');
	//				_buffer.append('/');
	//				_buffer.append(s3);
	//				_buffer.append('>');
	//				if (i == 3 && !flag1) {
	//					_buffer.append('<');
	//					_buffer.append('/');
	//					_buffer.append(s);
	//					_buffer.append('>');
	//				}
	//			}
	//		}
	//	}

	@Override
	public void encodeBooleanForKey(boolean flag, String s) {
		encodeStringInTag(flag ? "True" : "False", xmlTagForPropertyKey(s, encodedClassName()), "boolean");
	}

	@Override
	public void encodeIntForKey(int i, String s) {
		encodeStringInTag(Integer.toString(i), xmlTagForPropertyKey(s, encodedClassName()), "int");
	}

	@Override
	public void encodeFloatForKey(float f, String s) {
		encodeStringInTag(Float.toString(f), xmlTagForPropertyKey(s, encodedClassName()), "float");
	}

	@Override
	public void encodeDoubleForKey(double d, String s) {
		encodeStringInTag(Double.toString(d), xmlTagForPropertyKey(s, encodedClassName()), "double");
	}

	/**
	 * Indique s'il faut coder le tag correspondant a la cle pour l'objet.
	 * 
	 * @param obj
	 * @param key
	 * @return
	 */
	public boolean isKeyToCode(Object obj, String key) {
		boolean rep = isCodeEmptyValues();
		if (!rep) {
			if (obj instanceof ICktlReportXmlCodingAdditions) {
				NSArray res = ((ICktlReportXmlCodingAdditions) obj).codeEvenIfEmptyXmlContentKeys();

				if (res != null && res.count() > 0 && res.contains(key)) {
					return true;
				}
			}
		}
		return rep;
	}

	public boolean isCodeEmptyValues() {
		return codeEmptyValues;
	}

	public void setCodeEmptyValues(boolean codeEmptyValues) {
		this.codeEmptyValues = codeEmptyValues;
	}

	public synchronized String compactXML(String s) {
		s = s.replaceAll(">[\\s]*", ">").replaceAll("[\\s]*<", "<");
		return s;
	}

	public Enumeration sortedContentKeysForObject(Object obj) {
		if (obj instanceof ICktlReportXmlCodingAdditions) {
			NSArray res = ((ICktlReportXmlCodingAdditions) obj).sortedXmlContentKeys();
			if (res != null && res.count() > 0) {
				return res.objectEnumerator();
			}
		}
		return null;
	}

	public Enumeration sortedAttributeKeysForObject(Object obj) {
		if (obj instanceof ICktlReportXmlCodingAdditions) {
			NSArray res = ((ICktlReportXmlCodingAdditions) obj).sortedXmlAttributeKeys();
			if (res != null && res.count() > 0) {
				return res.objectEnumerator();
			}
		}
		return null;
	}

}
