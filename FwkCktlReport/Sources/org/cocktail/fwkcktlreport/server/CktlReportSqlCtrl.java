/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server;

import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

/**
 * Permet d'analyser une requete sql.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportSqlCtrl {
	private String sql;
	private Boolean hasParams = Boolean.FALSE;
	private NSArray<String> fields;
	private NSArray<String> params;

	private static final String REGEXP_PARAM_MATCHER = "\\$P\\{(\\w*)\\}";

	//public static final String REGEXP_SQL_SELEC = "(SELECT\\s[\\w\\*\\)\\(\\,\\s]+\\sFROM\\s[\\w]+)";

	public CktlReportSqlCtrl(String sql) {
		setSql(sql);
	}

	public String getSql() {
		return sql;
	}

	public void setSql(String sql) {
		if (sql != null) {
			sql = sql.trim();
		}
		this.sql = sql;
		analyse();
	}

	private void analyse() {
		cleanSql();
		if (!sql.toLowerCase().startsWith("select ")) {
			sql = null;
		}
		if (sql.contains("$P{")) {
			hasParams = Boolean.TRUE;
		}
		analyseFields(sql);
		analyseParams(sql);

	}

	private void cleanSql() {
		//		if (sql != null) {
		//			String tmp = sql;
		//			StringBuffer sb = new StringBuffer();
		//			//Supprimer les commentaires
		//			int lasIndex = 0;
		//			while (tmp.indexOf("\n") > -1) {
		//				String line = tmp.substring(0, tmp.indexOf("\n")).trim();
		//				if (!line.startsWith("--")) {
		//					sb.append(line);
		//				}
		//				tmp = tmp.substring(tmp.indexOf("\n"));
		//			}
		//			sb.append(tmp.substring(beginIndex))
		//			tmp = sb.toString();
		//			
		//
		//			while (tmp.indexOf("/*") > -1) {
		//				int end = tmp.indexOf("*/");
		//				tmp = tmp.substring(0, tmp.indexOf("/*")).concat(" ").concat(tmp.substring(end + 2));
		//			}
		//
		//			tmp = tmp.replaceAll("\n", " ").replaceAll("\t", " ");
		//			sql = tmp;
		//		}
	}

	public NSArray<NSDictionary<String, Object>> rawRowsForSQL(EOEditingContext edc, NSArray keys, CktlReportRapportCriteres rapportCriteresCache) {
		String sql2 = prepare(rapportCriteresCache);
		//Si le sql contient encore des criteres non remplacés, on renvoit un tableau vide
		if (sql2.contains("$P{")) {
			return NSArray.emptyArray();
		}

		return EOUtilities.rawRowsForSQL(edc, "FwkCktlReport", sql2, keys);
	}

	/**
	 * @param rapportCriteresCache
	 * @return Le sql avec les parametres remplacés par les valeurs
	 */
	public String prepare(CktlReportRapportCriteres rapportCriteresCache) {
		String sql2 = sql;
		if (rapportCriteresCache != null && hasParams) {
			Iterator<String> it = rapportCriteresCache.allKeys().iterator();

			while (it.hasNext()) {
				String key = (String) it.next();
				sql2 = sql2.replace("$P{" + key + "}", rapportCriteresCache.getValue(key).toString());
			}
		}
		return sql2;
	}

	/**
	 * Remplace les parametres trouvés dans la requete par leurs valeurs.
	 * 
	 * @param map
	 * @return
	 */
	public String prepare(Map<String, Object> map) {
		String sql2 = sql;

		if (map != null && hasParams) {

			Iterator<String> it = map.keySet().iterator();

			while (it.hasNext()) {
				String key = (String) it.next();
				Object val = map.get(key);
				if (val != null) {
					if (val instanceof String) {
						val = "'" + (String) val + "'";
					}
					else {
						val = val.toString();
					}
					sql2 = sql2.replace("$P{" + key + "}", (String) val);
				}

			}
		}

		if (map != null && hasParams) {
			Iterator<String> it = params.iterator();

			while (it.hasNext()) {
				String key = (String) it.next();
				Object val = map.get(key);
				if (val != null) {
					if (val instanceof String) {
						val = "'" + (String) val + "'";
					}
					else {
						val = val.toString();
					}
					sql2 = sql2.replace("$P{" + key + "}", (String) val);
				}

			}
		}

		//		String sql2 = sql;

		return sql2;

	}

	//
	//	/**
	//	 * Balaye les parametres detectés dans la requete et les remplis avec la map passée en parametre
	//	 * 
	//	 * @param map
	//	 * @return
	//	 */
	//	public String prepareFromParams(Map<String, Object> map) {
	//		String sql2 = sql;
	//		if (map != null && hasParams) {
	//
	//			Iterator<String> it = map.keySet().iterator();
	//
	//			while (it.hasNext()) {
	//				String key = (String) it.next();
	//				Object val = map.get(key);
	//				if (val instanceof String) {
	//					val = "'" + (String) val + "'";
	//				}
	//				else {
	//					val = val.toString();
	//				}
	//
	//				sql2 = sql2.replace("$P{" + key + "}", (String) val);
	//			}
	//		}
	//		return sql2;
	//	}

	public Boolean containsParams() {
		return hasParams;
	}

	public NSArray<String> getFields() {
		return fields;
	}

	public String buildWhereClauseFromFields(String valeur, String operateur, String join, boolean caseInsensitive) {
		NSMutableArray<String> whereClauses = new NSMutableArray<String>();
		String where = "";
		NSArray<String> res = getFields();
		Iterator<String> it = res.iterator();
		while (it.hasNext()) {
			String field = (String) it.next();
			if (caseInsensitive) {
				whereClauses.add("upper(TO_CHAR(" + field + ")) " + operateur + " upper('" + valeur + "')");
			}
			else {
				whereClauses.add("TO_CHAR(" + field + ") " + operateur + " '" + valeur + "'");
			}

		}
		String whereClause = whereClauses.componentsJoinedByString(" " + join + " ");
		return whereClause;
	}

	private void analyseParams(String sqlSelect) {
		NSMutableArray<String> res = new NSMutableArray<String>();
		Pattern p = Pattern.compile(REGEXP_PARAM_MATCHER);
		Matcher m = p.matcher(sqlSelect);
		int count = 0;
		while (m.find()) {
			count++;
			res.add(m.group(1));
		}
		this.params = res;
	}

	private void analyseFields(String sqlSelect) {
		String tmp = sqlSelect;

		int posFrom = tmp.toLowerCase().indexOf(" from ");
		if (posFrom == -1) {
			posFrom = tmp.toLowerCase().indexOf("\nfrom ");
		}
		if (posFrom == -1) {
			posFrom = tmp.toLowerCase().indexOf("\tfrom ");
		}
		tmp = tmp.substring(7, posFrom).trim();
		//String tmp = sql.substring(7, sql.toLowerCase().indexOf(" from")).trim();
		NSMutableArray<String> fields = new NSMutableArray<String>();
		NSArray<String> projection = NSArray.componentsSeparatedByString(tmp, ",");
		Iterator<String> it = projection.iterator();
		while (it.hasNext()) {
			String field = (String) it.next();
			field = field.trim();
			int start = 0;
			if (field.lastIndexOf(".") > start) {
				start = field.lastIndexOf(".") + 1;
			}
			if (field.lastIndexOf(" ") > start) {
				start = field.lastIndexOf(" ") + 1;
			}
			field = field.substring(start);
			fields.addObject(field);
		}
		this.fields = fields.immutableClone();
	}

	public NSArray<String> getParams() {
		return params;
	}

	/**
	 * @return La valeur préparée pour une chaine sql.
	 */
	public static Object getValueForSql(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof String) {
			return "'" + value + "'";
		}
		return value;
	}

}
