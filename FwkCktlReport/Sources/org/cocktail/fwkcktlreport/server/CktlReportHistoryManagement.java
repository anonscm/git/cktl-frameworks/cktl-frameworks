/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2012 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server;

import java.util.Date;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EOTypeExport;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

import er.extensions.eof.ERXEOAccessUtilities;

/**
 * Gere les enregistrements dans la table historique.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportHistoryManagement {
	public final static Logger logger = Logger.getLogger(CktlReportHistoryManagement.class);

	public static Integer startHistory(EOEditingContext ec, EORapport rapport, EOPersonne personne, EOTypeExport typeExport) {
		Integer cle = null;
		String sqlCle = "select " + EOPersRapportHisto.ENTITY_TABLE_NAME + "_seq.nextval as CLE from dual";
		NSArray res = EOUtilities.rawRowsForSQL(ec, "FwkCktlReport", sqlCle, null);
		if (res.count() > 0) {
			cle = Integer.valueOf(((Number) ((NSDictionary) res.objectAtIndex(0)).valueForKey("CLE")).intValue());
		}
		if (cle != null) {
			String sql = "insert into " + EOPersRapportHisto.ENTITY_TABLE_NAME + " (" + EOPersRapportHisto.PRH_ID_COLKEY + "," + EOPersRapportHisto.RAP_ID_COLKEY + "," + EOPersRapportHisto.PERS_ID_COLKEY + "," + EOPersRapportHisto.TEX_ID_COLKEY + "," + EOPersRapportHisto.PRH_START_TIME_COLKEY
					+ ") ";
			sql += " values(";
			sql += " " + cle;
			sql += ", " + rapport.primaryKey();
			sql += "," + personne.persId();
			sql += "," + typeExport.primaryKey();
			sql += ", sysdate";
			sql += ")";
			executeSql(ec, sql);
		}
		return cle;
	}

	public static void finishHistory(EOEditingContext ec, Integer cle, String status, String msg) {
		if (cle != null) {
			String sql = "update " + EOPersRapportHisto.ENTITY_TABLE_NAME;
			sql += " set " + EOPersRapportHisto.PRH_STATUS_COLKEY + " = '" + status + "'";
			if (msg != null) {
				sql += ", " + EOPersRapportHisto.PRH_MESSAGE_COLKEY + " = '" + msg.replaceAll("'", "''") + "'";
			}
			sql += ", " + EOPersRapportHisto.PRH_END_TIME_COLKEY + " = sysdate";

			sql += " where " + EOPersRapportHisto.PRH_ID_COLKEY + "=" + cle;
			executeSql(ec, sql);
		}
	}

	/**
	 * Supprime des enregistrements dans la table historique
	 * 
	 * @param ec
	 * @param persId si null on ne tient pas compte de l'utilisateur
	 * @param nbJoursAGarder
	 */
	public static void cleanHistory(EOEditingContext ec, Integer persId, Integer nbJoursAGarder) {
		Date dateMin = new Date();
		dateMin = MyDateCtrl.getDateOnly(dateMin);
		dateMin = MyDateCtrl.addDHMS(dateMin, -nbJoursAGarder.intValue() + 1, 0, 0, 0);
		String res = "delete from " + EOPersRapportHisto.ENTITY_TABLE_NAME + " where ";
		if (persId != null) {
			res += " pers_id=" + persId + " and ";
		}
		res += EOPersRapportHisto.PRH_START_TIME_COLKEY + "< to_date('" + new SimpleDateFormat("dd/MM/yyyy").format(dateMin) + "','dd/mm/yyyy')";
		executeSql(ec, res);
	}

	private static void executeSql(EOEditingContext ec, String sql) {
		ERXEOAccessUtilities.evaluateSQLWithEntityNamed(ec, EOPersRapportHisto.ENTITY_NAME, sql);
		//EOUtilities.rawRowsForSQL(ec, VersionMe.APPLICATIONINTERNALNAME, sql, null);
	}

}
