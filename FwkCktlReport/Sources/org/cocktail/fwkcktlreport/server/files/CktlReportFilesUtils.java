/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.files;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;
import org.cocktail.fwkcktlreport.server.FwkCktlReportParamManager;
import org.cocktail.fwkcktlreport.server.Version;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.IRapportExt;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.foundation.ERXFileUtilities;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Utilitaires pour gérer les fichiers et répertoires associés aux rapports.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class CktlReportFilesUtils {

	public static Boolean isRapportExtFileFoundInLocal(IRapportExt rapportExt, FwkCktlReportApplicationUser appUser) {
		if (rapportExt.hasTemporaryGlobalID()) {
			return Boolean.FALSE;
		}
		String res = localPathOfFile(appUser, rapportExt, rapportExt.getSrcFile());
		return fileExists(res);
	}

	public static Boolean isRapportExtFileFoundInGlobal(IRapportExt rapportExt, FwkCktlReportApplicationUser appUser) {
		if (rapportExt.hasTemporaryGlobalID()) {
			return Boolean.FALSE;
		}
		String res = globalPathOfFile(appUser, rapportExt, rapportExt.getSrcFile());
		return fileExists(res);
	}

	/**
	 * Le nom du répertoire dédié au rapport.
	 */
	public static String getReportDirectoryName(EORapport rapport) {
		return (rapport.getActualReportDirectoryName() != null ? rapport.getActualReportDirectoryName() : rapport.getNewAttachedDirectoryName());
	}

	/**
	 * Recherche la presence du fichier associé au rapport d'abod dans le repertoire local puis dans le répertoire global.
	 * 
	 * @param rapportExt
	 * @param appUser
	 * @return
	 */
	public static Boolean isRapportExtFileFound(IRapportExt rapportExt, FwkCktlReportApplicationUser appUser) {
		Boolean res = isRapportExtFileFoundInLocal(rapportExt, appUser) || isRapportExtFileFoundInGlobal(rapportExt, appUser);
		return res;
	}

	public static Boolean checkReportDirectoryFound(EORapport rapport, FwkCktlReportApplicationUser appUser) throws Exception {
		String location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION);
		if (!MyStringCtrl.isEmpty(location)) {
			location = location.trim();
			if (!fileExists(location)) {
				//System.err.println("Attention : le chemin indiqué par le parametre " + CktlReportParameters.CKTLREPORT_LOCAL_REPORTS_LOCATION + " = " + location + " n'est pas accessible");
				System.err.println(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION, location));
				location = null;
			}
			else {
				if (fileExists(localReportDirectoryPath(appUser, rapport))) {
					return true;
				}
				else {
					location = null;
				}
			}

		}

		if (MyStringCtrl.isEmpty(location)) {
			location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION);
			if (MyStringCtrl.isEmpty(location)) {
				//				throw new Exception("Le chemin est vide. Veuillez indiquer un chemin valide pour le paramètre <b>" + CktlReportParameters.CKTLREPORT_COCKTAIL_REPORTS_LOCATION + "</b>");
				throw new Exception("Le chemin est vide. " + msgFillParametre(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION));
			}
			location = location.trim();
			if (!fileExists(location)) {
				//				throw new Exception("Attention : le chemin indiqué par le paramètre <b>" + CktlReportParameters.CKTLREPORT_COCKTAIL_REPORTS_LOCATION + " = </b><br><b>" + location + "</b> n'est pas accessible");
				throw new Exception(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION, location));
			}
			if (fileExists(globalReportDirectoryPath(appUser, rapport))) {
				return true;
			}
			else {
				//				throw new Exception("Attention : le répertoire<br><b>" + globalReportDirectoryPath(appUser, rapport) + "</b> n'a pas été trouvé. ");
				throw new Exception("Répertoire non trouvé : " + msgNotAccessible(globalReportDirectoryPath(appUser, rapport).concat(File.separator)));
			}

		}
		return false;

	}

	public static Boolean checkRapportExtFileFound(IRapportExt rapportExt, FwkCktlReportApplicationUser appUser) throws Exception {
		String location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION);
		if (!MyStringCtrl.isEmpty(location)) {
			location = location.trim();
			if (!fileExists(location)) {
				//				System.err.println("Attention : le chemin indiqué par le parametre " + CktlReportParameters.CKTLREPORT_LOCAL_REPORTS_LOCATION + " = " + location + " n'est pas accessible");
				System.err.println(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION, location));
				location = null;
			}
			else {
				if (isRapportExtFileFoundInLocal(rapportExt, appUser)) {
					return true;
				}
				else {
					location = null;
				}
			}

		}

		if (MyStringCtrl.isEmpty(location)) {
			location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION);
			if (MyStringCtrl.isEmpty(location)) {
				//				throw new Exception("Le chemin est vide. Veuillez remplir le parametre " + CktlReportParameters.CKTLREPORT_COCKTAIL_REPORTS_LOCATION);
				throw new Exception("Le chemin est vide. " + msgFillParametre(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION));
			}
			location = location.trim();
			if (!fileExists(location)) {
				//				throw new Exception("Attention : le chemin indiqué par le parametre <br><b>" + CktlReportParameters.CKTLREPORT_COCKTAIL_REPORTS_LOCATION + " = " + location + "</b> n'est pas accessible");
				throw new Exception(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION, location));
			}
			if (isRapportExtFileFoundInGlobal(rapportExt, appUser)) {
				return true;
			}
			else {
				//				throw new Exception("Attention : le fichier <br><b>" + globalPathOfFile(appUser, rapportExt, rapportExt.getSrcFile()) + "</b></br> n'a pas été trouvé. ");
				throw new Exception("Fichier non trouvé : " + msgNotAccessible(globalPathOfFile(appUser, rapportExt, rapportExt.getSrcFile())));
			}

		}
		return false;

	}

	public static String globalReportDirectoryPath(FwkCktlReportApplicationUser appUser, EORapport rapport) {
		return getGlobalDirectory(appUser).concat(File.separator).concat(getReportDirectoryName(rapport));
	}

	public static String localReportDirectoryPath(FwkCktlReportApplicationUser appUser, EORapport rapport) {
		return getLocalDirectory(appUser).concat(File.separator).concat(getReportDirectoryName(rapport));
	}

	public static String globalPathOfFile(FwkCktlReportApplicationUser appUser, IRapportExt rapportExt, String fileName) {
		return globalReportDirectoryPath(appUser, rapportExt.toRapport()).concat(File.separator).concat(fileName);
	}

	public static String localPathOfFile(FwkCktlReportApplicationUser appUser, IRapportExt rapportExt, String fileName) {
		return localReportDirectoryPath(appUser, rapportExt.toRapport()).concat(File.separator).concat(fileName);
	}

	public static String pathOfRapportExtFile(IRapportExt rapportExt, FwkCktlReportApplicationUser appUser) {
		if (isRapportExtFileFoundInLocal(rapportExt, appUser)) {
			return localPathOfFile(appUser, rapportExt, rapportExt.getSrcFile());
		}
		else
			return globalPathOfFile(appUser, rapportExt, rapportExt.getSrcFile());
	}

	public static String getLocalDirectory(FwkCktlReportApplicationUser appUser) {
		String location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION);
		if (!MyStringCtrl.isEmpty(location)) {
			if (location.endsWith(File.separator)) {
				location = location.substring(0, location.lastIndexOf(File.separator));
			}
			location = location.trim();
			if (!fileExists(location)) {
				//				System.err.println("Attention : le chemin indiqué par le parametre " + CktlReportParameters.CKTLREPORT_LOCAL_REPORTS_LOCATION + " = " + location + " n'est pas accessible");
				System.err.println(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_LOCAL_REPORTS_LOCATION, location));
				location = null;
			}
		}
		return location;
	}

	public static String getGlobalDirectory(FwkCktlReportApplicationUser appUser) {
		String location = (String) appUser.getApplicationParams().get(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION);
		if (!MyStringCtrl.isEmpty(location)) {
			if (location.endsWith(File.separator)) {
				location = location.substring(0, location.lastIndexOf(File.separator));
			}
			location = location.trim();
			if (!fileExists(location)) {
				//System.err.println("Attention : le chemin indiqué par le parametre " + CktlReportParameters.CKTLREPORT_COCKTAIL_REPORTS_LOCATION + " = " + location + " n'est pas accessible");
				System.err.println(msgNotAccessible(FwkCktlReportParamManager.CKTLREPORT_COCKTAIL_REPORTS_LOCATION, location));
				location = null;
			}
		}
		return location;
	}

	public static boolean fileExists(String path) {
		if (path == null) {
			return false;
		}
		File f = new File(path);
		return (f.exists());
	}

	public static String getTemporaryDir() throws Exception {
		File tmpDir = new File(System.getProperty("java.io.tmpdir"));
		if (!tmpDir.exists()) {
			throw new ValidationException("Le répertoire temporaire '" + tmpDir.getAbsolutePath() + "' n'existe pas");
		}
		if (!tmpDir.isDirectory()) {
			throw new ValidationException("'" + tmpDir.getAbsolutePath() + "' n'est pas un répertoire");
		}
		if (!tmpDir.canWrite()) {
			throw new ValidationException("Impossible d'écrire dans le répertoire temporaire '" + tmpDir.getAbsolutePath() + "'");
		}
		return tmpDir.getAbsolutePath();
	}

	/**
	 * @param fileName
	 * @return le chemin d'accès à un fichier temporaire
	 */
	public static String getTemporaryFilePath(String fileName) {
		if (ERXStringUtilities.stringIsNullOrEmpty(fileName)) {
			return null;
		}
		String tmpdir = System.getProperty("java.io.tmpdir");
		tmpdir = (tmpdir.endsWith(File.separator) ? tmpdir : tmpdir.concat(File.separator));
		return tmpdir.concat(fileName);
	}

	/**
	 * Enregistre le fichier principal associé à un rapport dans le repertoire local
	 * 
	 * @param appUser
	 * @param filePath
	 * @param dataUploaded
	 * @param rapportExt
	 * @throws Exception
	 */
	public static void saveFileInLocalForRapportExt(FwkCktlReportApplicationUser appUser, String filePath, NSData dataUploaded, IRapportExt rapportExt) throws Exception {
		//enregistrer le fichier dans le rep temp
		//File tmpFile = new File(CktlReportFilesUtils.getTemporaryDir() + File.separator + "_CktlReportTmpFile_" + appUser.getPersId() + "_" + filePath);
		File tmpFile = ERXFileUtilities.reserveUniqueFile(new File(CktlReportFilesUtils.getTemporaryDir() + File.separator + "_CktlReportTmpFile_" + appUser.getPersId() + "_" + filePath), true);
		FileOutputStream fo = new FileOutputStream(tmpFile);
		dataUploaded.writeToStream(fo);
		fo.close();

		//Verifier si le fichier tmp est du bon type
		if (!rapportExt.isFileOfType(tmpFile)) {
			throw new ValidationException("Le fichier <b>" + filePath + "</b> n'est pas du type " + rapportExt.toTypeFormatRapport().tfrLibelle());
		}

		if (rapportExt.toRapport().getActualReportDirectoryName() == null && rapportExt.toRapport().getNewAttachedDirectoryName() == null) {
			throw new ValidationException("Le répertoire associé au rapport n'est pas défini");
		}

		String originalFileName = rapportExt.getSrcFile();
		String newFileName = filePath;

		if (ERXStringUtilities.stringIsNullOrEmpty(originalFileName)) {
			originalFileName = newFileName;
		}

		//Verifier que le fichier a le même nom que l'original
		String originalFilePath = CktlReportFilesUtils.localPathOfFile(appUser, rapportExt, originalFileName);
		if (!originalFileName.equals(newFileName)) {
			throw new Exception("Les fichiers n'ont pas le même nom (original : <b>" + originalFileName + "</b> / nouveau : <b>" + newFileName + "</b>");
		}

		//on archive l'ancien fichier
		if (CktlReportFilesUtils.fileExists(originalFilePath)) {
			ERXFileUtilities.copyFileToFile(new File(originalFilePath), new File(originalFilePath.concat(".bak")), true, false);
		}

		//On enregistre le nouveau
		ERXFileUtilities.copyFileToFile(tmpFile, new File(originalFilePath), true, false);

		//on supprime le fichier temp
		tmpFile.delete();
	}

	/**
	 * Enregistre un fichier dans le repertoire local.
	 * 
	 * @param appUser
	 * @param rapportExt
	 * @param filePath
	 * @param dataUploaded
	 * @throws Exception
	 */
	public static void saveFileInLocal(FwkCktlReportApplicationUser appUser, IRapportExt rapportExt, String filePath, NSData dataUploaded) throws Exception {
		String originalFilePath = CktlReportFilesUtils.localPathOfFile(appUser, rapportExt, filePath);
		File dest = new File(originalFilePath);
		if (dest.exists()) {
			ERXFileUtilities.copyFileToFile(new File(originalFilePath), new File(originalFilePath.concat(".bak")), true, false);
			dest.delete();
		}
		FileOutputStream fo = new FileOutputStream(dest);
		dataUploaded.writeToStream(fo);
		fo.close();
	}

	public static NSData loadFileFromPath(String filePath) {
		NSData res = null;
		if (!ERXStringUtilities.stringIsNullOrEmpty(filePath)) {
			if (fileExists(filePath)) {
				File f = new File(filePath);
				try {
					FileInputStream fs = new FileInputStream(filePath);
					res = new NSData(fs, Long.valueOf(f.length()).intValue());
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return res;
	}

	public static String getGlobalFilePath(FwkCktlReportApplicationUser appUser, String fileName) {
		String res = getGlobalDirectory(appUser);
		if (res == null) {
			return null;
		}
		return res.concat(File.separator).concat(fileName);
	}

	public static String getLocalFilePath(FwkCktlReportApplicationUser appUser, String fileName) {
		String res = getLocalDirectory(appUser);
		if (res == null) {
			return null;
		}
		return res.concat(File.separator).concat(fileName);
	}

	/**
	 * Renomme le dossier du rapport.
	 * 
	 * @param appUser
	 * @param actualAttachedDirectoryName
	 * @param pendingAttachedDirectoryName
	 * @throws Exception
	 */
	public static boolean renameReportDirectory(FwkCktlReportApplicationUser appUser, String actualAttachedDirectoryName, String pendingAttachedDirectoryName) throws Exception {
		boolean res1 = renameLocalReportDirectory(appUser, actualAttachedDirectoryName, pendingAttachedDirectoryName);
		boolean res2 = renameGlobalReportDirectory(appUser, actualAttachedDirectoryName, pendingAttachedDirectoryName);
		return res1 || res2;
	}

	protected static boolean renameGlobalReportDirectory(FwkCktlReportApplicationUser appUser, String actualAttachedDirectoryName, String pendingAttachedDirectoryName) throws Exception {
		if (ERXStringUtilities.stringIsNullOrEmpty(actualAttachedDirectoryName)) {
			throw new NullPointerException("actualAttachedDirectoryName est null.");
		}
		if (ERXStringUtilities.stringIsNullOrEmpty(pendingAttachedDirectoryName)) {
			throw new NullPointerException("pendingAttachedDirectoryName est null.");
		}

		String completeSrcPath = getGlobalFilePath(appUser, actualAttachedDirectoryName);
		if (completeSrcPath == null) {
			throw new Exception("completeSrcPath est null");
		}
		String completeDestPath = getGlobalFilePath(appUser, pendingAttachedDirectoryName);
		if (completeDestPath == null) {
			throw new Exception("completeSrcPath est null");
		}
		if (fileExists(completeSrcPath)) {
			ERXFileUtilities.renameTo(new File(completeSrcPath), new File(completeDestPath));
			System.err.println("renommage de " + completeSrcPath + " vers " + completeDestPath);
			return true;
		}
		return false;
	}

	public static boolean renameLocalReportDirectory(FwkCktlReportApplicationUser appUser, String actualAttachedDirectoryName, String pendingAttachedDirectoryName) throws Exception {
		if (ERXStringUtilities.stringIsNullOrEmpty(actualAttachedDirectoryName)) {
			throw new NullPointerException("actualAttachedDirectoryName est null.");
		}
		if (ERXStringUtilities.stringIsNullOrEmpty(pendingAttachedDirectoryName)) {
			throw new NullPointerException("pendingAttachedDirectoryName est null.");
		}
		if (fileExists(getLocalDirectory(appUser))) {
			String completeSrcPath = getLocalFilePath(appUser, actualAttachedDirectoryName);
			if (completeSrcPath == null) {
				throw new Exception("completeSrcPath est null");
			}
			String completeDestPath = getLocalFilePath(appUser, pendingAttachedDirectoryName);
			if (completeDestPath == null) {
				throw new Exception("completeSrcPath est null");
			}
			if (fileExists(completeSrcPath)) {
				ERXFileUtilities.renameTo(new File(completeSrcPath), new File(completeDestPath));
				CktlLog.trace("Renommage de " + completeSrcPath + " vers " + completeDestPath);
				return true;
			}
		}
		return false;
	}

	public static File createLocalReportDirectory(FwkCktlReportApplicationUser appUser, String directoryName) throws Exception {
		File f = null;
		if (fileExists(getLocalDirectory(appUser))) {
			String completeSrcPath = getLocalFilePath(appUser, directoryName);
			f = new File(completeSrcPath);
			File p = new File(getLocalDirectory(appUser));
			if (!p.canWrite()) {
				//				throw new Exception("Impossible d'ecrire dans le repertoire " + p.getAbsolutePath());
				throw new Exception(msgCannotWriteInto(p.getAbsolutePath()));
			}
			f.mkdir();
			//System.err.println("Creation de " + completeSrcPath);
		}
		return null;
	}

	public static File createGlobalReportDirectory(FwkCktlReportApplicationUser appUser, String directoryName) throws Exception {
		File f = null;
		if (fileExists(getGlobalDirectory(appUser))) {
			String completeSrcPath = getLocalFilePath(appUser, directoryName);
			f = new File(completeSrcPath);
			File p = new File(getLocalDirectory(appUser));
			if (!p.canWrite()) {
				//				throw new Exception("Impossible d'ecrire dans le repertoire " + p.getAbsolutePath());
				throw new Exception(msgCannotWriteInto(p.getAbsolutePath()));
			}
			f.mkdir();
			//System.err.println("Creation de " + completeSrcPath);

		}
		return null;
	}

	public static void deleteLocalReportDirectory(FwkCktlReportApplicationUser appUser, String directoryName) throws Exception {
		String completeSrcPath = getLocalFilePath(appUser, directoryName);
		if (fileExists(completeSrcPath)) {
			ERXFileUtilities.deleteDirectory(new File(completeSrcPath));
			System.err.println("Suppression de " + completeSrcPath);
		}
	}

	public static void deleteGlobalReportDirectory(FwkCktlReportApplicationUser appUser, String directoryName) throws Exception {
		String completeSrcPath = getGlobalFilePath(appUser, directoryName);
		if (fileExists(completeSrcPath)) {
			ERXFileUtilities.deleteDirectory(new File(completeSrcPath));
			System.err.println("Suppression de " + completeSrcPath);
		}
	}

	public static void deleteReportDirectory(FwkCktlReportApplicationUser appUser, String directoryName) throws Exception {
		deleteLocalReportDirectory(appUser, directoryName);
		deleteGlobalReportDirectory(appUser, directoryName);
	}

	/**
	 * @return Un tableau contenant tous les fichiers trouvés pour un rapport. Si le répertoire du rapport est trouvé dans le dossier local et qu'il
	 *         contient des fichiers, ce sont ces fichiers qui sont renvoyés, sinon ce sont les fichiers trouvés dans le répartoire global.
	 */
	public static NSArray<File> getAllFilesForRapport(FwkCktlReportApplicationUser appUser, EORapport rapport, FileFilter filter) {
		NSArray<File> res = new NSArray<File>();
		String localReportDir = localReportDirectoryPath(appUser, rapport);
		if (fileExists(localReportDir)) {
			res = new NSArray<File>(ERXFileUtilities.listFiles(new File(localReportDir), false, filter));
		}
		if (res.count() == 0) {
			String globalReportDir = globalReportDirectoryPath(appUser, rapport);
			if (fileExists(globalReportDir)) {
				res = new NSArray<File>(ERXFileUtilities.listFiles(new File(globalReportDir), false, filter));
			}
		}
		return res;
	}

	public static String getUniqueFileName(FwkCktlReportApplicationUser appUser, String fileName) {
		//return Version.APPLICATION_STRID + "_" + appUser.getLogin() + "_" + new NSTimestampFormatter("%y%m%d%H%M%F").format(new NSTimestamp()) + "_" + fileName;
		return Version.APPLICATION_STRID + "_" + appUser.getLogin() + "_" + new SimpleDateFormat("yyyyMMddHHmmS").format(new Date()) + "_" + fileName;
	}

	/**
	 * Crée un sous-réertoire de parent.
	 * 
	 * @param parent
	 * @param name
	 */
	public static File createDirectory(File parent, String name) throws Exception {
		if (parent.exists() && parent.isDirectory()) {
			if (!ERXStringUtilities.stringIsNullOrEmpty(name)) {
				if (!parent.canWrite()) {
					throw new Exception(msgCannotWriteInto(parent.getAbsolutePath()));
				}
				File f = new File(parent, name);
				f.mkdir();
				return f;
			}
		}
		return null;
	}

	private static String msgNotAccessible(String param, String location) {
		return "Le chemin indiqué par le paramètre <b>" + param + "</b> : <br><b>" + location + "</b> n'est pas accessible";
	}

	private static String msgNotAccessible(String location) {
		return "Le chemin <br><b>" + location + "</b><br> n'est pas accessible";
	}

	private static String msgFillParametre(String param) {
		return "Veuillez remplir une valeur pour le paramètre <b>" + param + "</b>";
	}

	private static String msgCannotWriteInto(String location) {
		return "Impossible d'écrire dans le répertoire <br><b>" + location + "</b>";
	}

}
