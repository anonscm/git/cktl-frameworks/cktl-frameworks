/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server.files;

import org.cocktail.fwkcktlreport.server.FwkCktlReportApplicationUser;

public class CktlReportFileAction {
	public static String ACTION_RENAME_ALL = "RENAME_ALL";
	public static String ACTION_RENAME_LOCAL = "RENAME_LOCAL";
	public static String ACTION_CREATE = "CREATE";
	public static String ACTION_DELETE = "DELETE";

	private FwkCktlReportApplicationUser appUser;
	private String action;
	private String file1;
	private String file2;

	protected CktlReportFileAction(FwkCktlReportApplicationUser appUser, String action, String file1, String file2) {
		this.appUser = appUser;
		this.action = action;
		this.file1 = file1;
		this.file2 = file2;
	}

	public static CktlReportFileAction deleteAction(FwkCktlReportApplicationUser appUser, String file) {
		return new CktlReportFileAction(appUser, ACTION_DELETE, file, null);
	}

	public static CktlReportFileAction createAction(FwkCktlReportApplicationUser appUser, String file) {
		return new CktlReportFileAction(appUser, ACTION_CREATE, file, null);
	}

	/**
	 * @param appUser
	 * @param fileSrc
	 * @param fileDest
	 * @return Une action pour renommer les répertoires locaux et globaux. Dans le cas d'un archivage par exemple.
	 */
	public static CktlReportFileAction renameAllAction(FwkCktlReportApplicationUser appUser, String fileSrc, String fileDest) {
		return new CktlReportFileAction(appUser, ACTION_RENAME_ALL, fileSrc, fileDest);
	}

	/**
	 * @param appUser
	 * @param fileSrc
	 * @param fileDest
	 * @return Une action pour renommer uniquement le répertoire local. Notamment dans le cas d'un changement d'id du rapport.
	 */
	public static CktlReportFileAction renameLocalAction(FwkCktlReportApplicationUser appUser, String fileSrc, String fileDest) {
		return new CktlReportFileAction(appUser, ACTION_RENAME_LOCAL, fileSrc, fileDest);
	}

	public void performAction() throws Exception {
		try {
			if (ACTION_CREATE.equals(action)) {
				CktlReportFilesUtils.createLocalReportDirectory(appUser, file1);
			}
			if (ACTION_DELETE.equals(action)) {
				CktlReportFilesUtils.deleteReportDirectory(appUser, file1);
			}
			if (ACTION_RENAME_ALL.equals(action)) {
				CktlReportFilesUtils.renameReportDirectory(appUser, file1, file2);
			}
			if (ACTION_RENAME_LOCAL.equals(action)) {
				CktlReportFilesUtils.renameLocalReportDirectory(appUser, file1, file2);
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

}
