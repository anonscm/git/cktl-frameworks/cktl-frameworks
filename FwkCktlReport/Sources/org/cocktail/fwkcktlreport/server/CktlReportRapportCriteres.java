/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPretCrit;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlreport.server.metier.EORapportCritere;

import com.ibm.icu.text.SimpleDateFormat;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Stocke les valeurs par critere sous la forme d'un NSMutableDictionary<String, Object>. Permet d'avoir un "cache". Cle : EORapportCritere.cle. Offre
 * des méthodes pour préparer une map avec cles et valeurs exploitées lors de la génération des rapports.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportRapportCriteres extends NSMutableDictionary<String, Object> {
	private static final long serialVersionUID = 1L;
	public final static Logger logger = Logger.getLogger(CktlReportRapportCriteres.class);

	private static String CA_OPENREPORTS_USER_NAME = "CA_OPENREPORTS_USER_NAME";
	private static String REQUIRED_CRIT_CLES = CA_OPENREPORTS_USER_NAME;

	public void reset() {
		removeAllObjects();
	}

	/**
	 * Cree et renvoie un cache de criteres a partir d'un objet CktlReportRapportCriteres.
	 * 
	 * @param source
	 * @return
	 */
	//	public static CktlReportRapportCriteres createFrom(CktlReportParameters params, CktlReportRapportCriteres source) {
	//		CktlReportRapportCriteres res = new CktlReportRapportCriteres();
	//		if (params != null) {
	//			res.addEntriesFromDictionary(params);
	//		}
	//		if (source != null) {
	//			res.addEntriesFromDictionary(source);
	//		}
	//
	//		return res;
	//	}

	public static CktlReportRapportCriteres createFrom(Map<String, Object> params, CktlReportRapportCriteres source) {
		CktlReportRapportCriteres res = new CktlReportRapportCriteres();
		if (params != null) {
			res.addEntriesFromDictionary(new NSDictionary<String, Object>(params));
		}
		if (source != null) {
			res.addEntriesFromDictionary(source);
		}

		return res;
	}

	/**
	 * Verifie si les criteres obligatoires pour un report sont bien indiqués.
	 * 
	 * @param rapport
	 * @throws Exception
	 */
	public void checkRequiredCriteres(EORapport rapport) throws Exception {
		String res = "";
		Iterator<EORapportCritere> it = rapport.getRapportCritereObligatoires().iterator();
		while (it.hasNext()) {
			EORapportCritere rc = (EORapportCritere) it.next();
			Object value = get(rc.getCle());
			if (value == null) {
				res += "\n" + rc.rcrLibelleToDisplay();
			}
		}
		if (res.length() > 0) {
			throw new Exception("Les critères suivants sont obligatoires : " + res);
		}
	}

	/**
	 * Ne conserve que les criteres specifiques au rapport
	 * 
	 * @param rapport
	 */
	public void cleanCriteres(EORapport rapport) {
		@SuppressWarnings("unchecked")
		NSArray<String> crits = (NSArray<String>) rapport.toRapportCriteres().valueForKey(EORapportCritere.CLE_KEY);

		Iterator<String> it = allKeys().iterator();
		while (it.hasNext()) {
			String k = (String) it.next();
			if (!crits.contains(k) && !REQUIRED_CRIT_CLES.contains(k)) {
				remove(k);
			}
		}

	}

	@Override
	public Object put(String key, Object value) {
		if (value == null) {
			return remove(key);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Ajout du critere : " + key + "=" + value);
		}
		return super.put(key, value);
	}

	@Override
	public void removeAllObjects() {
		// TODO Auto-generated method stub
		super.removeAllObjects();
	}

	@Override
	public Object remove(Object key) {
		// TODO Auto-generated method stub
		return super.remove(key);
	}

	/**
	 * Charge les critères à partir de ceux enregistrés dans le rapport prêt
	 * 
	 * @param rapportPret
	 */
	public void loadFromRapportPret(FwkCktlReportApplicationUser appUser, EOPersRapportPret pret) {
		removeAllObjects();
		put(CA_OPENREPORTS_USER_NAME, appUser.getLogin());
		for (int i = 0; i < pret.toPersRapportPretCrits().count(); i++) {
			EOPersRapportPretCrit crit = (EOPersRapportPretCrit) pret.toPersRapportPretCrits().get(i);
			//			put(crit.toRapportCritere().getCle(), crit.prpcCritValeur().replaceAll("\"", ""));
			put(crit.toRapportCritere().getCle(), crit.toRapportCritere().toCritere().convertStringToValeur(crit.prpcCritValeur().replaceAll("\"", "")));
		}

	}

	/**
	 * @param key
	 * @return La valeur associée à la clé. Si l'objet est de type NSKeyValueCoding, la valeur associé à la clé CLE est renvoyée.
	 */
	public Object getValue(String key) {
		Object obj = get(key);
		if (obj instanceof NSKeyValueCoding) {
			return ((NSKeyValueCoding) get(key)).valueForKey("CLE");
		}
		return obj;
	}

	public Object getValueForDisplay(String key) {
		Object obj = getValue(key);
		if (obj instanceof Date) {
			return new SimpleDateFormat("dd/MM/yyyy").format((Date) obj);
		}
		return obj;
	}

	/**
	 * @return Une version preparee des criteres pour la génération du rapport
	 */
	public Map<String, Object> preparedMapForReport() {
		HashMap<String, Object> res = new HashMap<String, Object>();
		Iterator<String> keys = this.allKeys().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			Object value = getValue(key);
			if (key.equals("CA_EXER")) {
				if (!(value instanceof Double)) {
					value = Double.valueOf(value.toString());
				}
			}
			res.put(key, value);
		}
		//res.put("CA_OPENREPORTS_USER_NAME", get("CA_LOGIN"));
		//		if (res.get("CA_SOUSCR") != null) {
		//			res.put("CA_SOUS_CR", get("CA_SOUSCR"));
		//		}
		return res;
	}

	/**
	 * @return Une version preparee des criteres pour enregistrement (les objets sont sauves sous forme de String).
	 */
	public Map<String, String> preparedMapForSave() {
		HashMap<String, String> res = new HashMap<String, String>();
		Iterator<String> keys = this.allKeys().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			Object value = getValue(key);
			res.put(key, value.toString());
		}
		return res;
	}


}
