/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreport.server;

import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.metier.EOFonction;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlreport.server.metier.EOCategorie;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportHisto;
import org.cocktail.fwkcktlreport.server.metier.EOPersRapportPret;
import org.cocktail.fwkcktlreport.server.metier.EORapport;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.foundation.ERXThreadStorage;

/**
 * Représente un utilisateur qui veut generer un rapport. (Création de profils, de règles etc.).
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class FwkCktlReportApplicationUser extends ApplicationUser {
	private EOIndividu individu;
	private static final String APPLICATION_STR_ID = "CANNELLE";
	public static final String THREAD_STORAGE_KEY = "fwkCktlReportAppUser";

	//private CktlReportParameters cktlReportParameters = new CktlReportParameters();
	//	private CktlReportCriteresDic cktlReportCriteresDic = new CktlReportCriteresDic();
	//	private CktlReportRapportCriteres cktlReportRapportCriteresGlobal = CktlReportRapportCriteres.createFrom(cktlReportParameters, null);
	private CktlReportRapportCriteres cktlReportRapportCriteresGlobal;
	private NSArray<EOCategorie> categoriesAutorisees;
	private NSArray<EOCategorie> categoriesAdministrees;
	private NSArray<EORapport> rapportsAdministres = NSArray.emptyArray();
	private NSArray<EORapport> rapportsAutorises = NSArray.emptyArray();

	/**
	 * Eviter de faire appel à ce constructeur (les params ne seront pas initialisés)
	 * 
	 * @param ec
	 * @param persId
	 */
	public FwkCktlReportApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, APPLICATION_STR_ID, persId);
		ERXThreadStorage.takeValueForKey(this, THREAD_STORAGE_KEY);
		//CktlWebSession.session().takeValueForKey(this, THREAD_STORAGE_KEY);
		updateCategoriesAdministrees();
		updateCategoriesAutorisees();
		cktlReportRapportCriteresGlobal = CktlReportRapportCriteres.createFrom(FwkCktlReport.paramManager.getParams(), null);
	}

	//
	//	public FwkCktlReportApplicationUser(EOEditingContext ec, Integer persId, Map<String, Object> params) throws Exception {
	//		super(ec, APPLICATION_STR_ID, persId);
	//		updateCategoriesAdministrees();
	//		updateCategoriesAutorisees();
	//		//initialiseParametres(params);
	//		ERXThreadStorage.takeValueForKey(this, THREAD_STORAGE_KEY);
	//		cktlReportRapportCriteresGlobal = CktlReportRapportCriteres.createFrom(params, null);
	//		initialiseCriteres();
	//	}

	public FwkCktlReportApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId, Map<String, Object> params) throws Exception {
		super(ec, APPLICATION_STR_ID, persId);
		ERXThreadStorage.takeValueForKey(this, THREAD_STORAGE_KEY);
		//CktlWebSession.session().takeValueForKey(this, THREAD_STORAGE_KEY);
		updateCategoriesAdministrees();
		updateCategoriesAutorisees();
		//initialiseParametres(params);
		cktlReportRapportCriteresGlobal = CktlReportRapportCriteres.createFrom(params, null);
		initialiseCriteres();
	}

	//	private void initialiseParametres(Map<String, Object> params) throws Exception {
	//		if (params instanceof CktlConfig) {
	//			//Obligé tant que CktlConfig ne respecte pas correctement MAp
	//			cktlReportParameters.addFromConfig((CktlConfig) params);
	//		}
	//		else {
	//			cktlReportParameters.putAll(params);
	//		}
	//		//	cktlReportParameters.put(cktlReportParameters.CA_LOGIN, this.getLogin());
	//		cktlReportParameters.checkRequiredParameters();
	//	}

	private void initialiseCriteres() {
		cktlReportRapportCriteresGlobal.put("CA_LOGIN", this.getLogin());
		cktlReportRapportCriteresGlobal.put("CA_OPENREPORTS_USER_NAME", this.getLogin());
		cktlReportRapportCriteresGlobal.put("CA_EXER", DateCtrl.getCurrentYear());
		cktlReportRapportCriteresGlobal.put("CA_DATE_DEBUT", new NSTimestamp(MyDateCtrl.getFirstDayOfYear(DateCtrl.getCurrentYear())));
		cktlReportRapportCriteresGlobal.put("CA_DATE_FIN", new NSTimestamp(MyDateCtrl.getLastDayOfYear(DateCtrl.getCurrentYear())));
		//cktlReportRapportCriteresGlobal.put("CA_DATE_MIN", DateCtrl.);
	}

	public EOIndividu getIndividu() {
		if (individu == null && getPersId() != null) {
			individu = EOIndividu.fetchByKeyValue(getEditingContext(), EOIndividu.PERS_ID_KEY, getPersId());
		}
		return individu;
	}

	public NSArray getServices() {
		if (getIndividu() != null) {
			return getIndividu().getServices();
		}
		return null;
	}

	public NSArray getEtablissementsAffectation() {
		if (getIndividu() != null) {
			return getIndividu().getEtablissementsAffectation(null);
		}
		return null;
	}

	/**
	 * @return Les categories autorisees pour l'utilisateur (il faut que la catégorie soit explicitement affectée à l'utilsiateur)
	 */
	public NSArray<EOCategorie> getCategoriesAutorisees() {
		return categoriesAutorisees;
	}

	public NSArray<EOCategorie> getCategoriesAdministrees() {
		return categoriesAdministrees;
	}

	/**
	 * Les fon_id_interne contiennent les id des categories. On ajoute aux categories autorisées celles administrées.
	 */
	public void updateCategoriesAutorisees() {
		NSMutableArray quals = new NSMutableArray();
		NSMutableArray<EOCategorie> res = new NSMutableArray<EOCategorie>();
		//Pour l'instant les categories sont stockees dans JefyAdmin
		NSArray fonctions = getAllowedFonctions(null);
		for (int i = 0; i < fonctions.count(); i++) {
			EOFonction fonction = (EOFonction) fonctions.objectAtIndex(i);
			Integer cat = null;
			try {
				cat = Integer.valueOf(fonction.fonIdInterne());
			} catch (NumberFormatException e) {
				//On ne fait rien
			}
			if (cat != null) {
				quals.addObject(new EOKeyValueQualifier(EOCategorie.CAT_ID_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(cat.intValue() - 10000000)));
			}
		}
		if (quals.count() > 0) {
			EOQualifier qual = new EOOrQualifier(quals);
			res.addObjectsFromArray(EOCategorie.fetchAll(getEditingContext(), qual, null));
		}
		//On ajoute les categories administrees
		res.addObjectsFromArray(categoriesAdministrees);
		NSArrayCtrl.removeDuplicatesInNSArray(res);
		categoriesAutorisees = res;
	}

	/**
	 * Les fon_id_interne des droits d'administration des categories contiennent les id des categories + 10000000.
	 */
	public void updateCategoriesAdministrees() {
		//Si droit d'administrer Cannelle, toutes les categories sont administrables
		if (isFonctionAutoriseeByFonID(getTyapStrId(), "ADM", null)) {
			categoriesAdministrees = EOCategorie.fetchAll(getEditingContext());
		}
		else {
			NSMutableArray quals = new NSMutableArray();
			NSArray<EOCategorie> res = NSArray.EmptyArray;
			//Pour l'instant les categories sont stockees dans JefyAdmin
			NSArray fonctions = getAllowedFonctions(null);
			for (int i = 0; i < fonctions.count(); i++) {
				EOFonction fonction = (EOFonction) fonctions.objectAtIndex(i);
				Integer cat = null;
				try {
					cat = Integer.valueOf(fonction.fonIdInterne());
				} catch (NumberFormatException e) {
					//On ne fait rien
				}
				if (cat != null) {
					quals.addObject(new EOKeyValueQualifier(EOCategorie.CAT_ID_KEY, EOQualifier.QualifierOperatorEqual, cat));
				}
			}
			if (quals.count() > 0) {
				EOQualifier qual = new EOOrQualifier(quals);
				res = EOCategorie.fetchAll(getEditingContext(), qual, null);
			}
			categoriesAdministrees = res;
		}
	}

	public NSArray getAllRapportAutorises() {
		updateRapportsAutorises();
		NSArray res = rapportsAutorises;
		//Permet d'initialiser le cache des favoris
		getRapportsFavoris();
		return res;
	}

	public NSArray getAllRapportAdministres() {
		if (rapportsAdministres.count() == 0) {
			updateRapportsAdministres();
		}
		//NSArray res = EORapport.fetchAllRapportsAdministresPourUtilisateur(getEditingContext(), this, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
		return rapportsAdministres;
	}

	public NSArray getAllRapportAutorises(String filtre, Integer fetchLimit) {
		NSArray res = EORapport.fetchAllRapportsAutorisesPourUtilisateur(getEditingContext(), this, filtre, fetchLimit);
		//Permet d'initialiser le cache des favoris
		getRapportsFavoris();
		return res;
	}

	public NSArray getAllRapportAdministres(String filtre, Integer fetchLimit) {
		NSArray res = EORapport.fetchAllRapportsAdministresPourUtilisateur(getEditingContext(), this, filtre, fetchLimit);
		//Permet d'initialiser le cache des favoris
		//	getRapportsFavoris();
		return res;
	}

	public NSArray getRapportAutorisesPourCategorie(EOCategorie categorie) {
		NSArray res = EORapport.fetchRapportsAutorisesPourUtilisateur(getEditingContext(), this, categorie, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
		//Permet d'initialiser le cache des favoris
		getRapportsFavoris();
		return res;
	}

	public NSArray getRapportAdministresPourCategorie(EOCategorie categorie) {
		NSArray res = EORapport.fetchRapportsAdministresPourUtilisateur(getEditingContext(), this, categorie, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
		return res;
	}

	public NSArray getRapportsFavoris() {
		NSArray res = EORapport.fetchAllRapportsFavorisPourUtilisateur(getEditingContext(), this, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
		return res;
	}

	public NSArray getRapportsHistorique() {
		NSArray tmp = EOPersRapportHisto.fetchAllRapportHistoPourUtilisateur(getEditingContext(), this, 25);
		NSArray<EORapport> tmp2 = (NSArray<EORapport>) tmp.valueForKey(EOPersRapportHisto.TO_RAPPORT_KEY);
		NSArray<EORapport> res = NSArrayCtrl.getDistinctsOfNSArray(tmp2);
		//Permet d'initialiser le cache des favoris
		getRapportsFavoris();
		return res;
	}

	public NSArray<EOPersRapportPret> getAllRapportPrets() {
		NSArray res = EOPersRapportPret.fetchRapportsPretsPourUtilisateur(getEditingContext(), this, null);
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(res, new NSArray(EOPersRapportPret.SORT_RAPPORT_RAP_LIBELLE_ASC));
	}

	public Integer getExerciceComptableOuvert() {
		String sql = "select exe_ordre from jefy_admin.exercice where exe_stat='O'";
		NSArray res = EOUtilities.rawRowsForSQL(getEditingContext(), "FwkCktlReport", sql, null);
		if (res.count() >= 1) {
			return Integer.valueOf(((NSDictionary) res.objectAtIndex(0)).allValues().lastObject().toString());
		}
		return null;
	}

	public Map<String, Object> getApplicationParams() {
		return FwkCktlReport.paramManager.getParams();
	}

	//
	//	public CktlReportParameters getCktlReportParameters() {
	//		return cktlReportParameters;
	//	}
	//
	//	public void setCktlReportParameters(CktlReportParameters cktlReportParameters) {
	//		this.cktlReportParameters = cktlReportParameters;
	//	}

	//	public CktlReportCriteresDic getCktlReportCriteresDic() {
	//		return cktlReportCriteresDic;
	//	}
	//
	//	public void setCktlReportCriteresDic(CktlReportCriteresDic cktlReportCriteresDic) {
	//		this.cktlReportCriteresDic = cktlReportCriteresDic;
	//	}

	public CktlReportRapportCriteres getCktlReportRapportCriteresGlobal() {
		return cktlReportRapportCriteresGlobal;
	}

	//	public void setCktlReportRapportCriteresGlobal(CktlReportRapportCriteres cktlReportRapportCriteresGlobal) {
	//		this.cktlReportRapportCriteresGlobal = cktlReportRapportCriteresGlobal;
	//	}

	/**
	 * @return true si l'utilisateur possède au moins un droit d'administration.
	 */
	public Boolean hasDroitAdministration() {
		return Boolean.valueOf(getCategoriesAdministrees().count() > 0);
	}

	public Boolean hasDroitStatistiques() {
		return hasDroitAdministration();
	}

	public Boolean hasDroitNewRapport() {
		return hasDroitAdministration();
	}

	/**
	 * Vérifie si l'utilisateur a le droit d'administrer le rapport passé en parametre.
	 * 
	 * @param rapport
	 * @return
	 */
	public Boolean hasDroitAdministration(EORapport rapport) {
		return getAllRapportAdministres().contains(rapport);
	}

	public void updateRapportsAdministres() {
		rapportsAdministres = EORapport.fetchAllRapportsAdministresPourUtilisateur(getEditingContext(), this, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
	}

	public void updateRapportsAutorises() {
		rapportsAutorises = EORapport.fetchAllRapportsAutorisesPourUtilisateur(getEditingContext(), this, EORapport.SORT_ARRAY_RAP_LIBELLE_ASC);
	}

}
