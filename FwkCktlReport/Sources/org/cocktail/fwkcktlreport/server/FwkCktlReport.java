package org.cocktail.fwkcktlreport.server;

import org.apache.log4j.Logger;

import er.extensions.ERXFrameworkPrincipal;

public class FwkCktlReport extends ERXFrameworkPrincipal {

	public static final Logger logger = Logger.getLogger(FwkCktlReport.class);

	public static FwkCktlReportParamManager paramManager = new FwkCktlReportParamManager();

	// Registers the class as the framework principal
	static {
		setUpFrameworkPrincipalClass(FwkCktlReport.class);
	}

	@Override
	public void didFinishInitialization() {
		super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();
	}

	@Override
	public void finishInitialization() {
	}
}
