package org.cocktail.fwkcktldroitsperimetrerecherche.serveur;

import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

/**
 * Gestion des parametres
 */
public class FwkCktlDroitsPerimetreRechercheParamManager extends CktlParamManager {

	@Override
	public void createNewParam(String key, String value, String comment) {
		// TODO Auto-generated method stub
	}

	@Override
	public void createNewParam(String key, String value, String comment, String type) {
		// TODO Auto-generated method stub
	}

	@Override
	public String getParam(String key) {
		return getApplication().config().stringForKey(key);
	}

}
