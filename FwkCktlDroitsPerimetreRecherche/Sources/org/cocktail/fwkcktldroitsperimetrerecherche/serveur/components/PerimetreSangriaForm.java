package org.cocktail.fwkcktldroitsperimetrerecherche.serveur.components;


import org.cocktail.fwkcktldroitsperimetrerecherche.serveur.commun.ModeEdition;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdStrategieSangria;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

import er.ajax.AjaxUpdateContainer;

public class PerimetreSangriaForm extends FwkCktlDroitsPerimetreRechercheFormComponent {
	
	private static final long serialVersionUID = 1L;
	private static final String BINDING_PERIMETRE = "perimetre";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	private static final String BINDING_MODE_EDITION = "modeEdition";
	
	private ModeEdition modeEdition;
	
	private EOGdStrategieSangria currentTypeStrategie;
	private EOGdStrategieSangria selectedTypeStrategie;
	
    public PerimetreSangriaForm(WOContext context) {
        super(context);
    }
    
    @Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setSelectedTypeStrategie(null);

		super.appendToResponse(response, context);
	}
    
    public String getLibelleId() {
    	return getComponentId() + "_libelleId";                                                              
    }
    
    public String getTypeStrategieId() {
    	return getComponentId() + "_typeStrategieId";                                                              
    }
    
    public String getPerimetreLibelleContainerId() {
		return getComponentId() + "_perimetreLibelleContainerId";
	}
    
    public String getTypeStrategieContainerId() {
		return getComponentId() + "_typeStrategieContainerId";
	}
    
    /**
     * @return EOGdPerimetreSangria : le perimetre passe en binding
     */
    public EOGdPerimetreSangria getPerimetre() {
		return ((EOGdPerimetreSangria) valueForBinding(BINDING_PERIMETRE));
	}
    
    /**
	 * @return mode d'edition
	 */
	public ModeEdition modeEdition() {
		if (hasBinding(BINDING_MODE_EDITION)) {
			setModeEdition((ModeEdition) valueForBinding(BINDING_MODE_EDITION));
		} else {
			setModeEdition(ModeEdition.UNKNOWN);
		}
		
		return modeEdition;
	}

	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}
    
	public boolean isModeEditionCreation() {
		return (modeEdition().equals(ModeEdition.CREATION));
	}

	public boolean isModeEditionModification() {
		return (modeEdition().equals(ModeEdition.MODIFICATION));
	}

	public boolean isModeEditionConsultation() {
		return (modeEdition().equals(ModeEdition.CONSULTATION));
	}

	public boolean isModeEditionUnknown() {
		return (modeEdition().equals(ModeEdition.UNKNOWN));
	}
	
	public EOGdApplication getGdApplication() {
	    return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
    }
    
    /**
	 * @param gdApplication : application concerne par la gestion des droits
	 */
	public void setGdApplication(EOGdApplication gdApplication) {
	    setValueForBinding(gdApplication, BINDING_GD_APPLICATION);
    }
	
	/**
	 * @return NSArray<EOGdStrategieSangria> : liste des types de strategies
	 */
	public NSArray<EOGdStrategieSangria> typesStrategies() {
		return EOGdStrategieSangria.fetchAll(edc());
	}

	public EOGdStrategieSangria getCurrentTypeStrategie() {
		return currentTypeStrategie;
	}
	
	public void setCurrentTypeStrategie(EOGdStrategieSangria currentTypeStrategie) {
		this.currentTypeStrategie = currentTypeStrategie;
	}

	/**
	 * Retourne le type de strategie
	 * Si en mode modification le type de strategie est sette a celui du perimetre passe en binding
	 * @return EOGdStrategieSangria : le type de strategie selectionnee
	 */
	public EOGdStrategieSangria getSelectedTypeStrategie() {
		if (isModeEditionModification() && getPerimetre().strategie() != null) {

			if (selectedTypeStrategie == null) {
				selectedTypeStrategie = getPerimetre().strategie();
			}
		}

		return selectedTypeStrategie;
	}
	
	/**
     * Set le type de strategie en parametre et le set aussi a la strategie du perimetre passe en binding
     * @param selectedTypeStrategie : le type de strategie selectionne
     */
	public void setSelectedTypeStrategie(EOGdStrategieSangria selectedTypeStrategie) {
		this.selectedTypeStrategie = selectedTypeStrategie;
		
		if (selectedTypeStrategie != null) {
			getPerimetre().setStrategieRelationship(selectedTypeStrategie);
			getPerimetre().setLibelle(selectedTypeStrategie.libelle());
//			getPerimetre().setComposant(null);
//			getPerimetre().setTypeComposant(null);
//			getPerimetre().setStructure(null);
//			getPerimetre().setGradeUniversitaire(null);
//			getPerimetre().setAnnee(null);
			
		}
	}
	
	public boolean isTypeStrategieAccesConsultationDemandesSubventionStructureSelected() {
		return EOGdStrategieSangria.strategieAccesConsultationDemandesSubventionStructure(edc()).equals(getSelectedTypeStrategie());
	}
	
	public boolean isTypeStrategieAccesConsultationToutesDemandesSubventionStructureSelected() {
		return EOGdStrategieSangria.strategieAccesConsultationToutesDemandesSubventionStructure(edc()).equals(getSelectedTypeStrategie());
	}

	/**
	 * @return WOActionResults : update les containers et reste sur la page
	 */
	public WOActionResults update() {
		AjaxUpdateContainer.updateContainerWithID(getPerimetreLibelleContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(getTypeStrategieContainerId(), context());
		return doNothing();
	}
}