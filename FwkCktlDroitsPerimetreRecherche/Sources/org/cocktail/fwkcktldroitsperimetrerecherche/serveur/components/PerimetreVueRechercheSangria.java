package org.cocktail.fwkcktldroitsperimetrerecherche.serveur.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktldroitsperimetrerecherche.serveur.commun.ModeEdition;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdApplication;
import org.cocktail.fwkcktlpersonne.common.metier.droits.EOGdProfil;
import org.cocktail.fwkcktlrecherche.server.metier.EOGdPerimetreSangria;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;

/**
 * Classe gerant les perimetres de sangria
 */
public class PerimetreVueRechercheSangria extends FwkCktlDroitsPerimetreRechercheFormComponent {
	// Private fields
	private static final long serialVersionUID = 1L;
	private static final String BINDING_PROFIL = "profil";
	private static final String BINDING_GD_APPLICATION = "gdApplication";
	private ERXDisplayGroup<EOGdPerimetreSangria> displayGroupPerimetres = null;

	private EOGdPerimetreSangria editedPerimetre;
	private EOGdPerimetreSangria selectedPerimetre;
	private EOGdPerimetreSangria currentPerimetreSangria;
	private EOArrayDataSource perimetreDatasource;

	// Constructor
	/**
	 * @param context : contexte d'edition
	 */
	public PerimetreVueRechercheSangria(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		displayGroupPerimetres = null;
		setSelectedPerimetre(null);
		super.appendToResponse(response, context);
	}

	// Properties
	public String getPerimetresContainerId() {
		return getComponentId() + "_perimetresContainerId";
	}

	public String getBoutonsContainerId() {
		return getComponentId() + "_boutonsContainerId";
	}

	public String getPerimetreWindowId() {
		return getComponentId() + "_ajouterPerimetreWindowId";
	}

	public String getBoutonsPerimetreWindowContainerId() {
		return getComponentId() + "_boutonsPerimetreWindowContainerId";
	}

	public String getTableViewContainerId() {
		return getComponentId() + "_tableViewContainerId";
	}
	
	public String getTableViewId() {
		return getComponentId() + "_tableViewId";
	}

	public EOGdProfil getProfil() {
		return (EOGdProfil) valueForBinding(BINDING_PROFIL);
	}

	public EOGdApplication getGdApplication() {
		return (EOGdApplication) valueForBinding(BINDING_GD_APPLICATION);
	}

	public EOGdPerimetreSangria getEditedPerimetre() {
		return editedPerimetre;
	}

	public void setEditedPerimetre(EOGdPerimetreSangria perimetreEdite) {
		this.editedPerimetre = perimetreEdite;
	}

	/**
	 * @return le display group des perimetres du profil pour l'application gdApplication
	 */
	public ERXDisplayGroup<EOGdPerimetreSangria> getDisplayGroupPerimetres() {
		if (displayGroupPerimetres == null) {
			displayGroupPerimetres = new ERXDisplayGroup<EOGdPerimetreSangria>();
			displayGroupPerimetres.setDelegate(new DisplayGroupDelegate());
			displayGroupPerimetres.setDataSource(getPerimetresDatasource());

			NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOGdPerimetreSangria.STRATEGIE_KEY, EOSortOrdering.CompareAscending));
			displayGroupPerimetres.setSortOrderings(sortOrderings);

			displayGroupPerimetres.setQualifier(EOGdPerimetreSangria.APPLICATION.eq(getGdApplication()).and(
					EOGdPerimetreSangria.PROFILS.containsObject(getProfil())));

			displayGroupPerimetres.setSelectsFirstObjectAfterFetch(false);
			displayGroupPerimetres.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupPerimetres.fetch();
		}

		return displayGroupPerimetres;
	}
	
	/**
	 * Display group pour la table des périmètres de sangria
	 */
	public class DisplayGroupDelegate {

		/**
		 * @param group : le groupe d'objets du tableau
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EOGdPerimetreSangria> _groupe = (ERXDisplayGroup<EOGdPerimetreSangria>) group;
			if (_groupe.selectedObject() != null) {
				setSelectedPerimetre(_groupe.selectedObject());
			} else {
				setSelectedPerimetre(null);
			}
		}
	}

	/**
	 * Retourne le titre dans la fenetre selon le modeEdition
	 * @return String
	 */
	private String getTitreWindow() {
		if (getModeEdition() != null) {
			switch (getModeEdition()) {
			case CREATION:
				return localisation("PerimetresSrch.WindowPerimetre.TitreAjouter");
			case MODIFICATION:
				return localisation("PerimetresSrch.WindowPerimetre.TitreModifier");
			default:
				return localisation("PerimetresSrch.WindowPerimetre.TitreUnknown");
			}
		}

		return localisation("PerimetresSrch.WindowPerimetre.titreUnknown");
	}

	public EOGdPerimetreSangria getSelectedPerimetre() {
		return selectedPerimetre;
	}

	public void setSelectedPerimetre(EOGdPerimetreSangria selectedPerimetre) {
		this.selectedPerimetre = selectedPerimetre;
	}

	public boolean isNotSelectedPerimetre() {
		return (getSelectedPerimetre() == null);
	}

	public EOGdPerimetreSangria getCurrentPerimetreSangria() {
		return currentPerimetreSangria;
	}

	public void setCurrentPerimetreSangria(EOGdPerimetreSangria currentPerimetreSangria) {
		this.currentPerimetreSangria = currentPerimetreSangria;
	}

	// Public Methods
	@Override
	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	/**
	 * Ouvre une fenetre qui permet de creer un perimetre et de l'ajouter
	 * @return null (reste sur la page)
	 */
	public WOActionResults ajouter() {
		try {
			EOGdPerimetreSangria nouveauPerimetre = EOGdPerimetreSangria.create(edc());
			nouveauPerimetre.setApplicationRelationship(getGdApplication());
			nouveauPerimetre.setAppId(Integer.valueOf(getGdApplication().primaryKey()));
			nouveauPerimetre.setLibelle("");

			setEditedPerimetre(nouveauPerimetre);

		} catch (ValidationException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}

		setModeEdition(ModeEdition.CREATION);
		CktlAjaxWindow.open(context(), getPerimetreWindowId(), getTitreWindow());

		return doNothing();
	}

	/**
	 * Ouvre une fenetre qui permet de modifier le perimetre selectionne
	 * @return null (reste sur la page)
	 */
	public WOActionResults modifier() {
		if (getSelectedPerimetre() != null) {

			setEditedPerimetre(getSelectedPerimetre());

			setModeEdition(ModeEdition.MODIFICATION);
			CktlAjaxWindow.open(context(), getPerimetreWindowId(), getTitreWindow());
		}
		return doNothing();
	}

	/**
	 * @return enregistre les modifications sur le perimetres
	 */
	public WOActionResults modifierPerimetre() {
		if (!statutValidation()) {
			return null;
		}

		if (enregistrerPerimetre()) {
			updateDisplaygroup();
			AjaxUpdateContainer.updateContainerID(getTableViewContainerId());
			update();

			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));

		}

		return doNothing();
	}

	/**
	 * @return message de confirmation avant la suppression du perimetre
	 */
	public String onClickBeforeSupprimer() {
		String msgConfirm = localisation("PerimetresSrch.ConfirmerSuppression");
		return "confirm('" + msgConfirm + "')";
	}

	/**
	 * Supprime le perimetre selectionne apres demande de confirmation
	 * @return null (reste sur la page)
	 */
	public WOActionResults supprimer() {
		if (getSelectedPerimetre() != null) {
			getProfil().removeFromPerimetresRelationship(getSelectedPerimetre());
			edc().saveChanges();
			session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetreVueScolarite.PerimetreEnleveDuProfil"));

			if (getSelectedPerimetre().profils().count() == 0) {
				getSelectedPerimetre().delete();
				session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreSupprime"));
			}

			edc().saveChanges();
			updateDisplaygroup();
		}
		return doNothing();
	}

	/**
	 * Enregistre un nouveau perimetre
	 * @return WOActionResults
	 */
	public WOActionResults enregistrerEtCreerPerimetre() {
		if (!statutValidation()) {
			return null;
		}
		if (enregistrerPerimetreProfil()) {
			update();
			CktlAjaxWindow.close(context(), getPerimetreWindowId());

			session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetresSrch.PerimetreEnregistre"));
		}

		return doNothing();
	}

	/**
	 * annule l'edition en cours et ferme la fenếtre
	 * @return null (reste sur la page)
	 */
	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), getPerimetreWindowId());

		return doNothing();
	}

	// Private methods
	/**
	 * mise a jour des ecrans suite a la selection
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {
		getDisplayGroupPerimetres().updateDisplayedObjects();
		setSelectedPerimetre(getDisplayGroupPerimetres().selectedObject());

		AjaxUpdateContainer.updateContainerWithID(getTableViewContainerId(), context());
		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());

		return doNothing();
	}

	/**
	 * @return null
	 */
	public WOActionResults onSelect() {
		AjaxUpdateContainer.updateContainerWithID(getBoutonsContainerId(), context());
		return doNothing();
	}

	private void updateDisplaygroup() {
		getDisplayGroupPerimetres().setObjectArray(getPerimetres());
		getDisplayGroupPerimetres().fetch();
		getDisplayGroupPerimetres().updateDisplayedObjects();
	}

	/**
	 * enregistre l'association du perimetre pour le profil existant et cree le perimetre au besoin
	 * @return boolean : indique si l'enregistrement s'est bien passe
	 */
	private boolean enregistrerPerimetreProfil() throws ValidationException {
		try {
			cherchePerimetreSimilaireExistant();

			if (!getProfil().perimetres().contains(getEditedPerimetre())) {
				getEditedPerimetre().addToProfilsRelationship(getProfil());
				edc().saveChanges();
				updateDisplaygroup();
			} else {
				session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetreVueScolariteGirofle.PerimetreDejaAssocie"));
				edc().revert();
			}
			return true;
		} catch (ValidationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * enregistre le perimetre en cours d'edition
	 * @return boolean : indique si l'enregistrement s'est bien passe
	 */
	private boolean enregistrerPerimetre() throws ValidationException {
		try {
			edc().saveChanges();
			return true;
		} catch (ValidationException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private void cherchePerimetreSimilaireExistant() {
		EOGdPerimetreSangria perimetreSimilaire = perimetreSimilaire(getEditedPerimetre());
		if (perimetreSimilaire != null) {
			session().addSimpleSuccessMessage(localisation("application.OK"), localisation("PerimetreVueScolariteGirofle.UtilisationPerimetreExistant"));
			edc().deleteObject(getEditedPerimetre());
			setEditedPerimetre(perimetreSimilaire);
		} else {
			getDisplayGroupPerimetres().insertObjectAtIndex(getEditedPerimetre(), 0);
		}
	}

	/**
	 * @return perimetre similaire au perimetre courant (même attributs)
	 */
	private EOGdPerimetreSangria perimetreSimilaire(EOGdPerimetreSangria perimetre) {
		EOQualifier qualifier = ERXQ.and(
				EOGdPerimetreSangria.APPLICATION.eq(perimetre.application()),
				EOGdPerimetreSangria.STRATEGIE.eq(perimetre.strategie())
				);
		return EOGdPerimetreSangria.fetchGdPerimetreSangria(edc(), qualifier);
	}

	/**
	 * @return ERXDatabaseDataSource : data sources des perimetres
	 */
	private EOArrayDataSource getPerimetresDatasource() {
		if (perimetreDatasource == null) {
			perimetreDatasource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOGdPerimetreSangria.class), edc());
			perimetreDatasource.setArray(getPerimetres());
		}
		return perimetreDatasource;
	}

	private NSArray<EOGdPerimetreSangria> getPerimetres() {
		return ERXArrayUtilities.arrayWithoutDuplicates(EOGdPerimetreSangria.fetchGdPerimetreSangrias(edc(), null, null));
	}
}