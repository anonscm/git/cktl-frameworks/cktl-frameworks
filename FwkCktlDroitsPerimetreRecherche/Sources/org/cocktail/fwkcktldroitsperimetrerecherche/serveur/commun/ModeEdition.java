package org.cocktail.fwkcktldroitsperimetrerecherche.serveur.commun;

/**
 * Cet enum liste les différents modes d'éditions possibles.
 */
public enum ModeEdition {
    UNKNOWN, CREATION, MODIFICATION, DUPLICATION, CONSULTATION;
}