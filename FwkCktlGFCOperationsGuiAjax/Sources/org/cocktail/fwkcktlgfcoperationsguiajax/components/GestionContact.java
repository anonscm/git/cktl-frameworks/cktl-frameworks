/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory.FactoryOperationPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

/**
 * Page de gestion des contact (contient l'assistant de création des contacts). Typiquement ouvert dans une modalWindow.
 * ATTENTION : Veillez à bien appeler les setters suivant :
 * - operation
 * - partenaire
 * - selectedPersonne
 * - contact
 * - applicationUser
 * - isApplicationInDebugMode
 * - indexModulActif
 * - avecModuleDeRecherche
 * - isDirectAction
 * - applicationMessageContainerID
 *  selon votre besoin...
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class GestionContact extends ACktlAjaxGFCOperationsComponent {
	private static final long serialVersionUID = 8563271771157070338L;
	
	private NSArray modules;
	private NSArray<String> etapes;
	private Operation operation;
	private OperationPartenaire partenaire;
	private OperationPartContact contact;
	private IPersonne selectedPersonne;
	private NSArray roles;
	private boolean avecModuleDeRecherche = true;

	Boolean isDirectAction = null;
	private boolean isSuccess;

	private FwkCktlGFCOperationsApplicationUser applicationUser;
	private boolean isApplicationInDebugMode;
	private Integer indexModuleActif;
	private String applicationMessageContainerID;

	private EOEditingContext editingContextForPersonne;
	
	public GestionContact(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlGFCOperationsGuiAjax.framework", "css/fwkcktlgfcoperationsguiajax.css");
	}

	public WOActionResults annuler() {
		CktlAjaxWindow.close(context());
		isSuccess = false;
		return null;
	}

	public WOActionResults terminer() throws ExceptionFinder {
		OperationPartenaire lePartenaire = partenaire();
		IPersonne personne = selectedPersonne();
		if (personne != null && lePartenaire != null) {
			try {
			    // On enregistre d'abord la personne
				if (editingContextForPersonne().hasChanges()) {
					personne.setPersIdModification(applicationUser().getPersonne().persId());
					editingContextForPersonne().saveChanges();
				}
				if (contact() == null) {
					FactoryOperationPartenaire fap = new FactoryOperationPartenaire(editingContextForContact(), isApplicationInDebugMode());
					IPersonne personneAAjouter = (IPersonne) EOUtilities.localInstanceOfObject(editingContextForContact(), selectedPersonne());
					fap.ajouterContact(partenaire(), personneAAjouter, roles());
				} else {
					Operation operation = partenaire.operation();
					contact().personne().definitLesRoles(editingContextForContact(), roles(),
							operation.groupePartenaire(),
							applicationUser().getPersonne().persId(),
							operation.dateDebut(), operation.dateFin(), null, null,
							null, true);
				}
//				if (isDirectAction() != null && isDirectAction().booleanValue()==true) {
//				    partenaire.operation().setShouldNotValidate(true);
//					ed.saveChanges();
//				}
				isSuccess = true;
				CktlAjaxWindow.close(context());
			}  catch (ValidationException e2) {
				personne.editingContext().revert();
				context().response().setStatus(500);
				mySession().addSimpleErrorMessage("Erreur", e2);
			} catch (Exception e) {
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}

		}
		return null;
	}

	public String moduleName() {
		String moduleName = null;
		if (modules() != null && modules().count() > 0) {
			moduleName = (String) modules().objectAtIndex(indexModuleActif().intValue());
		}
		return moduleName;
	}

	/**
	 * @return the modules
	 */
	public NSArray modules() {
		if (contact() != null) {
			if (contact().personne().isStructure()) {
				modules = new NSArray<String>(new String[] {
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModuleContactRoles", 
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse",
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneMembres", 
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupeAdmin" });
			} else {
				modules = new NSArray<String>(new String[] {
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModuleContactRoles", 
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupe", 
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse" });
			}
		} else if (selectedPersonne() != null) {
			if (avecModuleDeRecherche()) {
				modules = new NSArray<String>(
						new String[] { "org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneSearch" });
			} else {
				modules = new NSArray<String>();
			}
			if (selectedPersonne().isStructure()) {
				modules = modules.arrayByAddingObjectsFromArray(new NSArray<String>(
								new String[] { "org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModuleContactRoles",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneMembres",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupeAdmin" }));
			} else {
				modules = modules.arrayByAddingObjectsFromArray(new NSArray<String>(
								new String[] { "org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModuleContactRoles",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupe",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
										"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse" }));
			}
		} else {
			modules = new NSArray<String>(
					new String[] { "org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneSearch" });
		}
//		session.setModulesGestionContact(modules); //TODO : Verifier dans les autres composants que la référence soit passée par binding et non-plus par session
		return modules;
	}

	/**
	 * @param modules
	 *            the modules to set
	 */
	public void setModules(NSArray modules) {
		this.modules = modules;
//		((Session) session()).setModulesGestionContact(modules); //TODO : Verifier dans les autres composants que la référence soit passée par binding et non-plus par session
		
	}

	public NSArray<String> etapes() {
		if (contact() != null) {
			if (contact().personne().isStructure()) {
				etapes = new NSArray<String>(new String[] { "R&ocirc;les",
						"Informations", "T&eacute;l&eacute;phones", "Adresses",
						"Membres", "Groupe" });
			} else {
				etapes = new NSArray<String>(new String[] { "R&ocirc;les",
						"Informations", "Groupes", "T&eacute;l&eacute;phones",
						"Adresses" });
			}
		} else if (selectedPersonne() != null) {
			if (avecModuleDeRecherche()) {
				etapes = new NSArray<String>(new String[] { "Recherche" });
			} else {
				etapes = new NSArray<String>();
			}
			if (selectedPersonne().isStructure()) {
				etapes = etapes
						.arrayByAddingObjectsFromArray(new NSArray<String>(
								new String[] { "R&ocirc;les", "Informations",
										"T&eacute;l&eacute;phones", "Adresses",
										"Membres", "Groupe" }));
			} else {
				etapes = etapes
						.arrayByAddingObjectsFromArray(new NSArray<String>(
								new String[] { "R&ocirc;les", "Informations",
										"Groupes", "T&eacute;l&eacute;phones",
										"Adresses" }));
			}
		} else {
			etapes = new NSArray<String>(new String[] { "Recherche" });
		}
		return etapes;
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContextForPersonne() {
	    if (editingContextForPersonne == null) {
	        editingContextForPersonne = ERXEC.newEditingContext();
	    }
	    return editingContextForPersonne;
	}

	public EOEditingContext editingContextForContact() {
	    if (operation() != null)
	        return operation().editingContext();
	    else
	        return null;
	}
	
	/**
	 * @return the selectedPersonne
	 */
	public IPersonne selectedPersonne() {
		return selectedPersonne;
	}

	/**
	 * @param selectedPersonne
	 *            the selectedPersonne to set
	 */
	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}

	/**
	 * @return the partenaire
	 */
	public OperationPartenaire partenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire
	 *            the partenaire to set
	 */
	public void setPartenaire(OperationPartenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the contact
	 */
	public OperationPartContact contact() {
		return contact;
	}

	/**
	 * @param contact
	 *            the contact to set
	 */
	public void setContact(OperationPartContact contact) {
		this.contact = contact;
	}

	/**
	 * @return the roles
	 */
	public NSArray roles() {
		return roles;
	}

	/**
	 * @param roles
	 *            the roles to set
	 */
	public void setRoles(NSArray roles) {
		this.roles = roles;
	}

	/**
	 * @return the operation
	 */
	public Operation operation() {
		return operation;
	}

	/**
	 * @param operation
	 *            the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public boolean avecModuleDeRecherche() {
		return avecModuleDeRecherche;
	}

	public void setAvecModuleDeRecherche(boolean avecModuleDeRecherche) {
		this.avecModuleDeRecherche = avecModuleDeRecherche;
	}

	/**
	 * @return the isDirectAction
	 */
	public Boolean isDirectAction() {
		return isDirectAction;
	}

	/**
	 * @param isDirectAction the isDirectAction to set
	 */
	public void setIsDirectAction(Boolean isDirectAction) {
		this.isDirectAction = isDirectAction;
	}
	
	public boolean isSuccess() {
        return isSuccess;
    }
	
	/**
	 * @return the applicationUser
	 */
	public FwkCktlGFCOperationsApplicationUser applicationUser() {
		return applicationUser;
	}

	/**
	 * @param applicationUser the applicationUser to set
	 */
	public void setApplicationUser(FwkCktlGFCOperationsApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}

	/**
	 * @return the isApplicationInDebugMode
	 */
	public boolean isApplicationInDebugMode() {
		return isApplicationInDebugMode;
	}

	/**
	 * @param isApplicationInDebugMode the isApplicationInDebugMode to set
	 */
	public void setApplicationInDebugMode(boolean isApplicationInDebugMode) {
		this.isApplicationInDebugMode = isApplicationInDebugMode;
	}
	
	public Integer indexModuleActif() {
		return indexModuleActif;
	}
	/**
	 * @param indexModuleActif the indexModuleActif to set
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}

	/**
	 * @return the applicationMessageContainerID
	 */
	public String applicationMessageContainerID() {
		return applicationMessageContainerID;
	}

	/**
	 * @param applicationMessageContainerID the applicationMessageContainerID to set
	 */
	public void setApplicationMessageContainerID(String applicationMessageContainerID) {
		this.applicationMessageContainerID = applicationMessageContainerID;
	}
	
	/**
	 * @return the onFailure JS call string
	 */
	public String onFailure() {
		String onFailureStr = "";
		if (applicationMessageContainerID!=null && applicationMessageContainerID.length()>0 ) {
			onFailureStr = "function () {"+applicationMessageContainerID+"Update();}";
		}
		return onFailureStr;
	}
	
	
	public String localMessageContainerID() {
		return componentId()+"_GestionContact_FwkCktlGFCOperationsGuiAjax_MessageContainer";
	}
}
