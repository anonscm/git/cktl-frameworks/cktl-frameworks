/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;

public class AssistantPartenaire extends Assistant {

	public static final String OPERATION_BDG = "operation";
	public static final String PARTENAIRE_BDG = "partenaire";
	public static final String PERSONNE_BDG = "personne";
	
	private Operation operation;
	private OperationPartenaire partenaire;
	private IPersonne personne;
	
	public AssistantPartenaire(WOContext context) {
        super(context);
    }
	
	public void reset() {
		operation = null;
		partenaire = null;
		personne = null;
	}

	public String updateContainerID() {
		return "ContainerGestionPartenaire";
	}
	
	public String updateContainerIDOnSelectionnerPersonneInTableview() {
		return "ContainerGestionPartenaireMenu";
	}

	/**
	 * @return the operation
	 */
	public Operation operation() {
		if (hasBinding(OPERATION_BDG)) {
			operation = (Operation)valueForBinding(OPERATION_BDG);
		}
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
		if (hasBinding(OPERATION_BDG)) {
			setValueForBinding(operation, OPERATION_BDG);
		}
	}

	/**
	 * @return the partenaire
	 */
	public OperationPartenaire partenaire() {
		if (hasBinding(PARTENAIRE_BDG)) {
			partenaire = (OperationPartenaire)valueForBinding(PARTENAIRE_BDG);
		}
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(OperationPartenaire partenaire) {
		this.partenaire = partenaire;
		if (hasBinding(PARTENAIRE_BDG)) {
			setValueForBinding(partenaire, PARTENAIRE_BDG);
		}
	}

	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		if (hasBinding(PERSONNE_BDG)) {
			personne = (IPersonne) valueForBinding(PERSONNE_BDG);
		}
		return personne;
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		this.personne = personne;
		if (hasBinding(PERSONNE_BDG)) {
			setValueForBinding(personne, PERSONNE_BDG);
		}
	}

	public boolean isTerminerDisabled() {
		boolean isTerminerDisabled = (partenaire() == null && personne() == null);
		
		return isTerminerDisabled;
	}
}
