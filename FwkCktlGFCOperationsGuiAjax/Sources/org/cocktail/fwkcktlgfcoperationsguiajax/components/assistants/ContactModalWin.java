/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.ACktlAjaxGFCOperationsComponent;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.GestionContact;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.ajax.AjaxUtils;

public class ContactModalWin extends ACktlAjaxGFCOperationsComponent {

	private OperationPartenaire partenaire = null;
	private OperationPartContact contact = null;
	private GestionContact pageInModal;
	
	private Operation operation;


	public ContactModalWin(WOContext context) {
        super(context);
    }

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		super.appendToResponse(response, context);

		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/strings.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/formatteurs.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/gfcoperations.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/gfcoperations.css");
		
	}

	public WOActionResults ajouterOuModifierUnContact() {
	    pageInModal = (GestionContact)pageWithName(GestionContact.class.getName());
	    pageInModal.setPartenaire(partenaire());
	    pageInModal.setOperation(partenaire().operation());
		IPersonne personne = null;
		if (contact() != null) {
			personne = contact().personne().localInstanceIn(pageInModal.editingContextForPersonne());
		}
		pageInModal.setContact(contact());
		pageInModal.setSelectedPersonne(personne);
		pageInModal.setIsDirectAction(Boolean.TRUE);
		//session.setIndexModuleActifGestionContact(null);
		//TODO : faire un binding
		
		return pageInModal;
	}

	public boolean showMessageSuccess() {
	    return pageInModal != null && pageInModal.isSuccess();
	}

	public boolean showMessageCancel() {
	    return pageInModal != null && !pageInModal.isSuccess();
	}
	
	/**
	 * @return the partenaire
	 */
	public OperationPartenaire partenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(OperationPartenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the contact
	 */
	public OperationPartContact contact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(OperationPartContact contact) {
		this.contact = contact;
	}
	
	/**
	 * @return the contrat
	 */
	public Operation operation() {
		return operation;
	}

	/**
	 * @param contrat
	 *            the contrat to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public String messageContainerID() {
		return "contactmod_FwkCktlGFCOperationsGuiAjax_MessageContainer";
	}
}
