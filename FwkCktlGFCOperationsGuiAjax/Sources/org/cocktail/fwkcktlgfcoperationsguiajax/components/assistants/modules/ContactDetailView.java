/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules;

import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.ACktlAjaxGFCOperationsComponent;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

public class ContactDetailView extends ACktlAjaxGFCOperationsComponent {
	private static final long serialVersionUID = -6552206461216506264L;
	
	public static String BINDING_editingContext = "editingContext";
	public static String BINDING_contact = "contact";
	public static String BINDING_applicationUser = "applicationUser";
	
	private EORepartAssociation unRole;
	
    public ContactDetailView(WOContext context) {
        super(context);
    }

	/**
	 * @return the contact
	 */
	public OperationPartContact contact() {
		return (OperationPartContact)valueForBinding(BINDING_contact);
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(OperationPartContact contact) {
		if (canSetValueForBinding(BINDING_contact)) {
			setValueForBinding(contact, BINDING_contact);
		}
	}

	/**
	 * @return the unRole
	 */
	public EORepartAssociation unRole() {
		return unRole;
	}

	/**
	 * @param unRole the unRole to set
	 */
	public void setUnRole(EORepartAssociation unRole) {
		this.unRole = unRole;
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContext() {
		return (EOEditingContext)valueForBinding(BINDING_editingContext);
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		if (canSetValueForBinding(BINDING_editingContext)) {
			setValueForBinding(editingContext, BINDING_editingContext);
		}
	}
	
	public FwkCktlGFCOperationsApplicationUser applicationUser() {
	    return (FwkCktlGFCOperationsApplicationUser) valueForBinding(BINDING_applicationUser);
	}
	
}
