/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory.FactoryOperationPartenaire;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.GestionContact;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.GestionPartenaire;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.Assistant;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.controlers.ConventionPartenairesEtContactsCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSValidation;
import com.webobjects.woextensions.WOExceptionPage;

import er.extensions.eof.ERXEC;

/**
 * Composant de gestion des partenaires et des contacts d'une convention.
 * Ce composant peut être une étape d'un assistant (création d'une convention par exemple), mais aussi être utilisé en 
 * mode 'standalone' comme un composant indépendant (dans un onglet par exemple).
 * 
 * @binding etablissementsAffectation : obligatoire liste des établissements d'affectation.
 * 
 * @binding editingContext : facultatif editingContext à utiliser. (passé par l'assistant si le composant est dans un assistant) 
 * @binding applicationUser : obligatoire CocoworkApplicationUser représentant l'utilisateur manipulant les partenaires. (passé par l'assistant si le composant est dans un assistant) 
 * @binding isAppInDebugMode : facultatif flag disant si l'application est en mode debug ou non. (passé par l'assistant si le composant est dans un assistant) 
 * @binding updateContainerID : facultatif id du container à mettre à jour après avoir modifié, ajouté un partenaire ou un contact
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class ConventionPartenairesEtContacts extends ModuleAssistant {
	private static final long serialVersionUID = -1392179663746384811L;
	
	public static final String BDG_ETABLISSEMENTS_AFFECTATION = "etablissementsAffectation";
	public static final String BDG_PARTENAIRE_INDEX_MODULE_ACTIF = "indexPartenaireModuleActif";
	public static final String BDG_CONTACT_INDEX_MODULE_ACTIF = "indexContactModuleActif";
	public static final String BDG_UPDATE_CONTAINER_ID = "updateContainerID";

	public ConventionPartenairesEtContactsCtrl ctrl = null;
	
	private NSArray etablissementsAffectation;
	
	public ConventionPartenairesEtContacts(WOContext context) {
        super(context);
    }

	@Override
	public boolean disabled() {
	    return super.disabled() || ctrl().nePeutEditerPartenaires();
	}
	
	public String disabledAttribut() {
		return getOtherTagAttribut(disabled());
	}
	
	private String getOtherTagAttribut(boolean isDisabled) {
		if (isDisabled) {
			return "disabled";
		}
		return "";
	}
	
	public ConventionPartenairesEtContactsCtrl ctrl() {
		if (ctrl == null) {
	        ctrl = new ConventionPartenairesEtContactsCtrl(this);
	        Class[] notificationArray = new Class[] { NSNotification.class };
	        NSNotificationCenter.defaultCenter().addObserver(ctrl, new NSSelector("refreshContacts",notificationArray), "refreshContactsNotification", null);
	        NSNotificationCenter.defaultCenter().addObserver(ctrl, new NSSelector("refreshPartenaires",notificationArray), "refreshPartenairesNotification", null);
		}
		return ctrl;
	}

	public void setCtrl(ConventionPartenairesEtContactsCtrl ctrl) {
		this.ctrl = ctrl;
	}

	@Override
	public boolean valider() {
		boolean validate = false;
		Operation operation = operation();
		
		if (operation != null && operation.etablissement() != null && 
			operation.centreResponsabilite() != null) {
			if (operation.operationPartenaires().count()<=1) {
				((Assistant)parent()).setFailureMessage("La convention doit avoir au moins deux partenaires.");
			} else {
				validate = true;
				((Assistant)parent()).setFailureMessage(null);
			}
		} else {
			((Assistant)parent()).setFailureMessage("Vous devez renseigner l'\u00E9tablissement interne principal et le centre gestionnaire.");
		}
		
    	return validate;
    }

	
	public boolean isAjouterCentreGestionnaireAuxPartenairesDisabled() {
		boolean isAjouterCentreGestionnaireAuxPartenairesDisabled = true;
		if (operation() != null && operation().centreResponsabilite() != null) {
			// TODO Tester si centre pas deja present dans la liste des partenaires
			isAjouterCentreGestionnaireAuxPartenairesDisabled = false;
		}
		return isAjouterCentreGestionnaireAuxPartenairesDisabled;
	}

	public WOActionResults ajouterUnPartenaire() {
		GestionPartenaire nextPage = (GestionPartenaire)pageWithName(GestionPartenaire.class.getName());
		nextPage.setOperation(operation());
		nextPage.setPartenaire(null);
		nextPage.setSelectedPersonne(null);
		nextPage.setIndexModuleActif(indexPartenaireModuleActif());
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		return nextPage;
	}
	public WOActionResults editerUnPartenaire() {
		return null;
	}
	public WOActionResults modifierUnPartenaire() {
		GestionPartenaire nextPage = (GestionPartenaire)pageWithName(GestionPartenaire.class.getName());
		nextPage.setOperation(operation());
		OperationPartenaire partenaire = (OperationPartenaire)ctrl().dgPartenaires().selectedObject();
		nextPage.setPartenaire(partenaire);
		nextPage.setIndexModuleActif(indexPartenaireModuleActif());
		IPersonne personne = (IPersonne)EOUtilities.localInstanceOfObject(ERXEC.newEditingContext(), partenaire.partenaire());
		personne.setValidationEditingContext(editingContext());
		nextPage.setSelectedPersonne(personne);
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		
		return nextPage;
	}

	public String onClickAjouterPartenaire() {
		String onClickAjouterPartenaire = null;
		onClickAjouterPartenaire = "openWinAjouterPartenaire(this.href,'Ajouter un partenaire');return false;";
		
		return onClickAjouterPartenaire;
	}
	public String onClickEditerPartenaire() {
		String onClickEditerPartenaire = null;
		onClickEditerPartenaire = "openWinAjouterPartenaire(this.href,'Modifier un partenaire');return false;";
		
		return onClickEditerPartenaire;
	}
	public String onClickModifierUnPartenaire() {
		String onClickModifierPartenaire = null;
		onClickModifierPartenaire = "openWinAjouterPartenaire(this.href,'Modifier un partenaire');return false;";
		
		return onClickModifierPartenaire;
	}

	public WOActionResults closeWinAjouterPartenaire() {
		return null;
	}

	public WOActionResults supprimerUnPartenaire() {
		OperationPartenaire lePartenaireASupprimer = (OperationPartenaire)ctrl().dgPartenaires().selectedObject();

		if (lePartenaireASupprimer != null) {
			ERXEC edc = (ERXEC)lePartenaireASupprimer.editingContext();
			
			FactoryOperationPartenaire fcp = new FactoryOperationPartenaire(edc,isApplicationInDebugMode());
			try {
				fcp.supprimerOperationPartenaire(lePartenaireASupprimer, applicationUser().getPersId());
				ctrl().setDgPartenaires(null);
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				context().response().setStatus(500);
//				session().setMessageErreur(e.getMessage());
				mySession().addSimpleErrorMessage("Erreur", e);
				e.printStackTrace();
			}
		}
		
		return null;
	}
	public String onClickSupprimerPartenaire() {
		String onClickAjouterPartenaire = null;
		onClickAjouterPartenaire = "openWinAjouterPartenaire(this.href,'Ajouter un partenaire');return false;";
		
		return onClickAjouterPartenaire;
	}

	
	public WOComponent initialiserDetailContact() {
		return ctrl().initialiserDetailContact();
	}

	public boolean isSupprimerUnPartenaireEnabled() {
		return enabled() && ctrl != null && ctrl().dgPartenaires() != null && ctrl().dgPartenaires().selectedObject() != null ? true:false;
	}
	
	public String supprimerUnPartenaireDisabledAttribut() {
		return getOtherTagAttribut(isSupprimerUnPartenaireDisabled());
	}
	
	public boolean isSupprimerUnPartenaireDisabled() {
		return !isSupprimerUnPartenaireEnabled();
	}
	public boolean isModifierUnPartenaireDisabled() {
		return disabled() || ctrl == null || ctrl().dgPartenaires() == null || ctrl().dgPartenaires().selectedObject() == null ? true:false;
	}
	
	public String modifierUnPartenaireDisabledAttribut() {
		return getOtherTagAttribut(isModifierUnPartenaireDisabled());
	}


	public String onClickAjouterContact() {
		String onClickAjouterContact = null;
		onClickAjouterContact = "openWinAjouterContact(this.href,'Ajouter un contact');return false;";
		
		return onClickAjouterContact;
	}
	public WOActionResults ajouterUnContact() {
		if (disabled()) {
			return null;
		}
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setOperation(operation());
		nextPage.setPartenaire((OperationPartenaire)ctrl().dgPartenaires().selectedObject());
		setIndexContactModuleActif(null);
		nextPage.setIndexModuleActif(indexContactModuleActif());
		nextPage.setContact(null);
		nextPage.setSelectedPersonne(null);
		
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		// Pour forcer le refetch du display group a la fermeture de la fenetre d'ajout d'un contact
		ctrl().setDgContacts(null);
		return nextPage;
	}
	public WOActionResults editerUnContact() {
		return null;
	}
	public String onClickModifierUnContact() {
		String onClickModifierContact = null;
		onClickModifierContact = "openWinAjouterContact(this.href,'Modifier un contact');return false;";
		return onClickModifierContact;
	}
	public WOActionResults modifierUnContact() {
		if (isModifierUnContactDisabled()) {
			return null;
		}
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setOperation(operation());
		OperationPartenaire operationPartenaire = (OperationPartenaire)ctrl().dgPartenaires().selectedObject();
		nextPage.setPartenaire(operationPartenaire);
		nextPage.setOperation(operationPartenaire.operation());
		setIndexContactModuleActif(null);
		nextPage.setIndexModuleActif(indexContactModuleActif());
		
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		
		OperationPartContact contact = (OperationPartContact)ctrl().dgContacts().selectedObject();
		if (contact != null) {
			nextPage.setContact(contact);
			IPersonne personne = contact.personne().localInstanceIn(nextPage.editingContextForPersonne());
			personne.setValidationEditingContext(nextPage.editingContextForPersonne());
			nextPage.setSelectedPersonne(personne);
		}
		return nextPage;
	}

	public boolean isModifierUnContactDisabled() {
		return disabled() || ctrl == null || ctrl().dgContacts() == null || ctrl().dgContacts().selectedObject() == null? true:false;
	}
	
	public String modifierUnContactDisabledAttribut() {
		return getOtherTagAttribut(isModifierUnContactDisabled());
	}

	public boolean isSupprimerUnContactDisabled() {
		return !isSupprimerUnContactEnabled();
	}
	
	public String supprimerUnContactDisabledAttribut() {
		return getOtherTagAttribut(isSupprimerUnContactDisabled());
	}

	public boolean isSupprimerUnContactEnabled() {
		return enabled() && ctrl != null && ctrl().dgContacts() != null && ctrl().dgContacts().selectedObject() != null? true:false;
	}
	
	public WOActionResults supprimerUnContact() {
		if(isSupprimerUnContactDisabled()) {
			return null;
		}
		OperationPartContact leContactASupprimer = (OperationPartContact)ctrl().dgContacts().selectedObject();
		OperationPartenaire lePartenaire = (OperationPartenaire)ctrl().dgPartenaires().selectedObject();
		if (leContactASupprimer != null) {
			FactoryOperationPartenaire fap = new FactoryOperationPartenaire(leContactASupprimer.editingContext(),isApplicationInDebugMode());
			try {
				fap.supprimerContact(leContactASupprimer, lePartenaire, applicationUser().getPersId());
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				context().response().setStatus(500);
//				session().setMessageErreur(e.getMessage());
				mySession().addSimpleErrorMessage("Erreur", e);
				e.printStackTrace();
			}
		}
		return null;
	}
	public String onClickSupprimerContact() {
		String onClickSupprimerContact = null;
		onClickSupprimerContact = "openWinAjouterContact(this.href,'Supprimer un Contact');return false;";
		
		return onClickSupprimerContact;
	}

	public String onClickEditerContact() {
		String onClickEditerContact = null;
		onClickEditerContact = "openWinAjouterContact(this.href,'Modifier un contact');return false;";
		
		return onClickEditerContact;
	}

	public WOActionResults closeWinAjouterContact() {
		return null;
	}


	public Boolean isModeEditionTVContacts() {
		return Boolean.TRUE;
	}


	public EOQualifier typeAdressePro() {
		return EOTypeAdresse.QUAL_TADR_CODE_PRO;
	}
	public EOQualifier typeTelPro() {
		return EOTypeTel.QUAL_C_TYPE_TEL_PRF;
	}


	/**
	 * @return the unOperationPartenaire
	 */
	public OperationPartenaire unOperationPartenaire() {
		return ctrl().unOperationPartenaire();
	}


	/**
	 * @param unOperationPartenaire the unOperationPartenaire to set
	 */
	public void setUnOperationPartenaire(OperationPartenaire unOperationPartenaire) {
		ctrl().setUnOperationPartenaire(unOperationPartenaire);
	}


	/**
	 * @return the unOperationPartContact
	 */
	public OperationPartContact getUnOperationPartContact() {
		return ctrl().unOperationPartContact();
	}


	/**
	 * @param unOperationPartContact the unOperationPartContact to set
	 */
	public void setUnOperationPartContact(OperationPartContact unOperationPartContact) {
		ctrl().setUnOperationPartContact(unOperationPartContact);
	}


	public WOActionResults ajouterPersonneAffecteeAuxContacts() {
		EORepartStructure unePersonneRepartStructure = ctrl().unePersonneAffecteeSelectionnee();
		if (unePersonneRepartStructure != null) {
			IPersonne unePersonne = unePersonneRepartStructure.toPersonneElt();
			IPersonne personneAAjouter = (IPersonne)EOUtilities.localInstanceOfObject(editingContext(), unePersonne);
			FactoryOperationPartenaire fcp = new FactoryOperationPartenaire(editingContext(),isApplicationInDebugMode());
			try {
				fcp.ajouterContact((OperationPartenaire)ctrl().dgPartenaires().selectedObject(), personneAAjouter, null);
				ctrl().setIsTabContactsSelected(true);
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}


	public boolean isAfficherAjouterPersonneAffectee() {
		boolean isAfficherAjouterPersonneAffectee = false;
		if (ctrl().lePartenaireSelectionne() != null && ctrl().lePartenaireSelectionne().isStructure()) {
			NSArray repartStructuresAffectees = ((EOStructure)ctrl().lePartenaireSelectionne()).toRepartStructuresElts();
			if (repartStructuresAffectees != null && repartStructuresAffectees.count()>0) {
				isAfficherAjouterPersonneAffectee = true;
			}
		}
			
		return isAfficherAjouterPersonneAffectee;
	}

	public WOActionResults onCloseAssistantPartenaire() {
//		session.setIndexModuleActifGestionPartenaire(null);
		setIndexModuleActif(null);
		return null;
	}

	public WOActionResults onCloseAssistantContact() {
//		session.setIndexModuleActifGestionContact(null);
		setIndexModuleActif(null);
		return null;
	}

	public String updateContainerIDsForTVContributions() {
		String updateContainerIDsForTVContributions = null;
		WODisplayGroup dgPartenaires = ctrl().dgPartenaires();
		if (dgPartenaires != null && dgPartenaires.selectedObject() != null) {
			updateContainerIDsForTVContributions = "TableViewPartenaires_TrLigne"+dgPartenaires.allObjects().indexOfIdenticalObject(dgPartenaires.selectedObject());
		}
		return updateContainerIDsForTVContributions;
	}

	public String updateContainerJsAferHide() {
        String func = "function() { ContainerPartenairesEtContactsUpdate(); ";
        String updateId = stringValueForBinding(BDG_UPDATE_CONTAINER_ID, null);
	    if ( updateId != null) {
	        func = func + updateId +"Update(); }";
	    } else {
	        func = func + "}";
	    }
	    return func;
	}
	
	/**
	 * @return the etablissementsAffectation
	 */
	public NSArray etablissementsAffectation() {
		if (hasBinding(BDG_ETABLISSEMENTS_AFFECTATION) && valueForBinding(BDG_ETABLISSEMENTS_AFFECTATION)!=null) {
			etablissementsAffectation = (NSArray)valueForBinding(BDG_ETABLISSEMENTS_AFFECTATION);
		} else {
			etablissementsAffectation = new NSArray();
		}
		return etablissementsAffectation;
	}

	/**
	 * @param etablissementsAffectation the etablissementsAffectation to set
	 */
	public void setEtablissementsAffectation(NSArray etablissementsAffectation) {
		this.etablissementsAffectation = etablissementsAffectation;
		if (canSetValueForBinding(BDG_ETABLISSEMENTS_AFFECTATION)) {
			setValueForBinding(etablissementsAffectation, BDG_ETABLISSEMENTS_AFFECTATION);
		}
	}
	
	
	/**
	 * @return the indexPartenaireModuleActif
	 */
	public Integer indexPartenaireModuleActif() {
		return (Integer) valueForBinding(BDG_PARTENAIRE_INDEX_MODULE_ACTIF);
	}

	/**
	 * @param indexPartenaireModuleActif the indexPartenaireModuleActif to set
	 */
	public void setIndexPartenaireModuleActif(Integer indexPartenaireModuleActif) {
		setValueForBinding(indexPartenaireModuleActif, BDG_PARTENAIRE_INDEX_MODULE_ACTIF);
	}
	
	/**
	 * @return the indexPartenaireModuleActif
	 */
	public Integer indexContactModuleActif() {
		return (Integer) valueForBinding(BDG_CONTACT_INDEX_MODULE_ACTIF);
	}

	/**
	 * @param indexContactModuleActif the indexContactModuleActif to set
	 */
	public void setIndexContactModuleActif(Integer indexContactModuleActif) {
		setValueForBinding(indexContactModuleActif, BDG_CONTACT_INDEX_MODULE_ACTIF);
	}

	/**
	 * @return Le script pour permettre d'ajouter la classe 'info' sur la ligne sélectionnée et de la supprimer des éventuelles autres lignes
	 */
	public String jsTdOnClick() {
		String str = "";
		str += "var $ = jQuery;";
		str += "$(this).parent().parent().children().removeClass('info');";
		str += "$(this).parent().addClass('info');";
		return str;
	}
}
