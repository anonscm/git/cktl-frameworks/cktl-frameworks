/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class AssistantContact extends Assistant {

	public static final String CONTRAT_BDG = "operation";
	public static final String CONTACT_BDG = "contact";
	public static final String PARTENAIRE_BDG = "partenaire";
	public static final String PERSONNE_BDG = "personne";
	public static final String ROLES_BDG = "roles";
    public static final String EC_FOR_CONTACT_BDG = "editingContextForContact"; 

	private Operation operation;
	private OperationPartenaire partenaire;
	private OperationPartContact contact;
	private IPersonne personne;
	private NSArray roles;
	
	public AssistantContact(WOContext context) {
        super(context);
    }

	public String updateContainerID() {
		return "ContainerGestionContact";
	}
	public String updateContainerIDOnSelectionnerPersonneInTableview() {
		return "ContainerGestionContactMenu";
	}

	public void reset() {
		operation = null;
		partenaire = null;
		personne = null;
	}

	public boolean isTerminerDisabled() {
		boolean isTerminerDisabled = (contact() == null && personne() == null);
		
		return isTerminerDisabled;
	}

	/**
	 * @return the operation
	 */
	public Operation operation() {
		if (hasBinding(CONTRAT_BDG)) {
			operation = (Operation)valueForBinding(CONTRAT_BDG);
		}
		return operation;
	}
	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
		if (hasBinding(CONTRAT_BDG)) {
			setValueForBinding(operation, CONTRAT_BDG);
		}
	}

	/**
	 * @return the partenaire
	 */
	public OperationPartenaire partenaire() {
		if (hasBinding(PARTENAIRE_BDG)) {
			partenaire = (OperationPartenaire)valueForBinding(PARTENAIRE_BDG);
		}
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(OperationPartenaire partenaire) {
		this.partenaire = partenaire;
		if (hasBinding(PARTENAIRE_BDG)) {
			setValueForBinding(partenaire, PARTENAIRE_BDG);
		}
	}

	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		if (hasBinding(PERSONNE_BDG)) {
			personne = (IPersonne)valueForBinding(PERSONNE_BDG);
		}
		return personne;
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		this.personne = personne;
		if (hasBinding(PERSONNE_BDG)) {
			setValueForBinding(personne, PERSONNE_BDG);
		}
	}

	/**
	 * @return the contact
	 */
	public OperationPartContact contact() {
		return (OperationPartContact)valueForBinding(CONTACT_BDG);
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(OperationPartContact contact) {
		this.contact = contact;
	}

	/**
	 * @return the roles
	 */
	public NSArray roles() {
		return (NSArray)valueForBinding(ROLES_BDG);
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(NSArray roles) {
		this.roles = roles;
		if (hasBinding(ROLES_BDG)) {
			setValueForBinding(roles, ROLES_BDG);
		}
	}
	
	public EOEditingContext editingContextForContact() {
	    return (EOEditingContext)valueForBinding(EC_FOR_CONTACT_BDG);
	}
	
}
