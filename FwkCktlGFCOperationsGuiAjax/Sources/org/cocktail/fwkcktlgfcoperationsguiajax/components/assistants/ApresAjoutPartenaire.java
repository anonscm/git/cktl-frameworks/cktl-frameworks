/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperationsguiajax.components.ACktlAjaxGFCOperationsComponent;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class ApresAjoutPartenaire extends ACktlAjaxGFCOperationsComponent {
	
	private Operation operation;
	private IPersonne personne;
	
    public ApresAjoutPartenaire(WOContext context) {
        super(context);
    }

	/**
	 * @return the operation
	 */
	public Operation operation() {
		return operation;
	}

	/**
	 * @param operation the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}

	public WOActionResults consulter() {
//		Convention nextPage = (Convention)pageWithName(Convention.class.getName());
//		nextPage.setConvention(operation());
//		return nextPage;
		return null; //TODO : faire un binding
	}

	public WOActionResults accueil() {
//		Accueil nextPage = (Accueil)pageWithName(Accueil.class.getName());
//		session.reset();
//		return nextPage;
		return null; //TODO : faire un binding
	}

	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		return personne;
	}

	/**
	 * @param personnne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		this.personne = personne;
	}
}
