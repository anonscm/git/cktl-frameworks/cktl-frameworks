/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import java.math.BigDecimal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory.FactoryConvention;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

/**
 * Page de gestion des partenaires (contient l'assistant de création des partenaires). Typiquement ouvert dans une modalWindow.
 * ATTENTION : Veillez à bien appeler les setters suivant :
 * - operation
 * - partenaire
 * - selectedPersonne
 * - applicationUser
 * - isApplicationInDebugMode
 * - indexModulActif
 * - avecModuleDeRecherche
 * - isDirectAction
 *  selon votre besoin...
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class GestionPartenaire extends ACktlAjaxGFCOperationsComponent {
	private static final long serialVersionUID = -8917779507300648842L;
	
	private EOEditingContext editingContext=null;
	private NSArray modules;
	private NSArray<String> etapes;
    private OperationPartenaire partenaire;
	private IPersonne selectedPersonne;
	private boolean isSuccess;
	
	Boolean isDirectAction = null;
	private FwkCktlGFCOperationsApplicationUser applicationUser;
	private boolean isApplicationInDebugMode;
	private Integer indexModuleActif;
	private Operation operation;

	public GestionPartenaire(WOContext context) {
        super(context);
    }

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlGFCOperationsGuiAjax.framework", "css/fwkcktlgfcoperationsguiajax.css");
	}

	
	public WOActionResults annuler() {
	    isSuccess = false;
		CktlAjaxWindow.close(context());
		return null;
	}
	
	
	public WOActionResults terminer() throws ExceptionFinder {
		IPersonne personne = selectedPersonne();
		if (personne != null) {
			try {
				if (personne.editingContext().hasChanges()){
					personne.setPersIdModification(applicationUser().getPersonne().persId());
					personne.editingContext().saveChanges();
				}
				EOEditingContext ed = operation().editingContext();
				if (partenaire() == null) {
					FactoryConvention fc = new FactoryConvention(ed,isApplicationInDebugMode());
					IPersonne personneAAjouter = (IPersonne)EOUtilities.localInstanceOfObject(ed, selectedPersonne());
					fc.ajouterPartenaireContractant(operation(), personneAAjouter, Boolean.FALSE, BigDecimal.valueOf(0));
					if (isDirectAction() != null && isDirectAction().booleanValue()==true) {
					    operation().setShouldNotValidate(true);
						ed.saveChanges();
					}
				}
				isSuccess = true;
				CktlAjaxWindow.close(context());
			} catch (ExceptionFinder e1) {
				personne.editingContext().revert();
				context().response().setStatus(500);
				mySession().addSimpleErrorMessage("Erreur", e1);
				throw e1;
			}  catch (ValidationException e2) {
				context().response().setStatus(500);
				mySession().addSimpleErrorMessage("Erreur", e2);

			}  catch (ExceptionUtilisateur e3) {
			    context().response().setStatus(500);
				mySession().addSimpleErrorMessage("Erreur", e3);
			} catch (Exception e) {
			    personne.editingContext().revert();
				e.printStackTrace();
				throw NSForwardException._runtimeExceptionForThrowable(e);
			}
			
		}
		return null;
	}

	public String moduleName() {
		String moduleName = null;
		if (modules() != null && modules().count()>0) {
			moduleName = (String)modules().objectAtIndex(indexModuleActif().intValue());
		}
		return moduleName;
	}


	/**
	 * @return the modules
	 */
	public NSArray modules() {
			if (partenaire() != null) {
				if (partenaire().partenaire().isStructure()) {
					modules = new NSArray<String>(new String[]{
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneMembres",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupeAdmin"});
				} else {
					modules = new NSArray<String>(new String[]{
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupe",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse"});
				}
			} else if (selectedPersonne() != null) {
				if (selectedPersonne().isStructure()) {
					modules = new NSArray<String>(new String[]{
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneSearch",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneMembres",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupeAdmin"});
				} else {
					modules = new NSArray<String>(new String[]{
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneSearch",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdmin",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneGroupe",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneTelephone",
							"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneAdresse"});
				}
			} else {
				modules = new NSArray<String>(new String[]{
						"org.cocktail.fwkcktlgfcoperationsguiajax.components.assistants.modules.ModulePersonneSearch"});
			}
//			session.setModulesGestionPartenaire(modules); //TODO : Verifier dans les autres composants que la référence soit passée par binding et non-plus par session
		return modules;
	}
	/**
	 * @param modules the modules to set
	 */
	public void setModules(NSArray modules) {
		this.modules = modules;
//		((Session)session()).setModulesGestionPartenaire(modules); //TODO : Verifier dans les autres composants que la référence soit passée par binding et non-plus par session
	}

	public NSArray<String> etapes() {
			if (partenaire() != null) {
				if (partenaire().partenaire().isStructure()) {
					etapes = new NSArray<String>(new String[]{
							"Informations",
							"T&eacute;l&eacute;phones",
							"Adresses",
							"Membres",
							"Groupe"});
				} else {
					etapes = new NSArray<String>(new String[]{
							"Informations",
							"Groupes",
							"T&eacute;l&eacute;phones",
							"Adresses"});
				}
			} else if (selectedPersonne() != null) {
				if (selectedPersonne().isStructure()) {
					etapes = new NSArray<String>(new String[]{
							"Recherche",
							"Informations",
							"T&eacute;l&eacute;phones",
							"Adresses",
							"Membres",
							"Groupe"});
				} else {
					etapes = new NSArray<String>(new String[]{
							"Recherche",
							"Informations",
							"Groupes",
							"T&eacute;l&eacute;phones",
							"Adresses"});
				}
			} else {
				etapes = new NSArray<String>(new String[]{
						"Recherche"});
			}
		return etapes;
	}

	/**
	 * @return the selectedPersonne
	 */
	public IPersonne selectedPersonne() {
		return selectedPersonne;
	}

	/**
	 * @param selectedPersonne the selectedPersonne to set
	 */
	public void setSelectedPersonne(IPersonne selectedPersonne) {
		this.selectedPersonne = selectedPersonne;
	}


	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContext() {
		if (editingContext == null) {
			if (selectedPersonne() != null) {
				editingContext = selectedPersonne().editingContext();
			} else {
				editingContext = ERXEC.newEditingContext();
			}
		}
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @return the partenaire
	 */
	public OperationPartenaire partenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(OperationPartenaire partenaire) {
		this.partenaire = partenaire;
	}

	/**
	 * @return the isDirectAction
	 */
	public Boolean isDirectAction() {
		return isDirectAction;
	}

	/**
	 * @param isDirectAction the isDirectAction to set
	 */
	public void setIsDirectAction(Boolean isDirectAction) {
		this.isDirectAction = isDirectAction;
	}

	public boolean isSuccess() {
        return isSuccess;
    }
	
	/**
	 * @return the operation
	 */
	public Operation operation() {
		return operation;
	}

	/**
	 * @param operation
	 *            the operation to set
	 */
	public void setOperation(Operation operation) {
		this.operation = operation;
	}
	
	/**
	 * @return the applicationUser
	 */
	public FwkCktlGFCOperationsApplicationUser applicationUser() {
		return applicationUser;
	}

	/**
	 * @param applicationUser the applicationUser to set
	 */
	public void setApplicationUser(FwkCktlGFCOperationsApplicationUser applicationUser) {
		this.applicationUser = applicationUser;
	}
	
	/**
	 * @return the isApplicationInDebugMode
	 */
	public boolean isApplicationInDebugMode() {
		return isApplicationInDebugMode;
	}

	/**
	 * @param isApplicationInDebugMode the isApplicationInDebugMode to set
	 */
	public void setApplicationInDebugMode(boolean isApplicationInDebugMode) {
		this.isApplicationInDebugMode = isApplicationInDebugMode;
	}

	public Integer indexModuleActif() {
		return indexModuleActif;
	}
	/**
	 * @param indexModuleActif the indexModuleActif to set
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		this.indexModuleActif = indexModuleActif;
	}

	public String messageContainerID() {
		return componentId()+"_GestPartenaire_FwkCktlGFCOperationsGuiAjax_MessageContainer";
	}
	
}
