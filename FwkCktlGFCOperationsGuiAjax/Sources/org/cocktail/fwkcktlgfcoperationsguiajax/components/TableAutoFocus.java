package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXResponseRewriter;

public class TableAutoFocus {

	private String tableId;
	
	public TableAutoFocus(String tableId) {
		setTableId(tableId);
	}
	
	public void appendRequiredScripts(final WOResponse response, final WOContext context) {
		ERXResponseRewriter.addScriptResourceInHead(response, context, "Ajax", "prototype.js");
		ERXResponseRewriter.addScriptResourceInHead(response, context, "FwkCktlAjaxWebExt.framework", "scripts/cktlajaxsimpletableview.js");
		response.appendContentString("<script>CASTBV.reapplyFocus();" + jsRegisterHandlerOnInputs() + "</script>");
	}
	
	public String jsRegisterHandlerOnInputs() {
		return "CASTBV.registerHandlerOnInputs($('" + getTableId() + "'));";
	}

	public String getTableId() {
		return tableId;
	}

	private void setTableId(String tableId) {
		this.tableId = tableId;
	}
	
	
}
