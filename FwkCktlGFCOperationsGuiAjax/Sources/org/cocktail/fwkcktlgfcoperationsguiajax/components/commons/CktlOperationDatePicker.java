package org.cocktail.fwkcktlgfcoperationsguiajax.components.commons;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxDatePicker;

import com.webobjects.appserver.WOContext;

/**
 * Surcharge du composant {@link CktlAjaxDatePicker}
 * 
 * @see CktlAjaxDatePicker
 *
 */
public class CktlOperationDatePicker extends CktlAjaxDatePicker {
	private static final String BINDING_NAME = "name";

    /**
	 * @param context {@link WOContext}
	 */
	public CktlOperationDatePicker(WOContext context) {
		super(context);
	}
	
	public String displayNameForEntity() {
	    return (String) valueForBinding(BINDING_NAME);
	}

	/**
	 * @return Le script de création du calendrier
	 */
	public String jsCalendarSetup() {
		String str = "";
		str += "Calendar.setup(";
		str += "{\n inputField  : " + addQuote(fieldID());
		str += ",\n dateFormat  : " + addQuote(dateFormatJS());
		str += ",\n showTime    : " + showsTime();
		str += ",\n trigger     : " + addQuote(imageID());
		str += ",\n singleClick : true";
		str += ",\n step        : 1";
		str += ",\n align       : \"TL/ / /T/r\"";
		str += ",\n onSelect    : onSelectCktl";
		str += ",\n weekNumbers : true";
		str += ",\n updateContainerID	: " + addQuote(stringValueForBinding("updateContainerID", ""));
		str += ",\n min         : " + getMinDate();
		str += ",\n max         : " + getMaxDate();
		str += ",\n bottomBar   : " + bottomBar();
		str += "\n});";
		return str;
	}

	private String addQuote(String str) {
		return "\"" + str + "\"";
	}

    public String getContainerDivClass() {
        if (disabled()) {
            return "";
        }
        return "input-group";
    }
}