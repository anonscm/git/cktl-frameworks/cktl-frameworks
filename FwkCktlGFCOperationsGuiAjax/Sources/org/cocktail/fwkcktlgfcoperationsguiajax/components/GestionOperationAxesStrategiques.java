package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.foundation.ERXArrayUtilities;

public class GestionOperationAxesStrategiques extends ACktlAjaxGFCOperationsComponent {

	protected static final String BINDING_utilisateurPersId = "utilisateurPersId";
	protected static final String BINDING_editingContext = "editingContext";
	protected static final String BINDING_contrat = "contrat";
	protected static final String BINDING_edition = "edition";
	
	private ERXDisplayGroup<AxeStrategique> axesDisplayGroup;
	
	private AxeStrategique currentAxeStrategique;
	
	private NSArray<AxeStrategique> listeAxesStrategiques;
	
	private AxeStrategique currentAxeStrategiqueSelectionnable;
	
	public String axesTableViewId = getComponentId() + "_axesTableView";
	public String axesTableViewContainerId = getComponentId() + "_axesTableViewContainer";
	public String editerAxeWindowId = getComponentId() + "_editerAxeWindow";
	
	public EOEditingContext edc;
	public GestionOperationAxesStrategiques(WOContext context) {
        super(context);
    }
	
	@Override
	public EOEditingContext edc() {
		if(edc == null) {
			edc = (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return edc;
	}

	public Integer utilisateurPersId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}
	
	public Operation contrat() {
		return (Operation) valueForBinding(BINDING_contrat);
	}
	
	
	public Boolean edition() {
		return booleanValueForBinding(BINDING_edition, true);
	}

	

	public ERXDisplayGroup<AxeStrategique> getAxesDisplayGroup() {
		if(axesDisplayGroup == null) {
			axesDisplayGroup = new ERXDisplayGroup<AxeStrategique>();
			axesDisplayGroup.setObjectArray(contrat().axeStrategiques());
			axesDisplayGroup.setSortOrderings(AxeStrategique.LIBELLE.ascInsensitives());
			axesDisplayGroup.fetch();
		}
		return axesDisplayGroup;
	}

	public void setAxesDisplayGroup(ERXDisplayGroup<AxeStrategique> axesDisplayGroup) {
		this.axesDisplayGroup = axesDisplayGroup;
	}

	public AxeStrategique getCurrentAxeStrategique() {
		return currentAxeStrategique;
	}

	public void setCurrentAxeStrategique(AxeStrategique currentAxeStrategique) {
		this.currentAxeStrategique = currentAxeStrategique;
	}

	
	public WOActionResults ajouter() {
		return doNothing();
	}
	
	
	public WOActionResults supprimer() {
		contrat().removeFromAxeStrategiquesRelationship(getAxesDisplayGroup().selectedObject());
		getAxesDisplayGroup().setObjectArray(contrat().axeStrategiques());
		getAxesDisplayGroup().updateDisplayedObjects();
		return doNothing();
	}
	
	public WOActionResults selectionner() {
		contrat().addToAxeStrategiquesRelationship(getCurrentAxeStrategiqueSelectionnable());
		getAxesDisplayGroup().setObjectArray(contrat().axeStrategiques());
		getAxesDisplayGroup().updateDisplayedObjects();
		CktlAjaxWindow.close(context(), editerAxeWindowId);
		return doNothing();
	}
	
	public WOActionResults annuler() {
		CktlAjaxWindow.close(context(), editerAxeWindowId);
		return doNothing();
	}

	public NSArray<AxeStrategique> getListeAxesStrategiques() {
		if(listeAxesStrategiques == null) {
			listeAxesStrategiques = AxeStrategique.fetchAll(edc());
		}
		return listeAxesStrategiques;
	}

	public void setListeAxesStrategiques(NSArray<AxeStrategique> listeAxesStrategiques) {
		this.listeAxesStrategiques = listeAxesStrategiques;
	}
	
	public NSArray<AxeStrategique> getListeAxesStrategiquesSelectionnables() {
		return ERXArrayUtilities.arrayMinusArray(getListeAxesStrategiques(), getAxesDisplayGroup().allObjects());
	}

	public AxeStrategique getCurrentAxeStrategiqueSelectionnable() {
		return currentAxeStrategiqueSelectionnable;
	}

	public void setCurrentAxeStrategiqueSelectionnable(
			AxeStrategique currentAxeStrategiqueSelectionnable) {
		this.currentAxeStrategiqueSelectionnable = currentAxeStrategiqueSelectionnable;
	}
	
	
	
}