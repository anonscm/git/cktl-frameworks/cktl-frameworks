package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumn;
import org.cocktail.fwkcktlajaxwebext.serveur.component.tableview.column.CktlAjaxTableViewColumnAssociation;
import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlged.serveur.metier.EOTypeDocument;
import org.cocktail.fwkcktlgedguiajax.serveur.components.CktlAjaxTVGedDocumentCell;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.AvenantDocument;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

public class AvenantDocumentsTableView extends ACktlAjaxGFCOperationsComponent {

	private String BINDING_dg = "dg";
	private String BINDING_showAvenantColumn = "showAvenantColumn";
	private String BINDING_showTypeDocumentColumn = "showTypeDocumentColumn";

	private AvenantDocument unAvenantDocument;

	public static final String UN_AVENANT_DOCUMENT_KEY = "unAvenantDocument";

	public static final String AVENANT_KEY = "AVENANT";
	public static final String DOCUMENT_KEY = "DOCUMENT";
	public static final String DATE_KEY = "DATE";
	public static final String VIEW_KEY = "VIEW";
	public static final String TYPE_DOCUMENT_KEY = "TYPE_DOCUMENT";

	public NSMutableArray<String> _colonnesKeys = null;
	public NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap = null;

	public AvenantDocumentsTableView(WOContext context) {
		super(context);
	}

	public String documentsTableViewId() {
		return getComponentId() + "_documentsTableView";
	}

	public Boolean showAvenantColumn() {
		return booleanValueForBinding(BINDING_showAvenantColumn, false);
	}

	public Boolean showTypeDocumentColumn() {
		return booleanValueForBinding(BINDING_showTypeDocumentColumn, false);
	}

	private NSMutableArray<String> _colonnesKeys() {
		if (_colonnesKeys == null) {
			_colonnesKeys = new NSMutableArray<String>();
			if (showAvenantColumn()) {
				_colonnesKeys().add(AVENANT_KEY);
			}
			_colonnesKeys().add(DATE_KEY);
			if (showTypeDocumentColumn()) {
				_colonnesKeys().add(TYPE_DOCUMENT_KEY);
			}
			_colonnesKeys().add(DOCUMENT_KEY);
			_colonnesKeys().add(VIEW_KEY);
		}
		return _colonnesKeys;
	}

	private NSMutableDictionary<String, CktlAjaxTableViewColumn> _colonnesMap() {
		if (_colonnesMap == null) {

			_colonnesMap = new NSMutableDictionary<String, CktlAjaxTableViewColumn>();

			CktlAjaxTableViewColumn avenant_colonne = new CktlAjaxTableViewColumn();
			avenant_colonne.setLibelle("Avenant");
			avenant_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			CktlAjaxTableViewColumnAssociation avenant_colonne_ass = new CktlAjaxTableViewColumnAssociation(UN_AVENANT_DOCUMENT_KEY + "." + AvenantDocument.AVENANT_KEY + "."
					+ Avenant.AVT_OBJET_COURT_KEY, "");
			avenant_colonne.setAssociations(avenant_colonne_ass);
			_colonnesMap.takeValueForKey(avenant_colonne, AVENANT_KEY);

			CktlAjaxTableViewColumn date_colonne = new CktlAjaxTableViewColumn();
			date_colonne.setLibelle("Date");
			date_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			CktlAjaxTableViewColumnAssociation date_colonne_ass = new CktlAjaxTableViewColumnAssociation(UN_AVENANT_DOCUMENT_KEY + "." + AvenantDocument.DOCUMENT_KEY + "."
					+ EODocument.D_CREATION_KEY, "-");
			date_colonne_ass.setDateformat("%d/%m/%y");
			date_colonne.setAssociations(date_colonne_ass);
			_colonnesMap.takeValueForKey(date_colonne, DATE_KEY);

			CktlAjaxTableViewColumn libelle_colonne = new CktlAjaxTableViewColumn();
			libelle_colonne.setLibelle("Libellé");
			CktlAjaxTableViewColumnAssociation libelle_colonne_ass = new CktlAjaxTableViewColumnAssociation(UN_AVENANT_DOCUMENT_KEY + "." + AvenantDocument.DOCUMENT_KEY + "."
					+ EODocument.OBJET_KEY, "");
			libelle_colonne.setAssociations(libelle_colonne_ass);
			_colonnesMap.takeValueForKey(libelle_colonne, DOCUMENT_KEY);

			CktlAjaxTableViewColumn type_colonne = new CktlAjaxTableViewColumn();
			type_colonne.setLibelle("Type");
			type_colonne.setRowCssClass("alignToCenter useMinWidth");
			CktlAjaxTableViewColumnAssociation type_colonne_ass = new CktlAjaxTableViewColumnAssociation(UN_AVENANT_DOCUMENT_KEY + "." + AvenantDocument.DOCUMENT_KEY + "."
					+ EODocument.TO_TYPE_DOCUMENT_KEY + "." + EOTypeDocument.TD_LIBELLE_KEY, "");
			type_colonne.setAssociations(type_colonne_ass);
			_colonnesMap.takeValueForKey(type_colonne, TYPE_DOCUMENT_KEY);

			CktlAjaxTableViewColumn document_colonne = new CktlAjaxTableViewColumn();
			document_colonne.setLibelle("Document");
			document_colonne.setRowCssClass("alignToCenter useMinWidth nowrap");
			document_colonne.setComponent(CktlAjaxTVGedDocumentCell.class.getName());
			CktlAjaxTableViewColumnAssociation document_colonne_ass = new CktlAjaxTableViewColumnAssociation();
			document_colonne_ass.takeValueForKey(UN_AVENANT_DOCUMENT_KEY + "." + AvenantDocument.DOCUMENT_KEY, "gedDocument");
			document_colonne_ass.takeValueForKey("gfc.acte", "famille");
			document_colonne.setAssociations(document_colonne_ass);
			_colonnesMap.takeValueForKey(document_colonne, VIEW_KEY);

		}
		return _colonnesMap;

	}

	public NSArray<CktlAjaxTableViewColumn> colonnes() {
		NSMutableArray<CktlAjaxTableViewColumn> colonnes = new NSMutableArray<CktlAjaxTableViewColumn>();
		for (String key : _colonnesKeys()) {
			colonnes.add((CktlAjaxTableViewColumn) _colonnesMap().valueForKey(key));
		}
		return colonnes;
	}

	public WODisplayGroup dgDocuments() {
		return (WODisplayGroup) valueForBinding(BINDING_dg);
	}

	public AvenantDocument unAvenantDocument() {
		return unAvenantDocument;
	}

	public void setUnAvenantDocument(AvenantDocument unAvenantDocument) {
		this.unAvenantDocument = unAvenantDocument;
	}

}