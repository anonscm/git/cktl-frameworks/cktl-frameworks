package org.cocktail.fwkcktlgfcoperationsguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;
import er.extensions.eof.ERXEC;

public class GestionAxesStrategiques extends ACktlAjaxGFCOperationsComponent {

	private static final String BINDING_utilisateurPersId = "utilisateurPersId";
	
	private ERXDatabaseDataSource axesDataSource;
	private ERXDisplayGroup<AxeStrategique> axesDisplayGroup;
	
	private AxeStrategique currentAxeStrategique;
	private AxeStrategique editedAxeStrategique;
	
	public String axesTableViewId = getComponentId() + "_axesTableView";
	public String axesTableViewContainerId = getComponentId() + "_axesTableViewContainer";
	public String editerAxeWindowId = getComponentId() + "_editerAxeWindow";
	
	public EOEditingContext edc;
	public GestionAxesStrategiques(WOContext context) {
        super(context);
    }
	
	@Override
	public EOEditingContext edc() {
		if(edc == null) {
			edc = ERXEC.newEditingContext();
		}	
		return edc;
	}

	public Integer utilisateurPersId() {
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}
	
	public ERXDatabaseDataSource getAxesDataSource() {
		if(axesDataSource == null) {
			axesDataSource = new ERXDatabaseDataSource(edc(), AxeStrategique.ENTITY_NAME);
		}
		return axesDataSource;
	}

	public void setAxesDataSource(ERXDatabaseDataSource axesDataSource) {
		this.axesDataSource = axesDataSource;
	}

	public ERXDisplayGroup<AxeStrategique> getAxesDisplayGroup() {
		if(axesDisplayGroup == null) {
			axesDisplayGroup = new ERXDisplayGroup<AxeStrategique>();
			axesDisplayGroup.setDataSource(getAxesDataSource());
			axesDisplayGroup.fetch();
		}
		return axesDisplayGroup;
	}

	public void setAxesDisplayGroup(ERXDisplayGroup<AxeStrategique> axesDisplayGroup) {
		this.axesDisplayGroup = axesDisplayGroup;
	}

	public AxeStrategique getCurrentAxeStrategique() {
		return currentAxeStrategique;
	}

	public void setCurrentAxeStrategique(AxeStrategique currentAxeStrategique) {
		this.currentAxeStrategique = currentAxeStrategique;
	}

	public AxeStrategique getEditedAxeStrategique() {
		return editedAxeStrategique;
	}

	public void setEditedAxeStrategique(AxeStrategique editedAxeStrategique) {
		this.editedAxeStrategique = editedAxeStrategique;
	}
	
	public WOActionResults ajouter() {
		AxeStrategique nouveau  = 
				AxeStrategique.create(
						edc(), 
						new NSTimestamp(),
						new NSTimestamp(), 
						null,
						utilisateurPersId(),
						utilisateurPersId()
					);
		setEditedAxeStrategique(nouveau);
		return doNothing();
	}
	
	public WOActionResults editer() {
		setEditedAxeStrategique(getAxesDisplayGroup().selectedObject());
		return doNothing();
	}
	
	public WOActionResults supprimer() {
		getAxesDisplayGroup().selectedObject().delete();
		try {
			edc().saveChanges();
			getAxesDisplayGroup().fetch();
		} catch(ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults enregistrer() {
		try {
			edc().saveChanges();
			getAxesDisplayGroup().fetch();
			CktlAjaxWindow.close(context(), editerAxeWindowId);
		} catch(ValidationException e) {
			session().addSimpleErrorMessage("Erreur", e);
		}
		return doNothing();
	}
	
	public WOActionResults annuler() {
		edc().revert();
		CktlAjaxWindow.close(context(), editerAxeWindowId);
		return doNothing();
	}
}