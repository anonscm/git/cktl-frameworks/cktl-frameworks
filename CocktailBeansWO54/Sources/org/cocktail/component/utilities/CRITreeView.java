
//CRITreeView.java
/*
 * Copyright Consortium Cocktail
 *
 * mangue@univ-lr.fr
 *
 * Logiciel de gestion des ressources humaines des établissements.
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.component.utilities;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Enumeration;

import javax.swing.BorderFactory;
import javax.swing.JScrollPane;
import javax.swing.JTree;
import javax.swing.border.TitledBorder;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;

import org.cocktail.component.COConstants;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSNotificationCenter;


/** Sous classe de EOView destinee a contenir un treeview. Implemente la construction
de l'arbre a partir des cle et cle pere comme dans le NSBrowser.
Pour le moment : charge l'arbre de facon statique au lancement de initialize().
Cree un arbre a partir des elements (key et parentKey) ou parentRelationship, affiche les valeurs contenues dans
le champ fieldForDisplay. 
Lorsque l'utilisateur clique ou doubleClique sur un noeud, une notification est envoyee
(ULRTreeViewDidClick ou ULRTreeViewDidDoubleClick). Le dictionnaire userInfo de la notification
contient l'enregistrement selectionne et un booleen correspondant a isLeaf : 
- cles : "selectedRecord" (l'enregistrement correspondant au noeud selectionne) & "isLeaf".
- setRootVisible(boolean) permet de definir si le noeud racine doit etre affiche ou non
- setRestrictionQualifier(EOQualifier) permet de definir le qualifier qui sera applique
a tous les enregistrements (par ex : username = 'nom').
- setQualifierForFirstColumn(EOQualifier) permet de definir le qualifier utilise pour les noeuds
du permier niveau
- selectedObject() permet de recuperer l'objet selectionne dans le displayGroup associe
Remarque importante : dans l'implementation presente, le "fecth on load" ne doit pas etre active si la table ne contient pas tous les enregistrements
(si le restriction qualifier ne contient pas tous les enregistrements de la table).
 */
// 19/10/2010 - à chaque update les tailles sont perdues, on la garde donc pour la remettre à chaque fois
public class CRITreeView extends EOView  implements TreeExpansionListener {
	public static final String TREE_VIEW_DID_CLICK = "CRITreeViewDidClick";
	public static final String TREE_VIEW_DID_DOUBLE_CLICK = "CRITreeViewDidDoubleClick";

	private EOEditingContext editingContext;
	private String entityName;
	private NSArray displayedObjects;
	private EOGenericRecord selectedObject;
	private boolean isExpending;
	private boolean mustFetch;
	// les elements graphiques
	private CRITreeMouseListener mouseListener;
	protected JScrollPane theScrollPane;
	// protected JTree theTree;
	protected JTree theTree;
	// les attributs
	protected CRITreeNode rootNode;
	protected String key, parentKey, fieldForDisplay, title;
	protected String parentRelationship;
	protected boolean isDynamic, isRootVisible, isRootSelected;
	protected EOQualifier restrictionQualifier, qualifierForFirstColumn;
	private boolean manageChildren;
	private Dimension viewDimension; 	// 19/10/2010
	/** Constructor
	 */
	public CRITreeView() {
		this(null);
	}

	/** Constructor providing the root node<BR>
	 * Constructeur avec le noeud racine
	 */
	public CRITreeView(Object rootObject) {
		isDynamic = false;
		isRootVisible = false;
		if (rootObject == null) {
			rootNode =  new CRITreeNode(rootObject);
		} else {
			rootNode = null;
		}
		mustFetch = true;
		mouseListener = new CRITreeMouseListener();
	}
	/** TreeView Initialization : to be called when the attributes have been initialized with NetBeans<BR>
	 * All attributes must be initialized:<BR>
	 * entityName, key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionally, restrictionQualifier.<BR>
	 * If the tree does not fetch the objects to be displayed (mustFetch = false), they must have been provided by the caller with the method setDisplayedObjects()<BR>
	 * If parentRelationship and key are not provided, the user of the tree manage the children and the root and its children must implement the DynamicTreeInterface<BR>
	 * @param selectRootNode true if the root node must be selected when displaying the tree
	*/
	public void initialize(EOEditingContext editingContext,boolean selectRootNode) throws Exception {
		viewDimension = this.getSize();	// 19/10/2010
		this.editingContext = editingContext;
		if (entityName() == null) {
			throw new Exception(this.getClass().getName() + ": no entity name provided");
		}
		if (!manageChildren) {
			// Controle des attributs qui doivent imperativement etre definis
			if ((((key == null) || (parentKey == null)) && (parentRelationship == null)) || (fieldForDisplay == null)) {
				throw new Exception(this.getClass().getName() + ": one or more attributes mandatory ((key or parenRelationship), parentKey, fieldForDisplay) are not defined");
			}
			if (key != null && parentKey != null) {
				setKeyParentKey(key, parentKey);
			}
		}
		setDynamic(!mustFetch);	// Pour que les données soient chargées au fur et à mesure

		// Si le noeud racine n'a pas encore ete cree...
		rootNode = new CRITreeNode("");

		// Initialisation de l'arbre
		this.update((javax.swing.tree.TreePath)null);
		this.createTree(selectRootNode);
	
	}
	/** TreeView Initialization<BR>
	 * All attributes must be initialized:<BR>
	 * key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionally, restrictionQualifier. 
	 * If parentRelationship and key are not provided, the root and its children must implement the DynamicTreeInterface<BR>
	 * @param entityName name of the fetch entity (null if objects are provided by the caller)
	 * @param selectRootNode true if the root node must be selected when displaying the tree
	 * @param mustFetch	true if objects should be fetched, false if they are provided by the caller with the method setObjects<BR>
	 * @param manageChildren true if the children are managed by the enterprise objects (in such a case the tree must be dynamic)<BR>
	 * Throws an exception is the tree is not correctly initialized
	 */
	public void initialize(EOEditingContext editingContext,String entityName,boolean selectRootNode,boolean mustFetch,boolean manageChildren) throws Exception {
		this.entityName = entityName;
		this.mustFetch = mustFetch;
		this.manageChildren = manageChildren;
		initialize(editingContext, selectRootNode);
	}
	/** TreeView Initialization for a non-dynamic tree with fetch<BR>
	 * All attributes must be initialized: key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionally, restrictionQualifier. <BR>
	 * @param entityName name of the fetch entity (null if objects are provided by the caller)
	 * @param selectRootNode true if the root node must be selected when displaying the tree
	 */
	public void initialize(EOEditingContext editingContext,String entityName,boolean selectRootNode) throws Exception {
		initialize(editingContext,entityName,selectRootNode,true,false);
	}
	/** TreeView Initialization : &agrave; utiliser quand les attributs ont &eacute;t&eacute; fournis via une interface NetBeans<BR>
	 * Tous les attributs doivent avoir ete initialises :<BR>
	 * key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionellement, restrictionQualifier.
	 * Si parentRelationship et key ne sont pas fournis alors il faut que l'objet racine et ses fils impl&eacute;mentent l'interface InterfaceArbreAvecFils
	 * Si l'arbre ne fait pas le fetch des objets &agrave; afficher (mustFetch = false), ceux-ci doivent &ecirc;tre fournis par l'appelant avec la m&eacute;thode setDisplayedObjects()<BR>
	 * Si parentRelationship et la cl&eacute; ne sont pas fournies, alors l'utilisateur de l'arbre doit g&eacute;rer les enfants et la racine et les fils doivent impl&eacute;menter l'interface  DynamicTreeInterface<BR>
	 * @param selectionnerRacine true si la racine est selectionn&eacute;e lors de l'affichage de l'arbre
	*/
	public void initialiser(EOEditingContext editingContext,boolean selectionnerRacine) throws Exception {
		initialize(editingContext, selectionnerRacine);
	}
	/** Initialisation du treeView :<BR>
	 * Tous les attributs doivent avoir ete initialises :<BR>
	 * key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionellement, restrictionQualifier.
	 * Si parentRelationship et key ne sont pas fournis alors il faut que l'objet racine et ses fils impl&eacute;mentent l'interface InterfaceArbreAvecFils
	 * @param nomEntite nom de l'entit&eacute; &agrave; fetcher (null si les objets sont fournis par l'appelant)
	 * @param selectionnerRacine true si la racine est selectionn&eacute;e lors de l'affichage de l'arbre
	 * @param doitFetcher true si les objets doivent &ecirc;tre fetch&eacute;s, false si ils sont fournis par l'appelant avec la m&eacute;thode setObjetsGeres
	 * @param gereFils	true si les fils sont g&eacute;r&eacute;s par les objets m&eacute;tier (dans ce cas la construction de l'arbre
	 * est n&eacute;cessairement dynamique)
	 * Lance une exception si l'arbre n'est pas correctement param&eacute;tr&eacute;
	 */
	public void initialiser(EOEditingContext editingContext,String nomEntite,boolean selectionnerRacine,boolean doitFetcher,boolean gereFils) throws Exception {
		initialize(editingContext, nomEntite, selectionnerRacine, doitFetcher, gereFils);
	}
	/** Initialisation du treeView non-dynamique avec fetch<BR>
    Tous les attributs doivent avoir ete initialises : key, parentKey, fieldForDisplay, qualifierForFirstColumn et, optionellement, restrictionQualifier
	 * @param nomEntite nom de l'entit&eacute; &agrave; fetcher (null si les objets sont fournis par l'appelant)
	 * @param selectionnerRacine true si la racine est selectionn&eacute;e lors de l'affichage de l'arbre
	 */
	public void initialiser(EOEditingContext editingContext,String nomEntite,boolean selectionnerRacine) throws Exception {
		initialiser(editingContext,nomEntite,selectionnerRacine,true,false);
	}
	// Accessors
	/** Returns the attribute of the parent to be compared with the parent attribute<BR>
	 * Retourne l'attribut du parent qui sera compar&eacute; &agrave; l'attribut du parent dans le fils */
	public String attributeOfParent() {
		return key;
	}

	public void setAttributeOfParent(String key) {
		this.key = key;
	}
	/** Returns the parent attribute of the child used to find the parent node<BR>
	 * Retourne l'attribut parent du fils utilis&eacute; pour identifier le noeud parent */
	public String parentAttributeOfChild() {
		return parentKey;
	}

	public void setParentAttributeOfChild(String parentKey) {
		this.parentKey = parentKey;
	}

	/** Define the key and parent  key to build the tree. This option is not compatible with parentRelationship<BR>
        Definit les champs cles et cles pere pour la construction de l'arbre. Cette option est incompatible avec parentRelationship.
	 */
	public void setKeyParentKey(String keyValue, String parentKeyValue) {
		if((keyValue != null) && (parentKeyValue != null)) {
			key = keyValue;
			parentKey = parentKeyValue;
			parentRelationship = null;
			if(qualifierForFirstColumn == null) {
				qualifierForFirstColumn = EOQualifier.qualifierWithQualifierFormat(keyValue+"="+parentKeyValue, null);
			}
		}
		else {
			key = null;
			parentKey = null;
			NSLog.out.appendln("CRITreeView.setKeyParentKey() - key = null et/ou parentKey = null");
		}
	}
	/** Returns the name of the relationship child->parent used to build the treew<BR>
	 * Retourne le nom de la relation fils -> pere utilis&eacute;e pour la construction de l'arbre.
	 */
	public String parentRelationship() {
		return parentRelationship;
	}
	/** Define the name of the relationship child->parent to build the tree. This option is not compatible with key and parentKey<BR>
        Definit le nom de la relation fils -> pere pour la construction de l'arbre. Cette option est incompatible avec l'utilisation de key et parentKey.
	 */
	public void setParentRelationship(String relationName) {
		if (relationName != null) {
			parentRelationship = relationName;
			key = null;
			parentKey = null;
			if (qualifierForFirstColumn == null) {
				qualifierForFirstColumn = EOQualifier.qualifierWithQualifierFormat(parentRelationship+"=%@", new NSArray(NSKeyValueCoding.NullValue));
				NSLog.out.appendln("CRITreeView.setParentRelationship() - qualifier for column: " + qualifierForFirstColumn);
			}
		}
		else {
			parentRelationship = null;
		}
	}
	/** Set the qualifier to restrict the list of displayed nodes<BR>
	 * D&eacute;finit le qualifier qui permet de restreindre la liste des enregistrements accessibles dans l'arbre
	 */
	public void setRestrictionQualifier(EOQualifier theQualifier) {
		restrictionQualifier = theQualifier;
	}
	/** Set the qualifier for the first column of the records displayed in the tree<BR>
	 * D&eacute;finit le qualifier pour la premi&egrave;re colonne des enregistrements de l'arbre */
	public void setQualifierForFirstColumn(EOQualifier theQualifier) {
		qualifierForFirstColumn = theQualifier;
	}
	/** Returns the attribute name to be displayed<BR>
    Retourne le nom de l'attribut destine &agrave; l'affichage.*/
	public String fieldForDisplay() {
		return fieldForDisplay;
	}
	/** Set the attribute name to be displayed<BR>
    D&eacute;finit le nom de l'attribut destine &agrave; l'affichage.*/
	public void setFieldForDisplay(String fieldName) {
		fieldForDisplay = fieldName;
	}
	/** Returns the title of the tree wiew<BR>
    Retourne le titre du tree view. */
	public String title() {
		return title;
	}	
	/** Define the title of the tree view<BR>
        Definit le titre qui s'affiche en haut du treeview */
	public void setTitle(String aTitle) {
		title = aTitle;
	}
	/** Returns the entity name to be displayed in the tree<BR>
	 * Retourne le nom de l'entit&eacute; affich&eacute;e dans l'arbre
	 */
	public String entityName() {
		return entityName;
	}

	public void setEntityName(String entityName) {
		this.entityName = entityName;
	}
	/** Returns true if the view is responsible for fetching data else objects should be provided before initializing the
	 * tree with the setDisplayedObjects<BR>
	 * Retourne true si la vue est responsable du fetch des objets qui doivent sinon &ecirc;tre fournis avant l'initialisation
	 * par l'appel &agrave; la m&eacute;thode setDisplayedObjects<BR>
	 * @return
	 */
	public boolean mustFetch() {
		return mustFetch;
	}
	public void setMustFetch(boolean mustFetch) {
		this.mustFetch = mustFetch;
	}
	/** Used by Netbeans */
	public void setMustFetch(Boolean mustFetch) {
		this.mustFetch = mustFetch.booleanValue();
	}
	/** Returns the qualifier used to filter the nodes to be displayed.<BR>
	 * Retourne le qualifier utilis&eacute; pour filtrer les objets.
	 * @return String
	 */
	public String filterQualifier() {
		if (restrictionQualifier != null) {
			return restrictionQualifier.toString();
		} else {
			return null;
		}
	}
	/** Provides the qualifier used to filter the nodes to be displayed
	 * If a qualifier with variables must be used, you should call the method setRestrictionQualifier<BR>
	 * Fournit le qualifier utilis&eacute; pour filtrer les objets. Si ce qualifier doit comporter des variables,
	 * utiliser directement la m&eacute;thode setRestrictionQualifier
	 * @param stringForQualifier String
	 */
	public void setFilterQualifier(String stringForQualifier) {
		EOQualifier qualifier = null;
		if (stringForQualifier != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat(stringForQualifier, null);
		} 
		setRestrictionQualifier(qualifier);
	}
	/** Returns the qualifier used to find the root node.<BR>
	 * Retourne le qualifier utilis&eacute; pour trouver le noeud racine.
	 * @return String
	 */
	public String qualifierForRootNode() {
		if (qualifierForFirstColumn != null) {
			return qualifierForFirstColumn.toString();
		} else {
			return null;
		}
	}
	/** Provides the qualifier used to find the root node
	 * If a qualifier with variables must be used, you should call the method setQualifierForFirstColumn<BR>
	 * Fournit le qualifier utilis&eacute; pour trouver le noeud racine. Si ce qualifier doit comporter des variables,
	 * utiliser directement la m&eacute;thode setQualifierForFirstColumn
	 * @param stringForQualifier String
	 */
	public void setQualifierForRootNode(String stringForQualifier) {
		EOQualifier qualifier = null;
		if (stringForQualifier != null) {
			qualifier = EOQualifier.qualifierWithQualifierFormat(stringForQualifier, null);
		} 
		setQualifierForFirstColumn(qualifier);
	}
	/** Return true if the tree is dynamic<BR>
        Retourne true si l'arbre est dynamique */
	public boolean isDynamic() {
		return isDynamic;
	}
	/** Set the tree as dynamic or not: dynamic tree look for children when clicking on a node, for non-dynamic tree all children are loaded
	 * when initializing the tree<BR>
	 * D&eacute;finit si le tree view est dynamic : un arbre dynamique recherche les enfants d'un noeud lors du clic sur ce noeud,
	 * un arbre statique charge tous les enregistrements au debut.
	 */
	public void setDynamic(boolean isDynamic) {
		this.isDynamic = isDynamic;
	}
	/** Used by Netbeans */
	public void setDynamic(Boolean isDynamic) {
		this.isDynamic = isDynamic.booleanValue();
	}
	/** Return true if the children are managed by the enterprise objects (in such a case the tree must be dynamic)<BR>
	 * Retourne true si les fils sont g&eacute;r&eacute;s par les objets m&eacute;tier (dans ce cas la construction de l'arbre
	 * est n&eacute;cessairement dynamique) */
	public boolean manageChildren() {
		return manageChildren;
	}
	public void setManageChildren(boolean manageChildren) {
		this.manageChildren = manageChildren;
	}
	/** Used by Netbeans */
	public void setManageChildren(Boolean manageChildren) {
		this.manageChildren = manageChildren.booleanValue();
	}
	/** Return true if the root node is visible<BR>
	 *  Retourne true si le noeud racine est visible */
	public boolean isRootVisible() {
		return isRootVisible;
	}
	/** Set the visibility of the root node<BR>
	 * Modifie la visibilit&eacute; du noeud racine */
	public void setRootVisible(boolean isVisible) {
		isRootVisible = isVisible;
		theTree.setRootVisible(isRootVisible);
		// Fonctionne bien sans cela, a remettre si necessiare
		// this.invalidate();
		// this.validate();
	}
	/** Used by Netbeans */
	public void setRootVisible(Boolean isVisible) {
		setRootVisible(isVisible.booleanValue());
	}
	/** Returns a string displaying the properties of the tree */
	public String displayTreeProperties() {
		String text = "entity : " + entityName() + ", mustFetch : " + mustFetch + ", isDynamic : " + isDynamic() + ", manageChildren : " + manageChildren();
		text += "\nfield for display :" + fieldForDisplay();
		if (attributeOfParent() != null && parentAttributeOfChild() != null) {
			text += "\nattributeOfParent : " + attributeOfParent() + "parentAttributeOfChild : " + parentAttributeOfChild();
		}
		if (parentRelationship() != null) {
			text += "\nparentRelationship : " + parentRelationship();
		}
		text += "\nRoot node qualifier " + qualifierForFirstColumn;
		text += "\nRestriction qualifier " + restrictionQualifier;
		return text;
	}
	/** Returns the JTRee<BR>
    Retourne le JTree */
	public JTree tree() {
		return theTree;
	}
	/** Return true if the root node is selected<BR>
        Retourne true si le noeud racine est selectionne */
	public boolean isRootSelected() {
		return isRootSelected;
	}
	/** Select the root node<BR>
        Selectionne le noeud racine */
	public void selectRoot() {
		TreePath rootPath = new TreePath(rootNode);
		theTree.setSelectionPath(rootPath);
	}
	/** Enable or disable the selection in the tree<BR>
        Autorise ou emp&ecirc;che la s&eacute;lection dans l'arbre */
	public void setEnabled(boolean aBool) {
		if (theTree != null) {
			theTree.setEnabled(aBool);
			theTree.validate();
		}
	}

	/** Returns an array containing the records of the selected path, starting at the root node<BR
        Retourne un NSArray qui contient les elements du chemin selectionne, en commencant par le noeud racine */
	public NSArray pathElements() {
		NSArray lArray;
		TreePath lePath = theTree.getSelectionPath();
		lArray = new NSArray(lePath.getPath());

		return lArray;
	}
	/** Returns the selected path as a TreePath<BR>
        Retourne le chemin selectionne sous forme de TreePath */
	public TreePath selectedPath() {
		return theTree.getSelectionPath();
	}
	/** Select the tree path and open it if required<BR>
        Selectionne le chemin passe en parametre et l'ouvre si necessaire */
	public void setSelectedPath(TreePath aTreePath) {
		selectAndShowPath(aTreePath);
	}
	/** Returns true if the selected object is a leaf<BR>
        Retourne true si le noeud s&eacute;lectionn&eacute; est une feuille */
	public boolean selectedObjectIsLeaf() {
		CRITreeNode theNode = (CRITreeNode)theTree.getLastSelectedPathComponent();
		return theNode.isLeaf();
	}
	/** Return the selected CRITreeNode<BR>
        Retourne le CRITreeNode s&eacute;lectionn&eacute;*/
	public CRITreeNode selectedNode() {
		return (CRITreeNode)theTree.getLastSelectedPathComponent();
	}
	/** Return the node containing the tree path<BR>
        Retourne le noeud contenu dans le tree path */
	public CRITreeNode nodeAtPath(TreePath aPath) {
		return (CRITreeNode)aPath.getLastPathComponent();
	}
	/** Returns the value of the key of the selected object or null if no object is selected<BR>
        Retourne la valeur de la cle de l'objet s&eacute;lectionn&eacute; ou null si aucun objet n'est s&eacute;lectionn&eacute; */
	public Object selectedKey() {
		if (key != null) {
			if (selectedObject != null) {
				return (selectedObject.valueForKey(key));
			}
			else {
				return null;
			}
		}
		else {
			return null;
		}
	}
	/** Set the list of displayed objects: must be used for trees with no fetch */
	public void setDisplayedObjects(NSArray anArray) {
		displayedObjects = anArray;
	}
	/** Fournit la liste des objets &agrave; afficher : &agrave; utiliser pour les arbres qui ne font pas de fetchs */
	public void setObjetsGeres(NSArray anArray) {
		displayedObjects = anArray;
	}
	/** Selects the node whose key is equal to the provided value and displays it<BR>
        Recherche le noeud dont la cle (key) est &eacute;gale &agrave; la valeur pass&eacute;e en param&egrave;tre et l'affiche dans l'arbre.
     Selectionne le noeud correspondant, l'affiche est notifie les observers que le treeview a change.*/
	public void selectKey(Object value) {
		CRITreeNode currentNode = null;
		TreePath leTreePath;
		boolean found = false;

		if (key == null) {
			return;
		}
		// Recherche du noeud correspondant a partir de la racine :
		// - Enumeration en profondeur d'abord
		Enumeration enumerator = rootNode.depthFirstEnumeration();
		// - Recherche du noeud contenant la valeur
		while(enumerator.hasMoreElements()) {
			currentNode = (CRITreeNode)enumerator.nextElement();
			// si c'est le noeud racine : pas d'enregistrement
			if(currentNode == rootNode) {
				continue;
			}
			// comparaison des deux valeurs
			if(currentNode.keyValue().equals(value)) {
				// si la valeur est identique, quitter la boucle
				found = true;
				break;
			}
		}
		// Si l'on a parcouru toute l'enumerator et qu'on a rien trouve
		if(!found) {
			currentNode = rootNode;
		}

		// si le noeud est null ou si c'est la racine
		if(currentNode == null || currentNode == rootNode) {
			// selectionner le noeud racine
			leTreePath = new TreePath(rootNode);
		}
		else {
			// Contruction du path jusqu'au noeud
			TreeNode[] lePath = currentNode.getPath();
			leTreePath = new TreePath(lePath);
			// Selection du path dans l'arbre
		}
		//
		selectAndShowPath(leTreePath);
	}
	/** Selects the node which contains the provided objects<BR>
        Recherche le noeud qui contient l'object pass&eacute; en param&egrave;tre et le selectionne */
	public void selectObject(Object anObject) {
		CRITreeNode currentNode = null;
		TreePath leTreePath;
		boolean found = false;

		// Recherche du noeud correspondant a partir de la racine :
		// - Enumeration en profondeur d'abord
		Enumeration enumerator = rootNode.depthFirstEnumeration();
		// - Recherche du noeud contenant la valeur
		while(enumerator.hasMoreElements()) {
			currentNode = (CRITreeNode)enumerator.nextElement();
			// si c'est le noeud racine : pas d'enregistrement
			if(currentNode == rootNode) {
				continue;
			}
			// comparaison des deux valeurs
			if(currentNode.record() == anObject) {
				// si la valeur est identique, quitter la boucle
				found = true;
				break;
			}
		}

		// Si l'on a parcouru toute l'enumerator et qu'on a rien trouve...
		if(!found) {
			currentNode = rootNode;
		}

		// si le noeud est null ou si c'est la racine
		if(currentNode == null || currentNode == rootNode) {
			// selectionner le noeud racine
			leTreePath = new TreePath(rootNode);
		}
		else {
			// Contruction du path jusqu'au noeud
			TreeNode[] lePath = currentNode.getPath();
			leTreePath = new TreePath(lePath);
			// Selection du path dans l'arbre
		}

		selectAndShowPath(leTreePath);


	}
	/** Update the tree starting at tree path, refetch all the children of tree path. If tree path is null, the whole tree is rebuilt<BR>
	 * Mise &agrave; jour d'une partie de l'arbre, refetch tous les descendants du tree path. Si tree path est null, tout l'arbre est reconstruit */
	public void update(TreePath aTreePath) {
		CRITreeNode updateNode, currentNode, newNode;
		NSArray childs, previousLevel;
		NSMutableArray currentLevel;
		int itLevel, itChilds;
		int levelMax, childMax;

		if(aTreePath == null) {
			// Mise a jour complete
			updateNode = rootNode;
		}
		else {
			// Recuperer le node selectionne
			theTree.invalidate();
			theTree.setSelectionPath(aTreePath);
			updateNode = (CRITreeNode)theTree.getLastSelectedPathComponent();
		}
		// Rechargement de la table
		if (mustFetch || isDynamic() == false) {
			fetchTreeTable();
		}
		// Suppression des descendants
		updateNode.removeAllChildren();
		// Reconstruire l'arborescence du node
		previousLevel = new NSArray(updateNode);
		boolean goOn;
		int nbTimes = 0;
		do {
			currentLevel = new NSMutableArray();
			levelMax = previousLevel.count();
			for(itLevel = 0 ; itLevel < levelMax ; itLevel++ ) {
				currentNode = (CRITreeNode)previousLevel.objectAtIndex(itLevel);
				childs = findChildren(currentNode);
				childMax = childs.count();
				for(itChilds = 0 ; itChilds < childMax ; itChilds++) {
					newNode = new CRITreeNode((EOGenericRecord)childs.objectAtIndex(itChilds), key, parentKey, fieldForDisplay);
					currentNode.add(newNode);
					currentLevel.addObject(newNode);
				}
			}
			nbTimes++;
			previousLevel = (NSArray)currentLevel;
			if (isDynamic() == false) {
				goOn = (previousLevel.count() > 0);
			} else {
				goOn = (nbTimes < 2);
			}
			// Tant qu'il y a des enregistrements qui peuvent avoir des descendants
		}while (goOn);

		this.createTree(false);
		if (aTreePath != null) {
			this.setSelectedPath(aTreePath);
		}
	}
	/** Update the whole tree and select the provided object<BR>
	 * Met &agrave; jour tout l'arbre et s&eacute;lectionne l'objet fourni */
	public void updateAndSelect(Object object) {
		this.update((TreePath)null);
		selectObject(object);
	}
	/** Return a string describing the tree<BR>
        Representation de l'arbre sous forme de String */
	public String treeRepresentation() {
		return childrenRepresentation(rootNode);
	}
	/** Returns the tree as a string starting at node<BR>
        Representation de l'arbre a partir d'un noeud donne */
	public String childrenRepresentation(TreeNode node) {
		int i;
		StringBuffer buffer = new StringBuffer();
		Enumeration enumerator = node.children();
		CRITreeNode leNode;
		TreeNode[] lePath;

		while(enumerator.hasMoreElements()) {
			leNode = (CRITreeNode)enumerator.nextElement();
			lePath=leNode.getPath();
			for(i = 0 ; i < lePath.length ; i++) {
				if(i < lePath.length-1) {
					if(i != 0) 
						buffer.append("    ");
				}
				else {
					buffer.append("/"+((CRITreeNode)lePath[i]).displayValue()+"\n");
				}
			}
			if(!childrenRepresentation(leNode).equals("")) {
				buffer.append(childrenRepresentation(leNode));
			}

		}
		return buffer.toString();
	}
	/** Add a mouse listener for tree view management
        Ajoute un listener pour la gestion du treeView */
	public void addMouseListener(MouseListener l) {
		theTree.addMouseListener(l);
	}
	// Méthodes protégées
	/** Returns an array of the children of a node<BR>
    Retourne un NSArray qui contient les enfants d'un noeuds.
	 */
	protected NSArray findChildren(CRITreeNode theNode) {
		EOQualifier baseQualifier;
		EOSortOrdering aSort = EOSortOrdering.sortOrderingWithKey(fieldForDisplay, EOSortOrdering.CompareCaseInsensitiveAscending);


		if (theNode == rootNode) { // premier niveau de l'arbre
			baseQualifier = qualifierForFirstColumn;
		} else { 		// Les autres niveaux
			if (theNode.record() instanceof DynamicTreeInterface) {
				return ((DynamicTreeInterface)theNode.record()).findNodes();
			} else if (theNode.record() instanceof InterfaceArbreAvecFils) {
				return ((InterfaceArbreAvecFils)theNode.record()).trouverFils();
			}
			if (key != null) {
				if (theNode.keyValue() == null) {
					return new NSArray();
				}
				baseQualifier = EOQualifier.qualifierWithQualifierFormat("%@=%@ and %@<>%@", new NSArray(new Object[]{parentKey, theNode.keyValue(), key, theNode.keyValue()}));
			}
			else {
				baseQualifier = EOQualifier.qualifierWithQualifierFormat("%@ = %@", new NSArray(new Object[]{parentRelationship, theNode.record()}));    
			}
		}

		// Recherche des enregistrements dans la table
		NSMutableArray fetchResult;
		if (!isDynamic()) {
			fetchResult = new NSMutableArray(EOQualifier.filteredArrayWithQualifier(displayedObjects, baseQualifier));
			EOSortOrdering.sortArrayUsingKeyOrderArray(fetchResult, new NSArray(aSort));
		} else {
			EOQualifier qualifier;
			if (theNode != rootNode) {
				NSMutableArray qualifiers = new NSMutableArray(baseQualifier);
				if (restrictionQualifier != null) {
					qualifiers.addObject(restrictionQualifier);
				}
				qualifier = new EOAndQualifier(qualifiers);
			} else {
				qualifier = qualifierForFirstColumn;
			}
			EOFetchSpecification fetchSpec = new EOFetchSpecification(entityName,qualifier, new NSArray(aSort));
			fetchResult = new NSMutableArray(editingContext.objectsWithFetchSpecification(fetchSpec));	
		}

		// On evite de "boucler" en cas de probleme
		if (fetchResult.containsObject(theNode.record())) {
			fetchResult.removeObject(theNode.record());
		}
		return (NSArray)fetchResult;
	}
	/**  Selectionne l'element choisi dans le display group pour pouvoir repondre
    a displayGroup.selectedObject().
    Envoi la notification CRITreeViewDidClick.
    Le dictionnaire userInfo lie a la notification contient deux valeurs :
        - "selectedObject" : l'enregistrement selectionne
        - "isLeaf" : reponse du noeud a isLeaf "TRUE" ou "FALSE"
	 */
	protected void treeViewDidClick(CRITreeNode node) {
		if (!isEnabled()) {
			return;
		}
		if (node == null) {
			return;
		}
		if (node == rootNode) {
			selectedObject = null;
			isRootSelected = true;
			NSArray keyArray = new NSArray(new Object[]{"isLeaf"});
			NSArray valueArray = new NSArray(new Object[]{"FALSE"});
			NSDictionary userDictionary = new NSDictionary(valueArray, keyArray);
			NSNotificationCenter.defaultCenter().postNotification(TREE_VIEW_DID_CLICK, this, userDictionary);
		}
		else {
			isRootSelected = false;
			selectedObject = node.record();
			NSArray keyArray = new NSArray(new Object[]{"selectedRecord", "isLeaf"});
			NSArray valueArray = new NSArray(new Object[]{node.record(), (node.isLeaf() ? "TRUE" : "FALSE")});
			NSDictionary userDictionary = new NSDictionary(valueArray, keyArray);
			NSNotificationCenter.defaultCenter().postNotification(TREE_VIEW_DID_CLICK, this, userDictionary);
		}
	}

	/**  Selectionne l'element choisi dans le display group pour pouvoir repondre
    a displayGroup.selectedObject().
    Envoi la notification CRITreeViewDidClick.
    Le dictionnaire userInfo lie a la notification contient deux valeurs :
    - "selectedObject" : l'enregistrement selectionne ou null si aucun enregistrement selectionne (ROOT)
    - "isLeaf" : reponse du noeud a isLeaf "TRUE" ou "FALSE"
	 */
	protected void treeViewDidDoubleClick(CRITreeNode node) {
		if (!isEnabled()) {
			return;
		}
		if (node == rootNode) {
			selectedObject = null;
			isRootSelected = true;
			NSArray keyArray = new NSArray(new Object[]{"isLeaf"});
			NSArray valueArray = new NSArray(new Object[]{"FALSE"});
			NSDictionary userDictionary = new NSDictionary(valueArray, keyArray);
			NSNotificationCenter.defaultCenter().postNotification(TREE_VIEW_DID_CLICK, this, userDictionary);
		}
		else {
			selectedObject = node.record();
			// preparation du dictionnaire pour userInfo()
			NSArray keyArray = new NSArray(new Object[]{"selectedRecord", "isLeaf"});
			NSArray valueArray = new NSArray(new Object[]{node.record(), (node.isLeaf() ? "TRUE" : "FALSE")});
			NSDictionary userDictionary = new NSDictionary(valueArray, keyArray);
			// envoi notification
			NSNotificationCenter.defaultCenter().postNotification(TREE_VIEW_DID_DOUBLE_CLICK, this, userDictionary);
		}
	}
	// Chargement de la table associee a l'arbre a afficher. Tient compte du restrictionQualifier.
	private void fetchTreeTable() {
		// Recherche de tous les enregistrements dont on aura besoin...
		EOSortOrdering aSort = EOSortOrdering.sortOrderingWithKey(fieldForDisplay, EOSortOrdering.CompareCaseInsensitiveAscending);
		EOFetchSpecification fetchSpec = new EOFetchSpecification(entityName, restrictionQualifier, new NSArray(aSort));
		fetchSpec.setRefreshesRefetchedObjects(true);
		displayedObjects = editingContext.objectsWithFetchSpecification(fetchSpec);
	}
	// Creation de l'arbre a partir du rootNode
	private void createTree(boolean selectFirst) {
		this.setSize(viewDimension);
		this.setPreferredSize(viewDimension);
		boolean premierAffichage = (theScrollPane == null);
		// Remettre à zéro les listeners si l'arbre existait déjà
		if (theTree != null) {
			theTree.removeTreeExpansionListener(this);
			theTree.removeMouseListener(mouseListener);
		}
		theTree = new JTree(rootNode);
		// Modifier le rendu pour les feuilles (leaf)
		DefaultTreeCellRenderer renderer = new DefaultTreeCellRenderer();
		renderer.setLeafIcon(renderer.getClosedIcon());
		theTree.setCellRenderer(renderer);
		// ajout et affichage de l'arbre
		theTree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
		if (isDynamic()) {
			setRootVisible(true);
			theTree.setShowsRootHandles(true);
			theTree.setToggleClickCount(1);
			theTree.addTreeExpansionListener(this);	
		} 
		theTree.setBounds(this.getBounds());
		theTree.setFont(this.getFont());
		theTree.setRootVisible(isRootVisible);
		theTree.setScrollsOnExpand(true);
		addMouseListener(mouseListener);
		theScrollPane = new JScrollPane(theTree);

		theScrollPane.setSize(this.getSize());
		theScrollPane.setPreferredSize(this.getSize());
		theScrollPane.setMaximumSize(this.getSize());

		if (premierAffichage) {
			this.setLayout(new BorderLayout());
			this.setBorder(new TitledBorder(BorderFactory.createEmptyBorder(), title, TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.TOP | TitledBorder.LEFT, COConstants.defaultFont()));
		} else {
			this.removeAll();
		}
		this.add(theScrollPane);

		//this.invalidate();
		this.validate();
		// Fonctionne bien sans ca, a remettre si necessaire
		/*    this.invalidate();
        this. validate();*/
		if (selectFirst) {
			// Selection du premier noeud
			theTree.setSelectionRow(0);
			theTree.expandPath(theTree.getSelectionPath());
			// Notifier la selection
			treeViewDidClick((CRITreeNode)theTree.getLastSelectedPathComponent());
		}
	}


	// méthodes privées
	/**
        Selectionne le chemin passe en parametre et tente de le positionner dans la vue
	 */
	private void selectAndShowPath(TreePath aPath) {
		int originRow;
		int destinationRow;
		int showRow;

		// Recuperer la ligne selectionnee en cours
		originRow = theTree.getMinSelectionRow();
		// Selectionner le nouveau chemin
		theTree.setSelectionPath(aPath);
		if (!isDynamic()) {
			theTree.expandPath(aPath);
		} else {
			theTree.collapsePath(theTree.getSelectionPath());
		}
		// Recuperer la nouvelle ligne a selectionner
		destinationRow = theTree.getRowForPath(aPath);
		// Si on remonte
		if(destinationRow <= originRow) {
			// Calcul de la ligne a afficher
			showRow = destinationRow-(theTree.getVisibleRowCount()/2);
			if(showRow < 0)
				showRow = 0;
			// Positionner la ligne
			theTree.scrollRowToVisible(showRow);
		}
		// Si on descend
		else {
			// Calcul de la ligne a afficher
			showRow = (destinationRow+(theTree.getVisibleRowCount()/2));
			if(showRow >= theTree.getRowCount())
				showRow = theTree.getRowCount() - 1;
			theTree.scrollRowToVisible(showRow);
		}

		// Informer les observers que le treeView a change de selection
		treeViewDidClick((CRITreeNode)theTree.getLastSelectedPathComponent());
	}


	/**
        Classe privee CRITreeMouseListener :
     - Gere les clicks de souris dans le treeView.
	 */    
	private class CRITreeMouseListener extends MouseAdapter {
		// methode executee lorsque la souris a clicke
		// recupere le noeud selectionne lors du click et envoie
		// la fonction correspondante : treeViewDidClick ou treeViewDidDoubleClick
		public void mousePressed(MouseEvent e) {
			CRITreeNode node;
			int selRow;

			// TreePath selPath;
			selRow = theTree.getRowForLocation(e.getX(), e.getY());
			// selPath = theTree.getPathForLocation(e.getX(), e.getY());
			// si un element a ete selectionne
			if (selRow != -1) {

				// recuperer le noeud selectionne
				node = (CRITreeNode)theTree.getLastSelectedPathComponent();
				if(node == null)
					return;
				if(e.getClickCount() == 1) {
					// lance la methode de CRITreeView
					treeViewDidClick(node);
				}
				// double click
				else if(e.getClickCount() == 2) {
					// lance la methode ...
					treeViewDidDoubleClick(node);
				}
			}
		}    
	}

	// TreeExpansion listener
	public void treeCollapsed(TreeExpansionEvent event) {
		// ne rien faire
	}
	public void treeExpanded(TreeExpansionEvent event) {
		if (isDynamic() && !isExpending) {
			isExpending = true;
			update(event.getPath());
			theTree.expandPath(event.getPath());
			isExpending = false;
		}

	}



}
