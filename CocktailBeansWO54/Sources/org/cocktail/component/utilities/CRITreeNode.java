//
// CRITreeNode.java
/*
 * Copyright Consortium Cocktail
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */
package org.cocktail.component.utilities;

import javax.swing.tree.DefaultMutableTreeNode;

import com.webobjects.eocontrol.EOGenericRecord;

public class CRITreeNode extends DefaultMutableTreeNode {
    protected EOGenericRecord nodeRecord;
    protected String key, parentKey, fieldForDisplay;
    
    /** Default constructor to build an empty node (usually the root node when it's not displayed)<BR>
     * Constructeur par defaut cr&eacute;&eacute; un noeud vide, principalement pour le noeud racine (quand il n'est pas affich&eacute;) */
    public CRITreeNode() {
        super();
        nodeRecord = null;
        key = null;
        parentKey = null;
        fieldForDisplay = null;
    }
    
    /** Constructor with an object to display (can be used for the root node)<BR>
     * Constructeur avec un objet &grave; afficher (&eacute;ventuellement pour le noeud racine) */
    public CRITreeNode(Object nodeValue) {
        super(nodeValue);
        nodeRecord = null;
        key = null;
        parentKey = null;
        fieldForDisplay = null;
    }
    /** Constructor with an EOGenericRecord object to display, the key to display, the parent key and the value to display<BR>
     * Constructeur avec un EOGenericRecord &grave; afficher avec la cl&eacute; &agrave; afficher, la cl&eacute, parent et la valeur affich&eacute;e */
    public CRITreeNode(EOGenericRecord nodeValue, String keyValue, String parentKeyValue, String fieldForDisplayValue) {
        super(nodeValue.valueForKey(fieldForDisplayValue));
        nodeRecord = nodeValue;
        key = keyValue;
        parentKey = parentKeyValue;
        fieldForDisplay = fieldForDisplayValue;
    }
   /** Add a child to the node<BR>
    * Ajoute un fils au noeud */
    public void add(CRITreeNode newChild) {
        super.add(newChild);
    }
 	/** Returns the associated record<BR>
 	 * Retourne le record associ&eacute;
 	 */ 
    public EOGenericRecord record() {
        return nodeRecord;
    }
   /** Set the record associated to the node<BR>
    * D&eacute;finit le record associ&eacute; au noeud */
    public void setRecord(EOGenericRecord nodeValue) {
        nodeRecord = nodeValue;
    }
    /** Returns the value associated to the key or null if no key<BR>
     * Retourne la valeur associ&eacute;e &agrave; la cl&eacute; ou null si pas de cl&eacute;  */ 
    public Object keyValue() {
        if(key != null) {
            return nodeRecord.valueForKey(key);
        }
        else {
            return null;
        }
    }
   /** Set the key<BR>
    * D&eacute;finit la cl&eacute; */
    public void setKey(String keyValue) {
        key = keyValue;
    }
    /** Returns the value associated to the parent key or null if no parent key<BR>
     * Retourne la valeur associ&eacute;e &agrave; la cl&eacute; parent ou null si pas de cl&eacute; parent */ 
    public Object parentKeyValue() {
        if(parentKey != null) {
            return nodeRecord.valueForKey(parentKey);
        }
        else {
            return null;
        }
    }
    /** Set the parent key<BR>
     * D&eacute;finit la cl&eacute; parent */
    public void setParentKey(String parentKeyValue) {
        parentKey = parentKeyValue;
    }
    /** Returns the displayed value <BR>
     * Retourne la valeur affich&eacute;e */ 
    public Object displayValue() {
        if(fieldForDisplay != null) {
            return nodeRecord.valueForKey(fieldForDisplay);
        }
        else {
            return null;
        }
    }
}
