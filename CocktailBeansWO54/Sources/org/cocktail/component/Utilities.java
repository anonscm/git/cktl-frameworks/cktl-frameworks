/* CeCILL FREE SOFTWARE LICENSE AGREEMENT


    Notice

This Agreement is a Free Software license agreement that is the result
of discussions between its authors in order to ensure compliance with
the two main principles guiding its drafting:

 * firstly, compliance with the principles governing the distribution
      of Free Software: access to source code, broad rights granted to
      users,
 * secondly, the election of a governing law, French law, with which
      it is conformant, both as regards the law of torts and
      intellectual property law, and the protection that it offers to
      both authors and holders of the economic rights over software.

The authors of the CeCILL (for Ce[a] C[nrs] I[nria] L[ogiciel] L[ibre])
license are:

Commissariat a l'Energie Atomique - CEA, a public scientific, technical
and industrial research establishment, having its principal place of
business at 25 rue Leblanc, immeuble Le Ponant D, 75015 Paris, France.

Centre National de la Recherche Scientifique - CNRS, a public scientific
and technological establishment, having its principal place of business
at 3 rue Michel-Ange, 75794 Paris cedex 16, France.

Institut National de Recherche en Informatique et en Automatique -
INRIA, a public scientific and technological establishment, having its
principal place of business at Domaine de Voluceau, Rocquencourt, BP
105, 78153 Le Chesnay cedex, France.


    Preamble

The purpose of this Free Software license agreement is to grant users
the right to modify and redistribute the software governed by this
license within the framework of an open source distribution model.

The exercising of these rights is conditional upon certain obligations
for users so as to preserve this status for all subsequent redistributions.

In consideration of access to the source code and the rights to copy,
modify and redistribute granted by the license, users are provided only
with a limited warranty and the software's author, the holder of the
economic rights, and the successive licensors only have limited liability.

In this respect, the risks associated with loading, using, modifying
and/or developing or reproducing the software by the user are brought to
the user's attention, given its Free Software status, which may make it
complicated to use, with the result that its use is reserved for
developers and experienced professionals having in-depth computer
knowledge. Users are therefore encouraged to load and test the
suitability of the software as regards their requirements in conditions
enabling the security of their systems and/or data to be ensured and,
more generally, to use and operate it in the same conditions of
security. This Agreement may be freely reproduced and published,
provided it is not altered, and that no provisions are either added or
removed herefrom.

This Agreement may apply to any or all software for which the holder of
the economic rights decides to submit the use thereof to its provisions.


    Article 1 - DEFINITIONS

For the purpose of this Agreement, when the following expressions
commence with a capital letter, they shall have the following meaning:

Agreement: means this license agreement, and its possible subsequent
versions and annexes.

Software: means the software in its Object Code and/or Source Code form
and, where applicable, its documentation, "as is" when the Licensee
accepts the Agreement.

Initial Software: means the Software in its Source Code and possibly its
Object Code form and, where applicable, its documentation, "as is" when
it is first distributed under the terms and conditions of the Agreement.

Modified Software: means the Software modified by at least one
Contribution.

Source Code: means all the Software's instructions and program lines to
which access is required so as to modify the Software.

Object Code: means the binary files originating from the compilation of
the Source Code.

Holder: means the holder(s) of the economic rights over the Initial
Software.

Licensee: means the Software user(s) having accepted the Agreement.

Contributor: means a Licensee having made at least one Contribution.

Licensor: means the Holder, or any other individual or legal entity, who
distributes the Software under the Agreement.

Contribution: means any or all modifications, corrections, translations,
adaptations and/or new functions integrated into the Software by any or
all Contributors, as well as any or all Internal Modules.

Module: means a set of sources files including their documentation that
enables supplementary functions or services in addition to those offered
by the Software.

External Module: means any or all Modules, not derived from the
Software, so that this Module and the Software run in separate address
spaces, with one calling the other when they are run.

Internal Module: means any or all Module, connected to the Software so
that they both execute in the same address space.

GNU GPL: means the GNU General Public License version 2 or any
subsequent version, as published by the Free Software Foundation Inc.

Parties: mean both the Licensee and the Licensor.

These expressions may be used both in singular and plural form.


    Article 2 - PURPOSE

The purpose of the Agreement is the grant by the Licensor to the
Licensee of a non-exclusive, transferable and worldwide license for the
Software as set forth in Article 5 hereinafter for the whole term of the
protection granted by the rights over said Software. 


    Article 3 - ACCEPTANCE

3.1 The Licensee shall be deemed as having accepted the terms and
conditions of this Agreement upon the occurrence of the first of the
following events:

 * (i) loading the Software by any or all means, notably, by
      downloading from a remote server, or by loading from a physical
      medium;
 * (ii) the first time the Licensee exercises any of the rights
      granted hereunder.

3.2 One copy of the Agreement, containing a notice relating to the
characteristics of the Software, to the limited warranty, and to the
fact that its use is restricted to experienced users has been provided
to the Licensee prior to its acceptance as set forth in Article 3.1
hereinabove, and the Licensee hereby acknowledges that it has read and
understood it.


    Article 4 - EFFECTIVE DATE AND TERM


      4.1 EFFECTIVE DATE

The Agreement shall become effective on the date when it is accepted by
the Licensee as set forth in Article 3.1.


      4.2 TERM

The Agreement shall remain in force for the entire legal term of
protection of the economic rights over the Software.


    Article 5 - SCOPE OF RIGHTS GRANTED

The Licensor hereby grants to the Licensee, who accepts, the following
rights over the Software for any or all use, and for the term of the
Agreement, on the basis of the terms and conditions set forth hereinafter.

Besides, if the Licensor owns or comes to own one or more patents
protecting all or part of the functions of the Software or of its
components, the Licensor undertakes not to enforce the rights granted by
these patents against successive Licensees using, exploiting or
modifying the Software. If these patents are transferred, the Licensor
undertakes to have the transferees subscribe to the obligations set
forth in this paragraph.


      5.1 RIGHT OF USE

The Licensee is authorized to use the Software, without any limitation
as to its fields of application, with it being hereinafter specified
that this comprises:

   1. permanent or temporary reproduction of all or part of the Software
      by any or all means and in any or all form.

   2. loading, displaying, running, or storing the Software on any or
      all medium.

   3. entitlement to observe, study or test its operation so as to
      determine the ideas and principles behind any or all constituent
      elements of said Software. This shall apply when the Licensee
      carries out any or all loading, displaying, running, transmission
      or storage operation as regards the Software, that it is entitled
      to carry out hereunder.


      5.2 ENTITLEMENT TO MAKE CONTRIBUTIONS

The right to make Contributions includes the right to translate, adapt,
arrange, or make any or all modifications to the Software, and the right
to reproduce the resulting software.

The Licensee is authorized to make any or all Contributions to the
Software provided that it includes an explicit notice that it is the
author of said Contribution and indicates the date of the creation thereof.


      5.3 RIGHT OF DISTRIBUTION

In particular, the right of distribution includes the right to publish,
transmit and communicate the Software to the general public on any or
all medium, and by any or all means, and the right to market, either in
consideration of a fee, or free of charge, one or more copies of the
Software by any means.

The Licensee is further authorized to distribute copies of the modified
or unmodified Software to third parties according to the terms and
conditions set forth hereinafter.


        5.3.1 DISTRIBUTION OF SOFTWARE WITHOUT MODIFICATION

The Licensee is authorized to distribute true copies of the Software in
Source Code or Object Code form, provided that said distribution
complies with all the provisions of the Agreement and is accompanied by:

   1. a copy of the Agreement,

   2. a notice relating to the limitation of both the Licensor's
      warranty and liability as set forth in Articles 8 and 9,

and that, in the event that only the Object Code of the Software is
redistributed, the Licensee allows future Licensees unhindered access to
the full Source Code of the Software by indicating how to access it, it
being understood that the additional cost of acquiring the Source Code
shall not exceed the cost of transferring the data.


        5.3.2 DISTRIBUTION OF MODIFIED SOFTWARE

When the Licensee makes a Contribution to the Software, the terms and
conditions for the distribution of the resulting Modified Software
become subject to all the provisions of this Agreement.

The Licensee is authorized to distribute the Modified Software, in
source code or object code form, provided that said distribution
complies with all the provisions of the Agreement and is accompanied by:

   1. a copy of the Agreement,

   2. a notice relating to the limitation of both the Licensor's
      warranty and liability as set forth in Articles 8 and 9,

and that, in the event that only the object code of the Modified
Software is redistributed, the Licensee allows future Licensees
unhindered access to the full source code of the Modified Software by
indicating how to access it, it being understood that the additional
cost of acquiring the source code shall not exceed the cost of
transferring the data.


        5.3.3 DISTRIBUTION OF EXTERNAL MODULES

When the Licensee has developed an External Module, the terms and
conditions of this Agreement do not apply to said External Module, that
may be distributed under a separate license agreement.


        5.3.4 COMPATIBILITY WITH THE GNU GPL

The Licensee can include a code that is subject to the provisions of one
of the versions of the GNU GPL in the Modified or unmodified Software,
and distribute that entire code under the terms of the same version of
the GNU GPL.

The Licensee can include the Modified or unmodified Software in a code
that is subject to the provisions of one of the versions of the GNU GPL,
and distribute that entire code under the terms of the same version of
the GNU GPL.


    Article 6 - INTELLECTUAL PROPERTY


      6.1 OVER THE INITIAL SOFTWARE

The Holder owns the economic rights over the Initial Software. Any or
all use of the Initial Software is subject to compliance with the terms
and conditions under which the Holder has elected to distribute its work
and no one shall be entitled to modify the terms and conditions for the
distribution of said Initial Software.

The Holder undertakes that the Initial Software will remain ruled at
least by this Agreement, for the duration set forth in Article 4.2.


      6.2 OVER THE CONTRIBUTIONS

The Licensee who develops a Contribution is the owner of the
intellectual property rights over this Contribution as defined by
applicable law.


      6.3 OVER THE EXTERNAL MODULES

The Licensee who develops an External Module is the owner of the
intellectual property rights over this External Module as defined by
applicable law and is free to choose the type of agreement that shall
govern its distribution.


      6.4 JOINT PROVISIONS

The Licensee expressly undertakes:

   1. not to remove, or modify, in any manner, the intellectual property
      notices attached to the Software;

   2. to reproduce said notices, in an identical manner, in the copies
      of the Software modified or not.

The Licensee undertakes not to directly or indirectly infringe the
intellectual property rights of the Holder and/or Contributors on the
Software and to take, where applicable, vis-à-vis its staff, any and all
measures required to ensure respect of said intellectual property rights
of the Holder and/or Contributors.


    Article 7 - RELATED SERVICES

7.1 Under no circumstances shall the Agreement oblige the Licensor to
provide technical assistance or maintenance services for the Software.

However, the Licensor is entitled to offer this type of services. The
terms and conditions of such technical assistance, and/or such
maintenance, shall be set forth in a separate instrument. Only the
Licensor offering said maintenance and/or technical assistance services
shall incur liability therefor.

7.2 Similarly, any Licensor is entitled to offer to its licensees, under
its sole responsibility, a warranty, that shall only be binding upon
itself, for the redistribution of the Software and/or the Modified
Software, under terms and conditions that it is free to decide. Said
warranty, and the financial terms and conditions of its application,
shall be subject of a separate instrument executed between the Licensor
and the Licensee.


    Article 8 - LIABILITY

8.1 Subject to the provisions of Article 8.2, the Licensee shall be
entitled to claim compensation for any direct loss it may have suffered
from the Software as a result of a fault on the part of the relevant
Licensor, subject to providing evidence thereof.

8.2 The Licensor's liability is limited to the commitments made under
this Agreement and shall not be incurred as a result of in particular:
(i) loss due the Licensee's total or partial failure to fulfill its
obligations, (ii) direct or consequential loss that is suffered by the
Licensee due to the use or performance of the Software, and (iii) more
generally, any consequential loss. In particular the Parties expressly
agree that any or all pecuniary or business loss (i.e. loss of data,
loss of profits, operating loss, loss of customers or orders,
opportunity cost, any disturbance to business activities) or any or all
legal proceedings instituted against the Licensee by a third party,
shall constitute consequential loss and shall not provide entitlement to
any or all compensation from the Licensor.


    Article 9 - WARRANTY

9.1 The Licensee acknowledges that the scientific and technical
state-of-the-art when the Software was distributed did not enable all
possible uses to be tested and verified, nor for the presence of
possible defects to be detected. In this respect, the Licensee's
attention has been drawn to the risks associated with loading, using,
modifying and/or developing and reproducing the Software which are
reserved for experienced users.

The Licensee shall be responsible for verifying, by any or all means,
the suitability of the product for its requirements, its good working
order, and for ensuring that it shall not cause damage to either persons
or properties.

9.2 The Licensor hereby represents, in good faith, that it is entitled
to grant all the rights over the Software (including in particular the
rights set forth in Article 5).

9.3 The Licensee acknowledges that the Software is supplied "as is" by
the Licensor without any other express or tacit warranty, other than
that provided for in Article 9.2 and, in particular, without any warranty 
as to its commercial value, its secured, safe, innovative or relevant
nature.

Specifically, the Licensor does not warrant that the Software is free
from any error, that it will operate without interruption, that it will
be compatible with the Licensee's own equipment and software
configuration, nor that it will meet the Licensee's requirements.

9.4 The Licensor does not either expressly or tacitly warrant that the
Software does not infringe any third party intellectual property right
relating to a patent, software or any other property right. Therefore,
the Licensor disclaims any and all liability towards the Licensee
arising out of any or all proceedings for infringement that may be
instituted in respect of the use, modification and redistribution of the
Software. Nevertheless, should such proceedings be instituted against
the Licensee, the Licensor shall provide it with technical and legal
assistance for its defense. Such technical and legal assistance shall be
decided on a case-by-case basis between the relevant Licensor and the
Licensee pursuant to a memorandum of understanding. The Licensor
disclaims any and all liability as regards the Licensee's use of the
name of the Software. No warranty is given as regards the existence of
prior rights over the name of the Software or as regards the existence
of a trademark.


    Article 10 - TERMINATION

10.1 In the event of a breach by the Licensee of its obligations
hereunder, the Licensor may automatically terminate this Agreement
thirty (30) days after notice has been sent to the Licensee and has
remained ineffective.

10.2 A Licensee whose Agreement is terminated shall no longer be
authorized to use, modify or distribute the Software. However, any
licenses that it may have granted prior to termination of the Agreement
shall remain valid subject to their having been granted in compliance
with the terms and conditions hereof.


    Article 11 - MISCELLANEOUS


      11.1 EXCUSABLE EVENTS

Neither Party shall be liable for any or all delay, or failure to
perform the Agreement, that may be attributable to an event of force
majeure, an act of God or an outside cause, such as defective
functioning or interruptions of the electricity or telecommunications
networks, network paralysis following a virus attack, intervention by
government authorities, natural disasters, water damage, earthquakes,
fire, explosions, strikes and labor unrest, war, etc.

11.2 Any failure by either Party, on one or more occasions, to invoke
one or more of the provisions hereof, shall under no circumstances be
interpreted as being a waiver by the interested Party of its right to
invoke said provision(s) subsequently.

11.3 The Agreement cancels and replaces any or all previous agreements,
whether written or oral, between the Parties and having the same
purpose, and constitutes the entirety of the agreement between said
Parties concerning said purpose. No supplement or modification to the
terms and conditions hereof shall be effective as between the Parties
unless it is made in writing and signed by their duly authorized
representatives.

11.4 In the event that one or more of the provisions hereof were to
conflict with a current or future applicable act or legislative text,
said act or legislative text shall prevail, and the Parties shall make
the necessary amendments so as to comply with said act or legislative
text. All other provisions shall remain effective. Similarly, invalidity
of a provision of the Agreement, for any reason whatsoever, shall not
cause the Agreement as a whole to be invalid.


      11.5 LANGUAGE

The Agreement is drafted in both French and English and both versions
are deemed authentic.


    Article 12 - NEW VERSIONS OF THE AGREEMENT

12.1 Any person is authorized to duplicate and distribute copies of this
Agreement.

12.2 So as to ensure coherence, the wording of this Agreement is
protected and may only be modified by the authors of the License, who
reserve the right to periodically publish updates or new versions of the
Agreement, each with a separate number. These subsequent versions may
address new issues encountered by Free Software.

12.3 Any Software distributed under a given version of the Agreement may
only be subsequently distributed under the same version of the Agreement
or a subsequent version, subject to the provisions of Article 5.3.4.


    Article 13 - GOVERNING LAW AND JURISDICTION

13.1 The Agreement is governed by French law. The Parties agree to
endeavor to seek an amicable solution to any disagreements or disputes
that may arise during the performance of the Agreement.

13.2 Failing an amicable solution within two (2) months as from their
occurrence, and unless emergency proceedings are necessary, the
disagreements or disputes shall be referred to the Paris Courts having
jurisdiction, by the more diligent Party.

Version 2.0 dated 2006-09-05.

 */
package org.cocktail.component;

import java.io.File;
import java.io.FileInputStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.net.URL;
import java.util.Properties;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

/** Static methods to list the methods of a class and to get information about parameters or returned values */
public class Utilities {
	public static Boolean isModeDebug;
	/** Lookup recursivelly for the methods of a class of a certain type (accessors, actions,... see COConstants), and with a certain type of parameter. 
	 * For Accessors, you can specify if it should look for read or write accessors.<BR>
	 * Methods are ordered by alphabetical order and duplicate names are removed.
	 * @param classForLookup name of the class we want to know the methods
	 * @param methodType : type of method we want (see COConstants)
	 * @param parameterClass class of the parameter of a write accessor
	 * @param shouldHaveReadWriteAccessors true if we want to get read/write accessors  */
	public static NSArray lookForMethods(Class classForLookup,int methodType,Class parameterClass,boolean shouldHaveReadWriteAccessors) {
		try {
			NSArray methods = methodsForClassHierarchy(classForLookup);
			if (methods.count() == 0) {
				return null;
			}
			NSMutableArray methodNames = new NSMutableArray();
			java.util.Enumeration e = methods.objectEnumerator();
			while (e.hasMoreElements()) {
				Method method = (Method)e.nextElement();
				// An action has the type void and no parameter
				// an enabling method has the type boolean and no parameter
				// An accessor has the format getXXX() isXXX() ou xXX() and returns an object or setXXX(Object yy). When read AND write
				// accessors must be provided, only write accessors are looked for and we verify that read accessors exist
				// For boolean accessors, we check that the parameter of the write accessor is a boolean
				Class returnedType = method.getReturnType();
				String methodName = method.getName();
				if (Modifier.isPublic(method.getModifiers())) {
					switch (methodType) {
					case COConstants.ACCESSORS :
					case COConstants.BOOLEAN_ACCESSOR :
					case COConstants.INTEGER_ACCESSOR :
					case COConstants.STRING_ACCESSOR :
					case COConstants.TEXT_ACCESSOR :
					case COConstants.IMAGE_ACCESSOR :
					case COConstants.OBJECT_ACCESSOR :
						if (shouldHaveReadWriteAccessors) {
							if (returnedType.getName().equals("void")) {
								if (methodName.startsWith("set") && methodName.length() > 3 && method.getParameterTypes().length == 1) {
									boolean lookForReadAccessor = (methodType == COConstants.ACCESSORS);
									if (!lookForReadAccessor) {
										Class classeParametre = method.getParameterTypes()[0];
										if (methodType == COConstants.BOOLEAN_ACCESSOR) {
											lookForReadAccessor = (classeParametre == boolean.class);
										} else if (methodType == COConstants.INTEGER_ACCESSOR) {
											lookForReadAccessor =  (classeParametre == int.class || classeParametre == Integer.class);
										} else if (methodType == COConstants.STRING_ACCESSOR) {
											lookForReadAccessor = (classeParametre == String.class);
										} else if (methodType == COConstants.TEXT_ACCESSOR) {
											lookForReadAccessor = (classeParametre != boolean.class);
										} else if (methodType == COConstants.IMAGE_ACCESSOR) {
											lookForReadAccessor = (classeParametre.getName().equals(NSData.class.getName()));
										}else if (methodType == COConstants.OBJECT_ACCESSOR) {
											if (parameterClass == null || parameterClass.getName().equals(EOGenericRecord.class.getName())) {
												lookForReadAccessor = classeParametre.getName().equals(EOGenericRecord.class.getName());
											} else {
												lookForReadAccessor = (sameClasses(classeParametre,parameterClass) || classeParametre.getName().equals(EOGenericRecord.class.getName()));
											}
										}
									}
									if (lookForReadAccessor) {
										String readAccessorName = methodName.substring(3);
										if (hasReadAccessor(methods,readAccessorName)) {
											String name = makeMethodName(readAccessorName);
											if (methodNames.containsObject(name) == false) {	// to avoid duplicating the same inherited method
												methodNames.addObject(name);
											}
										}
									}
								}
							}
						} else {	// On recherche tous les accesseurs de lecture
							if (returnedType.getName().equals("void") == false && method.getParameterTypes().length == 0) {
								boolean keepIt = (methodType == COConstants.ACCESSORS);
								if (!keepIt) {
									if (methodType == COConstants.BOOLEAN_ACCESSOR) {
										keepIt = (returnedType == boolean.class);
									} else if (methodType == COConstants.INTEGER_ACCESSOR) {
										keepIt =  (returnedType == int.class || returnedType == Integer.class);
									} else if (methodType == COConstants.STRING_ACCESSOR) {
										keepIt = (returnedType == String.class);
									} else if (methodType == COConstants.TEXT_ACCESSOR) {
										keepIt = (returnedType != boolean.class);
									} else if (methodType == COConstants.IMAGE_ACCESSOR) {
										keepIt = (returnedType != null && returnedType.getName().equals(NSData.class.getName()));
									} else if (methodType == COConstants.OBJECT_ACCESSOR) {
										if (returnedType == null || returnedType.getName().equals(EOGenericRecord.class.getName())) {
											keepIt = returnedType.getName().equals(EOGenericRecord.class.getName());
										} else {
											keepIt = (sameClasses(returnedType,parameterClass) || returnedType.getName().equals(EOGenericRecord.class.getName()));
										}
									}
								}
								if (keepIt) {
									//System.out.println("method found " + methodName);
									if (methodNames.containsObject(methodName) == false) {	// to avoid duplicating the same inherited method
										if (methodName.indexOf("create") < 0 && methodName.indexOf("validate") < 0) {	
											// WOLips create functions must not be considered as accessors
											// Validation methods must not be considered as accessors
											methodNames.addObject(methodName);
										}
									}
								}
							}
						}
						break;
					case COConstants.ACTIONS :
						if (returnedType.getName().equals("void") && method.getParameterTypes().length == 0) {
							methodNames.addObject(methodName);
						}
						break;
					case COConstants.ENABLING_METHOD :
						if (returnedType.getName().equals("boolean") && method.getParameterTypes().length == 0) {
							methodNames.addObject(methodName);
						}
						break;
					}
				}
			}
			if (shouldHaveReadWriteAccessors) {
				switch (methodType) {
				case COConstants.ACCESSORS :
				case COConstants.BOOLEAN_ACCESSOR :
				case COConstants.INTEGER_ACCESSOR :
				case COConstants.STRING_ACCESSOR :
				case COConstants.TEXT_ACCESSOR :
				case COConstants.OBJECT_ACCESSOR : 
					NSArray fields = fieldNames(classForLookup,parameterClass,methodType == COConstants.TEXT_ACCESSOR);
					e = fields.objectEnumerator();
					while (e.hasMoreElements()) {
						String fieldName = (String)e.nextElement();
						if (methodNames.containsObject(fieldName) == false) {
							methodNames.addObject(fieldName);
						}
					}
				}
			}
			return clean(methodNames);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	/** Returns the returned type of the method from the class provided or null if the method cannot be found */
	public static String returnedTypeForMethodWithName(Class classForLookup,String methodName) {
		NSArray methods = methodsForClassHierarchy(classForLookup);
		java.util.Enumeration e = methods.objectEnumerator();
		while (e.hasMoreElements()) {
			Method method = (Method)e.nextElement();
			String name = method.getName();
			if (name.equals(methodName)) {
				return method.getReturnType().getName();
			}
		}
		return null;
	}
	/** Returns the type of the parameters of the method or null if the method cannot be found  */
	public static Class[] parameterTypeForMethodWithName(Class classForLookup,String methodName) {
		NSArray methods = methodsForClassHierarchy(classForLookup);
		java.util.Enumeration e = methods.objectEnumerator();
		while (e.hasMoreElements()) {
			Method method = (Method)e.nextElement();
			String name = method.getName();
			if (name.equals(methodName)) {
				return method.getParameterTypes();
			}
		}
		return null;
	}

	/** Return a displayable String array with the NSArray provided. The NSArray must contain Strings.
	 * The first key is COConstants.NONE
	 * @param stringArray
	 * @return String array
	 */
	public static String[] arrayFromNSArray(NSArray stringArray) {
		String[] s = new String[stringArray.count() + 1];
		s[0] = COConstants.NONE;
		for (int i = 0; i < stringArray.count(); i++) {
			s[i + 1] = (String)stringArray.objectAtIndex(i);
		}
		return s;
	}
	/** Returns the path of the project : it's identified by looking for JavaClient.wo<BR>
	 * In a Netbeans Java Client project, include the Components folder that should include JavaClient.wo */
	public static String pathProject() {
		String pathProject = null;
		URL projectUrl = ModelHelper.class.getClassLoader().getResource("JavaClient.wo"); 
		if (projectUrl != null) {
			String path = projectUrl.getPath();
			pathProject  = path.substring(0,path.indexOf("Components"));
		}  else {	// It's not an application but a jar
			projectUrl = ModelHelper.class.getClassLoader().getResource("Netbeans_Config"); 
			System.out.println("url " + projectUrl);
			if (projectUrl != null) {
				System.out.println("pathProject " + projectUrl);
				String path = projectUrl.getPath();
				pathProject  = path.substring(0,path.indexOf("Netbeans_Config"));
				System.out.println("pathProject " + pathProject);

			}
		}
		return pathProject;
	}
	/** Returns true if debug messages must be diplayed<BR>
	 * To get debug on, create in the project a file Netbeans_Config and add a property Debug_Mode=YES
	 */
	public static boolean isModeDebug() {
		if (isModeDebug == null) {
			isModeDebug = new Boolean(false);
			URL projectUrl = ModelHelper.class.getClassLoader().getResource("Netbeans_Config"); 
			if (projectUrl != null) {
				try {
					String fileConfigPath = projectUrl.getPath();
					if (fileConfigPath.lastIndexOf(File.separator) != fileConfigPath.length()) {
						// add a "/"
						fileConfigPath = fileConfigPath + File.separator;
					}
					fileConfigPath += "Cocktail.config";
					System.out.println("path config " + fileConfigPath);
					File fileConfig = new File(fileConfigPath);
					if (fileConfig.exists()) {
						FileInputStream stream = new FileInputStream(fileConfig);
						if (stream != null) {
							Properties properties = new Properties();
							properties.load(stream);
							String debug = properties.getProperty("Debug_Mode");
							isModeDebug = new Boolean(debug != null && debug.equals("YES"));
						}
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
		return isModeDebug.booleanValue();
	}
	/** To display a debug message */
	public static void logDebugMessage(String aStr) {
		if (isModeDebug()) {
			System.out.println(aStr);
		}
	}
	// Private methods
	private static NSArray methodsForClassHierarchy(Class aClass) {
		// Lookup of the methods that belong to class of the project
		NSMutableArray<Object> methods = new NSMutableArray<Object>();
		if (aClass != null && aClass.getName().equals("java.lang.Object") == false && aClass.getName().indexOf("com.webobjects") < 0) {
			methods.addObjects((Object[]) aClass.getDeclaredMethods());
			methods.addObjectsFromArray(methodsForClassHierarchy(aClass.getSuperclass()));
		}
		return methods;
	}
	private static NSArray fieldNames(Class classForLookup,Class parameterClass,boolean isTextAccessor) {
		NSMutableArray fieldNames = new NSMutableArray();
		Field[] fields = classForLookup.getFields();
		for (int i = 0; i < fields.length;i++) {
			Field field = fields[i];
			if (Modifier.isPublic(field.getModifiers()) &&(field.getDeclaringClass().getName().equals(parameterClass.getName())) ||
					(isTextAccessor && field.getDeclaringClass() != boolean.class))	{
				fieldNames.addObject(field.getName());
			}
		}
		return fieldNames;
	}
	private static boolean hasReadAccessor(NSArray methods,String readAccessorName) {
		java.util.Enumeration e = methods.objectEnumerator();
		while (e.hasMoreElements()) {
			Method method = (Method)e.nextElement();
			String methodName = method.getName();
			if (methodName.equals("is" + readAccessorName)|| methodName.equals("get" + readAccessorName) || methodName.equals(makeMethodName(readAccessorName))) {
				return true;
			}
		}
		return false;
	}
	private static String makeMethodName(String name) {
		if (name == null || "".equals(name)) {
			return "";
		}
		return name.substring(0,1).toLowerCase() + name.substring(1);
	}
	private static boolean sameClasses(Class class1, Class class2) {
		try {
			Object object1 = class1.newInstance();
			return class2.isInstance(object1);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
	// To suppress same names and order by alphabetical order
	private static NSArray clean(NSArray methodNames) {
		NSMutableArray newNames = new NSMutableArray();
		java.util.Enumeration e = methodNames.objectEnumerator();
		while (e.hasMoreElements()) {
			String method = (String)e.nextElement();
			if (newNames.containsObject(method) == false) {
				newNames.addObject(method);
			}
		}
		try {
			return new NSArray(newNames.sortedArrayUsingComparator(NSComparator.AscendingCaseInsensitiveStringComparator));
		} catch (Exception exc) {
			return new NSArray(newNames);
		}
	}
}
