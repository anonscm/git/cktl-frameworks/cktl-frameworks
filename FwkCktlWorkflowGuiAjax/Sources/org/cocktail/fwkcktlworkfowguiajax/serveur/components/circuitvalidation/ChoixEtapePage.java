package org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.eocontrol.EOEditingContext;

public interface ChoixEtapePage extends WOActionResults {

	public abstract void setChemin(EOChemin chemin);

	public abstract EOChemin getChemin();

	public abstract void setPageAppelante(WOComponent pageAppelante);

	public abstract WOComponent getPageAppelante();
	
	public abstract EOEditingContext getEditingContext();
	
	public abstract void setEditingContext(EOEditingContext editingContext);

}