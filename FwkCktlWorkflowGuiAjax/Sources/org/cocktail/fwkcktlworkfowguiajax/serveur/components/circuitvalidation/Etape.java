package org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

import er.extensions.foundation.ERXProperties;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflowguiajax.components.AWorkflowComponent;
import org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation.CircuitsValidationContexte;
import org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation.DefinitionCircuitsValidation;
import org.cocktail.fwkcktlworkflow.commun.ModeEdition;
import org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation.ChoixEtape;

public class Etape extends AWorkflowComponent {
	
	/** Numéro de série. */
	private static final long serialVersionUID = -2574005760318761728L;
	
	/** Page parente de ce composant. */
	private WOComponent pageParente;
	/** Contexte de circuit de validation. */
	private CircuitsValidationContexte contexteCircuitValidation = new CircuitsValidationContexte();
	/** Est-ce que l'étape est déjà affichée pour ce circuit de validation ? */
	private Boolean dejaAffichee;
	
	/** Mode édition de la fonction (consultation, modification, ...). */
	private ModeEdition modeEdition;
	/** Chemin à l'origine de cette étape. */
	private EOChemin chemin;
	/** L'étape. */
	private EOEtape etape;
	/** Le chemin en cours de parcours. */
	private EOChemin currentChemin;
	/** Index du chemin en cours de parcours. */
	private int currentCheminIndex;
	
	private EOEtape etapeArrivee;

	
    public Etape(WOContext context) {
        super(context);
    }
    
	@Override
	public void pullValuesFromParent() {
		super.pullValuesFromParent();
		setModeEdition((ModeEdition) valueForBinding("modeEdition"));
		setChemin((EOChemin) valueForBinding("chemin"));
		setEtape((EOEtape) valueForBinding("etape"));
		setContexteCircuitValidation((CircuitsValidationContexte) valueForBinding("contexteCircuitValidation"));
		pageParente = (WOComponent) valueForBinding("pageParente");
	}

    
    
    
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return true;
    }

    
    @Override
	public void awake() {
		super.awake();

		dejaAffichee = null;
	}
    
	/**
	 * @return the notesEtudiantContainer
	 */
	public String containerEtapeId() {
		return getComponentId() + "_containerEtapeId";
	}
	
	public WOComponent getPageParente() {
		return pageParente;
	}

	public void setPageParente(WOComponent pageParente) {
		this.pageParente = pageParente;
	}

	public CircuitsValidationContexte getContexteCircuitValidation() {
		return contexteCircuitValidation;
	}

	public void setContexteCircuitValidation(CircuitsValidationContexte contexteCircuitValidation) {
		this.contexteCircuitValidation = contexteCircuitValidation;
	}

	public ModeEdition getModeEdition() {
		return modeEdition;
	}

	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}

	public boolean isModeEditionConsultation() {
		return ModeEdition.CONSULTATION.equals(modeEdition);
	}
	
	public boolean isModeEditionModification() {
		return ModeEdition.MODIFICATION.equals(modeEdition);
	}

	public EOChemin getChemin() {
		return chemin;
	}

	public void setChemin(EOChemin chemin) {
		this.chemin = chemin;
	}

	public EOEtape getEtape() {
		return etape;
	}

	public void setEtape(EOEtape etape) {
		this.etape = etape;
	}

	public EOChemin getCurrentChemin() {
		return currentChemin;
	}

	public void setCurrentChemin(EOChemin currentChemin) {
		this.currentChemin = currentChemin;
	}

	public int getCurrentCheminIndex() {
		return currentCheminIndex;
	}

	public void setCurrentCheminIndex(int currentCheminIndex) {
		this.currentCheminIndex = currentCheminIndex;
	}
	
	/**
	 * @return the etapeArrivee
	 */
	public EOEtape getEtapeArrivee() {
		etapeArrivee = currentChemin.toEtapeArrivee();
		return etapeArrivee;
	}

	/**
	 * @param etapeArrivee the etapeArrivee to set
	 */
	public void setEtapeArrivee(EOEtape etapeArrivee) {
		
	}

	/**
	 * Est-ce la première étape du circuit ?
	 * <p>
	 * Attention : différent de {@link EOEtape#isInitiale()} car une étape initiale est forcément première étape du circuit mais peut apparaitre ailleurs sur le circuit.
	 * 
	 * @return <code>true</code> si c'est la première étape du circuit
	 */
	public boolean isPremiereEtape() {
		if (chemin == null) {
			contexteCircuitValidation.initialiser();
			return true;
		}
		
		return false;
	}
	
	public boolean isEtapeModifiable() {
		return isModeEditionModification() && chemin != null;
	}
	
	public boolean isDernierChemin() {
		return etape.toChemins().size() - 1 == currentCheminIndex;
	}

	/**
	 * Est-ce que cette étape est déjà affichée dans le circuit ?
	 * <p>
	 * Accède et met à jour le {@link #contexteCircuitValidation contexte du circuit de validation} pour connaitre cette information.
	 * 
	 * @return <code>true</code> si cette étape est déjà affichée dans le circuit
	 */
	public boolean isDejaAffichee() {
		if (etape.isFinale()) {
			dejaAffichee = false;
		} else {
			dejaAffichee = contexteCircuitValidation.isEtapeDejaAffichee(etape, chemin);
		}
		
		contexteCircuitValidation.ajouterEtape(etape, chemin);
		
		return dejaAffichee;
	}
	
	/**
	 * Changer l'étape d'arrivée du chemin à l'origine de cette étape.
	 * <p>
	 * Appel de la page "Choix de l'étape".
	 * 
	 * @return La page "Choix de l'étape"
	 */
	public WOActionResults changerAction() {
//		ChoixEtape choixEtape = (ChoixEtape) pageWithName(ChoixEtape.class.getName());
//		choixEtape.setChemin(chemin);
//		choixEtape.setPageAppelante(pageParente);
		ChoixEtapePage choixEtapePage = (ChoixEtapePage) pageWithName(ERXProperties.stringForKey("CHOIX_ETAPE_PAGE"));
		choixEtapePage.setChemin(chemin);
		choixEtapePage.setEditingContext(edc());
		choixEtapePage.setPageAppelante(context().page());
		
		return choixEtapePage;
	}
	
	
	
	/**
	 * Supprimer le lien vers l'étape d'arrivée du chemin à l'origine de cette étape.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults supprimerAction() {
		contexteCircuitValidation.supprimerEtape(chemin.toEtapeArrivee());
		chemin.setToEtapeArriveeRelationship(null);
		
		return doNothing();
	}
    
}