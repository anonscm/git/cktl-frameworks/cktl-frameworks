package org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

import er.ajax.CktlAjaxUtils;

import org.cocktail.fwkcktlajaxwebext.serveur.CocktailAjaxSession;
import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktlworkflow.commun.ModeEdition;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOCircuitValidation;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationHelper;
import org.cocktail.fwkcktlworkflow.validation.CircuitValidationTechniqueException;
import org.cocktail.fwkcktlworkflowguiajax.components.AWorkflowComponent;
import org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation.CircuitsValidationContexte;

/**
 * 
 * @author isabelle
 *
 */
public class DefinitionCircuitsValidation extends AWorkflowComponent {
	
	/** Numéro de série. */
	private static final long serialVersionUID = -2574005760318761728L;
	
	/**
	 * Un contexte de circuit de validation.
	 * <p>
	 * Permet de conserver des informations, par exemple pour contrôler l'arborescence du circuit.
	 */
	private CircuitsValidationContexte contexteCircuitValidation = new CircuitsValidationContexte();
	
	
	private ModeEdition modeEdition;
    /** Liste des circuits de validation. */
	private NSArray<EOCircuitValidation> listeCircuitsValidation;
    /** Liste des versions du circuit de validation. */
	private NSArray<EOCircuitValidation> listeVersionCircuitsValidation;
	/** Le circuit de validation en cours de parcours dans la liste {@link #listeCircuitsValidation}. */
	private EOCircuitValidation currentCircuit;
	/** Le circuit de validation en cours de parcours dans la liste {@link #listeVersionCircuitsValidation}. */
	private EOCircuitValidation currentVersionCircuit;
	/** Le circuit sélectionné. */
	private EOCircuitValidation circuit;
	/** Le circuit sélectionné. */
	private EOCircuitValidation versionCircuit;
	/** Le circuit sélectionné précédent. */
	private EOCircuitValidation versionCircuitPrecedent;
	/** Etape initiale du circuit. */
	private EOEtape etapeInitiale;
	/** Est-ce que la version du circuit en cours en active ? */
	private Boolean versionActive;

	private EOEditingContext editingContext;
	
	private boolean hasDroitModification;
	private String appId;
	private Integer utilisateurPersId;
	
	
	
    public DefinitionCircuitsValidation(WOContext context) {
        super(context);

  
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return true;
    }


	

	/**
	 * @return the editingContext
	 */
	public EOEditingContext getEditingContext() {
		if (hasBinding(BINDING_editingContext)) {
			editingContext= (EOEditingContext) valueForBinding(BINDING_editingContext);
		} else {
			editingContext = mySession().defaultEditingContext();
		}
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @return the hasDroitModification
	 */
	public boolean isHasDroitModification() {
		hasDroitModification = (booleanValueForBinding(BINDING_hasDroitModification, true));
		return hasDroitModification;
	}

	/**
	 * @param hasDroitModification the hasDroitModification to set
	 */
	public void setHasDroitModification(boolean hasDroitModification) {
		this.hasDroitModification = hasDroitModification;
	}

	/**
	 * @return the appId
	 */
	public String getAppId() {
		if (appId == null) {
			appId =  (String) valueForBinding(BINDING_applicationId);
		}
		return appId;
	}

	/**
	 * @param appId the appId to set
	 */
	public void setAppId(String appId) {
		this.appId = appId;
	}
    
	
	/**
	 * @return the persId
	 */
	public Integer getPersId() {
		return utilisateurPersId;
	}

	/**
	 * Retourne <code>this</code>.
	 * 
	 * @return <code>this</code>
	 */
	public WOComponent moi() {
		return context().page();
	}

	/**
	 * Initialisation du contexte du circuit de validation sur le {@link #awake()}.
	 */
	@Override
	public void awake() {
		super.awake();
		
		contexteCircuitValidation.initialiser();
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlWorkflowGuiAjax.framework", "css/circuitValidation.css");
		
		
	}
	
	/**
	 * Retourne le contexte du circuit de validation.
	 * 
	 * @return Le contexte du circuit de validation
	 */
	public CircuitsValidationContexte getContexteCircuitValidation() {
		return contexteCircuitValidation;
	}

	/**
	 * Retourne le mode en cours du composant (CONSULTATION, MODIFICATION, ...)
	 * 
	 * @return Le mode en cours du composant
	 */
	public ModeEdition modeEdition() {
		return modeEdition;
	}
	
	/**
	 * Affecte le mode en cours du composant (CONSULTATION, MODIFICATION, ...)
	 * 
	 * @param modeEdition Le mode d'édition
	 */
	public void setModeEdition(ModeEdition modeEdition) {
		this.modeEdition = modeEdition;
	}

	/**
	 * Est-ce que le composant est en mode "Consultation" ?
	 * 
	 * @return <code>true</code> si le composant est en mode "Consultation"
	 */
	public boolean isModeEditionConsultation() {
		return ModeEdition.CONSULTATION.equals(modeEdition);
	}
	
	/**
	 * Est-ce que le composant est en mode "Modification" ?
	 * 
	 * @return <code>true</code> si le composant est en mode "Modification"
	 */
	public boolean isModeEditionModification() {
		return ModeEdition.MODIFICATION.equals(modeEdition);
	}

	/**
	 * Retourne le mode d'édition pour les étapes du circuit.
	 * <p>
	 * On ne peut pas modifier un circuit qui a été utilisés.
	 * 
	 * @return Le {@link ModeEdition mode d'édition}
	 */
	public ModeEdition modeEditionEtapes() {
		return (versionCircuit.hasEuDemandes() ? ModeEdition.CONSULTATION : modeEdition());
	}
	
	/**
	 * Retourne la liste des circuits de validation.
	 * 
	 * @return La liste des circuits de validation
	 */
	public NSArray<EOCircuitValidation> listeCircuitsValidation() {
		if (listeCircuitsValidation == null) {
			listeCircuitsValidation = EOCircuitValidation.rechercherCircuitsValidation(getEditingContext(), getAppId());
	        
	        if (listeCircuitsValidation.size() > 0) {
	        	setCircuit(listeCircuitsValidation.get(0));
	        }

	        modeEdition = ModeEdition.CONSULTATION;
		}
		return listeCircuitsValidation;
	}

	/**
	 * Affecte la liste des circuits de validation.
	 * 
	 * @param listeCircuitsValidation Une liste des circuits de validation
	 */
	public void setListeCircuitsValidation(NSArray<EOCircuitValidation> listeCircuitsValidation) {
		this.listeCircuitsValidation = listeCircuitsValidation;
	}
	
	/**
	 * Retourne la liste des versions du circuits de validation sélectionné.
	 * 
	 * @return La liste des versions du circuits de validation sélectionné
	 */
	public NSArray<EOCircuitValidation> listeVersionCircuitsValidation() {
		return listeVersionCircuitsValidation;
	}

	/**
	 * Affecte la liste des versions du circuits de validation sélectionné.
	 * 
	 * @param listeVersionCircuitsValidation Une liste de version de circuits de validation
	 */
	public void setListeVersionCircuitsValidation(NSArray<EOCircuitValidation> listeVersionCircuitsValidation) {
		this.listeVersionCircuitsValidation = listeVersionCircuitsValidation;
	}
	
	/**
	 * Retourne le circuit de validation en cours de parcours dans la liste {@link #listeCircuitsValidation}.
	 * 
	 * @return Le circuit de validation en cours de parcours dans la liste {@link #listeCircuitsValidation}
	 */
	public EOCircuitValidation currentCircuit() {
		return currentCircuit;
	}

	/**
	 * Retourne le circuit de validation en cours de parcours dans la liste {@link #listeCircuitsValidation}.
	 * 
	 * @param currentCircuit Un circuit de validation de la liste {@link #listeCircuitsValidation}
	 */
	public void setCurrentCircuit(EOCircuitValidation currentCircuit) {
		this.currentCircuit = currentCircuit;
	}

	/**
	 * Retourne le circuit de validation en cours de parcours dans la liste {@link #listeVersionCircuitsValidation}.
	 * 
	 * @return Le circuit de validation en cours de parcours dans la liste {@link #listeVersionCircuitsValidation}
	 */
	public EOCircuitValidation currentVersionCircuit() {
		return currentVersionCircuit;
	}

	/**
	 * Retourne le circuit de validation en cours de parcours dans la liste {@link #listeVersionCircuitsValidation}.
	 * 
	 * @param currentVersionCircuit Un circuit de validation de la liste {@link #listeVersionCircuitsValidation}
	 */
	public void setCurrentVersionCircuit(EOCircuitValidation currentVersionCircuit) {
		this.currentVersionCircuit = currentVersionCircuit;
	}

	/**
	 * Retourne le circuit sélectionné de la liste {@link #listeCircuitsValidation}.
	 * 
	 * @return Le circuit sélectionné de la liste {@link #listeCircuitsValidation}
	 */
	public EOCircuitValidation getCircuit() {
		return circuit;
	}

	/**
	 * Affecte le circuit sélectionné de la liste {@link #listeCircuitsValidation}.
	 * 
	 * @param circuit Un circuit de la liste {@link #listeCircuitsValidation}
	 */
	public void setCircuit(EOCircuitValidation circuit) {
		this.circuit = circuit;
		
		setListeVersionCircuitsValidation(EOCircuitValidation.rechercherCircuitsValidation(circuit.editingContext(), getAppId(), circuit.codeCircuitValidation()));

		for (EOCircuitValidation uneVersionCircuit : listeVersionCircuitsValidation) {
			if (uneVersionCircuit.isUtilisable()) {
				setVersionCircuit(uneVersionCircuit);
				break;
			}
		}
		
		setEtapeInitiale(null);
	}

	/**
	 * Retourne le circuit sélectionné de la liste {@link #listeVersionCircuitsValidation}.
	 * 
	 * @return Le circuit sélectionné de la liste {@link #listeVersionCircuitsValidation}
	 */
	public EOCircuitValidation getVersionCircuit() {
		return versionCircuit;
	}

	/**
	 * Affecte le circuit sélectionné de la liste {@link #listeVersionCircuitsValidation}.
	 * 
	 * @param versionCircuit Un circuit de la liste {@link #listeVersionCircuitsValidation}
	 */
	public void setVersionCircuit(EOCircuitValidation versionCircuit) {
		// Si la version du circuit ne correspond pas au circuit en cours, on force ce circuit
		if (!versionCircuit.codeCircuitValidation().equals(circuit.codeCircuitValidation())) {
			for (EOCircuitValidation unCircuit : listeCircuitsValidation) {
				if (unCircuit.codeCircuitValidation().equals(versionCircuit.codeCircuitValidation())) {
					setCircuit(unCircuit);
					break;
				}
			}
		}
		
		this.versionCircuit = versionCircuit;
		setEtapeInitiale(null);
	}

	/**
	 * Retourne le texte affiché dans la combo-box de la liste des version du circuit de validation sélectionné.
	 * 
	 * @return Le texte affiché dans la combo-box de la liste des version du circuit de validation sélectionné
	 */
	public String texteVersion() {
		return currentVersionCircuit.texteVersion();
	}
	
	public boolean isBoutonModifierDisabled() {
		return (isModeEditionModification() || versionCircuit.isUtilisable() /*versionCircuit.hasEuDemandes()*/);
	}
	
	/**
	 * Retourne l'étape initiale de la version du circuit de validation sélectionné.
	 * 
	 * @return L'étape initiale de la version du circuit de validation sélectionné
	 */
	public EOEtape getEtapeInitiale() {
		if (etapeInitiale == null && versionCircuit != null) {
			etapeInitiale = versionCircuit.etapeInitiale();
		}
		
		return etapeInitiale;
	}

	/**
	 * Affecte l'étape initiale de la version du circuit de validation sélectionné
	 * 
	 * @param etapeInitiale L'étape initiale de la version du circuit de validation sélectionné
	 */
	public void setEtapeInitiale(EOEtape etapeInitiale) {
		this.etapeInitiale = etapeInitiale;
	}

	/**
	 * Est-ce que la version affichées du circuit est active (Utilisable) ?
	 * 
	 * @return <code>true</code> si active
	 */
	public boolean isVersionActive() {
		if (versionActive == null) {
			return versionCircuit.isUtilisable();
		}
		
		return versionActive;
	}
	
	/**
	 * Affecte le témoin "active" à la version du circuit.
	 * 
	 * @param active "<code>true</code>" pour active (vient d'une checkbox)
	 */
	public void setVersionActive(String active) {
		versionActive = "true".equals(active);
	}
	
	/**
	 * Est-ce que la checkbox "Version active" est disable ?
	 * 
	 * @return <code>true</code> pour disable
	 */
	public boolean isCheckboxVersionActiveDisabled() {
		return (isModeEditionConsultation() || versionCircuit.isUtilisable());
	}
	
	/**
	 * Passer le composant en {@link ModeEdition#MODIFICATION} sur le circuit de validation sélectionné.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults modifierAction() {
		modeEdition = ModeEdition.MODIFICATION;
		
		return null;
	}

	/**
	 * Duplique la version en cours du circuit et passe en modification sur cette version dupliquée.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults dupliquerVersionAction() {
		EOCircuitValidation nouvelleVersion = EOCircuitValidation.dupliquerCircuitValidation(versionCircuit, getPersId());
		
		versionCircuitPrecedent = versionCircuit;
		listeVersionCircuitsValidation.add(nouvelleVersion);
		setVersionCircuit(nouvelleVersion);
		
		modeEdition = ModeEdition.MODIFICATION;
		
		return null;
	}
	
	/**
	 * Générer un aperçu du circuit de validation sélectionné.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults apercuAction() {
		// TODO
		return null;
	}

	/**
	 * Valider les modifications en base et passer le composant en mode {@link ModeEdition#CONSULTATION}.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults validerAction() {
		CircuitValidationHelper circuitValidationHelper = new CircuitValidationHelper();
		circuitValidationHelper.controlerEtNormaliser(versionCircuit);
		
		if (circuitValidationHelper.getListeMessages().size() != 0) {
			((CocktailAjaxSession)session()).addSimpleErrorMessage(versionCircuit.codeCircuitValidation(), circuitValidationHelper.getListeMessages().get(0));
		} else {
			
			// Si c'est une nouvelle version du circuit, on affecte son numéro de version
			if (versionCircuit.numeroVersion() == null) {
				versionCircuit.setNumeroVersion(versionCircuit.rechercherNumeroVersionSuivant());
			}
			
			// Si cette version est rendue "active"
			if (versionActive) {
				versionCircuit.rendreUtilisable();
			}
			
			versionCircuit.editingContext().saveChanges();
			modeEdition = ModeEdition.CONSULTATION;
			versionActive = null;
		}
		
		return null;
	}

	/**
	 * Annuler les modifications et passer le composant en mode {@link ModeEdition#CONSULTATION}.
	 * 
	 * @return <code>null</code>
	 */
	public WOActionResults annulerAction() {
		versionCircuit.editingContext().revert();
		modeEdition = ModeEdition.CONSULTATION;
		versionActive = null;
		
		if (versionCircuit.numeroVersion() == null) {
			listeVersionCircuitsValidation.remove(versionCircuit);
		}
		
		if (versionCircuitPrecedent != null) {
			setVersionCircuit(versionCircuitPrecedent);
			versionCircuitPrecedent = null;
		}
		
		return null;
	}
	
	public Integer getUtilisateurPersId() {
		return utilisateurPersId;
	}

	public void setUtilisateurPersId(Integer utilisateurPersId) {
		this.utilisateurPersId = utilisateurPersId;
	}
	
}