package org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;

/**
 * Contexte d'un circuit de validation.
 * <p>
 * Classe utilitaire pour le parcours de l'arborescence des étapes d'un circuit de validation.
 * 
 * @author Pascal MACOUIN
 */
public class CircuitsValidationContexte {
	private Map<String, EOChemin> etapesDejaAffichees = new HashMap<String, EOChemin>();
	
	/**
	 * Initialise le contexte.
	 */
	public void initialiser() {
		etapesDejaAffichees.clear();
	}
	
	/**
	 * Ajouter une étape comme déjà parcourue.
	 * 
	 * @param etape Une étape du circuit de validation
	 * @param cheminOrigine Le chemin à l'origine de cette étape (est <code>null</code> pour la première étape du circuit)
	 */
	public void ajouterEtape(EOEtape etape, EOChemin cheminOrigine) {
		if (etape != null) {
			if (!etapesDejaAffichees.containsKey(etape.codeEtape())) {
				etapesDejaAffichees.put(etape.codeEtape(), cheminOrigine);
			}
		}
	}
	
	/**
	 * Est-ce que l'étape a déjà été parcouru ?
	 * 
	 * @param etape Une étape du circuit de validation
	 * @param cheminOrigine Le chemin à l'origine de cette étape (est <code>null</code> pour la première étape du circuit)
	 * @return <code>true</code> si l'étape a déjà été parcouru
	 */
	public boolean isEtapeDejaAffichee(EOEtape etape, EOChemin cheminOrigine) {
		boolean resultat = false;
		
		if (etape != null && cheminOrigine != null) {
			boolean cheminExist = etapesDejaAffichees.containsKey(etape.codeEtape());
			EOChemin cheminExistant = etapesDejaAffichees.get(etape.codeEtape());
			
			if (!cheminExist || cheminExistant == cheminOrigine) {
				resultat = false;
			} else {
				resultat = true;
			}
		}
		
		return resultat;
	}
	
	/**
	 * Ajouter une étape comme déjà parcourue.
	 * 
	 * @param etape Une étape du circuit de validation
	 */
	public void supprimerEtape(EOEtape etape) {
		if (etape != null) {
			if (etapesDejaAffichees.containsKey(etape.codeEtape())) {
				etapesDejaAffichees.remove(etape.codeEtape());
			}
		}
	}
	

}
