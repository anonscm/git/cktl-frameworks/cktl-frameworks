package org.cocktail.fwkcktlworkfowguiajax.serveur.components.circuitvalidation;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxUpdateContainer;
import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXDatabaseDataSource;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOChemin;
import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;
import org.cocktail.fwkcktlworkflowguiajax.components.AWorkflowComponent;

public class ChoixEtape extends AWorkflowComponent {
    
	private static final long serialVersionUID = 4666692343819926862L;

	private static final Integer NUMBER_OF_OBJECT_PER_BATCH = 15;

	
	/** 
	 * Page appelante de cette page.
	 * <p>
	 * C'est la page qui sera rappelée à la fermeture de celle-ci.
	 */
	private WOComponent pageAppelante;
	/** Le chemin à modifier. */
	private EOChemin chemin;

	private EOEditingContext editingContext;

	// Table de recherche des étapes
	private ERXDisplayGroup<EOEtape> displayGroupEtape = null;
	private EOEtape currentEtape;
	private EOEtape selectedEtape = null;
	private ERXDatabaseDataSource etapeDatasource = null;
	
	/**
	 * Constructeur
	 * 
	 * @param context Un contexte WO
	 */
    public ChoixEtape(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return true;
    }

    public void setPageAppelante(WOComponent pageAppelante) {
		this.pageAppelante = pageAppelante;
	}

    /**
	 * @return the chemin
	 */
	public EOChemin getChemin() {
		return chemin;
	}

	/**
	 * @param chemin the chemin to set
	 */
	public void setChemin(EOChemin chemin) {
		this.chemin = chemin;
	}

	/**
	 * @return the editingContext
	 */
	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * @return : ongletEValuationsId.
	 */
	public String choixEtapeContainerId() {
		return getComponentId() + "_choixEtapeId";
	}

	public String choixEtapeTableViewId() {
		return getComponentId() + "choixEtapeTableViewId";
	}


	/**
	 * @return the preEtapeDatasource
	 */
	public ERXDatabaseDataSource getEtapeDatasource() {
		return etapeDatasource;
	}

	/**
	 * @param preEtapeDatasource the preEtapeDatasource to set
	 */
	public void setEtapeDatasource(ERXDatabaseDataSource etapeDatasource) {
		this.etapeDatasource = etapeDatasource;
	}

	/**
	 * @return the displayGroupEtape
	 */
	public ERXDisplayGroup<EOEtape> getDisplayGroupEtape() {
		if (displayGroupEtape == null) {
		  	NSArray<EOEtape> listeEtapes = getChemin().toEtapeDepart().toCircuitValidation().toEtapes();
	    	// On enlève l'étape d'arrivée et l'étape de départ
	    	NSMutableArray<EOEtape> listeEtapesAffichees = new NSMutableArray<EOEtape>();
	    	for (EOEtape uneEtape : listeEtapes) {
				if (uneEtape !=  getChemin().toEtapeArrivee() && uneEtape !=  getChemin().toEtapeDepart()) {
					listeEtapesAffichees.add(uneEtape);
				}
			}
			
			displayGroupEtape = new ERXDisplayGroup<EOEtape>();
			EOArrayDataSource dataSource = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOEtape.class), getEditingContext());
			dataSource.setArray(listeEtapesAffichees);
			
			displayGroupEtape.setDataSource(dataSource);
			displayGroupEtape.setDelegate(new DisplayGroupEtapeDelegate());

			displayGroupEtape.setSelectsFirstObjectAfterFetch(true);
			displayGroupEtape.setNumberOfObjectsPerBatch(NUMBER_OF_OBJECT_PER_BATCH);
			displayGroupEtape.fetch();
		}

		return displayGroupEtape;
	}


	/**
	 * @param displayGroupEtape the displayGroupEtape to set
	 */
	public void setDisplayGroupEtape(
			ERXDisplayGroup<EOEtape> displayGroupEtape) {
		this.displayGroupEtape = displayGroupEtape;
	}
	

	/**
	 * @return the currentEtape
	 */
	public EOEtape getCurrentEtape() {
		return currentEtape;
	}


	/**
	 * @param currentEtape the currentEtape to set
	 */
	public void setCurrentEtape(EOEtape currentEtape) {
		this.currentEtape = currentEtape;
	}


	/**
	 * @return the selectedEtape
	 */
	public EOEtape getSelectedEtape() {
		return selectedEtape;
	}


	/**
	 * @param selectedEtape the selectedEtape to set
	 */
	public void setSelectedEtape(EOEtape selectedEtape) {
		this.selectedEtape = selectedEtape;
	}
	
	public boolean isSelectedEtape() {
		return (getSelectedEtape() == null);
	}
  
	/**
	 * mise à jour de l'interface
	 * @return null (reste sur la page)
	 */
	public WOActionResults update() {
		AjaxUpdateContainer.updateContainerWithID(choixEtapeContainerId(), context());
		return doNothing();
	}


	/**
	 * delegate class handle the current selected item
	 */
	public class DisplayGroupEtapeDelegate {

		/**
		 * @param group : groupe d'élement sélectionné
		 */
		public void displayGroupDidChangeSelectedObjects(final WODisplayGroup group) {
			@SuppressWarnings("unchecked")
			ERXDisplayGroup<EOEtape> groupe = (ERXDisplayGroup<EOEtape>) group;
			if (groupe.selectedObject() != null) {
				setSelectedEtape(groupe.selectedObject());
			} else {
				setSelectedEtape(null);
			}
		}
	}
	
	
	/**
	 * Annuler la sélection de l'étape et retourner à la page précédente.
	 * 
	 * @return La page précédente
	 */
	public WOActionResults annulerAction() {
		return pageAppelante;
	}

	/**
	 * Changer l'étape d'arrivée du chemin par l'étape sélectionné et retourner à la page précédente.
	 * 
	 * @return La page précédente
	 */
	public WOActionResults selectionnerAction() {
		// Si rien n'est sélectionné, on reste sur l'écran
		if (getSelectedEtape() == null) {
			return null;
		}
		
		EOEtape etapeSelectionnee = getSelectedEtape();
		
		// Recherche de l'étape dans l'ec d'origine
		for (EOEtape uneEtape : chemin.toEtapeDepart().toCircuitValidation().toEtapes()) {
			if (uneEtape.codeEtape().equals(etapeSelectionnee.codeEtape())) {
				chemin.setToEtapeArriveeRelationship(uneEtape);
				break;
			}
		}
		
		return pageAppelante;
	}
	
	public WOComponent getPageAppelante() {
		return pageAppelante;
	}
	
}