package org.cocktail.fwkcktlworkflowguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSPathUtilities;


public class AWorkflowComponent extends CktlAjaxWOComponent{
	
	private static final long serialVersionUID = 1L;
	public final static String BINDING_utilisateurPersId = "utilisateurPersId";
	public static final String BINDING_editingContext = "editingContext";
	public static final String BINDING_applicationId ="appId";
	public static final String BINDING_hasDroitModification ="hasDroitModification";
	
	public static final String BINDING_updateContainerID = "updateContainerID";

	private String onloadJS;  

	
	public AWorkflowComponent(WOContext context) {
		super(context);
	}
	
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}


	/**
	 * @return Le binding <i>editingContext</i> s'il est renseigne, sinon l'editingContext de la session. Cette methode peut etre surchargee pour
	 *         renvoyer un nestedEditingContext.
	 */
	@Override
	public EOEditingContext edc() {
		if (hasBinding(BINDING_editingContext)) {
			return (EOEditingContext) valueForBinding(BINDING_editingContext);
		}
		return mySession().defaultEditingContext();
	}

	/**
	 * @return la valeur du binding utilisateurPersId.
	 */
	public Integer getUtilisateurPersId() {
		if (valueForBinding(BINDING_utilisateurPersId) == null) {
			System.err.println("**** Le binding utilisateurPersId n'est pas renseigné pour le composant " + name());
		}
		return (Integer) valueForBinding(BINDING_utilisateurPersId);
	}
	
	public String getUpdateContainerID() {
		return (String) valueForBinding(BINDING_updateContainerID);
	}
	
	public String getAppId() {
		return (String) valueForBinding(BINDING_applicationId);
		
	}


	/**
	 * @return the onloadJS
	 */
	public String onloadJS() {
		return onloadJS;
	}

	/**
	 * @param onloadJS the onloadJS to set
	 */
	public void setOnloadJS(String onloadJS) {
		this.onloadJS = onloadJS;
	}

}
