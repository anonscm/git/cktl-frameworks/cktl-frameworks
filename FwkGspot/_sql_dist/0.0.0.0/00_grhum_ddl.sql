-- Saisir ici vos instructions ddl (executables a partir de grhum ou d'un user dba)
--changement de user pour la table Type_occupation
CREATE TABLE "GRHUM"."SALLE_TYPE_OCCUPATION"
  (
    "SAL_NUMERO" NUMBER NOT NULL ENABLE,
    "TOC_ORDRE"  NUMBER NOT NULL ENABLE,
    CONSTRAINT "PK_SALLE_TYPE_OCC" PRIMARY KEY ("SAL_NUMERO", "TOC_ORDRE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 16384 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "GRH_INDX" ENABLE,
    CONSTRAINT "KF_SALLE" FOREIGN KEY ("SAL_NUMERO") REFERENCES "GRHUM"."SALLES" ("SAL_NUMERO") ENABLE,
    CONSTRAINT "FK_TYPE_OCCUPATION" FOREIGN KEY ("TOC_ORDRE") REFERENCES "GRHUM"."TYPE_OCCUPATION" ("TOC_ORDRE") ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 24576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "GRH" ;

 CREATE TABLE "GRHUM"."TYPE_OCCUPATION"
  (
    "TOC_ORDRE"   NUMBER NOT NULL ENABLE,
    "TOC_LIBELLE" VARCHAR2(20 BYTE),
    CONSTRAINT "PK_TYPE_OCCUPATION" PRIMARY KEY ("TOC_ORDRE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 24576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "GRH_INDX" ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 16384 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "GRH" ;
CREATE INDEX "GRHUM"."TOC01" ON "GRHUM"."TYPE_OCCUPATION"
  (
    "TOC_ORDRE"
  )
  PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE
  (
    INITIAL 24576 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "GRH_INDX" ;
  
  CREATE TABLE "GRHUM"."DETAIL_POURCENTAGE"
  (
    "SAL_NUMERO"      NUMBER,
    "C_STRUCTURE"     VARCHAR2(10 BYTE),
    "DET_POURCENTAGE" NUMBER(12,2),
    "TOC_ORDRE"       NUMBER,
    CONSTRAINT "PK_DETAIL_POURCENTAGE" PRIMARY KEY ("SAL_NUMERO", "C_STRUCTURE", "TOC_ORDRE") USING INDEX PCTFREE 10 INITRANS 2 MAXTRANS 255 STORAGE(INITIAL 65536 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT) TABLESPACE "GRH" ENABLE,
    CONSTRAINT "FK_SALLE_PCT" FOREIGN KEY ("SAL_NUMERO") REFERENCES "GRHUM"."SALLES" ("SAL_NUMERO") ENABLE,
    CONSTRAINT "FK_TYPE_OCCUPATION_PCT" FOREIGN KEY ("TOC_ORDRE") REFERENCES "GRHUM"."TYPE_OCCUPATION" ("TOC_ORDRE") ENABLE,
    CONSTRAINT "FK_STRUCTURE_PCT" FOREIGN KEY ("C_STRUCTURE") REFERENCES "GRHUM"."STRUCTURE_ULR" ("C_STRUCTURE") ENABLE
  )
  PCTFREE 10 PCTUSED 40 INITRANS 1 MAXTRANS 255 NOCOMPRESS LOGGING STORAGE
  (
    INITIAL 81920 NEXT 1048576 MINEXTENTS 1 MAXEXTENTS 2147483645 PCTINCREASE 0 FREELISTS 1 FREELIST GROUPS 1 BUFFER_POOL DEFAULT
  )
  TABLESPACE "GRH" ;