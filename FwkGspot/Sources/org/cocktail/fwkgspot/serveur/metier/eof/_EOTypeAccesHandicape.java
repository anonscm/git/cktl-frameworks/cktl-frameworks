// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeAccesHandicape.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeAccesHandicape extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_TypeAccesHandicape";

	// Attributes
	public static final String LIBELLE_KEY = "libelle";

	// Relationships
	public static final String TO_FWK_GSPOT__REPART_SALLE_ACCES_HANDICAPES_KEY = "toFwkGspot_RepartSalleAccesHandicapes";

  private static Logger LOG = Logger.getLogger(_EOTypeAccesHandicape.class);

  public EOTypeAccesHandicape localInstanceIn(EOEditingContext editingContext) {
    EOTypeAccesHandicape localInstance = (EOTypeAccesHandicape)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String libelle() {
    return (String) storedValueForKey("libelle");
  }

  public void setLibelle(String value) {
    if (_EOTypeAccesHandicape.LOG.isDebugEnabled()) {
    	_EOTypeAccesHandicape.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, "libelle");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toFwkGspot_RepartSalleAccesHandicapes() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)storedValueForKey("toFwkGspot_RepartSalleAccesHandicapes");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toFwkGspot_RepartSalleAccesHandicapes(EOQualifier qualifier) {
    return toFwkGspot_RepartSalleAccesHandicapes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toFwkGspot_RepartSalleAccesHandicapes(EOQualifier qualifier, boolean fetch) {
    return toFwkGspot_RepartSalleAccesHandicapes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toFwkGspot_RepartSalleAccesHandicapes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicapes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toFwkGspot_RepartSalleAccesHandicapes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToFwkGspot_RepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    if (_EOTypeAccesHandicape.LOG.isDebugEnabled()) {
      _EOTypeAccesHandicape.LOG.debug("adding " + object + " to toFwkGspot_RepartSalleAccesHandicapes relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toFwkGspot_RepartSalleAccesHandicapes");
  }

  public void removeFromToFwkGspot_RepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    if (_EOTypeAccesHandicape.LOG.isDebugEnabled()) {
      _EOTypeAccesHandicape.LOG.debug("removing " + object + " from toFwkGspot_RepartSalleAccesHandicapes relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkGspot_RepartSalleAccesHandicapes");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape createToFwkGspot_RepartSalleAccesHandicapesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartSalleAccesHandicape");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toFwkGspot_RepartSalleAccesHandicapes");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape) eo;
  }

  public void deleteToFwkGspot_RepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toFwkGspot_RepartSalleAccesHandicapes");
    editingContext().deleteObject(object);
  }

  public void deleteAllToFwkGspot_RepartSalleAccesHandicapesRelationships() {
    Enumeration objects = toFwkGspot_RepartSalleAccesHandicapes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToFwkGspot_RepartSalleAccesHandicapesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape)objects.nextElement());
    }
  }


  public static EOTypeAccesHandicape createFwkGspot_TypeAccesHandicape(EOEditingContext editingContext, String libelle
) {
    EOTypeAccesHandicape eo = (EOTypeAccesHandicape) EOUtilities.createAndInsertInstance(editingContext, _EOTypeAccesHandicape.ENTITY_NAME);    
		eo.setLibelle(libelle);
    return eo;
  }

  public static NSArray<EOTypeAccesHandicape> fetchAllFwkGspot_TypeAccesHandicapes(EOEditingContext editingContext) {
    return _EOTypeAccesHandicape.fetchAllFwkGspot_TypeAccesHandicapes(editingContext, null);
  }

  public static NSArray<EOTypeAccesHandicape> fetchAllFwkGspot_TypeAccesHandicapes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeAccesHandicape.fetchFwkGspot_TypeAccesHandicapes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeAccesHandicape> fetchFwkGspot_TypeAccesHandicapes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeAccesHandicape.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeAccesHandicape> eoObjects = (NSArray<EOTypeAccesHandicape>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeAccesHandicape fetchFwkGspot_TypeAccesHandicape(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAccesHandicape.fetchFwkGspot_TypeAccesHandicape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAccesHandicape fetchFwkGspot_TypeAccesHandicape(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeAccesHandicape> eoObjects = _EOTypeAccesHandicape.fetchFwkGspot_TypeAccesHandicapes(editingContext, qualifier, null);
    EOTypeAccesHandicape eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeAccesHandicape)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_TypeAccesHandicape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAccesHandicape fetchRequiredFwkGspot_TypeAccesHandicape(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeAccesHandicape.fetchRequiredFwkGspot_TypeAccesHandicape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeAccesHandicape fetchRequiredFwkGspot_TypeAccesHandicape(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeAccesHandicape eoObject = _EOTypeAccesHandicape.fetchFwkGspot_TypeAccesHandicape(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_TypeAccesHandicape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeAccesHandicape localInstanceIn(EOEditingContext editingContext, EOTypeAccesHandicape eo) {
    EOTypeAccesHandicape localInstance = (eo == null) ? null : (EOTypeAccesHandicape)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
