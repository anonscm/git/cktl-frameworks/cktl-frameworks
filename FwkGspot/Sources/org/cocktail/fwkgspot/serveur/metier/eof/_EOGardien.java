// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGardien.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGardien extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Gardien";

	// Attributes

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGardien.class);

  public EOGardien localInstanceIn(EOEditingContext editingContext) {
    EOGardien localInstance = (EOGardien)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }


  public static EOGardien createGardien(EOEditingContext editingContext) {
    EOGardien eo = (EOGardien) EOUtilities.createAndInsertInstance(editingContext, _EOGardien.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOGardien> fetchAllGardiens(EOEditingContext editingContext) {
    return _EOGardien.fetchAllGardiens(editingContext, null);
  }

  public static NSArray<EOGardien> fetchAllGardiens(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGardien.fetchGardiens(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGardien> fetchGardiens(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGardien.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGardien> eoObjects = (NSArray<EOGardien>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGardien fetchGardien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGardien.fetchGardien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGardien fetchGardien(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGardien> eoObjects = _EOGardien.fetchGardiens(editingContext, qualifier, null);
    EOGardien eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGardien)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Gardien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGardien fetchRequiredGardien(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGardien.fetchRequiredGardien(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGardien fetchRequiredGardien(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGardien eoObject = _EOGardien.fetchGardien(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Gardien that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGardien localInstanceIn(EOEditingContext editingContext, EOGardien eo) {
    EOGardien localInstance = (eo == null) ? null : (EOGardien)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
