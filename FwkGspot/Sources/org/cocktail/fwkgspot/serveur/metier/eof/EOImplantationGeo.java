package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOImplantationGeo extends _EOImplantationGeo implements IImplantationGeo{
	private static Logger log = Logger.getLogger(EOImplantationGeo.class);
	public static EOQualifier QUAL_VALID_IMPLANTATION = EOQualifier
			.qualifierWithQualifierFormat(EOImplantationGeo.D_DEB_VAL_KEY
					+ " <= %@ AND (" + EOImplantationGeo.D_FIN_VAL_KEY
					+ " = nil OR " + EOImplantationGeo.D_FIN_VAL_KEY
					+ " > %@ )", new NSArray<NSTimestamp>(new NSTimestamp[] {
					new NSTimestamp(), new NSTimestamp() }));

	public static EOImplantationGeo getNewEmptyImplantation(EOEditingContext ec) {
		/*
		 * NSArray<NSDictionary<String, String>> result = EOUtilities
		 * .rawRowsForSQL( ec, "Gspot",
		 * "select to_char(grhum.IMPLANTATION_GEO_SEQ.nextval) as ID from dual",
		 * null); EOImplantationGeo newImp = EOImplantationGeo
		 * .createFwkGspot_ImplantationGeo(ec, (String) result
		 * .lastObject().valueForKey("ID"), new NSTimestamp(), new
		 * NSTimestamp());
		 */
		EOImplantationGeo newImp = EOImplantationGeo
				.createFwkGspot_ImplantationGeo(ec, new NSTimestamp(),
						new NSTimestamp());
		newImp.setDDebVal(new NSTimestamp());

		return newImp;
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

		if ((this.lcImplantationGeo() == null)
				|| ("".equals(this.lcImplantationGeo()))) {
			throw new NSValidation.ValidationException(
					"Le libellé court est obligatoire !");
		}

		if ((this.llImplantationGeo() == null)
				|| ("".equals(this.llImplantationGeo()))) {
			throw new NSValidation.ValidationException(
					"Le libellé est obligatoire !");
		}

		if (this.dDebVal() == null) {
			throw new NSValidation.ValidationException(
					"La date de début de validité est obligatoire !");
		}

	}
}
