// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSalles.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSalles extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_Salles";

	// Attributes
	public static final String D_ANNULATION_KEY = "dAnnulation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String SAL_ACCES_KEY = "salAcces";
	public static final String SAL_BADGEUR_KEY = "salBadgeur";
	public static final String SAL_CAPACITE_KEY = "salCapacite";
	public static final String SAL_CAPA_EXAM_KEY = "salCapaExam";
	public static final String SAL_COORDONNEES_KEY = "salCoordonnees";
	public static final String SAL_DESCRIPTIF_KEY = "salDescriptif";
	public static final String SAL_ECRAN_KEY = "salEcran";
	public static final String SAL_ETAGE_KEY = "salEtage";
	public static final String SAL_IDENT_ACCES_KEY = "salIdentAcces";
	public static final String SAL_INSONORISEE_KEY = "salInsonorisee";
	public static final String SAL_NATURE_KEY = "salNature";
	public static final String SAL_NB_ARMOIRES_KEY = "salNbArmoires";
	public static final String SAL_NB_BUREAUX_KEY = "salNbBureaux";
	public static final String SAL_NB_CHAISES_KEY = "salNbChaises";
	public static final String SAL_NB_FENETRES_KEY = "salNbFenetres";
	public static final String SAL_NB_PLACES_EXAM_LIB_KEY = "salNbPlacesExamLib";
	public static final String SAL_NB_TABLES_KEY = "salNbTables";
	public static final String SAL_NO_POSTE_KEY = "salNoPoste";
	public static final String SAL_NUM_DEPART_KEY = "salNumDepart";
	public static final String SAL_OBSCUR_KEY = "salObscur";
	public static final String SAL_PAS_NUM_KEY = "salPasNum";
	public static final String SAL_PORTE_KEY = "salPorte";
	public static final String SAL_POUR_ADMINISTRATION_KEY = "salPourAdministration";
	public static final String SAL_POUR_DOCUMENTATION_KEY = "salPourDocumentation";
	public static final String SAL_POUR_ENSEIGNEMENT_KEY = "salPourEnseignement";
	public static final String SAL_POUR_RECHERCHE_KEY = "salPourRecherche";
	public static final String SAL_POUR_TECHNIQUE_KEY = "salPourTechnique";
	public static final String SAL_RESERVABLE_KEY = "salReservable";
	public static final String SAL_RETRO_KEY = "salRetro";
	public static final String SAL_SALLE_PROCHE_TEL_KEY = "salSalleProcheTel";
	public static final String SAL_SUPERFICIE_KEY = "salSuperficie";
	public static final String SAL_TABLEAU_KEY = "salTableau";
	public static final String SAL_TABLEAU_BLANC_KEY = "salTableauBlanc";
	public static final String SAL_TABLEAU_PAPIER_KEY = "salTableauPapier";
	public static final String SAL_TELEVISION_KEY = "salTelevision";
	public static final String SAL_TEM_EN_SERVICE_KEY = "salTemEnService";
	public static final String SAL_TEM_HANDICAP_KEY = "salTemHandicap";

	// Relationships
	public static final String TO_DEPOSITAIRE_SALLESS_KEY = "toDepositaireSalless";
	public static final String TO_DETAIL_POURCENTAGES_KEY = "toDetailPourcentages";
	public static final String TO_LOCAL_KEY = "toLocal";
	public static final String TO_PRISESES_KEY = "toPriseses";
	public static final String TO_REPART_BUREAU_KEY = "toRepartBureau";
	public static final String TO_REPART_LOT_SALLES_KEY = "toRepartLotSalles";
	public static final String TO_REPART_SALLE_ACCES_HANDICAPES_KEY = "toRepartSalleAccesHandicapes";
	public static final String TO_RESA_OBJETS_KEY = "toResaObjets";
	public static final String TO_SALLE_PORTES_KEY = "toSallePortes";
	public static final String TO_SALLE_TELEPHONES_KEY = "toSalleTelephones";
	public static final String TO_TYPE_SALLE_KEY = "toTypeSalle";
	public static final String TO_V_ETAGE_LOCAL_KEY = "toVEtageLocal";
	public static final String TO_V_TYPE_SALLE_LOCAL_ETAGE_KEY = "toVTypeSalleLocalEtage";

  private static Logger LOG = Logger.getLogger(_EOSalles.class);

  public EOSalles localInstanceIn(EOEditingContext editingContext) {
    EOSalles localInstance = (EOSalles)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dAnnulation() {
    return (NSTimestamp) storedValueForKey("dAnnulation");
  }

  public void setDAnnulation(NSTimestamp value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating dAnnulation from " + dAnnulation() + " to " + value);
    }
    takeStoredValueForKey(value, "dAnnulation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String salAcces() {
    return (String) storedValueForKey("salAcces");
  }

  public void setSalAcces(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salAcces from " + salAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "salAcces");
  }

  public String salBadgeur() {
    return (String) storedValueForKey("salBadgeur");
  }

  public void setSalBadgeur(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salBadgeur from " + salBadgeur() + " to " + value);
    }
    takeStoredValueForKey(value, "salBadgeur");
  }

  public Double salCapacite() {
    return (Double) storedValueForKey("salCapacite");
  }

  public void setSalCapacite(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salCapacite from " + salCapacite() + " to " + value);
    }
    takeStoredValueForKey(value, "salCapacite");
  }

  public Double salCapaExam() {
    return (Double) storedValueForKey("salCapaExam");
  }

  public void setSalCapaExam(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salCapaExam from " + salCapaExam() + " to " + value);
    }
    takeStoredValueForKey(value, "salCapaExam");
  }

  public String salCoordonnees() {
    return (String) storedValueForKey("salCoordonnees");
  }

  public void setSalCoordonnees(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salCoordonnees from " + salCoordonnees() + " to " + value);
    }
    takeStoredValueForKey(value, "salCoordonnees");
  }

  public String salDescriptif() {
    return (String) storedValueForKey("salDescriptif");
  }

  public void setSalDescriptif(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salDescriptif from " + salDescriptif() + " to " + value);
    }
    takeStoredValueForKey(value, "salDescriptif");
  }

  public String salEcran() {
    return (String) storedValueForKey("salEcran");
  }

  public void setSalEcran(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salEcran from " + salEcran() + " to " + value);
    }
    takeStoredValueForKey(value, "salEcran");
  }

  public String salEtage() {
    return (String) storedValueForKey("salEtage");
  }

  public void setSalEtage(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salEtage from " + salEtage() + " to " + value);
    }
    takeStoredValueForKey(value, "salEtage");
  }

  public String salIdentAcces() {
    return (String) storedValueForKey("salIdentAcces");
  }

  public void setSalIdentAcces(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salIdentAcces from " + salIdentAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "salIdentAcces");
  }

  public String salInsonorisee() {
    return (String) storedValueForKey("salInsonorisee");
  }

  public void setSalInsonorisee(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salInsonorisee from " + salInsonorisee() + " to " + value);
    }
    takeStoredValueForKey(value, "salInsonorisee");
  }

  public String salNature() {
    return (String) storedValueForKey("salNature");
  }

  public void setSalNature(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNature from " + salNature() + " to " + value);
    }
    takeStoredValueForKey(value, "salNature");
  }

  public Double salNbArmoires() {
    return (Double) storedValueForKey("salNbArmoires");
  }

  public void setSalNbArmoires(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbArmoires from " + salNbArmoires() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbArmoires");
  }

  public Double salNbBureaux() {
    return (Double) storedValueForKey("salNbBureaux");
  }

  public void setSalNbBureaux(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbBureaux from " + salNbBureaux() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbBureaux");
  }

  public Double salNbChaises() {
    return (Double) storedValueForKey("salNbChaises");
  }

  public void setSalNbChaises(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbChaises from " + salNbChaises() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbChaises");
  }

  public Double salNbFenetres() {
    return (Double) storedValueForKey("salNbFenetres");
  }

  public void setSalNbFenetres(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbFenetres from " + salNbFenetres() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbFenetres");
  }

  public Double salNbPlacesExamLib() {
    return (Double) storedValueForKey("salNbPlacesExamLib");
  }

  public void setSalNbPlacesExamLib(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbPlacesExamLib from " + salNbPlacesExamLib() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbPlacesExamLib");
  }

  public Double salNbTables() {
    return (Double) storedValueForKey("salNbTables");
  }

  public void setSalNbTables(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNbTables from " + salNbTables() + " to " + value);
    }
    takeStoredValueForKey(value, "salNbTables");
  }

  public String salNoPoste() {
    return (String) storedValueForKey("salNoPoste");
  }

  public void setSalNoPoste(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNoPoste from " + salNoPoste() + " to " + value);
    }
    takeStoredValueForKey(value, "salNoPoste");
  }

  public Double salNumDepart() {
    return (Double) storedValueForKey("salNumDepart");
  }

  public void setSalNumDepart(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salNumDepart from " + salNumDepart() + " to " + value);
    }
    takeStoredValueForKey(value, "salNumDepart");
  }

  public String salObscur() {
    return (String) storedValueForKey("salObscur");
  }

  public void setSalObscur(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salObscur from " + salObscur() + " to " + value);
    }
    takeStoredValueForKey(value, "salObscur");
  }

  public Long salPasNum() {
    return (Long) storedValueForKey("salPasNum");
  }

  public void setSalPasNum(Long value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPasNum from " + salPasNum() + " to " + value);
    }
    takeStoredValueForKey(value, "salPasNum");
  }

  public String salPorte() {
    return (String) storedValueForKey("salPorte");
  }

  public void setSalPorte(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPorte from " + salPorte() + " to " + value);
    }
    takeStoredValueForKey(value, "salPorte");
  }

  public java.math.BigDecimal salPourAdministration() {
    return (java.math.BigDecimal) storedValueForKey("salPourAdministration");
  }

  public void setSalPourAdministration(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPourAdministration from " + salPourAdministration() + " to " + value);
    }
    takeStoredValueForKey(value, "salPourAdministration");
  }

  public java.math.BigDecimal salPourDocumentation() {
    return (java.math.BigDecimal) storedValueForKey("salPourDocumentation");
  }

  public void setSalPourDocumentation(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPourDocumentation from " + salPourDocumentation() + " to " + value);
    }
    takeStoredValueForKey(value, "salPourDocumentation");
  }

  public java.math.BigDecimal salPourEnseignement() {
    return (java.math.BigDecimal) storedValueForKey("salPourEnseignement");
  }

  public void setSalPourEnseignement(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPourEnseignement from " + salPourEnseignement() + " to " + value);
    }
    takeStoredValueForKey(value, "salPourEnseignement");
  }

  public java.math.BigDecimal salPourRecherche() {
    return (java.math.BigDecimal) storedValueForKey("salPourRecherche");
  }

  public void setSalPourRecherche(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPourRecherche from " + salPourRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "salPourRecherche");
  }

  public java.math.BigDecimal salPourTechnique() {
    return (java.math.BigDecimal) storedValueForKey("salPourTechnique");
  }

  public void setSalPourTechnique(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salPourTechnique from " + salPourTechnique() + " to " + value);
    }
    takeStoredValueForKey(value, "salPourTechnique");
  }

  public String salReservable() {
    return (String) storedValueForKey("salReservable");
  }

  public void setSalReservable(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salReservable from " + salReservable() + " to " + value);
    }
    takeStoredValueForKey(value, "salReservable");
  }

  public String salRetro() {
    return (String) storedValueForKey("salRetro");
  }

  public void setSalRetro(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salRetro from " + salRetro() + " to " + value);
    }
    takeStoredValueForKey(value, "salRetro");
  }

  public Double salSalleProcheTel() {
    return (Double) storedValueForKey("salSalleProcheTel");
  }

  public void setSalSalleProcheTel(Double value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salSalleProcheTel from " + salSalleProcheTel() + " to " + value);
    }
    takeStoredValueForKey(value, "salSalleProcheTel");
  }

  public java.math.BigDecimal salSuperficie() {
    return (java.math.BigDecimal) storedValueForKey("salSuperficie");
  }

  public void setSalSuperficie(java.math.BigDecimal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salSuperficie from " + salSuperficie() + " to " + value);
    }
    takeStoredValueForKey(value, "salSuperficie");
  }

  public String salTableau() {
    return (String) storedValueForKey("salTableau");
  }

  public void setSalTableau(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTableau from " + salTableau() + " to " + value);
    }
    takeStoredValueForKey(value, "salTableau");
  }

  public String salTableauBlanc() {
    return (String) storedValueForKey("salTableauBlanc");
  }

  public void setSalTableauBlanc(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTableauBlanc from " + salTableauBlanc() + " to " + value);
    }
    takeStoredValueForKey(value, "salTableauBlanc");
  }

  public String salTableauPapier() {
    return (String) storedValueForKey("salTableauPapier");
  }

  public void setSalTableauPapier(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTableauPapier from " + salTableauPapier() + " to " + value);
    }
    takeStoredValueForKey(value, "salTableauPapier");
  }

  public String salTelevision() {
    return (String) storedValueForKey("salTelevision");
  }

  public void setSalTelevision(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTelevision from " + salTelevision() + " to " + value);
    }
    takeStoredValueForKey(value, "salTelevision");
  }

  public String salTemEnService() {
    return (String) storedValueForKey("salTemEnService");
  }

  public void setSalTemEnService(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTemEnService from " + salTemEnService() + " to " + value);
    }
    takeStoredValueForKey(value, "salTemEnService");
  }

  public String salTemHandicap() {
    return (String) storedValueForKey("salTemHandicap");
  }

  public void setSalTemHandicap(String value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
    	_EOSalles.LOG.debug( "updating salTemHandicap from " + salTemHandicap() + " to " + value);
    }
    takeStoredValueForKey(value, "salTemHandicap");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLocal)storedValueForKey("toLocal");
  }

  public void setToLocalRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLocal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("updating toLocal from " + toLocal() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLocal oldValue = toLocal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLocal");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLocal");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle toTypeSalle() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle)storedValueForKey("toTypeSalle");
  }

  public void setToTypeSalleRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("updating toTypeSalle from " + toTypeSalle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle oldValue = toTypeSalle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeSalle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeSalle");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal toVEtageLocal() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal)storedValueForKey("toVEtageLocal");
  }

  public void setToVEtageLocalRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("updating toVEtageLocal from " + toVEtageLocal() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal oldValue = toVEtageLocal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVEtageLocal");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toVEtageLocal");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage toVTypeSalleLocalEtage() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage)storedValueForKey("toVTypeSalleLocalEtage");
  }

  public void setToVTypeSalleLocalEtageRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage value) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("updating toVTypeSalleLocalEtage from " + toVTypeSalleLocalEtage() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage oldValue = toVTypeSalleLocalEtage();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toVTypeSalleLocalEtage");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toVTypeSalleLocalEtage");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalless() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)storedValueForKey("toDepositaireSalless");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalless(EOQualifier qualifier) {
    return toDepositaireSalless(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalless(EOQualifier qualifier, boolean fetch) {
    return toDepositaireSalless(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalless(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles.fetchFwkGspot_DepositaireSalleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDepositaireSalless();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDepositaireSallessRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toDepositaireSalless relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDepositaireSalless");
  }

  public void removeFromToDepositaireSallessRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toDepositaireSalless relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDepositaireSalless");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles createToDepositaireSallessRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_DepositaireSalles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDepositaireSalless");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles) eo;
  }

  public void deleteToDepositaireSallessRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDepositaireSalless");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDepositaireSallessRelationships() {
    Enumeration objects = toDepositaireSalless().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDepositaireSallessRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)storedValueForKey("toDetailPourcentages");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier) {
    return toDetailPourcentages(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, boolean fetch) {
    return toDetailPourcentages(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.fetchFwkGspot_DetailPourcentages(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDetailPourcentages();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toDetailPourcentages relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public void removeFromToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toDetailPourcentages relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage createToDetailPourcentagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_DetailPourcentage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPourcentages");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage) eo;
  }

  public void deleteToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDetailPourcentagesRelationships() {
    Enumeration objects = toDetailPourcentages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDetailPourcentagesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises> toPriseses() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises>)storedValueForKey("toPriseses");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises> toPriseses(EOQualifier qualifier) {
    return toPriseses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises> toPriseses(EOQualifier qualifier, boolean fetch) {
    return toPriseses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises> toPriseses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOPrises.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOPrises.fetchFwkGspot_Priseses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPriseses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOPrises>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrisesesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOPrises object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toPriseses relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toPriseses");
  }

  public void removeFromToPrisesesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOPrises object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toPriseses relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toPriseses");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOPrises createToPrisesesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_Prises");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toPriseses");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOPrises) eo;
  }

  public void deleteToPrisesesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOPrises object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toPriseses");
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrisesesRelationships() {
    Enumeration objects = toPriseses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrisesesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOPrises)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)storedValueForKey("toRepartBureau");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier) {
    return toRepartBureau(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier, boolean fetch) {
    return toRepartBureau(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau.fetchFwkGspot_RepartBureaus(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartBureau();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toRepartBureau relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartBureau");
  }

  public void removeFromToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toRepartBureau relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBureau");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau createToRepartBureauRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartBureau");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartBureau");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau) eo;
  }

  public void deleteToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBureau");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartBureauRelationships() {
    Enumeration objects = toRepartBureau().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartBureauRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)storedValueForKey("toRepartLotSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier) {
    return toRepartLotSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier, boolean fetch) {
    return toRepartLotSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle.fetchFwkGspot_RepartLotSalles(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartLotSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toRepartLotSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
  }

  public void removeFromToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toRepartLotSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle createToRepartLotSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartLotSalle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartLotSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle) eo;
  }

  public void deleteToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartLotSallesRelationships() {
    Enumeration objects = toRepartLotSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartLotSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toRepartSalleAccesHandicapes() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)storedValueForKey("toRepartSalleAccesHandicapes");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toRepartSalleAccesHandicapes(EOQualifier qualifier) {
    return toRepartSalleAccesHandicapes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toRepartSalleAccesHandicapes(EOQualifier qualifier, boolean fetch) {
    return toRepartSalleAccesHandicapes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> toRepartSalleAccesHandicapes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape.TO_FWK_GSPOT__SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicapes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartSalleAccesHandicapes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toRepartSalleAccesHandicapes relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartSalleAccesHandicapes");
  }

  public void removeFromToRepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toRepartSalleAccesHandicapes relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartSalleAccesHandicapes");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape createToRepartSalleAccesHandicapesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartSalleAccesHandicape");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartSalleAccesHandicapes");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape) eo;
  }

  public void deleteToRepartSalleAccesHandicapesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartSalleAccesHandicapes");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartSalleAccesHandicapesRelationships() {
    Enumeration objects = toRepartSalleAccesHandicapes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartSalleAccesHandicapesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartSalleTypeAccesHandicape)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)storedValueForKey("toResaObjets");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier) {
    return toResaObjets(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier, boolean fetch) {
    return toResaObjets(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet.fetchFwkGspot_ResaObjets(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toResaObjets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toResaObjets relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toResaObjets");
  }

  public void removeFromToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toResaObjets relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaObjets");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet createToResaObjetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_ResaObjet");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toResaObjets");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet) eo;
  }

  public void deleteToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaObjets");
    editingContext().deleteObject(object);
  }

  public void deleteAllToResaObjetsRelationships() {
    Enumeration objects = toResaObjets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToResaObjetsRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte> toSallePortes() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte>)storedValueForKey("toSallePortes");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte> toSallePortes(EOQualifier qualifier) {
    return toSallePortes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte> toSallePortes(EOQualifier qualifier, boolean fetch) {
    return toSallePortes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte> toSallePortes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte.fetchFwkGspot_SallePortes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSallePortes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSallePortesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toSallePortes relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSallePortes");
  }

  public void removeFromToSallePortesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toSallePortes relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSallePortes");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte createToSallePortesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_SallePorte");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSallePortes");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte) eo;
  }

  public void deleteToSallePortesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSallePortes");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSallePortesRelationships() {
    Enumeration objects = toSallePortes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSallePortesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSallePorte)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone> toSalleTelephones() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone>)storedValueForKey("toSalleTelephones");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone> toSalleTelephones(EOQualifier qualifier) {
    return toSalleTelephones(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone> toSalleTelephones(EOQualifier qualifier, boolean fetch) {
    return toSalleTelephones(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone> toSalleTelephones(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone.TO_SALLES_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone.fetchFwkGspot_SalleTelephones(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSalleTelephones();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSalleTelephonesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("adding " + object + " to toSalleTelephones relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSalleTelephones");
  }

  public void removeFromToSalleTelephonesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone object) {
    if (_EOSalles.LOG.isDebugEnabled()) {
      _EOSalles.LOG.debug("removing " + object + " from toSalleTelephones relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalleTelephones");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone createToSalleTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_SalleTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSalleTelephones");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone) eo;
  }

  public void deleteToSalleTelephonesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalleTelephones");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSalleTelephonesRelationships() {
    Enumeration objects = toSalleTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSalleTelephonesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSalleTelephone)objects.nextElement());
    }
  }


  public static EOSalles createFwkGspot_Salles(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOSalles eo = (EOSalles) EOUtilities.createAndInsertInstance(editingContext, _EOSalles.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOSalles> fetchAllFwkGspot_Salleses(EOEditingContext editingContext) {
    return _EOSalles.fetchAllFwkGspot_Salleses(editingContext, null);
  }

  public static NSArray<EOSalles> fetchAllFwkGspot_Salleses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSalles.fetchFwkGspot_Salleses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSalles> fetchFwkGspot_Salleses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSalles.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSalles> eoObjects = (NSArray<EOSalles>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSalles fetchFwkGspot_Salles(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalles.fetchFwkGspot_Salles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalles fetchFwkGspot_Salles(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSalles> eoObjects = _EOSalles.fetchFwkGspot_Salleses(editingContext, qualifier, null);
    EOSalles eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSalles)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_Salles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalles fetchRequiredFwkGspot_Salles(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalles.fetchRequiredFwkGspot_Salles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalles fetchRequiredFwkGspot_Salles(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSalles eoObject = _EOSalles.fetchFwkGspot_Salles(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_Salles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalles localInstanceIn(EOEditingContext editingContext, EOSalles eo) {
    EOSalles localInstance = (eo == null) ? null : (EOSalles)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
