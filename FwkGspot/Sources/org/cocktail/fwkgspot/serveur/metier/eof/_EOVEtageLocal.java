// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVEtageLocal.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVEtageLocal extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_VEtageLocal";

	// Attributes
	public static final String SAL_ETAGE_KEY = "salEtage";

	// Relationships
	public static final String TO_LOCAL_KEY = "toLocal";
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EOVEtageLocal.class);

  public EOVEtageLocal localInstanceIn(EOEditingContext editingContext) {
    EOVEtageLocal localInstance = (EOVEtageLocal)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String salEtage() {
    return (String) storedValueForKey("salEtage");
  }

  public void setSalEtage(String value) {
    if (_EOVEtageLocal.LOG.isDebugEnabled()) {
    	_EOVEtageLocal.LOG.debug( "updating salEtage from " + salEtage() + " to " + value);
    }
    takeStoredValueForKey(value, "salEtage");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLocal)storedValueForKey("toLocal");
  }

  public void setToLocalRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLocal value) {
    if (_EOVEtageLocal.LOG.isDebugEnabled()) {
      _EOVEtageLocal.LOG.debug("updating toLocal from " + toLocal() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLocal oldValue = toLocal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLocal");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLocal");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)storedValueForKey("toSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier) {
    return toSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, boolean fetch) {
    return toSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.TO_V_ETAGE_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.fetchFwkGspot_Salleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOVEtageLocal.LOG.isDebugEnabled()) {
      _EOVEtageLocal.LOG.debug("adding " + object + " to toSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public void removeFromToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOVEtageLocal.LOG.isDebugEnabled()) {
      _EOVEtageLocal.LOG.debug("removing " + object + " from toSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles createToSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_Salles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles) eo;
  }

  public void deleteToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSallesRelationships() {
    Enumeration objects = toSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)objects.nextElement());
    }
  }


  public static EOVEtageLocal createFwkGspot_VEtageLocal(EOEditingContext editingContext, String salEtage
, org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal) {
    EOVEtageLocal eo = (EOVEtageLocal) EOUtilities.createAndInsertInstance(editingContext, _EOVEtageLocal.ENTITY_NAME);    
		eo.setSalEtage(salEtage);
    eo.setToLocalRelationship(toLocal);
    return eo;
  }

  public static NSArray<EOVEtageLocal> fetchAllFwkGspot_VEtageLocals(EOEditingContext editingContext) {
    return _EOVEtageLocal.fetchAllFwkGspot_VEtageLocals(editingContext, null);
  }

  public static NSArray<EOVEtageLocal> fetchAllFwkGspot_VEtageLocals(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVEtageLocal.fetchFwkGspot_VEtageLocals(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVEtageLocal> fetchFwkGspot_VEtageLocals(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVEtageLocal.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVEtageLocal> eoObjects = (NSArray<EOVEtageLocal>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVEtageLocal fetchFwkGspot_VEtageLocal(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVEtageLocal.fetchFwkGspot_VEtageLocal(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVEtageLocal fetchFwkGspot_VEtageLocal(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVEtageLocal> eoObjects = _EOVEtageLocal.fetchFwkGspot_VEtageLocals(editingContext, qualifier, null);
    EOVEtageLocal eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVEtageLocal)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_VEtageLocal that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVEtageLocal fetchRequiredFwkGspot_VEtageLocal(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVEtageLocal.fetchRequiredFwkGspot_VEtageLocal(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVEtageLocal fetchRequiredFwkGspot_VEtageLocal(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVEtageLocal eoObject = _EOVEtageLocal.fetchFwkGspot_VEtageLocal(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_VEtageLocal that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVEtageLocal localInstanceIn(EOEditingContext editingContext, EOVEtageLocal eo) {
    EOVEtageLocal localInstance = (eo == null) ? null : (EOVEtageLocal)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
