// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartBureau.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartBureau extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartBureau";

	// Attributes
	public static final String BUR_PRINCIPAL_KEY = "burPrincipal";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_INDIVIDU_KEY = "toIndividu";
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_STRUCTURE_ULR_KEY = "toStructureUlr";

  private static Logger LOG = Logger.getLogger(_EORepartBureau.class);

  public EORepartBureau localInstanceIn(EOEditingContext editingContext) {
    EORepartBureau localInstance = (EORepartBureau)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Boolean burPrincipal() {
    return (Boolean) storedValueForKey("burPrincipal");
  }

  public void setBurPrincipal(Boolean value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
    	_EORepartBureau.LOG.debug( "updating burPrincipal from " + burPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, "burPrincipal");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
    	_EORepartBureau.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
    	_EORepartBureau.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toIndividu");
  }

  public void setToIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
      _EORepartBureau.LOG.debug("updating toIndividu from " + toIndividu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toIndividu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toIndividu");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
      _EORepartBureau.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlr");
  }

  public void setToStructureUlrRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EORepartBureau.LOG.isDebugEnabled()) {
      _EORepartBureau.LOG.debug("updating toStructureUlr from " + toStructureUlr() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlr");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlr");
    }
  }
  

  public static EORepartBureau createFwkGspot_RepartBureau(EOEditingContext editingContext, Boolean burPrincipal
, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividu, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles) {
    EORepartBureau eo = (EORepartBureau) EOUtilities.createAndInsertInstance(editingContext, _EORepartBureau.ENTITY_NAME);    
		eo.setBurPrincipal(burPrincipal);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToIndividuRelationship(toIndividu);
    eo.setToSallesRelationship(toSalles);
    return eo;
  }

  public static NSArray<EORepartBureau> fetchAllFwkGspot_RepartBureaus(EOEditingContext editingContext) {
    return _EORepartBureau.fetchAllFwkGspot_RepartBureaus(editingContext, null);
  }

  public static NSArray<EORepartBureau> fetchAllFwkGspot_RepartBureaus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartBureau.fetchFwkGspot_RepartBureaus(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartBureau> fetchFwkGspot_RepartBureaus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartBureau.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartBureau> eoObjects = (NSArray<EORepartBureau>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartBureau fetchFwkGspot_RepartBureau(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartBureau.fetchFwkGspot_RepartBureau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartBureau fetchFwkGspot_RepartBureau(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartBureau> eoObjects = _EORepartBureau.fetchFwkGspot_RepartBureaus(editingContext, qualifier, null);
    EORepartBureau eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartBureau)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartBureau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartBureau fetchRequiredFwkGspot_RepartBureau(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartBureau.fetchRequiredFwkGspot_RepartBureau(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartBureau fetchRequiredFwkGspot_RepartBureau(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartBureau eoObject = _EORepartBureau.fetchFwkGspot_RepartBureau(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartBureau that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartBureau localInstanceIn(EOEditingContext editingContext, EORepartBureau eo) {
    EORepartBureau localInstance = (eo == null) ? null : (EORepartBureau)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
