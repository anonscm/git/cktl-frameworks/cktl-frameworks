// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeSalle.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeSalle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_TypeSalle";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String TSAL_LIBELLE_KEY = "tsalLibelle";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_V_TYPE_SALLE_LOCAL_ETAGES_KEY = "toVTypeSalleLocalEtages";

  private static Logger LOG = Logger.getLogger(_EOTypeSalle.class);

  public EOTypeSalle localInstanceIn(EOEditingContext editingContext) {
    EOTypeSalle localInstance = (EOTypeSalle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
    	_EOTypeSalle.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
    	_EOTypeSalle.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String tsalLibelle() {
    return (String) storedValueForKey("tsalLibelle");
  }

  public void setTsalLibelle(String value) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
    	_EOTypeSalle.LOG.debug( "updating tsalLibelle from " + tsalLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tsalLibelle");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)storedValueForKey("toSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier) {
    return toSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, boolean fetch) {
    return toSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.TO_TYPE_SALLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.fetchFwkGspot_Salleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
      _EOTypeSalle.LOG.debug("adding " + object + " to toSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public void removeFromToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
      _EOTypeSalle.LOG.debug("removing " + object + " from toSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles createToSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_Salles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles) eo;
  }

  public void deleteToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSallesRelationships() {
    Enumeration objects = toSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)storedValueForKey("toVTypeSalleLocalEtages");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier) {
    return toVTypeSalleLocalEtages(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier, boolean fetch) {
    return toVTypeSalleLocalEtages(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage.TO_TYPE_SALLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtages(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVTypeSalleLocalEtages();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
      _EOTypeSalle.LOG.debug("adding " + object + " to toVTypeSalleLocalEtages relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
  }

  public void removeFromToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    if (_EOTypeSalle.LOG.isDebugEnabled()) {
      _EOTypeSalle.LOG.debug("removing " + object + " from toVTypeSalleLocalEtages relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage createToVTypeSalleLocalEtagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_VTypeSalleLocalEtage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toVTypeSalleLocalEtages");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage) eo;
  }

  public void deleteToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
    editingContext().deleteObject(object);
  }

  public void deleteAllToVTypeSalleLocalEtagesRelationships() {
    Enumeration objects = toVTypeSalleLocalEtages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVTypeSalleLocalEtagesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage)objects.nextElement());
    }
  }


  public static EOTypeSalle createFwkGspot_TypeSalle(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOTypeSalle eo = (EOTypeSalle) EOUtilities.createAndInsertInstance(editingContext, _EOTypeSalle.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOTypeSalle> fetchAllFwkGspot_TypeSalles(EOEditingContext editingContext) {
    return _EOTypeSalle.fetchAllFwkGspot_TypeSalles(editingContext, null);
  }

  public static NSArray<EOTypeSalle> fetchAllFwkGspot_TypeSalles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeSalle.fetchFwkGspot_TypeSalles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeSalle> fetchFwkGspot_TypeSalles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeSalle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeSalle> eoObjects = (NSArray<EOTypeSalle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeSalle fetchFwkGspot_TypeSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeSalle.fetchFwkGspot_TypeSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeSalle fetchFwkGspot_TypeSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeSalle> eoObjects = _EOTypeSalle.fetchFwkGspot_TypeSalles(editingContext, qualifier, null);
    EOTypeSalle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeSalle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_TypeSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeSalle fetchRequiredFwkGspot_TypeSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeSalle.fetchRequiredFwkGspot_TypeSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeSalle fetchRequiredFwkGspot_TypeSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeSalle eoObject = _EOTypeSalle.fetchFwkGspot_TypeSalle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_TypeSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeSalle localInstanceIn(EOEditingContext editingContext, EOTypeSalle eo) {
    EOTypeSalle localInstance = (eo == null) ? null : (EOTypeSalle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
