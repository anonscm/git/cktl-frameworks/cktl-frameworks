package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

public class EODetailPourcentage extends _EODetailPourcentage {
    private static final long serialVersionUID = 4099410697506171979L;
    private static Logger log = Logger.getLogger(EODetailPourcentage.class);

    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (this.toStructureUlr() == null) {
            throw new ValidationException("Le service est obligatoire !");
        }
        if (this.toTypeOccupation() == null) {
            throw new ValidationException("Le type occupation est obligatoire !");
        }
        if (this.detPourcentage() == null) {
            throw new ValidationException("Le pourcentage est obligatoire !");
        }
        if (this.toSalles() == null)
            throw new ValidationException("La salle est obligatoire !");
        //
        for (EODetailPourcentage detPct : toSalles().toDetailPourcentages()) {
            if ((!this.equals(detPct) && detPct.toStructureUlr().equals(this.toStructureUlr()))
                    && (detPct.toTypeOccupation()
                            .equals(this.toTypeOccupation()))) {
                throw new ValidationException("Cette occupation existe déjà !!");
            }
        }
    }

}
