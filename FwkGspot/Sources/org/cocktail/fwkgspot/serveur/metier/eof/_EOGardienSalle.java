// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOGardienSalle.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOGardienSalle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "GardienSalle";

	// Attributes

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOGardienSalle.class);

  public EOGardienSalle localInstanceIn(EOEditingContext editingContext) {
    EOGardienSalle localInstance = (EOGardienSalle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }


  public static EOGardienSalle createGardienSalle(EOEditingContext editingContext) {
    EOGardienSalle eo = (EOGardienSalle) EOUtilities.createAndInsertInstance(editingContext, _EOGardienSalle.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOGardienSalle> fetchAllGardienSalles(EOEditingContext editingContext) {
    return _EOGardienSalle.fetchAllGardienSalles(editingContext, null);
  }

  public static NSArray<EOGardienSalle> fetchAllGardienSalles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOGardienSalle.fetchGardienSalles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOGardienSalle> fetchGardienSalles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOGardienSalle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOGardienSalle> eoObjects = (NSArray<EOGardienSalle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOGardienSalle fetchGardienSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGardienSalle.fetchGardienSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGardienSalle fetchGardienSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOGardienSalle> eoObjects = _EOGardienSalle.fetchGardienSalles(editingContext, qualifier, null);
    EOGardienSalle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOGardienSalle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one GardienSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGardienSalle fetchRequiredGardienSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOGardienSalle.fetchRequiredGardienSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOGardienSalle fetchRequiredGardienSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOGardienSalle eoObject = _EOGardienSalle.fetchGardienSalle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no GardienSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOGardienSalle localInstanceIn(EOEditingContext editingContext, EOGardienSalle eo) {
    EOGardienSalle localInstance = (eo == null) ? null : (EOGardienSalle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
