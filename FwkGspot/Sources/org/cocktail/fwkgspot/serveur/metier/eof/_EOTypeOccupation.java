// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeOccupation.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeOccupation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_TypeOccupation";

	// Attributes
	public static final String TOC_LIBELLE_KEY = "tocLibelle";

	// Relationships
	public static final String TO_DETAIL_POURCENTAGES_KEY = "toDetailPourcentages";

  private static Logger LOG = Logger.getLogger(_EOTypeOccupation.class);

  public EOTypeOccupation localInstanceIn(EOEditingContext editingContext) {
    EOTypeOccupation localInstance = (EOTypeOccupation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tocLibelle() {
    return (String) storedValueForKey("tocLibelle");
  }

  public void setTocLibelle(String value) {
    if (_EOTypeOccupation.LOG.isDebugEnabled()) {
    	_EOTypeOccupation.LOG.debug( "updating tocLibelle from " + tocLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "tocLibelle");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)storedValueForKey("toDetailPourcentages");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier) {
    return toDetailPourcentages(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, boolean fetch) {
    return toDetailPourcentages(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.TO_TYPE_OCCUPATION_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.fetchFwkGspot_DetailPourcentages(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDetailPourcentages();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOTypeOccupation.LOG.isDebugEnabled()) {
      _EOTypeOccupation.LOG.debug("adding " + object + " to toDetailPourcentages relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public void removeFromToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOTypeOccupation.LOG.isDebugEnabled()) {
      _EOTypeOccupation.LOG.debug("removing " + object + " from toDetailPourcentages relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage createToDetailPourcentagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_DetailPourcentage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPourcentages");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage) eo;
  }

  public void deleteToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDetailPourcentagesRelationships() {
    Enumeration objects = toDetailPourcentages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDetailPourcentagesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage)objects.nextElement());
    }
  }


  public static EOTypeOccupation createFwkGspot_TypeOccupation(EOEditingContext editingContext) {
    EOTypeOccupation eo = (EOTypeOccupation) EOUtilities.createAndInsertInstance(editingContext, _EOTypeOccupation.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOTypeOccupation> fetchAllFwkGspot_TypeOccupations(EOEditingContext editingContext) {
    return _EOTypeOccupation.fetchAllFwkGspot_TypeOccupations(editingContext, null);
  }

  public static NSArray<EOTypeOccupation> fetchAllFwkGspot_TypeOccupations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeOccupation.fetchFwkGspot_TypeOccupations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeOccupation> fetchFwkGspot_TypeOccupations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeOccupation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeOccupation> eoObjects = (NSArray<EOTypeOccupation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeOccupation fetchFwkGspot_TypeOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOccupation.fetchFwkGspot_TypeOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOccupation fetchFwkGspot_TypeOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeOccupation> eoObjects = _EOTypeOccupation.fetchFwkGspot_TypeOccupations(editingContext, qualifier, null);
    EOTypeOccupation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeOccupation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_TypeOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOccupation fetchRequiredFwkGspot_TypeOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeOccupation.fetchRequiredFwkGspot_TypeOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeOccupation fetchRequiredFwkGspot_TypeOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeOccupation eoObject = _EOTypeOccupation.fetchFwkGspot_TypeOccupation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_TypeOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeOccupation localInstanceIn(EOEditingContext editingContext, EOTypeOccupation eo) {
    EOTypeOccupation localInstance = (eo == null) ? null : (EOTypeOccupation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
