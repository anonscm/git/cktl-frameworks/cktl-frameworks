// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOTypeMateriel.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOTypeMateriel extends  EOGenericRecord {
	public static final String ENTITY_NAME = "TypeMateriel";

	// Attributes
	public static final String STU_LIBELLE_KEY = "stuLibelle";
	public static final String STU_NIVEAU_KEY = "stuNiveau";
	public static final String STU_PERE_KEY = "stuPere";
	public static final String STU_UNITE_ORDRE_KEY = "stuUniteOrdre";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOTypeMateriel.class);

  public EOTypeMateriel localInstanceIn(EOEditingContext editingContext) {
    EOTypeMateriel localInstance = (EOTypeMateriel)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String stuLibelle() {
    return (String) storedValueForKey("stuLibelle");
  }

  public void setStuLibelle(String value) {
    if (_EOTypeMateriel.LOG.isDebugEnabled()) {
    	_EOTypeMateriel.LOG.debug( "updating stuLibelle from " + stuLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "stuLibelle");
  }

  public Double stuNiveau() {
    return (Double) storedValueForKey("stuNiveau");
  }

  public void setStuNiveau(Double value) {
    if (_EOTypeMateriel.LOG.isDebugEnabled()) {
    	_EOTypeMateriel.LOG.debug( "updating stuNiveau from " + stuNiveau() + " to " + value);
    }
    takeStoredValueForKey(value, "stuNiveau");
  }

  public Double stuPere() {
    return (Double) storedValueForKey("stuPere");
  }

  public void setStuPere(Double value) {
    if (_EOTypeMateriel.LOG.isDebugEnabled()) {
    	_EOTypeMateriel.LOG.debug( "updating stuPere from " + stuPere() + " to " + value);
    }
    takeStoredValueForKey(value, "stuPere");
  }

  public Double stuUniteOrdre() {
    return (Double) storedValueForKey("stuUniteOrdre");
  }

  public void setStuUniteOrdre(Double value) {
    if (_EOTypeMateriel.LOG.isDebugEnabled()) {
    	_EOTypeMateriel.LOG.debug( "updating stuUniteOrdre from " + stuUniteOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "stuUniteOrdre");
  }


  public static EOTypeMateriel createTypeMateriel(EOEditingContext editingContext) {
    EOTypeMateriel eo = (EOTypeMateriel) EOUtilities.createAndInsertInstance(editingContext, _EOTypeMateriel.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOTypeMateriel> fetchAllTypeMateriels(EOEditingContext editingContext) {
    return _EOTypeMateriel.fetchAllTypeMateriels(editingContext, null);
  }

  public static NSArray<EOTypeMateriel> fetchAllTypeMateriels(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOTypeMateriel.fetchTypeMateriels(editingContext, null, sortOrderings);
  }

  public static NSArray<EOTypeMateriel> fetchTypeMateriels(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOTypeMateriel.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOTypeMateriel> eoObjects = (NSArray<EOTypeMateriel>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOTypeMateriel fetchTypeMateriel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMateriel.fetchTypeMateriel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMateriel fetchTypeMateriel(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOTypeMateriel> eoObjects = _EOTypeMateriel.fetchTypeMateriels(editingContext, qualifier, null);
    EOTypeMateriel eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOTypeMateriel)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one TypeMateriel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMateriel fetchRequiredTypeMateriel(EOEditingContext editingContext, String keyName, Object value) {
    return _EOTypeMateriel.fetchRequiredTypeMateriel(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOTypeMateriel fetchRequiredTypeMateriel(EOEditingContext editingContext, EOQualifier qualifier) {
    EOTypeMateriel eoObject = _EOTypeMateriel.fetchTypeMateriel(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no TypeMateriel that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOTypeMateriel localInstanceIn(EOEditingContext editingContext, EOTypeMateriel eo) {
    EOTypeMateriel localInstance = (eo == null) ? null : (EOTypeMateriel)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
