package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;

public class EORepartBureau extends _EORepartBureau {
	private static Logger log = Logger.getLogger(EORepartBureau.class);

	public static final String BUREAU_PRINCIPAL_KEY = "bureauPrincipal";
	public static final ERXKey<String> BUREAU_PRICIPAL = new ERXKey<String>(
			BUREAU_PRINCIPAL_KEY);

	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();

		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		for (EORepartBureau repart : toSalles().toRepartBureau()) {
			if (!this.equals(repart)
					&& repart.toIndividu().equals(this.toIndividu())) {
				throw new ValidationException("Cette occupation existe déjà !!");
			}
		}

	}

	/**
	 * Transforme la proprité burPrincipal (valeur provenant de la Base de donnée) en valeur afficher dans l'IHM
	 * @return Chaine à afficher dans l'ihm pour le booleen burPrincipal
	 */
	public String bureauPrincipal() {
		if (burPrincipal())
			return "Oui";
		else
			return "";
	}

}
