// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartBatImpGeo.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartBatImpGeo extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartBatImpGeo";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_IMPLANTATION_GEO_KEY = "toImplantationGeo";
	public static final String TO_LOCAL_KEY = "toLocal";

  private static Logger LOG = Logger.getLogger(_EORepartBatImpGeo.class);

  public EORepartBatImpGeo localInstanceIn(EOEditingContext editingContext) {
    EORepartBatImpGeo localInstance = (EORepartBatImpGeo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartBatImpGeo.LOG.isDebugEnabled()) {
    	_EORepartBatImpGeo.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartBatImpGeo.LOG.isDebugEnabled()) {
    	_EORepartBatImpGeo.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo toImplantationGeo() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo)storedValueForKey("toImplantationGeo");
  }

  public void setToImplantationGeoRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo value) {
    if (_EORepartBatImpGeo.LOG.isDebugEnabled()) {
      _EORepartBatImpGeo.LOG.debug("updating toImplantationGeo from " + toImplantationGeo() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo oldValue = toImplantationGeo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toImplantationGeo");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toImplantationGeo");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLocal)storedValueForKey("toLocal");
  }

  public void setToLocalRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLocal value) {
    if (_EORepartBatImpGeo.LOG.isDebugEnabled()) {
      _EORepartBatImpGeo.LOG.debug("updating toLocal from " + toLocal() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLocal oldValue = toLocal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLocal");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLocal");
    }
  }
  

  public static EORepartBatImpGeo createFwkGspot_RepartBatImpGeo(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo toImplantationGeo, org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal) {
    EORepartBatImpGeo eo = (EORepartBatImpGeo) EOUtilities.createAndInsertInstance(editingContext, _EORepartBatImpGeo.ENTITY_NAME);    
    eo.setToImplantationGeoRelationship(toImplantationGeo);
    eo.setToLocalRelationship(toLocal);
    return eo;
  }

  public static NSArray<EORepartBatImpGeo> fetchAllFwkGspot_RepartBatImpGeos(EOEditingContext editingContext) {
    return _EORepartBatImpGeo.fetchAllFwkGspot_RepartBatImpGeos(editingContext, null);
  }

  public static NSArray<EORepartBatImpGeo> fetchAllFwkGspot_RepartBatImpGeos(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeos(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartBatImpGeo> fetchFwkGspot_RepartBatImpGeos(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartBatImpGeo.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartBatImpGeo> eoObjects = (NSArray<EORepartBatImpGeo>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartBatImpGeo fetchFwkGspot_RepartBatImpGeo(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartBatImpGeo fetchFwkGspot_RepartBatImpGeo(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartBatImpGeo> eoObjects = _EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeos(editingContext, qualifier, null);
    EORepartBatImpGeo eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartBatImpGeo)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartBatImpGeo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartBatImpGeo fetchRequiredFwkGspot_RepartBatImpGeo(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartBatImpGeo.fetchRequiredFwkGspot_RepartBatImpGeo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartBatImpGeo fetchRequiredFwkGspot_RepartBatImpGeo(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartBatImpGeo eoObject = _EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeo(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartBatImpGeo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartBatImpGeo localInstanceIn(EOEditingContext editingContext, EORepartBatImpGeo eo) {
    EORepartBatImpGeo localInstance = (eo == null) ? null : (EORepartBatImpGeo)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
