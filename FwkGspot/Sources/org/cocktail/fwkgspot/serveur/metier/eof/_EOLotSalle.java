// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLotSalle.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLotSalle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_LotSalle";

	// Attributes
	public static final String LOT_LIBELLE_KEY = "lotLibelle";

	// Relationships
	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_REPART_LOT_INDIVIDUS_KEY = "toRepartLotIndividus";
	public static final String TO_REPART_LOT_SALLES_KEY = "toRepartLotSalles";

  private static Logger LOG = Logger.getLogger(_EOLotSalle.class);

  public EOLotSalle localInstanceIn(EOEditingContext editingContext) {
    EOLotSalle localInstance = (EOLotSalle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String lotLibelle() {
    return (String) storedValueForKey("lotLibelle");
  }

  public void setLotLibelle(String value) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
    	_EOLotSalle.LOG.debug( "updating lotLibelle from " + lotLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "lotLibelle");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
  }

  public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
      _EOLotSalle.LOG.debug("updating toFwkpers_Individu from " + toFwkpers_Individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu> toRepartLotIndividus() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu>)storedValueForKey("toRepartLotIndividus");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu> toRepartLotIndividus(EOQualifier qualifier) {
    return toRepartLotIndividus(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu> toRepartLotIndividus(EOQualifier qualifier, boolean fetch) {
    return toRepartLotIndividus(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu> toRepartLotIndividus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu.TO_LOT_SALLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu.fetchFwkGspot_RepartLotIndividus(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartLotIndividus();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartLotIndividusRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu object) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
      _EOLotSalle.LOG.debug("adding " + object + " to toRepartLotIndividus relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartLotIndividus");
  }

  public void removeFromToRepartLotIndividusRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu object) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
      _EOLotSalle.LOG.debug("removing " + object + " from toRepartLotIndividus relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotIndividus");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu createToRepartLotIndividusRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartLotIndividu");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartLotIndividus");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu) eo;
  }

  public void deleteToRepartLotIndividusRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotIndividus");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartLotIndividusRelationships() {
    Enumeration objects = toRepartLotIndividus().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartLotIndividusRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotIndividu)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)storedValueForKey("toRepartLotSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier) {
    return toRepartLotSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier, boolean fetch) {
    return toRepartLotSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> toRepartLotSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle.TO_LOT_SALLE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle.fetchFwkGspot_RepartLotSalles(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartLotSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
      _EOLotSalle.LOG.debug("adding " + object + " to toRepartLotSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
  }

  public void removeFromToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    if (_EOLotSalle.LOG.isDebugEnabled()) {
      _EOLotSalle.LOG.debug("removing " + object + " from toRepartLotSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle createToRepartLotSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartLotSalle");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartLotSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle) eo;
  }

  public void deleteToRepartLotSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartLotSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartLotSallesRelationships() {
    Enumeration objects = toRepartLotSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartLotSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartLotSalle)objects.nextElement());
    }
  }


  public static EOLotSalle createFwkGspot_LotSalle(EOEditingContext editingContext, String lotLibelle
, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu) {
    EOLotSalle eo = (EOLotSalle) EOUtilities.createAndInsertInstance(editingContext, _EOLotSalle.ENTITY_NAME);    
		eo.setLotLibelle(lotLibelle);
    eo.setToFwkpers_IndividuRelationship(toFwkpers_Individu);
    return eo;
  }

  public static NSArray<EOLotSalle> fetchAllFwkGspot_LotSalles(EOEditingContext editingContext) {
    return _EOLotSalle.fetchAllFwkGspot_LotSalles(editingContext, null);
  }

  public static NSArray<EOLotSalle> fetchAllFwkGspot_LotSalles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLotSalle.fetchFwkGspot_LotSalles(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLotSalle> fetchFwkGspot_LotSalles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLotSalle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLotSalle> eoObjects = (NSArray<EOLotSalle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLotSalle fetchFwkGspot_LotSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLotSalle.fetchFwkGspot_LotSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLotSalle fetchFwkGspot_LotSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLotSalle> eoObjects = _EOLotSalle.fetchFwkGspot_LotSalles(editingContext, qualifier, null);
    EOLotSalle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLotSalle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_LotSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLotSalle fetchRequiredFwkGspot_LotSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLotSalle.fetchRequiredFwkGspot_LotSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLotSalle fetchRequiredFwkGspot_LotSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLotSalle eoObject = _EOLotSalle.fetchFwkGspot_LotSalle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_LotSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLotSalle localInstanceIn(EOEditingContext editingContext, EOLotSalle eo) {
    EOLotSalle localInstance = (eo == null) ? null : (EOLotSalle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
