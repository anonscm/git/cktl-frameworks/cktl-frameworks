package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class EOLocal extends _EOLocal implements ILocal{
	private static Logger log = Logger.getLogger(EOLocal.class);
	public static EOQualifier QUAL_VALID_LOCAL = EOQualifier
			.qualifierWithQualifierFormat(EOLocal.D_DEB_VAL_KEY
					+ " <= %@ AND (" + EOLocal.D_FIN_VAL_KEY + " = nil OR "
					+ EOLocal.D_FIN_VAL_KEY + " > %@ )",
					new NSArray<NSTimestamp>(new NSTimestamp[] {
							new NSTimestamp(), new NSTimestamp() }));

	public static final String TO_FIRST_IMPLANTATION_GEO = "toFirstImplantationGeo";

	/**
	 * @return la première implantation correspondant à ce local, null si aucune
	 *         implantation n'est attachée à ce local.
	 */
	public EOImplantationGeo toFirstImplantationGeo() {
		NSArray<EORepartBatImpGeo> reparts = toRepartBatImpGeos();
		return reparts != null && !reparts.isEmpty() ? reparts.lastObject()
				.toImplantationGeo() : null;
	}

	public void validateObjectMetier() throws NSValidation.ValidationException {

		if ((this.cLocal() == null) || ("".equals(this.cLocal()))) {
			throw new NSValidation.ValidationException(
					"Le code batiment est obligatoire !");
		}
		// verif si le code existe deja
		EOLocal existLocal = EOLocal.fetchFwkGspot_Local(this.editingContext(),
				EOLocal.C_LOCAL_KEY, this.cLocal());
		if ((existLocal != null) && (!existLocal.equals(this))) {
			throw new NSValidation.ValidationException(
					"Le code du batiment est déjà utilisé pour :"
							+ existLocal.appellation());

		}

		if ((this.appellation() == null) || ("".equals(this.appellation()))) {
			throw new NSValidation.ValidationException(
					"L'appellation est obligatoire !");
		}

		if ((this.adresseInterne() == null)) {
			throw new NSValidation.ValidationException(
					"L'adresse interne est obligatoire !");
		}

		if (this.dDebVal() == null) {
			throw new NSValidation.ValidationException(
					"La date de début de validité est obligatoire !");
		}

	}

}
