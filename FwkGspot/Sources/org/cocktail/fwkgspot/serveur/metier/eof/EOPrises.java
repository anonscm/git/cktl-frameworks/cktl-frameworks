package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class EOPrises extends _EOPrises {
  /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
public static final String PRI_TYPE_LIB_KEY = "priseTypeLibelle";

public void validateForInsert() throws NSValidation.ValidationException {
	this.validateObjectMetier();

	super.validateForInsert();
}

public void validateForUpdate() throws NSValidation.ValidationException {
	this.validateObjectMetier();

	super.validateForUpdate();
}

public void validateForDelete() throws NSValidation.ValidationException {
	super.validateForDelete();
}

/**
 * Apparemment cette methode n'est pas appelée.
 * 
 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
 */
public void validateForSave() throws NSValidation.ValidationException {
	validateObjectMetier();

	super.validateForSave();

}

public void validateObjectMetier() throws NSValidation.ValidationException {
	
	if (this.priType() == null) {
		throw new NSValidation.ValidationException(
				"Le type est obligatoire !!");						
	}
	if (this.priCode() == null) {
		throw new NSValidation.ValidationException(
				"Le code est obligatoire !!");			
	}

}
  
  public String priseTypeLibelle(){
	  if (lstTypePrise.containsKey(this.priType())){		  
		  return lstTypePrise.objectForKey(this.priType());
	  }
	  return null;
  }
  
  public static NSDictionary<String, String> lstTypePrise = 
	  new NSMutableDictionary<String, String>(new String[]{"Informatique","Téléphone","Video"},
			  new String[]{"I","T","V"});
  
}
