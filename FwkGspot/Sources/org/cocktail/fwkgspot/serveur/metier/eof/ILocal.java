package org.cocktail.fwkgspot.serveur.metier.eof;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;

/**
 *  un local au sens géographique (batiment, immeuble)
 */
public interface ILocal {
	/**
	 * @return adresse renvoyée par le framework personne
	 */
	IAdresse toFwkpers_Adresse();
}
