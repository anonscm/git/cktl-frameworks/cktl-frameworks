// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVoirie.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVoirie extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Voirie";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String L_VOIE_KEY = "lVoie";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOVoirie.class);

  public EOVoirie localInstanceIn(EOEditingContext editingContext) {
    EOVoirie localInstance = (EOVoirie)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOVoirie.LOG.isDebugEnabled()) {
    	_EOVoirie.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOVoirie.LOG.isDebugEnabled()) {
    	_EOVoirie.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lVoie() {
    return (String) storedValueForKey("lVoie");
  }

  public void setLVoie(String value) {
    if (_EOVoirie.LOG.isDebugEnabled()) {
    	_EOVoirie.LOG.debug( "updating lVoie from " + lVoie() + " to " + value);
    }
    takeStoredValueForKey(value, "lVoie");
  }


  public static EOVoirie createVoirie(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOVoirie eo = (EOVoirie) EOUtilities.createAndInsertInstance(editingContext, _EOVoirie.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOVoirie> fetchAllVoiries(EOEditingContext editingContext) {
    return _EOVoirie.fetchAllVoiries(editingContext, null);
  }

  public static NSArray<EOVoirie> fetchAllVoiries(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVoirie.fetchVoiries(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVoirie> fetchVoiries(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVoirie.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVoirie> eoObjects = (NSArray<EOVoirie>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVoirie fetchVoirie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVoirie.fetchVoirie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVoirie fetchVoirie(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVoirie> eoObjects = _EOVoirie.fetchVoiries(editingContext, qualifier, null);
    EOVoirie eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVoirie)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Voirie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVoirie fetchRequiredVoirie(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVoirie.fetchRequiredVoirie(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVoirie fetchRequiredVoirie(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVoirie eoObject = _EOVoirie.fetchVoirie(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Voirie that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVoirie localInstanceIn(EOEditingContext editingContext, EOVoirie eo) {
    EOVoirie localInstance = (eo == null) ? null : (EOVoirie)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
