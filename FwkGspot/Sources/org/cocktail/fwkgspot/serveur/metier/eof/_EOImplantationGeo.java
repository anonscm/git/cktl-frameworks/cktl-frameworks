// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOImplantationGeo.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOImplantationGeo extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_ImplantationGeo";

	// Attributes
	public static final String C_IMPLANTATION_KEY = "cImplantation";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LC_IMPLANTATION_GEO_KEY = "lcImplantationGeo";
	public static final String LL_IMPLANTATION_GEO_KEY = "llImplantationGeo";

	// Relationships
	public static final String TO_REPART_BAT_IMP_GEOS_KEY = "toRepartBatImpGeos";

  private static Logger LOG = Logger.getLogger(_EOImplantationGeo.class);

  public EOImplantationGeo localInstanceIn(EOEditingContext editingContext) {
    EOImplantationGeo localInstance = (EOImplantationGeo)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cImplantation() {
    return (String) storedValueForKey("cImplantation");
  }

  public void setCImplantation(String value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating cImplantation from " + cImplantation() + " to " + value);
    }
    takeStoredValueForKey(value, "cImplantation");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String lcImplantationGeo() {
    return (String) storedValueForKey("lcImplantationGeo");
  }

  public void setLcImplantationGeo(String value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating lcImplantationGeo from " + lcImplantationGeo() + " to " + value);
    }
    takeStoredValueForKey(value, "lcImplantationGeo");
  }

  public String llImplantationGeo() {
    return (String) storedValueForKey("llImplantationGeo");
  }

  public void setLlImplantationGeo(String value) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
    	_EOImplantationGeo.LOG.debug( "updating llImplantationGeo from " + llImplantationGeo() + " to " + value);
    }
    takeStoredValueForKey(value, "llImplantationGeo");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)storedValueForKey("toRepartBatImpGeos");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier) {
    return toRepartBatImpGeos(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier, boolean fetch) {
    return toRepartBatImpGeos(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeos(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartBatImpGeos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
      _EOImplantationGeo.LOG.debug("adding " + object + " to toRepartBatImpGeos relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
  }

  public void removeFromToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    if (_EOImplantationGeo.LOG.isDebugEnabled()) {
      _EOImplantationGeo.LOG.debug("removing " + object + " from toRepartBatImpGeos relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo createToRepartBatImpGeosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartBatImpGeo");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartBatImpGeos");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo) eo;
  }

  public void deleteToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartBatImpGeosRelationships() {
    Enumeration objects = toRepartBatImpGeos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartBatImpGeosRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo)objects.nextElement());
    }
  }


  public static EOImplantationGeo createFwkGspot_ImplantationGeo(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
) {
    EOImplantationGeo eo = (EOImplantationGeo) EOUtilities.createAndInsertInstance(editingContext, _EOImplantationGeo.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOImplantationGeo> fetchAllFwkGspot_ImplantationGeos(EOEditingContext editingContext) {
    return _EOImplantationGeo.fetchAllFwkGspot_ImplantationGeos(editingContext, null);
  }

  public static NSArray<EOImplantationGeo> fetchAllFwkGspot_ImplantationGeos(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOImplantationGeo.fetchFwkGspot_ImplantationGeos(editingContext, null, sortOrderings);
  }

  public static NSArray<EOImplantationGeo> fetchFwkGspot_ImplantationGeos(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOImplantationGeo.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOImplantationGeo> eoObjects = (NSArray<EOImplantationGeo>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOImplantationGeo fetchFwkGspot_ImplantationGeo(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImplantationGeo.fetchFwkGspot_ImplantationGeo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImplantationGeo fetchFwkGspot_ImplantationGeo(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOImplantationGeo> eoObjects = _EOImplantationGeo.fetchFwkGspot_ImplantationGeos(editingContext, qualifier, null);
    EOImplantationGeo eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOImplantationGeo)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_ImplantationGeo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImplantationGeo fetchRequiredFwkGspot_ImplantationGeo(EOEditingContext editingContext, String keyName, Object value) {
    return _EOImplantationGeo.fetchRequiredFwkGspot_ImplantationGeo(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOImplantationGeo fetchRequiredFwkGspot_ImplantationGeo(EOEditingContext editingContext, EOQualifier qualifier) {
    EOImplantationGeo eoObject = _EOImplantationGeo.fetchFwkGspot_ImplantationGeo(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_ImplantationGeo that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOImplantationGeo localInstanceIn(EOEditingContext editingContext, EOImplantationGeo eo) {
    EOImplantationGeo localInstance = (eo == null) ? null : (EOImplantationGeo)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
