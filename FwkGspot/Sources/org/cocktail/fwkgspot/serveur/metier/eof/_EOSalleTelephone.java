// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSalleTelephone.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSalleTelephone extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_SalleTelephone";

	// Attributes
	public static final String NO_TELEPHONE_KEY = "noTelephone";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EOSalleTelephone.class);

  public EOSalleTelephone localInstanceIn(EOEditingContext editingContext) {
    EOSalleTelephone localInstance = (EOSalleTelephone)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String noTelephone() {
    return (String) storedValueForKey("noTelephone");
  }

  public void setNoTelephone(String value) {
    if (_EOSalleTelephone.LOG.isDebugEnabled()) {
    	_EOSalleTelephone.LOG.debug( "updating noTelephone from " + noTelephone() + " to " + value);
    }
    takeStoredValueForKey(value, "noTelephone");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EOSalleTelephone.LOG.isDebugEnabled()) {
      _EOSalleTelephone.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  

  public static EOSalleTelephone createFwkGspot_SalleTelephone(EOEditingContext editingContext, String noTelephone
, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles) {
    EOSalleTelephone eo = (EOSalleTelephone) EOUtilities.createAndInsertInstance(editingContext, _EOSalleTelephone.ENTITY_NAME);    
		eo.setNoTelephone(noTelephone);
    eo.setToSallesRelationship(toSalles);
    return eo;
  }

  public static NSArray<EOSalleTelephone> fetchAllFwkGspot_SalleTelephones(EOEditingContext editingContext) {
    return _EOSalleTelephone.fetchAllFwkGspot_SalleTelephones(editingContext, null);
  }

  public static NSArray<EOSalleTelephone> fetchAllFwkGspot_SalleTelephones(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSalleTelephone.fetchFwkGspot_SalleTelephones(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSalleTelephone> fetchFwkGspot_SalleTelephones(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSalleTelephone.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSalleTelephone> eoObjects = (NSArray<EOSalleTelephone>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSalleTelephone fetchFwkGspot_SalleTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalleTelephone.fetchFwkGspot_SalleTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalleTelephone fetchFwkGspot_SalleTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSalleTelephone> eoObjects = _EOSalleTelephone.fetchFwkGspot_SalleTelephones(editingContext, qualifier, null);
    EOSalleTelephone eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSalleTelephone)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_SalleTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalleTelephone fetchRequiredFwkGspot_SalleTelephone(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalleTelephone.fetchRequiredFwkGspot_SalleTelephone(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalleTelephone fetchRequiredFwkGspot_SalleTelephone(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSalleTelephone eoObject = _EOSalleTelephone.fetchFwkGspot_SalleTelephone(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_SalleTelephone that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalleTelephone localInstanceIn(EOEditingContext editingContext, EOSalleTelephone eo) {
    EOSalleTelephone localInstance = (eo == null) ? null : (EOSalleTelephone)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
