package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSValidation;

public class EOTypeSalle extends _EOTypeSalle {
  private static Logger log = Logger.getLogger(EOTypeSalle.class);
  public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();

		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {		
		if (this.tsalLibelle() == null) {
			throw new NSValidation.ValidationException(
					"Le libelle est obligatoire !!");						
		}		
	}
}
