// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartTypeGroupe.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartTypeGroupe extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartTypeGroupe";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TO_STRUCTURE_ULRS_KEY = "toStructureUlrs";
	public static final String TO_TYPE_GROUPE_KEY = "toTypeGroupe";

  private static Logger LOG = Logger.getLogger(_EORepartTypeGroupe.class);

  public EORepartTypeGroupe localInstanceIn(EOEditingContext editingContext) {
    EORepartTypeGroupe localInstance = (EORepartTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Double persIdCreation() {
    return (Double) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Double value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Double persIdModification() {
    return (Double) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Double value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
    	_EORepartTypeGroupe.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlrs() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlrs");
  }

  public void setToStructureUlrsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
      _EORepartTypeGroupe.LOG.debug("updating toStructureUlrs from " + toStructureUlrs() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlrs();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlrs");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlrs");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe toTypeGroupe() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe)storedValueForKey("toTypeGroupe");
  }

  public void setToTypeGroupeRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe value) {
    if (_EORepartTypeGroupe.LOG.isDebugEnabled()) {
      _EORepartTypeGroupe.LOG.debug("updating toTypeGroupe from " + toTypeGroupe() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe oldValue = toTypeGroupe();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeGroupe");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeGroupe");
    }
  }
  

  public static EORepartTypeGroupe createFwkGspot_RepartTypeGroupe(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlrs, org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe toTypeGroupe) {
    EORepartTypeGroupe eo = (EORepartTypeGroupe) EOUtilities.createAndInsertInstance(editingContext, _EORepartTypeGroupe.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToStructureUlrsRelationship(toStructureUlrs);
    eo.setToTypeGroupeRelationship(toTypeGroupe);
    return eo;
  }

  public static NSArray<EORepartTypeGroupe> fetchAllFwkGspot_RepartTypeGroupes(EOEditingContext editingContext) {
    return _EORepartTypeGroupe.fetchAllFwkGspot_RepartTypeGroupes(editingContext, null);
  }

  public static NSArray<EORepartTypeGroupe> fetchAllFwkGspot_RepartTypeGroupes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartTypeGroupe.fetchFwkGspot_RepartTypeGroupes(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartTypeGroupe> fetchFwkGspot_RepartTypeGroupes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartTypeGroupe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartTypeGroupe> eoObjects = (NSArray<EORepartTypeGroupe>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartTypeGroupe fetchFwkGspot_RepartTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartTypeGroupe.fetchFwkGspot_RepartTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartTypeGroupe fetchFwkGspot_RepartTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartTypeGroupe> eoObjects = _EORepartTypeGroupe.fetchFwkGspot_RepartTypeGroupes(editingContext, qualifier, null);
    EORepartTypeGroupe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartTypeGroupe)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartTypeGroupe fetchRequiredFwkGspot_RepartTypeGroupe(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartTypeGroupe.fetchRequiredFwkGspot_RepartTypeGroupe(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartTypeGroupe fetchRequiredFwkGspot_RepartTypeGroupe(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartTypeGroupe eoObject = _EORepartTypeGroupe.fetchFwkGspot_RepartTypeGroupe(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartTypeGroupe localInstanceIn(EOEditingContext editingContext, EORepartTypeGroupe eo) {
    EORepartTypeGroupe localInstance = (eo == null) ? null : (EORepartTypeGroupe)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
