// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartLotIndividu.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartLotIndividu extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartLotIndividu";

	// Attributes

	// Relationships
	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_LOT_SALLE_KEY = "toLotSalle";

  private static Logger LOG = Logger.getLogger(_EORepartLotIndividu.class);

  public EORepartLotIndividu localInstanceIn(EOEditingContext editingContext) {
    EORepartLotIndividu localInstance = (EORepartLotIndividu)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
  }

  public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EORepartLotIndividu.LOG.isDebugEnabled()) {
      _EORepartLotIndividu.LOG.debug("updating toFwkpers_Individu from " + toFwkpers_Individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle toLotSalle() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle)storedValueForKey("toLotSalle");
  }

  public void setToLotSalleRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle value) {
    if (_EORepartLotIndividu.LOG.isDebugEnabled()) {
      _EORepartLotIndividu.LOG.debug("updating toLotSalle from " + toLotSalle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle oldValue = toLotSalle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLotSalle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLotSalle");
    }
  }
  

  public static EORepartLotIndividu createFwkGspot_RepartLotIndividu(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu, org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle toLotSalle) {
    EORepartLotIndividu eo = (EORepartLotIndividu) EOUtilities.createAndInsertInstance(editingContext, _EORepartLotIndividu.ENTITY_NAME);    
    eo.setToFwkpers_IndividuRelationship(toFwkpers_Individu);
    eo.setToLotSalleRelationship(toLotSalle);
    return eo;
  }

  public static NSArray<EORepartLotIndividu> fetchAllFwkGspot_RepartLotIndividus(EOEditingContext editingContext) {
    return _EORepartLotIndividu.fetchAllFwkGspot_RepartLotIndividus(editingContext, null);
  }

  public static NSArray<EORepartLotIndividu> fetchAllFwkGspot_RepartLotIndividus(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartLotIndividu.fetchFwkGspot_RepartLotIndividus(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartLotIndividu> fetchFwkGspot_RepartLotIndividus(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartLotIndividu.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartLotIndividu> eoObjects = (NSArray<EORepartLotIndividu>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartLotIndividu fetchFwkGspot_RepartLotIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLotIndividu.fetchFwkGspot_RepartLotIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartLotIndividu fetchFwkGspot_RepartLotIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartLotIndividu> eoObjects = _EORepartLotIndividu.fetchFwkGspot_RepartLotIndividus(editingContext, qualifier, null);
    EORepartLotIndividu eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartLotIndividu)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartLotIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartLotIndividu fetchRequiredFwkGspot_RepartLotIndividu(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLotIndividu.fetchRequiredFwkGspot_RepartLotIndividu(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartLotIndividu fetchRequiredFwkGspot_RepartLotIndividu(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartLotIndividu eoObject = _EORepartLotIndividu.fetchFwkGspot_RepartLotIndividu(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartLotIndividu that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartLotIndividu localInstanceIn(EOEditingContext editingContext, EORepartLotIndividu eo) {
    EORepartLotIndividu localInstance = (eo == null) ? null : (EORepartLotIndividu)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
