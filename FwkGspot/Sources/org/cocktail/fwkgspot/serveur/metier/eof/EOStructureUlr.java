package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

public class EOStructureUlr extends _EOStructureUlr {
	private static Logger log = Logger.getLogger(EOStructureUlr.class);
	public static final String PATH_KEY = "path";
	public static final String PATH__COURT_KEY = "pathCourt";
	public static final EOQualifier QUAL_GROUPES_SERVICES = new EOKeyValueQualifier(
			EOStructureUlr.TO_REPART_TYPE_GROUPE_KEY + "."
					+ EORepartTypeGroupe.TO_TYPE_GROUPE_KEY + "."
					+ EOTypeGroupe.TGRP_CODE_KEY,
			EOQualifier.QualifierOperatorEqual, EOTypeGroupe.TGRP_CODE_S);

	public String path() {

		return buildPath(EOStructureUlr.LL_STRUCTURE_KEY);
	}

	public String pathCourt() {
		return buildPath(EOStructureUlr.LC_STRUCTURE_KEY);
	}

	private String buildPath(String key) {
		String retour = "";
		EOStructureUlr pere = this.toStructureUlrPere();
		while (pere != null) {
			retour = pere.valueForKey(key) + "/" + retour;
			if (pere.equals(pere.toStructureUlrPere()))
				pere = null;
			else
				pere = pere.toStructureUlrPere();
		}
		return retour.toLowerCase();
	}
}
