// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartStructure.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartStructure extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartStructure";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

	// Relationships
	public static final String TO_FWKPERS__INDIVIDU_KEY = "toFwkpers_Individu";
	public static final String TO_STRUCTURE_ULR_KEY = "toStructureUlr";

  private static Logger LOG = Logger.getLogger(_EORepartStructure.class);

  public EORepartStructure localInstanceIn(EOEditingContext editingContext) {
    EORepartStructure localInstance = (EORepartStructure)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Double persIdCreation() {
    return (Double) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Double value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Double persIdModification() {
    return (Double) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Double value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
    	_EORepartStructure.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey("toFwkpers_Individu");
  }

  public void setToFwkpers_IndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
      _EORepartStructure.LOG.debug("updating toFwkpers_Individu from " + toFwkpers_Individu() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toFwkpers_Individu();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Individu");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Individu");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlr");
  }

  public void setToStructureUlrRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EORepartStructure.LOG.isDebugEnabled()) {
      _EORepartStructure.LOG.debug("updating toStructureUlr from " + toStructureUlr() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlr");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlr");
    }
  }
  

  public static EORepartStructure createFwkGspot_RepartStructure(EOEditingContext editingContext, org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toFwkpers_Individu, org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr) {
    EORepartStructure eo = (EORepartStructure) EOUtilities.createAndInsertInstance(editingContext, _EORepartStructure.ENTITY_NAME);    
    eo.setToFwkpers_IndividuRelationship(toFwkpers_Individu);
    eo.setToStructureUlrRelationship(toStructureUlr);
    return eo;
  }

  public static NSArray<EORepartStructure> fetchAllFwkGspot_RepartStructures(EOEditingContext editingContext) {
    return _EORepartStructure.fetchAllFwkGspot_RepartStructures(editingContext, null);
  }

  public static NSArray<EORepartStructure> fetchAllFwkGspot_RepartStructures(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartStructure.fetchFwkGspot_RepartStructures(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartStructure> fetchFwkGspot_RepartStructures(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartStructure.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartStructure> eoObjects = (NSArray<EORepartStructure>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartStructure fetchFwkGspot_RepartStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartStructure.fetchFwkGspot_RepartStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartStructure fetchFwkGspot_RepartStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartStructure> eoObjects = _EORepartStructure.fetchFwkGspot_RepartStructures(editingContext, qualifier, null);
    EORepartStructure eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartStructure)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartStructure fetchRequiredFwkGspot_RepartStructure(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartStructure.fetchRequiredFwkGspot_RepartStructure(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartStructure fetchRequiredFwkGspot_RepartStructure(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartStructure eoObject = _EORepartStructure.fetchFwkGspot_RepartStructure(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartStructure that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartStructure localInstanceIn(EOEditingContext editingContext, EORepartStructure eo) {
    EORepartStructure localInstance = (eo == null) ? null : (EORepartStructure)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
