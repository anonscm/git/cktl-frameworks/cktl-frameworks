// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOSallePorte.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOSallePorte extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_SallePorte";

	// Attributes
	public static final String SAL_CB_KEY = "salCb";
	public static final String SAL_PORTE_KEY = "salPorte";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EOSallePorte.class);

  public EOSallePorte localInstanceIn(EOEditingContext editingContext) {
    EOSallePorte localInstance = (EOSallePorte)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String salCb() {
    return (String) storedValueForKey("salCb");
  }

  public void setSalCb(String value) {
    if (_EOSallePorte.LOG.isDebugEnabled()) {
    	_EOSallePorte.LOG.debug( "updating salCb from " + salCb() + " to " + value);
    }
    takeStoredValueForKey(value, "salCb");
  }

  public String salPorte() {
    return (String) storedValueForKey("salPorte");
  }

  public void setSalPorte(String value) {
    if (_EOSallePorte.LOG.isDebugEnabled()) {
    	_EOSallePorte.LOG.debug( "updating salPorte from " + salPorte() + " to " + value);
    }
    takeStoredValueForKey(value, "salPorte");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EOSallePorte.LOG.isDebugEnabled()) {
      _EOSallePorte.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  

  public static EOSallePorte createFwkGspot_SallePorte(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles) {
    EOSallePorte eo = (EOSallePorte) EOUtilities.createAndInsertInstance(editingContext, _EOSallePorte.ENTITY_NAME);    
    eo.setToSallesRelationship(toSalles);
    return eo;
  }

  public static NSArray<EOSallePorte> fetchAllFwkGspot_SallePortes(EOEditingContext editingContext) {
    return _EOSallePorte.fetchAllFwkGspot_SallePortes(editingContext, null);
  }

  public static NSArray<EOSallePorte> fetchAllFwkGspot_SallePortes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSallePorte.fetchFwkGspot_SallePortes(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSallePorte> fetchFwkGspot_SallePortes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSallePorte.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSallePorte> eoObjects = (NSArray<EOSallePorte>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSallePorte fetchFwkGspot_SallePorte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSallePorte.fetchFwkGspot_SallePorte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSallePorte fetchFwkGspot_SallePorte(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSallePorte> eoObjects = _EOSallePorte.fetchFwkGspot_SallePortes(editingContext, qualifier, null);
    EOSallePorte eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSallePorte)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_SallePorte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSallePorte fetchRequiredFwkGspot_SallePorte(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSallePorte.fetchRequiredFwkGspot_SallePorte(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSallePorte fetchRequiredFwkGspot_SallePorte(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSallePorte eoObject = _EOSallePorte.fetchFwkGspot_SallePorte(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_SallePorte that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSallePorte localInstanceIn(EOEditingContext editingContext, EOSallePorte eo) {
    EOSallePorte localInstance = (eo == null) ? null : (EOSallePorte)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
