// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOResaTypeObjet.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOResaTypeObjet extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_ResaTypeObjet";

	// Attributes
	public static final String RTO_COMMENTAIRE_KEY = "rtoCommentaire";
	public static final String RTO_LIBELLE_KEY = "rtoLibelle";

	// Relationships
	public static final String TO_RESA_FAMILLE_OBJET_KEY = "toResaFamilleObjet";
	public static final String TO_RESA_OBJETS_KEY = "toResaObjets";

  private static Logger LOG = Logger.getLogger(_EOResaTypeObjet.class);

  public EOResaTypeObjet localInstanceIn(EOEditingContext editingContext) {
    EOResaTypeObjet localInstance = (EOResaTypeObjet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String rtoCommentaire() {
    return (String) storedValueForKey("rtoCommentaire");
  }

  public void setRtoCommentaire(String value) {
    if (_EOResaTypeObjet.LOG.isDebugEnabled()) {
    	_EOResaTypeObjet.LOG.debug( "updating rtoCommentaire from " + rtoCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "rtoCommentaire");
  }

  public String rtoLibelle() {
    return (String) storedValueForKey("rtoLibelle");
  }

  public void setRtoLibelle(String value) {
    if (_EOResaTypeObjet.LOG.isDebugEnabled()) {
    	_EOResaTypeObjet.LOG.debug( "updating rtoLibelle from " + rtoLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "rtoLibelle");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet toResaFamilleObjet() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet)storedValueForKey("toResaFamilleObjet");
  }

  public void setToResaFamilleObjetRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet value) {
    if (_EOResaTypeObjet.LOG.isDebugEnabled()) {
      _EOResaTypeObjet.LOG.debug("updating toResaFamilleObjet from " + toResaFamilleObjet() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet oldValue = toResaFamilleObjet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toResaFamilleObjet");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toResaFamilleObjet");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)storedValueForKey("toResaObjets");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier) {
    return toResaObjets(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier, boolean fetch) {
    return toResaObjets(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> toResaObjets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet.TO_RESA_TYPE_OBJET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet.fetchFwkGspot_ResaObjets(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toResaObjets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    if (_EOResaTypeObjet.LOG.isDebugEnabled()) {
      _EOResaTypeObjet.LOG.debug("adding " + object + " to toResaObjets relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toResaObjets");
  }

  public void removeFromToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    if (_EOResaTypeObjet.LOG.isDebugEnabled()) {
      _EOResaTypeObjet.LOG.debug("removing " + object + " from toResaObjets relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaObjets");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet createToResaObjetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_ResaObjet");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toResaObjets");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet) eo;
  }

  public void deleteToResaObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaObjets");
    editingContext().deleteObject(object);
  }

  public void deleteAllToResaObjetsRelationships() {
    Enumeration objects = toResaObjets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToResaObjetsRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOResaObjet)objects.nextElement());
    }
  }


  public static EOResaTypeObjet createFwkGspot_ResaTypeObjet(EOEditingContext editingContext, String rtoLibelle
, org.cocktail.fwkgspot.serveur.metier.eof.EOResaFamilleObjet toResaFamilleObjet) {
    EOResaTypeObjet eo = (EOResaTypeObjet) EOUtilities.createAndInsertInstance(editingContext, _EOResaTypeObjet.ENTITY_NAME);    
		eo.setRtoLibelle(rtoLibelle);
    eo.setToResaFamilleObjetRelationship(toResaFamilleObjet);
    return eo;
  }

  public static NSArray<EOResaTypeObjet> fetchAllFwkGspot_ResaTypeObjets(EOEditingContext editingContext) {
    return _EOResaTypeObjet.fetchAllFwkGspot_ResaTypeObjets(editingContext, null);
  }

  public static NSArray<EOResaTypeObjet> fetchAllFwkGspot_ResaTypeObjets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOResaTypeObjet.fetchFwkGspot_ResaTypeObjets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOResaTypeObjet> fetchFwkGspot_ResaTypeObjets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOResaTypeObjet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOResaTypeObjet> eoObjects = (NSArray<EOResaTypeObjet>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOResaTypeObjet fetchFwkGspot_ResaTypeObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaTypeObjet.fetchFwkGspot_ResaTypeObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaTypeObjet fetchFwkGspot_ResaTypeObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOResaTypeObjet> eoObjects = _EOResaTypeObjet.fetchFwkGspot_ResaTypeObjets(editingContext, qualifier, null);
    EOResaTypeObjet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOResaTypeObjet)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_ResaTypeObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaTypeObjet fetchRequiredFwkGspot_ResaTypeObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaTypeObjet.fetchRequiredFwkGspot_ResaTypeObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaTypeObjet fetchRequiredFwkGspot_ResaTypeObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    EOResaTypeObjet eoObject = _EOResaTypeObjet.fetchFwkGspot_ResaTypeObjet(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_ResaTypeObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaTypeObjet localInstanceIn(EOEditingContext editingContext, EOResaTypeObjet eo) {
    EOResaTypeObjet localInstance = (eo == null) ? null : (EOResaTypeObjet)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
