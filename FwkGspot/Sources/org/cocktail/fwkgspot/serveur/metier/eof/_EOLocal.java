// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOLocal.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOLocal extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_Local";

	// Attributes
	public static final String ADRESSE_INTERNE_KEY = "adresseInterne";
	public static final String APPELLATION_KEY = "appellation";
	public static final String C_LOCAL_KEY = "cLocal";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_DEB_VAL_KEY = "dDebVal";
	public static final String D_FIN_VAL_KEY = "dFinVal";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_ADRESSE_ADMIN_KEY = "idAdresseAdmin";

	// Relationships
	public static final String TO_FWKPERS__ADRESSE_KEY = "toFwkpers_Adresse";
	public static final String TO_REPART_BAT_IMP_GEOS_KEY = "toRepartBatImpGeos";
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_V_ETAGE_LOCALS_KEY = "toVEtageLocals";
	public static final String TO_V_TYPE_SALLE_LOCAL_ETAGES_KEY = "toVTypeSalleLocalEtages";

  private static Logger LOG = Logger.getLogger(_EOLocal.class);

  public EOLocal localInstanceIn(EOEditingContext editingContext) {
    EOLocal localInstance = (EOLocal)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String adresseInterne() {
    return (String) storedValueForKey("adresseInterne");
  }

  public void setAdresseInterne(String value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating adresseInterne from " + adresseInterne() + " to " + value);
    }
    takeStoredValueForKey(value, "adresseInterne");
  }

  public String appellation() {
    return (String) storedValueForKey("appellation");
  }

  public void setAppellation(String value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating appellation from " + appellation() + " to " + value);
    }
    takeStoredValueForKey(value, "appellation");
  }

  public String cLocal() {
    return (String) storedValueForKey("cLocal");
  }

  public void setCLocal(String value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating cLocal from " + cLocal() + " to " + value);
    }
    takeStoredValueForKey(value, "cLocal");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dDebVal() {
    return (NSTimestamp) storedValueForKey("dDebVal");
  }

  public void setDDebVal(NSTimestamp value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating dDebVal from " + dDebVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dDebVal");
  }

  public NSTimestamp dFinVal() {
    return (NSTimestamp) storedValueForKey("dFinVal");
  }

  public void setDFinVal(NSTimestamp value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating dFinVal from " + dFinVal() + " to " + value);
    }
    takeStoredValueForKey(value, "dFinVal");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public Long idAdresseAdmin() {
    return (Long) storedValueForKey("idAdresseAdmin");
  }

  public void setIdAdresseAdmin(Long value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
    	_EOLocal.LOG.debug( "updating idAdresseAdmin from " + idAdresseAdmin() + " to " + value);
    }
    takeStoredValueForKey(value, "idAdresseAdmin");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOAdresse toFwkpers_Adresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOAdresse)storedValueForKey("toFwkpers_Adresse");
  }

  public void setToFwkpers_AdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOAdresse value) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("updating toFwkpers_Adresse from " + toFwkpers_Adresse() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOAdresse oldValue = toFwkpers_Adresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Adresse");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Adresse");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)storedValueForKey("toRepartBatImpGeos");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier) {
    return toRepartBatImpGeos(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier, boolean fetch) {
    return toRepartBatImpGeos(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> toRepartBatImpGeos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo.TO_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo.fetchFwkGspot_RepartBatImpGeos(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartBatImpGeos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("adding " + object + " to toRepartBatImpGeos relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
  }

  public void removeFromToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("removing " + object + " from toRepartBatImpGeos relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo createToRepartBatImpGeosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartBatImpGeo");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartBatImpGeos");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo) eo;
  }

  public void deleteToRepartBatImpGeosRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBatImpGeos");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartBatImpGeosRelationships() {
    Enumeration objects = toRepartBatImpGeos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartBatImpGeosRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)storedValueForKey("toSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier) {
    return toSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, boolean fetch) {
    return toSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.TO_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.fetchFwkGspot_Salleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("adding " + object + " to toSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public void removeFromToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("removing " + object + " from toSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles createToSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_Salles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles) eo;
  }

  public void deleteToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSallesRelationships() {
    Enumeration objects = toSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal> toVEtageLocals() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal>)storedValueForKey("toVEtageLocals");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal> toVEtageLocals(EOQualifier qualifier) {
    return toVEtageLocals(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal> toVEtageLocals(EOQualifier qualifier, boolean fetch) {
    return toVEtageLocals(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal> toVEtageLocals(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal.TO_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal.fetchFwkGspot_VEtageLocals(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVEtageLocals();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVEtageLocalsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("adding " + object + " to toVEtageLocals relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toVEtageLocals");
  }

  public void removeFromToVEtageLocalsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("removing " + object + " from toVEtageLocals relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVEtageLocals");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal createToVEtageLocalsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_VEtageLocal");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toVEtageLocals");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal) eo;
  }

  public void deleteToVEtageLocalsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVEtageLocals");
    editingContext().deleteObject(object);
  }

  public void deleteAllToVEtageLocalsRelationships() {
    Enumeration objects = toVEtageLocals().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVEtageLocalsRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)storedValueForKey("toVTypeSalleLocalEtages");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier) {
    return toVTypeSalleLocalEtages(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier, boolean fetch) {
    return toVTypeSalleLocalEtages(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> toVTypeSalleLocalEtages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage.TO_LOCAL_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtages(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toVTypeSalleLocalEtages();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("adding " + object + " to toVTypeSalleLocalEtages relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
  }

  public void removeFromToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    if (_EOLocal.LOG.isDebugEnabled()) {
      _EOLocal.LOG.debug("removing " + object + " from toVTypeSalleLocalEtages relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage createToVTypeSalleLocalEtagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_VTypeSalleLocalEtage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toVTypeSalleLocalEtages");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage) eo;
  }

  public void deleteToVTypeSalleLocalEtagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toVTypeSalleLocalEtages");
    editingContext().deleteObject(object);
  }

  public void deleteAllToVTypeSalleLocalEtagesRelationships() {
    Enumeration objects = toVTypeSalleLocalEtages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToVTypeSalleLocalEtagesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage)objects.nextElement());
    }
  }


  public static EOLocal createFwkGspot_Local(EOEditingContext editingContext, String adresseInterne
, String cLocal
, NSTimestamp dCreation
, NSTimestamp dDebVal
, NSTimestamp dModification
) {
    EOLocal eo = (EOLocal) EOUtilities.createAndInsertInstance(editingContext, _EOLocal.ENTITY_NAME);    
		eo.setAdresseInterne(adresseInterne);
		eo.setCLocal(cLocal);
		eo.setDCreation(dCreation);
		eo.setDDebVal(dDebVal);
		eo.setDModification(dModification);
    return eo;
  }

  public static NSArray<EOLocal> fetchAllFwkGspot_Locals(EOEditingContext editingContext) {
    return _EOLocal.fetchAllFwkGspot_Locals(editingContext, null);
  }

  public static NSArray<EOLocal> fetchAllFwkGspot_Locals(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOLocal.fetchFwkGspot_Locals(editingContext, null, sortOrderings);
  }

  public static NSArray<EOLocal> fetchFwkGspot_Locals(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOLocal.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOLocal> eoObjects = (NSArray<EOLocal>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOLocal fetchFwkGspot_Local(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLocal.fetchFwkGspot_Local(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLocal fetchFwkGspot_Local(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOLocal> eoObjects = _EOLocal.fetchFwkGspot_Locals(editingContext, qualifier, null);
    EOLocal eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOLocal)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_Local that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLocal fetchRequiredFwkGspot_Local(EOEditingContext editingContext, String keyName, Object value) {
    return _EOLocal.fetchRequiredFwkGspot_Local(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOLocal fetchRequiredFwkGspot_Local(EOEditingContext editingContext, EOQualifier qualifier) {
    EOLocal eoObject = _EOLocal.fetchFwkGspot_Local(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_Local that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOLocal localInstanceIn(EOEditingContext editingContext, EOLocal eo) {
    EOLocal localInstance = (eo == null) ? null : (EOLocal)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
