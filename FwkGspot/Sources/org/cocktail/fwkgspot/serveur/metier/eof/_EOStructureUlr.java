// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOStructureUlr.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOStructureUlr extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_StructureUlr";

	// Attributes
	public static final String C_ACADEMIE_KEY = "cAcademie";
	public static final String C_NAF_KEY = "cNaf";
	public static final String C_NIC_KEY = "cNic";
	public static final String C_RNE_KEY = "cRne";
	public static final String C_STATUT_JURIDIQUE_KEY = "cStatutJuridique";
	public static final String C_TYPE_DECISION_STR_KEY = "cTypeDecisionStr";
	public static final String C_TYPE_ETABLISSEMEN_KEY = "cTypeEtablissemen";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String DATE_DECISION_KEY = "dateDecision";
	public static final String DATE_FERMETURE_KEY = "dateFermeture";
	public static final String DATE_OUVERTURE_KEY = "dateOuverture";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETAB_CODURSSAF_KEY = "etabCodurssaf";
	public static final String ETAB_NUMURSSAF_KEY = "etabNumurssaf";
	public static final String EXPORT_KEY = "export";
	public static final String GENCOD_KEY = "gencod";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String GRP_ALIAS_KEY = "grpAlias";
	public static final String GRP_APE_CODE_KEY = "grpApeCode";
	public static final String GRP_APE_CODE_BIS_KEY = "grpApeCodeBis";
	public static final String GRP_APE_CODE_COMP_KEY = "grpApeCodeComp";
	public static final String GRP_CA_KEY = "grpCa";
	public static final String GRP_CAPITAL_KEY = "grpCapital";
	public static final String GRP_CENTRE_DECISION_KEY = "grpCentreDecision";
	public static final String GRP_EFFECTIFS_KEY = "grpEffectifs";
	public static final String GRP_FONCTION1_KEY = "grpFonction1";
	public static final String GRP_FONCTION2_KEY = "grpFonction2";
	public static final String GRP_FORME_JURIDIQUE_KEY = "grpFormeJuridique";
	public static final String GRP_MOTS_CLEFS_KEY = "grpMotsClefs";
	public static final String GRP_OWNER_KEY = "grpOwner";
	public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
	public static final String GRP_RESPONSABLE_KEY = "grpResponsable";
	public static final String GRP_TRADEMARQUE_KEY = "grpTrademarque";
	public static final String GRP_WEBMESTRE_KEY = "grpWebmestre";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String MOYENNE_AGE_KEY = "moyenneAge";
	public static final String NUM_ASSEDIC_KEY = "numAssedic";
	public static final String NUM_CNRACL_KEY = "numCnracl";
	public static final String NUM_IRCANTEC_KEY = "numIrcantec";
	public static final String NUM_RAFP_KEY = "numRafp";
	public static final String ORG_ORDRE_KEY = "orgOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PSEC_ORDRE_KEY = "psecOrdre";
	public static final String REF_DECISION_KEY = "refDecision";
	public static final String REF_EXT_COMP_KEY = "refExtComp";
	public static final String REF_EXT_CR_KEY = "refExtCr";
	public static final String REF_EXT_ETAB_KEY = "refExtEtab";
	public static final String RISQUE_ACC_TRAV_KEY = "risqueAccTrav";
	public static final String SIREN_KEY = "siren";
	public static final String SIRET_KEY = "siret";
	public static final String STR_ACCUEIL_KEY = "strAccueil";
	public static final String STR_ACTIVITE_KEY = "strActivite";
	public static final String STR_AFFICHAGE_KEY = "strAffichage";
	public static final String STR_ORIGINE_KEY = "strOrigine";
	public static final String STR_PHOTO_KEY = "strPhoto";
	public static final String STR_RECHERCHE_KEY = "strRecherche";
	public static final String TAUX_ACC_TRAV_KEY = "tauxAccTrav";
	public static final String TAUX_EXONERATION_TVA_KEY = "tauxExonerationTva";
	public static final String TAUX_IR_KEY = "tauxIr";
	public static final String TAUX_TRANSPORT_KEY = "tauxTransport";
	public static final String TEM_COTIS_ASSEDIC_KEY = "temCotisAssedic";
	public static final String TEM_DADS_KEY = "temDads";
	public static final String TEM_ETABLISSEMENT_PAYE_KEY = "temEtablissementPaye";
	public static final String TEM_PLAFOND_REDUIT_KEY = "temPlafondReduit";
	public static final String TEM_SECTORISE_KEY = "temSectorise";
	public static final String TEM_SOUMIS_TVA_KEY = "temSoumisTva";
	public static final String TEM_VALIDE_KEY = "temValide";
	public static final String TVA_INTRACOM_KEY = "tvaIntracom";

	// Relationships
	public static final String TO_DEPOSITAIRE_SALLES_KEY = "toDepositaireSalles";
	public static final String TO_DETAIL_POURCENTAGES_KEY = "toDetailPourcentages";
	public static final String TO_FWKPERS__STRUCTURE_KEY = "toFwkpers_Structure";
	public static final String TO_REPART_BUREAU_KEY = "toRepartBureau";
	public static final String TO_REPART_STRUCTURE_KEY = "toRepartStructure";
	public static final String TO_REPART_TYPE_GROUPE_KEY = "toRepartTypeGroupe";
	public static final String TO_SSTRUCTURE_ULR_FILS_KEY = "toSstructureUlrFils";
	public static final String TO_STRUCTURE_ULR_PERE_KEY = "toStructureUlrPere";

  private static Logger LOG = Logger.getLogger(_EOStructureUlr.class);

  public EOStructureUlr localInstanceIn(EOEditingContext editingContext) {
    EOStructureUlr localInstance = (EOStructureUlr)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String cAcademie() {
    return (String) storedValueForKey("cAcademie");
  }

  public void setCAcademie(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cAcademie from " + cAcademie() + " to " + value);
    }
    takeStoredValueForKey(value, "cAcademie");
  }

  public String cNaf() {
    return (String) storedValueForKey("cNaf");
  }

  public void setCNaf(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cNaf from " + cNaf() + " to " + value);
    }
    takeStoredValueForKey(value, "cNaf");
  }

  public String cNic() {
    return (String) storedValueForKey("cNic");
  }

  public void setCNic(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cNic from " + cNic() + " to " + value);
    }
    takeStoredValueForKey(value, "cNic");
  }

  public String cRne() {
    return (String) storedValueForKey("cRne");
  }

  public void setCRne(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cRne from " + cRne() + " to " + value);
    }
    takeStoredValueForKey(value, "cRne");
  }

  public String cStatutJuridique() {
    return (String) storedValueForKey("cStatutJuridique");
  }

  public void setCStatutJuridique(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cStatutJuridique from " + cStatutJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "cStatutJuridique");
  }

  public String cTypeDecisionStr() {
    return (String) storedValueForKey("cTypeDecisionStr");
  }

  public void setCTypeDecisionStr(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cTypeDecisionStr from " + cTypeDecisionStr() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeDecisionStr");
  }

  public String cTypeEtablissemen() {
    return (String) storedValueForKey("cTypeEtablissemen");
  }

  public void setCTypeEtablissemen(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cTypeEtablissemen from " + cTypeEtablissemen() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeEtablissemen");
  }

  public String cTypeStructure() {
    return (String) storedValueForKey("cTypeStructure");
  }

  public void setCTypeStructure(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating cTypeStructure from " + cTypeStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "cTypeStructure");
  }

  public NSTimestamp dateDecision() {
    return (NSTimestamp) storedValueForKey("dateDecision");
  }

  public void setDateDecision(NSTimestamp value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating dateDecision from " + dateDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "dateDecision");
  }

  public NSTimestamp dateFermeture() {
    return (NSTimestamp) storedValueForKey("dateFermeture");
  }

  public void setDateFermeture(NSTimestamp value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating dateFermeture from " + dateFermeture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateFermeture");
  }

  public NSTimestamp dateOuverture() {
    return (NSTimestamp) storedValueForKey("dateOuverture");
  }

  public void setDateOuverture(NSTimestamp value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating dateOuverture from " + dateOuverture() + " to " + value);
    }
    takeStoredValueForKey(value, "dateOuverture");
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public String etabCodurssaf() {
    return (String) storedValueForKey("etabCodurssaf");
  }

  public void setEtabCodurssaf(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating etabCodurssaf from " + etabCodurssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "etabCodurssaf");
  }

  public String etabNumurssaf() {
    return (String) storedValueForKey("etabNumurssaf");
  }

  public void setEtabNumurssaf(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating etabNumurssaf from " + etabNumurssaf() + " to " + value);
    }
    takeStoredValueForKey(value, "etabNumurssaf");
  }

  public Double export() {
    return (Double) storedValueForKey("export");
  }

  public void setExport(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating export from " + export() + " to " + value);
    }
    takeStoredValueForKey(value, "export");
  }

  public String gencod() {
    return (String) storedValueForKey("gencod");
  }

  public void setGencod(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating gencod from " + gencod() + " to " + value);
    }
    takeStoredValueForKey(value, "gencod");
  }

  public String grpAcces() {
    return (String) storedValueForKey("grpAcces");
  }

  public void setGrpAcces(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpAcces from " + grpAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "grpAcces");
  }

  public String grpAlias() {
    return (String) storedValueForKey("grpAlias");
  }

  public void setGrpAlias(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpAlias from " + grpAlias() + " to " + value);
    }
    takeStoredValueForKey(value, "grpAlias");
  }

  public String grpApeCode() {
    return (String) storedValueForKey("grpApeCode");
  }

  public void setGrpApeCode(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpApeCode from " + grpApeCode() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCode");
  }

  public String grpApeCodeBis() {
    return (String) storedValueForKey("grpApeCodeBis");
  }

  public void setGrpApeCodeBis(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpApeCodeBis from " + grpApeCodeBis() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCodeBis");
  }

  public String grpApeCodeComp() {
    return (String) storedValueForKey("grpApeCodeComp");
  }

  public void setGrpApeCodeComp(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpApeCodeComp from " + grpApeCodeComp() + " to " + value);
    }
    takeStoredValueForKey(value, "grpApeCodeComp");
  }

  public Double grpCa() {
    return (Double) storedValueForKey("grpCa");
  }

  public void setGrpCa(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpCa from " + grpCa() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCa");
  }

  public Double grpCapital() {
    return (Double) storedValueForKey("grpCapital");
  }

  public void setGrpCapital(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpCapital from " + grpCapital() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCapital");
  }

  public String grpCentreDecision() {
    return (String) storedValueForKey("grpCentreDecision");
  }

  public void setGrpCentreDecision(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpCentreDecision from " + grpCentreDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "grpCentreDecision");
  }

  public Double grpEffectifs() {
    return (Double) storedValueForKey("grpEffectifs");
  }

  public void setGrpEffectifs(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpEffectifs from " + grpEffectifs() + " to " + value);
    }
    takeStoredValueForKey(value, "grpEffectifs");
  }

  public String grpFonction1() {
    return (String) storedValueForKey("grpFonction1");
  }

  public void setGrpFonction1(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpFonction1 from " + grpFonction1() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFonction1");
  }

  public String grpFonction2() {
    return (String) storedValueForKey("grpFonction2");
  }

  public void setGrpFonction2(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpFonction2 from " + grpFonction2() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFonction2");
  }

  public String grpFormeJuridique() {
    return (String) storedValueForKey("grpFormeJuridique");
  }

  public void setGrpFormeJuridique(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpFormeJuridique from " + grpFormeJuridique() + " to " + value);
    }
    takeStoredValueForKey(value, "grpFormeJuridique");
  }

  public String grpMotsClefs() {
    return (String) storedValueForKey("grpMotsClefs");
  }

  public void setGrpMotsClefs(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpMotsClefs from " + grpMotsClefs() + " to " + value);
    }
    takeStoredValueForKey(value, "grpMotsClefs");
  }

  public Double grpOwner() {
    return (Double) storedValueForKey("grpOwner");
  }

  public void setGrpOwner(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpOwner from " + grpOwner() + " to " + value);
    }
    takeStoredValueForKey(value, "grpOwner");
  }

  public String grpResponsabilite() {
    return (String) storedValueForKey("grpResponsabilite");
  }

  public void setGrpResponsabilite(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpResponsabilite from " + grpResponsabilite() + " to " + value);
    }
    takeStoredValueForKey(value, "grpResponsabilite");
  }

  public Double grpResponsable() {
    return (Double) storedValueForKey("grpResponsable");
  }

  public void setGrpResponsable(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpResponsable from " + grpResponsable() + " to " + value);
    }
    takeStoredValueForKey(value, "grpResponsable");
  }

  public String grpTrademarque() {
    return (String) storedValueForKey("grpTrademarque");
  }

  public void setGrpTrademarque(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpTrademarque from " + grpTrademarque() + " to " + value);
    }
    takeStoredValueForKey(value, "grpTrademarque");
  }

  public String grpWebmestre() {
    return (String) storedValueForKey("grpWebmestre");
  }

  public void setGrpWebmestre(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating grpWebmestre from " + grpWebmestre() + " to " + value);
    }
    takeStoredValueForKey(value, "grpWebmestre");
  }

  public String lcStructure() {
    return (String) storedValueForKey("lcStructure");
  }

  public void setLcStructure(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating lcStructure from " + lcStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "lcStructure");
  }

  public String llStructure() {
    return (String) storedValueForKey("llStructure");
  }

  public void setLlStructure(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating llStructure from " + llStructure() + " to " + value);
    }
    takeStoredValueForKey(value, "llStructure");
  }

  public Double moyenneAge() {
    return (Double) storedValueForKey("moyenneAge");
  }

  public void setMoyenneAge(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating moyenneAge from " + moyenneAge() + " to " + value);
    }
    takeStoredValueForKey(value, "moyenneAge");
  }

  public String numAssedic() {
    return (String) storedValueForKey("numAssedic");
  }

  public void setNumAssedic(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating numAssedic from " + numAssedic() + " to " + value);
    }
    takeStoredValueForKey(value, "numAssedic");
  }

  public String numCnracl() {
    return (String) storedValueForKey("numCnracl");
  }

  public void setNumCnracl(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating numCnracl from " + numCnracl() + " to " + value);
    }
    takeStoredValueForKey(value, "numCnracl");
  }

  public String numIrcantec() {
    return (String) storedValueForKey("numIrcantec");
  }

  public void setNumIrcantec(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating numIrcantec from " + numIrcantec() + " to " + value);
    }
    takeStoredValueForKey(value, "numIrcantec");
  }

  public String numRafp() {
    return (String) storedValueForKey("numRafp");
  }

  public void setNumRafp(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating numRafp from " + numRafp() + " to " + value);
    }
    takeStoredValueForKey(value, "numRafp");
  }

  public Double orgOrdre() {
    return (Double) storedValueForKey("orgOrdre");
  }

  public void setOrgOrdre(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating orgOrdre from " + orgOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "orgOrdre");
  }

  public Double persId() {
    return (Double) storedValueForKey("persId");
  }

  public void setPersId(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, "persId");
  }

  public Double persIdCreation() {
    return (Double) storedValueForKey("persIdCreation");
  }

  public void setPersIdCreation(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdCreation");
  }

  public Double persIdModification() {
    return (Double) storedValueForKey("persIdModification");
  }

  public void setPersIdModification(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, "persIdModification");
  }

  public Double psecOrdre() {
    return (Double) storedValueForKey("psecOrdre");
  }

  public void setPsecOrdre(Double value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating psecOrdre from " + psecOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, "psecOrdre");
  }

  public String refDecision() {
    return (String) storedValueForKey("refDecision");
  }

  public void setRefDecision(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating refDecision from " + refDecision() + " to " + value);
    }
    takeStoredValueForKey(value, "refDecision");
  }

  public String refExtComp() {
    return (String) storedValueForKey("refExtComp");
  }

  public void setRefExtComp(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating refExtComp from " + refExtComp() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtComp");
  }

  public String refExtCr() {
    return (String) storedValueForKey("refExtCr");
  }

  public void setRefExtCr(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating refExtCr from " + refExtCr() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtCr");
  }

  public String refExtEtab() {
    return (String) storedValueForKey("refExtEtab");
  }

  public void setRefExtEtab(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating refExtEtab from " + refExtEtab() + " to " + value);
    }
    takeStoredValueForKey(value, "refExtEtab");
  }

  public String risqueAccTrav() {
    return (String) storedValueForKey("risqueAccTrav");
  }

  public void setRisqueAccTrav(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating risqueAccTrav from " + risqueAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "risqueAccTrav");
  }

  public String siren() {
    return (String) storedValueForKey("siren");
  }

  public void setSiren(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating siren from " + siren() + " to " + value);
    }
    takeStoredValueForKey(value, "siren");
  }

  public String siret() {
    return (String) storedValueForKey("siret");
  }

  public void setSiret(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating siret from " + siret() + " to " + value);
    }
    takeStoredValueForKey(value, "siret");
  }

  public String strAccueil() {
    return (String) storedValueForKey("strAccueil");
  }

  public void setStrAccueil(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strAccueil from " + strAccueil() + " to " + value);
    }
    takeStoredValueForKey(value, "strAccueil");
  }

  public String strActivite() {
    return (String) storedValueForKey("strActivite");
  }

  public void setStrActivite(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strActivite from " + strActivite() + " to " + value);
    }
    takeStoredValueForKey(value, "strActivite");
  }

  public String strAffichage() {
    return (String) storedValueForKey("strAffichage");
  }

  public void setStrAffichage(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strAffichage from " + strAffichage() + " to " + value);
    }
    takeStoredValueForKey(value, "strAffichage");
  }

  public String strOrigine() {
    return (String) storedValueForKey("strOrigine");
  }

  public void setStrOrigine(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strOrigine from " + strOrigine() + " to " + value);
    }
    takeStoredValueForKey(value, "strOrigine");
  }

  public String strPhoto() {
    return (String) storedValueForKey("strPhoto");
  }

  public void setStrPhoto(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strPhoto from " + strPhoto() + " to " + value);
    }
    takeStoredValueForKey(value, "strPhoto");
  }

  public String strRecherche() {
    return (String) storedValueForKey("strRecherche");
  }

  public void setStrRecherche(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating strRecherche from " + strRecherche() + " to " + value);
    }
    takeStoredValueForKey(value, "strRecherche");
  }

  public String tauxAccTrav() {
    return (String) storedValueForKey("tauxAccTrav");
  }

  public void setTauxAccTrav(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating tauxAccTrav from " + tauxAccTrav() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxAccTrav");
  }

  public String tauxExonerationTva() {
    return (String) storedValueForKey("tauxExonerationTva");
  }

  public void setTauxExonerationTva(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating tauxExonerationTva from " + tauxExonerationTva() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxExonerationTva");
  }

  public String tauxIr() {
    return (String) storedValueForKey("tauxIr");
  }

  public void setTauxIr(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating tauxIr from " + tauxIr() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxIr");
  }

  public String tauxTransport() {
    return (String) storedValueForKey("tauxTransport");
  }

  public void setTauxTransport(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating tauxTransport from " + tauxTransport() + " to " + value);
    }
    takeStoredValueForKey(value, "tauxTransport");
  }

  public String temCotisAssedic() {
    return (String) storedValueForKey("temCotisAssedic");
  }

  public void setTemCotisAssedic(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temCotisAssedic from " + temCotisAssedic() + " to " + value);
    }
    takeStoredValueForKey(value, "temCotisAssedic");
  }

  public String temDads() {
    return (String) storedValueForKey("temDads");
  }

  public void setTemDads(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temDads from " + temDads() + " to " + value);
    }
    takeStoredValueForKey(value, "temDads");
  }

  public String temEtablissementPaye() {
    return (String) storedValueForKey("temEtablissementPaye");
  }

  public void setTemEtablissementPaye(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temEtablissementPaye from " + temEtablissementPaye() + " to " + value);
    }
    takeStoredValueForKey(value, "temEtablissementPaye");
  }

  public String temPlafondReduit() {
    return (String) storedValueForKey("temPlafondReduit");
  }

  public void setTemPlafondReduit(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temPlafondReduit from " + temPlafondReduit() + " to " + value);
    }
    takeStoredValueForKey(value, "temPlafondReduit");
  }

  public String temSectorise() {
    return (String) storedValueForKey("temSectorise");
  }

  public void setTemSectorise(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temSectorise from " + temSectorise() + " to " + value);
    }
    takeStoredValueForKey(value, "temSectorise");
  }

  public String temSoumisTva() {
    return (String) storedValueForKey("temSoumisTva");
  }

  public void setTemSoumisTva(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temSoumisTva from " + temSoumisTva() + " to " + value);
    }
    takeStoredValueForKey(value, "temSoumisTva");
  }

  public String temValide() {
    return (String) storedValueForKey("temValide");
  }

  public void setTemValide(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating temValide from " + temValide() + " to " + value);
    }
    takeStoredValueForKey(value, "temValide");
  }

  public String tvaIntracom() {
    return (String) storedValueForKey("tvaIntracom");
  }

  public void setTvaIntracom(String value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
    	_EOStructureUlr.LOG.debug( "updating tvaIntracom from " + tvaIntracom() + " to " + value);
    }
    takeStoredValueForKey(value, "tvaIntracom");
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure toFwkpers_Structure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey("toFwkpers_Structure");
  }

  public void setToFwkpers_StructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("updating toFwkpers_Structure from " + toFwkpers_Structure() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = toFwkpers_Structure();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkpers_Structure");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkpers_Structure");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlrPere() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlrPere");
  }

  public void setToStructureUlrPereRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("updating toStructureUlrPere from " + toStructureUlrPere() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlrPere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlrPere");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlrPere");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)storedValueForKey("toDepositaireSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalles(EOQualifier qualifier) {
    return toDepositaireSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalles(EOQualifier qualifier, boolean fetch) {
    return toDepositaireSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> toDepositaireSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles.TO_STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles.fetchFwkGspot_DepositaireSalleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDepositaireSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDepositaireSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toDepositaireSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDepositaireSalles");
  }

  public void removeFromToDepositaireSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toDepositaireSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDepositaireSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles createToDepositaireSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_DepositaireSalles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDepositaireSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles) eo;
  }

  public void deleteToDepositaireSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDepositaireSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDepositaireSallesRelationships() {
    Enumeration objects = toDepositaireSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDepositaireSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)storedValueForKey("toDetailPourcentages");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier) {
    return toDetailPourcentages(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, boolean fetch) {
    return toDetailPourcentages(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> toDetailPourcentages(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.TO_STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage.fetchFwkGspot_DetailPourcentages(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toDetailPourcentages();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toDetailPourcentages relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public void removeFromToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toDetailPourcentages relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage createToDetailPourcentagesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_DetailPourcentage");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toDetailPourcentages");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage) eo;
  }

  public void deleteToDetailPourcentagesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toDetailPourcentages");
    editingContext().deleteObject(object);
  }

  public void deleteAllToDetailPourcentagesRelationships() {
    Enumeration objects = toDetailPourcentages().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToDetailPourcentagesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)storedValueForKey("toRepartBureau");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier) {
    return toRepartBureau(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier, boolean fetch) {
    return toRepartBureau(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> toRepartBureau(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau.TO_STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau.fetchFwkGspot_RepartBureaus(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartBureau();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toRepartBureau relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartBureau");
  }

  public void removeFromToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toRepartBureau relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBureau");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau createToRepartBureauRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartBureau");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartBureau");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau) eo;
  }

  public void deleteToRepartBureauRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartBureau");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartBureauRelationships() {
    Enumeration objects = toRepartBureau().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartBureauRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure> toRepartStructure() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure>)storedValueForKey("toRepartStructure");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure> toRepartStructure(EOQualifier qualifier) {
    return toRepartStructure(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure> toRepartStructure(EOQualifier qualifier, boolean fetch) {
    return toRepartStructure(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure> toRepartStructure(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure.TO_STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure.fetchFwkGspot_RepartStructures(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartStructure();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartStructureRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toRepartStructure relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartStructure");
  }

  public void removeFromToRepartStructureRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toRepartStructure relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartStructure");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure createToRepartStructureRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartStructure");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartStructure");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure) eo;
  }

  public void deleteToRepartStructureRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartStructure");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartStructureRelationships() {
    Enumeration objects = toRepartStructure().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartStructureRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe> toRepartTypeGroupe() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe>)storedValueForKey("toRepartTypeGroupe");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe> toRepartTypeGroupe(EOQualifier qualifier) {
    return toRepartTypeGroupe(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe> toRepartTypeGroupe(EOQualifier qualifier, boolean fetch) {
    return toRepartTypeGroupe(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe> toRepartTypeGroupe(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe.TO_STRUCTURE_ULRS_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe.fetchFwkGspot_RepartTypeGroupes(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartTypeGroupe();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartTypeGroupeRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toRepartTypeGroupe relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toRepartTypeGroupe");
  }

  public void removeFromToRepartTypeGroupeRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toRepartTypeGroupe relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartTypeGroupe");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe createToRepartTypeGroupeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_RepartTypeGroupe");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toRepartTypeGroupe");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe) eo;
  }

  public void deleteToRepartTypeGroupeRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toRepartTypeGroupe");
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartTypeGroupeRelationships() {
    Enumeration objects = toRepartTypeGroupe().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartTypeGroupeRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe)objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr> toSstructureUlrFils() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr>)storedValueForKey("toSstructureUlrFils");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr> toSstructureUlrFils(EOQualifier qualifier) {
    return toSstructureUlrFils(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr> toSstructureUlrFils(EOQualifier qualifier, boolean fetch) {
    return toSstructureUlrFils(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr> toSstructureUlrFils(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr.TO_STRUCTURE_ULR_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr.fetchFwkGspot_StructureUlrs(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSstructureUlrFils();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSstructureUlrFilsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("adding " + object + " to toSstructureUlrFils relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSstructureUlrFils");
  }

  public void removeFromToSstructureUlrFilsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr object) {
    if (_EOStructureUlr.LOG.isDebugEnabled()) {
      _EOStructureUlr.LOG.debug("removing " + object + " from toSstructureUlrFils relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSstructureUlrFils");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr createToSstructureUlrFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_StructureUlr");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSstructureUlrFils");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr) eo;
  }

  public void deleteToSstructureUlrFilsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSstructureUlrFils");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSstructureUlrFilsRelationships() {
    Enumeration objects = toSstructureUlrFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSstructureUlrFilsRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)objects.nextElement());
    }
  }


  public static EOStructureUlr createFwkGspot_StructureUlr(EOEditingContext editingContext, String cTypeStructure
, Double persId
, String temCotisAssedic
, String temDads
, String temSectorise
, String temSoumisTva
, String temValide
, org.cocktail.fwkcktlpersonne.common.metier.EOStructure toFwkpers_Structure) {
    EOStructureUlr eo = (EOStructureUlr) EOUtilities.createAndInsertInstance(editingContext, _EOStructureUlr.ENTITY_NAME);    
		eo.setCTypeStructure(cTypeStructure);
		eo.setPersId(persId);
		eo.setTemCotisAssedic(temCotisAssedic);
		eo.setTemDads(temDads);
		eo.setTemSectorise(temSectorise);
		eo.setTemSoumisTva(temSoumisTva);
		eo.setTemValide(temValide);
    eo.setToFwkpers_StructureRelationship(toFwkpers_Structure);
    return eo;
  }

  public static NSArray<EOStructureUlr> fetchAllFwkGspot_StructureUlrs(EOEditingContext editingContext) {
    return _EOStructureUlr.fetchAllFwkGspot_StructureUlrs(editingContext, null);
  }

  public static NSArray<EOStructureUlr> fetchAllFwkGspot_StructureUlrs(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOStructureUlr.fetchFwkGspot_StructureUlrs(editingContext, null, sortOrderings);
  }

  public static NSArray<EOStructureUlr> fetchFwkGspot_StructureUlrs(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOStructureUlr.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOStructureUlr> eoObjects = (NSArray<EOStructureUlr>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOStructureUlr fetchFwkGspot_StructureUlr(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureUlr.fetchFwkGspot_StructureUlr(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructureUlr fetchFwkGspot_StructureUlr(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOStructureUlr> eoObjects = _EOStructureUlr.fetchFwkGspot_StructureUlrs(editingContext, qualifier, null);
    EOStructureUlr eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_StructureUlr that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructureUlr fetchRequiredFwkGspot_StructureUlr(EOEditingContext editingContext, String keyName, Object value) {
    return _EOStructureUlr.fetchRequiredFwkGspot_StructureUlr(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOStructureUlr fetchRequiredFwkGspot_StructureUlr(EOEditingContext editingContext, EOQualifier qualifier) {
    EOStructureUlr eoObject = _EOStructureUlr.fetchFwkGspot_StructureUlr(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_StructureUlr that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOStructureUlr localInstanceIn(EOEditingContext editingContext, EOStructureUlr eo) {
    EOStructureUlr localInstance = (eo == null) ? null : (EOStructureUlr)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
