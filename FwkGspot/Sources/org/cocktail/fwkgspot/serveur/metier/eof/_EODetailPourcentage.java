// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODetailPourcentage.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODetailPourcentage extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_DetailPourcentage";

	// Attributes
	public static final String DET_POURCENTAGE_KEY = "detPourcentage";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_STRUCTURE_ULR_KEY = "toStructureUlr";
	public static final String TO_TYPE_OCCUPATION_KEY = "toTypeOccupation";

  private static Logger LOG = Logger.getLogger(_EODetailPourcentage.class);

  public EODetailPourcentage localInstanceIn(EOEditingContext editingContext) {
    EODetailPourcentage localInstance = (EODetailPourcentage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal detPourcentage() {
    return (java.math.BigDecimal) storedValueForKey("detPourcentage");
  }

  public void setDetPourcentage(java.math.BigDecimal value) {
    if (_EODetailPourcentage.LOG.isDebugEnabled()) {
    	_EODetailPourcentage.LOG.debug( "updating detPourcentage from " + detPourcentage() + " to " + value);
    }
    takeStoredValueForKey(value, "detPourcentage");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EODetailPourcentage.LOG.isDebugEnabled()) {
      _EODetailPourcentage.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlr");
  }

  public void setToStructureUlrRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EODetailPourcentage.LOG.isDebugEnabled()) {
      _EODetailPourcentage.LOG.debug("updating toStructureUlr from " + toStructureUlr() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlr");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlr");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation toTypeOccupation() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation)storedValueForKey("toTypeOccupation");
  }

  public void setToTypeOccupationRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation value) {
    if (_EODetailPourcentage.LOG.isDebugEnabled()) {
      _EODetailPourcentage.LOG.debug("updating toTypeOccupation from " + toTypeOccupation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation oldValue = toTypeOccupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeOccupation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeOccupation");
    }
  }
  

  public static EODetailPourcentage createFwkGspot_DetailPourcentage(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles, org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr, org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation toTypeOccupation) {
    EODetailPourcentage eo = (EODetailPourcentage) EOUtilities.createAndInsertInstance(editingContext, _EODetailPourcentage.ENTITY_NAME);    
    eo.setToSallesRelationship(toSalles);
    eo.setToStructureUlrRelationship(toStructureUlr);
    eo.setToTypeOccupationRelationship(toTypeOccupation);
    return eo;
  }

  public static NSArray<EODetailPourcentage> fetchAllFwkGspot_DetailPourcentages(EOEditingContext editingContext) {
    return _EODetailPourcentage.fetchAllFwkGspot_DetailPourcentages(editingContext, null);
  }

  public static NSArray<EODetailPourcentage> fetchAllFwkGspot_DetailPourcentages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODetailPourcentage.fetchFwkGspot_DetailPourcentages(editingContext, null, sortOrderings);
  }

  public static NSArray<EODetailPourcentage> fetchFwkGspot_DetailPourcentages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODetailPourcentage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODetailPourcentage> eoObjects = (NSArray<EODetailPourcentage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODetailPourcentage fetchFwkGspot_DetailPourcentage(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailPourcentage.fetchFwkGspot_DetailPourcentage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODetailPourcentage fetchFwkGspot_DetailPourcentage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODetailPourcentage> eoObjects = _EODetailPourcentage.fetchFwkGspot_DetailPourcentages(editingContext, qualifier, null);
    EODetailPourcentage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODetailPourcentage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_DetailPourcentage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODetailPourcentage fetchRequiredFwkGspot_DetailPourcentage(EOEditingContext editingContext, String keyName, Object value) {
    return _EODetailPourcentage.fetchRequiredFwkGspot_DetailPourcentage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODetailPourcentage fetchRequiredFwkGspot_DetailPourcentage(EOEditingContext editingContext, EOQualifier qualifier) {
    EODetailPourcentage eoObject = _EODetailPourcentage.fetchFwkGspot_DetailPourcentage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_DetailPourcentage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODetailPourcentage localInstanceIn(EOEditingContext editingContext, EODetailPourcentage eo) {
    EODetailPourcentage localInstance = (eo == null) ? null : (EODetailPourcentage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
