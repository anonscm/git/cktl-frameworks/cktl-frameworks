// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOUnite.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOUnite extends  EOGenericRecord {
	public static final String ENTITY_NAME = "Unite";

	// Attributes
	public static final String UNI_ABREV_KEY = "uniAbrev";
	public static final String UNI_LIBELLE_KEY = "uniLibelle";

	// Relationships

  private static Logger LOG = Logger.getLogger(_EOUnite.class);

  public EOUnite localInstanceIn(EOEditingContext editingContext) {
    EOUnite localInstance = (EOUnite)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String uniAbrev() {
    return (String) storedValueForKey("uniAbrev");
  }

  public void setUniAbrev(String value) {
    if (_EOUnite.LOG.isDebugEnabled()) {
    	_EOUnite.LOG.debug( "updating uniAbrev from " + uniAbrev() + " to " + value);
    }
    takeStoredValueForKey(value, "uniAbrev");
  }

  public String uniLibelle() {
    return (String) storedValueForKey("uniLibelle");
  }

  public void setUniLibelle(String value) {
    if (_EOUnite.LOG.isDebugEnabled()) {
    	_EOUnite.LOG.debug( "updating uniLibelle from " + uniLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "uniLibelle");
  }


  public static EOUnite createUnite(EOEditingContext editingContext) {
    EOUnite eo = (EOUnite) EOUtilities.createAndInsertInstance(editingContext, _EOUnite.ENTITY_NAME);    
    return eo;
  }

  public static NSArray<EOUnite> fetchAllUnites(EOEditingContext editingContext) {
    return _EOUnite.fetchAllUnites(editingContext, null);
  }

  public static NSArray<EOUnite> fetchAllUnites(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOUnite.fetchUnites(editingContext, null, sortOrderings);
  }

  public static NSArray<EOUnite> fetchUnites(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOUnite.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOUnite> eoObjects = (NSArray<EOUnite>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOUnite fetchUnite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOUnite.fetchUnite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOUnite fetchUnite(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOUnite> eoObjects = _EOUnite.fetchUnites(editingContext, qualifier, null);
    EOUnite eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOUnite)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one Unite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOUnite fetchRequiredUnite(EOEditingContext editingContext, String keyName, Object value) {
    return _EOUnite.fetchRequiredUnite(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOUnite fetchRequiredUnite(EOEditingContext editingContext, EOQualifier qualifier) {
    EOUnite eoObject = _EOUnite.fetchUnite(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no Unite that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOUnite localInstanceIn(EOEditingContext editingContext, EOUnite eo) {
    EOUnite localInstance = (eo == null) ? null : (EOUnite)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
