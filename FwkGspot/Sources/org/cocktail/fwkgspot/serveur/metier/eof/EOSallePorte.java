package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.foundation.NSValidation;

public class EOSallePorte extends _EOSallePorte {
  private static Logger log = Logger.getLogger(EOSallePorte.class);
  public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForInsert();
	}

	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();

		super.validateForUpdate();
	}

	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Apparemment cette methode n'est pas appelée.
	 * 
	 * @see com.webobjects.eocontrol.EOValidation#validateForUpdate()
	 */
	public void validateForSave() throws NSValidation.ValidationException {
		validateObjectMetier();

		super.validateForSave();

	}

	public void validateObjectMetier() throws NSValidation.ValidationException {
		
		if ((this.salPorte() == null)&&(this.salCb()==null)) {
			throw new NSValidation.ValidationException(
					"Vous devez saisir un code porte ou un code barre !");						
		}

	}
}
