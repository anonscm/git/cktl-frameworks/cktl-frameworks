// $LastChangedRevision: 5773 $ DO NOT EDIT.  Make changes to EOSalleTypeOccupation.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

@SuppressWarnings("all")
public abstract class _EOSalleTypeOccupation extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_SalleTypeOccupation";

	// Attributes

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_TYPE_OCCUPATION_KEY = "toTypeOccupation";

  private static Logger LOG = Logger.getLogger(_EOSalleTypeOccupation.class);

  public EOSalleTypeOccupation localInstanceIn(EOEditingContext editingContext) {
    EOSalleTypeOccupation localInstance = (EOSalleTypeOccupation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EOSalleTypeOccupation.LOG.isDebugEnabled()) {
      _EOSalleTypeOccupation.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation toTypeOccupation() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation)storedValueForKey("toTypeOccupation");
  }

  public void setToTypeOccupationRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation value) {
    if (_EOSalleTypeOccupation.LOG.isDebugEnabled()) {
      _EOSalleTypeOccupation.LOG.debug("updating toTypeOccupation from " + toTypeOccupation() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation oldValue = toTypeOccupation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeOccupation");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeOccupation");
    }
  }
  

  public static EOSalleTypeOccupation createFwkGspot_SalleTypeOccupation(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles, org.cocktail.fwkgspot.serveur.metier.eof.EOTypeOccupation toTypeOccupation) {
    EOSalleTypeOccupation eo = (EOSalleTypeOccupation) EOUtilities.createAndInsertInstance(editingContext, _EOSalleTypeOccupation.ENTITY_NAME);    
    eo.setToSallesRelationship(toSalles);
    eo.setToTypeOccupationRelationship(toTypeOccupation);
    return eo;
  }

  public static NSArray<EOSalleTypeOccupation> fetchAllFwkGspot_SalleTypeOccupations(EOEditingContext editingContext) {
    return _EOSalleTypeOccupation.fetchAllFwkGspot_SalleTypeOccupations(editingContext, null);
  }

  public static NSArray<EOSalleTypeOccupation> fetchAllFwkGspot_SalleTypeOccupations(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOSalleTypeOccupation.fetchFwkGspot_SalleTypeOccupations(editingContext, null, sortOrderings);
  }

  public static NSArray<EOSalleTypeOccupation> fetchFwkGspot_SalleTypeOccupations(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSalleTypeOccupation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOSalleTypeOccupation> eoObjects = (NSArray<EOSalleTypeOccupation>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOSalleTypeOccupation fetchFwkGspot_SalleTypeOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalleTypeOccupation.fetchFwkGspot_SalleTypeOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalleTypeOccupation fetchFwkGspot_SalleTypeOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOSalleTypeOccupation> eoObjects = _EOSalleTypeOccupation.fetchFwkGspot_SalleTypeOccupations(editingContext, qualifier, null);
    EOSalleTypeOccupation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOSalleTypeOccupation)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_SalleTypeOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalleTypeOccupation fetchRequiredFwkGspot_SalleTypeOccupation(EOEditingContext editingContext, String keyName, Object value) {
    return _EOSalleTypeOccupation.fetchRequiredFwkGspot_SalleTypeOccupation(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOSalleTypeOccupation fetchRequiredFwkGspot_SalleTypeOccupation(EOEditingContext editingContext, EOQualifier qualifier) {
    EOSalleTypeOccupation eoObject = _EOSalleTypeOccupation.fetchFwkGspot_SalleTypeOccupation(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_SalleTypeOccupation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOSalleTypeOccupation localInstanceIn(EOEditingContext editingContext, EOSalleTypeOccupation eo) {
    EOSalleTypeOccupation localInstance = (eo == null) ? null : (EOSalleTypeOccupation)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
