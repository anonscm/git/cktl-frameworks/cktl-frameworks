// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOResaObjet.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOResaObjet extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_ResaObjet";

	// Attributes
	public static final String RO_ACCES_KEY = "roAcces";
	public static final String RO_CODE_BARRE_KEY = "roCodeBarre";
	public static final String RO_LIBELLE1_KEY = "roLibelle1";
	public static final String RO_LIBELLE2_KEY = "roLibelle2";
	public static final String RO_RESERVABLE_KEY = "roReservable";
	public static final String SAL_NUMERO_KEY = "salNumero";

	// Relationships
	public static final String TO_RESA_TYPE_OBJET_KEY = "toResaTypeObjet";
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EOResaObjet.class);

  public EOResaObjet localInstanceIn(EOEditingContext editingContext) {
    EOResaObjet localInstance = (EOResaObjet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String roAcces() {
    return (String) storedValueForKey("roAcces");
  }

  public void setRoAcces(String value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating roAcces from " + roAcces() + " to " + value);
    }
    takeStoredValueForKey(value, "roAcces");
  }

  public String roCodeBarre() {
    return (String) storedValueForKey("roCodeBarre");
  }

  public void setRoCodeBarre(String value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating roCodeBarre from " + roCodeBarre() + " to " + value);
    }
    takeStoredValueForKey(value, "roCodeBarre");
  }

  public String roLibelle1() {
    return (String) storedValueForKey("roLibelle1");
  }

  public void setRoLibelle1(String value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating roLibelle1 from " + roLibelle1() + " to " + value);
    }
    takeStoredValueForKey(value, "roLibelle1");
  }

  public String roLibelle2() {
    return (String) storedValueForKey("roLibelle2");
  }

  public void setRoLibelle2(String value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating roLibelle2 from " + roLibelle2() + " to " + value);
    }
    takeStoredValueForKey(value, "roLibelle2");
  }

  public String roReservable() {
    return (String) storedValueForKey("roReservable");
  }

  public void setRoReservable(String value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating roReservable from " + roReservable() + " to " + value);
    }
    takeStoredValueForKey(value, "roReservable");
  }

  public Long salNumero() {
    return (Long) storedValueForKey("salNumero");
  }

  public void setSalNumero(Long value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
    	_EOResaObjet.LOG.debug( "updating salNumero from " + salNumero() + " to " + value);
    }
    takeStoredValueForKey(value, "salNumero");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet toResaTypeObjet() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet)storedValueForKey("toResaTypeObjet");
  }

  public void setToResaTypeObjetRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
      _EOResaObjet.LOG.debug("updating toResaTypeObjet from " + toResaTypeObjet() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet oldValue = toResaTypeObjet();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toResaTypeObjet");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toResaTypeObjet");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EOResaObjet.LOG.isDebugEnabled()) {
      _EOResaObjet.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  

  public static EOResaObjet createFwkGspot_ResaObjet(EOEditingContext editingContext, String roLibelle1
, org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet toResaTypeObjet) {
    EOResaObjet eo = (EOResaObjet) EOUtilities.createAndInsertInstance(editingContext, _EOResaObjet.ENTITY_NAME);    
		eo.setRoLibelle1(roLibelle1);
    eo.setToResaTypeObjetRelationship(toResaTypeObjet);
    return eo;
  }

  public static NSArray<EOResaObjet> fetchAllFwkGspot_ResaObjets(EOEditingContext editingContext) {
    return _EOResaObjet.fetchAllFwkGspot_ResaObjets(editingContext, null);
  }

  public static NSArray<EOResaObjet> fetchAllFwkGspot_ResaObjets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOResaObjet.fetchFwkGspot_ResaObjets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOResaObjet> fetchFwkGspot_ResaObjets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOResaObjet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOResaObjet> eoObjects = (NSArray<EOResaObjet>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOResaObjet fetchFwkGspot_ResaObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaObjet.fetchFwkGspot_ResaObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaObjet fetchFwkGspot_ResaObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOResaObjet> eoObjects = _EOResaObjet.fetchFwkGspot_ResaObjets(editingContext, qualifier, null);
    EOResaObjet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOResaObjet)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_ResaObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaObjet fetchRequiredFwkGspot_ResaObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaObjet.fetchRequiredFwkGspot_ResaObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaObjet fetchRequiredFwkGspot_ResaObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    EOResaObjet eoObject = _EOResaObjet.fetchFwkGspot_ResaObjet(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_ResaObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaObjet localInstanceIn(EOEditingContext editingContext, EOResaObjet eo) {
    EOResaObjet localInstance = (eo == null) ? null : (EOResaObjet)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
