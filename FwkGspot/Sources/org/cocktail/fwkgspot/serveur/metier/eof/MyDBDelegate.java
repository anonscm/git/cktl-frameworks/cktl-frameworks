package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.EOAdaptorOperation;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXDatabaseContextMulticastingDelegate;

public class MyDBDelegate extends ERXDatabaseContextMulticastingDelegate
		implements EODatabaseContext.Delegate {

	public MyDBDelegate() {
		super();
		// TODO Auto-generated constructor stub
	}

	// tentative de delegate pour ordonner les operation lors d'un save changes
	// mais
	// les op que je voulais sont dans des EODatabaseContext différents donc ca
	// marche pas
	// J'ai commenté l'utilisation du delegate dans Application.main
	public NSArray c(
			EODatabaseContext dbCtxt, NSArray databaseOps) {
		NSArray base = super.databaseContextWillOrderAdaptorOperations(dbCtxt,
				databaseOps);
		NSMutableArray retour = new NSMutableArray();
		System.out.println(dbCtxt);
		for (Object op : base) {
			EOAdaptorOperation dbOp = (EOAdaptorOperation) op;
			System.out.println(dbOp.entity());

			System.out.println("dbop :" + op);

		}
		return base;
	}

}
