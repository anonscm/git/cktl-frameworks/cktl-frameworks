// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOVTypeSalleLocalEtage.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOVTypeSalleLocalEtage extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_VTypeSalleLocalEtage";

	// Attributes
	public static final String SAL_ETAGE_KEY = "salEtage";

	// Relationships
	public static final String TO_LOCAL_KEY = "toLocal";
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_TYPE_SALLE_KEY = "toTypeSalle";

  private static Logger LOG = Logger.getLogger(_EOVTypeSalleLocalEtage.class);

  public EOVTypeSalleLocalEtage localInstanceIn(EOEditingContext editingContext) {
    EOVTypeSalleLocalEtage localInstance = (EOVTypeSalleLocalEtage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String salEtage() {
    return (String) storedValueForKey("salEtage");
  }

  public void setSalEtage(String value) {
    if (_EOVTypeSalleLocalEtage.LOG.isDebugEnabled()) {
    	_EOVTypeSalleLocalEtage.LOG.debug( "updating salEtage from " + salEtage() + " to " + value);
    }
    takeStoredValueForKey(value, "salEtage");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLocal)storedValueForKey("toLocal");
  }

  public void setToLocalRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLocal value) {
    if (_EOVTypeSalleLocalEtage.LOG.isDebugEnabled()) {
      _EOVTypeSalleLocalEtage.LOG.debug("updating toLocal from " + toLocal() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLocal oldValue = toLocal();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLocal");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLocal");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle toTypeSalle() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle)storedValueForKey("toTypeSalle");
  }

  public void setToTypeSalleRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle value) {
    if (_EOVTypeSalleLocalEtage.LOG.isDebugEnabled()) {
      _EOVTypeSalleLocalEtage.LOG.debug("updating toTypeSalle from " + toTypeSalle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle oldValue = toTypeSalle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toTypeSalle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toTypeSalle");
    }
  }
  
  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)storedValueForKey("toSalles");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier) {
    return toSalles(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, boolean fetch) {
    return toSalles(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> toSalles(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.TO_V_TYPE_SALLE_LOCAL_ETAGE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOSalles.fetchFwkGspot_Salleses(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toSalles();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOSalles>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOVTypeSalleLocalEtage.LOG.isDebugEnabled()) {
      _EOVTypeSalleLocalEtage.LOG.debug("adding " + object + " to toSalles relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public void removeFromToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    if (_EOVTypeSalleLocalEtage.LOG.isDebugEnabled()) {
      _EOVTypeSalleLocalEtage.LOG.debug("removing " + object + " from toSalles relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles createToSallesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_Salles");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toSalles");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles) eo;
  }

  public void deleteToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toSalles");
    editingContext().deleteObject(object);
  }

  public void deleteAllToSallesRelationships() {
    Enumeration objects = toSalles().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToSallesRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)objects.nextElement());
    }
  }


  public static EOVTypeSalleLocalEtage createFwkGspot_VTypeSalleLocalEtage(EOEditingContext editingContext, String salEtage
, org.cocktail.fwkgspot.serveur.metier.eof.EOLocal toLocal, org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle toTypeSalle) {
    EOVTypeSalleLocalEtage eo = (EOVTypeSalleLocalEtage) EOUtilities.createAndInsertInstance(editingContext, _EOVTypeSalleLocalEtage.ENTITY_NAME);    
		eo.setSalEtage(salEtage);
    eo.setToLocalRelationship(toLocal);
    eo.setToTypeSalleRelationship(toTypeSalle);
    return eo;
  }

  public static NSArray<EOVTypeSalleLocalEtage> fetchAllFwkGspot_VTypeSalleLocalEtages(EOEditingContext editingContext) {
    return _EOVTypeSalleLocalEtage.fetchAllFwkGspot_VTypeSalleLocalEtages(editingContext, null);
  }

  public static NSArray<EOVTypeSalleLocalEtage> fetchAllFwkGspot_VTypeSalleLocalEtages(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtages(editingContext, null, sortOrderings);
  }

  public static NSArray<EOVTypeSalleLocalEtage> fetchFwkGspot_VTypeSalleLocalEtages(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOVTypeSalleLocalEtage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOVTypeSalleLocalEtage> eoObjects = (NSArray<EOVTypeSalleLocalEtage>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOVTypeSalleLocalEtage fetchFwkGspot_VTypeSalleLocalEtage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVTypeSalleLocalEtage fetchFwkGspot_VTypeSalleLocalEtage(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOVTypeSalleLocalEtage> eoObjects = _EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtages(editingContext, qualifier, null);
    EOVTypeSalleLocalEtage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOVTypeSalleLocalEtage)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_VTypeSalleLocalEtage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVTypeSalleLocalEtage fetchRequiredFwkGspot_VTypeSalleLocalEtage(EOEditingContext editingContext, String keyName, Object value) {
    return _EOVTypeSalleLocalEtage.fetchRequiredFwkGspot_VTypeSalleLocalEtage(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOVTypeSalleLocalEtage fetchRequiredFwkGspot_VTypeSalleLocalEtage(EOEditingContext editingContext, EOQualifier qualifier) {
    EOVTypeSalleLocalEtage eoObject = _EOVTypeSalleLocalEtage.fetchFwkGspot_VTypeSalleLocalEtage(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_VTypeSalleLocalEtage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOVTypeSalleLocalEtage localInstanceIn(EOEditingContext editingContext, EOVTypeSalleLocalEtage eo) {
    EOVTypeSalleLocalEtage localInstance = (eo == null) ? null : (EOVTypeSalleLocalEtage)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
