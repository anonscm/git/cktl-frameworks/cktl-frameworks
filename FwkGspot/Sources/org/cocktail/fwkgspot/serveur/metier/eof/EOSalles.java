package org.cocktail.fwkgspot.serveur.metier.eof;

import org.apache.log4j.Logger;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public class EOSalles extends _EOSalles {
  /**
	 * 
	 */
	private static final long serialVersionUID = 6951058266433490749L;
@SuppressWarnings("unused")
private static Logger log = Logger.getLogger(EOSalles.class);
  
  public static NSArray<EOSalles> fetchDistinctFwkGspot_Salleses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOSalles.ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(true);
	    @SuppressWarnings("unchecked")
		NSArray<EOSalles> eoObjects = (NSArray<EOSalles>)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }
  
  
  public void setSalReservableBoolean(Boolean reservable) {
    super.setSalReservable(reservable ? "O" : "N");
  }
  
  public Boolean salReservableBoolean() {
    if(super.salReservable() == null) {
      setSalReservableBoolean(false);
    }
    if(super.salReservable().equalsIgnoreCase("O")) {
      return true;
    } else {
      return false;
    }
  }
  
}
