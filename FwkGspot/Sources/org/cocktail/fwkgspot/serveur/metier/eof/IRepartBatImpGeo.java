package org.cocktail.fwkgspot.serveur.metier.eof;

/**
 * Lien entre une implantation geographique et son batiment
 */
public interface IRepartBatImpGeo {

	/**
	 * @return local au sens géographique (batiment, immeuble)
	 */
	ILocal toLocal();
}
