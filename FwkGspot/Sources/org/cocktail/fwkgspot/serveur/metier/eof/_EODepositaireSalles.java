// $LastChangedRevision$ DO NOT EDIT.  Make changes to EODepositaireSalles.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EODepositaireSalles extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_DepositaireSalles";

	// Attributes
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";
	public static final String TO_STRUCTURE_ULR_KEY = "toStructureUlr";

  private static Logger LOG = Logger.getLogger(_EODepositaireSalles.class);

  public EODepositaireSalles localInstanceIn(EOEditingContext editingContext) {
    EODepositaireSalles localInstance = (EODepositaireSalles)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey("dCreation");
  }

  public void setDCreation(NSTimestamp value) {
    if (_EODepositaireSalles.LOG.isDebugEnabled()) {
    	_EODepositaireSalles.LOG.debug( "updating dCreation from " + dCreation() + " to " + value);
    }
    takeStoredValueForKey(value, "dCreation");
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey("dModification");
  }

  public void setDModification(NSTimestamp value) {
    if (_EODepositaireSalles.LOG.isDebugEnabled()) {
    	_EODepositaireSalles.LOG.debug( "updating dModification from " + dModification() + " to " + value);
    }
    takeStoredValueForKey(value, "dModification");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EODepositaireSalles.LOG.isDebugEnabled()) {
      _EODepositaireSalles.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr)storedValueForKey("toStructureUlr");
  }

  public void setToStructureUlrRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr value) {
    if (_EODepositaireSalles.LOG.isDebugEnabled()) {
      _EODepositaireSalles.LOG.debug("updating toStructureUlr from " + toStructureUlr() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr oldValue = toStructureUlr();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toStructureUlr");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toStructureUlr");
    }
  }
  

  public static EODepositaireSalles createFwkGspot_DepositaireSalles(EOEditingContext editingContext, NSTimestamp dCreation
, NSTimestamp dModification
, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles, org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr toStructureUlr) {
    EODepositaireSalles eo = (EODepositaireSalles) EOUtilities.createAndInsertInstance(editingContext, _EODepositaireSalles.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
    eo.setToSallesRelationship(toSalles);
    eo.setToStructureUlrRelationship(toStructureUlr);
    return eo;
  }

  public static NSArray<EODepositaireSalles> fetchAllFwkGspot_DepositaireSalleses(EOEditingContext editingContext) {
    return _EODepositaireSalles.fetchAllFwkGspot_DepositaireSalleses(editingContext, null);
  }

  public static NSArray<EODepositaireSalles> fetchAllFwkGspot_DepositaireSalleses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EODepositaireSalles.fetchFwkGspot_DepositaireSalleses(editingContext, null, sortOrderings);
  }

  public static NSArray<EODepositaireSalles> fetchFwkGspot_DepositaireSalleses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EODepositaireSalles.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EODepositaireSalles> eoObjects = (NSArray<EODepositaireSalles>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EODepositaireSalles fetchFwkGspot_DepositaireSalles(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepositaireSalles.fetchFwkGspot_DepositaireSalles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepositaireSalles fetchFwkGspot_DepositaireSalles(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EODepositaireSalles> eoObjects = _EODepositaireSalles.fetchFwkGspot_DepositaireSalleses(editingContext, qualifier, null);
    EODepositaireSalles eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EODepositaireSalles)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_DepositaireSalles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepositaireSalles fetchRequiredFwkGspot_DepositaireSalles(EOEditingContext editingContext, String keyName, Object value) {
    return _EODepositaireSalles.fetchRequiredFwkGspot_DepositaireSalles(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EODepositaireSalles fetchRequiredFwkGspot_DepositaireSalles(EOEditingContext editingContext, EOQualifier qualifier) {
    EODepositaireSalles eoObject = _EODepositaireSalles.fetchFwkGspot_DepositaireSalles(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_DepositaireSalles that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EODepositaireSalles localInstanceIn(EOEditingContext editingContext, EODepositaireSalles eo) {
    EODepositaireSalles localInstance = (eo == null) ? null : (EODepositaireSalles)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
