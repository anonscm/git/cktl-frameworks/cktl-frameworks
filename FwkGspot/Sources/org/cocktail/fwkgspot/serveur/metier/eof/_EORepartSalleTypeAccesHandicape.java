// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartSalleTypeAccesHandicape.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartSalleTypeAccesHandicape extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartSalleAccesHandicape";

	// Attributes

	// Relationships
	public static final String TO_FWK_GSPOT__SALLES_KEY = "toFwkGspot_Salles";
	public static final String TO_FWK_GSPOT__TYPE_ACCES_HANDICAPE_KEY = "toFwkGspot_TypeAccesHandicape";

  private static Logger LOG = Logger.getLogger(_EORepartSalleTypeAccesHandicape.class);

  public EORepartSalleTypeAccesHandicape localInstanceIn(EOEditingContext editingContext) {
    EORepartSalleTypeAccesHandicape localInstance = (EORepartSalleTypeAccesHandicape)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toFwkGspot_Salles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toFwkGspot_Salles");
  }

  public void setToFwkGspot_SallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EORepartSalleTypeAccesHandicape.LOG.isDebugEnabled()) {
      _EORepartSalleTypeAccesHandicape.LOG.debug("updating toFwkGspot_Salles from " + toFwkGspot_Salles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toFwkGspot_Salles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkGspot_Salles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkGspot_Salles");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape toFwkGspot_TypeAccesHandicape() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape)storedValueForKey("toFwkGspot_TypeAccesHandicape");
  }

  public void setToFwkGspot_TypeAccesHandicapeRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape value) {
    if (_EORepartSalleTypeAccesHandicape.LOG.isDebugEnabled()) {
      _EORepartSalleTypeAccesHandicape.LOG.debug("updating toFwkGspot_TypeAccesHandicape from " + toFwkGspot_TypeAccesHandicape() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape oldValue = toFwkGspot_TypeAccesHandicape();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toFwkGspot_TypeAccesHandicape");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toFwkGspot_TypeAccesHandicape");
    }
  }
  

  public static EORepartSalleTypeAccesHandicape createFwkGspot_RepartSalleAccesHandicape(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toFwkGspot_Salles, org.cocktail.fwkgspot.serveur.metier.eof.EOTypeAccesHandicape toFwkGspot_TypeAccesHandicape) {
    EORepartSalleTypeAccesHandicape eo = (EORepartSalleTypeAccesHandicape) EOUtilities.createAndInsertInstance(editingContext, _EORepartSalleTypeAccesHandicape.ENTITY_NAME);    
    eo.setToFwkGspot_SallesRelationship(toFwkGspot_Salles);
    eo.setToFwkGspot_TypeAccesHandicapeRelationship(toFwkGspot_TypeAccesHandicape);
    return eo;
  }

  public static NSArray<EORepartSalleTypeAccesHandicape> fetchAllFwkGspot_RepartSalleAccesHandicapes(EOEditingContext editingContext) {
    return _EORepartSalleTypeAccesHandicape.fetchAllFwkGspot_RepartSalleAccesHandicapes(editingContext, null);
  }

  public static NSArray<EORepartSalleTypeAccesHandicape> fetchAllFwkGspot_RepartSalleAccesHandicapes(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicapes(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartSalleTypeAccesHandicape> fetchFwkGspot_RepartSalleAccesHandicapes(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartSalleTypeAccesHandicape.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartSalleTypeAccesHandicape> eoObjects = (NSArray<EORepartSalleTypeAccesHandicape>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartSalleTypeAccesHandicape fetchFwkGspot_RepartSalleAccesHandicape(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartSalleTypeAccesHandicape fetchFwkGspot_RepartSalleAccesHandicape(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartSalleTypeAccesHandicape> eoObjects = _EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicapes(editingContext, qualifier, null);
    EORepartSalleTypeAccesHandicape eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartSalleTypeAccesHandicape)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartSalleAccesHandicape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartSalleTypeAccesHandicape fetchRequiredFwkGspot_RepartSalleAccesHandicape(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartSalleTypeAccesHandicape.fetchRequiredFwkGspot_RepartSalleAccesHandicape(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartSalleTypeAccesHandicape fetchRequiredFwkGspot_RepartSalleAccesHandicape(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartSalleTypeAccesHandicape eoObject = _EORepartSalleTypeAccesHandicape.fetchFwkGspot_RepartSalleAccesHandicape(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartSalleAccesHandicape that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartSalleTypeAccesHandicape localInstanceIn(EOEditingContext editingContext, EORepartSalleTypeAccesHandicape eo) {
    EORepartSalleTypeAccesHandicape localInstance = (eo == null) ? null : (EORepartSalleTypeAccesHandicape)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
