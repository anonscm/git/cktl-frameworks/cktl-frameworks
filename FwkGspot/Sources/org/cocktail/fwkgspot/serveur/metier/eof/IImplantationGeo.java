package org.cocktail.fwkgspot.serveur.metier.eof;

import java.util.List;

/**
 * implantation géographique d'une composante
 */
public interface IImplantationGeo {

	/**
	 * @return liste des liens vers les batiments de l'implantation géographique
	 */
	List<? extends IRepartBatImpGeo> toRepartBatImpGeos();
	
	/**
	 * @return Le libellé court de l'implantation géographique
	 */
	String lcImplantationGeo();

	/**
	 * @param unLibelleCourt Un libelle court pour l'implantation géographique
	 */
	void setLcImplantationGeo(String unLibelleCourt);
	
	/**
	 * @return Le libellé long de l'implantation géographique
	 */
	String llImplantationGeo();
	
	/**
	 * @param unLibelleLong Un libellé long pour l'implantation géographique
	 */
	void setLlImplantationGeo(String unLibelleLong);
	
}
