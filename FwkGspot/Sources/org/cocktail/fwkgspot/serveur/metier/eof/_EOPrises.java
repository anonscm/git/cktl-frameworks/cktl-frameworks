// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOPrises.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOPrises extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_Prises";

	// Attributes
	public static final String PRI_CODE_KEY = "priCode";
	public static final String PRI_TYPE_KEY = "priType";

	// Relationships
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EOPrises.class);

  public EOPrises localInstanceIn(EOEditingContext editingContext) {
    EOPrises localInstance = (EOPrises)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String priCode() {
    return (String) storedValueForKey("priCode");
  }

  public void setPriCode(String value) {
    if (_EOPrises.LOG.isDebugEnabled()) {
    	_EOPrises.LOG.debug( "updating priCode from " + priCode() + " to " + value);
    }
    takeStoredValueForKey(value, "priCode");
  }

  public String priType() {
    return (String) storedValueForKey("priType");
  }

  public void setPriType(String value) {
    if (_EOPrises.LOG.isDebugEnabled()) {
    	_EOPrises.LOG.debug( "updating priType from " + priType() + " to " + value);
    }
    takeStoredValueForKey(value, "priType");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EOPrises.LOG.isDebugEnabled()) {
      _EOPrises.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  

  public static EOPrises createFwkGspot_Prises(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles) {
    EOPrises eo = (EOPrises) EOUtilities.createAndInsertInstance(editingContext, _EOPrises.ENTITY_NAME);    
    eo.setToSallesRelationship(toSalles);
    return eo;
  }

  public static NSArray<EOPrises> fetchAllFwkGspot_Priseses(EOEditingContext editingContext) {
    return _EOPrises.fetchAllFwkGspot_Priseses(editingContext, null);
  }

  public static NSArray<EOPrises> fetchAllFwkGspot_Priseses(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOPrises.fetchFwkGspot_Priseses(editingContext, null, sortOrderings);
  }

  public static NSArray<EOPrises> fetchFwkGspot_Priseses(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOPrises.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOPrises> eoObjects = (NSArray<EOPrises>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOPrises fetchFwkGspot_Prises(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPrises.fetchFwkGspot_Prises(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPrises fetchFwkGspot_Prises(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOPrises> eoObjects = _EOPrises.fetchFwkGspot_Priseses(editingContext, qualifier, null);
    EOPrises eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOPrises)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_Prises that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPrises fetchRequiredFwkGspot_Prises(EOEditingContext editingContext, String keyName, Object value) {
    return _EOPrises.fetchRequiredFwkGspot_Prises(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOPrises fetchRequiredFwkGspot_Prises(EOEditingContext editingContext, EOQualifier qualifier) {
    EOPrises eoObject = _EOPrises.fetchFwkGspot_Prises(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_Prises that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOPrises localInstanceIn(EOEditingContext editingContext, EOPrises eo) {
    EOPrises localInstance = (eo == null) ? null : (EOPrises)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
