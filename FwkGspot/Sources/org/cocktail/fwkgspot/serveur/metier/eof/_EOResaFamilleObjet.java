// $LastChangedRevision$ DO NOT EDIT.  Make changes to EOResaFamilleObjet.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EOResaFamilleObjet extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_ResaFamilleObjet";

	// Attributes
	public static final String RFO_COMMENTAIRE_KEY = "rfoCommentaire";
	public static final String RFO_LIBELLE_KEY = "rfoLibelle";

	// Relationships
	public static final String TO_RESA_TYPE_OBJETS_KEY = "toResaTypeObjets";

  private static Logger LOG = Logger.getLogger(_EOResaFamilleObjet.class);

  public EOResaFamilleObjet localInstanceIn(EOEditingContext editingContext) {
    EOResaFamilleObjet localInstance = (EOResaFamilleObjet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String rfoCommentaire() {
    return (String) storedValueForKey("rfoCommentaire");
  }

  public void setRfoCommentaire(String value) {
    if (_EOResaFamilleObjet.LOG.isDebugEnabled()) {
    	_EOResaFamilleObjet.LOG.debug( "updating rfoCommentaire from " + rfoCommentaire() + " to " + value);
    }
    takeStoredValueForKey(value, "rfoCommentaire");
  }

  public String rfoLibelle() {
    return (String) storedValueForKey("rfoLibelle");
  }

  public void setRfoLibelle(String value) {
    if (_EOResaFamilleObjet.LOG.isDebugEnabled()) {
    	_EOResaFamilleObjet.LOG.debug( "updating rfoLibelle from " + rfoLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, "rfoLibelle");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet> toResaTypeObjets() {
    return (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet>)storedValueForKey("toResaTypeObjets");
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet> toResaTypeObjets(EOQualifier qualifier) {
    return toResaTypeObjets(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet> toResaTypeObjets(EOQualifier qualifier, boolean fetch) {
    return toResaTypeObjets(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet> toResaTypeObjets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet.TO_RESA_FAMILLE_OBJET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet.fetchFwkGspot_ResaTypeObjets(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toResaTypeObjets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToResaTypeObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet object) {
    if (_EOResaFamilleObjet.LOG.isDebugEnabled()) {
      _EOResaFamilleObjet.LOG.debug("adding " + object + " to toResaTypeObjets relationship");
    }
    addObjectToBothSidesOfRelationshipWithKey(object, "toResaTypeObjets");
  }

  public void removeFromToResaTypeObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet object) {
    if (_EOResaFamilleObjet.LOG.isDebugEnabled()) {
      _EOResaFamilleObjet.LOG.debug("removing " + object + " from toResaTypeObjets relationship");
    }
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaTypeObjets");
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet createToResaTypeObjetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkGspot_ResaTypeObjet");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, "toResaTypeObjets");
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet) eo;
  }

  public void deleteToResaTypeObjetsRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, "toResaTypeObjets");
    editingContext().deleteObject(object);
  }

  public void deleteAllToResaTypeObjetsRelationships() {
    Enumeration objects = toResaTypeObjets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToResaTypeObjetsRelationship((org.cocktail.fwkgspot.serveur.metier.eof.EOResaTypeObjet)objects.nextElement());
    }
  }


  public static EOResaFamilleObjet createFwkGspot_ResaFamilleObjet(EOEditingContext editingContext, String rfoLibelle
) {
    EOResaFamilleObjet eo = (EOResaFamilleObjet) EOUtilities.createAndInsertInstance(editingContext, _EOResaFamilleObjet.ENTITY_NAME);    
		eo.setRfoLibelle(rfoLibelle);
    return eo;
  }

  public static NSArray<EOResaFamilleObjet> fetchAllFwkGspot_ResaFamilleObjets(EOEditingContext editingContext) {
    return _EOResaFamilleObjet.fetchAllFwkGspot_ResaFamilleObjets(editingContext, null);
  }

  public static NSArray<EOResaFamilleObjet> fetchAllFwkGspot_ResaFamilleObjets(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EOResaFamilleObjet.fetchFwkGspot_ResaFamilleObjets(editingContext, null, sortOrderings);
  }

  public static NSArray<EOResaFamilleObjet> fetchFwkGspot_ResaFamilleObjets(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EOResaFamilleObjet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EOResaFamilleObjet> eoObjects = (NSArray<EOResaFamilleObjet>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EOResaFamilleObjet fetchFwkGspot_ResaFamilleObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaFamilleObjet.fetchFwkGspot_ResaFamilleObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaFamilleObjet fetchFwkGspot_ResaFamilleObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EOResaFamilleObjet> eoObjects = _EOResaFamilleObjet.fetchFwkGspot_ResaFamilleObjets(editingContext, qualifier, null);
    EOResaFamilleObjet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EOResaFamilleObjet)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_ResaFamilleObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaFamilleObjet fetchRequiredFwkGspot_ResaFamilleObjet(EOEditingContext editingContext, String keyName, Object value) {
    return _EOResaFamilleObjet.fetchRequiredFwkGspot_ResaFamilleObjet(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EOResaFamilleObjet fetchRequiredFwkGspot_ResaFamilleObjet(EOEditingContext editingContext, EOQualifier qualifier) {
    EOResaFamilleObjet eoObject = _EOResaFamilleObjet.fetchFwkGspot_ResaFamilleObjet(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_ResaFamilleObjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EOResaFamilleObjet localInstanceIn(EOEditingContext editingContext, EOResaFamilleObjet eo) {
    EOResaFamilleObjet localInstance = (eo == null) ? null : (EOResaFamilleObjet)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
