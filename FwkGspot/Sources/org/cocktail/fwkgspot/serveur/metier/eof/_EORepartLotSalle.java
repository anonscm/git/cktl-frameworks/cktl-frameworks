// $LastChangedRevision$ DO NOT EDIT.  Make changes to EORepartLotSalle.java instead.
package org.cocktail.fwkgspot.serveur.metier.eof;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

@SuppressWarnings("all")
public abstract class _EORepartLotSalle extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkGspot_RepartLotSalle";

	// Attributes

	// Relationships
	public static final String TO_LOT_SALLE_KEY = "toLotSalle";
	public static final String TO_SALLES_KEY = "toSalles";

  private static Logger LOG = Logger.getLogger(_EORepartLotSalle.class);

  public EORepartLotSalle localInstanceIn(EOEditingContext editingContext) {
    EORepartLotSalle localInstance = (EORepartLotSalle)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle toLotSalle() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle)storedValueForKey("toLotSalle");
  }

  public void setToLotSalleRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle value) {
    if (_EORepartLotSalle.LOG.isDebugEnabled()) {
      _EORepartLotSalle.LOG.debug("updating toLotSalle from " + toLotSalle() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle oldValue = toLotSalle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toLotSalle");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toLotSalle");
    }
  }
  
  public org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles() {
    return (org.cocktail.fwkgspot.serveur.metier.eof.EOSalles)storedValueForKey("toSalles");
  }

  public void setToSallesRelationship(org.cocktail.fwkgspot.serveur.metier.eof.EOSalles value) {
    if (_EORepartLotSalle.LOG.isDebugEnabled()) {
      _EORepartLotSalle.LOG.debug("updating toSalles from " + toSalles() + " to " + value);
    }
    if (value == null) {
    	org.cocktail.fwkgspot.serveur.metier.eof.EOSalles oldValue = toSalles();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, "toSalles");
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, "toSalles");
    }
  }
  

  public static EORepartLotSalle createFwkGspot_RepartLotSalle(EOEditingContext editingContext, org.cocktail.fwkgspot.serveur.metier.eof.EOLotSalle toLotSalle, org.cocktail.fwkgspot.serveur.metier.eof.EOSalles toSalles) {
    EORepartLotSalle eo = (EORepartLotSalle) EOUtilities.createAndInsertInstance(editingContext, _EORepartLotSalle.ENTITY_NAME);    
    eo.setToLotSalleRelationship(toLotSalle);
    eo.setToSallesRelationship(toSalles);
    return eo;
  }

  public static NSArray<EORepartLotSalle> fetchAllFwkGspot_RepartLotSalles(EOEditingContext editingContext) {
    return _EORepartLotSalle.fetchAllFwkGspot_RepartLotSalles(editingContext, null);
  }

  public static NSArray<EORepartLotSalle> fetchAllFwkGspot_RepartLotSalles(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return _EORepartLotSalle.fetchFwkGspot_RepartLotSalles(editingContext, null, sortOrderings);
  }

  public static NSArray<EORepartLotSalle> fetchFwkGspot_RepartLotSalles(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    EOFetchSpecification fetchSpec = new EOFetchSpecification(_EORepartLotSalle.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<EORepartLotSalle> eoObjects = (NSArray<EORepartLotSalle>)editingContext.objectsWithFetchSpecification(fetchSpec);
    return eoObjects;
  }

  public static EORepartLotSalle fetchFwkGspot_RepartLotSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLotSalle.fetchFwkGspot_RepartLotSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartLotSalle fetchFwkGspot_RepartLotSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<EORepartLotSalle> eoObjects = _EORepartLotSalle.fetchFwkGspot_RepartLotSalles(editingContext, qualifier, null);
    EORepartLotSalle eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = (EORepartLotSalle)eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one FwkGspot_RepartLotSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartLotSalle fetchRequiredFwkGspot_RepartLotSalle(EOEditingContext editingContext, String keyName, Object value) {
    return _EORepartLotSalle.fetchRequiredFwkGspot_RepartLotSalle(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static EORepartLotSalle fetchRequiredFwkGspot_RepartLotSalle(EOEditingContext editingContext, EOQualifier qualifier) {
    EORepartLotSalle eoObject = _EORepartLotSalle.fetchFwkGspot_RepartLotSalle(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no FwkGspot_RepartLotSalle that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static EORepartLotSalle localInstanceIn(EOEditingContext editingContext, EORepartLotSalle eo) {
    EORepartLotSalle localInstance = (eo == null) ? null : (EORepartLotSalle)EOUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}
