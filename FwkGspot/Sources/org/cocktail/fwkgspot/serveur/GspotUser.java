package org.cocktail.fwkgspot.serveur;

import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkgspot.serveur.finder.FinderDepositaires;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class GspotUser extends ApplicationUser {

	public static final String PARAM_STRUCTURE_SALLE = "SALLE_STRUCTURE";

	public GspotUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
		// TODO Auto-generated constructor stub
	}

	public GspotUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	private Boolean haveAdminRight;

	/**
	 *retourne true si le user ayant le persid fait partie d'un groupe ayant
	 * les droits de gestion des salles
	 * 
	 * @return boolean
	 */
	public boolean haveAdminRight() {
		if (haveAdminRight == null) {
			// recup des cstructures dans grhum_parametres
			NSArray<String> lstParams = EOGrhumParametres.parametresPourCle(
					getEditingContext(), PARAM_STRUCTURE_SALLE);
			for (String param : lstParams) {
				// check si l'utilisateur est dans le groupe
				EOQualifier myQual = EOQualifier
						.qualifierWithQualifierFormat(
								EORepartStructure.PERS_ID_KEY + " = %@ AND "
										+ EORepartStructure.C_STRUCTURE_KEY
										+ " =%@", new NSArray<Object>(
										new Object[] {getPersId(), param }));
				EOFetchSpecification mySpec = new EOFetchSpecification(
						EORepartStructure.ENTITY_NAME, myQual, null);

				haveAdminRight = (getEditingContext()
						.objectsWithFetchSpecification(mySpec).count() > 0);
				if (haveAdminRight) {
					break;
				}
			}

		}
		return haveAdminRight.booleanValue();

	}

	public boolean userCanUseAppli() {
		return haveAdminRight() || userIsDepositaire();
	}
	
	private Boolean userIsDepositaire;

	public boolean userIsDepositaire() {
		if (userIsDepositaire == null) {
			userIsDepositaire = FinderDepositaires.isIndividuDepositaire(getEditingContext(), getPersId());
		}
		return userIsDepositaire;
	}

	public boolean canEditSalle(EOSalles salle) {
		if (haveAdminRight()) {
			return true;
		}
		// si c'est un dépositaire de la salle c'est bon
		return FinderDepositaires.isDepositaireForSalle(getEditingContext(),
				this.getPersId(), salle);
	}
	
	public boolean canEditRoleDepositaireStructure(EOStructure str){
		return haveDroitsModification(str);
	}
	
	public boolean haveDroitsModification(IPersonne pers) {
		if (pers == null) {
			return false;
		}
		return pers.getPersonneDelegate()
				.hasPersonneUtilisateurDroitModification(
						this.getPersonne().persId());
	}
	
	/**
	 * Droit de gestion des tables de references
	 * @return boolean
	 */
	public boolean canEditRefTable() {
		return haveAdminRight();
	}
}
