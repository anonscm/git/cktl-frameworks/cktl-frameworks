package org.cocktail.fwkgspot.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.metier.eof.EODepositaireSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartTypeGroupe;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeGroupe;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;

public abstract class FinderDepositaires {
	public static final String ASS_CODE_DEPOS = "DEPOSITAIRE";
	public static final String TYPE_GRP_DEPOS = "DS";

	/**
	 * qualifier des individus avec un role depositaire salle
	 */
	public static final EOQualifier QUAL_INDIV_ROLE_DEPOSITAIRE = EOQualifier
			.qualifierWithQualifierFormat(EOIndividu.TO_REPART_ASSOCIATIONS_KEY
					+ "." + EORepartAssociation.TO_ASSOCIATION_KEY + "."
					+ EOAssociation.ASS_CODE_KEY + "=%s", new NSArray<Object>(
					new Object[] {ASS_CODE_DEPOS }));
	/**
	 * qualifier des repart_association avec un role depositaire de salle
	 */
	public static final EOQualifier QUAL_REPART_ASSOC_DEPOS = EOQualifier
			.qualifierWithQualifierFormat(
					EORepartAssociation.TO_ASSOCIATION_KEY + "."
							+ EOAssociation.ASS_CODE_KEY + "=%s",
					new NSArray<Object>(new Object[] {ASS_CODE_DEPOS }));

	/**
	 * Qualifier pour recuperer les structures de type depositaire (lib court
	 * =DEPOS ou type groupe DS-)
	 */
	@SuppressWarnings("unchecked")
	public static final EOQualifier QUAL_GROUPE_DEPOS = new EOOrQualifier(
			new NSArray(
					new EOQualifier[] {
							new EOKeyValueQualifier(
									EOStructureUlr.LC_STRUCTURE_KEY,
									EOQualifier.QualifierOperatorEqual,
									ASS_CODE_DEPOS),
							new EOKeyValueQualifier(
									EOStructureUlr.TO_REPART_TYPE_GROUPE_KEY
											+ "."
											+ EORepartTypeGroupe.TO_TYPE_GROUPE_KEY
											+ "." + EOTypeGroupe.TGRP_CODE_KEY,
									EOQualifier.QualifierOperatorEqual,
									TYPE_GRP_DEPOS) }));
  public static NSArray<EORepartAssociation> getDepositaireIndivForSalle(
      EOEditingContext ec, EOSalles salle) {
    if ((salle == null) || (ec == null)) {
		return null;
		// recherche des individus ayant le role dépositaire pour la salle
	}

    // reccup des structures affectees a la salle
    @SuppressWarnings("unchecked")
    NSArray<EOStructure> lstStructures = (NSArray<EOStructure>) EOStructureUlr
        .fetchFwkGspot_StructureUlrs(
            ec,
            EOQualifier.qualifierWithQualifierFormat(
                EOStructureUlr.TO_DETAIL_POURCENTAGES_KEY + "."
                    + EODetailPourcentage.TO_SALLES_KEY
                    + "=%@", new NSArray<EOSalles>(salle)),
            null).valueForKey(
            EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY);
    // contruction du tab de retour
    NSMutableArray<EORepartAssociation> retour = new NSMutableArray<EORepartAssociation>();
    NSArray<EORepartAssociation> repAsso = null;

    for (EOStructure str : lstStructures) {
      repAsso = EORepartAssociation.fetchAll(ec, new EOAndQualifier(
          new NSArray<EOQualifier>(new EOQualifier[] {
              QUAL_REPART_ASSOC_DEPOS,
              EOQualifier.qualifierWithQualifierFormat(
                  EORepartAssociation.TO_STRUCTURE_KEY
                      + "=%@", new NSArray<EOStructure>(
                      str)), })), null, Boolean.TRUE);
      for (EORepartAssociation occur : repAsso) {
        if (retour.indexOf(occur) <= -1) {
			retour.addObject(occur);
		}
      }

    }

    return retour;
  }
//	/**
//	 * Retourne une liste de EORepartAssociation ayant le role (association)
//	 * dépositaire (DEPOS) pour une ou des structure associées a la salle
//	 * (DETAIL_POURCENTAGE)
//	 * 
//	 * @param ec
//	 * @param salle
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public static NSArray<EORepartAssociation> getDepositaireIndivForSalle(
//			EOEditingContext ec, EOSalles salle) {
//		if ((salle == null) || (ec == null))
//			return null;
//		// recherche des individus ayant le role dépositaire pour la salle
//
//		// reccup des structures affectees a la salle
//    EOQualifier qualifierStructures = ERXQ.equals(
//        ERXQ.keyPath(EOStructureUlr.TO_DETAIL_POURCENTAGES_KEY, EODetailPourcentage.TO_SALLES_KEY),
//        salle
//    );
//    NSArray<EOStructure> lstStructures = (NSArray<EOStructure>) EOStructureUlr.fetchFwkGspot_StructureUlrs(ec, qualifierStructures, null).valueForKey(EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY);
//    EOQualifier qualifierRepartAssociationDespositaires = 
//        EORepartAssociation.TO_STRUCTURE.in(lstStructures)
//        .and(EORepartAssociation.TO_ASSOCIATION.dot(EOAssociation.ASS_CODE).eq(ASS_CODE_DEPOS));
//    return EORepartAssociation.fetchAll(ec, qualifierRepartAssociationDespositaires);
//		
//	
//	}

	/**
	 * Retourne la liste des groupes des déposiataires de la salle
	 * 
	 * @param ec
	 * @param salle
	 * @return
	 */
	public static NSArray<EOStructureUlr> getGroupesDepositaires(
			EOEditingContext ec, EOSalles salle) {
		if ((salle == null) || (ec == null)) {
			return null;
		}
		return (NSArray<EOStructureUlr>) salle.toDepositaireSalless()
				.valueForKey(EODepositaireSalles.TO_STRUCTURE_ULR_KEY);

	}

	public static boolean isDepositaireForSalle(EOEditingContext ec,
			Integer persId, EOSalles salle) {
		if ((salle == null) || (ec == null) || (persId == null)) {
			return false;
		}
		// recherche dans le role dépositaire
		NSArray<EORepartAssociation> lstDepositaires = FinderDepositaires
				.getDepositaireIndivForSalle(ec, salle);
		if (EOQualifier.filteredArrayWithQualifier(
				lstDepositaires,
				EOQualifier.qualifierWithQualifierFormat(
						EORepartAssociation.TO_INDIVIDUS_ASSOCIES_KEY + "."
								+ EOIndividu.PERS_ID_KEY + "=%@",
						new NSArray<Integer>(persId))).count() > 0) {
			return true;
		}

		// recherche dans les groupes dépositaires
		for (EOStructureUlr str : FinderDepositaires.getGroupesDepositaires(ec,
				salle)) {
			NSArray<EOIndividu> lstIndGrp = (NSArray) str.toFwkpers_Structure()
					.toRepartStructures().valueForKey(
							EORepartStructure.TO_INDIVIDU_ELTS_KEY);
			if (EOQualifier.filteredArrayWithQualifier(
					lstIndGrp,
					EOQualifier.qualifierWithQualifierFormat(
							EOIndividu.PERS_ID_KEY + "=%@",
							new NSArray<Integer>(persId))).count() > 0) {
				return true;
			}
		}
		return false;
	}

	public static boolean isIndividuDepositaire(EOEditingContext ec,
			Integer persId) {
		// recherche si il a un role depositaire
		if (EOIndividu.fetchAll(
				ec,
				new EOAndQualifier(new NSArray<EOQualifier>(new EOQualifier[] {
						QUAL_INDIV_ROLE_DEPOSITAIRE,
						EOQualifier.qualifierWithQualifierFormat(
								EOIndividu.PERS_ID_KEY + "=%@",
								new NSArray<Integer>(persId)) }))).size() > 0) {
			return true;
		}

		// rechreche si il appartient a un groupe depositaire
		if (EOStructureUlr
				.fetchFwkGspot_StructureUlrs(
						ec,
						new EOAndQualifier(
								new NSArray<EOQualifier>(
										new EOQualifier[] {
												QUAL_GROUPE_DEPOS,
												EOQualifier
														.qualifierWithQualifierFormat(
																EOStructureUlr.TO_REPART_STRUCTURE_KEY
																		+ "."
																		+ org.cocktail.fwkgspot.serveur.metier.eof.EORepartStructure.TO_FWKPERS__INDIVIDU_KEY
																		+ "."
																		+ EOIndividu.PERS_ID_KEY
																		+ "=%@",
																new NSArray<Integer>(
																		persId)) })),
						null).size() > 0) {
			return true;
		}

		return false;

	}
}
