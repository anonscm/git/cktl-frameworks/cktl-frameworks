package org.cocktail.fwkgspot.serveur.finder;

import org.cocktail.fwkgspot.serveur.metier.eof.EOImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBatImpGeo;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderLocal {

	public static NSArray<EOLocal> getLocalsForImplantation(
			EOImplantationGeo impl, EOEditingContext ec) {
		if ((impl==null)||(ec==null))
			return null;
		return getLocalsForImplantation(impl, ec, null);
	}

	public static NSArray<EOLocal> getLocalsForImplantation(
			EOImplantationGeo impl, EOEditingContext ec, EOQualifier addQual) {
		if ((impl==null)||(ec==null))
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOLocal.TO_REPART_BAT_IMP_GEOS_KEY + "."
				+ EORepartBatImpGeo.TO_IMPLANTATION_GEO_KEY + " = %@";
		tbKeys.add(impl);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOQualifier qual = null;
		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		//que les valid
		qual = new EOAndQualifier(new NSArray<EOQualifier>(
				new EOQualifier[] { qual, EOLocal.QUAL_VALID_LOCAL }));
		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOLocal.ENTITY_NAME,
				qual,
				new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(
								EOLocal.APPELLATION_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);
		NSArray<EOLocal> retour = ec.objectsWithFetchSpecification(fetchSpec);
		return retour;
	}

}
