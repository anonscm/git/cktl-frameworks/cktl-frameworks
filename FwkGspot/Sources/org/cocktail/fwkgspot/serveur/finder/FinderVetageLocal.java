package org.cocktail.fwkgspot.serveur.finder;

import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVTypeSalleLocalEtage;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public abstract class FinderVetageLocal {

	public static NSArray<EOVEtageLocal> getEtageForLocal(EOLocal local,
			EOEditingContext ec) {
		if ((local==null)||(ec==null))
			return null;
		return getEtageForLocal(local, ec, null);
	}

	public static NSArray<EOVEtageLocal> getEtageForLocal(EOLocal local,
			EOEditingContext ec, EOQualifier addQual) {
		if ((local==null)||(ec==null))
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOVEtageLocal.TO_LOCAL_KEY + " = %@";
		tbKeys.add(local);

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOQualifier qual = null;

		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOVEtageLocal.ENTITY_NAME,
				qual,
				new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(
								EOVEtageLocal.SAL_ETAGE_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);

		NSArray<EOVEtageLocal> retour = ec
				.objectsWithFetchSpecification(fetchSpec);

		return retour;
	}
	
	public static NSArray<EOTypeSalle> getTypeSalleForEtageAndLocal(
			EOLocal local, EOVEtageLocal etage, EOEditingContext ec) {
		if ((local==null)||(ec==null))
			return null;
		return getTypeSalleForEtageAndLocal(local, etage, ec, null);
	}

	public static NSArray<EOTypeSalle> getTypeSalleForEtageAndLocal(
			EOLocal local, EOVEtageLocal etage, EOEditingContext ec,
			EOQualifier addQual) {
		if ((local==null)||(ec==null))
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOTypeSalle.TO_V_TYPE_SALLE_LOCAL_ETAGES_KEY
				+ "." + EOVTypeSalleLocalEtage.TO_LOCAL_KEY + " = %@";
		tbKeys.add(local);

		if (etage != null) {
			conditionStr += " AND "
					+ EOTypeSalle.TO_V_TYPE_SALLE_LOCAL_ETAGES_KEY + "."
					+ EOVTypeSalleLocalEtage.SAL_ETAGE_KEY
					+ " caseInsensitiveLike %s";
			tbKeys.add(etage.salEtage());
		}

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOQualifier qual = null;

		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOTypeSalle.ENTITY_NAME,
				qual,
				new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(
								EOTypeSalle.TSAL_LIBELLE_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);

		NSArray<EOTypeSalle> retour = ec
				.objectsWithFetchSpecification(fetchSpec);

		return retour;
	}
}
