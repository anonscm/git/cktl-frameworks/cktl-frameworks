package org.cocktail.fwkgspot.serveur.finder;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkgspot.serveur.metier.eof.EODetailPourcentage;
import org.cocktail.fwkgspot.serveur.metier.eof.EOLocal;
import org.cocktail.fwkgspot.serveur.metier.eof.EORepartBureau;
import org.cocktail.fwkgspot.serveur.metier.eof.EOSalles;
import org.cocktail.fwkgspot.serveur.metier.eof.EOStructureUlr;
import org.cocktail.fwkgspot.serveur.metier.eof.EOTypeSalle;
import org.cocktail.fwkgspot.serveur.metier.eof.EOVEtageLocal;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXQ;

public abstract class FinderSalle {

	public static NSArray<EOSalles> getSallesForLocal(EOLocal local,
			EOEditingContext ec) {
		if ((local==null)||(ec==null))
			return null;
		return getSallesForLocalEtageAndType(local, ec, null, null, null);
	}

	public static NSArray<EOSalles> getSallesForLocalAndEtage(EOLocal local,
			EOEditingContext ec, EOVEtageLocal etg) {
		if ((local==null)||(ec==null))
			return null;
		return getSallesForLocalEtageAndType(local, ec, null, etg, null);
	}

	public static NSArray<EOSalles> getSallesForLocalEtageAndType(
			EOLocal local, EOEditingContext ec, EOVEtageLocal etg,
			EOTypeSalle type) {
		if ((local==null)||(ec==null))
			return null;
		return getSallesForLocalEtageAndType(local, ec, null, etg, type);
	}

	public static NSArray<EOSalles> getSallesForLocalEtageAndType(
			EOLocal local, EOEditingContext ec, EOQualifier addQual,
			EOVEtageLocal etg, EOTypeSalle type) {
		if ((local==null)||(ec==null))
			return null;
		NSMutableArray<Object> tbKeys = new NSMutableArray<Object>();
		String conditionStr = EOSalles.TO_LOCAL_KEY + " = %@";
		tbKeys.add(local);

		if (etg != null) {
			conditionStr += " AND " + EOSalles.SAL_ETAGE_KEY
					+ " caseInsensitiveLike %s ";
			tbKeys.add(etg.salEtage());
		}

		if (type != null) {
			conditionStr += " AND " + EOSalles.TO_TYPE_SALLE_KEY + " = %@ ";
			tbKeys.add(type);
		}

		EOQualifier qualIni = EOQualifier.qualifierWithQualifierFormat(
				conditionStr, tbKeys);
		EOQualifier qual = null;

		if (addQual != null)
			qual = new EOAndQualifier(new NSArray<EOQualifier>(
					new EOQualifier[] { qualIni, addQual }));
		else
			qual = qualIni;

		EOFetchSpecification fetchSpec = new EOFetchSpecification(
				EOSalles.ENTITY_NAME,
				qual,
				new NSArray<EOSortOrdering>(
						new EOSortOrdering[] { new EOSortOrdering(
								EOSalles.SAL_PORTE_KEY,
								EOSortOrdering.CompareCaseInsensitiveAscending) }));
		fetchSpec.setUsesDistinct(true);
		fetchSpec.setIsDeep(true);

		NSArray<EOSalles> retour = ec.objectsWithFetchSpecification(fetchSpec);

		return retour;
	}
	
	/**
	 * @param ec un editing context
	 * @param cStructure un cStructure
	 * @return les localisations de la structure. Les salles associées sont prefetchées.
	 *         La fetch spec utilisée prend en compte aussi les objets récemment 
	 *         insérés ou supprimés.
	 */
	public static NSArray<EODetailPourcentage> getSallesForStructure(EOEditingContext ec,
	                                                        String cStructure) {
	    String keyPath = ERXQ.keyPath(EODetailPourcentage.TO_STRUCTURE_ULR_KEY,
    	                 EOStructureUlr.TO_FWKPERS__STRUCTURE_KEY,
    	                 EOStructure.C_STRUCTURE_KEY);
	    ERXFetchSpecification<EODetailPourcentage> fspec = 
	        new ERXFetchSpecification<EODetailPourcentage>(
	                EODetailPourcentage.ENTITY_NAME, ERXQ.equals(keyPath, cStructure), null);
	    fspec.setIncludeEditingContextChanges(false);
	    fspec.setPrefetchingRelationshipKeyPaths(new NSArray<String>(EODetailPourcentage.TO_SALLES_KEY));
	    return fspec.fetchObjects(ec);
	}
	
	/**
	 * @param ec un editing context
	 * @param noIndividu un numéro d'individu
	 * @return les localisations de l'individu. Les salles associées sont prefetchées.
	 *         La fetch spec utilisée prend en compte aussi les objets récemment 
     *         insérés ou supprimés.
	 */
	public static NSArray<EORepartBureau> getSallesForIndividu(EOEditingContext ec, Number noIndividu) {
	    String keyPath = ERXQ.keyPath(EORepartBureau.TO_INDIVIDU_KEY, EOIndividu.NO_INDIVIDU_KEY);
	    EOQualifier qualifier = ERXQ.equals(keyPath, noIndividu);
	    ERXFetchSpecification fspec = new ERXFetchSpecification<EORepartBureau>(EORepartBureau.ENTITY_NAME, qualifier, null);
        fspec.setIncludeEditingContextChanges(true);
        fspec.setPrefetchingRelationshipKeyPaths(new NSArray<String>(EORepartBureau.TO_SALLES_KEY));
        NSArray<EORepartBureau> reparts = ec.objectsWithFetchSpecification(fspec);
        // BUG SUR LE NESTED - BIDOUILLE A NANAR !
        NSMutableArray<EORepartBureau> resultats = new NSMutableArray<EORepartBureau>();
        for(EORepartBureau repart : reparts) {
        	if(repart.editingContext().equals(ec)) {
        		resultats.add(repart);
        	}
        }
	    return resultats;
	}
	
	
}
