/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlprescolarite.common;

import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects;
import org.cocktail.fwkcktlwebapp.server.version.CktlVersionWonder;

/**
 * Gestion du versioning. Modifiez cette classe pour gérer les dépendances de version. Modifiez la classe VersionMe pour les changements de version.
 */
public final class Version extends A_CktlVersion {
	// Nom de l'appli
	/** The Constant APPLICATIONFINALNAME. */
	public static final String APPLICATIONFINALNAME = VersionMe.APPLICATIONFINALNAME;
	
	/** The Constant APPLICATIONINTERNALNAME. */
	public static final String APPLICATIONINTERNALNAME = VersionMe.APPLICATIONINTERNALNAME;
	
	/** The Constant APPLICATION_STRID. */
	public static final String APPLICATION_STRID = VersionMe.APPLICATION_STRID;
    
	// Version appli
    /** The Constant SERIALVERSIONUID. */
	public static final long SERIALVERSIONUID = VersionMe.SERIALVERSIONUID;
    
    /** The Constant VERSIONNUMMAJ. */
    public static final int VERSIONNUMMAJ =     VersionMe.VERSIONNUMMAJ;
    
    /** The Constant VERSIONNUMMIN. */
    public static final int VERSIONNUMMIN =     VersionMe.VERSIONNUMMIN;
    
    /** The Constant VERSIONNUMPATCH. */
    public static final int VERSIONNUMPATCH =   VersionMe.VERSIONNUMPATCH;
    
    /** The Constant VERSIONNUMBUILD. */
    public static final int VERSIONNUMBUILD =   VersionMe.VERSIONNUMBUILD;
    
    /** The Constant VERSIONDATE. */
    public static final String VERSIONDATE = VersionMe.VERSIONDATE;
    
    /** The Constant COMMENT. */
    public static final String COMMENT = VersionMe.COMMENT;
    
    
//    /** Version de la base de donnees requise */
//    private static final String BD_VERSION_MIN = null;
//    private static final String BD_VERSION_MAX = null;
    
    /** Version du frmk FwkCktlWebApp. */
	private static final String CKTLWEBAPP_VERSION_MIN = "4.0.1";
	
	/** The Constant CKTLWEBAPP_VERSION_MAX. */
	private static final String CKTLWEBAPP_VERSION_MAX = null;

	/** Version de WebObjects. */
    private static final String WO_VERSION_MIN = "5.3.3.0";
    
    /** The Constant WO_VERSION_MAX. */
    private static final String WO_VERSION_MAX = null;
    
    /** Version du JRE. */
    private static final String JRE_VERSION_MIN = "1.5.0.0";
    
    /** The Constant JRE_VERSION_MAX. */
//    private static final String JRE_VERSION_MAX = "1.6";
    private static final String JRE_VERSION_MAX = null;
    
    /** Version d'ORACLE. */
    private static final String ORACLE_VERSION_MIN = "9.0";
    
    /** The Constant ORACLE_VERSION_MAX. */
    private static final String ORACLE_VERSION_MAX = null;
    
    /** Version de WONDER. */
    private static final String WONDER_VERSION_MIN = "5.0.0.9000";
    
    /** The Constant WONDER_VERSION_MAX. */
    private static final String WONDER_VERSION_MAX = null;
    
//    /** Version de JasperReports */
//    private static final String JASPER_VERSION_MIN = "1.2.5";
//    private static final String JASPER_VERSION_MAX = null;
    

	/* (non-Javadoc)
 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#name()
 */
    public String name() {
		return APPLICATIONFINALNAME;
	}
	
	/**
	 * Internal name.
	 *
	 * @return the string
	 */
	public String internalName() {
		return APPLICATIONINTERNALNAME;
	}

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#versionNumBuild()
	 */
	public int versionNumBuild() {
		return VERSIONNUMBUILD;
	}

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#versionNumMaj()
	 */
	public int versionNumMaj() {
		return VERSIONNUMMAJ;
	}

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#versionNumMin()
	 */
	public int versionNumMin() {
		return VERSIONNUMMIN;
	}

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#versionNumPatch()
	 */
	public int versionNumPatch() {
		return VERSIONNUMPATCH;
	}
	
	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#date()
	 */
	public String date() {
		return VERSIONDATE;
	}
	
	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#comment()
	 */
	public String comment() {
		return COMMENT;
	}
	
	
	/**
	 * Liste des d̩ependances.
	 *
	 * @return the cktl version requirements[]
	 * @see org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion#dependencies()
	 */
	public CktlVersionRequirements[] dependencies() {
	    return new CktlVersionRequirements[]{
	    		new CktlVersionRequirements(new CktlVersionWebObjects(), WO_VERSION_MIN, WO_VERSION_MAX, true)
	    		, new CktlVersionRequirements(new CktlVersionJava() , JRE_VERSION_MIN, JRE_VERSION_MAX, true)
	    		, new CktlVersionRequirements(new CktlVersionOracleServer(), ORACLE_VERSION_MIN, ORACLE_VERSION_MAX, false)
	    		//,new CktlVersionRequirements(new VersionDatabase(), BD_VERSION_MIN, BD_VERSION_MAX, false)
				, new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.Version(), CKTLWEBAPP_VERSION_MIN, CKTLWEBAPP_VERSION_MAX, true)
	    		, new CktlVersionRequirements(new CktlVersionWonder(), WONDER_VERSION_MIN, WONDER_VERSION_MAX, true)
	    		//,new CktlVersionRequirements(new CktlVersionJar("net.sf.jasperreports.engine.JasperReport"), JASPER_VERSION_MIN, JASPER_VERSION_MAX, true)
	      };
	}
	    
}
