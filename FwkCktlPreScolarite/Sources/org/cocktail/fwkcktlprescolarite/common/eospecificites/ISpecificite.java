/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.eospecificites;

import org.cocktail.fwkcktlprescolarite.common.metier.AFwkCktlPreScolariteRecord;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

// TODO: Auto-generated Javadoc
/**
 * Interface a respecter pour ajouter des specificites métier à l'objet. Pour chaque objet déclaré comme spécificité, les méthodes seront appelées
 * lors des étapes de la vie de l'objet, que ce soit lors de la création ou lors des étapes de validation. Ceci permet par exemple d'ajouter du code
 * pour définir les règles métier d'un partenaire (qui est une spécificité d'un objet EOStructure). <br/>
 
 * @author isabelle Reau <isabelle.reau at asso-cocktail.fr>
 *
 */
public interface ISpecificite {
	
	
	/**
	 * Checks if is specificite.
	 *
	 * @param prescolariteRecord the prescolarite record
	 * @return true si l'objet coriandreRecord peut être considéré comme une spécificité.
	 */
	public boolean isSpecificite(AFwkCktlPreScolariteRecord prescolariteRecord);

	/**
	 * Appele lors de l'insertion dans l'editing context.<br/>
	 * NB : Cette methode ne peut etre appelee que si l'instance de l'objet a été créée en indiquant la spécificité dans les arguments de la methode (
	 *
	 * @param prescolariteRecord the prescolarite record
	 * {@link AFwkCktlPreScolariteRecord#createAndInsertInstance(EOEditingContext, String, NSArray)}.
	 */
	public void onAwakeFromInsertion(AFwkCktlPreScolariteRecord prescolariteRecord);

	/**
	 * Appele par le saveChanges lors de l'insertion d'un objet (par validateForUpdate).
	 *
	 * @param prescolariteRecord L'objet sur lequel porte la methode.
	 * @throws ValidationException the validation exception
	 */
	public void onValidateForInsert(AFwkCktlPreScolariteRecord prescolariteRecord) throws NSValidation.ValidationException;

	/**
	 * Appele par le saveChanges lors de la mise a jour d'un objet (par validateForUpdate).
	 *
	 * @param prescolariteRecord L'objet sur lequel porte la methode.
	 * @throws ValidationException the validation exception
	 */
	public void onValidateForUpdate(AFwkCktlPreScolariteRecord prescolariteRecord) throws NSValidation.ValidationException;

	/**
	 * Appele par le saveChanges lors lors de la suppression d'un objet (par validateForDelete).
	 *
	 * @param prescolariteRecord L'objet sur lequel porte la methode.
	 * @throws ValidationException the validation exception
	 */
	public void onValidateForDelete(AFwkCktlPreScolariteRecord prescolariteRecord) throws NSValidation.ValidationException;

	/**
	 * Appele avant l'enregistrement d'un objet (par validateObjectMetier).
	 *
	 * @param prescolariteRecord L'objet sur lequel porte la methode.
	 * @throws ValidationException the validation exception
	 */
	public void onValidateObjectMetier(AFwkCktlPreScolariteRecord prescolariteRecord) throws NSValidation.ValidationException;

	/**
	 * Appele avant l'enregistrement d'un objet (par ValidateBeforeTransactionSave).
	 *
	 * @param prescolariteRecord L'objet sur lequel porte la methode.
	 * @throws ValidationException the validation exception
	 */
	public void onValidateBeforeTransactionSave(AFwkCktlPreScolariteRecord prescolariteRecord) throws NSValidation.ValidationException;



}
