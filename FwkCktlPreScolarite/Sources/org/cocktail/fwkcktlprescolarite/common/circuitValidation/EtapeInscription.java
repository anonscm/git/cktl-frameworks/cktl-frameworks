/*
     * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software 
     * is governed by the CeCILL license under French law and abiding by the
     * rules of distribution of free software. You can use, modify and/or 
     * redistribute the software under the terms of the CeCILL license as 
     * circulated by CEA, CNRS and INRIA at the following URL 
     * "http://www.cecill.info". 
     * As a counterpart to the access to the source code and rights to copy, modify 
     * and redistribute granted by the license, users are provided only with a 
     * limited warranty and the software's author, the holder of the economic 
     * rights, and the successive licensors have only limited liability. In this 
     * respect, the user's attention is drawn to the risks associated with loading,
     * using, modifying and/or developing or reproducing the software by the user 
     * in light of its specific status of free software, that may mean that it
     * is complicated to manipulate, and that also therefore means that it is 
     * reserved for developers and experienced professionals having in-depth
     * computer knowledge. Users are therefore encouraged to load and test the 
     * software's suitability as regards their requirements in conditions enabling
     * the security of their systems and/or data to be ensured and, more generally, 
     * to use and operate it in the same conditions as regards security. The
     * fact that you are presently reading this means that you have had knowledge 
     * of the CeCILL license and that you accept its terms.
     */
package org.cocktail.fwkcktlprescolarite.common.circuitValidation;

import org.cocktail.fwkcktlworkflow.serveur.metier.EOEtape;


public enum EtapeInscription {
	/** Etape "Inscription à valider par l'étudiant". */
	VALID_ETUDIANT ("VALID_ETUDIANT", null, "En attente", "Visa étudiant"),
	/** Etape "Fiche à valider par l'étudiant". */
//	VALID_ETU_IMPRESSION ("VALID_ETU_IMPRESSION", null, "Validation par l''étudiant (impression)", "Visa étudiant"),
	/** Etape "Fiche à valider par le gestionnaire". */
	VALID_GESTIONNAIRE ("VALID_GESTIONNAIRE", null, "A valider" , "Visa gestionnaire"),
	/** Etape "Fiche à valider par le gestionnaire". */
	VALID_GES_PAIEMENT ("VALID_GES_PAIEMENT", null, "En attente de paiement", "Visa gestionnaire"),

	/** Etape "Fiche à valider par le gestionnaire". */
	VALID_GES_IMPRESSION ("VALID_GES_IMPRESSION", null, "En attente d''impression/validation ", "Visa gestionnaire"),
	/** Etape finale "Fiche validée". */
	VALIDEE ("VALIDEE", null, "", "Validée");

	/** Code de l'étape. */
	private String codeEtape;
	
	/**
	 * Circuit de l'étape.
	 * <p>
	 * Ce circuit peut être <code>null</code> pour indiqué "peu importe le circuit" pour une étape multi-circuit.
	 */
	private CircuitInscription circuit;
	
	/** Le texte de l'état de la demande pour cette étape. */
	private String etatDemande;
	
	/** Le libellé du visa pour cette étape. */
	private String libelleVisa;
	
	/**
	 * Liste des fonctions autorisées à faire avancer une demande depuis cette étape.
	 * <p>
	 * Si cette liste est <code>null</code>, n'importe qui à les droits sur cette étape.
	 */
	private String[] listeFonctionsAutorisees;
	
	/**
	 * Constructeur.
	 * 
	 * @param codeEtape Le code de l'étape
	 * @param circuit Le code du circuit (<code>null</code> pour "n'importe quel circuit")
	 * @param etatDemande Le texte de l'état de la demande pour cette étape
	 * @param fonctions Liste des fonction autorisées à faire avancer une demande depuis cette étape (<code>null</code> pour "n'importe qui")
	 */
	EtapeInscription(String codeEtape, CircuitInscription circuit, String etatDemande, String libelleVisa, String... fonctions) {
		this.circuit = circuit;
		this.codeEtape = codeEtape;
		this.etatDemande = etatDemande;
		this.libelleVisa = libelleVisa;
		this.listeFonctionsAutorisees = fonctions;
	}

	/**
	 * Retourne l'étape correspondant à l'étape.
	 * <p>
	 * L'étape retournée en priorité est celle correspondant au code circuit et au code étape.
	 * Si elle n'est pas trouvé celle correspondant au code étape qui à un code circuit à <code>null</code>.
	 * 
	 * @param etape Une étape
	 * @return L'étape si trouvée ou <code>null</code>
	 */
	public static EtapeInscription getEtape(EOEtape etape) {
		return getEtape(etape.toCircuitValidation().codeCircuitValidation(), etape.codeEtape());
	}
	
	/**
	 * Retourne l'étape correspondant au code circuit et au code étape.
	 * <p>
	 * L'étape retournée en priorité est celle correspondant au code circuit et au code étape.
	 * Si elle n'est pas trouvé celle correspondant au code étape qui à un code circuit à <code>null</code>.
	 * 
	 * @param codeCircuit Un code circuit
	 * @param codeEtape Un code étape
	 * @return L'étape si trouvée ou <code>null</code>
	 */
	public static EtapeInscription getEtape(String codeCircuit, String codeEtape) {
		EtapeInscription resultat = null;
		
		for (int i = 0; i < EtapeInscription.values().length; i++) {
			EtapeInscription etape = EtapeInscription.values()[i];
			
			if (etape.codeEtape.equals(codeEtape)) {
				if (etape.circuit == null) {
					resultat = etape;
				} else if (etape.circuit.getCodeCircuit().equals(codeCircuit)) {
					// Niveau le plus fin trouvé, on sort
					resultat = etape;
					break;
				}
			}
		}
		
		return resultat;
	}
	
	/**
	 * Retourne le code étape de cette étape.
	 * 
	 * @return Le code étape
	 */
	public String getCodeEtape() {
		return codeEtape;
	}

	/**
	 * Retourne le texte de l'état de la demande pour cette étape.
	 * 
	 * @return Le texte de l'état de la demande
	 */
	public String getEtatDemande() {
		return etatDemande;
	}

	/**
	 * Retourne le libellé du visa pour cette étape.
	 * 
	 * @return Le libellé du visa
	 */
	public String getLibelleVisa() {
		return libelleVisa;
	}

	/**
	 * Retourne la liste des fonctions autorisées. Peut retourner <code>null</code>.
	 * 
	 * @return La liste des fonctions autorisées. Peut retourner <code>null</code>.
	 */
	public String[] getListeFonctionsAutorisees() {
		return listeFonctionsAutorisees;
	}

}
