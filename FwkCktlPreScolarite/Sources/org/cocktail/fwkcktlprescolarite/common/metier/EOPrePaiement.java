/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.math.BigDecimal;
import java.util.List;

import javax.annotation.Nullable;

import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBordereau;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementCBPrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementChequePrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementVirementPrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.TypePaiement;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * {@inheritDoc}
 */
public class EOPrePaiement extends _EOPrePaiement implements IPrePaiement {

	private static final long serialVersionUID = 8727467133654411784L;
	
	@Inject
	@Nullable
	private UserInfo userInfo;
	
	/** 
     * {@inheritDoc}
     */
    public Integer id() {
    	return primaryKey() == null ? null : Integer.valueOf(primaryKey());
    }
    
	/**
	 * {@inheritDoc}
	 */
    public IEtudiantAnnee toEtudiantAnnee() {
    	return super.toPreEtudiantAnnee();
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantAnneeRelationship(IEtudiantAnnee unEtudiantAnnee) {
		super.setToPreEtudiantAnneeRelationship((EOPreEtudiantAnnee) unEtudiantAnnee);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetails() {
		return super.toPrePaiementDetails();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementMoyen> toPaiementMoyens() {
		return super.toPrePaiementMoyens();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementMoyen> toPaiementMoyensTries() {
		return EOPrePaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.ORDRE).desc().sorted(super.toPrePaiementMoyens());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<EOPrePaiementDetail> toPrePaiementDetailsTries() {
		List<EOPrePaiementDetail> listeDetailPaiement = toPrePaiementDetailFormations();
		listeDetailPaiement.addAll(toPrePaiementDetailDiplomes());
		listeDetailPaiement.addAll(toPrePaiementDetailArticlesComplementaires());
		
		return listeDetailPaiement;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailsTries() {
		return toPrePaiementDetailsTries();
	}

	/**
	 * {@inheritDoc}
	 */
	public List<EOPrePaiementDetail> toPrePaiementDetailFormations() {
		return toPrePaiementDetails(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.isNotNull(),
				EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.dot(EOParametragePaieFormation.TO_STRUCTURE).dot(EOStructure.LC_STRUCTURE).asc()
				.then(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_FORMATION.dot(EOParametragePaieFormation.TO_TYPE_FORMATION).dot(EOTypeFormation.LIBELLE).asc()),
				false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailFormations() {
		return toPrePaiementDetailFormations();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<EOPrePaiementDetail> toPrePaiementDetailDiplomes() {
		return toPrePaiementDetails(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.isNotNull(),
				EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.dot(EOParametragePaieDiplome.TO_DIPLOME).dot(EODiplome.LIBELLE).asc()
				.then(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_DIPLOME.dot(EOParametragePaieDiplome.TO_PARCOURS).dot(EOParcours.LIBELLE).asc()),
				false);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailDiplomes() {
		return toPrePaiementDetailDiplomes();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<EOPrePaiementDetail> toPrePaiementDetailArticlesComplementaires() {
		return toPrePaiementDetails(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.isNotNull(),
				EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.dot(EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE).dot(EOTypeArticleComplementaire.LIBELLE).asc()
				.then(EOPrePaiementDetail.TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE.dot(EOParametragePaieArticleComplementaire.LIBELLE).asc()),
				false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailArticlesComplementaires() {
		return toPrePaiementDetailArticlesComplementaires();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPrePaiementDetail> toPrePaiementDetailArticlesAuto() {
		return toPrePaiementDetails(EOPrePaiementDetail.AUTO.isTrue(), null, false);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailArticlesAuto() {
		return toPrePaiementDetailArticlesAuto();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPrePaiementDetail> toPrePaiementDetailArticlesManuel() {
		return toPrePaiementDetails(EOPrePaiementDetail.AUTO.isFalse(), null, false);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementDetail> toPaiementDetailArticlesManuel() {
		return toPrePaiementDetailArticlesManuel();
	}
	
	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de paiement
	 */
	@Override
	public void setDatePaiement(NSTimestamp date) {
		if (date == null) {
			super.setDatePaiement(null);
		} else {
			super.setDatePaiement(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}

	/**
	 * {@inheritDoc}
	 */
	public TypePaiement typePaiement() {
		if (isInitial()) {
			return TypePaiement.PAIEMENT_INITIAL;
		} else if (montant().compareTo(BigDecimal.ZERO) >= 0) {
			return TypePaiement.PAIEMENT;
		} else if (!toEtudiantAnnee().valide() && isDernier()) {
			return TypePaiement.DEMISSION;
		} else {
			return TypePaiement.REMBOURSEMENT;
		}
	}
	
	/**
	 * @return le mode de paiement, peut-être simple ou combinés (chèque + prel)
	 */
	public IModePaiement modePaiement() {
		IModePaiement modePaiement = null;
		if (hasPlusieursPaiementMoyens()) {
			modePaiement = modePaiementCombine();
		} else if (hasUnPaiementMoyen()) {
			modePaiement = modePaiementSimple();
		}
		return modePaiement;
	}
	
	private IModePaiement modePaiementSimple() {
		return toPrePaiementMoyens().lastObject().toModePaiement();
	}

	public IModePaiement modePaiementCombine() {
		IModePaiement modePaiement = null;
		NSArray<String> codesModePaiement = codesModePaiement();
		if (codesModePaiement.contains(IModePaiement.CODE_MODE_PRELEVEMENT)) {
			if (codesModePaiement.contains(IModePaiement.CODE_MODE_CHEQUE)) {
				modePaiement = ModePaiementChequePrelevement.instance();
			} else if (codesModePaiement.contains(IModePaiement.CODE_MODE_CB)) {
				modePaiement = ModePaiementCBPrelevement.instance();
			} else if (codesModePaiement.contains(IModePaiement.CODE_MODE_VIREMENT)) {
				modePaiement = ModePaiementVirementPrelevement.instance();
			}
		} 
		return modePaiement;
	}

	@SuppressWarnings("unchecked")
	private NSArray<String> codesModePaiement() {
		NSArray<String> codesModePaiement = 
				(NSArray<String>) toPrePaiementMoyens().valueForKey(EOPrePaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).key());
		return codesModePaiement;
	}

	private boolean hasUnPaiementMoyen() {
		return toPrePaiementMoyens().count() == 1;
	}
	
	public boolean hasPlusieursPaiementMoyens() {
		return toPrePaiementMoyens().count() > 1;
	}

	public boolean hasPaiementMoyenPrelevement() {
		return codesModePaiement().contains(IModePaiement.CODE_MODE_PRELEVEMENT);
	}

	/** 
     * {@inheritDoc}
     */
	public BigDecimal montantTotalDu() {
		BigDecimal montantTotalDu = BigDecimal.ZERO;
		
		for (IPaiementDetail detail : toPaiementDetails()) {
			if (detail.montantAPayer() != null) {
				montantTotalDu = montantTotalDu.add(detail.montantAPayer());
			}
			if (detail.montantARembourser() != null) {
				montantTotalDu = montantTotalDu.subtract(detail.montantARembourser());
			}
		}

		return montantTotalDu;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean aUnLienAvecGfc() {
		return false;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IBordereau toBordereau() {
		// Jamais de bordereau pour un prePaiement
		return null;
	}
	
	/**
	 * {@inheritDoc}
	 */
    public boolean isInitial() {
    	return ordre() == 1;
    }
    
	/**
	 * {@inheritDoc}
	 */
	public boolean isDernier() {
		return this.equals(toPreEtudiantAnnee().toPrePaiements(null, EOPrePaiement.ORDRE.ascs(), false).lastObject());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IPrePaiementMoyen toPrePaiementMoyenPrelevement() {
		return toPrePaiementMoyens(EOPrePaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).eq(IModePaiement.CODE_MODE_PRELEVEMENT)).lastObject();
	}

	/**
	 * {@inheritDoc}
	 */
	public IPaiementMoyen toPaiementMoyenPrelevement() {
		return toPrePaiementMoyenPrelevement();
	}
	
	/**
	 * {@inheritDoc}
	 */
	public IPaiementMoyen toPaiementMoyenPremiereEcheance() {
		NSArray<EOPrePaiementMoyen> preMoyenPaiements = toPrePaiementMoyens(EOPrePaiementMoyen.TO_MODE_PAIEMENT.dot(EOModePaiement.CODE).ne(IModePaiement.CODE_MODE_PRELEVEMENT));
		return preMoyenPaiements.lastObject();
	}
}
