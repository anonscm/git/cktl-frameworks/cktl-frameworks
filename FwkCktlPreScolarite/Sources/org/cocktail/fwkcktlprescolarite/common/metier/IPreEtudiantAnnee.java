package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;

/**
 * Information de l'étudiant relatives à une année donnée
 */
/**
 * @author lilia
 * 
 */
public interface IPreEtudiantAnnee extends IEtudiantAnnee {

	/**
	 * @return lien vers un preEtudiant
	 */
	IPreEtudiant toPreEtudiant();

	/**
	 * @param value un pre étudiant
	 */
	void setToPreEtudiantRelationship(IPreEtudiant value);

	/**
	 * @return liste des pre-paiements de l'étudiants (composé d'un seul élément)
	 */
	List<? extends IPrePaiement> toPrePaiements();

	/**
	 * @return Le paiement initial (le premier paiement)
	 */
	IPrePaiement toPrePaiementInitial();

}
