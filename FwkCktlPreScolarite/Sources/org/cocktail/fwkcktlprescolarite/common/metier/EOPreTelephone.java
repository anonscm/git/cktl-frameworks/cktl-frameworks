/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.Comparator;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

// TODO: Auto-generated Javadoc
/**
 * classe métier PreTelephone.
 * @author isabelle
 */
public class EOPreTelephone extends _EOPreTelephone implements IPreTelephone {

	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreTelephone.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	public static final String TYPE_TEL_ETUD = "ETUD";

	// TODO jamais utilisé. A virer?
	public static final EOQualifier QUAL_TYPE_TEL_ETUD = new EOKeyValueQualifier(EOPreTelephone.TYPE_TEL_KEY, EOQualifier.QualifierOperatorEqual, TYPE_TEL_ETUD);

	/**
	 * Instantiates a new eO pre telephone.
	 */
	public EOPreTelephone() {
		super();
	}

	/**
	 * Creer.
	 * @param edc : contexte d'édition
	 * @param etudiant the etudiant to set
	 * @param persId the persId to set
	 * @return EOPreTelephone
	 */
	public static EOPreTelephone creer(EOEditingContext edc, EOPreEtudiant etudiant, Integer persId) {
		EOPreTelephone tel = new EOPreTelephone();
		edc.insertObject(tel);
		tel.setToPreEtudiantRelationship(etudiant);
		tel.setPersIdCreation(persId);
		tel.setPersIdModifcation(persId);
		tel.setDCreation(new NSTimestamp());
		tel.setDModification(new NSTimestamp());

		return tel;
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public void setToPreEtudiantRelationship(IPreEtudiant value) {
		setToPreEtudiantRelationship((EOPreEtudiant) value);

	}

	/**
	 * Comparateur pour trier les n° de téléphones.
	 * <ol>
	 * <li>En premier les mobiles étudiant,</li>
	 * <li>puis les n° étudiant,</li>
	 * <li>puis les autres.</li>
	 * </ol>
	 */
	public static class ComparatorEtudiantMobilePuisEtudiantPuisLesAutres implements Comparator<EOPreTelephone> {
		/**
		 * {@inheritDoc}
		 */
		public int compare(EOPreTelephone tel1, EOPreTelephone tel2) {
			boolean isTel1Etudiant = EOTypeTel.C_TYPE_TEL_ETUD.equals(tel1.toTypeTels().cTypeTel());
			boolean isTel2Etudiant = EOTypeTel.C_TYPE_TEL_ETUD.equals(tel2.toTypeTels().cTypeTel());

			if (isTel1Etudiant && isTel2Etudiant) {
				// Si deux n° étudiant, le mobile est prioritaire
				boolean isTel1Mobile = EOTypeNoTel.TYPE_NO_TEL_MOB.equals(tel1.toTypeNoTel().cTypeNoTel());
				boolean isTel2Mobile = EOTypeNoTel.TYPE_NO_TEL_MOB.equals(tel2.toTypeNoTel().cTypeNoTel());

				if (isTel1Mobile && isTel2Mobile) {
					return 0;
				} else if (isTel1Mobile) {
					return -1;
				} else if (isTel2Mobile) {
					return 1;
				}
			} else if (isTel1Etudiant) {
				return -1;
			} else if (isTel2Etudiant) {
				return 1;
			}
			return 0; // Peu importe
		}
	}

	public void setToTypeNoTelRelationship(ITypeNoTel value) {
		super.setToTypeNoTelRelationship((EOTypeNoTel) value);

	}

	public void setToTypeTelsRelationship(ITypeTel value) {
		super.setToTypeTelsRelationship((EOTypeTel) value);
	}

	public String getTelephoneFormateAvecIndicatif() {
		// TODO méthode redondante dans EOPersonneTelephone
		return  getTelephoneFormateSuivantIndicatif(noTelephone());
	}

	/**
	 * Mise en forme pour des numéros français ou allemands
	 * Pas de mise en forme si aucun des 2
	 * @param telephone
	 * @return
	 */
	private String getTelephoneFormateSuivantIndicatif(String telephone){
		String res = telephone;
		if (res != null && indicatif() != null){
			res = MyStringCtrl.formatPhoneNumber(noTelephone());
			
			if ( 49 == indicatif()){
				res = MyStringCtrl.formatPhoneNumberGermany(noTelephone());
			}
			
			res = "(+" + indicatif() + ") " + res;
		}
		return res;
	}
}
