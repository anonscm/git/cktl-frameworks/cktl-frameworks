/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreInscription.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreInscription extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreInscription.class);

	public static final String ENTITY_NAME = "EOPreInscription";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_INSCRIPTION";


// Attribute Keys
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Boolean> CYCLE_AMENAGE = new ERXKey<Boolean>("cycleAmenage");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> ID_ETABLISSEMENT_CUMULATIF = new ERXKey<Integer>("idEtablissementCumulatif");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_ANNEE = new ERXKey<String>("idLienConsultationAnnee");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_DIPLOME = new ERXKey<String>("idLienConsultationDiplome");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_PARCOURS_ANNEE = new ERXKey<String>("idLienConsultationParcoursAnnee");
  public static final ERXKey<String> ID_LIEN_CONSULTATION_PARCOURS_DIPLOME = new ERXKey<String>("idLienConsultationParcoursDiplome");
  public static final ERXKey<Integer> ID_PARCOURS_ANNEE = new ERXKey<Integer>("idParcoursAnnee");
  public static final ERXKey<Integer> ID_PARCOURS_DIPLOME = new ERXKey<Integer>("idParcoursDiplome");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Boolean> PASSAGE_CONDITIONNEL = new ERXKey<Boolean>("passageConditionnel");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Boolean> REDOUBLEMENT = new ERXKey<Boolean>("redoublement");
  public static final ERXKey<Boolean> REORIENTATION = new ERXKey<Boolean>("reorientation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome> TO_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome>("toDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif> TO_ETABLISSEMENT_CUMULATIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif>("toEtablissementCumulatif");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> TO_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>("toEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> TO_PARCOURS_ANNEE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("toParcoursAnnee");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours> TO_PARCOURS_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours>("toParcoursDiplome");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription> TO_REGIME_INSCRIPTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription>("toRegimeInscription");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange> TO_TYPE_ECHANGE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange>("toTypeEchange");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation> TO_TYPE_INSCRIPTION_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation>("toTypeInscriptionFormation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreInscription";

	public static final String ANNEE_KEY = "annee";
	public static final String CYCLE_AMENAGE_KEY = "cycleAmenage";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ID_ETABLISSEMENT_CUMULATIF_KEY = "idEtablissementCumulatif";
	public static final String ID_LIEN_CONSULTATION_ANNEE_KEY = "idLienConsultationAnnee";
	public static final String ID_LIEN_CONSULTATION_DIPLOME_KEY = "idLienConsultationDiplome";
	public static final String ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY = "idLienConsultationParcoursAnnee";
	public static final String ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY = "idLienConsultationParcoursDiplome";
	public static final String ID_PARCOURS_ANNEE_KEY = "idParcoursAnnee";
	public static final String ID_PARCOURS_DIPLOME_KEY = "idParcoursDiplome";
	public static final String NIVEAU_KEY = "niveau";
	public static final String PASSAGE_CONDITIONNEL_KEY = "passageConditionnel";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String REDOUBLEMENT_KEY = "redoublement";
	public static final String REORIENTATION_KEY = "reorientation";

// Attributs non visibles
	public static final String GRADE_KEY = "grade";
	public static final String ID_DIPLOME_KEY = "idDiplome";
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String ID_PRE_ETUDIANT_ANNEE_KEY = "idPreEtudiantAnnee";
	public static final String ID_PRE_INSCRIPTION_KEY = "idPreInscription";
	public static final String ID_REGIME_INSCRIPTION_KEY = "idRegimeInscription";
	public static final String IDTYPE_ECHANGE_KEY = "idtypeEchange";
	public static final String ID_TYPE_INSCRIPTION_FORMATION_KEY = "idTypeInscriptionFormation";

//Colonnes dans la base de donnees
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String CYCLE_AMENAGE_COLKEY = "CYCLE_AMENAGE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ID_ETABLISSEMENT_CUMULATIF_COLKEY = "ID_ETABLISSEMENT_CUMULATIF";
	public static final String ID_LIEN_CONSULTATION_ANNEE_COLKEY = "ID_LIEN_CT_ANNEE";
	public static final String ID_LIEN_CONSULTATION_DIPLOME_COLKEY = "ID_LIEN_CT_DIPLOME";
	public static final String ID_LIEN_CONSULTATION_PARCOURS_ANNEE_COLKEY = "ID_LIEN_CT_PARC_AN";
	public static final String ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_COLKEY = "ID_LIEN_CT_PARC_DIP";
	public static final String ID_PARCOURS_ANNEE_COLKEY = "ID_PARCOURS_ANNEE";
	public static final String ID_PARCOURS_DIPLOME_COLKEY = "ID_PARCOURS_DIPLOME";
	public static final String NIVEAU_COLKEY = "NIVEAU";
	public static final String PASSAGE_CONDITIONNEL_COLKEY = "PASSAGE_CONDITIONNEL";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String REDOUBLEMENT_COLKEY = "REDOUBLEMENT";
	public static final String REORIENTATION_COLKEY = "REORIENTATION";

	public static final String GRADE_COLKEY = "GRADE";
	public static final String ID_DIPLOME_COLKEY = "ID_DIPLOME";
	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String ID_PRE_ETUDIANT_ANNEE_COLKEY = "ID_PRE_ETUDIANT_ANNEE";
	public static final String ID_PRE_INSCRIPTION_COLKEY = "ID_PRE_INSCRIPTION";
	public static final String ID_REGIME_INSCRIPTION_COLKEY = "ID_REGIME_INSCRIPTION";
	public static final String IDTYPE_ECHANGE_COLKEY = "ID_TYPE_ECHANGE";
	public static final String ID_TYPE_INSCRIPTION_FORMATION_COLKEY = "ID_TYPE_INSCRIPTION_FORMATION";


	// Relationships
	public static final String TO_DIPLOME_KEY = "toDiplome";
	public static final String TO_ETABLISSEMENT_CUMULATIF_KEY = "toEtablissementCumulatif";
	public static final String TO_ETUDIANT_ANNEE_KEY = "toEtudiantAnnee";
	public static final String TO_GRADE_UNIVERSITAIRE_KEY = "toGradeUniversitaire";
	public static final String TO_PARCOURS_ANNEE_KEY = "toParcoursAnnee";
	public static final String TO_PARCOURS_DIPLOME_KEY = "toParcoursDiplome";
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";
	public static final String TO_REGIME_INSCRIPTION_KEY = "toRegimeInscription";
	public static final String TO_TYPE_ECHANGE_KEY = "toTypeEchange";
	public static final String TO_TYPE_INSCRIPTION_FORMATION_KEY = "toTypeInscriptionFormation";



	// Accessors methods
  public Integer annee() {
    return (Integer) storedValueForKey(ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    takeStoredValueForKey(value, ANNEE_KEY);
  }

  public Boolean cycleAmenage() {
    return (Boolean) storedValueForKey(CYCLE_AMENAGE_KEY);
  }

  public void setCycleAmenage(Boolean value) {
    takeStoredValueForKey(value, CYCLE_AMENAGE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer idEtablissementCumulatif() {
    return (Integer) storedValueForKey(ID_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public void setIdEtablissementCumulatif(Integer value) {
    takeStoredValueForKey(value, ID_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public String idLienConsultationAnnee() {
    return (String) storedValueForKey(ID_LIEN_CONSULTATION_ANNEE_KEY);
  }

  public void setIdLienConsultationAnnee(String value) {
    takeStoredValueForKey(value, ID_LIEN_CONSULTATION_ANNEE_KEY);
  }

  public String idLienConsultationDiplome() {
    return (String) storedValueForKey(ID_LIEN_CONSULTATION_DIPLOME_KEY);
  }

  public void setIdLienConsultationDiplome(String value) {
    takeStoredValueForKey(value, ID_LIEN_CONSULTATION_DIPLOME_KEY);
  }

  public String idLienConsultationParcoursAnnee() {
    return (String) storedValueForKey(ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY);
  }

  public void setIdLienConsultationParcoursAnnee(String value) {
    takeStoredValueForKey(value, ID_LIEN_CONSULTATION_PARCOURS_ANNEE_KEY);
  }

  public String idLienConsultationParcoursDiplome() {
    return (String) storedValueForKey(ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY);
  }

  public void setIdLienConsultationParcoursDiplome(String value) {
    takeStoredValueForKey(value, ID_LIEN_CONSULTATION_PARCOURS_DIPLOME_KEY);
  }

  public Integer idParcoursAnnee() {
    return (Integer) storedValueForKey(ID_PARCOURS_ANNEE_KEY);
  }

  public void setIdParcoursAnnee(Integer value) {
    takeStoredValueForKey(value, ID_PARCOURS_ANNEE_KEY);
  }

  public Integer idParcoursDiplome() {
    return (Integer) storedValueForKey(ID_PARCOURS_DIPLOME_KEY);
  }

  public void setIdParcoursDiplome(Integer value) {
    takeStoredValueForKey(value, ID_PARCOURS_DIPLOME_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    takeStoredValueForKey(value, NIVEAU_KEY);
  }

  public Boolean passageConditionnel() {
    return (Boolean) storedValueForKey(PASSAGE_CONDITIONNEL_KEY);
  }

  public void setPassageConditionnel(Boolean value) {
    takeStoredValueForKey(value, PASSAGE_CONDITIONNEL_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Boolean redoublement() {
    return (Boolean) storedValueForKey(REDOUBLEMENT_KEY);
  }

  public void setRedoublement(Boolean value) {
    takeStoredValueForKey(value, REDOUBLEMENT_KEY);
  }

  public Boolean reorientation() {
    return (Boolean) storedValueForKey(REORIENTATION_KEY);
  }

  public void setReorientation(Boolean value) {
    takeStoredValueForKey(value, REORIENTATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome)storedValueForKey(TO_DIPLOME_KEY);
  }

  public void setToDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome oldValue = toDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif toEtablissementCumulatif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif)storedValueForKey(TO_ETABLISSEMENT_CUMULATIF_KEY);
  }

  public void setToEtablissementCumulatifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtablissementCumulatif oldValue = toEtablissementCumulatif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETABLISSEMENT_CUMULATIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETABLISSEMENT_CUMULATIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee toEtudiantAnnee() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee)storedValueForKey(TO_ETUDIANT_ANNEE_KEY);
  }

  public void setToEtudiantAnneeRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee oldValue = toEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETUDIANT_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(TO_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours toParcoursAnnee() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(TO_PARCOURS_ANNEE_KEY);
  }

  public void setToParcoursAnneeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = toParcoursAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARCOURS_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARCOURS_ANNEE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours toParcoursDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours)storedValueForKey(TO_PARCOURS_DIPLOME_KEY);
  }

  public void setToParcoursDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours oldValue = toParcoursDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARCOURS_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARCOURS_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription toRegimeInscription() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription)storedValueForKey(TO_REGIME_INSCRIPTION_KEY);
  }

  public void setToRegimeInscriptionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription oldValue = toRegimeInscription();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGIME_INSCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGIME_INSCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange toTypeEchange() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange)storedValueForKey(TO_TYPE_ECHANGE_KEY);
  }

  public void setToTypeEchangeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange oldValue = toTypeEchange();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ECHANGE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ECHANGE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation toTypeInscriptionFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation)storedValueForKey(TO_TYPE_INSCRIPTION_FORMATION_KEY);
  }

  public void setToTypeInscriptionFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation oldValue = toTypeInscriptionFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_INSCRIPTION_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_INSCRIPTION_FORMATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOPreInscription avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreInscription createEOPreInscription(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome toDiplome, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee toEtudiantAnnee, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant			) {
    EOPreInscription eo = (EOPreInscription) createAndInsertInstance(editingContext, _EOPreInscription.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
    eo.setToDiplomeRelationship(toDiplome);
    eo.setToEtudiantAnneeRelationship(toEtudiantAnnee);
    eo.setToPreEtudiantRelationship(toPreEtudiant);
    return eo;
  }

  
	  public EOPreInscription localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreInscription)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreInscription creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreInscription creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreInscription object = (EOPreInscription)createAndInsertInstance(editingContext, _EOPreInscription.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreInscription localInstanceIn(EOEditingContext editingContext, EOPreInscription eo) {
    EOPreInscription localInstance = (eo == null) ? null : (EOPreInscription)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreInscription#localInstanceIn a la place.
   */
	public static EOPreInscription localInstanceOf(EOEditingContext editingContext, EOPreInscription eo) {
		return EOPreInscription.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreInscription fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreInscription fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreInscription> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreInscription)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreInscription fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreInscription> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreInscription eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreInscription)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreInscription fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreInscription eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreInscription ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreInscription fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
