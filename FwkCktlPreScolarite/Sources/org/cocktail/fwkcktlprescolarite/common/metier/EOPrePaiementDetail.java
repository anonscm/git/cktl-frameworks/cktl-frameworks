/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import javax.annotation.Nullable;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieFormation;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.google.inject.Inject;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * @author isabelle
 */
public class EOPrePaiementDetail extends _EOPrePaiementDetail implements IPrePaiementDetail {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	@Inject
	@Nullable
	private UserInfo userInfo;

	/**
	 * @param paiement un paiement
	 */
	public void setToPaiementRelationship(IPaiement paiement) {
		super.setToPrePaiementRelationship((EOPrePaiement) paiement);
	}
	
	/**
	 * @param prePaiement un pre-paiement
	 */
	public void setToPrePaiementRelationship(IPrePaiement prePaiement) {
		super.setToPrePaiementRelationship((EOPrePaiement) prePaiement);
	}

	/**
	 * {@inheritDoc}
	 */
	public IPaiement toPaiement() {
		return toPrePaiement();
	}

	/**
	 * @param parametragePaieArticleComplementaire
	 */
	public void setToParametragePaieArticleComplementaireRelationship(IParametragePaieArticleComplementaire parametragePaieArticleComplementaire) {
		super.setToParametragePaieArticleComplementaireRelationship((EOParametragePaieArticleComplementaire) parametragePaieArticleComplementaire);
	}

	/**
	 * @param parametragePaieFormation
	 */
	public void setToParametragePaieFormationRelationship(IParametragePaieFormation parametragePaieFormation) {
		super.setToParametragePaieFormationRelationship((EOParametragePaieFormation) parametragePaieFormation);
	}

	/**
	 * @param parametragePaieDiplome
	 */
	public void setToParametragePaieDiplomeRelationship(IParametragePaieDiplome parametragePaieDiplome) {
		super.setToParametragePaieDiplomeRelationship((EOParametragePaieDiplome) parametragePaieDiplome);
	}

	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de paiement
	 */
	@Override
	public void setDatePaye(NSTimestamp date) {
		if (date == null) {
			super.setDatePaye(null);
		} else {
			super.setDatePaye(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}

	/**
	 * On ne veut que la date (sans les heure, minutes, secondes, ...).
	 * @param date la date de remboursement
	 */
	@Override
	public void setDateRembourse(NSTimestamp date) {
		if (date == null) {
			super.setDateRembourse(null);
		} else {
			super.setDateRembourse(new NSTimestamp(DateCtrl.getDateOnly(date)));
		}
	}

	/**
	 * @return the libelle
	 */
	public String getLibelle() {
		String libelle = "";

		if (this.toParametragePaieFormation() != null) {
			libelle = this.toParametragePaieFormation().libelleComplet();
		} else if (this.toParametragePaieDiplome() != null) {
			libelle = this.toParametragePaieDiplome().libelleComplet();
		} else if (this.toParametragePaieArticleComplementaire() != null) {
			libelle = this.toParametragePaieArticleComplementaire().libelleComplet();
		}

		return libelle;
	}

	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}

	/**
	 * {@inheritDoc}
	 */
	public IParametragePaie toParametragePaie() {
		if (toParametragePaieFormation() != null) {
			return toParametragePaieFormation();
		} else if (toParametragePaieDiplome() != null) {
			return toParametragePaieDiplome();
		} else if (toParametragePaieArticleComplementaire() != null) {
			return toParametragePaieArticleComplementaire();
		} else {
			return null;
		}
	}
}
