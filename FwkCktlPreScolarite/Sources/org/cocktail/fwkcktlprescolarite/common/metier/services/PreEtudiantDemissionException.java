package org.cocktail.fwkcktlprescolarite.common.metier.services;

/**
 * Exception pouvant être levée par la classe de service {@link EOPreEtudiantDemissionService}.
 * 
 * @author Pascal MACOUIN
 */
public class PreEtudiantDemissionException extends Exception {

	private static final long serialVersionUID = 5687945736244742878L;

	/**
	 * Construit une exception avec ce message en detail.
	 * @param message un message
	 */
	public PreEtudiantDemissionException(String message) {
		super(message);
	}
}
