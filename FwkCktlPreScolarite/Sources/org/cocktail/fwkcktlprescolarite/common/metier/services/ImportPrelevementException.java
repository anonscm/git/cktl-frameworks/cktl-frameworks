package org.cocktail.fwkcktlprescolarite.common.metier.services;

/**
 * Exception levée lors de problème d'import des prélèvements dans la GFC. 
 * 
 * @author Alexis Tual
 *
 */
public class ImportPrelevementException extends Exception {
    
    private static final long serialVersionUID = 1L;

    /**
     * @param message le message d'erreur
     */
    public ImportPrelevementException(String message) {
        super(message);
    }
    
    /**
     * @param message le message
     * @param cause la cause
     */
    public ImportPrelevementException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
