package org.cocktail.fwkcktlprescolarite.common.metier.services;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;

public class TitulaireInfos {

	private IAdresse adresse;
	private IRib rib;
	private IFournis fournisseur;
	
	public TitulaireInfos(IAdresse adresse, IRib rib, IFournis fournisseur) {
		this.adresse = adresse;
		this.rib = rib;
		this.fournisseur = fournisseur;
	}
	
	public IAdresse getAdresse() {
		return adresse;
	}
	
	public IRib getRib() {
		return rib;
	}
	
	public IFournis getFournisseur() {
		return fournisseur;
	}
	
}
