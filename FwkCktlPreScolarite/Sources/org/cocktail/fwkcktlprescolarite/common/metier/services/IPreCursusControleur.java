package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.ICursusControleur;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.IScoElementListeCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CktlFiltreUtils;

import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXDisplayGroup;
import er.extensions.eof.ERXQ;

/**
 * 
 * @author isabelle
 *
 */
public class IPreCursusControleur implements ICursusControleur {
	
	private ERXDisplayGroup<IScoElementListeCursus> displayGroupCursus = null;
	private EOArrayDataSource dataSourceCursus = null;
	private EOEditingContext editingContext;
	private Integer annee;
	private IPreEtudiant etudiant;
	private IRne rne;
	
	/**
	 * 
	 * @param editingContext l'editing contexte
	 * @param annee l'annee universitaire
	 * @param etudiant l'étudiant
	 * @param rne l'établissement
	 */
	public IPreCursusControleur(EOEditingContext editingContext, Integer annee, IPreEtudiant etudiant, IRne rne) {
		this.editingContext = editingContext;
		this.annee = annee;
		this.etudiant = etudiant;
		this.rne = rne;
	}
	
	/**
	 * @return liste des cursus elements
	 */
	public NSArray<IScoElementListeCursus> getListeCursus()  {
		List<IScoElementListeCursus> listeElement = new ArrayList<IScoElementListeCursus>();
		List<IPreCursus> listeCursus = (List<IPreCursus>) getEtudiant().toPreCursus();
		for(IPreCursus cursus : listeCursus) {
			IScoElementListeCursus element = new IScoElementListeCursus();
			element.setCursus(cursus);
			element.setAnneeDebut(cursus.anneeDebut());
			element.setAnneeFin(cursus.anneeFin());
			element.setDiplomeObtenu(cursus.diplome());
			element.setGardeUniversitraire(cursus.toGradeUniversitaire());
			element.setLibelleFormation(cursus.formation());
			element.setNiveau(cursus.niveau());
			element.setRne(cursus.toCRne());
			element.setTypeFormation(cursus.toTypeFormation());
			element.setTypeInscription(cursus.typeInscription());
			element.setInterruptionEtude(cursus.interruptionEtud());
			listeElement.add(element);
		}
		
		EOQualifier qualifierInscription = ERXQ.and(EOInscription.TO_ETUDIANT.eq((EOEtudiant) getEtudiant().toEtudiant()), EOInscription.ANNEE.lessThan(getAnnee()));
		NSArray<EOInscription> listeInscriptions = EOInscription.fetchSco_Inscriptions(getEditingContext(), qualifierInscription, null);
		for (IScoInscription inscription : listeInscriptions) {
			IScoElementListeCursus element = new IScoElementListeCursus();
			element.setAnneeDebut(inscription.annee());
			element.setGardeUniversitraire(inscription.toGradeUniversitaire());
			element.setLibelleFormation(inscription.toDiplome().libelle());
			element.setNiveau(inscription.niveau());
			element.setRne(getRne());
			element.setTypeFormation(inscription.toDiplome().typeFormation());
			element.setInterruptionEtude(false);
			element.setTypeInscription(ITypeInscription.CODE_LOCAL);
			listeElement.add(element);
		}
		if (getEtudiant().toBac() != null) {
			IScoElementListeCursus element = new IScoElementListeCursus();
			element.setAnneeDebut(getEtudiant().etudAnbac().intValue());
			element.setGardeUniversitraire(EOGradeUniversitaire.gradeBaccalaureat(getEditingContext()));
			element.setLibelleFormation(getEtudiant().toBac().bacLibelle());
			element.setRne(getEtudiant().toRneEtabBac());
			element.setInterruptionEtude(false);
			listeElement.add(element);			
		}
		
		return new NSArray<IScoElementListeCursus>(listeElement);
	}

	/**
	 * @return the dataSourceCursus
	 */
	public EOArrayDataSource getDataSourceCursus() {
		if (dataSourceCursus == null) {
			dataSourceCursus = new EOArrayDataSource(null, getEditingContext());
		}
		return dataSourceCursus;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public ERXDisplayGroup<IScoElementListeCursus> getDisplayGroupeElementsCursus() {
		
		displayGroupCursus = new ERXDisplayGroup<IScoElementListeCursus>();
		getDataSourceCursus().setArray(getListeCursus());
		displayGroupCursus.setDataSource(getDataSourceCursus());

		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
		sortOrderings.add(EOSortOrdering.sortOrderingWithKey(IScoElementListeCursus.ANNEE_DEBUT_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
		displayGroupCursus.setSortOrderings(sortOrderings);

		displayGroupCursus.setSelectsFirstObjectAfterFetch(true);
		displayGroupCursus.fetch();
		return displayGroupCursus;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public EOQualifier getDisplayGroupElementsCursusQualifier(String tokenAnneDebut, String tokenAnneeFin,
			String tokenEtablissement, String tokenTypeFormation, String tokenGrade, String tokenNiveau,
			String tokenIntituleFormation, String tokenDiplome,	String tokenTypeInsc) {
		CktlFiltreUtils filtreUtils = new CktlFiltreUtils();
		EOQualifier qualifier =
			    ERXQ.and( filtreUtils.getLikeQualifier(IScoElementListeCursus.ANNEE_DEBUT_KEY, tokenAnneDebut), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.ANNEE_FIN_KEY, tokenAnneeFin), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.RNE.dot(EORne.LC_RNE).key(), tokenEtablissement), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.TYPE_FORMATION.dot(EOTypeFormation.LIBELLE).key(), tokenTypeFormation), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.GRADE_UNIVERSITAIRE.dot(EOGradeUniversitaire.LIBELLE).key(), tokenGrade), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.NIVEAU_KEY, tokenNiveau), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.LIBELLE_FORMATION_KEY, tokenIntituleFormation), 
			    		filtreUtils.getLikeQualifier(IScoElementListeCursus.DIPLOME_OBTENU_KEY, tokenDiplome)
			);

		return qualifier;
	}
	
	/**
	 * @return the editingContext
	 */
	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	/**
	 * @param editingContext the editingContext to set
	 */
	public void setEditingContext(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @return the Annee
	 */
	public Integer getAnnee() {
		return annee;
	}

	/**
	 * @param annee the Annee to set
	 */
	public void setAnnee(Integer annee) {
		this.annee = annee;
	}

	/**
	 * @return the etudiant
	 */
	public IPreEtudiant getEtudiant() {
		return etudiant;
	}

	/**
	 * @param etudiant the etudiant to set
	 */
	public void setEtudiant(IPreEtudiant etudiant) {
		this.etudiant = etudiant;
	}

	/**
	 * @return the rne
	 */
	public IRne getRne() {
		return rne;
	}

	/**
	 * @param rne the rne to set
	 */
	public void setRne(IRne rne) {
		this.rne = rne;
	}
}
