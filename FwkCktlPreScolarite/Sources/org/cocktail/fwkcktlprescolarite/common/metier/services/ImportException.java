package org.cocktail.fwkcktlprescolarite.common.metier.services;

/**
 * 
 * Exception à lever lorsqu'une incohérence de donnée ne permet pas
 * d'importer tout ou partie de l'étudiant dans le SI.
 * 
 * @author Alexis Tual
 *
 */
public class ImportException extends Exception {

    private static final long serialVersionUID = 1L;

    /**
     * @param message le message d'erreur
     */
    public ImportException(String message) {
        super(message);
    }
    
    /**
     * @param message le message
     * @param cause la cause
     */
    public ImportException(String message, Throwable cause) {
        super(message, cause);
    }
    
}
