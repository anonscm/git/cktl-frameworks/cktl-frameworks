package org.cocktail.fwkcktlprescolarite.common.metier.services;

/**
 * Exception pouvant être levée par la classe de service {@link EOPreEtudiantPaiementService}.
 * 
 * @author Pascal MACOUIN
 */
public class PreEtudiantPaiementException extends Exception {

	private static final long serialVersionUID = -9035091188162373070L;

	/**
	 * Construit une exception avec ce message en detail.
	 * @param message un message
	 */
	public PreEtudiantPaiementException(String message) {
		super(message);
	}
}
