
package org.cocktail.fwkcktlprescolarite.common.metier.services.aglae3.ws;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the fr.edu.paris.aglae3.ws package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: fr.edu.paris.aglae3.ws
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetStatutBoursier }
     * 
     */
    public GetStatutBoursier createGetStatutBoursier() {
        return new GetStatutBoursier();
    }

    /**
     * Create an instance of {@link Authentifier }
     * 
     */
    public Authentifier createAuthentifier() {
        return new Authentifier();
    }

    /**
     * Create an instance of {@link InscriptionWSResponse }
     * 
     */
    public InscriptionWSResponse createInscriptionWSResponse() {
        return new InscriptionWSResponse();
    }

    /**
     * Create an instance of {@link GetStatutBoursierResponse }
     * 
     */
    public GetStatutBoursierResponse createGetStatutBoursierResponse() {
        return new GetStatutBoursierResponse();
    }

    /**
     * Create an instance of {@link InscriptionWS }
     * 
     */
    public InscriptionWS createInscriptionWS() {
        return new InscriptionWS();
    }

    /**
     * Create an instance of {@link AuthentifierResponse }
     * 
     */
    public AuthentifierResponse createAuthentifierResponse() {
        return new AuthentifierResponse();
    }

}
