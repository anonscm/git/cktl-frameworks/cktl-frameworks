package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * Classe de service permettant de filtrer et de contrôler les formations possibles pour un étudiant.
 * 
 * @author Pascal MACOUIN
 */
public class FiltrageFormationsPossiblesEtudiantService {

	private IPreEtudiant preEtudiant;
	private IEtudiant etudiant;

	/**
	 * Construit un service pour cet étudiant.
	 * @param preEtudiant un preEtudiant
	 */
	public FiltrageFormationsPossiblesEtudiantService(IPreEtudiant preEtudiant) {
		this.preEtudiant = preEtudiant;
	}

	/**
	 * Construit un service pour cet étudiant.
	 * @param etudiant un etudiant
	 */
	public FiltrageFormationsPossiblesEtudiantService(IEtudiant etudiant) {
		this.etudiant = etudiant;
	}

	public IPreEtudiant getPreEtudiant() {
		return preEtudiant;
	}

	public void setPreEtudiant(IPreEtudiant preEtudiant) {
		this.preEtudiant = preEtudiant;
	}

	/**
	 * @return the etudiant
	 */
	public IEtudiant getEtudiant() {
		return etudiant;
	}

	/**
	 * @param etudiant the etudiant to set
	 */
	public void setEtudiant(IEtudiant etudiant) {
		this.etudiant = etudiant;
	}

	/**
	 * @return la liste des codes des types de formations interdites pour un étudiant post-bac
	 */
	private NSArray<String> getListeTypeGradeUniversitairesInterditsPourPostBac() {
		return new NSArray<String>("M", "D");
	}

	/**
	 * @param editingContext contexte d'edition
	 * @return liste des grades universitaires non ouverts à l'inscription selon le contexte de l'étudiant
	 */
	public List<String> getListeTypeGradeInterdits(EOEditingContext editingContext) {
		if (isEtudiantPostBac(editingContext)) {
			List<String> gradesInterdits = new ArrayList<String>();
			gradesInterdits.add(IGradeUniversitaire.GRADE_MASTER);
			gradesInterdits.add(IGradeUniversitaire.GRADE_DOCTORAT);
			return gradesInterdits;
		}

		return null;
	}

	/**
	 * @param editingContext contexte d'édition
	 * @param inscription une inscription
	 * @return la liste des niveaux autorisée pour cette inscription
	 */
	public List<Long> getNiveauxFormation(IInscription inscription, EOEditingContext editingContext) {
		if (isEtudiantPostBac(editingContext)) {
			List<Long> listeNiveaux = new ArrayList<Long>();
			listeNiveaux.add(1L);
			return listeNiveaux;
		}
		return null;
	}

	/**
	 * @param editingContext contexte d'édition
	 * @param inscription une inscription
	 * @return <code>true</code> si le diplôme de cette preInscription a un grade universitaire autorisé
	 */
	public boolean isGradeUniversitaireAutorise(IInscription inscription, EOEditingContext editingContext) {
		IGradeUniversitaire gradeUniversitaire = inscription.toDiplome().gradeUniversitaire();

		if (gradeUniversitaire != null && isEtudiantPostBac(editingContext)
		    && getListeTypeGradeUniversitairesInterditsPourPostBac().contains(gradeUniversitaire.type())) {
			return false;
		}

		return true;
	}

	/**
	 * @param editingContext contexte d'édition
	 * @param inscription une inscription
	 * @return <code>true</code> si le niveau choisi est autorisé
	 */
	public boolean isNiveauFormationAutorise(IInscription inscription, EOEditingContext editingContext) {
		List<Long> niveauxFormation = getNiveauxFormation(inscription, editingContext);
		if (inscription.niveau() != null && niveauxFormation != null) {
			return niveauxFormation.contains(new Long(inscription.niveau()));
		}
		return true;
	}

	private boolean isEtudiantPostBac(EOEditingContext editingContext) {
		boolean isOk = false;
		if (getPreEtudiant() != null) {
			return getPreEtudiant().toPreCursus().size() == 0;
		} else if (getEtudiant() != null) {
			if (EOCursus.fetchSco_Cursuses(editingContext, EOCursus.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()), null) != null
			    && EOCursus.fetchSco_Cursuses(editingContext, EOCursus.TO_ETUDIANT.eq((EOEtudiant) getEtudiant()), null).size() > 0) {
				isOk = false;
			} else {
				isOk = true;
			}
		}

		return isOk;
	}
}
