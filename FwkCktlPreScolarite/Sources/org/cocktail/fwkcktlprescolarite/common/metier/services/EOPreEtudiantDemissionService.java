package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.util.Date;

import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.ICompte;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoEtudiantAnnee;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Service de démission d'un étudiant.
 * 
 * @author Pascal MACOUIN
 */
public class EOPreEtudiantDemissionService {

	private EOEditingContext editingContext;

	/**
	 * @param editingContext Un editing context
	 */
	public EOPreEtudiantDemissionService(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * Démissionner un étudiant.
	 * 
	 * @param etudiantAnnee L'étudiant année à démissioner
	 * @throws PreEtudiantDemissionException Si la démission n'est pas possible
	 */
	public void demissionner(IScoEtudiantAnnee etudiantAnnee) throws PreEtudiantDemissionException {
		// On invalide les IP
		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
		for (IInscription inscription : etudiantAnnee.toInscriptions()) {
			boolean isOk = etudiantService.suppressionIpsEtIpsElmtsPourInscription(editingContext, inscription);
			
			if (!isOk) {
				throw new PreEtudiantDemissionException(String.format("Impossible de supprimer les IP et IP éléments de l'inscription %s. Vérifiez qu'il n'y ait pas de notes saisies sur ces IP.", inscription.toDiplome().libelle()));
			}
		}
		
		// On invalide l'étudiant
		etudiantAnnee.setValide(false);
		
		// On invalide son compte du VLanE
		ICompte compte = rechercherCompteEtudiant(etudiantAnnee);
		if (compte != null) {
			compte.setCptValide(EOCompte.CPT_VALIDE_NON);
			compte.setCptFinValide(new Date());
		}
	}
	
	/**
	 * Annuler la démission d'un étudiant.
	 * 
	 * @param etudiantAnnee L'étudiant année à revalider
	 */
	public void annulerDemission(IScoEtudiantAnnee etudiantAnnee, Integer persId) {
		// On crée les IP
		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
		EOTypeEtudiant typeEtudiant = EOTypeEtudiant.typeClassique(editingContext);
		for (IInscription inscription : etudiantAnnee.toInscriptions()) {
			etudiantService.creerIpPourIa(editingContext, inscription, persId, typeEtudiant);
		}
		
		// On valide l'étudiant
		etudiantAnnee.setValide(true);
		
		// On valide son compte du VLanE
		ICompte compte = rechercherCompteEtudiant(etudiantAnnee);
		if (compte != null) {
			compte.setCptValide(EOCompte.CPT_VALIDE_OUI);
			compte.setCptFinValide(null);
		}
	}
	
	/**
	 * Rechercher le compte sur le VLan étudiant.
	 * @param etudiantAnnee Un étudiant année
	 * @return Le compte du VLan étudiant (ou <code>null</code> si pas de compte trouvé)
	 */
	private ICompte rechercherCompteEtudiant(IScoEtudiantAnnee etudiantAnnee) {
		String vlanEtudiant = EOGrhumParametres.parametrePourCle(editingContext, EOGrhumParametres.PARAM_GRHUM_VLAN_ETUD_KEY, EOVlans.VLAN_E);
		ICompte compteVlanEtudiant = null;
		
		for (ICompte compte : etudiantAnnee.toEtudiant().toIndividu().toComptes()) {
			if (compte.toVlans().cVlan().equals(vlanEtudiant)) {
				compteVlanEtudiant = compte;
				break;
			}
		}
		
		return compteVlanEtudiant;
	}
}
