package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

/**
 * Service permettant de manipuler les différentes années de premières inscriptions.
 * 
 * @author Pascal MACOUIN
 */
public class CalculAnnee1InscService {
	
	/**
	 * Calculer les différentes années de première inscription à partir des cursus de l'étudiant.
	 * 
	 * @param etudiant un pré-étudiant
	 * @param cursusAjoute le cursus qui sera ajouté, permet de gérer le cas où le cursus n'est pas encore lié à l'étudiant
	 * @param codeRneEtablissement Le code RNE de l'établissement
	 */
	public void calculerAnneesInscription(IInfosEtudiant etudiant, ICursus cursusAjoute, IRne rneEtablissement, int anneeUniversitaire) {
		// ATTENTION, les calculs sur un IEtudiant devront sans doute être différents
		Integer annee1InscEtab = null;
		Integer annee1InscUniv = null;
		Integer annee1InscSup = null;
		IRne rneEtabSup = null;
		String codeRneEtablissement = rneEtablissement.cRne();
		
		// On recherche la plus petite année de chaque catégorie
		List<ICursus> lesCursus = new ArrayList<ICursus>(etudiant.toCursus());
		if (cursusAjoute != null && !lesCursus.contains(cursusAjoute)) {
			lesCursus.add(cursusAjoute);
		}
		for (ICursus unCursus : lesCursus) {
			if (unCursus.toCRne() != null && codeRneEtablissement.equals(unCursus.toCRne().cRne())) {
				annee1InscEtab = annee1InscEtab == null ? unCursus.anneeDebut() : Math.min(annee1InscEtab, unCursus.anneeDebut());
			}
			
			if (unCursus.toCRne() != null && unCursus.toCRne().isUneUniversite()) {
				annee1InscUniv = annee1InscUniv == null ? unCursus.anneeDebut() : Math.min(annee1InscUniv, unCursus.anneeDebut());
			}
			
			if (annee1InscSup == null || unCursus.anneeDebut() < annee1InscSup) {
				if (!unCursus.toGradeUniversitaire().isBac()) {
					annee1InscSup = unCursus.anneeDebut();
					rneEtabSup = unCursus.toCRne();
				}
			}
		}
		Long anneeUniversitaireLong = integerToLong(anneeUniversitaire);
		if (annee1InscEtab != null) {
			etudiant.setEtudAnnee1InscEtab(integerToLong(annee1InscEtab));
		} else if (etudiant.etudAnnee1inscEtab() == null) {
			etudiant.setEtudAnnee1InscEtab(anneeUniversitaireLong);
		}
		if (annee1InscUniv != null) {
			etudiant.setEtudAnnee1InscUniv(integerToLong(annee1InscUniv));
		} else if (etudiant.etudAnnee1inscUniv() == null && rneEtablissement.isUneUniversite()) {
			etudiant.setEtudAnnee1InscUniv(anneeUniversitaireLong);
		}
		if (annee1InscSup != null) {
			etudiant.setEtudAnnee1InscSup(integerToLong(annee1InscSup));
			etudiant.setToRneEtabSupRelationship(rneEtabSup);
		} else if (etudiant.etudAnnee1inscSup() == null) {
			etudiant.setEtudAnnee1InscSup(anneeUniversitaireLong);
			etudiant.setToRneEtabSupRelationship(rneEtablissement);
		}
	}

	/**
	 * Conversion sans NPE.
	 * @param entier un entier
	 * @return un long
	 */
	private Long integerToLong(Integer entier) {
		return entier == null ? null : entier.longValue();
	}
}

