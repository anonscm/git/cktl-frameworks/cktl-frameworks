package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolarite;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.services.aglae3.inscriptionws.InscriptionWS;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormationAglae;

/**
 * client au service AGLAE
 */
public class AglaeClientService {
    private String qNameURL;
	private String serviceURL;
	private String codeRneQuestionneur;
	private String codeRneEtablissement;
	
	private static final String QNAME_NAME = "InscriptionWSService";
	private static final String CURSUS_LMD = "C";
	private static final String CURSUS_HORS_LMD = "H";
	private static final String PROGRESSION_LICENCE = "L";
	private static final String PROGRESSION_MASTER = "M";
	private static final String PROGRESSION_DOCTORAT = "D";
	private static final String PROGRESSION_SANS_OBJET = "H";
	private InscriptionWS inscriptionWS;

	public static final String STATUT_BOURSIER_ATTENTE = "A";
	public static final String STATUT_BOURSIER_NON = "N";
	public static final String STATUT_BOURSIER_ERREUR_CONNECTION = "KO";
	public static final String STATUT_BOURSIER_ERREUR_PARAMETRE = "RF";

	public AglaeClientService(
	        String qNameURL, String codeRneQuestionneur, String codeRneEtablissement) {
	    this.qNameURL = qNameURL;
	    this.serviceURL = qNameURL + "?wsdl";
	    this.codeRneQuestionneur = codeRneQuestionneur;
	    this.codeRneEtablissement = codeRneEtablissement;
    }
	
	/**
	 * @return le web service Aglae
	 * @throws MalformedURLException exception en cas de URL mal incorrecte
	 */
	InscriptionWS getInscriptionWS() throws Exception {
		// throw new Exception("my exception");
		if (inscriptionWS == null) {
			URL url = new URL(serviceURL);
			QName qname = new QName(qNameURL, QNAME_NAME);
			Service service = Service.create(url, qname);
			inscriptionWS = service.getPort(InscriptionWS.class);
			Map requestContext = ((BindingProvider) inscriptionWS).getRequestContext();
			requestContext.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, Boolean.TRUE);
		}
		return inscriptionWS;
	}

	/**
	 * @param referenceUAIRNE identifiant de l’entité qui interroge AGLAE
	 * @param motDePasse mot de passe
	 * @return vrai si l'authentification a réussi, faux sinon
	 * @throws Exception exception en cas d'URL mal formée
	 */
	public Boolean authentifier(String referenceUAIRNE, String motDePasse) throws Exception {
		return getInscriptionWS().authentifier(referenceUAIRNE, motDePasse);
	}

	/**
	 * @param preEtudiant l'étudiant en cours d'inscription
	 * @param test si vrai appel du webservice fourni pour tests
	 * @return le statut boursier de l'étudiant
	 * @throws MalformedURLException exception en cas d'URL mal formée
	 */
	String getStatutBoursier(IInfosEtudiant preEtudiant, Boolean test) throws Exception {
		IInscription inscriptionPrincipale = preEtudiant.getInscriptionPrincipale();
		if (inscriptionPrincipale == null) {
			return STATUT_BOURSIER_ERREUR_PARAMETRE;
		}
		IDiplome diplome = inscriptionPrincipale.toDiplome();
		if (diplome == null) {
			return STATUT_BOURSIER_ERREUR_PARAMETRE;
		}

		String indicateurTest = "";
		if (test) {
			indicateurTest = "test";
		}

		String INE = preEtudiant.codeIne();
		String codeRNEQuestionneur = getCodeRNEQuestionneur();
		String codeRNEEtablissement = getCodeRNEEtablissement();
		String typeCursus = getTypeCursus(diplome);
		String formation = getCodeFormation(diplome);
		String progression = getProgression(diplome);
		String niveau = inscriptionPrincipale.niveau().toString();
		String anneeGestion = getAnneeUniversitaire();
		
		String statut = getInscriptionWS().getStatutBoursier(INE, codeRNEQuestionneur, codeRNEEtablissement, typeCursus, formation, progression, niveau,
		    anneeGestion, indicateurTest);
		return statut;
	}

	/**
	 * @param preEtudiant l'étudiant en cours d'inscription
	 * @return le statut boursier de l'étudiant
	 * @throws Exception exception en cas d'URL mal formée
	 */
	public String getStatutBoursier(IInfosEtudiant preEtudiant) throws Exception {
		return getStatutBoursier(preEtudiant, false);
	}

	private String getAnneeUniversitaire() {
		return FwkCktlPreScolarite.getParamManager().getPreScolariteAnneeUniversitaire();
	}

	String getCodeRNEQuestionneur() {
		return codeRneQuestionneur;
	}

	String getCodeRNEEtablissement() {
		return codeRneEtablissement;
	}

	String getCodeFormation(IDiplome diplome) {
	    ITypeFormation typeFormation = diplome.typeFormation();
	    String codeTypeFormationAglae = null;
	    if (typeFormation != null) {
	        ITypeFormationAglae typeFormationAglae = typeFormation.typeFormationAglae();
	        if (typeFormationAglae != null) {
	            codeTypeFormationAglae = typeFormationAglae.code();
	        }
	    }
		return codeTypeFormationAglae;
	}

	private String getProgression(IDiplome diplome) {
		if (!diplome.isHabilitationRequise()) {
			return PROGRESSION_SANS_OBJET;
		}
		IGradeUniversitaire grade = diplome.gradeUniversitaire();
		if (grade.type().equals(EOGradeUniversitaire.GRADE_LICENCE)) {
			return PROGRESSION_LICENCE;
		} else if (grade.type().equals(EOGradeUniversitaire.GRADE_MASTER)) {
			return PROGRESSION_MASTER;
		} else if (grade.type().equals(EOGradeUniversitaire.GRADE_DOCTORAT)) {
			return PROGRESSION_DOCTORAT;
		} else {
			return PROGRESSION_SANS_OBJET;
		}
	}

	private String getTypeCursus(IDiplome diplome) {
		if (diplome.isHabilitationRequise()) {
			return CURSUS_LMD;
		} else {
			return CURSUS_HORS_LMD;
		}
	}

}
