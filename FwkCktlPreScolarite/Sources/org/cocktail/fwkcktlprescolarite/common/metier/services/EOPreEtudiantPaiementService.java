package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.sepasdd.helpers.SepaSddEcheancierHelper;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiementMoyen;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs.CotisationRegimeSecuControle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaie;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBoursesCF;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier._EOPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.compta.services.PaiementService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ExplorationMaquetteService;
import org.joda.time.LocalDate;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXQ;

/**
 * Classe de service permettant de manipuler les EOPrePaiement.
 */
public class EOPreEtudiantPaiementService {

	private static final Logger LOG = Logger.getLogger(EOPreEtudiantPaiementService.class);
	private static final String PREFIX_PRE_MESSAGE_LOG = "PrePaiement id %s, PrePaiementMoyen id %s - %s";
	private static final String PREFIX_SCO_MESSAGE_LOG = "Paiement id %s, PaiementMoyen id %s - %s";
	public static final String LIBELLE_ECHEANCE = "Échéance %s/%s";
	public static final String IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_COMPTANT = "Impossible de créer un échéancier pour un paiement comptant";
	public static final String IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_ZERO = "Impossible de créer un échéancier pour un paiement d'un montant de zéro";
	public static final String IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_INFERIEUR_ECHEANCES = "Impossible de créer un échéancier pour un paiement d'un montant (%s) inférieur au nombre d'échéances (%s)";
	public static final String IMPOSSIBLE_SUPPRIMER_ECHEANCIER_PAIEMENT_VALIDE = "Impossible de supprimer un échéancier d'un paiement validé";

	@Inject
	private PaiementService paiementService;
	
	/**
	 * Construit le service.
	 */
	public EOPreEtudiantPaiementService() {
	}

	/**
	 * Créer un paiement (avec les articles par défaut associés pour le paiement initial).
	 * @param editingContext un editing context
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return le paiement créé
	 */
	public IPaiement creerPaiement(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee) {
		IPaiement paiement = creerEntitePaiement(editingContext, etudiantAnnee);
		
		if (paiement.isInitial()) {
			// Pour le paiement initial, on recherche les articles par défaut
			List<IParametragePaie> listeArticlesPaiement = rechercherArticlesPaiementParDefaut(editingContext, etudiantAnnee);
			ajouterDetailsPaiementAuto(editingContext, paiement, listeArticlesPaiement);
		}
		
		return paiement;
	}

	/**
	 * Créer un paiement de remboursement avec pré-sélection des articles remboursables (cas de la démission) pour un étudiant.
	 * @param editingContext un editing context
	 * @param etudiantAnnee un étudiant année
	 * @return le paiement créé
	 */
	public IPaiement creerPaiementRemboursementAuto(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee) {
		List<IPaiementDetail> listeDetailPaiement = paiementService.rechercherDetailsPaiementRemboursables(etudiantAnnee);
		
		IPaiement paiement = creerEntitePaiement(editingContext, etudiantAnnee);
		ajouterDetailsRemboursement(editingContext, paiement, listeDetailPaiement);
		
		return paiement;
	}
	
	/**
	 * Créer un paiement avec les articles associés.
	 * @param editingContext un editing context
	 * @param etudiantAnnee un etudiantAnnee
	 * @param listeArticlesPaiement une liste d'article de paiement
	 * @return le paiement créé
	 */
	private IPaiement creerEntitePaiement(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee) {
		IPaiement paiement;
		
		if (etudiantAnnee instanceof IPreEtudiantAnnee) {
			paiement = EOPrePaiement.creerInstance(editingContext);
		} else {
			paiement = (IPaiement) EOUtilities.createAndInsertInstance(editingContext, EOPaiement.ENTITY_NAME);
		}
		
		paiement.setToEtudiantAnneeRelationship(etudiantAnnee);
		paiement.setOrdre(getPaiementOrdreSuivant(etudiantAnnee));
		paiement.setMontant(BigDecimal.ZERO);
		paiement.setPaiementValide(false);

		return paiement;
	}
	
	/**
	 * @param etudiantAnnee un étudiant année
	 * @return Le numéro d'ordre suivant à utiliser pour le paiement
	 */
	private int getPaiementOrdreSuivant(IEtudiantAnnee etudiantAnnee) {
		int numeroOrdre = 0;
		
		for (IPaiement paiement : etudiantAnnee.toPaiements()) {
			if (paiement.ordre() != null) {
				numeroOrdre = Math.max(numeroOrdre, paiement.ordre().intValue());
			}
		}
		
		return numeroOrdre + 1;
	}
	
	private void ajouterDetailsPaiementAuto(EOEditingContext editingContext, IPaiement paiement, List<IParametragePaie> listeArticlesPaiement) {
		for (IParametragePaie articlePaiement : listeArticlesPaiement) {
			ajouterDetailPaiement(editingContext, paiement, articlePaiement, true);
		}
	}

	/**
	 * Ajouter un article de paiement manuel au paiement.
	 * @param editingContext un editingContext
	 * @param paiement un paiement
	 * @param parametragePaie l'article à ajouter
	 * @return le detail paiement nouvellement créé
	 */
	public IPaiementDetail ajouterDetailPaiementManuel(EOEditingContext editingContext, IPaiement paiement, IParametragePaie parametragePaie) {
		return ajouterDetailPaiement(editingContext, paiement, parametragePaie, false);
	}
	
	/**
	 * Ajouter un article de paiement au paiement.
	 * @param editingContext un editingContext
	 * @param paiement un paiement
	 * @param parametragePaie l'article à ajouter
	 * @param auto true si l'article a été ajouté automatique, false sinon (ajout manuel)
	 * @return le detail paiement nouvellement créé
	 */
	private IPaiementDetail ajouterDetailPaiement(EOEditingContext editingContext, IPaiement paiement, IParametragePaie parametragePaie, boolean auto) {
		IPaiementDetail detailPaiement = creerEntiteDetailPaiement(editingContext, paiement);

		if (parametragePaie instanceof IParametragePaieFormation) {
			detailPaiement.setToParametragePaieFormationRelationship((IParametragePaieFormation) parametragePaie);
		} else if (parametragePaie instanceof IParametragePaieDiplome) {
			detailPaiement.setToParametragePaieDiplomeRelationship((IParametragePaieDiplome) parametragePaie);
		} else if (parametragePaie instanceof IParametragePaieArticleComplementaire) {
			detailPaiement.setToParametragePaieArticleComplementaireRelationship((IParametragePaieArticleComplementaire) parametragePaie);
		} else {
			String message = String.format("Ce type d'article de paiement (%s) n'est pas implémenté", parametragePaie.getClass());
			LOG.error(message);
			throw new IllegalArgumentException(message);
		}

		BigDecimal montantAPayer = getMontantAPayer(paiement.toEtudiantAnnee(), parametragePaie);
		paiement.setMontant(paiement.montant().add(montantAPayer));
		detailPaiement.setMontantAPayer(montantAPayer);
		detailPaiement.setAuto(auto);

		return detailPaiement;
	}

	/**
	 * AJouter un détail paiement au paiement en clonant celui passé en paramètre.
	 * @param editingContext Un editing context
	 * @param paiement Un paiement
	 * @param detailPaiement Un détail paiement
	 */
	public void ajouterDetailPaiement(EOEditingContext editingContext, IPaiement paiement, IPaiementDetail detailPaiement) {
		IPaiementDetail nouveauDetailPaiement = creerEntiteDetailPaiement(editingContext, paiement);
		
		nouveauDetailPaiement.setToParametragePaieFormationRelationship(detailPaiement.toParametragePaieFormation());
		nouveauDetailPaiement.setToParametragePaieDiplomeRelationship(detailPaiement.toParametragePaieDiplome());
		nouveauDetailPaiement.setToParametragePaieArticleComplementaireRelationship(detailPaiement.toParametragePaieArticleComplementaire());
		nouveauDetailPaiement.setAuto(detailPaiement.auto());
		nouveauDetailPaiement.setMontantAPayer(detailPaiement.montantAPayer());
		nouveauDetailPaiement.setMontantPaye(detailPaiement.montantPaye());
		nouveauDetailPaiement.setDatePaye(detailPaiement.datePaye());
		nouveauDetailPaiement.setMontantARembourser(detailPaiement.montantARembourser());
		nouveauDetailPaiement.setMontantRembourse(detailPaiement.montantRembourse());
		nouveauDetailPaiement.setDateRembourse(detailPaiement.dateRembourse());
		
		if (nouveauDetailPaiement.montantAPayer() != null) {
			paiement.setMontant(paiement.montant().add(nouveauDetailPaiement.montantAPayer()));
		}
		
		if (nouveauDetailPaiement.montantARembourser() != null) {
			paiement.setMontant(paiement.montant().subtract(nouveauDetailPaiement.montantARembourser()));
		}
	}
	
	private void ajouterDetailsRemboursement(EOEditingContext editingContext, IPaiement paiement, List<IPaiementDetail> listeDetailPaiement) {
		for (IPaiementDetail detailPaiement : listeDetailPaiement) {
			ajouterDetailRemboursement(editingContext, paiement, detailPaiement, true);
		}
	}
	
	private void ajouterDetailRemboursement(EOEditingContext editingContext, IPaiement paiement, IPaiementDetail detailPaiementReference, boolean auto) {
		IPaiementDetail detailPaiement = creerEntiteDetailPaiement(editingContext, paiement);
		
		detailPaiement.setToParametragePaieDiplomeRelationship(detailPaiementReference.toParametragePaieDiplome());
		detailPaiement.setToParametragePaieFormationRelationship(detailPaiementReference.toParametragePaieFormation());
		detailPaiement.setToParametragePaieArticleComplementaireRelationship(detailPaiementReference.toParametragePaieArticleComplementaire());
		detailPaiement.setMontantARembourser(detailPaiementReference.montantPaye());
		detailPaiement.setAuto(auto);
		
		paiement.setMontant(paiement.montant().subtract(detailPaiementReference.montantPaye()));
	}
	
	private IPaiementDetail creerEntiteDetailPaiement(EOEditingContext editingContext, IPaiement paiement) {
		IPaiementDetail detailPaiement;
		if (paiement instanceof IPrePaiement) {
			detailPaiement = EOPrePaiementDetail.creerInstance(editingContext);
		} else {
			detailPaiement = (IPaiementDetail) EOUtilities.createAndInsertInstance(editingContext, EOPaiementDetail.ENTITY_NAME);
		}
		detailPaiement.setToPaiementRelationship(paiement);
		return detailPaiement;
	}

	/**
	 * Retourne le montant à payer pour l'article de paiement.
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @param parametragePaie un article de paiement
	 * @return le montant à payer
	 */
	public BigDecimal getMontantAPayer(IEtudiantAnnee etudiantAnnee, IParametragePaie parametragePaie) {
		BigDecimal montantAPayer = parametragePaie.montant();

		// Si l'article est exonéré pour les boursiers et que l'étudiant est boursier, le montant à payer est à zéro
		if (parametragePaie.boursier() && etudiantAnnee.boursier()) {
			for (IBourses bourse : etudiantAnnee.toBourses()) {
				if (isMontantZero(parametragePaie, bourse)) {
					montantAPayer = BigDecimal.ZERO;
					break;
				}
			}
		}

		// Les articles de type Sécurité sociale non bourse peuvent aussi être éxonérés
		if (parametragePaie instanceof IParametragePaieArticleComplementaire
		    && ((IParametragePaieArticleComplementaire) parametragePaie).toTypeArticleComplementaire().code().equals(ITypeArticleComplementaire.CODE_SECU)) {
			CotisationRegimeSecuControle cotisationRegimeSecuControle = new CotisationRegimeSecuControle(etudiantAnnee.annee());

			if (cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee)) {
				montantAPayer = BigDecimal.ZERO;
			}
		}

		return montantAPayer;
	}

	/**
	 * Supprimer un détail paiement d'un paiement.
	 * <p>
	 * Met à jour le montant du paiement.
	 * @param editingContext un editing context
	 * @param detailPaiement le détail paiement à supprimer du paiement
	 */
	public void supprimerDetailPaiement(EOEditingContext editingContext, IPaiementDetail detailPaiement) {
		IPaiement paiement = detailPaiement.toPaiement();
		BigDecimal montantASoustraire = BigDecimal.ZERO;
		
		if (detailPaiement.montantAPayer() != null) {
			montantASoustraire = montantASoustraire.add(detailPaiement.montantAPayer());
		}
		if (detailPaiement.montantPaye() != null) {
			montantASoustraire = montantASoustraire.add(detailPaiement.montantPaye());
		}
		
		if (detailPaiement.montantARembourser() != null) {
			montantASoustraire = montantASoustraire.subtract(detailPaiement.montantARembourser());
		}
		if (detailPaiement.montantRembourse() != null) {
			montantASoustraire = montantASoustraire.subtract(detailPaiement.montantRembourse());
		}
		
		paiement.setMontant(paiement.montant().subtract(montantASoustraire));
		detailPaiement.setToPaiementRelationship(null);
		editingContext.deleteObject((EOEnterpriseObject) detailPaiement);
	}
	
	/**
	 * Est-ce que cette bourse implique une éxonération (un montant à zéro) de l'article de paiement ?
	 * @param parametragePaie un article de paiement
	 * @param bourse une bourse
	 * @return <code>true</code> si l'article est exonéré (montant à zéro)
	 */
	private boolean isMontantZero(IParametragePaie parametragePaie, IBourses bourse) {
		boolean isMontantZero = false;

		if (!bourse.isCampusFrance()) {
			isMontantZero = true;
		} else if (bourse.toTypeBoursesCF() != null) {
			String codeTypeBoursesCF = bourse.toTypeBoursesCF().code();

			if ((parametragePaie instanceof IParametragePaieFormation || parametragePaie instanceof IParametragePaieDiplome)
			    && (codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_INSCRIPTION) || codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_INSCRIPTION_SECU))) {
				isMontantZero = true;
			} else if (parametragePaie instanceof IParametragePaieArticleComplementaire
			    && ((IParametragePaieArticleComplementaire) parametragePaie).toTypeArticleComplementaire().code().equals(ITypeArticleComplementaire.CODE_SECU)
			    && (codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_SECU) || codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_INSCRIPTION_SECU))) {
				isMontantZero = true;
			}
		}

		return isMontantZero;
	}

	/**
	 * Rechercher la liste des articles de paiement par défaut pour cet étudiant.
	 * @param editingContext un editing context
	 * @param preEtudiantAnnee un preEtudiant année
	 * @return la liste des articles de paiement par défaut pour cette inscription
	 */
	public List<IParametragePaie> rechercherArticlesPaiementParDefaut(EOEditingContext editingContext, IEtudiantAnnee preEtudiantAnnee) {
		List<IParametragePaie> listeArticlesPaiement = new ArrayList<IParametragePaie>();

		// Articles liés aux inscriptions
		for (IInscription inscription : preEtudiantAnnee.toInscriptions()) {
			listeArticlesPaiement.addAll(rechercherArticlesPaiementParDefaut(editingContext, inscription));
		}

		// Articles complémentaires
		// On ajoute tous les articles complémentaires systématique sauf les articles de type SS si l'étudiant n'est pas affilié
		EOQualifier qualifier = EOParametragePaieArticleComplementaire.APPLICATION_SYSTEMATIQUE.eq(Boolean.TRUE);

		CotisationRegimeSecuControle cotisationRegimeSecuControle = new CotisationRegimeSecuControle(preEtudiantAnnee.annee());
		if (!cotisationRegimeSecuControle.doitEtreAffilie(preEtudiantAnnee)) {
			qualifier = ERXQ.and(qualifier,
			    EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE.dot(EOTypeArticleComplementaire.CODE).ne(ITypeArticleComplementaire.CODE_SECU));
		}

		listeArticlesPaiement.addAll(EOParametragePaieArticleComplementaire.fetchSco_ParametragePaieArticleComplementaires(
		    editingContext,
		    qualifier,
		    EOParametragePaieArticleComplementaire.TO_TYPE_ARTICLE_COMPLEMENTAIRE.dot(EOTypeArticleComplementaire.LIBELLE).asc()
		        .then(EOParametragePaieArticleComplementaire.LIBELLE.asc())));

		return listeArticlesPaiement;
	}

	/**
	 * Rechercher la liste des articles de paiement par défaut pour cette inscription.
	 * @param editingContext un editing context
	 * @param inscription une inscription
	 * @return la liste des articles de paiement par défaut pour cette inscription
	 */
	public List<? extends IParametragePaie> rechercherArticlesPaiementParDefaut(EOEditingContext editingContext, IInscription inscription) {
		List<? extends IParametragePaie> listeArticlesPaiement = Collections.emptyList();

		IDiplome diplome = inscription.toDiplome();
		IParcours parcoursDiplome = inscription.toParcoursDiplome();
		Integer niveau = inscription.niveau();
		List<IParcours> listeParcoursAnnee = Collections.emptyList();
		if (inscription.toParcoursAnnee() != null) {
			ExplorationMaquetteService explorationMaquetteService = new ExplorationMaquetteService(editingContext, true);
			listeParcoursAnnee = explorationMaquetteService.getParcoursPourSpecialiteNiveauEtParcours(diplome, inscription.annee(), parcoursDiplome, niveau,
			    inscription.toParcoursAnnee());
		}

		// Recherche des articles de paiement "diplômes et parcours"
		for (IParcours parcours : listeParcoursAnnee) {
			listeArticlesPaiement = rechercherArticlesPaiementParDefautPourParcoursAnnee(editingContext, diplome, niveau, parcours);

			if (!listeArticlesPaiement.isEmpty()) {
				break;
			}
		}

		if (listeArticlesPaiement.isEmpty()) {
			listeArticlesPaiement = rechercherArticlesPaiementParDefautPourParcoursDiplome(editingContext, diplome, niveau, parcoursDiplome);
		}

		// Si pas d'article "diplômes et parcours" trouvé, on recherche parmis les articles "composantes et formations"
		if (listeArticlesPaiement.isEmpty()) {
			ITypeFormation typeFormation = diplome.typeFormation();
			IGradeUniversitaire grade = inscription.toGradeUniversitaire();

			EOQualifier qualifier = ERXQ.and(
			    EOParametragePaieFormation.TO_TYPE_FORMATION.eq((EOTypeFormation) typeFormation),
			    ERXQ.or(EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE.eq((EOGradeUniversitaire) grade),
			        EOParametragePaieFormation.TO_GRADE_UNIVERSITAIRE.isNull()));

			listeArticlesPaiement = EOParametragePaieFormation.fetchSco_ParametragePaieFormations(editingContext, qualifier, null);
		}

		return listeArticlesPaiement;
	}

	private List<? extends IParametragePaie> rechercherArticlesPaiementParDefautPourParcoursAnnee(EOEditingContext editingContext, IDiplome diplome,
	    Integer niveau, IParcours parcoursAnnee) {
		// On recherche d'abord avec le niveau
		EOQualifier qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.eq(niveau),
		    EOParametragePaieDiplome.TO_PARCOURS.eq((EOParcours) parcoursAnnee));

		List<? extends IParametragePaie> listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);

		// On recherche ensuite sans niveau
		if (listeArticlesPaiement.isEmpty()) {
			qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.isNull(),
			    EOParametragePaieDiplome.TO_PARCOURS.eq((EOParcours) parcoursAnnee));

			listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
		}

		return listeArticlesPaiement;
	}

	private List<? extends IParametragePaie> rechercherArticlesPaiementParDefautPourParcoursDiplome(EOEditingContext editingContext, IDiplome diplome,
	    Integer niveau, IParcours parcoursDiplome) {
		List<? extends IParametragePaie> listeArticlesPaiement = Collections.emptyList();

		// On recherche d'abord avec le niveau et le parcours
		if (parcoursDiplome != null) {
			EOQualifier qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.eq(niveau),
			    EOParametragePaieDiplome.TO_PARCOURS.eq((EOParcours) parcoursDiplome));

			listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
		}

		// On recherche ensuite pour le niveau
		if (listeArticlesPaiement.isEmpty()) {
			EOQualifier qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.eq(niveau),
			    EOParametragePaieDiplome.TO_PARCOURS.isNull());

			listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
		}

		// On recherche enfin pour le parcours
		if (listeArticlesPaiement.isEmpty() && parcoursDiplome != null) {
			EOQualifier qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.isNull(),
			    EOParametragePaieDiplome.TO_PARCOURS.eq((EOParcours) parcoursDiplome));

			listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
		}

		// On recherche finalement pour le diplôme
		if (listeArticlesPaiement.isEmpty()) {
			EOQualifier qualifier = ERXQ.and(EOParametragePaieDiplome.TO_DIPLOME.eq((EODiplome) diplome), EOParametragePaieDiplome.NIVEAU.isNull(),
			    EOParametragePaieDiplome.TO_PARCOURS.isNull());

			listeArticlesPaiement = EOParametragePaieDiplome.fetchSco_ParametragePaieDiplomes(editingContext, qualifier, null);
		}

		return listeArticlesPaiement;
	}

	/**
	 * Créer l'échéancier d'un paiement non encore payé (les montants à payer ne sont pas <code>null</code>).
	 * @param editingContext un editing contexte
	 * @param montantPaiement montant du paiement
	 * @param moyenPaiement un moyen de paiement
	 * @param moyenPaiementPremiereEcheance le moyen de paiement pour la première échéance
	 * @param nombreEcheances le nombre d'échéances à créer
	 * @param numeroJourEcheance pour le calcul de la date d'échéance, le numéro du jour dans le mois où sera prélevée l'échéance
	 * @param nbjoursMoisSuivant pour le calcul de la date de première échéance, le nombre de jour minimum qui doit nous séparer du mois suivant
	 * @throws PreEtudiantPaiementException Si l'échéancier n'a pas pu être créé
	 */
	public void creerEcheancier(EOEditingContext editingContext, BigDecimal montantPaiement, IPaiementMoyen moyenPaiement,
	    IPaiementMoyen moyenPaiementPremiereEcheance, int nombreEcheances, int numeroJourEcheance, int nbjoursMoisSuivant)
	    throws PreEtudiantPaiementException {

		if (moyenPaiement.toTypePaiement().code().equals(ITypePaiement.CODE_TYPE_COMPTANT)) {
			String message = IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_COMPTANT;
			LOG.debug(getMessageLog(moyenPaiement, message));
			throw new PreEtudiantPaiementException(message);
		}

		// Récupérer la date de première échéance
		LocalDate datePremiereEcheance = SepaSddEcheancierHelper.getSharedInstance().calculeDatePremiereEcheanceAvecDateDuJour(nbjoursMoisSuivant,
		    numeroJourEcheance);

		// Total à payer
		BigDecimal totalAPayer = montantPaiement;

		BigDecimal nombreEcheancesBigDec = new BigDecimal(nombreEcheances);

		// Si rien à payer ou pas assez à payer, on ne génère pas d'échéancier
		if (totalAPayer.compareTo(BigDecimal.ZERO) == 0) {
			String message = IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_ZERO;
			LOG.debug(getMessageLog(moyenPaiement, message));
			throw new PreEtudiantPaiementException(message);
		} else if (totalAPayer.compareTo(nombreEcheancesBigDec) < 0) {
			String message = String.format(IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_INFERIEUR_ECHEANCES, totalAPayer, nombreEcheancesBigDec);
			LOG.debug(getMessageLog(moyenPaiement, message));
			throw new PreEtudiantPaiementException(message);
		}

		// Calculer le montant de chaque échéances (arrondis à l'entier égal ou inférieur)
		// La première échéance est la plus élevées (dans le cas où il y a des arrondis fait sur les autres)
		BigDecimal montantEcheances = totalAPayer.divideToIntegralValue(nombreEcheancesBigDec);
		BigDecimal montantPremiereEcheance = totalAPayer.subtract(montantEcheances.multiply(nombreEcheancesBigDec.subtract(BigDecimal.ONE)));

		// Créer chaque échéance
		int numeroEcheance = 1;
		LocalDate dateEcheance = datePremiereEcheance;
		IPaiementEcheance premiereEcheance = creerEcheance(editingContext, moyenPaiement, dateEcheance, getLibelleEcheance(numeroEcheance, nombreEcheances), montantPremiereEcheance);
		if (moyenPaiementPremiereEcheance != null) {
			premiereEcheance.setDateEcheance(new NSTimestamp());
			premiereEcheance.setLibelle("Paiement par " + moyenPaiementPremiereEcheance.toModePaiement().libelle());
			premiereEcheance.setToAutrePaiementMoyenRelationship(moyenPaiementPremiereEcheance);
			BigDecimal montantTotalPrelevement = totalAPayer.subtract(montantPremiereEcheance);
			moyenPaiement.setMontantPaye(montantTotalPrelevement);
			moyenPaiementPremiereEcheance.setMontantPaye(montantPremiereEcheance);
		} else {
			moyenPaiement.setMontantPaye(totalAPayer);
		}

		while (numeroEcheance < nombreEcheances) {
			numeroEcheance++;
			dateEcheance = dateEcheance.plusMonths(1);
			creerEcheance(editingContext, moyenPaiement, dateEcheance, getLibelleEcheance(numeroEcheance, nombreEcheances), montantEcheances);
		}
	}

	/**
	 * Créer une échéance.
	 * @param editingContext un editing context
	 * @param moyenPaiement un preMoyenPaiement
	 * @param date la date d'échéance
	 * @param libelle le libellé de l'échéance
	 * @param montant le montant de l'échéance
	 * @return l'échéance créé
	 */
	private IPaiementEcheance creerEcheance(EOEditingContext editingContext, IPaiementMoyen moyenPaiement, LocalDate date, String libelle,
	    BigDecimal montant) {
		
		IPaiementEcheance echeancePaiement;
		if (moyenPaiement instanceof IPrePaiementMoyen) {
			echeancePaiement = EOPrePaiementEcheance.creerInstance(editingContext);
		} else {
			echeancePaiement = (EOPaiementEcheance) EOUtilities.createAndInsertInstance(editingContext, _EOPaiementEcheance.ENTITY_NAME);
		}
		
		echeancePaiement.setToPaiementMoyenRelationship(moyenPaiement);
		echeancePaiement.setDateEcheance(new NSTimestamp(date.toDate()));
		echeancePaiement.setLibelle(libelle);
		echeancePaiement.setMontant(montant);

		return echeancePaiement;
	}

	/**
	 * Retourne le libellé d'une échéance.
	 * @param numeroEcheance le numéro de l'échéance (de 1 à n)
	 * @param nombreEcheances le nombre total d'échéances (n)
	 * @return le libellé de l'échéance
	 */
	private String getLibelleEcheance(int numeroEcheance, int nombreEcheances) {
		return String.format(LIBELLE_ECHEANCE, numeroEcheance, nombreEcheances);
	}

	/**
	 * Supprimer un échéancier.
	 * @param editingContext un editing context
	 * @param moyenPaiement un moyen de paiement
	 * @throws PreEtudiantPaiementException Si l'échéancier n'a pas pu être supprimé
	 */
	public void supprimerEcheancier(EOEditingContext editingContext, IPaiementMoyen moyenPaiement) throws PreEtudiantPaiementException {
		// Pour le moment on ne peut pas modifier un échéancier validé
		if (moyenPaiement.toPaiement().paiementValide()) {
			String message = IMPOSSIBLE_SUPPRIMER_ECHEANCIER_PAIEMENT_VALIDE;
			LOG.debug(getMessageLog(moyenPaiement, message));
			throw new PreEtudiantPaiementException(message);
		}

		List<IPaiementEcheance> listeEcheances = new ArrayList<IPaiementEcheance>(moyenPaiement.toPaiementEcheances());
		for (IPaiementEcheance echeancePaiement : listeEcheances) {
			editingContext.deleteObject((EOEnterpriseObject) echeancePaiement);
		}
	}

	/**
	 * Retourne le message à afficher dans les log. Ajoute l'id du paiement en début de message.
	 * @param moyenPaiement un moyen de paiement
	 * @param message un message
	 * @return le message à afficher dans les log
	 */
	private String getMessageLog(IPaiementMoyen moyenPaiement, String message) {
		String prefix;
		if (moyenPaiement instanceof IPrePaiementMoyen) {
			prefix = PREFIX_PRE_MESSAGE_LOG;
		} else {
			prefix = PREFIX_SCO_MESSAGE_LOG;
		}
		
		return String.format(prefix, ((ERXGenericRecord) moyenPaiement.toPaiement()).primaryKey(),
		    ((ERXGenericRecord) moyenPaiement).primaryKey(), message);
	}

	/**
	 * Supprimer tout les paiements d'un étudiant année.
	 * @param editingContext un editing context
	 * @param etudiantAnnee l'étudiant année
	 */
	public void supprimerPaiements(EOEditingContext editingContext, IEtudiantAnnee etudiantAnnee) {
		for (IPaiement paiement : new ArrayList<IPaiement>(etudiantAnnee.toPaiements())) {
			supprimerPaiement(editingContext, paiement);
		}
	}

	/**
	 * Supprimer le paiement.
	 * @param editingContext un editing context
	 * @param paiement Le paiement à supprimer
	 */
	public void supprimerPaiement(EOEditingContext editingContext, IPaiement paiement) {
		for (IPaiementDetail paiementDetail : new ArrayList<IPaiementDetail>(paiement.toPaiementDetails())) {
			paiementDetail.setToPaiementRelationship(null);
			editingContext.deleteObject((EOEnterpriseObject) paiementDetail);
		}
		
		for (IPaiementMoyen paiementMoyen : new ArrayList<IPaiementMoyen>(paiement.toPaiementMoyens())) {
			for (IPaiementEcheance paiementEcheance : new ArrayList<IPaiementEcheance>(paiementMoyen.toPaiementEcheances())) {
				paiementEcheance.setToPaiementMoyenRelationship(null);
				paiementEcheance.setToAutrePaiementMoyenRelationship(null);
				editingContext.deleteObject((EOEnterpriseObject) paiementEcheance);
			}
			
			paiementMoyen.setToPaiementRelationship(null);
			editingContext.deleteObject((EOEnterpriseObject) paiementMoyen);
		}
		
		paiement.setToEtudiantAnneeRelationship(null);
		editingContext.deleteObject((EOEnterpriseObject) paiement);
	}
	
	/**
	 * Recalculer les montants à payer d'un étudiant quand sa situation change
	 * @param editingContext contexte d'édition
	 * @param paiement le paiement qui a été modifié
	 */
	public void recalculerMontantsAPayer(EOEditingContext editingContext, IPaiement paiement) {
		BigDecimal nouveauTotal = BigDecimal.ZERO;
		
		// mise à jour des articles qui ont été ajoutés automatiquement
		for (IPaiementDetail detailPaiement : paiement.toPaiementDetailArticlesManuel()) {
			BigDecimal nouveauMontant = getMontantAPayer(paiement.toEtudiantAnnee(), detailPaiement.toParametragePaie());
			detailPaiement.setMontantAPayer(nouveauMontant);
			nouveauTotal = nouveauTotal.add(nouveauMontant);
		}

		// suppression des details autos
		for (IPaiementDetail detailPaiement : paiement.toPaiementDetailArticlesAuto()) {
		    detailPaiement.setToPaiementRelationship(null);
		    editingContext.deleteObject((EOEnterpriseObject) detailPaiement);
	    }
		
		paiement.setMontant(nouveauTotal);
		
		// Recalcul et ajout des nouveaux détails auto
		List<IParametragePaie> listeArticlesPaiement = rechercherArticlesPaiementParDefaut(editingContext, paiement.toEtudiantAnnee());
		ajouterDetailsPaiementAuto(editingContext, paiement, listeArticlesPaiement);
	}
	
}
