package org.cocktail.fwkcktlprescolarite.common.metier.services;


public class CalculIneService {

	public String calculer(String uai, Integer numcle, Integer annee) {

		Integer uai_nbre = new Integer(uai.substring(0, 7));

		// 'Calcul des restes (5 caractères)
		Integer r5 = uai_nbre % 36;
		Integer r4 = ((uai_nbre - r5) / 36) % 36;
		Integer r3 = (((uai_nbre - r5) / 36 - r4) / 36) % 36;
		Integer r2 = ((((uai_nbre - r5) / 36 - r4) / 36 - r3) / 36) % 36;
		Integer r1 = (((((uai_nbre - r5) / 36 - r4) / 36 - r3) / 36 - r2) / 36) % 36;

		//
		// 'Calcul du numéro de série
		Integer r6 = calculNumSerie(annee);

		// Calcul des rangs
		Integer r10 = numcle % 36;
		Integer r9 = (numcle - r10) / 36 % 36;
		Integer r8 = ((numcle - r10) / 36 - r9) / 36 % 36;
		Integer r7 = (((numcle - r10) / 36 - r9) / 36 - r8) / 36 % 36;

		// 'Calcul de la clé
		Integer cle = (6 * (r1 + r2 + r3 + r4 + r5 + r6 + r7 + r8 + r9) + r10) % 10;
		//
		// 'Ecriture de l'INE
		String ine  = b36(r1) + b36(r2) + b36(r3) + b36(r4) + b36(r5) + b36(r6) + b36(r7) + b36(r8) + b36(r9) + b36(r10) + cle.toString();

		return ine;

	}

	/**
	 * @param annee
	 */
	private Integer calculNumSerie(Integer annee) {
		Integer numSerie;
		if (annee < 2016) {
			numSerie = annee - 1990 + 1;
		} else {
			numSerie = 11 + (annee - 1990) % 26;
		}
		return numSerie;
	}

	private String b36(Integer numero) {
		switch (numero) {
		case 10:
			return "a";
		case 11:
			return "b";
		case 12:
			return "c";
		case 13:
			return "d";
		case 14:
			return "e";
		case 15:
			return "f";
		case 16:
			return "g";
		case 17:
			return "h";
		case 18:
			return "i";
		case 19:
			return "j";
		case 20:
			return "k";
		case 21:
			return "l";
		case 22:
			return "m";
		case 23:
			return "n";
		case 24:
			return "o";
		case 25:
			return "p";
		case 26:
			return "q";
		case 27:
			return "r";
		case 28:
			return "s";
		case 29:
			return "t";
		case 30:
			return "u";
		case 31:
			return "v";
		case 32:
			return "w";
		case 33:
			return "x";
		case 34:
			return "y";
		case 35:
			return "z";
		default:
			return numero.toString();
		}
	}
	
}
