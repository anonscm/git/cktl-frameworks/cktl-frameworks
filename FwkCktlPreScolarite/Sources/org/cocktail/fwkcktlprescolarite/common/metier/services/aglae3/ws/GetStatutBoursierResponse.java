
package org.cocktail.fwkcktlprescolarite.common.metier.services.aglae3.ws;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getStatutBoursierReturn" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getStatutBoursierReturn"
})
@XmlRootElement(name = "getStatutBoursierResponse")
public class GetStatutBoursierResponse {

    @XmlElement(required = true)
    protected String getStatutBoursierReturn;

    /**
     * Gets the value of the getStatutBoursierReturn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGetStatutBoursierReturn() {
        return getStatutBoursierReturn;
    }

    /**
     * Sets the value of the getStatutBoursierReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGetStatutBoursierReturn(String value) {
        this.getStatutBoursierReturn = value;
    }

}
