package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolarite;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;
import er.extensions.localization.ERXLocalizer;

public class ExportListeEtudiantsService {

	private static final String SHOW_ETAT_INSCRIPTION = "showEtatInscription";
	private static final String SHOW_ETAT_PAIEMENT = "showEtatPaiement";
	private static final String INFOS_ETUDIANTS = "infosEtudiants";
	private static final String ENTETE = "entete";

	private static final String REPORTS_FOLDER = "Reports";
	private static final String REPORTS_SUB_FOLDER = "ExportEtudiants";
	private static final String REPORTS_FILENAME = REPORTS_SUB_FOLDER + ".xls";
	private static final String REPORT_EXPORT_ETUDIANTS_PATH = REPORTS_FOLDER + File.separator + REPORTS_SUB_FOLDER + File.separator + REPORTS_FILENAME;

	public CktlDataResponse genererReponseExcel(EOEditingContext editingContext, EOQualifier qualifier, Boolean showEtatInscription, Boolean showEtatPaiement, Boolean isPreEtudiant) throws IOException, ParsePropertyException, InvalidFormatException {
    	Map<String, Object> beans = getDonneesExport(editingContext, qualifier, showEtatInscription, showEtatPaiement, isPreEtudiant);
        
        XLSTransformer transformer = new XLSTransformer();
    	ERXResourceManager erm = (ERXResourceManager) ERXApplication.application().resourceManager();
    	URL url = erm.pathURLForResourceNamed(REPORT_EXPORT_ETUDIANTS_PATH, FwkCktlPreScolarite.class.getSimpleName()	, null);
        InputStream is = new BufferedInputStream(url.openStream());
        Workbook workbook = transformer.transformXLS(is, beans);
        
        // width auto pour les colonnes
        Sheet firstSheet = workbook.getSheetAt(0);
        Row firstRow = firstSheet.getRow(0);
        Integer columnCount = 0;
        for (int i = 0; i < firstRow.getLastCellNum(); i++) {
        	firstSheet.autoSizeColumn(i);
        	//  Code un peu bidon mais bon, what else ? 
        	if(!firstRow.getCell(i).getStringCellValue().isEmpty()) {
        		columnCount++;
        	}
        }
        firstSheet.setAutoFilter(new CellRangeAddress(firstSheet.getFirstRowNum(), firstSheet.getLastRowNum(), 0, columnCount-1));

        
        // téléchargement du fichier généré
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        workbook.write(os);
        byte[] bytes = os.toByteArray();
		CktlDataResponse resp = new CktlDataResponse();
		resp.setHeader("maxage=1", "Cache-Control");
		resp.setHeader("public", "Pragma");
		resp.setContent(bytes, "application/xls");
		resp.setFileName(REPORTS_FILENAME);
		return resp;
	}
	
	
	private Map<String, Object> getDonneesExport(EOEditingContext editingContext, EOQualifier qualifier, Boolean showEtatInscription, Boolean showEtatPaiement, Boolean isPreEtudiant) {
		Map<String, Object> donnees = new HashMap<String, Object>();
		if (isPreEtudiant) {
			donnees.put(INFOS_ETUDIANTS, getInfosEtudiants(editingContext, qualifier, showEtatInscription, showEtatPaiement));
		} else {
			donnees.put(INFOS_ETUDIANTS, getInfosEtudiantsTermines(editingContext, qualifier));
		}
		donnees.put(SHOW_ETAT_INSCRIPTION, showEtatInscription);
		donnees.put(SHOW_ETAT_PAIEMENT, showEtatPaiement);
		donnees.put(ENTETE, getLibellesEntete());
		return donnees;
	}
	
	private List<InfosEtudiant> getLibellesEntete() {
		List<InfosEtudiant> infos = new ArrayList<ExportListeEtudiantsService.InfosEtudiant>();
		
		ERXLocalizer localizer = ERXLocalizer.currentLocalizer();
		
		InfosEtudiant infosEtudiant = new InfosEtudiant();
		infosEtudiant.setIne(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneINE"));
		infosEtudiant.setNomPrenom(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneDenominationEtudiant"));
		infosEtudiant.setDateDeNaissance(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneDateNaissance"));
		infosEtudiant.setCodeInsee(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneINSEE"));
		infosEtudiant.setSituationFamilliale(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneSitFam"));
		infosEtudiant.setSituationProfessionnelle(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneSitProfession"));
		infosEtudiant.setPremiereInscription(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneFirstInsc"));
		infosEtudiant.setDeuxiemeInscription(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneSecondInsc"));
		infosEtudiant.setTroisiemeInscription(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneThirdInsc"));
		infosEtudiant.setExonerationBourse(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneBoursesExo"));
		infosEtudiant.setEtatInscription(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtat"));
		infosEtudiant.setEtatPaiement(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement"));
		infosEtudiant.setEtatEtudiant(localizer.localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtat"));
		infos.add(infosEtudiant);
		
		return infos;		
	}
	
	private List<InfosEtudiant> getInfosEtudiants(EOEditingContext editingContext, EOQualifier qualifier, Boolean showEtatInscription, Boolean showEtatPaiement) {
		List<InfosEtudiant> infos = new ArrayList<ExportListeEtudiantsService.InfosEtudiant>();
		
		NSArray<EOPreEtudiant> preEtudiants = EOPreEtudiant.fetchAll(editingContext, qualifier, EOPreEtudiant.NOM_USUEL.asc().then(EOPreEtudiant.PRENOM.asc()));
		
		for(EOPreEtudiant preEtudiant : preEtudiants) {
			InfosEtudiant infosEtudiant = new InfosEtudiant();
			infosEtudiant.setIne(preEtudiant.codeIne());
			infosEtudiant.setNomPrenom(preEtudiant.nomUsuel() + " " + preEtudiant.prenom());
			infosEtudiant.setDateDeNaissance(DateFormatUtils.format(preEtudiant.dNaissance(), "dd/MM/yyyy"));
			infosEtudiant.setCodeInsee(preEtudiant.noInsee());
			infosEtudiant.setSituationFamilliale(preEtudiant.toSituationFamiliale().lSituationFamille());
			if (preEtudiant.toPreEtudiantAnneeExercice() != null && preEtudiant.toPreEtudiantAnneeExercice().toSituationProfessionnelle() != null) {
				infosEtudiant.setSituationProfessionnelle(preEtudiant.toPreEtudiantAnneeExercice().toSituationProfessionnelle().libelle());
			}
			if (preEtudiant.getInscriptionPrincipale() != null) {
				infosEtudiant.setPremiereInscription(preEtudiant.getInscriptionPrincipale().toDiplome().libelle());
			}
			if (preEtudiant.getInscriptionSecondaire() != null) {
				infosEtudiant.setDeuxiemeInscription(preEtudiant.getInscriptionSecondaire().toDiplome().libelle());
			}
			if (preEtudiant.getInscriptionTertiaire() != null) {
				infosEtudiant.setTroisiemeInscription(preEtudiant.getInscriptionTertiaire().toDiplome().libelle());
			}
			if (preEtudiant.toPreEtudiantAnneeExercice() != null) {
				infosEtudiant.setExonerationBourse(preEtudiant.toPreEtudiantAnneeExercice().boursier() ? "O" : "X");
			}
			if (showEtatInscription) {
				infosEtudiant.setEtatInscription(preEtudiant.currentEtat());
			}
			if (showEtatPaiement) {
				infosEtudiant.setEtatPaiement(preEtudiant.getInfoAffichagePaiement());
			}
			infos.add(infosEtudiant);
		}
		
		return infos;
	}
	
	private List<InfosEtudiant> getInfosEtudiantsTermines(EOEditingContext editingContext, EOQualifier qualifier) {
		List<InfosEtudiant> infos = new ArrayList<ExportListeEtudiantsService.InfosEtudiant>();
		
		NSArray<EOSortOrdering> sortOrderings = new NSMutableArray<EOSortOrdering>();
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOEtudiantAnnee.TO_ETUDIANT.dot(EOEtudiant.TO_INDIVIDU.dot(EOIndividu.NOM_AFFICHAGE).key()).key(), EOSortOrdering.CompareCaseInsensitiveDescending));
			sortOrderings.add(EOSortOrdering.sortOrderingWithKey(EOEtudiantAnnee.TO_ETUDIANT.dot(EOEtudiant.TO_INDIVIDU.dot(EOIndividu.PRENOM).key()).key(), EOSortOrdering.CompareCaseInsensitiveDescending));
		
		NSArray<EOEtudiantAnnee> etudiantsAnnees = EOEtudiantAnnee.fetchSco_EtudiantAnnees(editingContext, qualifier, sortOrderings);   
		
		for (EOEtudiantAnnee etudiantAnnee : etudiantsAnnees) {
			InfosEtudiant infosEtudiant = new InfosEtudiant();
			infosEtudiant.setIne(etudiantAnnee.toEtudiant().etudCodeIne());
			infosEtudiant.setNomPrenom(etudiantAnnee.toEtudiant().toIndividu().nomAffichage() + " " + etudiantAnnee.toEtudiant().toIndividu().prenom());
			infosEtudiant.setDateDeNaissance(DateFormatUtils.format(etudiantAnnee.toEtudiant().toIndividu().dNaissance(), "dd/MM/yyyy"));
			infosEtudiant.setCodeInsee(etudiantAnnee.toEtudiant().toIndividu().indNoInsee());
			if(etudiantAnnee.toSituationFamiliale() != null) {
				infosEtudiant.setSituationFamilliale(etudiantAnnee.toSituationFamiliale().lSituationFamille());
			}
			if (etudiantAnnee.toSituationProfessionnelle() != null) {
				infosEtudiant.setSituationProfessionnelle(etudiantAnnee.toSituationProfessionnelle().libelle());
			}
			if (etudiantAnnee.getInscriptionPrincipale() != null) {
				infosEtudiant.setPremiereInscription(etudiantAnnee.getInscriptionPrincipale().toDiplome().libelle());
			}
			if (etudiantAnnee.getInscriptionSecondaire() != null) {
				infosEtudiant.setDeuxiemeInscription(etudiantAnnee.getInscriptionSecondaire().toDiplome().libelle());
			}
			if (etudiantAnnee.getInscriptionTertiaire() != null) {
				infosEtudiant.setTroisiemeInscription(etudiantAnnee.getInscriptionTertiaire().toDiplome().libelle());
			}
			infosEtudiant.setExonerationBourse(etudiantAnnee.boursier() ? "O" : "X");
			
			
			if (etudiantAnnee.toPaiementInitial() != null && etudiantAnnee.toPaiementInitial().paiementValide()) {
				infosEtudiant.setEtatPaiement(ERXLocalizer.currentLocalizer().localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement.Paye"));
			} else {
				infosEtudiant.setEtatPaiement(ERXLocalizer.currentLocalizer().localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement.NonPaye"));
			}
			
			if (etudiantAnnee.valide()) {
				infosEtudiant.setEtatEtudiant("");
			} else {
				infosEtudiant.setEtatEtudiant(ERXLocalizer.currentLocalizer().localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement.Demissionnaire"));
			}
			infos.add(infosEtudiant);
		}
		
		return infos;
	}
	
	
	
	public static class InfosEtudiant {
		
		private String ine;
		private String nomPrenom;
		private String dateDeNaissance;
		private String codeInsee;
		private String situationFamilliale;
		private String situationProfessionnelle;
		private String premiereInscription;
		private String deuxiemeInscription;
		private String troisiemeInscription;
		private String exonerationBourse;
		private String etatInscription;
		private String etatPaiement;
		private String etatEtudiant;
		
		public String getIne() {
			return ine;
		}
		public void setIne(String ine) {
			this.ine = ine;
		}
		public String getNomPrenom() {
			return nomPrenom;
		}
		public void setNomPrenom(String nomPrenom) {
			this.nomPrenom = nomPrenom;
		}

		public String getDateDeNaissance() {
			return dateDeNaissance;
		}
		public void setDateDeNaissance(String dateDeNaissance) {
			this.dateDeNaissance = dateDeNaissance;
		}
		public String getCodeInsee() {
			return codeInsee;
		}
		public void setCodeInsee(String codeInsee) {
			this.codeInsee = codeInsee;
		}
		public String getSituationFamilliale() {
			return situationFamilliale;
		}
		public void setSituationFamilliale(String situationFamilliale) {
			this.situationFamilliale = situationFamilliale;
		}
		public String getSituationProfessionnelle() {
			return situationProfessionnelle;
		}
		public void setSituationProfessionnelle(String situationProfessionnelle) {
			this.situationProfessionnelle = situationProfessionnelle;
		}
		public String getPremiereInscription() {
			return premiereInscription;
		}
		public void setPremiereInscription(String premiereInscription) {
			this.premiereInscription = premiereInscription;
		}
		public String getDeuxiemeInscription() {
			return deuxiemeInscription;
		}
		public void setDeuxiemeInscription(String deuxiemeInscription) {
			this.deuxiemeInscription = deuxiemeInscription;
		}
		public String getTroisiemeInscription() {
			return troisiemeInscription;
		}
		public void setTroisiemeInscription(String troisiemeInscription) {
			this.troisiemeInscription = troisiemeInscription;
		}
		public String getExonerationBourse() {
			return exonerationBourse;
		}
		public void setExonerationBourse(String exonerationBourse) {
			this.exonerationBourse = exonerationBourse;
		}
		public String getEtatInscription() {
			return etatInscription;
		}
		public void setEtatInscription(String etatInscription) {
			this.etatInscription = etatInscription;
		}
		public String getEtatPaiement() {
			return etatPaiement;
		}
		public void setEtatPaiement(String etatPaiement) {
			this.etatPaiement = etatPaiement;
		}
		public String getEtatEtudiant() {
			return etatEtudiant;
		}
		public void setEtatEtudiant(String etatEtudiant) {
			this.etatEtudiant = etatEtudiant;
		};
	}
}
