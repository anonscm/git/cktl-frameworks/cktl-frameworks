package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Nullable;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddEcheancier;
import org.cocktail.fwkcktlcompta.common.sepasdd.entities.ISepaSddMandat;
import org.cocktail.fwkcktlcompta.server.scolarite.IEcheanceLight;
import org.cocktail.fwkcktlcompta.server.scolarite.PrelevementSepaService;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.eospecificites.ISpecificite;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOCommune;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompte;
import org.cocktail.fwkcktlpersonne.common.metier.EOCompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EOPhotosEtudiantsGrhum;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPhoto;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;
import org.cocktail.fwkcktlpersonne.common.metier.repositories.EOPaysIndicatifRepository;
import org.cocktail.fwkcktlpersonne.common.metier.repositories.IPaysIndicatifRepository;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreAdresse;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreBourses;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiementDetail;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiementEcheance;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiementMoyen;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreTelephone;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs.CotisationRegimeSecuControle;
import org.cocktail.fwkcktlprescolarite.common.metier.factories.EOPreTelephoneFactory;
import org.cocktail.fwkcktlprescolarite.common.metier.repositories.EOPreCandidatRepository;
import org.cocktail.fwkcktlprescolarite.common.metier.repositories.IPreCandidatRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupe;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGroupeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEchange;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITelephoneFactory;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.NumeroEtudiantGenerateur;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.NumeroEtudiantGenerateurPrefixeSequence;
import org.cocktail.fwkcktlscolpeda.serveur.metier.interfaces.ITypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services.InscriptionPedagogiqueService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ip.services.InscriptionPedagogiqueServiceImpl;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.google.inject.Inject;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXGenericRecord;
import er.extensions.eof.ERXQ;

/**
 * @author isabelle
 */
public class EOPreEtudiantService {

	private static Logger LOG = Logger.getLogger(EOPreEtudiantService.class);

	@Inject
	@Nullable
	private UserInfo userInfo;

	private EOPreEtudiantPaiementService preEtudiantPaiementService = new EOPreEtudiantPaiementService();

	private IPaysIndicatifRepository paysIndicatifRepository;

	public EOPreEtudiantService() {
	}

	public EOPreEtudiantService(IPaysIndicatifRepository paysIndicatifRepository) {
		this.paysIndicatifRepository = paysIndicatifRepository;
	}

	public void setPaysIndicatifRepository(IPaysIndicatifRepository repository) {
		this.paysIndicatifRepository = repository;
	}

	private IPaysIndicatifRepository paysIndicatifRepository(EOEditingContext editingContext) {
		if (paysIndicatifRepository == null) {
			paysIndicatifRepository = new EOPaysIndicatifRepository(editingContext);
		}
		return paysIndicatifRepository;
	}

	/**
	 * @param editingContext to set
	 * @param persIdCreateur to set
	 * @param annee to set
	 * @return EOPreEtudiant
	 */
	public EOPreEtudiant creerNouveauPreEtudiantPourPreInscription(EOEditingContext editingContext, Integer persIdCreateur, String annee) {
		EOPreEtudiant preEtudiant = EOPreEtudiant.creer(editingContext, persIdCreateur);
		preEtudiant.setTypeInscription("P");
		preEtudiant.setPreType("P");
		preEtudiant.setAnnee1InscEtab(new Integer(annee));

		EOPreEtudiantAnnee.creer(editingContext, persIdCreateur, preEtudiant, Integer.valueOf(annee));
		IPreAdresse preAdresse = EOPreAdresse.creer(editingContext, preEtudiant, persIdCreateur);
		EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_ETUD);
		preAdresse.setToTypeAdresseRelationship(typeAdresse);

		IPaysIndicatifRepository repo = paysIndicatifRepository(editingContext);
		EOPreTelephoneFactory factory = new EOPreTelephoneFactory(editingContext, repo, persIdCreateur);
		factory.creerTelephoneFixeEtudiant(preEtudiant);
		factory.creerTelephoneGsmEtudiant(preEtudiant);

		return preEtudiant;
	}

	/**
	 * @param editingContext to set
	 * @param etudiant to set
	 * @param individu to set
	 * @param annee to set
	 * @return EOPreEtudiant
	 */
	public EOPreEtudiant creerNouveauPreEtudiantPourReInscription(EOEditingContext editingContext, IEtudiant etudiant, IIndividu individu, Integer annee) {

		IPreEtudiant preEtudiant = importEtudiantDansPreEtudiant(editingContext, etudiant, individu, annee, false);
		importEtudiantAnneeDansPreEtudiantAnne(editingContext, preEtudiant, annee, false, null, null);
		importBoursesDansPreBourses(editingContext, etudiant, preEtudiant, annee, false, null);
		importCursusDansPreCursus(editingContext, etudiant, preEtudiant, false, null);
		importAdressesDansPreAdresse(editingContext, individu, preEtudiant, false, null);
		importTelephonesDansPreTelephones(editingContext, individu, preEtudiant, false, null);

		return (EOPreEtudiant) preEtudiant;
	}

	/**
	 * @param editingContext to set
	 * @param preCandidat to set
	 * @param annee to set
	 * @return EOPreEtudiant
	 */
	public EOPreEtudiant creerNouveauPreEtudiantPourPreInscriptionPostBac(EOEditingContext editingContext, EOPreCandidat preCandidat, String annee) {
		IPreCandidatRepository candidatRepo = new EOPreCandidatRepository(editingContext);
		EOPreEtudiant preEtudiant = (EOPreEtudiant) candidatRepo.fetchPreEtudiantFromCandidat(preCandidat);

		if (preEtudiant == null) {
			preEtudiant = EOPreEtudiant.creer(editingContext, null);
			preEtudiant.setTypeInscription("P");
			preEtudiant.setPreType("P");
			if (preCandidat.annePremEtab() != null) {
				preEtudiant.setAnnee1InscEtab(preCandidat.annePremEtab());
			} else {
				preEtudiant.setAnnee1InscEtab(new Integer(annee));
			}
			EOCivilite civilite = deduireCivilite(editingContext, preCandidat.candCivilite());
			preEtudiant.setCodeIne(preCandidat.candBea());
			preEtudiant.setToCiviliteRelationship(civilite);
			if (preCandidat.candNomUsuel() != null) {
				preEtudiant.setNomUsuel(preCandidat.candNomUsuel());
			} else {
				preEtudiant.setNomUsuel(preCandidat.candNom());
			}
			preEtudiant.setNomPatronymique(preCandidat.candNom());
			if (StringCtrl.containsIgnoreCase(preCandidat.candPrenom(), " ")) {
				preEtudiant.setPrenom(StringCtrl.getPrefix(preCandidat.candPrenom(), " "));
				preEtudiant.setPrenom2(StringCtrl.getSuffix(preCandidat.candPrenom(), " "));
			}
			preEtudiant.setPrenom(preCandidat.candPrenom());
			preEtudiant.setPrenom2(preCandidat.candPrenom2());
			if (!StringCtrl.isEmpty(preCandidat.candPrenom3())) {
				preEtudiant.setPrenom2(preEtudiant.prenom2() + " " + preCandidat.candPrenom3());
			}
			preEtudiant.setDNaissance(DateCtrl.stringToDate(preCandidat.candDateNais(), "%d%m%Y"));
			preEtudiant.setVilleDeNaissance(preCandidat.candComNais());
			EOPays paysNais;
			if (preCandidat.paysCodeNais() != null) {
				if ("P".equals(preCandidat.paysCodeNais())) {
					paysNais = EOPays.fetchPaysbyCode(editingContext, preCandidat.dptgCodeNais());
				} else {
					paysNais = EOPays.fetchPaysbyCode(editingContext, preCandidat.paysCodeNais());
				}
				preEtudiant.setToPaysNaissanceRelationship(paysNais);
				if (preCandidat.natOrdre() != null) {
					EOPays paysNationalite = EOPays.fetchPaysbyCode(editingContext, preCandidat.natOrdre().toString());
					preEtudiant.setToPaysNationaliteRelationship(paysNationalite);
				}
				if (paysNais.cPays().equals(EOPays.CODE_PAYS_FRANCE) && !"P".equals(preCandidat.paysCodeNais())) {
					String communeNaissanceMajuscule = getStringMajusculeSansAcents(preCandidat.candComNais());
					EOKeyValueQualifier llQualifier = new EOKeyValueQualifier(EOCommune.LL_COM_KEY, EOQualifier.QualifierOperatorEqual, communeNaissanceMajuscule);
					EOKeyValueQualifier lcQualifier = new EOKeyValueQualifier(EOCommune.LC_COM_KEY, EOQualifier.QualifierOperatorEqual, communeNaissanceMajuscule);
					EOKeyValueQualifier cDepQualifier = null;
					if (preCandidat.dptgCodeNais().length() == 2) {
						cDepQualifier = new EOKeyValueQualifier(EOCommune.C_DEP_KEY, EOQualifier.QualifierOperatorEqual, "0" + preCandidat.dptgCodeNais());
					} else {
						cDepQualifier = new EOKeyValueQualifier(EOCommune.C_DEP_KEY, EOQualifier.QualifierOperatorEqual, preCandidat.dptgCodeNais());
					}

					NSArray<EOQualifier> qualifiers = new NSArray<EOQualifier>(new EOQualifier[] { llQualifier, lcQualifier });
					EOQualifier qualifier = new EOOrQualifier(qualifiers);
					NSArray<EOQualifier> qualifiersDep = new NSArray<EOQualifier>(new EOQualifier[] { cDepQualifier, qualifier });
					EOQualifier qualifierfinal = new EOAndQualifier(qualifiersDep);

					EOCommune commune = EOCommune.fetchFirstByQualifier(editingContext, qualifierfinal);
					if (commune != null) {
						preEtudiant.setDeptNaissance(commune.cDep());
						preEtudiant.setVilleDeNaissance(commune.llCom());
					}
				}
			}
			preEtudiant.setCandNumero(preCandidat.candNumero());
			EOBac bac = EOBac.fetchByKeyValue(editingContext, EOBac.BAC_CODE_KEY, preCandidat.bacCode());
			preEtudiant.setToBacsRelationship(bac);
			preEtudiant.setAnneeBac(preCandidat.candAnBac());
			if (preCandidat.etabCodeBac() != null) {
				EORne etabBac = EORne.fetchByKeyValue(editingContext, EORne.C_RNE_KEY, preCandidat.etabCodeBac());
				preEtudiant.setToRneEtabBacRelationship(etabBac);
			}
			preEtudiant.setVilleBac(preCandidat.candVilleBac());
			if (preCandidat.paysEtabBac() != null) {
				EOPays paysBac = EOPays.fetchPaysbyCode(editingContext, preCandidat.paysEtabBac());
				preEtudiant.setToPaysEtabBacRelationship(paysBac);
			}
			EOPreEtudiantAnnee preEtudAnne = EOPreEtudiantAnnee.creer(editingContext, null, preEtudiant, Integer.valueOf(annee));
			if (preCandidat.candEnfantsCharge() != null && preCandidat.candEnfantsCharge().intValue() > 0) {
				preEtudAnne.setEnfantsCharge(true);
				preEtudAnne.setNbEnfantsCharge(preCandidat.candEnfantsCharge());
			}
			if (preCandidat.proCode1() != null) {
				EOProfession procode1 = EOProfession.fetchFirstByQualifier(editingContext, EOProfession.PRO_CODE.eq(preCandidat.proCode1()));
				if (procode1 != null) {
					preEtudiant.setToProfession1Relationship(procode1);
					preEtudAnne.setToProfession1Relationship(procode1);
				}
			}
			if (preCandidat.proCode2() != null) {
				EOProfession procode2 = EOProfession.fetchFirstByQualifier(editingContext, EOProfession.PRO_CODE.eq(preCandidat.proCode2()));
				if (procode2 != null) {
					preEtudiant.setToProfession2Relationship(procode2);
					preEtudAnne.setToProfession2Relationship(procode2);
				}
			}

			EOPreAdresse preAdresseEtud = EOPreAdresse.creer(editingContext, preEtudiant, null);
			EOTypeAdresse typeAdresseEtud = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_ETUD);
			EOPays paysParent = null;
			if (preCandidat.candPaysParent() != null) {
				paysParent = EOPays.fetchPaysbyCode(editingContext, preCandidat.candPaysParent());
			}

			preAdresseEtud.setToTypeAdresseRelationship(typeAdresseEtud);
			preAdresseEtud.setAdrAdresse1(preCandidat.candAdr1Parent());
			preAdresseEtud.setAdrAdresse2(preCandidat.candAdr2Parent());
			if (!StringCtrl.isEmpty(preCandidat.candAdr3Parent())) {
				preAdresseEtud.setAdrAdresse2(preCandidat.candAdr2Parent() + " " + preCandidat.candAdr3Parent());
			}
			if (preCandidat.candCpParent() != null) {
				preAdresseEtud.setCodePostal(preCandidat.candCpParent());
			} else {
				preAdresseEtud.setCodePostal(preCandidat.candVilleParentCode());
			}
			preAdresseEtud.setVille(preCandidat.candVilleParent());
			preAdresseEtud.setVille(preCandidat.candVilleParent());
			preAdresseEtud.setToPaysRelationship(paysParent);
			preAdresseEtud.setEmail(preCandidat.candEmailScol());
			EOPreAdresse preAdresse = EOPreAdresse.creer(editingContext, preEtudiant, null);
			EOTypeAdresse typeAdresse = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_PAR);

			preAdresse.setToTypeAdresseRelationship(typeAdresse);
			preAdresse.setAdrAdresse1(preAdresseEtud.adrAdresse1());
			preAdresse.setAdrAdresse2(preAdresseEtud.adrAdresse2());
			preAdresse.setCodePostal(preAdresseEtud.codePostal());
			preAdresse.setVille(preAdresseEtud.ville());
			preAdresse.setToPaysRelationship(paysParent);

			creerTelephones(editingContext, preCandidat, preEtudiant);
		}

		return preEtudiant;
	}

	private String getStringMajusculeSansAcents(String candComNais) {
		candComNais = candComNais.replace("é", "e");
		candComNais = candComNais.replace("è", "e");
		candComNais = candComNais.replace("ê", "e");
		candComNais = candComNais.replace("à", "a");
		candComNais = candComNais.replace("â", "a");
		candComNais = candComNais.replace("ô", "o");
		candComNais = candComNais.replace("î", "i");
		candComNais = candComNais.replace("ù", "u");
		candComNais = candComNais.replace("ç", "c");
		return candComNais.toUpperCase();
	}

	private void creerTelephones(EOEditingContext editingContext, EOPreCandidat preCandidat, EOPreEtudiant preEtudiant) {

		IPaysIndicatifRepository repo = paysIndicatifRepository(editingContext);
		ITelephoneFactory factory = new EOPreTelephoneFactory(editingContext, repo, null);
		factory.creerTelephoneFixeEtudiant(preEtudiant);
		EOPreTelephone preTelGsm = EOPreTelephone.creer(editingContext, preEtudiant, null);
		preTelGsm.setToTypeTelsRelationship(EOTypeTel.fetchByQualifier(editingContext, EOTypeTel.QUAL_C_TYPE_TEL_ETUD));
		preTelGsm.setToTypeNoTelRelationship(EOTypeNoTel.fetchByQualifier(editingContext, EOTypeNoTel.QUAL_TYPE_NO_TEL_MOB));
		String numPortscol = StringCtrl.keepDigits(preCandidat.candPortScol());
		if (numPortscol != null && numPortscol.length() > 10) {
			int longeurTel = numPortscol.length();
			preTelGsm.setIndicatif(new Integer(StringCtrl.cut(numPortscol, (longeurTel - 9))));
			preTelGsm.setNoTelephone("0" + StringCtrl.cut(numPortscol, 9, true));

		} else {
			if (numPortscol.length() == 9) {
				preTelGsm.setNoTelephone("0" + numPortscol);
			} else {
				preTelGsm.setNoTelephone(numPortscol);
			}
			preTelGsm.setIndicatif(33);
		}

		EOPreTelephone preTelFixeParent = EOPreTelephone.creer(editingContext, preEtudiant, null);
		preTelFixeParent.setToTypeTelsRelationship(EOTypeTel.fetchByQualifier(editingContext, EOTypeTel.QUAL_C_TYPE_TEL_PAR));
		preTelFixeParent.setToTypeNoTelRelationship(EOTypeNoTel.fetchByQualifier(editingContext, EOTypeNoTel.QUAL_TYPE_NO_TEL_TEL));
		String numTelParent = StringCtrl.keepDigits(preCandidat.candTelParent());
		if (numTelParent != null && numTelParent.length() > 10) {
			int longeurTel = numTelParent.length();
			preTelFixeParent.setIndicatif(new Integer(StringCtrl.cut(numTelParent, (longeurTel - 9))));
			preTelFixeParent.setNoTelephone("0" + StringCtrl.cut(numTelParent, 9, true));

		} else {
			if (numTelParent.length() == 9) {
				preTelFixeParent.setNoTelephone("0" + numTelParent);
			} else {
				preTelFixeParent.setNoTelephone(numTelParent);
			}
			preTelFixeParent.setIndicatif(33);
		}

		factory.creerTelephoneGsmParent(preEtudiant);
	}

	private EOCivilite deduireCivilite(EOEditingContext editingContext, String candCivilite) {
		String candCiviliteUpper = candCivilite.toUpperCase();
		EOCivilite civilite = EOCivilite.fetchByKeyValue(editingContext, EOCivilite.C_CIVILITE_KEY, candCiviliteUpper);
		if (civilite == null) {
			civilite = EOCivilite.fetchFirstByQualifier(editingContext, EOCivilite.SEXE_ONP.eq(candCiviliteUpper));
		}
		if (civilite == null) {
			civilite = EOCivilite.fetchFirstByQualifier(editingContext, EOCivilite.SEXE.eq(candCivilite));
		}
		return civilite;
	}

	/**
	 * Importer un étudiant dans le SI. ATTENTION : Cette méthode doit être idempotente.
	 * 
	 * @param editingContext contexte d'édition
	 * @param preEtudiant l'étudiant qui est importé dans le SI
	 * @param user l'utilisateur qui réalise l'import
	 * @param annee annee pour laquelle on réalise l'import
	 * @param codeRneEtablissement Le code RNE de l'établissement
	 * @param structureEtudiant structure à laquelle est rattaché l'étudiant
	 * @return L'étudiant année créé dans le SI
	 * @throws ImportException si une erreur survient lors de l'import dans le SI
	 */
	public IScoEtudiantAnnee importEtudiantDansSi(EOEditingContext editingContext, IPreEtudiant preEtudiant, PersonneApplicationUser user, Integer annee,
	    String codeRneEtablissement,
	    String structureEtudiant) throws ImportException {
		IIndividu individuEtudiant = creerOuMiseAJourIndividu(editingContext, preEtudiant, user, structureEtudiant, annee);
		IAdresse adresseStable = creerOuMiseAJourAdresses(editingContext, individuEtudiant, preEtudiant, user, annee);
		creerOuMiseAJourTelephones(editingContext, individuEtudiant, preEtudiant, user);
		IEtudiant etudiant = creerOuMiseAJourEtudiant(editingContext, preEtudiant, individuEtudiant, annee, codeRneEtablissement);
		IScoEtudiantAnnee etudiantAnnee = creerOuMiseAJourEtudiantAnnee(editingContext, etudiant, preEtudiant, user, annee);
		importPreCursusDansCursus(editingContext, etudiant, preEtudiant, user);
		importPreBoursesDansBourses(editingContext, etudiant, preEtudiant, annee, user);
		ITypeEtudiant typeEtudiant = EOTypeEtudiant.typeClassique(editingContext);
		creerIasPourPreinscriptions(editingContext, preEtudiant, etudiant, etudiantAnnee, annee, user, typeEtudiant, true);

		IPrePaiementMoyen moyenPrelevement = ((EOPreEtudiantAnnee) preEtudiant.toPreEtudiantAnnee(annee)).toPrePaiementInitial().toPrePaiementMoyenPrelevement();

		TitulaireInfos titulaireInfos = null;
		if (moyenPrelevement != null) {
			IIndividu individuTitulaire;
			IAdresse adresse;
			if (moyenPrelevement.etudiantTitulaire()) {
				individuTitulaire = individuEtudiant;
				adresse = adresseStable;
			} else {
				individuTitulaire = null;
				adresse = moyenPrelevement.toPreAdresseTitulaire();
			}
			titulaireInfos = creerOuRecupererInfosTitulaire(editingContext, user, individuTitulaire, moyenPrelevement.idPersonneTitulaire(),
			    moyenPrelevement.toCiviliteTitulaire(), adresse, EOFournis.FOU_TYPE_CLIENT, moyenPrelevement);
		}

		importerPrePaiementDansPaiement(editingContext, (IPreEtudiantAnnee) preEtudiant.toPreEtudiantAnnee(annee), etudiantAnnee, titulaireInfos);

		return etudiantAnnee;
	}

	/**
	 * Créer ou récupèrer les informations du titulaire du compte qui est prélevé.
	 * 
	 * @param editingContext un editing context
	 * @param user l'utilisateur connecté qui fait la demande
	 * @param individuTitulaire l'individu qui est le titulaire du compte (l'étudiant ou un tier)
	 * @param idPersonneTitulaire id de la personne titulaire du compte
	 * @param civiliteTitulaire Civilité du titaulaire du compte
	 * @param adresseStable une adresse (adresse stable de l'étudiant si titulaire ou adresse saisie si tier)
	 * @param typeFournisseur type de fournisseur
	 * @param moyenPrelevement le moyen de paiement de type prélèvement
	 * @return les information du titulaire
	 * @throws ImportException exception d'import
	 */
	public TitulaireInfos creerOuRecupererInfosTitulaire(EOEditingContext editingContext, PersonneApplicationUser user,
	    IIndividu individuTitulaire, Integer idPersonneTitulaire, ICivilite civiliteTitulaire, IAdresse adresseStable,
	    String typeFournisseur, IPaiementMoyen moyenPrelevement) throws ImportException {

		IAdresse adresseFacturationTitulaire = null;
		IFournis fournis = null;
		IRib rib = null;
		if (moyenPrelevement.etudiantTitulaire()) {
			adresseFacturationTitulaire = creerOuMiseAJourAdresseFacturation(individuTitulaire, adresseStable);
			fournis = creerOuMiseAJourFournisseur(editingContext, individuTitulaire, adresseFacturationTitulaire, typeFournisseur);
			rib = creerOuMiseAJourRib(editingContext, fournis, moyenPrelevement);
		} else {
			if (individuTitulaire == null) {
				individuTitulaire = creerOuRecupererIndividuTitulaire(editingContext, moyenPrelevement, idPersonneTitulaire, civiliteTitulaire, user);
			}
			adresseFacturationTitulaire = creerOuRecupererAdresseTitulaire(editingContext, individuTitulaire, adresseStable, user);
			fournis = creerOuMiseAJourFournisseur(editingContext, individuTitulaire, adresseFacturationTitulaire, typeFournisseur);
			rib = creerOuMiseAJourRib(editingContext, fournis, moyenPrelevement);
		}

		TitulaireInfos titulaireInfos = new TitulaireInfos(adresseFacturationTitulaire, rib, fournis);

		return titulaireInfos;
	}

	private IAdresse creerOuRecupererAdresseTitulaire(EOEditingContext editingContext, IIndividu individuTitulaire, IAdresse adresseTitulaireACopier,
	    PersonneApplicationUser user) {
		IAdresse adresseFacturation = individuTitulaire.adresseFacturation();
		if (adresseFacturation == null) {
			adresseFacturation = EOAdresse.creerInstance(editingContext);
			adresseFacturation.setPersIdCreation(user.getPersId());
			adresseFacturation.setPersIdModification(user.getPersId());
			adresseFacturation.setDCreation(new NSTimestamp());
			adresseFacturation.setDModification(new NSTimestamp());
			creerRepartAdresseFacturation(individuTitulaire, adresseFacturation, editingContext);
		}
		adresseTitulaireACopier.copierAdresseTo(adresseFacturation);
		return adresseFacturation;
	}

	private IIndividu creerOuRecupererIndividuTitulaire(EOEditingContext editingContext, IPaiementMoyen moyenPaiement, Integer idPersonneTitulaire,
	    ICivilite civiliteTitulaire, PersonneApplicationUser user) {
		EOIndividu individu = null;
		if (idPersonneTitulaire == null) {
			individu = EOIndividu.creerInstance(editingContext, new NSArray<ISpecificite>());
			if (user.iPersonne().isIndividu() && ((EOIndividu) user.iPersonne()).noIndividu() != null) {
				individu.setNoIndividuCreateur(((EOIndividu) user.iPersonne()).noIndividu());
			}
			individu.setPersIdModification(user.getPersId());
			individu.setDCreation(new NSTimestamp());
			individu.setDModification(new NSTimestamp());
			individu.setNomAffichage(moyenPaiement.nomTireur());
			individu.setPrenomAffichage(moyenPaiement.prenomTireur());
			individu.setToCiviliteRelationship(civiliteTitulaire);
		} else {
			individu = EOIndividu.fetchByQualifier(editingContext, EOIndividu.PERS_ID.eq(idPersonneTitulaire));
		}
		return individu;
	}

	private IIndividu creerOuMiseAJourIndividu(EOEditingContext editingContext, IPreEtudiant preEtudiant, PersonneApplicationUser user, String structureEtudiant,
	    Integer annee) throws ImportException {
		// On cree l'individu a partir des informations saisies
		EOIndividu individu = getIndividuPourPreEtudiant(editingContext, preEtudiant, user, true);
		individu.setPersIdModification(user.getPersId());
		individu.setNomAffichage(MyStringCtrl.initCap(preEtudiant.nomAffichage()));
		individu.setPrenomAffichage(MyStringCtrl.initCap(preEtudiant.prenomAffichage()));
		individu.setToCiviliteRelationship(preEtudiant.toCivilite());
		individu.setNomPatronymiqueAffichage(preEtudiant.nomPatronymique());
		individu.setNomUsuel(preEtudiant.nomUsuel());
		individu.setPrenom(preEtudiant.prenom());
		individu.setPrenom2(preEtudiant.prenom2());
		individu.setDNaissance(preEtudiant.dNaissance());
		individu.setVilleDeNaissance(preEtudiant.villeNaissance());
		if (preEtudiant.toDepartement() != null && preEtudiant.toDepartement().getCode() != null) {
			individu.setToDepartementRelationship(EODepartement.fetchByKeyValue(editingContext, EODepartement.C_DEPARTEMENT_KEY, preEtudiant.toDepartement()
			    .getCode()));
		}
		individu.setToPaysNaissanceRelationship(preEtudiant.toPaysNaissance());
		individu.setToPaysNationaliteRelationship(preEtudiant.toPaysNationalite());
		individu.setToSituationFamilialeRelationship(preEtudiant.toSituationFamiliale());
		if (preEtudiant.inseeProvisoire()) {
			individu.setIndNoInseeProv(preEtudiant.noInsee());
			individu.setIndCleInseeProv(preEtudiant.cleInsee());
			individu.setPriseCptInsee("P");
		} else {
			individu.setIndNoInsee(preEtudiant.noInsee());
			individu.setIndCleInsee(preEtudiant.cleInsee());
			individu.setPriseCptInsee("R");
		}
		individu.setIndQualite(EOIndividu.QUALITE_ETUDIANT);
		if (user.iPersonne().isIndividu() && ((EOIndividu) user.iPersonne()).noIndividu() != null) {
			individu.setNoIndividuCreateur(((EOIndividu) user.iPersonne()).noIndividu());
		}

		if (structureEtudiant != null) {
			NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
			qualifiers.addObject(new EOKeyValueQualifier(EOStructure.LC_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, structureEtudiant));
			qualifiers.addObject(new EOKeyValueQualifier(EOStructure.LL_STRUCTURE_KEY, EOQualifier.QualifierOperatorEqual, structureEtudiant));
			EOStructure structure = EOStructure.fetchFirstByQualifier(editingContext, new EOOrQualifier(qualifiers));
			if (structure != null) {
				EORepartStructure.creerInstanceSiNecessaire(editingContext, individu, structure, user.getPersId());
			}
		}

		// Photo (création, maj ou suppression)
		IPhoto prePhoto = preEtudiant.toPhoto();
		if (prePhoto != null) {
			IPhoto photo = individu.toPhoto();
			if (photo == null) {
				photo = EOPhotosEtudiantsGrhum.creerInstance(editingContext);
				photo.setNoIndividu(individu.noIndividu());
			}
			photo.setDatasPhoto(prePhoto.datasPhoto());
			photo.setDatePrise(prePhoto.datePrise());
		} else {
			IPhoto photo = individu.toPhoto();
			if (photo != null) {
				editingContext.deleteObject(photo);
			}
		}
		individu.setIndPhoto(preEtudiant.indPhoto());

		if (individu.toComptes().isEmpty()) {
			EOCompte compte = EOCompte.creerInstance(editingContext);
			String vlanEtudiant = EOGrhumParametres.parametrePourCle(editingContext, EOGrhumParametres.PARAM_GRHUM_VLAN_ETUD_KEY, EOVlans.VLAN_E);
			try {
				compte.initialiseCompte(editingContext, user, individu, vlanEtudiant, null, null, true, null, new NSTimestamp(), null);
				String email = EOCompteEmail.computeCanonicalEmailAdresseAvecDomaineFromPersonne(individu, editingContext, compte.toVlans());

				if (!MyStringCtrl.isEmpty(email)) {
					if (!MyStringCtrl.isEmailValid(email)) {
						throw new NSValidation.ValidationException("L'adresse email " + email + " n'est pas une adresse email valide.");
					}
					if (EOCompteEmail.isEmailAffecte(compte, email)) {
						throw new NSValidation.ValidationException("L'adresse email " + email + " est déjà affectée à ce compte.");
					}
					EOCompteEmail compteEmail = EOCompteEmail.creerInstance(editingContext);
					compteEmail.setToCompteRelationship(compte);
					compteEmail.setEmailFormatte(email);
				}

			} catch (Exception e) {
				throw new ImportException("Erreurs lors de la création du compte", e);
			}
		}
		renseigneCharteCompte(individu, (IPreEtudiantAnnee) preEtudiant.toPreEtudiantAnnee(annee), user);
		preEtudiant.setToIndividuEtudiantRelationship(individu);
		return (IIndividu) individu;
	}

	/**
	 * Pour un preEtudiant, va essayer de récupérer un individu existant le créer sinon
	 * @param editingContext Pour bosser
	 * @param preEtudiant le preEtudiant en question
	 * @param user le user courant
	 * @return
	 */
	protected EOIndividu getIndividuPourPreEtudiant(EOEditingContext editingContext, IPreEtudiant preEtudiant, PersonneApplicationUser user, Boolean peutCreer) {

		if (preEtudiant.toIndividuEtudiant() != null) {
			return (EOIndividu) preEtudiant.toIndividuEtudiant();
		}

		// On cherche un étudiant par son code
		if (preEtudiant.codeIne() != null) {
			EOEtudiant etudiant = EOEtudiant.fetchFirstByQualifier(editingContext, EOEtudiant.ETUD_CODE_INE.eq(preEtudiant.codeIne()));
			if (etudiant != null) {
				return etudiant.toIndividu();
			}
		}

		NSTimestamp dateNaissance = preEtudiant.dNaissance() != null ? new NSTimestamp(preEtudiant.dNaissance()) : null;

		EOQualifier individuQualifier = (EOIndividu.NOM_PATRONYMIQUE.likeInsensitive(preEtudiant.nomPatronymique()).or(EOIndividu.NOM_USUEL
		    .likeInsensitive(preEtudiant.nomUsuel()))).and(EOIndividu.PRENOM.likeInsensitive(preEtudiant.prenom())).and(
		    EOIndividu.D_NAISSANCE.eq(dateNaissance));

		EOIndividu individu = EOIndividu.fetchFirstByQualifier(editingContext, individuQualifier);

		if (individu != null) {
			return individu;
		} else {
			if (!preEtudiant.inseeProvisoire()) {
				individuQualifier = EOIndividu.IND_NO_INSEE.eq(preEtudiant.noInsee());
				individu = EOIndividu.fetchFirstByQualifier(editingContext, individuQualifier);
				if (individu != null) {
					return individu;
				}
			}
		}

		if (peutCreer) {
			individu = EOIndividu.creerInstance(editingContext, new NSArray<ISpecificite>());
			individu.setPersIdCreation(user.getPersId());
			individu.setDCreation(new NSTimestamp());

			return individu;
		} else {
			return null;
		}

	}

	IFournis creerOuMiseAJourFournisseur(EOEditingContext editingContext, IIndividu individu, IAdresse adresseFacturation, String typeFournisseur)
	    throws ImportException {
		EOFournis fournis = null;
		if (adresseFacturation != null) {
			if (individu == null) {
				throw new ImportException(String.format("Le preetudiant doit avoir un individu"));
			}

			fournis = (EOFournis) individu.toFournis();
			if (fournis == null) {
				fournis = EOFournis.creerInstance(editingContext);
				fournis.setToIndividu((EOIndividu) individu);
				fournis.setFouValide(EOFournis.FOU_VALIDE_OUI);
				try {
					fournis.initialise(userInfo.persId().intValue(), (EOAdresse) adresseFacturation, individu.isEtranger(), typeFournisseur);
				} catch (Exception e) {
					throw new ImportException("Une erreur est survenue lors de l'initialisation du fournisseur pour l'individu " + individu.nomAffichage(), e);
				}
			} else {
				fournis.setToAdresseRelationship((EOAdresse) adresseFacturation);
				fournis.ajouterFouType(typeFournisseur);
			}
		}
		return fournis;
	}

	IAdresse creerOuMiseAJourAdresseFacturation(IIndividu individu, IAdresse adresseStable) {
		IAdresse adresseFacturation = (EOAdresse) individu.adresseFacturation();
		EOEditingContext editingContext = ((EOEnterpriseObject) individu).editingContext();
		if (adresseFacturation == null) {
			creerRepartAdresseFacturation(individu, adresseStable, editingContext);
			adresseFacturation = adresseStable;
		} else {
			adresseFacturation.copierAdresseFrom(adresseStable);
		}
		return adresseFacturation;
	}

	private void creerRepartAdresseFacturation(IIndividu individu, IAdresse adresse, EOEditingContext editingContext) {
		EOTypeAdresse typeFacturation = EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_FACT);
		EORepartPersonneAdresse repart = EORepartPersonneAdresse.creerInstance(editingContext);
		repart.initForPersonne(editingContext, (IPersonne) individu, (EOAdresse) adresse, typeFacturation);
		repart.setRpaValide(EORepartPersonneAdresse.RPA_VALIDE_OUI);
	}

	IRib creerOuMiseAJourRib(EOEditingContext editingContext, IFournis fournis, IPaiementMoyen prelevement) throws ImportException {
		if (fournis == null) {
			throw new ImportException("Un fournisseur doit être renseigné");
		}
		if (!prelevement.verifierInfosMinimalesRib()) {
			throw new ImportException("Il manque les infos du RIB (iban, bic, titulaire)");
		}
		EORib rib = (EORib) fournis.rechercherRib(prelevement.iban());
		if (rib == null) {
			rib = EORib.creerInstance(editingContext);
			rib.setPersIdCreation(userInfo.persId().intValue());
			fournis.addToToRibsRelationship(rib);
			rib.setRibTitco(prelevement.nomTireur() + " " + prelevement.prenomTireur());
			rib.setIban(prelevement.iban());
			rib.setBic(prelevement.bic());
			rib.fillRibFRFromIban();
			rib.setPersIdModification(userInfo.persId().intValue());
		}
		return rib;
	}

	/**
	 * Renseigne les éléments de charte informatique du compte
	 * @param individu individu correspondant à l'étudiant
	 * @param preEtudiantAnnee les infos de l'étudiant sur l'année en cours
	 */
	private void renseigneCharteCompte(EOIndividu individu, IPreEtudiantAnnee preEtudiantAnnee, PersonneApplicationUser user) {
		if (individu.toComptes().count() == 1) {
			EOCompte compte = individu.toComptes().get(0);
			compte.setCptModificateur(user.getPersId().intValue());
			compte.setDModification(new NSTimestamp());
			if (preEtudiantAnnee.respectCharte() == Boolean.TRUE) {
				compte.setCptCharte(EOCompte.CPT_CHARTE_OUI);
				compte.setCptDateCharte(new NSTimestamp());
			} else {
				compte.setCptCharte(EOCompte.CPT_CHARTE_NON);
				compte.setCptDateCharte(null);
			}
			if (preEtudiantAnnee.accordAnnuaire() == Boolean.TRUE) {
				compte.setCptListeRouge(EOCompte.CPT_LISTE_ROUGE_NON);
			} else {
				compte.setCptListeRouge(EOCompte.CPT_LISTE_ROUGE_OUI);
			}
		}
	}

	/**
	 * 
	 * @param editingContext
	 * @param etudiant
	 * @param etudAnnee
	 * @param annee
	 * @return
	 */
	public IPreEtudiant devaliderEtudiantScolarite(EOEditingContext editingContext, IEtudiant etudiant, IScoEtudiantAnnee etudAnnee, Integer annee,
	    PersonneApplicationUser personneApplicationUser) {
		Boolean isOk = true;

		IPreEtudiant preEtudiant = importEtudiantDansPreEtudiant(editingContext, etudiant, etudiant.toIndividu(), annee, true);
		IPreEtudiantAnnee preEtudiantAnnee = importEtudiantAnneeDansPreEtudiantAnne(editingContext, preEtudiant, annee, true, etudAnnee, personneApplicationUser);
		isOk = suppressionDonneesPre(editingContext, preEtudiant, preEtudiantAnnee, annee);

		importBoursesDansPreBourses(editingContext, etudiant, preEtudiant, annee, true, personneApplicationUser);
		importCursusDansPreCursus(editingContext, etudiant, preEtudiant, true, personneApplicationUser);
		importAdressesDansPreAdresse(editingContext, etudiant.toIndividu(), preEtudiant, true, personneApplicationUser);
		importTelephonesDansPreTelephones(editingContext, etudiant.toIndividu(), preEtudiant, true, personneApplicationUser);
		importerPaiementsDansPrePaiement(editingContext, preEtudiantAnnee, etudAnnee);
		importInscriptionsDansPreInscriptions(editingContext, etudAnnee, preEtudiant, preEtudiantAnnee, annee, personneApplicationUser);

		isOk = suppressionInscription(editingContext, etudAnnee, annee);
		preEtudiantPaiementService.supprimerPaiements(editingContext, etudAnnee);
		suppressionEtudiantAnnee(editingContext, etudiant, etudAnnee);
		suppressionBourses(editingContext, etudiant, annee);
		if (annee.equals(etudiant.etudAnnee1inscUlr().intValue())) {
			suppressionCursus(editingContext, etudiant);
		}
		if (isOk) {
			return preEtudiant;
		}
		return null;

	}

	/**
	 * @param editingContext
	 * @param etudiant
	 */
	private void suppressionCursus(EOEditingContext editingContext,
	    IEtudiant etudiant) {
		NSArray<EOCursus> listeC = EOCursus.fetchSco_Cursuses(editingContext, new EOKeyValueQualifier(EOCursus.TO_ETUDIANT_KEY,
		    EOQualifier.QualifierOperatorEqual, etudiant), null);
		LinkedList<EOCursus> listeCursus = new LinkedList<EOCursus>(listeC);
		while (!listeCursus.isEmpty()) {
			EOCursus cursus = listeCursus.remove();
			cursus.setToEtudiantRelationship((IEtudiant) null);
			editingContext.deleteObject(cursus);
		}
	}

	/**
	 * @param editingContext
	 * @param etudiant
	 * @param annee
	 */
	private void suppressionBourses(EOEditingContext editingContext,
	    IEtudiant etudiant, Integer annee) {
		NSMutableArray<EOQualifier> qualifiersBourse = new NSMutableArray<EOQualifier>();
		qualifiersBourse.addObject(new EOKeyValueQualifier(EOBourses.TO_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, etudiant));
		qualifiersBourse.addObject(new EOKeyValueQualifier(EOBourses.ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));
		NSArray<EOBourses> listeB = EOBourses.fetchSco_Bourseses(editingContext, new EOAndQualifier(qualifiersBourse), null);
		LinkedList<EOBourses> listeBourses = new LinkedList<EOBourses>(listeB);
		while (!listeBourses.isEmpty()) {
			EOBourses bourse = listeBourses.remove();
			// bourse.setToEtudiantRelationship(null);
			bourse.setToEtudiant(null);
			editingContext.deleteObject(bourse);
		}
	}

	/**
	 * @param editingContext
	 * @param etudiant
	 * @param etudAnnee
	 */
	private void suppressionEtudiantAnnee(EOEditingContext editingContext,
	    IEtudiant etudiant, IScoEtudiantAnnee etudAnnee) {
		etudAnnee.setToActiviteProfessionnelleRelationship(null);
		etudAnnee.setToCotisationRelationship(null);
		etudAnnee.setToEtudiantRelationship(etudiant);
		etudAnnee.setToMutuelleRelationship(null);
		etudAnnee.setToOrigineRessourcesRelationship(null);
		etudAnnee.setToProfession2Relationship(null);
		etudAnnee.setToProfession1Relationship(null);
		etudAnnee.setToQuotiteTravailRelationship(null);
		etudAnnee.setToRneRelationship(null);
		etudAnnee.setToSituationFamilialeRelationship(null);
		etudAnnee.setToSituationProfessionnelleRelationship(null);
		etudAnnee.setToTypeHandicapRelationship(null);
		editingContext.deleteObject((EOEnterpriseObject) etudAnnee);
	}

	/**
	 * @param editingContext
	 * @param etudiantAnnee
	 * @param annee
	 */
	private boolean suppressionInscription(EOEditingContext editingContext, IScoEtudiantAnnee etudiantAnnee, Integer annee) {
		boolean isOk = true;

		NSArray<EOInscription> listeIa = (NSArray<EOInscription>) etudiantAnnee.toInscriptions();
		LinkedList<EOInscription> liste = new LinkedList<EOInscription>(listeIa);

		while (!liste.isEmpty()) {
			EOInscription inscription = liste.remove();
			isOk = suppressionIpsEtIpsElmtsPourInscription(editingContext, inscription);

			inscription.setToDiplomeRelationship(null);
			inscription.setToEtudiantRelationship(null);
			inscription.setToGradeUniversitaireRelationship(null);
			inscription.setToParcoursAnneeRelationship(null);
			inscription.setToParcoursDiplomeRelationship(null);
			inscription.setToTypeEchangeRelationship(null);
			inscription.setToTypeInscriptionFormationRelationship(null);
			inscription.setToRegimeInscriptionRelationship(null);
			inscription.setToEtablissementCumulatifRelationship(null);
			editingContext.deleteObject(inscription);
		}

		return isOk;
	}

	/**
	 * Suppression de toutes les ips de l'inscription donnée. On vérifie au préalable qu'aucune note n'est saisie avant de faire la suppression.
	 * 
	 * @param editingContext Un editing context
	 * @param inscription L'inscription pour laquelle les IPs sont à supprimer
	 * @return <code>true</code> si la suppression a été faite, false si une note était présente et empêche la suppression.
	 */
	public boolean suppressionIpsEtIpsElmtsPourInscription(EOEditingContext editingContext, IInscription inscription) {
		List<EOInscriptionPedagogique> ips = ((EOInscription) inscription).toInscriptionsPedagogiques();
		boolean hasNote = uneNoteEstSaisieSurUneIp(ips);
		if (hasNote) {
			return false;
		} else {
			for (EOInscriptionPedagogique ip : new ArrayList<EOInscriptionPedagogique>(ips)) {
				supprimerIpEtIpsElmts(editingContext, inscription, ip);
			}
		}
		return true;
	}

	private boolean uneNoteEstSaisieSurUneIp(List<EOInscriptionPedagogique> ips) {
		for (EOInscriptionPedagogique ip : ips) {
			for (EOInscriptionPedagogiqueElement elt : ip.toInscriptionPedagogiqueElements()) {
				if (elt.toNotes() != null && elt.toNotes().size() > 0) {
					return true;
				}
			}
		}
		return false;
	}

	private void supprimerIpEtIpsElmts(EOEditingContext editingContext, IInscription inscription, EOInscriptionPedagogique ip) {
		if (ip != null) {
			List<? extends IInscriptionPedagogiqueElement> listelmt = ip.toInscriptionPedagogiqueElements();
			LinkedList<IInscriptionPedagogiqueElement> listeIpElmt = new LinkedList<IInscriptionPedagogiqueElement>(listelmt);
			while (!listeIpElmt.isEmpty()) {
				IInscriptionPedagogiqueElement ipElmt = listeIpElmt.remove();
				if (ipElmt.toNotes() != null && ipElmt.toNotes().size() > 0) {
					throw new IllegalStateException("Il ne devrait pas y avoir de notes sur cette ipElt en cours de suppression ! " + ipElmt);
				} else {
					// on supprime l'etudiant des groupes etudiants
					List<IComposant> listeComposants = ipElmt.toLien().child().childs(EOTypeComposant.typeAP(editingContext));
					for (IComposant composant : listeComposants) {
						NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
						qualifiers.addObject(new EOKeyValueQualifier(EOGroupeEtudiant.TO_GROUPE.dot(EOGroupe.TO_COMPOSANT).key(), EOQualifier.QualifierOperatorEqual,
						    composant));
						qualifiers.addObject(new EOKeyValueQualifier(EOGroupeEtudiant.TO_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, inscription
						    .toEtudiantAnnee()));
						EOGroupeEtudiant groupeEtudiant = EOGroupeEtudiant.fetchSco_GroupeEtudiant(editingContext, new EOAndQualifier(qualifiers));
						if (groupeEtudiant != null) {
							editingContext.deleteObject(groupeEtudiant);
						}
					}
					ipElmt.setToInscriptionPedagogiqueRelationship(null);
					ipElmt.setToLienRelationship(null);
					ipElmt.setToTypeInscriptionPedaRelationship(null);
					editingContext.deleteObject((EOInscriptionPedagogiqueElement) ipElmt);
				}
			}
			ip.setToInscriptionRelationship(null);
			editingContext.deleteObject((EOInscriptionPedagogique) ip);
		}
	}

	private Boolean suppressionDonneesPre(EOEditingContext editingContext, IPreEtudiant preEtudiant, IPreEtudiantAnnee preEtudiantAnnee, Integer annee) {
		boolean isOk = true;
		if (preEtudiant.toPreBourses().size() > 0) {
			((EOPreEtudiant) preEtudiant).deleteAllToPreBoursesRelationships();
		}

		if (preEtudiant.toPreCursus().size() > 0) {
			((EOPreEtudiant) preEtudiant).deleteAllToPreCursusRelationships();
		}

		if (preEtudiant.toPreAdresses().size() > 0) {
			((EOPreEtudiant) preEtudiant).deleteAllToPreAdressesRelationships();
		}

		if (preEtudiant.toPreTelephones().size() > 0) {
			((EOPreEtudiant) preEtudiant).deleteAllToPreTelephonesRelationships();
		}

		preEtudiantPaiementService.supprimerPaiements(editingContext, preEtudiantAnnee);

		isOk = suppressionPreInscription(editingContext, preEtudiantAnnee, annee);
		return isOk;
	}

	/**
	 * @param editingContext
	 * @param etudiant
	 * @param annee
	 */
	private boolean suppressionPreInscription(EOEditingContext editingContext, IPreEtudiantAnnee preEtudiantAnnee, Integer annee) {
		boolean isOk = true;

		NSArray<EOPreInscription> listeIa = (NSArray<EOPreInscription>) preEtudiantAnnee.toInscriptions();
		LinkedList<EOPreInscription> liste = new LinkedList<EOPreInscription>(listeIa);

		while (!liste.isEmpty()) {
			EOPreInscription preInscription = liste.remove();
			preInscription.setToDiplomeRelationship(null);
			preInscription.setToPreEtudiantRelationship(null);
			preInscription.setToGradeUniversitaireRelationship(null);
			preInscription.setToParcoursAnneeRelationship(null);
			preInscription.setToParcoursDiplomeRelationship(null);
			preInscription.setToTypeEchangeRelationship(null);
			preInscription.setToTypeInscriptionFormationRelationship(null);
			preInscription.setToRegimeInscriptionRelationship(null);
			preInscription.setToEtablissementCumulatifRelationship(null);
			editingContext.deleteObject(preInscription);
		}

		return isOk;
	}

	public Boolean devaliderEtudiant(EOEditingContext editingContext, IPreEtudiant preEtudiant, Integer annee, PersonneApplicationUser personneApplicationUser) {
		EOEtudiant etudiant = (EOEtudiant) preEtudiant.toEtudiant();
		NSMutableArray<EOQualifier> qualifiersEtud = new NSMutableArray<EOQualifier>();
		qualifiersEtud.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.TO_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, etudiant));
		qualifiersEtud.addObject(new EOKeyValueQualifier(EOEtudiantAnnee.ANNEE_KEY, EOQualifier.QualifierOperatorEqual, annee));

		EOEtudiantAnnee etudAnnee = EOEtudiantAnnee.fetchRequiredSco_EtudiantAnnee(editingContext, new EOAndQualifier(qualifiersEtud));
		devaliderEtudiantScolarite(editingContext, etudiant, etudAnnee, annee, personneApplicationUser);
		return true;

	}

	private void importerPaiementsDansPrePaiement(EOEditingContext editingContext, IPreEtudiantAnnee preEtudiantAnnee, IScoEtudiantAnnee etudiantAnnee) {
		if (etudiantAnnee.toPaiementInitial() != null) {
			IScoPaiementMoyen moyenPrelevement = etudiantAnnee.toScoPaiementInitial().toScoPaiementMoyenPrelevement();
			TitulaireInfos titulaireInfos = null;
			if (moyenPrelevement != null) {
				titulaireInfos = new TitulaireInfos(moyenPrelevement.toAdresseTitulaire(), moyenPrelevement.toRibTitulaire(), moyenPrelevement.toFournisseurTitulaire());
			}
			for (IScoPaiement paiement : etudiantAnnee.toScoPaiements()) {
				IPrePaiement prePaiement = (IPrePaiement) EOUtilities.createAndInsertInstance(editingContext, EOPrePaiement.ENTITY_NAME);

				prePaiement.setToEtudiantAnneeRelationship(preEtudiantAnnee);
				prePaiement.setOrdre(paiement.ordre());
				prePaiement.setMontant(paiement.montant());
				prePaiement.setPaiementValide(paiement.paiementValide());
				prePaiement.setDatePaiement(paiement.datePaiement());

				for (IPaiementDetail detailPaiement : paiement.toPaiementDetails()) {
					importerDetailPaiementDansPrePaiementDetail(editingContext, detailPaiement, prePaiement);
				}

				Map<IScoPaiementMoyen, IPrePaiementMoyen> correspondanceMoyen2PreMoyen = new HashMap<IScoPaiementMoyen, IPrePaiementMoyen>();

				for (IScoPaiementMoyen moyen : paiement.toScoPaiementMoyens()) {
					IPrePaiementMoyen preMoyenPaiement = importerPaiementMoyenDansPreMoyenPaiement(editingContext, moyen, prePaiement, titulaireInfos);
					correspondanceMoyen2PreMoyen.put(moyen, preMoyenPaiement);
				}

				for (IScoPaiementMoyen moyen : paiement.toScoPaiementMoyens()) {
					importerPaiementEcheanceDansPreEcheancePaiement(editingContext, moyen, correspondanceMoyen2PreMoyen);
				}
			}
		}
	}

	private IEtudiant creerOuMiseAJourEtudiant(EOEditingContext editingContext, IPreEtudiant preEtudiant, IIndividu individu, Integer annee,
	    String codeRneEtablissement)
	    throws ImportException {

		Long sportHn = new Long(0);
		if (preEtudiant.sportHN() != null && preEtudiant.sportHN()) {
			sportHn = new Long(1);
		}

		EOEtudiant etudiant = getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, individu, sportHn, true);

		etudiant.setEtudCodeIne(preEtudiant.codeIne());
		etudiant.setToBacRelationship(preEtudiant.toBacs());
		if (preEtudiant.toRneEtabBac() != null) {
			etudiant.setToRneCodeBacRelationship(preEtudiant.toRneEtabBac());
		}
		if (preEtudiant.toMention() != null) {
			etudiant.setMentCode(preEtudiant.toMention().mentCode());
		}
		if (preEtudiant.toPreAdresses() != null) {
			for (EOPreAdresse adresse : preEtudiant.toPreAdresses()) {
				if (adresse.toTypeAdresse().equals(EOTypeAdresse.fetchByQualifier(editingContext, EOTypeAdresse.QUAL_TADR_CODE_PAR))) {
					if (EOCommune.fetchFirstByQualifier(editingContext, EOCommune.C_POSTAL.eq(adresse.codePostal()), null) != null) {
						etudiant.setToDepartementParentRelationship(EOCommune.fetchFirstByQualifier(editingContext, EOCommune.C_POSTAL.eq(adresse.codePostal()), null)
						    .toDepartement());
						break;
					}
				}
			}
		}

		EORne rneEtablissement = EORne.fetchByKeyValue(editingContext, EORne.C_RNE_KEY, codeRneEtablissement);
		if (rneEtablissement == null) {
			throw new ImportException(String.format("Le RNE de l'établissement (%s) n'a pas été trouvé dans la table des RNE", codeRneEtablissement));
		}

		Integer annee1InscUniv = preEtudiant.anne1InscUniv();
		Integer annee1InscSup = preEtudiant.annee1InscSup();
		Integer annee1InscEtab = preEtudiant.annee1InscEtab();

		etudiant.setEtudAnnee1inscUniv(annee1InscUniv == null ? null : new Long(annee1InscUniv));
		etudiant.setEtudAnnee1inscSup(annee1InscSup == null ? null : new Long(annee1InscSup));
		etudiant.setEtudAnnee1inscUlr(annee1InscEtab == null ? null : new Long(annee1InscEtab));
		etudiant.setToRneCodeSupRelationship((EORne) preEtudiant.toRneEtabSup());

		if (preEtudiant.candNumero() != null) {
			etudiant.setCandNumero(preEtudiant.candNumero());
		}

		if (preEtudiant.toProfession1() != null) {
			etudiant.setProCode(preEtudiant.toProfession1().proCode());
		}
		if (preEtudiant.toProfession2() != null) {
			etudiant.setProCode2(preEtudiant.toProfession2().proCode());
		}
		if (preEtudiant.anneeBac() != null) {
			etudiant.setEtudAnbac(new Long(preEtudiant.anneeBac()));
		}
		if (preEtudiant.toRneEtabBac() != null) {
			EORne rne = preEtudiant.toRneEtabBac();

			if (rne != null) {
				EODepartement leDepartement = rne.getDepartement();
				etudiant.setToDepartementEtabBacRelationship(leDepartement);
				etudiant.setToAcademieRelationship(rne.toAcademie());
			}
		}
		etudiant.setToPaysEtabBacRelationship(preEtudiant.toPaysEtabBac());
		etudiant.setEtudVilleBac(preEtudiant.villeBac());
		etudiant.setThebCode(preEtudiant.tHebCode());

		if (preEtudiant.snAttestation() != null) {
			etudiant.setEtudSnAttestation(new Long(preEtudiant.snAttestation()));
		} else {
			etudiant.setEtudSnAttestation(new Long(0));
		}
		if (preEtudiant.snCertification() != null) {
			etudiant.setEtudSnCertification(new Long(preEtudiant.snCertification()));
		} else {
			etudiant.setEtudSnCertification(new Long(0));
		}
		preEtudiant.setToEtudiantRelationship(etudiant);
		etudiant.setEtudReimmatriculation(preEtudiant.etudReimmatriculation());

		return etudiant;
	}

	public Integer getNumeroEtudiant(EOEditingContext editingContext, Integer annee) {
		NumeroEtudiantGenerateur etudiantGenerateur = new NumeroEtudiantGenerateurPrefixeSequence(editingContext, annee);
		Integer numEtud = null;
		try {
			numEtud = etudiantGenerateur.getNouveauNumero();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return numEtud;
	}

	/**
	 * Renvoi un etudiant correspondant au preEtudiant
	 * @param preEtudiant le preEtudiant en question
	 * @param editingContext pour bosser
	 * @param annee l'année d'inscription
	 * @param sitFamEtud la situation familiale
	 * @param individu l'individu
	 * @param sportHn témoin pour les sportifs de haut niveau
	 * @return l'étudiant
	 */
	protected EOEtudiant getEtudiantPourPreEtudiant(IPreEtudiant preEtudiant, EOEditingContext editingContext, Integer annee,
	    IIndividu individu, Long sportHn, Boolean peutCreer) {

		if (preEtudiant.toEtudiant() != null) {
			return (EOEtudiant) preEtudiant.toEtudiant();
		}

		// On cherche un étudiant par son code

		EOEtudiant etudiant = null;
		if (preEtudiant.codeIne() != null) {

			etudiant = EOEtudiant.fetchFirstByQualifier(editingContext, EOEtudiant.ETUD_CODE_INE.eq(preEtudiant.codeIne()));
			if (etudiant != null) {
				return etudiant;
			}

		}

		NSTimestamp dateNaissance = preEtudiant.dNaissance() != null ? new NSTimestamp(preEtudiant.dNaissance()) : null;

		EOQualifier individuQualifier = EOIndividu.NOM_PATRONYMIQUE.likeInsensitive(preEtudiant.nomPatronymique())
		    .and(EOIndividu.PRENOM.likeInsensitive(preEtudiant.prenom())).and(EOIndividu.D_NAISSANCE.eq(dateNaissance));

		EOQualifier etudiantQualifier = EOEtudiant.TO_INDIVIDU.prefix(individuQualifier);

		etudiant = EOEtudiant.fetchFirstByQualifier(editingContext, etudiantQualifier);

		if (etudiant != null) {
			return etudiant;
		}
		if (peutCreer) {
			etudiant = EOEtudiant.creerInstance(editingContext);
			etudiant.setEtudNumero(getNumeroEtudiant(editingContext, new Integer(annee)));
			etudiant.setDCreation(new NSTimestamp());
			etudiant.setDModification(new NSTimestamp());
			etudiant.setToIndividuRelationship((EOIndividu) individu);
			if (preEtudiant.annee1InscEtab() != null) {
				etudiant.setEtudAnnee1inscUlr(new Long(preEtudiant.annee1InscEtab()));
			} else {
				etudiant.setEtudAnnee1inscUlr(new Long(annee));
			}
			etudiant.setSituationFamilialeEtudiante(preEtudiant.enfantsCharge());
			etudiant.setEtudSportHn(sportHn);
			etudiant.setProCode(preEtudiant.toProfession1().proCode());

			return etudiant;
		} else {
			return null;
		}
	}

	private IAdresse creerOuMiseAJourAdresses(EOEditingContext editingContext, IIndividu individu, IPreEtudiant preEtudiant, PersonneApplicationUser user,
	    Integer annee) {
		IAdresse adresseStable = null;
		for (IPreAdresse preAdresse : preEtudiant.toPreAdresses()) {
			EOQualifier qualifier = ERXQ.and(new EOKeyValueQualifier(EORepartPersonneAdresse.TADR_CODE_KEY, EOQualifier.QualifierOperatorEqual, preAdresse
			    .toTypeAdresse().tadrCode()), EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_VALIDE);
			NSArray<EORepartPersonneAdresse> listeAdresse = ((EOIndividu) individu).toRepartPersonneAdresses(qualifier);
			EORepartPersonneAdresse repartPersonneAdresse;
			EOAdresse adresse;
			if (listeAdresse.size() > 0) {
				repartPersonneAdresse = listeAdresse.get(0);
				adresse = repartPersonneAdresse.toAdresse();
			} else {
				repartPersonneAdresse = EORepartPersonneAdresse.creerInstance(editingContext);
				repartPersonneAdresse.setDCreation(new NSTimestamp());
				adresse = EOAdresse.creerInstance(editingContext);
				adresse.setPersIdCreation(user.getPersId());
				adresse.setDCreation(new NSTimestamp());
				repartPersonneAdresse.initForPersonne(editingContext, (EOIndividu) individu, adresse, preAdresse.toTypeAdresse());
			}
			adresse.setAdrAdresse1(preAdresse.adrAdresse1());
			adresse.setAdrAdresse2(preAdresse.adrAdresse2());
			adresse.setAdrBp(preAdresse.boitePostale());
			adresse.setCodePostal(preAdresse.codePostal());
			adresse.setVille(preAdresse.ville());
			adresse.setToPaysRelationship(preAdresse.toPays());
			adresse.setCpEtranger(preAdresse.cpEtranger());
			adresse.setPersIdModification(user.getPersId());
			adresse.setDModification(new NSTimestamp());
			repartPersonneAdresse.setEMail(preAdresse.email());
			if (preAdresse.isAdresseStable()) {
				adresseStable = adresse;
			}
		}
		return adresseStable;
	}

	private IScoEtudiantAnnee creerOuMiseAJourEtudiantAnnee(EOEditingContext editingContext, IEtudiant etudiant, IPreEtudiant preEtudiant,
	    PersonneApplicationUser user, Integer annee) {
		IScoEtudiantAnnee etudiantAnnee = null;
		NSArray<EOPreEtudiantAnnee> listePreEtudiantAnne = ((EOPreEtudiant) preEtudiant).toPreEtudiantAnnee(ERXQ.equals(EOPreEtudiantAnnee.ANNEE_KEY, annee));
		if (listePreEtudiantAnne.size() > 0) {
			IPreEtudiantAnnee preEudAnne = listePreEtudiantAnne.get(0);

			// on cherche si etudiant année existe déjà
			EOQualifier qualifier = ERXQ.and(ERXQ.equals(EOEtudiantAnnee.ANNEE_KEY, annee), ERXQ.equals(EOEtudiantAnnee.TO_ETUDIANT_KEY, etudiant));
			etudiantAnnee = EOEtudiantAnnee.fetchSco_EtudiantAnnee(editingContext, qualifier);

			if (etudiantAnnee == null) {
				etudiantAnnee = EOEtudiantAnnee.createSco_EtudiantAnnee(editingContext, annee, new NSTimestamp(), 0, user.getPersId(), user.getPersId(),
				    true, (EOEtudiant) etudiant);
			}

			etudiantAnnee.setStatutBoursierCnous(preEudAnne.statutBoursierCnous());
			etudiantAnnee.setAutreCasExo(preEudAnne.autreCasExo());
			etudiantAnnee.setToOrigineRessourcesRelationship(preEudAnne.toOrigineRessources());
			etudiantAnnee.setTuteur1(preEudAnne.tuteur1());
			etudiantAnnee.setTuteur2(preEudAnne.tuteur2());
			etudiantAnnee.setToProfession1Relationship((EOProfession) preEudAnne.toProfession1());
			etudiantAnnee.setToProfession2Relationship((EOProfession) preEudAnne.toProfession2());
			etudiantAnnee.setToSituationFamilialeRelationship(preEudAnne.toSituationFamiliale());
			etudiantAnnee.setToSituationProfessionnelleRelationship(preEudAnne.toSituationProfessionnelle());
			etudiantAnnee.setToRneRelationship(preEudAnne.toRne());
			etudiantAnnee.setSalLibelle(preEudAnne.salLibelle());
			etudiantAnnee.setToQuotiteTravailRelationship(preEudAnne.toQuotiteTravail());
			etudiantAnnee.setToActiviteProfessionnelleRelationship(preEudAnne.toActiviteProfessionnelle());
			etudiantAnnee.setAffSs(preEudAnne.affSs());
			etudiantAnnee.setToMutuelleRelationship(preEudAnne.toMutuelle());
			etudiantAnnee.setAyantDroit(calculAyantDroit(preEudAnne));
			etudiantAnnee.setToRegimeParticulierRelationship(preEudAnne.toRegimeParticulier());
			etudiantAnnee.setTypeRegime(preEudAnne.typeRegime());
			etudiantAnnee.setSportUniv(preEudAnne.sportUniv());
			etudiantAnnee.setAdhMutuelle(preEudAnne.adhMutuelle());
			etudiantAnnee.setInterruptEtude(preEudAnne.interruptEtude());
			etudiantAnnee.setDureeInterrupt(preEudAnne.dureeInterrupt());
			etudiantAnnee.setDemandeurEmploi(preEudAnne.demandeurEmploi());
			etudiantAnnee.setFormationFinancee(preEudAnne.formationFinancee());
			etudiantAnnee.setEtudHandicap(preEudAnne.etudHandicap());
			etudiantAnnee.setToTypeHandicapRelationship(preEudAnne.toTypeHandicap());
			etudiantAnnee.setAssuranceCivile(preEudAnne.assuranceCivile());
			etudiantAnnee.setAssuranceCivileCie(preEudAnne.assuranceCivileCie());
			etudiantAnnee.setAssuranceCivileNum(preEudAnne.assuranceCivileNum());
			etudiantAnnee.setRecensement(preEudAnne.recensement());
			etudiantAnnee.setCertificatPartAppel(preEudAnne.certificatPartAppel());
			etudiantAnnee.setEtudSportHN(preEudAnne.etudSportHN());
			etudiantAnnee.setEnfantsCharge(preEudAnne.enfantsCharge());
			etudiantAnnee.setNbEnfantsCharge(preEudAnne.nbEnfantsCharge());
			etudiantAnnee.setToCotisationRelationship(preEudAnne.toCotisation());
			etudiantAnnee.setEmailParent(preEudAnne.emailParent());
			etudiantAnnee.setCommentaire(preEudAnne.commentaire());
			etudiantAnnee.setDModification(new NSTimestamp());
			etudiantAnnee.setPersIdModification(user.getPersId());
			etudiantAnnee.setRespectCharte(preEudAnne.respectCharte());
			etudiantAnnee.setAccordAnnuaire(preEudAnne.accordAnnuaire());
			etudiantAnnee.setAccordPhoto(preEudAnne.accordPhoto());
			etudiantAnnee.setToNiveauSportifRelationship(preEudAnne.toNiveauSportif());
			etudiantAnnee.setSportPratique(preEudAnne.sportPratique());
			etudiantAnnee.setEtudNoOrigine(preEudAnne.etudNoOrigine());
			etudiantAnnee.setValide(true);
			etudiantAnnee.setEtabBacEtranger(preEudAnne.etabBacEtranger());
		}

		return etudiantAnnee;
	}

	private Integer calculAyantDroit(IPreEtudiantAnnee preEtudiantAnnee) {
		CotisationRegimeSecuControle cotisationRegimeSecuControle = new CotisationRegimeSecuControle(preEtudiantAnnee.annee());
		return cotisationRegimeSecuControle.calculAyantDroit(preEtudiantAnnee);
	}

	private void creerOuMiseAJourTelephones(EOEditingContext editingContext, IIndividu individu, IPreEtudiant preEtudiant, PersonneApplicationUser user) {

		// On trie les n°, pour que le n° étudiant soit principal et son n° de portable encore plus (le premier de la liste sera le principal)
		NSMutableArray<EOPreTelephone> listeTelephonesPreEtudiant = preEtudiant.toPreTelephones().mutableClone();
		Collections.sort(listeTelephonesPreEtudiant, new EOPreTelephone.ComparatorEtudiantMobilePuisEtudiantPuisLesAutres());

		boolean premierTour = true;
		for (IPreTelephone preTelephone : listeTelephonesPreEtudiant) {
			// On ne peux pas modifier un n° de téléphone (n° en clé primaire)
			// On supprime tous ces téléphones de ce type et type pour le re-créer avec le nouveau n°
			EOQualifier qualifier = ERXQ.and(EOPersonneTelephone.TO_TYPE_NO_TEL.eq(preTelephone.toTypeNoTel()),
			    EOPersonneTelephone.TO_TYPE_TEL.eq(preTelephone.toTypeTels()));

			NSArray<EOPersonneTelephone> listeTel = ((EOIndividu) individu).toPersonneTelephones(qualifier);

			for (EOPersonneTelephone personneTelephone : listeTel) {
				personneTelephone.delete();
			}

			EOPersonneTelephone personneTelephone = EOPersonneTelephone.creerInstance(editingContext);
			personneTelephone.setToTypeTelRelationship(preTelephone.toTypeTels());
			personneTelephone.setToTypeNoTelRelationship(preTelephone.toTypeNoTel());
			if (premierTour) {
				personneTelephone.setTelPrincipal(EOPersonneTelephone.OUI);
			}
			personneTelephone.setDCreation(new NSTimestamp());
			personneTelephone.setToPersonneRelationship((EOIndividu) individu);
			personneTelephone.setIndicatif(preTelephone.indicatif());
			personneTelephone.setListeRouge("N");

			personneTelephone.setDModification(new NSTimestamp());
			personneTelephone.setIndicatif(preTelephone.indicatif());
			personneTelephone.setNoTelephone(preTelephone.noTelephone());

			premierTour = false;
		}
	}

	private void importPreCursusDansCursus(EOEditingContext editingContext, IEtudiant etudiant, IPreEtudiant preEtudiant, PersonneApplicationUser user) {

		suppressionCursus(editingContext, etudiant);

		EOQualifier qualifier = ERXQ.equals(EOPreCursus.TO_PRE_ETUDIANT_KEY, ((EOPreEtudiant) preEtudiant));
		NSArray<EOPreCursus> listePreCursus = EOPreCursus.fetchAll(editingContext, qualifier);
		for (IPreCursus preCursus : listePreCursus) {
			IScoCursus cursus = EOCursus
			    .createSco_Cursus(editingContext, preCursus.anneeDebut(), new NSTimestamp(), preCursus.interruptionEtud(), (EOEtudiant) etudiant);
			cursus.setAnneeFin(preCursus.anneeFin());
			cursus.setToCRneRelationship(preCursus.toCRne());
			cursus.setFormation(preCursus.formation());
			cursus.setNiveau(preCursus.niveau());
			cursus.setToGradeUniversitaireRelationship(preCursus.toGradeUniversitaire());
			cursus.setToTypeFormationRelationship(preCursus.toTypeFormation());
			cursus.setDiplome(preCursus.diplome());
			cursus.setToAccesTitreGradeUniversitaireRelationship(preCursus.toAccesTitreGradeUniversitaire());
			cursus.setToAccesMentionRelationship(preCursus.toAccesMention());
			cursus.setTypeInscription(preCursus.typeInscription());
			cursus.setDModifcation(new NSTimestamp());
			cursus.setPersIdCreation(user.getPersId());
			cursus.setPersIdMofication(user.getPersId());
			cursus.setToPaysRelationship(preCursus.toPays());
			cursus.setVille(preCursus.ville());
			cursus.setEtabEtranger(preCursus.etabEtranger());
			cursus.setAccesAnnee(preCursus.accesAnnee());
		}
	}

	private void importPreBoursesDansBourses(EOEditingContext editingContext, IEtudiant etudiant, IPreEtudiant preEtudiant, Integer annee,
	    PersonneApplicationUser user) {

		suppressionBourses(editingContext, etudiant, annee);

		EOQualifier qualifier = ERXQ.and(EOPreBourses.TO_PRE_ETUDIANT.eq((EOPreEtudiant) preEtudiant), EOPreBourses.ANNEE.eq(annee));
		NSArray<EOPreBourses> listePreBourses = EOPreBourses.fetchAll(editingContext, qualifier);
		for (IPreBourses preBourse : listePreBourses) {
			IScoBourses bourse = EOBourses.createSco_Bourses(editingContext, annee, new NSTimestamp(), (EOEtudiant) etudiant);
			bourse.setOrganisme(preBourse.organisme());
			bourse.setNumAllocataire(preBourse.numAllocataire());
			bourse.setToEchelonBourseRelationship(preBourse.toEchelonBourse());
			bourse.setToTypeInscriptionFormationRelationship(preBourse.toTypeInscriptionFormation());
			bourse.setIsCampusFrance(preBourse.isCampusFrance());
			if (preBourse.toTypeBoursesCF() != null) {
				bourse.setToTypeBoursesCFRelationship(preBourse.toTypeBoursesCF());
			}
			bourse.setDMofication(preBourse.dMofication());
			bourse.setPersIdCreation(user.getPersId());
			bourse.setPersIdModification(user.getPersId());
		}
	}

	private EOPreEtudiant importEtudiantDansPreEtudiant(EOEditingContext editingContext, IEtudiant etudiant, IIndividu individu, Integer annee, boolean isMaj) {
		EOPreEtudiant preEtudiant = null;
		if (etudiant != null) {
			preEtudiant = EOPreEtudiant.fetchByKeyValue(editingContext, EOPreEtudiant.ETUD_NUMERO_KEY, etudiant.etudNumero());
		}
		if (preEtudiant == null || isMaj) {
			if (preEtudiant == null) {
				preEtudiant = EOPreEtudiant.creer(editingContext, individu.persId());
				preEtudiant.setTypeInscription("R");
				preEtudiant.setPreType("R");
			}
			if (etudiant != null && etudiant.etudAnnee1inscUniv() != null) {
				preEtudiant.setAnne1InscUniv(etudiant.etudAnnee1inscUniv().intValue());
			} else {
				preEtudiant.setAnne1InscUniv(null);
			}
			if (etudiant != null && etudiant.etudAnnee1inscUlr() != null) {
				preEtudiant.setAnnee1InscEtab(etudiant.etudAnnee1inscUlr().intValue());
				if (isMaj && annee == etudiant.etudAnnee1inscUlr().intValue()) {
					preEtudiant.setTypeInscription("P");
					preEtudiant.setPreType("P");

				}
			} else {
				preEtudiant.setAnnee1InscEtab(null);

			}
			if (etudiant != null && etudiant.etudAnnee1inscSup() != null) {
				preEtudiant.setAnnee1InscSup(etudiant.etudAnnee1inscSup().intValue());
			} else {
				preEtudiant.setAnnee1InscSup(null);
			}
			if (etudiant != null && etudiant.etudCodeIne() != null) {
				preEtudiant.setCodeIne(etudiant.etudCodeIne());
				preEtudiant.setIneProvisoire(false);
			} else {
				preEtudiant.setIneProvisoire(true);
			}
			preEtudiant.setToCiviliteRelationship(individu.getCivilite());
			preEtudiant.setNomUsuel(individu.nomUsuel());
			preEtudiant.setNomPatronymique(individu.nomPatronymique());
			preEtudiant.setPrenom(individu.prenom());
			preEtudiant.setPrenom2(individu.prenom2());
			preEtudiant.setDNaissance((NSTimestamp) individu.dNaissance());
			preEtudiant.setToDepartementRelationship(individu.toDepartement());
			preEtudiant.setVilleNaissance(individu.villeDeNaissance());
			preEtudiant.setToPaysNaissanceRelationship(individu.toPaysNaissance());
			preEtudiant.setToPaysNationaliteRelationship(individu.toPaysNationalite());
			if (etudiant != null && etudiant.candNumero() != null) {
				preEtudiant.setCandNumero(etudiant.candNumero().intValue());
			}
			if (etudiant != null && etudiant.toDepartementParent() != null) {
				preEtudiant.setCDepartementParent(etudiant.toDepartementParent().cDepartement());
			}
			preEtudiant.setDNaturalisation((NSTimestamp) individu.getDtNaturalisation());
			if (individu.toSituationMilitaire() != null) {
				preEtudiant.setSitMilitaire(individu.toSituationMilitaire().cSitMilitaire());
			}
			preEtudiant.setToSituationFamilialeRelationship(individu.toSituationFamiliale());

			if (etudiant != null && etudiant.etudSitfam() != null && (etudiant.etudSitfam().intValue() == 3 || etudiant.etudSitfam().intValue() == 4)) {
				preEtudiant.setEnfantsCharge(true);
			} else {
				preEtudiant.setEnfantsCharge(false);
			}
			if (individu.getCodeInsee() != null) {
				preEtudiant.setNoInsee(individu.getCodeInsee().getCode());
				preEtudiant.setCleInsee(individu.getCleInsee());
				preEtudiant.setInseeProvisoire(false);
			} else if (individu.getCodeInseeProv() != null) {
				preEtudiant.setNoInsee(individu.getCodeInseeProv().getCode());
				preEtudiant.setCleInsee(individu.getCleInseeProv());
				preEtudiant.setInseeProvisoire(true);
			} else {
				preEtudiant.setInseeProvisoire(true);
			}
			if (etudiant != null && etudiant.toAcademie() != null) {
				preEtudiant.setAcadCodeBac(etudiant.toAcademie().cAcademie());
			}
			if (etudiant != null) {
				preEtudiant.setToRneEtabBacRelationship(etudiant.toRneCodeBac());
				if (etudiant.etudAnbac() != null) {
					preEtudiant.setAnneeBac(etudiant.etudAnbac().intValue());
				}
				preEtudiant.setToBacsRelationship(etudiant.toBac());
				preEtudiant.setToMentionRelationship(etudiant.toMention());
				preEtudiant.setToProfession1Relationship(EOProfession.fetchByKeyValue(editingContext, EOProfession.PRO_CODE_KEY, etudiant.proCode()));
				preEtudiant.setToProfession2Relationship(EOProfession.fetchByKeyValue(editingContext, EOProfession.PRO_CODE_KEY, etudiant.proCode2()));
				if (etudiant.etudSportHn().intValue() == 1) {
					preEtudiant.setSportHN(true);
				} else {
					preEtudiant.setSportHN(false);
				}
				preEtudiant.setToRneEtabSupRelationship(etudiant.toRneCodeSup());
				if (etudiant.toDepartementEtabBac() != null) {
					preEtudiant.setDepartementEtabBac(etudiant.toDepartementEtabBac().cDepartement());
				}
				preEtudiant.setToPaysEtabBacRelationship(etudiant.toPaysEtabBac());
				preEtudiant.setVilleBac(etudiant.etudVilleBac());
				preEtudiant.setReimmatriculation(etudiant.etudReimmatriculation());
				preEtudiant.setTHebCode(etudiant.thebCode());
				if (etudiant.etudSnAttestation() != null) {
					preEtudiant.setSnAttestation(etudiant.etudSnAttestation().intValue());
				}
				if (etudiant.etudSnCertification() != null) {
					preEtudiant.setSnCertification(etudiant.etudSnCertification().intValue());
				}
				preEtudiant.setToEtudiantRelationship(etudiant);
			}
			preEtudiant.setToIndividuEtudiantRelationship(individu);
			preEtudiant.setIndPhoto(individu.indPhoto());

			if (individu.toPhoto() != null) {
				IPhoto photo = individu.toPhoto();
				EOPrePhoto prePhoto = EOPrePhoto.createEOPrePhoto(editingContext, preEtudiant);
				prePhoto.setDatasPhoto(photo.datasPhoto());
				prePhoto.setDatePrise(photo.datePrise());
			}
		}

		return preEtudiant;

	}

	@SuppressWarnings("unused")
	private IPreEtudiantAnnee importEtudiantAnneeDansPreEtudiantAnne(EOEditingContext editingContext, IPreEtudiant preEtudiant, Integer annee, Boolean isMaj,
	    IScoEtudiantAnnee etudAnnee, PersonneApplicationUser personneApplicationUser) {
		IPreEtudiantAnnee preEtudiantAnnee = null;
		IScoEtudiantAnnee etudiantAnnee = null;
		NSArray<EOPreEtudiantAnnee> listePreEtudiantAnnee = ((EOPreEtudiant) preEtudiant).toPreEtudiantAnnee(ERXQ.equals(EOPreEtudiantAnnee.ANNEE_KEY, annee));
		if (listePreEtudiantAnnee.size() > 0) {
			preEtudiantAnnee = listePreEtudiantAnnee.get(0);
		}
		if (preEtudiantAnnee == null || isMaj) {
			if (preEtudiantAnnee == null) {
				preEtudiantAnnee = EOPreEtudiantAnnee.creer(editingContext, preEtudiant.persId(), (EOPreEtudiant) preEtudiant, annee);
				EOQualifier qualifierEtud = ERXQ.and(ERXQ.equals(EOEtudiantAnnee.ANNEE_KEY, (annee - 1)), ERXQ.equals(EOEtudiantAnnee.TO_ETUDIANT_KEY, preEtudiant));
				etudiantAnnee = EOEtudiantAnnee.fetchSco_EtudiantAnnee(editingContext, qualifierEtud);
			} else {
				etudiantAnnee = etudAnnee;
			}
			if (etudiantAnnee != null) {
				preEtudiantAnnee.setStatutBoursierCnous(etudiantAnnee.statutBoursierCnous());
				preEtudiantAnnee.setAutreCasExo(etudiantAnnee.autreCasExo());
				preEtudiantAnnee.setToOrigineRessourcesRelationship(etudiantAnnee.toOrigineRessources());
				preEtudiantAnnee.setTuteur1(etudiantAnnee.tuteur1());
				preEtudiantAnnee.setTuteur2(etudiantAnnee.tuteur2());
				preEtudiantAnnee.setToProfession1Relationship((EOProfession) etudiantAnnee.toProfession1());
				preEtudiantAnnee.setToProfession2Relationship((EOProfession) etudiantAnnee.toProfession2());
				preEtudiantAnnee.setToSituationFamilialeRelationship(etudiantAnnee.toSituationFamiliale());
				preEtudiantAnnee.setToSituationProfessionnelleRelationship(etudiantAnnee.toSituationProfessionnelle());
				preEtudiantAnnee.setToRneRelationship(etudiantAnnee.toRne());
				preEtudiantAnnee.setSalLibelle(etudiantAnnee.salLibelle());
				preEtudiantAnnee.setToQuotiteTravailRelationship(etudiantAnnee.toQuotiteTravail());
				preEtudiantAnnee.setToActiviteProfessionnelleRelationship(etudiantAnnee.toActiviteProfessionnelle());
				preEtudiantAnnee.setAffSs(etudiantAnnee.affSs());
				preEtudiantAnnee.setToMutuelleRelationship(etudiantAnnee.toMutuelle());
				preEtudiantAnnee.setToRegimeParticulierRelationship(etudiantAnnee.toRegimeParticulier());
				preEtudiantAnnee.setTypeRegime(etudiantAnnee.typeRegime());
				preEtudiantAnnee.setSportUniv(etudiantAnnee.sportUniv());
				preEtudiantAnnee.setAdhMutuelle(etudiantAnnee.adhMutuelle());
				preEtudiantAnnee.setInterruptEtude(etudiantAnnee.interruptEtude());
				preEtudiantAnnee.setDureeInterrupt(etudiantAnnee.dureeInterrupt());
				preEtudiantAnnee.setDemandeurEmploi(etudiantAnnee.demandeurEmploi());
				preEtudiantAnnee.setFormationFinancee(etudiantAnnee.formationFinancee());
				preEtudiantAnnee.setEtudHandicap(etudiantAnnee.etudHandicap());
				preEtudiantAnnee.setToTypeHandicapRelationship(etudiantAnnee.toTypeHandicap());
				preEtudiantAnnee.setAssuranceCivile(etudiantAnnee.assuranceCivile());
				preEtudiantAnnee.setAssuranceCivileCie(etudiantAnnee.assuranceCivileCie());
				preEtudiantAnnee.setAssuranceCivileNum(etudiantAnnee.assuranceCivileNum());
				preEtudiantAnnee.setRecensement(etudiantAnnee.recensement());
				preEtudiantAnnee.setCertificatPartAppel(etudiantAnnee.certificatPartAppel());
				preEtudiantAnnee.setEtudSportHN(etudiantAnnee.etudSportHN());
				preEtudiantAnnee.setEnfantsCharge(etudiantAnnee.enfantsCharge());
				preEtudiantAnnee.setNbEnfantsCharge(etudiantAnnee.nbEnfantsCharge());
				preEtudiantAnnee.setToCotisationRelationship(etudiantAnnee.toCotisation());
				preEtudiantAnnee.setEmailParent(etudiantAnnee.emailParent());
				preEtudiantAnnee.setCommentaire(etudiantAnnee.commentaire());
				preEtudiantAnnee.setDModification(new NSTimestamp());
				preEtudiantAnnee.setRespectCharte(etudiantAnnee.respectCharte());
				preEtudiantAnnee.setAccordAnnuaire(etudiantAnnee.accordAnnuaire());
				preEtudiantAnnee.setAccordPhoto(etudiantAnnee.accordPhoto());
				preEtudiantAnnee.setToNiveauSportifRelationship(etudiantAnnee.toNiveauSportif());
				preEtudiantAnnee.setSportPratique(etudiantAnnee.sportPratique());
				preEtudiantAnnee.setEtudNoOrigine(etudiantAnnee.etudNoOrigine());
				preEtudiantAnnee.setDModification(new NSTimestamp());
				if (isMaj) {
					preEtudiantAnnee.setPersIdModification(personneApplicationUser.getPersId());

				} else {
					preEtudiantAnnee.setPersIdModification(preEtudiant.persId());
				}
				preEtudiantAnnee.setEtabBacEtranger(etudAnnee.etabBacEtranger());
			}
		}

		return preEtudiantAnnee;
	}

	private void importBoursesDansPreBourses(EOEditingContext editingContext, IEtudiant etudiant, IPreEtudiant preEtudiant, Integer annee, boolean isMaj,
	    PersonneApplicationUser personneApplicationUser) {
		EOQualifier qualifier = ERXQ.and(EOPreBourses.TO_PRE_ETUDIANT.eq((EOPreEtudiant) preEtudiant), EOBourses.ANNEE.eq(annee));
		NSArray<EOPreBourses> listePreBourses = EOPreBourses.fetchAll(editingContext, qualifier, null);

		if (etudiant != null && (listePreBourses.size() == 0 || isMaj)) {
			EOQualifier qualifierBourse;
			if (isMaj) {
				qualifierBourse = ERXQ.and(EOBourses.TO_ETUDIANT.eq((EOEtudiant) etudiant), EOBourses.ANNEE.eq(annee));
			} else {
				qualifierBourse = ERXQ.and(EOBourses.TO_ETUDIANT.eq((EOEtudiant) etudiant), EOBourses.ANNEE.eq(annee - 1));
			}

			NSArray<EOBourses> listeBourses = EOBourses.fetchSco_Bourseses(editingContext, qualifierBourse, null);
			for (IScoBourses bourse : listeBourses) {
				IPreBourses preBourse = EOPreBourses.creer(editingContext, preEtudiant.persId());
				preBourse.setAnnee(annee);
				preBourse.setToPreEtudiantRelationship((EOPreEtudiant) preEtudiant);
				preBourse.setOrganisme(bourse.organisme());
				preBourse.setNumAllocataire(bourse.numAllocataire());
				preBourse.setToEchelonBourseRelationship(bourse.toEchelonBourse());
				preBourse.setToTypeInscriptionFormationRelationship(bourse.toTypeInscriptionFormation());
				preBourse.setIsCampusFrance(bourse.isCampusFrance());
				if (bourse.toTypeBoursesCF() != null) {
					preBourse.setToTypeBoursesCFRelationship(bourse.toTypeBoursesCF());
				}
				preBourse.setDMofication(bourse.dMofication());
				if (isMaj) {
					preBourse.setPersIdCreation(personneApplicationUser.getPersId());
					preBourse.setPersIdModification(personneApplicationUser.getPersId());

				} else {
					preBourse.setPersIdCreation(preEtudiant.persId());
					preBourse.setPersIdModification(preEtudiant.persId());
				}
			}
		}
	}

	private void importInscriptionsDansPreInscriptions(EOEditingContext editingContext, IScoEtudiantAnnee etudiantAnnee,
	    IPreEtudiant preEtudiant, IPreEtudiantAnnee preEtudiantAnnee, Integer annee, PersonneApplicationUser personneApplicationUser) {
		List<IScoInscription> listeInscription = (List<IScoInscription>) etudiantAnnee.toInscriptions();
		for (IScoInscription scoInscription : listeInscription) {
			EOPreInscription preInscription = EOPreInscription.createEOPreInscription(editingContext, annee, new NSTimestamp(),
			    (EODiplome) scoInscription.toDiplome(), (EOPreEtudiantAnnee) preEtudiantAnnee, (EOPreEtudiant) preEtudiant);

			preInscription.setDModification(new NSTimestamp());

			preInscription.setCycleAmenage(scoInscription.cycleAmenage());
			preInscription.setNiveau(scoInscription.niveau());
			preInscription.setPassageConditionnel(scoInscription.passageConditionnel());
			preInscription.setPersIdCreation(personneApplicationUser.getPersId());
			preInscription.setPersIdModification(personneApplicationUser.getPersId());
			preInscription.setRedoublement(scoInscription.redoublement());
			preInscription.setReorientation(scoInscription.reorientation());

			preInscription.setToGradeUniversitaireRelationship((EOGradeUniversitaire) scoInscription.toGradeUniversitaire());
			preInscription.setToTypeEchangeRelationship((EOTypeEchange) scoInscription.toTypeEchange());
			preInscription.setToTypeInscriptionFormationRelationship((EOTypeInscriptionFormation) scoInscription.toTypeInscriptionFormation());
			preInscription.setToParcoursDiplomeRelationship(scoInscription.toParcoursDiplome());
			preInscription.setToParcoursAnneeRelationship(scoInscription.toParcoursAnnee());
			preInscription.setIdLienConsultationDiplome(scoInscription.idLienConsultationDiplome());
			preInscription.setIdLienConsultationParcoursDiplome(scoInscription.idLienConsultationParcoursDiplome());
			preInscription.setIdLienConsultationAnnee(scoInscription.idLienConsultationAnnee());
			preInscription.setIdLienConsultationParcoursAnnee(scoInscription.idLienConsultationParcoursAnnee());
			if (scoInscription.toRegimeInscription() != null) {
				preInscription.setToRegimeInscriptionRelationship((EORegimeInscription) scoInscription.toRegimeInscription());
			}
			if (scoInscription.toEtablissementCumulatif() != null) {
				preInscription.setToEtablissementCumulatifRelationship(scoInscription.toEtablissementCumulatif());
			}

		}
	}

	private void importCursusDansPreCursus(EOEditingContext editingContext, IEtudiant etudiant, IPreEtudiant preEtudiant, boolean isMaj,
	    PersonneApplicationUser personneApplicationUser) {
		NSArray<EOPreCursus> listePreCursus = EOPreCursus.fetchAll(editingContext, EOPreCursus.TO_PRE_ETUDIANT.eq((EOPreEtudiant) preEtudiant), null);
		if (etudiant != null && (listePreCursus.size() == 0 || isMaj)) {
			NSArray<EOCursus> listeCursus = EOCursus.fetchSco_Cursuses(editingContext, EOCursus.TO_ETUDIANT.eq((EOEtudiant) etudiant), null);
			for (IScoCursus cursus : listeCursus) {
				IPreCursus preCursus = EOPreCursus.createEOPreCursus(editingContext, cursus.anneeDebut(), new NSTimestamp(), cursus.interruptionEtud(),
				    (EOPreEtudiant) preEtudiant);
				preCursus.setAnneeFin(cursus.anneeFin());
				preCursus.setToCRneRelationship(cursus.toCRne());
				preCursus.setFormation(cursus.formation());
				preCursus.setNiveau(cursus.niveau());
				preCursus.setToGradeUniversitaireRelationship(cursus.toGradeUniversitaire());
				preCursus.setToTypeFormationRelationship(cursus.toTypeFormation());
				preCursus.setDiplome(cursus.diplome());
				preCursus.setToAccesTitreGradeUniversitaireRelationship(cursus.toAccesTitreGradeUniversitaire());
				preCursus.setToAccesMentionRelationship(cursus.toAccesMention());
				preCursus.setTypeInscription(cursus.typeInscription());
				preCursus.setDModifcation(new NSTimestamp());
				if (isMaj) {
					preCursus.setPersIdCreation(personneApplicationUser.getPersId());
					preCursus.setPersIdMofication(personneApplicationUser.getPersId());

				} else {
					preCursus.setPersIdCreation(preEtudiant.persId());
					preCursus.setPersIdMofication(preEtudiant.persId());
				}
				preCursus.setToPaysRelationship(cursus.toPays());
				preCursus.setVille(cursus.ville());
				preCursus.setEtabEtranger(cursus.etabEtranger());
				preCursus.setAccesAnnee(cursus.accesAnnee());
			}
		}
	}

	private void importAdressesDansPreAdresse(EOEditingContext editingContext, IIndividu individu, IPreEtudiant preEtudiant, boolean isMaj,
	    PersonneApplicationUser personneApplicationUser) {
		if (preEtudiant.toPreAdresses().size() == 0) {
			NSArray<EORepartPersonneAdresse> listeAdresse = ((EOIndividu) individu).toRepartPersonneAdresses();
			EOAdresse adresse;
			for (EORepartPersonneAdresse repartPersonneAdresse : listeAdresse) {
				adresse = repartPersonneAdresse.toAdresse();
				IPreAdresse preAdresse = EOPreAdresse.createEOPreAdresse(editingContext, new NSTimestamp(), repartPersonneAdresse.toTypeAdresse());
				preAdresse.setToPreEtudiantRelationship(preEtudiant);
				preAdresse.setDCreation(new NSTimestamp());
				preAdresse.setAdrAdresse1(adresse.adrAdresse1());
				preAdresse.setAdrAdresse2(adresse.adrAdresse2());
				preAdresse.setBoitePostale(adresse.adrBp());
				preAdresse.setCodePostal(adresse.codePostal());
				preAdresse.setVille(adresse.ville());
				preAdresse.setToPaysRelationship(adresse.toPays());
				preAdresse.setCpEtranger(adresse.cpEtranger());
				if (isMaj) {
					preAdresse.setPersIdCreation(personneApplicationUser.getPersId());
					preAdresse.setPersIdModification(personneApplicationUser.getPersId());
				} else {
					preAdresse.setPersIdCreation(preEtudiant.persId());
					preAdresse.setPersIdModification(preEtudiant.persId());
				}
				preAdresse.setDModification(new NSTimestamp());
				preAdresse.setEmail(repartPersonneAdresse.eMail());
			}
		}
	}

	private void importTelephonesDansPreTelephones(EOEditingContext editingContext, IIndividu individu, IPreEtudiant preEtudiant, boolean isMaj,
	    PersonneApplicationUser personneApplicationUser) {
		if (preEtudiant.toPreTelephones().size() == 0) {
			NSArray<EOPersonneTelephone> listeTel = ((EOIndividu) individu).toPersonneTelephones();
			for (EOPersonneTelephone personneTelephone : listeTel) {
				IPreTelephone preTelephone = EOPreTelephone.createEOPreTelephone(editingContext, new NSTimestamp(), personneTelephone.noTelephone(),
				    (EOPreEtudiant) preEtudiant, personneTelephone.toTypeNoTel(), personneTelephone.toTypeTel());
				preTelephone.setIndicatif(personneTelephone.indicatif());
				preTelephone.setDModification(new NSTimestamp());
				if (isMaj) {
					preTelephone.setPersIdCreation(personneApplicationUser.getPersId());
					preTelephone.setPersIdModifcation(personneApplicationUser.getPersId());

				} else {
					preTelephone.setPersIdCreation(preEtudiant.persId());
					preTelephone.setPersIdModifcation(preEtudiant.persId());
				}
			}
		}
	}

	/**
	 * Importer les données préPaiement dans paiement.
	 * @param editingContext un editing contexte
	 * @param preEtudiantAnnee un pré-étudiant année
	 * @param etudiantAnnee un étudiant année
	 * @param titulaireInfos les informations du tituaire du compte
	 */
	public void importerPrePaiementDansPaiement(EOEditingContext editingContext, IPreEtudiantAnnee preEtudiantAnnee, IScoEtudiantAnnee etudiantAnnee,
	    TitulaireInfos titulaireInfos) {

		preEtudiantPaiementService.supprimerPaiements(editingContext, etudiantAnnee);

		for (IPrePaiement prePaiement : preEtudiantAnnee.toPrePaiements()) {
			IScoPaiement paiement = (IScoPaiement) EOUtilities.createAndInsertInstance(editingContext, EOPaiement.ENTITY_NAME);

			paiement.setToEtudiantAnneeRelationship(etudiantAnnee);
			paiement.setOrdre(prePaiement.ordre());
			paiement.setMontant(prePaiement.montant());
			paiement.setPaiementValide(prePaiement.paiementValide());
			paiement.setDatePaiement(prePaiement.datePaiement());

			for (IPrePaiementDetail preDetailPaiement : prePaiement.toPrePaiementDetails()) {
				importerPreDetailPaiementDansPaiementDetail(editingContext, preDetailPaiement, paiement);
			}

			Map<IPrePaiementMoyen, IScoPaiementMoyen> correspondancePreMoyen2Moyen = new HashMap<IPrePaiementMoyen, IScoPaiementMoyen>();

			for (IPrePaiementMoyen preMoyenPaiement : prePaiement.toPrePaiementMoyens()) {
				IScoPaiementMoyen moyen = importerPreMoyenPaiementDansPaiementMoyen(editingContext, preMoyenPaiement, paiement, titulaireInfos);
				correspondancePreMoyen2Moyen.put(preMoyenPaiement, moyen);
			}

			for (IPrePaiementMoyen preMoyenPaiement : prePaiement.toPrePaiementMoyens()) {
				importerPreEcheancePaiementDansPaiementEcheance(editingContext, preMoyenPaiement, correspondancePreMoyen2Moyen);
			}
		}
	}

	private void importerPreDetailPaiementDansPaiementDetail(EOEditingContext editingContext, IPrePaiementDetail preDetailPaiement, IScoPaiement paiement) {
		IScoPaiementDetail paiementDetail = (IScoPaiementDetail) EOUtilities.createAndInsertInstance(editingContext, EOPaiementDetail.ENTITY_NAME);

		paiementDetail.setToPaiementRelationship(paiement);
		paiementDetail.setDatePaye(preDetailPaiement.datePaye());
		paiementDetail.setDateRembourse(preDetailPaiement.dateRembourse());
		paiementDetail.setMontantAPayer(preDetailPaiement.montantAPayer());
		paiementDetail.setMontantARembourser(preDetailPaiement.montantARembourser());
		paiementDetail.setMontantPaye(preDetailPaiement.montantPaye());
		paiementDetail.setMontantRembourse(preDetailPaiement.montantRembourse());
		paiementDetail.setToParametragePaieArticleComplementaireRelationship((EOParametragePaieArticleComplementaire) preDetailPaiement
		    .toParametragePaieArticleComplementaire());
		paiementDetail.setToParametragePaieFormationRelationship((EOParametragePaieFormation) preDetailPaiement.toParametragePaieFormation());
		paiementDetail.setToParametragePaieDiplomeRelationship((EOParametragePaieDiplome) preDetailPaiement.toParametragePaieDiplome());
		paiementDetail.setAuto(preDetailPaiement.auto());
	}

	private void importerDetailPaiementDansPrePaiementDetail(EOEditingContext editingContext, IPaiementDetail paiementDetail, IPrePaiement prePaiement) {
		IPrePaiementDetail preDetailPaiement = (IPrePaiementDetail) EOUtilities.createAndInsertInstance(editingContext, EOPrePaiementDetail.ENTITY_NAME);

		preDetailPaiement.setToPrePaiementRelationship(prePaiement);
		preDetailPaiement.setDatePaye(paiementDetail.datePaye());
		preDetailPaiement.setDateRembourse(paiementDetail.dateRembourse());
		preDetailPaiement.setMontantAPayer(paiementDetail.montantAPayer());
		preDetailPaiement.setMontantARembourser(paiementDetail.montantARembourser());
		preDetailPaiement.setMontantPaye(paiementDetail.montantPaye());
		preDetailPaiement.setMontantRembourse(paiementDetail.montantRembourse());
		preDetailPaiement.setToParametragePaieArticleComplementaireRelationship((EOParametragePaieArticleComplementaire) paiementDetail
		    .toParametragePaieArticleComplementaire());
		preDetailPaiement.setToParametragePaieFormationRelationship((EOParametragePaieFormation) paiementDetail.toParametragePaieFormation());
		preDetailPaiement.setToParametragePaieDiplomeRelationship((EOParametragePaieDiplome) paiementDetail.toParametragePaieDiplome());
		preDetailPaiement.setAuto(paiementDetail.auto());
	}

	private IScoPaiementMoyen importerPreMoyenPaiementDansPaiementMoyen(EOEditingContext editingContext, IPrePaiementMoyen preMoyenPaiement,
	    IScoPaiement paiement,
	    TitulaireInfos titulaireInfos) {
		IScoPaiementMoyen moyen = (IScoPaiementMoyen) EOUtilities.createAndInsertInstance(editingContext, EOPaiementMoyen.ENTITY_NAME);

		moyen.setToPaiementRelationship(paiement);
		moyen.setToTypePaiementRelationship(preMoyenPaiement.toTypePaiement());
		moyen.setToModePaiementRelationship(preMoyenPaiement.toModePaiement());
		moyen.setMontantPaye(preMoyenPaiement.montantPaye());
		moyen.setCodeBanque(preMoyenPaiement.codeBanque());
		moyen.setNomTireur(preMoyenPaiement.nomTireur());
		moyen.setPrenomTireur(preMoyenPaiement.prenomTireur());
		moyen.setNumeroCheque(preMoyenPaiement.numeroCheque());
		moyen.setNumeroCompte(preMoyenPaiement.numeroCompte());
		moyen.setBic(preMoyenPaiement.bic());
		moyen.setIban(preMoyenPaiement.iban());
		moyen.setEtudiantTitulaire(preMoyenPaiement.etudiantTitulaire());

		if (moyen.isDiffere() && titulaireInfos != null) {
			moyen.setToAdresseTitulaireRelationship(titulaireInfos.getAdresse());
			moyen.setToRibTitulaireRelationship(titulaireInfos.getRib());
			moyen.setToFournisseurTitulaireRelationship(titulaireInfos.getFournisseur());
		}
		return moyen;
	}

	private void importerPreEcheancePaiementDansPaiementEcheance(EOEditingContext editingContext, IPrePaiementMoyen preMoyenPaiement,
	    Map<IPrePaiementMoyen, IScoPaiementMoyen> correspondancePreMoyen2Moyen) {
		IScoPaiementMoyen moyenPaiement = correspondancePreMoyen2Moyen.get(preMoyenPaiement);

		for (IPrePaiementEcheance preEcheancePaiement : preMoyenPaiement.toPrePaiementEcheances()) {
			IScoPaiementEcheance echeance = (IScoPaiementEcheance) EOUtilities.createAndInsertInstance(editingContext, EOPaiementEcheance.ENTITY_NAME);

			echeance.setToPaiementMoyenRelationship(moyenPaiement);
			echeance.setDateEcheance(preEcheancePaiement.dateEcheance());
			echeance.setLibelle(preEcheancePaiement.libelle());
			echeance.setMontant(preEcheancePaiement.montant());

			IPrePaiementMoyen autrePreMoyenPaiement = preEcheancePaiement.toAutrePrePaiementMoyen();
			if (autrePreMoyenPaiement != null) {
				echeance.setToAutrePaiementMoyenRelationship(correspondancePreMoyen2Moyen.get(autrePreMoyenPaiement));
			}
		}
	}

	private IPrePaiementMoyen importerPaiementMoyenDansPreMoyenPaiement(EOEditingContext editingContext, IScoPaiementMoyen moyen, IPrePaiement prePaiement,
	    TitulaireInfos titulaireInfos) {
		IPrePaiementMoyen preMoyenPaiement = (IPrePaiementMoyen) EOUtilities.createAndInsertInstance(editingContext, EOPrePaiementMoyen.ENTITY_NAME);

		preMoyenPaiement.setToPrePaiementRelationship(prePaiement);
		preMoyenPaiement.setToTypePaiementRelationship(moyen.toTypePaiement());
		preMoyenPaiement.setToModePaiementRelationship(moyen.toModePaiement());
		preMoyenPaiement.setMontantPaye(moyen.montantPaye());
		preMoyenPaiement.setCodeBanque(moyen.codeBanque());
		preMoyenPaiement.setNomTireur(moyen.nomTireur());
		preMoyenPaiement.setPrenomTireur(moyen.prenomTireur());
		preMoyenPaiement.setNumeroCheque(moyen.numeroCheque());
		preMoyenPaiement.setNumeroCompte(moyen.numeroCompte());
		preMoyenPaiement.setBic(moyen.bic());
		preMoyenPaiement.setIban(moyen.iban());
		preMoyenPaiement.setEtudiantTitulaire(moyen.etudiantTitulaire());

		if (preMoyenPaiement.isDiffere() && titulaireInfos != null) {
			IIndividu individuFournisseur = titulaireInfos.getFournisseur().toIIndividu();
			preMoyenPaiement.setIdPersonneTitulaire(individuFournisseur.persId());
			preMoyenPaiement.setNomTireur(individuFournisseur.nomUsuel());
			preMoyenPaiement.setPrenomTireur(individuFournisseur.prenom());
			preMoyenPaiement.setToCiviliteTitulaireRelationship(individuFournisseur.getCivilite());

			if (!moyen.etudiantTitulaire()) {
				IPreAdresse adressefacturation = EOPreAdresse.creerInstance(editingContext);
				adressefacturation.copierFromAdresseReferentiel(titulaireInfos.getAdresse());
				preMoyenPaiement.setToPreAdresseTitulaireRelationship(adressefacturation);
			}

		}
		return preMoyenPaiement;
	}

	private void importerPaiementEcheanceDansPreEcheancePaiement(EOEditingContext editingContext, IScoPaiementMoyen moyenPaiement,
	    Map<IScoPaiementMoyen, IPrePaiementMoyen> correspondanceMoyen2PreMoyen) {
		IPrePaiementMoyen preMoyenPaiement = correspondanceMoyen2PreMoyen.get(moyenPaiement);

		for (IPaiementEcheance echeancePaiement : moyenPaiement.toPaiementEcheances()) {
			IPrePaiementEcheance preEcheance = (IPrePaiementEcheance) EOUtilities.createAndInsertInstance(editingContext, EOPrePaiementEcheance.ENTITY_NAME);

			preEcheance.setToPrePaiementMoyenRelationship(preMoyenPaiement);
			preEcheance.setDateEcheance(echeancePaiement.dateEcheance());
			preEcheance.setLibelle(echeancePaiement.libelle());
			preEcheance.setMontant(echeancePaiement.montant());

			IPaiementMoyen autreMoyenPaiement = echeancePaiement.toAutrePaiementMoyen();
			if (autreMoyenPaiement != null) {
				preEcheance.setToAutrePrePaiementMoyenRelationship(correspondanceMoyen2PreMoyen.get(autreMoyenPaiement));
			}
		}
	}

	private void creerIasPourPreinscriptions(EOEditingContext editingContext, IPreEtudiant preEtudiant, IEtudiant etudiant, IScoEtudiantAnnee etudiantAnnee,
	    Integer annee, PersonneApplicationUser personneApplicationUser, ITypeEtudiant typeEtudiant, Boolean creerIp) {

		suppressionInscription(editingContext, etudiantAnnee, annee);

		for (IInscription preInscription : preEtudiant.toInscriptions()) {
			creerIaPourPreinscription(editingContext, (EOPreInscription) preInscription, preEtudiant, etudiant, etudiantAnnee, annee, personneApplicationUser,
			    typeEtudiant, creerIp);
		}
	}

	/**
	 * @param editingContext
	 * @param preInscription
	 * @param preEtudiant
	 * @param etudiant
	 * @param etudiantAnnee
	 * @param annee
	 * @param personneApplicationUser
	 * @param typeEtudiant
	 * @param creerIp
	 */
	public void creerIaPourPreinscription(EOEditingContext editingContext, EOPreInscription preInscription, IPreEtudiant preEtudiant, IEtudiant etudiant,
	    IScoEtudiantAnnee etudiantAnnee, Integer annee, PersonneApplicationUser personneApplicationUser, ITypeEtudiant typeEtudiant, Boolean creerIp) {

		EOInscription inscription = (EOInscription) EOUtilities.createAndInsertInstance(editingContext, EOInscription.ENTITY_NAME);

		inscription.setToEtudiantRelationship((EOEtudiant) etudiant);
		inscription.setToEtudiantAnneeRelationship((EOEtudiantAnnee) etudiantAnnee);

		inscription.setDCreation(new NSTimestamp());
		inscription.setDModification(new NSTimestamp());

		inscription.setAnnee(annee);
		inscription.setCycleAmenage(preInscription.cycleAmenage());
		inscription.setNiveau(preInscription.niveau());
		inscription.setPassageConditionnel(preInscription.passageConditionnel());
		inscription.setPersIdCreation(personneApplicationUser.getPersId());
		inscription.setPersIdModification(personneApplicationUser.getPersId());
		inscription.setRedoublement(preInscription.redoublement());
		inscription.setReorientation(preInscription.reorientation());

		inscription.setToDiplomeRelationship(preInscription.toDiplome());
		inscription.setToGradeUniversitaireRelationship(preInscription.toGradeUniversitaire());
		inscription.setToTypeEchangeRelationship(preInscription.toTypeEchange());
		inscription.setToTypeInscriptionFormationRelationship(preInscription.toTypeInscriptionFormation());
		inscription.setToParcoursDiplomeRelationship(preInscription.toParcoursDiplome());
		inscription.setToParcoursAnneeRelationship(preInscription.toParcoursAnnee());
		inscription.setIdLienConsultationDiplome(preInscription.idLienConsultationDiplome());
		inscription.setIdLienConsultationParcoursDiplome(preInscription.idLienConsultationParcoursDiplome());
		inscription.setIdLienConsultationAnnee(preInscription.idLienConsultationAnnee());
		inscription.setIdLienConsultationParcoursAnnee(preInscription.idLienConsultationParcoursAnnee());
		if (preInscription.toRegimeInscription() != null) {
			inscription.setToRegimeInscription(preInscription.toRegimeInscription());
		}
		if (preInscription.toEtablissementCumulatif() != null) {
			inscription.setToEtablissementCumulatifRelationship(preInscription.toEtablissementCumulatif());
		}

		if (creerIp) {			
			creerIpPourIa(editingContext, inscription, personneApplicationUser.getPersId(), typeEtudiant);
		}
	}

	
	/**
	 * cree les IPs pour une IA
	 * @param editingContext contexte d'edition
	 * @param inscription inscription administrative
	 * @param persId persId de l'utilisateur qui crée lIP
	 * @param typeEtudiant type d'étudiant
	 */
	public void creerIpPourIa(EOEditingContext editingContext, IInscription inscription, Integer persId, ITypeEtudiant typeEtudiant) {
		InscriptionPedagogiqueService ipService = new InscriptionPedagogiqueServiceImpl(editingContext);
		ipService.creerIpsPourIa(editingContext, inscription, persId, typeEtudiant);		
	}
	
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

	/**
	 * Méthode d'import dans le SI
	 * @param prelevementSepaService le service de prelevement sepa
	 * @param etudiantAnnee l'etudiant annee
	 * @param objet l'objet du prélèvement
	 * @throws ImportPrelevementException si les données en entrée ne sont pas complêtes
	 */
	public void importerPrelevementSepa(PrelevementSepaService prelevementSepaService, IScoEtudiantAnnee etudiantAnnee, String objet)
	    throws ImportPrelevementException {

		List<IScoPaiementMoyen> prelevements = etudiantAnnee.moyensPaiementPrelevementSansEcheancierSepa();
		if (prelevements.isEmpty()) {
			throw new ImportPrelevementException("Aucun prélèvement trouvé pour l'étudiant");
		}
		IScoPaiementMoyen prelevement = prelevements.get(0);

		IFournis fournisseur = prelevement.toFournisseurTitulaire();
		if (fournisseur == null || fournisseur.toPersonne() == null) {
			throw new ImportPrelevementException(errorMessagePrelevement("Aucun fournisseur titulaire trouvé pour le prelevement : ", prelevement));
		}
		if (prelevement.toAdresseTitulaire() == null) {
			throw new ImportPrelevementException(errorMessagePrelevement("Aucune adresse de facturation trouvée pour le prelevement : ", prelevement));
		}
		if (prelevement.toRibTitulaire() == null) {
			throw new ImportPrelevementException(errorMessagePrelevement("Aucun rib trouvé pour le prelevement : ", prelevement));
		}
		Integer debiteurPersIdTitulaire = prelevement.toFournisseurTitulaire().toPersonne().persId();
		Integer adresseIdTitulaire = prelevement.toAdresseTitulaire().adrOrdre();
		Integer ribIdTitulaire = prelevement.toRibTitulaire().ribOrdre();

		// Génération échéancier
		List<IEcheanceLight> echeances = new ArrayList<IEcheanceLight>(prelevement.toPaiementEcheances().size());
		for (IPaiementEcheance echeancePaiement : prelevement.toPaiementEcheances()) {
			if (echeancePaiement.toAutrePaiementMoyen() == null) {
				echeances.add(echeancePaiement);
			}
		}
		Integer idTiersDebiteur = null;
		if (!prelevement.etudiantTitulaire()) {
			idTiersDebiteur = etudiantAnnee.toEtudiant().toIndividu().persId();
		}
		ISepaSddMandat mandat = prelevementSepaService.creerMandat(objet, debiteurPersIdTitulaire, adresseIdTitulaire, ribIdTitulaire, idTiersDebiteur);
		ISepaSddEcheancier echeancier = prelevementSepaService.creerEcheancier(mandat, echeances, prelevement.toPaiement().id());
		prelevement.setToEcheancierRelationship(echeancier);
	}

	private String errorMessagePrelevement(String message, IScoPaiementMoyen prelevement) {
		return message + ((ERXGenericRecord) prelevement).primaryKey();
	}

	protected Boolean isEtudiantInscritDansAnneeSuperieure(EOEditingContext editingContext, IPreEtudiant preEtudiant, Integer annee, PersonneApplicationUser user) {
		EOIndividu individu = getIndividuPourPreEtudiant(editingContext, preEtudiant, user, false);
		if (individu == null) {
			return false;
		}

		EOEtudiant etudiant = getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, individu, null, false);
		if (etudiant == null) {
			return false;
		}

		Integer count = (Integer) ERXEOControlUtilities.aggregateFunctionWithQualifier(editingContext, EOEtudiantAnnee.ENTITY_NAME,
		    EOEtudiantAnnee.ID_ETUDIANT_ANNEE.key(), "COUNT", EOEtudiantAnnee.ANNEE.gt(annee).and(EOEtudiantAnnee.TO_ETUDIANT.eq(etudiant)));

		return count > 0;
	}

	/**
	 * @param editingContext contexte d'édition
	 * @param preEtudiant l'étudiant qui est importé dans le SI
	 * @param user l'utilisateur qui réalise l'import
	 * @param annee annee pour laquelle on réalise l'import
	 * @param codeRneEtablissement Le code RNE de l'établissement
	 * @param structureEtudiant structure à laquelle est rattaché l'étudiant
	 * @return Un etudiant année importé
	 * @throws ImportException si une erreur survient lors de l'import dans le SI
	 */
	public IScoEtudiantAnnee importEtudiantDansSiPourBatch(EOEditingContext editingContext, IPreEtudiant preEtudiant, PersonneApplicationUser user,
	    Integer annee, String codeRneEtablissement,
	    String structureEtudiant) throws ImportException {

		Boolean isEtudiantInscritDansAnneeSuperieure = isEtudiantInscritDansAnneeSuperieure(editingContext, preEtudiant, annee, user);

		IEtudiant etudiant;
		IIndividu individu;
		if (!isEtudiantInscritDansAnneeSuperieure) {
			individu = creerOuMiseAJourIndividu(editingContext, preEtudiant, user, structureEtudiant, annee);
			creerOuMiseAJourAdresses(editingContext, individu, preEtudiant, user, annee);
			// creerOuMiseAJourFournisseur(editingContext, preEtudiant, adresseFacturation, annee);
			creerOuMiseAJourTelephones(editingContext, individu, preEtudiant, user);
			etudiant = creerOuMiseAJourEtudiant(editingContext, preEtudiant, individu, annee, codeRneEtablissement);
		} else {
			individu = getIndividuPourPreEtudiant(editingContext, preEtudiant, user, false);
			etudiant = getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, individu, null, false);
		}

		IScoEtudiantAnnee etudiantAnnee = creerOuMiseAJourEtudiantAnnee(editingContext, etudiant, preEtudiant, user, annee);
		importPreCursusDansCursus(editingContext, etudiant, preEtudiant, user);
		importPreBoursesDansBourses(editingContext, etudiant, preEtudiant, annee, user);
		ITypeEtudiant typeEtudiant = EOTypeEtudiant.typeClassique(editingContext);
		creerIasPourPreinscriptions(editingContext, preEtudiant, etudiant, etudiantAnnee, annee, user, typeEtudiant, false);

		// A voir si on les met
		// importerPrePaiementDansPaiement(editingContext, etudiantAnnee, preEtudiant.toPreEtudiantAnnee(annee), user.getPersId());
		return etudiantAnnee;
	}

}
