/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePhoto.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPrePhoto extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPrePhoto.class);

	public static final String ENTITY_NAME = "EOPrePhoto";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_PHOTO";


// Attribute Keys
  public static final ERXKey<NSData> DATAS_PHOTO = new ERXKey<NSData>("datasPhoto");
  public static final ERXKey<NSTimestamp> DATE_PRISE = new ERXKey<NSTimestamp>("datePrise");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPrePhoto";

	public static final String DATAS_PHOTO_KEY = "datasPhoto";
	public static final String DATE_PRISE_KEY = "datePrise";

// Attributs non visibles
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String ID_PRE_PHOTO_KEY = "idPrePhoto";

//Colonnes dans la base de donnees
	public static final String DATAS_PHOTO_COLKEY = "DATAS_PHOTO";
	public static final String DATE_PRISE_COLKEY = "DATE_PRISE";

	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String ID_PRE_PHOTO_COLKEY = "ID_PRE_PHOTO";


	// Relationships
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";



	// Accessors methods
  public NSData datasPhoto() {
    return (NSData) storedValueForKey(DATAS_PHOTO_KEY);
  }

  public void setDatasPhoto(NSData value) {
    takeStoredValueForKey(value, DATAS_PHOTO_KEY);
  }

  public NSTimestamp datePrise() {
    return (NSTimestamp) storedValueForKey(DATE_PRISE_KEY);
  }

  public void setDatePrise(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PRISE_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrePhoto avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrePhoto createEOPrePhoto(EOEditingContext editingContext, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant			) {
    EOPrePhoto eo = (EOPrePhoto) createAndInsertInstance(editingContext, _EOPrePhoto.ENTITY_NAME);    
    eo.setToPreEtudiantRelationship(toPreEtudiant);
    return eo;
  }

  
	  public EOPrePhoto localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrePhoto)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePhoto creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePhoto creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPrePhoto object = (EOPrePhoto)createAndInsertInstance(editingContext, _EOPrePhoto.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrePhoto localInstanceIn(EOEditingContext editingContext, EOPrePhoto eo) {
    EOPrePhoto localInstance = (eo == null) ? null : (EOPrePhoto)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrePhoto#localInstanceIn a la place.
   */
	public static EOPrePhoto localInstanceOf(EOEditingContext editingContext, EOPrePhoto eo) {
		return EOPrePhoto.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrePhoto fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePhoto fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrePhoto> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrePhoto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrePhoto)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrePhoto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrePhoto fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrePhoto> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrePhoto eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrePhoto)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrePhoto fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrePhoto eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrePhoto ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrePhoto fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
