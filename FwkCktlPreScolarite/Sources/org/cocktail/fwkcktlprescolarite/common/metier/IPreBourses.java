package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

public interface IPreBourses extends IBourses {

	IPreEtudiant toPreEtudiant();

	void setToPreEtudiantRelationship(IInfosEtudiant value);
	
//	// Accessors methods
//	public Integer annee();
//
//	public void setAnnee(Integer value);
//
//	public NSTimestamp dCreation();
//
//	public void setDCreation(NSTimestamp value);
//
//	public NSTimestamp dMofication();
//
//	public void setDMofication(NSTimestamp value);
//
//	public IEchelonBourse toEchelonBourse();
//
//	public void setToEchelonBourseRelationship(IEchelonBourse value);
//
//	public String numAllocataire();
//
//	public void setNumAllocataire(String value);
//
//	public String organisme();
//
//	public void setOrganisme(String value);
//
//	public Integer persIdCreation();
//
//	public void setPersIdCreation(Integer value);
//
//	public Integer persIdModification();
//
//	public void setPersIdModification(Integer value);
//
//	public IPreEtudiant toPreEtudiant();
//
//	public void setToPreEtudiantRelationship(IPreEtudiant value);
//
//	public void setToTypeInscriptionFormationRelationship(
//			ITypeInscriptionFormation value);
//
//	public ITypeInscriptionFormation toTypeInscriptionFormation();
//
//	public Boolean isCampusFrance();
//
//	public void setIsCampusFrance(Boolean value);
//
//	public ITypeBoursesCF toTypeBoursesCF();
//
//	public void setToTypeBoursesCFRelationship(ITypeBoursesCF value);
}
