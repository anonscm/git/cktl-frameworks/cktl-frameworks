/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Nullable;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOAcademie;
import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EODepartement;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOMention;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationMilitaire;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.ICompte;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IDepartement;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPhoto;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;
import org.cocktail.fwkcktlprescolarite.common.circuitValidation.EtapeInscription;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.ControlesContexte;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.InfoEtudiantControle;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.PreEtudiantControle;
import org.cocktail.fwkcktlprescolarite.common.metier.controles.ResultatControle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IMention;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.IAnneeProvider;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXKey;
import er.extensions.localization.ERXLocalizer;

/**
 * classe métier EOPreEtudiant.
 * @author isabelle réau
 */
public class EOPreEtudiant extends _EOPreEtudiant implements IPreEtudiant {
	
	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreEtudiant.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	@Inject
	@Nullable
	private IAnneeProvider anneeProvider;
	
	public static final String TO_PRE_ETUDIANT_ANNEE_EXERCICE_KEY = "toPreEtudiantAnneeExercice";
	public static final ERXKey<EOPreEtudiantAnnee> TO_PRE_ETUDIANT_ANNEE_EXERCICE = new ERXKey<EOPreEtudiantAnnee>(TO_PRE_ETUDIANT_ANNEE_EXERCICE_KEY);
	public static final ERXKey<String> TO_PRE_ETUDIANT_ETAT = new ERXKey<String>("currentEtat");

	public static final String CST_REIMMATRICULATION_IMPORTED= "I";
	public static final String CST_REIMMATRICULATION_UPDATED= "U";
	
    /**
     * Instantiates a new eO pre etudiant.
     */
    public EOPreEtudiant() {
        super();
   }
	
	public Integer getAnneeExercice() {
	    Integer annee = null;
	    if (anneeProvider != null) {
	        annee = anneeProvider.getAnneeExercice();
	    }
		return annee;
	}
	
	public void setAnneeExercice(Integer annee) {
	    if (anneeProvider != null) {
	        this.anneeProvider.setAnneeExercice(annee);
	    }
	}
    
	/**
	 * Creer.
	 * @param edc : contexte d'édition
	 * @param persId :
	 * @return EODiplome
	 */
	public static EOPreEtudiant creer(EOEditingContext edc, Integer persId) {

		EOPreEtudiant etudiant = new EOPreEtudiant();
		edc.insertObject(etudiant);
		etudiant.setPersIdCreation(persId);
		etudiant.setPersIdModification(persId);
		etudiant.setDCreation(new NSTimestamp());
		etudiant.setDModification(new NSTimestamp());
		etudiant.setIneProvisoire(false);
		etudiant.setInseeProvisoire(false);
		etudiant.setEnfantsCharge(false);
		etudiant.setIndicateurPhoto(false);
		etudiant.setSportHN(false);
		etudiant.setSnAttestation(new Integer(0));
		etudiant.setSnCertification(new Integer(0));
		etudiant.setEnfantsCharge(false);

		return etudiant;
	}
    
	/**
	 * La méthode teste si l'étudiant est de sexe masculin à partir de la civilité
	 * @return true si la civilité est non ET qu'il s'agit d'un Monsieur
	 */
	public boolean estHomme() {
		return civilite() != null && civilite().equals(EOCivilite.C_CIVILITE_MONSIEUR);
	}
	
	/**
	 * Méthode qui renvoie un objet CodeInsee si noInsee est renseigné
	 * @return null si la String est vide sinon on crée un objet CodeInsee avec la String dans NoInsee
	 */
	public CodeInsee getCodeInsee() {
		if (StringCtrl.isEmpty(noInsee())) {
			return null;
		}
		return new CodeInsee(noInsee());
	}

	/**
	 * La méthode renvoie la String de la civilité
	 * @return la civilité si la relation est renseignée sinon null
	 */
	public String civilite() {
		if (toCivilite() != null) {
			return toCivilite().cCivilite();
		} else {
			return null;
		}
	}
	
	protected static void check(EOPreEtudiant preEtudiant, PreEtudiantControle controle) {

		if (controle.checkable(preEtudiant)) {
			ResultatControle resultat = controle.check(preEtudiant);
			if (!resultat.valide()) {
				throw new NSValidation.ValidationException(resultat.getMessage());
			}
		}
	}
    
    /**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws ValidationException the validation exception
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
		InfoEtudiantControle ctrl =  new InfoEtudiantControle(new ControlesContexte());
		Boolean isOK = ctrl.controleNonExistance(editingContext(), this);
		if (!isOK) {
				throw new  NSValidation.ValidationException(ctrl.getContexte().getListeErreurs().get(0));
		} else {
	        validateBeforeTransactionSave();
	        super.validateForInsert();
		}
    }

    /**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws ValidationException the validation exception
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws ValidationException the validation exception
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws ValidationException the validation exception
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws ValidationException the validation exception
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

    /**
     * {@inheritDoc}
     */
	public EODepartement toDepartement() {
		EODepartement departement = null;
		if (deptNaissance() != null) {
			departement = EODepartement.fetchFirstByQualifier(editingContext(), EODepartement.C_DEPARTEMENT.eq(deptNaissance()));
		}
		return departement;
	}

	public boolean isPersonnel() {
		return false;
	}

	public boolean isEtudiant() {
		return true;
	}

	public void setToRneEtabSupRelationship(IRne rneEtabSup) {
		super.setToRneEtabSupRelationship((EORne) rneEtabSup);
	}
	
	/**
	 * @return <code>true</code> si l'étudiant est post-bac (n'a que le bac donc pas de cursus)
	 */
	public boolean isEtudiantPostBac() {
		return toPreCursus().size() == 0;
	}
	
	/**
	 * @return nomAffichage
	 */
	public String nomAffichage() {
		return nomUsuel();
	}

	/**
	 * @return prenomAffichage
	 */
	public String prenomAffichage() {
		return prenom();
	}

	/**
	 * @return noIndividu
	 */
	public Integer noIndividu() {
		return (Integer) ERXEOControlUtilities.primaryKeyObjectForObject(this);
	}

	public Date getDtNaissance() {
		return super.dNaissance();
	}

	public IDepartement getDepartement() {
		return super.toDepartementNaissance();
	}

	public IPays getPaysNaissance() {
		return super.toPaysNaissance();
	}

	public ICivilite getCivilite() {
		return super.toCivilite();
	}

	/**
	 * @param value to set
	 */
	public void setToIndividuEtudiantRelationship(IIndividu value) {
		super.setToIndividuEtudiantRelationship((EOIndividu) value);
		
	}

	/**
	 * @param etudiant to set
	 */
	public void setToEtudiantRelationship(IEtudiant etudiant) {
		super.setToEtudiantRelationship((EOEtudiant) etudiant);
		
	}

	/**
	 * @return villeNaissance
	 */
	public String villeDeNaissance() {
		return super.villeNaissance();
	}

	/**
	 * @return persId
	 */
	public Integer persId() {
		return super.toIndividuEtudiant().persId();
	}

	public Date getDtNaturalisation() {
		return super.dNaturalisation();
	}

	/**
	 * @return CodeInseeProv
	 */
	public CodeInsee getCodeInseeProv() {
		if (inseeProvisoire() && !StringCtrl.isEmpty(noInsee())) {
			return new CodeInsee(noInsee());
		}
		return null;
	}

	public Integer getCleInsee() {
		return super.cleInsee();
	}

	/**
	 * @return CleInseeProv
	 */
	public Integer getCleInseeProv() {
		if (inseeProvisoire()) {
			return cleInsee();
		}
		return null;
	}

	/**
	 * @return toSituationMilitaire
	 */
	public EOSituationMilitaire toSituationMilitaire() {
		return toSituationMilitaire();
	}

	/**
	 * @param value to set
	 */
	public void setToCiviliteRelationship(ICivilite value) {
		setToCiviliteRelationship((EOCivilite) value);
		
	}

	/**
	 * @param annee l'année que l'on veut récupérer
	 * @return la vleur correspondante à l'étudiant et année
	 */
	public IScoEtudiantAnnee toEtudiantAnnee(Integer annee) {
		EOQualifier qualifier = EOEtudiantAnnee.TO_ETUDIANT.eq(toEtudiant()).and(EOEtudiantAnnee.ANNEE.eq(annee));
		return EOEtudiantAnnee.fetchSco_EtudiantAnnee(editingContext(), qualifier);
	}

	/**
	 * @param annee l'année que l'on veut récupérer
	 * @return la valeur correspondante à l'étudiant et année
	 */
	public IPreEtudiantAnnee toPreEtudiantAnnee(Integer annee) {
		IPreEtudiantAnnee preEtudiantAnnee = null;
		
		NSArray<EOPreEtudiantAnnee> listePreEtudiantAnnee = toPreEtudiantAnnee(EOPreEtudiantAnnee.ANNEE.eq(annee));
		
		if (!listePreEtudiantAnnee.isEmpty()) {
			preEtudiantAnnee = listePreEtudiantAnnee.get(0);
		}
		
		return preEtudiantAnnee;
	}
	
	public IPreEtudiantAnnee toPreEtudiantAnneeExercice() {
		return toPreEtudiantAnnee(getAnneeExercice());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IPreBourses> toPreBourses(Integer annee) {
		return new ArrayList<IPreBourses>(super.toPreBourses(EOPreBourses.ANNEE.eq(annee)));
  }

	public IDemande getToDemande() {
		return todemande();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean vaPayerParVirement(Integer annee) {
	    IPreEtudiantAnnee preEtudiantAnnee = toPreEtudiantAnnee(annee);
	    boolean result = false;
	    if (preEtudiantAnnee != null) {
	        IPrePaiement prePaiement = preEtudiantAnnee.toPrePaiementInitial();
	        if (prePaiement == null) {
	            throw new IllegalStateException("Le PreEtudiantAnnee " + preEtudiantAnnee + " n'a pas de PrePaiement !");
	        }
	        
	        if (prePaiement.paiementValide()) {
		        // Si un des moyens de paiements est virement
		        for (IPrePaiementMoyen preMoyenPaiement : prePaiement.toPrePaiementMoyens()) {
	        		result = preMoyenPaiement.toTypePaiement().isDiffere();
	        		
	        		if (result) {
	        			break;
	        		}
	        	}
			}
	    }
	    
	    return result;
	}

	/**
	 * {@inheritDoc}
	 */
    public IAdresse adresseFacturation() {
        throw new UnsupportedOperationException();
    }

    /**
	 * {@inheritDoc}
	 */
    public IAdresse adresseEtudiant() {
    	return toPreAdresses(EOPreAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(EOTypeAdresse.TADR_CODE_ETUD)).lastObject();
	}

    /**
	 * {@inheritDoc}
	 */
	public IAdresse adresseParent() {
    	return toPreAdresses(EOPreAdresse.TO_TYPE_ADRESSE.dot(EOTypeAdresse.TADR_CODE).eq(EOTypeAdresse.TADR_CODE_PAR)).lastObject();
	}

	/**
	 * {@inheritDoc}
	 */
	public EOPersonneTelephone telephone(String typeNoTel, String typeTel) {
		throw new UnsupportedOperationException();
	}
    
    /**
     * {@inheritDoc}
     */
    public IFournis toFournis() {
        throw new UnsupportedOperationException();
    }
	
	/**
	 * {@inheritDoc}
	 */
	public IPreInscription getInscriptionPrincipale() {
		for (IPreInscription preInscription : toPreInscriptions()) {
			if (StringUtils.equals(preInscription.toTypeInscriptionFormation().codeInscription(), ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE)) {
				return preInscription;
			}
		}
		return null;
    }
	
	public List<IPreInscription> getInscriptionsComplementaires() {
		EOQualifier qualifier = EOPreInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).ne(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		return new ArrayList<IPreInscription>(toPreInscriptions(qualifier, EOPreInscription.D_CREATION.ascs(), false));
	}

	public IPreInscription getInscriptionComplementaire(int index) {
		if(getInscriptionsComplementaires().size() > index) {
			return getInscriptionsComplementaires().get(index);
		} else {
			return null;
		}	}
	
	public IPreInscription getInscriptionSecondaire() {
		return getInscriptionComplementaire(0);
	}
	
	public IPreInscription getInscriptionTertiaire() {
		return getInscriptionComplementaire(1);
	}
	
	/**
	 * @return the currentEtat
	 */
	public String currentEtat() {
		return (EtapeInscription.getEtape(this.todemande().toEtape())).getEtatDemande();
	}
	
	public String getInfoAffichagePaiement() {
		if (toPreEtudiantAnneeExercice() != null && toPreEtudiantAnneeExercice().toPrePaiementInitial() != null && toPreEtudiantAnneeExercice().toPrePaiementInitial().paiementValide()) {
			return  ERXLocalizer.currentLocalizer().localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement.Paye");
		} else {
			return  ERXLocalizer.currentLocalizer().localizedStringForKey("ListesFiltres.EtudiantTableView.ColonneEtatPaiement.NonPaye");
		}

	}

	public IPhoto setPhoto(NSData data) {
		EOPrePhoto photo = (EOPrePhoto) toPhoto();
		if (photo == null) {
			photo = EOPrePhoto.creerInstance(editingContext());
		}
		// Si on crée une photo, cet indice est vide donc à null.
		// On sette par défaut à "OUI".
		if (indPhoto() == null) {
			setIndPhoto(IIndividu.TEM_VALIDE_O);
		}
		photo.setDatasPhoto(data);
		photo.setDatePrise(new NSTimestamp());
		photo.setToPreEtudiantRelationship(this);
		return  (IPhoto)photo;
	}

	
	
	public IPhoto toPhoto() {
		NSArray<EOPrePhoto> photos = new NSMutableArray<EOPrePhoto>();
		photos = toPrePhotos();
		return  photos.lastObject();
	}


	public void setIndPhoto(String string) {
		if (IIndividu.TEM_VALIDE_O.equals(string)) {
			setIndicateurPhoto(true);
		} else {
			setIndicateurPhoto(false);
		}
		
	}

	public String indPhoto() {
		if (this.indicateurPhoto()) {
			return IIndividu.TEM_VALIDE_O;
		} else {
			return IIndividu.TEM_VALIDE_N;
		}
	}

	public String deptNaissanceLibelle() {
		EODepartement dept = EODepartement.fetchFirstByQualifier(editingContext(), EODepartement.C_DEPARTEMENT.eq(deptNaissance()));
		if (dept == null) {
			return "";
		} else {
			return dept.llDepartement();
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean isEtranger() {
		throw new UnsupportedOperationException();
	}


	
	/**
	 * {@inheritDoc}
	 */
	public IIndividu asIndividu() {
		return this;
	}

	/**
	 * {@inheritDoc}
	 */
	public List<IAdresse> toAdresses() {
		return new ArrayList<IAdresse>(toPreAdresses());
	}

	/**
	 * {@inheritDoc}
	 */
	public List<ITelephone> toTelephones() {
		return new ArrayList<ITelephone>(toPreTelephones());
	}

	/**
	 * {@inheritDoc}
	 */
	public void setIndNoInsee(String value) {
		setNoInsee(value);
	}

	public void setToDepartementRelationship(IDepartement departement) {
		if (departement != null) {
			setDeptNaissance(departement.getCode());
		} else {
			setDeptNaissance(null);
		}
	}

	public void setToSituationFamilialeRelationship(ISituationFamiliale value) {
		setToSituationFamilialeRelationship((EOSituationFamiliale) value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public String indNoInsee() {
		return noInsee();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setNomPatronymiqueAffichage(String value) {
		setNomPatronymique(value);
	}

	/**
	 * {@inheritDoc}
	 */
	public String indNoInseeProv() {
		return noInsee();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setIndNoInseeProv(String numero) {
		setNoInsee(numero);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setIndCleInseeProv(Integer clef) {
		setCleInsee(clef);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public boolean hasNoInseeProvisoire() {
		return inseeProvisoire();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setHasNoInseeProvisoire(boolean inseeProvisoire) {
		setInseeProvisoire(inseeProvisoire);
	}

	/**
	 * {@inheritDoc}
	 */
	public void supprimerAdresse(IAdresse currentAdresse, ITypeAdresse type) {
		removeFromToPreAdressesRelationship((EOPreAdresse) currentAdresse);
		((EOPreAdresse) currentAdresse).delete();
	}

	public List<IInscription> toInscriptions() {
		return new ArrayList<IInscription>(super.toPreInscriptions());
	}

	public List<IInscription> toInscriptions(EOQualifier qualifier) {
		return new ArrayList<IInscription>(super.toPreInscriptions(qualifier));
	}
	
	public List<? extends ICursus> toCursus() {
		return super.toPreCursus();
	}

	public void setToPaysNaissanceRelationship(IPays value) {
		super.setToPaysNaissanceRelationship((EOPays) value);
	}

	public void setToPaysNationaliteRelationship(IPays value) {
		super.setToPaysNationaliteRelationship((EOPays) value);
	}

	public void setVilleDeNaissance(String ville) {
		setVilleNaissance(ville);
	}

	public void setDNaissance(Date dNaissance) {
		super.setDNaissance((NSTimestamp) dNaissance);
	}

	public ITelephone toTelephone(ITypeTel typeTel, ITypeNoTel typeNoTel) {
		ITelephone tel = null;
		EOQualifier qualifier = EOPreTelephone.TO_TYPE_TELS.dot(EOTypeTel.C_TYPE_TEL_KEY).eq(typeTel.cTypeTel())
				.and(EOPreTelephone.TO_TYPE_NO_TEL.dot(EOTypeNoTel.C_TYPE_NO_TEL_KEY).eq(typeNoTel.cTypeNoTel()));
		NSArray<EOPreTelephone> malisteTel = super.toPreTelephones(qualifier);
		if (malisteTel != null && malisteTel.size() > 0) {
			tel = malisteTel.get(0);
		}
		return tel;
	}

	
	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu#indQualite()
	 * Implémenté depuis IIndividu, mais non pertinent pour un PreEtudiant
	 */
	public String indQualite() {
	  // TODO Auto-generated method stub
	  return null;
  }

	/* (non-Javadoc)
	 * @see org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu#toComptes()
	 * Implémenté depuis IIndividu, mais non pertinent pour un PreEtudiant
	 */
	public List<? extends ICompte> toComptes() {
	  // TODO Auto-generated method stub
	  return null;
  }
	
	public List<? extends IBourses> toBourses() {
		return toPreBourses();
	}
	
	public List<? extends IBourses> toBourses(EOQualifier qualifier) {
		return toPreBourses(qualifier);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getNomPrenomAffichage() {
		return nomAffichage();
	}

	public EOBac toBac() {
		return toBacs();
	}

	public Long etudAnbac() {
		Integer anneeBac = anneeBac();
		Long anneeBacAsLong = anneeBac != null ? new Long(anneeBac) : null;
		return anneeBacAsLong;
	}
	
	public void setEtudAnbac (Long annee) {
		super.setAnneeBac(longToInteger(annee));
	}
	
	public EOAcademie toAcademie() {
		return toAcademie();
	}

	public Long etudAnnee1inscEtab() {
		return integerToLong(super.annee1InscEtab());
	}
	
	public Long etudAnnee1inscSup() {
		return integerToLong(super.annee1InscSup());
	}
	
	public Long etudAnnee1inscUniv() {
		return integerToLong(super.anne1InscUniv());
	}

	public void setEtudAnnee1InscEtab(Long uneAnnee) {
		super.setAnnee1InscEtab(longToInteger(uneAnnee));
	}
	
	public void setEtudAnnee1InscSup(Long uneAnnee) {
		super.setAnnee1InscSup(longToInteger(uneAnnee));
	}
	
	public void setEtudAnnee1InscUniv(Long uneAnnee) {
		super.setAnne1InscUniv(longToInteger(uneAnnee));
	}
	
	public String priseCptInsee() {
		return priseCptInsee();
	}

	public EODepartement toDepartementEtabBac() {
		return toDepartementEtabBac();
	}

	/**
	 * Conversion sans NPE.
	 * @param entier un entier
	 * @return un long
	 */
	private Long integerToLong(Integer entier) {
		if (entier == null) {
			return null;
		}
		
		return entier.longValue();
	}
	
	/**
	 * Conversion sans NPE.
	 * @param unLong un long
	 * @return un entier
	 */
	private Integer longToInteger(Long unLong) {
		if (unLong == null) {
			return null;
		}
		
		return unLong.intValue();
	}

	public void setToBacRelationship(EOBac bac) {
		super.setToBacsRelationship(bac);
		
	}

	public void setToMentionRelationship(IMention mentionBacSelected) {
		super.setToMentionRelationship((EOMention) mentionBacSelected);
		
	}

	public void setToRneEtabBacRelationship(IRne etablissementBac) {
		super.setToRneEtabBacRelationship((EORne) etablissementBac);
		
	}

	public void setToPaysEtabBacRelationship(IPays selectedPays) {
		super.setToPaysEtabBacRelationship((EOPays) selectedPays);
		
	}

	public void setEtudReimmatriculation(String value) {
		super.setReimmatriculation(value);
		
	}

	public String etudReimmatriculation() {
		return super.reimmatriculation();
	}
}
