package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;

/** 
 * Article d'un paiement
 */
public interface IPrePaiementDetail extends IPaiementDetail {

	/**
	 * @return lien vers le paiement global
	 */
	IPrePaiement toPrePaiement();

	/**
	 * @param prePaiement lien vers le paiement global
	 */
	void setToPrePaiementRelationship(IPrePaiement prePaiement);

}