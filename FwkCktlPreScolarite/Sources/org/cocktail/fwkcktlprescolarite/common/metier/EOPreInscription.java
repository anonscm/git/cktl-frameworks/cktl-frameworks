/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * The Class EOPreInscription.
 *
 * @author isabelle Réau
 */
public class EOPreInscription extends _EOPreInscription implements IPreInscription {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreInscription.class);
    
    /**
     * Instantiates a new eO pre inscription.
     */
    public EOPreInscription() {
        super();
    }
    
    /**
	 * 
	 * @param edc l'editing context
	 * @param persId du createur
	 * @param annee l'annee universitaire
	 * @return une inscription
	 */
    public static EOPreInscription creer(EOEditingContext edc, Integer persId, Integer annee){
    	EOPreInscription inscription = creerInstance(edc);
    	inscription.setPersIdCreation(persId);
    	inscription.setPersIdModification(persId);
    	inscription.setDCreation(new NSTimestamp());
    	inscription.setDModification(new NSTimestamp());
    	inscription.setAnnee(annee);
    	inscription.setRedoublement(false);
    	inscription.setReorientation(false);
    	inscription.setPassageConditionnel(false);
    	inscription.setCycleAmenage(false);
    	return inscription;
    }

    /**
     * {@inheritDoc}
     */
    public void setToParcoursDiplomeRelationship(IParcours unParcoursType) {
    	super.setToParcoursDiplomeRelationship((EOParcours) unParcoursType);
    }

    /**
     * {@inheritDoc}
     */
    public void setToParcoursAnneeRelationship(IParcours unParcours) {
    	super.setToParcoursAnneeRelationship((EOParcours) unParcours);
    }

	/**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }

    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
        super.validateBeforeTransactionSave();
    }

    /**
	 * relation avec l'IPreEtudiant
	 * @param preEtudiant le preEtudiant
	 */
	public void setToPreEtudiantRelationship(IPreEtudiant preEtudiant) {
		super.setToPreEtudiantRelationship((EOPreEtudiant) preEtudiant);
	}

	/**
	 * 
	 * @param regimeInscription le regime d'inscription
	 */
	public void setToRegimeInscriptionRelationship(IRegimeInscription regimeInscription) {
		super.setToRegimeInscriptionRelationship((EORegimeInscription) regimeInscription);
	}


	/**
	 * relation avec l'IPreEtudiant
	 * @return IPreEtudiant 
	 */
	public IEtudiant toEtudiant() {
		return (IEtudiant) super.toPreEtudiant();
	}


	/**
	 * relation avec l'IEtudiantAnne
	 * @param etudiantAnne l'étudiantAnnee
	 */
	public void setToEtudiantAnneeRelationship(IEtudiantAnnee etudiantAnne) {
		super.setToEtudiantAnneeRelationship((EOPreEtudiantAnnee) etudiantAnne);
	}

	/**
	 * lien vers le diplome
	 * @param diplome le diplome sur lequel est l'inscription
	 */
	public void setToDiplomeRelationship(IDiplome diplome) {
		super.setToDiplomeRelationship((EODiplome) diplome);
	}


	/**
	 * 
	 * @param gradeUniversitaire le grade universitaire sur lequel est l'inscription
	 */
	public void setToGradeUniversitaireRelationship(IGradeUniversitaire gradeUniversitaire) {
		super.setToGradeUniversitaireRelationship((EOGradeUniversitaire) gradeUniversitaire);
	}
	
	/**
	 * 
	 * @param typeInscription le typeInscription sur lequel est l'inscription
	 */
	public void setToTypeInscriptionFormationRelationship(ITypeInscriptionFormation typeInscription) {
		super.setToTypeInscriptionFormationRelationship((EOTypeInscriptionFormation) typeInscription);
	}

	/**
	 * {@inheritDoc}
	 */
	public IVersionDiplome versionDiplome() {
		return toDiplome().getVersionDiplome(this.annee());
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer id() {
		return Integer.valueOf(primaryKey());
	}

	public void setToEtudiantRelationship(IEtudiant etudiant) {
		// TODO Auto-generated method stub
		
	}
	
	public boolean isDetteOuAnticipation() {
		return 
			StringUtils.equals(toTypeInscriptionFormation().codeInscription(), ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_DETTE)
			||
			StringUtils.equals(toTypeInscriptionFormation().codeInscription(), ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ANTICIPATION);
	}

}
