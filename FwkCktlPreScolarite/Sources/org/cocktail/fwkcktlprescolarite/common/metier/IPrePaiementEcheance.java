package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementEcheance;

public interface IPrePaiementEcheance extends IPaiementEcheance {

	/**
	 * @return le moyen de paiement associé à cette échéance
	 */
	IPrePaiementMoyen toPrePaiementMoyen();

	/**
	 * Affecte le moyen de paiement associé à cette échéance.
	 * @param value un moyen de paiement
	 */
	void setToPrePaiementMoyenRelationship(IPrePaiementMoyen value);

	/**
	 * Le moyen de paiement ayant servi à payer cette échéance.
	 * <p>
	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
	 * a été payé avec un chèque.
	 * @return Le moyen de paiement ayant servi à payer cette échéance
	 */
	IPrePaiementMoyen toAutrePrePaiementMoyen();

	/**
	 * Affecte le moyen de paiement ayant servi à payer cette échéance.
	 * <p>
	 * Par exemple dans le cas ou la première écheance d'un paiement par prélèvement
	 * a été payé avec un chèque.
	 * 
	 * @param unAutreMoyenDePaiement un autre moyen le paiement
	 */
	void setToAutrePrePaiementMoyenRelationship(IPrePaiementMoyen unAutreMoyenDePaiement);
}