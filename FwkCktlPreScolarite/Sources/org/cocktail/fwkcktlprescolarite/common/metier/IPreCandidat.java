package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.Date;

/**
 * 
 * Représente un pré candidat
 * 
 * @author Alexis Tual
 *
 */
public interface IPreCandidat {

    /**
     * @return le numéro de pre candidat
     */
    Integer candNumero();
    
    /**
     * @return le numéro bea/ine du pre candidat
     */
    String candBea();
    
    /**
     * @return le nom du candidat
     */
    String candNom();
    
    /**
     * @return le prénom du candidat
     */
    String candPrenom();
    
    /**
     * @return la date de naissance
     */
    String candDateNais();
    
    /**
     * @return la date de naissance sous forme d'objet date plutot que de string
     */
    Date dateNaissance();
    
    /**
     * @return le dernier enseignement suivi
     */
    String histensDerEtab();
    
    /**
     * @return la ville du dernier établissement fréquenté
     */
    String histvilleDerEtab();
    
}
