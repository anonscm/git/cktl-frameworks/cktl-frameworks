/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.List;

import javax.annotation.Nullable;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypePaiement;
import org.cocktail.fwkcktlwebapp.common.UserInfo;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


public class EOPrePaiementMoyen extends _EOPrePaiementMoyen implements IPrePaiementMoyen {

	@Inject
	@Nullable
	private UserInfo userInfo;

    public EOPrePaiementMoyen() {
        super();
    }

    @Override
    public void awakeFromInsertion(EOEditingContext editingContext) {
    	super.awakeFromInsertion(editingContext);
    	setEtudiantTitulaire(Boolean.TRUE);
    }
    
    /**
     * {@inheritDoc}
     */
	public void setToPrePaiementRelationship(IPrePaiement value) {
		super.setToPrePaiementRelationship((EOPrePaiement) value);
	}
	
    /**
     * {@inheritDoc}
     */
	public void setToPaiementRelationship(IPaiement value) {
		super.setToPrePaiementRelationship((EOPrePaiement) value);
	}
	
    /**
     * {@inheritDoc}
     */
    public IPaiement toPaiement() {
		return toPrePaiement();
	}

    /**
     * {@inheritDoc}
     */
	public List<? extends IPaiementEcheance> toPaiementEcheances() {
		return toPrePaiementEcheances();
	}

    /**
	 * {@inheritDoc}
	 */
	public List<? extends IPaiementEcheance> toPaiementEcheancesTriees() {
		return super.toPrePaiementEcheances(null, EOPrePaiementEcheance.DATE_ECHEANCE.ascs(), false);
	}
	
	/**
     * {@inheritDoc}
     */
	public void setToTypePaiementRelationship(ITypePaiement value) {
		super.setToTypePaiementRelationship((EOTypePaiement) value);
	}
	
    /**
     * {@inheritDoc}
     */
	public void setToModePaiementRelationship(IModePaiement value) {
		super.setToModePaiementRelationship((EOModePaiement) value);
	}
	
    /**
     * {@inheritDoc}
     */
	public void setToPreAdresseTitulaireRelationship(IPreAdresse preAdresse) {
		super.setToPreAdresseTitulaireRelationship((EOPreAdresse) preAdresse);
	}
	
	/**
	 * @return la liste des échéances triées par date d'échéances
	 */
	public NSArray<EOPrePaiementEcheance> toPrePaiementEcheancesTriees() {
		return super.toPrePaiementEcheances(null, EOPrePaiementEcheance.DATE_ECHEANCE.ascs(), false);
	}
	
    /** 
     * {@inheritDoc}
     */
    public boolean isDiffere() {
        return toTypePaiement().isDiffere();
    }

    /** 
     * {@inheritDoc}
     */
    public boolean isComptant() {
        return toTypePaiement().isComptant();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isCheque() {
    	return toModePaiement().isCheque();
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isVirement() {
    	return toModePaiement().isVirement();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isPrelevement() {
    	return toModePaiement().isPrelevement();
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isPaiement() {
    	return montantPaye().signum() >= 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean isRemboursement() {
    	return montantPaye().signum() < 0;
    }
    
    /**
     * {@inheritDoc}
     */
    public boolean verifierInfosMinimalesRib() {
        return iban() != null && bic() != null && nomTireur() != null && prenomTireur() != null;
    }

	/**
	 * {@inheritDoc}
	 */
    public IAdresse toAdresseTitulaire() {
    	return super.toPreAdresseTitulaire();
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setToCiviliteTitulaireRelationship(ICivilite civilite) {
		super.setToCiviliteTitulaireRelationship((EOCivilite) civilite);
	}
	
	/**
	 * {@inheritDoc}
	 */
    public void copierInformationsTitulaireCompteDepuis(IPaiementMoyen moyenPaiementSource) {
    	if (moyenPaiementSource instanceof IPrePaiementMoyen) {
    		setNomTireur(moyenPaiementSource.nomTireur());
    		setPrenomTireur(moyenPaiementSource.prenomTireur());
    		setIban(moyenPaiementSource.iban());
    		setBic(moyenPaiementSource.bic());
    		setEtudiantTitulaire(moyenPaiementSource.etudiantTitulaire());
    		setIdPersonneTitulaire(((IPrePaiementMoyen) moyenPaiementSource).idPersonneTitulaire());
    		setToCiviliteTitulaireRelationship(((IPrePaiementMoyen) moyenPaiementSource).toCiviliteTitulaire());
    		setToPreAdresseTitulaireRelationship(((IPrePaiementMoyen) moyenPaiementSource).toPreAdresseTitulaire());
    	} else {
    		throw new UnsupportedOperationException("On ne peut copier les informations du titulaire du compte que depuis/vers un EOPrePaiementMoyen");
    	}
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForInsert();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validateForUpdate() throws ValidationException {
		if (userInfo != null && userInfo.persId() != null) {
			actualiserInfoEnregistrement(userInfo.persId().intValue());
		}
		super.validateForUpdate();
	}

	/**
	 * Met à jour les données d'audit de l'enregistrement.
	 * @param utilisateurPersId identifiant de l'utilisateur
	 */
	public void actualiserInfoEnregistrement(Integer utilisateurPersId) {
		if (persIdCreation() == null) {
			setPersIdCreation(utilisateurPersId);
			setDCreation(new NSTimestamp());
		}
		setPersIdModification(utilisateurPersId);
		setDModification(new NSTimestamp());
	}
}
