package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;

import com.webobjects.foundation.NSArray;

public interface IPrePaiementMoyen extends IPaiementMoyen {

	public IPrePaiement toPrePaiement();

	public void setToPrePaiementRelationship(IPrePaiement value);
	
	
	public void setToPreAdresseTitulaireRelationship(IPreAdresse preAdresse);

	IPreAdresse toPreAdresseTitulaire();
	
	NSArray<EOPrePaiementEcheance> toPrePaiementEcheances();
	
	/**
	 * @return la liste des échéances triées par date d'échéances
	 */
	List<? extends IPrePaiementEcheance> toPrePaiementEcheancesTriees();

    /**
     * @return le persId du titulaire du rib
     */
    Integer idPersonneTitulaire();
    
    /**
     * @param id l'id du titulaire
     */
    void setIdPersonneTitulaire(Integer id);
    
    /**
     * @return la civilité
     */
    ICivilite toCiviliteTitulaire();
    
    /**
     * @param civilite la civilité
     */
    void setToCiviliteTitulaireRelationship(ICivilite civilite);

}