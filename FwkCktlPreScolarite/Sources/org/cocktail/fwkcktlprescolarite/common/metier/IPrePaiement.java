package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.List;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;

/**
 *  Représente un paiement avant l'inscription
 */
public interface IPrePaiement extends IPaiement {

	/**
	 * @return relation vers les infos relatives à l'étudiant et à l'année
	 */
	IPreEtudiantAnnee toPreEtudiantAnnee();

	/**
	 * @return  détails du paiement
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetails();

	/**
	 * @return les articles détail du paiement triés avec le tri par défaut
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailsTries();

	/**
	 * @return détail du paiement relatifs aux formations
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailFormations();

	/**
	 * @return détail du paiement relatifs aux diplomes
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailDiplomes();
	
	/**
	 * @return détail du paiement relatifs aux articles complémentaires
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailArticlesComplementaires();
	
	/**
	 * @return détail du paiement relatifs aux articles générés automatiquement
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailArticlesAuto();
	
	/**
	 * @return détail du paiement relatifs aux articles générés manuellement
	 */
	List<? extends IPrePaiementDetail> toPrePaiementDetailArticlesManuel();
	
	/**
	 * @return moyens de payement 
	 */
	List<? extends IPrePaiementMoyen> toPrePaiementMoyens();	
	
	/**
	 * @return Le moyen de paiement prélèvement
	 */
	IPrePaiementMoyen toPrePaiementMoyenPrelevement();
}