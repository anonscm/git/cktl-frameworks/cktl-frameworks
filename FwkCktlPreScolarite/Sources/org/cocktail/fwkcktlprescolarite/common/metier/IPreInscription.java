package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeInscription;

/**
 *
 * Représentation d'une préinscription
 *
 */
public interface IPreInscription  extends IInscription {
	
	/**
	 * @return le préEtudiant concerné par la préinscription
	 */
	IEtudiant toEtudiant();
	
	
	/**
	 * @param unParcoursType un parcours type (de niveau diplôme) pour cette inscription
	 */
	void setToParcoursDiplomeRelationship(IParcours unParcoursType);
	
	/**
	 * @return le parcours (sous le niveau année) de cette inscription
	 */
	IParcours toParcoursAnnee();
	
	/**
	 * @param unParcours un parcours (sous le niveau année) pour cette inscription
	 */
	void setToParcoursAnneeRelationship(IParcours unParcours);
	
	/**
	 * 
	 * @return le régime d'inscription
	 */
	IRegimeInscription toRegimeInscription();


	/**
	 * @param preEtudiant l'étudiant en cours d'inscription
	 */
	void setToPreEtudiantRelationship(IPreEtudiant preEtudiant);


	
	
}
