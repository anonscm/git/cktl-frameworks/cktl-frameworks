/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreTelephone.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreTelephone extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreTelephone.class);

	public static final String ENTITY_NAME = "EOPreTelephone";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_TELEPHONE";


// Attribute Keys
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> INDICATIF = new ERXKey<Integer>("indicatif");
  public static final ERXKey<String> NO_TELEPHONE = new ERXKey<String>("noTelephone");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFCATION = new ERXKey<Integer>("persIdModifcation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel> TO_TYPE_NO_TEL = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel>("toTypeNoTel");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel> TO_TYPE_TELS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel>("toTypeTels");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreTelephone";

	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String INDICATIF_KEY = "indicatif";
	public static final String NO_TELEPHONE_KEY = "noTelephone";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFCATION_KEY = "persIdModifcation";

// Attributs non visibles
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String ID_PRE_TELEPHONE_KEY = "idPreTelephone";
	public static final String TYPE_NO_KEY = "typeNo";
	public static final String TYPE_TEL_KEY = "typeTel";

//Colonnes dans la base de donnees
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String INDICATIF_COLKEY = "INDICATIF";
	public static final String NO_TELEPHONE_COLKEY = "NO_TELEPHONE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFCATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String ID_PRE_TELEPHONE_COLKEY = "ID_PRE_TELEPHONE";
	public static final String TYPE_NO_COLKEY = "TYPE_NO";
	public static final String TYPE_TEL_COLKEY = "TYPE_TEL";


	// Relationships
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";
	public static final String TO_TYPE_NO_TEL_KEY = "toTypeNoTel";
	public static final String TO_TYPE_TELS_KEY = "toTypeTels";



	// Accessors methods
  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer indicatif() {
    return (Integer) storedValueForKey(INDICATIF_KEY);
  }

  public void setIndicatif(Integer value) {
    takeStoredValueForKey(value, INDICATIF_KEY);
  }

  public String noTelephone() {
    return (String) storedValueForKey(NO_TELEPHONE_KEY);
  }

  public void setNoTelephone(String value) {
    takeStoredValueForKey(value, NO_TELEPHONE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModifcation() {
    return (Integer) storedValueForKey(PERS_ID_MODIFCATION_KEY);
  }

  public void setPersIdModifcation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFCATION_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel toTypeNoTel() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel)storedValueForKey(TO_TYPE_NO_TEL_KEY);
  }

  public void setToTypeNoTelRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel oldValue = toTypeNoTel();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_NO_TEL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_NO_TEL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel toTypeTels() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel)storedValueForKey(TO_TYPE_TELS_KEY);
  }

  public void setToTypeTelsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel oldValue = toTypeTels();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_TELS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_TELS_KEY);
    }
  }
  

/**
 * Créer une instance de EOPreTelephone avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreTelephone createEOPreTelephone(EOEditingContext editingContext, NSTimestamp dCreation
, String noTelephone
, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant, org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel toTypeNoTel, org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel toTypeTels			) {
    EOPreTelephone eo = (EOPreTelephone) createAndInsertInstance(editingContext, _EOPreTelephone.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setNoTelephone(noTelephone);
    eo.setToPreEtudiantRelationship(toPreEtudiant);
    eo.setToTypeNoTelRelationship(toTypeNoTel);
    eo.setToTypeTelsRelationship(toTypeTels);
    return eo;
  }

  
	  public EOPreTelephone localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreTelephone)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreTelephone creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreTelephone creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreTelephone object = (EOPreTelephone)createAndInsertInstance(editingContext, _EOPreTelephone.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreTelephone localInstanceIn(EOEditingContext editingContext, EOPreTelephone eo) {
    EOPreTelephone localInstance = (eo == null) ? null : (EOPreTelephone)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreTelephone#localInstanceIn a la place.
   */
	public static EOPreTelephone localInstanceOf(EOEditingContext editingContext, EOPreTelephone eo) {
		return EOPreTelephone.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreTelephone fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreTelephone fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreTelephone> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreTelephone eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreTelephone)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreTelephone fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreTelephone> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreTelephone eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreTelephone)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreTelephone fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreTelephone eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreTelephone ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreTelephone fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
