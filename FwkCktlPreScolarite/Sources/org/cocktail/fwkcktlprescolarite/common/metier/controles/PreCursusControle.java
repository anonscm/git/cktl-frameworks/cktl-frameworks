/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ia.services.IScoElementListeCursus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.foundation.NSArray;

/**
 * Classe qui permet de controler que les champs obligatoires sont bien remplis
 * @author isabelle
 */
public class PreCursusControle extends AbstractControle {
	
	public static final String PERIODE_INTERRUPTION_SUR_CETTE_PERIODE = "PERIODE_INTERRUPTION_SUR_CETTE_PERIODE";
	public static final String ANNEE_DEBUT_SUPERIEURE_ANNEE_FIN = "ANNEE_DEBUT_SUPERIEURE_ANNEE_FIN";
	public static final String ANNEE_FIN_NON_VALIDE = "ANNEE_FIN_NON_VALIDE";
	public static final String ANNEE_DEBUT_NON_VALIDE = "ANNEE_DEBUT_NON_VALIDE";
	public static final String ETABLISSEMENT_VIDE = "ETABLISSEMENT_VIDE";
	public static final String TYPE_VIDE = "TYPE_VIDE";
	public static final String GRADE_VIDE = "GRADE_VIDE";
	public static final String NIVEAU_VIDE = "NIVEAU_VIDE";
	
	private VerificateurAnnee  verifAnnee = new VerificateurAnnee();
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public PreCursusControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur l'onglet PreCursus.
	 * @param cursus le pré-cursus
	 * @param listePreCursus la liste des pré-cursus de l'étudiant
	 * @param anneeUniversitaire l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(ICursus cursus, List<IScoElementListeCursus> listeElementsPresents, Integer anneeUniversitaire) {
		boolean isOK = true;
		
//		for (int i = 0; listePreCursus.size() > i; i++) {
		for(IScoElementListeCursus elementPresent : listeElementsPresents) {
			if (elementPresent.getCursus() != null) {
				isOK = controleSiPasInteruptionsurPeriode(cursus, elementPresent.getCursus()) && isOK;
			}
		}

		isOK = controleDatePreCursus(cursus, anneeUniversitaire) && isOK;
		isOK = controleChampObligatoires(cursus) && isOK;
			
		return isOK;
	}
	
	/**
	 * Lancer les contrôles sur l'onglet PreCursus.
	 * @param cursus le pré-cursus
	 * @param listePreCursus la liste des pré-cursus de l'étudiant
	 * @param anneeUniversitaire l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controlerPreCursus(EOPreCursus cursus, List<EOPreCursus> listePreCursus, Integer anneeUniversitaire) {
		boolean isOK = true;
		
//		for (int i = 0; listePreCursus.size() > i; i++) {
		for(IPreCursus elementPresent : listePreCursus) {
			isOK = controleSiPasInteruptionsurPeriode(cursus, elementPresent) && isOK;
		}

		isOK = controleDatePreCursus(cursus, anneeUniversitaire) && isOK;
		isOK = controleChampObligatoires(cursus) && isOK;
			
		return isOK;
	}
	
	/**
	 * Controle sur les date de début et fin.
	 * @param preCursus le pré-cursus
	 * @param anneeUniversitaire l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleDatePreCursus(ICursus preCursus, Integer anneeUniversitaire) {
		boolean dateOk = false;
		
		// verif date de debut
		if (preCursus.anneeDebut() != null && verifAnnee.isAnneeValide(preCursus.anneeDebut().toString(), anneeUniversitaire)) {
			dateOk = true;
		} else {
			dateOk = false; 
			getContexte().addErreur(ANNEE_DEBUT_NON_VALIDE, getLocalizedMessage(ANNEE_DEBUT_NON_VALIDE));
		}
		
		if (preCursus.anneeFin() == null || !verifAnnee.isAnneeValide(preCursus.anneeFin().toString(), anneeUniversitaire)) {
			dateOk = false; 
			getContexte().addErreur(ANNEE_FIN_NON_VALIDE, getLocalizedMessage(ANNEE_FIN_NON_VALIDE));
		}
		
		if (preCursus.anneeDebut() == null || preCursus.anneeFin() == null || preCursus.anneeDebut().compareTo(preCursus.anneeFin()) >= 0) {
			dateOk = false;
			getContexte().addErreur(ANNEE_DEBUT_SUPERIEURE_ANNEE_FIN, getLocalizedMessage(ANNEE_DEBUT_SUPERIEURE_ANNEE_FIN));
		}
	
		return dateOk;
		
	}
	
	/**
	 * Contrôle si'il n'y a pas d'interruption sur la période.
	 * @param preCursusNew le pré-cursus
	 * @param preCursus un autre pré-cursus
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleSiPasInteruptionsurPeriode(ICursus preCursusNew, ICursus preCursus) {
		boolean controleOk = false;
		
		if (!preCursus.interruptionEtud()) {
			controleOk = true;
		} else {
			int dateDebutNew = preCursusNew.anneeDebut().intValue();
			int dateFinNew = preCursusNew.anneeFin().intValue();
			int dateDebut = preCursus.anneeDebut().intValue();
			int dateFin = preCursus.anneeFin().intValue();
			if ((dateDebutNew >= dateDebut && dateDebutNew < dateFin) || (dateFinNew > dateDebut && dateFinNew <= dateFin)) {
				controleOk = false;
				getContexte().addErreur(PERIODE_INTERRUPTION_SUR_CETTE_PERIODE, getLocalizedMessage(PERIODE_INTERRUPTION_SUR_CETTE_PERIODE));
			} else {
				controleOk = true;
			}
		}
		
		return controleOk;
	}
	
	
	/**
	 * Contrôle du caractère obligatoire des champs.
	 * @param cursus le pré-cursus
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleChampObligatoires(ICursus cursus) {
		boolean controleOk = true;
		
		if (!cursus.interruptionEtud()) {
			controleOk = controleChampEtablissement(cursus);
			if (cursus.toTypeFormation() == null) {
				getContexte().addErreur(TYPE_VIDE, getLocalizedMessage(TYPE_VIDE));
				controleOk = false;
			}
			
			if (cursus.toGradeUniversitaire() == null) {
				getContexte().addErreur(GRADE_VIDE, getLocalizedMessage(GRADE_VIDE));
				controleOk = false;
			}
			
			if (cursus.niveau() == null) {
				getContexte().addErreur(NIVEAU_VIDE, getLocalizedMessage(NIVEAU_VIDE));
				controleOk = false;
			}
			
		}
		
		return controleOk;
	}
	
	/**
	 * @param cursus le cursus à controler
	 * @return true si l'établissement du cursus est bien renseigné
	 */
	boolean controleChampEtablissement(ICursus cursus) {
		boolean controleOk = true;
		IPays pays = cursus.toPays();
		
		if (pays != null && pays.isFrance()) {
			if (cursus.toCRne() == null) {
				getContexte().addErreur(ETABLISSEMENT_VIDE, getLocalizedMessage(ETABLISSEMENT_VIDE));
				controleOk = false;
			}
		} else if (pays != null && !pays.isFrance()) {
			if (cursus.etabEtranger() == null) {
				getContexte().addErreur(ETABLISSEMENT_VIDE, getLocalizedMessage(ETABLISSEMENT_VIDE));
				controleOk = false;
			}
		}
		return controleOk;
	}
	
	/**
	 * Supprimer tous les objects {@link EOPrecursus} (les mettre à l'état "Delete") de l'editing context, s'ils ne contiennent aucune valeurs significatives.
	 * @param edc l'editing context
	 */
	@SuppressWarnings("unchecked")
  public  void supprimerSiSansValeurs(EOEditingContext edc) {
		for (EOGenericRecord object : (NSArray<EOGenericRecord>) edc.registeredObjects().immutableClone()) {
			if (object instanceof EOPreCursus) {
				((IPreCursus) object).supprimerSiSansValeurs();
			} else if (object instanceof EOCursus) {
				((ICursus) object).supprimerSiSansValeurs();
			}
		}
	}
}
