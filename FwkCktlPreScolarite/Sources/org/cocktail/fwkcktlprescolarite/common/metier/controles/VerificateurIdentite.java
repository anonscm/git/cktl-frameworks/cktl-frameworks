/*
     * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software 
     * is governed by the CeCILL license under French law and abiding by the
     * rules of distribution of free software. You can use, modify and/or 
     * redistribute the software under the terms of the CeCILL license as 
     * circulated by CEA, CNRS and INRIA at the following URL 
     * "http://www.cecill.info". 
     * As a counterpart to the access to the source code and rights to copy, modify 
     * and redistribute granted by the license, users are provided only with a 
     * limited warranty and the software's author, the holder of the economic 
     * rights, and the successive licensors have only limited liability. In this 
     * respect, the user's attention is drawn to the risks associated with loading,
     * using, modifying and/or developing or reproducing the software by the user 
     * in light of its specific status of free software, that may mean that it
     * is complicated to manipulate, and that also therefore means that it is 
     * reserved for developers and experienced professionals having in-depth
     * computer knowledge. Users are therefore encouraged to load and test the 
     * software's suitability as regards their requirements in conditions enabling
     * the security of their systems and/or data to be ensured and, more generally, 
     * to use and operate it in the same conditions as regards security. The
     * fact that you are presently reading this means that you have had knowledge 
     * of the CeCILL license and that you accept its terms.
     */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICivilite;
import org.cocktail.fwkcktlpersonne.common.metier.validateurs.TypageException;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

import er.extensions.foundation.ERXStringUtilities;

public class VerificateurIdentite {

	public VerificateurIdentite() {
		super();
	}
	
	/**
	 * Méthode qui contrôle l'absence d'un blanc au début d'une chaîne de caractères qui est non nulle
	 * @param nomEnParam
	 * @throws TypageException
	 */
	public void controleSansEspaceAuDebut(String nomEnParam) throws TypageException {
		MessageErreur.controleNullite(nomEnParam, "Problème avec le nom !");
		
		if (MyStringCtrl.isChaineSansEspaceDevant(nomEnParam) == false){
			throw new MessageErreur(MessageErreur.MESSAGE_CARACTERE_BLANC_AU_DEBUT);
		}
	}
	
	/**
	 * Méthode qui contrôle qu'il n'y ait pas de caractère non autorisé dans une chaîne
	 * non nulle servant à une dénomination 
	 * @param nomEnParam
	 * @throws TypageException
	 */
	public void controleDenomination(String nomEnParam, String message) throws TypageException {
		MessageErreur.controleNullite(nomEnParam, message);
		
		if (MyStringCtrl.isCaractereNonAutorisePourNom(nomEnParam) == true){
			throw new MessageErreur(MessageErreur.MESSAGE_CARACTERE_NON_VALIDE);
		}
	}
	
	public boolean controleDenominationOK(String nomEnParam) {
		
		return (!MyStringCtrl.isEmpty(nomEnParam) && !MyStringCtrl.isCaractereNonAutorisePourNom(nomEnParam));
		
	}
	
	/**
	 * Méthode ne testant que l'absence de caractère non autorisé
	 * dans un nom patronymique car celui-ci peut être nul.
	 * @param nomEnParam
	 * @throws TypageException
	 */
	public void controleNomPatronymique(String nomEnParam) throws TypageException {
		// On ne fait pas de contrôle de nullité sur le nom patronymique
		if ( ERXStringUtilities.stringIsNullOrEmpty(nomEnParam) && MyStringCtrl.isCaractereNonAutorisePourNom(nomEnParam) == true){
			throw new MessageErreur(MessageErreur.MESSAGE_CARACTERE_NON_VALIDE);
		}
	}
	
	/**
	 * Méthode qui supprime les accents et met tout en majuscule
	 * car le NOM, le PRENOM, le PRENOM2 et le NOM_PATRONYMIQUE suivent cette règle dans le SI
	 * @param chaineEnParam
	 * @return 
	 */
	public String miseEnFormeStringPourBaseDonnees(String chaineEnParam){
		// Méthode à utiliser après avoir effectué les contrôles
		return MyStringCtrl.chaineSansAccents(chaineEnParam).toUpperCase();
	}
	
	/**
	 * Méthode qui se limite à mettre l'initiale en Majuscule sans toucher
	 * au reste de la chaîne de caractères.
	 * Cette méthode est destinée à toutes les String pour l'affichage.
	 * @param chaineEnParam
	 * @return
	 */
	public String miseEnFormeStringPourAffichage(String chaineEnParam){
		// Cette méthode n'est utilisée qu'après avoir contrôlé la chaîne de caractères
		return MyStringCtrl.initCap(chaineEnParam);
	}
	
	/**
	 * Contrôle de la civilité
	 * Vérification que la civilité est en 'accord' avec le sexe de l'individu déduit du N° INSEE
	 */
	public boolean controleCivilite(IInfosEtudiant preEtudiant){
		boolean isCiviliteOk = false;
		
		if (!MyStringCtrl.isEmpty(preEtudiant.civilite()) && !MyStringCtrl.isEmpty(preEtudiant.noInsee())) {
			if (preEtudiant.getCodeInsee().getSexe().equals(preEtudiant.asIndividu().getCivilite().getSexe())) {
				isCiviliteOk = true;
			}
		}
		
		return isCiviliteOk;
	}
	
	/**
	 * Contrôle si épouse
	 */
	public boolean isFemmeMarieeComplete(IInfosEtudiant preEtudiant){
		boolean isMarieeOK = false;
		
		if (preEtudiant.asIndividu().getCivilite() != null && ICivilite.C_CIVILITE_MADAME.equals(preEtudiant.asIndividu().getCivilite().getCode()) ){
			isMarieeOK = controleDenominationOK(preEtudiant.nomPatronymique());
		}
		
		return isMarieeOK;
	}
	
	/**
	 * Vérification que le 1er prénom, le nom d'usage et la civilité sont renseignés
	 */
	public void controleIndentiteRemplie(IInfosEtudiant preEtudiant){
		
		controleDenomination(preEtudiant.nomUsuel(), "Le nom d'usage est OBLIGATOIRE !");
		controleDenomination(preEtudiant.prenom(), "Le prénom est OBLIGATOIRE !");
		if (!preEtudiant.inseeProvisoire() && !controleCivilite(preEtudiant)){
			throw new MessageErreur("La civilité n'est pas correcte et obligatoire. Vérifiez le sexe et vérifiez que la civilité est bien renseignée.");
		}
		
		
	}
	
}
