/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.services.FiltrageFormationsPossiblesEtudiantService;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ComposantRepository;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.ExplorationMaquetteService;

import com.google.inject.Injector;
import com.webobjects.eocontrol.EOEditingContext;
import com.woinject.InjectableApplication;

/**
 * Classe qui permet de controler que les champs obligatoires sont bien remplis
 * @author isabelle
 */
public class FormationEnvisageeControle extends AbstractControle {

	public static final String FROMATION_PRINCIPALE_EXISTE = "FROMATION_PRINCIPALE_EXISTE";
	public static final String DOIT_SELECTIONNER_SPECIALITE = "DOIT_SELECTIONNER_SPECIALITE";
	public static final String DOIT_SELECTIONNER_PARCOURS = "DOIT_SELECTIONNER_PARCOURS";
	public static final String CHAMP_DIPLOME_OBLIGATOIRE = "CHAMP_DIPLOME_OBLIGATOIRE";
	public static final String CHAMP_GRADE_OBLIGATOIRE = "CHAMP_GRADE_OBLIGATOIRE";
	public static final String CHAMP_NIVEAU_OBLIGATOIRE = "CHAMP_NIVEAU_OBLIGATOIRE";
	public static final String CHAMP_TYPE_INSCRIPTION_OBLIGATOIRE = "CHAMP_TYPE_INSCRIPTION_OBLIGATOIRE";
	public static final String TYPE_FORMATION_NON_AUTORISE = "TYPE_FORMATION_NON_AUTORISE";
	public static final String NIVEAU_NON_AUTORISE = "NIVEAU_NON_AUTORISE";
	public static final String ETABLISSEMENT_CUMULATIF_OBLIGATOIRE = "ETABLISSEMENT_CUMULATIF_OBLIGATOIRE"; 
	
	private FiltrageFormationsPossiblesEtudiantService serviceFiltrageFormation;
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public FormationEnvisageeControle(ControlesContexte contexte) {
		super(contexte);
	}
	
	/**
	 * Lancer les contrôles sur une formation envisagée.
	 * @param inscription la pré-inscription à contrôler
	 * @param listeInscription la liste des pré-inscriptions de l'étudiant
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IPreEtudiant preEtudiant, IEtudiant etudiant, IInscription inscription, List<IInscription> listeInscription, EOEditingContext editingContext) {
		boolean isOK = true;
		
		Injector injector = InjectableApplication.application().injector();
		ComposantRepository composantRepository = injector.getInstance(ComposantRepository.class);
		composantRepository.initialiserGrapheComposant(inscription.toDiplome());
		if (preEtudiant != null) {
			serviceFiltrageFormation = new FiltrageFormationsPossiblesEtudiantService(preEtudiant);
		} else if (etudiant != null) {
			serviceFiltrageFormation = new FiltrageFormationsPossiblesEtudiantService(etudiant);
			
		}
		isOK = controleFormationPrincipaleExiste(inscription, listeInscription) && isOK;
		isOK = controleDiplomeInscription(inscription) && isOK;
		isOK = controleSpecialiteInscription(inscription, editingContext) && isOK;
		isOK = controleParcoursInscription(inscription, editingContext) && isOK;
		isOK = controleGradeInscription(inscription, editingContext) && isOK;
		isOK = controleNiveauInscription(inscription, editingContext) && isOK;
		isOK = controleTypeInscriptionInscription(inscription) && isOK;
		// isOK = controleEtablissmentCumulatifObligatoire(preInscription) && isOK;

		return isOK;
	}
	
	/**
	 * 
	 * @param inscription to set
	 * @param listeInscription to set
	 * @return isOk
	 */
	private boolean controleFormationPrincipaleExiste(IInscription inscription, List<IInscription> listeInscription) {
		boolean isOk = true;
		
		if (inscription.toTypeInscriptionFormation() != null && (inscription.toTypeInscriptionFormation().codeInscription().equals(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE) 
				|| inscription.toTypeInscriptionFormation().codeInscription().equals(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL)))  {
			for (int i = 0; listeInscription.size() > i; i++) {
				IInscription inscrip = listeInscription.get(i);
				
				if (!inscription.equals(inscrip)
						&& (inscrip.toTypeInscriptionFormation().codeInscription().equals(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE)
						|| inscrip.toTypeInscriptionFormation().codeInscription().equals(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL))) {
					isOk = false;
					getContexte().addErreur(FROMATION_PRINCIPALE_EXISTE, getLocalizedMessage(FROMATION_PRINCIPALE_EXISTE));
				} 
			}
		}
		
		return isOk;
	}

	/**
	 * Fait un controle sur la specialite par rapport au diplome choisi
	 * @param preInscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleSpecialiteInscription(IInscription inscription, EOEditingContext editingContext) {
		boolean isOk = true;
		
		if (inscription.toDiplome() != null) {
			ExplorationMaquetteService explorationMaquetteService = new ExplorationMaquetteService(editingContext, true);
			List<IParcours> specialites = explorationMaquetteService.getSpecialitesDeVersionDiplome(inscription.toDiplome(), inscription.annee());
			
			if (!specialites.isEmpty()) {
				isOk = (inscription.toParcoursDiplome() != null);
			}
			
			if (!isOk) {
				getContexte().addErreur(DOIT_SELECTIONNER_SPECIALITE, getLocalizedMessage(FormationEnvisageeControle.DOIT_SELECTIONNER_SPECIALITE));
			}
		}
		
		return isOk;
	}
	
	/**
	 * Fait un controle sur le parcours par rapport au diplome choisi
	 * @param inscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleParcoursInscription(IInscription inscription, EOEditingContext editingContext) {
		boolean isOk = true;
		
		if (inscription.toParcoursDiplome() != null) {
			List<IParcours> parcours = new ArrayList<IParcours>();
			
			ExplorationMaquetteService explorationMaquetteService = new ExplorationMaquetteService(editingContext, true);
			IParcours specialite = inscription.toParcoursDiplome();
			if (specialite != null) {
				parcours.addAll(explorationMaquetteService.getParcoursPourSpecialiteEtNiveau(specialite, inscription.niveau()));
			} else {
				parcours.addAll(explorationMaquetteService.getParcoursPourDiplomeEtAnnee(inscription.toDiplome(), inscription.annee(), inscription.niveau()));
			}
			
			if (!parcours.isEmpty()) {
				isOk = (inscription.toParcoursAnnee() != null);
			}
			
			if (!isOk) {
				getContexte().addErreur(DOIT_SELECTIONNER_PARCOURS, getLocalizedMessage(FormationEnvisageeControle.DOIT_SELECTIONNER_PARCOURS));
			}
		}

		return isOk;
	}
	
	/**
	 * Fait un controle sur le diplome 
	 * @param inscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleDiplomeInscription(IInscription inscription) {
		boolean isOk = true;
		
		if (inscription.toDiplome() == null) {
			isOk = false;
			getContexte().addErreur(CHAMP_DIPLOME_OBLIGATOIRE, getLocalizedMessage(CHAMP_DIPLOME_OBLIGATOIRE));
		}
		
		return isOk;
	}
	
	/**
	 * Fait un controle sur le grade
	 * @param preInscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleGradeInscription(IInscription inscription, EOEditingContext editingContext) {
		boolean isOk = true;
		
		if (inscription.toGradeUniversitaire() == null) {
			isOk = false;
			getContexte().addErreur(CHAMP_GRADE_OBLIGATOIRE, getLocalizedMessage(CHAMP_GRADE_OBLIGATOIRE));
		}
		
		if(!getContexte().isImportEtudiant()) {
		
			if (!serviceFiltrageFormation.isGradeUniversitaireAutorise(inscription, editingContext)) {
				isOk = false;
				String libelleGradeUniversitaire = "";
				if (inscription.toDiplome().gradeUniversitaire() != null) {
					libelleGradeUniversitaire = "(" + inscription.toDiplome().gradeUniversitaire().libelle() + ")";
				}
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("LIBELLE_GRADE", libelleGradeUniversitaire);
				getContexte().addErreur(TYPE_FORMATION_NON_AUTORISE, getLocalizedTemplate(TYPE_FORMATION_NON_AUTORISE, values));
			}
		}
		
		return isOk;
	}
	
	/**
	 * Fait un controle sur le type d'inscription
	 * @param preInscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleTypeInscriptionInscription(IInscription inscription) {
		boolean isOk = true;
		
		if (inscription.toTypeInscriptionFormation() == null) {
			isOk = false;
			getContexte().addErreur(CHAMP_TYPE_INSCRIPTION_OBLIGATOIRE, getLocalizedMessage(CHAMP_TYPE_INSCRIPTION_OBLIGATOIRE));
		}
		
		return isOk;
	}
	
	/**
	 * Fait un controle sur le niveau
	 * @param preInscription l'inscription à tester
	 * @return isOk
	 */
	private boolean controleNiveauInscription(IInscription inscription, EOEditingContext editingContext) {
		boolean isOk = true;
		
		if (inscription.niveau() == null) {
			isOk = false;
			getContexte().addErreur(CHAMP_NIVEAU_OBLIGATOIRE, getLocalizedMessage(CHAMP_NIVEAU_OBLIGATOIRE));	
		}
		if(!getContexte().isImportEtudiant()) {

			if (!serviceFiltrageFormation.isNiveauFormationAutorise(inscription, editingContext)) {
				isOk = false;
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("NIVEAU", inscription.niveau());
				getContexte().addErreur(NIVEAU_NON_AUTORISE, getLocalizedTemplate(NIVEAU_NON_AUTORISE, values));
			}
		}
		
		return isOk;
	}
	
	
	private boolean controleEtablissmentCumulatifObligatoire(IInscription inscription) {
		boolean isOK = true;
		
		if (inscription.toTypeInscriptionFormation() != null && EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_CUMULATIF.equals(inscription.toTypeInscriptionFormation().codeInscription())) {
			if (inscription.toEtablissementCumulatif() == null) {
				isOK = false;
				getContexte().addErreur(ETABLISSEMENT_CUMULATIF_OBLIGATOIRE, getLocalizedMessage(ETABLISSEMENT_CUMULATIF_OBLIGATOIRE));
			}
		}
		return isOK;
	}
}
