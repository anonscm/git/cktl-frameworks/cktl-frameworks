package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Cette classe représente les contrôles à appliquer sur l'onglet "Formations envisagées".
 */
public class ListeFormationsEnvisageesControle extends AbstractControle {

	public static final String AU_MOINS_UNE_FORMATION_ENVISAGEE = "AU_MOINS_UNE_FORMATION_ENVISAGEE";
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public ListeFormationsEnvisageesControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur l'onglet "Formations envisagées".
	 * @param preEtudiant un pré-étudiant
	 * @param editingContext 
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IPreEtudiant preEtudiant, IEtudiant etudiant, IEtudiantAnnee etudiantAnnee, EOEditingContext editingContext) {
		boolean isOk = true;
		
		FormationEnvisageeControle formationEnvisageeControle = new FormationEnvisageeControle(getContexte());
		
		List<IInscription> listeInscriptions = (List<IInscription>) etudiantAnnee.toInscriptions();
		
		if (listeInscriptions.isEmpty()) {
			getContexte().addErreur(AU_MOINS_UNE_FORMATION_ENVISAGEE, getLocalizedMessage(AU_MOINS_UNE_FORMATION_ENVISAGEE));
			isOk = false;
		}
		
		for (IInscription inscription : listeInscriptions) {
			isOk = formationEnvisageeControle.controler(preEtudiant, etudiant, inscription, listeInscriptions, editingContext) && isOk;
		}
		
		return isOk;
	}
}
