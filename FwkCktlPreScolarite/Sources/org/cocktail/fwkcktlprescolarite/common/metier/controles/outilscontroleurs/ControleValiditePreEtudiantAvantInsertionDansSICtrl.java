package org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs;

import org.cocktail.fwkcktlprescolarite.common.metier.controles.MessageErreur;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

public class ControleValiditePreEtudiantAvantInsertionDansSICtrl {

	public ControleValiditePreEtudiantAvantInsertionDansSICtrl() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	/* ***** Méthode Générique de Validation ***** */
	public void testLaNullite(EOEnterpriseObject objetATester) {
		String message = objetATester.entityName();
		if (objetATester == null){
			throw new MessageErreur(message + " est un champs qui est vide et qui bloque la validation vers le SI");
		}
		
	}
	
	/**
	 * Nettoie toutes les chaines de l'objet (en effectuant un trim + remplace chaine vide par null). A appeler en debut de ValidateObjectMetier.
	 */
	public static void trimAllString(EOEnterpriseObject obj) {
//		EOEnterpriseObject obj = this;
		NSArray atts = obj.attributeKeys();
		for (int i = 0; i < atts.count(); i++) {
			String key = (String) atts.objectAtIndex(i);
			if (obj.valueForKey(key) != null && obj.valueForKey(key) instanceof String) {
				String value = ((String) obj.valueForKey(key)).trim();
				if (value.length() == 0) {
					value = null;
				}
				obj.takeValueForKey(value, key);
			}
		}
	}

}
