package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class NumeroUaiControle extends AbstractControle{
	
	public static final String UAI_VIDE = "UAI_VIDE";
	public static final String UAI_LONGUEUR_NON_OK = "UAI_LONGUEUR_NON_OK";
	public static final String UAI_DERNIER_CARACTERE_NON_OK = "UAI_DERNIER_CARACTERE_NON_OK";
	public static final String UAI_PREMIERS_CARACTERES_NON_OK = "UAI_PREMIERS_CARACTERES_NON_OK";

	
	public NumeroUaiControle(ControlesContexte contexte) {
		super(contexte);
	}
	
	public boolean controler(String numUai) {
		boolean isOk = true;
		isOk = numUAINonVide(numUai);
		if (!isOk) {
			return isOk;
		}
		isOk = longeurOk(numUai) && isOk;
		if (!isOk) {
			return isOk;
		}
		isOk = dernierCaractereOk(numUai) && isOk;
		isOk = premiersCaractereOk(numUai) && isOk;
		return isOk;
	}

	private boolean premiersCaractereOk(String numUai) {
		if (StringCtrl.toInt(numUai.substring(0, 7), 0) == 0) {
			getContexte().addErreur(UAI_PREMIERS_CARACTERES_NON_OK, getLocalizedMessage(UAI_PREMIERS_CARACTERES_NON_OK));
			return false;
		}
		return true;
	}

	private boolean dernierCaractereOk(String numUai) {
		if (StringCtrl.isBasicLetter(numUai.charAt(7))) {
			return true;
		}
		getContexte().addErreur(UAI_DERNIER_CARACTERE_NON_OK, getLocalizedMessage(UAI_DERNIER_CARACTERE_NON_OK));
		return false;
	}

	private boolean longeurOk(String numUai) {
		if (numUai.length() == 8) {
			return true;
		}
		getContexte().addErreur(UAI_LONGUEUR_NON_OK, getLocalizedMessage(UAI_LONGUEUR_NON_OK));
		return false;
	}

	private boolean numUAINonVide(String numUai) {
		if (StringCtrl.isEmpty(numUai)) {
			getContexte().addErreur(UAI_VIDE, getLocalizedMessage(UAI_VIDE));
			return false;
		}
		
		return true;
	}
}
