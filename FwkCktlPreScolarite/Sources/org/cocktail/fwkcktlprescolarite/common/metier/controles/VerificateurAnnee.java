/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;



import er.extensions.foundation.ERXStringUtilities;

/**
 * C'est une classe pour effectuer des contrôles
 * sur les années renseignées par un étudiant
 * @author alainmalaplate
 *
 */
public class VerificateurAnnee {

	/**
	 * VérificateurAnnee est le constructeur de la classe
	 */
	public VerificateurAnnee() {
		super();
	}
	
	/**
	 * Méthode testant la non nullité et que l'année ne contient que des digits
	 * @param anneeATester une année donnée sous forme de String
	 * @return true si la valeur de l'année est non nulle et ne contient que des chiffres
	 */
	public boolean isYearNotNullAndValide(String anneeATester) {
		MessageErreur.controleNullite(anneeATester, "Problème avec la valeur de l'annee !");

		/**
		 * Expression simplifiée de ERXStringUtilities.isDigitsOnly(anneeATester) == false
		 * pour visualiser immédiatement ce que l'on attend
		 */
		if (!ERXStringUtilities.isDigitsOnly(anneeATester)) {
//			throw new MessageErreur(MessageErreur.MESSAGE_ANNE_AVEC_CARACTERE_NON_NUMERIQUE);
			return false;
		}
		return true;
	}
	
	/**
	 * Vérification que l'année rentrée est inférieure ou égal à l'année universitaire en cours.
	 */
	public boolean isAnneeValide(String anneeATester, Integer anneeUniversitaire ){
		boolean anneeValide = false;
				
		if (isYearNotNullAndValide(anneeATester)){
			int anneeTest = Integer.parseInt(anneeATester);
			if ( (anneeUniversitaire.intValue() >= anneeTest) && (anneeTest > 1900) ){
				anneeValide = true;
			}
		}
		
		return anneeValide;
	}
	
	
}
