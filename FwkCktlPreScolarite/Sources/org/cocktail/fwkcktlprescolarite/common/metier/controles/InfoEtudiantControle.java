/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * Classe qui permet de controler que les champs obligatoires sont bien remplis.
 * @author isabelle
 *
 */
public class InfoEtudiantControle extends AbstractControle {

	private static final String INE_EXISTANT = "INE_EXISTANT";
	private static final String NOM_PRENOM_DNAISS_EXISTANT = "NOM_PRENOM_DNAISS_EXISTANT";
	
	/**
	 * Controle des données
	 * @param contexte 
	 */
	public InfoEtudiantControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur l'onglet Info étudiant
	 * @param etudiant un preEtudiant
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IInfosEtudiant etudiant, IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;
		
		CadreAControle cadreAControle = new CadreAControle(getContexte());
		CadreBControle cadreBControle = new CadreBControle(getContexte());
		CadreTelControle cadreTelControle = new CadreTelControle(getContexte());
		
		isOk = cadreAControle.controler(etudiant, etudiantAnnee) && isOk;
		isOk = cadreBControle.controler(etudiant) && isOk;
		isOk = cadreTelControle.controler(etudiant) && isOk;
		
		return isOk;
	}
	
	/**
	 * 
	 * @param editingContext editingContext
	 * @param preEtudiant preEtudiant
	 * @return true si pas de PReEtudiant trouvé
	 */
	public Boolean controleNonExistance(EOEditingContext editingContext, IPreEtudiant preEtudiant) {
		Boolean isOK = true;
		if (!preEtudiant.ineProvisoire()) {
			EOPreEtudiant preEtudiant2 = EOPreEtudiant.fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(EOPreEtudiant.CODE_INE_KEY, EOQualifier.QualifierOperatorEqual, preEtudiant.codeIne()));
			if (preEtudiant2 != null) {
				getContexte().addErreur(INE_EXISTANT, getLocalizedMessage(INE_EXISTANT));
				isOK = false;
			}
		}
		EOQualifier qualifier = ERXQ.and(new EOKeyValueQualifier(EOPreEtudiant.NOM_PATRONYMIQUE_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, preEtudiant.nomPatronymique()), 
			new EOKeyValueQualifier(EOPreEtudiant.PRENOM_KEY, EOQualifier.QualifierOperatorCaseInsensitiveLike, preEtudiant.prenom()),
			new EOKeyValueQualifier(EOPreEtudiant.D_NAISSANCE_KEY, EOQualifier.QualifierOperatorEqual, preEtudiant.dNaissance()));
		EOPreEtudiant preEtudiant2 = EOPreEtudiant.fetchFirstByQualifier(editingContext, qualifier);
		
		if (preEtudiant2 != null) {
			getContexte().addErreur(NOM_PRENOM_DNAISS_EXISTANT, getLocalizedMessage(NOM_PRENOM_DNAISS_EXISTANT));
			isOK = false;
		}
					
		return isOK;
	}
}
