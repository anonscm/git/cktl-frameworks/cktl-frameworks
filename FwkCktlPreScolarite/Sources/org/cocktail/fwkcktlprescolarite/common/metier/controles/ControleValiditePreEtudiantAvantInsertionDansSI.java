/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;


/**
 * Réalisation via le code de la validité des données
 * en accords avec les règles du SI
 * @author alainmalaplate
 *
 */
public class ControleValiditePreEtudiantAvantInsertionDansSI {

	private EOPreEtudiant preEtudiant;
	private NSMutableArray<String> listeErreurs;
	private VerificateurAnnee verifAnnee = new VerificateurAnnee();

	/**
	 * Constructeur de la classe de contrôle
	 * des champs de EOPreEtudiantAnnee avant
	 * d'insérer dans le SI
	 */
	public ControleValiditePreEtudiantAvantInsertionDansSI() {
		super();
		
	}
	
	/* **************************************** */
	/* ***** Accesseurs sur les variables ***** */ 
	/* **************************************** */

	/**
	 * Getter de preEtudiant
	 * @return l'objet de type EOPretEtudiant
	 */
	public EOPreEtudiant getPreEtudiant() {
		return preEtudiant;
	}

	/**
	 * Setter de preEtudiant
	 * @param preEtudiant est l'objet de type EOPreEtudiant
	 * que l'on souhaite attribuer à la variable preEtudiant.
	 */
	public void setPreEtudiant(EOPreEtudiant preEtudiant) {
		this.preEtudiant = preEtudiant;
	}
	
	
	
	/* **************************************** */
	/* ********* Méthodes de la classe ******** */
	/* **************************************** */
	public void checkDeNonNulliteDesChamps(){
		if (MyStringCtrl.isEmpty(getPreEtudiant().acadCodeBac())) {
			listeErreurs.add("Le code de l'académie du Bac doit être renseigné");
		}
		if (getPreEtudiant().toPaysNaissance().cPays().equals(EOPays.CODE_PAYS_FRANCE) && MyStringCtrl.isEmpty(getPreEtudiant().deptNaissance())) {
			listeErreurs.add("Le département de naissance doit être renseigné");
		}
		
		// Contrôle du renseignement de la Situation Familiale
		if (getPreEtudiant().toSituationFamiliale() == null){
			listeErreurs.add("La situation familiale doit être renseignée.");
		}
		
		// Contrôle du ProCode
		if ( getPreEtudiant().toProfession1() == null){
			listeErreurs.add("La profession du tuteur légal est OBLIGATOIRE.");
		}
		
		// Contrôle de l'année de 1ère Inscr dans l'établissement
		if (!verifAnnee.isAnneeValide(getPreEtudiant().annee1InscEtab().toString(), 
				new Integer(getGrhumParameter(getPreEtudiant().editingContext(), EOGrhumParametres.PARAM_GRHUM_ANNEE_UNIVERSITAIRE)))){
			listeErreurs.add("L'année de première inscription dans l'établissement est obligatoire et doit être inférieure ou égale à l'année universitaire en cours : "
					+ EOGrhumParametres.parametrePourCle(getPreEtudiant().editingContext(), EOGrhumParametres.PARAM_GRHUM_ANNEE_UNIVERSITAIRE));
		}
		
	}
	
	
	protected String getGrhumParameter(EOEditingContext ec, String nomParam) {
		return EOGrhumParametres.parametrePourCle(ec, nomParam);
	}
	
}
