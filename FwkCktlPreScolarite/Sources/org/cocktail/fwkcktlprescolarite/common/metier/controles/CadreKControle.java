/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;

/**
 * Contrôles sur le cadre K.
 * @author isabelle
 */
public class CadreKControle extends AbstractControle {
	
	public static final String PERIODE_INTERRUPTION = "PERIODE_INTERRUPTION";
	public static final String TYPE_HANDICAP = "TYPE_HANDICAP";
	public static final String ASSURANCE_CIVILE = "ASSURANCE_CIVILE";
	private static final String EMAIL_INVALIDE = "EMAIL_INVALIDE";
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public CadreKControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Appel de tous les controles du cadre K.
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;

		isOK = controleInterruptDureeOK(etudiantAnnee) && isOK;
		isOK = controleTypeHandicapOK(etudiantAnnee) && isOK;
		// isOK = controleAssuranceCivileOK(preEtudiantAnnee) && isOK;
		isOK = controleMailValide(etudiantAnnee) && isOK;
		
		return isOK;
	}
	
	/**
	 * Si l'interruption des études est à Oui, la période est obligatoire.
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleInterruptDureeOK(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;
		
		if (etudiantAnnee.interruptEtude() && (etudiantAnnee.dureeInterrupt() == null || etudiantAnnee.dureeInterrupt().intValue() == 0)) {
			getContexte().addErreur(PERIODE_INTERRUPTION, getLocalizedMessage(PERIODE_INTERRUPTION));
			isOK = false;
		}
		
		return isOK;
	}
	
	/**
	 * Si Handicap est à Oui, le type d'handicap est obligatoire.
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleTypeHandicapOK(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;
		
		if (etudiantAnnee.etudHandicap() != null && etudiantAnnee.etudHandicap() && etudiantAnnee.toTypeHandicap() == null) {
			getContexte().addErreur(TYPE_HANDICAP, getLocalizedMessage(TYPE_HANDICAP));
			isOK = false;
		}
		
		return isOK;
	}
	
	/**
	 * Si l'assurance responsabilité civile est à Oui, contrôle que la compagnie et le n° d'assurence soient saisis.
	 * @param etudiantAnnee un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleAssuranceCivileOK(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;
		
		if (etudiantAnnee.assuranceCivile() && (etudiantAnnee.assuranceCivileCie() == null || etudiantAnnee.assuranceCivileNum() == null)) {
			getContexte().addErreur(ASSURANCE_CIVILE, getLocalizedMessage(ASSURANCE_CIVILE));
			isOK = false;
		}
		
		return isOK;
	}
	
	/**
	 * @param etudiantAnnee  informations de l'étudiant sur l'année
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleMailValide(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;
		if (!MyStringCtrl.isEmpty(etudiantAnnee.emailParent()) && !MyStringCtrl.isEmailValid(etudiantAnnee.emailParent())) {
			getContexte().addErreur(EMAIL_INVALIDE, getLocalizedMessage(EMAIL_INVALIDE));
			isOK = false;
		}
		return isOK;
	}
}
