package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlpersonne.common.controles.ControleIban;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementDetail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEC;

/**
 * Cette classe représente les contrôles appliqués sur la page DetailPaiementEtudiant.
 * 
 * @author Pascal MACOUIN
 */
public class DetailPaiementEtudiantControle extends AbstractControle {

	private static final String MODE_PAIEMENT_OBLIGATOIRE = "MODE_PAIEMENT_OBLIGATOIRE";
	private static final String MODE_PAIEMENT_DOIT_ETRE_VIDE = "MODE_PAIEMENT_DOIT_ETRE_VIDE";
	private static final String TYPE_PAIEMENT_OBLIGATOIRE = "TYPE_PAIEMENT_OBLIGATOIRE";
	private static final String MODE_PRELEVEMENT_NON_AUTORISE_POUR_COMPTANT = "MODE_PRELEVEMENT_NON_AUTORISE_POUR_COMPTANT";
	private static final String MODE_PRELEVEMENT_OBLIGATOIRE_POUR_DIFFERE = "MODE_PRELEVEMENT_OBLIGATOIRE_POUR_DIFFERE";
	private static final String SAISIE_CHEQUE_TOUT_OU_RIEN = "SAISIE_CHEQUE_TOUT_OU_RIEN";
	private static final String SAISIE_DETAIL_PRELEVEMENT_OBLIGATOIRE = "SAISIE_DETAIL_PRELEVEMENT_OBLIGATOIRE";
	private static final int IBAN_LONGUEUR_MINIMUM = 15;
	private static final String IBAN_LONGEUR_INCORRECTE = "IBAN_LONGEUR_INCORRECTE";
	private static final String TITULAIRE_OBLIGATOIRE = "TITULAIRE_OBLIGATOIRE";
	private static final String MONTANTS_DETAILS_INCOHERENTS = "MONTANTS_DETAILS_INCOHERENTS";
	private static final String MONTANTS_MOYENS_INCOHERENTS = "MONTANTS_MOYENS_INCOHERENTS";
	private static final String MONTANTS_ECHEANCES_INCOHERENTS = "MONTANTS_ECHEANCES_INCOHERENTS";
	private static final String MOYENS_DANS_PAIEMENTS_DIFFERENTS = "MOYENS_DANS_PAIEMENTS_DIFFERENTS";
	
	private AdresseControle adresseControle;
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public DetailPaiementEtudiantControle(ControlesContexte contexte) {
		super(contexte);
		this.adresseControle = new AdresseControle(contexte);
	}
	
	/**
	 * Appel de tous les controles de la page détail paiement étudiant.
	 * @param paiement un paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IPaiement paiement) {
		boolean isOK = true;

		if (BigDecimal.ZERO.compareTo(paiement.montant()) == 0) {
			isOK = controlerPaiementAZero(paiement) && isOK;
		} else {
			isOK = controlerPaiementDifferentZero(paiement) && isOK;
		}
		
		if (isOK) {
			isOK = controlerCoherenceMontantsPaiementArticlesMoyens(paiement) && isOK;
		}
		
		return isOK;
	}

	/**
	 * Contrôler un paiement d'un montant de zéro.
	 * @param paiement Le paiement
	 * @return <code>true</code> si les contrôles dont ok
	 */
	private boolean controlerPaiementAZero(IPaiement paiement) {
		boolean isOK = true;
		
		if (!paiement.toPaiementMoyens().isEmpty()) {
			getContexte().addErreur(MODE_PAIEMENT_DOIT_ETRE_VIDE, getLocalizedMessage(MODE_PAIEMENT_DOIT_ETRE_VIDE));
			isOK = false;
		}
		
		return isOK;
	}
	
	/**
	 * Contrôler un paiement d'un montant différent de zéro.
	 * @param paiement Le paiement
	 * @return <code>true</code> si les contrôles dont ok
	 */
	private boolean controlerPaiementDifferentZero(IPaiement paiement) {
		boolean isOK = true;
		
		if (paiement.toPaiementMoyens().isEmpty()) {
			getContexte().addErreur(MODE_PAIEMENT_OBLIGATOIRE, getLocalizedMessage(MODE_PAIEMENT_OBLIGATOIRE));
			isOK = false;
		} else {
			for (IPaiementMoyen moyenPaiement : paiement.toPaiementMoyens()) {
				isOK = controlerModeEtTypePaiement(moyenPaiement) && isOK;
				
				if (isOK) {
					isOK = controlerSaisiePaiementParCheque(moyenPaiement) && isOK;
					isOK = controlerSaisiePaiementAvecDebiteur(moyenPaiement) && isOK;
				}
			}
		}
		
		return isOK;
	}
	
	/**
	 * Contrôler la cohérence entre mode et type de paiement.
	 * @param moyenPaiement un moyen de paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerModeEtTypePaiement(IPaiementMoyen moyenPaiement) {
		boolean isOK = true;
		
		// Le mode de paiement est obligatoire
		if (moyenPaiement.toModePaiement() == null) {
			getContexte().addErreur(MODE_PAIEMENT_OBLIGATOIRE, getLocalizedMessage(MODE_PAIEMENT_OBLIGATOIRE));
			isOK = false;
		}
		
		// Le type de paiement est obligatoire
		if (moyenPaiement.toTypePaiement() == null) {
			getContexte().addErreur(TYPE_PAIEMENT_OBLIGATOIRE, getLocalizedMessage(TYPE_PAIEMENT_OBLIGATOIRE));
			isOK = false;
		}
		
		if (isOK) {
			// Pour un paiement comptant, le prélèvement n'est pas autorisé
			if (moyenPaiement.toTypePaiement().isComptant() && moyenPaiement.isPrelevement()) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("MODE_PAIEMENT", moyenPaiement.toModePaiement().libelle().toLowerCase());
				values.put("TYPE_PAIEMENT", moyenPaiement.toTypePaiement().libelle().toLowerCase());
				getContexte().addErreur(MODE_PRELEVEMENT_NON_AUTORISE_POUR_COMPTANT, getLocalizedTemplate(MODE_PRELEVEMENT_NON_AUTORISE_POUR_COMPTANT, values));
				isOK = false;
			}
			
			// Pour un paiement différé, seul le prélèvement est autorisé
			if (moyenPaiement.toTypePaiement().isDiffere() && !moyenPaiement.isPrelevement()) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("MODE_PAIEMENT", moyenPaiement.toModePaiement().libelle().toLowerCase());
				values.put("TYPE_PAIEMENT", moyenPaiement.toTypePaiement().libelle().toLowerCase());
				getContexte().addErreur(MODE_PRELEVEMENT_OBLIGATOIRE_POUR_DIFFERE, getLocalizedTemplate(MODE_PRELEVEMENT_OBLIGATOIRE_POUR_DIFFERE, values));
				isOK = false;
			}
		}
		
		return isOK;
	}
	
	/**
	 * Contrôler les informations obligatoires pour un paiement par chèque.
	 * @param moyenPaiement un moyen de paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerSaisiePaiementParCheque(IPaiementMoyen moyenPaiement) {
		boolean isOK = true;

		if (moyenPaiement.toModePaiement() != null && moyenPaiement.isCheque()) {
			// Si un des champs est renseigné alors tous sont obligatoires
			if (!StringCtrl.isEmpty(moyenPaiement.codeBanque())
					|| !StringCtrl.isEmpty(moyenPaiement.numeroCompte())
					|| !StringCtrl.isEmpty(moyenPaiement.numeroCheque())
					|| !StringCtrl.isEmpty(moyenPaiement.nomTireur())
					|| !StringCtrl.isEmpty(moyenPaiement.prenomTireur())) {
				
				if (StringCtrl.isEmpty(moyenPaiement.codeBanque())
						|| StringCtrl.isEmpty(moyenPaiement.numeroCompte())
						|| StringCtrl.isEmpty(moyenPaiement.numeroCheque())
						|| StringCtrl.isEmpty(moyenPaiement.nomTireur())
						|| StringCtrl.isEmpty(moyenPaiement.prenomTireur())) {
					getContexte().addErreur(SAISIE_CHEQUE_TOUT_OU_RIEN, getLocalizedMessage(SAISIE_CHEQUE_TOUT_OU_RIEN));
					isOK = false;
				}
			}
		}
		
		return isOK;
	}
	
	/**
	 * Contrôler les informations obligatoires pour un paiement avec débiteur (prélèvement ou virement de remboursement).
	 * @param moyenPaiement un moyen de paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerSaisiePaiementAvecDebiteur(IPaiementMoyen moyenPaiement) {
		boolean isOK = true;

		if (moyenPaiement.toModePaiement() != null && (moyenPaiement.isPrelevement() || (moyenPaiement.isVirement() && moyenPaiement.isRemboursement()))) {
			// Tous les champs sont obligatoires
			if (StringCtrl.isEmpty(moyenPaiement.iban())
					|| StringCtrl.isEmpty(moyenPaiement.bic())
					|| StringCtrl.isEmpty(moyenPaiement.nomTireur())
					|| StringCtrl.isEmpty(moyenPaiement.prenomTireur())) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("MODE_PAIEMENT", moyenPaiement.toModePaiement().libelle().toLowerCase());
				getContexte().addErreur(SAISIE_DETAIL_PRELEVEMENT_OBLIGATOIRE, getLocalizedTemplate(SAISIE_DETAIL_PRELEVEMENT_OBLIGATOIRE, values));
				isOK = false;
			}

			// L'IBAN doit au minimum faire 15 caractères (Norvège - https://fr.wikipedia.org/wiki/ISO_13616)
			if (isOK && moyenPaiement.iban().trim().length() < IBAN_LONGUEUR_MINIMUM) {
				getContexte().addErreur(IBAN_LONGEUR_INCORRECTE, getLocalizedMessage(IBAN_LONGEUR_INCORRECTE));
				isOK = false;
			}
			
			if (isOK) {
				isOK = controlerValiditeRib(moyenPaiement);
			}
			
			if (isOK && !moyenPaiement.etudiantTitulaire()) {
			    isOK = controlerValiditerDebiteur(moyenPaiement);
			}
			
		}
		return isOK;
	}

    private boolean controlerValiditerDebiteur(IPaiementMoyen moyenPaiement) {
        if (StringCtrl.isEmpty(moyenPaiement.nomTireur()) 
                || StringCtrl.isEmpty(moyenPaiement.prenomTireur())) {
            getContexte().addErreur(TITULAIRE_OBLIGATOIRE, getLocalizedMessage(TITULAIRE_OBLIGATOIRE));
            return false;
        }
        IAdresse adresseTitulaire = moyenPaiement.toAdresseTitulaire();
        return adresseControle.isAdresseComplete(adresseTitulaire);
    }

    private boolean controlerValiditeRib(IPaiementMoyen moyenPaiement) {
        boolean isOK = true;
        try {
        	EORib rib = EORib.creerInstance(ERXEC.newEditingContext());
        	rib.setIban(moyenPaiement.iban());
        	rib.setBic(moyenPaiement.bic());
        	rib.setRibTitco(moyenPaiement.nomTireur() + " " + moyenPaiement.prenomTireur());
        	
        	ControleIban.verifierFormatIban(rib.iban());
        	
        	if (ControleIban.isCompteFR(rib.iban())) {
        		//Calculer le RIB FR
        		rib.fillRibFRFromIban();
        	}
        	
        	rib.checkRibComplet();
        	
        } catch (NSValidation.ValidationException e) {
        	getContexte().addErreur(null, e.getMessage());
        	isOK = false;
        }
        return isOK;
    }

	/**
	 * Contrôler la cohérence des montants entre le paiement, les articles (le détail) et les moyens de paiement.
	 * @param paiement un paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerCoherenceMontantsPaiementArticlesMoyens(IPaiement paiement) {
		boolean isOK = true;
		
		BigDecimal totalDetail = calculerTotalArticles(paiement);
		BigDecimal totalMoyenPaiement = BigDecimal.ZERO;
		
		for (IPaiementMoyen moyenPaiement : paiement.toPaiementMoyens()) {
			isOK = controlerCoherenceMontantsMoyensEcheances(moyenPaiement) && isOK;
			totalMoyenPaiement = totalMoyenPaiement.add(moyenPaiement.montantPaye());
		}
		
		// Si les totaux sont différents
		if (totalDetail.compareTo(paiement.montant()) != 0) {
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("TOTAL_ARTICLES", totalDetail);
			values.put("TOTAL_MONTANT_PAIEMENT", paiement.montant());
			getContexte().addErreur(MONTANTS_DETAILS_INCOHERENTS, getLocalizedTemplate(MONTANTS_DETAILS_INCOHERENTS, values));
			isOK = false;
		}
		
		if (totalDetail.compareTo(totalMoyenPaiement) != 0) {
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("TOTAL_ARTICLES", totalDetail);
			values.put("TOTAL_MOYENS_PAIEMENT", totalMoyenPaiement);
			getContexte().addErreur(MONTANTS_MOYENS_INCOHERENTS, getLocalizedTemplate(MONTANTS_MOYENS_INCOHERENTS, values));
			isOK = false;
		}
		
		return isOK;
	}

	/**
	 * Contrôler la cohérence des montants entre un moyen de paiement et ces échéances.
	 * @param moyenPaiement un moyen de paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerCoherenceMontantsMoyensEcheances(IPaiementMoyen moyenPaiement) {
		boolean isOK = true;

		// S'il y a des échéances
		if (!moyenPaiement.toPaiementEcheances().isEmpty()) {
			BigDecimal totalEcheancePaiement = BigDecimal.ZERO;
			
			for (IPaiementEcheance echeancePaiement : moyenPaiement.toPaiementEcheances()) {
				isOK = controlerCoherenceMoyensPaiements(echeancePaiement) && isOK;
				
				if (echeancePaiement.toAutrePaiementMoyen() == null) {
					totalEcheancePaiement = totalEcheancePaiement.add(echeancePaiement.montant());
				}
			}
			
			if (totalEcheancePaiement.compareTo(moyenPaiement.montantPaye()) != 0) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("TOTAL_MOYENS_PAIEMENT", moyenPaiement.montantPaye());
				values.put("TOTAL_ECHANCES", totalEcheancePaiement);
				getContexte().addErreur(MONTANTS_ECHEANCES_INCOHERENTS, getLocalizedTemplate(MONTANTS_ECHEANCES_INCOHERENTS, values));
				isOK = false;
			}
		}
		
		return isOK;
	}

	/**
	 * Contrôler que les "autres" moyens de paiement font parti du même paiement.
	 * @param echeancePaiement une échéance de paiement
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerCoherenceMoyensPaiements(IPaiementEcheance echeancePaiement) {
		boolean isOK = true;

		if (echeancePaiement.toAutrePaiementMoyen() != null) {
			if (!echeancePaiement.toAutrePaiementMoyen().toPaiement().equals(echeancePaiement.toPaiementMoyen().toPaiement())) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("MOYEN_PAIEMENT", echeancePaiement.libelle());
				getContexte().addErreur(MOYENS_DANS_PAIEMENTS_DIFFERENTS, getLocalizedTemplate(MOYENS_DANS_PAIEMENTS_DIFFERENTS, values));

				isOK = false;
			}
		}
		
		return isOK;
	}
	
	/**
	 * Calculer le total des articles de paiement.
	 * @param paiement un paiement
	 * @return le total des articles
	 */
	private BigDecimal calculerTotalArticles(IPaiement paiement) {
		BigDecimal totalDetail = BigDecimal.ZERO;

		for (IPaiementDetail detail : paiement.toPaiementDetails()) {
			if (paiement.paiementValide()) {
				if (detail.montantPaye() != null) {
					totalDetail = totalDetail.add(detail.montantPaye());
				}
				if (detail.montantRembourse() != null) {
					totalDetail = totalDetail.subtract(detail.montantRembourse());
				}
			} else {
				if (detail.montantAPayer() != null) {
					totalDetail = totalDetail.add(detail.montantAPayer());
				}
				if (detail.montantARembourser() != null) {
					totalDetail = totalDetail.subtract(detail.montantARembourser());
				}
			}
		}
		return totalDetail;
	}
}
