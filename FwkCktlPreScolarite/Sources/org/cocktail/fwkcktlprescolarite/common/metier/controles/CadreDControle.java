package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

/**
 * Cette classe représente les contrôles à appliquer sur le cadre D.
 */
public class CadreDControle extends AbstractControle {

	private static final String BAC_ANNEE_OBLIGATOIRE = "BAC_ANNEE_OBLIGATOIRE";
	private static final String BAC_TITRE_OBLIGATOIRE = "BAC_TITRE_OBLIGATOIRE";
	private static final String BAC_ETABLISSEMENT_OBLIGATOIRE = "BAC_ETABLISSEMENT_OBLIGATOIRE";
	private static final String ANNEE_TITRE_ACCES_ENSEIGNEMENT_SUPERIEUR_FRANCAIS_SUPERIEURE_ANNEE_UNIVERSITAIRE = "ANNEE_TITRE_ACCES_ENSEIGNEMENT_SUPERIEUR_FRANCAIS_SUPERIEURE_ANNEE_UNIVERSITAIRE";
	private static final String BAC_MENTION_OBLIGATOIRE = "BAC_MENTION_OBLIGATOIRE";
	private static final String ANNEE_TITRE_ACCES_FORMATION_SOUHAITEE_SUPERIEURE_ANNEE_UNIVERSITAIRE = "ANNEE_TITRE_ACCES_FORMATION_SOUHAITEE_SUPERIEURE_ANNEE_UNIVERSITAIRE";

	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public CadreDControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur le cadre D.
	 * @param etudiantAnnee un étudiant année
	 * @param cursus un pré-cursus de l'étudiant
	 * @param anneeUniversitaireEnCours l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IEtudiantAnnee etudiantAnnee, ICursus cursus, Integer anneeUniversitaireEnCours) {
		boolean isOK = true;
		
		isOK = controlerBac(etudiantAnnee, anneeUniversitaireEnCours) && isOK;
		isOK = controlerCursus(cursus, anneeUniversitaireEnCours) && isOK;
		
		return isOK;
	}

	/**
	 * Contrôler le cadre BAC ou équivalent.
	 * @param etudiantAnnee un étudiant année
	 * @param anneeUniversitaireEnCours L'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerBac(IEtudiantAnnee etudiantAnnee, Integer anneeUniversitaireEnCours) {
		boolean isOK = true;
		
		IInfosEtudiant etudiant = etudiantAnnee.toInfosEtudiant();
		
		if (etudiant.etudAnbac() == null) {
			getContexte().addErreur(BAC_ANNEE_OBLIGATOIRE, getLocalizedMessage(BAC_ANNEE_OBLIGATOIRE));
			isOK = false;
		} else if (etudiant.etudAnbac().compareTo(anneeUniversitaireEnCours.longValue()) > 0) {
			getContexte().addErreur(ANNEE_TITRE_ACCES_ENSEIGNEMENT_SUPERIEUR_FRANCAIS_SUPERIEURE_ANNEE_UNIVERSITAIRE, getLocalizedMessage(ANNEE_TITRE_ACCES_ENSEIGNEMENT_SUPERIEUR_FRANCAIS_SUPERIEURE_ANNEE_UNIVERSITAIRE));
			isOK = false;
		}
		
		if (etudiant.toBac() == null) {
			getContexte().addErreur(BAC_TITRE_OBLIGATOIRE, getLocalizedMessage(BAC_TITRE_OBLIGATOIRE));
			isOK = false;
		} else {
			if ((EOBac.BAC_TYPE_BAC.equals(etudiant.toBac().bacType()) || EOBac.BAC_TYPE_BAC_PROFESSIONEL.equals(etudiant.toBac().bacType()))
					&& etudiant.toMention() == null) {
				getContexte().addErreur(BAC_MENTION_OBLIGATOIRE, getLocalizedMessage(BAC_MENTION_OBLIGATOIRE));
				isOK = false;
			}
		}
		
		if (etudiant.toRneEtabBac() == null && etudiantAnnee.etabBacEtranger() == null) {
			getContexte().addErreur(BAC_ETABLISSEMENT_OBLIGATOIRE, getLocalizedMessage(BAC_ETABLISSEMENT_OBLIGATOIRE));
			isOK = false;
		}
		
		return isOK;
	}

	/**
	 * Contrôler le cadre Titre d'accès à la formation souhaitée.
	 * @param cursus le cursus donnant droit à la formation
	 * @param anneeUniversitaireEnCours l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controlerCursus(ICursus cursus, Integer anneeUniversitaireEnCours) {
		boolean isOK = true;
		
		if (cursus != null && cursus.toAccesTitreGradeUniversitaire() != null && cursus.anneeDebut() != null && cursus.anneeDebut().compareTo(anneeUniversitaireEnCours) > 0) {
			getContexte().addErreur(ANNEE_TITRE_ACCES_FORMATION_SOUHAITEE_SUPERIEURE_ANNEE_UNIVERSITAIRE, getLocalizedMessage(ANNEE_TITRE_ACCES_FORMATION_SOUHAITEE_SUPERIEURE_ANNEE_UNIVERSITAIRE));
			isOK = false;
		}
		
		return isOK;
	}
}
