/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;

/**
 * Controles sur le cadre H.
 * @author isabelle
 */
public class CadreHcontrole extends AbstractControle {
	
	public static final String ORGANISME_VIDE = "ORGANISME_VIDE";
	public static final String NUM_ALLOCATAIRE_VIDE = "NUM_ALLOCATAIRE_VIDE";
	public static final String ECHELON_VIDE = "ECHELON_VIDE";
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public CadreHcontrole(ControlesContexte contexte) {
		super(contexte);
	}
	
	/**
	 * Lancer les contrôles sur le cadre H.
	 * @param preBourses une bourse
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IBourses bourses) {
		boolean isOK = true;
		
		isOK = controleChampObligatoires(bourses) && isOK;
			
		return isOK;
	}
	
	private boolean controleChampObligatoires(IBourses bourses) {
		boolean controleOk = true;
		
		if (bourses.organisme() == null) {
			getContexte().addErreur(ORGANISME_VIDE, getLocalizedMessage(ORGANISME_VIDE));
			controleOk = false;
		}
		
		if (bourses.numAllocataire() == null) {
			getContexte().addErreur(NUM_ALLOCATAIRE_VIDE, getLocalizedMessage(NUM_ALLOCATAIRE_VIDE));
			controleOk = false;
		}
		
		if (bourses.toEchelonBourse() == null) {
			getContexte().addErreur(ECHELON_VIDE, getLocalizedMessage(ECHELON_VIDE));
			controleOk = false;
		}
		
		return controleOk;
	}
}
