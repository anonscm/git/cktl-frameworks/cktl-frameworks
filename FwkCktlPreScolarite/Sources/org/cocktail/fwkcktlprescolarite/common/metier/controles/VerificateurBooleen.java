/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;




/**
 * Vérificateur des booléens
 * @author alainmalaplate
 *
 */
public class VerificateurBooleen {
	
	/**
	 * Constructeur de la classe de vérification des booléens
	 */
	public VerificateurBooleen() {
		super();
	}
	
	/**
	 * 
	 * @param booleenATester la valeur du booléen que l'on souhaite tester
	 * @param message le message que l'on souhaite lancer lors de la levée d'exception
	 */
	public void controleDeNonNullite(Boolean booleenATester, String message) {
		if (booleenATester == null) {
			throw new MessageErreur(message + " " + MessageErreur.MESSAGE_BOOLEEN_NULLITE);
		}
	}
	
//	/**
//	 * Méthode qui teste en plus de la non nullité, les seules valeurs possibles
//	 * @param objetATester la valeur du booléen que l'on souhaite tester
//	 * @param message le message que l'on souhaite lancer lors de la levée d'exception
//	 */
//	public void controleTrueouFalse(Boolean objetATester, String message) {
//		if (objetATester != null) {
//			if (!objetATester.equals(Boolean.TRUE)
//					&& !objetATester.equals(Boolean.FALSE)) {
//				throw new MessageErreur(message + " "
//						+ MessageErreur.MESSAGE_NOT_BOOLEEN);
//			}
//		} else {
//			throw new MessageErreur(message + " " + MessageErreur.MESSAGE_BOOLEEN_NULLITE);
//		}
//	}
	
	
}
