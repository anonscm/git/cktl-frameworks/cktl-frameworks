package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;

/**
 * 
 * Contrôle de validité d'une adresse.
 *
 */
public class AdresseControle extends AbstractControle {

    private static final String ADRESSE_1_OBLIGATOIRE = "ADRESSE_1_OBLIGATOIRE";
    private static final String VILLE_OBLIGATOIRE = "VILLE_OBLIGATOIRE";
    private static final String CODE_POSTAL_OBLIGATOIRE = "CODE_POSTAL_OBLIGATOIRE";
    
    /**
     * @param contexte le contexte
     */
    public AdresseControle(ControlesContexte contexte) {
        super(contexte);
    }

    /**
     * Méthode qui vérifie que l'adresse est bien remplie
     * @param adresse adresse complete
     * @return true si l'adresse est complete
     */
    public boolean isAdresseComplete(IAdresse adresse) {

        // la premiere ligne de l'adresse doit être remplie
        if (MyStringCtrl.isEmpty(adresse.adrAdresse1())) {
            getContexte().addErreur(ADRESSE_1_OBLIGATOIRE, getLocalizedMessage(ADRESSE_1_OBLIGATOIRE));
            return false;
        }

        // la ville doit etre remplie
        if (adresse.ville() == null || adresse.ville().isEmpty()) {
            getContexte().addErreur(VILLE_OBLIGATOIRE, getLocalizedMessage(VILLE_OBLIGATOIRE));
            return false;
        }
        // le code postal doit être rempli
        if (adresse.toPays() != null && adresse.toPays().getCode().equals(EOPays.CODE_PAYS_FRANCE)) {
            if ((adresse.codePostal() == null || adresse.codePostal().isEmpty())) {
                getContexte().addErreur(CODE_POSTAL_OBLIGATOIRE, getLocalizedMessage(CODE_POSTAL_OBLIGATOIRE));
                return false;
            }
        } else if ((adresse.cpEtranger() == null || adresse.cpEtranger().isEmpty())) {
            getContexte().addErreur(CODE_POSTAL_OBLIGATOIRE, getLocalizedMessage(CODE_POSTAL_OBLIGATOIRE));
            return false;
        }

        return true;
    }
    
}
