package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

/**
 * Cette classe représente les contrôles à appliquer sur le cadre A.
 */
public class CadreBControle extends AbstractControle {

	private static final String AU_MOINS_UNE_ADRESSE = "AU_MOINS_UNE_ADRESSE";
	private static final String ADRESSE_INCOMPLETE = "ADRESSE_INCOMPLETE";
	private static final String EMAIL_INVALIDE = "EMAIL_INVALIDE";
	private static final String EMAIL_ETUDIANT_OBLIGATOIRE = "EMAIL_ETUDIANT_OBLIGATOIRE";
	private static final String EMAIL_STABLE_OBLIGATOIRE = "EMAIL_STABLE_OBLIGATOIRE";
	private static final String POUR_TYPE_ADRESSE = "POUR_TYPE_ADRESSE";
	private AdresseControle adresseControle;
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public CadreBControle(ControlesContexte contexte) {
		super(contexte);
		adresseControle = new AdresseControle(contexte);
	}

	/**
	 * Lancer les contrôles sur le cadre B
	 * @param etudiant Un étudiant
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IInfosEtudiant etudiant) {
		boolean isOK = true;
		
		List<IAdresse> listeAdresse = etudiant.toAdresses();
		
		if (listeAdresse.size() == 0) {
			getContexte().addErreur(AU_MOINS_UNE_ADRESSE, getLocalizedMessage(AU_MOINS_UNE_ADRESSE));
			return false;
		}	
	
		if ((listeAdresse.size() > 1)) {
			IAdresse adresseStable = null;
			IAdresse adresseEtud = null;
			
			for (int i = 0; listeAdresse.size() > i; i++) {
				IAdresse adresse = listeAdresse.get(i);
				
				isOK = controleAdresse(adresse) && isOK;
				
				if (adresse.isAdresseParent()) {
					adresseStable = adresse;
				} else if (adresse.isAdresseEtudiant()) {
					adresseEtud = adresse;
				}
			}
			
			if (adresseEtud != null && isAdresseVide(adresseEtud)) {
				// L'email de l'adresse stable est obligatoire s'il y a copie dans l'adresse étudiant
				isOK = controleMailStableObligatoire(adresseStable) && isOK;
			}
		}
		
		return isOK;
	}
	
	/**
	 * @param adresse adresse
	 * @return true si l'adresse est ok
	 */
	private boolean controleAdresse(IAdresse adresse) {
		boolean isOK = true;
		
		if (adresse.isAdresseEtudiant() && isAdresseVide(adresse)) {
			return isOK;
		}
		
		if (!isAdresseComplete(adresse)) {
			getContexte().addErreur(ADRESSE_INCOMPLETE, getLocalizedMessage(ADRESSE_INCOMPLETE));
			isOK = false;
		}
		
		// L'e-mail étudiant est obligatoire
		if (adresse.isAdresseEtudiant()) {
			if (MyStringCtrl.isEmpty(adresse.getEmail())) {
				getContexte().addErreur(EMAIL_ETUDIANT_OBLIGATOIRE, getLocalizedMessage(EMAIL_ETUDIANT_OBLIGATOIRE));
				isOK = false;
			}
		}
		
		if (!MyStringCtrl.isEmpty(adresse.getEmail()) && !MyStringCtrl.isEmailValid(adresse.getEmail())) {
			getContexte().addErreur(EMAIL_INVALIDE, getLocalizedMessage(EMAIL_INVALIDE));
			isOK = false;
		}
		
		if(!isOK) {
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("TYPE_ADRESSE", adresse.typeAdresse().tadrLibelle());
			getContexte().addErreur(POUR_TYPE_ADRESSE, getLocalizedTemplate(POUR_TYPE_ADRESSE, values));
		}
		
		return isOK;
	}
	
	/**
	 * @return <code>true</code> si l'adresse est vide
	 */
    private boolean isAdresseVide(IAdresse adresse) {
    	boolean isAdresseVide = false;
    	
		if (adresse.adrAdresse1() == null && adresse.codePostal() == null
				&& adresse.ville() == null && adresse.adrAdresse2() == null
				&& adresse.adrBp() == null
				&& adresse.getEmail() == null) {
			isAdresseVide = true;
		}
		
		return isAdresseVide;
    }
	
	private boolean isAdresseComplete(IAdresse adresse) {
        return adresseControle.isAdresseComplete(adresse);
    }

    /**
	 * L'email stable devient obligatoire si on copie l'adresse stable dans l'adresse étudiant
	 * @param adresseStable adresse stable (parent)
	 * @return true si le mail est ok 
	 */
	private boolean controleMailStableObligatoire(IAdresse adresseStable) {
		boolean isOK = true;		
	
		// L'email stable devient obligatoire si on copie l'adresse stable dans l'adresse étudiant
		if (MyStringCtrl.isEmpty(adresseStable.getEmail())) {
			getContexte().addErreur(EMAIL_STABLE_OBLIGATOIRE, getLocalizedMessage(EMAIL_STABLE_OBLIGATOIRE));
			isOK = false;
		}
		
		return isOK;
	}
	
}
