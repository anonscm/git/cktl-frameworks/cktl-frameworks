/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSMutableArray;

/**
 * Réalisation via le code de la validité des données
 * en accords avec les règles du SI
 * @author alainmalaplate
 *
 */
public class ControleValiditePreEtudiantAnneeAvantInsertionDansSI {
	
	private EOPreEtudiant preEtudiant;
	private VerificateurBooleen verifBooleen;
	private EOPreEtudiantAnnee preEtudiantAnnee;
	private NSMutableArray<String> listeErreurs;
	private SituationSocialeControle verifSituSociale = new SituationSocialeControle(new ControlesContexte());

	/**
	 * Constructeur de la classe de contrôle
	 * des champs de EOPreEtudiantAnnee avant
	 * d'insérer dans le SI
	 */
	public ControleValiditePreEtudiantAnneeAvantInsertionDansSI() {
		super();
		verifBooleen = new VerificateurBooleen();
	}
	
	/* **************************************** */
	/* ***** Accesseurs sur les variables ***** */ 
	/* **************************************** */
	/**
	 * Getter sur l'objet verifBooleen
	 * @return la valeur de verifBooleen
	 */
	public VerificateurBooleen getVerifBooleen() {
		return verifBooleen;
	}

	/**
	 * Setter de verifBooleen
	 * @param verifBooleen setter de l'objet VerifBooleen
	 */
	public void setVerifBooleen(VerificateurBooleen verifBooleen) {
		this.verifBooleen = verifBooleen;
	}

	/**
	 * Getter de preEtudiant
	 * @return l'objet de type EOPretEtudiant
	 */
	public EOPreEtudiant getPreEtudiant() {
		return preEtudiant;
	}

	/**
	 * Setter de preEtudiant
	 * @param preEtudiant est l'objet de type EOPreEtudiant
	 * que l'on souhaite attribuer à la variable preEtudiant.
	 */
	public void setPreEtudiant(EOPreEtudiant preEtudiant) {
		this.preEtudiant = preEtudiant;
	}
	
	
	
	/**
	 * @return the preEtudiantAnnee
	 */
	public EOPreEtudiantAnnee getPreEtudiantAnnee() {
		return preEtudiantAnnee;
	}

	/**
	 * @param preEtudiantAnnee the preEtudiantAnnee to set
	 */
	public void setPreEtudiantAnnee(EOPreEtudiantAnnee preEtudiantAnnee) {
		this.preEtudiantAnnee = preEtudiantAnnee;
	}

	/* **************************************** */
	/* ********* Méthodes de la classe ******** */
	/* **************************************** */
	/**
	 * Méthode qui checke les attributs
	 * de EOPreEtudiant qui sont des booleéns
	 */
	public void checkDesTemoins() {
		getVerifBooleen().controleDeNonNullite(getPreEtudiant().enfantsCharge(), "Case à cocher d'Enfant(s) à charge");
		getVerifBooleen().controleDeNonNullite(getPreEtudiant().ineProvisoire(), "Case à cocher de l'INE provisoire");
		getVerifBooleen().controleDeNonNullite(getPreEtudiant().inseeProvisoire(), "Case à cocher du Numéro INSEE provisoire");
		getVerifBooleen().controleDeNonNullite(getPreEtudiant().sportHN(), "Case à cocher pour Sportif de Haut Niveau");
		
	}
	
	public void checkDeNonNulliteDesChamps(){
		
		// Contrôle du ProCode
		if (getPreEtudiantAnnee().toProfession1() == null) {
			listeErreurs.add("La profession du tuteur légal est OBLIGATOIRE.");
		}
		
		// controle origine des ressources 
		if (getPreEtudiantAnnee().toOrigineRessources() == null) {
			listeErreurs.add("L'origine des ressources est obligatoire.");
		}
		
		// controle situation professionnelle
		if (getPreEtudiantAnnee().toSituationProfessionnelle() == null) {
			listeErreurs.add("la situation professionnelle est obligatoire");
		}
		
		// Controle sur le nom des tuteurs 
		if (!verifSituSociale.controleNomPrenomTuteur1Ok(getPreEtudiantAnnee())) {
			listeErreurs.add("Le nom du tuteur légal est obligatoire ");
		}
		
		// on controle que le nom du tuteur 2 s'il est renseigné ne comporte pas de carcatères non autorisés
		if (!StringCtrl.isEmpty(getPreEtudiantAnnee().tuteur2()) && !verifSituSociale.controleNomPrenomTuteur2Ok(getPreEtudiantAnnee())) {
			listeErreurs.add("Le nom du tuteur 2 comporte des caractères non autorisésl est obligatoire ");
			
		}
	}

}
