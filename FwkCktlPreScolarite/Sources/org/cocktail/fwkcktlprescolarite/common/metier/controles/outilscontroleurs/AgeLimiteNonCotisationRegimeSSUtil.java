package org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs;

import java.util.GregorianCalendar;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.foundation.NSTimestamp;

/**
 * Classe utilitaire permettant de calculer si l'étudiant doit (ou non) être affilié et cotiser au régime SS.
 * <p>
 * Si l'étudiant a 20 ans ou plus à la fin de l'année universitaire (au 30/09/2014 pour l'année universitaire 2013/2014)
 * il doit être affilier et cotiser au régime de la SS.
 */
public class AgeLimiteNonCotisationRegimeSSUtil {

	private NSTimestamp dateNaissanceLimiteNonCotisationRegimeSS;
	private NSTimestamp dateNaissanceLimitePlus28Ans;
	
	/**
	 * Constructeur.
	 * @param anneeUniversitaire l'année universitaire
	 */
	public AgeLimiteNonCotisationRegimeSSUtil(int anneeUniversitaire) {
		dateNaissanceLimiteNonCotisationRegimeSS = new NSTimestamp(new GregorianCalendar(anneeUniversitaire - 19, 8, 30).getTime());
		dateNaissanceLimitePlus28Ans = new NSTimestamp(new GregorianCalendar(anneeUniversitaire - 28, 8, 30).getTime());
	}
	
	public NSTimestamp getDateNaissanceLimiteNonCotisationRegimeSS() {
		return dateNaissanceLimiteNonCotisationRegimeSS;
	}

	public NSTimestamp getDateNaissanceLimitePlus28Ans() {
		return dateNaissanceLimitePlus28Ans;
	}
	
	/**
	 * Est-ce qu'il aura 20 ans ou plus en fin d'année universitaire ?
	 * @param dateNaissance une date de naissance
	 * @return <code>true</code> s'il aura 20 ans ou plus en fin d'année universitaire
	 */
	public boolean has20AnsOuPlus(NSTimestamp dateNaissance) {
		if (DateCtrl.isAfter(dateNaissance, getDateNaissanceLimiteNonCotisationRegimeSS())) {
			return false;
		}
		
		return true;
	}
	
	/**
	 * Est-ce qu'il aura plus de 28 ans en fin d'année universitaire ?
	 * @param dateNaissance une date de naissance
	 * @return <code>true</code> s'il aura plus de 28 ans en fin d'année universitaire
	 */
	public boolean hasPlus28Ans(NSTimestamp dateNaissance) {
		if (DateCtrl.isAfter(dateNaissance, getDateNaissanceLimitePlus28Ans())) {
			return false;
		}
		
		return true;
	}

	/**
	 * Est-ce qu'il aura plus de 28 ans en fin d'année universitaire passée en paramètre ?
	 * @param dateNaissance une date de naissance
	 * @param anneeUniversitaire année universitaire
	 * @return <code>true</code> s'il aura plus de 28 ans en fin d'année universitaire
	 */
	public boolean hasPlus28Ans(NSTimestamp dateNaissance, int anneeUniversitaire) {
		NSTimestamp dateNaissanceLimite = new NSTimestamp(new GregorianCalendar(anneeUniversitaire - 28, 8, 30).getTime());

		if (DateCtrl.isAfter(dateNaissance, dateNaissanceLimite)) {
			return false;
		}
		
		return true;
	}
}
