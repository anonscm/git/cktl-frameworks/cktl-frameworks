package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuCondition;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.IIndividuControle;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeAlgerieFrancaiseEtProtectoratMarocain;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeAnneeNaissance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeCodePaysEtranger;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeCorse;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeDepartementNaissanceFrance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeMoisNaissance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseePaysNaissanceFrance;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeProtectoratTunisien;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeSeineEtSeineEtOise;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.InseeSexe;
import org.cocktail.fwkcktlpersonne.common.metier.individu.controles.ResultatControle;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.services.IndividuServiceImpl;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSValidation.ValidationException;

/**
 * Cette classe représente les contrôles à appliquer sur le cadre A.
 */
public class CadreAControle extends AbstractControle {

	public static final int TAILLE_CODE_INE = 11;
	public static final int TAILLE_CODE_INSEE = 13;

	public static final String CODE_INE_OBLIGATOIRE = "CODE_INE_OBLIGATOIRE";
	public static final String TAILLE_CODE_INE_INCORRECT = "TAILLE_CODE_INE_INCORRECT";
	public static final String CODE_INE_CARACTERE_ALPHANUMERIQUE = "CODE_INE_CARACTERE_ALPHANUMERIQUE";
	public static final String CODE_INSEE_OBLIGATOIRE = "CODE_INSEE_OBLIGATOIRE";
	public static final String CLE_INSEE_INCORRECTE = "CLE_INSEE_INCORRECTE";
	public static final String TAILLE_INSEE_INCORRECTE = "TAILLE_INSEE_INCORRECTE";
	public static final String NB_ENFANTS_CHARGE_OBLIGATOIRE = "NB_ENFANTS_CHARGE_OBLIGATOIRE";
	public static final String NB_ENFANTS_CHARGE_HORS_LIMITE = "NB_ENFANTS_CHARGE_HORS_LIMITE";

	public static final String NOM_FAMILLE_INCORRECT = "NOM_FAMILLE_INCORRECT";
	public static final String PRENOM_INCORRECT = "PRENOM_INCORRECT";
	public static final String CIVILITE_OBLIGATOIRE = "CIVILITE_OBLIGATOIRE";
	public static final String CIVILITE_INCORRECTE = "CIVILITE_INCORRECTE";
	private static final String ALPHANUMERIC_CHAR_SET = "abcdefghijklmnopqrstuvwxyz0123456789";
	public static final String DATE_NAISSANCE_OBLIGATOIRE = "DATE_NAISSANCE_OBLIGATOIRE";
	public static final String DEPARTEMENT_NAISSANCE_OBLIGATOIRE = "DEPARTEMENT_NAISSANCE_OBLIGATOIRE";
	public static final String VILLE_NAISSANCE_OBLIGATOIRE = "VILLE_NAISSANCE_OBLIGATOIRE";
	public static final String SITUATION_FAMILLIALE_OBLIGATOIRE = "SITUATION_FAMILLIALE_OBLIGATOIRE";
	public static final String SITUATION_PROFESSIONNELLE_COHERENTE = "SITUATION_PROFESSIONNELLE_COHERENTE";

	private InseeControle inseeControle;

	/**
	 * Constructeur.
	 * 
	 * @param contexte
	 *            un contexte des contrôles
	 */
	public CadreAControle(ControlesContexte contexte) {
		super(contexte);
		inseeControle = new InseeControle();
	}

	/**
	 * Lancer les contrôles sur le cadre A
	 * 
	 * @param etudiant
	 *            un preEtudiant
	 * @param etudiantAnnee
	 *            un preEtudiantAnnee
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IInfosEtudiant etudiant, IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;

		isOK = controleIdentiteRemplie(etudiant) && isOK;
		isOK = controleINEAlphanumerique(etudiant) && isOK;
		isOK = controleInsee(etudiant) && isOK;
		isOK = controleEnfantsACharge(etudiantAnnee) && isOK;
		isOK = controleSituationProfessionnelleCoherente(etudiantAnnee) && isOK;

		return isOK;
	}

	/**
	 * attention le controle n'est pas le meme que dans la situation sociale
	 * 
	 * @param preEtudiantAnnee
	 *            les informations du pre-étudiant relatives à l'année
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controleSituationProfessionnelleCoherente(IEtudiantAnnee preEtudiantAnnee) {
		if (preEtudiantAnnee.toSituationProfessionnelle() != null) {
			if ((preEtudiantAnnee.toSituationProfessionnelle().codeSitProf().equals(EOSituationProfessionnelle.CODE_SITPROF_SALARIE) || preEtudiantAnnee.toSituationProfessionnelle().codeSitProf()
					.equals(EOSituationProfessionnelle.CODE_SITPROF_FONCTIONNAIRE))
					&& (preEtudiantAnnee.toQuotiteTravail() != null && preEtudiantAnnee.toQuotiteTravail().qtraCode().equals(EOQuotiteTravail.QTRA_CODE_SANS_OBJET))) {
				getContexte().addErreur(SITUATION_PROFESSIONNELLE_COHERENTE, getLocalizedMessage(SITUATION_PROFESSIONNELLE_COHERENTE));
				return false;
			}
		}
		return true;
	}

	/**
	 * @param etudiant
	 *            preEtudiant
	 * @return true si le controle d'identité est ok
	 */
	private boolean controleIdentiteRemplie(IInfosEtudiant etudiant) {
		boolean isOK = true;

		VerificateurIdentite verifIdentite = new VerificateurIdentite();
		if (StringCtrl.isEmpty(etudiant.codeIne()) && !etudiant.ineProvisoire()) {
			getContexte().addErreur(CODE_INE_OBLIGATOIRE, getLocalizedMessage(CODE_INE_OBLIGATOIRE));
			isOK = false;
		}

		if (!StringCtrl.isEmpty(etudiant.codeIne()) && !etudiant.ineProvisoire() && etudiant.codeIne().length() != TAILLE_CODE_INE) {
			Map<String, Object> values = new HashMap<String, Object>();
			values.put("TAILLE_CODE_INE", TAILLE_CODE_INE);
			getContexte().addErreur(TAILLE_CODE_INE_INCORRECT, getLocalizedTemplate(TAILLE_CODE_INE_INCORRECT, values));
			isOK = false;
		}

		if (etudiant.civilite() == null) {
			getContexte().addErreur(CIVILITE_OBLIGATOIRE, getLocalizedMessage(CIVILITE_OBLIGATOIRE));
			isOK = false;
		}

		if (!verifIdentite.controleDenominationOK(etudiant.nomPatronymique())) {
			getContexte().addErreur(NOM_FAMILLE_INCORRECT, getLocalizedMessage(NOM_FAMILLE_INCORRECT));
			isOK = false;
		}

		if (!verifIdentite.controleDenominationOK(etudiant.prenom())) {
			getContexte().addErreur(PRENOM_INCORRECT, getLocalizedMessage(PRENOM_INCORRECT));
			isOK = false;
		}

		if (etudiant.dNaissance() == null) {
			getContexte().addErreur(DATE_NAISSANCE_OBLIGATOIRE, getLocalizedMessage(DATE_NAISSANCE_OBLIGATOIRE));
			isOK = false;
		}

		if (etudiant.toPaysNaissance() != null && etudiant.toPaysNaissance().cPays().equals(EOPays.CODE_PAYS_FRANCE) && etudiant.toDepartement() == null) {
			getContexte().addErreur(DEPARTEMENT_NAISSANCE_OBLIGATOIRE, getLocalizedMessage(DEPARTEMENT_NAISSANCE_OBLIGATOIRE));
			isOK = false;
		}

		if (StringCtrl.isEmpty(etudiant.villeNaissance())) {
			getContexte().addErreur(VILLE_NAISSANCE_OBLIGATOIRE, getLocalizedMessage(VILLE_NAISSANCE_OBLIGATOIRE));
			isOK = false;
		}

		if (etudiant.toSituationFamiliale() == null) {
			getContexte().addErreur(SITUATION_FAMILLIALE_OBLIGATOIRE, getLocalizedMessage(SITUATION_FAMILLIALE_OBLIGATOIRE));
			isOK = false;
		}

		return isOK;
	}

	/**
	 * @param etudiant
	 *            preEtudiant
	 * @return true si le controle insee est ok
	 */
	private boolean controleInsee(IInfosEtudiant etudiant) {
		boolean isOK = true;

		if (!etudiant.inseeProvisoire()) {
			if (etudiant.noInsee() == null || etudiant.noInsee().length() < TAILLE_CODE_INSEE) {
				Map<String, Object> values = new HashMap<String, Object>();
				values.put("TAILLE_CODE_INSEE", TAILLE_CODE_INSEE);
				getContexte().addErreur(TAILLE_INSEE_INCORRECTE, getLocalizedTemplate(TAILLE_INSEE_INCORRECTE, values));
				return false;
			}

			if (etudiant.getCodeInsee() == null) {
				getContexte().addErreur(CODE_INSEE_OBLIGATOIRE, getLocalizedMessage(CODE_INSEE_OBLIGATOIRE));
				return false;
			}

			try {
				List<ResultatControle> listeResultats = inseeControle.appliqueControles(etudiant.asIndividu());
				isOK = inseeControle.convertirResultats(listeResultats, getContexte());
			} catch (ValidationException e) {
				getContexte().addErreur(null, e.getMessage());
				return false;
			}

			VerificateurIdentite verifIdentite = new VerificateurIdentite();
			if (etudiant.civilite() != null && !verifIdentite.controleCivilite(etudiant)) {
				getContexte().addErreur(CIVILITE_INCORRECTE, getLocalizedMessage(CIVILITE_INCORRECTE));
				isOK = false;
			}

			if (!CodeInsee.calculeClefinsee(etudiant.getCodeInsee().getCode()).equals(etudiant.cleInsee())) {
				getContexte().addErreur(CLE_INSEE_INCORRECTE, getLocalizedMessage(CLE_INSEE_INCORRECTE));
				return false;
			}
		}

		return isOK;
	}

	/**
	 * Le nombre d'enfants à charge est obligatoire si on a coché la case
	 * 'Enfant(s) à charge'.
	 * 
	 * @param etudiantAnnee
	 *            un pré-étudiant année
	 * @return <code>true</code> si les contrôles sont ok
	 */
	private boolean controleEnfantsACharge(IEtudiantAnnee etudiantAnnee) {
		boolean isOK = true;

		if (etudiantAnnee.enfantsCharge()) {
			if (etudiantAnnee.nbEnfantsCharge() == null || etudiantAnnee.nbEnfantsCharge() == 0) {
				getContexte().addErreur(NB_ENFANTS_CHARGE_OBLIGATOIRE, getLocalizedMessage(NB_ENFANTS_CHARGE_OBLIGATOIRE));
				isOK = false;
			} else if (etudiantAnnee.nbEnfantsCharge() < 0 || etudiantAnnee.nbEnfantsCharge() > 99) {
				getContexte().addErreur(NB_ENFANTS_CHARGE_HORS_LIMITE, getLocalizedMessage(NB_ENFANTS_CHARGE_HORS_LIMITE));
				isOK = false;
			}
		}

		return isOK;
	}

	/**
	 * @param etudiant
	 *            preEtudiant
	 * @return true si le code INE est alphanumérique
	 */
	private boolean controleINEAlphanumerique(IInfosEtudiant etudiant) {
		boolean isOK = true;

		if (etudiant.codeIne() != null) {
			if (!StringUtils.containsOnly(etudiant.codeIne().toLowerCase(), ALPHANUMERIC_CHAR_SET)) {
				getContexte().addErreur(CODE_INE_CARACTERE_ALPHANUMERIQUE, getLocalizedMessage(CODE_INE_CARACTERE_ALPHANUMERIQUE));
				isOK = false;
			}
		}

		return isOK;
	}

	/**
	 * Classe de contrôles du code INSEE.
	 */
	private class InseeControle extends IndividuServiceImpl {
		public InseeControle() {
			super();

			List<IIndividuControle> controles = new ArrayList<IIndividuControle>();
			controles.add(new InseeSexe());
			controles.add(new InseeAnneeNaissance());
			controles.add(new InseeMoisNaissance());
			controles.add(new InseeCodePaysEtranger());
			controles.add(new InseeProtectoratTunisien());
			controles.add(new InseeAlgerieFrancaiseEtProtectoratMarocain());
			controles.add(new InseeSeineEtSeineEtOise());
			controles.add(new InseeCorse());
			controles.add(new InseePaysNaissanceFrance());
			controles.add(new InseeDepartementNaissanceFrance());
			
			Map<IIndividuCondition,List<IIndividuControle>> conditions = new HashMap<IIndividuCondition,List<IIndividuControle>>();
			conditions.put(IndividuServiceImpl.CONDITION_TJRS_VRAIE, controles);
			setControles(conditions);
		}

		public boolean convertirResultats(List<ResultatControle> listeResultats, ControlesContexte controlesContexte) {
			boolean isOK = true;

			for (ResultatControle resultatControle : listeResultats) {
				if (!resultatControle.valide()) {
					controlesContexte.addErreur(resultatControle.getCode(), resultatControle.getMessage());
					isOK = false;
				}
			}

			return isOK;
		}
	}
}
