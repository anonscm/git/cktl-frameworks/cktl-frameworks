/*
     * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software 
     * is governed by the CeCILL license under French law and abiding by the
     * rules of distribution of free software. You can use, modify and/or 
     * redistribute the software under the terms of the CeCILL license as 
     * circulated by CEA, CNRS and INRIA at the following URL 
     * "http://www.cecill.info". 
     * As a counterpart to the access to the source code and rights to copy, modify 
     * and redistribute granted by the license, users are provided only with a 
     * limited warranty and the software's author, the holder of the economic 
     * rights, and the successive licensors have only limited liability. In this 
     * respect, the user's attention is drawn to the risks associated with loading,
     * using, modifying and/or developing or reproducing the software by the user 
     * in light of its specific status of free software, that may mean that it
     * is complicated to manipulate, and that also therefore means that it is 
     * reserved for developers and experienced professionals having in-depth
     * computer knowledge. Users are therefore encouraged to load and test the 
     * software's suitability as regards their requirements in conditions enabling
     * the security of their systems and/or data to be ensured and, more generally, 
     * to use and operate it in the same conditions as regards security. The
     * fact that you are presently reading this means that you have had knowledge 
     * of the CeCILL license and that you accept its terms.
     */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import com.webobjects.foundation.NSValidation;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Classe des messages d'erreur personnalisés pour les données de PreEtudiant et PreEtudiantAnnee
 * @author alainmalaplate
 *
 */
public class MessageErreur extends NSValidation.ValidationException {
	
	
	/**
	 * 
	 * @author Alain Malaplate
	 * 
	 * Ajout d'une classe contenant en fait le message d'erreur
	 * relatif à l'exception levée dans un vérificateur 
	 */
	private static final long serialVersionUID = 111111111111L;
	
	
	public static final String MESSAGE_NULLITE = "Ce champs ne doit pas être nul.";
	public static final String MESSAGE_CARACTERE_NON_VALIDE = "Seuls les lettres avec ou sans accent, l'apostrophe, l'espace et le tiret sont autorisés !";
	public static final String MESSAGE_CARACTERE_BLANC_AU_DEBUT = "Pas d'espace en début de nom !";
	public static final String MESSAGE_ANNE_AVEC_CARACTERE_NON_NUMERIQUE = "L'année ne doit contenir que des chiffres !";
	
	public static final String MESSAGE_ADRESSE_NON_VALIDE = "Adresse non valide : présence de caractère non alpha numérique ou caractère non français!";
	public static final String MESSAGE_CP_NON_VALIDE = "Code postal non valide : il devrait être formé de 5 chiffres!";
	
	public static final String MESSAGE_BOOLEEN_NULLITE = "Ce booléen ne doit pas être nul car c'est un témoin de status.";
	public static final String MESSAGE_NOT_BOOLEEN = "La valeur ne correspond pas à un booléen";
	
	/**
	 * Méthode Constructeur des Messages d'Erreur
	 * @param message détaiilant l'erreur
	 */
	public MessageErreur(String message) {
		super(message);
	}
	
	
	/**
	 * Test général de non nullité
	 * @param stringEnParam qui est le paramètre à tester
	 * @param message qui associe un message d'erreur au paramètre
	 */
	public static void controleNullite(String stringEnParam, String message) {
		if (ERXStringUtilities.stringIsNullOrEmpty(stringEnParam)) {
			throw new MessageErreur(message + " " + MessageErreur.MESSAGE_NULLITE);
		}
//		if ( stringEnParam == null) {
//			throw new MessageErreur( message + " " + MessageErreur.MESSAGE_NULLITE);
//		}
	}

}
