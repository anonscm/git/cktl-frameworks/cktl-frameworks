package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

/**
 * Cette classe représente les contrôles à appliquer sur le cadre Téléphones.
 */
public class CadreTelControle extends AbstractControle {

	public static final int INDICATIF_TELEPHONIQUE_FRANCE = 33;
	public static final String UN_NUMERO_TELEPHONE_OBLIGATOIRE = "UN_NUMERO_TELEPHONE_OBLIGATOIRE";
	public static final String TELEPHONE_INCORRECT = "TELEPHONE_INCORRECT";
	public static final String TELEPHONE_PAS_FRANCAIS = "TELEPHONE_PAS_FRANCAIS";
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public CadreTelControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur le cadre Téléphones.
	 * 
	 * @param etudiant Un étudiant
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(IInfosEtudiant etudiant) {
		boolean isOK = true;
		
		List<ITelephone> listeTelephones = etudiant.toTelephones();
		
		int nbTelNonVide = 0;
		
		for (ITelephone telephone : listeTelephones) {
			if (!MyStringCtrl.isEmpty(telephone.noTelephone())) {
				nbTelNonVide++;
				isOK = controleTelephone(telephone) && isOK;
			}
		}
		
		if (nbTelNonVide == 0) {
			getContexte().addErreur(UN_NUMERO_TELEPHONE_OBLIGATOIRE, getLocalizedMessage(UN_NUMERO_TELEPHONE_OBLIGATOIRE));
			isOK = false;
		}
		
		return isOK;
	}

	/**
	 * @param telephone tel
	 * @return true si le telephone est ok
	 */
	private boolean controleTelephone(ITelephone telephone) {
		boolean isOK = true;
		if (telephone != null && telephone.noTelephone() != null) {
		
			VerificateurTelephone verifTelephone = new VerificateurTelephone();
			if (!verifTelephone.isPhoneNumber(telephone.noTelephone())) {
				getContexte().addErreur(TELEPHONE_INCORRECT, getLocalizedMessage(TELEPHONE_INCORRECT));
				isOK = false;
			}
			if (telephone.indicatif() != null ) {
				if (INDICATIF_TELEPHONIQUE_FRANCE == telephone.indicatif().intValue() && !verifTelephone.isFrenchPhoneLength(telephone.noTelephone(), telephone.indicatif())) {
					getContexte().addErreur(TELEPHONE_PAS_FRANCAIS, getLocalizedMessage(TELEPHONE_PAS_FRANCAIS));
					isOK = false;
				}
			}
		}
		return isOK;
	}
	
}
