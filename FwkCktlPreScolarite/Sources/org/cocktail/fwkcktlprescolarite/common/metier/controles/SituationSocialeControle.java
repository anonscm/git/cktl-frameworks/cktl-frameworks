/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;

/**
 * Classe qui permet de controler que les champs obligatoires sont bien remplis
 * @author isabelle
 */
public class SituationSocialeControle extends AbstractControle {

	public static final String NOM_TUTEUR1_NON_VALIDE = "NOM_TUTEUR1_NON_VALIDE";
	public static final String NOM_TUTEUR2_NON_VALIDE = "NOM_TUTEUR2_NON_VALIDE";
	public static final String PROFESSION_TUTEUR1 = "PROFESSION_TUTEUR1";
	public static final String PROFESSION_TUTEUR2 = "PROFESSION_TUTEUR2";
	public static final String AFFILIATION_ET_COTISATION_OBLIGATOIRE = "AFFILIATION_ET_COTISATION_OBLIGATOIRE";
	public static final String MOTIF_EXO = "MOTIF_EXO";
	public static final String MOTIF_NON_SECU = "MOTIF_NON_SECU";
	public static final String MUTUELLE_COTIS = "MUTUELLE_COTIS";
	public static final String MOTIF_COTIS_OUI_EXO = "MOTIF_COTIS_OUI_EXO";
	public static final String ORIGINE_RESSOURCES = "ORIGINE_RESSOURCES";
	public static final String SITUATION_PROFESSIONNELLE_COHERENTE = "SITUATION_PROFESSIONNELLE_COHERENTE";
	public static final String MUTUELLE_OBLIGATOIRE = "MUTUELLE_OBLIGATOIRE";
	public static final String COTIS_VIDE = "COTIS_VIDE";
	public static final String REGIME_PARTICULIER_VIDE = "REGIME_PARTICULIER_VIDE";
	
	private VerificateurIdentite verifIdentite = new VerificateurIdentite();

	/**
	 * Constructeur.
	 * @param contexte un contexte des controles
	 */
	public SituationSocialeControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Appel de tous les controles.
	 * @param etudiant : un Etudiant
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @param anneeUniversitaire l'annee universitaire
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controler(IInfosEtudiant etudiant, IEtudiantAnnee etudiantAnnee, int anneeUniversitaire) {
		boolean isOk = true;

		//SituationSocialeControle situControle = new SituationSocialeControle(new ControlesContexte());
		isOk = controleCotisationRempli(etudiantAnnee);
		isOk = controleAffilieSecu(etudiantAnnee) & isOk;
		isOk = controleSituationProfessionnelleCoherente(etudiantAnnee) & isOk;
		isOk = controleMutuellePresente(etudiantAnnee) & isOk;
		isOk = controleNomPrenomTuteur1Ok(etudiantAnnee) & isOk;
		isOk = controleProfessionTuteur1Ok(etudiant, etudiantAnnee) && isOk;
		isOk = controleNomPrenomTuteur2Ok(etudiantAnnee) && isOk;
		isOk = controleProfessionTuteur2Ok(etudiant, etudiantAnnee) && isOk;
		isOk = controleOrigineRessources(etudiantAnnee) && isOk;

		//isOk = controleAffiliation(preEtudiant, preEtudiantAnnee, anneeUniversitaire);
		//isOk = controleMotifExonneration(preEtudiantAnnee) && isOk;
//		isOk = controleOrigineRessources(preEtudiantAnnee) && isOk;
//		isOk = controleNomPrenomTuteur1Ok(preEtudiantAnnee) && isOk;
//		isOk = controleProfessionTuteur1Ok(preEtudiant, preEtudiantAnnee) && isOk;
//		isOk = controleNomPrenomTuteur2Ok(preEtudiantAnnee) && isOk;
//		isOk = controleProfessionTuteur2Ok(preEtudiant, preEtudiantAnnee) && isOk;
//		isOk = controleSituationProfessionnelleCoherente(preEtudiantAnnee) && isOk;

		return isOk;
	}

	/**
	 * Attention le controle n'est pas le meme que dans le cadre A
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleSituationProfessionnelleCoherente(IEtudiantAnnee etudiantAnnee) {
		if (etudiantAnnee.toSituationProfessionnelle() != null) {
			if ((etudiantAnnee.toSituationProfessionnelle().codeSitProf().equals(EOSituationProfessionnelle.CODE_SITPROF_SALARIE)
					|| etudiantAnnee.toSituationProfessionnelle().codeSitProf().equals(EOSituationProfessionnelle.CODE_SITPROF_FONCTIONNAIRE))
			    && (EOCotisation.COTI_CODE_SALARIE.equals(etudiantAnnee.toCotisation()))) {
				getContexte().addErreur(SITUATION_PROFESSIONNELLE_COHERENTE, getLocalizedMessage(SITUATION_PROFESSIONNELLE_COHERENTE));
				return false;
			}
		}
		return true;
	}

	/**
	 * Methode qui controle que le nom prenom du tuteur 1 est non null et valide.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleNomPrenomTuteur1Ok(IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

		if (!verifIdentite.controleDenominationOK(etudiantAnnee.tuteur1())) {
			getContexte().addErreur(NOM_TUTEUR1_NON_VALIDE, getLocalizedMessage(NOM_TUTEUR1_NON_VALIDE));
			isOk = false;
		}

		return isOk;
	}

	/**
	 * Methode qui controle que le nom prenom du tuteur 2 s'il est renseigne est valide.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleNomPrenomTuteur2Ok(IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

		if (etudiantAnnee.tuteur2() != null) {
			if (!verifIdentite.controleDenominationOK(etudiantAnnee.tuteur2())) {
				getContexte().addErreur(NOM_TUTEUR2_NON_VALIDE, getLocalizedMessage(NOM_TUTEUR2_NON_VALIDE));
				isOk = false;
			}
		}

		return isOk;
	}

	/**
	 * Methode qui controle que la profession du tuteur 1 est remplie.
	 * @param etudiant : un Etudiant
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleProfessionTuteur1Ok(IInfosEtudiant etudiant, IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

		//if (etudiantAnnee.tuteur1() != null && verifIdentite.controleDenominationOK(etudiantAnnee.tuteur1())) {
			if (etudiantAnnee.toProfession1() == null || etudiant.toProfession1() == null) {
				getContexte().addErreur(PROFESSION_TUTEUR1, getLocalizedMessage(PROFESSION_TUTEUR1));
				isOk = false;
			}
		//}

		return isOk;
	}

	/**
	 * Methode qui controle que la profession du tuteur 2 est remplie si le nom du tuteur 2 est rempli.
	 * @param etudiant : un Etudiant
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleProfessionTuteur2Ok(IInfosEtudiant etudiant, IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

			if (etudiantAnnee.toProfession2() == null || etudiant.toProfession2() == null) {
				getContexte().addErreur(PROFESSION_TUTEUR2, getLocalizedMessage(PROFESSION_TUTEUR2));
				isOk = false;
			}

		return isOk;
	}

	/**
	 * Methode qui controle que le motif d'exoneration est rempli si oui a ete coche pour exoneration.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleMotifExonneration(IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

		if (etudiantAnnee.boursier()) {
			if (MyStringCtrl.isEmpty(etudiantAnnee.autreCasExo())) {
				getContexte().addErreur(MOTIF_EXO, getLocalizedMessage(MOTIF_EXO));
				isOk = false;
			}
		}

		return isOk;
	}

	/**
	 * Verification si pas affilie au regime "Etudiant" de la securite sociale, le motif doit etre selectionner.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleNonAffilieSecu(IEtudiantAnnee etudiantAnnee) {
		boolean isOk = true;

		if (!etudiantAnnee.affSs() && etudiantAnnee.toCotisation() == null) {
			getContexte().addErreur(MOTIF_NON_SECU, getLocalizedMessage(MOTIF_NON_SECU));
			isOk = false;
		}

		return isOk;
	}

	/**
	 * Verification si affilie au regime, une case cotisation doit etre cochee et si oui, mais exonere est coche le motif doit etre saisi.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return boolean
	 */
	public boolean controleAffilieSecu(IEtudiantAnnee etudiantAnnee) {
		boolean isControleOk = true;

		if (etudiantAnnee.affSs()) {
			if (etudiantAnnee.toMutuelle() == null || etudiantAnnee.toCotisation() == null) {
				getContexte().addErreur(MUTUELLE_COTIS, getLocalizedMessage(MUTUELLE_COTIS));
				isControleOk = false;
			}
//			} else if (etudiantAnnee.toCotisation() != null && etudiantAnnee.toCotisation().cotiCode().equals(EOCotisation.COTI_CODE_OUI_MAIS_EXONERE)) {
//
//				if (StringCtrl.isEmpty(etudiantAnnee.typeRegime())) {
//					getContexte().addErreur(MOTIF_COTIS_OUI_EXO);
//					isControleOk = false;
//				}
//			}
		}

		return isControleOk;
	}

	/**
	 * Methode qui controle que l'origine des ressources est remplie.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleOrigineRessources(IEtudiantAnnee etudiantAnnee) {
		boolean isControleOk = true;

		if (etudiantAnnee.toOrigineRessources() == null && !getContexte().isImportEtudiant()) {
			isControleOk = false;
			getContexte().addErreur(ORIGINE_RESSOURCES, getLocalizedMessage(ORIGINE_RESSOURCES));
		}

		return isControleOk;
	}
	
	/**
	 * Methode qui controle que la cotisation est remplie.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleCotisationRempli(IEtudiantAnnee etudiantAnnee) {
		boolean isControleOk = true;
		if (etudiantAnnee.toCotisation() == null) {
			isControleOk = false;
			getContexte().addErreur(COTIS_VIDE, getLocalizedMessage(COTIS_VIDE));
		} else if (EOCotisation.COTI_CODE_PARENT_REGIME_PARTICULIER.equals(etudiantAnnee.toCotisation().cotiCode())
				&& etudiantAnnee.toRegimeParticulier() == null) {
			// On doit preciser le regime
			isControleOk = false;
			getContexte().addErreur(REGIME_PARTICULIER_VIDE, getLocalizedMessage(REGIME_PARTICULIER_VIDE));
		}
		
		return isControleOk;
	}
	
	/**
	 * Methode qui controle que la mutuelle est remplie.
	 * @param etudiantAnnee : un EtudiantAnnee
	 * @return <code>true</code> si les controles sont ok
	 */
	public boolean controleMutuellePresente(IEtudiantAnnee etudiantAnnee) {
		boolean isControleOk = true;
		if (etudiantAnnee.adhMutuelle() && etudiantAnnee.toMutuelle() == null) {
			isControleOk = false;
			getContexte().addErreur(MUTUELLE_OBLIGATOIRE, getLocalizedMessage(MUTUELLE_OBLIGATOIRE));
		}
		
		return isControleOk;
	}

}
