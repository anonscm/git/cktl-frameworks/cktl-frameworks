package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.Map;

import er.extensions.localization.ERXLocalizer;

/**
 * Cette classe représente la classe mère de tous les contrôles.
 */
public abstract class AbstractControle {
	
	private ControlesContexte contexte;
	
	/**
	 * Constructeur.
	 * @param contexte un contexte des controles
	 */
	public AbstractControle(ControlesContexte contexte) {
		this.contexte = contexte;
	}
	
	/**
	 * @return le contexte des contrôles
	 */
	public ControlesContexte getContexte() {
		return contexte;
	}
	
	/**
	 * @param key la cle du mesage localisé
	 * @return la chaine localisée
	 */
	public String getLocalizedMessage(String key) {
		return ERXLocalizer.currentLocalizer().localizedStringForKey(prefixKey(key));
	}
	
	/**
	 * @param key la cle du mesage localisé
	 * @param values les parametres du template
	 * @return la chaine localisée
	 */
	public String getLocalizedTemplate(String key, Map<String, Object> values) {
		return ERXLocalizer.currentLocalizer().localizedTemplateStringForKeyWithObject(prefixKey(key), values);
	}
	
	private String prefixKey(String key) {
		return this.getClass().getSimpleName() + "." + key;
	}
	
}
