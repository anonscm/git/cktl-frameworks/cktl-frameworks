package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe représente le contexte des contrôles.
 * <p>
 * Stocke la liste des messages émis par les différents contrôles.
 */
public class ControlesContexte {

	private List<String> listeErreurs = new ArrayList<String>();
	private List<String> codeErreurs = new ArrayList<String>();
	private boolean isImportEtudiant = false;
	
	/**
	 * @return the listeErreurs
	 */
	public List<String> getListeErreurs() {
		return listeErreurs;
	}
	
	public List<String> getCodeErreurs() {
		return codeErreurs;
	}

	/**
	 * Ajouter un message dans la liste des erreurs.
	 * @param key le clé de l'erreur
	 * @param message un message
	 */
	public void addErreur(String key, String message) {
		if (key != null) {
			codeErreurs.add(key);
		}
		listeErreurs.add(message);
	}
	
	/**
	 * @return vrai si la liste des erreurs n'est pas vide
	 */
	public Boolean hasErreurs() {
		return listeErreurs != null && !listeErreurs.isEmpty();
	}

	/**
	 * @return the isImportEtudiant
	 */
	public boolean isImportEtudiant() {
		return isImportEtudiant;
	}

	/**
	 * @param isImportEtudiant the isImportEtudiant to set
	 */
	public void setImportEtudiant(boolean isImportEtudiant) {
		this.isImportEtudiant = isImportEtudiant;
	}
	
	
}
