package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;

import com.webobjects.foundation.NSArray;

/**
 * Cette classe représente les contrôles à appliquer sur le dossier d'un étudiant.
 */
public class DossierEtudiantControle extends AbstractControle {

	/**
	 * Constructeur.
	 * @param contexte un contexte des contrôles
	 */
	public DossierEtudiantControle(ControlesContexte contexte) {
		super(contexte);
	}

	/**
	 * Lancer les contrôles sur le dossier administratif complet.
	 * 
	 * @param preEtudiant un pré-étudiant
	 * @param anneeUniversitaire l'année universitaire en cours
	 * @return <code>true</code> si les contrôles sont ok
	 */
	public boolean controler(EOPreEtudiant preEtudiant, Integer anneeUniversitaire) {
		InfoEtudiantControle infoEtudiantControle = new InfoEtudiantControle(getContexte());
		ListeFormationsEnvisageesControle listeFormationsEnvisageesControle = new ListeFormationsEnvisageesControle(getContexte());
		CadreDControle cadreDControle = new CadreDControle(getContexte());
		PreCursusControle preCursusControle = new PreCursusControle(getContexte());
		SituationSocialeControle situationSocialeControle = new SituationSocialeControle(getContexte());
		CadreKControle cadreKControle = new CadreKControle(getContexte());
		
		// Récupération des données
		IPreEtudiantAnnee preEtudiantAnnee = preEtudiant.toPreEtudiantAnnee(anneeUniversitaire);
		NSArray<EOPreCursus> listePreCursus = preEtudiant.toPreCursus(EOPreCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE.isNull());
		NSArray<EOPreCursus> listePreCursusTitreAcces = preEtudiant.toPreCursus(EOPreCursus.TO_ACCES_TITRE_GRADE_UNIVERSITAIRE.isNotNull());
		IPreCursus preCursusTitreAcces = null;
		
		if (!listePreCursusTitreAcces.isEmpty()) {
			preCursusTitreAcces = listePreCursusTitreAcces.get(0);
		}
		
		// Contrôles
		boolean isOk = true;
		
		isOk = infoEtudiantControle.controler(preEtudiant, preEtudiantAnnee) && isOk;
		isOk = listeFormationsEnvisageesControle.controler(preEtudiant, null, preEtudiantAnnee,  preEtudiant.editingContext()) && isOk;
		isOk = cadreDControle.controler(preEtudiantAnnee, preCursusTitreAcces, anneeUniversitaire) && isOk;
		
		for (EOPreCursus preCursus : listePreCursus) {
			isOk = preCursusControle.controlerPreCursus(preCursus, listePreCursus, anneeUniversitaire) && isOk;
		}
		
		isOk = situationSocialeControle.controler(preEtudiant, (EOPreEtudiantAnnee) preEtudiantAnnee, anneeUniversitaire) && isOk;
		isOk = cadreKControle.controler((EOPreEtudiantAnnee) preEtudiantAnnee) && isOk;
		
		return isOk;
	}
}
