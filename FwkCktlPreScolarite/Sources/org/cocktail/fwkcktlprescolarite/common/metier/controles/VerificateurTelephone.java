package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

public class VerificateurTelephone {

	public VerificateurTelephone() {
		super();
	}
	
	
	/**
	 * Contrôle : seulement des digits pour un téléphone
	 */
	public boolean isPhoneNumber(String numTel){
		return MyStringCtrl.hasOnlyDigits(numTel);
	}
	
	/**
	 * Contrôle de la longueur du numéro de téléphone
	 * pour un indicatif français
	 */
	public boolean isFrenchPhoneLength(String numTel, Integer indicatif){
		boolean isFrench = false;
		
		if (33 == indicatif.intValue() && numTel.length() == 10){
			isFrench = true;
		}
		
		return isFrench;
	}
	
	/**
	 * Mise en forme des téléphones
	 */
	public String phoneNumberWithOnlyDigits(String numTel){
		String cleanedString = numTel;
		cleanedString = cleanedString.replaceAll(".", "");
		cleanedString = cleanedString.replaceAll("-", "");
		cleanedString = cleanedString.replaceAll(" ", "");
//		cleanedString = cleanedString.replaceAll("*", "");
		cleanedString = cleanedString.replaceAll("/", "");
		
		return cleanedString;
		
	}
	
	
	
}
