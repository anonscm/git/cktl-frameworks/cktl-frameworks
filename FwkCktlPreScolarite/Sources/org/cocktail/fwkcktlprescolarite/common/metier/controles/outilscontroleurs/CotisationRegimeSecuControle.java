package org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs;

import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBoursesCF;

import com.webobjects.foundation.NSTimestamp;

/**
 * Controle si un étudiant peut/doit s'inscrire à la sécurité sociale
 */
public class CotisationRegimeSecuControle {

	private AgeLimiteNonCotisationRegimeSSUtil ageLimiteControle;

	/**
	 * Construit un objet pour l'année universitaire.
	 * @param annee une année universitaire
	 */
	public CotisationRegimeSecuControle(Integer annee) {
		ageLimiteControle = new AgeLimiteNonCotisationRegimeSSUtil(annee);
	}

	/**
	 * @param etudiantAnnee informations de l'étudiant pour l'année
	 * @return true si l'étudiant doit cotiser à la sécurité sociale étudiante
	 */
	public Boolean doitEtreAffilie(IEtudiantAnnee etudiantAnnee) {
		boolean doitEtreAffilier = false;

		IInfosEtudiant infosEtudiant = etudiantAnnee.toInfosEtudiant();
		
		if (ageLimiteControle.hasPlus28Ans(new NSTimestamp(infosEtudiant.dNaissance()))) {
			doitEtreAffilier = doitEtreAffilieSSEtudiantePlus28Ans(etudiantAnnee);
		} else if (ageLimiteControle.has20AnsOuPlus(new NSTimestamp(infosEtudiant.dNaissance()))) {
			doitEtreAffilier = doitEtreAffilieSSEtudiante20AnsOuPlus(etudiantAnnee);
		} else {
			doitEtreAffilier = doitEtreAffilieSSEtudianteMoinsDe20Ans(etudiantAnnee);
		}

		return doitEtreAffilier;
	}

	/**
	 * Est-il exonéré des droits de cotisation SS étudiante ?
	 * @param etudiantAnnee un pré étudiant année
	 * @return <code>true</code> s'il doit être affilié mais est éxonéré
	 */
	public boolean estExonereDroitSsEtudiante(IEtudiantAnnee etudiantAnnee) {
		boolean estExonere = false;

		if (doitEtreAffilie(etudiantAnnee)) {
			if (EOCotisation.COTI_CODE_ENFANT_SALARIE.equals(getCodeCotisation(etudiantAnnee))
			    && !ageLimiteControle.has20AnsOuPlus(new NSTimestamp(etudiantAnnee.toInfosEtudiant().dNaissance()))) {
				estExonere = true;
			} else if (etudiantAnnee.boursier()) {
				// Est-ce que une de ces bourses l'exonère (bourse "normale" ou campus france sécu)
				for (IBourses bourse : etudiantAnnee.toBourses()) {
					if (isBourseExonere(bourse)) {
						estExonere = true;
						break;
					}
				}
			}
		}

		return estExonere;
	}

	/**
	 * @param etudiantAnnee un pré étudiant année
	 * @return situation sociale de l''étudiant (1 : Non affilié, -1 : Affilié et payant, -2 : Affilié mais exonéré)
	 */
	public Integer calculAyantDroit(IEtudiantAnnee etudiantAnnee) {
		if (!etudiantAnnee.affSs()) {
			return 1;
		} else {
			if (estExonereDroitSsEtudiante(etudiantAnnee)) {
				return -2;
			} else {
				return -1;
			}
		}
	}

	/**
	 * Est-ce que cette bourse exonère de la SS étudiante ?
	 * @param bourse un bourse
	 * @return <code>true</code> si elle exonère
	 */
	private boolean isBourseExonere(IBourses bourse) {
		boolean cetteBourseExonere = false;

		if (!bourse.isCampusFrance()) {
			cetteBourseExonere = true;
		} else if (bourse.toTypeBoursesCF() != null) {
			String codeTypeBoursesCF = bourse.toTypeBoursesCF().code();

			cetteBourseExonere = codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_INSCRIPTION_SECU) || codeTypeBoursesCF.equals(ITypeBoursesCF.CODE_SECU);
		}

		return cetteBourseExonere;
	}

	/**
	 * Gestion des cas d'affiliation obligatoire à la sécu étudiant pour les 28 ans ou plus.
	 * @param etudiantAnnee un pre étudiant année
	 * @return <code>true</code> s'il doit être affilié à la sécu étudiante
	 */
	private boolean doitEtreAffilieSSEtudiantePlus28Ans(IEtudiantAnnee etudiantAnnee) {
		String codeCotisation = getCodeCotisation(etudiantAnnee);

		if (EOCotisation.COTI_CODE_ETUDIANT_ETRANGER.equals(codeCotisation)) {
			return true;
		} else if (EOCotisation.COTI_CODE_ENFANT_SALARIE.equals(codeCotisation) || EOCotisation.COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE.equals(codeCotisation)
		    || EOCotisation.COTI_CODE_PLUS_DE_28_ANS.equals(codeCotisation)) {
			// Gestion des cas des doctorant de 28 ans ou plus et ayant commencé leur doctorat avant 28 ans
			// On recherche, parmis ses cursus, s'il ne suis pas un doctorat
			ICursus premiereAnneeDoctorat = rechercherPremiereAnneeDoctorat(etudiantAnnee);

			if (premiereAnneeDoctorat != null) {
				// On a trouvé un doctorat
				return !ageLimiteControle.hasPlus28Ans(new NSTimestamp(etudiantAnnee.toInfosEtudiant().dNaissance()), premiereAnneeDoctorat.anneeDebut());
			}
		}

		return false;
	}

	/**
	 * Recherche parmis les cursus de l'étudiant, la première année de doctorat.
	 * @param etudiantAnnee un pré étudiant année
	 * @return le premier cursus (en terme d'année) de l'étudiant si trouvé
	 */
	private ICursus rechercherPremiereAnneeDoctorat(IEtudiantAnnee etudiantAnnee) {
		ICursus premiereAnneeDoctorat = null;

		for (ICursus cursus : etudiantAnnee.toInfosEtudiant().toCursus()) {
			if (cursus.toGradeUniversitaire() != null && EOGradeUniversitaire.GRADE_DOCTORAT.equals(cursus.toGradeUniversitaire().type())
			    && (premiereAnneeDoctorat == null || premiereAnneeDoctorat.anneeDebut() > cursus.anneeDebut())) {
				premiereAnneeDoctorat = cursus;
			}
		}

		return premiereAnneeDoctorat;
	}

	/**
	 * Gestion des cas d'affiliation obligatoire à la sécu étudiant pour les 20 ans ou plus.
	 * @param etudiantAnnee un pre étudiant année
	 * @return <code>true</code> s'il doit être affilié à la sécu étudiante
	 */
	private boolean doitEtreAffilieSSEtudiante20AnsOuPlus(IEtudiantAnnee etudiantAnnee) {
		String codeCotisation = getCodeCotisation(etudiantAnnee);

		if (EOCotisation.COTI_CODE_ENFANT_SALARIE.equals(codeCotisation) || EOCotisation.COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE.equals(codeCotisation)
		    || EOCotisation.COTI_CODE_ETUDIANT_ETRANGER.equals(codeCotisation)) {
			return true;
		} else if (EOCotisation.COTI_CODE_PARENT_REGIME_PARTICULIER.equals(codeCotisation)) {
			// On vérifie que le régime des parents ne force pas l'affiliation pour les [20, 28[ ans
			return (etudiantAnnee.toRegimeParticulier() != null && etudiantAnnee.toRegimeParticulier().affiliationSsEtudiante());
		}

		return false;
	}

	/**
	 * Gestion des cas d'affiliation obligatoire à la sécu étudiant pour les moins de 20 ans.
	 * @param etudiantAnnee un pre étudiant année
	 * @return <code>true</code> s'il doit être affilié à la sécu étudiante
	 */
	private boolean doitEtreAffilieSSEtudianteMoinsDe20Ans(IEtudiantAnnee etudiantAnnee) {
		String codeCotisation = getCodeCotisation(etudiantAnnee);

		if (EOCotisation.COTI_CODE_ENFANT_SALARIE.equals(codeCotisation) || EOCotisation.COTI_CODE_ETUDIANT_ETRANGER.equals(codeCotisation)) {
			return true;
		}

		return false;
	}

	private String getCodeCotisation(IEtudiantAnnee etudiantAnnee) {
		EOCotisation cotisation = etudiantAnnee.toCotisation();
		return cotisation == null ? null : cotisation.cotiCode();
	}
}
