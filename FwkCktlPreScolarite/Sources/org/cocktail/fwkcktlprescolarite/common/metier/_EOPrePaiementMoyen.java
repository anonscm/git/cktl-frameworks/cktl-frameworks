/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePaiementMoyen.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPrePaiementMoyen extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPrePaiementMoyen.class);

	public static final String ENTITY_NAME = "EOPrePaiementMoyen";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_MOYEN_PAIEMENT";


// Attribute Keys
  public static final ERXKey<String> BIC = new ERXKey<String>("bic");
  public static final ERXKey<String> CODE_BANQUE = new ERXKey<String>("codeBanque");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Boolean> ETUDIANT_TITULAIRE = new ERXKey<Boolean>("etudiantTitulaire");
  public static final ERXKey<String> IBAN = new ERXKey<String>("iban");
  public static final ERXKey<Integer> ID_PERSONNE_TITULAIRE = new ERXKey<Integer>("idPersonneTitulaire");
  public static final ERXKey<java.math.BigDecimal> MONTANT_PAYE = new ERXKey<java.math.BigDecimal>("montantPaye");
  public static final ERXKey<String> NOM_TIREUR = new ERXKey<String>("nomTireur");
  public static final ERXKey<String> NUMERO_CHEQUE = new ERXKey<String>("numeroCheque");
  public static final ERXKey<String> NUMERO_COMPTE = new ERXKey<String>("numeroCompte");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PRENOM_TIREUR = new ERXKey<String>("prenomTireur");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_CIVILITE_TITULAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toCiviliteTitulaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement> TO_MODE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement>("toModePaiement");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> TO_PRE_ADRESSE_TITULAIRE = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>("toPreAdresseTitulaire");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> TO_PRE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>("toPrePaiement");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> TO_PRE_PAIEMENT_ECHEANCES = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance>("toPrePaiementEcheances");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement> TO_TYPE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement>("toTypePaiement");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreMoyenPaiement";

	public static final String BIC_KEY = "bic";
	public static final String CODE_BANQUE_KEY = "codeBanque";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String ETUDIANT_TITULAIRE_KEY = "etudiantTitulaire";
	public static final String IBAN_KEY = "iban";
	public static final String ID_PERSONNE_TITULAIRE_KEY = "idPersonneTitulaire";
	public static final String MONTANT_PAYE_KEY = "montantPaye";
	public static final String NOM_TIREUR_KEY = "nomTireur";
	public static final String NUMERO_CHEQUE_KEY = "numeroCheque";
	public static final String NUMERO_COMPTE_KEY = "numeroCompte";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRENOM_TIREUR_KEY = "prenomTireur";

// Attributs non visibles
	public static final String C_CIVILITE_TITULAIRE_KEY = "cCiviliteTitulaire";
	public static final String ID_MODE_PAIEMENT_KEY = "idModePaiement";
	public static final String ID_PRE_ADRESSE_TITULAIRE_KEY = "idPreAdresseTitulaire";
	public static final String ID_PRE_MOYEN_PAIEMENT_KEY = "idPreMoyenPaiement";
	public static final String ID_PRE_PAIEMENT_KEY = "idPrePaiement";
	public static final String ID_TYPE_PAIEMENT_KEY = "idTypePaiement";

//Colonnes dans la base de donnees
	public static final String BIC_COLKEY = "BIC";
	public static final String CODE_BANQUE_COLKEY = "C_BANQUE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String ETUDIANT_TITULAIRE_COLKEY = "ETUDIANT_TITULAIRE";
	public static final String IBAN_COLKEY = "IBAN";
	public static final String ID_PERSONNE_TITULAIRE_COLKEY = "ID_PERSONNE_TITULAIRE";
	public static final String MONTANT_PAYE_COLKEY = "MONTANT_PAYE";
	public static final String NOM_TIREUR_COLKEY = "NOM_TIREUR";
	public static final String NUMERO_CHEQUE_COLKEY = "NUMERO_CHEQUE";
	public static final String NUMERO_COMPTE_COLKEY = "NUMERO_COMPTE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PRENOM_TIREUR_COLKEY = "PRENOM_TIREUR";

	public static final String C_CIVILITE_TITULAIRE_COLKEY = "C_CIVILITE_TITULAIRE";
	public static final String ID_MODE_PAIEMENT_COLKEY = "ID_MODE_PAIEMENT";
	public static final String ID_PRE_ADRESSE_TITULAIRE_COLKEY = "ID_PRE_ADRESSE_TITULAIRE";
	public static final String ID_PRE_MOYEN_PAIEMENT_COLKEY = "ID_PRE_MOYEN_PAIEMENT";
	public static final String ID_PRE_PAIEMENT_COLKEY = "ID_PRE_PAIEMENT";
	public static final String ID_TYPE_PAIEMENT_COLKEY = "ID_TYPE_PAIEMENT";


	// Relationships
	public static final String TO_CIVILITE_TITULAIRE_KEY = "toCiviliteTitulaire";
	public static final String TO_MODE_PAIEMENT_KEY = "toModePaiement";
	public static final String TO_PRE_ADRESSE_TITULAIRE_KEY = "toPreAdresseTitulaire";
	public static final String TO_PRE_PAIEMENT_KEY = "toPrePaiement";
	public static final String TO_PRE_PAIEMENT_ECHEANCES_KEY = "toPrePaiementEcheances";
	public static final String TO_TYPE_PAIEMENT_KEY = "toTypePaiement";



	// Accessors methods
  public String bic() {
    return (String) storedValueForKey(BIC_KEY);
  }

  public void setBic(String value) {
    takeStoredValueForKey(value, BIC_KEY);
  }

  public String codeBanque() {
    return (String) storedValueForKey(CODE_BANQUE_KEY);
  }

  public void setCodeBanque(String value) {
    takeStoredValueForKey(value, CODE_BANQUE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Boolean etudiantTitulaire() {
    return (Boolean) storedValueForKey(ETUDIANT_TITULAIRE_KEY);
  }

  public void setEtudiantTitulaire(Boolean value) {
    takeStoredValueForKey(value, ETUDIANT_TITULAIRE_KEY);
  }

  public String iban() {
    return (String) storedValueForKey(IBAN_KEY);
  }

  public void setIban(String value) {
    takeStoredValueForKey(value, IBAN_KEY);
  }

  public Integer idPersonneTitulaire() {
    return (Integer) storedValueForKey(ID_PERSONNE_TITULAIRE_KEY);
  }

  public void setIdPersonneTitulaire(Integer value) {
    takeStoredValueForKey(value, ID_PERSONNE_TITULAIRE_KEY);
  }

  public java.math.BigDecimal montantPaye() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_PAYE_KEY);
  }

  public void setMontantPaye(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_PAYE_KEY);
  }

  public String nomTireur() {
    return (String) storedValueForKey(NOM_TIREUR_KEY);
  }

  public void setNomTireur(String value) {
    takeStoredValueForKey(value, NOM_TIREUR_KEY);
  }

  public String numeroCheque() {
    return (String) storedValueForKey(NUMERO_CHEQUE_KEY);
  }

  public void setNumeroCheque(String value) {
    takeStoredValueForKey(value, NUMERO_CHEQUE_KEY);
  }

  public String numeroCompte() {
    return (String) storedValueForKey(NUMERO_COMPTE_KEY);
  }

  public void setNumeroCompte(String value) {
    takeStoredValueForKey(value, NUMERO_COMPTE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String prenomTireur() {
    return (String) storedValueForKey(PRENOM_TIREUR_KEY);
  }

  public void setPrenomTireur(String value) {
    takeStoredValueForKey(value, PRENOM_TIREUR_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toCiviliteTitulaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey(TO_CIVILITE_TITULAIRE_KEY);
  }

  public void setToCiviliteTitulaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toCiviliteTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CIVILITE_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CIVILITE_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement toModePaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement)storedValueForKey(TO_MODE_PAIEMENT_KEY);
  }

  public void setToModePaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement oldValue = toModePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MODE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MODE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse toPreAdresseTitulaire() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse)storedValueForKey(TO_PRE_ADRESSE_TITULAIRE_KEY);
  }

  public void setToPreAdresseTitulaireRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse oldValue = toPreAdresseTitulaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ADRESSE_TITULAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ADRESSE_TITULAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement toPrePaiement() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement)storedValueForKey(TO_PRE_PAIEMENT_KEY);
  }

  public void setToPrePaiementRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement oldValue = toPrePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_PAIEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement toTypePaiement() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement)storedValueForKey(TO_TYPE_PAIEMENT_KEY);
  }

  public void setToTypePaiementRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement oldValue = toTypePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_PAIEMENT_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> toPrePaiementEcheances() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance>)storedValueForKey(TO_PRE_PAIEMENT_ECHEANCES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> toPrePaiementEcheances(EOQualifier qualifier) {
    return toPrePaiementEcheances(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> toPrePaiementEcheances(EOQualifier qualifier, boolean fetch) {
    return toPrePaiementEcheances(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> toPrePaiementEcheances(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance.TO_PRE_PAIEMENT_MOYEN_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrePaiementEcheances();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrePaiementEcheancesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_ECHEANCES_KEY);
  }

  public void removeFromToPrePaiementEcheancesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_ECHEANCES_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance createToPrePaiementEcheancesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPrePaiementEcheance");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_PAIEMENT_ECHEANCES_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance) eo;
  }

  public void deleteToPrePaiementEcheancesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_ECHEANCES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrePaiementEcheancesRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> objects = toPrePaiementEcheances().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrePaiementEcheancesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrePaiementMoyen avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrePaiementMoyen createEOPrePaiementMoyen(EOEditingContext editingContext, NSTimestamp dCreation
, Boolean etudiantTitulaire
, java.math.BigDecimal montantPaye
, Integer persIdCreation
, org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement toModePaiement, org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement toPrePaiement, org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement toTypePaiement			) {
    EOPrePaiementMoyen eo = (EOPrePaiementMoyen) createAndInsertInstance(editingContext, _EOPrePaiementMoyen.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setEtudiantTitulaire(etudiantTitulaire);
		eo.setMontantPaye(montantPaye);
		eo.setPersIdCreation(persIdCreation);
    eo.setToModePaiementRelationship(toModePaiement);
    eo.setToPrePaiementRelationship(toPrePaiement);
    eo.setToTypePaiementRelationship(toTypePaiement);
    return eo;
  }

  
	  public EOPrePaiementMoyen localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrePaiementMoyen)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementMoyen creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementMoyen creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPrePaiementMoyen object = (EOPrePaiementMoyen)createAndInsertInstance(editingContext, _EOPrePaiementMoyen.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrePaiementMoyen localInstanceIn(EOEditingContext editingContext, EOPrePaiementMoyen eo) {
    EOPrePaiementMoyen localInstance = (eo == null) ? null : (EOPrePaiementMoyen)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrePaiementMoyen#localInstanceIn a la place.
   */
	public static EOPrePaiementMoyen localInstanceOf(EOEditingContext editingContext, EOPrePaiementMoyen eo) {
		return EOPrePaiementMoyen.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrePaiementMoyen fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePaiementMoyen fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrePaiementMoyen> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrePaiementMoyen eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrePaiementMoyen)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrePaiementMoyen fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrePaiementMoyen fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrePaiementMoyen> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrePaiementMoyen eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrePaiementMoyen)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrePaiementMoyen fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrePaiementMoyen eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrePaiementMoyen ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrePaiementMoyen fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
