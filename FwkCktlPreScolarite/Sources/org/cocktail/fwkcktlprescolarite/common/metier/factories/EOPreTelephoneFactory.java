package org.cocktail.fwkcktlprescolarite.common.metier.factories;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.repositories.IPaysIndicatifRepository;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreTelephone;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITelephoneFactory;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * 
 * Implémentation EOF.
 * 
 * @author Alexis Tual
 *
 */
public class EOPreTelephoneFactory implements ITelephoneFactory {

    private IPaysIndicatifRepository indicatifRepository;
    private EOEditingContext editingContext;
    private Integer persIdCreateur;
    
    /**
     * @param editingContext l'editingcontext
     * @param indicatifRepository le repo des indicatifs
     * @param persIdCreateur le persId du createur
     */
    public EOPreTelephoneFactory(
            EOEditingContext editingContext, 
            IPaysIndicatifRepository indicatifRepository,
            Integer persIdCreateur) {
        this.editingContext = editingContext;
        this.persIdCreateur = persIdCreateur;
        this.indicatifRepository = indicatifRepository;
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneFixe(IInfosEtudiant preEtudiant, ITypeTel typeTelephone) {
        IPreTelephone preTel = EOPreTelephone.creer(editingContext, (EOPreEtudiant) preEtudiant, persIdCreateur);
        preTel.setToTypeTelsRelationship((EOTypeTel) typeTelephone);
        preTel.setToTypeNoTelRelationship(EOTypeNoTel.typeFixe(editingContext));
        preTel.setIndicatif(defaultPaysIndicatif());
        return preTel;
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneGsm(IInfosEtudiant preEtudiant, ITypeTel typeTelephone) {
        IPreTelephone preTel = EOPreTelephone.creer(editingContext, (EOPreEtudiant) preEtudiant, persIdCreateur);
        preTel.setToTypeTelsRelationship((EOTypeTel) typeTelephone);
        preTel.setToTypeNoTelRelationship(EOTypeNoTel.typePortable(editingContext));
        preTel.setIndicatif(defaultPaysIndicatif());
        return preTel;
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneFixeEtudiant(IInfosEtudiant preEtudiant) {
    	return creerTelephoneFixe(preEtudiant, EOTypeTel.typeEtudiant(editingContext));
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneFixeParent(IInfosEtudiant preEtudiant) {
    	return creerTelephoneFixe(preEtudiant, EOTypeTel.typeParent(editingContext));
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneGsmEtudiant(IInfosEtudiant preEtudiant) {
    	return creerTelephoneGsm(preEtudiant, EOTypeTel.typeEtudiant(editingContext));
    }
    
    /**
     * {@inheritDoc}
     */
    public IPreTelephone creerTelephoneGsmParent(IInfosEtudiant preEtudiant) {
    	return creerTelephoneGsm(preEtudiant, EOTypeTel.typeParent(editingContext));
    }
    
    private Integer defaultPaysIndicatif() {
        IPaysIndicatif indicatif = indicatifRepository.defaultPaysIndicatif();
        return indicatif.indicatif();
    }

}
