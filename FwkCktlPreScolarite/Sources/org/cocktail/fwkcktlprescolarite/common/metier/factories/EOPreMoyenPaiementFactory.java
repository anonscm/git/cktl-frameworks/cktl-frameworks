package org.cocktail.fwkcktlprescolarite.common.metier.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementCBPrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementChequePrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ModePaiementVirementPrelevement;
import org.cocktail.fwkcktlscolpeda.serveur.metier._EOPaiementMoyen;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * Factory pour creation du moyen de paiement.
 * 
 * @author alexistual
 *
 */
public class EOPreMoyenPaiementFactory implements IPreMoyenPaiementFactory {

	private EOEditingContext editingContext;
	
	/**
	 * @param editingContext l'ec
	 */
	public EOPreMoyenPaiementFactory(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void creerMoyenPaiement(IPaiement paiement, IModePaiement modePaiement, Integer persId, BigDecimal montantTotalDu) {
		if (modePaiement.isPrelevement()) {
			creerMoyenPaiementPrelevement(paiement, persId, montantTotalDu);
		} else if (modePaiement.isSimple()) {
			creerMoyenPaiementSimple(paiement, modePaiement, persId, montantTotalDu);
		} else if (modePaiement instanceof ModePaiementChequePrelevement) {
			creerMoyenPaiementCheque(paiement, persId);
			creerMoyenPaiementPrelevement(paiement, persId,  montantTotalDu);
		} else if (modePaiement instanceof ModePaiementCBPrelevement) {
			creerMoyenPaiementCB(paiement, persId);
			creerMoyenPaiementPrelevement(paiement, persId, montantTotalDu);
		} else if (modePaiement instanceof ModePaiementVirementPrelevement) {
			creerMoyenPaiementVirement(paiement, persId);
			creerMoyenPaiementPrelevement(paiement, persId, montantTotalDu);
		}
	}

	private IPaiementMoyen creerMoyenPaiement(IPaiement paiement, IModePaiement modePaiement, Integer persId) {
		IPaiementMoyen moyenPaiement = null;
		
		if (paiement instanceof IPrePaiement) {
			moyenPaiement = EOPrePaiementMoyen.creerInstance(editingContext);
		} else {
			moyenPaiement = (IPaiementMoyen) EOUtilities.createAndInsertInstance(editingContext, _EOPaiementMoyen.ENTITY_NAME);
		}
		moyenPaiement.setDCreation(new NSTimestamp());
		moyenPaiement.setMontantPaye(BigDecimal.ZERO);
		moyenPaiement.setPersIdCreation(persId);
		moyenPaiement.setPersIdModification(persId);
		moyenPaiement.setToPaiementRelationship(paiement);
		if (modePaiement.isPrelevement()) {
			moyenPaiement.setToTypePaiementRelationship(EOTypePaiement.typeDiffere(editingContext));
		} else {
			moyenPaiement.setToTypePaiementRelationship(EOTypePaiement.typeComptant(editingContext));
		}
		moyenPaiement.setToModePaiementRelationship(modePaiement);
		
		return moyenPaiement;
	}
	
	private void creerMoyenPaiementSimple(IPaiement paiement, IModePaiement modePaiement, Integer persId, BigDecimal montant) {
		IPaiementMoyen moyenPaiement = creerMoyenPaiement(paiement, modePaiement, persId);
		moyenPaiement.setMontantPaye(montant);
	}

	private void creerMoyenPaiementCB(IPaiement paiement, Integer persId) {
		IModePaiement modePaiement = EOModePaiement.fetchSco_ModePaiement(editingContext, EOModePaiement.CODE.eq(EOModePaiement.CODE_MODE_CB));
		creerMoyenPaiement(paiement, modePaiement, persId);
	}

	private void creerMoyenPaiementVirement(IPaiement paiement, Integer persId) {
		IModePaiement modePaiement = EOModePaiement.fetchSco_ModePaiement(editingContext, EOModePaiement.CODE.eq(EOModePaiement.CODE_MODE_VIREMENT));
		creerMoyenPaiement(paiement, modePaiement, persId);
	}
	
	private void creerMoyenPaiementPrelevement(IPaiement paiement, Integer persId, BigDecimal montant) {
		IModePaiement modePaiement = EOModePaiement.fetchSco_ModePaiement(editingContext, EOModePaiement.CODE.eq(EOModePaiement.CODE_MODE_PRELEVEMENT));
		IPaiementMoyen prelevement = creerMoyenPaiement(paiement, modePaiement, persId);
		prelevement.setMontantPaye(montant);
	}

	private void creerMoyenPaiementCheque(IPaiement paiement, Integer persId) {
		IModePaiement modePaiement = EOModePaiement.fetchSco_ModePaiement(editingContext, EOModePaiement.CODE.eq(EOModePaiement.CODE_MODE_CHEQUE));
		creerMoyenPaiement(paiement, modePaiement, persId);
	}

}
