package org.cocktail.fwkcktlprescolarite.common.metier.factories;

import java.math.BigDecimal;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;

/**
 * 
 * Creation des moyens de paiement.
 * 
 * @author alexistual
 *
 */
public interface IPreMoyenPaiementFactory {

	/**
	 * @param paiement le paiement
	 * @param modePaiement le mode de paiement
	 * @param persId le persId de l'utilisateur créant le moyen de paiement
	 * @param montantParPrelevement le montant à payer par prelevement
	 */
	void creerMoyenPaiement(IPaiement paiement, IModePaiement modePaiement, Integer persId, BigDecimal montantParPrelevement);
	
}
