/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePaiementEcheance.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPrePaiementEcheance extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPrePaiementEcheance.class);

	public static final String ENTITY_NAME = "EOPrePaiementEcheance";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_ECHEANCE_PAIEMENT";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_ECHEANCE = new ERXKey<NSTimestamp>("dateEcheance");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> TO_AUTRE_PRE_PAIEMENT_MOYEN = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>("toAutrePrePaiementMoyen");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> TO_PRE_PAIEMENT_MOYEN = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>("toPrePaiementMoyen");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreEcheancePaiement";

	public static final String DATE_ECHEANCE_KEY = "dateEcheance";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String LIBELLE_KEY = "libelle";
	public static final String MONTANT_KEY = "montant";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

// Attributs non visibles
	public static final String ID_AUTRE_PRE_MOYEN_PAIEMENT_KEY = "idAutrePreMoyenPaiement";
	public static final String ID_PRE_ECHEANCE_PAIEMENT_KEY = "idPreEcheancePaiement";
	public static final String ID_PRE_MOYEN_PAIEMENT_KEY = "idPreMoyenPaiement";

//Colonnes dans la base de donnees
	public static final String DATE_ECHEANCE_COLKEY = "DATE_ECHEANCE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String LIBELLE_COLKEY = "LIBELLE";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_AUTRE_PRE_MOYEN_PAIEMENT_COLKEY = "ID_AUTRE_PRE_MOYEN_PAIEMENT";
	public static final String ID_PRE_ECHEANCE_PAIEMENT_COLKEY = "ID_PRE_ECHEANCE_PAIEMENT";
	public static final String ID_PRE_MOYEN_PAIEMENT_COLKEY = "ID_PRE_MOYEN_PAIEMENT";


	// Relationships
	public static final String TO_AUTRE_PRE_PAIEMENT_MOYEN_KEY = "toAutrePrePaiementMoyen";
	public static final String TO_PRE_PAIEMENT_MOYEN_KEY = "toPrePaiementMoyen";



	// Accessors methods
  public NSTimestamp dateEcheance() {
    return (NSTimestamp) storedValueForKey(DATE_ECHEANCE_KEY);
  }

  public void setDateEcheance(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_ECHEANCE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    takeStoredValueForKey(value, LIBELLE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen toAutrePrePaiementMoyen() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen)storedValueForKey(TO_AUTRE_PRE_PAIEMENT_MOYEN_KEY);
  }

  public void setToAutrePrePaiementMoyenRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen oldValue = toAutrePrePaiementMoyen();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_AUTRE_PRE_PAIEMENT_MOYEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_AUTRE_PRE_PAIEMENT_MOYEN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen toPrePaiementMoyen() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen)storedValueForKey(TO_PRE_PAIEMENT_MOYEN_KEY);
  }

  public void setToPrePaiementMoyenRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen oldValue = toPrePaiementMoyen();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_PAIEMENT_MOYEN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_PAIEMENT_MOYEN_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrePaiementEcheance avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrePaiementEcheance createEOPrePaiementEcheance(EOEditingContext editingContext, NSTimestamp dateEcheance
, NSTimestamp dCreation
, NSTimestamp dModification
, String libelle
, java.math.BigDecimal montant
, Integer persIdCreation
, Integer persIdModification
, org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen toPrePaiementMoyen			) {
    EOPrePaiementEcheance eo = (EOPrePaiementEcheance) createAndInsertInstance(editingContext, _EOPrePaiementEcheance.ENTITY_NAME);    
		eo.setDateEcheance(dateEcheance);
		eo.setDCreation(dCreation);
		eo.setDModification(dModification);
		eo.setLibelle(libelle);
		eo.setMontant(montant);
		eo.setPersIdCreation(persIdCreation);
		eo.setPersIdModification(persIdModification);
    eo.setToPrePaiementMoyenRelationship(toPrePaiementMoyen);
    return eo;
  }

  
	  public EOPrePaiementEcheance localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrePaiementEcheance)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementEcheance creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementEcheance creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPrePaiementEcheance object = (EOPrePaiementEcheance)createAndInsertInstance(editingContext, _EOPrePaiementEcheance.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrePaiementEcheance localInstanceIn(EOEditingContext editingContext, EOPrePaiementEcheance eo) {
    EOPrePaiementEcheance localInstance = (eo == null) ? null : (EOPrePaiementEcheance)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrePaiementEcheance#localInstanceIn a la place.
   */
	public static EOPrePaiementEcheance localInstanceOf(EOEditingContext editingContext, EOPrePaiementEcheance eo) {
		return EOPrePaiementEcheance.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementEcheance>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrePaiementEcheance fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePaiementEcheance fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrePaiementEcheance> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrePaiementEcheance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrePaiementEcheance)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrePaiementEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrePaiementEcheance fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrePaiementEcheance> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrePaiementEcheance eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrePaiementEcheance)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrePaiementEcheance fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrePaiementEcheance eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrePaiementEcheance ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrePaiementEcheance fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
