/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IMention;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscription;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

/**
 * Classe permettant de definir la formation sur une période donnée
 * @author isabelle
 */
public class EOPreCursus extends _EOPreCursus implements IPreCursus {

	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreCursus.class);

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	/**
	 * Instantiates a new EOPRECursus
	 */
	public EOPreCursus() {
		super();
	}

	/**
	 * @param edc editing context dans lequel on crée le pre-cursus
	 * @param persId identifiant de l'utilisateur qu crée le pre-cursus
	 * @return un pre-cursus
	 */
	public static EOPreCursus creer(EOEditingContext edc, Integer persId) {
		EOPreCursus cursus = creerInstance(edc);
		cursus.setPersIdCreation(persId);
		cursus.setPersIdMofication(persId);
		cursus.setDCreation(new NSTimestamp());
		cursus.setDModifcation(new NSTimestamp());
		cursus.setInterruptionEtud(false);
		return cursus;
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException exception remontée en cas d'erreur
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException exception remontée en cas d'erreur
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException exception remontée en cas d'erreur
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException exception remontée en cas d'erreur
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 *  * @throws NSValidation.ValidationException exception remontée en cas d'erreur
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
		super.validateBeforeTransactionSave();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToAccesTitreGradeUniversitaireRelationship(IGradeUniversitaire value) {
		super.setToAccesTitreGradeUniversitaireRelationship((EOGradeUniversitaire) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToGradeUniversitaireRelationship(IGradeUniversitaire value) {
		super.setToGradeUniversitaireRelationship((EOGradeUniversitaire) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToAccesMentionRelationship(IMention value) {
		super.setToAccesMentionRelationship((EOMention) value);
	}
	
	
	 public void setToEtudiantRelationship(IInfosEtudiant value) {
			setToPreEtudiantRelationship((EOPreEtudiant) value);
		}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToEtudiantRelationship(IEtudiant value) {
		super.setToPreEtudiantRelationship((EOPreEtudiant) value);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void setToPreEtudiantRelationship(IPreEtudiant value) {
		super.setToPreEtudiantRelationship((EOPreEtudiant) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToTypeFormationRelationship(ITypeFormation value) {
	  super.setToTypeFormationRelationship((EOTypeFormation) value);
	  
  }

	/**
	 * {@inheritDoc}
	 */
	public void setToCRneRelationship(IRne value) {
	  super.setToCRneRelationship((EORne) value);	  
  }
	
	/**
	 * {@inheritDoc}
	 */
	public boolean supprimerSiSansValeurs() {
		if (toAccesTitreGradeUniversitaire() == null && anneeDebut() == null && anneeFin() == null && toCRne() == null && !interruptionEtud()) {
			setToPreEtudiantRelationship(null);
			delete();
			return true;
		}

		return false;
	}
	
	/**
	 * Calculer le type d'inscription pour le cursus.
	 * 
	 * @param codeRneEtablissement Le code RNE de l'établissement
	 */
	public void calculerTypeInscription(String codeRneEtablissement) {
		if (toCRne() == null) {
			setTypeInscription(null);
		} else if (toCRne().cRne().equals(codeRneEtablissement)) {
			setTypeInscription(ITypeInscription.CODE_LOCAL);
		} else if (toCRne().isUneUniversite()) {
			setTypeInscription(ITypeInscription.CODE_UNIV_FRANCAISE);
		} else {
			setTypeInscription(ITypeInscription.CODE_ENS_SUP);
		}
	}

	public void setToPaysRelationship(IPays value) {
		super.setToPaysRelationship((EOPays) value);
		
	}	
}
