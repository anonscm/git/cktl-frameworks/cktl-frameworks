package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;

import com.webobjects.foundation.NSTimestamp;

public interface IPreTelephone extends ITelephone {

	public NSTimestamp dCreation();

	public void setDCreation(NSTimestamp value);

	public NSTimestamp dModification();

	public void setDModification(NSTimestamp value);

	public Integer indicatif();

	public void setIndicatif(Integer value);

	public String noTelephone();

	public void setNoTelephone(String value);

	public Integer persIdCreation();

	public void setPersIdCreation(Integer value);

	public Integer persIdModifcation();

	public void setPersIdModifcation(Integer value);

	public IPreEtudiant toPreEtudiant();

	public void setToPreEtudiantRelationship(IPreEtudiant value);

	public EOTypeNoTel toTypeNoTel();

	public void setToTypeNoTelRelationship(EOTypeNoTel value);

	public EOTypeTel toTypeTels();

	public void setToTypeTelsRelationship(EOTypeTel value);

}
