/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreEtudiant.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreEtudiant extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreEtudiant.class);

	public static final String ENTITY_NAME = "EOPreEtudiant";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_ETUDIANT";


// Attribute Keys
  public static final ERXKey<String> ACAD_CODE_BAC = new ERXKey<String>("acadCodeBac");
  public static final ERXKey<Integer> ANNE1_INSC_UNIV = new ERXKey<Integer>("anne1InscUniv");
  public static final ERXKey<Integer> ANNEE1_INSC_ETAB = new ERXKey<Integer>("annee1InscEtab");
  public static final ERXKey<Integer> ANNEE1_INSC_SUP = new ERXKey<Integer>("annee1InscSup");
  public static final ERXKey<Integer> ANNEE_BAC = new ERXKey<Integer>("anneeBac");
  public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
  public static final ERXKey<String> C_DEPARTEMENT_PARENT = new ERXKey<String>("cDepartementParent");
  public static final ERXKey<Integer> CLE_INSEE = new ERXKey<Integer>("cleInsee");
  public static final ERXKey<String> CODE_INE = new ERXKey<String>("codeIne");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<String> DEPARTEMENT_ETAB_BAC = new ERXKey<String>("departementEtabBac");
  public static final ERXKey<String> DEPT_NAISSANCE = new ERXKey<String>("deptNaissance");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<NSTimestamp> D_NAISSANCE = new ERXKey<NSTimestamp>("dNaissance");
  public static final ERXKey<NSTimestamp> D_NATURALISATION = new ERXKey<NSTimestamp>("dNaturalisation");
  public static final ERXKey<Boolean> ENFANTS_CHARGE = new ERXKey<Boolean>("enfantsCharge");
  public static final ERXKey<Integer> ETUD_NUMERO = new ERXKey<Integer>("etudNumero");
  public static final ERXKey<Boolean> INDICATEUR_PHOTO = new ERXKey<Boolean>("indicateurPhoto");
  public static final ERXKey<Boolean> INE_PROVISOIRE = new ERXKey<Boolean>("ineProvisoire");
  public static final ERXKey<Boolean> INSEE_PROVISOIRE = new ERXKey<Boolean>("inseeProvisoire");
  public static final ERXKey<String> NO_INSEE = new ERXKey<String>("noInsee");
  public static final ERXKey<String> NOM_PATRONYMIQUE = new ERXKey<String>("nomPatronymique");
  public static final ERXKey<String> NOM_USUEL = new ERXKey<String>("nomUsuel");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> PRENOM = new ERXKey<String>("prenom");
  public static final ERXKey<String> PRENOM2 = new ERXKey<String>("prenom2");
  public static final ERXKey<String> PRE_TYPE = new ERXKey<String>("preType");
  public static final ERXKey<String> REIMMATRICULATION = new ERXKey<String>("reimmatriculation");
  public static final ERXKey<String> SIT_MILITAIRE = new ERXKey<String>("sitMilitaire");
  public static final ERXKey<Integer> SN_ATTESTATION = new ERXKey<Integer>("snAttestation");
  public static final ERXKey<Integer> SN_CERTIFICATION = new ERXKey<Integer>("snCertification");
  public static final ERXKey<Boolean> SPORT_HN = new ERXKey<Boolean>("sportHN");
  public static final ERXKey<String> T_HEB_CODE = new ERXKey<String>("tHebCode");
  public static final ERXKey<String> TYPE_INSCRIPTION = new ERXKey<String>("typeInscription");
  public static final ERXKey<String> VILLE_BAC = new ERXKey<String>("villeBac");
  public static final ERXKey<String> VILLE_NAISSANCE = new ERXKey<String>("villeNaissance");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBac> TO_BACS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOBac>("toBacs");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite> TO_CIVILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOCivilite>("toCivilite");
  public static final ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande> TODEMANDE = new ERXKey<org.cocktail.fwkcktlworkflow.serveur.metier.EODemande>("todemande");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement> TO_DEPARTEMENT_NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODepartement>("toDepartementNaissance");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant> TO_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant>("toEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> TO_INDIVIDU_ETUDIANT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("toIndividuEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMention> TO_MENTION = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOMention>("toMention");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysEtabBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_NAISSANCE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysNaissance");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS_NATIONALITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPaysNationalite");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> TO_PRE_ADRESSES = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>("toPreAdresses");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> TO_PRE_BOURSES = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses>("toPreBourses");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> TO_PRE_CURSUS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus>("toPreCursus");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> TO_PRE_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>("toPreEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> TO_PRE_INSCRIPTIONS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>("toPreInscriptions");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> TO_PRE_PHOTOS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto>("toPrePhotos");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> TO_PRE_TELEPHONES = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone>("toPreTelephones");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION1 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession1");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION2 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession2");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_ETAB_BAC = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneEtabBac");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE_ETAB_SUP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRneEtabSup");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale> TO_SITUATION_FAMILIALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale>("toSituationFamiliale");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreEtudiant";

	public static final String ACAD_CODE_BAC_KEY = "acadCodeBac";
	public static final String ANNE1_INSC_UNIV_KEY = "anne1InscUniv";
	public static final String ANNEE1_INSC_ETAB_KEY = "annee1InscEtab";
	public static final String ANNEE1_INSC_SUP_KEY = "annee1InscSup";
	public static final String ANNEE_BAC_KEY = "anneeBac";
	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String C_DEPARTEMENT_PARENT_KEY = "cDepartementParent";
	public static final String CLE_INSEE_KEY = "cleInsee";
	public static final String CODE_INE_KEY = "codeIne";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEPARTEMENT_ETAB_BAC_KEY = "departementEtabBac";
	public static final String DEPT_NAISSANCE_KEY = "deptNaissance";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String D_NAISSANCE_KEY = "dNaissance";
	public static final String D_NATURALISATION_KEY = "dNaturalisation";
	public static final String ENFANTS_CHARGE_KEY = "enfantsCharge";
	public static final String ETUD_NUMERO_KEY = "etudNumero";
	public static final String INDICATEUR_PHOTO_KEY = "indicateurPhoto";
	public static final String INE_PROVISOIRE_KEY = "ineProvisoire";
	public static final String INSEE_PROVISOIRE_KEY = "inseeProvisoire";
	public static final String NO_INSEE_KEY = "noInsee";
	public static final String NOM_PATRONYMIQUE_KEY = "nomPatronymique";
	public static final String NOM_USUEL_KEY = "nomUsuel";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String PRENOM_KEY = "prenom";
	public static final String PRENOM2_KEY = "prenom2";
	public static final String PRE_TYPE_KEY = "preType";
	public static final String REIMMATRICULATION_KEY = "reimmatriculation";
	public static final String SIT_MILITAIRE_KEY = "sitMilitaire";
	public static final String SN_ATTESTATION_KEY = "snAttestation";
	public static final String SN_CERTIFICATION_KEY = "snCertification";
	public static final String SPORT_HN_KEY = "sportHN";
	public static final String T_HEB_CODE_KEY = "tHebCode";
	public static final String TYPE_INSCRIPTION_KEY = "typeInscription";
	public static final String VILLE_BAC_KEY = "villeBac";
	public static final String VILLE_NAISSANCE_KEY = "villeNaissance";

// Attributs non visibles
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String CIVILITE_KEY = "civilite";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String ETAB_CODE_SUP_KEY = "etabCodeSup";
	public static final String ID_DEMANDE_KEY = "idDemande";
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String ID_SITUATION_FAMILIALE_KEY = "idSituationFamiliale";
	public static final String MENT_CODE_KEY = "mentCode";
	public static final String NO_INDIVIDU_KEY = "noIndividu";
	public static final String PAYS_ETAB_BAC_KEY = "paysEtabBac";
	public static final String PAYS_NAISSANCE_KEY = "paysNaissance";
	public static final String PAYS_NATIONALITE_KEY = "paysNationalite";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String PRO_CODE2_KEY = "proCode2";

//Colonnes dans la base de donnees
	public static final String ACAD_CODE_BAC_COLKEY = "ACAD_CODE_BAC";
	public static final String ANNE1_INSC_UNIV_COLKEY = "ANNEE_1INSC_UNIV";
	public static final String ANNEE1_INSC_ETAB_COLKEY = "ANNEE_1Insc_ETAB";
	public static final String ANNEE1_INSC_SUP_COLKEY = "ANNEE_1INSC_SUP";
	public static final String ANNEE_BAC_COLKEY = "ANNEE_BAC";
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String C_DEPARTEMENT_PARENT_COLKEY = "C_DEPARTEMENT_PARENT";
	public static final String CLE_INSEE_COLKEY = "CLE_INSEE";
	public static final String CODE_INE_COLKEY = "CODE_INE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEPARTEMENT_ETAB_BAC_COLKEY = "DEPARTEMENT_ETAB_BAC";
	public static final String DEPT_NAISSANCE_COLKEY = "DEPT_NAISSANCE";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String D_NAISSANCE_COLKEY = "D_NAISSANCE";
	public static final String D_NATURALISATION_COLKEY = "D_NATURALISATION";
	public static final String ENFANTS_CHARGE_COLKEY = "ENFANTS_CHARGE";
	public static final String ETUD_NUMERO_COLKEY = "ETUD_NUMERO";
	public static final String INDICATEUR_PHOTO_COLKEY = "PHOTO";
	public static final String INE_PROVISOIRE_COLKEY = "INE_PROVISOIRE";
	public static final String INSEE_PROVISOIRE_COLKEY = "INSEE_PROVISOIRE";
	public static final String NO_INSEE_COLKEY = "NO_INSEE";
	public static final String NOM_PATRONYMIQUE_COLKEY = "NOM_PATRONYMIQUE";
	public static final String NOM_USUEL_COLKEY = "NOM_USUEL";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String PRENOM_COLKEY = "PRENOM";
	public static final String PRENOM2_COLKEY = "PRENOM2";
	public static final String PRE_TYPE_COLKEY = "PRE_TYPE";
	public static final String REIMMATRICULATION_COLKEY = "REIMMATRICULATION";
	public static final String SIT_MILITAIRE_COLKEY = "SIT_MILITAIRE";
	public static final String SN_ATTESTATION_COLKEY = "SN_ATTESTATION";
	public static final String SN_CERTIFICATION_COLKEY = "SN_CERTIFICATION";
	public static final String SPORT_HN_COLKEY = "SPORT_HN";
	public static final String T_HEB_CODE_COLKEY = "THEB_CODE";
	public static final String TYPE_INSCRIPTION_COLKEY = "TYPE_INSCRIPTION";
	public static final String VILLE_BAC_COLKEY = "VILLE_BAC";
	public static final String VILLE_NAISSANCE_COLKEY = "VILLE_NAISSANCE";

	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String CIVILITE_COLKEY = "CIVILITE";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String ETAB_CODE_SUP_COLKEY = "ETAB_CODE_SUP";
	public static final String ID_DEMANDE_COLKEY = "Id_DEMANDE";
	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String ID_SITUATION_FAMILIALE_COLKEY = "ID_SITUATION_FAMILIALE";
	public static final String MENT_CODE_COLKEY = "MENT_CODE";
	public static final String NO_INDIVIDU_COLKEY = "NO_INDIVIDU";
	public static final String PAYS_ETAB_BAC_COLKEY = "PAYS_ETAB_BAC";
	public static final String PAYS_NAISSANCE_COLKEY = "PAYS_NAISSANCE";
	public static final String PAYS_NATIONALITE_COLKEY = "PAYS_NATIONALITE";
	public static final String PRO_CODE_COLKEY = "PRO_CODE";
	public static final String PRO_CODE2_COLKEY = "PRO_CODE2";


	// Relationships
	public static final String TO_BACS_KEY = "toBacs";
	public static final String TO_CIVILITE_KEY = "toCivilite";
	public static final String TODEMANDE_KEY = "todemande";
	public static final String TO_DEPARTEMENT_NAISSANCE_KEY = "toDepartementNaissance";
	public static final String TO_ETUDIANT_KEY = "toEtudiant";
	public static final String TO_INDIVIDU_ETUDIANT_KEY = "toIndividuEtudiant";
	public static final String TO_MENTION_KEY = "toMention";
	public static final String TO_PAYS_ETAB_BAC_KEY = "toPaysEtabBac";
	public static final String TO_PAYS_NAISSANCE_KEY = "toPaysNaissance";
	public static final String TO_PAYS_NATIONALITE_KEY = "toPaysNationalite";
	public static final String TO_PRE_ADRESSES_KEY = "toPreAdresses";
	public static final String TO_PRE_BOURSES_KEY = "toPreBourses";
	public static final String TO_PRE_CURSUS_KEY = "toPreCursus";
	public static final String TO_PRE_ETUDIANT_ANNEE_KEY = "toPreEtudiantAnnee";
	public static final String TO_PRE_INSCRIPTIONS_KEY = "toPreInscriptions";
	public static final String TO_PRE_PHOTOS_KEY = "toPrePhotos";
	public static final String TO_PRE_TELEPHONES_KEY = "toPreTelephones";
	public static final String TO_PROFESSION1_KEY = "toProfession1";
	public static final String TO_PROFESSION2_KEY = "toProfession2";
	public static final String TO_RNE_ETAB_BAC_KEY = "toRneEtabBac";
	public static final String TO_RNE_ETAB_SUP_KEY = "toRneEtabSup";
	public static final String TO_SITUATION_FAMILIALE_KEY = "toSituationFamiliale";



	// Accessors methods
  public String acadCodeBac() {
    return (String) storedValueForKey(ACAD_CODE_BAC_KEY);
  }

  public void setAcadCodeBac(String value) {
    takeStoredValueForKey(value, ACAD_CODE_BAC_KEY);
  }

  public Integer anne1InscUniv() {
    return (Integer) storedValueForKey(ANNE1_INSC_UNIV_KEY);
  }

  public void setAnne1InscUniv(Integer value) {
    takeStoredValueForKey(value, ANNE1_INSC_UNIV_KEY);
  }

  public Integer annee1InscEtab() {
    return (Integer) storedValueForKey(ANNEE1_INSC_ETAB_KEY);
  }

  public void setAnnee1InscEtab(Integer value) {
    takeStoredValueForKey(value, ANNEE1_INSC_ETAB_KEY);
  }

  public Integer annee1InscSup() {
    return (Integer) storedValueForKey(ANNEE1_INSC_SUP_KEY);
  }

  public void setAnnee1InscSup(Integer value) {
    takeStoredValueForKey(value, ANNEE1_INSC_SUP_KEY);
  }

  public Integer anneeBac() {
    return (Integer) storedValueForKey(ANNEE_BAC_KEY);
  }

  public void setAnneeBac(Integer value) {
    takeStoredValueForKey(value, ANNEE_BAC_KEY);
  }

  public Integer candNumero() {
    return (Integer) storedValueForKey(CAND_NUMERO_KEY);
  }

  public void setCandNumero(Integer value) {
    takeStoredValueForKey(value, CAND_NUMERO_KEY);
  }

  public String cDepartementParent() {
    return (String) storedValueForKey(C_DEPARTEMENT_PARENT_KEY);
  }

  public void setCDepartementParent(String value) {
    takeStoredValueForKey(value, C_DEPARTEMENT_PARENT_KEY);
  }

  public Integer cleInsee() {
    return (Integer) storedValueForKey(CLE_INSEE_KEY);
  }

  public void setCleInsee(Integer value) {
    takeStoredValueForKey(value, CLE_INSEE_KEY);
  }

  public String codeIne() {
    return (String) storedValueForKey(CODE_INE_KEY);
  }

  public void setCodeIne(String value) {
    takeStoredValueForKey(value, CODE_INE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String departementEtabBac() {
    return (String) storedValueForKey(DEPARTEMENT_ETAB_BAC_KEY);
  }

  public void setDepartementEtabBac(String value) {
    takeStoredValueForKey(value, DEPARTEMENT_ETAB_BAC_KEY);
  }

  public String deptNaissance() {
    return (String) storedValueForKey(DEPT_NAISSANCE_KEY);
  }

  public void setDeptNaissance(String value) {
    takeStoredValueForKey(value, DEPT_NAISSANCE_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public NSTimestamp dNaissance() {
    return (NSTimestamp) storedValueForKey(D_NAISSANCE_KEY);
  }

  public void setDNaissance(NSTimestamp value) {
    takeStoredValueForKey(value, D_NAISSANCE_KEY);
  }

  public NSTimestamp dNaturalisation() {
    return (NSTimestamp) storedValueForKey(D_NATURALISATION_KEY);
  }

  public void setDNaturalisation(NSTimestamp value) {
    takeStoredValueForKey(value, D_NATURALISATION_KEY);
  }

  public Boolean enfantsCharge() {
    return (Boolean) storedValueForKey(ENFANTS_CHARGE_KEY);
  }

  public void setEnfantsCharge(Boolean value) {
    takeStoredValueForKey(value, ENFANTS_CHARGE_KEY);
  }

  public Integer etudNumero() {
    return (Integer) storedValueForKey(ETUD_NUMERO_KEY);
  }

  public void setEtudNumero(Integer value) {
    takeStoredValueForKey(value, ETUD_NUMERO_KEY);
  }

  public Boolean indicateurPhoto() {
    return (Boolean) storedValueForKey(INDICATEUR_PHOTO_KEY);
  }

  public void setIndicateurPhoto(Boolean value) {
    takeStoredValueForKey(value, INDICATEUR_PHOTO_KEY);
  }

  public Boolean ineProvisoire() {
    return (Boolean) storedValueForKey(INE_PROVISOIRE_KEY);
  }

  public void setIneProvisoire(Boolean value) {
    takeStoredValueForKey(value, INE_PROVISOIRE_KEY);
  }

  public Boolean inseeProvisoire() {
    return (Boolean) storedValueForKey(INSEE_PROVISOIRE_KEY);
  }

  public void setInseeProvisoire(Boolean value) {
    takeStoredValueForKey(value, INSEE_PROVISOIRE_KEY);
  }

  public String noInsee() {
    return (String) storedValueForKey(NO_INSEE_KEY);
  }

  public void setNoInsee(String value) {
    takeStoredValueForKey(value, NO_INSEE_KEY);
  }

  public String nomPatronymique() {
    return (String) storedValueForKey(NOM_PATRONYMIQUE_KEY);
  }

  public void setNomPatronymique(String value) {
    takeStoredValueForKey(value, NOM_PATRONYMIQUE_KEY);
  }

  public String nomUsuel() {
    return (String) storedValueForKey(NOM_USUEL_KEY);
  }

  public void setNomUsuel(String value) {
    takeStoredValueForKey(value, NOM_USUEL_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String prenom() {
    return (String) storedValueForKey(PRENOM_KEY);
  }

  public void setPrenom(String value) {
    takeStoredValueForKey(value, PRENOM_KEY);
  }

  public String prenom2() {
    return (String) storedValueForKey(PRENOM2_KEY);
  }

  public void setPrenom2(String value) {
    takeStoredValueForKey(value, PRENOM2_KEY);
  }

  public String preType() {
    return (String) storedValueForKey(PRE_TYPE_KEY);
  }

  public void setPreType(String value) {
    takeStoredValueForKey(value, PRE_TYPE_KEY);
  }

  public String reimmatriculation() {
    return (String) storedValueForKey(REIMMATRICULATION_KEY);
  }

  public void setReimmatriculation(String value) {
    takeStoredValueForKey(value, REIMMATRICULATION_KEY);
  }

  public String sitMilitaire() {
    return (String) storedValueForKey(SIT_MILITAIRE_KEY);
  }

  public void setSitMilitaire(String value) {
    takeStoredValueForKey(value, SIT_MILITAIRE_KEY);
  }

  public Integer snAttestation() {
    return (Integer) storedValueForKey(SN_ATTESTATION_KEY);
  }

  public void setSnAttestation(Integer value) {
    takeStoredValueForKey(value, SN_ATTESTATION_KEY);
  }

  public Integer snCertification() {
    return (Integer) storedValueForKey(SN_CERTIFICATION_KEY);
  }

  public void setSnCertification(Integer value) {
    takeStoredValueForKey(value, SN_CERTIFICATION_KEY);
  }

  public Boolean sportHN() {
    return (Boolean) storedValueForKey(SPORT_HN_KEY);
  }

  public void setSportHN(Boolean value) {
    takeStoredValueForKey(value, SPORT_HN_KEY);
  }

  public String tHebCode() {
    return (String) storedValueForKey(T_HEB_CODE_KEY);
  }

  public void setTHebCode(String value) {
    takeStoredValueForKey(value, T_HEB_CODE_KEY);
  }

  public String typeInscription() {
    return (String) storedValueForKey(TYPE_INSCRIPTION_KEY);
  }

  public void setTypeInscription(String value) {
    takeStoredValueForKey(value, TYPE_INSCRIPTION_KEY);
  }

  public String villeBac() {
    return (String) storedValueForKey(VILLE_BAC_KEY);
  }

  public void setVilleBac(String value) {
    takeStoredValueForKey(value, VILLE_BAC_KEY);
  }

  public String villeNaissance() {
    return (String) storedValueForKey(VILLE_NAISSANCE_KEY);
  }

  public void setVilleNaissance(String value) {
    takeStoredValueForKey(value, VILLE_NAISSANCE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOBac toBacs() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOBac)storedValueForKey(TO_BACS_KEY);
  }

  public void setToBacsRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOBac value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOBac oldValue = toBacs();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_BACS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_BACS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toCivilite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOCivilite)storedValueForKey(TO_CIVILITE_KEY);
  }

  public void setToCiviliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOCivilite value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOCivilite oldValue = toCivilite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CIVILITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CIVILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlworkflow.serveur.metier.EODemande todemande() {
    return (org.cocktail.fwkcktlworkflow.serveur.metier.EODemande)storedValueForKey(TODEMANDE_KEY);
  }

  public void setTodemandeRelationship(org.cocktail.fwkcktlworkflow.serveur.metier.EODemande value) {
    if (value == null) {
    	org.cocktail.fwkcktlworkflow.serveur.metier.EODemande oldValue = todemande();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TODEMANDE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TODEMANDE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODepartement toDepartementNaissance() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODepartement)storedValueForKey(TO_DEPARTEMENT_NAISSANCE_KEY);
  }

  public void setToDepartementNaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODepartement value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EODepartement oldValue = toDepartementNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_DEPARTEMENT_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_DEPARTEMENT_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant toEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant)storedValueForKey(TO_ETUDIANT_KEY);
  }

  public void setToEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant oldValue = toEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu toIndividuEtudiant() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(TO_INDIVIDU_ETUDIANT_KEY);
  }

  public void setToIndividuEtudiantRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = toIndividuEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_INDIVIDU_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_INDIVIDU_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOMention toMention() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOMention)storedValueForKey(TO_MENTION_KEY);
  }

  public void setToMentionRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOMention value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOMention oldValue = toMention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysEtabBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_ETAB_BAC_KEY);
  }

  public void setToPaysEtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysEtabBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_ETAB_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_ETAB_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNaissance() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_NAISSANCE_KEY);
  }

  public void setToPaysNaissanceRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysNaissance();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NAISSANCE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NAISSANCE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNationalite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_NATIONALITE_KEY);
  }

  public void setToPaysNationaliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPaysNationalite();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_NATIONALITE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_NATIONALITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession1() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(TO_PROFESSION1_KEY);
  }

  public void setToProfession1Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession1();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROFESSION1_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROFESSION1_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession2() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(TO_PROFESSION2_KEY);
  }

  public void setToProfession2Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROFESSION2_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROFESSION2_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneEtabBac() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_ETAB_BAC_KEY);
  }

  public void setToRneEtabBacRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneEtabBac();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_ETAB_BAC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_ETAB_BAC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRneEtabSup() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_ETAB_SUP_KEY);
  }

  public void setToRneEtabSupRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRneEtabSup();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_ETAB_SUP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_ETAB_SUP_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toSituationFamiliale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale)storedValueForKey(TO_SITUATION_FAMILIALE_KEY);
  }

  public void setToSituationFamilialeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale oldValue = toSituationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_FAMILIALE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> toPreAdresses() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>)storedValueForKey(TO_PRE_ADRESSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> toPreAdresses(EOQualifier qualifier) {
    return toPreAdresses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> toPreAdresses(EOQualifier qualifier, boolean fetch) {
    return toPreAdresses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> toPreAdresses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreAdresses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreAdressesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_ADRESSES_KEY);
  }

  public void removeFromToPreAdressesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_ADRESSES_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse createToPreAdressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreAdresse");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_ADRESSES_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse) eo;
  }

  public void deleteToPreAdressesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_ADRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreAdressesRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> objects = toPreAdresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreAdressesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> toPreBourses() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses>)storedValueForKey(TO_PRE_BOURSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> toPreBourses(EOQualifier qualifier) {
    return toPreBourses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> toPreBourses(EOQualifier qualifier, boolean fetch) {
    return toPreBourses(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> toPreBourses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreBourses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreBoursesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_BOURSES_KEY);
  }

  public void removeFromToPreBoursesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_BOURSES_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses createToPreBoursesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreBourses");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_BOURSES_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses) eo;
  }

  public void deleteToPreBoursesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_BOURSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreBoursesRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreBourses> objects = toPreBourses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreBoursesRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> toPreCursus() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus>)storedValueForKey(TO_PRE_CURSUS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> toPreCursus(EOQualifier qualifier) {
    return toPreCursus(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> toPreCursus(EOQualifier qualifier, boolean fetch) {
    return toPreCursus(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> toPreCursus(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreCursus();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreCursusRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_CURSUS_KEY);
  }

  public void removeFromToPreCursusRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_CURSUS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus createToPreCursusRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreCursus");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_CURSUS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus) eo;
  }

  public void deleteToPreCursusRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_CURSUS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreCursusRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> objects = toPreCursus().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreCursusRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> toPreEtudiantAnnee() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>)storedValueForKey(TO_PRE_ETUDIANT_ANNEE_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> toPreEtudiantAnnee(EOQualifier qualifier) {
    return toPreEtudiantAnnee(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> toPreEtudiantAnnee(EOQualifier qualifier, boolean fetch) {
    return toPreEtudiantAnnee(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> toPreEtudiantAnnee(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreEtudiantAnnee();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreEtudiantAnneeRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_ETUDIANT_ANNEE_KEY);
  }

  public void removeFromToPreEtudiantAnneeRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_ETUDIANT_ANNEE_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee createToPreEtudiantAnneeRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreEtudiantAnnee");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_ETUDIANT_ANNEE_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee) eo;
  }

  public void deleteToPreEtudiantAnneeRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_ETUDIANT_ANNEE_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreEtudiantAnneeRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> objects = toPreEtudiantAnnee().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreEtudiantAnneeRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toPreInscriptions() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)storedValueForKey(TO_PRE_INSCRIPTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toPreInscriptions(EOQualifier qualifier) {
    return toPreInscriptions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toPreInscriptions(EOQualifier qualifier, boolean fetch) {
    return toPreInscriptions(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toPreInscriptions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreInscriptions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_INSCRIPTIONS_KEY);
  }

  public void removeFromToPreInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_INSCRIPTIONS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription createToPreInscriptionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreInscription");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_INSCRIPTIONS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription) eo;
  }

  public void deleteToPreInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_INSCRIPTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreInscriptionsRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> objects = toPreInscriptions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreInscriptionsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> toPrePhotos() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto>)storedValueForKey(TO_PRE_PHOTOS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> toPrePhotos(EOQualifier qualifier) {
    return toPrePhotos(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> toPrePhotos(EOQualifier qualifier, boolean fetch) {
    return toPrePhotos(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> toPrePhotos(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrePhotos();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrePhotosRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_PHOTOS_KEY);
  }

  public void removeFromToPrePhotosRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PHOTOS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto createToPrePhotosRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPrePhoto");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_PHOTOS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto) eo;
  }

  public void deleteToPrePhotosRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PHOTOS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrePhotosRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePhoto> objects = toPrePhotos().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrePhotosRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> toPreTelephones() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone>)storedValueForKey(TO_PRE_TELEPHONES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> toPreTelephones(EOQualifier qualifier) {
    return toPreTelephones(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> toPreTelephones(EOQualifier qualifier, boolean fetch) {
    return toPreTelephones(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> toPreTelephones(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPreTelephones();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPreTelephonesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_TELEPHONES_KEY);
  }

  public void removeFromToPreTelephonesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_TELEPHONES_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone createToPreTelephonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreTelephone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_TELEPHONES_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone) eo;
  }

  public void deleteToPreTelephonesRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_TELEPHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPreTelephonesRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreTelephone> objects = toPreTelephones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPreTelephonesRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPreEtudiant avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreEtudiant createEOPreEtudiant(EOEditingContext editingContext, NSTimestamp dCreation
, Boolean enfantsCharge
, Boolean indicateurPhoto
, Boolean ineProvisoire
, Boolean inseeProvisoire
, String prenom
, String preType
, String typeInscription
, org.cocktail.fwkcktlpersonne.common.metier.EOCivilite toCivilite, org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNaissance, org.cocktail.fwkcktlpersonne.common.metier.EOPays toPaysNationalite, org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toSituationFamiliale			) {
    EOPreEtudiant eo = (EOPreEtudiant) createAndInsertInstance(editingContext, _EOPreEtudiant.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setEnfantsCharge(enfantsCharge);
		eo.setIndicateurPhoto(indicateurPhoto);
		eo.setIneProvisoire(ineProvisoire);
		eo.setInseeProvisoire(inseeProvisoire);
		eo.setPrenom(prenom);
		eo.setPreType(preType);
		eo.setTypeInscription(typeInscription);
    eo.setToCiviliteRelationship(toCivilite);
    eo.setToPaysNaissanceRelationship(toPaysNaissance);
    eo.setToPaysNationaliteRelationship(toPaysNationalite);
    eo.setToSituationFamilialeRelationship(toSituationFamiliale);
    return eo;
  }

  
	  public EOPreEtudiant localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreEtudiant)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreEtudiant creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreEtudiant creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreEtudiant object = (EOPreEtudiant)createAndInsertInstance(editingContext, _EOPreEtudiant.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreEtudiant localInstanceIn(EOEditingContext editingContext, EOPreEtudiant eo) {
    EOPreEtudiant localInstance = (eo == null) ? null : (EOPreEtudiant)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreEtudiant#localInstanceIn a la place.
   */
	public static EOPreEtudiant localInstanceOf(EOEditingContext editingContext, EOPreEtudiant eo) {
		return EOPreEtudiant.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreEtudiant fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreEtudiant fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreEtudiant> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreEtudiant)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreEtudiant fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreEtudiant> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreEtudiant eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreEtudiant)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreEtudiant fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreEtudiant eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreEtudiant ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreEtudiant fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
