/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale;
import org.cocktail.fwkcktlpersonne.common.metier.ISituationFamiliale;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeInscriptionFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeParticulier;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeInscriptionFormation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXKey;

/**
 * The Class EOPreEtudiantAnnee.
 * @author isabelle Réau
 */
public class EOPreEtudiantAnnee extends _EOPreEtudiantAnnee implements IPreEtudiantAnnee {

	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreEtudiantAnnee.class);

	public static final String BOURSIER_KEY = "boursier";
	public static final ERXKey<Boolean> BOURSIER = new ERXKey<Boolean>(BOURSIER_KEY);

	/**
	 * Instantiates a new eO pre etudiant annee.
	 */
	public EOPreEtudiantAnnee() {
		super();
	}

	/**
	 * Creer.
	 * @param edc contexte d'édition
	 * @param persId :
	 * @param etudiant etudiant inscrit
	 * @param annee année d'inscription
	 * @return EODiplome
	 */
	public static EOPreEtudiantAnnee creer(EOEditingContext edc, Integer persId, EOPreEtudiant etudiant, Integer annee) {

		EOPreEtudiantAnnee etudiantAnnee = new EOPreEtudiantAnnee();
		edc.insertObject(etudiantAnnee);
		etudiantAnnee.setToPreEtudiantRelationship(etudiant);
		etudiantAnnee.setAnnee(annee);
		etudiantAnnee.setPersIdCreation(persId);
		etudiantAnnee.setPersIdModification(persId);
		etudiantAnnee.setDCreation(new NSTimestamp());
		etudiantAnnee.setDModification(new NSTimestamp());
		etudiantAnnee.initPreEtudiantAnnee();

		return etudiantAnnee;
	}

	/**
	 * initilaisation des valeurs par défaut
	 */
	public void initPreEtudiantAnnee() {
		setAdhMutuelle(false);
		setAffSs(false);
		setAssuranceCivile(false);
		setCertificatPartAppel(0);
		setDemandeurEmploi(false);
		setEnfantsCharge(false);
		setNbEnfantsCharge(Integer.valueOf(0));
		setEtudHandicap(false);
		setEtudSportHN(false);
		setFormationFinancee(false);
		setInterruptEtude(false);
		setRecensement(0);
		setSportUniv(false);
	}

	/**
	 * {@inheritDoc}
	 */
	public IInfosEtudiant toInfosEtudiant() {
		return super.toPreEtudiant();
	}
	
	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws ValidationException the validation exception
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	/**
	 * @return les paiements de l'étudiant pour cette année.
	 */
	public List<? extends IPaiement> toPaiements() {
		return super.toPrePaiements();
	}
	
	/**
	 * @return Paiement
	 */
	public IPaiement toPaiementInitial() {
		return toPrePaiementInitial();
	}
	
	public IPrePaiement toPrePaiementInitial() {
		return super.toPrePaiements(EOPrePaiement.ORDRE.eq(1)).lastObject();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean boursier() {
		return (isStatutBourseCNOUSEchelonValide() || toPreEtudiant().toPreBourses(annee()).size() > 0);
	}

	/**
	 * {@inheritDoc}
	 */
	public List<? extends IBourses> toBourses() {
		return toPreEtudiant().toPreBourses(annee());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public Boolean isStatutBourseCNOUSEchelonValide() {
		// TODO à completer si besoin (échelon 0Bis)
		if (statutBoursierCnous() != null) {
			return StringUtils.isNumeric(statutBoursierCnous());
		}

		return false;
	}

	/**
	 * @return <code>true</code> si l'étudiant année est valide (un pré est toujours valide)
	 */
	public Boolean valide() {
		return true;
	}

	/**
	 * {@inheritDoc}
	 */
    public void setToPreEtudiantRelationship(IPreEtudiant value) {
        super.setToPreEtudiantRelationship((EOPreEtudiant) value);
    }

	/**
	 * {@inheritDoc}
	 */
	public void setToRegimeParticulierRelationship(IRegimeParticulier unRegimeParticulier) {
		super.setToRegimeParticulierRelationship((EORegimeParticulier) unRegimeParticulier);
	}
	
	public IPreInscription getInscriptionPrincipale() {
		EOQualifier qualifier = EOPreInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).eq(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		NSArray<EOPreInscription> listeInscription = toInscriptions(qualifier);
		if (listeInscription != null && listeInscription.size() > 0) {
			return listeInscription.lastObject();
		} else {
			qualifier = EOPreInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).eq(ITypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_ECHANGE_INTERNATIONAL);
			listeInscription = toInscriptions(qualifier);
			if (listeInscription != null && listeInscription.size() > 0) {
				return listeInscription.lastObject();
			}
		}
		return null;
	}

	/**
	 * {@inheritDoc}
	 */
	public void setToSituationFamilialeRelationship(ISituationFamiliale value) {
		super.setToSituationFamilialeRelationship((EOSituationFamiliale) value);
	}

	
	public List<IPreInscription> getInscriptionsComplementaires() {
		EOQualifier qualifier = EOPreInscription.TO_TYPE_INSCRIPTION_FORMATION.dot(EOTypeInscriptionFormation.CODE_INSCRIPTION).ne(EOTypeInscriptionFormation.TYPE_INSCRIPTION_FORMATION_PRINCIPALE);
		return new ArrayList<IPreInscription>(toInscriptions(qualifier, EOInscription.D_CREATION.ascs(), false));
	}

	public IPreInscription getInscriptionComplementaire(int index) {
		if (getInscriptionsComplementaires().size() > index) {
			return getInscriptionsComplementaires().get(index);
		} else {
			return null;
		}
	}
	
	public IPreInscription getInscriptionSecondaire() {
		return getInscriptionComplementaire(0);
	}
	
	public IPreInscription getInscriptionTertiaire() {
		return getInscriptionComplementaire(1);
	}
}
