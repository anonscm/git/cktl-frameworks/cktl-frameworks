/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreCursus.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreCursus extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreCursus.class);

	public static final String ENTITY_NAME = "EOPreCursus";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_CURSUS";


// Attribute Keys
  public static final ERXKey<Integer> ACCES_ANNEE = new ERXKey<Integer>("accesAnnee");
  public static final ERXKey<Integer> ANNEE_DEBUT = new ERXKey<Integer>("anneeDebut");
  public static final ERXKey<Integer> ANNEE_FIN = new ERXKey<Integer>("anneeFin");
  public static final ERXKey<String> C_PAYS = new ERXKey<String>("cPays");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<String> DIPLOME = new ERXKey<String>("diplome");
  public static final ERXKey<NSTimestamp> D_MODIFCATION = new ERXKey<NSTimestamp>("dModifcation");
  public static final ERXKey<String> ETAB_ETRANGER = new ERXKey<String>("etabEtranger");
  public static final ERXKey<String> FORMATION = new ERXKey<String>("formation");
  public static final ERXKey<Boolean> INTERRUPTION_ETUD = new ERXKey<Boolean>("interruptionEtud");
  public static final ERXKey<Integer> NIVEAU = new ERXKey<Integer>("niveau");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MOFICATION = new ERXKey<Integer>("persIdMofication");
  public static final ERXKey<String> TYPE_INSCRIPTION = new ERXKey<String>("typeInscription");
  public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention> TO_ACCES_MENTION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention>("toAccesMention");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_ACCES_TITRE_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toAccesTitreGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_C_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toCRne");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire> TO_GRADE_UNIVERSITAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire>("toGradeUniversitaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPays");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation> TO_TYPE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation>("toTypeFormation");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreCursus";

	public static final String ACCES_ANNEE_KEY = "accesAnnee";
	public static final String ANNEE_DEBUT_KEY = "anneeDebut";
	public static final String ANNEE_FIN_KEY = "anneeFin";
	public static final String C_PAYS_KEY = "cPays";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DIPLOME_KEY = "diplome";
	public static final String D_MODIFCATION_KEY = "dModifcation";
	public static final String ETAB_ETRANGER_KEY = "etabEtranger";
	public static final String FORMATION_KEY = "formation";
	public static final String INTERRUPTION_ETUD_KEY = "interruptionEtud";
	public static final String NIVEAU_KEY = "niveau";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MOFICATION_KEY = "persIdMofication";
	public static final String TYPE_INSCRIPTION_KEY = "typeInscription";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String ACCESS_MENTION_KEY = "accessMention";
	public static final String ACCES_TITRE_KEY = "accesTitre";
	public static final String CRNE_KEY = "crne";
	public static final String GRADE_KEY = "grade";
	public static final String ID_PRE_CURSUS_KEY = "idPreCursus";
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String TYPE_KEY = "type";

//Colonnes dans la base de donnees
	public static final String ACCES_ANNEE_COLKEY = "ACCES_ANNEE";
	public static final String ANNEE_DEBUT_COLKEY = "ANNEE_DEBUT";
	public static final String ANNEE_FIN_COLKEY = "ANNEE_FIN";
	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DIPLOME_COLKEY = "DIPLOME";
	public static final String D_MODIFCATION_COLKEY = "D_MODIFICATION";
	public static final String ETAB_ETRANGER_COLKEY = "ETAB_ETRANGER";
	public static final String FORMATION_COLKEY = "FORMATION";
	public static final String INTERRUPTION_ETUD_COLKEY = "INTERRUPTION_ETUD";
	public static final String NIVEAU_COLKEY = "NIVEAU";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MOFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String TYPE_INSCRIPTION_COLKEY = "TYPE_INSCTRIPTION";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String ACCESS_MENTION_COLKEY = "ACCES_MENTION";
	public static final String ACCES_TITRE_COLKEY = "ACCES_TITRE";
	public static final String CRNE_COLKEY = "C_RNE";
	public static final String GRADE_COLKEY = "GRADE";
	public static final String ID_PRE_CURSUS_COLKEY = "ID_PRE_CURSUS";
	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String TYPE_COLKEY = "TYPE";


	// Relationships
	public static final String TO_ACCES_MENTION_KEY = "toAccesMention";
	public static final String TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY = "toAccesTitreGradeUniversitaire";
	public static final String TO_C_RNE_KEY = "toCRne";
	public static final String TO_GRADE_UNIVERSITAIRE_KEY = "toGradeUniversitaire";
	public static final String TO_PAYS_KEY = "toPays";
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";
	public static final String TO_TYPE_FORMATION_KEY = "toTypeFormation";



	// Accessors methods
  public Integer accesAnnee() {
    return (Integer) storedValueForKey(ACCES_ANNEE_KEY);
  }

  public void setAccesAnnee(Integer value) {
    takeStoredValueForKey(value, ACCES_ANNEE_KEY);
  }

  public Integer anneeDebut() {
    return (Integer) storedValueForKey(ANNEE_DEBUT_KEY);
  }

  public void setAnneeDebut(Integer value) {
    takeStoredValueForKey(value, ANNEE_DEBUT_KEY);
  }

  public Integer anneeFin() {
    return (Integer) storedValueForKey(ANNEE_FIN_KEY);
  }

  public void setAnneeFin(Integer value) {
    takeStoredValueForKey(value, ANNEE_FIN_KEY);
  }

  public String cPays() {
    return (String) storedValueForKey(C_PAYS_KEY);
  }

  public void setCPays(String value) {
    takeStoredValueForKey(value, C_PAYS_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public String diplome() {
    return (String) storedValueForKey(DIPLOME_KEY);
  }

  public void setDiplome(String value) {
    takeStoredValueForKey(value, DIPLOME_KEY);
  }

  public NSTimestamp dModifcation() {
    return (NSTimestamp) storedValueForKey(D_MODIFCATION_KEY);
  }

  public void setDModifcation(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFCATION_KEY);
  }

  public String etabEtranger() {
    return (String) storedValueForKey(ETAB_ETRANGER_KEY);
  }

  public void setEtabEtranger(String value) {
    takeStoredValueForKey(value, ETAB_ETRANGER_KEY);
  }

  public String formation() {
    return (String) storedValueForKey(FORMATION_KEY);
  }

  public void setFormation(String value) {
    takeStoredValueForKey(value, FORMATION_KEY);
  }

  public Boolean interruptionEtud() {
    return (Boolean) storedValueForKey(INTERRUPTION_ETUD_KEY);
  }

  public void setInterruptionEtud(Boolean value) {
    takeStoredValueForKey(value, INTERRUPTION_ETUD_KEY);
  }

  public Integer niveau() {
    return (Integer) storedValueForKey(NIVEAU_KEY);
  }

  public void setNiveau(Integer value) {
    takeStoredValueForKey(value, NIVEAU_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdMofication() {
    return (Integer) storedValueForKey(PERS_ID_MOFICATION_KEY);
  }

  public void setPersIdMofication(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MOFICATION_KEY);
  }

  public String typeInscription() {
    return (String) storedValueForKey(TYPE_INSCRIPTION_KEY);
  }

  public void setTypeInscription(String value) {
    takeStoredValueForKey(value, TYPE_INSCRIPTION_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention toAccesMention() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention)storedValueForKey(TO_ACCES_MENTION_KEY);
  }

  public void setToAccesMentionRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMention oldValue = toAccesMention();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCES_MENTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCES_MENTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toAccesTitreGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToAccesTitreGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toAccesTitreGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACCES_TITRE_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toCRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_C_RNE_KEY);
  }

  public void setToCRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toCRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_C_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_C_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire toGradeUniversitaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire)storedValueForKey(TO_GRADE_UNIVERSITAIRE_KEY);
  }

  public void setToGradeUniversitaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire oldValue = toGradeUniversitaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_GRADE_UNIVERSITAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_GRADE_UNIVERSITAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_KEY);
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation toTypeFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation)storedValueForKey(TO_TYPE_FORMATION_KEY);
  }

  public void setToTypeFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation oldValue = toTypeFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_FORMATION_KEY);
    }
  }
  

/**
 * Créer une instance de EOPreCursus avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreCursus createEOPreCursus(EOEditingContext editingContext, Integer anneeDebut
, NSTimestamp dCreation
, Boolean interruptionEtud
, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant			) {
    EOPreCursus eo = (EOPreCursus) createAndInsertInstance(editingContext, _EOPreCursus.ENTITY_NAME);    
		eo.setAnneeDebut(anneeDebut);
		eo.setDCreation(dCreation);
		eo.setInterruptionEtud(interruptionEtud);
    eo.setToPreEtudiantRelationship(toPreEtudiant);
    return eo;
  }

  
	  public EOPreCursus localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreCursus)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreCursus creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreCursus creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreCursus object = (EOPreCursus)createAndInsertInstance(editingContext, _EOPreCursus.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreCursus localInstanceIn(EOEditingContext editingContext, EOPreCursus eo) {
    EOPreCursus localInstance = (eo == null) ? null : (EOPreCursus)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreCursus#localInstanceIn a la place.
   */
	public static EOPreCursus localInstanceOf(EOEditingContext editingContext, EOPreCursus eo) {
		return EOPreCursus.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreCursus fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreCursus fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreCursus> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreCursus eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreCursus)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreCursus fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreCursus fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreCursus> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreCursus eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreCursus)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreCursus fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreCursus eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreCursus ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreCursus fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
