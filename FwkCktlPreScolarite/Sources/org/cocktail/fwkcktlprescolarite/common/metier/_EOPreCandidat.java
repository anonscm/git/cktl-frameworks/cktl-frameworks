/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreCandidat.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreCandidat extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreCandidat.class);

	public static final String ENTITY_NAME = "EOPreCandidat";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_CANDIDAT";


// Attribute Keys
  public static final ERXKey<Integer> ANNE_PREM_ETAB = new ERXKey<Integer>("annePremEtab");
  public static final ERXKey<String> BAC_CODE = new ERXKey<String>("bacCode");
  public static final ERXKey<String> CAND_ADR1_PARENT = new ERXKey<String>("candAdr1Parent");
  public static final ERXKey<String> CAND_ADR2_PARENT = new ERXKey<String>("candAdr2Parent");
  public static final ERXKey<String> CAND_ADR3_PARENT = new ERXKey<String>("candAdr3Parent");
  public static final ERXKey<Integer> CAND_AN_BAC = new ERXKey<Integer>("candAnBac");
  public static final ERXKey<String> CAND_BEA = new ERXKey<String>("candBea");
  public static final ERXKey<String> CAND_CIVILITE = new ERXKey<String>("candCivilite");
  public static final ERXKey<String> CAND_COM_NAIS = new ERXKey<String>("candComNais");
  public static final ERXKey<String> CAND_COM_NAIS_CODE = new ERXKey<String>("candComNaisCode");
  public static final ERXKey<String> CAND_CP_PARENT = new ERXKey<String>("candCpParent");
  public static final ERXKey<String> CAND_DATE_NAIS = new ERXKey<String>("candDateNais");
  public static final ERXKey<String> CAND_DATE_NAIS_PROVISOIRE = new ERXKey<String>("candDateNaisProvisoire");
  public static final ERXKey<String> CAND_EMAIL_SCOL = new ERXKey<String>("candEmailScol");
  public static final ERXKey<Integer> CAND_ENFANTS_CHARGE = new ERXKey<Integer>("candEnfantsCharge");
  public static final ERXKey<String> CAND_ETAB_CHOIX = new ERXKey<String>("candEtabChoix");
  public static final ERXKey<String> CAND_NOM = new ERXKey<String>("candNom");
  public static final ERXKey<String> CAND_NOM_USUEL = new ERXKey<String>("candNomUsuel");
  public static final ERXKey<Integer> CAND_NUMERO = new ERXKey<Integer>("candNumero");
  public static final ERXKey<String> CAND_PAYS_PARENT = new ERXKey<String>("candPaysParent");
  public static final ERXKey<String> CAND_PORT_SCOL = new ERXKey<String>("candPortScol");
  public static final ERXKey<String> CAND_PRENOM = new ERXKey<String>("candPrenom");
  public static final ERXKey<String> CAND_PRENOM2 = new ERXKey<String>("candPrenom2");
  public static final ERXKey<String> CAND_PRENOM3 = new ERXKey<String>("candPrenom3");
  public static final ERXKey<String> CAND_TEL_PARENT = new ERXKey<String>("candTelParent");
  public static final ERXKey<String> CAND_VILLE_BAC = new ERXKey<String>("candVilleBac");
  public static final ERXKey<String> CAND_VILLE_PARENT = new ERXKey<String>("candVilleParent");
  public static final ERXKey<String> CAND_VILLE_PARENT_CODE = new ERXKey<String>("candVilleParentCode");
  public static final ERXKey<String> DPTG_CODE_DER_ETAB = new ERXKey<String>("dptgCodeDerEtab");
  public static final ERXKey<String> DPTG_CODE_NAIS = new ERXKey<String>("dptgCodeNais");
  public static final ERXKey<String> DPTG_ETAB_BAC = new ERXKey<String>("dptgEtabBac");
  public static final ERXKey<String> ETAB_CODE_BAC = new ERXKey<String>("etabCodeBac");
  public static final ERXKey<String> ETAB_CODE_DER_ETAB = new ERXKey<String>("etabCodeDerEtab");
  public static final ERXKey<String> ETAB_LIBELLE_BAC = new ERXKey<String>("etabLibelleBac");
  public static final ERXKey<String> HISTENS_DER_ETAB = new ERXKey<String>("histensDerEtab");
  public static final ERXKey<String> HIST_LIBELLE_DER_ETAB = new ERXKey<String>("histLibelleDerEtab");
  public static final ERXKey<String> HISTVILLE_DER_ETAB = new ERXKey<String>("histvilleDerEtab");
  public static final ERXKey<Integer> NAT_ORDRE = new ERXKey<Integer>("natOrdre");
  public static final ERXKey<String> PAYS_CODE_DER_ETAB = new ERXKey<String>("paysCodeDerEtab");
  public static final ERXKey<String> PAYS_CODE_NAIS = new ERXKey<String>("paysCodeNais");
  public static final ERXKey<String> PAYS_ETAB_BAC = new ERXKey<String>("paysEtabBac");
  public static final ERXKey<String> PRO_CODE1 = new ERXKey<String>("proCode1");
  public static final ERXKey<String> PRO_CODE2 = new ERXKey<String>("proCode2");
  // Relationship Keys

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "candNumero";

	public static final String ANNE_PREM_ETAB_KEY = "annePremEtab";
	public static final String BAC_CODE_KEY = "bacCode";
	public static final String CAND_ADR1_PARENT_KEY = "candAdr1Parent";
	public static final String CAND_ADR2_PARENT_KEY = "candAdr2Parent";
	public static final String CAND_ADR3_PARENT_KEY = "candAdr3Parent";
	public static final String CAND_AN_BAC_KEY = "candAnBac";
	public static final String CAND_BEA_KEY = "candBea";
	public static final String CAND_CIVILITE_KEY = "candCivilite";
	public static final String CAND_COM_NAIS_KEY = "candComNais";
	public static final String CAND_COM_NAIS_CODE_KEY = "candComNaisCode";
	public static final String CAND_CP_PARENT_KEY = "candCpParent";
	public static final String CAND_DATE_NAIS_KEY = "candDateNais";
	public static final String CAND_DATE_NAIS_PROVISOIRE_KEY = "candDateNaisProvisoire";
	public static final String CAND_EMAIL_SCOL_KEY = "candEmailScol";
	public static final String CAND_ENFANTS_CHARGE_KEY = "candEnfantsCharge";
	public static final String CAND_ETAB_CHOIX_KEY = "candEtabChoix";
	public static final String CAND_NOM_KEY = "candNom";
	public static final String CAND_NOM_USUEL_KEY = "candNomUsuel";
	public static final String CAND_NUMERO_KEY = "candNumero";
	public static final String CAND_PAYS_PARENT_KEY = "candPaysParent";
	public static final String CAND_PORT_SCOL_KEY = "candPortScol";
	public static final String CAND_PRENOM_KEY = "candPrenom";
	public static final String CAND_PRENOM2_KEY = "candPrenom2";
	public static final String CAND_PRENOM3_KEY = "candPrenom3";
	public static final String CAND_TEL_PARENT_KEY = "candTelParent";
	public static final String CAND_VILLE_BAC_KEY = "candVilleBac";
	public static final String CAND_VILLE_PARENT_KEY = "candVilleParent";
	public static final String CAND_VILLE_PARENT_CODE_KEY = "candVilleParentCode";
	public static final String DPTG_CODE_DER_ETAB_KEY = "dptgCodeDerEtab";
	public static final String DPTG_CODE_NAIS_KEY = "dptgCodeNais";
	public static final String DPTG_ETAB_BAC_KEY = "dptgEtabBac";
	public static final String ETAB_CODE_BAC_KEY = "etabCodeBac";
	public static final String ETAB_CODE_DER_ETAB_KEY = "etabCodeDerEtab";
	public static final String ETAB_LIBELLE_BAC_KEY = "etabLibelleBac";
	public static final String HISTENS_DER_ETAB_KEY = "histensDerEtab";
	public static final String HIST_LIBELLE_DER_ETAB_KEY = "histLibelleDerEtab";
	public static final String HISTVILLE_DER_ETAB_KEY = "histvilleDerEtab";
	public static final String NAT_ORDRE_KEY = "natOrdre";
	public static final String PAYS_CODE_DER_ETAB_KEY = "paysCodeDerEtab";
	public static final String PAYS_CODE_NAIS_KEY = "paysCodeNais";
	public static final String PAYS_ETAB_BAC_KEY = "paysEtabBac";
	public static final String PRO_CODE1_KEY = "proCode1";
	public static final String PRO_CODE2_KEY = "proCode2";

// Attributs non visibles

//Colonnes dans la base de donnees
	public static final String ANNE_PREM_ETAB_COLKEY = "ANNEE_PREM_ETAB";
	public static final String BAC_CODE_COLKEY = "BAC_CODE";
	public static final String CAND_ADR1_PARENT_COLKEY = "CAND_ADR1_PARENT";
	public static final String CAND_ADR2_PARENT_COLKEY = "CAND_ADR2_PARENT";
	public static final String CAND_ADR3_PARENT_COLKEY = "CAND_ADR3_PARENT";
	public static final String CAND_AN_BAC_COLKEY = "CAND_ANBAC";
	public static final String CAND_BEA_COLKEY = "CAND_BEA";
	public static final String CAND_CIVILITE_COLKEY = "CAND_CIVILITE";
	public static final String CAND_COM_NAIS_COLKEY = "CAND_COMNAIS";
	public static final String CAND_COM_NAIS_CODE_COLKEY = "CAND_COMNAIS_CODE";
	public static final String CAND_CP_PARENT_COLKEY = "CAND_CP_PARENT";
	public static final String CAND_DATE_NAIS_COLKEY = "CAND_DATE_NAIS";
	public static final String CAND_DATE_NAIS_PROVISOIRE_COLKEY = "CAND_DATE_NAIS_PROVISOIRE";
	public static final String CAND_EMAIL_SCOL_COLKEY = "CAND_EMAIL_SCOL";
	public static final String CAND_ENFANTS_CHARGE_COLKEY = "CAND_ENFANTS_CHARGE";
	public static final String CAND_ETAB_CHOIX_COLKEY = "CAND_ETAB_CHOIX";
	public static final String CAND_NOM_COLKEY = "CAND_NOM";
	public static final String CAND_NOM_USUEL_COLKEY = "CAND_NOM_USUEL";
	public static final String CAND_NUMERO_COLKEY = "CAND_NUMERO";
	public static final String CAND_PAYS_PARENT_COLKEY = "CAND_PAYS_PARENT";
	public static final String CAND_PORT_SCOL_COLKEY = "CAND_PORT_SCOL";
	public static final String CAND_PRENOM_COLKEY = "CAND_PRENOM";
	public static final String CAND_PRENOM2_COLKEY = "CAND_PRENOM2";
	public static final String CAND_PRENOM3_COLKEY = "CAND_PRENOM3";
	public static final String CAND_TEL_PARENT_COLKEY = "CAND_TEL_PARENT";
	public static final String CAND_VILLE_BAC_COLKEY = "CAND_VILLE_BAC";
	public static final String CAND_VILLE_PARENT_COLKEY = "CAND_VILLE_PARENT";
	public static final String CAND_VILLE_PARENT_CODE_COLKEY = "CAND_VILLE_PARENT_CODE";
	public static final String DPTG_CODE_DER_ETAB_COLKEY = "DPTG_CODE_DER_ETAB";
	public static final String DPTG_CODE_NAIS_COLKEY = "DPTG_CODE_NAIS";
	public static final String DPTG_ETAB_BAC_COLKEY = "DPTG_ETAB_BAC";
	public static final String ETAB_CODE_BAC_COLKEY = "ETAB_CODE_BAC";
	public static final String ETAB_CODE_DER_ETAB_COLKEY = "ETAB_CODE_DER_ETAB";
	public static final String ETAB_LIBELLE_BAC_COLKEY = "ETAB_LIBELLE_BAC";
	public static final String HISTENS_DER_ETAB_COLKEY = "HIST_ENS_DER_ETAB";
	public static final String HIST_LIBELLE_DER_ETAB_COLKEY = "HIST_LIBELLE_DER_ETAB";
	public static final String HISTVILLE_DER_ETAB_COLKEY = "HIST_VILLE_DER_ETAB";
	public static final String NAT_ORDRE_COLKEY = "NAT_ORDRE";
	public static final String PAYS_CODE_DER_ETAB_COLKEY = "PAYS_CODE_DER_ETAB";
	public static final String PAYS_CODE_NAIS_COLKEY = "PAYS_CODE_NAIS";
	public static final String PAYS_ETAB_BAC_COLKEY = "PAYS_ETAB_BAC";
	public static final String PRO_CODE1_COLKEY = "PRO_CODE_1";
	public static final String PRO_CODE2_COLKEY = "PRO_CODE_2";



	// Relationships



	// Accessors methods
  public Integer annePremEtab() {
    return (Integer) storedValueForKey(ANNE_PREM_ETAB_KEY);
  }

  public void setAnnePremEtab(Integer value) {
    takeStoredValueForKey(value, ANNE_PREM_ETAB_KEY);
  }

  public String bacCode() {
    return (String) storedValueForKey(BAC_CODE_KEY);
  }

  public void setBacCode(String value) {
    takeStoredValueForKey(value, BAC_CODE_KEY);
  }

  public String candAdr1Parent() {
    return (String) storedValueForKey(CAND_ADR1_PARENT_KEY);
  }

  public void setCandAdr1Parent(String value) {
    takeStoredValueForKey(value, CAND_ADR1_PARENT_KEY);
  }

  public String candAdr2Parent() {
    return (String) storedValueForKey(CAND_ADR2_PARENT_KEY);
  }

  public void setCandAdr2Parent(String value) {
    takeStoredValueForKey(value, CAND_ADR2_PARENT_KEY);
  }

  public String candAdr3Parent() {
    return (String) storedValueForKey(CAND_ADR3_PARENT_KEY);
  }

  public void setCandAdr3Parent(String value) {
    takeStoredValueForKey(value, CAND_ADR3_PARENT_KEY);
  }

  public Integer candAnBac() {
    return (Integer) storedValueForKey(CAND_AN_BAC_KEY);
  }

  public void setCandAnBac(Integer value) {
    takeStoredValueForKey(value, CAND_AN_BAC_KEY);
  }

  public String candBea() {
    return (String) storedValueForKey(CAND_BEA_KEY);
  }

  public void setCandBea(String value) {
    takeStoredValueForKey(value, CAND_BEA_KEY);
  }

  public String candCivilite() {
    return (String) storedValueForKey(CAND_CIVILITE_KEY);
  }

  public void setCandCivilite(String value) {
    takeStoredValueForKey(value, CAND_CIVILITE_KEY);
  }

  public String candComNais() {
    return (String) storedValueForKey(CAND_COM_NAIS_KEY);
  }

  public void setCandComNais(String value) {
    takeStoredValueForKey(value, CAND_COM_NAIS_KEY);
  }

  public String candComNaisCode() {
    return (String) storedValueForKey(CAND_COM_NAIS_CODE_KEY);
  }

  public void setCandComNaisCode(String value) {
    takeStoredValueForKey(value, CAND_COM_NAIS_CODE_KEY);
  }

  public String candCpParent() {
    return (String) storedValueForKey(CAND_CP_PARENT_KEY);
  }

  public void setCandCpParent(String value) {
    takeStoredValueForKey(value, CAND_CP_PARENT_KEY);
  }

  public String candDateNais() {
    return (String) storedValueForKey(CAND_DATE_NAIS_KEY);
  }

  public void setCandDateNais(String value) {
    takeStoredValueForKey(value, CAND_DATE_NAIS_KEY);
  }

  public String candDateNaisProvisoire() {
    return (String) storedValueForKey(CAND_DATE_NAIS_PROVISOIRE_KEY);
  }

  public void setCandDateNaisProvisoire(String value) {
    takeStoredValueForKey(value, CAND_DATE_NAIS_PROVISOIRE_KEY);
  }

  public String candEmailScol() {
    return (String) storedValueForKey(CAND_EMAIL_SCOL_KEY);
  }

  public void setCandEmailScol(String value) {
    takeStoredValueForKey(value, CAND_EMAIL_SCOL_KEY);
  }

  public Integer candEnfantsCharge() {
    return (Integer) storedValueForKey(CAND_ENFANTS_CHARGE_KEY);
  }

  public void setCandEnfantsCharge(Integer value) {
    takeStoredValueForKey(value, CAND_ENFANTS_CHARGE_KEY);
  }

  public String candEtabChoix() {
    return (String) storedValueForKey(CAND_ETAB_CHOIX_KEY);
  }

  public void setCandEtabChoix(String value) {
    takeStoredValueForKey(value, CAND_ETAB_CHOIX_KEY);
  }

  public String candNom() {
    return (String) storedValueForKey(CAND_NOM_KEY);
  }

  public void setCandNom(String value) {
    takeStoredValueForKey(value, CAND_NOM_KEY);
  }

  public String candNomUsuel() {
    return (String) storedValueForKey(CAND_NOM_USUEL_KEY);
  }

  public void setCandNomUsuel(String value) {
    takeStoredValueForKey(value, CAND_NOM_USUEL_KEY);
  }

  public Integer candNumero() {
    return (Integer) storedValueForKey(CAND_NUMERO_KEY);
  }

  public void setCandNumero(Integer value) {
    takeStoredValueForKey(value, CAND_NUMERO_KEY);
  }

  public String candPaysParent() {
    return (String) storedValueForKey(CAND_PAYS_PARENT_KEY);
  }

  public void setCandPaysParent(String value) {
    takeStoredValueForKey(value, CAND_PAYS_PARENT_KEY);
  }

  public String candPortScol() {
    return (String) storedValueForKey(CAND_PORT_SCOL_KEY);
  }

  public void setCandPortScol(String value) {
    takeStoredValueForKey(value, CAND_PORT_SCOL_KEY);
  }

  public String candPrenom() {
    return (String) storedValueForKey(CAND_PRENOM_KEY);
  }

  public void setCandPrenom(String value) {
    takeStoredValueForKey(value, CAND_PRENOM_KEY);
  }

  public String candPrenom2() {
    return (String) storedValueForKey(CAND_PRENOM2_KEY);
  }

  public void setCandPrenom2(String value) {
    takeStoredValueForKey(value, CAND_PRENOM2_KEY);
  }

  public String candPrenom3() {
    return (String) storedValueForKey(CAND_PRENOM3_KEY);
  }

  public void setCandPrenom3(String value) {
    takeStoredValueForKey(value, CAND_PRENOM3_KEY);
  }

  public String candTelParent() {
    return (String) storedValueForKey(CAND_TEL_PARENT_KEY);
  }

  public void setCandTelParent(String value) {
    takeStoredValueForKey(value, CAND_TEL_PARENT_KEY);
  }

  public String candVilleBac() {
    return (String) storedValueForKey(CAND_VILLE_BAC_KEY);
  }

  public void setCandVilleBac(String value) {
    takeStoredValueForKey(value, CAND_VILLE_BAC_KEY);
  }

  public String candVilleParent() {
    return (String) storedValueForKey(CAND_VILLE_PARENT_KEY);
  }

  public void setCandVilleParent(String value) {
    takeStoredValueForKey(value, CAND_VILLE_PARENT_KEY);
  }

  public String candVilleParentCode() {
    return (String) storedValueForKey(CAND_VILLE_PARENT_CODE_KEY);
  }

  public void setCandVilleParentCode(String value) {
    takeStoredValueForKey(value, CAND_VILLE_PARENT_CODE_KEY);
  }

  public String dptgCodeDerEtab() {
    return (String) storedValueForKey(DPTG_CODE_DER_ETAB_KEY);
  }

  public void setDptgCodeDerEtab(String value) {
    takeStoredValueForKey(value, DPTG_CODE_DER_ETAB_KEY);
  }

  public String dptgCodeNais() {
    return (String) storedValueForKey(DPTG_CODE_NAIS_KEY);
  }

  public void setDptgCodeNais(String value) {
    takeStoredValueForKey(value, DPTG_CODE_NAIS_KEY);
  }

  public String dptgEtabBac() {
    return (String) storedValueForKey(DPTG_ETAB_BAC_KEY);
  }

  public void setDptgEtabBac(String value) {
    takeStoredValueForKey(value, DPTG_ETAB_BAC_KEY);
  }

  public String etabCodeBac() {
    return (String) storedValueForKey(ETAB_CODE_BAC_KEY);
  }

  public void setEtabCodeBac(String value) {
    takeStoredValueForKey(value, ETAB_CODE_BAC_KEY);
  }

  public String etabCodeDerEtab() {
    return (String) storedValueForKey(ETAB_CODE_DER_ETAB_KEY);
  }

  public void setEtabCodeDerEtab(String value) {
    takeStoredValueForKey(value, ETAB_CODE_DER_ETAB_KEY);
  }

  public String etabLibelleBac() {
    return (String) storedValueForKey(ETAB_LIBELLE_BAC_KEY);
  }

  public void setEtabLibelleBac(String value) {
    takeStoredValueForKey(value, ETAB_LIBELLE_BAC_KEY);
  }

  public String histensDerEtab() {
    return (String) storedValueForKey(HISTENS_DER_ETAB_KEY);
  }

  public void setHistensDerEtab(String value) {
    takeStoredValueForKey(value, HISTENS_DER_ETAB_KEY);
  }

  public String histLibelleDerEtab() {
    return (String) storedValueForKey(HIST_LIBELLE_DER_ETAB_KEY);
  }

  public void setHistLibelleDerEtab(String value) {
    takeStoredValueForKey(value, HIST_LIBELLE_DER_ETAB_KEY);
  }

  public String histvilleDerEtab() {
    return (String) storedValueForKey(HISTVILLE_DER_ETAB_KEY);
  }

  public void setHistvilleDerEtab(String value) {
    takeStoredValueForKey(value, HISTVILLE_DER_ETAB_KEY);
  }

  public Integer natOrdre() {
    return (Integer) storedValueForKey(NAT_ORDRE_KEY);
  }

  public void setNatOrdre(Integer value) {
    takeStoredValueForKey(value, NAT_ORDRE_KEY);
  }

  public String paysCodeDerEtab() {
    return (String) storedValueForKey(PAYS_CODE_DER_ETAB_KEY);
  }

  public void setPaysCodeDerEtab(String value) {
    takeStoredValueForKey(value, PAYS_CODE_DER_ETAB_KEY);
  }

  public String paysCodeNais() {
    return (String) storedValueForKey(PAYS_CODE_NAIS_KEY);
  }

  public void setPaysCodeNais(String value) {
    takeStoredValueForKey(value, PAYS_CODE_NAIS_KEY);
  }

  public String paysEtabBac() {
    return (String) storedValueForKey(PAYS_ETAB_BAC_KEY);
  }

  public void setPaysEtabBac(String value) {
    takeStoredValueForKey(value, PAYS_ETAB_BAC_KEY);
  }

  public String proCode1() {
    return (String) storedValueForKey(PRO_CODE1_KEY);
  }

  public void setProCode1(String value) {
    takeStoredValueForKey(value, PRO_CODE1_KEY);
  }

  public String proCode2() {
    return (String) storedValueForKey(PRO_CODE2_KEY);
  }

  public void setProCode2(String value) {
    takeStoredValueForKey(value, PRO_CODE2_KEY);
  }


/**
 * Créer une instance de EOPreCandidat avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreCandidat createEOPreCandidat(EOEditingContext editingContext, String candCivilite
, String candNom
, Integer candNumero
			) {
    EOPreCandidat eo = (EOPreCandidat) createAndInsertInstance(editingContext, _EOPreCandidat.ENTITY_NAME);    
		eo.setCandCivilite(candCivilite);
		eo.setCandNom(candNom);
		eo.setCandNumero(candNumero);
    return eo;
  }

  
	  public EOPreCandidat localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreCandidat)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreCandidat creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreCandidat creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreCandidat object = (EOPreCandidat)createAndInsertInstance(editingContext, _EOPreCandidat.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreCandidat localInstanceIn(EOEditingContext editingContext, EOPreCandidat eo) {
    EOPreCandidat localInstance = (eo == null) ? null : (EOPreCandidat)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreCandidat#localInstanceIn a la place.
   */
	public static EOPreCandidat localInstanceOf(EOEditingContext editingContext, EOPreCandidat eo) {
		return EOPreCandidat.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreCandidat fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreCandidat fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreCandidat> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreCandidat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreCandidat)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreCandidat fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreCandidat> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreCandidat eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreCandidat)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreCandidat fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreCandidat eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreCandidat ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreCandidat fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
