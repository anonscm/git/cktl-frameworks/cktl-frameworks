/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.ITypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;


// TODO: Auto-generated Javadoc
/**
 * classe métier PreAdresse.
 *
 * @author isabelle Réau
 */
public class EOPreAdresse extends _EOPreAdresse implements IPreAdresse {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 11111111L;

	/** The log. */
	@SuppressWarnings("unused")
	private static Logger log = Logger.getLogger(EOPreAdresse.class);
	
    /**
     * Instantiates a new eO pre adresse.
     */
    public EOPreAdresse() {
        super();
    }
	
	/**
	 * Creer.
	 *
	 * @param edc : contexte d'édition
	 * @param etudiant : etudiant
	 * @param persId : persId
	 * @return EODiplome
	 */
	public static EOPreAdresse creer(EOEditingContext edc, EOPreEtudiant etudiant, Integer persId) {
		EOPreAdresse adresse = new EOPreAdresse();
		edc.insertObject(adresse);
		adresse.setToPreEtudiantRelationship(etudiant);
		adresse.setPersIdCreation(persId);
		adresse.setPersIdModification(persId);
		adresse.setDCreation(new NSTimestamp());
		adresse.setDModification(new NSTimestamp());
		adresse.setToPaysRelationship(EOPays.getPaysDefaut(edc));
		
		return adresse;
	}	
	
	/**
	 * Copie les valeurs de l'adresse source dans l'adresse.
	 * @param adresseSource adresse source
	 */
	public void copier(IPreAdresse adresseSource) {
		setAdrAdresse1(adresseSource.adrAdresse1());
		setAdrAdresse2(adresseSource.adrAdresse2());
		setBoitePostale(adresseSource.boitePostale());
		setCodePostal(adresseSource.codePostal());
		setCpEtranger(adresseSource.cpEtranger());
		setVille(adresseSource.ville());
		setToPaysRelationship(adresseSource.toPays());
		setEmail(adresseSource.email());
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void copier(IAdresse adresseCible) {
		adresseCible.setAdrAdresse1(adrAdresse1());
		adresseCible.setAdrAdresse2(adrAdresse2());
		adresseCible.setAdrBp(boitePostale());
		adresseCible.setCodePostal(codePostal());
		adresseCible.setCpEtranger(cpEtranger());
		adresseCible.setVille(ville());
		adresseCible.setToPaysRelationship(toPays());
	}

	/**
	 * Copie les infos de l'adresse source issue du référentiel.
	 * @param adresseSource l'adresse source
	 */
	public void copierFromAdresseReferentiel(IAdresse adresseSource) {
		setAdrAdresse1(adresseSource.adrAdresse1());
		setAdrAdresse2(adresseSource.adrAdresse2());
		setBoitePostale(adresseSource.adrBp());
		setCodePostal(adresseSource.codePostal());
		setCpEtranger(adresseSource.cpEtranger());
		setVille(adresseSource.ville());
		setToPaysRelationship((EOPays) adresseSource.toPays());
	}

	
	/**
	 * @return <code>true</code> si c'est une adresse stable (parent)
	 */
	public boolean isAdresseStable() {
		return EOTypeAdresse.TADR_CODE_PAR.equals(toTypeAdresse().tadrCode());
	}
	
	/**
	 * @return <code>true</code> si c'est une adresse étudiant
	 */
	public boolean isAdresseEtudiant() {
		return EOTypeAdresse.TADR_CODE_ETUD.equals(toTypeAdresse().tadrCode());
	}
	
	/**
	 * Checks if is etranger.
	 *
	 * @return the boolean
	 */
	public Boolean isEtranger() {
		return Boolean.valueOf(!(toPays() == null || EOPays.getPaysDefaut(this.editingContext()).cPays().equals(toPays().cPays())));
	}

	/**
	 * Checks if is not etranger.
	 *
	 * @return the boolean
	 */
	public Boolean isNotEtranger() {
		return Boolean.valueOf(!isEtranger().booleanValue());
	}

	/**
	 * Gets the une adresse.
	 *
	 * @param edc the edc to set
	 * @param etudiant the etudiant to set
	 * @param typeAdresse the typeAdresse to set
	 * @return uneAdresse
	 */
	public static EOPreAdresse getUneAdresse(EOEditingContext edc, EOPreEtudiant etudiant, EOTypeAdresse typeAdresse) {
		EOPreAdresse uneAdresse = null;
		EOKeyValueQualifier qualEtud =  new EOKeyValueQualifier(EOPreAdresse.TO_PRE_ETUDIANT_KEY, EOQualifier.QualifierOperatorEqual, etudiant);
		EOKeyValueQualifier qualType = new EOKeyValueQualifier(EOPreAdresse.TO_TYPE_ADRESSE_KEY, EOQualifier.QualifierOperatorEqual, typeAdresse);
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {qualEtud, qualType}));
//		uneAdresse = EOPreAdresse.fetchRequiredEOPreAdresse(edc, qual);
		uneAdresse = EOPreAdresse.fetchByQualifier(edc, qual);
		
		return uneAdresse;
	}
	
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     * @throws ValidationException the validation exception
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }


	public void setToPreEtudiantRelationship(IPreEtudiant value) {
		setToPreEtudiantRelationship((EOPreEtudiant) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public String adrBp() {
		return boitePostale();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrBp(String value) {
		setBoitePostale(value);
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal adrGpsLatitude() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrGpsLatitude(BigDecimal value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public BigDecimal adrGpsLongitude() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrGpsLongitude(BigDecimal value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String adrListeRouge() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrListeRouge(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public Integer adrOrdre() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrOrdre(Integer value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String adrUrlPere() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setAdrUrlPere(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String bisTer() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setBisTer(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String cImplantation() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setCImplantation(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String cVoie() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setCVoie(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public NSTimestamp dDebVal() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDDebVal(NSTimestamp value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public NSTimestamp dFinVal() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setDFinVal(NSTimestamp value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String localite() {
		return ville();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setLocalite(String value) {
		setVille(value);
	}

	/**
	 * {@inheritDoc}
	 */
	public String nomVoie() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setNomVoie(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public String noVoie() {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setNoVoie(String value) {
		throw new UnsupportedOperationException();
	}

	public String temPayeUtil() {
		throw new UnsupportedOperationException();
	}

	public void setTemPayeUtil(String value) {
		throw new UnsupportedOperationException();
	}

	/**
	 * {@inheritDoc}
	 */
	public void copierAdresseFrom(IAdresse adresseACopier) {
		this.copier((IPreAdresse) adresseACopier);
	}

	/**
	 * {@inheritDoc}
	 */
    public void copierAdresseTo(IAdresse adresseARenseigner) {
    	this.copier(adresseARenseigner);
    }
    
	/**
	 * {@inheritDoc}
	 */
	public void setToPaysRelationship(IPays value) {
		super.setToPaysRelationship((EOPays) value);
	}

	/**
	 * {@inheritDoc}
	 */
	public String getEmail() {
		return email();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdresseParent() {
		return isAdresseStable();
	}

	/**
	 * {@inheritDoc}
	 */
	public ITypeAdresse typeAdresse() {
		return toTypeAdresse();
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdresseInvalide() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public boolean isAdressePerso() {
		return false;
	}

	/**
	 * {@inheritDoc}
	 */
	public String getEmailDeType(String codeType) {
		return email();
	}

	/**
	 * {@inheritDoc}
	 */
	public void setEmailDeType(String email, String type) {
		setEmail(email);
	}

	public String getCPCache() {
		String cpCache = null;
		if (cpCache == null) {
			cpCache = codePostal();
		}
		if (cpCache == null) {
			cpCache = cpEtranger();
		}
		return cpCache;
	}

}
