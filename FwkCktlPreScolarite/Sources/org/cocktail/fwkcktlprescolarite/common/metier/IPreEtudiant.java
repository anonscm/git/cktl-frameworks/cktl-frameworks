/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOBac;
import org.cocktail.fwkcktlpersonne.common.metier.EOMention;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EORne;
import org.cocktail.fwkcktlpersonne.common.metier.insee.CodeInsee;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInscription;
import org.cocktail.fwkcktlworkflow.serveur.metier.IDemande;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * Infos relatives à un pre Etudiant
 */
public interface IPreEtudiant extends IInfosEtudiant, IIndividu {
	
	public EOEditingContext editingContext();

	NSTimestamp dNaissance();

	CodeInsee getCodeInsee();

	/**
	 * @return civilite (Mr, Mme, Mlle)
	 */
	String civilite();

	boolean estHomme();

	String acadCodeBac();

	Integer anne1InscUniv();

	Integer annee1InscEtab();

	Integer annee1InscSup();

	Integer anneeBac();

	Integer candNumero();

	Integer snAttestation();

	Integer snCertification();

	String tHebCode();

	String villeBac();

	IProfession toProfession1();

	IProfession toProfession2();

	NSArray<EOPreAdresse> toPreAdresses();

	NSArray<EOPreAdresse> toPreAdresses(EOQualifier qualifier);

	IPreEtudiantAnnee toPreEtudiantAnnee(Integer annee);

	/**
	 * @return les téléphones de l'étudiant
	 */
	NSArray<EOPreTelephone> toPreTelephones();

	EOBac toBacs();

	EOMention toMention();

	EOPays toPaysEtabBac();

	EORne toRneEtabBac();

	IRne toRneEtabSup();

	void setToRneEtabSupRelationship(IRne rneEtabSup);
	
	void setToIndividuEtudiantRelationship(IIndividu value);

	IEtudiant toEtudiant();

	void setToEtudiantRelationship(IEtudiant etudiant);

	List<IInscription> toInscriptions();
	
	List<IInscription> toInscriptions(EOQualifier qualifier);
	
	/**
	 * @return lien vers les bourses d'études de l'étudiant
	 */
	List<? extends IPreBourses> toPreBourses();

	/**
	 * @return lien vers les bourses d'études de l'étudiant
	 */
	List<IPreBourses> toPreBourses(Integer annee);

	/**
	 * @return lien vers les cursus de l'étudiant
	 */
	List<? extends IPreCursus> toPreCursus();

	IDemande todemande();

	/**
	 * @return Demande d'avancement sur le workflow
	 */
	IDemande getToDemande();

	/**
	 * @return <code>true</code> si l'étudiant est post-bac (n'a que le bac donc pas de cursus)
	 */
	boolean isEtudiantPostBac();

	/**
	 * @param annee année concerné
	 * @return  <code>true</code> si l'étudiant va payer par virement
	 */
	boolean vaPayerParVirement(Integer annee);
	
	
	void setReimmatriculation(String value);
}
