package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;

import com.webobjects.foundation.NSTimestamp;

public interface IPreAdresse extends IAdresse {

	// Accessors methods
	public String adrAdresse1();

	public void setAdrAdresse1(String value);

	public String adrAdresse2();

	public void setAdrAdresse2(String value);

	public String boitePostale();

	public void setBoitePostale(String value);

	public String codePostal();

	public void setCodePostal(String value);

	public String cpEtranger();

	public void setCpEtranger(String value);

	public NSTimestamp dCreation();

	public void setDCreation(NSTimestamp value);

	public NSTimestamp dModification();

	public void setDModification(NSTimestamp value);

	public String email();

	public void setEmail(String value);

	public Integer persIdCreation();

	public void setPersIdCreation(Integer value);

	public Integer persIdModification();

	public void setPersIdModification(Integer value);

	public String ville();

	public void setVille(String value);

	public EOPays toPays();

	public void setToPaysRelationship(IPays value);

	public IPreEtudiant toPreEtudiant();

	public void setToPreEtudiantRelationship(IPreEtudiant value);

	public EOTypeAdresse toTypeAdresse();

	public void setToTypeAdresseRelationship(EOTypeAdresse value);
	
	public boolean isAdresseEtudiant();
	
	public boolean isAdresseStable();

	public void copier(IPreAdresse adresseSource);
	
	public void copier(IAdresse adresseCible);

	public void copierFromAdresseReferentiel(IAdresse adresseSource);

}
