/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreAdresse.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreAdresse extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreAdresse.class);

	public static final String ENTITY_NAME = "EOPreAdresse";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_ADRESSE";


// Attribute Keys
  public static final ERXKey<String> ADR_ADRESSE1 = new ERXKey<String>("adrAdresse1");
  public static final ERXKey<String> ADR_ADRESSE2 = new ERXKey<String>("adrAdresse2");
  public static final ERXKey<String> BOITE_POSTALE = new ERXKey<String>("boitePostale");
  public static final ERXKey<String> CODE_POSTAL = new ERXKey<String>("codePostal");
  public static final ERXKey<String> CP_ETRANGER = new ERXKey<String>("cpEtranger");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<String> EMAIL = new ERXKey<String>("email");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> VILLE = new ERXKey<String>("ville");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays> TO_PAYS = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOPays>("toPays");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse> TO_TYPE_ADRESSE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse>("toTypeAdresse");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreAdresse";

	public static final String ADR_ADRESSE1_KEY = "adrAdresse1";
	public static final String ADR_ADRESSE2_KEY = "adrAdresse2";
	public static final String BOITE_POSTALE_KEY = "boitePostale";
	public static final String CODE_POSTAL_KEY = "codePostal";
	public static final String CP_ETRANGER_KEY = "cpEtranger";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String EMAIL_KEY = "email";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String VILLE_KEY = "ville";

// Attributs non visibles
	public static final String C_PAYS_KEY = "cPays";
	public static final String ID_PRE_ADRESSE_KEY = "idPreAdresse";
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String TYPE_ADRESSE_KEY = "typeAdresse";

//Colonnes dans la base de donnees
	public static final String ADR_ADRESSE1_COLKEY = "ADR_ADRESSE1";
	public static final String ADR_ADRESSE2_COLKEY = "ADR_ADRESSE2";
	public static final String BOITE_POSTALE_COLKEY = "BOITE_POSTALE";
	public static final String CODE_POSTAL_COLKEY = "CODE_POSTAL";
	public static final String CP_ETRANGER_COLKEY = "CP_ETRANGER";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String EMAIL_COLKEY = "ADR_EMAIL";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String VILLE_COLKEY = "VILLE";

	public static final String C_PAYS_COLKEY = "C_PAYS";
	public static final String ID_PRE_ADRESSE_COLKEY = "ID_PRE_ADRESSE";
	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String TYPE_ADRESSE_COLKEY = "TYPE_ADRESSE";


	// Relationships
	public static final String TO_PAYS_KEY = "toPays";
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";
	public static final String TO_TYPE_ADRESSE_KEY = "toTypeAdresse";



	// Accessors methods
  public String adrAdresse1() {
    return (String) storedValueForKey(ADR_ADRESSE1_KEY);
  }

  public void setAdrAdresse1(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE1_KEY);
  }

  public String adrAdresse2() {
    return (String) storedValueForKey(ADR_ADRESSE2_KEY);
  }

  public void setAdrAdresse2(String value) {
    takeStoredValueForKey(value, ADR_ADRESSE2_KEY);
  }

  public String boitePostale() {
    return (String) storedValueForKey(BOITE_POSTALE_KEY);
  }

  public void setBoitePostale(String value) {
    takeStoredValueForKey(value, BOITE_POSTALE_KEY);
  }

  public String codePostal() {
    return (String) storedValueForKey(CODE_POSTAL_KEY);
  }

  public void setCodePostal(String value) {
    takeStoredValueForKey(value, CODE_POSTAL_KEY);
  }

  public String cpEtranger() {
    return (String) storedValueForKey(CP_ETRANGER_KEY);
  }

  public void setCpEtranger(String value) {
    takeStoredValueForKey(value, CP_ETRANGER_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public String email() {
    return (String) storedValueForKey(EMAIL_KEY);
  }

  public void setEmail(String value) {
    takeStoredValueForKey(value, EMAIL_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public String ville() {
    return (String) storedValueForKey(VILLE_KEY);
  }

  public void setVille(String value) {
    takeStoredValueForKey(value, VILLE_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOPays toPays() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOPays)storedValueForKey(TO_PAYS_KEY);
  }

  public void setToPaysRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOPays value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOPays oldValue = toPays();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PAYS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PAYS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse toTypeAdresse() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse)storedValueForKey(TO_TYPE_ADRESSE_KEY);
  }

  public void setToTypeAdresseRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse oldValue = toTypeAdresse();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_ADRESSE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_ADRESSE_KEY);
    }
  }
  

/**
 * Créer une instance de EOPreAdresse avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreAdresse createEOPreAdresse(EOEditingContext editingContext, NSTimestamp dCreation
, org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse toTypeAdresse			) {
    EOPreAdresse eo = (EOPreAdresse) createAndInsertInstance(editingContext, _EOPreAdresse.ENTITY_NAME);    
		eo.setDCreation(dCreation);
    eo.setToTypeAdresseRelationship(toTypeAdresse);
    return eo;
  }

  
	  public EOPreAdresse localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreAdresse)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreAdresse creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreAdresse creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreAdresse object = (EOPreAdresse)createAndInsertInstance(editingContext, _EOPreAdresse.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreAdresse localInstanceIn(EOEditingContext editingContext, EOPreAdresse eo) {
    EOPreAdresse localInstance = (eo == null) ? null : (EOPreAdresse)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreAdresse#localInstanceIn a la place.
   */
	public static EOPreAdresse localInstanceOf(EOEditingContext editingContext, EOPreAdresse eo) {
		return EOPreAdresse.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreAdresse>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreAdresse> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreAdresse)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreAdresse> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreAdresse eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreAdresse)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreAdresse ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreAdresse fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
