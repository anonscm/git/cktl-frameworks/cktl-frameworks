/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePaiementDetail.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPrePaiementDetail extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPrePaiementDetail.class);

	public static final String ENTITY_NAME = "EOPrePaiementDetail";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_DETAIL_PAIEMENT";


// Attribute Keys
  public static final ERXKey<Boolean> AUTO = new ERXKey<Boolean>("auto");
  public static final ERXKey<NSTimestamp> DATE_PAYE = new ERXKey<NSTimestamp>("datePaye");
  public static final ERXKey<NSTimestamp> DATE_REMBOURSE = new ERXKey<NSTimestamp>("dateRembourse");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT_A_PAYER = new ERXKey<java.math.BigDecimal>("montantAPayer");
  public static final ERXKey<java.math.BigDecimal> MONTANT_A_REMBOURSER = new ERXKey<java.math.BigDecimal>("montantARembourser");
  public static final ERXKey<java.math.BigDecimal> MONTANT_PAYE = new ERXKey<java.math.BigDecimal>("montantPaye");
  public static final ERXKey<java.math.BigDecimal> MONTANT_REMBOURSE = new ERXKey<java.math.BigDecimal>("montantRembourse");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire> TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire>("toParametragePaieArticleComplementaire");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome> TO_PARAMETRAGE_PAIE_DIPLOME = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome>("toParametragePaieDiplome");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation> TO_PARAMETRAGE_PAIE_FORMATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation>("toParametragePaieFormation");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> TO_PRE_PAIEMENT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>("toPrePaiement");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreDetailPaiement";

	public static final String AUTO_KEY = "auto";
	public static final String DATE_PAYE_KEY = "datePaye";
	public static final String DATE_REMBOURSE_KEY = "dateRembourse";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MONTANT_A_PAYER_KEY = "montantAPayer";
	public static final String MONTANT_A_REMBOURSER_KEY = "montantARembourser";
	public static final String MONTANT_PAYE_KEY = "montantPaye";
	public static final String MONTANT_REMBOURSE_KEY = "montantRembourse";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

// Attributs non visibles
	public static final String ID_PARAM_PAIE_ART_COMP_KEY = "idParamPaieArtComp";
	public static final String ID_PARAM_PAIE_DIPLOME_KEY = "idParamPaieDiplome";
	public static final String ID_PARAM_PAIE_FORMATION_KEY = "idParamPaieFormation";
	public static final String ID_PRE_DETAIL_PAIEMENT_KEY = "idPreDetailPaiement";
	public static final String ID_PRE_PAIEMENT_KEY = "idPrePaiement";

//Colonnes dans la base de donnees
	public static final String AUTO_COLKEY = "auto";
	public static final String DATE_PAYE_COLKEY = "DATE_PAYE";
	public static final String DATE_REMBOURSE_COLKEY = "DATE_REMBOURSE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String MONTANT_A_PAYER_COLKEY = "MONTANT_A_PAYER";
	public static final String MONTANT_A_REMBOURSER_COLKEY = "MONTANT_A_REMBOURSER";
	public static final String MONTANT_PAYE_COLKEY = "MONTANT_PAYE";
	public static final String MONTANT_REMBOURSE_COLKEY = "MONTANT_REMBOURSE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_PARAM_PAIE_ART_COMP_COLKEY = "ID_PARAM_PAIE_ART_COMP";
	public static final String ID_PARAM_PAIE_DIPLOME_COLKEY = "ID_PARAM_PAIE_DIPLOME";
	public static final String ID_PARAM_PAIE_FORMATION_COLKEY = "ID_PARAM_PAIE_FORMATION";
	public static final String ID_PRE_DETAIL_PAIEMENT_COLKEY = "ID_PRE_DETAIL_PAIEMENT";
	public static final String ID_PRE_PAIEMENT_COLKEY = "ID_PRE_PAIEMENT";


	// Relationships
	public static final String TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY = "toParametragePaieArticleComplementaire";
	public static final String TO_PARAMETRAGE_PAIE_DIPLOME_KEY = "toParametragePaieDiplome";
	public static final String TO_PARAMETRAGE_PAIE_FORMATION_KEY = "toParametragePaieFormation";
	public static final String TO_PRE_PAIEMENT_KEY = "toPrePaiement";



	// Accessors methods
  public Boolean auto() {
    return (Boolean) storedValueForKey(AUTO_KEY);
  }

  public void setAuto(Boolean value) {
    takeStoredValueForKey(value, AUTO_KEY);
  }

  public NSTimestamp datePaye() {
    return (NSTimestamp) storedValueForKey(DATE_PAYE_KEY);
  }

  public void setDatePaye(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PAYE_KEY);
  }

  public NSTimestamp dateRembourse() {
    return (NSTimestamp) storedValueForKey(DATE_REMBOURSE_KEY);
  }

  public void setDateRembourse(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_REMBOURSE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montantAPayer() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_A_PAYER_KEY);
  }

  public void setMontantAPayer(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_A_PAYER_KEY);
  }

  public java.math.BigDecimal montantARembourser() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_A_REMBOURSER_KEY);
  }

  public void setMontantARembourser(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_A_REMBOURSER_KEY);
  }

  public java.math.BigDecimal montantPaye() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_PAYE_KEY);
  }

  public void setMontantPaye(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_PAYE_KEY);
  }

  public java.math.BigDecimal montantRembourse() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_REMBOURSE_KEY);
  }

  public void setMontantRembourse(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_REMBOURSE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire toParametragePaieArticleComplementaire() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire)storedValueForKey(TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
  }

  public void setToParametragePaieArticleComplementaireRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieArticleComplementaire oldValue = toParametragePaieArticleComplementaire();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARAMETRAGE_PAIE_ARTICLE_COMPLEMENTAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome toParametragePaieDiplome() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome)storedValueForKey(TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
  }

  public void setToParametragePaieDiplomeRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieDiplome oldValue = toParametragePaieDiplome();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARAMETRAGE_PAIE_DIPLOME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation toParametragePaieFormation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation)storedValueForKey(TO_PARAMETRAGE_PAIE_FORMATION_KEY);
  }

  public void setToParametragePaieFormationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOParametragePaieFormation oldValue = toParametragePaieFormation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PARAMETRAGE_PAIE_FORMATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PARAMETRAGE_PAIE_FORMATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement toPrePaiement() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement)storedValueForKey(TO_PRE_PAIEMENT_KEY);
  }

  public void setToPrePaiementRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement oldValue = toPrePaiement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_PAIEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_PAIEMENT_KEY);
    }
  }
  

/**
 * Créer une instance de EOPrePaiementDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrePaiementDetail createEOPrePaiementDetail(EOEditingContext editingContext, NSTimestamp dCreation
, Integer persIdCreation
, org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement toPrePaiement			) {
    EOPrePaiementDetail eo = (EOPrePaiementDetail) createAndInsertInstance(editingContext, _EOPrePaiementDetail.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setPersIdCreation(persIdCreation);
    eo.setToPrePaiementRelationship(toPrePaiement);
    return eo;
  }

  
	  public EOPrePaiementDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrePaiementDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiementDetail creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPrePaiementDetail object = (EOPrePaiementDetail)createAndInsertInstance(editingContext, _EOPrePaiementDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrePaiementDetail localInstanceIn(EOEditingContext editingContext, EOPrePaiementDetail eo) {
    EOPrePaiementDetail localInstance = (eo == null) ? null : (EOPrePaiementDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrePaiementDetail#localInstanceIn a la place.
   */
	public static EOPrePaiementDetail localInstanceOf(EOEditingContext editingContext, EOPrePaiementDetail eo) {
		return EOPrePaiementDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrePaiementDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePaiementDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrePaiementDetail> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrePaiementDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrePaiementDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrePaiementDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrePaiementDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrePaiementDetail> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrePaiementDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrePaiementDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrePaiementDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrePaiementDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrePaiementDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrePaiementDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
