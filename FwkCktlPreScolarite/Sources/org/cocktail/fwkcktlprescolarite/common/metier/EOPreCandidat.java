/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier;
import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;


public class EOPreCandidat extends _EOPreCandidat implements IPreCandidat {

    public static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("ddMMyyyy");
    private static final SimpleDateFormat SIMPLE_DATE_FORMAT_2 = new SimpleDateFormat("dd/MM/yyyy");
    
    private static final Logger LOG = Logger.getLogger(EOPreCandidat.class);
    
    public EOPreCandidat() {
        super();
    }
    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

    /**
     * @return la date de naissance formatée
     */
    public String dateNaissanceFormatee() {
        return SIMPLE_DATE_FORMAT_2.format(dateNaissance());
    }
    
    /**
     * @return la date de naissance sous forme d'objet date plutot que de string
     */
    public Date dateNaissance() {
        Date date = null;
        try {
            date = SIMPLE_DATE_FORMAT.parse(candDateNais());
        } catch (ParseException e) {
            LOG.warn(e.getMessage(), e);
        }
        return date;
    }
    
}
