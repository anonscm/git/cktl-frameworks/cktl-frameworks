package org.cocktail.fwkcktlprescolarite.common.metier;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;

/**
 * Un élément du cursus de l'étudiant avant son inscription
 */
public interface IPreCursus extends ICursus {

	/**
	 * @return pre-etudiant concerné
	 */
	IPreEtudiant toPreEtudiant();

	/**
	 * @param value pre-etudiant concerné
	 */
	void setToPreEtudiantRelationship(IPreEtudiant value);

	/**
	 * @return supprime le pre-cusus si aucune des valeurs de ce dernier n'est remplie
	 */
	boolean supprimerSiSansValeurs();
	
	/**
	 * Calculer le type d'inscription pour le cursus.
	 * 
	 * @param codeRneEtablissement Le code RNE de l'établissement
	 */
	void calculerTypeInscription(String codeRneEtablissement);
}
