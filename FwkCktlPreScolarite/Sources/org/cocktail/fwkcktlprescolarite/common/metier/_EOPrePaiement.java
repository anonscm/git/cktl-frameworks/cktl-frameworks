/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrePaiement.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPrePaiement extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPrePaiement.class);

	public static final String ENTITY_NAME = "EOPrePaiement";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_PAIEMENT";


// Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_PAIEMENT = new ERXKey<NSTimestamp>("datePaiement");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<Integer> ORDRE = new ERXKey<Integer>("ordre");
  public static final ERXKey<Boolean> PAIEMENT_VALIDE = new ERXKey<Boolean>("paiementValide");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> TO_PRE_ETUDIANT_ANNEE = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>("toPreEtudiantAnnee");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> TO_PRE_PAIEMENT_DETAILS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail>("toPrePaiementDetails");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> TO_PRE_PAIEMENT_MOYENS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>("toPrePaiementMoyens");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPrePaiement";

	public static final String DATE_PAIEMENT_KEY = "datePaiement";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String MONTANT_KEY = "montant";
	public static final String ORDRE_KEY = "ordre";
	public static final String PAIEMENT_VALIDE_KEY = "paiementValide";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";

// Attributs non visibles
	public static final String ID_PRE_ETUDIANT_ANNEE_KEY = "idPreEtudiantAnnee";
	public static final String ID_PRE_PAIEMENT_KEY = "idPrePaiement";

//Colonnes dans la base de donnees
	public static final String DATE_PAIEMENT_COLKEY = "DATE_PAIEMENT";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String MONTANT_COLKEY = "MONTANT";
	public static final String ORDRE_COLKEY = "ORDRE";
	public static final String PAIEMENT_VALIDE_COLKEY = "PAIEMENT_VALIDE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";

	public static final String ID_PRE_ETUDIANT_ANNEE_COLKEY = "ID_PRE_ETUDIANT_ANNEE";
	public static final String ID_PRE_PAIEMENT_COLKEY = "ID_PRE_PAIEMENT";


	// Relationships
	public static final String TO_PRE_ETUDIANT_ANNEE_KEY = "toPreEtudiantAnnee";
	public static final String TO_PRE_PAIEMENT_DETAILS_KEY = "toPrePaiementDetails";
	public static final String TO_PRE_PAIEMENT_MOYENS_KEY = "toPrePaiementMoyens";



	// Accessors methods
  public NSTimestamp datePaiement() {
    return (NSTimestamp) storedValueForKey(DATE_PAIEMENT_KEY);
  }

  public void setDatePaiement(NSTimestamp value) {
    takeStoredValueForKey(value, DATE_PAIEMENT_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_KEY);
  }

  public Integer ordre() {
    return (Integer) storedValueForKey(ORDRE_KEY);
  }

  public void setOrdre(Integer value) {
    takeStoredValueForKey(value, ORDRE_KEY);
  }

  public Boolean paiementValide() {
    return (Boolean) storedValueForKey(PAIEMENT_VALIDE_KEY);
  }

  public void setPaiementValide(Boolean value) {
    takeStoredValueForKey(value, PAIEMENT_VALIDE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee toPreEtudiantAnnee() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee)storedValueForKey(TO_PRE_ETUDIANT_ANNEE_KEY);
  }

  public void setToPreEtudiantAnneeRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee oldValue = toPreEtudiantAnnee();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_ANNEE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_ANNEE_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> toPrePaiementDetails() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail>)storedValueForKey(TO_PRE_PAIEMENT_DETAILS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> toPrePaiementDetails(EOQualifier qualifier) {
    return toPrePaiementDetails(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> toPrePaiementDetails(EOQualifier qualifier, boolean fetch) {
    return toPrePaiementDetails(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> toPrePaiementDetails(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail.TO_PRE_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrePaiementDetails();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrePaiementDetailsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_DETAILS_KEY);
  }

  public void removeFromToPrePaiementDetailsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_DETAILS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail createToPrePaiementDetailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPrePaiementDetail");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_PAIEMENT_DETAILS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail) eo;
  }

  public void deleteToPrePaiementDetailsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_DETAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrePaiementDetailsRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail> objects = toPrePaiementDetails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrePaiementDetailsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> toPrePaiementMoyens() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>)storedValueForKey(TO_PRE_PAIEMENT_MOYENS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> toPrePaiementMoyens(EOQualifier qualifier) {
    return toPrePaiementMoyens(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> toPrePaiementMoyens(EOQualifier qualifier, boolean fetch) {
    return toPrePaiementMoyens(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> toPrePaiementMoyens(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen.TO_PRE_PAIEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrePaiementMoyens();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrePaiementMoyensRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_MOYENS_KEY);
  }

  public void removeFromToPrePaiementMoyensRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_MOYENS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen createToPrePaiementMoyensRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPrePaiementMoyen");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_PAIEMENT_MOYENS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen) eo;
  }

  public void deleteToPrePaiementMoyensRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENT_MOYENS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrePaiementMoyensRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen> objects = toPrePaiementMoyens().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrePaiementMoyensRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPrePaiement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrePaiement createEOPrePaiement(EOEditingContext editingContext, NSTimestamp dCreation
, java.math.BigDecimal montant
, Integer ordre
, Boolean paiementValide
, Integer persIdCreation
, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee toPreEtudiantAnnee			) {
    EOPrePaiement eo = (EOPrePaiement) createAndInsertInstance(editingContext, _EOPrePaiement.ENTITY_NAME);    
		eo.setDCreation(dCreation);
		eo.setMontant(montant);
		eo.setOrdre(ordre);
		eo.setPaiementValide(paiementValide);
		eo.setPersIdCreation(persIdCreation);
    eo.setToPreEtudiantAnneeRelationship(toPreEtudiantAnnee);
    return eo;
  }

  
	  public EOPrePaiement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrePaiement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiement creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrePaiement creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPrePaiement object = (EOPrePaiement)createAndInsertInstance(editingContext, _EOPrePaiement.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPrePaiement localInstanceIn(EOEditingContext editingContext, EOPrePaiement eo) {
    EOPrePaiement localInstance = (eo == null) ? null : (EOPrePaiement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrePaiement#localInstanceIn a la place.
   */
	public static EOPrePaiement localInstanceOf(EOEditingContext editingContext, EOPrePaiement eo) {
		return EOPrePaiement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrePaiement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrePaiement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPrePaiement> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrePaiement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrePaiement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPrePaiement> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrePaiement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrePaiement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrePaiement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrePaiement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrePaiement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrePaiement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
