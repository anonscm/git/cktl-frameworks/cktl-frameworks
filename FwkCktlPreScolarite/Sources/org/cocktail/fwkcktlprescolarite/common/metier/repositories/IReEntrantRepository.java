package org.cocktail.fwkcktlprescolarite.common.metier.repositories;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * 
 * @author isabelle
 *
 */
public interface IReEntrantRepository {
	 /**
     * @return la liste des étudiants qui n'ont pas encore de pré étudiants
     */
    public List<EOEtudiant> etudiantsNonConnectes(EOQualifier qualifier);

    /**
     * @param etudiant l'etudiant
     * @return le pre etudiant correspondant à l'etudiant null si aucun n'est trouvé
     */
    public NSArray<EOPreEtudiant> fetchPreEtudiantFromEtudiant(NSArray<EOEtudiant> etudiants);

}
