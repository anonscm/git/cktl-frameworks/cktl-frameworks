package org.cocktail.fwkcktlprescolarite.common.metier.repositories;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * 
 *  L'implémentation EOF de {@link IReEntrantRepository}
 * @author isabelle
 *
 */
public class EOREEntrantRepository implements IReEntrantRepository {

	private EOEditingContext editingContext;

	/**
	 * 
	 * @param editingContext the editingContext
	 */
	public EOREEntrantRepository(EOEditingContext editingContext) {
		this.editingContext = editingContext;
	}

	/**
	 * @param qualifier the qualifier
	 * @return liste etudiants non connectés
	 */
	public List<EOEtudiant> etudiantsNonConnectes(EOQualifier qualifier) {

		NSArray<EOEtudiant> listeEtudiants = EOEtudiant.fetchAll(editingContext, qualifier);
		List<EOEtudiant> listeEtudiantsResultat = new ArrayList<EOEtudiant>();
		NSArray<EOPreEtudiant> listePreEtudiants = fetchPreEtudiantFromEtudiant(listeEtudiants);
		for(EOEtudiant etudiant: listeEtudiants) {
			if (!etudiantAvecPreEtudiant(etudiant, listePreEtudiants)) {
				listeEtudiantsResultat.add(etudiant);
			}
		}
		return listeEtudiantsResultat;

	}

	/**
	 * @param etudiants the liste des étudiants
	 * @return liste des preetudiants correspondants aux etudiants
	 */
	public NSArray<EOPreEtudiant> fetchPreEtudiantFromEtudiant(NSArray<EOEtudiant> etudiants) {
		NSArray<Integer> listeEtudNumero =  (NSArray<Integer>) etudiants.valueForKey(EOEtudiant.ETUD_NUMERO_KEY);

		NSArray<EOPreEtudiant> preEtudiants = EOPreEtudiant.fetchAll(editingContext, EOPreEtudiant.ETUD_NUMERO.in(listeEtudNumero));
		return preEtudiants;

	}
	  
	private boolean etudiantAvecPreEtudiant(IEtudiant etudiant, NSArray<EOPreEtudiant> preEtudiants) {
	        boolean resultat = false;
	        for (EOPreEtudiant preEtudiant : preEtudiants) {
	            if (preEtudiant.toEtudiant().equals(etudiant)) {
	                resultat = true;
	                break;
	            }
	        }
	        return resultat;
	    }
}
