package org.cocktail.fwkcktlprescolarite.common.metier.repositories;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCandidat;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCandidat;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.qualifiers.ERXKeyValueQualifier;

/**
 * 
 * L'implémentation EOF de {@link IPreCandidatRepository}
 * 
 * @author Alexis Tual
 *
 */
public class EOPreCandidatRepository implements IPreCandidatRepository {

    private EOEditingContext editingContext;
    
    /**
     * @param editingContext l'editing context
     */
    public EOPreCandidatRepository(EOEditingContext editingContext) {
        this.editingContext = editingContext;
    }
    
    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    public List<IPreCandidat> candidatsNonConnectes() {
        EOFetchSpecification preEtudiantFetchSpec = EOPreEtudiant.fetchSpecification(null, null, true);
        preEtudiantFetchSpec.setFetchesRawRows(true);
        preEtudiantFetchSpec.setRawRowKeyPaths(new NSArray<String>(
                EOPreEtudiant.CODE_INE_KEY, 
                EOPreEtudiant.NOM_PATRONYMIQUE_KEY, 
                EOPreEtudiant.PRENOM_KEY,
                EOPreEtudiant.D_NAISSANCE_KEY));
        
        NSArray<NSDictionary<String, String>> rawRows = editingContext.objectsWithFetchSpecification(preEtudiantFetchSpec);
        
        NSArray<PreEtudiantLight> preEtudiants = new NSMutableArray<EOPreCandidatRepository.PreEtudiantLight>();
        
        for (NSDictionary<String, String> row : rawRows) {
            preEtudiants.add(new PreEtudiantLight(
                    row.objectForKey(EOPreEtudiant.NOM_PATRONYMIQUE_KEY),
                    row.objectForKey(EOPreEtudiant.PRENOM_KEY), 
                    row.objectForKey(EOPreEtudiant.CODE_INE_KEY),
                    row.objectForKey(EOPreEtudiant.D_NAISSANCE_KEY)));
        }
        
        return candidatsNonConnectes(preEtudiants);
        
    }
    
    /**
     * @param preEtudiants les pré étudiants
     * @return la liste des pre candidats qui ne sont pas déjà présent dans la liste des pré éutidnats passés.
     * Cette vérification se fait par rapport au nom, prénom et bea/ine et est censée être optimale.
     */
    List<IPreCandidat> candidatsNonConnectes(NSArray<PreEtudiantLight> preEtudiants) {
        NSArray<EOPreCandidat> candidats =
                EOPreCandidat.fetchAll(editingContext, EOPreCandidat.CAND_NOM.asc().then(EOPreCandidat.CAND_PRENOM.asc()));
        
        List<IPreCandidat> candidatsFiltres = new ArrayList<IPreCandidat>();
        for (EOPreCandidat candidat : candidats) {
            if (!candidatAvecPreEtudiant(candidat, preEtudiants)) {
                candidatsFiltres.add(candidat);
            }
        }
        return candidatsFiltres;
    }

    private boolean candidatAvecPreEtudiant(EOPreCandidat candidat, NSArray<PreEtudiantLight> preEtudiants) {
        boolean resultat = false;
        for (PreEtudiantLight preEtudiant : preEtudiants) {
            if (preEtudiant.correspond(candidat.candBea(), candidat.candNom(), candidat.candPrenom(), candidat.candDateNais())) {
                resultat = true;
                break;
            }
        }
        return resultat;
    }

    /**
     * Une classe prenant les infos minimales 
     *
     */
    public static class PreEtudiantLight {
        
        private Object nom, prenom, bea;
        private String dateNaissance;

        /**
         * @param nom le nom
         * @param prenom le prenom
         * @param bea le bea
         * @param dateNaissance sans surprise, la date de naissance
         */
        public PreEtudiantLight(Object nom, Object prenom, Object bea, Object dateNaissance) {
            this.nom = nom;
            this.prenom = prenom;
            this.bea = bea;
            this.dateNaissance = formatDateNaissance(dateNaissance);
        }
        
        private String formatDateNaissance(Object dateNaissance) {
            String result = null;
            if (dateNaissance instanceof Date) {
                result = EOPreCandidat.SIMPLE_DATE_FORMAT.format(dateNaissance);
            }
            return result;
        }

        public Object getNom() {
            return nom;
        }
        
        public Object getPrenom() {
            return prenom;
        }
        
        public Object getBea() {
            return bea;
        }
        
        public String getDateNaissance() {
            return dateNaissance;
        }
        
        /**
         * @param bea le bea
         * @param nom le nom
         * @param prenom le prénom
         * @param dateNaissance la date de naissance
         * @return true si le PreCandidat correspondant aux infos passées en paramètre, 
         * l'identification se fait d'abord selon le bea puis sur nom, prénom et date de naissance
         */
        public boolean correspond(String bea, String nom, String prenom, String dateNaissance) {
            boolean resultat = false;
            if (bea != null) {
                resultat = bea.equals(getBea());
            } else {
                if (nom != null && prenom != null && dateNaissance != null) {
                    resultat = nom.equals(getNom()) && prenom.equals(getPrenom()) && dateNaissance.equals(getDateNaissance());
                }
            }
            return resultat;
        }
        
    }

    /**
     * {@inheritDoc}
     */
    public IPreEtudiant fetchPreEtudiantFromCandidat(IPreCandidat preCandidat) {
        ERXKeyValueQualifier qualifierDate = null;
        Date dateNaissance = preCandidat.dateNaissance();
        if (dateNaissance != null) {
            qualifierDate = EOPreEtudiant.D_NAISSANCE.eq(new NSTimestamp(dateNaissance));
        }
        EOQualifier qualifierEtud = 
                ERXQ.and(
                        EOPreEtudiant.CODE_INE.eq(preCandidat.candBea()),
                        EOPreEtudiant.NOM_PATRONYMIQUE.eq(preCandidat.candNom()),
                        EOPreEtudiant.PRENOM.eq(preCandidat.candPrenom()),
                        qualifierDate
                        );
        EOPreEtudiant preEtudiant = EOPreEtudiant.fetchByQualifier(editingContext, qualifierEtud);
        return preEtudiant;
    }
    
}
