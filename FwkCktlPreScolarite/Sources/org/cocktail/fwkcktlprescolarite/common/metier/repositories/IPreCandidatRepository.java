package org.cocktail.fwkcktlprescolarite.common.metier.repositories;

import java.util.List;

import org.cocktail.fwkcktlprescolarite.common.metier.IPreCandidat;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;

/**
 * Repository gérant les Pre candidat.
 * 
 * @author Alexis Tual
 *
 */
public interface IPreCandidatRepository {

    /**
     * @return la liste des pré candidats qui n'ont pas encore de pré étudiants
     */
    public List<IPreCandidat> candidatsNonConnectes();

    /**
     * @param preCandidat le precandidat
     * @return le pre etudiant correspondant au candidat null si aucun n'est trouvé
     */
    public IPreEtudiant fetchPreEtudiantFromCandidat(IPreCandidat preCandidat);
    
}
