/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPreEtudiantAnnee.java instead.
package org.cocktail.fwkcktlprescolarite.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;

import er.extensions.eof.ERXKey;

import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlprescolarite.common.eospecificites.ISpecificite;


public abstract class _EOPreEtudiantAnnee extends  AFwkCktlPreScolariteRecord {
//	private static Logger logger = Logger.getLogger(_EOPreEtudiantAnnee.class);

	public static final String ENTITY_NAME = "EOPreEtudiantAnnee";
	public static final String ENTITY_TABLE_NAME = "SCO_PRE_SCOLARITE.PRE_ETUDIANT_ANNEE";


// Attribute Keys
  public static final ERXKey<Boolean> ACCORD_ANNUAIRE = new ERXKey<Boolean>("accordAnnuaire");
  public static final ERXKey<Boolean> ACCORD_PHOTO = new ERXKey<Boolean>("accordPhoto");
  public static final ERXKey<Boolean> ADH_MUTUELLE = new ERXKey<Boolean>("adhMutuelle");
  public static final ERXKey<Boolean> AFF_SS = new ERXKey<Boolean>("affSs");
  public static final ERXKey<Integer> ANNEE = new ERXKey<Integer>("annee");
  public static final ERXKey<Boolean> ASSURANCE_CIVILE = new ERXKey<Boolean>("assuranceCivile");
  public static final ERXKey<String> ASSURANCE_CIVILE_CIE = new ERXKey<String>("assuranceCivileCie");
  public static final ERXKey<String> ASSURANCE_CIVILE_NUM = new ERXKey<String>("assuranceCivileNum");
  public static final ERXKey<String> AUTRE_CAS_EXO = new ERXKey<String>("autreCasExo");
  public static final ERXKey<Integer> CERTIFICAT_PART_APPEL = new ERXKey<Integer>("certificatPartAppel");
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<NSTimestamp> D_CREATION = new ERXKey<NSTimestamp>("dCreation");
  public static final ERXKey<Boolean> DEMANDEUR_EMPLOI = new ERXKey<Boolean>("demandeurEmploi");
  public static final ERXKey<NSTimestamp> D_MODIFICATION = new ERXKey<NSTimestamp>("dModification");
  public static final ERXKey<Integer> DUREE_INTERRUPT = new ERXKey<Integer>("dureeInterrupt");
  public static final ERXKey<String> EMAIL_PARENT = new ERXKey<String>("emailParent");
  public static final ERXKey<Boolean> ENFANTS_CHARGE = new ERXKey<Boolean>("enfantsCharge");
  public static final ERXKey<String> ETAB_BAC_ETRANGER = new ERXKey<String>("etabBacEtranger");
  public static final ERXKey<Boolean> ETUD_HANDICAP = new ERXKey<Boolean>("etudHandicap");
  public static final ERXKey<String> ETUD_NO_ORIGINE = new ERXKey<String>("etudNoOrigine");
  public static final ERXKey<Boolean> ETUD_SPORT_HN = new ERXKey<Boolean>("etudSportHN");
  public static final ERXKey<Boolean> FORMATION_FINANCEE = new ERXKey<Boolean>("formationFinancee");
  public static final ERXKey<Boolean> INTERRUPT_ETUDE = new ERXKey<Boolean>("interruptEtude");
  public static final ERXKey<Integer> NB_ENFANTS_CHARGE = new ERXKey<Integer>("nbEnfantsCharge");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<Integer> RECENSEMENT = new ERXKey<Integer>("recensement");
  public static final ERXKey<Boolean> RESPECT_CHARTE = new ERXKey<Boolean>("respectCharte");
  public static final ERXKey<String> SAL_LIBELLE = new ERXKey<String>("salLibelle");
  public static final ERXKey<String> SPORT_PRATIQUE = new ERXKey<String>("sportPratique");
  public static final ERXKey<Boolean> SPORT_UNIV = new ERXKey<Boolean>("sportUniv");
  public static final ERXKey<String> STATUT_BOURSIER_CNOUS = new ERXKey<String>("statutBoursierCnous");
  public static final ERXKey<String> TUTEUR1 = new ERXKey<String>("tuteur1");
  public static final ERXKey<String> TUTEUR2 = new ERXKey<String>("tuteur2");
  public static final ERXKey<String> TYPE_REGIME = new ERXKey<String>("typeRegime");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle> TO_ACTIVITE_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle>("toActiviteProfessionnelle");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation> TO_COTISATION = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation>("toCotisation");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> TO_INSCRIPTIONS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>("toInscriptions");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle> TO_MUTUELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle>("toMutuelle");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif> TO_NIVEAU_SPORTIF = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif>("toNiveauSportif");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources> TO_ORIGINE_RESSOURCES = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources>("toOrigineRessources");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant> TO_PRE_ETUDIANT = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant>("toPreEtudiant");
  public static final ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> TO_PRE_PAIEMENTS = new ERXKey<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>("toPrePaiements");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION1 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession1");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession> TO_PROFESSION2 = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOProfession>("toProfession2");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail> TO_QUOTITE_TRAVAIL = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail>("toQuotiteTravail");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier> TO_REGIME_PARTICULIER = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier>("toRegimeParticulier");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne> TO_RNE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EORne>("toRne");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale> TO_SITUATION_FAMILIALE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale>("toSituationFamiliale");
  public static final ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle> TO_SITUATION_PROFESSIONNELLE = new ERXKey<org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle>("toSituationProfessionnelle");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap> TO_TYPE_HANDICAP = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap>("toTypeHandicap");

	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "idPreEtudiantAnnee";

	public static final String ACCORD_ANNUAIRE_KEY = "accordAnnuaire";
	public static final String ACCORD_PHOTO_KEY = "accordPhoto";
	public static final String ADH_MUTUELLE_KEY = "adhMutuelle";
	public static final String AFF_SS_KEY = "affSs";
	public static final String ANNEE_KEY = "annee";
	public static final String ASSURANCE_CIVILE_KEY = "assuranceCivile";
	public static final String ASSURANCE_CIVILE_CIE_KEY = "assuranceCivileCie";
	public static final String ASSURANCE_CIVILE_NUM_KEY = "assuranceCivileNum";
	public static final String AUTRE_CAS_EXO_KEY = "autreCasExo";
	public static final String CERTIFICAT_PART_APPEL_KEY = "certificatPartAppel";
	public static final String COMMENTAIRE_KEY = "commentaire";
	public static final String D_CREATION_KEY = "dCreation";
	public static final String DEMANDEUR_EMPLOI_KEY = "demandeurEmploi";
	public static final String D_MODIFICATION_KEY = "dModification";
	public static final String DUREE_INTERRUPT_KEY = "dureeInterrupt";
	public static final String EMAIL_PARENT_KEY = "emailParent";
	public static final String ENFANTS_CHARGE_KEY = "enfantsCharge";
	public static final String ETAB_BAC_ETRANGER_KEY = "etabBacEtranger";
	public static final String ETUD_HANDICAP_KEY = "etudHandicap";
	public static final String ETUD_NO_ORIGINE_KEY = "etudNoOrigine";
	public static final String ETUD_SPORT_HN_KEY = "etudSportHN";
	public static final String FORMATION_FINANCEE_KEY = "formationFinancee";
	public static final String INTERRUPT_ETUDE_KEY = "interruptEtude";
	public static final String NB_ENFANTS_CHARGE_KEY = "nbEnfantsCharge";
	public static final String PERS_ID_CREATION_KEY = "persIdCreation";
	public static final String PERS_ID_MODIFICATION_KEY = "persIdModification";
	public static final String RECENSEMENT_KEY = "recensement";
	public static final String RESPECT_CHARTE_KEY = "respectCharte";
	public static final String SAL_LIBELLE_KEY = "salLibelle";
	public static final String SPORT_PRATIQUE_KEY = "sportPratique";
	public static final String SPORT_UNIV_KEY = "sportUniv";
	public static final String STATUT_BOURSIER_CNOUS_KEY = "statutBoursierCnous";
	public static final String TUTEUR1_KEY = "tuteur1";
	public static final String TUTEUR2_KEY = "tuteur2";
	public static final String TYPE_REGIME_KEY = "typeRegime";

// Attributs non visibles
	public static final String ID_ACTIVITE_KEY = "idActivite";
	public static final String ID_COTISATION_KEY = "idCotisation";
	public static final String ID_MUTUELLE_KEY = "idMutuelle";
	public static final String ID_NIVEAU_SPORTIF_KEY = "idNiveauSportif";
	public static final String ID_ORIGINE_RESSOURCES_KEY = "idOrigineRessources";
	public static final String ID_PRE_ETUDIANT_KEY = "idPreEtudiant";
	public static final String ID_PRE_ETUDIANT_ANNEE_KEY = "idPreEtudiantAnnee";
	public static final String ID_QUOTITE_KEY = "idQuotite";
	public static final String ID_REGIME_PARTICULIER_KEY = "idRegimeParticulier";
	public static final String ID_SITUATION_FAMILIALE_KEY = "idSituationFamiliale";
	public static final String ID_SITUATION_PROF_KEY = "idSituationProf";
	public static final String PRO_CODE_KEY = "proCode";
	public static final String PRO_CODE2_KEY = "proCode2";
	public static final String SAL_CRNE_KEY = "salCrne";
	public static final String THAN_CODE_KEY = "thanCode";

//Colonnes dans la base de donnees
	public static final String ACCORD_ANNUAIRE_COLKEY = "ACCORD_ANNUAIRE";
	public static final String ACCORD_PHOTO_COLKEY = "ACCORD_PHOTO";
	public static final String ADH_MUTUELLE_COLKEY = "ADH_MUTUELLE";
	public static final String AFF_SS_COLKEY = "AFF_SS";
	public static final String ANNEE_COLKEY = "ANNEE";
	public static final String ASSURANCE_CIVILE_COLKEY = "ASSURANCE_CIVILE";
	public static final String ASSURANCE_CIVILE_CIE_COLKEY = "ASSURANCE_CIVILE_CIE";
	public static final String ASSURANCE_CIVILE_NUM_COLKEY = "ASSURANCE_CIVILE_NUM";
	public static final String AUTRE_CAS_EXO_COLKEY = "AUTRE_CAS_EXO";
	public static final String CERTIFICAT_PART_APPEL_COLKEY = "CERTIFICAT_PART_APPEL";
	public static final String COMMENTAIRE_COLKEY = "COMMENTAIRE";
	public static final String D_CREATION_COLKEY = "D_CREATION";
	public static final String DEMANDEUR_EMPLOI_COLKEY = "DEMANDEUR_EMPLOI";
	public static final String D_MODIFICATION_COLKEY = "D_MODIFICATION";
	public static final String DUREE_INTERRUPT_COLKEY = "DUREE_INTERRUPT";
	public static final String EMAIL_PARENT_COLKEY = "EMAIL_PARENT";
	public static final String ENFANTS_CHARGE_COLKEY = "ENFANTS_CHARGE";
	public static final String ETAB_BAC_ETRANGER_COLKEY = "ETAB_BAC_ETRANGER";
	public static final String ETUD_HANDICAP_COLKEY = "ETUD_HANDICAP";
	public static final String ETUD_NO_ORIGINE_COLKEY = "ETUD_NO_ORIGINE";
	public static final String ETUD_SPORT_HN_COLKEY = "ETUD_SPORT_HN";
	public static final String FORMATION_FINANCEE_COLKEY = "FORMATION_FINANCEE";
	public static final String INTERRUPT_ETUDE_COLKEY = "INTERRUPT_ETUDE";
	public static final String NB_ENFANTS_CHARGE_COLKEY = "NB_ENFANTS_CHARGE";
	public static final String PERS_ID_CREATION_COLKEY = "PERS_ID_CREATION";
	public static final String PERS_ID_MODIFICATION_COLKEY = "PERS_ID_MODIFICATION";
	public static final String RECENSEMENT_COLKEY = "RECENSEMENT";
	public static final String RESPECT_CHARTE_COLKEY = "RESPECT_CHARTE";
	public static final String SAL_LIBELLE_COLKEY = "SAL_LIBELLE";
	public static final String SPORT_PRATIQUE_COLKEY = "SPORT_PRATIQUE";
	public static final String SPORT_UNIV_COLKEY = "SPORT_UNIV";
	public static final String STATUT_BOURSIER_CNOUS_COLKEY = "STATUS_BOURSIER_CNOUS";
	public static final String TUTEUR1_COLKEY = "TUTEUR1";
	public static final String TUTEUR2_COLKEY = "TUTEUR2";
	public static final String TYPE_REGIME_COLKEY = "TYPE_REGIME";

	public static final String ID_ACTIVITE_COLKEY = "ID_ACTIVITE";
	public static final String ID_COTISATION_COLKEY = "ID_COTISATION";
	public static final String ID_MUTUELLE_COLKEY = "ID_MUTUELLE";
	public static final String ID_NIVEAU_SPORTIF_COLKEY = "ID_NIVEAU_SPORTIF";
	public static final String ID_ORIGINE_RESSOURCES_COLKEY = "ID_ORIGINE_RESSOURCES";
	public static final String ID_PRE_ETUDIANT_COLKEY = "ID_PRE_ETUDIANT";
	public static final String ID_PRE_ETUDIANT_ANNEE_COLKEY = "ID_PRE_ETUDIANT_ANNEE";
	public static final String ID_QUOTITE_COLKEY = "ID_QUOTITE";
	public static final String ID_REGIME_PARTICULIER_COLKEY = "ID_REGIME_PARTICULIER";
	public static final String ID_SITUATION_FAMILIALE_COLKEY = "ID_SITUATION_FAMILIALE";
	public static final String ID_SITUATION_PROF_COLKEY = "ID_SITUATION_PROF";
	public static final String PRO_CODE_COLKEY = "PRO_CODE";
	public static final String PRO_CODE2_COLKEY = "PRO_CODE2";
	public static final String SAL_CRNE_COLKEY = "SAL_CRNE";
	public static final String THAN_CODE_COLKEY = "THAN_CODE";


	// Relationships
	public static final String TO_ACTIVITE_PROFESSIONNELLE_KEY = "toActiviteProfessionnelle";
	public static final String TO_COTISATION_KEY = "toCotisation";
	public static final String TO_INSCRIPTIONS_KEY = "toInscriptions";
	public static final String TO_MUTUELLE_KEY = "toMutuelle";
	public static final String TO_NIVEAU_SPORTIF_KEY = "toNiveauSportif";
	public static final String TO_ORIGINE_RESSOURCES_KEY = "toOrigineRessources";
	public static final String TO_PRE_ETUDIANT_KEY = "toPreEtudiant";
	public static final String TO_PRE_PAIEMENTS_KEY = "toPrePaiements";
	public static final String TO_PROFESSION1_KEY = "toProfession1";
	public static final String TO_PROFESSION2_KEY = "toProfession2";
	public static final String TO_QUOTITE_TRAVAIL_KEY = "toQuotiteTravail";
	public static final String TO_REGIME_PARTICULIER_KEY = "toRegimeParticulier";
	public static final String TO_RNE_KEY = "toRne";
	public static final String TO_SITUATION_FAMILIALE_KEY = "toSituationFamiliale";
	public static final String TO_SITUATION_PROFESSIONNELLE_KEY = "toSituationProfessionnelle";
	public static final String TO_TYPE_HANDICAP_KEY = "toTypeHandicap";



	// Accessors methods
  public Boolean accordAnnuaire() {
    return (Boolean) storedValueForKey(ACCORD_ANNUAIRE_KEY);
  }

  public void setAccordAnnuaire(Boolean value) {
    takeStoredValueForKey(value, ACCORD_ANNUAIRE_KEY);
  }

  public Boolean accordPhoto() {
    return (Boolean) storedValueForKey(ACCORD_PHOTO_KEY);
  }

  public void setAccordPhoto(Boolean value) {
    takeStoredValueForKey(value, ACCORD_PHOTO_KEY);
  }

  public Boolean adhMutuelle() {
    return (Boolean) storedValueForKey(ADH_MUTUELLE_KEY);
  }

  public void setAdhMutuelle(Boolean value) {
    takeStoredValueForKey(value, ADH_MUTUELLE_KEY);
  }

  public Boolean affSs() {
    return (Boolean) storedValueForKey(AFF_SS_KEY);
  }

  public void setAffSs(Boolean value) {
    takeStoredValueForKey(value, AFF_SS_KEY);
  }

  public Integer annee() {
    return (Integer) storedValueForKey(ANNEE_KEY);
  }

  public void setAnnee(Integer value) {
    takeStoredValueForKey(value, ANNEE_KEY);
  }

  public Boolean assuranceCivile() {
    return (Boolean) storedValueForKey(ASSURANCE_CIVILE_KEY);
  }

  public void setAssuranceCivile(Boolean value) {
    takeStoredValueForKey(value, ASSURANCE_CIVILE_KEY);
  }

  public String assuranceCivileCie() {
    return (String) storedValueForKey(ASSURANCE_CIVILE_CIE_KEY);
  }

  public void setAssuranceCivileCie(String value) {
    takeStoredValueForKey(value, ASSURANCE_CIVILE_CIE_KEY);
  }

  public String assuranceCivileNum() {
    return (String) storedValueForKey(ASSURANCE_CIVILE_NUM_KEY);
  }

  public void setAssuranceCivileNum(String value) {
    takeStoredValueForKey(value, ASSURANCE_CIVILE_NUM_KEY);
  }

  public String autreCasExo() {
    return (String) storedValueForKey(AUTRE_CAS_EXO_KEY);
  }

  public void setAutreCasExo(String value) {
    takeStoredValueForKey(value, AUTRE_CAS_EXO_KEY);
  }

  public Integer certificatPartAppel() {
    return (Integer) storedValueForKey(CERTIFICAT_PART_APPEL_KEY);
  }

  public void setCertificatPartAppel(Integer value) {
    takeStoredValueForKey(value, CERTIFICAT_PART_APPEL_KEY);
  }

  public String commentaire() {
    return (String) storedValueForKey(COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    takeStoredValueForKey(value, COMMENTAIRE_KEY);
  }

  public NSTimestamp dCreation() {
    return (NSTimestamp) storedValueForKey(D_CREATION_KEY);
  }

  public void setDCreation(NSTimestamp value) {
    takeStoredValueForKey(value, D_CREATION_KEY);
  }

  public Boolean demandeurEmploi() {
    return (Boolean) storedValueForKey(DEMANDEUR_EMPLOI_KEY);
  }

  public void setDemandeurEmploi(Boolean value) {
    takeStoredValueForKey(value, DEMANDEUR_EMPLOI_KEY);
  }

  public NSTimestamp dModification() {
    return (NSTimestamp) storedValueForKey(D_MODIFICATION_KEY);
  }

  public void setDModification(NSTimestamp value) {
    takeStoredValueForKey(value, D_MODIFICATION_KEY);
  }

  public Integer dureeInterrupt() {
    return (Integer) storedValueForKey(DUREE_INTERRUPT_KEY);
  }

  public void setDureeInterrupt(Integer value) {
    takeStoredValueForKey(value, DUREE_INTERRUPT_KEY);
  }

  public String emailParent() {
    return (String) storedValueForKey(EMAIL_PARENT_KEY);
  }

  public void setEmailParent(String value) {
    takeStoredValueForKey(value, EMAIL_PARENT_KEY);
  }

  public Boolean enfantsCharge() {
    return (Boolean) storedValueForKey(ENFANTS_CHARGE_KEY);
  }

  public void setEnfantsCharge(Boolean value) {
    takeStoredValueForKey(value, ENFANTS_CHARGE_KEY);
  }

  public String etabBacEtranger() {
    return (String) storedValueForKey(ETAB_BAC_ETRANGER_KEY);
  }

  public void setEtabBacEtranger(String value) {
    takeStoredValueForKey(value, ETAB_BAC_ETRANGER_KEY);
  }

  public Boolean etudHandicap() {
    return (Boolean) storedValueForKey(ETUD_HANDICAP_KEY);
  }

  public void setEtudHandicap(Boolean value) {
    takeStoredValueForKey(value, ETUD_HANDICAP_KEY);
  }

  public String etudNoOrigine() {
    return (String) storedValueForKey(ETUD_NO_ORIGINE_KEY);
  }

  public void setEtudNoOrigine(String value) {
    takeStoredValueForKey(value, ETUD_NO_ORIGINE_KEY);
  }

  public Boolean etudSportHN() {
    return (Boolean) storedValueForKey(ETUD_SPORT_HN_KEY);
  }

  public void setEtudSportHN(Boolean value) {
    takeStoredValueForKey(value, ETUD_SPORT_HN_KEY);
  }

  public Boolean formationFinancee() {
    return (Boolean) storedValueForKey(FORMATION_FINANCEE_KEY);
  }

  public void setFormationFinancee(Boolean value) {
    takeStoredValueForKey(value, FORMATION_FINANCEE_KEY);
  }

  public Boolean interruptEtude() {
    return (Boolean) storedValueForKey(INTERRUPT_ETUDE_KEY);
  }

  public void setInterruptEtude(Boolean value) {
    takeStoredValueForKey(value, INTERRUPT_ETUDE_KEY);
  }

  public Integer nbEnfantsCharge() {
    return (Integer) storedValueForKey(NB_ENFANTS_CHARGE_KEY);
  }

  public void setNbEnfantsCharge(Integer value) {
    takeStoredValueForKey(value, NB_ENFANTS_CHARGE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    takeStoredValueForKey(value, PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    takeStoredValueForKey(value, PERS_ID_MODIFICATION_KEY);
  }

  public Integer recensement() {
    return (Integer) storedValueForKey(RECENSEMENT_KEY);
  }

  public void setRecensement(Integer value) {
    takeStoredValueForKey(value, RECENSEMENT_KEY);
  }

  public Boolean respectCharte() {
    return (Boolean) storedValueForKey(RESPECT_CHARTE_KEY);
  }

  public void setRespectCharte(Boolean value) {
    takeStoredValueForKey(value, RESPECT_CHARTE_KEY);
  }

  public String salLibelle() {
    return (String) storedValueForKey(SAL_LIBELLE_KEY);
  }

  public void setSalLibelle(String value) {
    takeStoredValueForKey(value, SAL_LIBELLE_KEY);
  }

  public String sportPratique() {
    return (String) storedValueForKey(SPORT_PRATIQUE_KEY);
  }

  public void setSportPratique(String value) {
    takeStoredValueForKey(value, SPORT_PRATIQUE_KEY);
  }

  public Boolean sportUniv() {
    return (Boolean) storedValueForKey(SPORT_UNIV_KEY);
  }

  public void setSportUniv(Boolean value) {
    takeStoredValueForKey(value, SPORT_UNIV_KEY);
  }

  public String statutBoursierCnous() {
    return (String) storedValueForKey(STATUT_BOURSIER_CNOUS_KEY);
  }

  public void setStatutBoursierCnous(String value) {
    takeStoredValueForKey(value, STATUT_BOURSIER_CNOUS_KEY);
  }

  public String tuteur1() {
    return (String) storedValueForKey(TUTEUR1_KEY);
  }

  public void setTuteur1(String value) {
    takeStoredValueForKey(value, TUTEUR1_KEY);
  }

  public String tuteur2() {
    return (String) storedValueForKey(TUTEUR2_KEY);
  }

  public void setTuteur2(String value) {
    takeStoredValueForKey(value, TUTEUR2_KEY);
  }

  public String typeRegime() {
    return (String) storedValueForKey(TYPE_REGIME_KEY);
  }

  public void setTypeRegime(String value) {
    takeStoredValueForKey(value, TYPE_REGIME_KEY);
  }

  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle toActiviteProfessionnelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle)storedValueForKey(TO_ACTIVITE_PROFESSIONNELLE_KEY);
  }

  public void setToActiviteProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOActiviteProfessionnelle oldValue = toActiviteProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ACTIVITE_PROFESSIONNELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ACTIVITE_PROFESSIONNELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation toCotisation() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation)storedValueForKey(TO_COTISATION_KEY);
  }

  public void setToCotisationRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation oldValue = toCotisation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_COTISATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_COTISATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle toMutuelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle)storedValueForKey(TO_MUTUELLE_KEY);
  }

  public void setToMutuelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOMutuelle oldValue = toMutuelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_MUTUELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_MUTUELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif toNiveauSportif() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif)storedValueForKey(TO_NIVEAU_SPORTIF_KEY);
  }

  public void setToNiveauSportifRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EONiveauSportif oldValue = toNiveauSportif();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_NIVEAU_SPORTIF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_NIVEAU_SPORTIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources toOrigineRessources() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources)storedValueForKey(TO_ORIGINE_RESSOURCES_KEY);
  }

  public void setToOrigineRessourcesRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOOrigineRessources oldValue = toOrigineRessources();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_ORIGINE_RESSOURCES_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_ORIGINE_RESSOURCES_KEY);
    }
  }
  
  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant() {
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant)storedValueForKey(TO_PRE_ETUDIANT_KEY);
  }

  public void setToPreEtudiantRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant value) {
    if (value == null) {
    	org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant oldValue = toPreEtudiant();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PRE_ETUDIANT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PRE_ETUDIANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession1() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(TO_PROFESSION1_KEY);
  }

  public void setToProfession1Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession1();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROFESSION1_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROFESSION1_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOProfession toProfession2() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOProfession)storedValueForKey(TO_PROFESSION2_KEY);
  }

  public void setToProfession2Relationship(org.cocktail.fwkcktlpersonne.common.metier.EOProfession value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOProfession oldValue = toProfession2();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_PROFESSION2_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_PROFESSION2_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail toQuotiteTravail() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail)storedValueForKey(TO_QUOTITE_TRAVAIL_KEY);
  }

  public void setToQuotiteTravailRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail oldValue = toQuotiteTravail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_QUOTITE_TRAVAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_QUOTITE_TRAVAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier toRegimeParticulier() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier)storedValueForKey(TO_REGIME_PARTICULIER_KEY);
  }

  public void setToRegimeParticulierRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EORegimeParticulier oldValue = toRegimeParticulier();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_REGIME_PARTICULIER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_REGIME_PARTICULIER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EORne toRne() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EORne)storedValueForKey(TO_RNE_KEY);
  }

  public void setToRneRelationship(org.cocktail.fwkcktlpersonne.common.metier.EORne value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EORne oldValue = toRne();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_RNE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_RNE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale toSituationFamiliale() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale)storedValueForKey(TO_SITUATION_FAMILIALE_KEY);
  }

  public void setToSituationFamilialeRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale oldValue = toSituationFamiliale();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_FAMILIALE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_FAMILIALE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle toSituationProfessionnelle() {
    return (org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle)storedValueForKey(TO_SITUATION_PROFESSIONNELLE_KEY);
  }

  public void setToSituationProfessionnelleRelationship(org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle value) {
    if (value == null) {
    	org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle oldValue = toSituationProfessionnelle();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_SITUATION_PROFESSIONNELLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_SITUATION_PROFESSIONNELLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap toTypeHandicap() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap)storedValueForKey(TO_TYPE_HANDICAP_KEY);
  }

  public void setToTypeHandicapRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap value) {
    if (value == null) {
    	org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap oldValue = toTypeHandicap();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_TYPE_HANDICAP_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_TYPE_HANDICAP_KEY);
    }
  }
  
	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toInscriptions() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)storedValueForKey(TO_INSCRIPTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toInscriptions(EOQualifier qualifier) {
    return toInscriptions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toInscriptions(EOQualifier qualifier, boolean fetch) {
    return toInscriptions(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> toInscriptions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription.TO_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toInscriptions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_INSCRIPTIONS_KEY);
  }

  public void removeFromToInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INSCRIPTIONS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription createToInscriptionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPreInscription");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_INSCRIPTIONS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription) eo;
  }

  public void deleteToInscriptionsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_INSCRIPTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToInscriptionsRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription> objects = toInscriptions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToInscriptionsRelationship(objects.nextElement());
    }
  }

	@SuppressWarnings("unchecked")
  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> toPrePaiements() {
    return (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>)storedValueForKey(TO_PRE_PAIEMENTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> toPrePaiements(EOQualifier qualifier) {
    return toPrePaiements(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> toPrePaiements(EOQualifier qualifier, boolean fetch) {
    return toPrePaiements(qualifier, null, fetch);
  }

@SuppressWarnings("unchecked")
public NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> toPrePaiements(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement.TO_PRE_ETUDIANT_ANNEE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toPrePaiements();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToPrePaiementsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENTS_KEY);
  }

  public void removeFromToPrePaiementsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENTS_KEY);
  }

  public org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement createToPrePaiementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("EOPrePaiement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TO_PRE_PAIEMENTS_KEY);
    return (org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement) eo;
  }

  public void deleteToPrePaiementsRelationship(org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TO_PRE_PAIEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToPrePaiementsRelationships() {
    Enumeration<org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement> objects = toPrePaiements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToPrePaiementsRelationship(objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPreEtudiantAnnee avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPreEtudiantAnnee createEOPreEtudiantAnnee(EOEditingContext editingContext, Integer annee
, NSTimestamp dCreation
, Boolean enfantsCharge
, Integer nbEnfantsCharge
, org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant toPreEtudiant			) {
    EOPreEtudiantAnnee eo = (EOPreEtudiantAnnee) createAndInsertInstance(editingContext, _EOPreEtudiantAnnee.ENTITY_NAME);    
		eo.setAnnee(annee);
		eo.setDCreation(dCreation);
		eo.setEnfantsCharge(enfantsCharge);
		eo.setNbEnfantsCharge(nbEnfantsCharge);
    eo.setToPreEtudiantRelationship(toPreEtudiant);
    return eo;
  }

  
	  public EOPreEtudiantAnnee localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPreEtudiantAnnee)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreEtudiantAnnee creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPreEtudiantAnnee creerInstance(EOEditingContext editingContext, NSArray<ISpecificite> specificites) {
	  		EOPreEtudiantAnnee object = (EOPreEtudiantAnnee)createAndInsertInstance(editingContext, _EOPreEtudiantAnnee.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPreEtudiantAnnee localInstanceIn(EOEditingContext editingContext, EOPreEtudiantAnnee eo) {
    EOPreEtudiantAnnee localInstance = (eo == null) ? null : (EOPreEtudiantAnnee)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPreEtudiantAnnee#localInstanceIn a la place.
   */
	public static EOPreEtudiantAnnee localInstanceOf(EOEditingContext editingContext, EOPreEtudiantAnnee eo) {
		return EOPreEtudiantAnnee.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray<EOSortOrdering> sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  return fetchAll(editingContext, qualifier, sortOrderings, distinct, true, null );
	  }
	  
	  public static NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct, boolean isDeep, NSArray<String> prefetchKeyPaths) {
		  EOFetchSpecification fetchSpec = fetchSpecification(qualifier, sortOrderings, distinct);
		  fetchSpec.setIsDeep(isDeep);
		  fetchSpec.setPrefetchingRelationshipKeyPaths(prefetchKeyPaths);
		  NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee> eoObjects = (NSArray<org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee>)editingContext.objectsWithFetchSpecification(fetchSpec);
		  return eoObjects;
	  }

	  /**
	   * Cree une fetchSpec sur l'entité avec les parametres.
	   * @param qualifier
	   * @param sortOrderings
	   * @param distinct
	   * @return
	   */
	  public static EOFetchSpecification fetchSpecification(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean distinct) {
		  EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		  fetchSpec.setIsDeep(true);
		  fetchSpec.setUsesDistinct(distinct);
		  return fetchSpec;
	  }
	  
	  
		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPreEtudiantAnnee fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPreEtudiantAnnee fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray<EOPreEtudiantAnnee> eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPreEtudiantAnnee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPreEtudiantAnnee)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPreEtudiantAnnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPreEtudiantAnnee fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
	    NSArray<EOPreEtudiantAnnee> eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPreEtudiantAnnee eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPreEtudiantAnnee)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPreEtudiantAnnee fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPreEtudiantAnnee eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPreEtudiantAnnee ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPreEtudiantAnnee fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
