-- ATTENTION
-- Ce script doit être exécuter sur 1 User qui a le rôle de DBA (ex.: User GRHUM)
-- USER SQL


--
-- Patch DBA de SCO_PRE_SCOLARITE du 04/06/2013 à exécuter depuis le user GRHUM
--
-- Rem : fichier encodé UTF-8

SET DEFINE OFF;
--
--
-- Fichier : 1/3
-- Type : DML
-- Schema : SCO_PRE_SCOLARITE
-- Numero de version : 0.1.0.0
-- Date de publication : 04/06/2013
-- Auteur(s) : Association Cocktail
--
--
 
 -- activation d'affichage
set serveroutput on feedback on

declare

  dbn varchar2(255);
  ibn varchar2(255);
  ord_data varchar2(1024);
  ord_indx varchar2(1024);
  is_tbs int;
        
   c int;

begin  

-- Test d'existence du User Sco_Pre_Scolarite
   select count(*) into c from dba_users where username = upper('SCO_PRE_SCOLARITE');
   if c <> 0 then
      execute immediate '
      DROP USER SCO_PRE_SCOLARITE CASCADE
      ';
   end if;   
  
  -- test d’existence du tablespace DATA
  select count(*) into is_tbs from dba_tablespaces where tablespace_name = 'DATA_CKTL_SCOL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into dbn from dba_data_files where upper(tablespace_name) = 'DATA_GRHUM';
    -- construction de l’ordre a executer
    ord_data := 'create tablespace DATA_CKTL_SCOL datafile '||chr(39)||dbn||'/data_cktl_scol.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace DATA : '||ord_data);
    -- creation du tablespace DATA
    execute immediate ord_data;
  end if;
  
  -- test d’existence du tablespace INDX
  select count(*) into is_tbs from dba_tablespaces where upper(tablespace_name) = 'INDX_CKTL_SCOL';
  if is_tbs = 0 then
    -- extraction des repertoires
    select substr(max(file_name),1,(instr(max(file_name),'/',-1,1)-1)) into ibn from dba_data_files where upper(tablespace_name) = 'INDX_GRHUM';
    -- construction de l’ordre a executer
    ord_indx := 'create tablespace INDX_CKTL_SCOL datafile '||chr(39)||ibn||'/indx_cktl_scol.dbf'||chr(39)||' size 50M';
    -- controle des noms par affichage
    dbms_output.put_line('Création du tablespace INDEXES : '||ord_indx);
    -- creation du tablespace INDX
    execute immediate ord_indx;
  end if;
end;
/

-- CREATION DU USER
CREATE USER SCO_PRE_SCOLARITE IDENTIFIED BY bdlmmdesco__pre_scolarite
DEFAULT TABLESPACE DATA_CKTL_SCOL TEMPORARY TABLESPACE TEMP quota unlimited on DATA_CKTL_SCOL;
ALTER USER SCO_PRE_SCOLARITE quota unlimited on INDX_CKTL_SCOL;

GRANT CONNECT, RESOURCE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.ORIGINE_RESSOURCES TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.MUTUELLE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.SITUATION_PROFESSIONNELLE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.QUOTITE_TRAVAIL TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.ACTIVITE_PROFESSIONNELLE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.COTISATION TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.GRADE_UNIVERSITAIRE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.COTISATION TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.TYPE_FORMATION TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.TYPE_INSCRIPTION_FORMATION  TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.TYPE_ECHANGE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON SCO_SCOLARITE.DIPLOME TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.PROFESSION TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.SITUATION_FAMILIALE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.RNE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.TYPE_HANDICAP TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.PERSONNE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.CIVILITE TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.PAYS TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.TYPE_TEL TO SCO_PRE_SCOLARITE;
GRANT select, REFERENCES ON GRHUM.TYPE_NO_TEL TO SCO_PRE_SCOLARITE;

