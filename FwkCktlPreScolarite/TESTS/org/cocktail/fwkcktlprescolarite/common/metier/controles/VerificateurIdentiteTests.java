/*
     * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software 
     * is governed by the CeCILL license under French law and abiding by the
     * rules of distribution of free software. You can use, modify and/or 
     * redistribute the software under the terms of the CeCILL license as 
     * circulated by CEA, CNRS and INRIA at the following URL 
     * "http://www.cecill.info". 
     * As a counterpart to the access to the source code and rights to copy, modify 
     * and redistribute granted by the license, users are provided only with a 
     * limited warranty and the software's author, the holder of the economic 
     * rights, and the successive licensors have only limited liability. In this 
     * respect, the user's attention is drawn to the risks associated with loading,
     * using, modifying and/or developing or reproducing the software by the user 
     * in light of its specific status of free software, that may mean that it
     * is complicated to manipulate, and that also therefore means that it is 
     * reserved for developers and experienced professionals having in-depth
     * computer knowledge. Users are therefore encouraged to load and test the 
     * software's suitability as regards their requirements in conditions enabling
     * the security of their systems and/or data to be ensured and, more generally, 
     * to use and operate it in the same conditions as regards security. The
     * fact that you are presently reading this means that you have had knowledge 
     * of the CeCILL license and that you accept its terms.
     */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPaysIndicatif;
import org.cocktail.fwkcktlpersonne.common.metier.repositories.IPaysIndicatifRepository;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.services.EOPreEtudiantService;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEnterpriseObjectCache;

/**
 * Classe contenant les contrôleurs sur les identités
 * @author alainmalaplate
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class VerificateurIdentiteTests {
	
	private VerificateurIdentite verifIdentite = null;
	private EOTypeTel typeTelEtudiant = null;
	private EOTypeTel typeTelParent = null;
	private EOTypeNoTel typeFixe = null;
	private EOTypeNoTel typePortable = null;
	
	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
	
	/**
	 * Méthode setUp pour l'initialisation avant de faire n'importe quel test
	 */
	@Before
	public void setUp() {
		System.out.println(">>>VerificateurIdentiteTests.setUp()");
		System.out.println(System.getProperty("java.class.path"));
		System.out.println(NSBundle.allBundles());
		initialiserAllCacheMock(ec);
		verifIdentite = new VerificateurIdentite();
		typeTelEtudiant = new EOTypeTel();
		typeTelEtudiant.setCTypeTel(EOTypeTel.C_TYPE_TEL_ETUD);
		ec.insertSavedObject(typeTelEtudiant);
		typeTelParent = new EOTypeTel();
		typeTelParent.setCTypeTel(EOTypeTel.C_TYPE_TEL_PAR);
		ec.insertSavedObject(typeTelParent);
		typeFixe = new EOTypeNoTel();
		typeFixe.setCTypeNoTel(EOTypeNoTel.TYPE_NO_TEL_TEL);
		ec.insertSavedObject(typeFixe);
		typePortable = new EOTypeNoTel();
		typePortable.setCTypeNoTel(EOTypeNoTel.TYPE_NO_TEL_MOB);
		ec.insertSavedObject(typePortable);
		
	}

	/**
	 * Méthode tearDown pour terminer tous les tests
	 */
	@After
	public void tearDown() {
		verifIdentite = null;
	}
	
	/**
	 * la méthode controleDesNoms procèdent à une série de tests
	 * qui doivent fonctionner. S'il y a une erreur, c'est que le code à changer !
	 */
	@Test
    public void controleDesNoms() {
        
		try {
			verifIdentite.controleSansEspaceAuDebut("Lafourcade");
			verifIdentite.controleDenomination("Le Van", "Problème avec le nom !");
			verifIdentite.controleNomPatronymique("Le Van");
			
		} catch (MessageErreur e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
    }
	
	/**
	 * Test avec une erreur attendue dans le cas d'un nom
	 * qui est juste un espace 
	 */
	@Test(expected = MessageErreur.class)
	public void testNonValiderBlanc() {
		verifIdentite.controleSansEspaceAuDebut(" ");	
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'un nom
	 * avec un espace au début
	 */
	@Test(expected = MessageErreur.class)
	public void testNonmDebutEspace() {
		verifIdentite.controleSansEspaceAuDebut(" Lafourcade");	
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'un nom
	 * qui ne contient pas que des lettres
	 */
	@Test(expected = MessageErreur.class)
	public void testNonmAvecChiffre() {
		verifIdentite.controleDenomination("Moncad3", "Problème avec le nom !");	
	}
	
	
	/**
	 * Vérification que le contrôle de la civilité fonctionne
	 */
	@Test
	public void testCiviliteOK(){
        IPaysIndicatifRepository repo = mockIndicatifRepository();
        EOPreEtudiantService etudiantService = new EOPreEtudiantService();
        etudiantService.setPaysIndicatifRepository(repo);
		EOCivilite  civilite = EOCivilite.createEOCivilite(ec, EOCivilite.C_CIVILITE_MONSIEUR, new NSTimestamp(), new NSTimestamp());
		civilite.setSexe("1");
		EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
		preEtudiant.setToCiviliteRelationship(civilite);
		preEtudiant.setNoInsee("1700133555856");
		
		assertTrue("ce test doit fonctionner pour un H", verifIdentite.controleCivilite(preEtudiant));
		
		civilite.setCCivilite(EOCivilite.C_CIVILITE_MADAME);
		civilite.setSexe("2");
		preEtudiant.setNoInsee("2700133555856");
		assertTrue("ce test doit fonctionner pour une F", verifIdentite.controleCivilite(preEtudiant));
		
		civilite.setCCivilite(EOCivilite.C_CIVILITE_MADEMOISELLE);
		assertTrue("ce test doit fonctionner pour une demoiselle", verifIdentite.controleCivilite(preEtudiant));
	}
	
	/**
	 * Vérification que les erreurs de civilité sont détectées
	 */
	@Test
	public void testCivilitePasOK(){
	    IPaysIndicatifRepository repo = mockIndicatifRepository();
	    EOPreEtudiantService etudiantService = new EOPreEtudiantService();
	    etudiantService.setPaysIndicatifRepository(repo);
	    EOCivilite  civilite = EOCivilite.createEOCivilite(ec, EOCivilite.C_CIVILITE_MONSIEUR, new NSTimestamp(), new NSTimestamp());
	    civilite.setSexe("1");
	    EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
	    preEtudiant.setToCiviliteRelationship(civilite);
	    preEtudiant.setNoInsee("2700133555856");

	    assertFalse("ce test ne doit pas fonctionner pour un H", verifIdentite.controleCivilite(preEtudiant));

	    civilite.setSexe("2");
	    preEtudiant.setNoInsee("1700133555856");
	    assertFalse("ce test ne doit pas fonctionner pour un H car son sexe est mal renseigné", verifIdentite.controleCivilite(preEtudiant));
	}
	
	/**
	 * Vérification que le nom de famille est renseignée pour les femmes mariées
	 */
	@Test
	public void testNomFamilleOK(){
	    IPaysIndicatifRepository repo = mockIndicatifRepository();
		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
		etudiantService.setPaysIndicatifRepository(repo);
		EOCivilite  civilite = EOCivilite.createEOCivilite(ec, EOCivilite.C_CIVILITE_MADAME, new NSTimestamp(), new NSTimestamp());
		EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
		preEtudiant.setToCiviliteRelationship(civilite);
		preEtudiant.setNomPatronymique("Angegardien");
		
		assertTrue("Le nom de famille doit être renseigné pour une femme mariée", verifIdentite.isFemmeMarieeComplete(preEtudiant));

		civilite.setCCivilite(EOCivilite.C_CIVILITE_MADEMOISELLE);
		assertFalse("Le nom de famille n'est pas obligatoire si ce n'est pas une femme mariée", verifIdentite.isFemmeMarieeComplete(preEtudiant));
		
	}
	
	@Test
	public void testNomFamilleFalse(){
		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
		IPaysIndicatifRepository repo = mockIndicatifRepository();
		etudiantService.setPaysIndicatifRepository(repo);
		EOCivilite  civilite = EOCivilite.createEOCivilite(ec, EOCivilite.C_CIVILITE_MADAME, new NSTimestamp(), new NSTimestamp());
		EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
		preEtudiant.setToCiviliteRelationship(civilite);
		preEtudiant.setNomPatronymique("Ang3g@rdien");
		
		assertFalse("Le nom de famille doit être renseigné pour une femme mariée et ne pas contenir de caractères farfelus", verifIdentite.isFemmeMarieeComplete(preEtudiant));
		
		preEtudiant.setNomPatronymique("");
		assertFalse("Le nom de famille ne doit pas être renseigné pour une femme mariée une chaîne vide", verifIdentite.isFemmeMarieeComplete(preEtudiant));
		
		preEtudiant.setNomPatronymique(null);
		assertFalse("Le nom de famille ne doit pas être renseigné pour une femme mariée avec null", verifIdentite.isFemmeMarieeComplete(preEtudiant));
		
	}

    private IPaysIndicatifRepository mockIndicatifRepository() {
        IPaysIndicatif defaultPaysIndicatif = Mockito.mock(IPaysIndicatif.class);
		Mockito.when(defaultPaysIndicatif.indicatif()).thenReturn(33);
		IPaysIndicatifRepository repo = Mockito.mock(IPaysIndicatifRepository.class);
		Mockito.when(repo.defaultPaysIndicatif()).thenReturn(defaultPaysIndicatif);
        return repo;
    }
	
	public static <T extends EOEnterpriseObject> ERXEnterpriseObjectCache<T> createCacheMock(Class<T> clazz, String key, final MockEditingContext mockEc) {
		ERXEnterpriseObjectCache<T> cache = new ERXEnterpriseObjectCache<T>(clazz, key) {
			@Override
			protected ERXEC editingContext() {
				return mockEc;
			}
		};
		cache.setReuseEditingContext(true);
		return cache;
	}

	public static void initialiserAllCacheMock(final MockEditingContext mockEc) {
		EOTypeTel.setTypeTelCache(createCacheMock(EOTypeTel.class, EOTypeTel.C_TYPE_TEL_KEY, mockEc));
		EOTypeNoTel.setTypeNoTelCache(createCacheMock(EOTypeNoTel.class, EOTypeNoTel.C_TYPE_NO_TEL_KEY, mockEc));
	}
}
