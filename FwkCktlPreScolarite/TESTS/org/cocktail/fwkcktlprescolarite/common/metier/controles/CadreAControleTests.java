package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle;
import org.junit.Test;

/**
 * Tests des controle du cadre A (identite de l'étudiant)
 */
public class CadreAControleTests {

	@Test
	public void codeINEAlphanumeriqueOK() {
		// Arrange
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		// Act
		CadreAControle cadreAControle = new CadreAControle(new ControlesContexte());

		// Assert
		when(preEtudiant.codeIne()).thenReturn("123GTF0");
		cadreAControle.controler(preEtudiant, preEtudiantAnnee);
		assertThat(cadreAControle.getContexte().getCodeErreurs()).doesNotContain(CadreAControle.CODE_INE_CARACTERE_ALPHANUMERIQUE);
	}

	@Test
	public void codeINEAlphanumeriqueKO() {
		// Arrange
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		// Act
		CadreAControle cadreAControle = new CadreAControle(new ControlesContexte());
		// Assert
		when(preEtudiant.codeIne()).thenReturn("123GTF:");
		cadreAControle.controler(preEtudiant, preEtudiantAnnee);
		assertThat(cadreAControle.getContexte().getCodeErreurs()).contains(CadreAControle.CODE_INE_CARACTERE_ALPHANUMERIQUE);
	}
	
	//attention le controle n'est pas le meme que dans la situation sociale
	@Test
	public void testControleSituationProfessionnelleCoherente() {
		// Arrange
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		EOSituationProfessionnelle situationProfessionnelle = mock(EOSituationProfessionnelle.class);
		EOQuotiteTravail quotiteTravail = mock(EOQuotiteTravail.class);

		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(situationProfessionnelle);
		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(quotiteTravail);

	// Act
			CadreAControle cadreAControle = new CadreAControle(new ControlesContexte());
			
		// Assert
		when(situationProfessionnelle.codeSitProf()).thenReturn(EOSituationProfessionnelle.CODE_SITPROF_SALARIE);
		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_TEMPS_COMPLET);
		assertTrue("étudiant salarié et quotité de travail à temps complet", cadreAControle.controleSituationProfessionnelleCoherente(preEtudiantAnnee));

		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_SANS_OBJET);
		assertFalse("étudiant salarié et quotité de travail sans objet", cadreAControle.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
		
		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(null);
		assertTrue("étudiant salarié et quotité de travail null", cadreAControle.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
		
		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(quotiteTravail);
		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(null);
		assertTrue("étudiant sans situation professionnelle", cadreAControle.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
		
		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(situationProfessionnelle);
		when(situationProfessionnelle.codeSitProf()).thenReturn(EOSituationProfessionnelle.CODE_SITPROF_MILITAIRE);
		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_TEMPS_COMPLET);
		assertTrue("étudiant militaie", cadreAControle.controleSituationProfessionnelleCoherente(preEtudiantAnnee));				
	}
	
}
