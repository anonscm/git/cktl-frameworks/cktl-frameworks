package org.cocktail.fwkcktlprescolarite.common.metier.controles.outilscontroleurs;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlprescolarite.common.metier.IPreBourses;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegimeParticulier;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBoursesCF;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;

public class CotisationRegimeControleTests {

	private static final int ANNEE_UNIVERSITAIRE = 2014;
	private SimpleDateFormat simpleDateFormat;
	private IInfosEtudiant etudiantMoinsDe20Ans;
	private IInfosEtudiant etudiant20AnsOuPlus;
	private IInfosEtudiant etudiant28AnsOuPlus;
	private IEtudiantAnnee etudiantAnneeMoinsDe20Ans;
	private IEtudiantAnnee etudiantAnnee20AnsOuPlus;
	private IEtudiantAnnee etudiantAnnee28AnsOuPlus;
	private EOCotisation cotisation;
	private IRegimeParticulier regimeParticulierParent;
	private CotisationRegimeSecuControle cotisationRegimeSecuControle;

	@Before
	public void setUp() throws ParseException {
		simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		etudiantMoinsDe20Ans = mock(IPreEtudiant.class);
		when(etudiantMoinsDe20Ans.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1997")));

		etudiant20AnsOuPlus = mock(IPreEtudiant.class);
		when(etudiant20AnsOuPlus.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1994")));

		etudiant28AnsOuPlus = mock(IPreEtudiant.class);
		when(etudiant28AnsOuPlus.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1986")));

		cotisation = mock(EOCotisation.class);
		regimeParticulierParent = mock(IRegimeParticulier.class);

		etudiantAnneeMoinsDe20Ans = mock(IPreEtudiantAnnee.class);
		when(etudiantAnneeMoinsDe20Ans.toInfosEtudiant()).thenReturn(etudiantMoinsDe20Ans);
		when(etudiantAnneeMoinsDe20Ans.toCotisation()).thenReturn(cotisation);
		when(etudiantAnneeMoinsDe20Ans.toRegimeParticulier()).thenReturn(regimeParticulierParent);

		etudiantAnnee20AnsOuPlus = mock(IPreEtudiantAnnee.class);
		when(etudiantAnnee20AnsOuPlus.toInfosEtudiant()).thenReturn(etudiant20AnsOuPlus);
		when(etudiantAnnee20AnsOuPlus.toCotisation()).thenReturn(cotisation);
		when(etudiantAnnee20AnsOuPlus.toRegimeParticulier()).thenReturn(regimeParticulierParent);

		etudiantAnnee28AnsOuPlus = mock(IPreEtudiantAnnee.class);
		when(etudiantAnnee28AnsOuPlus.toInfosEtudiant()).thenReturn(etudiant28AnsOuPlus);
		when(etudiantAnnee28AnsOuPlus.toCotisation()).thenReturn(cotisation);
		when(etudiantAnnee28AnsOuPlus.toRegimeParticulier()).thenReturn(regimeParticulierParent);

		cotisationRegimeSecuControle = new CotisationRegimeSecuControle(ANNEE_UNIVERSITAIRE);
	}

	@Test
	public void doitCotiserCode1() throws Exception {
		// Bénéficiaire du régime particulier d'un de vos parents (SNCF par exemple) - justificatif à fournir
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PARENT_REGIME_PARTICULIER);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();

		// Avec un régime qui ne force pas l'affiliation à la SS étudiante
		when(regimeParticulierParent.affiliationSsEtudiante()).thenReturn(false);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();

		// Avec un régime qui force l'affiliation à la SS étudiante
		when(regimeParticulierParent.affiliationSsEtudiante()).thenReturn(true);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode2() throws Exception {
		// Agé(e) de plus de 28 ans
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PLUS_DE_28_ANS);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode3() throws Exception {
		// Salarié(e) à plus de 60h par mois - justificatif à fournir
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_SALARIE);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode4() throws Exception {
		// Ayant droit de votre conjoint salarié
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_CONJOINT);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode5() throws Exception {
		// Enfant d'un travailleur non salarié (artisan, commerçant, profession libérale ...)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode6() throws Exception {
		// Déjà affilié(e) à la sécurité sociale étudiante (dans un autre établissement)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_DEJA_AFFILIE);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode7() throws Exception {
		// Demandeur(euse) d'emploi
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_DEMANDEUR_D_EMPLOI);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode9() throws Exception {
		// Titulaire de la carte européenne en cours de validité
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_CARTE_EUROPEENNE);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode10() throws Exception {
		// Etudiant étranger hors union européenne
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ETUDIANT_ETRANGER);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isTrue();
	}

	@Test
	public void doitCotiserCode11() throws Exception {
		// Enfant d'au moins un salarié (privé ou public)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ENFANT_SALARIE);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void doitCotiserCode12() throws Exception {
		// Etudiant indépendant
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ETUDIANT_INDEPENDANT);
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.doitEtreAffilie(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode1() throws Exception {
		// Bénéficiaire du régime particulier d'un de vos parents (SNCF par exemple) - justificatif à fournir
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PARENT_REGIME_PARTICULIER);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode2() throws Exception {
		// Agé(e) de plus de 28 ans
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PLUS_DE_28_ANS);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode3() throws Exception {
		// Salarié(e) à plus de 60h par mois - justificatif à fournir
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_SALARIE);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode4() throws Exception {
		// Ayant droit de votre conjoint salarié
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_CONJOINT);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode5() throws Exception {
		// Enfant d'un travailleur non salarié (artisan, commerçant, profession libérale ...)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_PARENT_TRAVAILLEUR_NON_SALARIE);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode6() throws Exception {
		// Déjà affilié(e) à la sécurité sociale étudiante (dans un autre établissement)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_DEJA_AFFILIE);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode7() throws Exception {
		// Demandeur(euse) d'emploi
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_DEMANDEUR_D_EMPLOI);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode9() throws Exception {
		// Titulaire de la carte européenne en cours de validité
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_CARTE_EUROPEENNE);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode10() throws Exception {
		// Etudiant étranger hors union européenne
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ETUDIANT_ETRANGER);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode11() throws Exception {
		// Enfant d'au moins un salarié (privé ou public)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ENFANT_SALARIE);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode12() throws Exception {
		// Etudiant indépendant
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ETUDIANT_INDEPENDANT);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void estExonereCode11Boursier() throws Exception {
		// Enfant d'au moins un salarié (privé ou public)
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ENFANT_SALARIE);

		IPreBourses preBourse = mock(IPreBourses.class);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);

		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		// Bourse "normale"
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();

		// Bourse campus france
		when(preBourse.isCampusFrance()).thenReturn(true);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();

		ITypeBoursesCF typeBoursesCF = mock(ITypeBoursesCF.class);
		when(preBourse.toTypeBoursesCF()).thenReturn(typeBoursesCF);
		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_SANS);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();

		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_INSCRIPTION);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isFalse();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();

		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_SECU);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();

		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_INSCRIPTION_SECU);
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnneeMoinsDe20Ans)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee20AnsOuPlus)).isTrue();
		assertThat(cotisationRegimeSecuControle.estExonereDroitSsEtudiante(etudiantAnnee28AnsOuPlus)).isFalse();
	}

	@Test
	public void calculAyantDroit() {
		// Arrange
		CotisationRegimeSecuControle cotisationRegimeControle = spy(new CotisationRegimeSecuControle(ANNEE_UNIVERSITAIRE));
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);

		// etudiant affilié et payant
		when(preEtudiantAnnee.affSs()).thenReturn(true);
		doReturn(false).when(cotisationRegimeControle).estExonereDroitSsEtudiante(preEtudiantAnnee);
		assertThat(cotisationRegimeControle.calculAyantDroit(preEtudiantAnnee)).isEqualTo(-1);

		// etudiant affilié et exonere
		when(preEtudiantAnnee.affSs()).thenReturn(true);
		doReturn(true).when(cotisationRegimeControle).estExonereDroitSsEtudiante(preEtudiantAnnee);
		assertThat(cotisationRegimeControle.calculAyantDroit(preEtudiantAnnee)).isEqualTo(-2);
		
		when(preEtudiantAnnee.affSs()).thenReturn(false);
		assertThat(cotisationRegimeControle.calculAyantDroit(preEtudiantAnnee)).isEqualTo(1);
	}
}
