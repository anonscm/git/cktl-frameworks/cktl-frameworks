/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeHandicap;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.junit.Test;

/**
 *  Classe  qui sert pour les test unitaires sur  le cadre K, renseignements divers 
 * @author isabelle
 *
 */
public class CadreKControleTest {

	@Test
	public void testControleInterruptDureeOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.interruptEtude()).thenReturn(true);
		when(preEtudiantAnnee.dureeInterrupt()).thenReturn(2);
		
		CadreKControle controle = new CadreKControle(new ControlesContexte());
		assertTrue(controle.controleInterruptDureeOK(preEtudiantAnnee));
		assertEquals(0, controle.getContexte().getListeErreurs().size());
	}

	@Test
	public void testControleInterruptDureePasOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.interruptEtude()).thenReturn(true);
		
		CadreKControle controle = new CadreKControle(new ControlesContexte());
		assertFalse(controle.controleInterruptDureeOK(preEtudiantAnnee));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(CadreKControle.PERIODE_INTERRUPTION, controle.getContexte().getCodeErreurs().get(0));
	}
	
	@Test
	public void testControleTypeHandicapOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.etudHandicap()).thenReturn(true);
		EOTypeHandicap typeHandicap = mock(EOTypeHandicap.class);
		when(preEtudiantAnnee.toTypeHandicap()).thenReturn(typeHandicap);
		
		CadreKControle controle = new CadreKControle(new ControlesContexte());
		assertTrue(controle.controleTypeHandicapOK(preEtudiantAnnee));
		assertEquals(0, controle.getContexte().getListeErreurs().size());
	}
	
	@Test
	public void testControleTypeHandicapPasOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.etudHandicap()).thenReturn(true);
		
		CadreKControle controle = new CadreKControle(new ControlesContexte());
		assertFalse(controle.controleTypeHandicapOK(preEtudiantAnnee));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(CadreKControle.TYPE_HANDICAP, controle.getContexte().getCodeErreurs().get(0));
	}
	
//	@Test
//	public void testControleAssuranceCivileOK()  {
//		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
//		when(preEtudiantAnnee.assuranceCivile()).thenReturn(true);
//		when(preEtudiantAnnee.assuranceCivileCie()).thenReturn("ma compagnie");
//		when(preEtudiantAnnee.assuranceCivileNum()).thenReturn("125478");
//		
//		CadreKControle controle = new CadreKControle(new ControlesContexte());
//		assertTrue(controle.controleAssuranceCivileOK(preEtudiantAnnee));
//		assertEquals(0, controle.getContexte().getListeErreurs().size());
//			
//	}
	
//	@Test
//	public void testControleAssuranceCivilePasNomCompagniePasOK()  {
//		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
//		when(preEtudiantAnnee.assuranceCivile()).thenReturn(true);
//		when(preEtudiantAnnee.assuranceCivileNum()).thenReturn("125478");
//		
//		CadreKControle controle = new CadreKControle(new ControlesContexte());
//		assertFalse(controle.controleAssuranceCivileOK(preEtudiantAnnee));
//		assertEquals(1, controle.getContexte().getListeErreurs().size());
//		assertEquals(CadreKControle.ASSURANCE_CIVILE, controle.getContexte().getListeErreurs().get(0));
//		
//	}
	
//	@Test
//	public void testControleAssuranceCivilePasNumAssurancePasOK()  {
//		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
//		when(preEtudiantAnnee.assuranceCivile()).thenReturn(true);
//		when(preEtudiantAnnee.assuranceCivileCie()).thenReturn("ma compagnie");
//		
//		CadreKControle controle = new CadreKControle(new ControlesContexte());
//		assertFalse(controle.controleAssuranceCivileOK(preEtudiantAnnee));
//		assertEquals(1, controle.getContexte().getListeErreurs().size());
//		assertEquals(CadreKControle.ASSURANCE_CIVILE, controle.getContexte().getListeErreurs().get(0));
//		
//	}
	
}
