/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2013 This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOEditingContext;
import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

/**
 * Classe de Tests Unitaires sur les Années
 * @author alainmalaplate
 *
 */
public class VerificateurAnneeTests {

	
	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");
	
	@Dummy
	EOGrhumParametres grhumParametres;
	
	private VerificateurAnnee verifAnnee = null;
	
	/**
	 * Méthode setUp pour l'initialisation avant de faire n'importe quel test
	 */
	@Before
	public void setUp() {
		verifAnnee = new VerificateurAnnee();
	}

	/**
	 * Méthode tearDown pour terminer tous les tests
	 */
	@After
	public void tearDown() {
		verifAnnee = null;
	}
	
	/**
	 * la méthode controleDesAnnees procèdent à une série de tests
	 * qui doivent fonctionner. S'il y a une erreur, c'est que le code à changer !
	 */
	@Test
	public void controleDesAnnees() {
		try {
			verifAnnee.isYearNotNullAndValide("2013");
			verifAnnee.isYearNotNullAndValide("2000");
			verifAnnee.isYearNotNullAndValide("1973");
			
		} catch (MessageErreur e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'une année
	 * qui est nulle
	 */
	@Test(expected = MessageErreur.class)
	public void testAvecNull() {
		verifAnnee.isYearNotNullAndValide(null);
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'une année
	 * avec un caractère vide
	 */
	@Test(expected = MessageErreur.class)
	public void testAvecVide() {
		verifAnnee.isYearNotNullAndValide("");
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'une année
	 * avec un caractère non blanc
	 */
	@Test
	public void testAvecBlanc() {
		assertFalse(verifAnnee.isYearNotNullAndValide(" "));
	}
	
	/**
	 * Test avec une erreur attendue dans le cas d'une année
	 * avec un caractère qui n'est pas un digit
	 */
	@Test
	public void testAvecCaractere() {
		assertFalse(verifAnnee.isYearNotNullAndValide("21o3"));
	}
	
	/**
	 * Test de validité et que la valeur est correcte
	 */
	@Test
	public void testValiditeAnneeOK() {
		VerificateurAnnee verifAnnee = new VerificateurAnnee();
		assertTrue("Ce test sur l'année doit fonctionner", verifAnnee.isAnneeValide("2000", 2013));
		assertTrue("Ce test sur l'année doit fonctionner car égal à l'année", verifAnnee.isAnneeValide("2013", 2013));
	}
	
	@Test
	public void testValiditeAnneePasOK() {
		VerificateurAnnee verifAnnee = new VerificateurAnnee();
		assertFalse("Ce test sur l'année ne doit pas fonctionner", verifAnnee.isAnneeValide("2025", 2013));
		assertFalse("L'année ne contient pas que des digits", verifAnnee.isAnneeValide("2O13", 2013));
	}
	
	
	
}
