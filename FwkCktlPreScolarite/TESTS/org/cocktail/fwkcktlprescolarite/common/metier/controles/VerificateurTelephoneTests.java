package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class VerificateurTelephoneTests {
	
	private VerificateurTelephone verifTel = null;
	
	/**
	 * Méthode setUp pour l'initialisation avant de faire n'importe quel test
	 */
	@Before
	public void setUp() {
		verifTel = new VerificateurTelephone();
		
	}

	/**
	 * Méthode tearDown pour terminer tous les tests
	 */
	@After
	public void tearDown() {
		verifTel = null;
	}
	
	
	/**
	 * Vérifications de base sur les numéros de téléphones
	 */
	@Test
	public void ControleDesTelephones(){
		String numTel = "0506070809";
		Integer indicatif = new Integer("33");
		
		assertTrue("Ce test doit fonctionner", verifTel.isPhoneNumber(numTel));
		assertTrue("Ce doit être un numéro français", verifTel.isFrenchPhoneLength(numTel, indicatif));
		
		numTel = "05.06.07.08.09";
		assertFalse("Ce test ne doit pas fonctionner", verifTel.isPhoneNumber(numTel));
		assertFalse("Ce doit être un numéro français", verifTel.isFrenchPhoneLength(numTel, indicatif));
		
		indicatif = new Integer("49");
		assertFalse("Ce doit être un numéro français", verifTel.isFrenchPhoneLength(numTel, indicatif));
	}
	
	
	/**
	 * Test du nettoyage des numéros de téléphones
	 */
	@Test
	public void NettoyageDesSeparateurs(){
		String numTel = "05.06.07.08.09";
		
		
		numTel = verifTel.phoneNumberWithOnlyDigits(numTel);
		assertTrue("Ce test doit fonctionner", verifTel.isPhoneNumber(numTel));
		
		numTel = "05 06 07 08 09";
		numTel = verifTel.phoneNumberWithOnlyDigits(numTel);
		assertTrue("Ce test doit fonctionner", verifTel.isPhoneNumber(numTel));
		
		numTel = "05-06-07-08-09";
		numTel = verifTel.phoneNumberWithOnlyDigits(numTel);
		assertTrue("Ce test doit fonctionner", verifTel.isPhoneNumber(numTel));
		
		numTel = "05/06/07/08/09";
		numTel = verifTel.phoneNumberWithOnlyDigits(numTel);
		assertTrue("Ce test doit fonctionner", verifTel.isPhoneNumber(numTel));
		
	}

}
