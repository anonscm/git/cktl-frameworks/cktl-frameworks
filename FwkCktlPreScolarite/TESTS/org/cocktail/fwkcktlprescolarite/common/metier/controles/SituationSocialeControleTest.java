/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IProfession;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOQuotiteTravail;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOSituationProfessionnelle;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.rules.MockEditingContext;

/**
 * Classe qui sert pour les test unitaires sur la situation sociale
 * @author isabelle
 */
@RunWith(MockitoJUnitRunner.class)
public class SituationSocialeControleTest {

	private SituationSocialeControle verifSituSoc = null;

	@Rule
	public MockEditingContext ec = new MockEditingContext("FwkCktlPersonne");

	/**
	 * Méthode setUp pour l'initialisation avant de faire n'importe quel test
	 */
	@Before
	public void setUp() {
		verifSituSoc = new SituationSocialeControle(new ControlesContexte());
	}

	/**
	 * Méthode tearDown pour terminer tous les tests
	 */
	@After
	public void tearDown() {
		verifSituSoc = null;
	}

	/**
	 * Vérification que le contrôle su le nom et prénom tuteur 1 fonctionne
	 */
	@Test
	public void testNomPrenomTuteur1OK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur1()).thenReturn("DUPOND Jacques");

		assertTrue("ce test doit fonctionner pour tuteur 1", verifSituSoc.controleNomPrenomTuteur1Ok(preEtudiantAnnee));

	}

	/**
	 * Vérification que le contrôle su le nom et prénom tuteur 1 fonctionne pas
	 */
	@Test
	public void testNomPrenomTuteur1PasOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur1()).thenReturn("DUPOND %Jacq2ues");

		assertFalse("ce test ne doit pas fonctionner pour tuteur 1 caractères interdits", verifSituSoc.controleNomPrenomTuteur1Ok(preEtudiantAnnee));

		when(preEtudiantAnnee.tuteur1()).thenReturn("");
		assertFalse("ce test ne doit pas fonctionner pour tuteur 1 , vide", verifSituSoc.controleNomPrenomTuteur1Ok(preEtudiantAnnee));

	}

	/**
	 * Verification quele controle sur la professin du tuteur 1 fonctionne
	 */
	@Test
	public void testControleProfessionTuteur1Ok() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur1()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiant.toProfession1()).thenReturn(profession);
		when(preEtudiantAnnee.toProfession1()).thenReturn(profession);

		assertTrue("ce test doit fonctionner pour tuteur 1", verifSituSoc.controleProfessionTuteur1Ok(preEtudiant, preEtudiantAnnee));

	}

	/**
	 * Verification que le controle sur la profession du tuteur 1 ne fonctionne pas pas de profession dans preEtudiantAnnee
	 */
	@Test
	public void testControleProfessionTuteur1PasOk() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur1()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiant.toProfession1()).thenReturn(profession);

		assertFalse("ce test ne doit pas fonctionner pour tuteur 1", verifSituSoc.controleProfessionTuteur1Ok(preEtudiant, preEtudiantAnnee));
	}

	/**
	 * Verification que le controle sur la profession du tuteur 1 ne fonctionne pas pas de profession dans preEtudiant
	 */
	@Test
	public void testControleProfessionTuteur1PasOkBis() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur1()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiantAnnee.toProfession1()).thenReturn(profession);

		assertFalse("ce test ne doit pas fonctionner pour tuteur 1", verifSituSoc.controleProfessionTuteur1Ok(preEtudiant, preEtudiantAnnee));
	}

	/**
	 * Vérification que le contrôle su le nom et prénom tuteur 2 fonctionne
	 */
	@Test
	public void testNomPrenomTuteur2OK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur2()).thenReturn("DUPOND Jacques");

		assertTrue("ce test doit fonctionner pour tuteur 2", verifSituSoc.controleNomPrenomTuteur2Ok(preEtudiantAnnee));

	}

	/**
	 * Vérification que le contrôle su le nom et prénom tuteur 2f onctionne pas
	 */
	@Test
	public void testNomPrenomTuteur2PasOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur2()).thenReturn("DUPOND %Jacq2ues");

		assertFalse("ce test ne doit pas fonctionner pour tuteur 2", verifSituSoc.controleNomPrenomTuteur2Ok(preEtudiantAnnee));

	}

	/**
	 * Verification quele controle sur la professin du tuteur 2 fonctionne
	 */
	@Test
	public void testControleProfessionTuteur2Ok() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur2()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiant.toProfession2()).thenReturn(profession);
		when(preEtudiantAnnee.toProfession2()).thenReturn(profession);

		assertTrue("ce test doit fonctionner pour tuteur 2", verifSituSoc.controleProfessionTuteur2Ok(preEtudiant, preEtudiantAnnee));

	}

	/**
	 * Verification que le controle sur la profession du tuteur 2 ne fonctionne pas pas de profession dans preEtudiantAnnee
	 */
	@Test
	public void testControleProfessionTuteur2PasOk() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur2()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiant.toProfession2()).thenReturn(profession);

		assertFalse("ce test ne doit pas fonctionner pour tuteur 2", verifSituSoc.controleProfessionTuteur2Ok(preEtudiant, preEtudiantAnnee));
	}

	/**
	 * Verification que le controle sur la profession du tuteur 2 ne fonctionne pas pas de profession dans preEtudiant
	 */
	@Test
	public void testControleProfessionTuteur2PasOkBis() {
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		IPreEtudiantAnnee preEtudiantAnnee = mock(IPreEtudiantAnnee.class);
		when(preEtudiantAnnee.tuteur2()).thenReturn("DUPOND Jacques");
		IProfession profession = mock(IProfession.class);
		when(preEtudiantAnnee.toProfession2()).thenReturn(profession);

		assertFalse("ce test ne doit pas fonctionner pour tuteur 2", verifSituSoc.controleProfessionTuteur2Ok(preEtudiant, preEtudiantAnnee));
	}

	/**
	 * Vérification que le contrôle sur le motif de non affiliation est rempli
	 */
	@Test
	public void testNonaffilieSecuOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		EOCotisation cotis = mock(EOCotisation.class);
		when(preEtudiantAnnee.affSs()).thenReturn(false);
		when(preEtudiantAnnee.toCotisation()).thenReturn(cotis);

		assertTrue("ce test doit fonctionner pour non affiliation", verifSituSoc.controleNonAffilieSecu(preEtudiantAnnee));

	}

	/**
	 * Vérification que le contrôle sur le motif de non affiliation est rempli
	 */
	@Test
	public void testNonaffilieSecuPasOK() {
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		when(preEtudiantAnnee.affSs()).thenReturn(false);

		assertFalse("ce test doit  pas fonctionner pour non affiliation , motif null", verifSituSoc.controleNonAffilieSecu(preEtudiantAnnee));

	}

	/**
	 * Vérification que le contrôle sur l' affiliation est rempli
	 */
	@Test
	public void testAffilieSecuOK() {
//		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
//		EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
//		EOPreEtudiantAnnee preEtudiantAnnee = EOPreEtudiantAnnee.creer(ec, 12, preEtudiant, 2013);
//
//		EOCotisation cotis = EOCotisation.createSco_Cotisation(ec, EOCotisation.COTI_CODE_NON, new Integer(2), new NSTimestamp(), new Integer(2008),
//		    "PLUS DE 28 ANS", 12, "O");
//		EOMutuelle mutuelle = EOMutuelle.createSco_Mutuelle(ec, 19748, new NSTimestamp(), "601", "O", 12);
//		preEtudiantAnnee.setAffSs(true);
//		preEtudiantAnnee.setToCotisationRelationship(cotis);
//		preEtudiantAnnee.setToMutuelleRelationship(mutuelle);
//
//		assertTrue("ce test doit fonctionner pour affiliation et coti code non", verifSituSoc.controleAffilieSecu(preEtudiantAnnee));
//
//		cotis.setCotiCode(EOCotisation.COTI_CODE_OUI);
//		assertTrue("ce test doit fonctionner pour affiliation et coti code oui", verifSituSoc.controleAffilieSecu(preEtudiantAnnee));
//
//		cotis.setCotiCode(EOCotisation.COTI_CODE_OUI_MAIS_EXONERE);
//		preEtudiantAnnee.setTypeRegime("mon regime");
//		assertTrue("ce test doit fonctionner pour affiliation et coti code oui mais exonere", verifSituSoc.controleAffilieSecu(preEtudiantAnnee));

	}

	/**
	 * Vérification que le contrôle sur l' affiliation est rempli
	 */
	@Test
	public void testAffilieSecuPasOK() {
//		EOPreEtudiantService etudiantService = new EOPreEtudiantService();
//		EOPreEtudiant preEtudiant = etudiantService.creerNouveauPreEtudiantPourPreInscription(ec, 12, "2013");
//		EOPreEtudiantAnnee preEtudiantAnnee = EOPreEtudiantAnnee.creer(ec, 12, preEtudiant, 2013);
//
//		EOCotisation cotis = EOCotisation.createSco_Cotisation(ec, EOCotisation.COTI_CODE_NON, new Integer(2), new NSTimestamp(), new Integer(2008),
//		    "PLUS DE 28 ANS", 12, "O");
//		EOMutuelle mutuelle = EOMutuelle.createSco_Mutuelle(ec, 19748, new NSTimestamp(), "601", "O", 12);
//		preEtudiantAnnee.setAffSs(true);
//		preEtudiantAnnee.setToCotisationRelationship(cotis);
//		preEtudiantAnnee.setToMutuelleRelationship(null);
//
//		assertFalse("ce test ne doit pas fonctionner pour affiliation et coti code non", verifSituSoc.controleAffilieSecu(preEtudiantAnnee));
//
//		preEtudiantAnnee.setToMutuelleRelationship(mutuelle);
//		cotis.setCotiCode(EOCotisation.COTI_CODE_OUI_MAIS_EXONERE);
//		preEtudiantAnnee.setTypeRegime(null);
//		assertFalse("ce test ne doit pasfonctionner pour affiliation et coti code oui mais exonere", verifSituSoc.controleAffilieSecu(preEtudiantAnnee));

	}

	//attention le controle n'est pas le meme que dans le cadre A
	@Test
	public void testControleSituationProfessionnelleCoherente() {
		// Arrange
		EOPreEtudiantAnnee preEtudiantAnnee = mock(EOPreEtudiantAnnee.class);
		EOSituationProfessionnelle situationProfessionnelle = mock(EOSituationProfessionnelle.class);
		EOQuotiteTravail quotiteTravail = mock(EOQuotiteTravail.class);

		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(situationProfessionnelle);
		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(quotiteTravail);

		// Assert
		//situaion salarié
		when(situationProfessionnelle.codeSitProf()).thenReturn(EOSituationProfessionnelle.CODE_SITPROF_SALARIE);
		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_TEMPS_COMPLET);
		assertTrue("étudiant salarié et quotité de travail à temps complet", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));

		
//		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_SANS_OBJET);
//		assertFalse("étudiant salarié et quotité de travail sans objet", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
		
//		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(null);
//		assertFalse("étudiant salarié et quotité de travail null", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
		
		//sans situation
//		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(null);
//		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(quotiteTravail);		
//		assertTrue("étudiant sans situation professionnelle", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
//		
//		when(preEtudiantAnnee.toSituationProfessionnelle()).thenReturn(situationProfessionnelle);
//		when(situationProfessionnelle.codeSitProf()).thenReturn(EOSituationProfessionnelle.CODE_SITPROF_MILITAIRE);
//		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_TEMPS_COMPLET);
//		assertTrue("étudiant militaie", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));				
//		
//		when(situationProfessionnelle.codeSitProf()).thenReturn(EOSituationProfessionnelle.CODE_SITPROF_FONCTIONNAIRE);
//		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_ACTIVITE_SALARIEE_INFERIEURE_60H);
//		assertTrue("étudiant fonctionnaire et quotité de travail à temps complet", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
//		
//		when(quotiteTravail.qtraCode()).thenReturn(EOQuotiteTravail.QTRA_CODE_SANS_OBJET);
//		assertFalse("étudiant salarié et quotité de travail sans objet", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));
//		
//		when(preEtudiantAnnee.toQuotiteTravail()).thenReturn(null);
//		assertFalse("étudiant salarié et quotité de travail null", verifSituSoc.controleSituationProfessionnelleCoherente(preEtudiantAnnee));

	}
}
