/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, ${year} This software
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as
 * circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability. In this
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.fail;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * Classe de Test Unitaires sur les Booleéns
 * @author alainmalaplate
 *
 */
public class VerificateurBooleenTests {

	private VerificateurBooleen verifBooleen = null;
	
	/**
	 * Méthode setUp pour l'initialisation avant de faire n'importe quel test
	 */
	@Before
	public void setUp() {
		verifBooleen = new VerificateurBooleen();
	}

	/**
	 * Méthode tearDown pour terminer tous les tests
	 */
	@After
	public void tearDown() {
		verifBooleen = null;
	}
	
	/**
	 * la méthode controleDesNoms procèdent à une série de tests
	 * qui doivent fonctionner. S'il y a une erreur, c'est que le code à changer !
	 */
	@Test
    public void controleDesBooleens() {
        
//		Boolean objetATester;
		
		try {
			verifBooleen.controleDeNonNullite(Boolean.TRUE, "Booléen de test à true");
//			objetATester = new Boolean("NON");
//			verifBooleen.controleTrueouFalse(objetATester, "Test de non conformité avec True ou False");
//			objetATester = new Boolean("OUI");
//			verifBooleen.controleTrueouFalse(objetATester, "Test de non conformité avec True ou False");
			
		} catch (MessageErreur e) {
			fail("La validation ne devrait pas lever d'exception pour un élément valide");
		}
    }
	
	/**
	 * Test avec une erreur attendue dans le cas d'un booléen
	 * qui est nul 
	 */
	@Test(expected = MessageErreur.class)
	public void testNonValiderNul() {
		verifBooleen.controleDeNonNullite(null, "Test de nullité");	
	}
	
//	/**
//	 * Test avec une erreur attendue dans le cas d'un booléen
//	 * qui n'a pas de valeur True ou False 
//	 */
//	@Test(expected = MessageErreur.class)
//	public void testNonValiderSiNonTrueOuFalse() {
//		verifBooleen.controleTrueouFalse(null, "Test de non conformité avec True ou False");
////		Boolean objetATester = new Boolean("Perdu!");
////		verifBooleen.controleTrueouFalse(objetATester, "Test de non conformité avec True ou False");
//	}
}
