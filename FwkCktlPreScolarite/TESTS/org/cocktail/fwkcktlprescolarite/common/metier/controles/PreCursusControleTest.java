package org.cocktail.fwkcktlprescolarite.common.metier.controles;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlacces.common.network.IP;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreCursus;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.junit.Test;

/**
 * Classe  qui sert pour les test unitaires sur  le cadre K, renseignements divers 
 * @author isabelle
 *
 */
public class PreCursusControleTest {
//	@Rule
//	public MockEditingContext ec = new MockEditingContext("PreScolarite");
	

	@Test
	public void controleDatePreCursus() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2009);
		when(preCursus.anneeFin()).thenReturn(2010);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertTrue(controle.controleDatePreCursus(preCursus, 2013));
		assertEquals(0, controle.getContexte().getListeErreurs().size());
	}

	@Test
	public void controleDatePreCursusInvalide() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2010);
		when(preCursus.anneeFin()).thenReturn(2015);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleDatePreCursus(preCursus, 2013));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.ANNEE_FIN_NON_VALIDE, controle.getContexte().getCodeErreurs().get(0));
	}
	
	@Test
	public void controleDatePreCursusPeriodeInvalide() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2014);
		when(preCursus.anneeFin()).thenReturn(2015);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleDatePreCursus(preCursus, 2013));
		assertEquals(2, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.ANNEE_DEBUT_NON_VALIDE, controle.getContexte().getCodeErreurs().get(0));
		assertEquals(PreCursusControle.ANNEE_FIN_NON_VALIDE, controle.getContexte().getCodeErreurs().get(1));
	}
	
	@Test
	public void controleDatePreCursusFinInferieurADebut() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2010);
		when(preCursus.anneeFin()).thenReturn(2008);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleDatePreCursus(preCursus, 2013));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.ANNEE_DEBUT_SUPERIEURE_ANNEE_FIN, controle.getContexte().getCodeErreurs().get(0));

	}
	
	@Test
	public void controleDatePreCursusDebutInferieurSiecle() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(1899);
		when(preCursus.anneeFin()).thenReturn(2008);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleDatePreCursus(preCursus, 2013));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.ANNEE_DEBUT_NON_VALIDE, controle.getContexte().getCodeErreurs().get(0));

	}
	
	@Test
	public void controleSiPasInteruptionsurPeriode() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2008);
		when(preCursus.anneeFin()).thenReturn(2009);
		when(preCursus.interruptionEtud()).thenReturn(true);

		EOPreCursus preCursusNew = mock(EOPreCursus.class);
		when(preCursusNew.anneeDebut()).thenReturn(2009);
		when(preCursusNew.anneeFin()).thenReturn(2010);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertTrue(controle.controleSiPasInteruptionsurPeriode(preCursusNew, preCursus));
		assertEquals(0, controle.getContexte().getListeErreurs().size());

	}
	
	@Test
	public void controleDateDebutDansInteruptionsurPeriode() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2008);
		when(preCursus.anneeFin()).thenReturn(2010);
		when(preCursus.interruptionEtud()).thenReturn(true);

		EOPreCursus preCursusNew = mock(EOPreCursus.class);
		when(preCursusNew.anneeDebut()).thenReturn(2009);
		when(preCursusNew.anneeFin()).thenReturn(2011);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleSiPasInteruptionsurPeriode(preCursusNew, preCursus));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.PERIODE_INTERRUPTION_SUR_CETTE_PERIODE, controle.getContexte().getCodeErreurs().get(0));
	}
	
	@Test
	public void controleDateFinEgaleDateDebutInteruptionsurPeriode() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2008);
		when(preCursus.anneeFin()).thenReturn(2010);
		when(preCursus.interruptionEtud()).thenReturn(true);

		EOPreCursus preCursusNew = mock(EOPreCursus.class);
		when(preCursusNew.anneeDebut()).thenReturn(2007);
		when(preCursusNew.anneeFin()).thenReturn(2008);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertTrue(controle.controleSiPasInteruptionsurPeriode(preCursusNew, preCursus));
		assertEquals(0, controle.getContexte().getListeErreurs().size());
	}
	
	@Test
	public void controleDateFinDansInteruptionsurPeriode() {
		EOPreCursus preCursus = mock(EOPreCursus.class);
		when(preCursus.anneeDebut()).thenReturn(2008);
		when(preCursus.anneeFin()).thenReturn(2010);
		when(preCursus.interruptionEtud()).thenReturn(true);

		EOPreCursus preCursusNew = mock(EOPreCursus.class);
		when(preCursusNew.anneeDebut()).thenReturn(2007);
		when(preCursusNew.anneeFin()).thenReturn(2009);
		
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		assertFalse(controle.controleSiPasInteruptionsurPeriode(preCursusNew, preCursus));
		assertEquals(1, controle.getContexte().getListeErreurs().size());
		assertEquals(PreCursusControle.PERIODE_INTERRUPTION_SUR_CETTE_PERIODE, controle.getContexte().getCodeErreurs().get(0));
	}
	
	@Test
	public void controleChampEtablissementSaisiCasEtranger() {
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		ICursus cursus = mockCursus(true);
		boolean isOk = controle.controleChampEtablissement(cursus);
		
		assertTrue("L'établissement doit être saisi", isOk);
	}
	
	@Test
	public void controleChampEtablissementNonSaisiCasEtranger() {
		PreCursusControle controle = new PreCursusControle(new ControlesContexte());
		ICursus cursus = mockCursus(false);
		boolean isOk = controle.controleChampEtablissement(cursus);
		
		assertTrue("L'établissement n'est pas saisi, un message d'erreur apparait", !isOk);
		assertEquals(PreCursusControle.ETABLISSEMENT_VIDE, controle.getContexte().getCodeErreurs().get(0));
	}

	private ICursus mockCursus(boolean saisi) {
		ICursus cursus = mock(ICursus.class);
		IPays pays = mock(IPays.class);
		when(pays.cPays()).thenReturn("103");
		when(cursus.interruptionEtud()).thenReturn(false);
		when(cursus.toPays()).thenReturn(pays);
		if (saisi) {
			when(cursus.etabEtranger()).thenReturn("FIF");
		}
		return cursus;
	}
	
}
