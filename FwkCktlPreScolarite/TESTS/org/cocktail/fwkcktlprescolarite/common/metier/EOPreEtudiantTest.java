package org.cocktail.fwkcktlprescolarite.common.metier;

import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlpersonne.common.metier.EOCivilite;
import org.cocktail.fwkcktlpersonne.common.metier.EOProfession;
import org.cocktail.fwkcktlpersonne.common.metier.EOSituationFamiliale;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypePaiement;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

/**
 * Tests de la classe EOPreEtudiant : <br/> 
 * - tests basiques des contraintes sur les attributs (not null) <br/>
 * Remarque : les contraintes étant souples niveau modèle, ces tests sont minimaux.
 *  cf classe PreEtudiantControleTest pour des tests plus poussés
 * 
 * @author Alexis Tual
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EOPreEtudiantTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("PreScolarite");
	
	@UnderTest
	private EOPreEtudiant preEtudiant;
	
	@Dummy
	private EOPreEtudiantAnnee preEtudiantAnnee;
	
	@Dummy
	private EOCivilite civilite;
	
	@Dummy
	private EOProfession prof1;
	
	@Dummy
	private EOSituationFamiliale situationFamiliale;
	
	@Before
	public void initBonPreEtudiantAnnee() {
		preEtudiant = EOPreEtudiant.creer(ec, 999);
		//preEtudiant.setAnnee1InscEtab(2010);
		//preEtudiant.setBacCode("S");
		preEtudiant.setToCiviliteRelationship(civilite);
		preEtudiant.setToSituationFamilialeRelationship(situationFamiliale);
		preEtudiant.setDCreation(new NSTimestamp());
		preEtudiant.setEnfantsCharge(true);
		// VERIFIER LA REGLE METIER SUR L'OBLIGATION DES INE
		preEtudiant.setIneProvisoire(false);
		preEtudiant.setInseeProvisoire(false);
		preEtudiant.setNomUsuel("Martin");
		preEtudiant.setPersIdCreation(999);
		preEtudiant.setPersIdModification(999);
		// PHOTO CHANGER LA MODELISATION
		preEtudiant.setIndPhoto("N");
		preEtudiant.setPrenom("Jojo");
		// QU'EST-CE-DONC ? CREER PEUT ETRE UNE TABLE DE REF
		preEtudiant.setPreType("F");
		preEtudiant.setToProfession1Relationship(prof1);
		preEtudiant.setSportHN(false);
		// CHANGER LA MODELISATION, CREER UNE TABLE DE REF
		preEtudiant.setTypeInscription("R");
		
		preEtudiantAnnee.setAnnee(2014);
		preEtudiant.addToToPreEtudiantAnneeRelationship(preEtudiantAnnee);
	}
	
	@Test
	public void testEnregistrementMinimal() {
		//confirm(preEtudiant, canBeSaved());
	}
	
	@Test
	public void testVaPayerParVirement() {
	    EOPrePaiement prePaiement = ec.createSavedObject(EOPrePaiement.class);
	    EOPrePaiementMoyen preMoyenPaiement = ec.createSavedObject(EOPrePaiementMoyen.class);
	    EOTypePaiement typePaiement = ec.createSavedObject(EOTypePaiement.class);
	    typePaiement.setCode(ITypePaiement.CODE_TYPE_DIFFERE);
	    prePaiement.setPaiementValide(true);
	    prePaiement.setOrdre(1);
	    preMoyenPaiement.setToPrePaiementRelationship(prePaiement);
	    preMoyenPaiement.setToTypePaiementRelationship(typePaiement);
	    preEtudiantAnnee.addToToPrePaiementsRelationship(prePaiement);
	    
	    assertTrue(preEtudiant.vaPayerParVirement(2014));
	}
	
}
