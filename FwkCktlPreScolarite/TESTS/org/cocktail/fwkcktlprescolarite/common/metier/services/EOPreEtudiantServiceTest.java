package org.cocktail.fwkcktlprescolarite.common.metier.services;

import static org.fest.assertions.api.Assertions.assertThat;

import java.util.Calendar;
import java.util.List;

import org.cocktail.fwkcktldroitsutils.common.metier.EOFonction;
import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktldroitsutils.common.metier.EOTypeApplication;
import org.cocktail.fwkcktldroitsutils.common.metier.EOTypeEtat;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur;
import org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateurFonction;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOBanque;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOFournis;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EORib;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiantAnnee;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EODiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOModePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOStatutFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.TestsInitializer;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.FwkCktlScolPedaTestUtils.DummySequence;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;

@RunWith(value=MockitoJUnitRunner.class)
public class EOPreEtudiantServiceTest extends EOScolTestCase {

	
	private Integer year = Calendar.getInstance().get(Calendar.YEAR);

	private EODiplome diplomeSimple; 
	private EODiplome diplomeAvecSpecialite; 
	
	@Dummy
	private EOIndividu individu;
	
	@Dummy
	private EOEtudiant etudiant;

	@Dummy
	private EOEtudiantAnnee etudiantAnnee;

	@Dummy
	private EOPreInscription preInscription;
	
	@Dummy
	private EOPersonne personne;
	
	@Dummy
	private EOUtilisateur utilisateur;
	
	private PersonneApplicationUser personneApplicationUser;
	
	@Spy
	private EOPreEtudiantService preEtudiantService;
	
	private DummySequence sequenceLiens;
	
	private EOTypeLien versionner;
	
	private EOTypePeriode annee;
	private EOTypePeriode semestre;

	private EOStatutFormation statutFormation;
	
	private EOParcours specialite1;
	private EOParcours parcoursAnnee;
	
	private TestsInitializer testsInitiator;
	@Dummy
	private EOTypeEtudiant typeEtudiant;
	
	// EOGradeUniversitaire
	private EOGradeUniversitaire gradeUniversitaireMaster;
	
	// EOTypeFormation
	private EOTypeFormation typeFormationMaster;
		
	
	@Before
	public void setUp() {
		super.setUp();
        UserInfo userInfo = Mockito.mock(UserInfo.class);
        Mockito.when(userInfo.persId()).thenReturn(new Integer(1));
        preEtudiantService.setUserInfo(userInfo);
		
		sequenceLiens = new DummySequence();
		
		gradeUniversitaireMaster = EOTestsHelper.creerGradeUniversitaire(editingContext, "M", "master", 0);
		
		typeFormationMaster = EOTestsHelper.creerTypeFormation(editingContext, "MAS", "Master");
		
		init();
		creerDiplomeSimple();
		creerDiplomeAvecSpecialiteEtParcours();
		etudiant.setToIndividuRelationship(individu);
	
		preInscription.setNiveau(1);
		personne.setPersId(1);
		personneApplicationUser  = new PersonneApplicationUser(editingContext, 1);
		EOTypeEtat etatValide = editingContext.createSavedObject(EOTypeEtat.class);
		etatValide.setTyetLibelle(EOTypeEtat.ETAT_VALIDE);
		utilisateur = editingContext.createSavedObject(EOUtilisateur.class);
		utilisateur.setToTypeEtatRelationship(etatValide);
		utilisateur.setToPersonneRelationship(personne);
		utilisateur.setUtlOuverture(new NSTimestamp());
		
		
		EOTypeApplication typeApp = editingContext.createSavedObject(EOTypeApplication.class);
		typeApp.setTyapStrid(PersonneApplicationUser.TYAP_STR_ID_ANNUAIRE);
		EOFonction fonctionCreationFournis = editingContext.createSavedObject(EOFonction.class);
		fonctionCreationFournis.setFonIdInterne(PersonneApplicationUser.FON_ID_CREATION_FOURNISSEUR);
		fonctionCreationFournis.setToTypeApplicationRelationship(typeApp);
		fonctionCreationFournis.setFonSpecExercice("N");
		EOUtilisateurFonction utilisateurFonction = editingContext.createSavedObject(EOUtilisateurFonction.class);
		utilisateurFonction.setToFonctionRelationship(fonctionCreationFournis);
		utilisateurFonction.setToUtilisateurRelationship(utilisateur);
	}
	
	private void creerDiplomeSimple() {
		diplomeSimple = (EODiplome) EOTestsHelper.creerDiplomeSimple(testsInitiator, sequenceLiens, year).getDiplome();
	}


	private void creerDiplomeAvecSpecialiteEtParcours() {
		
		diplomeAvecSpecialite = EOTestsHelper.creerDiplome(editingContext, "DIPSPE", "DIPLOME AVEC SPECIALITE", statutFormation, gradeUniversitaireMaster, typeFormationMaster, year);
		
		EOVersionDiplome vd = EOTestsHelper.creerVersionDiplome(editingContext, "VD1", "Version Diplome 1", year);
		
		EOTestsHelper.creerLien(editingContext, sequenceLiens.getNextVal(), diplomeAvecSpecialite, vd, versionner);
		
		specialite1 = EOTestsHelper.creerParcours(editingContext, "SPE1", "Specialite 1");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), vd, specialite1, false);
		
		EOParcours specialite2 = EOTestsHelper.creerParcours(editingContext, "SPE2", "Specialite 2");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), vd, specialite2, false);

		
		EOPeriode licence1 = EOTestsHelper.creerPeriode(editingContext, "L1", "Licence 1", annee, 1);
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), specialite1, licence1, false);
		
		EOPeriode licence2 = EOTestsHelper.creerPeriode(editingContext, "L2", "Licence 2", annee, 2);
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), specialite1, licence2, false);
		
		parcoursAnnee = EOTestsHelper.creerParcours(editingContext, "PAR1", "Parcours annee 1");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), licence1, parcoursAnnee, false);

		EOPeriode semestre1 = EOTestsHelper.creerPeriode(editingContext, "S1", "Semestre 1", semestre, 1);
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), parcoursAnnee, semestre1, false);
		
		EOPeriode semestre2 = EOTestsHelper.creerPeriode(editingContext, "S2", "Semestre 2", semestre, 2);
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), parcoursAnnee, semestre2, false);

		EOParcours parcoursSemestre = EOTestsHelper.creerParcours(editingContext, "PARSEM1", "Parcours semestre 1");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), semestre1, parcoursSemestre, true);

		EOEC ec1 = EOTestsHelper.creerEC(editingContext, "EC1", "Ec 1");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), parcoursSemestre, ec1, true);
		
		EOEC ec2 = EOTestsHelper.creerEC(editingContext, "EC2", "Ec 2");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), parcoursSemestre, ec2, true);
		
		EOEC ec3 = EOTestsHelper.creerEC(editingContext, "EC3", "Ec 3");
		EOTestsHelper.creerLienComposer(editingContext, sequenceLiens.getNextVal(), parcoursSemestre, ec3, false);
		
	}
	
	private void init() {
	    testsInitiator = new TestsInitializer(editingContext);
		versionner = testsInitiator.getTypeLienVersionner();
		annee = testsInitiator.getTypePeriodeAnnee();
		semestre = testsInitiator.getTypePeriodeSemestre();
		
		statutFormation = testsInitiator.getStatutFormation();
	}

	@Ignore
	@Test
	public void testCreerNouveauPreEtudiantPourPreInscription() {
		preInscription.setToDiplomeRelationship(diplomeSimple);

		preEtudiantService.creerIaPourPreinscription(editingContext, preInscription, null, etudiant,etudiantAnnee, year, personneApplicationUser, typeEtudiant, true);
		
		NSArray<EOInscription> ias = EOInscription.fetchAllSco_Inscriptions(editingContext);
		assertThat(ias.count()).isEqualTo(1);
		
		NSArray<EOInscriptionPedagogique> ips = EOInscriptionPedagogique.fetchAllSco_InscriptionPedagogiques(editingContext);
		assertThat(ips.count()).isEqualTo(1);
		
	
		
	}

	@Ignore
	@Test
	public void testCreerNouveauPreEtudiantPourPreInscriptionAvcSpecialite() {
		preInscription.setToDiplomeRelationship(diplomeAvecSpecialite);
		preInscription.setToParcoursDiplomeRelationship(specialite1);
		preInscription.setToParcoursAnneeRelationship(parcoursAnnee);
		preEtudiantService.creerIaPourPreinscription(editingContext, preInscription, null, etudiant,etudiantAnnee, year, personneApplicationUser, typeEtudiant, true);
		
		NSArray<EOInscription> ias = EOInscription.fetchAllSco_Inscriptions(editingContext);
		assertThat(ias.count()).isEqualTo(1);
		
		NSArray<EOInscriptionPedagogique> ips = EOInscriptionPedagogique.fetchAllSco_InscriptionPedagogiques(editingContext);
		assertThat(ips.count()).isEqualTo(1);
		
		NSArray<EOInscriptionPedagogiqueElement> elements = EOInscriptionPedagogiqueElement.fetchAllSco_InscriptionPedagogiqueElements(editingContext);
		assertThat(elements.count()).isEqualTo(8);
		
	}
	
	@Test
	public void testCreerAdresseFacturation() {
        EOTypeAdresse typeStable = editingContext.createSavedObject(EOTypeAdresse.class);
        typeStable.setTadrCode(EOTypeAdresse.TADR_CODE_PAR);
        
        EOTypeAdresse typeFacturation = editingContext.createSavedObject(EOTypeAdresse.class);
        typeFacturation.setTadrCode(EOTypeAdresse.TADR_CODE_FACT);
        
        
        EOAdresse adresseStable = editingContext.createSavedObject(EOAdresse.class);

        EORepartPersonneAdresse repart = editingContext.createSavedObject(EORepartPersonneAdresse.class);
        repart.setToTypeAdresseRelationship(typeStable);
        repart.setToAdresseRelationship(adresseStable);
        repart.setToPersonneRelationship(individu);
        
	    EOAdresse adresseFacturation = 
	            (EOAdresse) preEtudiantService.creerOuMiseAJourAdresseFacturation(individu, adresseStable);

	    assertThat(adresseFacturation).isNotNull();
	    
	    List<EORepartPersonneAdresse> reparts =
	            adresseFacturation.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_FACT);
	    
	    assertThat(adresseFacturation).isEqualTo(adresseStable);
	    assertThat(reparts).isNotEmpty();
	}
	
	@Test
	public void testMajAdresseFacturation() {
	    EOTypeAdresse typeStable = editingContext.createSavedObject(EOTypeAdresse.class);
	    typeStable.setTadrCode(EOTypeAdresse.TADR_CODE_PAR);
	    
	    EOTypeAdresse typeFacturation = editingContext.createSavedObject(EOTypeAdresse.class);
	    typeFacturation.setTadrCode(EOTypeAdresse.TADR_CODE_FACT);
	    
	    
	    EOAdresse adresseStable = editingContext.createSavedObject(EOAdresse.class);
	    adresseStable.setAdrAdresse1("RUE DU BAIN");
	    
	    EOAdresse adresseFacturationExistante = editingContext.createSavedObject(EOAdresse.class);
	    adresseFacturationExistante.setAdrAdresse1("RUE DES LOIS");
	    
	    EORepartPersonneAdresse repart = editingContext.createSavedObject(EORepartPersonneAdresse.class);
	    repart.setToTypeAdresseRelationship(typeStable);
	    repart.setToAdresseRelationship(adresseStable);
	    repart.setToPersonneRelationship(individu);
	    
	    EORepartPersonneAdresse repart2 = editingContext.createSavedObject(EORepartPersonneAdresse.class);
	    repart2.setToTypeAdresseRelationship(typeFacturation);
	    repart2.setToAdresseRelationship(adresseFacturationExistante);
	    repart2.setToPersonneRelationship(individu);
	    
	    EOAdresse adresseFacturation = 
	            (EOAdresse) preEtudiantService.creerOuMiseAJourAdresseFacturation(individu, adresseStable);
	    
	    assertThat(adresseFacturation).isNotNull();
	    
	    List<EORepartPersonneAdresse> reparts =
	            adresseFacturation.toRepartPersonneAdresses(EORepartPersonneAdresse.QUAL_PERSONNE_ADRESSE_FACT);

	    assertThat(reparts).isNotEmpty();
	    assertThat(adresseFacturation).isNotEqualTo(adresseStable);
	    assertThat(adresseFacturation).isEqualTo(adresseFacturationExistante);
	    assertThat(adresseFacturation.adrAdresse1()).isEqualTo(adresseStable.adrAdresse1());
	}
	
	/**
	 * Test impossible car loin dans le code du sql brut est appelé...
	 * @throws ImportException 
	 */
	@Ignore
	public void testCreerFournis() throws ImportException {
	    EOAdresse adresseFacturation = editingContext.createSavedObject(EOAdresse.class);
	    
	    preEtudiantService.creerOuMiseAJourFournisseur(editingContext, individu, adresseFacturation, EOFournis.FOU_TYPE_CLIENT);
	}
	
	@Test
	public void testCreerRib() throws ImportException {
	    final String IBAN = "FR703000200550000157845Z02";
	    final String BIC = "AGRIFRPP882";
	    
	    EOFournis fournis = editingContext.createSavedObject(EOFournis.class);
        EOPreEtudiant preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
        EOPrePaiementMoyen preMoyenPaiement = creerPrePaiementDiffere(IBAN, BIC, preEtudiant);

	    preEtudiantService.creerOuMiseAJourRib(editingContext, fournis, preMoyenPaiement);
	    
	    assertThat(fournis.toRibs()).hasSize(1);
	    assertThat(fournis.toRibs().lastObject().iban()).isEqualTo(IBAN);
	    assertThat(fournis.toRibs().lastObject().bic()).isEqualTo(BIC);
	}

	@Test
	public void testMajRib() throws ImportException {
	    final String IBAN_EXISTANT = "FR709999900550000157845Z02";
	    final String IBAN = "FR703000200550000157845Z02";
	    final String BIC = "AGRIFRPP882";
	    
	    EORib existingRib = editingContext.createSavedObject(EORib.class);
	    existingRib.setIban(IBAN_EXISTANT);
	    EOFournis fournis = editingContext.createSavedObject(EOFournis.class);
	    fournis.addToToRibsRelationship(existingRib);
	    EOPreEtudiant preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
	    EOPrePaiementMoyen preMoyenPaiement = creerPrePaiementDiffere(IBAN, BIC, preEtudiant);
	    
	    preEtudiantService.creerOuMiseAJourRib(editingContext, fournis, preMoyenPaiement);
	    
	    assertThat(fournis.toRibs()).hasSize(2);
	    List<String> ibans = (List<String>) fournis.toRibs().valueForKey(EORib.IBAN_KEY);
	    assertThat(ibans).contains(IBAN_EXISTANT);
	    assertThat(ibans).contains(IBAN);
	}
	
    private EOPrePaiementMoyen creerPrePaiementDiffere(final String IBAN, final String BIC, EOPreEtudiant preEtudiant) {
        EOPreEtudiantAnnee preEtudiantAnnee = editingContext.createSavedObject(EOPreEtudiantAnnee.class);
        preEtudiantAnnee.setAnnee(year);
        preEtudiantAnnee.setToPreEtudiantRelationship(preEtudiant);
        EOTypePaiement typePaiementDiffere = editingContext.createSavedObject(EOTypePaiement.class);
        typePaiementDiffere.setCode(EOTypePaiement.CODE_TYPE_DIFFERE);
        EOPrePaiement prePaiement = editingContext.createSavedObject(EOPrePaiement.class);
        prePaiement.setPaiementValide(true);
        prePaiement.setDatePaiement(new NSTimestamp());
        preEtudiantAnnee.addToToPrePaiementsRelationship(prePaiement);
        EOPrePaiementMoyen preMoyenPaiement = editingContext.createSavedObject(EOPrePaiementMoyen.class);
        preMoyenPaiement.setIban(IBAN);
        preMoyenPaiement.setBic(BIC);
        preMoyenPaiement.setNomTireur("Durand");
        preMoyenPaiement.setPrenomTireur("Jean-Loup");
        preMoyenPaiement.setToTypePaiementRelationship(typePaiementDiffere);
        preMoyenPaiement.setToPrePaiementRelationship(prePaiement);
        EOModePaiement modePaiement = editingContext.createSavedObject(EOModePaiement.class);
        modePaiement.setCode(EOModePaiement.CODE_MODE_PRELEVEMENT);
		preMoyenPaiement.setToModePaiementRelationship(modePaiement );
        EOBanque banque = editingContext.createSavedObject(EOBanque.class);
        banque.setCBanque("30002");
        banque.setCGuichet("00550");
        banque.setDomiciliation("TLSE");
        banque.setBic(BIC);
        return preMoyenPaiement;
    }
    
    @Test
    public void testGetIndividuPourPreEtudiant() {
    	
    	String ine = "12345678910";
    	String nom = "DURAND";
    	String prenomJacques = "JACQUES";
    	String prenomRaymonde = "RAYMONDE";
    	NSTimestamp dateDeNaissance = new NSTimestamp();
    	
    	// CAS N°1 : le preEtudiant est deja lié à un étudiant
    	EOIndividu individuPreEtudiant = editingContext.createSavedObject(EOIndividu.class);
    	individuPreEtudiant.setNomPatronymique(nom);
    	individuPreEtudiant.setPrenom(prenomJacques);
    	EOPreEtudiant preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
    	preEtudiant.setToIndividuEtudiantRelationship(individuPreEtudiant);
    	assertThat(preEtudiantService.getIndividuPourPreEtudiant(editingContext, preEtudiant, personneApplicationUser, false)).isEqualTo(individuPreEtudiant);
    	
    	// CAS N°2 : le preEtudiant et l'étudiant ont le meme INE
    	preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
    	individuPreEtudiant = editingContext.createSavedObject(EOIndividu.class);
    	individuPreEtudiant.setNomPatronymique(nom);
    	individuPreEtudiant.setPrenom(prenomRaymonde);
    	individuPreEtudiant.setDNaissance(dateDeNaissance);
    	EOEtudiant etudiant = editingContext.createSavedObject(EOEtudiant.class);
		preEtudiant.setCodeIne(ine);
    	etudiant.setEtudCodeIne(ine);
    	etudiant.setToIndividuRelationship(individuPreEtudiant);
    	assertThat(preEtudiantService.getIndividuPourPreEtudiant(editingContext, preEtudiant, personneApplicationUser, false)).isEqualTo(individuPreEtudiant);

		preEtudiant.setCodeIne(null);
    	assertThat(preEtudiantService.getIndividuPourPreEtudiant(editingContext, preEtudiant, personneApplicationUser, false)).isNotEqualTo(individuPreEtudiant);

    	// CAS N°3 : le preEtudiant et l'individu ont les meme caracteristiques (nom, prenom, date de naissance)
    	preEtudiant.setNomPatronymique(nom);
    	preEtudiant.setPrenom(prenomRaymonde);
    	preEtudiant.setDNaissance(dateDeNaissance);
    	etudiant.setToIndividuRelationship(null);
    	assertThat(preEtudiantService.getIndividuPourPreEtudiant(editingContext, preEtudiant, personneApplicationUser, false)).isEqualTo(individuPreEtudiant);
    	
    	
    	
    }
    
    @Test
    public void testGetEtudiantPourPreEtudiant() {
    	
    	String ine = "12345678910";
    	String nom = "DURAND";
    	String prenomJacques = "JACQUES";
    	String prenomRaymonde = "RAYMONDE";
    	NSTimestamp dateDeNaissance = new NSTimestamp();
    	Integer annee = 2014;
    	// CAS N°1 : le preEtudiant est deja lié à un étudiant
    	EOIndividu individuPreEtudiant = editingContext.createSavedObject(EOIndividu.class);
    	individuPreEtudiant.setNomPatronymique(nom);
    	individuPreEtudiant.setPrenom(prenomJacques);
    	EOPreEtudiant preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
    	EOEtudiant etudiant = editingContext.createSavedObject(EOEtudiant.class);
    	preEtudiant.setToEtudiantRelationship(etudiant);
    	assertThat(preEtudiantService.getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, null, null, false)).isEqualTo(etudiant);
    	
    	// CAS N°2 : le preEtudiant et l'étudiant ont le meme INE
    	preEtudiant = editingContext.createSavedObject(EOPreEtudiant.class);
    	individuPreEtudiant = editingContext.createSavedObject(EOIndividu.class);
    	individuPreEtudiant.setNomPatronymique(nom);
    	individuPreEtudiant.setPrenom(prenomRaymonde);
    	individuPreEtudiant.setDNaissance(dateDeNaissance);
    	etudiant = editingContext.createSavedObject(EOEtudiant.class);
		preEtudiant.setCodeIne(ine);
    	etudiant.setEtudCodeIne(ine);
    	etudiant.setToIndividuRelationship(individuPreEtudiant);
    	assertThat(preEtudiantService.getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, null, null, false)).isEqualTo(etudiant);

		preEtudiant.setCodeIne(null);
    	assertThat(preEtudiantService.getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, null, null, false)).isNotEqualTo(etudiant);

    	// CAS N°3 : le preEtudiant et l'étudiant ont les meme caracteristiques (nom, prenom, date de naissance)
    	preEtudiant.setNomPatronymique(nom);
    	preEtudiant.setPrenom(prenomRaymonde);
    	preEtudiant.setDNaissance(dateDeNaissance);
    	assertThat(preEtudiantService.getEtudiantPourPreEtudiant(preEtudiant, editingContext, annee, null, null, false)).isEqualTo(etudiant);
    	
    	
    	
    }
    
}
