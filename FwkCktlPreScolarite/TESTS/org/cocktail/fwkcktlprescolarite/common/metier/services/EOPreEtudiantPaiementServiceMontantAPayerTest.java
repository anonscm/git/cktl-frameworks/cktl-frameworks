package org.cocktail.fwkcktlprescolarite.common.metier.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlprescolarite.common.metier.IPreBourses;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCotisation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParametragePaieFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeArticleComplementaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeBoursesCF;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSTimestamp;



@RunWith(value = MockitoJUnitRunner.class)
public class EOPreEtudiantPaiementServiceMontantAPayerTest {

	EOPreEtudiantPaiementService servicePaiement = new EOPreEtudiantPaiementService();
	
	private IInfosEtudiant etudiantMoinsDe20Ans;
	private IInfosEtudiant etudiant20AnsOuPlus;
	private IInfosEtudiant etudiant28AnsOuPlus;
	private IEtudiantAnnee etudiantAnneeMoinsDe20Ans;
	private IEtudiantAnnee etudiantAnnee20AnsOuPlus;
	private IEtudiantAnnee etudiantAnnee28AnsOuPlus;
	private IParametragePaieFormation parametragePaieFormation;
	private IParametragePaieDiplome parametragePaieDiplome;
	private IParametragePaieArticleComplementaire parametragePaieArticleComplementaireSecu;
	private IParametragePaieArticleComplementaire parametragePaieArticleComplementaireAutre;

	@Before
	public void setUp() throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		etudiantMoinsDe20Ans = mock(IInfosEtudiant.class);		
		when(etudiantMoinsDe20Ans.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1997")));
		
		etudiant20AnsOuPlus = mock(IInfosEtudiant.class);		
		when(etudiant20AnsOuPlus.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1994")));
		
		etudiant28AnsOuPlus = mock(IInfosEtudiant.class);		
		when(etudiant28AnsOuPlus.dNaissance()).thenReturn(new NSTimestamp(simpleDateFormat.parse("04/05/1986")));

		EOCotisation cotisation = mock(EOCotisation.class);
		when(cotisation.cotiCode()).thenReturn(EOCotisation.COTI_CODE_ENFANT_SALARIE);

		etudiantAnneeMoinsDe20Ans = mock(IPreEtudiantAnnee.class);
		when(etudiantAnneeMoinsDe20Ans.annee()).thenReturn(2014);
		when(etudiantAnneeMoinsDe20Ans.toInfosEtudiant()).thenReturn(etudiantMoinsDe20Ans);
		when(etudiantAnneeMoinsDe20Ans.toCotisation()).thenReturn(cotisation);
		etudiantAnnee20AnsOuPlus = mock(IPreEtudiantAnnee.class);
		when(etudiantAnnee20AnsOuPlus.annee()).thenReturn(2014);
		when(etudiantAnnee20AnsOuPlus.toInfosEtudiant()).thenReturn(etudiant20AnsOuPlus);
		when(etudiantAnnee20AnsOuPlus.toCotisation()).thenReturn(cotisation);
		etudiantAnnee28AnsOuPlus = mock(IPreEtudiantAnnee.class);
		when(etudiantAnnee28AnsOuPlus.annee()).thenReturn(2014);
		when(etudiantAnnee28AnsOuPlus.toInfosEtudiant()).thenReturn(etudiant28AnsOuPlus);
		when(etudiantAnnee28AnsOuPlus.toCotisation()).thenReturn(cotisation);

		parametragePaieFormation = mock(IParametragePaieFormation.class);
		when(parametragePaieFormation.montant()).thenReturn(new BigDecimal(10));
		when(parametragePaieFormation.boursier()).thenReturn(true);

		parametragePaieDiplome = mock(IParametragePaieDiplome.class);
		when(parametragePaieDiplome.montant()).thenReturn(new BigDecimal(20));

		parametragePaieArticleComplementaireSecu = mock(IParametragePaieArticleComplementaire.class);
		when(parametragePaieArticleComplementaireSecu.montant()).thenReturn(new BigDecimal(30));
		ITypeArticleComplementaire typeArticleComplementaireSecu = mock(ITypeArticleComplementaire.class);
		when(typeArticleComplementaireSecu.code()).thenReturn(ITypeArticleComplementaire.CODE_SECU);
		when(parametragePaieArticleComplementaireSecu.toTypeArticleComplementaire()).thenReturn(typeArticleComplementaireSecu);
		
		parametragePaieArticleComplementaireAutre = mock(IParametragePaieArticleComplementaire.class);
		when(parametragePaieArticleComplementaireAutre.montant()).thenReturn(new BigDecimal(40));
		ITypeArticleComplementaire typeArticleComplementaireAutre = mock(ITypeArticleComplementaire.class);
		when(typeArticleComplementaireAutre.code()).thenReturn(ITypeArticleComplementaire.CODE_ACTIVITE);
		when(parametragePaieArticleComplementaireAutre.toTypeArticleComplementaire()).thenReturn(typeArticleComplementaireAutre);
	}

	@Test
	public void testGetMontantAPayerEtudiantNonBoursier() {
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}
	
	@Test
	public void testGetMontantAPayerEtudiantBoursier() {
		IPreBourses preBourse = mock(IPreBourses.class);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);
		
		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}
	
	@Test
	public void testGetMontantAPayerEtudiantBoursierCampusFranceRien() {
		ITypeBoursesCF typeBoursesCF = mock(ITypeBoursesCF.class);
		IPreBourses preBourse = mock(IPreBourses.class);
		when(preBourse.isCampusFrance()).thenReturn(true);
		when(preBourse.toTypeBoursesCF()).thenReturn(typeBoursesCF);
		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_SANS);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);

		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}
	
	@Test
	public void testGetMontantAPayerEtudiantBoursierCampusFranceInscription() {
		ITypeBoursesCF typeBoursesCF = mock(ITypeBoursesCF.class);
		IPreBourses preBourse = mock(IPreBourses.class);
		when(preBourse.isCampusFrance()).thenReturn(true);
		when(preBourse.toTypeBoursesCF()).thenReturn(typeBoursesCF);
		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_INSCRIPTION);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);
		
		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}
	
	@Test
	public void testGetMontantAPayerEtudiantBoursierCampusFranceSecu() {
		ITypeBoursesCF typeBoursesCF = mock(ITypeBoursesCF.class);
		IPreBourses preBourse = mock(IPreBourses.class);
		when(preBourse.isCampusFrance()).thenReturn(true);
		when(preBourse.toTypeBoursesCF()).thenReturn(typeBoursesCF);
		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_SECU);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);
		
		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(new BigDecimal(10));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}

	@Test
	public void testGetMontantAPayerEtudiantBoursierCampusFranceTout() {
		ITypeBoursesCF typeBoursesCF = mock(ITypeBoursesCF.class);
		IPreBourses preBourse = mock(IPreBourses.class);
		when(preBourse.isCampusFrance()).thenReturn(true);
		when(preBourse.toTypeBoursesCF()).thenReturn(typeBoursesCF);
		when(preBourse.toTypeBoursesCF().code()).thenReturn(ITypeBoursesCF.CODE_INSCRIPTION_SECU);
		List<? extends IPreBourses> listeBourses = Arrays.asList(preBourse);
		
		when(etudiantAnneeMoinsDe20Ans.boursier()).thenReturn(true);
		when(etudiantAnnee20AnsOuPlus.boursier()).thenReturn(true);
		when(etudiantAnnee28AnsOuPlus.boursier()).thenReturn(true);
		doReturn(listeBourses).when(etudiantAnneeMoinsDe20Ans).toBourses();
		doReturn(listeBourses).when(etudiantAnnee20AnsOuPlus).toBourses();
		doReturn(listeBourses).when(etudiantAnnee28AnsOuPlus).toBourses();

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieFormation)).isEqualTo(BigDecimal.ZERO);
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieDiplome)).isEqualTo(new BigDecimal(20));
		
		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(BigDecimal.ZERO);
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireSecu)).isEqualTo(new BigDecimal(30));

		assertThat(servicePaiement.getMontantAPayer(etudiantAnneeMoinsDe20Ans, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee20AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
		assertThat(servicePaiement.getMontantAPayer(etudiantAnnee28AnsOuPlus, parametragePaieArticleComplementaireAutre)).isEqualTo(new BigDecimal(40));
	}
}
