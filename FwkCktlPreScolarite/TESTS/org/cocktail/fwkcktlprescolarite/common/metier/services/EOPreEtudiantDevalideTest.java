package org.cocktail.fwkcktlprescolarite.common.metier.services;


import static org.fest.assertions.api.Assertions.assertThat;

import java.math.BigDecimal;

import org.cocktail.fwkcktldroitsutils.common.metier.EOPersonne;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPreInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOBourses;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOCursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogique;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOInscriptionPedagogiqueElement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EONote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOScolTestCase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTestsHelper;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypeNote;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeComposant;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import com.webobjects.foundation.NSArray;
import com.wounit.annotations.Dummy;

@RunWith(value=MockitoJUnitRunner.class)
public class EOPreEtudiantDevalideTest extends EOScolTestCase {

	private static final int ANNEE = 2013;
	@Dummy
	private EOIndividu individu;
	
	@Dummy
	private EOEtudiant etudiant;

	@Dummy
	private EOPreEtudiant preEtudiant;
	

	@Dummy
	private EOInscription inscription;
	
	@Dummy
	private EOInscriptionPedagogique inscriptionPeda;
	
	@Dummy
	private EOPersonne personne;
	
	private PersonneApplicationUser personneApplicationUser;
	
	@Spy
	private EOPreEtudiantService preEtudiantService;

	// EOTypeLien
	private EOTypeLien typeLienVersionner;
	private EOTypeLien typeLienComposer;

	
	
	@Override
	@Before
	public void setUp() {
		// TODO Auto-generated method stub
		super.setUp();
		EOTypeComposant typeParcours = EOTestsHelper.creerTypeComposant(editingContext, 3, ITypeComposant.TYPEPARCOURS_NOM, ITypeComposant.TYPEPARCOURS_NOM);
		EOTypeComposant typeUE = EOTestsHelper.creerTypeComposant(editingContext, 4, ITypeComposant.TYPEUE_NOM, ITypeComposant.TYPEUE_NOM);
		EOTypeComposant typeEC = EOTestsHelper.creerTypeComposant(editingContext, 5, ITypeComposant.TYPEEC_NOM, ITypeComposant.TYPEEC_NOM);
		EOTypeComposant typeAE = EOTestsHelper.creerTypeComposant(editingContext, 6, ITypeComposant.TYPEAE_NOM, ITypeComposant.TYPEAE_NOM);
		
		
		typeLienComposer = EOTestsHelper.creerTypeLien(editingContext, 1, "composer");
		typeLienVersionner = EOTestsHelper.creerTypeLien(editingContext, 2, "versionner");

		EOTestsHelper.creerRegleLiaison(editingContext, typeParcours, typeUE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeEC, typeAE, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeUE, typeParcours, typeLienComposer);
		EOTestsHelper.creerRegleLiaison(editingContext, typeUE, typeEC, typeLienComposer);

	
	}
	
//	@Test
//	public void devalidPrimoEntrantOK() {
//		preEtudiant.setToEtudiantRelationship(etudiant);
//		preEtudiant.setPreType("P");
//		EOEtudiantAnnee etudAnnee = EOTestsHelper.creerEudiantAnnee(editingContext,  2013);
//		etudAnnee.setToEtudiantRelationship(etudiant);
//		
//		EOUE ue =EOTestsHelper.creerUE(editingContext, "ue", "ue");
//		EOEC ec = EOTestsHelper.creerEC(editingContext, "ec", "ec");
//		EOAE ae1 = EOTestsHelper.creerAE(editingContext, "ae1");
//		EOAE ae2 = EOTestsHelper.creerAE(editingContext, "ae2");
//		EOParcours parcours = EOTestsHelper.creerParcours(editingContext, "parcours1", "parcours1");
//		
//		EOLien lienue =EOTestsHelper.creerLienComposer(editingContext, 1, ue, ec, null);
//		EOLien lienae1 =EOTestsHelper.creerLienComposer(editingContext, 2, ec, ae1, null);
//		EOLien lienae2 =EOTestsHelper.creerLienComposer(editingContext, 3, ec, ae2, null);
//		EOLien lienparcours = EOTestsHelper.creerLienComposer(editingContext, 4, parcours, ue, null);
//
//		EOInscription inscription = EOTestsHelper.creerInscription(editingContext);
//		inscription.setToEtudiantRelationship(etudiant);
//		inscription.setAnnee(2013);
//		EOInscriptionPedagogique inscriptionPeda = EOTestsHelper.creerInscriptionPedagogique(editingContext, inscription);
//		
//		EOInscriptionPedagogiqueElement inscriptionPedaEltParcours = EOTestsHelper.creerInscriptionPedagogiqueElement(editingContext, lienue, inscriptionPeda);
//		EOInscriptionPedagogiqueElement inscriptionPedaEltUE = EOTestsHelper.creerInscriptionPedagogiqueElement(editingContext, lienparcours, inscriptionPeda);
//		EOInscriptionPedagogiqueElement inscriptionPedaElt1 = EOTestsHelper.creerInscriptionPedagogiqueElement(editingContext, lienae1, inscriptionPeda);
//		EOInscriptionPedagogiqueElement inscriptionPedaElt2 = EOTestsHelper.creerInscriptionPedagogiqueElement(editingContext, lienae2, inscriptionPeda);
//		
//		EOBourses bourse1 = EOTestsHelper.creerBourse(editingContext, 2013);
//		bourse1.setToEtudiantRelationship(etudiant);
//		EOCursus cursus1 = EOTestsHelper.creerCursus(editingContext, 2012);
//		cursus1.setToEtudiantRelationship(etudiant);
//		EOCursus cursus2 = EOTestsHelper.creerCursus(editingContext, 2011);
//		cursus2.setToEtudiantRelationship(etudiant);
//		
//		
//		editingContext.saveChanges();	
//		
//		preEtudiantService.devaliderEtudiant(editingContext, preEtudiant, 2013);
//		editingContext.deletedObjects();
//		editingContext.saveChanges();
//		
//		NSArray<EOInscription> ias = EOInscription.fetchAllSco_Inscriptions(editingContext);
//		assertThat(ias.count()).isEqualTo(0);
//		
//		NSArray<EOInscriptionPedagogique> ips = EOInscriptionPedagogique.fetchAllSco_InscriptionPedagogiques(editingContext);
//		assertThat(ips.count()).isEqualTo(0);
//		
//		NSArray<EOInscriptionPedagogiqueElement> elements = EOInscriptionPedagogiqueElement.fetchAllSco_InscriptionPedagogiqueElements(editingContext);
//		assertThat(elements.count()).isEqualTo(0);
//		
//		NSArray<EOBourses> bourses = EOBourses.fetchAllSco_Bourseses(editingContext);
//		assertThat(bourses.count()).isEqualTo(0);
//		
//		NSArray<EOCursus> cursus = EOCursus.fetchAllSco_Cursuses(editingContext);
//		assertThat(cursus.count()).isEqualTo(0);
//
//	}

}
