package org.cocktail.fwkcktlprescolarite.common.metier.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.doReturn;

import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolarite;
import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolariteParamManager;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreEtudiant;
import org.cocktail.fwkcktlprescolarite.common.metier.IPreInscription;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormationAglae;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class AglaeClientServiceTests {

	@Test
	public void authentifier() throws Exception {
		// Arrange
		AglaeClientService service = getAglaeClientService();

		// Assert ready to test
		// Act
		// Assert
		assertThat(service.authentifier("123", "motDePasse")).isEqualTo(false);
		assertThat(service.authentifier("0753292W", "103217I")).isEqualTo(true);
	}

    private AglaeClientService getAglaeClientService() {
        String qNameURL = "https://aglaestage.ac-paris.fr/inscriptionWS/services/InscriptionWS";
        String codeRneQuestionneur = "0751723R";
        String codeRneEtablissement = "0755189H";
        AglaeClientService service = new AglaeClientService(qNameURL, codeRneQuestionneur, codeRneEtablissement);
        return service;
    }

	@Test
	public void getStatuBoursierEchelon() throws Exception {
		AglaeClientService service = spy(getAglaeClientService());

		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		when(preEtudiant.codeIne()).thenReturn("2596907203W");

		IGradeUniversitaire grade = mock(IGradeUniversitaire.class);
		when(grade.type()).thenReturn(EOGradeUniversitaire.GRADE_MASTER);
		IDiplome diplome = mock(IDiplome.class);
		when(diplome.gradeUniversitaire()).thenReturn(grade);
		when(diplome.isHabilitationRequise()).thenReturn(false);

		IPreInscription preInscription = mock(IPreInscription.class);
		when(preInscription.toDiplome()).thenReturn(diplome);
		when(preInscription.toGradeUniversitaire()).thenReturn(grade);
		when(preInscription.niveau()).thenReturn(1);
		when(preEtudiant.getInscriptionPrincipale()).thenReturn(preInscription);

		FwkCktlPreScolariteParamManager paramManager = mock(FwkCktlPreScolariteParamManager.class);
		when(paramManager.getPreScolariteAnneeUniversitaire()).thenReturn("2014");
		when(paramManager.getPreScolariteDefaultEtablissementRne()).thenReturn("0751723R");
		FwkCktlPreScolarite.setParamManager(paramManager);

		doReturn("M20").when(service).getCodeFormation(diplome);
		doReturn("0751723R").when(service).getCodeRNEQuestionneur();
		doReturn("0755189H").when(service).getCodeRNEEtablissement();
		
		// Assert ready to test

		// Act
		service.authentifier("0751723R", "103205A");
		String statutBoursier = service.getStatutBoursier(preEtudiant, true);
		// Assert

		assertThat(statutBoursier).isEqualTo("OK");

		statutBoursier = service.getStatutBoursier(preEtudiant, false);
		assertThat(statutBoursier).isEqualTo("00");
	}

	@Test
	public void getStatutBoursierNon() throws Exception {
		AglaeClientService service = getAglaeClientService();
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);
		when(preEtudiant.codeIne()).thenReturn("0114D02122H");

		ITypeFormation typeFormation = mock(ITypeFormation.class);
		ITypeFormationAglae typeFormationAglae = mock(ITypeFormationAglae.class);
		when(typeFormation.typeFormationAglae()).thenReturn(typeFormationAglae);
		when(typeFormationAglae.code()).thenReturn("M20");
		
		IGradeUniversitaire grade = mock(IGradeUniversitaire.class);
		when(grade.type()).thenReturn(EOGradeUniversitaire.GRADE_LICENCE);
		IDiplome diplome = mock(IDiplome.class);
		when(diplome.gradeUniversitaire()).thenReturn(grade);
		when(diplome.isHabilitationRequise()).thenReturn(false);
		when(diplome.typeFormation()).thenReturn(typeFormation);

		IPreInscription preInscription = mock(IPreInscription.class);
		when(preInscription.toDiplome()).thenReturn(diplome);
		when(preInscription.toGradeUniversitaire()).thenReturn(grade);
		when(preInscription.niveau()).thenReturn(2);
		when(preEtudiant.getInscriptionPrincipale()).thenReturn(preInscription);

		FwkCktlPreScolariteParamManager paramManager = mock(FwkCktlPreScolariteParamManager.class);
        when(paramManager.getPreScolariteAnneeUniversitaire()).thenReturn("2014");
        when(paramManager.getPreScolariteDefaultEtablissementRne()).thenReturn("0752304X");
		FwkCktlPreScolarite.setParamManager(paramManager);
		// Assert ready to test

		// Act
		service.authentifier("0753292W", "103217I");
		String statutBoursier = service.getStatutBoursier(preEtudiant, true);
		// Assert

		assertThat(statutBoursier).isEqualTo("OK");

		statutBoursier = service.getStatutBoursier(preEtudiant, false);
		assertThat(statutBoursier).isEqualTo("N");

	}

	@Test
	public void getStatutBoursierCustomError() throws Exception {
		AglaeClientService service = getAglaeClientService();
		IPreEtudiant preEtudiant = mock(IPreEtudiant.class);

		// Assert
		String statutBoursier = service.getStatutBoursier(preEtudiant);
		assertThat(statutBoursier).isEqualTo(AglaeClientService.STATUT_BOURSIER_ERREUR_PARAMETRE);

		IPreInscription preInscription = mock(IPreInscription.class);
		when(preEtudiant.getInscriptionPrincipale()).thenReturn(preInscription);
		statutBoursier = service.getStatutBoursier(preEtudiant);
		assertThat(statutBoursier).isEqualTo(AglaeClientService.STATUT_BOURSIER_ERREUR_PARAMETRE);
	}

}
