package org.cocktail.fwkcktlprescolarite.common.metier.services;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyObject;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRne;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ICursus;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IInfosEtudiant;
import org.junit.Test;

public class CalculAnnee1InscServiceTest {
	
	@Test
	public void testCalculerAnneesInscriptionSansCursusAvecInfosExistantesAvecUniversite() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		String codeRneEtablissement = "ZZZ";
		IRne rne = mockRne(codeRneEtablissement, true);
		IInfosEtudiant etudiant = mockEtudiantSansCursusAvecInfos(2010L, 2010L, 2010L);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant, times(0)).setEtudAnnee1InscUniv(anneeUniversitaire);
		verify(etudiant, times(0)).setEtudAnnee1InscEtab(anyLong());
		verify(etudiant, times(0)).setEtudAnnee1InscSup(anyLong());
		verify(etudiant, times(0)).setToRneEtabSupRelationship((IRne) anyObject());
	}
	
	@Test
	public void testCalculerAnneesInscriptionSansCursusSansInfosExistantesAvecUniversite() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		String codeRneEtablissement = "ZZZ";
		IRne rne = mockRne(codeRneEtablissement, true);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(null);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant).setEtudAnnee1InscUniv(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaire);
		verify(etudiant).setToRneEtabSupRelationship(rne);
	}
	
	@Test
	public void testCalculerAnneesInscriptionSansCursusSansInfosExistantesSansUniversite() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		String codeRneEtablissement = "ZZZ";
		IRne rne = mockRne(codeRneEtablissement, false);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(null);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant, times(0)).setEtudAnnee1InscUniv(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaire);
		verify(etudiant).setToRneEtabSupRelationship(rne);
	}
	
	@Test
	public void testCalculerAnneesInscriptionAvecUnSeulCursusAvecMemeEtablissement() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		Long anneeUniversitaireCursus = 2013L;
		String codeRneEtablissement = "ZZZ";
		String codeRneCursus = "ZZZ";
		IRne rne = mockRne(codeRneEtablissement, true);
		IRne rneCursus = mockRne(codeRneCursus, true);
		ICursus cursus = mockCursus(anneeUniversitaireCursus, rneCursus);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(cursus);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant).setEtudAnnee1InscUniv(anneeUniversitaireCursus);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaireCursus);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaireCursus);
		verify(etudiant).setToRneEtabSupRelationship(rneCursus);
		
	}
	
	@Test
	public void testCalculerAnneesInscriptionAvecUnSeulCursusAvecEtablissementDifferent() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		Long anneeUniversitaireCursus = 2013L;
		String codeRneEtablissement = "ZZZ";
		String codeRneCursus = "YYY";
		IRne rne = mockRne(codeRneEtablissement, true);
		IRne rneCursus = mockRne(codeRneCursus, true);
		ICursus cursus = mockCursus(anneeUniversitaireCursus, rneCursus);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(cursus);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant).setEtudAnnee1InscUniv(anneeUniversitaireCursus);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaireCursus);
		verify(etudiant).setToRneEtabSupRelationship(rneCursus);
		
	}
	
	@Test
	public void testCalculerAnneesInscriptionAvecUnSeulCursusUniversite() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		Long anneeUniversitaireCursus = 2013L;
		String codeRneEtablissement = "ZZZ";
		String codeRneCursus = "YYY";
		IRne rne = mockRne(codeRneEtablissement, true);
		IRne rneCursus = mockRne(codeRneCursus, true);
		ICursus cursus = mockCursus(anneeUniversitaireCursus, rneCursus);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(cursus);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant).setEtudAnnee1InscUniv(anneeUniversitaireCursus);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaireCursus);
		verify(etudiant).setToRneEtabSupRelationship(rneCursus);
		
	}

	@Test
	public void testCalculerAnneesInscriptionAvecUnSeulCursusPasUniversite() {
		CalculAnnee1InscService calculAnnee1InscService = new CalculAnnee1InscService();
		Long anneeUniversitaire = 2014L;
		Long anneeUniversitaireCursus = 2013L;
		String codeRneEtablissement = "ZZZ";
		String codeRneCursus = "YYY";
		IRne rne = mockRne(codeRneEtablissement, true);
		IRne rneCursus = mockRne(codeRneCursus, false);
		ICursus cursus = mockCursus(anneeUniversitaireCursus, rneCursus);
		IInfosEtudiant etudiant = mockEtudiantSansInfos(cursus);
		
		calculAnnee1InscService.calculerAnneesInscription(etudiant, null, rne, anneeUniversitaire.intValue());
		
		verify(etudiant).setEtudAnnee1InscUniv(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscEtab(anneeUniversitaire);
		verify(etudiant).setEtudAnnee1InscSup(anneeUniversitaireCursus);
		verify(etudiant).setToRneEtabSupRelationship(rneCursus);
		
	}
	
	private IInfosEtudiant mockEtudiantSansInfos(ICursus cursus) {
		IInfosEtudiant etudiant = mockEtudiant(cursus);
		when(etudiant.etudAnnee1inscEtab()).thenReturn(null);
		when(etudiant.etudAnnee1inscUniv()).thenReturn(null);
		when(etudiant.etudAnnee1inscSup()).thenReturn(null);
		return etudiant;
	}
	
	private IInfosEtudiant mockEtudiantSansCursusAvecInfos(Long etudAnnee1inscEtab, Long etudAnnee1inscUniv, Long etudAnnee1inscSup) {
		IInfosEtudiant etudiant = mockEtudiant(null);
		when(etudiant.etudAnnee1inscEtab()).thenReturn(etudAnnee1inscEtab);
		when(etudiant.etudAnnee1inscUniv()).thenReturn(etudAnnee1inscUniv);
		when(etudiant.etudAnnee1inscSup()).thenReturn(etudAnnee1inscSup);
		return etudiant;
	}
	
	private IInfosEtudiant mockEtudiant(ICursus cursus) {
		List<ICursus> desCursus = new ArrayList<ICursus>();
		if (cursus != null) {
			desCursus.add(cursus);
		}
		IInfosEtudiant etudiant = mock(IInfosEtudiant.class);
		doReturn(desCursus).when(etudiant).toCursus();
		return etudiant;
	}

	private ICursus mockCursus(Long anneeUniversitaireCursus, IRne rneCursus) {
		ICursus cursus = mock(ICursus.class);
		when(cursus.toCRne()).thenReturn(rneCursus);
		when(cursus.anneeDebut()).thenReturn(anneeUniversitaireCursus.intValue());
		IGradeUniversitaire gradeUniversitaire = mock(IGradeUniversitaire.class);
		when(gradeUniversitaire.isBac()).thenReturn(false);
		when(cursus.toGradeUniversitaire()).thenReturn(gradeUniversitaire);
		return cursus;
	}

	private IRne mockRne(String codeRne, boolean isUniversite) {
		IRne rne = mock(IRne.class);
		when(rne.isUneUniversite()).thenReturn(isUniversite);
		when(rne.cRne()).thenReturn(codeRne);
		return rne;
	}
	

}
