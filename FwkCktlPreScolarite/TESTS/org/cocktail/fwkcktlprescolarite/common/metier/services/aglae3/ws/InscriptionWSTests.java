package org.cocktail.fwkcktlprescolarite.common.metier.services.aglae3.ws;

import static org.fest.assertions.api.Assertions.assertThat;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;

import org.cocktail.fwkcktlprescolarite.common.metier.services.aglae3.inscriptionws.InscriptionWS;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class InscriptionWSTests {

	@Test
	public void testWebServiceTest() throws MalformedURLException {
		// Arrange
		String INE = "";
		String uairneReference = "";
		String uairne = "";
		String typeCursus = "";
		String formation = "";
		String progression = "";
		String niveau = "";
		String anneeGestion = "";
		String indicateurTest = "test";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "OK");
	}
	
	@Test
	public void testWebServiceParametreVideHorsTest() throws MalformedURLException {
		// Arrange
		String INE = "";
		String uairneReference = "0753495S";
		String uairne = "0753495S";
		String typeCursus = "C";
		String formation = "L01";
		String progression = "L";
		String niveau = "1";
		String anneeGestion = "2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}

	@Test
	public void testWebServiceTypeCursusDifferentCH() throws MalformedURLException {
		// Arrange
		String INE="2404020298S";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="T";
		String formation = "L01";
		String progression = "L";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}

	@Test
	public void testWebServiceProgessionDifferentLMD() throws MalformedURLException {
		// Arrange
		String INE="2404020298S";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L01";
		String progression = "Y";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}

	@Test
	public void testWebServiceFormationInconnue() throws MalformedURLException {
		// Arrange
		String INE="2404020298S";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L35";
		String progression = "L";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}

	@Test
	public void testWebServiceEtudiantInconnu() throws MalformedURLException {
		// Arrange
		String INE="2592106908J";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}
	
	@Test
	public void testWebServiceEtudiantSansDSECourant() throws MalformedURLException {
		// Arrange
		String INE="2592177983P";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceDossierNappartientPasTypePopulationDS() throws MalformedURLException {
		// Arrange
		String INE="2592120289D";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	
	@Test
	public void testWebServiceDossierVerrouille() throws MalformedURLException {
		// Arrange
		String INE="2592054559H";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServiceDSENonValide() throws MalformedURLException {
		// Arrange
		String INE="2592425885Y";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServiceDSEdansCrousGestionDifferent() throws MalformedURLException {
		// Arrange
		String INE="2593054523A";
		String uairneReference ="0693410G";
		String uairne="0693410G";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}
	
	@Test
	public void testWebServiceDecisionPositive00() throws MalformedURLException {
		// Arrange
		String INE="2596907203W";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "00");
	}
	
	@Test
	public void testWebServiceDecisionPositive05() throws MalformedURLException {
		// Arrange
		String INE="2593000027R";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "05");
	}
	
	@Test
	public void testWebServiceDecisionPositive10() throws MalformedURLException {
		// Arrange
		String INE="2596900701D";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}

	@Test
	public void testWebServiceDecisionPositive20() throws MalformedURLException {
		// Arrange
		String INE="2592031233D";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "20");
	}
	
	@Test
	public void testWebServiceDecisionPositive30() throws MalformedURLException {
		// Arrange
		String INE="2592031284J";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "30");
	}
	
	@Test
	public void testWebServiceDecisionPositive40() throws MalformedURLException {
		// Arrange
		String INE="2592002953P";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "40");
	}
	
	@Test
	public void testWebServiceDecisionPositive50() throws MalformedURLException {
		// Arrange
		String INE="2592240262J";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "50");
	}
	
	@Test
	public void testWebServiceDecisionPositive60() throws MalformedURLException {
		// Arrange
		String INE="2592031175R";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "60");
	}
	
	@Test
	public void testWebServiceDecisionPositive70() throws MalformedURLException {
		// Arrange
		String INE="2592053438P";
		String uairneReference ="0751723R";
		String uairne="0755189H";
		String typeCursus="H";
		String formation = "M20";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "70");
	}
	
	@Test
	public void testWebServiceVoeuPorteurAideAutreQueBCS() throws MalformedURLException {
		// Arrange
		String INE="2592100665Y";
		String uairneReference ="0754790Z";
		String uairne="0750697A";
		String typeCursus="H";
		String formation = "L10";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceVoeuPorteur() throws MalformedURLException {
		// Arrange
		String INE="2593095044V";
		String uairneReference ="0750697A";
		String uairne="0750697A";
		String typeCursus="H";
		String formation = "L10";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}
	
	@Test
	public void testWebServiceVoeuPasPorteur() throws MalformedURLException {
		// Arrange
		String INE="2596900745B";
		String uairneReference ="0750697A";
		String uairne="0750697A";
		String typeCursus="H";
		String formation = "L10";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceDecisionEnRejet() throws MalformedURLException {
		// Arrange
		String INE="2596905889T";
		String uairneReference ="0754790Z";
		String uairne="0754790Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceDecisionPositive() throws MalformedURLException {
		// Arrange
		String INE="2592119011P";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}
	
	@Test
	public void testWebServiceDecisionNegative() throws MalformedURLException {
		// Arrange
		String INE="2593054647K";
		String uairneReference ="0753495S";
		String uairne="0753495S";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServicePresenceForcageDistant() throws MalformedURLException {
		// Arrange
		String INE="2496900389M";
		String uairneReference ="0754790Z";
		String uairne="0754790Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServicePresenceForcageDecision() throws MalformedURLException {
		// Arrange
		String INE="2592118966R";
		String uairneReference ="0754790Z";
		String uairne="0754790Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServiceNonRespectLimiteAge() throws MalformedURLException {
		// Arrange
		String INE="2592178171U";
		String uairneReference ="0754790Z";
		String uairne="0754790Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceSituationEtranger7() throws MalformedURLException {
		// Arrange
		String INE="2592117142H";
		String uairneReference ="0754790Z";
		String uairne="0754790Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I",INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceCursusNonHabilite() throws MalformedURLException {
		// Arrange
		String INE="2592119011P";
		String uairneReference ="0753364Z";
		String uairne="0753364Z";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceCursusFerme() throws MalformedURLException {
		// Arrange
		String INE="2592119011P";
		String uairneReference ="075019DD";
		String uairne="075019DD";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";
		
		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}
	
	@Test
	public void testWebServiceCursusNonReferenceDansAglae() throws MalformedURLException {
		// Arrange
		String INE="2596905889T";
		String uairneReference ="075030DY";
		String uairne="075030DY";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";
		
		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "RF");
	}

	@Test
	public void testWebServiceInscriptionL1() throws MalformedURLException {
		// Arrange
		String INE="2592119011P";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}
	
	@Test
	public void testWebServiceInscriptionL1KO() throws MalformedURLException {
		// Arrange
		String INE="2596909163B";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceInscriptionH1() throws MalformedURLException {
		// Arrange
		String INE="2592067560P";
		String uairneReference ="0752304X";
		String uairne="0752304X";
		String typeCursus="H";
		String formation = "L32";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}
	
	@Test
	public void testWebServiceInscriptionH1KO() throws MalformedURLException {
		// Arrange
		String INE="2592182871C";
		String uairneReference ="0752304X";
		String uairne="0752304X";
		String typeCursus="H";
		String formation = "L32";
		String progression = "H";
		String niveau ="1";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}

	@Test
	public void testWebServiceCasAutresProgressionOuNiveau() throws MalformedURLException {
		// Arrange
		String INE="2592119011P";
		String uairneReference ="0752304X";
		String uairne="0752304X";
		String typeCursus="H";
		String formation = "L32";
		String progression = "H";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServiceCasProgressionNSuperieurNm1() throws MalformedURLException {
		// Arrange
		String INE="2592117922F";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "M01";
		String progression = "M";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "A");
	}

	@Test
	public void testWebServiceCasProgressionNEgalNm1() throws MalformedURLException {
		// Arrange
		String INE="2404020298S";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "10");
	}

	@Test
	public void testWebServiceCasProgressionNEgalNm1KO() throws MalformedURLException {
		// Arrange
		String INE="2592120315G";
		String uairneReference ="075073DR";
		String uairne="075073DR";
		String typeCursus="C";
		String formation = "L01";
		String progression = "L";
		String niveau ="2";
		String anneeGestion ="2014";
		String indicateurTest = "";

		checkWebService("0753292W", "103217I", INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest, "N");
	}
	
	
	private void checkWebService(String login, String password, String INE, String uairneReference, String uairne, String typeCursus, String formation, String progression, String niveau,
	    String anneeGestion, String indicateurTest, String expectedReturnValue) throws MalformedURLException {
		InscriptionWS inscriptionWS = getInscriptionWS();
		Map requestContext = ((BindingProvider) inscriptionWS).getRequestContext();
		requestContext.put(BindingProvider.SESSION_MAINTAIN_PROPERTY, Boolean.TRUE);

		assertThat(inscriptionWS.authentifier(login, password)).isTrue();
		String statut = inscriptionWS.getStatutBoursier(INE, uairneReference, uairne, typeCursus, formation, progression, niveau, anneeGestion, indicateurTest);
		assertThat(statut).isEqualTo(expectedReturnValue);
	}

	private InscriptionWS getInscriptionWS() throws MalformedURLException {
		String QNAME_URL = "https://aglaestage.ac-paris.fr/inscriptionWS/services/InscriptionWS";
		String QNAME_NAME = "InscriptionWSService";
		String SERVICE_URL = "https://aglaestage.ac-paris.fr/inscriptionWS/services/InscriptionWS?wsdl";
		URL url = new URL(SERVICE_URL);
		QName qname = new QName(QNAME_URL, QNAME_NAME);
		Service service = Service.create(url, qname);
		InscriptionWS inscriptionWS = service.getPort(InscriptionWS.class);
		return inscriptionWS;
	}
}
