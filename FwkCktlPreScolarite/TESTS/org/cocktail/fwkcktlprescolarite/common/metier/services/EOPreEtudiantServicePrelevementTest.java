package org.cocktail.fwkcktlprescolarite.common.metier.services;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.cocktail.fwkcktlcompta.server.scolarite.PrelevementSepaService;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IEtudiant;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IFournis;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRib;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoEtudiantAnnee;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IScoPaiementMoyen;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(value=MockitoJUnitRunner.class)
public class EOPreEtudiantServicePrelevementTest {

    EOPreEtudiantService preEtudiantService;
    
    @Mock
    PrelevementSepaService prelevementSepaService;

    @Mock
    IScoEtudiantAnnee etudiantAnnee;
    @Mock
    IIndividu unIndividu;
    @Mock
    IEtudiant unEtudiant;
    @Mock
    IAdresse uneAdresse;
    @Mock
    IFournis fournis;
    @Mock
    IRib rib;
    @Mock
    IScoPaiement paiement;
    @Mock
    IScoPaiementMoyen moyen;
    @Mock
    IScoPaiementEcheance echeance;
    @Mock
    IPersonne personne;
    
    @Before
    public void setUp() {
        preEtudiantService = new EOPreEtudiantService();
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansEtudiant() throws ImportPrelevementException {
        
        when(etudiantAnnee.toEtudiant()).thenReturn(null);
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansEtudiant");
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansAdresseFacturation() throws ImportPrelevementException {
        
        when(unIndividu.adresseFacturation()).thenReturn(null);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansAdresseFacturation");
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansFournisseur() throws ImportPrelevementException {
        
        when(unIndividu.adresseFacturation()).thenReturn(uneAdresse);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansFournisseur");
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansRib() throws ImportPrelevementException {
        
        when(unIndividu.adresseFacturation()).thenReturn(uneAdresse);
        when(unIndividu.toFournis()).thenReturn(fournis);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansRib");
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansPaiement() throws ImportPrelevementException {
        
        when(fournis.toDernierRib()).thenReturn(rib);
        when(unIndividu.adresseFacturation()).thenReturn(uneAdresse);
        when(unIndividu.toFournis()).thenReturn(fournis);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansPaiement");
    }
    
    @Test(expected=ImportPrelevementException.class)
    public void testImporterPrelevementSepaSansEcheances() throws ImportPrelevementException {
        
        when(fournis.toDernierRib()).thenReturn(rib);
        when(unIndividu.adresseFacturation()).thenReturn(uneAdresse);
        when(unIndividu.toFournis()).thenReturn(fournis);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        doReturn(Arrays.asList(paiement)).when(etudiantAnnee).toPaiements();
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaSansEcheances");
    }
    
    @Test
    public void testImporterPrelevementSepaNormal() throws ImportPrelevementException {
    	when(fournis.toPersonne()).thenReturn(personne);
    	when(personne.persId()).thenReturn(900);
        when(unEtudiant.toIndividu()).thenReturn(unIndividu);
        when(etudiantAnnee.toEtudiant()).thenReturn(unEtudiant);
        doReturn(Arrays.asList(paiement)).when(etudiantAnnee).toPaiements();
        doReturn(Arrays.asList(moyen)).when(etudiantAnnee).moyensPaiementPrelevementSansEcheancierSepa();
        when(moyen.toFournisseurTitulaire()).thenReturn(fournis);
        doReturn(rib).when(moyen).toRibTitulaire();
        when(moyen.toAdresseTitulaire()).thenReturn(uneAdresse);
        doReturn(paiement).when(moyen).toPaiement();
        
        preEtudiantService.importerPrelevementSepa(prelevementSepaService, etudiantAnnee, "testImporterPrelevementSepaNormal");
    }
    
    
    
}
