package org.cocktail.fwkcktlprescolarite.common.metier.services;

import org.junit.Test;
import static org.fest.assertions.api.Assertions.assertThat;

public class CalculIneServiceTest {
	
	 @Test
	  public void calculerIne() {
		 String uai = "2547898C";
		 Integer numcle = 100;
		 Integer annee = 2014;
		 CalculIneService service = new CalculIneService();
		 String numIne = service.calculer(uai, numcle, annee);
		  // Arrange 
		  // Assert ready to test 
		  // Act 
		   assertThat(numIne).isEqualTo("1ilyyp002s8");
	  }

}
