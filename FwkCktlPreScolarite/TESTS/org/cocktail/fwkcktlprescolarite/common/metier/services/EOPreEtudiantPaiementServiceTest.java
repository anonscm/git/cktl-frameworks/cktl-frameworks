package org.cocktail.fwkcktlprescolarite.common.metier.services;

import java.math.BigDecimal;

import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiement;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementDetail;
import org.cocktail.fwkcktlprescolarite.common.metier.EOPrePaiementMoyen;
import org.cocktail.fwkcktlprescolarite.common.metier.IPrePaiementEcheance;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOTypePaiement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypePaiement;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.Dummy;
import com.wounit.rules.MockEditingContext;

@RunWith(value = MockitoJUnitRunner.class)
public class EOPreEtudiantPaiementServiceTest {

	EOPreEtudiantPaiementService servicePaiement = new EOPreEtudiantPaiementService();

	@Rule
	public MockEditingContext ec = new MockEditingContext("PreScolarite");

	private EOTypePaiement typePaiementComptant, typePaiementDiffere;

	@Dummy
	private EOPrePaiement prePaiementAPayer, prePaiementComptant, prePaiementAZero, prePaiementA1euro10Centimes;

	@Dummy
	private EOPrePaiementDetail preDetailPaiementAPayer1, preDetailPaiementAPayer2, preDetailPaiementAPayer3, preDetailPaiementAPayer4;
	@Dummy
	private EOPrePaiementDetail preDetailPaiementComptant1, preDetailPaiementA1euro10Centimes1;

	@Dummy
	private EOPrePaiementMoyen preMoyenPaiementAPayer, preMoyenPaiementComptant, preMoyenPaiementAZero, preMoyenPaiementA1euro10Centimes;

	@Before
	public void setUp() {
		UserInfo userInfo = Mockito.mock(UserInfo.class);
		Mockito.when(userInfo.persId()).thenReturn(new Integer(10));

		typePaiementComptant = EOTypePaiement.createSco_TypePaiement(ec, ITypePaiement.CODE_TYPE_COMPTANT, "Comptant");
		typePaiementDiffere = EOTypePaiement.createSco_TypePaiement(ec, ITypePaiement.CODE_TYPE_DIFFERE, "Différé");

		preDetailPaiementAPayer1.setToPrePaiementRelationship(prePaiementAPayer);
		preDetailPaiementAPayer1.setMontantAPayer(new BigDecimal("18.00"));
		preDetailPaiementAPayer2.setToPrePaiementRelationship(prePaiementAPayer);
		preDetailPaiementAPayer2.setMontantAPayer(new BigDecimal("15.12"));
		preDetailPaiementAPayer3.setToPrePaiementRelationship(prePaiementAPayer);
		preDetailPaiementAPayer3.setMontantAPayer(new BigDecimal("27.20"));
		preDetailPaiementAPayer4.setToPrePaiementRelationship(prePaiementAPayer);
		preDetailPaiementAPayer4.setMontantAPayer(new BigDecimal("8.00"));
		preMoyenPaiementAPayer.setToPrePaiementRelationship(prePaiementAPayer);
		preMoyenPaiementAPayer.setToTypePaiementRelationship(typePaiementDiffere);
		preMoyenPaiementAPayer.setMontantPaye(new BigDecimal("68.32"));

		preDetailPaiementComptant1.setToPrePaiementRelationship(prePaiementComptant);
		preDetailPaiementComptant1.setMontantAPayer(new BigDecimal("25.00"));
		preMoyenPaiementComptant.setToPrePaiementRelationship(prePaiementComptant);
		preMoyenPaiementComptant.setToTypePaiementRelationship(typePaiementComptant);
		preMoyenPaiementComptant.setMontantPaye(new BigDecimal("25.00"));

		preMoyenPaiementAZero.setToPrePaiementRelationship(prePaiementAZero);
		preMoyenPaiementAZero.setToTypePaiementRelationship(typePaiementDiffere);

		preDetailPaiementA1euro10Centimes1.setToPrePaiementRelationship(prePaiementA1euro10Centimes);
		preDetailPaiementA1euro10Centimes1.setMontantAPayer(new BigDecimal("1.10"));
		preMoyenPaiementA1euro10Centimes.setToPrePaiementRelationship(prePaiementA1euro10Centimes);
		preMoyenPaiementA1euro10Centimes.setToTypePaiementRelationship(typePaiementDiffere);
		preMoyenPaiementA1euro10Centimes.setMontantPaye(new BigDecimal("1.10"));
	}

	@Test
	public void testCreerEcheancier3Echeances() throws PreEtudiantPaiementException {

		servicePaiement.creerEcheancier(ec, new BigDecimal("68.32"), preMoyenPaiementAPayer, null, 3, 5, 10);

		Assert.assertTrue(preMoyenPaiementAPayer.toPrePaiementEcheances().size() == 3);

		IPrePaiementEcheance preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(0);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 1/3");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("24.32"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(1);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 2/3");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("22.00"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(2);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 3/3");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("22.00"));
	}

	@Test
	public void testCreerEcheancier5Echeances() throws PreEtudiantPaiementException {

		servicePaiement.creerEcheancier(ec, new BigDecimal("68.32"), preMoyenPaiementAPayer, null, 5, 5, 10);

		Assert.assertTrue(preMoyenPaiementAPayer.toPrePaiementEcheances().size() == 5);

		IPrePaiementEcheance preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(0);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 1/5");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("16.32"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(1);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 2/5");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("13.00"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(2);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 3/5");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("13.00"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(3);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 4/5");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("13.00"));

		preEcheancePaiement = preMoyenPaiementAPayer.toPrePaiementEcheances().get(4);
		Assert.assertEquals(preEcheancePaiement.libelle(), "Échéance 5/5");
		Assert.assertEquals(preEcheancePaiement.montant(), new BigDecimal("13.00"));
	}

	@Test
	public void testCreerEcheancierPaiementComptant() {
		boolean exception = false;

		try {
			servicePaiement.creerEcheancier(ec, new BigDecimal("25.00"), preMoyenPaiementComptant, null, 3, 5, 10);
		} catch (PreEtudiantPaiementException e) {
			Assert.assertEquals(e.getMessage(), EOPreEtudiantPaiementService.IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_COMPTANT);
			exception = true;
		}

		Assert.assertTrue("Une exception aurait dû survenir", exception);
	}

	@Test
	public void testCreerEcheancierMontantAZero() {
		boolean exception = false;

		try {
			servicePaiement.creerEcheancier(ec, BigDecimal.ZERO, preMoyenPaiementAZero, null, 3, 5, 10);
		} catch (PreEtudiantPaiementException e) {
			Assert.assertEquals(e.getMessage(), EOPreEtudiantPaiementService.IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_ZERO);
			exception = true;
		}

		Assert.assertTrue("Une exception aurait dû survenir", exception);
	}

	@Test
	public void testCreerEcheancierMontantTropFaible() {
		boolean exception = false;

		try {
			servicePaiement.creerEcheancier(ec, new BigDecimal("1.10"), preMoyenPaiementA1euro10Centimes, null, 3, 5, 10);
		} catch (PreEtudiantPaiementException e) {
			Assert.assertEquals(e.getMessage(),
			    String.format(EOPreEtudiantPaiementService.IMPOSSIBLE_CREER_ECHEANCIER_PAIEMENT_MONTANT_INFERIEUR_ECHEANCES, "1.10", "3"));
			exception = true;
		}

		Assert.assertTrue("Une exception aurait dû survenir", exception);
	}

}
