package org.cocktail.fwkcktlprescolarite.common.metier;

import static com.wounit.matchers.EOAssert.canBeSaved;
import static com.wounit.matchers.EOAssert.cannotBeSaved;
import static com.wounit.matchers.EOAssert.confirm;
import static org.mockito.Mockito.mock;

import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolarite;
import org.cocktail.fwkcktlprescolarite.common.FwkCktlPreScolariteParamManager;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

/**
 * Tests de la classe EOPreEtudiantAnnee :
 * - tests basiques des contraintes sur les attributs (not null)
 * 
 * @author Alexis Tual
 *
 */
@RunWith(MockitoJUnitRunner.class)
public class EOPreEtudiantAnneeTest {

	@Rule
	public MockEditingContext ec = new MockEditingContext("PreScolarite");
	
	@UnderTest
	private EOPreEtudiantAnnee preEtudiantAnnee;
	
	@Dummy
	private EOPreEtudiant preEtudiant;
	
	@Before
	public void initBonPreEtudiantAnnee() {
		// On mock le paramManager pour mettre l'annee scol à 2012
		FwkCktlPreScolariteParamManager paramManager = mock(FwkCktlPreScolariteParamManager.class);
		FwkCktlPreScolarite.setParamManager(paramManager);
		preEtudiantAnnee = EOPreEtudiantAnnee.creer(ec, 999, preEtudiant, 2012);
		preEtudiantAnnee.setEnfantsCharge(true);
	}
	
	@Test
	public void testEnregistrementMinimal() {
		confirm(preEtudiantAnnee, canBeSaved());
	}
	
	
	
	
	@Test
	public void testEnfantsCharge() {
		preEtudiantAnnee.setEnfantsCharge(null);
		confirm(preEtudiantAnnee, cannotBeSaved());
	}
	
}
