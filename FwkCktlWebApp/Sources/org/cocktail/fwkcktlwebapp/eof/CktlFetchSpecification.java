package org.cocktail.fwkcktlwebapp.eof;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXSortOrdering.ERXSortOrderings;

public class CktlFetchSpecification<T extends EOEnterpriseObject> extends ERXFetchSpecification<T> {

	private static final long serialVersionUID = 1L;
	
	private ERXKey<? extends Object> priorKey;
	private ERXKey<? extends Object> key;
	
	private EOQualifier startWithQualifier;
	private NSArray<EOSortOrdering> siblingsSortOrderings;
	
	private Integer maxLevel;
	
	public CktlFetchSpecification(String entityName, EOQualifier qualifier, ERXSortOrderings sortOrderings) {
		super(entityName, qualifier, sortOrderings);
		setIsDeep(false);
	}
	
	public CktlFetchSpecification(String entityName) {
		super(entityName);
		setIsDeep(false);
	}
	
	public CktlFetchSpecification(EOFetchSpecification fs) {
		super(fs);
		setIsDeep(false);
	}

	public void setConnectBy(ERXKey<? extends Object> priorKey, ERXKey<? extends Object> key) {
		this.priorKey = priorKey;
		this.key = key;
	}
	
	public void setStartWithQualifier(EOQualifier startWithQualifier) {
		this.startWithQualifier = startWithQualifier;
	}
	
	public ERXKey<? extends Object> getKey() {
		return key;
	}
	
	public ERXKey<? extends Object> getPriorKey() {
		return priorKey;
	}
	
	public EOQualifier getStartWithQualifier() {
		return startWithQualifier;
	}
	
	@SuppressWarnings({"unchecked"})
	public static <T extends EOEnterpriseObject> CktlFetchSpecification<T> fetchSpec(EOFetchSpecification fs) {
		if (fs instanceof CktlFetchSpecification) {
			return (CktlFetchSpecification <T>) fs;
		}
		return new CktlFetchSpecification<T>(fs);
	}
	
	@Override
	public Object clone() {
		CktlFetchSpecification<T> fs = fetchSpec((EOFetchSpecification) super.clone());
		fs.setConnectBy(this.priorKey, this.key);
		fs.setStartWithQualifier(this.startWithQualifier);
		fs.setSiblingsSortOrderings(siblingsSortOrderings);
		return fs;
	}

	public NSArray<EOSortOrdering> getSiblingsSortOrderings() {
		return siblingsSortOrderings;
	}
	
	public void setSiblingsSortOrderings(NSArray<EOSortOrdering> orderings) {
		this.siblingsSortOrderings = orderings;
	}

	public Integer getMaxLevel() {
		return maxLevel;
	}
	
	public void setMaxLevel(Integer maxLevel) {
		this.maxLevel = maxLevel;
	}
	
}
