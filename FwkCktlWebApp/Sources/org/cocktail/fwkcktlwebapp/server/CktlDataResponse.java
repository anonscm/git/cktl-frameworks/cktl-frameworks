package org.cocktail.fwkcktlwebapp.server;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;

import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSData;

/**
 * Cette classe permet d'envoyer au client des documents autres que les composants HTML generes par WebObjects. Le type de contenu des documents est
 * indique a l'aide d'une des instances <code>MIME_XXX</code>.
 * <p>
 * Une instance de cette classe est creee a l'aide de son contructeur. Ensuite, le contenu des donnees est definie en appelant une des methodes
 * <code>setContent</code> :
 * </p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * byte[] contenu = ...; <code class="comment">// Initialisation de contenu du document</code>
 * CktlDataResponse dataResponse = new CktlDataResponse();
 * dataResponse.setContent(contenu, CktlDataResponse.MIME_PDF);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 */
public class CktlDataResponse extends WOResponse {
	/**
	 * La constant definissant le type MIME "application/octet-stream".
	 */
	//  public static final String MIME_ANY = "application/octet-stream"; 
	public static final String MIME_ANY = CktlMimeType.getMimeTypeForExtension("*");

	/**
	 * La constant definissant le type MIME "application/pdf".
	 */
	//  public static final String MIME_PDF = "application/pdf";
	public static final String MIME_PDF = CktlMimeType.getMimeTypeForExtension("pdf");

	/**
	 * La constant definissant le type MIME "application/postscript".
	 */
	//  public static final String MIME_PS = "application/postscript";
	public static final String MIME_PS = CktlMimeType.getMimeTypeForExtension("ps");

	/**
	 * La constant definissant le type MIME "application/rtf".
	 */
	//  public static final String MIME_RTF = "application/rtf";
	public static final String MIME_RTF = CktlMimeType.getMimeTypeForExtension("rtf");

	/**
	 * La constant definissant le type MIME "text/plain".
	 */
	//  public static final String MIME_TXT = "text/plain";
	public static final String MIME_TXT = CktlMimeType.getMimeTypeForExtension("txt");

	/**
	 * La constant definissant le type MIME "text/xml".
	 */
	//  public static final String MIME_XML = "text/xml";
	public static final String MIME_XML = CktlMimeType.getMimeTypeForExtension("xml");

	/**
	 * La constant definissant le type MIME "text/html".
	 */
	//  public static final String MIME_HTML = "text/html";
	public static final String MIME_HTML = CktlMimeType.getMimeTypeForExtension("html");

	/**
	 * Creer une nouvel instance de la reponse. Le contenu de document envoye comme reponse est defini en eappelant une des methodes
	 * <code>setContent</code>.
	 */
	public CktlDataResponse() {
		super();
		this.defineContent(null, MIME_ANY);
	}

	/**
	 * Definit le contenu de la reponse envoyee au client.
	 * 
	 * @param content L'objet avec le contenu du document envoye au client.
	 * @param contentType Le type de contenu du document. Cette valeur doit correspondre a une des constantes <code>MIME_XXX</code>.
	 */
	private void defineContent(NSData content, String contentType) {
		//    super.removeHeadersForKey("cache-control");
		super.disableClientCaching();
		if (content != null) {
			super.setContent(content);
			setContentType(contentType);
			super.setHeader(String.valueOf(content.length()), "Content-Length");
		}
		else {
			super.setHeader("0", "Content-Length");
		}
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est stockes dans un tableau d'octets.
	 * 
	 * @param someContent Le contenu du document.
	 * @param contentType Le type MIME du contenu. Une des constantes <code>MIME_XXX</code> doit etre utilisee pour definir cette valeur.
	 * @see #setContent(InputStream, long, String)
	 */
	public void setContent(byte[] someContent,
							String contentType) {
		if (someContent == null)
			defineContent(null, contentType);
		else
			defineContent(new NSData(someContent), contentType);
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est lu a partir d'un flux d'octets.
	 * 
	 * @param contentStream Le flux d'octets avec le contenu du document.
	 * @param size La taille de contenu en octets ou -1 si la taille est inconnue.
	 * @param contentType Le type MIME du contenu. Une des constantes <code>MIME_XXX</code> doit etre utilisee pour definir cette valeur.
	 * @see #setContent(byte[], String)
	 */
	public void setContent(InputStream contentStream,
							long size,
							String contentType) {
		NSData content = null;
		if (contentStream != null) {
			ByteArrayOutputStream byteStream = new ByteArrayOutputStream();
			try {
				StreamCtrl.writeContentToStream(contentStream, byteStream, size);
				content = new NSData(byteStream.toByteArray());
			} catch (IOException ex) {
			}
		}
		defineContent(content, contentType);
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est place dans l'objet <code>NSData</code>.
	 * 
	 * @param someContent L'objet comportant le contenu du document.
	 * @param contentType Le type MIME du contenu. Une des constantes <code>MIME_XXX</code> doit etre utilisee pour definir cette valeur.
	 * @see #setContent(byte[], String)
	 */
	public void setContent(NSData someContent, String contentType) {
		defineContent(someContent, contentType);
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est place dans l'objet {@link NSData}. Le type MIME "application/octet-stream" est
	 * associe au document.
	 * 
	 * @param someContent L'object comportant le contenu de document.
	 */
	public void setContent(NSData someContent) {
		this.defineContent(someContent, MIME_ANY);
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est place dans une chaine de caracteres. Le type MIME "application/octet-stream"
	 * est associe au document.
	 * 
	 * @param someContent Le contenu de document.
	 */
	public void setContent(String someContent) {
		this.setContent(someContent.getBytes(), MIME_ANY);
	}

	/**
	 * Definit le contenu de document envoye au client. Le contenu est place dans un tableau des caracteres. Le type MIME "application/octet-stream"
	 * est associe au document.
	 * 
	 * @param someContent Le contenu de document.
	 */
	public void setContent(char[] someContent) {
		this.setContent(new String(someContent));
	}

	/**
	 * Definit le type du contenu de la reponse. Cette methode ajoute une definition "Content-Type" dans l'entete de la reponse HTTP.
	 * <p>
	 * Voir une des constantes <code>MIME_<i>XXX</i></code> pour les types des contenus possibles.
	 * </p>
	 */
	public void setContentType(String contentType) {
		super.setHeader(contentType, "Content-Type");
	}

	/**
	 * Definit le nom de fichier associe a la reponse. Cette methode peut etre appellee lorsque la reponse envoye correspond a un fichier qui doit
	 * etre enregistre par le client.
	 */
	public void setFileName(String fileName) {
		super.setHeader(fileName, "Content-Title");
		super.setHeader("attachement; filename=\"" + fileName + "\"", "Content-Disposition");
	}
}
