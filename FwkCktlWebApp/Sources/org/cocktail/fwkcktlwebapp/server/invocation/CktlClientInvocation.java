package org.cocktail.fwkcktlwebapp.server.invocation;


import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppFromJar;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

/**
 * Méthodes qui peuvent être appelées en RMI par les applications JavaClient.
 * 
 * @author rprin
 */
public class CktlClientInvocation {

	private CktlWebSession session;

	public CktlClientInvocation(CktlWebSession session) {
		this.session = session;
	}

	/**
	 * @return Le n° de version obtenu depuis le jar contenant les classes serveur.
	 * @throws Exception
	 */
	public final String clientSideRequestGetVersionFromJar() throws Exception {
		try {
			String sessionClassName = session.getClass().getName();
			VersionAppFromJar versionAppFromJar = new VersionAppFromJar(sessionClassName);
			return versionAppFromJar.fullVersion();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}

	}

	public final Boolean clientSideRequestIsDevelopmentMode() {
		return CktlWebApplication.application().isDevelopmentMode();

	}
}
