package org.cocktail.fwkcktlwebapp.server.injection;

import java.lang.reflect.Field;

import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

public class FlowTypeListener implements TypeListener {

	private FlowProvider flowProvider;
	private FlowNameClassNameRegistry flowNameClassNameRegistry;

	public <I> void hear(TypeLiteral<I> type, TypeEncounter<I> encounter) {
		for (Field field : type.getRawType().getDeclaredFields()) {
			if (isAFlowType(field.getType())) {
			    processInjectFlow(type.getRawType(), field, encounter);
	        }
	      }
		/**** TESTS JULIEN *******/
		/*Class<?> clazz = type.getRawType();
		if (clazz.isAnnotationPresent(InjectFlow.class)) {
			InjectFlow injectFlow = clazz.getAnnotation(InjectFlow.class);
			String flowName = injectFlow.name().getName();
            if (flowNameClassNameRegistry != null) {
                flowNameClassNameRegistry.register(flowName, clazz);
            }
		}*/
	}

	private boolean isAFlowType(Class<?> type) {
	    return Flow.class.isAssignableFrom(type);
	}
	
	private <I> void processInjectFlow(Class<?> clazz, Field field, TypeEncounter<I> encounter) {
        if (field.isAnnotationPresent(InjectFlow.class)) {
            InjectFlow injectFlow = field.getAnnotation(InjectFlow.class);
            String flowName = injectFlow.name().getName();
            encounter.register(new FlowMembersInjector<I>(flowProvider, field, flowName));
            if (flowNameClassNameRegistry != null) {
                flowNameClassNameRegistry.register(flowName, clazz);
            }
        }
	}
	
	public void setFlowProvider(FlowProvider flowProvider) {
		this.flowProvider = flowProvider;
	}
	
	public FlowNameClassNameRegistry getFlowNameClassNameRegistry() {
        return flowNameClassNameRegistry;
    }
	
	public void setFlowNameClassNameRegistry(FlowNameClassNameRegistry flowNameClassNameRegistry) {
        this.flowNameClassNameRegistry = flowNameClassNameRegistry;
    }

}
