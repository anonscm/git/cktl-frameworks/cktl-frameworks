package org.cocktail.fwkcktlwebapp.server.injection;

import java.util.Map;

public interface Flow {

	void start();
	Map<String, Object> store();

}
