package org.cocktail.fwkcktlwebapp.server.injection;

import java.util.HashMap;
import java.util.Map;

import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;

public class CktlFlow implements Flow {

	private EOEditingContext editingContext;
	private Map<String, Object> store = new HashMap<String, Object>();
	
	public void start() {
		this.editingContext = ERXEC.newEditingContext();
	}

	public EOEditingContext getEditingContext() {
		return editingContext;
	}

	public Map<String, Object> store() {
		return store;
	}
}
