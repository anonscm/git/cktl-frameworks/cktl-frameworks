package org.cocktail.fwkcktlwebapp.server.injection;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.google.inject.Inject;
import com.woinject.WORequestScoped;

@WORequestScoped
public class FlowManager {

	private CktlFlowNameComponentRegistry registry;
	private String currentFlowName;

	@Inject
	public FlowManager(CktlFlowNameComponentRegistry registry) {
		this.registry = registry;
	}

	public void updateCurrentFlow(Class<? extends CktlWebComponent> componentClass) {
		String flowName = registry.flowName(componentClass);
		if (flowName != null) {
			this.currentFlowName = flowName;
		}
	}

	public String getCurrentFlowName() {
		return currentFlowName;
	}

	protected void setCurrentFlowName(String currentFlowName) {
		this.currentFlowName = currentFlowName;
	}

}
