package org.cocktail.fwkcktlwebapp.server.injection;

public interface FlowProvider {

	Flow provide(String name);

}
