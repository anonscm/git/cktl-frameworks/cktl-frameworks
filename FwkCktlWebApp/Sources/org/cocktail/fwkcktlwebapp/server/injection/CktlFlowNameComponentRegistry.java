package org.cocktail.fwkcktlwebapp.server.injection;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.google.inject.Singleton;

@Singleton
public class CktlFlowNameComponentRegistry implements FlowNameClassNameRegistry {

    private Map<String, String> registry;

    public CktlFlowNameComponentRegistry() {
        registry = Collections.synchronizedMap(new HashMap<String, String>());
    }

    public void register(String flowName, Class<?> clazz) {
        registry.put(clazz.getName(), flowName);
    }

    public String flowName(Class<? extends CktlWebComponent> componentClass) {
        String result = null;
        if (componentClass != null) {
            result = registry.get(componentClass.getName());
        }
        return result;
    }
    
}
