package org.cocktail.fwkcktlwebapp.server.injection;

import com.google.inject.Inject;
import com.google.inject.Key;
import com.google.inject.OutOfScopeException;
import com.google.inject.Provider;
import com.google.inject.Scope;
import com.google.inject.Singleton;

public class FlowScope implements Scope {

	@Inject
	private FlowProvider flowProvider;

	public <T> Provider<T> scope(final Key<T> key, final Provider<T> provider) {

		return new Provider<T>() {
			public T get() {
				FlowScoped flowScoped = (FlowScoped) key.getAnnotation();
				Flow flow = flowProvider.provide(flowScoped.flowName().getName());

				if (flow == null) {
					throw new OutOfScopeException("Cannot access scoped object. Either the request has not been dispatched yet, or its cycle has ended.");
				}

				synchronized (flow) {

					Object object = flow.store().get(key.toString());

					if ("NULL".equals(object)) {
						return null;
					}

					@SuppressWarnings("unchecked")
					T t = (T) object;

					if (t == null) {
						t = provider.get();

						if (t == null) {
							flow.store().put(key.toString(), "NULL");
						} else {
							flow.store().put(key.toString(), t);
						}
					}

					return t;
				}
			}
		};
	}



}
