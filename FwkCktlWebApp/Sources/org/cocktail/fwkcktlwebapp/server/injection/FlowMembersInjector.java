package org.cocktail.fwkcktlwebapp.server.injection;

import java.lang.reflect.Field;

import com.google.inject.MembersInjector;

public class FlowMembersInjector<T> implements MembersInjector<T> {

	private Field field;
	private String flowName;

	private FlowProvider flowProvider;

	public FlowMembersInjector(FlowProvider flowProvider, Field field, String flowName) {
		this.field = field;
		this.flowName = flowName;
		this.flowProvider = flowProvider;
		this.field.setAccessible(true);
	}

	public void injectMembers(T instance) {
		try {
			Flow flow = flowProvider.provide(flowName);
			field.set(instance, flow);
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		}
	}

	public void setFlowProvider(FlowProvider flowProvider) {
		this.flowProvider = flowProvider;
	}

}
