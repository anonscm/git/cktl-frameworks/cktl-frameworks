package org.cocktail.fwkcktlwebapp.server.injection;

import org.cocktail.fwkcktlwebapp.server.CktlWebSession;

import er.extensions.appserver.ERXSession;

public class CktlFlowProvider implements FlowProvider {

    public CktlFlow provide(String name) {
        if (name == null) {
            throw new IllegalStateException("Le nom du flow est obligatoire");
        }
        CktlWebSession session = (CktlWebSession)ERXSession.session();
        CktlFlow flow = (CktlFlow)session.objectStore().valueForKey(name);
        if (flow == null) {
            flow = new CktlFlow();
            session.objectStore().takeValueForKey(flow, name);
        }
        return flow;
    }

}
