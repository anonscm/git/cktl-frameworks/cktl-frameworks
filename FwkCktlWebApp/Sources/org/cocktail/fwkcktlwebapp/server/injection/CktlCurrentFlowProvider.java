package org.cocktail.fwkcktlwebapp.server.injection;

import com.google.inject.Inject;
import com.google.inject.Provider;

public class CktlCurrentFlowProvider extends CktlFlowProvider implements Provider<CktlFlow> {

	private FlowManager flowManager;

	@Inject
	public CktlCurrentFlowProvider(FlowManager flowManager) {
		this.flowManager = flowManager;
	}

	public CktlFlow get() {
		String currentFlowName = flowManager.getCurrentFlowName();
		if (currentFlowName == null) {
            throw new IllegalStateException("Le nom du flow courant n'a pas pu être déterminé");
		}
		return provide(currentFlowName);
	}

}
