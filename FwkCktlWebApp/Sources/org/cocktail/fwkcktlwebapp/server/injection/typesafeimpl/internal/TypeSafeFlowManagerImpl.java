package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal;

import javax.inject.Inject;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlowComponentRegistry;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlowManager;

public class TypeSafeFlowManagerImpl implements TypeSafeFlowManager {

	private TypeSafeFlowComponentRegistry registry;
	private String currentFlowName;

	@Inject
	public TypeSafeFlowManagerImpl(TypeSafeFlowComponentRegistry registry) {
		this.registry = registry;
	}

	public void updateCurrentFlow(Class<? extends CktlWebComponent> componentClass) {
		String flowName = registry.lookup(componentClass);
		if (flowName != null) {
			this.currentFlowName = flowName;
		}
	}

	public String currentFlowName() {
		return currentFlowName;
	}

	protected void setCurrentFlowName(String currentFlowName) {
		this.currentFlowName = currentFlowName;
	}

}
