package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal.TypeSafeFlowImpl;

import com.google.inject.Provider;

public class TypeSafeFlowProvider implements Provider<TypeSafeFlow> {

	public TypeSafeFlow get() {
		return new TypeSafeFlowImpl();
	}

}
