package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

public interface TypeSafeFlowManager {

	void updateCurrentFlow(Class<? extends CktlWebComponent> componentClass);
	String currentFlowName();

}
