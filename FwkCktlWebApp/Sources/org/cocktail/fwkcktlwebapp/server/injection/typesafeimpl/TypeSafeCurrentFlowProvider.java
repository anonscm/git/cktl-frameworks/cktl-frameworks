package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import javax.inject.Inject;

import com.google.inject.Provider;

public class TypeSafeCurrentFlowProvider implements Provider<TypeSafeFlow> {

	private static final String UNDEFINED_FLOWNAME = "Le nom du flow courant n'a pas pu être déterminé";

	private TypeSafeFlowStore storage;
	private TypeSafeFlowManager manager;

	@Inject
	public TypeSafeCurrentFlowProvider(TypeSafeFlowStore storage, TypeSafeFlowManager manager) {
		this.storage = storage;
		this.manager = manager;
	}

	public TypeSafeFlow get() {
		String currentFlowName = manager.currentFlowName();

		if (currentFlowName == null) {
            throw new IllegalStateException(UNDEFINED_FLOWNAME);
		}

		return storage.get(currentFlowName);
	}

}
