package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal;

import java.lang.annotation.Annotation;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlow;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlowComponentRegistry;

import com.google.common.collect.Maps;
import com.google.inject.Key;

public class TypeSafeFlowComponentRegistryImpl implements TypeSafeFlowComponentRegistry {

	private Map<String, Key<TypeSafeFlow>> registry;

	public TypeSafeFlowComponentRegistryImpl() {
		// Cannot use Maps.<String, Key<TypeSafeFlow>>newHashMap() : Java 1.6 required
        registry = Collections.synchronizedMap(new HashMap<String, Key<TypeSafeFlow>>());
    }

	public void bind(Class<? extends CktlWebComponent> componentCallerClass, Class<? extends Annotation> flowAnnotationClass) {
        registry.put(componentCallerClass.getName(), createKey(flowAnnotationClass));
    }

    public String lookup(Class<? extends CktlWebComponent> componentCallerClass) {
    	if (componentCallerClass == null) {
    		return null;
    	}

    	return registry.get(componentCallerClass.getName()).toString();
    }

    protected Key<TypeSafeFlow> createKey(Class<? extends Annotation> flowAnnotationClass) {
    	return Key.get(TypeSafeFlow.class, flowAnnotationClass);
    }
}
