package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal;

import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlow;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlowStore;

import com.webobjects.appserver.WOSession;

import er.extensions.appserver.ERXSession;

public class TypeSafeFlowStoreImpl implements TypeSafeFlowStore {

	public TypeSafeFlow get(String flowName) {
		return (TypeSafeFlow) session().objectForKey(flowName);
	}

	private WOSession session() {
		return ERXSession.session();
	}

}
