package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal;

import java.lang.annotation.Annotation;

import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlow;

public class TypeSafeFlowImpl implements TypeSafeFlow {

	private String name;
	private Class<? extends Annotation> annotationType;

	public String name() {
		return name;
	}

	public void name(String name) {
		this.name = name;
	}

	public Class<? extends Annotation> annotationType() {
		return annotationType;
	}

	public void annotationType(Class<? extends Annotation> annotationType) {
		this.annotationType = annotationType;
	}

}
