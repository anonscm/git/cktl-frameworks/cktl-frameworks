package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import java.lang.annotation.Annotation;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

public interface TypeSafeFlowComponentRegistry {

	 void bind(Class<? extends CktlWebComponent> componentCallerClass, Class<? extends Annotation> flowAnnotationClass);
	 String lookup(Class<? extends CktlWebComponent> componentCallerClass);

}
