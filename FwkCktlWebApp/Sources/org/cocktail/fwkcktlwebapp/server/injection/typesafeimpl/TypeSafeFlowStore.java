package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

public interface TypeSafeFlowStore {

	TypeSafeFlow get(String flowName);

}
