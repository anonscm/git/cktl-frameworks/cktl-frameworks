package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.google.inject.Provider;
import com.google.inject.TypeLiteral;
import com.google.inject.spi.TypeEncounter;
import com.google.inject.spi.TypeListener;

public class TypeSafeFlowListener implements TypeListener {

	public <I> void hear(TypeLiteral<I> type, TypeEncounter<I> encounter) {
		Class<? super I> typeHolderClazz = type.getRawType();

		if (!isACktlWebComponent(typeHolderClazz)) {
			return;
		}

		for (Field field : typeHolderClazz.getDeclaredFields()) {
			if (isAStaticTypeFlowType(field.getType())) {
				hearFlowInjection(type.getRawType(), field, encounter);
			}
		}
	}

	private boolean isACktlWebComponent(Class<?> type) {
		return CktlWebComponent.class.isAssignableFrom(type);
	}

	private boolean isAStaticTypeFlowType(Class<?> type) {
	    return TypeSafeFlow.class.isAssignableFrom(type);
	}

	private <I> void hearFlowInjection(Class<? super I> clazz, Field field, TypeEncounter<I> encounter) {
		Annotation[] fieldAnnotations = field.getAnnotations();
		for (Annotation currentFieldAnnotation : fieldAnnotations) {
			Annotation[] internalAnnotations = currentFieldAnnotation.annotationType().getAnnotations();
			for (Annotation currentInternalAnnotation : internalAnnotations) {
				Class<? extends Annotation> annotationType = currentInternalAnnotation.annotationType();
				if (BindingFlow.class.equals(annotationType)) {
					Provider<TypeSafeFlowComponentRegistry> registryProvider = encounter.getProvider(TypeSafeFlowComponentRegistry.class);
					encounter.register(new TypeSafeFlowInjectionListener<I>(registryProvider, currentFieldAnnotation, clazz, field));
				}
			}
		}
	}

}
