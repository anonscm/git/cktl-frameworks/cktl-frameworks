package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import java.lang.annotation.Annotation;

public interface TypeSafeFlow {

	String name();
	void name(String name);

	Class<? extends Annotation> annotationType();
	void annotationType(Class<? extends Annotation> annotationType);

}
