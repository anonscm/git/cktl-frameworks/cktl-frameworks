package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.google.inject.Provider;
import com.google.inject.spi.InjectionListener;

public class TypeSafeFlowInjectionListener<T> implements InjectionListener<T> {

	private Provider<TypeSafeFlowComponentRegistry> registryProvider;
	private Annotation annotation;
	private Class<? super T> componentClass;
	private Field flowField;

	public TypeSafeFlowInjectionListener(Provider<TypeSafeFlowComponentRegistry> registryProvider, Annotation annotation, Class<? super T> componentClass, Field flowField) {
		this.registryProvider = registryProvider;
		this.annotation = annotation;
		this.componentClass = componentClass;
		this.flowField = flowField;
		this.flowField.setAccessible(true);
	}

	public void afterInjection(Object injectee) {
		try {
			TypeSafeFlow flow = (TypeSafeFlow) flowField.get(injectee);
			postConstructFlow(flow);
			register(flow);
		} catch (IllegalArgumentException iarge) {
			throw new RuntimeException(iarge);
		} catch (IllegalAccessException iacce) {
			throw new RuntimeException(iacce);
		}

	}

	private void postConstructFlow(TypeSafeFlow flow) {
		flow.name(annotation.annotationType().getName());
		flow.annotationType(annotation.annotationType());
	}

	@SuppressWarnings("unchecked")
	private void register(TypeSafeFlow flow) {
		TypeSafeFlowComponentRegistry registry = registry();

		if (registry == null) {
			return;
		}

		registry.bind((Class<CktlWebComponent>) componentClass, flow.annotationType());
	}

	private TypeSafeFlowComponentRegistry registry() {
		TypeSafeFlowComponentRegistry registry = registryProvider.get();
		return registry;
	}

}
