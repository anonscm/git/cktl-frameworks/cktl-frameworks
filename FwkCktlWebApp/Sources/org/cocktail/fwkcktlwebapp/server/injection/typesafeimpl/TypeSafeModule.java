package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import org.cocktail.fwkcktlwebapp.server.injection.CurrentFlow;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.flow.GestionAvenants;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.flow.GestionDocuments;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal.TypeSafeFlowComponentRegistryImpl;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal.TypeSafeFlowManagerImpl;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.internal.TypeSafeFlowStoreImpl;

import com.google.inject.AbstractModule;
import com.google.inject.Scopes;
import com.google.inject.matcher.Matchers;
import com.woinject.WOScopes;

public class TypeSafeModule extends AbstractModule {

	@Override
	protected void configure() {
		install(new StaticTypeCoreFlowModule());
		install(new StaticTypeFlowDeclarationsModule());
	}

	public static class StaticTypeCoreFlowModule extends AbstractModule {
		@Override
		protected void configure() {
			//sans interface : bind(StaticTypeFlowManager.class).in(WOScopes.REQUEST);

			bind(TypeSafeFlowManager.class).to(TypeSafeFlowManagerImpl.class).in(WOScopes.REQUEST);
			bind(TypeSafeFlowStore.class).to(TypeSafeFlowStoreImpl.class).in(WOScopes.SESSION);
			bind(TypeSafeFlowComponentRegistry.class).to(TypeSafeFlowComponentRegistryImpl.class).in(Scopes.SINGLETON);
			bind(TypeSafeFlow.class).annotatedWith(CurrentFlow.class).toProvider(TypeSafeCurrentFlowProvider.class).in(WOScopes.REQUEST);
			bindListener(Matchers.any(), new TypeSafeFlowListener());
		}
	}

	public static class StaticTypeFlowDeclarationsModule extends AbstractModule {
		@Override
		protected void configure() {
			bind(TypeSafeFlow.class).annotatedWith(GestionDocuments.class).toProvider(TypeSafeFlowProvider.class).in(WOScopes.SESSION);
			bind(TypeSafeFlow.class).annotatedWith(GestionAvenants.class).toProvider(TypeSafeFlowProvider.class).in(WOScopes.SESSION);
		}
	}
}
