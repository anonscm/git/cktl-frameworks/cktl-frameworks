package org.cocktail.fwkcktlwebapp.server.injection;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.google.inject.BindingAnnotation;

@Target({ FIELD, TYPE })
@Retention(RUNTIME)
@BindingAnnotation
public @interface InjectFlow {

	Class<? extends FlowName> name();

}
