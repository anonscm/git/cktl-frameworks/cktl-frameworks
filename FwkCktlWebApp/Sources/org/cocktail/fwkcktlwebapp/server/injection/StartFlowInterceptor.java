package org.cocktail.fwkcktlwebapp.server.injection;

import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;

public class StartFlowInterceptor implements MethodInterceptor {

	private FlowProvider flowProvider;

	public Object invoke(MethodInvocation invocation) throws Throwable {
		StartFlow startFlow = invocation.getMethod().getAnnotation(StartFlow.class);
		String flowName = startFlow.name().getName();

		Flow flow = flowProvider.provide(flowName);
		flow.start();

		return invocation.proceed();
	}

	public void setFlowProvider(FlowProvider flowProvider) {
		this.flowProvider = flowProvider;
	}
}
