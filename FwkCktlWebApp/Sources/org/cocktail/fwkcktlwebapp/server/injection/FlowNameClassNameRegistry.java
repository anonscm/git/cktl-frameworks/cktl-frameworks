package org.cocktail.fwkcktlwebapp.server.injection;

public interface FlowNameClassNameRegistry {

    void register(String flowName, Class<?> clazz);

}
