package org.cocktail.fwkcktlwebapp.server;

import java.util.ArrayList;
import java.util.List;

import com.google.inject.Module;
import com.webobjects.appserver.WOApplication;

/**
 * Chargé d'enregistrer tous les modules de l'application.
 * Les frameworks principaux récupèrent une instance de ce 
 * registre via l'objet {@link WOApplication}.
 * 
 * @author Alexis Tual
 *
 */
public class AllModulesRegister implements ModuleRegister {

    private List<Module> modules;
    
    /**
     * 
     */
    public AllModulesRegister() {
        modules = new ArrayList<Module>();
    }
    
    /** 
     * {@inheritDoc}
     */
    public List<? extends Module> modules() {
        return modules;
    }

    /** 
     * {@inheritDoc}
     */
    public void addModule(Module module) {
        this.modules.add(module);
    }
    
}
