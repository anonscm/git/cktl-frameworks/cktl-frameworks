package org.cocktail.fwkcktlwebapp.server;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.GEDClient;
import org.cocktail.fwkcktlwebapp.common.util.GEDDescription;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;


/**
 * Propose une implementation des fonctions de base permettant de communiquer
 * avec le serveur de gestion de fichier (GEDFS). Cette implementation utilise
 * les classes {@link GEDClient} et {@link GEDDescription}.
 * Les fonctions de la classe correspondent a l'utilisation la plus
 * commune des clients GED.
 * 
 * @see GEDClient
 * @see GEDDescription
 */
public class CktlGedBus {
  /**
   * Le gestionnaire de la configuration de l'application.
   */
  private CktlConfig config;
  
  /**
   * Le client de communication avec la base de donnees. 
   */
  private GEDClient gedClient;
  
  /**
   * Le message obtenu apres la derniere utilisation des fonctions
   * de la classe, s'il existe.
   */
  private String gedMessage;
  
  /**
   * L'identifiant de l'utilisateur accedant au serveur de documents.
   */
  private int noIndividu;
  
  /**
   * Creer un nouveau objet de la communication avec le serveur
   * de documents. L'objet sera initialise en fonction de la configuration
   * de l'application. En particulier, les parametres avec les cles
   * <code>GEDFS_SERVICE_HOST</code> et <code>GEDFS_SERVICE_PORT</code> sont
   * utilises. Si l'initialisation echoue, la methode {@link #gedMessage()}
   * retourne le message d'erreur.
   * 
   * @param config Le gestionnaire de la configuration de l'application.
   * 
   * @see CktlConfig
   */
  public CktlGedBus(CktlConfig config) {
    this.config = config;
    this.noIndividu = -1;
    if (!initGEDClient()) CktlLog.log(gedMessage);
  }
  
  /**
   * Retourne l'identifiant le l'application qui utilise cette instance
   * de {@link CktlGedBus}. Cette methode utilise le parametre de
   * configuration "APP_ID". Si ce parametre n'est pas definit
   */
  private String appId() {
    String id = config.stringForKey(CktlConfig.CONFIG_APP_ID_KEY);
    if (id == null) return "UNKNOWN";
    return id;
  }

  /**
   * Initialise le client. L'initialisation utilise les parametres
   * de configuration <code>GEDFS_SERVICE_HOST</code> et
   * <code>GEDFS_SERVICE_PORT</code>. Si l'initialisation echoue,
   * la methode {@link #gedMessage()} retourne le message d'erreur.
   * 
   * @return <code>false</code> si l'initialisation echoue et <code>true</code>
   *   dans le cas contraire.  
   */
  private boolean initGEDClient() {
    if (gedClient != null) {
      gedMessage = null;
      return true;
    }
    String nom 	= config.stringForKey(CktlConfig.CONFIG_GEDFS_SERVICE_HOST_KEY);
    String port = config.stringForKey(CktlConfig.CONFIG_GEDFS_SERVICE_PORT_KEY);
    if ((nom == null) || (port == null)) {
      gedMessage = "Erreur d'initialisation du service GEDFS ("+CktlConfig.CONFIG_GEDFS_SERVICE_HOST_KEY+", "+CktlConfig.CONFIG_GEDFS_SERVICE_PORT_KEY+")";
      return false;
    }
    gedClient = new GEDClient(nom, StringCtrl.toInt(port, -1), -1, appId());
    return true;
  }
  
  /**
   * Definit l'identifiant de l'utilisateur qui va effectuer les operations
   * avec le serveur GEDFS. Pour les documents nouvelementt enregistres,
   * il represente le proprietaire des documents.
   */
  public void setNoIndividu(int noIndividu) {
    this.noIndividu = noIndividu;
  }

  /**
   * Enregistre un nouveau document. Le contenu du document est represente
   * comme un tableau d'octets. Cette opration renvoie l'identifiant du
   * document qui doit etre memorise par l'application afin de pouvoir
   * y acceder ulterieurement. 
   * 
   * @param donnees Le contenue du document.
   * @param titre Le titre qui sera enregistre avec le document sur le serveur
   *   des documents.
   * @param nom Le nom de fichier dans lequel le contenu de document est
   *   enreistre.
   * @param categorie La categorie auquel le document est associe. Le document
   *   est enregistre dans l'arborescsence des docuemnts de cette categorie.
   *  
   * @return L'identifiant de document ou -1 si l'operation a echoue. Dans ce
   *   cas la methode {@link #gedMessage()} renvoie le message d'erreur survenue.
   */  
  public int saveDocumentGED(byte[] donnees, String titre, String nom, String categorie) {
    int noDocument;
    if (!initGEDClient()) return -1;
    synchronized(gedClient) {
      gedClient.reset();
      gedClient.setUserId(noIndividu);
      noDocument = gedClient.enregistrerDocument(donnees, titre, nom, categorie);
      if (noDocument == -1) gedMessage = "Erreur lors de la sauvegarde de document :\n"+gedClient.lastMessage();
      return noDocument;
    }
  }

  /**
   * Enregistre un nouveau document. Le document est indique par le nom de
   * fichier. Cette opration renvoie l'identifiant du document qui doit etre
   * memorise par l'application afin de pouvoir y acceder ulterieurement.
   * 
   * @param path Le chemin d'acces au fichier ou se trouve un document.
   * @param titre Le titre qui sera enregistre avec le document sur le serveur
   *   des documents.
   * @param categorie Le nom de fichier dans lequel le contenu de document est
   *   enreistre.
   * 
   * @return L'identifiant de document ou -1 si l'operation a echoue. Dans ce
   *   cas la methode {@link #gedMessage()} renvoie le message d'erreur survenue.
   */
  public int saveDocument(String path, String titre, String categorie) {
    int noDocument;
    if (!initGEDClient()) return -1;
    gedClient.reset();
    gedClient.setUserId(noIndividu);
    noDocument = gedClient.enregistrerDocument(path, titre, categorie);
    if (noDocument == -1) gedMessage = "Erreur lors de la sauvegarde de document :\n"+gedClient.lastMessage();
    return noDocument;
  }

  /**
   * Recupere les information relatives au document ayant l'identifiant indique.
   * Les information sont retournees dans un objet {@link GEDDescription}.
   * En general, le champ <code>reference</code> de l'objet
   * {@link GEDDescription} contient le URL d'acces au document.
   *  
   * @param noDocument L'identifiant de document.
   * 
   * @return L'objet {@link GEDDescription} ou <code>null</code> dans le cas d'echeque.
   *   Dans ce cas, la methode {@link #gedMessage()} contient le message d'erreur.
   */
  public GEDDescription inspectDocumentGED(int noDocument) {
    if (!initGEDClient()) return null;
    synchronized(gedClient) {
      gedClient.reset();
      gedClient.setUserId(noIndividu);
      if (gedClient.inspecterDocument(noDocument))
        return gedClient.description();
      else {
        gedMessage = "Erreur de communication avec le service GEDFS :\n"+gedClient.lastMessage();
        return null;
      }
    }
  }

  /**
   * Supprime le document avec l'identifiant donne. Retourne <code>true</code> dans
   * le cas de succes et <code>false</code> dans le cas d'echec.
   */
  public boolean deleteDocumentGED(int noDocument) {
    if (!initGEDClient()) return false;
    synchronized(gedClient) {
      gedClient.reset();
      gedClient.setUserId(noIndividu);
      if (gedClient.supprimerDocument(noDocument))
        return true;
      else {
        gedMessage = "Erreur de communication avec le service GEDFS :\n"+gedClient.lastMessage();
        return false;
      }
    }
  }
  
  /**
   * Retourne le message de la derniere erreur ou <code>null</code> si aucune erreur
   * n'est survenue. 
   */
  public String gedMessage() {
    return gedMessage;
  }

  /**
   * Retourne la description correspondant a la derniere operation effectue.
   */
  public GEDDescription gedDescription() {
    if (gedClient == null) return null;
    return gedClient.description();
  }
}
