package org.cocktail.fwkcktlwebapp.server.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.version.app.VersionAppFromVersionProperties;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSData;

import er.extensions.appserver.ERXRequest;

public class CktlJnlpGenerator {
	public static final Logger LOGGER = Logger.getLogger(CktlJnlpGenerator.class);
	private static final CktlWebApplication CKTL_APP = (CktlWebApplication) WOApplication.application();

	private String cgiBase;
	private String webBase;
	private WOContext context;

	public CktlJnlpGenerator(WOContext context, String cgiBase, String webBase) {
		this.context = context;
		this.cgiBase = cgiBase;
		this.webBase = webBase;

	}

	public String genererJnlpContent() throws Exception {
		try {
			checkJnlpTemplateFile();
			String jnlpTemplate = getJnlpTemplateContent();
			LOGGER.debug("Contenu du template JNLP");
			LOGGER.debug(jnlpTemplate);

			Map<String, String> jnlpProcessorParams = new HashMap<String, String>();
			jnlpProcessorParams.put(CktlJnlpTemplateProcessor.CGI_APPLI_URL_KEY, cgiBase());
			jnlpProcessorParams.put(CktlJnlpTemplateProcessor.WEB_APPLI_URL_KEY, webBase());

			CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(jnlpProcessorParams);
			String jnlpContent = processor.processTemplate(jnlpTemplate);
			LOGGER.debug(jnlpContent);
			return jnlpContent;
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * @return the cgiBase
	 */
	public String cgiBase() throws Exception {
		if (cgiBase == null) {
			genereDefaultCgiBase();
		}
		return cgiBase;
	}

	/**
	 * @param cgiBase the cgiBase to set
	 */
	public void setCgiBase(String cgiBase) {
		this.cgiBase = cgiBase;
	}

	/**
	 * @return the webBase
	 */
	public String webBase() throws Exception {
		if (webBase == null) {
			genereDefaultWebBase();
		}
		return webBase;
	}

	private void genereDefaultWebBase() throws Exception {
			setWebBase(getUrlServeurWeb());

	}

	private void genereDefaultCgiBase() throws Exception {
			setCgiBase(getUrlServeurCgi());
	}

	/**
	 * @param webBase the webBase to set
	 */
	public void setWebBase(String webBase) {
		this.webBase = webBase;
	}

	private String getUrlServeurWeb() throws Exception {
		if (getUrlServeurCgi() != null) {
			return getUrlServeurCgi().replaceAll("cgi-bin/", "");
		}
		return null;
	}

	public WOContext context() {
		return context;
	}

	private String getUrlServeurCgi() throws Exception {
		return urlServerGci(context());
	}

	public static String urlServerGci(WOContext context) {
		String urlServeurCGI = context.completeURLWithRequestHandlerKey(null, null, null, ERXRequest.isRequestSecure(context.request()), 0);
		try {
			if (urlServeurCGI == null) {
				throw new Exception("Impossible de recuperer le nom du serveur cgi");
			}
			//Nettoyer le n° d'instance s'il est present
			int i = urlServeurCGI.lastIndexOf(".woa");
			urlServeurCGI = urlServeurCGI.substring(0, i + 4);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return urlServeurCGI;
	}

	public File getJnlpTemplateFile() {
		NSBundle mainBundle = NSBundle.mainBundle();
		URL bundlePathURL = mainBundle.bundlePathURL();
		String pathToFile = bundlePathURL.getPath() + "/Contents/WebServerResources/" + getJnlpFileName();
		return new File(pathToFile);
	}

	/**
	 * @return Le nom du fichier JNLP, en se basant sur le nom du projet, et non celui de l'instance... À moins d'être en dev...<br>
	 *         Ex : on retournera toujours Bibasse.jnlp, même si l'instance est BibasseTEST.woa
	 */
	public String getJnlpFileName() {
		String realName = null;
		try {
			// Fichier version.properties
			realName = (new VersionAppFromVersionProperties(CKTL_APP)).projetVersion();
		} catch (Exception e) {
			// Nom de l'instance
			realName = CKTL_APP.getName();
		}
		return realName + ".jnlp";
	}

	protected String getJnlpTemplateContent() throws IOException {
		LOGGER.debug("Récupération du fichier " + getJnlpTemplateFile().getAbsolutePath());
		return FileCtrl.fileToString(getJnlpTemplateFile());
	}

	public void checkJnlpTemplateFile() throws Exception {
		File jnlpFile = getJnlpTemplateFile();
		if (!jnlpFile.exists()) {
			throw new FileNotFoundException("Le fichier " + jnlpFile.getAbsolutePath() + " n'existe pas.\n");
		}
		if (!jnlpFile.canRead()) {
			throw new FileNotFoundException("Le fichier " + jnlpFile.getAbsolutePath() + " ne peut être lu.\n");
		}
	}

	public WOActionResults genereJnlpResponse(String jnlpContent, String jnlpFileName) throws Exception {
		if (jnlpContent == null) {
			jnlpContent = genererJnlpContent();
		}
		if (jnlpFileName == null) {
			jnlpFileName = getJnlpFileName();
		}

		WOResponse jnlpResponse = new WOResponse();

		// Pour IE
		jnlpResponse.disableClientCaching();
		jnlpResponse.removeHeadersForKey("Cache-Control");
		jnlpResponse.removeHeadersForKey("pragma");

		jnlpResponse.setHeader("attachement; filename=\"" + jnlpFileName + "\"", "Content-Disposition");
		jnlpResponse.setHeader(Integer.toString(jnlpContent.length()), "content-length");
		jnlpResponse.appendContentData(new NSData(jnlpContent, "utf-8"));
		//ancien type, ne permet pas l'execution par defaut
		//jnlpResponse.setHeader("x-eojavaclient-message", "Content-type");
		jnlpResponse.setHeader("application/x-java-jnlp-file", "Content-type");

		return jnlpResponse;
	}

}
