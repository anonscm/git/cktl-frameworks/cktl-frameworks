package org.cocktail.fwkcktlwebapp.server.util;

import java.util.Map;

import org.apache.log4j.Logger;

import er.extensions.foundation.ERXStringUtilities;

public class CktlJnlpTemplateProcessor {
	public static final Logger log = Logger.getLogger(CktlJnlpTemplateProcessor.class);
	public static final String WEB_APPLI_URL_KEY = "webAppliUrl";
	public static final String CGI_APPLI_URL_KEY = "cgiAppliUrl";
	public static final String CODEBASE_FILTER = "codebase=\"*\"";
	public static final String APPLICATION_URL_FILTER = "-applicationURL";
	public static final String CODEBASE_NOT_FOUND_MSG = "La chaine de caractère " + CODEBASE_FILTER + " n'a pas été trouvée dans le template";
	public static final String APPLICATION_URL_FOUND_MSG = "La chaine de caractère " + APPLICATION_URL_FILTER + " n'a pas été trouvée dans le template";

	Map<String, String> params;

	public CktlJnlpTemplateProcessor(Map<String, String> params) {
		this.params = params;
	}

	public String processTemplate(String jnlpTemplate) throws Exception {
		if (ERXStringUtilities.stringIsNullOrEmpty(params.get(CGI_APPLI_URL_KEY))) {
			throw new NullPointerException("L'adresse CGI de l'application (sur le serveur d'application) est obligatoire.");
		}
		if (ERXStringUtilities.stringIsNullOrEmpty(params.get(WEB_APPLI_URL_KEY))) {
			throw new NullPointerException("L'adresse WEB de l'application (sur le serveur web) est obligatoire.");
		}
		if (ERXStringUtilities.stringIsNullOrEmpty(jnlpTemplate)) {
			throw new NullPointerException("Le contenu du template JNLP est vide.");
		}
		String jnlpContent = jnlpTemplate;
		jnlpContent = processWebAppliUrl(jnlpContent, params.get(WEB_APPLI_URL_KEY));
		jnlpContent = processCgiAppliUrl(jnlpContent, params.get(CGI_APPLI_URL_KEY));
		log.debug("processTemplate = " + jnlpContent);
		return jnlpContent;

	}

	protected String processWebAppliUrl(String jnlpTemplate, String webAppliUrl) throws Exception {
		if (jnlpTemplate.indexOf(CODEBASE_FILTER) == -1) {
			throw new Exception(CODEBASE_NOT_FOUND_MSG + " - " + jnlpTemplate);
		}
		String jnlpContent = jnlpTemplate.replace(CODEBASE_FILTER, "codebase=\"" + webAppliUrl + "\"");
		log.debug("processWebAppliUrl = " + jnlpContent);
		return jnlpContent;
	}

	protected String processCgiAppliUrl(String jnlpContent, String cgiAppliUrl) throws Exception {
		if (jnlpContent.indexOf(APPLICATION_URL_FILTER) == -1) {
			throw new Exception(APPLICATION_URL_FOUND_MSG + " - " + jnlpContent);
		}

		int i = jnlpContent.indexOf("-applicationURL");
		String debut = jnlpContent.substring(0, i);
		int j = jnlpContent.indexOf("*") + 1;
		//On isole la partie qui correspond à applicationURL + *
		String aRemplacer = jnlpContent.substring(i, j);
		String fin = jnlpContent.substring(j);
		aRemplacer = aRemplacer.replace("*", cgiAppliUrl);
		jnlpContent = debut.concat(aRemplacer).concat(fin);
		log.debug("processCgiAppliUrl = " + jnlpContent);

		return jnlpContent;
	}

}
