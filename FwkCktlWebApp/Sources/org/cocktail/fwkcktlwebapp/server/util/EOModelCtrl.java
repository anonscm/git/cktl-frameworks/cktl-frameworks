package org.cocktail.fwkcktlwebapp.server.util;

import java.util.Map;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Classe d'utilitaires sur les modeles WebObjects
 */
public class EOModelCtrl {

	public static final String DEFAULT_URL = "n/a";
    public static final String KEY_URL = "URL";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_PASSWORD = "password";
// le cache d'information sur les modeles
  private static NSMutableDictionary modelsDico = new NSMutableDictionary(); 
	
  /**
   * Lancer les operations de lecture des informations sur les
   * modeles utilises par l'application
   */
  public static void scanModels() {
    modelsDico.removeAllObjects();
    final EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
    for (int i = 0; i < vModelGroup.models().count(); i++) {
    	final EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
    	modelsDico.takeValueForKey(tmpEOModel, tmpEOModel.name());
    }
  }
  
  public static NSMutableDictionary getModelsDico() {
  	return modelsDico;
  } 
  
  /**
   * Renvoie l'url de connexion ala base de donn&eacute;es pour le modele sp&eacute;cifi&eacute; en parametre.
   *
   * @param model
   */
  public static String bdConnexionUrl(EOModel model) {
  	Map<String, Object> vDico = model.connectionDictionary();
  	String url = (String)vDico.get(KEY_URL);
  	if (url==null) {
  		url = DEFAULT_URL;
  	}
  	return url;
  }


  /**
   * Renvoie le user base de donn&eacute;es
   *
   * @param model
   */
  public static String bdConnexionUser(EOModel model) {
  	Map<String, Object> vDico = model.connectionDictionary();
  	return (String)vDico.get(KEY_USERNAME);
  }

  /**
   * Renvoie le serverid de la base de donn&eacute;es (par exemple gest).
   *
   * @param model
   */
  public static String bdConnexionServerId(EOModel model) {
  	String url = bdConnexionUrl(model);
  	String serverUrl=null;
  	String serverBdName=null;
  	//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
  	//On separe la partie jdbc de la partie server
  	String[] parts = url.split("@");
  	if ( parts.length > 1 ) {
  		serverUrl = parts[1];
  		parts = serverUrl.split(":");
  		if (parts.length>1) {
  			serverBdName = parts[parts.length - 1];
  		}
  	} else {
  		parts = url.split("/");
  		if (parts.length>1) {
  			serverBdName = parts[parts.length - 1];
  		}	
  	}
  	return serverBdName;
  }

  /**
   * Renvoie le serverid de la base de donn&eacute;es (par exemple jane).
   *
   * @param model
   */
  public static String bdConnexionServerName(EOModel model) {
  	String url = bdConnexionUrl(model);
  	String serverUrl=null;
  	String serverName=null;
  	//L'url est du type jdbc:oracle:thin:@caporal.univ-lr.fr:1521:gestlcl
  	//On separe la partie jdbc de la partie server
  	String[] parts = url.split("@");
  	if ( parts.length > 1 ) {
  		serverUrl = parts[1];
  		parts = serverUrl.split(":");
  		if (parts.length>0) {
  			serverName = parts[0];
  		}
  	}
  	return serverName;
  }

  /**
 * @param model un eomodel
 * @return le password du dictionnaire de connexion <param>model</param>
 */
public static String bdConnexionPassword(EOModel model) {
      Map<String, Object> vDico = model.connectionDictionary();
      return (String)vDico.get(KEY_PASSWORD);
  }

	
}
