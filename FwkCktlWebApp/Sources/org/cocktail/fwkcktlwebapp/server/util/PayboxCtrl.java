package org.cocktail.fwkcktlwebapp.server.util;

import java.math.BigDecimal;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WOSession;

/**
 * Pour les application cliente qui vont utiliser <i>Paybox</i>, il est necesaire
 * d'utiliser les methodes statiques de cette classe pour creer un paiement CB
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */

public class PayboxCtrl {

	/**
	 * Creer un nouveau paiement CB et rediriger le navigateur source vers www.paybox.com
	 * @param app : le pointeur sur la classe Application (doit heriter de CktlWebApplication)
	 * @param session : le pointeur sur la classe Session
	 * @param context
	 * @param appCode : code de l'application cliente
	 * @param somme : somme a payer par le client
	 * @param email : l'adresse email du client (pour recepetion du ticket de la transaction)
	 * @param numCommande : numero interne a l'application cliente<br>
	 * @return la page HTML qui redirige vers www.paybox.com
	 */
	public static WOComponent getComponentToAppliPaybox(
			CktlWebApplication app, WOSession session, WOContext context, String appCode, BigDecimal somme, String email, Number numCommande) {
		WORedirect redirect = new WORedirect(context);

		String reference = numCommande.intValue() + appCode;
		
		// on complete la somme avec un .0 si absent
		String sommeStr = somme.toString();
		if (sommeStr.indexOf(".") == -1) {
		    sommeStr += ".00";
		}
		
		// on colle 'gar' au IBS_CMD pour identifier la table paiement de garnuche
		String URL = app.config().stringForKey(CktlConfig.CONFIF_ENCAISSEMENT_WEB) + 
					"email=" + email+
					"&IBS_CMD=" + reference +
					"&IBS_TOTAL=" + somme +
 					"&idsession=" + session.sessionID() + 
 					"&idappli=" + app.getApplicationURL(context);

		CktlLog.log("PayboxCtrl - Redirection vers "+URL);
		redirect.setUrl(URL);
		return (WOComponent)redirect;
	}
}
