package org.cocktail.fwkcktlwebapp.server;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WODirectAction;
import com.webobjects.appserver.WORequest;



/**
 * Cette classe implemente une action permettant de tester si l'application
 * repond aux requetes HTTP. Ainsi, l'appel a une URL de type :
 * 
 * <div class="example"><code>http://.../MonApplication.woa/wa/WOAliveCheck</code></div>
 * 
 * <p>return la reponse "OK" si l'application repond (elle est en vie). Sinon, cet
 * appel doit produire une exception ou ne donner aucune reponse.</p>
 * 
 * <p>Cette action ne cree aucune session dans une instance d'une application.
 * Elle peut etre utilisee comme une alternative d'appel a la page
 * d'accueil de l'application, car ce dernier peut parfois creer une session
 * chaque fois qu'on y accede.</p>
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public class WOAliveCheck extends WODirectAction {

  /**
   * Cree un objet representation l'action qui traitera la requete
   * <code>request</code>.  
   */
  public WOAliveCheck(WORequest request) {
    super(request);
  }

  /**
   * Cette methode retourne la reponse contenant le texte "OK" pour indiquer
   * que l'application reponde aux requetes HTTP.
   */
  public final WOActionResults defaultAction() {
    CktlDataResponse resp = new CktlDataResponse();
    resp.setContent("OK");
    resp.setContentType(CktlDataResponse.MIME_TXT);
    return resp;
  }
}
