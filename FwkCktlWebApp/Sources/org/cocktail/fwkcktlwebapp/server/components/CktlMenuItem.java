package org.cocktail.fwkcktlwebapp.server.components;

/*
 * Copyright Université de La Rochelle 2006
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */


  /**
   * Cette classe represente une entree de menu. Chaque element visible
   * de menu correspond a un objet de cette classe.
   */

public class CktlMenuItem {

  /**
     * L'identifiant d'element de menu de niveau supperieur
     * auquele cet element appartient. -1 si l'element fait
     * partie du menu principal (premier niveau).
     */
    public int parentId;
    
    /**
     * L'identifiant de l'element.
     */
    public int id;
    
    /**
     * Le libelle de l'element.
     */
    public String label;
    
    /**
     * Le message affiche lorsque le courseur de la sourie
     * se positionne au-dessus de l'element.
     * Dans un document HTML, ceci correspond a la valeur de l'attribut
     * <code>title</code>.  
     */
    public String tip;
    
    /**
     * Le nom de la fenetre de navigateur dans laquelle le composant
     * correspondant a l'entree de menu sera affiche.  
     * Dans un document HTML, ceci correspond a la valeur de l'attribut
     * <code>target</code>.  
     */
    public String target;

    /**
     * L'index parmi les liste des sous-menus, du dernier selectionnee. Ce parametre
     * n'est pris en compte que si le binding <code>keepLastSelection</code>
     * du CktlMenu est a <code>true</code>
     */
    public int lastSelectionIndex;
        
    /**
     * Cree une nouvelle qui peut etre ajoutee dans un menu.
     */
    public CktlMenuItem(int newParentId, int newId, String newLabel,
                       String newTip, String newTarget) {
      parentId = newParentId;
      id = newId;
      label = newLabel;
      if (newTip == null) tip = "";
      else tip = newTip;
      if (newTarget == null) target = "";
      else target = newTarget;
      lastSelectionIndex = 0;
    }


  
}
