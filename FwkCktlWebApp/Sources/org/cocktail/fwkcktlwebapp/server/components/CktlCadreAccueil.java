package org.cocktail.fwkcktlwebapp.server.components;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOComponent;

public class CktlCadreAccueil extends WOComponent {
	
    private static final long serialVersionUID = -667122430335345808L;
    
	public static final String BINDING_DESCRIPTION_APPLICATION = "descriptionApplication";
	public static final String BINDING_SOUS_TITRE_APPLICATION = "sousTitreApplication";
	public static final String BINDING_SRC_IMAGE = "srcImage";
	public static final String BINDING_TITRE_APPLICATION = "titreApplication";
	
    public CktlCadreAccueil(WOContext context) {
        super(context);
    }
	
	public String titreApplication() {
		return valueForStringBinding(BINDING_TITRE_APPLICATION, "");
	}
	
	public void setTitreApplication(String titreApplication) {
		setValueForBinding(titreApplication, BINDING_TITRE_APPLICATION);
	}
	
	public String sousTitreApplication() {
		return valueForStringBinding(BINDING_SOUS_TITRE_APPLICATION, "");
	}
	
	public void setSousTitreApplication(String sousTitreApplication) {
		setValueForBinding(sousTitreApplication, BINDING_SOUS_TITRE_APPLICATION);
	}
	
	public String descriptionApplication() {
		return valueForStringBinding(BINDING_DESCRIPTION_APPLICATION, "");
	}
	
	public void setDescriptionApplication(String descriptionApplication) {
		setValueForBinding(descriptionApplication, BINDING_DESCRIPTION_APPLICATION);
	}
	
	public String srcImage() {
		return valueForStringBinding(BINDING_SRC_IMAGE, "");
	}
	
	public void setSrcImage(String srcImage) {
		setValueForBinding(srcImage, BINDING_SRC_IMAGE);
	}
	
	public Boolean hasSrcImage() {
		return !StringCtrl.isEmpty(srcImage());
	}
}