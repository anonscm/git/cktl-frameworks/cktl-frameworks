package org.cocktail.fwkcktlwebapp.server.components;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/*
 * Copyright CRI - Universite de La Rochelle, 2002-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */


/**
 * Ce composant facilite la creation des pages HTML avec le style par defaut.
 * 
 * <p>
 * Une page {@link CktlDefautPage} inclut donc les definitions des styles
 * et des icones par defaut. Elle permet de definir un titre a la page, de 
 * preciser une action javascript a executer, d'une ancre a atteindre au prochain
 * chargement et enfin d'inclure les sources d'un script java pour l'affichage 
 * d'infobulles (si elles sont utilisees dans la page).
 * 
 * <p>
 * <font class="importantNote">{@link CktlDefautPage} peut afficher automatiquement
 * un bas de page {@link CktlHTMLFooter}. Vous devez ndiquer explicitement si vous 
 * souhaitez l'utiliser (binding <code>hideFooter=true</code>).</font>
 * 
 * <p>
 * Le composant {@link CktlDefautPage} represente une <i>page complete</i>,
 * i.e., il contient les balises du document HTML (<code>&lt;html&gt;</code>),
 * de son entete (<code>&lt;head&gt;</code>) et de son corps
 * (<code>&lt;body&gt;</code>). Le document dans lequel vous utilisez ce
 * composant doit donc etre un <i>document partiel</i>.
 * </p>
 * 
 * <p>
 * Ce composant peut s'adapter automatiquement a un environnement encapsule
 * type ENT (iframe). Si la variable globale &quot;GRHUM_DOMAINE_PRINCIPAL&quot; existe, alors
 * un script javascript va tenter d'appeler la methode <code>parent.adapteENT(window)</code>
 * qui effectuera le travail de remplacement de la feuille de style, ainsi que redimensionner
 * l'iframe pour un affichage complet de la page (sans ascenceurs).
 * </p>
 * 
 * <p> 
 * Conseils d'utilisation dans une application Web :
 * <ul>
 * 	<li>Creer un unique composant conteneur, qui est dans une {@link CktlDefautPage}.
 * 			Votre page web doit heriter de {@link CktlWebComponent}. Si vous heritez de
 * 			la classe {@link CktlWebPage}, vous allez rajouter une couche "fullpage" qui
 * 			peut poser des problemes.</li>
 * 	<li>Affectez les binding a la {@link CktlDefautPage} de votre page conteneur.
 * 			Si besoin, faites passer les parametres en utilisant comme passerelle votre
 * 			page conteneur, pour parametrer votre {@link CktlDefautPage} (via le symbole "^")</li>
 * </ul>
 * </p>
 * 
 * <p>
 * <font class="importantNote">Le composant peut etre parametre via binding, ou bien
 * bien manuellement via les setters. Pour un meme nom de variable, la priorite sera
 * donnee aux bindings. Sinon, ce seront les valeurs manuelles qui seront prises en
 * compte.
 * </font>
 * </p>
 * 
 * <h3>Bindings</h3>
 * 
 * <p>
 *  <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *	 <tr align="center" class="paramBoxHeader">
 *	  <td>Connecteur</td>
 *	  <td>Definition</td>
 *	  <td>Valeur</td>
 *	  <td>Description</td>
 *	 </tr>
 *	  <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>onLoad</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Un script javascript a inserer dans le champ <code>
 *	  onLoad</code> entre les balises <code>&lt;body&gt;</code> et <code>&lt;/body&gt;</code> 
 *	  de la page.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>targetPosition</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *   Definit la position dans la page HTML ou le navigater Web doit etre
 *	 repositionne. La valeur <code>position</code> correspond a la valeur d'une
 * balise <code>&lt;a name="position"&gt;...&lt;/a&gt;</code>.
 *	  <td align="left">Un nom d'ancre HTML a atteindre lors du prochain rechargement
 *	  de la page.  La valeur <code>position</code> correspond a la valeur d'une
 * balise <code>&lt;a name="position"&gt;...&lt;/a&gt;</code>.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>pageTitle</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le titre de la page HTML. Cette valeur est incluse entre
 *	  les balises <code>&lt;title&gt;</code> et <code>&lt;/title&gt;</code> de
 *	  l'entete.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>useToolTip</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>boolean</code><br>[<i>false</i>]</td>
 *	  <td align="left">Indique s'il faut charger les sources JavaScript pour le
 *	  composant {@link CktlToolTip} - le gestionnaire d'info bulles.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	 	<td><code>ignoreDefaultStyles</code></td>
 *	 	<td>optionnel</td>
 *	  <td><code>boolean</code><br>
 *	 		[<i>false</i>]</td>
 *	  <td align="left">Indique s'il faut ignorer les definitions par defaut des icones d'une pages HTML. </td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	 	<td><code>ignoreDefaultLogos</code></td>
 *	 	<td>optionnel</td>
 *	 	<td><code>boolean</code><br>
 *	 		[<i>false</i>]</td>
 *	 	<td align="left">Indique s'il faut ignorer les definitions par defaut des styles CSS.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	 	<td><code>hideFooter</code></td>
 *	 	<td>optionnel</td>
 *	 	<td><code>boolean</code><br>
 *	 		[<i>true</i>]</td>
 *	 	<td align="left">Indique s'il faut cacher le bas de page par defaut inclus 
 *			dans une page (un composant {@link CktlHTMLFooter})</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	 	<td><code>contactMail</code></td>
 *	 	<td>optionnel</td>
 *	 	<td><code>String</code><br>
 *	 		[<i>null</i>]</td>
 *	 	<td align="left">Definit l'adresse email du lien de &quot;contact&quot; dans la bas d'une page HTML. <br>
 *	 	N'est pris en compte que si <code>hideFooter</code> est <code>true</code> </td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	 	<td><code>mailLabel</code></td>
 *	 	<td>optionnel</td>
 *	 	<td><code>String</code><br>
 *	 		[<i>null</i>]</td>
 *	 	<td align="left">D&eacute;finit le texte a afficher a cote de lien email dans le bas de la page.<br>
 *	 		N'est pris en compte que si <code>hideFooter</code> est <code>true</code></td>
 *	 </tr>
 *	</table>
 * </p>
 * 
 * @see CktlWebPage
 */
public class CktlDefaultPage extends CktlWebPage {

  /**
   * Cree un nouveau composant. 
   */
  public CktlDefaultPage(WOContext context) {
    super(context);
  }

  private final static String jsParentFolder = "jscript/";
  private final static String ToolTipScriptName = "wz_tooltip.js";
  private final static String KEY_DOMAIN = "GRHUM_DOMAINE_PRINCIPAL";
  
  /** le domaine principal est initialise 1 seul fois (modele singleton */
  private static String jsInstructionDomain;
  
  public void appendToResponse(WOResponse response, WOContext context) {
  	super.appendToResponse(response, context);
  	
    // insertion du tool tip si necessaire
    if (isUseToolTip())
      addScriptResource(response, jsParentFolder + ToolTipScriptName, null, fwkCktlWebAppFrameworkName(), 
          RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_BODY, RESOURCE_FROM_WEB_SERVER_RESOURCES);
    
  	
    // 
  	addScriptResource(response, null, jsInstructionDomain(), null, 
  			RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_BODY, RESOURCE_FROM_WEB_SERVER_RESOURCES);
  }

  /**
   * Renvoie la valeur <code>false</code>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe {@link WOComponent} (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  /**
   * Renvoie la valeur <code>true</code>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe {@link WOComponent} (WebObjects).
   */
  public boolean isStateless() {
    return true;
  }
  
  /**
   * La methode a implementer par un composant "stateless".
   */
  public void reset() {
    // Appeler les methodes de la super classe pour annuler
    // toute les modifications
    setIgnoreDefaultStyles(false);
    setIgnoreDefaultLogos(false);
  }

  // l'instruction pour le redimensionnnement automatique de la fenetre
  // lors de l'affichage en iFrame dans l'ENT
  private final static String JS_INSTRUCTION_ENT = 
  	"document.domain = '$1';\ntry {\n"+"	parent.adapteENT(window);\n}\n	catch(err){\n}\n";
  
  /**
   * Retourne le code javascript qui permet a l'application
   * de recuperer la css et de se redimensionner pour un affichage
   * dans l'ENT. 
   * Si le parametre <code>GRHUM_DOMAINE_PRINCIPAL</code> n'est pas
   * renseigne, alors cette instruction est vide.
   */
  public String jsInstructionDomain() {
  	if (jsInstructionDomain == null) {
  		String domaine = cktlApp.config().stringForKey(KEY_DOMAIN);
  		if (domaine != null) {
  			jsInstructionDomain = StringCtrl.replace(JS_INSTRUCTION_ENT, "$1", domaine);	
  		} else {
  			jsInstructionDomain = StringCtrl.emptyString(); 
  		}
  	}
  	return jsInstructionDomain;
  }
}
