package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;


/**
 * Ce composant permet de creer et parametrer une page "d'alerte".
 * Son fonctionnement et interface est similaire a celui des fenetres
 * des messages d'erreurs, des questions, des informations utilisees
 * dans les applications fenetrees classiques.
 * 
 * <p>Une page d'alerte est composee d'une icone representant le type de message,
 * du texte de message et un ou plusieurs boutons (s'ils sont presents)&nbsp;:
 * 
 * <p><div align="center"><img src="../../../../doc-files/CktlAlertPage-1.gif" border="1"></div>
 * 
 * <p>Une page d'alerte peut etre creee de plusieures manieres&nbsp;: soit de
 * maniere classique via l'appel a la methode <code>pageWithName</code>, soit
 * en appelant une des methodes statiques <code>newAlertPage...</code>.
 * Les exemples ci-dessous expliquent leur utilisation.
 * 
 * <p>La page generee utilise la mise en page par defaut des pages
 * HTML de l'application. Cette page est incluse dans une page de type 
 * {@link CktlDefaultPage}.
 * 
 * 
 * <h3>Exemples</h3>
 * 
 * <h4>Un message simple (information, avertissement)</h4>
 * 
 * On fait appel a la methode statique <code>newAlertPageWithCaller</code>.
 * Le premier parametre de cette methode est la page ou l'utilisateur sera
 * redirige apres l'affichage du message.  
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Afficher un message d'erreur</code>
 * public WOComponent messageErreur() {
 *   return CktlAlertPage.newAlertPageWithCaller(this, "Message d'erreur",
 *     "Une erreur s'est produite.&lt;br&gt;Cliquez pour retourner a la page precedente.",
 *     "Retourner", CktlAlertPage.ERROR);
 * }
 * 
 * <code class="comment">// Afficher un message de confirmation.</code>
 * public WOComponent messageSucces() {
 *   return CktlAlertPage.newAlertPageWithCaller(this, "Succes",
 *     "L'operation a ete effectue avec succes",
 *     "Retourner", CktlAlertPage.INFO);
 * }</pre></td></tr></table>
 * 
 * 
 * <h4>Un message avec plusieures reponses (questions)</h4>
 * 
 * On fait appel a la methode statique <code>newAlertPageWithResponder</code>.
 * Le dernier parametre de cette methode est l'objet qui implemente l'interface
 * <code>CktlAlertResponder</code>. Cette interface comporte la methode
 * <code>respondToButton</code> qui renvoit la page (le composant) a afficher
 * en fonction de la reponse choisie.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Afficher une question avec trois reponses possibles</code>
 * public WOComponent messageQuestions() {
 *   QuestionsResponder responder = new QuestionsResponder();
 *   return CktlAlertPage.newAlertPageWithResponder(this, "Question",
 *     "Questoin avec trois reponses.&lt;br&gt;Quelle reponse choisissez vous ?",
 *     "Reponse 1", "Reponse 2", "Reponse 3", CktlAlertPage.QUESTION, responder);
 * }
 * 
 * <code class="comment">// La classe interne - l'implementation de AlertResponder</code>
 * public class QuestionsResponder implements CktlAlertResponder {
 *   public WOComponent respondToButton(int buttonNo) {
 *     swith(buttonNo) {
 *       case 1: return pageWithName("PageReponse1");
 *       case 2: return pageWithName("PageReponse2");
 *       case 2: return pageWithName("PageReponse3");
 *     }
 *   }
 * }</pre></td></tr></table>
 *
 *
 * <h4>Un message "sans issue"</h4>
 * 
 * On fait appel a la methode statique <code>newAlertPageWithMessage</code>.
 * Cette appel genere la page qui ne contient aucun lien vers d'autres pages.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Afficher le message seulement, pas de liens, ni des boutons</code>
 * public WOComponent messageSansLiens() {
 *   return CktlAlertPage.newAlertPageWithMessage(this, "Attention !",
 *     "Operation sans reponse.&lt;br&gt;Aucun resultat ne peut etre affiche",
 *     CktlAlertPage.ATTENTION);
 * }</pre></td></tr></table>
 *
 *  
 * <h4>Un message personnalise</h4>
 * 
 * Une page d'alerte peut etre construite "manuellement", si les fonctionnalites
 * des methodes <code>newAlertPage...</code> ne sont pas suffisantes. Dans ce
 * cas, on cree une page a l'aide de la methode <code>pageWithName</code>,
 * ensuite on la parametrise et en fin on la prepare pour l'affichage en appelant
 * la methode <code>showMessage</code>.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Afficher un message 
 *  // dont les lients pointent sur la fenetre nommee "_top".</code>
 * 
 * public WOComponent messageSpecial() {
 *   CktlAlertPage alertPage = (CktlAlertPage)pageWithName("CktlAlertPage");
 *   alertPage.setTarget("_top");
 *   <code class="comment">// C'est la methode principale. Cette methode est utilisee dans tous les
 *   // appels newAlertPage...()</code>
 *   alertPage.showMessage(this, "Succes.",
 *     "Operation s'est deroulee avec succes&lt;br&gt;Cliquez sur OK pour afficher le resultat.",
 *     "OK", null, null, CktlAlertPage.ATTENTION, null);
 *   <code class="comment">// Et en fin, on affiche la page</code>
 *   return alertPage;
 * }</pre></td></tr></table>
 * 
 * @see CktlAlertResponder
 * @see CktlDefaultPage
 */
public class CktlAlertPage 
	extends CktlDefaultPage {
	
  /**
   * Le code du message de type "erreur". <code>alert-error.gif</code> est l'image
   * correspondante dans les ressources du framework. 
   */
  public final static int ERROR = 0;
  
  /**
   * Le code du message de type "avertissement". <code>alert-exclam.gif</code>
   * est l'image correspondante dans les ressources du framework.
   */ 
  public final static int ATTENTION = 1;
  
  /**
   * Le code du message de type "information". <code>alert-message.gif</code>
   * est l'image correspondante dans les ressources du framework.
   */ 
  public final static int INFO = 2;

  /**
   * Le code du message de type "question". <code>alert-question.gif</code>
   * est l'image correspondante dans les ressources du framework.
   */ 
  public final static int QUESTION = 3;

  /**
   * Le code de message sans aucun type. Aucune icone n'est affichee
   * lors de visualisation de ce message.
   */ 
  public final static int NONE = -1;

  /**
   * Le titre de message d'avertissement. Le titre est affichee
   * en haut de la zone de l'affichage de message.
   */
  public String title;
  
  /**
   * Le contenu du message a affiche. Le message peut avoir le formatage
   * HTML.
   */
  public String message;
  
  /**
   * Indicateur de la definition de type "erreur".
   * 
   * @see #ERROR
   */
  public boolean errorImageYN;
  
  /**
   * Indicateur de la definition de type "avertissement".
   * 
   * @see #ATTENTION
   */
  public boolean attentionImageYN;

  /**
   * Indicateur de la definition de type "information".
   * 
   * @see #INFO
   */
  public boolean infoImageYN;

  /**
   * Indicateur de la definition de type "question".
   * 
   * @see #QUESTION
   */
  public boolean questionImageYN;
  
  /**
   * Le nom de la fenetre ou du frame destination. Les liens
   * affiches avec le message seront rediriges vers la fenetre
   * portant ce nom.
   * 
   * <p>La valeur par defaut est "_self".
   */
  public String target;

  /**
   * Indique le composant dans le context de lequel le message
   * est affiche. C'est aussi la page ou l'utilisateur sera
   * renvoye ensuite, si le "responder" n'est pas precise. 
   */
  public WOComponent callerPage;
  
  /**
   * Le texte de premier bouton. <code>null</code> si le bouton
   * ne doit pas etre affiche.
   * 
   * <p><i>Il est conseille de definir cette valeur si le libelle du
   * deuxieme bouton est defini !</i>
   */
  public String button1;
  
  /**
   * Le libelle de deuxieme bouton. <code>null</code> si le bouton
   * ne doit pas etre affiche.
   * 
   * <p><i>Il est conseille de definir cette valeur si le libelle du
   * troisieme bouton est defini !</i>
   */
  public String button2;
  
  /**
   * Le libelle de troisieme bouton. <code>null</code> si le bouton
   * ne doit pas etre affiche.
   */
  public String button3;
  
  /**
   * L'objet "responder" de composante. Si le message est affiche avec
   * plusieurs reponses possibles, cet objet sera informe apres la selection
   * d'une des reponses.
   */
  private CktlAlertResponder responder;
  
//  /**
//   * Indique s'il faut cacher le bas de page standard dans la page
//   * HTML generee. La valeur par defaut est <i>false</i>.
//   */
//  private boolean hideFooter;
  
  /**
   * Construit un objet correspondant a une nouvelle page d'alerte.
   */
  public CktlAlertPage(WOContext context) {
    super(context);
  }

  /**
   * Prepare le composant pour l'affichage d'icone correspondant
   * au message de type donne.
   * 
   * @param imageType Le type de message, une des constantes <code>ERROR</code>,
   *   <code>ATTENTION</code>, <code>INFO</code>, <code>QUESTION</code> ou
   *   <code>NONE</code>.
   * 
   * @see #ERROR
   * @see #ATTENTION
   * @see #INFO
   * @see #QUESTION
   * @see #NONE
   */
  private void setImage(int imageType) {
    errorImageYN = (imageType == ERROR);
    attentionImageYN = (imageType == ATTENTION);
    infoImageYN = (imageType == INFO);
    questionImageYN = (imageType == QUESTION);
  }

  /**
   * Definit le nom de la fenetre ou du frame destination. Les liens
   * affiches avec le message seront rediriges vers la fenetre
   * portant ce nom. C'est la valeur qui est utilisee avec
   * l'attribut <code>target</code> de la balise <code>href</code>
   * de HTML.
   * 
   *  
   */
  public void setTarget(String newTarget) {
    target = newTarget;
  }
  
  /**
   * Renvoie le nom de la fenetre ou du frame de navigateur où
   * l'utilisateur sera redirige apres l'affichage du messge.
   * 
   * @see #setTarget(String) setTarget()
   */
  public String getTarget() {
    if (target == null) target = "_self";
    return target;
  }
  
  /**
   * Creer une nouvelle page d'alerte simple. Cette page peut etre utilisee
   * pour afficher les message d'erreurs, avertissements, informations, etc. et
   * ensuite rediriger l'utilisateur sur une page precise. Ceci correspond
   * aux boites de dialoge des applications fenetres ou une seule choix est
   * possible suite a l'affichage d'un message (ex, "OK").
   * 
   * @param caller Le composant correspondant a la page vers laquelle
   *   l'utilisateur sera redirige apres l'affichage de message.
   * @param newTitle Le titre du message.
   * @param newMessage Le texte du message. Il peut contenir des balises HTML.
   * @param newButton1 Le libelle du bouton de confirmation affiche sous
   *   le message. Le click sur ce bouton va rediriger l'utilisateur vers
   *   la page <code>caller</code>.
   * @param imageType Le code de type de message. Une des valuers
   *   <code>ERROR</code>, <code>ATTENTION</code>, <code>INFO</code>,
   *   <code>QUESTION</code> ou <code>NONE</code>.
   * 
   * @return Une nouvelle page d'alerte.
   */
  public static CktlAlertPage newAlertPageWithCaller(WOComponent caller, String newTitle,
                                                    String newMessage, String newButton1,
                                                    int imageType)
  {
    CktlAlertPage alertPage = (CktlAlertPage)caller.pageWithName("CktlAlertPage");
    alertPage.showMessage(caller, newTitle, newMessage, newButton1, null, null, imageType, null);
    return alertPage;
  }

  /**
   * Creer une nouvelle page d'alerte permettant d'afficher un message
   * de type "question" et choisir une des reponses proposees. Les
   * reponses sont representees par les boutons et le clicks sur les boutons
   * sont geres par un objet implementant l'interface <code>CktlAlertResponder</code>.
   * Ainsi, le fonctionnement de la page est similaire au celui de fenetres
   * de dialoge dans les application "classiques".
   * 
   * @param caller Le composant dans le context de lequel la page d'alerte
   *   est cree. Ceci peut etre le composant-origine de la question.  
   * @param newTitle Le titre du message.
   * @param newMessage Le texte du message. Il peut contenir des balises HTML.
   * @param newButton1 Le libelle du bouton correspondant a la premiere
   *   reponse. Le bouton n'est pas affiche si la valeur est <i>null</i>.
   *   <i>Si le message contient plusieurs boutons, au moins le premier bouton
   *   doit etre defini.</i>
   * @param newButton2 Le libelle du bouton correspondant a la deuxieme
   *   reponse (peut etre <i>null</i>).
   * @param newButton3 Le libelle du bouton correspondant a la troixieme
   *   reponse (peut etre <i>null</i>).
   * @param imageType Le code de type de message. Une des valuers
   *   <code>ERROR</code>, <code>ATTENTION</code>, <code>INFO</code>,
   *   <code>QUESTION</code> ou <code>NONE</code>.
   * @param newResponder L'objet qui va gerer les clicks sur les boutons.
   * 
   * @return Une nouvelle page d'alerte.
   * 
   * @see CktlAlertResponder
   */
  public static CktlAlertPage newAlertPageWithResponder(WOComponent caller, String newTitle,
                                                       String newMessage, String newButton1,
                                                       String newButton2, String newButton3,
                                                       int imageType,
                                                       CktlAlertResponder newResponder)
  {
    CktlAlertPage alertPage = (CktlAlertPage)caller.pageWithName("CktlAlertPage");
    alertPage.showMessage(caller, newTitle, newMessage, newButton1, newButton2, newButton3, imageType, newResponder);
    return alertPage;
  }

  /**
   * Creer une nouvelle page avec un message. La page ne propose
   * à l'utilisateur aucun lien vers d'autres pages. 
   * 
   * @param caller Le composant dans le context de lequel la page d'alerte
   *   est cree.  
   * @param newTitle Le titre du message.
   * @param newMessage Le texte du message. Il peut contenir des balises HTML.
   * @param imageType Le code de type de message. Une des valuers
   *   <code>ERROR</code>, <code>ATTENTION</code>, <code>INFO</code>,
   *   <code>QUESTION</code> ou <code>NONE</code>.
   * 
   * @return Nouvelle page avec le message.
   */
  public static CktlAlertPage newAlertPageWithMessage(WOComponent caller, String newTitle,
                                                     String newMessage, int imageType)
  {
    CktlAlertPage alertPage = (CktlAlertPage)caller.pageWithName("CktlAlertPage");
    alertPage.showMessage(caller, newTitle, newMessage, null, null, null, imageType, null);
    return alertPage;
  }

  /**
   * Permet de parametrer une page d'alert deja cree. Cette methode est implicitement
   * appelee par les methodes <code>newAlertPageWithCaller</code>,
   * <code>newAlertPageWithResponder</code> et <code>newAlertPageWithMessage</code>.
   * 
   * @param caller Le composant dans le context de lequel la page est cree.
   *   Il represente aussi la destination des actions, si l'objet
   *   <code>newResponder</code> est <i>null</i>.
   * @param newTitle Le titre du message.
   * @param newMessage Le texte du message.
   * @param newButton1 Le libelle du bouton correspondant a la premiere
   *   reponse. Le bouton n'est pas affiche si la valeur est <i>null</i>.
   *   <i>Si le message contient les boutons, au moins le premier bouton
   *   doit etre defini.</i>
   * @param newButton2 Le libelle du bouton correspondant a la deuxieme
   *   reponse (peut etre <i>null</i>).
   * @param newButton3 Le libelle du bouton correspondant a la troixieme
   *   reponse (peut etre <i>null</i>).
   * @param imageType Le code de type de message. Une des valuers
   *   <code>ERROR</code>, <code>ATTENTION</code>, <code>INFO</code>,
   *   <code>QUESTION</code> ou <code>NONE</code>.
   * @param newResponder L'objet qui va gerer les clicks sur les boutons.
   * 
   * @see #newAlertPageWithCaller(WOComponent, String, String, String, int) newAlertPageWithCaller()
   * @see #newAlertPageWithMessage(WOComponent, String, String, int) newAlertPageWithMessage()
   * @see #newAlertPageWithResponder(WOComponent, String, String, String, String, String, int, CktlAlertResponder) newAlertPageWithResponder()
   * @see CktlAlertResponder
   */
  public void showMessage(WOComponent caller, String newTitle, String newMessage,
                          String newButton1, String newButton2, String newButton3,
                          int imageType, CktlAlertResponder newResponder) {
    callerPage = caller;
    title = newTitle;
    message = newMessage;
    button1 = newButton1;
    button2 = newButton2;
    button3 = newButton3;
    setImage(imageType);
    responder = newResponder;
  }

  /**
   * Teste si le composant contient les boutons. On considere que les boutons
   * doivent etre affiche si au moins le libelle du premier bouton est
   * defini.
   */
  public boolean hasButtons() {
    return (button1 != null);
  }

  /**
   * Teste si le deuxieme bouton doit etre affiche. Le bouton
   * est visible si son libelle n'est pas <i>null</i>.
   */
  public boolean button2Visible() {
    return (button2 != null);
  }

  /**
   * Teste si le troixieme bouton doit etre affiche. Le bouton
   * est visible si son libelle n'est pas <i>null</i>.
   */
  public boolean button3Visible() {
    return (button3 != null);
  }

  /**
   * Cette action est appelee suite a un click sur le premier
   * bouton. Si l'objet "responder" est donne, alors le resultat
   * de l'execution de sa methode <code>respondToButton(1)</code>
   * est retourne. Sinon, l'objet <code>caller</code> est retoutne.
   *
   * @see #showMessage(WOComponent, String, String, String, String, String, int, CktlAlertResponder) showMessage()
   */
  public WOComponent click1() {
    if (responder != null) return responder.respondToButton(1);
    return callerPage;
  }

  /**
   * Cette action est appelee suite a un click sur le deuxieme
   * bouton. Si l'objet "responder" est donne, alors le resultat
   * de l'execution de sa methode <code>respondToButton(2)</code>
   * est retourne. Sinon, l'objet <code>caller</code> est retoutne.
   *
   * @see #showMessage(WOComponent, String, String, String, String, String, int, CktlAlertResponder) showMessage()
   */
  public WOComponent click2() {
    if (responder != null) return responder.respondToButton(2);
    return callerPage;
  }

  /**
   * Cette action est appelee suite a un click sur le deuxieme
   * bouton. Si l'objet "responder" est donne, alors le resultat
   * de l'execution de sa methode <code>respondToButton(3)</code>
   * est retourne. Sinon, l'objet <code>caller</code> est retoutne.
   *
   * @see #showMessage(WOComponent, String, String, String, String, String, int, CktlAlertResponder) showMessage()
   */
  public WOComponent click3() {
    if (responder != null) return responder.respondToButton(3);
    return callerPage;
  }
}
