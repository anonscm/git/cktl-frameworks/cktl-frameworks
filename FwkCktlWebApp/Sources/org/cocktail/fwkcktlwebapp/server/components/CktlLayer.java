package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Cette classe permet de faire apparaitre des popup
 * minimalistes au format DHTML. L'operation se fait 
 * par supersposition d'une grande div, masquant
 * l'arriere plan par transparence.
 * 
 * Si aucun des 2 bindings "action" et "href" n'ont
 * de valeur, alors le clic dans la fenetre la ferme.
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */


/**
 * Il s'agit d'un "calque" en DHTML qui permet d'afficher au
 * centre de la page un message dans une fenetre, a l'image
 * de la fenetre qui apparait en utilisant la fonction 
 * javascript <code>alert('message');</code>. L'utilisateur
 * n'a pas acces a la fenetre en fond, tant que la fenetre est
 * affichee.<br>
 * <br>
 * Il se depose graphiquement dans WebObjects Builder en
 * derniere position.<br>
 * <br>
 * <p align="center">
 * 	<img src="../../../../doc-files/CktlLayer-1.png" border="1"/>
 * </p>
 * <br>
 * <h3>Bindings</h3>
 * 
 * <p>
 *  <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *	 <tr align="center" class="paramBoxHeader">
 *	  <td>Connecteur</td>
 *	  <td>Definition</td>
 *	  <td>Valeur</td>
 *	  <td>Description</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>text</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le texte a afficher dans la fenetre</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>closeTip</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>"Fermer cette fen&egrave;tre"</i>]</td>
 *	  <td align="left">Le "tip" sur la croix pour fermer la fenetre</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>action</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>WOComponent</code><br>[<i>null</i>]</td>
 *	  <td align="left">L'action WebObjects a appeler lors du clic dans la fenetre</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>href</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>WOComponent</code><br>[<i>null</i>]</td>
 *	  <td align="left">L'URL HTML a retourne lors du clic sur la fenetre.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>title</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>WOComponent</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le titre de la fenetre.</td>
 *	 </tr>
 *	</table>
 * </p>
 *
 * @author Cyril Tarade <cyril.tarade at univ-lr.fr>
 */

public class CktlLayer extends CktlWebComponent {

  // Les noms des connecteurs.
  private final String BND_TEXT         = "text";
  private final String BND_CLOSE_TIP    = "closeTip";
  private final String BND_ACTION       = "action";
  private final String BND_HREF         = "href";
  private final String BND_TITLE        = "title";
  
  public CktlLayer(WOContext context) {
    super(context);
  }

  /*
   * @see com.webobjects.appserver.WOComponent#synchronizesVariablesWithBindings()
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  private final static String jsParentFolder = "jscript/CktlLayer/";
  private final static String cktlLayerScript = jsParentFolder+"CktlLayer.js";

  /**
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
  	// ajouter le javascript pour le CktlLayer
    addLocalJScript(arg0, cktlLayerScript, fwkCktlWebAppFrameworkName());  
    // ajouter la balise HTML pour griser la fenetre
    addHTMLBinding(arg0, "onResize", "\"resizeWindow();\"", TAG_OPEN_BODY);
  }
  
  /**
   * Le texte a afficher dans la popup
   */
  private String text() {
    if (valueForBinding(BND_TEXT) != null)
      return (String) valueForBinding(BND_TEXT);
    return "";
  }
  
  /**
   * Le texte a afficher sur la croix pour fermer
   * la fenetre
   */
  private String closeTip() {
    if (valueForBinding(BND_CLOSE_TIP) != null)
      return (String) valueForBinding(BND_CLOSE_TIP);
    return "Fermer cette fenetre";
  }

  /**
   * L'action WebObjects a appeler lors du clic dans la fenetre
   */
  public WOComponent action() {
    if (valueForBinding(BND_ACTION) != null)
      return (WOComponent) valueForBinding(BND_ACTION);
    return null;
  }

  /**
   * L'URL HTML a retourne lors du clic sur la fenetre.
   */
  public String href() {
    if (valueForBinding(BND_HREF) != null)
      return (String) valueForBinding(BND_HREF);
    return null;
  }

  /**
   * Le titre de la fenetre
   */
  public String title() {
    if (valueForBinding(BND_TITLE) != null)
      return (String) valueForBinding(BND_TITLE);
    return "";
  }
  
  /**
   * Est-ce que le binding est sur l'action
   * (si non, c'est sur href)
   */
  public boolean hasActionBinding() {
    return hasBinding(BND_ACTION);
  }
  
  /**
   * Est-ce que le binding est sur href
   * (si non, c'est sur action)
   */
  public boolean hasHrefBinding() {
    return hasBinding(BND_HREF);
  }
  
  // le lien vers l'icone pour fermer la fenetre
  private String _imgClose;
  
  private String imgClose() {
    if (_imgClose == null)
      _imgClose = getLocalWebServerResourceUrl(cktlLayerImg, fwkCktlWebAppFrameworkName());
    return _imgClose;
  }
  
  // le lien vers l'icone qui simule la transparence
  private String _imgTranparent;
  
  private String imgTranparent() {
    if (_imgTranparent == null)
      _imgTranparent = getLocalWebServerResourceUrl(cktlLayerImgTranparent, fwkCktlWebAppFrameworkName());
    return _imgTranparent;
  }
   
  private final static String imgParentFolder = "img/CktlLayer/";
  private final static String cktlLayerImg = imgParentFolder+"CktlLayerClose.gif";
  private final static String cktlLayerImgTranparent = imgParentFolder+"CktlLayerTransparent.gif";

  //--= LE CODE JS POUR OUVRIR LA POPUP (2 PARTIES) --//
  
  /**
   * 
   */
  public String beginCodeJavascript() {
   return "cktlLayer('#FFFFFF','#E5E5E5', \"" +
     text() + "\", '" +  closeTip()+ "', '";
  }
  
  /**
   *
   */
  public String endCodeJavascript() {
   return "', 350, 100, '" + imgClose() + "', '" + imgTranparent() + "', '" + title()+  "');";
  }
 
  
  
}