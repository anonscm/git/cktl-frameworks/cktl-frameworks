package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;

/**
 * Bouton permettant de nettoyer le formulaire englobant.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlResetButton extends CktlSubmitButton {

	public CktlResetButton(String arg0, NSDictionary<String, WOAssociation> nsdictionary, WOElement arg2) {
		super(arg0, nsdictionary, arg2);

	}

	@Override
	public void appendAttributesToResponse(WOResponse woresponse, WOContext wocontext) {
		super.appendAttributesToResponse(woresponse, wocontext);
		woresponse._appendTagAttributeAndValue("onclick", resetFormJavaScript(), false);
	}

	@Override
	protected String type() {
		return "reset";
	}

	public String resetFormJavaScript() {
		return "Form.clear(this.form.id); return false;";
	}

}
