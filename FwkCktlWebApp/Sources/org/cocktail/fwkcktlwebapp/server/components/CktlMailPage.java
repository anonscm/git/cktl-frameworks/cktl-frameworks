package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlMailBus;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Ce composant permet de creer une page d'envoi des messages mail.
 * Une page "mail" peut etre obtenue a l'aide de la methode
 * <code>newMailPage</code> ou bien en creant un nouvel objet avec
 * la methode <code>newPageWithName</code> et en la parametrant via
 * l'appel de la methode <code>showMessage</code>.
 * 
 * La page de mail propose deux fonctionnalites :
 * <ul>
 * 	<li>Une interface HTML d'envoi de message.</li>
 * 	<li>Un lien HTML permettant d'ouvrir le programme de messagerie de la
 * 		machine affichant la <code>CktlMailPage</code> (thunderbird, outlook ...)</li>
 * </ul>
 * 
 * <p align="center">
 * 	<table>
 * 		<tr>
 * 			<td align="center" valign="bottom"><img src="../../../../doc-files/CktlMailPage-1.png" border="1"/></td>
 * 			<td>&nbsp;&nbsp;&nbsp;</td>
 * 			<td align="center" valign="bottom"><img src="../../../../doc-files/CktlMailPage-2.png" border="1"/></td>
 *		</tr>
 * 		<tr>
 * 			<td><i>Interface HTML</i></td>
 * 			<td></td>
 * 			<td><i>Lien pour ouvrir le programme de mail</i></td>
 * 		</tr>
 *	</table>
 * </p>
 *  
 * <p>La page generee utilise la mise en page par defaut des pages
 * HTML de l'application. La mise en page defaut peut etre modifiee en
 * appelant les methodes <code>setLogos</code>, <code>setStyles</code>,
 * <code>setIgnoreDefaultLogos</code> et <code>setIgnoreDefaultStyles</code>
 * heritees de la classe {@link CktlWebPage}.
 *
 *  
 * <h3>Exemples</h3>
 * 
 * <h4>Creation d'une page mail a l'aide de la methode statique</h4>
 * 
 * On fait appel a la methode statique <code>newMailPage</code>.
 * Le premier parametre de cette methode est la page dans le contexte
 * de laquelle le message mail est envoye. C'est aussi la page vers
 * laquelle l'utilisateur sera redirige apres l'envoi du message.  
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Afficher une page d'envoi d'un message mail.</code>
 * public WOComponent getMailMessagePage() {
 *   <code class="comment">// Obtenir un nouveau composant "page mail"</code>
 *   CktlMailPage mPage = CktlMailPage(this, "me@company.com",
 *                                               "friend1@company.com",
 *                                               "friend2@company.com, friend3@company.com",
 *                                               "Test of my mail", "Hi folks !\n Anonymous.");
 *   <code class="comment">// Parametrer le composant</code>
 *   mPage.setCanChangeTo(false);
 *   mPage.setCanChangeSubject(false);
 *   mPage.setHasFixedMessage(false);
 *   <code class="comment">// Retourner pour l'affichage</code>
 *   return mPage;
 * }</pre></td></tr></table>
 * 
 * 
 * <h4>Creation "manuelle" d'une page mail</h4>
 * 
 * On creer un composant "mail" en appelant la methode
 * <code>pageWithName("CktlMailPage")</code>. On la parametrise ensuite en
 * appelant la methode <code>showPage</code> et eventuellement d'autres
 * methodes.  
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Exemple de creation d'une page "mail"</code>
 * public WOComponent getMailMessagePage2() {
 *   <code class="comment">// Obtenir un nouveau composant "page mail"</code>
 *   CktlMailPage mailPage = (CktlMailPage)newCaller.pageWithName("CktlMailPage");
 *   <code class="comment">// Parametrer le composant</code>
 *   mailPage.showPage(this, "me@company.com",
 *                           "friend1@company.com",
 *                           "friend2@company.com, friend3@company.com",
 *                           "Test of my mail", "Hi folks !\n Anonymous.");
 *   mPage.setCanChangeSubject(true);
 *   <code class="comment">// Le texte du message peut avoir une partie non-modifiabe. Parfois ca sert...</code> 
 *   mPage.setFixedMessage("> This is my comment.");
 *   mPage.setHasFixedMessage(true);
 *   <code class="comment">// On change aussi le style de la page (voir CktlWebPage)</code>
 *   mPage.setStyles(getMyStyleTags());
 *   mPage.setIgnoreDefaultStyles(true);
 *   <code class="comment">// Retourner pour l'affichage</code>
 *   return mPage;
 * }</pre></td></tr></table>
 * 
 * <p><font class="importantNote">Le composant <code>CktlMailPage</code> propose une interface pour
 * envoyer les messages mails. Si vous avez besoins d'envoyer un message
 * sans passer par cette interface, utiliser la classe
 * {@link CktlMailMessage} ou {@link CktlMailBus}.</font>
 * 
 * @see CktlMailMessage
 * @see CktlDefaultPage
 */
public class CktlMailPage 
	extends CktlWebComponent {
  /**
   * Indique si l'adresse TO est modifiable dans la page. La valeur par defaut
   * est <i>false</i>.
   */
	public boolean canChangeTo;
  
  /**
   * L'adresse du destinataire du message.
   */
  public String to;
  
  /**
   * Indique si le sujet du message peut etre modifie.
   * La valeur par defaut est <i>true</i>.
   */
  public boolean canChangeSubject;
  
  /**
   * Les adresses des destinataires en copie. Cette valeur
   * peut toujours etre modifie dans la page.
   */
  public String cc;
  
  /**
   * Le sujet du message.
   */
  public String subject;
  
  /**
   * L'adresse d'expediteur du message. Elle ne peut pas etre modifiee.
   */
  public String from;
  
  /**
   * Le texte du message.
   */
  public String message;
  
  /**
   * Indique si le message doit avoir une partie non-modifiable.
   * 
   * @see #hasFixedMessage
   */
  public boolean hasFixedMessage;
  
  /**
   * Le text de message non modifiable. Ce text sera ajouter a la
   * fin du message principale.
   * 
   * @see #hasFixedMessage  
   */
  public String fixedMessage;
  
  /**
   * Indique le composant dans le cadre de lequel le message est
   * envoye. L'utilisateur sera redirige vers ce composant
   * apres l'envoi du message.
   */
  public WOComponent caller;
  
  /**
   * Indique que l'adresse TO n'est pas defini. Un message
   * d'erreur est affiche dans ce cas.
   */
  public boolean errNoTo;

  /**
   * Indique que le message ne contient aucun texte. Un message
   * d'erreur est affiche dans ce cas.
   */
  public boolean errNoMessage;
 
  /**
   * Le contenu du message d'erreur lors de l'envoi.
   */
  public String strErrSendMessage;
  
  /**
   * La localisation du repertoire dans les webresources
   * contenant les css et les js du composant
   */
  private final static String CSS_PARENT_FOLDER = "css/";
  private final static String JS_PARENT_FOLDER = "jscript/";

  private final static String CKTLMAILPAGE_STYLE 	= CSS_PARENT_FOLDER+"CktlMailPage.css";
  private final static String CKTLMAILPAGE_JSCRIPT = JS_PARENT_FOLDER+"CktlMailPage.js";

  
  /**
   * Creer un nouveau composant.
   * 
   * @see #newMailPage(WOComponent, String, String, String, String, String) newMailPage() 
   */
  public CktlMailPage(WOContext context) {
    super(context);
    resetErrors();
    canChangeTo = false;
    canChangeSubject = true;
    fixedMessage = null;
  }
  
  /**
   * Annule toutes les messages d'erreurs. 
   */
  private void resetErrors() {
    errNoTo = errNoMessage = false;
    strErrSendMessage = StringCtrl.emptyString();
  }

  /**
   * Teste si des messages d'erreurs ont ete detectees.
   */
  private boolean hasErrors() {
    return errNoTo || errNoMessage;
  }
  
  /**
   * Cree et initialise un nouveau composant <code>CktlMailPage</code>.
   * Ce composant permet d'envoyer un message e-mail simple en indiquant
   * son expediteur, ses destinatairs, son sujet et son text.
   * 
   * <p>La classe {@link CktlMailMessage} est utilisee pour l'envoi effectif de message.
   *  
   * @param newCaller Le composant dans le context de lequel le message est
   *   cree. Vers ce composant l'utilisateur est redirige apres l'envoi du
   *   message.
   * @param newFrom L'adresse e-mail d'expediteur.
   * @param newTo L'adresse e-mail du destinataire.
   * @param newCC Les adresse des destinataires en copie. 
   * @param newSubject Le text du sujet propose initialement. 
   * @param newMessage Le text du message propose initialement. 
   * @return Le nouveau composant.
   * 
   * @see #showPage(WOComponent, String, String, String, String, String) showPage()
   * @see CktlMailMessage
   */
  public static CktlMailPage newMailPage(WOComponent newCaller, 
  		String newFrom, String newTo, String newCC, 
  		String newSubject, String newMessage) {
    CktlMailPage mailPage = (CktlMailPage)newCaller.pageWithName(CktlMailPage.class.getName());
    mailPage.showPage(newCaller, newFrom, newTo, newCC, newSubject, newMessage);
    return mailPage;
  }

  public void showPage(WOComponent newCaller, String newFrom, String newTo, String newCC, String newSubject, String newMessage) {
    caller = newCaller;
    fromOriginal = newFrom;
    toOriginal = newTo;
    ccOriginal = newCC;
    subjectOriginal = newSubject;
    messageOriginal = newMessage;
    from = newFrom;
    to = newTo;
    cc = newCC;
    subject = newSubject;
    message = newMessage;
    canChangeTo = true;
    canChangeSubject = true;
    hasFixedMessage = false;
    resetErrors();
  }

  public void setFrom(String newFrom) {
    from = newFrom;
  }

  public void setTo(String newTo) {
    to = newTo;
  }

  public void setCanChangeTo(boolean newCanChangeTo) {
    canChangeTo = newCanChangeTo;
  }

  public void setCC(String newCC) {
    cc = newCC;
  }

  public void setSubject(String newSubject) {
    subject = newSubject;
  }

  public void setCanChangeSubject(boolean newCanChangeSubject) {
    canChangeSubject = newCanChangeSubject;
  }

  public void setFixedMessage(String newFixedMessage) {
    fixedMessage = newFixedMessage;
  }

  public String fixedMessageHTML() {
    if (fixedMessage != null)
      return StringCtrl.replace(fixedMessage, "\n", "<br>");
    else
      return "&nbsp;";
  }

  public void setHasFixedMessage(boolean newHasFixedMessage) {
    hasFixedMessage = newHasFixedMessage;
  }

  public void setMessage(String newMessage) {
    message = newMessage;
  }

  public WOComponent send() {
    resetErrors();
    errNoTo = (StringCtrl.normalize(to).length() == 0);
    errNoMessage = (StringCtrl.normalize(message).length() == 0);
    if (hasErrors()) return null;
    if (hasFixedMessage && (StringCtrl.normalize(fixedMessage).length() > 0))
      message = fixedMessage+"\n---------------------------\n"+message;
    if (!cktlApp.mailBus().sendMail(from, to, cc, subject, message)) {
      //return CktlAlertPage.newAlertPageWithCaller(caller, "Envoi de e-mail", "Le message e-mail n'a pas pu &ecirc;tre envoy&eacute; !", " OK ", CktlAlertPage.ERROR);
    	// 14/12/2007 : on reste sur la page en affichant le message d'erreur
    	strErrSendMessage = "Le message e-mail n'a pas pu etre envoye : "+ cktlApp.mailBus().getErrorMessage();
    	return null;
    } else {
      return CktlAlertPage.newAlertPageWithCaller(caller, "Envoi de e-mail", "Votre message e-mail a &eacute;t&eacute; envoy&eacute;.", " OK ", CktlAlertPage.INFO);
    }
  }

  public WOComponent goBack() {
    return caller;
  }
   
  // ----------- rajouts utilisation mail interne ---------------
  
  // Afficher le composant Web d'envoi de mail
  private boolean useCktlMailPage = false;
  
  // Sauvegardes des valeurs originales
  private String fromOriginal;
  private String toOriginal;
  private String ccOriginal;
  private String subjectOriginal;
  private String messageOriginal;
  private boolean viewHasChanged = false;
    
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
    if (viewHasChanged) {
      setFrom(fromOriginal);
      setTo(toOriginal);
      setCC(ccOriginal);
      setSubject(subjectOriginal);
      setMessage(messageOriginal);
      viewHasChanged = false;
    }
    super.appendToResponse(arg0, arg1);
    addLocalJScript(arg0, CKTLMAILPAGE_JSCRIPT, fwkCktlWebAppFrameworkName());
   	addLocalCss(arg0, CKTLMAILPAGE_STYLE, fwkCktlWebAppFrameworkName());
  }
  
  /**
   * parametre du composant {@link CktlMailTo} de la forme
   * dest1[,destN]?subject=[sujet][&cc=CC1[,CCN]][&body=1ere_ligne[&body=nieme_ligne]]
   */
  public String mailLink() {
    StringBuffer mailLink = new StringBuffer();
    
    // remplacement des caracteres reserves
   mailLink.append(to.trim()).append("?");
    if (!StringCtrl.isEmpty(subject))
      mailLink.append("subject=").append(cleanStringForMailTo(subject));
    if (!StringCtrl.isEmpty(cc)) {
      if (!mailLink.toString().endsWith("&"))
        mailLink.append("&");
      mailLink.append("cc=").append(cleanStringForMailTo(cc.trim()));
    }
    if (!StringCtrl.isEmpty(message)) {
      if (!mailLink.toString().endsWith("&"))
        mailLink.append("&");
      mailLink.append("body=");
      mailLink.append(cleanStringForMailTo(message));
    }
    
    // remplacer les caracteres <espace>
    return mailLink.toString();
  }
  
  /**
   * methode interne qui supprime tous les caracteres speciaux
   * qui pourraient gener dans l'instruction html mailto 
   * @param string
   */
  private String cleanStringForMailTo(String string) {
    // nettoyage des caracteres speciaux
    string = StringCtrl.replace(string, "%", "%25");
    string = StringCtrl.replace(string, "&", "%26");
    string = StringCtrl.replace(string, "<", "%3C");
    string = StringCtrl.replace(string, ">", "%3E");
    string = StringCtrl.replace(string, "?", "%3F");
    string = StringCtrl.replace(string, "=", "%3D");
    string = StringCtrl.replace(string, "\n", "%0D%0A");
    string = StringCtrl.replace(string, "\"", "%22");
    string = StringCtrl.replace(string, "\\", "%5C");
    string = StringCtrl.replace(string, " ", "%20");
    return string;
  }
  
  public WOComponent resetForm() {
    // conserver les champs du formulaire
    viewHasChanged = true;
    return null;
  }

  /**
   * Indique si c'est le volet interface HTM qui est affiche
   */
	public boolean isUseCktlMailPage() {
		return useCktlMailPage;
	}

  /**
   * Indique une erreur lors de la tentative d'envoi du mail. Un message
   * d'erreur <code>strErrSendMessage</code> est affiche dans ce cas.
   */
  public boolean errSendMessage() {
  	return !StringCtrl.isEmpty(strErrSendMessage);
  }
  
	
	/**
   * Definit si c'est le volet interface HTM qui doit etre affiche
	 * @param useCktlMailPage
	 */
	public void setUseCktlMailPage(boolean useCktlMailPage) {
		this.useCktlMailPage = useCktlMailPage;
	}

}
