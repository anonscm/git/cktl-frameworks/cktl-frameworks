package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003 Universite de La Rochelle
 * Copyright 2001  Kenny Leung
 * 
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSRange;

/**
 * Ce composant permet d'afficher les donnees dans les listes hierarchiques.
 * Chaque element de la liste peut avoir un liste des fils et possede
 * un element pere (appartient a la liste de ses fils). Les elements sont
 * presentes dans les listes HTML affichees l'un a cote de l'autre.
 * 
 * <p><div align="center"><img src="../../../../doc-files/CktlHXBrowser-1.gif" border="1"></div>
 * 
 * <p>Cette implementation de <i>browser</i> est une version evoluee
 * de celle proposee par Kenny Leung (). Elle utilise un objet de la classe
 * <code>CktlHXBrowserNode</code> pour gerer les listes des objets.
 * 
 * 
 * <h3>Bindings</h3>
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>zeroItem</code></td>
 * <td>obligatoire</td>
 * <td><code>CktlHXBrowserNode</code><br>[<i>null</i>]</td>
 * <td align="left">La racine de l'arborescence des donnees. Cette element
 *   n'est pas affiche et ne represente aucune entite de donnees. Il est
 *   utilise pour acceder uniquement a la liste des elements de la premiere
 *   colonne.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>selectedObject</code></td>
 * <td>obligatoire</td>
 * <td><code>Object</code><br>[<i>null</i>]</td>
 * <td align="left">Le dernier objet selectionne. Si les donnees sont
 *   affichees sur plusieurs colonnes, cet objet correspond a l'objet
 *   selectionne dans la derniere colonne.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>selectedPath</code></td>
 * <td>obligatoire</td>
 * <td><code>NSArray</code><br>[<i>null</i>]</td>
 * <td align="left">Le suite des objets selectionnees de la premiere
 *   jusqu'a la derniere colonne.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>formName</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>null</i>]</td>
 * <td align="left">Le nom du formulaire dans lequel le composant est inclu.
 *   Si la valeur n'est pas indiquee, le premier formulaire du document
 *   est pris.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>hideColumnsControl</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique si les controles de navigation entre les
 *   colonnes doivent etre caches. Ces controles sont affiches en bas-gauche
 *   des listes.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>hideControls</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique s'il tous les controles doivent etre caches.
 *   Ils peuvent etre caches si les le nombre de colonnes de browser est
 *   connu en avance.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>showEmptyColumns</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique si les colonnes vides doivent etre affichees.
 *   Dans ce cas, les listes HTML vides sont affiches. Le nombre des colones
 *   vides depent de la valeur <code>columnsCount</code>.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>columnsCount</code></td>
 * <td>optionnel</td>
 * <td><code>integer</code><br>[<i>3</i>]</td>
 * <td align="left">Le nombre maximal des colonnes qui seront visible.</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @see CktlHXBrowserNode
 */
public class CktlHXBrowser extends CktlWebComponent {
  // Les noms des connecteurs
  private static final String BND_ZERO_ITEM = "zeroItem";
  private static final String BND_SELECTED_OBJECT = "selectedObject";
  private static final String BND_SELECTED_PATH = "selectedPath";
  private static final String BND_FORM_NAME = "formName";
  private static final String BND_HIDE_COLUMNS_CONTROL = "hideColumnsControl";
  private static final String BND_HIDE_CONTROLS = "hideControls";
  private static final String BND_SHOW_EMPTY_COLUMNS = "showEmptyColumns";
  private static final String BND_COLUMNS_COUNT = "columnsCount";
  // Les valeurs par defaut
  private static int DefaultVisibleColsCount = 3;
  private static String NoLeafSymbols = "  &#187;";
  // private static String NoLeafSymbols = "  &gt;&gt;";
  // private static String NoLeafSymbols = "  &raquo;";

  private boolean _selectionChanged;
  private int _numVisibleCols;
  private int _currentVisibleColNum;
  private int _firstVisibleColNum;
  private NSMutableArray _visibleColumns;
  private NSMutableArray _selectedPath;
  private NSArray emptyColumn;
  private CktlHXBrowserNode _item;
  
    //
    // Constructors
    //
  public CktlHXBrowser(WOContext context) {
    super(context);
    _numVisibleCols = -1;
    emptyColumn = new NSArray();
  }
  
    //
    // Overridden Methods
    //
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  public void appendToResponse(WOResponse response, WOContext context) {
    _visibleColumns = null;
    if ( hasBinding(BND_SELECTED_PATH) ) {
      _selectedPath = null;
    }
    _selectionChanged = false;
    super.appendToResponse(response, context);
  }
  
    //
    // Binding Accessors
    //

  private boolean getBoolValueForBinding(String bindingName, boolean defaultValue) {
    if (hasBinding(bindingName))
      return ((Boolean)valueForBinding(bindingName)).booleanValue();
    return defaultValue;
  }

  private NSArray children() {
    // return (NSArray)valueForBinding(BND_CHILDREN);
    return _item.childrenNodes();
  }

  private NSArray columnZero() {
    // return (NSArray)valueForBinding(BND_COLUMN_ZERO);
    return zeroItem().rootNodes();
  }

  private String displayString() {
    // return (String)valueForBinding(BND_DISPLAY_STRING);
    return _item.displayName();
  }

  private boolean isLeaf() {
    // Boolean rep = (Boolean)valueForBinding(BND_IS_LEAF);
    // return (rep == null)?false:(rep.booleanValue());
    return ((_item == null)?false:_item.isLeaf());
  }

  public CktlHXBrowserNode item() {
    return _item;
  }

  public void setItem(Object item) {
    // setValueForBinding(CktlHXBrowserNode, BND_ITEM);
    _item = (CktlHXBrowserNode)item;
  }

  private NSMutableArray selectedPath() {
    if ( _selectedPath == null ) {
      _selectedPath = (NSMutableArray)valueForBinding(BND_SELECTED_PATH);
      if ( _selectedPath == null ) {
        _selectedPath = new NSMutableArray();
        if ( hasBinding(BND_SELECTED_PATH) ) {
          setValueForBinding(_selectedPath, BND_SELECTED_PATH);
        }
      }
    }
    if (_selectedPath.count() == 0) {
      // selectedObject = null;
      _firstVisibleColNum = 0;
    }
    return _selectedPath;
  }

  private String formName() {
    return (String)valueForBinding(BND_FORM_NAME);
  }

  public boolean hideColumnsControl() {
    return getBoolValueForBinding(BND_HIDE_COLUMNS_CONTROL, false);
  }

  public boolean hideControls() {
    return getBoolValueForBinding(BND_HIDE_CONTROLS, false);
  }

  public boolean showEmptyColumns() {
    return getBoolValueForBinding(BND_SHOW_EMPTY_COLUMNS, false);
  }

  private int columnsCount() {
    if (hasBinding(BND_COLUMNS_COUNT)) {
      int value = StringCtrl.toInt(valueForBinding(BND_COLUMNS_COUNT).toString(), -1);
      if (value != -1) return value;
    }
    return DefaultVisibleColsCount;
  }

  private CktlHXBrowserNode zeroItem() {
    return (CktlHXBrowserNode)valueForBinding(BND_ZERO_ITEM);
  }
  
    //
    // Component Accessors
    //
  public int numVisibleCols() {
    if (_numVisibleCols == -1)
      _numVisibleCols = columnsCount();
    return _numVisibleCols;
  }

  public int currentVisibleColNum() {
    return _currentVisibleColNum;
  }

  public void setCurrentVisibleColNum(int colNum) {
    _currentVisibleColNum = colNum;
  }

  public int numColumnsMinusOne() {
    return _numVisibleCols - 1;
  }

  public int numColumnsPlusOne() {
    return _numVisibleCols + 1;
  }

  public boolean shiftRightButtonEnabled() {
    NSMutableArray path;
    int pathCount;
    Object lastObject;

    path = selectedPath();
    lastObject = path.lastObject();
    setItem(lastObject);
    if ( lastObject == null || isLeaf() ) {
      pathCount = path.count();
    } else {
      pathCount = path.count() + 1;
    }
    return pathCount - _firstVisibleColNum > _numVisibleCols;
  }

  public boolean shiftLeftButtonEnabled() {
    return _firstVisibleColNum > 0;
  }

  public boolean showColumn() {
    return currentColumn().count() > 0;
  }

  public NSArray currentColumn() {
    int colNum;
    NSMutableArray path;

    if ( _visibleColumns != null && _visibleColumns.count() >  _currentVisibleColNum ) {
      return (NSArray)_visibleColumns.objectAtIndex(_currentVisibleColNum);
    }
    
        // We're somewhere in the middle of the response part of the cycle.
    if ( _visibleColumns == null ) {
      _visibleColumns = new NSMutableArray();
    }
    path = selectedPath();
    colNum = _firstVisibleColNum + _currentVisibleColNum;

    if ( colNum == 0 ) {
      _visibleColumns.addObject(columnZero());
    } else if ( path.count() > colNum - 1 ) {
      setItem(path.objectAtIndex(colNum - 1));
      if ( !isLeaf() ) {
        _visibleColumns.addObject(children());
      } else {
        _visibleColumns.addObject(new NSArray());
      }
    } else {
      _visibleColumns.addObject(new NSArray());
    }
    return (NSArray)_visibleColumns.lastObject();
  }

  public NSArray emptyColumn() {
    return emptyColumn;
  }

  public String currentItemDisplayString() {
    if ( isLeaf() ) {
      return displayString();
    } else {
      return displayString() + NoLeafSymbols;
    }
  }

  public Object currentSelectedItems() {
    NSMutableArray path;

    path = selectedPath();
    if ( path.count() > _currentColNum() ) {
      return new NSArray(path.objectAtIndex(_currentColNum()));
    } else {
      return new NSArray();
    }
  }

  public void setCurrentSelectedItems(NSArray selection) {
    NSMutableArray path;
    Object selectedObject;
    int colNum;

    path = selectedPath();
    if ( _selectionChanged ) {
      return;
    }
    if ( selection != null && selection.count() > 0 ) {
      selectedObject = selection.objectAtIndex(0);
      colNum = _currentColNum();
      path.insertObjectAtIndex(selectedObject, colNum);
      if ( path.count() > colNum + 1 ) {
        path.removeObjectsInRange(new NSRange(colNum + 1, path.count() - (colNum + 1)));
      }
      if ( colNum - _firstVisibleColNum >= _numVisibleCols - 1 ) {
        _firstVisibleColNum = colNum - (_numVisibleCols - 2);
      }
      _fillVisibleColumns();
      setValueForBinding(path, BND_SELECTED_PATH);
      setValueForBinding(path.lastObject(), BND_SELECTED_OBJECT);
    }
    _selectionChanged = true;
  }

  public String submitActionName() {
    String formName = formName();
    if ((formName != null) && (formName.length() > 0))
      return "document."+formName+".submit()";
    else
      return "document.forms[1].submit()";
  }
  
    //
    // Actions
    //
  public void decreaseVisibleColumns() {
    if (decreaseVisibleColsEnabled()) _numVisibleCols--;
  }

  public boolean decreaseVisibleColsEnabled() {
    return (_numVisibleCols > 1);
  }

  public void increaseVisibleColumns() {
    _numVisibleCols++;
    _fillVisibleColumns();
  }

  public void shiftRight() {
    _firstVisibleColNum++;
  }

  public void shiftLeft() {
    _firstVisibleColNum--;
  }

  /**
    * This is just a dummy so the submission button will have something to be bound to. We do everything in setCurrentSelectedItems.
   **/
  public void selectObject() {
  }

  public void clearSelection() {
    NSMutableArray path;

    path = selectedPath();
    path.removeAllObjects();
    setValueForBinding(path, "path");
    setValueForBinding(null, "selectedObject");
    _firstVisibleColNum = 0;
  }
  
    //
    // Private Methods
    //
  private int _currentColNum() {
    return _firstVisibleColNum + _currentVisibleColNum;
  }

  private void _fillVisibleColumns() {
    NSMutableArray path;
    int pathCount;

    path = selectedPath();
    setItem(path.lastObject());
    if ( isLeaf() ) {
      pathCount = path.count();
    } else {
      pathCount = path.count() + 1;
    }
    if ( pathCount - _firstVisibleColNum < _numVisibleCols ) {
      _firstVisibleColNum = pathCount - _numVisibleCols >= 0 ? pathCount - _numVisibleCols : 0;
    }
  }
}
