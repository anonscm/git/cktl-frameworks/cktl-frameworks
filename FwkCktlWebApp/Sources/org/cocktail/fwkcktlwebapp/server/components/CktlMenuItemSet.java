package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Represente une collection des entrees d'un menu. Contient les methodes
 * pour construire une nouvelle collection.
 * 
 * <p>Chaque objet de la collection est un objet <code>CktlMenuItem</code>.
 * Un objet <code>CktlMenuItemSet</code> est typiquement connecter au
 * connecteur <code>itemSet</code> du composant <code>CktlMenu</code>.
 */
public class CktlMenuItemSet {
  
  /**
   * Toutes les entrees de menu.
   */
  private NSMutableArray menuItems;

  /**
   * Cree une nouvelle collection des entrees de menu.
   */
  public CktlMenuItemSet() {
    menuItems = new NSMutableArray();
  }

  /**
   * Retourne le tableau de tous les elements de menu. Chaque element
   * est un objet <code>CktlMenuItem</code>.
   */
  public NSArray getMenuItems() {
    return menuItems;
  }

  /**
   * Cree et ajoute un nouvelle entree dans le menu du premier niveau.
   * 
   * @param newId L'identifiant de menu.
   * @param newLabel Le libelle de menu.
   * @param newTip Le message affiche lorsque le curseur de la sourie
   *   se positionne au-dessus de cet element.
   * @param newTarget Le nom de la fenetre de navigateur dans laquelle
   *   le composant correspondant a cet element sera affiche.
   * 
   * @see CktlMenuItem
   */
  public void addMenuItem(int newId, String newLabel, String newTip, String newTarget) {
    menuItems.addObject(new CktlMenuItem(-1, newId, newLabel, newTip, newTarget));
  }

  /**
   * Cree et ajoute un nouvelle entree dans le menu du premier niveau.
   * 
   * @param newParentId L'identifiant de l'element pere auquel le nouveau
   *   element sera attache.
   * @param newId L'identifiant de menu.
   * @param newLabel Le libelle de menu.
   * @param newTip Le message affiche lorsque le curseur de la sourie
   *   se positionne au-dessus de cet element.
   * @param newTarget Le nom de la fenetre de navigateur dans laquelle
   *   le composant correspondant a cet element sera affiche.
   * 
   * @see CktlMenuItem
   */
  public void addMenuSubItem(int newParentId, int newId, String newLabel,
                             String newTip, String newTarget) {
    menuItems.addObject(new CktlMenuItem(newParentId, newId, newLabel, newTip, newTarget));
  }

}
