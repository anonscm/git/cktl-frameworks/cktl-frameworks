package org.cocktail.fwkcktlwebapp.server.components;

/*
 * Copyright Université de La Rochelle 2006
 *
 * ctarade@univ-lr.fr
 *
 * Ce logiciel est un programme informatique servant à gérer les comptes
 * informatiques des utilisateurs. 
 * 
 * Ce logiciel est régi par la licence CeCILL soumise au droit français et
 * respectant les principes de diffusion des logiciels libres. Vous pouvez
 * utiliser, modifier et/ou redistribuer ce programme sous les conditions
 * de la licence CeCILL telle que diffusée par le CEA, le CNRS et l'INRIA 
 * sur le site "http://www.cecill.info".

 * En contrepartie de l'accessibilité au code source et des droits de copie,
 * de modification et de redistribution accordés par cette licence, il n'est
 * offert aux utilisateurs qu'une garantie limitée.  Pour les mêmes raisons,
 * seule une responsabilité restreinte pèse sur l'auteur du programme,  le
 * titulaire des droits patrimoniaux et les concédants successifs.

 * A cet égard  l'attention de l'utilisateur est attirée sur les risques
 * associés au chargement,  à l'utilisation,  à la modification et/ou au
 * développement et à la reproduction du logiciel par l'utilisateur étant 
 * donné sa spécificité de logiciel libre, qui peut le rendre complexe à 
 * manipuler et qui le réserve donc à des développeurs et des professionnels
 * avertis possédant  des  connaissances  informatiques approfondies.  Les
 * utilisateurs sont donc invités à charger  et  tester  l'adéquation  du
 * logiciel à leurs besoins dans des conditions permettant d'assurer la
 * sécurité de leurs systèmes et ou de leurs données et, plus généralement, 
 * à l'utiliser et l'exploiter dans les mêmes conditions de sécurité. 

 * Le fait que vous puissiez accéder à cet en-tête signifie que vous avez 
 * pris connaissance de la licence CeCILL, et que vous en avez accepté les
 * termes.
 */

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;


/**
 * Composant permettant la saisie d'heures. C'est un 
 * champ de type texte qui s'auto complete et s'auto
 * controle via des fonctions javascript.
 * 
 * <p><div align="center"><img src="../../../../doc-files/CktlTimeField-1.png" border="1"/></div></p>
 *
 * <p>
 *  <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *	 <tr align="center" class="paramBoxHeader">
 *	  <td>Connecteur</td>
 *	  <td>Definition</td>
 *	  <td>Valeur</td>
 *	  <td>Description</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>fieldName</code></td>
 *	  <td>obligatoire</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le nom HTML du champ contenant l'heure</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>fieldValue</code></td>
 *	  <td>obligatoire</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">La variable contenant l'heure</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>disabled</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>boolean</code><br>[<i>false</i>]</td>
 *	  <td align="left">Indique si le composant est verrouille.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>limit24h</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>WOComponent</code><br>[<i>true</i>]</td>
 *	  <td align="left">Les valeur des heures est elle restreinte de 00h00 jusqu'a 23h59</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>btnEnter</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left"> le bouton a presser. S'il est vide, alors
 * 			ce sera this.form.submit() qui sera lance si submitOnPressEnter = true;</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>submitOnPressEnter</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>boolean</code><br>[<i>false</i>]</td>
 *	  <td align="left">Faut-il faire un submit du formulaire
 * 			contenant le CktlTimeField apres avoir appuyé sur la touche ENTREE</td>
 *	 </tr>
 *	</table>
 * </p>
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */

public class CktlTimeField extends CktlWebComponent {

  // Les noms des connecteurs.
 
  /**
   * La variable de l'heure
   */
	public String fieldValue;

  /**
   * disponibilite du composant. 
   * Par defaut disabled = false;
   */
  public boolean disabled = false;

  /**
   * Le nom du champ HTML
   */
  public String fieldName;
  
  /**
   * Le composant est-il limite a la saisie
   * d'heures 00:00 > 23:59
   * Par defaut limit24h = true
   */
  public boolean limit24h = true;
  
  /**
   * Faut-il faire un submit du formulaire
   * contenant le CktlTimeField apres avoir
   * appuyé sur la touche ENTREE.
   * Par defaut,  submitOnPressEnter = false
   */
  public boolean submitOnPressEnter = false;
  
  /**
   * le bouton a presser. S'il est vide, alors
   * ce sera this.form.submit() qui sera lance
   * si submitOnPressEnter = true;
   */
  public String btnEnter;
  
  public CktlTimeField(WOContext context) {
    super(context);
  }

  /**
   * Code javascript qui doit s'executer lors 
   * la zone de texte perd le focus
   */
  public String onBlur() {
    return "formatter("+ fieldName +","+ limit24h +");";
  }
  
  /**
   * Code javascript qui doit s'executer lors 
   * de l'appui sur une touche dans la zone de texte
   */
  public String onKeyDown() {
    String onKeyDown = "if(event.keyCode==13)";
    if (submitOnPressEnter)
      onKeyDown += "{";
    onKeyDown += onBlur();
    if (submitOnPressEnter) {
      if (!StringCtrl.isEmpty(btnEnter))
        onKeyDown += btnEnter + ".click();";
      else
        onKeyDown += "this.form.submit();";
      onKeyDown += "}";
    }
    onKeyDown += ";";
    return onKeyDown;
  }
  
  public int maxlength() {
    return (limit24h?5:7);
  }

  public int size() {
    return (limit24h?4:6);
  }

  /**
   * Ajout du code JS qui va bien
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
  	addLocalJScript(arg0, "jscript/CktlTimeField.js", fwkCktlWebAppFrameworkName());
  }
}