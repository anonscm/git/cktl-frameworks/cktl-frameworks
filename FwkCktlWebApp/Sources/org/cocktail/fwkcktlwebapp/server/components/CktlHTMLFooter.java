package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * Ce composant represente un element du bas des pages HTML. Il contient le libelle de
 * copyright et l'adresse e-mail de contact pour l'application. Le libelle
 * de copyright correspond a la valeur renvoyee par la ethode
 * <code>copyright</code> de {@link CktlWebApplication}. 
 * Le bas de page peut etre separe du reste de la page par une ligne horizontale&nbsp;:
 * 
 * <p><div align="center"><img src="../../../../doc-files/CktlHTMLFooter-1.gif" border="1"></div>
 * 
 * <h3>Bindings</h3>
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>mail</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>null</i>]</td>
 * <td align="left">L'adresse e-mail du contact.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>mailLabel</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique s'il faut utiliser l'adresse mail par defaut.
 *   Si cette valeur est <i>true</i> et le connecteur <code>mail</code> n'est
 *   pas defini, alors le resultat de la methode <code>contactMail</code> de
 *   {@link CktlWebApplication} est utilse.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>useDefaultMail</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique s'il faut utiliser l'adresse mail par defaut.
 *   Si cette valeur est <i>true</i> et le connecteur <code>mail</code> n'est
 *   pas defini, alors le resultat de la methode <code>contactMail</code> de
 *   {@link CktlWebApplication} est utilse.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>showBar</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique s'il faut separer par une ligne horizontale
 *   le bas de page du reste de la page HTML.</td>
 * </tr>
 * </table>
 * </p>
 */
public class CktlHTMLFooter extends CktlWebComponent {
  // Les noms des bindings du composant.
  private static final String BND_MAIL = "mail";
  private static final String BND_LABEL = "mailLabel";
  private static final String BND_USE_DEFAULT_MAIL = "useDefaultMail";
  private static final String BND_SHOW_BAR = "showBar";

  /**
   * Creer un nouveau composant dans le context donne.
   */
  public CktlHTMLFooter(WOContext context) {
    super(context);
    reset();
  }

  /**
   * Renvoie la valeur <i>false</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe {@link WOComponent} (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  /**
   * Renvoie la valeur <i>true</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean isStateless() {
    return true;
  }

  /**
   * Retourne l'adresse e-mail de contact. Elle est cherchee dans l'ordre
   * suivante :
   * <ul>
   * <li>la valeur du connecteur <code>mail</code> si elle est definit ;</li>
   * <li>la valeur de la methode <code>contacteMail</code> de
   * {@link CktlWebApplication}, si la valeur du connecteur
   * <code>useDefaultMail</code> est definie a true ;</li>
   * <li> <code>null</code> si aucun valeur n'a pas pu etre trouve. Aucun lien mail
   * n'est affiche dans ce cas.</li>
   * </ul>
   * 
   * @see #isUseDefaultMail()
   */
  public String adresseMail() {
    if (hasBinding(BND_MAIL)) return (String)valueForBinding(BND_MAIL);
    if (isUseDefaultMail()) return cktlApp.contactMail();
    return null;
  }

  /**
   * Indique s'il faut utiliser l'adresse e-mail par defaut comme adresse
   * de contact. La valeur retourne correspond au connecteur
   * <code>useDefaultMail</code>. La valeur par defaut et <code>false</code>.
   * 
   * @see #adresseMail()
   */
  public boolean isUseDefaultMail() {
    if (hasBinding(BND_USE_DEFAULT_MAIL))
      return ((Boolean)valueForBinding(BND_USE_DEFAULT_MAIL)).booleanValue();
    return false;
  }

  /**
   * Indique s'il faut afficher une ligne horizontale separant
   * le bas de page de reste du page HTML. La valeur retourne correspond
   * au connecteur <code>showBar</code>. La valeur par defaut et <code>false</code>.
   */
  public boolean isShowBar() {
    if (hasBinding(BND_SHOW_BAR))
      return ((Boolean)valueForBinding(BND_SHOW_BAR)).booleanValue();
    return false;
  }

  /**
   * Renvoie le URL a utiliser pour le lien e-mail. C'est une valeur
   * "<code>mailto:...</code>" utilisee pour l'attribut <code>href</code>
   * de la balise <code>&lt;a&gt;</code>. 
   */
  public String hrefMail() {
    if (adresseMail().length() > 0)
      return "mailto:"+adresseMail();
    else
      return "";
  }

  /**
   * Test si l'adresse e-mail de contact est definie. 
   */
  public boolean hasMail() {
    String mail = adresseMail();
    return ((mail != null) && (mail.length() > 0));
  }

  /**
   * Renvoie le libelle a afficher avant l'adresse mail
   * de contact. C'est la valeur correspodant au connecteur
   * <code>mailLabel</code>.
   * 
   * <p>Le libelle est affichee avant le lien mail. Finissez le libelle
   * par la balise <code>&lt;br&gt;</code> si vous voulez que le libelle
   * soit affiche au-dessus de l'adresse.
   */
  public String mailLabel() {
    if (hasBinding(BND_LABEL)) return (String)valueForBinding(BND_LABEL);
    return null;
  }

  /**
   * Retourne le message copyright de l'application. C'est le resultat
   * de la methode <code>copyright</code> de {@link CktlWebApplication}.
   */
  public String copyright() {
    return cktlApp.copyright();
  }

  private final static String jsParentFolder = "jscript/";
  private final static String DecrypterScriptName = "CktlDecrypter.js";
  
  /**
   * Ajout de la resource CktlDecripter.js
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
    addLocalJScript(arg0, jsParentFolder+DecrypterScriptName, fwkCktlWebAppFrameworkName());
  }
}
