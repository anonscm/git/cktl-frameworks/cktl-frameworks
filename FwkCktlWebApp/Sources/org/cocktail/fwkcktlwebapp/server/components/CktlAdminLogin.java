package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Ce composant represente le formulaire d'authentification pour
 * l'administrateur d'une application. L'administrateur est toujours
 * authentifie uniquement par son mot de passe.
 *
 * <p><div align="center"><img src="../../../../doc-files/CktlAdminLogin-1.gif" border="1"></div>
 *
 * <p>Le composant comporte un formulaire HTML.
 * <font class="importantNote">Ne placez pas <code>CktlAdminLogin</code> dans
 * un autre formulaire lorsque vous l'incluez dans votre page.</font>
 *
 *
 * <h3>Mot de passe</h3>
 *
 * Le mot de passe saisi est compare avec la valeur retournee par la methode
 * <code>getRootPassword</code> de la classe <code>Application</code>. La
 * classe {@link CktlWebApplication} propose une implementation par defaut de cette
 * methode.
 * 
 * <p>Si la methode getRootPassword retourne la valeur <i>null</i>, on
 * tout mot de passe saisi est accepte. Aucun mot de passe n'est necessaire
 * pour se connecter a l'application.</p>
 * 
 * <p>Le mot de passe retourne par la methode <code>getRootPassword</code>
 * doit etre crypte. Voir, par exemple, la commande unix <i>crypt</i>.</p>
 *
 *
 * <h3>Bindings</h3>
 *
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>passLabel</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>"Mot de passe"</i>]</td>
 * <td align="left">Le message affiche lorsque le courseur de la souris se
 *   trouve au-dessus du champ de saisi de mot de passe.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>passTip</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>"Votre mot de passe (password)"</i>]</td>
 * <td align="left">Le message affiche lorsque le courseur de la souris se
 *   trouve au-dessus du champ de saisi de mot de passe.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>buttonLabel</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>"Valider"</i>]</td>
 * <td align="left">Le libelle affiche sur le bouton de la validation de
 *   mot de passe.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>buttonTip</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>"Valider le nom d'utilisateur et
 *   le mot de passe saisis"</i>]</td>
 * <td align="left">Le message affiche lorsque le courseur de la souris
 *   se positionne au-dessus du bouton de validation.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>loginResponder</code></td>
 * <td>obligatoire</td>
 * <td><code>CktlAdminLoginResponder</code><br>[<i>null</i>]</td>
 * <td align="left">L'objet qui recevera les notifications lors
 *   d'authentification d'utilisateur. Voir la documentation de l'interface
 *   <code>CktlAdminLoginResponder</code> pour plus d'informations.
 *   </td>
 * </tr>
 * </table>
 * </p>
 *
 *
 * <h3>Utilisation</h3>
 *
 * Le composant <code>CktlAdminLogin</code> utilise un objet implementant
 * l'interface <code>CktlAdminLoginResponder</code> pour informer de la reusite
 * de l'authentification. Dans ce cas, la methode <code>loginAccepted</code>
 * de cet objet est appelee. Cette methode doit renvoyer le composant qui
 * sera affiche ensuite.  
 * 
 * <p>Si l'authetification echoue, la page en cours est reaffiche avec le
 * message de la derniere erreur.</p>
 *
 * 
 * @see CktlAdminLoginResponder
 * @see CktlLogin
 */
public class CktlAdminLogin 
	extends CktlWebComponent {
 
	// Les noms des bindings
  private static final String BND_PASS_LABEL = "passLabel";
  private static final String BND_PASS_TIP = "passTip";
  private static final String BND_BUTTON_LABEL = "buttonLabel";
  private static final String BND_BUTTON_TIP = "buttonTip";
  private static final String BND_LOGIN_RESPONDER = "loginResponder";
  // Les valeurs par defaut pour les bindings
  private static final String DFL_PASS_LABEL = "Mot de passe";
  private static final String DFL_PASS_TIP = "Mot de passe de l'administrateur";
  private static final String DFL_BUTTON_LABEL = "Valider";
  private static final String DFL_BUTTON_TIP = "Valider le mot de passe";

  /**
    * L'indicateur de l'erreur de saisi de mot de passe d'utilisateur.
   */
  public boolean erreurPassword;

  /**
    * Le mot de passe d'utilisateur saisi.
   */
  public String password;

  /**
    * Cree un nouveau composant dans le context donnee.
   */
  public CktlAdminLogin(WOContext context) {
    super(context);
  }

  /**
    * Renvoie la valeur <i>false</i>.
   *
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  /**
   * Effectue la validation de mot de passe saisi. Le mot de passe saisi
   * est compare avec la valeur retournee par la methode
   * <code>getRootPassword</code> de la classe <code>Application</code>.
   * 
   * <p>Si la validation echoue, cette methode retourne la reference vers
   * la meme page. Un message d'erreur est alors affiche sur la page.</p>
   * 
   * <p>Si la validation reusit, la page donnee par la methode
   * <code>connectAccepted</code> de <code>CktlAdminLoginResponder</code> est
   * retourne.</p>
   * 
   * @see CktlAdminLoginResponder#connectAccepted()
   */
  public WOComponent validate() {
    String rootPass =  cktlApp.getRootPassword();
    if (rootPass != null) {
      password = StringCtrl.normalize(password);
      erreurPassword = !CryptoCtrl.equalsToCryptedPass(
      		CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, password, rootPass);
    } else {
      erreurPassword = false;
    }
    if (erreurPassword) return null;
    return loginResponder().connectAccepted();
  }

  /**
   * Retourne le libelle du champ de mot de passe. Cette valeur peut etre
   * definie via le connecteur <code>passLabel</code>.
   */
  public String getPassLabel() {
    if (hasBinding(BND_PASS_LABEL))
      return (String)valueForBinding(BND_PASS_LABEL);
    return DFL_PASS_LABEL;
  }

  /**
   * Retourne le libelle du bouton de la validation. Cette valeur peut etre
   * definie via le connecteur <code>buttonLabel</code>.
   */
  public String getButtonLabel() {
    if (hasBinding(BND_BUTTON_LABEL))
      return (String)valueForBinding(BND_BUTTON_LABEL);
    return DFL_BUTTON_LABEL;
  }

  /**
   * Retourne le message qui est affiche lorsque la souris passe au-dessus
   * du champ de mot de passe. Cette valeur peut etre definie via le connecteur
   * <code>passTip</code>.
   */
  public String getPassTip() {
    if (hasBinding(BND_PASS_TIP))
      return (String)valueForBinding(BND_PASS_TIP);
    return DFL_PASS_TIP;
  }

  /**
   * Retourne le message qui est affiche lorsque la souris passe au-dessus
   * du bouton de la validation. Cette valeur peut etre definie via le
   * connecteur <code>buttonTip</code>.
   */
  public String getButtonTip() {
    if (hasBinding(BND_BUTTON_TIP))
      return (String)valueForBinding(BND_BUTTON_TIP);
    return DFL_BUTTON_TIP;
  }

  /**
   * Retourne l'objet dont la methode <code>connectAccepted</code> est utilisee
   * pour informer de succes de l'authentification.
   * 
   * <p>Cet objet est defini via le connecteur <code>loginResponder</code>.</p>
   *
   * @see CktlAdminLoginResponder 
   */
  private CktlAdminLoginResponder loginResponder() {
    return (CktlAdminLoginResponder)valueForBinding(BND_LOGIN_RESPONDER);
  }
}
