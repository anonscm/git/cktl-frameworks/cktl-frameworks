package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;


/**
 * Ce composant definit la mise en page par defaut pour afficher des
 * messages de type "remarque". Le text du message est saisi dans le
 * composant (c'est un conteneur WebObjects).
 *
 * <p><div align="center"><img src="../../../../doc-files/CktlNote-1.gif" border="1"/></div></p>
 * 
 * <h3>Bindings</h3>
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>width</code></td>
 * <td>optionnel</td>
 * <td><code>int</code><br>[<i>indefini</i>]</td>
 * <td align="left">La largeur de la zone des remarques. Le format de la valeur
 *   peut etre le meme que l'attribut <code>width</code> de la balise
 *   HTML <code>&lt;table&gt;</code>. Pa defaut, on utilise la largeur du
 *   texte de message.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>padding</code></td>
 * <td>optionnel</td>
 * <td><code>int</code><br>[<i>10</i>]</td>
 * <td align="left">L'espace entre les limites de la zone de note et le
 *   texte du message.</td>
 * </tr>
 * </table>
 * </p>
 */
public class CktlNote extends CktlWebComponent {
  // Les noms des connecteurs.
  private final String BND_PADDING = "padding";

  /**
   * Cree un nouveau composant dans le context donnee. 
   */
  public CktlNote(WOContext context) {
    super(context);
  }

  /**
   * Retourne la valeur de "padding" de texte du remarque
   * sous forme d'une chaine de caracteres.
   */
  public String contentPadding() {
    String padding = (String)valueForBinding(BND_PADDING);
    if (StringCtrl.normalize(padding).length() == 0)
      return "10";
    else
      return padding;
  }

  /**
   * Renvoie la valeur <i>false</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }
  
  /**
   * Renvoie la valeur <i>true</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean isStateless() {
    return true;
  }
}
