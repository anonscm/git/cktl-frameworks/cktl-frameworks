package org.cocktail.fwkcktlwebapp.server.components;


import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;

/**
 * Classe regroupant l'ensemble des champs calcules automatiquement
 * a partir de bindings d'une {@link CktlWebPage}.
 * 
 * @see CktlWebPageAccessors
 */

public abstract class CktlWebPageAutoBindings extends CktlWebPageBindings {

	public CktlWebPageAutoBindings(WOContext context) {
		super(context);
	}

	///** Le prefixe de la commande pour aller directement a une ancre HTML */
  //private final static String INSERTED_ONLOAD_DOCUMENT_LOCATION_PREFIX = "document.location='#";
	///** Le suffixe de la commande pour aller directement a une ancre HTML */
  //private final static String INSERTED_ONLOAD_DOCUMENT_LOCATION_SUFFIX =  "'";
	/** Le nom du connecteur pour definir l'action javscript a effectuer au prochain rechargement de la page */
  private static final String BND_ON_LOAD 								= "onLoad";
	///** L'ancre HTML a atteindre au prochain rechargement de la page */
  //private static final String BND_TARGET_POSITION 				= "targetPosition";

  /**
   * Retourne la valeur normalisee de la valeur de la variable <code>onLoad</code> du <code>body</code>.
   */
  public String onLoadValue() {
    String value = null;
    if (hasBinding(BND_ON_LOAD) &&
        (StringCtrl.normalize((String)valueForBinding(BND_ON_LOAD)).length() > 0)) {
       value = (String)valueForBinding(BND_ON_LOAD);
    } /*else if (hasBinding(BND_TARGET_POSITION)) {
      if (StringCtrl.normalize((String)valueForBinding(BND_TARGET_POSITION)).length() > 0) {
        StringBuffer sb = new StringBuffer(INSERTED_ONLOAD_DOCUMENT_LOCATION_PREFIX);
        sb.append(valueForBinding(BND_TARGET_POSITION)).append(INSERTED_ONLOAD_DOCUMENT_LOCATION_SUFFIX);
        value = sb.toString();
      }
    }*/
    return value;
  }
}
