package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOContext;

/**
 * Ce composant represente une page a afficher lorsque le delai d'inactivite
 * est depase pour une session de travail (<i>session time-out</i>). Cette page
 * peut remplacer la page par defaut affichee par WebObjects.
 * 
 * <p>La page contient le lien permettant se reconnecter a l'application.
 * Elle peut aussi contenir le lien permettant de se connecter a la page
 * principale du site (la methode <code>mainWebSiteURL</code> de
 * {@link CktlWebApplication}).
 * 
 * <p><div align="center"><img src="../../../../doc-files/CktlTimeoutPage-1.gif" border="1"></div>
 * 
 * <p>Si la methode {@link CktlWebApplication#getSessionTimeoutPage(WOContext)}
 * n'est pas surchargee, alors le composant {@link CktlTimeoutPage} est utilise comme la page
 * de timeout par defaut.
 * 
 * <p>La page generee utilise la mise en page par defaut des pages
 * HTML de l'application. La mise en page defaut peut etre modifiee en
 * appelant les methodes <code>setLogos</code>, <code>setStyles</code>,
 * <code>setIgnoreDefaultLogos</code> et <code>setIgnoreDefaultStyles</code>
 * heritees de la classe {@link CktlWebPage}.
 * 
 * @see CktlWebApplication
 * @see CktlDefautPage
 */
public class CktlTimeoutPage extends CktlWebComponent {

  public CktlTimeoutPage(WOContext context) {
    super(context);
  }

  public boolean isStateless() {
    return true;
  }
  
  public String applicationURL() {
    return cktlApp.getApplicationURL(context());
  }

  public String siteURL() {
    return cktlApp.mainWebSiteURL();
  }

  public boolean hasSiteURL() {
    return (cktlApp.mainWebSiteURL() != null);
  }

  public String mailAddress() {
    return cktlApp.contactMail();
  }

  public String mailToAddress() {
    return "mailto:"+mailAddress();
  }

  public boolean hasContactMail() {
    return (mailAddress() != null);
  }
}
