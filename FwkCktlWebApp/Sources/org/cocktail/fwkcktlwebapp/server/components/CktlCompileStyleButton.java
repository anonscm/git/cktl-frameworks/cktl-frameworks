package org.cocktail.fwkcktlwebapp.server.components;

import org.cocktail.fwkcktlwebapp.common.css.CktlLessCompiler;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class CktlCompileStyleButton extends CktlWebComponent {
	
	public final static String BINDING_lessInputFileName = "lessInputFileName";
	public final static String BINDING_outputFolder = "outputFolder";
	public final static String BINDING_cssOutputFileName = "cssOutputFileName";

	public CktlCompileStyleButton(WOContext context) {
        super(context);
    }

    public String lessInputFileName() {
    	return stringValueForBinding(BINDING_lessInputFileName, "application.less");
    }

    public String outputFolder() {
    	return stringValueForBinding(BINDING_outputFolder, "styles");
    }
    
    public String cssOutputFileName() {
    	return stringValueForBinding(BINDING_cssOutputFileName, "application.css");
    }
    
    public Boolean show() {
    	return cktlApp().config().booleanForKey("MODE_DEBUG");
    }
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }
	public WOActionResults compile() {
		
		CktlLessCompiler compiler = new CktlLessCompiler();
		
		try {
			compiler.compile(lessInputFileName(), outputFolder(), cssOutputFileName());
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
		
	}
}