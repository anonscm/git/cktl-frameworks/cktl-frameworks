package org.cocktail.fwkcktlwebapp.server.components;



import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLNode;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSData;


/**
 * Ce composant est utlise pour analyser une requete en format XML et
 * plus particulierement, une requete SOAP. Lors d'initialisation de
 * composant, il effectue l'analyse d'un document XML et creer l'arborescence
 * des noeuds. Chaque noeud est represente par une instance de la classe
 * {@link CktlXMLNode}. Le noeud racine est acessible a l'aide de
 * la methode {@link #documentRootNode()} .
 * 
 * <p>Lors que ce composant est utilisee, il cree un document XML en format
 * SOAP.</p>
 * 
 * @see CktlXMLNode
 */
public class CktlSOAPComponent extends CktlWebComponent {
  
  /**
   * Reference vers le noeud racine de la requette XML.
   */
  private CktlXMLNode rootNode;
  
  /**
   * Le code d'une erreur.
   */
  private String faultCode;
  
  /**
   * La description d'une erreur.
   */
  private String faultString;
  
  /**
   * Cree un nouveau composant pour representer un document SOAP/XML.
   * <code>context</code> represente le context dans lequel le document
   * est transmis. 
   */
  public CktlSOAPComponent(WOContext context) {
    super(context);
  }

  /**
   * Initialisation de dialogue requete/reponse entre le client et le serveur.
   * Cette methode fait appel a la methode initSOAP pour effetuer l'analyse
   * de document transmis de client vers l'application.
   */
  public void awake() {
    super.awake();
    rootNode = null;
    faultCode = null;
    faultString = null;
    initSOAP();
  }

  /**
   * Retourne <code>true</code> pour indiquer que le composant est un composant
   * sans etat et qu'il ne cree pas de sessions.
   */
  public boolean isStateless() {
    return true;
  }

  /**
   * Redefinie la reponse pour indiquer qu'il s'agit d'un document au format
   * XML : <code>Content-Type : text/xml</code>
   */
  public void appendToResponse(WOResponse aResponse, WOContext aContext) {
    super.appendToResponse(aResponse, aContext);
    aResponse.setHeader("text/xml", "Content-Type");
  }

  /**
   * Effectue l'analyse de la requette transmise de client vers l'application.
   * On suppose qu'il s'agit d'un document au format XML.
   */
  public void initSOAP() {
    NSData content = context().request().content();
    if (content != null)
      rootNode = CktlXMLNode.parse(new String(content.bytes()));
    if (rootNode == null) {
      CktlLog.log("CRISOAPComponent.initSoap() - Empty Message Content");
    }
  }
  
  /**
   * Definit le code d'une erreur SOAP.
   */
  public void setFaultCode(String newCode) {
    faultCode = newCode;
  }
  
  /**
   * Retourne le code d'une erreur SOAP. Retourne <code>null</code> si toutes les
   * operations sont effectuees sans erreurs.
   */
  public String getFaultCode() {
    return faultCode;
  }

  /**
   * Definit la description d'une erreur SOAP.
   */
  public void setFaultString(String newString) {
    faultString = newString;
  }
  
  /**
   * Retourne la description d'une erreur SOAP. Retourne <code>null</code> toutes
   * les operations sont effectuees sans erreurs.
   */
  public String getFaultString() {
    return faultString;
  }

  /**
   * Retourne la reference vers le noeud du document XML representant
   * la requete SOAP. Retourne <code>null</code> si la requete ne contient pas
   * de document.
   */
  public CktlXMLNode documentRootNode() {
    return rootNode;
  }

  /**
   * Retourne la valeur contenue entre les balises <code>tagName</code>.
   * Retourne <code>null</code> si la balise ne se trouve pas dans le document XML.
   * Dans ce cas, un message d'erreur est enregistre dans le composant et
   * il ensuite sera retourne au client.
   * 
   * <p>Si on souhaite recuperer la valeur d'une balise sans produire un
   * message d'erreur, il faut faire appel a la methode
   * {@link #tryValueForTagName(String)}</p>
   * 
   * @see #tryValueForTagName(String)
   */
  public String getValueForTagName(String tagName) {
    String tagValue = tryValueForTagName(tagName);
    if (tagValue != null) {
      return tagName;
    } else {
      faultCode = "1001";
      faultString = "Unknown tag name : "+tagName;
      return null;
    }
  }

  /**
   * Retourne la valeur contenue entre les balises <code>tagName</code>.
   * Retourne <code>null</code> si la balise ne se trouve pas dans le document XML.
   * 
   * @see #getValueForTagName(String)
   */
  public String tryValueForTagName(String tagName) {
    if (rootNode != null) {
      CktlXMLNode node = rootNode.findChild(tagName, true);
      if (node != null) return node.getCharacters();
    }
      return null;
  }
}
