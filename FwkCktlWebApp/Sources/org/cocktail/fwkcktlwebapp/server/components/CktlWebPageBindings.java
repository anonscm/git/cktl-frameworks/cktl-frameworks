package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;



/**
 * Classe regroupant l'ensemble des bindings d'une {@link CktlWebPage}.
 * Cette classe herite de {@link CktlWebPageAccessors}, elle surcharge
 * tous les getters de cette page, afin que les valeurs passees par 
 * binding soient toujours prioritaires par rapport aux valeurs passees
 * via setter.
 * 
 * @see CktlWebPageAccessors
 */

public abstract class CktlWebPageBindings 
	extends CktlWebPageAccessors {
  
	/**
   * Faut-il ne pas utilise le style css par defaut par defaut.
   */
  private static final String BND_IGNORE_DEFAULT_STYLES 	= "ignoreDefaultStyles";
  /**
   * Faut-il ne pas utilise l'icone par default dans la barre d'adresse.
   */
  private static final String BND_IGNORE_DEFAULT_LOGOS 		= "ignoreDefaultLogos";
  /**
   * {@link CktlDefautPage} affiche par defaut le composant {@link CktlHTMLFooter} 
   * Passer cette valeur a <code>true</code> pour le masquer.
   */
  private static final String BND_HIDE_FOOTER 						= "hideFooter";
  /**
   * C'est binding <code>contactMail</code> du composant {@link CktlHTMLFooter} 
   * si <code>hideFooter</code>=<code>false</code>
   */
  private static final String BND_CONTACT_MAIL 						= "contactMail";
  /**
   * C'est binding <code>mailLabel</code> du composant {@link CktlHTMLFooter} 
   * si <code>hideFooter</code>=<code>false</code>
   */
  private static final String BND_MAIL_LABEL 							= "mailLabel";

  // Les noms des connecteurs du composant.
  private static final String BND_USE_TOOL_TIP 						= "useToolTip";
  private static final String BND_PAGE_TITLE 							= "pageTitle";
	///** L'ancre HTML a atteindre au prochain rechargement de la page */
  private static final String BND_TARGET_POSITION 				= "targetPosition";

  public CktlWebPageBindings(WOContext context) {
		super(context);
	}


  /**
   * Indique s'il faut ignorer les definitions par defaut des icones d'une pages
   * HTML.
   */
  public boolean isIgnoreDefaultLogos() {
    return hasBinding(BND_IGNORE_DEFAULT_LOGOS) ? 
    		((Boolean) valueForBinding(BND_IGNORE_DEFAULT_LOGOS)).booleanValue() : 
    			super.isIgnoreDefaultLogos();
  }

  /**
   * Indique s'il faut ignorer les definitions par defaut des styles CSS.
   */
  public boolean isIgnoreDefaultStyles() {
    return hasBinding(BND_IGNORE_DEFAULT_STYLES) ? 
    		((Boolean) valueForBinding(BND_IGNORE_DEFAULT_STYLES)).booleanValue() : 
    			super.isIgnoreDefaultStyles();
  }

  /**
   * Indique s'il faut cacher le bas de page par defaut inclus dans une page
   * genere. La valeur par defaut est <code>true</code> (afficher le bas de page).
   * 
   * @see #setHideFooter(boolean)
   */
  public boolean isHideFooter() {
  	return hasBinding(BND_HIDE_FOOTER) ? 
  			((Boolean) valueForBinding(BND_HIDE_FOOTER)).booleanValue() : 
  				super.isHideFooter();
  }
  
  /**
   * Retourne le texte a afficher a cote de lien email dans le bas de la page
   * HTML. Si toutefois, le binding <code>mailLabel</code> est renseigne, alors
   * c'est cette valeur qui est prise en compte.
   */
  public String getMailLabel() {
    return hasBinding(BND_MAIL_LABEL) ? 
    		(String) valueForBinding(BND_MAIL_LABEL) : 
    			super.getMailLabel();
  }
    
  /**
   * Retourne l'adresse email du lien de "contact" dans le bas d'une page HTML.
   * Si toutefois, le binding <code>contactMail</code> est renseigne, alors
   * c'est cette valeur qui est prise en compte.
   */
  public String getContactMail() {
    return hasBinding(BND_CONTACT_MAIL) ?
    		(String) valueForBinding(BND_CONTACT_MAIL) : 
    			super.getContactMail();
  }
  
  /**
   * Indique s'il faut charger les sources JavaScript pour le composant
   * <code>CktlToolTip</code>. La valeur du connecteur
   * <code>useToolTip</code> est utilisee. La valeur <code>false</code> est
   * retournee si le connecteur n'est pas defini.
   */
  public boolean isUseToolTip() {	
    return hasBinding(BND_USE_TOOL_TIP) ?
    		((Boolean)valueForBinding(BND_USE_TOOL_TIP)).booleanValue() : 
    			super.isUseToolTip();
  }
  
  /**
   * Donne la valeur du binding <code>pageTitle</code>.
   * Si cette valeur n'est pas definie, alors <code>null</code>
   * est retourne. 
   */
  public String getPageTitle() {
    return hasBinding(BND_PAGE_TITLE) ?
      (String) valueForBinding(BND_PAGE_TITLE) :
      	super.getPageTitle();
  }

  /**
   * Donne la valeur du binding <code>targetPosition</code>.
   * Si cette valeur n'est pas definie, alors <code>null</code>
   * est retourne. 
   */
  public String getTargetPosition() {
    return hasBinding(BND_TARGET_POSITION) ?
      (String) valueForBinding(BND_TARGET_POSITION) :
      	super.getTargetPosition();
  }
}
