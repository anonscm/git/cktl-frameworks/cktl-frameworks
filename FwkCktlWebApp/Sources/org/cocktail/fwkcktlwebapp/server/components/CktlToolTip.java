package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2006 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */



import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;

/**
 * Ce composant permet de faire des infos-bulles plus completes
 * que celle fournie de base par un navigateur. Elle utilise 
 * le DHTML.
 * 
 * Il doit etre inserer au meme titre qu'on insere des WOHyperLink
 * dans une page HTML.
 * 
 * le code utilise la librairie disponible a cette adresse
 * <a href=http://www.walterzorn.com/tooltip/tooltip_e.htm>
 * http://www.walterzorn.com/tooltip/tooltip_e.htm</a>
 *
 * <p><div align="center"><img src="../../../../doc-files/CktlToolTip-1.png" border="1"/></div></p>
 * 
 * <p>
 *  <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *	 <tr align="center" class="paramBoxHeader">
 *	  <td>Connecteur</td>
 *	  <td>Definition</td>
 *	  <td>Valeur</td>
 *	  <td>Description</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>htmlText</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le contenu HTML de l'info bulle.</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>useHelpCursor</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>boolean</code><br>[<i>true</i>]</td>
 *	  <td align="left">Faut-il changer le curseur par un point d'interrogation
 * 			sur la zone sensible</td>
 *	 </tr>
 *	</table>
 * </p>
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */

public class CktlToolTip extends CktlWebComponent {

  // Les noms des connecteurs.
  private final String BND_HTML_TEXT        = "htmlText";
  private final String BND_USE_HELP_CURSOR  = "useHelpCursor";
  
  public CktlToolTip(WOContext context) {
    super(context);
  }

  /**
   * Le code javascript qui permet de declencher l'affichage de
   * l'info bulle.
   */
  public String onMouseOverTip() {
    String htmlText = (String)valueForBinding(BND_HTML_TEXT);
    // on escape le caractere "'" qui fait planter 
    htmlText = StringCtrl.replace(htmlText, "'", "\\'");
    //String jscript = "return escape('" + htmlText + "')"; 
    String jscript = "Tip('" + htmlText + "')";
    return jscript;
  }  
  
  /**
   * Faut-il changer le curseur par un point d'interrogation
   * sur la zone sensible (par defaut OUI)
   */
  public Boolean useHelpCursor() {
    if (valueForBinding(BND_USE_HELP_CURSOR) != null)
      return (Boolean) valueForBinding(BND_USE_HELP_CURSOR);
    return Boolean.TRUE;
  }

  /**
   * Renvoie la valeur <i>false</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }
  
  /**
   * Renvoie la valeur <i>true</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean isStateless() {
    return true;
  }
  
}