package org.cocktail.fwkcktlwebapp.server.components;

import java.util.UUID;

import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WOConstantValueAssociation;
import com.webobjects.foundation.NSDictionary;

import er.extensions.appserver.ERXApplication;
import er.extensions.components._private.ERXSubmitButton;

public class CktlSubmitButton extends ERXSubmitButton {

	// private WOAssociation _disabled;
	
	public static final String BINDING_id = "id";
	protected WOAssociation _id;
	// private String id;
	
	public static final String BINDING_type = "type";
	protected WOAssociation _type;
	public static final String BINDING_title = "title";
	protected WOAssociation _title;

	/** Le texte a afficher */
	public static final String BINDING_text = "text";
	protected WOAssociation _text;

	/** Indique s'il faut afficher le text */
	public static final String BINDING_showText = "showText";
	protected WOAssociation _showText;

	/** Indique s'il l'image est situee a droite du label */
	public static final String BINDING_isImagePositionIsRight = "isImagePositionIsRight";
	protected WOAssociation _isImagePositionIsRight;

    protected WOAssociation _style;

	public CktlSubmitButton(String arg0, NSDictionary<String, WOAssociation> nsdictionary, WOElement arg2) {
		super(arg0, nsdictionary, arg2);
		this._id = ((WOAssociation)this._associations.removeObjectForKey(BINDING_id));
		this._type = ((WOAssociation)this._associations.removeObjectForKey(BINDING_type));
		this._title = ((WOAssociation)this._associations.removeObjectForKey(BINDING_title));
		this._text = ((WOAssociation)this._associations.removeObjectForKey(BINDING_text));
		this._showText = ((WOAssociation)this._associations.removeObjectForKey(BINDING_showText));
		this._isImagePositionIsRight = ((WOAssociation)this._associations.removeObjectForKey(BINDING_isImagePositionIsRight));
        // hack for 5.4
        if (ERXApplication.isWO54()) {
        	_style = (WOAssociation) nsdictionary.valueForKey("style");
        }
        else {
        	_style = (WOAssociation)_associations.removeObjectForKey("style");
        }
	}

    public void appendAttributesToResponse(WOResponse woresponse, WOContext wocontext) {
    	super.appendAttributesToResponse(woresponse, wocontext);
       	String css = "";
       	
   		if (_style != null) {
			css = (String) _style.valueInComponent(wocontext.component());
		}
       	
   		css = "border:none;padding:0px;margin:0px;background:none;"+css;
    	if(isDisabledInContext(wocontext)) {
    		css += "opacity:0.5;cursor:default;";
    	} else {
    		css += "opacity:1;cursor:pointer;";
    	}

    	if(css.length() > 0) {
    		woresponse._appendTagAttributeAndValue("style", css, false);
    	}
    	if (_id != null) {
    		woresponse._appendTagAttributeAndValue("id", (String) _id.valueInComponent(wocontext.component()), false);
    	} else {
    		String newId = UUID.randomUUID().toString().replaceAll("-", "_");
    		this._id = new WOConstantValueAssociation(newId);
    		woresponse._appendTagAttributeAndValue("id", newId, false);    		
    	}
    	
    }
    
	public void appendTagAttributeToResponse(WOResponse response, String name, Object object) {
		if (object != null) {
			response._appendTagAttributeAndValue(name, object.toString(), true);
		}
	}
	
	@Override
	/*
	 * EGE 11/06/2010 Ne pas eliminer !!!
	 * Permet de provoquer une erreur de compilation si on n'utilise pas un frw ERExtensions issue d'une version svn > 10758
	 */
	protected String type() {
		return super.type();
	}
	
	@Override
	public void appendChildrenToResponse(WOResponse paramWOResponse, WOContext paramWOContext) {
		appendButtonLookToResponse(paramWOResponse, paramWOContext.component());
	}
	
	private void appendButtonLookToResponse(WOResponse response, WOComponent component) {
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button cktl_button_base");
		response.appendContentString(">");
		
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_outer_box");
		response.appendContentString(">");
		
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_inner_box");
		response.appendContentString(">");
		
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_pos");
		response.appendContentString(">");
		
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_top_shadow");
		response.appendContentString(">");
		response.appendContentString("&nbsp;");
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");
		
		response.appendContentString("<div"); 
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "id", getButtonId(component));
		appendTagAttributeToResponse(response, "class", "cktl_button_base_content");
		response.appendContentString(">");

		if (isImagePositionIsRight(component)) {
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString(getText(component)+"&nbsp;");
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
			response.appendContentString("<span"); 
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
			response.appendContentString(">");
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
		} else {
			response.appendContentString("<span"); 
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
			response.appendContentString(">");
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString("&nbsp;"+getText(component));
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");				
		}

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");
		
	}
	
	private boolean isImagePositionIsRight(WOComponent component) {
		return ((this._isImagePositionIsRight != null) && (this._isImagePositionIsRight.booleanValueInComponent(component)));
	}

	public String getId(WOComponent component) {
//		if (id == null) {
//			if (this._id == null) {
//				id = UUID.randomUUID().toString().replaceAll("-", "_");
//			} else {
//				id = (String)this._id.valueInComponent(component);
//			}
//		}
		return (String)this._id.valueInComponent(component);
	}

	public String getButtonId(WOComponent component) {
		return getId(component)+"_btn";
	}

	public String getButtonBaseImageClass(WOComponent component) {
		String buttonBaseImageClass = "cktl_action_button ";
		
		if ((this._type != null)) {
			buttonBaseImageClass += "cktl_action_" + (String)this._type.valueInComponent(component) + " ";
		}
		buttonBaseImageClass += "cktl_button_icon cktl_inline_block";
		
		return buttonBaseImageClass;
	}

	public String getText(WOComponent component) {
		if ((this._text != null)) {
			return (String) this._text.valueInComponent(component);
		}
		return null;
	}


}
