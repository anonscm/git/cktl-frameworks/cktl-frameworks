package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOElement;
import com.webobjects.appserver._private.WOConstantValueAssociation;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.components.conditionals.ERXWOTemplate;

public class CktlBreadcrumb extends ERXWOTemplate {
	public CktlBreadcrumb(String aName, NSDictionary associations, WOElement template) {
		super(aName, CktlBreadcrumb.processAssociations(associations), template);
	}

	protected static NSDictionary processAssociations(NSDictionary associations) {
		NSMutableDictionary mutableAssociations = (NSMutableDictionary) associations;
		mutableAssociations.setObjectForKey(new WOConstantValueAssociation("breadcrumb"), "templateName");
		return mutableAssociations;
	}


}