package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;

/**
 * Ce composant permet de generer les liens avec les adresses mail
 * dans les documents HTML. Il genere la balise
 * <code>&lt;a href="mailto:...&gt;</code>.
 * 
 * <p>
 * 	<div align="center">
 * 		<img src="../../../../doc-files/CktlMailTo-1.png" border="1"/>
 * 	</div>
 * </p>
 * 
 * <p>La balise generee peut etre cryptee afin de proteger l'adresse
 * mail des "spams". Dans ce cas, un JavaScript est utilise pour decrypter
 * et afficher la balise. Les fonctions de JavaScript se trouvent dans un
 * fichier externe.
 * 
 * <p>Pour inclure les le lien vers le fichier JavaScript, le fichier
 * <code>CktlDecrypter.js</code> doit etre utilise. Celui-si peut etre inclu
 * directement dans une entet de document HTML. Si le composant
 * {@link CktlDefautPage} est utilise, alors ce lien est inclu par defaut.
 * 
 * <p>Le fichier <code>CktlDecrypter.js</code> n'est pas necessaire, si les
 * donnees ne sont pas cryptees.
 * 
 * <p>Le composant {@link CktlMailTo} est utilise dans
 * {@link CktlHTMLFooter}.
 * 
 * 
 * <h3>Bindings</h3>
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>mail</code></td>
 * <td>obligatoire</td>
 * <td><code>String</code><br>[<i>null</i>]</td>
 * <td align="left">L'adresse e-mail a utiliser.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>mailLabel</code></td>
 * <td>obligatoire</td>
 * <td><code>String</code><br>[<i>null</i>]</td>
 * <td align="left">Le text a afficher sur le lien "mailto:".</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>mailTip</code></td>
 * <td>optionnel</td>
 * <td><code>String</code><br>[<i>"Cliquez pour envoyer un mail"</i>]</td>
 * <td align="left">Le texte du message affiche lorsque le courseur de la
 *   sourie passe au-dessus de lien "mailto:...".</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>useUncryptedMail</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique si le lien ne doit pas etre crypte. Par defaut,
 *   le lien avec l'adresse mail est cryptee.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>showMailIcon</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique si une icone graphique doit etre affichee
 *   devant le lien mail.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>useJavascriptEscape</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Indique si le href doit &ecirc;tre encode en utilisant la
 * fonction javascript escape (utile pour les mailto avec body)</td>
 * </tr>
 * </table>
 * </p>
 * 
 * @see CktlHTMLFooter
 */
public class CktlMailTo extends CktlWebComponent {
  // Les noms des bindings
  private static final String BND_MAIL = "mail";
  private static final String BND_MAIL_LABEL = "mailLabel";
  private static final String BND_MAIL_TIP = "mailTip";
  private static final String BND_USE_UNCRYPTED_MAIL = "useUncryptedMail";
  private static final String BND_SHOW_MAIL_ICON = "showMailIcon";
  // Les valeurs par defaut pour certains connecteurs.
  private static final String DefaultMailTip = "Cliquez pour envoyer un mail";
  
  public CktlMailTo(WOContext context) {
    super(context);
  }

  /**
   * Renvoie la valeur <code>false</code>
   *
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe {@link WOComponent} (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  /**
   * Renvoie la valeur <code>true</code>.
   *
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe {@link WOComponent} (WebObjects).
   */
  public boolean isStateless() {
    return true;
  }

  private String getMail() {
    return (String)valueForBinding(BND_MAIL);
  }

  private String getMailLabel() {
    return (String)valueForBinding(BND_MAIL_LABEL);
  }

  private String getMailTip() {
    if (hasBinding(BND_MAIL_TIP))
      return (String)valueForBinding(BND_MAIL_TIP);
    return DefaultMailTip;
  }
  
  private boolean isUseUncryptedMail() {
    if (hasBinding(BND_USE_UNCRYPTED_MAIL))
      return ((Boolean)valueForBinding(BND_USE_UNCRYPTED_MAIL)).booleanValue();
    return false;
  }
  
  public boolean isShowMailIcon() {
    if (hasBinding(BND_SHOW_MAIL_ICON))
      return ((Boolean)valueForBinding(BND_SHOW_MAIL_ICON)).booleanValue();
    return false;
  }
    
  public String getMailToScript() {
    if (isUseUncryptedMail()) {
      return getLinkText();
    } else {
      StringBuffer script = new StringBuffer();
      script.append("<script language=\"JavaScript\">\n");
      script.append("//<![CDATA[\n");  // compatibilite ENT
      script.append("document.write(decryptText(\"");
      script.append(cryptText(getLinkText()));
      script.append("\"))\n");
      script.append("//]]>\n");   // compatibilite ENT
      script.append("</script>");
      script.append("<noscript><font class=\"textError\"><font class=\"textNote\">");
      script.append("Votre navigateur ne supporte pas de<br>JavaScript pour ");
      script.append("afficher l'adresse mail&nbsp;!</font></font></noscript>");
      return script.toString();
    }
  }

  private String getLinkText() {
    StringBuffer text = new StringBuffer();
    String tip = getMailTip();
    text.append("<a href=\"mailto:").append(getMail()).append("\"");
    if (!isUseUncryptedMail()) {
      text.append(" onMouseover=\"window.status=''; return true;\"");
      text.append(" onMouseout=\"window.status=''; return true;\"");
    }
    if (tip.length() > 0) text.append(" title=\"").append(tip).append("\"");
    text.append(" class=\"linkPage\">");
    text.append(getMailLabel()).append("</a>");
    return text.toString();
  }
}
