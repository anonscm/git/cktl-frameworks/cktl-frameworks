package org.cocktail.fwkcktlwebapp.server.components;


/*
 * Copyright CRI - Universite de La Rochelle, 2002-2007
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

import java.util.Vector;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

/**
 * La page complete dont herite le WOComponent {@link CktlDefautPage} .
 *
 * Utiliser la classe {@link CktlWebComponent} si le composant represent un "partial document".
 *  
 * <p>Cette page contient tous les accesseurs issus des valeurs de configuration utilis&eacute;es 
 * (fichier de config ou la base de donn&eacute;es) :
 * <ul>
 * <li><code>HTML_MAIN_BG_IMAGE</code> ou "fond.jpg"</li>
 * <li><code>HTML_MENU_BG_IMAGE</code> ou "fondMenu.jpg"</li>
 * <li><code>HTML_URL_LOGOS</code></li>
 * <li><code>HTML_CSS_STYLES</code></li>
 * </ul>
 * 
 * Pour plus d'informations, voir la documentation des methodes
 * <code>mainBackgroundImage</code> et <code>menuBackgroundImage</code>.
 * </p>
 * 
 * @see CktlWebPageAutoBindings
 */
public class CktlWebPage extends CktlWebPageAutoBindings {
	
  /**
   * Cree une nouvelle instance du composant.
   */
  public CktlWebPage(WOContext context) {
    super(context);
  }
  
  /**
   * Retourne le URL de l'image de fond d'une page principale. Le URL est recherche
   * dans l'ordre suivant :
   * <ol>
   * <li>le parametre HTML_MAIN_BG_IMAGE. S'il est defini alors sa valeur est returnee;</li>
   * <li>le parametre HTML_IMAGES_ROOT. S'il est defini, alors la valeur
   *   &lt;HTML_IMAGES_ROOT&gt;+"/fond.gif" est retournee;</li>
   * <li>sinon, la valeur vide est retournee.</li>
   * </ol>
   * 
   * @return Le URL de l'image si elle peut peut etre trouvee, ou null si
   *   URL n'a pas ete trouve.
   * 
   * @see #menuBackgroundImage()
   */
  public String mainBackgroundImage() {
  	return cktlApp.getImageURL("HTML_MAIN_BG_IMAGE", "fond.jpg");
  }

  /**
   * Retourne le URL de l'image de fond d'une page de menu. Le URL est recherche
   * dans l'ordre suivant :
   * <ol>
   * <li>le parametre HTML_MENU_BG_IMAGE. S'il est defini alors sa valeur est returnee;</li>
   * <li>le parametre HTML_IMAGES_ROOT. S'il est defini, alors la valeur
   *   &lt;HTML_IMAGES_ROOT&gt;+"/bordure_frame_menu.jpg" est retournee;</li>
   * <li>sinon, la valeur vide est retournee.</li>
   * </ol>
   * 
   * @return Le URL de l'image si elle peut peut etre trouvee, ou null si
   *   URL n'a pas ete trouve.
   * 
   * @see #mainBackgroundImage()
   */
  public String menuBackgroundImage() {
    return cktlApp.getImageURL("HTML_MENU_BG_IMAGE", "fondMenu.jpg");
  }

 
  // Migration des variables et methodes de CRIHTMLHead
  
  /**
   * Retourne la definition des styles par defaut. Les balises de style CSS 
   * doivent etre donnees dans la configuration de l'application, le parametre
   * <code>HTML_CSS_STYLES</code>.
   */
  private String getDefaultStyles() {
    return valueToString(cktlApp.config().valuesForKey("HTML_CSS_STYLES"));
  }

  /**
   * Retourne la definition par defaut des images-logos a inclure dans l'entete
   * d'une page HTML. La definition des ces balises doit etre donnee dans la
   * configuration de l'application, le parametre <code>HTML_URL_LOGOS</code>.
   * @return
   */
  private String getDefaultLogos() {
    return valueToString(cktlApp.config().valuesForKey("HTML_URL_LOGOS"));
  }
    
  /**
   * Convertie la valeur en une chaine de caracteres. Si la valeur est une
   * instance de {@link Vector} ou {@link NSArray}, alors tous leurs
   * entres sont concatenes dans une chaine.
   */
  private String valueToString(Object value) {
    StringBuffer sb = new StringBuffer();
    if (value instanceof Vector) {
      Vector v = (Vector) value;
      for (int i = 0; i < v.size(); i++) {
        sb.append(v.elementAt(i)).append("\n");
      }
    } else if (value instanceof NSArray) {
      NSArray a = (NSArray) value;
      for (int i = 0; i < a.count(); i++) {
        sb.append(a.objectAtIndex(i)).append("\n");
      }
    } else {
      sb.append(StringCtrl.normalize((String) value));
    }
    return sb.toString();
  }

  // Constantes du contenu de la page

  private static final String META_CONTENT_ISO_8859_1 = 
  	"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\">\n";
  private static final String META_CONTENT_UTF_8 = 
  	"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">\n";
  private static final String META_CONTENT_FRENCH = 
  	"<meta http-equiv=\"Content-Language\" content=\"french\">\n";
  
  // Patterns du contenu de la page
  
  /**
   * La css definissant le style de la page par default
   * integrant le fond {@link #mainBackgroundImage()}
   */
  private static final String CSS_GLOBAL_BG = 
  	"body{\nbackground-image:url($1);\n}";
  
  /**
   * Balise HTML de debut de titre
   */
  private static final String INSERTED_TITLE_PREFIX = "<title>";
  /**
   * Balise HTML de fin de titre
   */
  private static final String INSERTED_TITLE_SUFFIX = "</title>\n";
  /**
   * attribut du body onLoad
   */
  private static final String INSERTED_ONLOAD_VAR_NAME = "onLoad";
  
  // Code javascript pour atteindre l'ancre via targetPosition
  private static final String JS_ANCHOR = "document.location='#$1';";
  
  /**
   * Ajout des elements communs a toutes les pages.
   */
  public void appendToResponse(WOResponse response, WOContext context) {
  	super.appendToResponse(response, context);

  	// le titre
  	String pageTitle =	getPageTitle();
  	if (!StringCtrl.isEmpty(pageTitle)) {
		addTextInHead(response, INSERTED_TITLE_PREFIX + pageTitle + INSERTED_TITLE_SUFFIX);
	}

  	// ajout des meta communs
  	addTextInHead(response, META_CONTENT_UTF_8);
  	addTextInHead(response, META_CONTENT_FRENCH);
  	
  	// icone
  	if (!isIgnoreDefaultLogos()) {
		addTextInHead(response, getDefaultLogos() + "\n");
	}

  	// styles
  	if (!isIgnoreDefaultStyles()) {
		addTextInHead(response, getDefaultStyles() + "\n");
	}
  	
  	// mettre le background global s'il existe
  	if (!StringCtrl.isEmpty(mainBackgroundImage())) {
		addTextCss(response, StringCtrl.replace(CSS_GLOBAL_BG, "$1", mainBackgroundImage()));
	}
  	
  	// la balise onload
  	String onLoadValue = onLoadValue();
  	if (!StringCtrl.isEmpty(onLoadValue)) {
		addHTMLBinding(response, INSERTED_ONLOAD_VAR_NAME, "\"" + onLoadValue + "\"", TAG_OPEN_BODY);
	}

  	// atteindre l'ancre targetPosition
  	String targetPosition = getTargetPosition();
  	if (!StringCtrl.isEmpty(targetPosition)) {
		addTextJScriptAtEnd(response, StringCtrl.replace(JS_ANCHOR, "$1", targetPosition));
	}
  }
}
