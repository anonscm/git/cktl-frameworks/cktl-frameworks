package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOAssociation;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;

import er.extensions.appserver.ERXWOContext;
import er.extensions.components._private.ERXHyperlink;

public class CktlButton extends ERXHyperlink {

	public static final String BINDING_id = "id";
	protected WOAssociation _id;

	public static final String BINDING_type = "type";
	protected WOAssociation _type;
	public static final String BINDING_title = "title";
	protected WOAssociation _title;

	/** Le texte a afficher */
	public static final String BINDING_text = "text";
	protected WOAssociation _text;

	/** Indique s'il faut afficher le text */
	public static final String BINDING_showText = "showText";
	protected WOAssociation _showText;

	/** Indique s'il l'image est situee a droite du label */
	public static final String BINDING_isImagePositionIsRight = "isImagePositionIsRight";
	protected WOAssociation _isImagePositionIsRight;

	/** Style du button */
	public static final String BINDING_style = "style";
	protected WOAssociation _style;
	
	public static final String TYPE_EDIT = "edit";
	public static final String TYPE_NEW = "new";
	public static final String TYPE_DELETE = "delete";
	public static final String TYPE_FIND = "find";

	public static final String ICON_SIZE_BIG = "big";
	public static final String ICON_SIZE_SMALL = "small";

	public static final Boolean DEFAULT_SHOW_TEXT = Boolean.FALSE;
	private static final Boolean DEFAULT_BUTTON = Boolean.FALSE;
	private static final String DEFAULT_ICON_SIZE = ICON_SIZE_SMALL;

	public CktlButton(String paramString, NSDictionary paramNSDictionary, WOElement paramWOElement) {
		super(paramString, paramNSDictionary, paramWOElement);
		this._id = ((WOAssociation) this._associations.removeObjectForKey(BINDING_id));
		this._type = ((WOAssociation) this._associations.removeObjectForKey(BINDING_type));
		this._title = ((WOAssociation) this._associations.removeObjectForKey(BINDING_title));
		this._text = ((WOAssociation) this._associations.removeObjectForKey(BINDING_text));
		this._showText = ((WOAssociation) this._associations.removeObjectForKey(BINDING_showText));
		this._isImagePositionIsRight = ((WOAssociation) this._associations.removeObjectForKey(BINDING_isImagePositionIsRight));
		this._style = ((WOAssociation) this._associations.removeObjectForKey(BINDING_style));
	}

	@Override
	public void appendAttributesToResponse(WOResponse woresponse, WOContext wocontext) {
		super.appendAttributesToResponse(woresponse, wocontext);
		woresponse.appendContentString(" class=\"cktl_button\"");
		if (this._title != null) {
			woresponse.appendContentString("title=\"" + this._title.valueInComponent(wocontext.component()) + "\"");
		}

		//		if (this._id == null) {
		//			woresponse.appendContentString(" id=\""+getId(wocontext.component())+"\"");
		//		}
	}

	public void appendToResponse(WOResponse response, WOContext context) {
		WOComponent component = context.component();

		super.appendToResponse(response, context);

		response.appendContentString("<script type=\"text/javascript\">");
		response.appendContentString(jsOnAfterDisplayed(component));
		response.appendContentString("</script>");

	}

	public void appendTagAttributeToResponse(WOResponse response, String name, Object object) {
		if (object != null) {
			response._appendTagAttributeAndValue(name, object.toString(), true);
		}
	}

	@Override
	public void appendChildrenToResponse(WOResponse paramWOResponse, WOContext paramWOContext) {
		WOComponent component = paramWOContext.component();
		if ((this._disabled != null) && (this._disabled.booleanValueInComponent(component))) {

			paramWOResponse.appendContentString("<");
			paramWOResponse.appendContentString("span");
			paramWOResponse.appendContentString(" ");

			appendTagAttributeToResponse(paramWOResponse, "id", getId(component));
			if (this._style != null) {
				appendTagAttributeToResponse(paramWOResponse, "style", "cursor:pointer;"+this._style.valueInComponent(component));
			} else {
				appendTagAttributeToResponse(paramWOResponse, "style", "cursor:pointer;");
			}
			paramWOResponse.appendContentString(">");

			appendButtonLookToResponse(paramWOResponse, component);

			paramWOResponse.appendContentString("</");
			paramWOResponse.appendContentString("span");
			paramWOResponse.appendContentString(">");
		}
		else {
			appendButtonLookToResponse(paramWOResponse, paramWOContext.component());
		}
	}

	private void appendButtonLookToResponse(WOResponse response, WOComponent component) {
		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button cktl_button_base");
		if (this._style != null) {
			appendTagAttributeToResponse(response, "style", this._style.valueInComponent(component));
		}
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_outer_box");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_inline_block cktl_button_base_inner_box");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_pos");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "class", "cktl_button_base_top_shadow");
		response.appendContentString(">");
		response.appendContentString("&nbsp;");
		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("<div");
		response.appendContentString(" ");
		appendTagAttributeToResponse(response, "id", getButtonId(component));
		appendTagAttributeToResponse(response, "class", "cktl_button_base_content");
		response.appendContentString(">");

		if (isImagePositionIsRight(component)) {
			response.appendContentString("<");
			response.appendContentString("span");
//			response.appendContentString(" ");
//			appendTagAttributeToResponse(response, "style", "vertical-align:middle;");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString(getText(component) + "&nbsp;");
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
			response.appendContentString("<span");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
			response.appendContentString(">");
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
		}
		else {
			response.appendContentString("<span");
			response.appendContentString(" ");
			appendTagAttributeToResponse(response, "class", getButtonBaseImageClass(component));
//			response.appendContentString(" ");
//			appendTagAttributeToResponse(response, "style", "vertical-align:middle;");
			response.appendContentString(">");
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
			response.appendContentString("<");
			response.appendContentString("span");
			response.appendContentString(" ");
			response.appendContentString(">");
			if (getText(component) != null) {
				response.appendContentString("&nbsp;" + getText(component));
			}
			response.appendContentString("</");
			response.appendContentString("span");
			response.appendContentString(">");
		}

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

		response.appendContentString("</");
		response.appendContentString("div");
		response.appendContentString(">");

	}

	private boolean isImagePositionIsRight(WOComponent component) {
		return ((this._isImagePositionIsRight != null) && (this._isImagePositionIsRight.booleanValueInComponent(component)));
	}

	public String getId(WOComponent component) {
	    String id = null;
	    if (this._id != null)
	        id = (String) this._id.valueInComponent(component);
	    if (id == null)
	        id = "cktl_" + ERXWOContext.safeIdentifierName(component.context(), false);
		return id;
	}

	public String getButtonId(WOComponent component) {
		return getId(component) + "_btn";
	}

	public String getButtonBaseImageClass(WOComponent component) {
		String buttonBaseImageClass = "cktl_action_button ";

		if ((this._type != null)) {
			buttonBaseImageClass += "cktl_action_" + (String) this._type.valueInComponent(component) + " ";
		}
		buttonBaseImageClass += "cktl_button_icon cktl_inline_block";

		return buttonBaseImageClass;
	}

	public String getText(WOComponent component) {
		if ((this._text != null)) {
			return (String) this._text.valueInComponent(component);
		}
		return null;
	}

	public String jsOnAfterDisplayed(WOComponent component) {
		if ((this._disabled != null) && (this._disabled.booleanValueInComponent(component))) {
			return "var btn = $('" + getId(component) + "');if (btn != null) {btn.setOpacity(0.5);btn.firstDescendant().addClassName('cktl_button_disabled');}";
		}
		else {
			return "var btn = $('" + getId(component) + "');if (btn != null) {btn.setOpacity(1);}";
		}
	}
}