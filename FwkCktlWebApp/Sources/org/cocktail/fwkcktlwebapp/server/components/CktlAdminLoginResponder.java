package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;

/**
 * Cette interface doit etre implementee par un objet recevant les notifications
 * du composant <code>CktlAdminLogin</code>. Elles permettent de parametriser le
 * processus d'authentification.
 */
public interface CktlAdminLoginResponder {
  /**
   * Renvoie la page a afficher apres que la connexion ait reussie.
   */
  public WOComponent connectAccepted();
}
