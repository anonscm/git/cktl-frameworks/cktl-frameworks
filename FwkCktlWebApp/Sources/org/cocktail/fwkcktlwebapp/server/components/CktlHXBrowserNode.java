package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
import com.webobjects.foundation.NSArray;

/**
 * Represente une entite de donnees affichees dans le composant
 * <code>CktlHXBrowser</code>. Chaque entite doit heriter de cette
 * classe, et notamment, l'objet indique par le connecteur
 * <code>zeroNode</code> de <code>CktlHXBrowser</code>.
 * 
 * <p>Chaque entite donne acces a la liste des objets <i>fils</i>
 * (la methode <code>childrenNodes</code>) et ceux de la racine
 * (<code>rootNodes</code>). Chaque fois que ces methodes sont appellees,
 * la liste retournee doit etre constitue des memes objets Java (les memes
 * references). Dans le cas contraire, la selection d'objets dans le
 * <i>browser</i> est toujours remise a "zero".
 * 
 * <p>L'implementation des methodes <code>rootNodes</code> et
 * <code>childrenNodes</code> assure ce fonctionnement et ne peut
 * pas etre change.
 * 
 * <p><code>CktlHXBrowserNode</code> est une classe abstraite. Les sous-classes
 * doivent implementer au moins les methodes <code>retrieveRootNodes</code>,
 * <code>retrieveChildrenNodes</code>, <code>isLeaf</code> et
 * <code>displayName</code>.
 * 
 * @see CktlHXBrowser
 */
public abstract class CktlHXBrowserNode {
  /**
   * La liste des noeuds des entites de premier niveau. Elles
   * sont affiche dans la premiers colonne de <i>browser</i>.
   */
  private NSArray rootNodes;
  
  /**
   * La liste des entites "fils" de l'objet en cours. 
   */
  private NSArray childrenNodes;

  /**
   * Cree une nouvelle entite. 
   */
  public CktlHXBrowserNode() {
    // nodeRecord = theRecord;
  }

  /**
   * Retourne la liste des objets representant les entites
   * du premier niveau (la premiere colonne de browser).
   * 
   * <p>Cette methode assure que la liste des memes objets
   * sera toujours accedee. Si les objets ne sont pas encore
   * charge, elle fait l'appel a la methode <code>retrieveRootNodes</code>.
   * Sinon, la liste de donnees precedemment recuperees sera retournee.
   */
  public final NSArray rootNodes() {
    if ((rootNodes == null) || (rootNodes.count() == 0))
      rootNodes = retrieveRootNodes();
    return rootNodes;
  }

  /**
   * Retourne la liste des objets representant les entites
   * des fils de l'objet courant.
   * 
   * <p>Cette methode assure que la liste des memes objets
   * sera toujours accedee. Si les objets ne sont pas encore
   * charge, elle fait l'appel a la methode <code>retrieveChildrenNodes</code>.
   * Sinon, la liste de donnees precedemment recuperees sera retournee.
   */
  public final NSArray childrenNodes() {
    if ((childrenNodes == null) || (childrenNodes.count() == 0)) {
      childrenNodes = retrieveChildrenNodes();
    }
    return childrenNodes;
  }

  /**
   * Recupere les donnees et initialise la liste des objets du
   * premier niveau. Typiquement, cette liste est constitue des
   * objets heritant de la classe <code>CktlHXBrowserNode</code>.
   * 
   * @see #rootNodes()
   */
  public abstract NSArray retrieveRootNodes();
  
  /**
   * Recupere les donnees et initialise la liste des objets fils
   * de l'objet courant. Typiquement, cette liste est constitue des
   * objets heritant de la classe <code>CktlHXBrowserNode</code>.
   * 
   * @see #childrenNodes()
   */
  public abstract NSArray retrieveChildrenNodes();

  /**
   * Test si l'objet en cours n'a plus de fils. Par exemple,
   * cette methode peut tester si la liste des fils ne contient
   * pas d'objets.
   * 
   * <p>Utilisateur peut proposer une autre implementation pour
   * cette methode.
   * 
   * @see #childrenNodes()
   */
  public abstract boolean isLeaf();
  
  /**
   * Renvoit le titre pour l'objet courant. Il est affiche dans
   * le <i>browser</i>.
   */
  public abstract String displayName();
}
