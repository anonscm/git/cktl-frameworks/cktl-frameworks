package org.cocktail.fwkcktlwebapp.server.components;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.injection.FlowManager;
import org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl.TypeSafeFlowManager;

import com.google.inject.Inject;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOMessage;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Ensemble d'outils complementaires pour l'utilisation de la classe <code>WOComponent</code><br>
 * <br>
 * <h4>Nouveaut&eacute;s CRIWebApp3</h4> Il est maintenant possible d'ajouter de dynamiquement des WebRessources au composant pendant sa construction
 * ({@link #appendToResponse(WOResponse, WOContext)}). Il est egalement possible de venir editer directement le code HTML grace a des methodes
 * accesseur simple d'utilisation.
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 *
 * <pre>
 *  <code class="comment">// surcharge de cette methode pour ajouter les ressources</code>
 *  public void appendToResponse(WOResponse response, WOContext context) {
 *   super.appendToResponse(response, context);
 *
 *   <code class="comment">// ajout de la css specifique a cette appli</code>
 *   addLocalCss(response, "css/macss.css");
 *
 *   <code class="comment">// ajout de la css partagee</code>
 *   addRemoteCss(response, "http://www.univ-lr.fr/Templates/assa.css");
 *
 *   <code class="comment">// mettre marginwidth="0" dans la balise body</code>
 *   addHTMLBinding(response, "marginwidth", "\"0\"", TAG_OPEN_BODY);
 *
 *   <code class="comment">// ajouter une css texte</code>
 *   addTextCss(response, "body{color:red;}");
 *
 *   <code class="comment">// ajouter des balises meta dans l'entete</code>
 *   addScriptResource(res, null, "&lt;meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"&gt;",
 *     null, RESOURCE_TYPE_OTHER, RESOURCE_DEST_END_HEAD, RESOURCE_NONE);
 * }
 * </pre>
 *
 * </td>
 * </tr>
 * </table>
 * </p>
 */

public class CktlWebComponent extends WOComponent {
	/**
	 * La cle de crytage de text. Elle est utilisee dans la methode {@link #cryptText(String)}.
	 *
	 * @see #cryptText
	 */
	protected static final String CRYPT_KEY = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1029384756><#].@";

	/**
	 * L'objet application. C'est un raccourci de<br>
	 * <div align="center"><code>(CktlApplication) WOApplication.application();</code></div>
	 */
	public final static CktlWebApplication cktlApp = (CktlWebApplication) WOApplication.application();

	/**
	 * Le nom du framework
	 */
	private final static String FWK_CKTL_WEB_APP_FRAMEWORK_NAME = "FwkCktlWebApp";

	@Inject
	private FlowManager flowManager;

	// pour ne pas casser les autres tests ; sinon retirer le optional.
	@Inject(optional = true)
	private TypeSafeFlowManager typesafeFlowManager;

	/**
	 * Creer un nouveau objet composant.
	 *
	 * @param context
	 */
	public CktlWebComponent(WOContext context) {
		super(context);
	}

	@Override
	public void awake() {
		if (flowManager != null) {
			flowManager.updateCurrentFlow(this.getClass());
			if (typesafeFlowManager != null) {
				typesafeFlowManager.updateCurrentFlow(this.getClass());
			}
		}
	    super.awake();
	}

	/**
	 * Le nom du framework local pour l'accès au webresources
	 */
	public String fwkCktlWebAppFrameworkName() {
		return FWK_CKTL_WEB_APP_FRAMEWORK_NAME;
	}

	public CktlWebApplication cktlApp() {
		return cktlApp;
	}

	/**
	 * Retourne l'objet session du composant. Si la session n'existe pas, elle sera creee lors de l'appel a cette methode.
	 * <p>
	 * C'est un raccourci de l'appel :<br>
	 * <div align="center"><code>(CktlWebSession) super.session();</code></div>
	 *
	 * @return L'objet {@link CktlWebSession}.
	 */
	public CktlWebSession cktlSession() {
		return (CktlWebSession) super.session();
	}

	/**
	 * Crypte le text un utilisant la cle de cryptage {@link #CRYPT_KEY}. Cette methode permet d'encoder le texte. L'algorithme de decodage est
	 * disponible sous forme d'un script java "Decrypter.js". Il doit etre disponible dans le repertoire des JavaScript's des applications. Les deux
	 * algorithmes doivent utiliser la meme cle de cryptage.
	 * <p>
	 * Le cryptage de text peut etre utilise pour la visualisation d'informations "sensibles" dans les pages HTML generees. Par exemple, le cryptage
	 * des adresses e-mail afin de les proteger contre les "spams".
	 */
	protected String cryptText(String text) {
		String xtext = "";
		int loc = -1;
		int mcH = CRYPT_KEY.length() / 2;

		for (int x = 0; x < text.length(); x++) {
			if (((int) text.charAt(x)) == 13 || ((int) text.charAt(x)) == 10) {
				xtext += " ";
			}
			else {
				loc = CRYPT_KEY.indexOf(text.charAt(x));
				if (loc < 0) {
					xtext += (text.charAt(x) == '\"' ? "\\\"" : String.valueOf(text.charAt(x)));
				}
				else if (loc > mcH) {
					xtext += CRYPT_KEY.charAt(33 - (loc - mcH));
				}
				else {
					xtext += CRYPT_KEY.charAt(33 + (mcH - loc));
				}
			}
		}
		return xtext;
	}

	//== COMPLEMENTS POUR L'AJOUT DYNAMIQUE DE RESOURCES ==//

	/**
	 * Typage des type de WebRessources a inserer
	 */
	public final static int RESOURCE_TYPE_JSCRIPT = 0;
	public final static int RESOURCE_TYPE_CSS = 1;
	public final static int RESOURCE_TYPE_OTHER = 2;

	/**
	 * Typer l'orgine des resources (interne a l'appli ou externe)
	 */
	public final static int RESOURCE_FROM_WEB_SERVER_RESOURCES = 0;
	public final static int RESOURCE_FROM_EXTERN = 1;
	public final static int RESOURCE_NONE = 2;

	/**
	 * Ou deposer le code d'inclusion de la resource (entete ou corps)
	 */
	public final static int RESOURCE_DEST_END_HEAD = 0;
	public final static int RESOURCE_DEST_END_BODY = 1;

	/**
   *
   */
	private final static String INSERTED_JSCRIPT_SRC_PREFIX = "<script type=\"text/javascript\" src=\"";
	private final static String INSERTED_JSCRIPT_SRC_SUFFIX = "\"></script>\n";
	private final static String INSERTED_JSCRIPT_TEXT_PREFIX = "<script language=\"JavaScript\" type=\"text/javascript\">\n";
	private final static String INSERTED_JSCRIPT_TEXT_SUFFIX = "\n</script>\n";
	private final static String INSERTED_CSS_SRC_PREFIX = "<link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"";
	private final static String INSERTED_CSS_SRC_SUFFIX = "\"/>\n";
	private final static String INSERTED_CSS_TEXT_PREFIX = "<style type=\"text/css\">\n";
	private final static String INSERTED_CSS_TEXT_SUFFIX = "\n</style>\n";

	public final static String TAG_CLOSE_HEAD = "</head>";
	private final static String TAG_CLOSE_BODY = "</body>";

	public final static String TAG_OPEN_BODY = "<body";

	/**
	 * Ajouter une ressource dans la page. Seul 1 des 2 parmi <code>fileName</code> et <code>text</code> ne doit etre renseigne.
	 *
	 * @param res : la WOResponse concernee
	 * @param fileName : le chemin absolu vers le fichier
	 * @param text : le texte a ajouter (ex: du code css)
	 * @param frameworkName : le nom du framework contenant le fichier (mettre null si la ressource se trouve dans l'application) - n'est pris en
	 *            compte que si <code>fileName</code> est renseigne.
	 * @param resourceType : le type de resource a inserer (JS, CSS...)
	 * @param resourceDst : l'endroit ou mettre le code dans la page (entete, corps ...)
	 * @param resourceSrc : ou prendre la resources (WebServerResources, externe ...) Si la balise de recherche n'est pas trouvee, le code n'est pas
	 *            inserer <i>Ce code est inspire de l'ancienne classe <code>AjaxComponent</code> du framework Project Wonder</i>
	 */
	protected void addScriptResource(WOResponse res, String fileName, String text,
			String frameworkName, int resourceType, int resourceDst, int resourceSrc) {

		// fileName ou text doit etre saisie
		if (StringCtrl.isEmpty(fileName) && StringCtrl.isEmpty(text)) {
			CktlLog.log("error : fileName and text are null (one and only one must be set)");
			return;
		}

		boolean addingFile = (!StringCtrl.isEmpty(fileName));
		boolean shouldInsert = true;
		NSMutableDictionary userInfo = this.mutableUserInfo(context().response());
		String urlFile = "???";
		if (addingFile) {
			String cleanFileName = StringCtrl.replace(fileName, "/", "_");
			if (userInfo.objectForKey(cleanFileName) == null) {
				// le framework n'est pas precise, mettre "app"
				if (StringCtrl.isEmpty(frameworkName))
					frameworkName = "app";
				userInfo.setObjectForKey(fileName, cleanFileName);

				// constuire l'url de la resource
				urlFile = new String();
				switch (resourceSrc) {
				case RESOURCE_FROM_WEB_SERVER_RESOURCES:
					urlFile = getLocalWebServerResourceUrl(fileName, frameworkName);
					break;
				case RESOURCE_FROM_EXTERN:
					urlFile = fileName;
					break;
				default:
					break;
				}

				//CktlLog.trace("adding resource : " + fileName + " ("+frameworkName +")" + " - url " + urlFile);
			}
			else
				shouldInsert = false;

		}
		else {

			// on constitue une cle avec le contenu du text
			StringBuffer textKey = new StringBuffer();
			for (int i = 0; i < text.length(); i++)
				textKey.append((int) text.charAt(i));

			if (userInfo.objectForKey(textKey.toString()) == null) {
				userInfo.setObjectForKey(text, textKey.toString());
				//CktlLog.trace("adding script text : " +
				//		StringCtrl.normalize(StringCtrl.cut(text, 20)) + "<...>");
			}
			else
				shouldInsert = false;
		}

		if (shouldInsert) {
			// generer le code d'inclusion
			StringBuffer resourceCode = new StringBuffer();
			switch (resourceType) {
			case RESOURCE_TYPE_JSCRIPT:
				if (addingFile)
					resourceCode.append(INSERTED_JSCRIPT_SRC_PREFIX).append(urlFile).append(INSERTED_JSCRIPT_SRC_SUFFIX);
				else
					resourceCode.append(INSERTED_JSCRIPT_TEXT_PREFIX).append(text).append(INSERTED_JSCRIPT_TEXT_SUFFIX);
				break;
			case RESOURCE_TYPE_CSS:
				if (addingFile)
					resourceCode.append(INSERTED_CSS_SRC_PREFIX).append(urlFile).append(INSERTED_CSS_SRC_SUFFIX);
				else
					resourceCode.append(INSERTED_CSS_TEXT_PREFIX).append(text).append(INSERTED_CSS_TEXT_SUFFIX);
				break;
			case RESOURCE_TYPE_OTHER:
				if (!addingFile)
					resourceCode.append(text);
				break;
			default:
				break;
			}
			String tag = "</none>";
			if (resourceDst == RESOURCE_DEST_END_HEAD)
				tag = TAG_CLOSE_HEAD;
			else if (resourceDst == RESOURCE_DEST_END_BODY)
				tag = TAG_CLOSE_BODY;
			if (!insertInResponseBeforeTag(res, resourceCode.toString(), tag))
				CktlLog.log("addScriptResource() : impossible d'inserer le code : " + resourceCode.toString() + " car " +
						"la balise HTML " + tag + " n'a pas ete trouvee!");

		}
	}

	/**
	 * Ajouter dynamiquement des bindings dans la declaration a un element de la page HTML generee par le composant. On rajoute dynamiquement des
	 * attributs a une balise.
	 *
	 * @param res : la reponse
	 * @param key : la cle du binding (ex : "onLoad" )
	 * @param value : la valeur du binding (ex : "'this.form.submit();'")
	 * @param tag : le debut de balise HTML a editer (ex : "<body")
	 */
	public void addHTMLBinding(WOResponse res, String key, String value, String tag) {
		NSMutableDictionary userInfo = this.mutableUserInfo(context().response());
		// on construit la cle pour le dictionnaire interne -  ex : "<body.onLoad"
		String binding = tag + "." + key;
		if (userInfo.objectForKey(binding) == null) {
			userInfo.setObjectForKey(value, binding);
			String bindingCode = key + "=" + value;
			CktlLog.trace("adding body binding : " + bindingCode);
			if (!insertInResponseInTag(res, bindingCode, tag))
				CktlLog.log("addBodyBinding() : impossible d'inserer le binding : " + bindingCode + " car " +
						"la balise HTML " + tag + " n'a pas ete trouvee!");
		}
	}

	/**
	 * Ajouter directement une CSS dans l'entete de la page HTML.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeResources
	 */
	public void addLocalCss(WOResponse res, String fileName) {
		addScriptResource(res, fileName, null, "app",
				RESOURCE_TYPE_CSS, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_WEB_SERVER_RESOURCES);
	}

	/**
	 * Ajouter directement une CSS dans l'entete de la page HTML. On precise le framework.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeResources
	 * @param frameworkName : le framework dans lequel se trouve le fichier css
	 */
	public void addLocalCss(WOResponse res, String fileName, String frameworkName) {
		addScriptResource(res, fileName, null, frameworkName,
				RESOURCE_TYPE_CSS, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_WEB_SERVER_RESOURCES);
	}

	/**
	 * Ajouter directement une ressource javascript situee dans un fichier dans l'entete de la page HTML.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeresources
	 */
	public void addLocalJScript(WOResponse res, String fileName) {
		addScriptResource(res, fileName, null, "app",
				RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_WEB_SERVER_RESOURCES);
	}

	/**
	 * Ajouter directement une ressource javascript situee dans un fichier dans l'entete de la page HTML.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeresources
	 * @param frameworkName : le framework dans lequel se trouve le fichier js
	 */
	public void addLocalJScript(WOResponse res, String fileName, String frameworkName) {
		addScriptResource(res, fileName, null, frameworkName,
				RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_WEB_SERVER_RESOURCES);
	}

	/**
	 * Ajouter directement une CSS dans l'entete de la page HTML.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeResources
	 */
	public void addRemoteCss(WOResponse res, String fileName) {
		addScriptResource(res, fileName, null, "app",
				RESOURCE_TYPE_CSS, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_EXTERN);
	}

	/**
	 * Ajouter directement une ressource javascript situee dans un fichier dans l'entete de la page HTML.
	 *
	 * @param res : la WOResponse
	 * @param fileName : le chemin a travers les WebServeresources
	 */
	public void addRemoteJScript(WOResponse res, String fileName) {
		addScriptResource(res, fileName, null, "app",
				RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_HEAD, RESOURCE_FROM_EXTERN);
	}

	/**
	 * Ajouter du script css sous forme de texte dans l'entete du fichier HTML. La methode se charge de rajouter les balises de declaration <style ...
	 *
	 * @param res : la WOResponse
	 * @param textCss : le texte CSS
	 */
	public void addTextCss(WOResponse res, String textCss) {
		addScriptResource(res, null, textCss, null,
				RESOURCE_TYPE_CSS, RESOURCE_DEST_END_HEAD, RESOURCE_NONE);
	}

	/**
	 * Ajouter du texte sous forme de texte dans l'entete du fichier HTML. La methode se charge de rajouter les balises de declaration <style ...
	 *
	 * @param res : la WOResponse
	 * @param text : le texte
	 */
	public void addTextInHead(WOResponse res, String text) {
		addScriptResource(res, null, text, null,
				RESOURCE_TYPE_OTHER, RESOURCE_DEST_END_HEAD, RESOURCE_NONE);
	}

	/**
	 * Ajouter du script javascript sous forme de texte a la fin du fichier HTML, juste avant la balise de fermeture du body. La methode se charge de
	 * rajouter les balises de declaration <script ...
	 *
	 * @param res : la WOResponse
	 * @param textJs : le texte JS
	 */
	public void addTextJScriptAtEnd(WOResponse res, String textJs) {
		addScriptResource(res, null, textJs, null,
				RESOURCE_TYPE_JSCRIPT, RESOURCE_DEST_END_BODY, RESOURCE_NONE);
	}

	/**
	 * Retourne le dictionnaire des donnees "custom" contenue dans un message HTTP
	 *
	 * @param message <i>Ce code est inspire de la classe <code>AjaxComponent</code> du framework ProjectWonder</i>
	 */
	private NSMutableDictionary mutableUserInfo(WOMessage message) {
		NSDictionary dict = message.userInfo();
		NSMutableDictionary result = null;
		if (dict == null) {
			result = new NSMutableDictionary();
			context().response().setUserInfo(result);
		}
		else if (dict instanceof NSMutableDictionary)
			result = (NSMutableDictionary) dict;
		else {
			result = dict.mutableClone();
			message.setUserInfo(result);
		}
		return result;
	}

	/**
	 * Placer manuellement du code dans le HTML produit par un objet WOResponse. Le code est deposer juste avant la detection d'un tag fermant (ex
	 * </body>, </head>)
	 *
	 * @param res : le WOResponse concerne
	 * @param content : la chaine a inserer
	 * @param tag : le tag HTML juste avant lequel inserer la chaine
	 * @return <code>false</code>s si l'insertion ne s'est pas faite <i>Ce code est inspire de l'ancienne classe <code>AjaxComponent</code> du
	 *         framework Project Wonder</i>
	 */
	protected boolean insertInResponseBeforeTag(WOResponse res, String content,
			String tag) {
		String stream = res.contentString();
		int idx = stream.indexOf(tag);
		if (idx < 0)
			idx = stream.toLowerCase().indexOf(tag.toLowerCase());
		if (idx >= 0) {
			String pre = stream.substring(0, idx);
			String post = stream.substring(idx, stream.length());
			res.setContent(pre + content + post);
			return true;
		}
		return false;
	}

	/**
	 * Placer manuellement du code dans le HTML produit par un objet WOResponse. Le code est depose comme attribut dans la balise specifiee.
	 *
	 * @param res : le WOResponse concerne
	 * @param content : la chaine a inserer
	 * @param tag : la balise HTML dans laquelle inserer la chaine
	 * @return <code>false</code>s si l'insertion ne s'est pas faite
	 */
	private boolean insertInResponseInTag(WOResponse res, String content, String tag) {
		String stream = res.contentString();
		int idx = stream.indexOf(tag);
		if (idx < 0)
			idx = stream.toLowerCase().indexOf(tag.toLowerCase());
		if (idx >= 0) {
			String pre = stream.substring(0, idx + tag.length());
			String post = stream.substring(idx + tag.length(), stream.length());
			res.setContent(pre + " " + content + " " + post);
			return true;
		}
		return false;
	}

	/**
	 * Donner l'url d'une resource qui se trouve dans les WebServerResources d'une application.
	 */
	public String getLocalWebServerResourceUrl(String fileName, String frameworkName) {
		WOResourceManager rm = application().resourceManager();
		return rm.urlForResourceNamed(fileName, frameworkName,
				session().languages(),
				context().request());
	}

	/**
	 * @param bindingName
	 * @param valueIfNull
	 * @return
	 */
	public Boolean booleanValueForBinding(String bindingName, Boolean valueIfNull) {
		return (valueForBinding(bindingName) != null) ? (Boolean) valueForBinding(bindingName) : valueIfNull;
	}

	/**
	 * @param bindingName
	 * @param valueIfNull
	 * @return
	 */
	public String stringValueForBinding(String bindingName, String valueIfNull) {
		if (hasBinding(bindingName)) {
			return (String) valueForBinding(bindingName);
		}
		return valueIfNull;
	}

	public void validationFailedWithException(Throwable e, Object obj, String keyPath) {
		CktlLog.rawLog("Erreur de validation [" + ((obj == null) ? "null" : obj.getClass().getName()) + "] keypath = " + keyPath + " exception: " + e.getMessage(), CktlLog.LEVEL_DEBUG);
		super.validationFailedWithException(e, obj, keyPath);
	}

	public void setFlowManager(FlowManager flowManager) {
		this.flowManager = flowManager;
	}
}
