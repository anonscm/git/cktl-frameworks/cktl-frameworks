package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;


/**
 * Ce composant permet d'affecter une couleur HTML, a une variable. 
 * La couleur est passee dans un <code>WOTextField</code> cache, puis renvoyee 
 * au serveur web en effectuant un submit sur un formulaire.<br>
 * <br>
 * <p align="center">
 * 	<table>
 * 		<tr>
 * 			<td align="center" valign="bottom"><img src="../../../../doc-files/CktlColorPicker-1.png" border="1"/></td>
 * 			<td>&nbsp;&nbsp;&nbsp;</td>
 * 			<td align="center"><img src="../../../../doc-files/CktlColorPicker-2.png" border="1"/></td>
 *		</tr>
 * 		<tr>
 * 			<td><i>"Bouton" permettant d'ouvrir la fenetre du CktlColorPicker</i></td>
 * 			<td></td>
 * 			<td><i>Fenetre du CktlColorPicker (se ferme des que l'utilisateur choisit une couleur)</i></td>
 * 		</tr>
 *	</table>
 * </p>
 * <br>
 * <h3>Bindings</h3>
 * <p>
 *  <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *	 <tr align="center" class="paramBoxHeader">
 *	  <td>Connecteur</td>
 *	  <td>Definition</td>
 *	  <td>Valeur</td>
 *	  <td>Description</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>formName</code></td>
 *	  <td>obligatoire</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le nom du formulaire HTML</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>fieldName</code></td>
 *	  <td>obligatoire</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le nom du WOTextField HTML</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>colorValue</code></td>
 *	  <td>obligatoire</td>
 *	  <td><code>String</code><br>[<i>null</i>]</td>
 *	  <td align="left">Le nom de la variable dans laquelle sera inscrite
 * 			la valeur HTML de la couleur choisie</td>
 *	 </tr>
 *	 <tr align="center" valign="top" class="paramBoxContents">
 *	  <td><code>disabled</code></td>
 *	  <td>optionnel</td>
 *	  <td><code>boolean</code><br>[<i>false</i>]</td>
 *	  <td align="left">Faut-il mettre le composant en lecture seule.</td>
 *	 </tr>
 *	</table>
 * </p>
 */

public class CktlColorPicker 
	extends CktlWebComponent {
  
  // variables entrantes
  
  /**
   * Le nom du formulaire HTML
   */
  public String formName;
  
  /**
   * Le nom du WOTextField HTML
   */
  public String fieldName;
  
  /**
   * Le nom de la variable dans laquelle sera inscrite
   * la valeur HTML de la couleur choisie.
   */
  public String colorValue;
  
  /**
   * Faut-il mettre le composant en lecture seule.
   * <i>facultatif</i>.
   */
  public boolean disabled;
  
  public CktlColorPicker(WOContext context) {
    super(context);
  }
  
  /**
   * Le code javascript attache a la zone sensible
   * pour choisir la couleur.
   */
  public String onClick() {
    return "cp.select(document." + formName + "." + fieldName + ",'pick');return false;";
  }

  /**
   * Le code javascript pour instancier l'objet
   */
  public String scriptInstance() {
    return "var cp = new ColorPicker('window', '" + formName + "');";
  }
  
  /**
   * La balise style de l'image du color picker qui 
   * permet de coloriser son fond en fonction de la selection
   */
  public String styleImg() {
  	return "border:5px solid #BBB; background-color: " + colorValue;
  }
  
  private final static String jsParentFolder = "jscript/";
  private final static String ColorPickerScriptName = "CktlColorPicker.js";
   
  /**
   * Ajouter la reference vers le script du colorPicker
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
  	addLocalJScript(arg0, jsParentFolder + ColorPickerScriptName, fwkCktlWebAppFrameworkName());
  }
 
}