package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;



/**
 * Classe regroupant l'ensemble des accesseurs manuels
 * disponibles dans la classe {@link CktlWebPage}.
 * 
 * @see CktlWebComponent
 */

public abstract class CktlWebPageAccessors extends CktlWebComponent {
 
	/** Indique si les definitions des styles par defaut ne doivent pas etre incluses */
  private boolean ignoreDefaultStyles;
  /** Indique si les definitions de logos par defaut ne doivent pas etre incluses */
  private boolean ignoreDefaultLogos;
  /** Indique si le bas de la page ne doit pas etre affiche */
  private boolean hideFooter;
  /** Le libelle a affiche dans le lien de email */
  private String mailLabel;
  /** L'adresse email a utiliser dans le lien du bas de page */
  private String contactMail;
  /** */
  private boolean useToolTip;
  /** */
  private String pageTitle;
  /** */
  private String targetPosition;

	public CktlWebPageAccessors(WOContext context) {
		super(context);
		ignoreDefaultStyles = false;
		ignoreDefaultLogos = false;
		hideFooter = true;
		mailLabel = null;
		contactMail = cktlApp.contactMail();
		useToolTip = false;
		pageTitle = null;
		targetPosition = null;
	}

  /**
   * Indique s'il faut cacher le bas de page par defaut inclus dans une page
   * genere. La valeur par defaut est <code>true</code> (afficher le bas de page).
   * 
   * @see #setHideFooter(boolean)
   */
	public boolean isHideFooter() {
		return hideFooter;
	}

  /**
   * Definit si le bas de page par defaut doit etre cache dans la page HTML
   * genere. Par defaut, il est toujours affiche.
   * 
   * @see #isHideFooter()
   */
	public void setHideFooter(boolean hideFooter) {
		this.hideFooter = hideFooter;
	}

  /**
   * Indique s'il faut ignorer les definitions par defaut des icones d'une pages
   * HTML.
   */
	public boolean isIgnoreDefaultLogos() {
		return ignoreDefaultLogos;
	}

  /**
   * Definit s'il faut ignorer les defnissions par defaut des icones d'une page
   * HTML.
   * 
   * @see #isIgnoreDefaultLogos()
   */
	public void setIgnoreDefaultLogos(boolean ignoreDefaultLogos) {
		this.ignoreDefaultLogos = ignoreDefaultLogos;
	}

  /**
   * Indique s'il faut ignorer les definitions par defaut des styles CSS.
   */
	public boolean isIgnoreDefaultStyles() {
		return ignoreDefaultStyles;
	}

  /**
   * Definit s'il faut ignorer les definitions par defaut des styles CSS dans
   * une page HTML.
   * 
   * @see #isIgnoreDefaultStyles()
   */
	public void setIgnoreDefaultStyles(boolean ignoreDefaultStyles) {
		this.ignoreDefaultStyles = ignoreDefaultStyles;
	}

  /**
   * Retourne le texte a afficher a cote de lien email dans le bas de la page
   * HTML. Si toutefois, le binding <code>mailLabel</code> est renseigne, alors
   * c'est cette valeur qui est prise en compte.
   */
	public String getMailLabel() {
		return mailLabel;
	}

	/**
	 * Definit l'adresse email du lien de "contact" dans la bas d'une page HTML.
   */
	public void setMailLabel(String mailLabel) {
		this.mailLabel = mailLabel;
	}
	
  
  /**
   * Retourne l'adresse email du lien de "contact" dans le bas d'une page HTML.
   * Si toutefois, le binding <code>contactMail</code> est renseigne, alors
   * c'est cette valeur qui est prise en compte.
   */
	public String getContactMail() {
		return contactMail;
	}

  /**
   * Definit l'adresse email du lien de "contact" dans la bas d'une page HTML.
   */
	public void setContactMail(String contactMail) {
		this.contactMail = contactMail;
	}

	/**
	 * 
	 */
	public String getPageTitle() {
		return pageTitle;
	}

	/**
	 * 
	 */
	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	/**
	 * 
	 */
	public boolean isUseToolTip() {
		return useToolTip;
	}

	/**
	 * 
	 */
	public void setUseToolTip(boolean useToolTip) {
		this.useToolTip = useToolTip;
	}
	
	/**
	 * 
	 */
	public String getTargetPosition() {
		return targetPosition;
	}

	/**
	 * 
	 */
	public void setTargetPosition(String targetPosition) {
		this.targetPosition = targetPosition;
	}
}
