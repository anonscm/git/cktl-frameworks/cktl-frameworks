package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;


/**
 * Ce composant permet de creer un menu compose de plusieurs entrees.
 * Le menu peut avoir au maximum deux niveau d'entrees. 
 * 
 * <table width="1%" border="0" cellspacing="2" cellpadding="3" align="center">
 *  <tr>
 *      <th class="paramBoxHeader" width=1% nowrap>Aperçu</td>
 *      <td class="paramBoxContents" align="center" width=1%><img src="../../../../doc-files/CktlMenu-1.png" border="1"></td>
 *      <td class="paramBoxContents" align="center" width=1%><img src="../../../../doc-files/CktlMenu-2.png" border="1"></td>
 *  </tr>
 *  <tr>
 *      <th class="paramBoxHeader" width=1% nowrap>useTopMenu</td>
 *      <td class="paramBoxContents" align="center"><code>false</code></td>
 *      <td class="paramBoxContents" align="center"><code>true</code></td>
 *  </tr>
 *  <tr>
 *      <th class="paramBoxHeader" width=1% nowrap>Remarques</td>
 *      <td class="paramBoxContents" align="center">&nbsp;</td>
 *      <td class="paramBoxContents" align="center">Il est possible d'intégrer une zone
 *          <code>WOComponentContent</code> entre le menu principal
 *          et le sous-menu</td>
 *  </tr>
 * </table>
 *   
 * <h3>Bindings</h3>
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Connecteur</td><td>Definition</td><td>Valeur</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>itemSet</code></td>
 * <td>obligatoire</td>
 * <td><code>CktlMenuItemSet</code><br>[<i>null</i>]</td>
 * <td align="left">Les elements de menu regroupes representes par un objet
 *   de la classe <code>CktlMenuItemSet</code>. Utiliser cette classe pour
 *   construire une collection des entrees du menu.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>listener</code></td>
 * <td>optionnel</td>
 * <td><code>CktlMenuListener</code><br>[<i>null</i>]</td>
 * <td align="left">L'element qui devra recevoir les evenements de menu. Il
 *   doit hériter de la classe abstraite <code>CktlMenuListener</code>.</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>useTopMenu</code></td>
 * <td>optionnel</td>
 * <td><code>boolean</code><br>[<i>false</i>]</td>
 * <td align="left">Déterminer si l'apparence du menu est en haut[true] ou a gauche[false]</code>.</td>
 * </tr>
 * </table>
 * </p>
 * 
 * 
 * <h3>Utilisation</h3>
 * 
 * <p>Un evenement est genere lorsque l'utilisateur choisit un element du
 * menu. Si le connecteur listener est definit, l'evenement est envoyer
 * a l'auditeur <code>CktlMenuListener</code>. 
 * 
 * <h4>Creation de menu</h4>
 * 
 * Tous les elements du menu sont regroupes dans un objet
 *  <code>CktlMenuItemSet</code>. Cette classe propose les methodes
 * <code>addMenuItem</code> et <code>addMenuSubItem</code> pour construire
 * un ensemble des elements. Chaque element correspond a l'objet de la
 * classe <code>CktlMenuItem</code>.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Construit et renvoie une collection des elements
 * // du menu. Pourrait etre appelee un fois.</code>
 * private CktlMenuItemSet initMenuItems() {
 *   CktlMenuItemSet menuItemSet = new CktlMenuItemSet();
 *   <code class="comment">// Les entrees du premier niveau</code> 
 *   menuItemSet.addMenuItem(1, "Item 1", "Menu Item 1", "_self");
 *   menuItemSet.addMenuItem(2, "Item 2", "Menu Item 2", "_self");
 *   menuItemSet.addMenuItem(3, "Item 3", "Menu Item 3", "_self");
 *   <code class="comment">// Les elements du deuxieme niveau.
 *   // Doivent etre attaches aux elements du premier niveau.</code>
 *   menuItemSet.addMenuSubItem(2, 11, "SubItem 1.1", "Menu 1 Sub-Item 1", "_self");
 *   menuItemSet.addMenuSubItem(2, 12, "SubItem 1.2", "Menu 1 Sub-Item 2", "_self");
 * }</pre></td></tr></table>
 * 
 * <h4>Evenements de menu</h4>
 * 
 * Un evenement est genere lorsque l'utilisateur choisit un element du
 * menu. L'evenement est envoye au receveur definit par le connecteur
 * <code>listener</code>. Il doit implementer <code>CRIWebMenuListener</code>.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 * <code class="comment">// Definit une classe de receveur d'evenements de menu.</code>
 * public class MenuListener extends CktlMenuListener {
 *   public void initMenu() {
 *     <code class="comment">// En general, on ne fait rien dans cette methode</code>
 *   }
 *
 *   <code class="comment">// La methode appelee chaque fois qu'un evenement est genere.</code>
 *   public WOComponent selectMenu(int menuId) {
 *     <code class="comment">// Le libelle de l'entree selectionnee</code>
 *     String menuTitre = menuComponent().getMenuWithId(menuId).label;
 *     <code class="comment">// La page-reponse a la selection de menu</code>
 *     WOComponent reponse;
 *     if (menuId == 1)
 *       reponse = pageReponseMenu1();
 *     else if (menuId == 2)
 *       <code class="comment">// Autres elements...</code>
 *     return reponse;
 *   }
 * }</pre></td></tr></table>
 * 
 * @see CktlMenuItemSet
 * @see CktlMenuListener
 */
public class CktlMenu extends CktlWebComponent {
  
  // Les noms des connecteurs
  public final static String BND_LISTENER = "listener";
  public final static String BND_ITEM_SET = "itemSet";
  public final static String BND_USE_TOP_MENU = "useTopMenu";
  public final static String BND_KEEP_LAST_SELECTION = "keepLastSelection";
  public final static String BND_LABEL_USER_INFO = "labelUserInfo";
  public final static String BND_SHOW_DISCONNECT_BUTTON = "showDisconnectButton";
   
  /**
   * L'objet receveur d'evenements de menu.
   */
  private CktlMenuListener menuListener;
  
  /**
   * Le style du menu
   */
  private Boolean _useTopMenu;
   
  /**
   * Le style du menu
   */
  private Boolean _keepLastSelection;
  
  /**
   * Creer un nouveau composant dans le contexte donne.
   */
  public CktlMenu(WOContext context) {
    super(context);
    menuListener = null;
 }

  /**
   * Renvoie la valeur <i>false</i>.
   * 
   * <p>Voir la definition de cette methode dans la documentation
   * de la classe <code>WOComponent</code> (WebObjects).
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }

  /**
   * Appelee lorsque le composant est (re)affiche. Si le composant est
   * affiche pour la premiere fois, initialise le menu.
   */
  public void awake() {
    menuListener().getItemSet();
  }
  
  /**
   * Renvoie le receveur des evenements de menu. Le receveur doit implementer
   * <code>CRIWebMenuListener</code>. Cette methode utilise le connecteur
   * <code>listener</code> pour recuperer cet objet.
   * 
   * <p>Si le receveur est recupere pour la premiere fois, sa methode
   * initMenu est appelee pour effectuer les initialisations supplementaire
   * de menu.
   *    
   * @return Le receveur d'evenements de menu.
   * 
   * @see CktlMenuListener
   */
  public CktlMenuListener menuListener() {
    if (menuListener == null) {
      if (hasBinding(BND_LISTENER)) {
        menuListener = (CktlMenuListener)valueForBinding(BND_LISTENER);
        if (!menuListener.isRegisteredMenu()) {
          menuListener.registerMenu(this);
          menuListener.initMenu();
        }
      }
    }
    return menuListener;
  }

  public boolean useTopMenu() {
    if (_useTopMenu == null) {
      _useTopMenu = Boolean.FALSE;
      if (hasBinding(BND_USE_TOP_MENU))
        _useTopMenu = (Boolean) valueForBinding(BND_USE_TOP_MENU);
    }
    return _useTopMenu.booleanValue();
  }
  
  public boolean keepLastSelection() {
    if (_keepLastSelection == null) {
      _keepLastSelection = Boolean.FALSE;
      if (hasBinding(BND_KEEP_LAST_SELECTION))
        _keepLastSelection = (Boolean) valueForBinding(BND_KEEP_LAST_SELECTION);
    }
    return _keepLastSelection.booleanValue();
  }
   
  /**
   * Ajouter la reference vers le script du CktlWebMenu NewLook !
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
  	addLocalCss(arg0, "css/CktlWebMenu.css", fwkCktlWebAppFrameworkName());
  }

  
}
