package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright 2003-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIWebExt framework.
 *
 * CRIWebExt is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIWebExt is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

import com.webobjects.appserver.WOComponent;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * L'interface qu'un receveur des evenement de menu <code>CktlMenu</code>
 * doit implementer. On objet implementant cette interface est generalement
 * connecte au connecteur <code>listener</code> du composant <code>CktlMenu</code>.
 * 
 * <p>Voir la desription de CktlMenu pour un exemple d'utilisation de cette classe.
 * 
 * @see CktlMenu
 */
public abstract class CktlMenuListener {
  
  private CktlMenu _criMenuComponent;
  
  /**
   * Il ne doit y avoir qu'une seule instance du composant dans
   * le listener. Le fonctionnement est le même que le modèle
   * singleton : un seule instance de l'objet. Le developpeur
   * n'a la main que sur cette classe, cela permet de piloter
   * le CktlMenu directement à partir d'ici.
   */  
  public boolean isRegisteredMenu() {
    return menuComponent() != null; 
  }
  
  /**
   * Le pointeur vers le composant CktlMenu.
   */
  public CktlMenu menuComponent() {
    return _criMenuComponent;
  }
  
  /**
   * Effectue l'initialisation du listener 
   * avec le composant menu passe en parametre.
   */
  public void registerMenu(CktlMenu value) {
    _criMenuComponent = value;
    menuItems = new NSArray();
    selectedMenuId = -1;
  }
  
  /**
   * La methode appelee lorsque le composant CktlMenu est initialise et
   * affiche pour la premiere fois. Permet d'effectuer le parametrage de
   * menu : preselection des entres, modification de ses elements, etc.
   */
  public abstract void initMenu();
  
  /**
   * La methode appele lorsqu'un element de menu est selectionne par l'utilisateur.
   * Cette methode doit renvoier le composant correspondant a l'element selectionne.
   * 
   * @param menuId L'identifiant de l'emenent selectionne.
   *
   *  @return Un composant a afficher.
   */
  public abstract WOComponent selectMenu(int menuId);
  
  /**
   * Un tableau des elements de menu a afficher, les objets
   * <code>CktlMenuItem</code>.
   */
  private NSArray menuItems;
  
  /**
   * L'element selectionne dans le menu de premier niveau.
   * C'est un objet <code>CktlMenuItem</code>.
   */
  public CktlMenuItem selectedMainItem;
  
  /**
   * L'element selectionne dans le menu de deuxieme niveau.
   * C'est un objet <code>CktlMenuItem</code>.
   */
  public CktlMenuItem selectedSubItem;
  
  /**
   * Un element reserver pour parcourir la liste des elements de menu.
   * C'est un objet <code>CktlMenuItem</code>.
   */
  public CktlMenuItem menuItem;

  /**
   * L'identifiant de l'entree de menu selectionnee suite a la derniere
   * opreation ou -1 si aucune selection. 
   */
  private int selectedMenuId;
    
  /**
   * La collection des elements de menu.
   */
  private CktlMenuItemSet itemSet;
  
  /**
   * Teste si un element de menu principal a ete selectionne.
   */
  public boolean isMainItemSelected() {
    return (menuItem == selectedMainItem);
  }

  /**
   * Teste si un element d'un sous-menu a ete selectionne.
   */
  public boolean isSubItemSelected() {
    return (menuItem == selectedSubItem);
  }

  /**
   * Teste si un element de menu principal selectionne a
   * des sous menus. 
   */
  public boolean hasSubItems() {
    if (selectedMainItem == null) return false;
    return (subMenuItems().count() > 0);
  }

  /**
   * Retourne l'identifiant de l'entree de menu selectionne lors de la
   * derniere operation ou -1 si aucune selection.
   */
  public int selectedMenuId() {
    return selectedMenuId;
  }
    
  /**
   * Returne l'objet representant la collection des elements de menu.
   * C'est un objet de la classe <code>CktlMenuItem</code>. Il est recupere
   * via le connecteur <code>itemSet</code>.
   * 
   * <p>Si la collection est recuperee pour la premiere fois, cet appel
   * initialise aussi la liste de tous les elements de menu. 
   */
  public CktlMenuItemSet getItemSet() {
    if (itemSet == null) {
      itemSet = (CktlMenuItemSet) menuComponent().valueForBinding(CktlMenu.BND_ITEM_SET);
      menuItems = itemSet.getMenuItems();
    }
    return itemSet;
  }

  /**
   * Retourne la liste des tous les elements de menu.
   * 
   * @return Un tableau des objets <code>CktlMenuItem</code>.
   */
  private NSArray allMenuItems() {
    if ((menuItems == null) || (menuItems.count() == 0))
      getItemSet();
    return menuItems;
  }
  
  /* ==== Gestion de menu ==== */
  /**
   * La methode est executee lorsque l'utilisateur selectionne
   * une entree dans le premier niveau de menu (menu principal).
   * 
   * <p>Si une entree de menu contient des sous-menus correspondant, alors
   * le premier element de sous menu est automatiquement selectionne
   * (la methode <code>selectMenu</code> est appelee pour celui-ci). Si la
   * variable 
   * 
   * @return Le composant correspondant au menu selectionne. C'est le
   *   resultat de l'execution de la methode <code>selectMenu</code>. 
   */
  public WOComponent selectMainMenuItem() {
    NSArray subMenus;   
    selectedMainItem = menuItem;
    subMenus = subMenuItems();
    selectedMenuId = selectedMainItem.id;
    if (subMenus.count() > 0) {
      if (menuComponent().keepLastSelection())
        selectedSubItem = (CktlMenuItem)subMenus.objectAtIndex(((CktlMenuItem)selectedMainItem).lastSelectionIndex);
      else
        selectedSubItem = (CktlMenuItem)subMenus.objectAtIndex(0);
      
      selectedMenuId = selectedSubItem.id;
    }
    return selectMenu(selectedMenuId);
  }

  /**
   * La methode est executee lorsque l'utilisateur selectionne
   * une entree dans le deuxieme niveau de menu (sous-menu).
   * 
   * @return Le composant correspondant au menu selectionne. C'est le
   *   resultat de l'execution de la methode <code>selectMenu</code>. 
   */
  public WOComponent selectSubMenuItem() {
    selectedSubItem = menuItem;
    selectedMenuId = menuItem.id;
    // sauvegarde de l'index 
    if (menuComponent().keepLastSelection())
      selectedMainItem.lastSelectionIndex = subMenuItems().indexOfIdenticalObject(selectedSubItem);
    return selectMenu(selectedMenuId);
  }

  /**
   * Indique que l'entree avec l'identifiant <code>id</code> doit etre
   * marquee comme selectionnee. Cet appel ne d'eclenche pas d'evenement
   * de selection de menu (methodes <code>selectMainMenuItem</code>
   * et <code>selectSubMenuItem</code>).
   */
  public void selectItemWithId(int id) {
    for(int i=0; i<allMenuItems().count(); i++) {
      if (((CktlMenuItem)allMenuItems().objectAtIndex(i)).id == id) {
        selectedMainItem = (CktlMenuItem)allMenuItems().objectAtIndex(i);
        if (selectedMainItem.parentId >= 0) {
          selectedSubItem = selectedMainItem;
          selectedMainItem = getMenuWithId(selectedSubItem.parentId);
        }
        return;
      }
    }
    selectedMainItem = null;
  }

   /**
    * Retourne le tableau des entrees d'un menu de deuxieme niveau (sous-menu).
    * La liste comporte les objets <code>CktlMenuItem</code> pour lesquels
    * le <code>id</code> de pere est le meme que celui de l'element
    * <code>selectedMainItem</code> (menu principal en cours).
    * 
    * <p>Retourne <i>null</i> si aucun menu principal n'est selectionne.
    */
   public NSArray subMenuItems() {
    if (selectedMainItem == null) return new NSArray();
    else return getMenusWithParentId(selectedMainItem.id);
  }

  /**
   * Retourne le tableau des entrees de menu ayant le pere avec
   * l'identifiant <code>parentId</code>.
   */
  private NSArray getMenusWithParentId(int parentId) {
    NSMutableArray newMenu = new NSMutableArray();
    for(int i=0; i<allMenuItems().count(); i++) {
      if (((CktlMenuItem)allMenuItems().objectAtIndex(i)).parentId == parentId)
        newMenu.addObject(allMenuItems().objectAtIndex(i));
    }
    return newMenu;
  }

  /**
   * Retourne l'entre de menu ayant l'identifiant indique.
   */
  public CktlMenuItem getMenuWithId(int id) {
    for(int i=0; i<allMenuItems().count(); i++) {
      if (((CktlMenuItem)allMenuItems().objectAtIndex(i)).id == id)
        return (CktlMenuItem)allMenuItems().objectAtIndex(i);
    }
    return null;
  }

  /**
   * Renvoie le tableau des entrees de menu de premier niveau
   * (menu principal). C'est un tableau des objets <code>CktlMenuItem</code>
   * pour lesquels l'identifiant du pere est defini a <code>-1</code>.
   */
  public NSArray mainMenuItems() {
    return getMenusWithParentId(-1);
  }
 
  /**
   * Methode appellee par le bouton de deconnexion du menu
   */
  public abstract WOComponent disconnect();

}
