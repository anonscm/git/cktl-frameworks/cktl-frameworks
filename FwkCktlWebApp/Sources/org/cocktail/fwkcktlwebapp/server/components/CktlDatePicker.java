package org.cocktail.fwkcktlwebapp.server.components;
/*
 * Copyright CRI - Universite de La Rochelle, 2002-2005 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
import org.cocktail.fwkcktlwebapp.common.util.RandomKeyGenerator;
import org.cocktail.fwkcktlwebapp.server.CktlBrowserDetector;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSTimestamp;

/**
 * Ce composant permet de selectionner une date, et de l'affecter a une variable. 
 * La valeur est passee dans un <code>WOTextField</code> cache, puis renvoyee 
 * au serveur web en effectuant un submit sur un formulaire.<br>
 * <br>
 * <p align="center">
 * 	<table>
 * 		<tr>
 * 			<td align="center" valign="bottom"><img src="../../../../doc-files/CktlDatePicker-1.png" border="1"/></td>
 * 			<td>&nbsp;&nbsp;&nbsp;</td>
 * 			<td align="center"><img src="../../../../doc-files/CktlDatePicker-2.png" border="1"/></td>
 *		</tr>
 * 		<tr>
 * 			<td><i>"Bouton" permettant d'ouvrir la fenetre du CktlDatePicker</i></td>
 * 			<td></td>
 * 			<td><i>Fenetre du CktlDatePicker (se ferme des que l'utilisateur choisit une date)</i></td>
 * 		</tr>
 *	</table>
 * </p>
 * <br>
 * <h3>Bindings</h3>
 * <p>
 * 	<table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 *		<tr align="center" class="paramBoxHeader">
 *	  	<td>Connecteur</td>
 *	  	<td>Definition</td>
 *	  	<td>Valeur</td>
 *	  	<td>Description</td>
 *	 	</tr>
 *		<tr align="center" valign="top" class="paramBoxContents">
 *	  	<td><code>dateValue</code></td>
 *	  	<td>obligatoire</td>
 *	  	<td><code>NSTimestamp</code><br>[<i>null</i>]</td>
 *	  	<td align="left">La date qui sera renseignee par la selection de l'utilisateur.</td>
 *	 	</tr>
 *		<tr align="center" valign="top" class="paramBoxContents">
 *	  	<td><code>showsTime</code></td>
 *	  	<td>optionnel</td>
 *	  	<td><code>boolean</code><br>[<i>false</i>]</td>
 *	  	<td align="left">Le composant gere les heures / minutes.</td>
 *	 	</tr>
 *		<tr align="center" valign="top" class="paramBoxContents">
 *	  	<td><code>disabled</code></td>
 *	  	<td>optionnel</td>
 *	  	<td><code>boolean</code><br>[<i>false</i>]</td>
 *	  	<td align="left">Le composant est-il accessible en modification.</td>
 *	 	</tr>
 *	</table>
 * </p>
 *
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public class CktlDatePicker 
	extends CktlWebComponent {
	
	
  // Les noms des connecteurs
  private static final String BND_DATE_VALUE 	= "dateValue";
  private static final String BND_DISABLED 		= "disabled";
  private static final String BND_SHOWS_TIME 	= "showsTime";
  // Le format de la date pour JavaScript
  private static final String DEFAULT_DATE_FORMAT_JS 			= "%d/%m/%Y";
  private static final String DEFAULT_DATETIME_FORMAT_JS 	= "%d/%m/%Y %H:%M";
  // Le format de la date pour WebObjects
  private static final String DEFAULT_DATE_FORMAT_WO 			= "%d/%m/%Y";
  private static final String DEFAULT_DATETIME_FORMAT_WO 	= "%d/%m/%Y %H:%M";
  private String fieldID;
  private String imageID;
  
  // Dans cette implementation, les sources JavaScript se trouvent dans un
  // sous-repertoire
  private final static String JS_PARENT_FOLDER = "jscript/CktlDatePicker/";
  private final static String CSS_PARENT_FOLDER = "css/CktlDatePicker/";
  // Les noms des fichiers avec les fonctions JavaScript du composant.
  private final static String DATE_PICKER_STYLE = CSS_PARENT_FOLDER+"calendar-ulr.css";
  private final static String DATE_PICKER_CODE =  JS_PARENT_FOLDER+"calendar.js";
  private final static String DATE_PICKER_LANG =  JS_PARENT_FOLDER+"calendar-fr-ulr.js";
  private final static String DATE_PICKER_SETUP = JS_PARENT_FOLDER+"calendar-setup.js";
  // detection du navigateur
  private CktlBrowserDetector clientBrowser;
  private final static int INPUT_SIZE_WITHOUT_TIME_WINDOWS = 11;
  private final static int INPUT_SIZE_WITHOUT_TIME_OTHER 	= 10;
  private final static int INPUT_SIZE_WITH_TIME_WINDOWS = 19;
  private final static int INPUT_SIZE_WITH_TIME_OTHER 	= 16;
  
  public CktlDatePicker(WOContext context) {
    super(context);
    fieldID = null;
    clientBrowser = new CktlBrowserDetector(context);
  }
  
  /*
   * @see com.webobjects.appserver.WOComponent#synchronizesVariablesWithBindings()
   */
  public boolean synchronizesVariablesWithBindings() {
    return false;
  }
  
  public NSTimestamp dateValue() {
    return (NSTimestamp)valueForBinding(BND_DATE_VALUE);
  }

  public boolean disabled() {
    if (valueForBinding(BND_DISABLED) != null)
      return ((Boolean)valueForBinding(BND_DISABLED)).booleanValue();
    return false;
  }
  
  public boolean showsTime() {
    if (valueForBinding(BND_SHOWS_TIME) != null)
      return ((Boolean)valueForBinding(BND_SHOWS_TIME)).booleanValue();
    return false;
  }
  
  public void setDateValue(NSTimestamp dateValue) {
    setValueForBinding(dateValue, BND_DATE_VALUE);
  }

  public String dateFormatJS() {
    return !showsTime() ? DEFAULT_DATE_FORMAT_JS : DEFAULT_DATETIME_FORMAT_JS;
  }

  public String dateFormatWO() {
    return !showsTime() ? DEFAULT_DATE_FORMAT_WO : DEFAULT_DATETIME_FORMAT_WO;
  }
  
  public String fieldID() {
    if (fieldID == null)
      fieldID = "f_date_"+RandomKeyGenerator.getNewKey(10);
    return fieldID;
  }
  
  public String imageID() {
    if (imageID == null)
      imageID = "f_trigger_"+RandomKeyGenerator.getNewKey(10);
    return imageID;
  }
  
  /**
   * Le nombre de caracteres visibles. Sous Windows, le nombre n'est
   * pas le bon ... il faut augmenter pour tout voir
   * @return
   */
  public int fieldSize() {
  	if (showsTime()) {
  		if (clientBrowser.getOS() == CktlBrowserDetector.OS_WINDOWS) {
  			return INPUT_SIZE_WITH_TIME_WINDOWS;
  		} else {
  			return INPUT_SIZE_WITH_TIME_OTHER;
  		}
  	} else {
  		if (clientBrowser.getOS() == CktlBrowserDetector.OS_WINDOWS) {
  			return INPUT_SIZE_WITHOUT_TIME_WINDOWS;
  		} else {
  			return INPUT_SIZE_WITHOUT_TIME_OTHER;
  		}
  	}
  }
  
  /**
   * Ajouter les CSS et js requis
   */
  public void appendToResponse(WOResponse arg0, WOContext arg1) {
  	super.appendToResponse(arg0, arg1);
    addLocalCss(arg0, DATE_PICKER_STYLE, fwkCktlWebAppFrameworkName());
    addLocalJScript(arg0, DATE_PICKER_CODE, fwkCktlWebAppFrameworkName());
    addLocalJScript(arg0, DATE_PICKER_LANG, fwkCktlWebAppFrameworkName());
    addLocalJScript(arg0, DATE_PICKER_SETUP, fwkCktlWebAppFrameworkName());
  }

}