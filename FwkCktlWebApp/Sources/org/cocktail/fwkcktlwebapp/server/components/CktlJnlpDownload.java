package org.cocktail.fwkcktlwebapp.server.components;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.util.CktlJnlpGenerator;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;

import er.extensions.components.ERXComponent;
import er.extensions.foundation.ERXStringUtilities;

public class CktlJnlpDownload extends ERXComponent {
	private static final CktlWebApplication CKTL_APP = (CktlWebApplication) WOApplication.application();
	public static final Logger log = Logger.getLogger(CktlJnlpDownload.class);

	private String lastErreur;
	private String infoJnlp;

	private CktlJnlpGenerator cktlJnlpGenerator;

	public CktlJnlpDownload(WOContext context) {
        super(context);
		cktlJnlpGenerator = new CktlJnlpGenerator(context, null, null);
    }

	public WOActionResults genererJnlp() {
		try {
			String jnlpContent = cktlJnlpGenerator.genererJnlpContent();
			return cktlJnlpGenerator.genereJnlpResponse(jnlpContent, null);
		} catch (Exception e) {
			e.printStackTrace();
			lastErreur = e.getMessage();
		}
		return null;
	}


	/**
	 * @return the cgiBase
	 */
	public String cgiBase() {
		try {
			return cktlJnlpGenerator.cgiBase();
		} catch (Exception e) {
			lastErreur = e.getMessage();
		}
		return null;

	}


	/**
	 * @param cgiBase the cgiBase to set
	 */
	public void setCgiBase(String cgiBase) {
		cktlJnlpGenerator.setCgiBase(cgiBase);
	}

	/**
	 * @return the webBase
	 */
	public String webBase() {
		try {
			return cktlJnlpGenerator.webBase();
		} catch (Exception e) {
			lastErreur = e.getMessage();
		}
		return null;
	}


	/**
	 * @param webBase the webBase to set
	 */
	public void setWebBase(String webBase) {
		cktlJnlpGenerator.setWebBase(webBase);
	}



	public String getLastErreur() {
		return lastErreur;
	}


	public String infoJnlp() {
		infoJnlp = "";
		try {
			cktlJnlpGenerator.checkJnlpTemplateFile();
			infoJnlp = "Template JNLP trouvé : " + cktlJnlpGenerator.getJnlpTemplateFile().getAbsolutePath();

		} catch (Exception e) {
			lastErreur = e.getMessage();
		}

		return infoJnlp;
	}



	public String applicationName() {
		return CKTL_APP.name();
	}

	public String version() {
		return CKTL_APP.version();
	}

	public Boolean isLastErreur() {
		return !ERXStringUtilities.stringIsNullOrEmpty(lastErreur);
	}

	public Boolean isInfoJnlp() {
		return !ERXStringUtilities.stringIsNullOrEmpty(infoJnlp());
	}


}