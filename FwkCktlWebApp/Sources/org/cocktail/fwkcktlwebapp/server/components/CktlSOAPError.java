package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;




/**
 * Represente la reponse SOAP correspondant a une erreur SOAP.
 * Il est utilise dans l'implementation du protocol SOAP.
 */
public final class CktlSOAPError extends CktlSOAPComponent {
  
  /**
   * Cree une nouvelle instance de composant.
   */
  public CktlSOAPError(WOContext context) {
    super(context);
  }

  /**
   * Initialise le composant SOAP.
   * 
   * <p>Initialisation par defaut d'un composante SOAP essaie de
   * recuperer la requete en tant que document XML et enregistrer
   * les erreurs dans le cas d'echeque. On reecrit cette methode pour
   * empecher de le faire.</p>
   */
  public void initSOAP() {
    // Il n'y a rien dans cette methode. C'est fait expres.
  }
}
