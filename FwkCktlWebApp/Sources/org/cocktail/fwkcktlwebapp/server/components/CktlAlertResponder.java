package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;

/**
 * Interface qui doit etre implementee par la classe gerant les actions/reponses
 * du composant <code>CktlAlertPage</code>.
 * 
 * @see CktlAlertPage
 */
public interface CktlAlertResponder {
  /**
   * Appelee suite a un click sur un bouton affiche dans la page d'alerte.
   * Le numero du bouton est passe en parametre. Le methode doit effectuer
   * les traitements corrspondant, generer et retourner la page de la
   * reponse. L'utilisateur est ensuite redirige vers cette page.
   * 
   * @param buttonNo Le numero de bouton sur lequel utilisateur a clique.
   *   La valeur peut entre 1 et 3.
   * 
   * @return Le composant de la page de reponse.
   */
  public WOComponent respondToButton(int buttonNo);
}
