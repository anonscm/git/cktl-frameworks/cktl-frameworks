package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

import er.extensions.appserver.ERXResponseRewriter;



public class CktlBreadcrumbElement extends CktlWebComponent {
	
	private static final String BINDING_action = "action";
	private static final String BINDING_text = "text";
	private static final String BINDING_confirmText = "confirmText";
	
    public CktlBreadcrumbElement(WOContext context) {
        super(context);
    }
    
    @Override
    public boolean synchronizesVariablesWithBindings() {
    	return false;
    }
    
    public WOActionResults action() {
    	return (WOActionResults) valueForBinding(BINDING_action);
    }
    
    public Boolean hasAction() {
    	return hasBinding(BINDING_action);
    }
    
    
    public String text() {
    	return stringValueForBinding(BINDING_text, "");
    }
    
    public Boolean shouldConfirm() {
    	return hasBinding(BINDING_confirmText);
    }
    
    public String confirmText() {
    	return stringValueForBinding(BINDING_confirmText, "");
    }
    
    public String onClickAction() {
    	return shouldConfirm() ? "return confirm(&quot;" + confirmText() + "&quot;);" : "";
    }
    
    @Override
    public void appendToResponse(WOResponse response, WOContext context) {
    	super.appendToResponse(response, context);
    	ERXResponseRewriter.addStylesheetResourceInHead(response, context, "FwkCktlWebApp.framework", "css/CktlBreadCrumb/CktlBreadCrumb.css");
    }
    
    
}