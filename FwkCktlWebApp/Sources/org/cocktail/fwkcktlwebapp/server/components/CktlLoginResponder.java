package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOComponent;

/**
 * Cette interface doit etre implementee par un objet recevant les notifications
 * du composant <code>CktlLogin</code>. Elles permettent de parametriser le
 * processus d'authentification.
 */
public interface CktlLoginResponder {
  /**
   * Renvoie la page a afficher apres que la connexion ait reussie.
   * Cette methode permet aussi de recouperer les informations sur
   * l'utilisateur authentifie via la methode <code>userInfo</code>
   * de <code>CktlLogin</code>. 
   */
  public WOComponent loginAccepted(CktlLogin loginComponent);

  /**
   * Teste si l'utilisateur donne peut se connecter a l'application.
   */
  public boolean acceptLoginName(String loginName);

  /**
   * Teste si le mot de passe vide peut etre accepte. Cette methode
   * peut etre utilisee dans les systemes l'authentification s'effectue
   * via le login uniquement.
   */
  public boolean acceptEmptyPassword();

  /**
   * Retourne le mot de passe pour l'administrateur de l'application.
   * Retourne <code>null</code> si le mot de passe d'utilisateur n'est
   * pas utilise.
   * 
   * <p>Si la methode retourne un mot de pas non-vide, l'utilisateur
   * peut s'authentifier un saisisant soit son mot de passe personnel, soit
   * le mot de passe d'administrateur.
   */
  public String getRootPassword();
}
