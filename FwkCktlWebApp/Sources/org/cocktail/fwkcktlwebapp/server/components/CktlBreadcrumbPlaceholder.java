package org.cocktail.fwkcktlwebapp.server.components;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.appserver._private.WOConstantValueAssociation;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.components.ERXWOComponentContent;

public class CktlBreadcrumbPlaceholder extends ERXWOComponentContent {
	
	public CktlBreadcrumbPlaceholder(String aName, NSDictionary associations, WOElement template) {
		super(aName, CktlBreadcrumbPlaceholder.processAssociations(associations), template);
	}
	
	protected static NSDictionary processAssociations(NSDictionary associations) {
		NSMutableDictionary mutableAssociations = (NSMutableDictionary) associations;
		mutableAssociations.setObjectForKey(new WOConstantValueAssociation("breadcrumb"), "templateName");
		return mutableAssociations;
	}
	
	@Override
	public void appendToResponse(WOResponse woresponse, WOContext wocontext) {
		woresponse.appendContentString("<ul id=\"crumbs\">");
		super.appendToResponse(woresponse, wocontext);
		woresponse.appendContentString("</ul>");
	}
}