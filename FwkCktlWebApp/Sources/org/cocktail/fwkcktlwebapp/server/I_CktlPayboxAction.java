package org.cocktail.fwkcktlwebapp.server;

import com.webobjects.appserver.WOActionResults;


/**
 * Pour les application cliente qui vont utiliser <i>Paybox</i>, il est necesaire
 * que leur classe DirectAction herite de {@link CktlWebAction}.
 * 
 * <p>
 *	Elle propose des methodes abstraites a surcharger, pour les 4 cas de figures
 *  possibles survenant lors d'une transaction : 
 * <ul>
 * <li>Un paiement est accepte</li>
 * <li>Un paiement est annule</li>
 * <li>Un paiement est refuse</li>
 * <li>Un paiement est réfuse pour la 3eme fois</li>
 * </ul>
 * Chacune des ces 4 méthodes ne doivent faire que de l'affichage, ce ne sont pas
 * des points d'entrees surs pour decrire l'etat d'un paiement.
 * </p>
 * 
 * <p>
 * La methode capitale a redefinir est payboxReponseAction, qui seule doit faire 
 * le traitement du paiement. Vous pouvez recuperer les 2 variables suivantes :
 * <ul>
 * <li>Le retour reutilise la session passee lors de l'appel (vous pouvez donc
 * recuperer vos propres variables)</li>
 * <li>context().request().stringFormValueForKey("IBS_CMD") : l'idenfiant de la commande</li>
 * <li>context().request().numericFormValueForKey("IBS_TOTAL", new NSNumberFormatter("#0")) 
 * : la somme de la transaction</li>
 * </ul>
 * </p>
 * 
 * @author Cyril TARADE <cyril.tarade at univ-lr.fr>
 */
public interface I_CktlPayboxAction {

	
	/**
	 * <b>Retour client</b><br> lorsque le paiement a ete accepte par la banque 
	 * Faire afficher une page indiquant que le paiement est reussit<br>
	 * <font color="red"><b>NE RIEN FAIRE D'AUTRE COMME TRAITEMENT (unsecure)</b></font>
	 */
	public abstract WOActionResults payboxAcceptAction();
    
	/**
	 * <b>Retour client</b><br> lorsque le paiement a ete refuse par la banque 
	 * Faire afficher une page indiquant que le paiement a ete refuse et le nombre d'essais restants (max 3)<br>
	 * <font color="red"><b>NE RIEN FAIRE D'AUTRE COMME TRAITEMENT (unsecure)</b></font>
	 */
	public abstract WOActionResults payboxRefuseAction();
    
	/**
	 * <b>Retour client</b><br> lorsque le paiement a ete annule par la client
	 * Faire afficher une page indiquant que le paiement est encore possible (annulations illimitees)<br>
	 * <font color="red"><b>NE RIEN FAIRE D'AUTRE COMME TRAITEMENT (unsecure)</b></font>
	 */
	public abstract WOActionResults payboxAnnuleAction();
    
	/**
	 * <b>Retour client</b><br> lorsque le paiement a ete refuse par la banque 3 fois de suite (sur une duree de 24h00)
	 * Faire afficher une page indiquant que le paiement est refuse et qu'il n'est plus possible pour 24h00<br>
	 * <font color="red"><b>NE RIEN FAIRE D'AUTRE COMME TRAITEMENT (unsecure)</b></font>
	 */
	public abstract WOActionResults payboxRefDepAction() ;
	
	/**
	 * <b>Retour commercant</b><br> paiement accepte -> reponse "secure"
	 * si cette methode est appelee c'est que le paiement est accepte par la banque (double requete)<br>
	 * <b>FAIRE LES TRAITEMENTS RELATIFS AU PAIEMENT ICI</b><br>
	 * <i>Attention, la page a retourner suite cette action doit etre une page completement vierge (pas de return null!!)</i>
	 */ 
	public abstract WOActionResults payboxReponseAction();

}
