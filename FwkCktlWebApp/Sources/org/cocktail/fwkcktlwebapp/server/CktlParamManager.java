package org.cocktail.fwkcktlwebapp.server;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

import er.extensions.foundation.ERXStringUtilities;

/**
 * Classe abstraite pour vérifier la présence de parametres et les créer s'ils ne sont pas trouvés. Voir les sous-classes pour des exemples.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class CktlParamManager {
	public static final Logger log = Logger.getLogger(CktlParamManager.class);
	public static final String O = "O";
	public static final String N = "O";

	private HashMap<String, String> paramComments = new HashMap<String, String>();
	private HashMap<String, String> paramDefault = new HashMap<String, String>();
	private HashMap<String, String> paramTypes = new HashMap<String, String>();
	private ArrayList<String> paramList = new ArrayList<String>();
	private ArrayList<String> requiredParamList = new ArrayList<String>();
	private static CktlWebApplication application;

	public CktlParamManager() {
	}

	public HashMap<String, String> getParamComments() {
		return paramComments;
	}

	public HashMap<String, String> getParamDefault() {
		return paramDefault;
	}

	public HashMap<String, String> getParamTypes() {
		return paramTypes;
	}

	public ArrayList<String> getParamList() {
		return paramList;
	}

	/**
	 * Vérifie l'existence des parametres et initialisation dans la base avec une valeur par défaut si ces valeurs par défaut sont définies.
	 */
	public void checkAndInitParamsWithDefault() {
		Iterator<String> keys = getParamList().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			String res = getParam(key);
			if (res == null) {
				if (getParamDefault().get(key) != null) {
					log.debug("Parametre " + key + " non trouvé, enregistrement avec la valeur par defaut " + getParamDefault().get(key) + ".");
					createNewParam(key, getParamDefault().get(key), getParamComments().get(key), getParamTypes().get(key));
				}
			}
			else {
				log.debug("Parametre " + key + " trouvé = " + res);
			}
		}
		checkRequiredParams();
	}

	public void checkRequiredParams() {
		log.debug("Verification des parametres obligatoires");
		boolean error = false;
		Iterator<String> keys = getRequiredParamList().iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			String res = getParam(key);
			if (res == null) {
				log.error("Parametre " + key + " non trouvé.");
				error = true;
			}
			else {
				log.info(key + " = " + res);
			}
		}
		if (error) {
			throw new RuntimeException("Certains parametres obligatoires n'ont pas été trouvés. Consultez le log.");
		}

	}

	/**
	 * Crée un nouveau parametre et l'enregistre dans la base.
	 * 
	 * @param key
	 * @param value
	 * @param comment
	 * @deprecated
	 */
	public abstract void createNewParam(String key, String value, String comment);

	/**
	 * Crée un nouveau parametre et l'enregistre dans la base.
	 * 
	 * @param key
	 * @param value
	 * @param comment
	 * @param type
	 */
	public abstract void createNewParam(String key, String value, String comment, String type);

	/**
	 * Renvoie la valeur du parametre.
	 * 
	 * @param key
	 * @return le paramètre correspondant à la clé key.
	 */
	public abstract String getParam(String key);

	public static CktlWebApplication getApplication() {
		return (CktlWebApplication) CktlWebApplication.application();
	}

	/**
	 * @param keyFrom
	 * @param keyTo
	 */
	public static void copyParamValue(String keyFrom, String keyTo) {
		Object res = getApplication().config().valueForKey(keyFrom);
		if (res == null) {
			if (log.isDebugEnabled()) {
				log.debug("Le paramètre de configuration " + keyFrom + " n'est pas défini, la copie vers " + keyTo + " n'est donc pas réalisée.");
			}
		}
		else {
			if (log.isDebugEnabled()) {
				log.debug("Le paramètre de configuration " + keyFrom + " n'est pas défini, la copie vers " + keyTo + " n'est donc pas réalisée.");
			}
			getApplication().config().takeValueForKey(res, keyTo);
		}
	}

	/**
	 * @param key
	 * @return true si le parametre est un code activation et si sa valeur est true (OUI, YES, O). Cf {@link CodeActivationValidateur}
	 */
	public boolean isCodeActivationActif(String key) {
		String res = getParam(key);
		return booleanForValue(res);
	}

	public static boolean booleanForValue(String value) {
		if (!ERXStringUtilities.stringIsNullOrEmpty(value)) {
			Pattern p = Pattern.compile("[OY]{1}|OUI|YES");
			Matcher m = p.matcher(value);
			return (m.matches());
		}
		return false;
	}

	/**
	 * initialise une valeur pour la clé. Cette méthode ne crée pas d'enregistrement dans la base.
	 * 
	 * @param key
	 * @param value
	 */
	public static void setParamValue(String key, String value) {
		getApplication().config().takeValueForKey(value, key);
	}

	@SuppressWarnings("unchecked")
	public Map<String, Object> getParams() {
		return getApplication().config();
	}

	/**
	 * @return La liste des parametres requis.
	 */
	public ArrayList<String> getRequiredParamList() {
		return requiredParamList;
	}

}
