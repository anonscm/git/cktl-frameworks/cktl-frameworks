package org.cocktail.fwkcktlwebapp.server;

import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.UserInfo;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppComposite;
import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;
import org.cocktail.fwkcktlwebapp.server.injection.CktlCurrentFlowProvider;
import org.cocktail.fwkcktlwebapp.server.injection.CktlFlow;
import org.cocktail.fwkcktlwebapp.server.injection.CktlFlowNameComponentRegistry;
import org.cocktail.fwkcktlwebapp.server.injection.CktlFlowProvider;
import org.cocktail.fwkcktlwebapp.server.injection.CurrentFlow;
import org.cocktail.fwkcktlwebapp.server.injection.FlowProvider;
import org.cocktail.fwkcktlwebapp.server.injection.FlowTypeListener;
import org.cocktail.fwkcktlwebapp.server.injection.StartFlow;
import org.cocktail.fwkcktlwebapp.server.injection.StartFlowInterceptor;
import org.cocktail.fwkcktlwebapp.server.version.app.VersionAppFromGradleProperties;
import org.cocktail.fwkcktlwebapp.server.version.app.VersionAppFromInfoPlist;
import org.cocktail.fwkcktlwebapp.server.version.app.VersionAppFromVersionProperties;

import com.google.inject.AbstractModule;
import com.google.inject.Provider;
import com.google.inject.Provides;
import com.google.inject.matcher.Matchers;
import com.google.inject.name.Named;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.woinject.WORequestScoped;
import com.woinject.WOScopes;

import er.extensions.ERXFrameworkPrincipal;
import er.extensions.appserver.ERXWOContext;
import er.extensions.eof.ERXEC;

/**
 * 
 * Framework principal de FwkCktlWebApp.
 * Contient les modules Guice contribués par ce framework.
 * 
 * @author Alexis Tual
 *
 */
public class FwkCktlWebApp extends ERXFrameworkPrincipal {

    static {
        setUpFrameworkPrincipalClass(FwkCktlWebApp.class);
    }
    
    @Override
    public void finishInitialization() {
        ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
        if (CktlWebApplication.application().isDevelopmentMode()) {
            moduleRegister.addModule(new FwkCktlWebAppModuleDev());
        } else {
            moduleRegister.addModule(new FwkCktlWebAppModule());
        }
    }
    
    
    
    /**
     * Module Guice de prod
     *
     */
    public static class FwkCktlWebAppModule extends AbstractModule {
    	
    	public static final String REQUEST_EDITING_CONTEXT = "RequestEditingContext";
        
        @Override
        protected void configure() {
            configureFlowInjection();
            bind(UserInfo.class).toProvider(FwkCktlUserInfoProvider.class);
 
            VersionApp versionApp = new VersionAppComposite(
                    new VersionAppFromVersionProperties(CktlWebApplication.application()),
                    new VersionAppFromInfoPlist()
                    );
            bind(VersionApp.class).toInstance(versionApp);
        }
        
        private void configureFlowInjection() {
            StartFlowInterceptor interceptor = new StartFlowInterceptor();
            FlowProvider flowProvider = new CktlFlowProvider();
            interceptor.setFlowProvider(flowProvider);
            FlowTypeListener flowTypeListener = new FlowTypeListener();
            flowTypeListener.setFlowProvider(flowProvider);
            CktlFlowNameComponentRegistry flowNameClassNameRegistry = new CktlFlowNameComponentRegistry();
            flowTypeListener.setFlowNameClassNameRegistry(flowNameClassNameRegistry);
            
            bindInterceptor(
                    Matchers.subclassesOf(CktlWebComponent.class),
                    Matchers.annotatedWith(StartFlow.class),
                    interceptor);
            bindListener(Matchers.any(), flowTypeListener);
            bind(CktlFlowNameComponentRegistry.class).toInstance(flowNameClassNameRegistry);
            bind(CktlFlow.class).annotatedWith(CurrentFlow.class).toProvider(CktlCurrentFlowProvider.class).in(WOScopes.REQUEST);
        }
        
    	@Provides @Named(REQUEST_EDITING_CONTEXT) @WORequestScoped
    	public EOEditingContext restRequestEditingContext() {
    		return ERXEC.newEditingContext();
    	}
        
    }
    

  
    /**
     * Module Guice de dev
     *
     */
    public static class FwkCktlWebAppModuleDev extends FwkCktlWebAppModule {
        
        @Override
        protected void configure() {
        	super.configureFlowInjection();
            bind(UserInfo.class).toProvider(FwkCktlUserInfoProvider.class);
            VersionApp versionApp = new VersionAppComposite(
                    new VersionAppFromGradleProperties(),
                    new VersionAppFromVersionProperties(CktlWebApplication.application())
                    );
            bind(VersionApp.class).toInstance(versionApp);
        }
        
    }
    
    /**
     * Provider du CktlUserInfo via la session.
     * Permet notamment d'injecter l'utilisateur courant dans les composants,
     * mais surtout la couche métier :
     * 
     * <pre>
     *    @Inject UserInfo userInfo;
     * </pre>
     */
    public static class FwkCktlUserInfoProvider implements Provider<CktlUserInfo> {
        /**
         * {@inheritDoc} 
         */
        public CktlUserInfo get() {
            CktlUserInfo userInfo = null;
            
            WOContext context = ERXWOContext.currentContext();
            if (context != null && context.hasSession()) {
            	 CktlWebSession session = (CktlWebSession) context.session();
            	
            	if (session != null) {
            		userInfo = session.connectedUserInfo();
            	}
            }
            
            return userInfo;
        }
    }
}
