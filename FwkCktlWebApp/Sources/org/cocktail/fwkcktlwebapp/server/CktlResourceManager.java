package org.cocktail.fwkcktlwebapp.server;

import java.lang.reflect.Method;
import java.util.Vector;

import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;

/**
 * Permet d'avoir acces aux ressources (fichiers) disponibles dans l'application ou dans les frameworks utilises par l'application.
 * <p>
 * Cette classe est propose une solution compatible entre differentes versions de WebObjects et notamment entre les versions 5.2.1 et 5.2.2.
 * </p>
 */
public class CktlResourceManager {

	/**
	 * Retourne la reference vers l'application en cours d'execution. On suppose que ceci est une instance de <code>CktlApplication</code>.
	 */
	public CktlWebApplication app() {
		return (CktlWebApplication) WOApplication.application();
	}

	/**
	 * Renvoie le chemin absolu d'acces au fichier <code>fileName</code>. Le fichier doit faire part des ressources de l'application ou un des
	 * frameworks qui y sont utilises.
	 */
	public String pathForResource(String fileName) {
		String aPath;
		NSArray files;
		// D'abord les ressources de l'application
		aPath = pathForFile(fileName, bundlePath(NSBundle.mainBundle()));
		if (aPath == null) {
			// Si ca n'a pas marche, on essaie autrement.
			files = NSBundle.mainBundle().resourcePathsForResources(fileName, null);
			if (files.count() > 0)
				aPath = (String) files.objectAtIndex(0);
		}
		if (aPath == null) {
			// Sinon, les ressources de tous les frameworks
			NSArray frameworks = NSBundle.frameworkBundles();
			NSBundle fBundle;
			for (int i = 0; (i < frameworks.count()) && (aPath == null); i++) {
				fBundle = (NSBundle) frameworks.objectAtIndex(i);
				aPath = pathForFile(fileName, bundlePath(fBundle));
				if (aPath == null) {
					// Pareil, si ceci n'a pas marche avant...
					files = NSBundle.mainBundle().resourcePathsForResources(fileName, fBundle.name());
					if (files.count() > 0)
						aPath = (String) files.objectAtIndex(0);
				}
			}
		}
		return aPath;
	}

	/**
	 * Retourne le chemin absolu de repertoire de <code>bundle</code>.
	 */
	private String bundlePath(NSBundle bundle) {
		// On utilise la reflection de Java
		// pour ne pas avoir le message "deprecated" a la compilation
		try {
			Method method = NSBundle.class.getMethod("bundlePath", (Class<?>[]) null);
			return (String) method.invoke(bundle, (Object[]) null);
		} catch (Throwable ex) {
		}
		return null;
	}

	/**
	 * Retourne le chemin absolu de fichier <code>fileName</code> qui se trouve dans le repertoire <code>dirName</code> ou dans un sous-repertoire.
	 * Retourne <i>null</i> si le fichier n'est pas trouve.
	 */
	private String pathForFile(String fileName, String dirName) {
		Vector v = FileCtrl.findFiles(dirName, fileName, true);
		if (v.size() > 0)
			return (String) v.elementAt(0);
		else
			return null;
	}

	//  /**
	//   * Renvoie le chemin absolu d'acces au fichier <code>fileName</code>.
	//   * Le fichier doit faire part des ressources de l'application ou un des
	//   * frameworks qui y sont utilises.
	//   */
	//  public String pathForResource(String fileName) {
	//    String aPath;
	//    NSArray frameworks;
	//    // D'abord les ressources de l'application
	//    aPath = pathForResource(fileName, null);
	//    if (aPath == null) {
	//      // Sur certaines plates-formes le resultat de cet appel n'est pas
	//      // correct. Alors on essaie autrement.
	//      aPath = NSBundle.mainBundle().resourcePathForLocalizedResourceNamed(fileName, null);
	//    }
	//    if (aPath == null) {
	//      // Sinon, les ressources de tous les frameworks
	//      frameworks = NSBundle.frameworkBundles();
	//      for(int i=0; (i<frameworks.count()) && (aPath == null); i++) {
	//        // aPath = ((NSBundle)fBundles.objectAtIndex(i)).pathForResource(fileName, null);
	//        // aPath = pathForFileInBundle(fileName, (NSBundle)fBundles.objectAtIndex(i));
	//        aPath = pathForResource(fileName, ((NSBundle)frameworks.objectAtIndex(i)).name());
	//        if (aPath == null) {
	//          // Pareil, si ceci n'a pas marche avant...
	//          aPath = ((NSBundle)frameworks.objectAtIndex(i)).resourcePathForLocalizedResourceNamed(fileName, null);
	//        }
	//      }
	//    }
	//    // NSLog.out.appendln("PathForFile > File : "+fileName+"; path : "+aPath);
	//    return aPath;
	//  }

	//  /**
	//   * Essaie de trouver le chemin absolu de fichier <code>fileName</code>.
	//   * Le parametre <code>frmName</code> indique le framework dans les ressource
	//   * de lequel le fichier est recherche. Si ce parametre est <i>null</i>,
	//   * alors le fichier est recherche dans les ressources de l'application.
	//   */
	//  public String pathForResource(String fileName, String frmName) {
	//    try {
	//      return pathForResource521(fileName, frmName);
	//    } catch(Throwable ex521) {
	//      System.out.println("-- pathForResource521 exception --");
	//      System.out.println(ex521.getClass().getName()+" : "+ex521.getMessage());
	//      try {
	//        return pathForResource522(fileName, frmName);
	//      } catch(Throwable ex522) {
	//        System.out.println("-- pathForResource522 exception --");
	//        ex522.printStackTrace();
	//        // System.out.println(ex522.getClass().getName()+" : "+ex522.getMessage());
	//      }
	//    }
	//    return null;
	//  }
	//  
	//  /**
	//   * Essaie de trouver le chemin absolu de fichier <code>fileName</code> en
	//   * utilisant les methodes de WebObjects 5.2.1. Le parametre <code>frmName</code>
	//   * indique le framework dans les ressource de lequel le fichier est recherche.
	//   * Si ce parametre est <i>null</i>, alors le fichier est recherche dans
	//   * les ressources de l'application en cours.
	//   * 
	//   * <p>La methode retourne <i>null</i> si le fichier ne peut pas etre
	//   * trouve.</p>
	//   * 
	//   * <p>Les appels aux methodes de WebObjects 5.2.1 utilisent le mecanisme
	//   * de reflection de Java, afin d'avoir le code independant de la version
	//   * concrete de WebObjects. Une exception est levee dans le cas ou cette
	//   * methode est appellee avec la version de WebObjects plus recente que
	//   * 5.2.1.</p>
	//   */
	//  private String pathForResource521(String fileName, String frmName)
	//    throws ClassNotFoundException, SecurityException, NoSuchMethodException,
	//           IllegalArgumentException, IllegalAccessException,
	//           InvocationTargetException
	//  {
	//    Method method = app().resourceManager().getClass().getMethod("_appProjectBundle", new Class[]{});
	//    Object bundleObject = method.invoke(app().resourceManager(), new Object[]{});
	//    Class bundleClass = Class.forName("com.webobjects.appserver._private.WODeployedBundle");
	//    method = bundleClass.getMethod("absolutePathForResource", 
	//                          new Class[]{String.class, String.class});
	//    return (String)method.invoke(bundleObject, new String[]{fileName, frmName});
	//  }
	//  
	//  /**
	//   * Essaie de trouver le chemin absolu de fichier <code>fileName</code> en
	//   * utilisant les methodes de WebObjects 5.2.2. Le parametre <code>frmName</code>
	//   * indique le framework dans les ressource de lequel le fichier est recherche.
	//   * Si ce parametre est <i>null</i>, alors le fichier est recherche dans
	//   * les ressources de l'application en cours.
	//   * 
	//   * <p>La methode retourne <i>null</i> si le fichier ne peut pas etre
	//   * trouve.</p>
	//   * 
	//   * <p>Les appels aux methodes de WebObjects 5.2.2 utilisent le mecanisme
	//   * de reflection de Java, afin d'avoir le code independant de la version
	//   * concrete de WebObjects. Une exception est levee dans le cas ou cette
	//   * methode est appellee avec la version de WebObjects plus ancienne que
	//   * 5.2.2.</p>
	//   */
	//  private String pathForResource522(String fileName, String frmName)
	//    throws SecurityException, NoSuchMethodException, ClassNotFoundException,
	//           IllegalArgumentException, IllegalAccessException,
	//           InvocationTargetException
	//  {
	//
	//    System.out.println("pathForResource522() FileName : "+fileName+", framework : "+frmName);
	//    Method method = app().resourceManager().getClass().getMethod("pathForResourceNamed", new Class[]{String.class, String.class, NSArray.class});
	//    System.out.println("pathForResource522() 2 : method "+method);
	//    String path = (String)method.invoke(app().resourceManager(),
	//                      new Object[]{fileName, frmName, null});
	//    System.out.println("pathForResource522() path : "+path);
	//    return path;
	///*    if (path != null) {
	//      String path = pathURL.toString();
	//      // Cette methode retourne le URL de type "file:/".
	//      // On le supprime et on laisse uniquement le chemin de fichier.
	//      if (path.startsWith("file:")) {
	//        path = path.substring(5);
	//        for(;path.startsWith("/"); path = path.substring(1));
	//        path = "/"+path;
	//      }
	//      return path;
	//    }
	//    return null;
	//*/  }

	/*
	 * private String pathForResource522(String fileName, String frmName) throws SecurityException, NoSuchMethodException, ClassNotFoundException,
	 * IllegalArgumentException, IllegalAccessException, InvocationTargetException {
	 * System.out.println("pathForResource522() FileName : "+fileName+", framework : "+frmName); Method method =
	 * app().resourceManager().getClass().getMethod("pathURLForResourceNamed", new Class[]{String.class, String.class, NSArray.class});
	 * System.out.println("pathForResource522() 2 : method "+method); URL pathURL = (URL)method.invoke(app().resourceManager(), new Object[]{fileName,
	 * frmName, null}); System.out.println("pathForResource522() pathURL : "+pathURL); if (pathURL != null) { String path = pathURL.toString(); //
	 * Cette methode retourne le URL de type "file:/". // On le supprime et on laisse uniquement le chemin de fichier. if (path.startsWith("file:")) {
	 * path = path.substring(5); for(;path.startsWith("/"); path = path.substring(1)); path = "/"+path; } return path; } return null; }
	 */

	//  /**
	//   * Renvoie le chemin complet jusqu'au fichier donne.
	//   * Le fichier doit faire part des ressources de l'application.
	//   */
	//  public String pathForFile(String fileName) {
	//    String aPath;
	//    NSArray frameworks;
	//    // D'abord les ressources de l'application
	//    WODeployedBundle appBundle = resourceManager()._appProjectBundle();
	//    aPath = appBundle.absolutePathForResource(fileName, (String)null);
	//    if (aPath == null) {
	//      // Sur certaines plates-formes le resultat de cet appel n'est pas
	//      // correct. Alors on essaie autrement.
	//      aPath = NSBundle.mainBundle().resourcePathForLocalizedResourceNamed(fileName, null);
	//    }
	//    if (aPath == null) {
	//      // Sinon, les ressources de tous les frameworks
	//      frameworks = NSBundle.frameworkBundles();
	//      for(int i=0; (i<frameworks.count()) && (aPath == null); i++) {
	//        // aPath = ((NSBundle)fBundles.objectAtIndex(i)).pathForResource(fileName, null);
	//        // aPath = pathForFileInBundle(fileName, (NSBundle)fBundles.objectAtIndex(i));
	//        aPath = appBundle.absolutePathForResource(fileName, ((NSBundle)frameworks.objectAtIndex(i)).name());
	//        if (aPath == null) {
	//          // Pareil, si ceci n'a pas marche avant...
	//          aPath = ((NSBundle)frameworks.objectAtIndex(i)).resourcePathForLocalizedResourceNamed(fileName, null);
	//        }
	//      }
	//    }
	//    // NSLog.out.appendln("PathForFile > File : "+fileName+"; path : "+aPath);
	//    return aPath;
	//  }
}
