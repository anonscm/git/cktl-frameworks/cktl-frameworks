package org.cocktail.fwkcktlwebapp.server;

import java.util.List;

import com.google.inject.Module;

/**
 * Représente un registre de modules Guice.
 * 
 * @author Alexis Tual
 *
 */
public interface ModuleRegister {

    /**
     * @return la liste des modules de ce registre.
     */
    List<? extends Module> modules();
    
    /**
     * @param module un module à enregistrer.
     */
    void addModule(Module module);
}
