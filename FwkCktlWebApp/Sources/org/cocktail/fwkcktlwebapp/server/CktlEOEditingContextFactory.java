package org.cocktail.fwkcktlwebapp.server;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOObjectStore;

import er.extensions.eof.ERXEC;
import er.extensions.foundation.ERXProperties;

/**
 * <p>
 *   Factory personnalisée pour la création des {@link EOEditingContext}.
 *   Cette factory est uniquement utilisée pour l'instant pour la 
 *   correction du bug sur les nested editingcontext... (cf DT 5744).
 *   <br/>
 *   Cette factory doit être utilisée uniquement dans les applications ayant
 *   les pbs de nested, car cela peut faire augmenter la mémoire de façon significative.
 *   
 *   
 * </p>
 * Pour que ce soit utile il faut bien sûr instancier les ECs avec
 * {@link ERXEC#newEditingContext(EOObjectStore)}
 * 
 * @author Alexis Tual
 *
 */
public class CktlEOEditingContextFactory extends ERXEC.DefaultFactory {

	public static final String DISABLE_UNDO_REGISTRATION = "org.cocktail.fwkcktlwebapp.ec.disableUndoRegistration";
	
    /**
     * Après la création du nested, on applique setRetainsRegisteredObjects(true)
     * pour corriger le bug de garbage collection d'eos nested.
     * @param parent l'objectstore parent
     * @return le nouvel ec
     */
    @Override
    public EOEditingContext _createEditingContext(EOObjectStore parent) {
        EOEditingContext ec = super._createEditingContext(parent);
        ec.setRetainsRegisteredObjects(true);
        if (ERXProperties.booleanForKey(DISABLE_UNDO_REGISTRATION)) {
            ec.undoManager().disableUndoRegistration();
        }
        return ec;
    }
    
}
