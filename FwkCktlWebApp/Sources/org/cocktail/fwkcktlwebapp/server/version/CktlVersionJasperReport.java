package org.cocktail.fwkcktlwebapp.server.version;



/**
 * Classe de lecture de la version de Jasper Report
 */
public class CktlVersionJasperReport extends CktlVersionJar {
	
	private static final String JASPER_REPORT_CLASS_NAME = "net.sf.jasperreports.engine.JasperReport";

	public CktlVersionJasperReport() {
		super(JASPER_REPORT_CLASS_NAME);
	}

}
