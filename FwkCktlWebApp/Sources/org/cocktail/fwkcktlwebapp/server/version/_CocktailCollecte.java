package org.cocktail.fwkcktlwebapp.server.version;


import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.server.CktlEOUtilities;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSTimeZone;


/**
 * Utiliser plut&ocirc;t la collecte automatique : voir {@link A_CktlVersion}<br/>
 * Attention, vous devez avoir surcharg&eacute; la m&eacute;thode {@link CktlWebApplication#mainModelName()} pour que la classe puisse interroger la
 * base de donn&eacute;es ! <br/>
 * <hr/>
 * Permet de Collecter diff&eacute;rentes informations sur le serveur ou est lanc&eacute;e l'application. Ces informations sont transmises a un
 * serveur de collecte. Appelez cette fonction a la fin de l'initialisation de l'application (la connexion a la base de donnees doit etre active, le
 * timezone initialise, etc.). Exemple de mise en oeuvre
 * 
 * <pre>
 * <code>
 *         try {
 *           _CocktailCollecte.envoyerCollecte(true, ENCODING_COLLECTE, this, Version.APPLICATIONINTERNALNAME, 
 *                   Version.VERSIONNUM, new Integer(Version.VERSIONNUMMAJ), new Integer(Version.VERSIONNUMMIN), Version.VERSIONNUMPATCH, 
 *                   Version.VERSIONNUM, VERSIONDATE, null);
 *       } catch (Exception e) {
 *           e.printStackTrace();
 *       }
 * </code>
 * </pre>
 * 
 * @author rodolphe.prin@univ-lr.fr
 * @deprecated
 */
@Deprecated
public class _CocktailCollecte {

    /** URL qui doit renvoyer les informations de connexion pour envoyer les informations collectees. Cette Url doit 
     * renvoyer une chaine de caractere  du type post#http://lesitecollecte/wa/push*/
	private static final String COCKTAIL_COLLECTE_REQUEST_URL = "";
   
    private static final String APP_SERVER_NOM = "Webobjects";
    
    private static final String WO_CFBUNDLESHORTVERSIONSTRING = "CFBundleShortVersionString";
    private static final String WO_BUILDVERSION = "BuildVersion";
    private static final String WO_FILE = "version.plist";
    private static final String WO_FRMK = "JavaWebObjects";
    
    private static final Exception EXCEPTION_APPLIID_VIDE = new Exception("appliId est non defini");
    private static final Exception EXCEPTION_APPLIVERSION_VIDE = new Exception("appliVersion est non defini");
    private static final Exception EXCEPTION_APPLIVERSIONMAJEURE_VIDE = new Exception("appliVersionMajeure est non defini");
   	private static final Exception EXCEPTION_APPLIVERSIONMINEURE_VIDE = new Exception("appliVersionMineure est non defini");
    private static final Exception EXCEPTION_APPLIVERSIONPATCH_VIDE = new Exception("appliVersionPatch est non defini");
    private static final Exception EXCEPTION_APPLIBDVERSION_VIDE = new Exception("appliBdVersion est non defini");
    private static final Exception EXCEPTION_APPLIVERSIONDATE_VIDE = new Exception("appliVersionDate est non defini");
		
   
    public static final Exception EXCEPTION_CONNEXION_COKTAIL_COLLECTE_URL = new Exception("Erreur lors de la connexion a " + COCKTAIL_COLLECTE_REQUEST_URL);
    public static final String STR_ERREUR_CONNEXION_COKTAIL_COLLECTE_URL = "Erreur lors de la recuperation de l'URL de push";
    public static final String STR_ERREUR_CONNEXION_COKTAIL_COLLECTE_REPONSE = "Erreur lors de la transmission des infos de collecte.";
    
    
    public static final String TYPE_COLLECTE_POST = "post";

    private static final String STR_NA = "n/a";
   
    
    private final CktlWebApplication _application;
    
    
    private Map infosCollectees = new HashMap();
    
    private static final String IP_INTERNE_KEY = "ipInterne";
    private static final String MACHINE_NOM_KEY = "machineNom"; 
    private static final String DOMAINE_KEY = "domaine";
    private static final String JRE_VERSION_KEY = "jreVersion";
    private static final String JDBC_URLS_KEY = "jdbcUrls";
    private static final String JDBC_BD_VERSION_KEY = "jdbcBdVersion";
    private static final String OS_NOM_KEY = "osNom";
    private static final String OS_VERSION_KEY = "osVersion";
    private static final String APP_SERVER_NOM_KEY = "appServerNom";
    private static final String APP_SERVER_VERSION_KEY = "appServerVersion";
    private static final String APPLI_NOM_KEY = "appliNom";
    private static final String APPLI_URL_KEY = "appliUrl";
    private static final String APPLI_ARGUMENTS_KEY = "appliArguments";
    private static final String APPLI_ID_KEY = "appliId";
    private static final String APPLI_VERSION_KEY = "appliVersion";
    private static final String APPLI_VERSION_MAJEURE_KEY = "appliVersionMajeure";
    private static final String APPLI_VERSION_MINEURE_KEY = "appliVersionMineure";
    private static final String APPLI_VERSION_PATCH_KEY = "appliVersionPatch";
    private static final String APPLI_BD_VERSION_KEY = "appliBdVersion";
    private static final String APPLI_VERSION_DATE_KEY = "appliVersionDate";
    private static final String APPLI_TIMEZONE_KEY = "appliTimezone";
    private static final String APPLI_LANCEMENT_DATE_KEY = "appliLancementDate";
    private static final String STRUCTURE_RACINE_KEY = "structureRacine";
    private static final String COMMENTAIRES_KEY = "commentaires";
    private static final Object RETOURPUSH_OK = "OK";
   
    
    
    
    private URL coktailCollectePushUrl;
    private String typeCollecte;
    private boolean showTrace = false;
    private String encoding;
    //private static Exception lastException;
    
    protected _CocktailCollecte(CktlWebApplication app) {
        _application = app;
    }
    
    
    /**
     * Récupère les informations sur le serveur d'application.
     * @throws Exception
     */
    protected void initAppServerInfos() throws Exception {
        final InputStream is = _application.resourceManager().inputStreamForResourceNamed(WO_FILE, WO_FRMK, null);
        final NSData data = new NSData(is, 1024);
        final Object plist = NSPropertyListSerialization.propertyListFromData(data, null);
        final String list = NSPropertyListSerialization.stringFromPropertyList(plist);
        final NSDictionary dico = NSPropertyListSerialization.dictionaryForString(list);
        infosCollectees.put(APP_SERVER_NOM_KEY, APP_SERVER_NOM);
        infosCollectees.put(APP_SERVER_VERSION_KEY, (String) dico.valueForKey(WO_CFBUNDLESHORTVERSIONSTRING) + "." + (String) dico.valueForKey(WO_BUILDVERSION));
    }    
    
    
    /**
     * Initialise les données concernant la machine, le serveur, le domaine, etc.
     * @throws Exception
     */
    protected void initNetInfos() throws Exception {
        try {
            InetAddress addr = InetAddress.getLocalHost();
            infosCollectees.put(IP_INTERNE_KEY, addr.getHostAddress());
            infosCollectees.put(MACHINE_NOM_KEY, addr.getHostName());
            infosCollectees.put(DOMAINE_KEY, addr.getCanonicalHostName());
            
        } catch (UnknownHostException e) {
            throw new Exception("Erreur lors de la recuperation des informations concernant la machine : " + e.getMessage());
        }
    }
    
    /**
     * Initialise les informations liees a la plateforme (OS, java, etc.)
     * @throws Exception
     */
    protected void initPlateformInfos() throws Exception {
        infosCollectees.put(OS_NOM_KEY, System.getProperties().getProperty("os.name"));
        infosCollectees.put(OS_VERSION_KEY, System.getProperties().getProperty("os.version"));
        infosCollectees.put(JRE_VERSION_KEY, System.getProperty("java.version"));
    }
    
    /**
     * Initialise les infos concernant l'application (url, instance, arguments, etc.)
     * @throws Exception
     */
    protected void initAppliInfos() throws Exception {
        File f = new File(_application.path());
        infosCollectees.put(APPLI_NOM_KEY, f.getName());
        infosCollectees.put(APPLI_URL_KEY, _application.webserverConnectURL());
        infosCollectees.put(APPLI_TIMEZONE_KEY, NSTimeZone.defaultTimeZone().toString());
        infosCollectees.put(APPLI_LANCEMENT_DATE_KEY, (new Date()).toString());
        infosCollectees.put(APPLI_ARGUMENTS_KEY, STR_NA);
        
    }
    
    /**
     * Initialise les infos de connexion a la base de donnees.
     * @throws Exception
     */
    protected void initJdbcInfos() throws Exception {
        String connexionUrls = null;
        final EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
        for (int i = 0; i < vModelGroup.models().count(); i++) {
            final EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
            final String url = bdConnexionUrl(tmpEOModel);
            connexionUrls = (connexionUrls == null ? url : connexionUrls + ", " + url); 
        }
        infosCollectees.put(JDBC_URLS_KEY, connexionUrls==null ? STR_NA : connexionUrls);
        infosCollectees.put(JDBC_BD_VERSION_KEY, STR_NA);
        
        if (_CktlBasicDataBus.isDatabaseConnected()) {
            final String sql = "select BANNER from v$version where banner like '%Oracle%'";
            final NSDictionary res = _executeSQLQuery(sql, new NSArray("BANNER"));
            if (res.valueForKey("BANNER") != null) {
                infosCollectees.put(JDBC_BD_VERSION_KEY, res.valueForKey("BANNER").toString());
            }            
        }        
        
    }
    
    protected void initEtabInfos() throws Exception {
        infosCollectees.put(STRUCTURE_RACINE_KEY, STR_NA);
        if (_CktlBasicDataBus.isDatabaseConnected()) {
            final String sql = "select LL_STRUCTURE from grhum.structure_ulr where c_type_structure='E' and tem_valide='O'";
            final NSDictionary res = _executeSQLQuery(sql, new NSArray("LL_STRUCTURE"));
            if (res.valueForKey("LL_STRUCTURE") != null) {
                infosCollectees.put(STRUCTURE_RACINE_KEY, res.valueForKey("LL_STRUCTURE").toString());
            }            
        }
    }
    
    
    /**
     * Collecte les informations et les transmet au site de collecte.
     * 
     * @param showTrace true si vous souhaitez afficher les logs
     * @param encoding Encodage a utiliser (par exemple "UTF-8")
     * @param app R&eacute;f&eacute;rence &agrave; l'instance de la classe Application
     * @param appliId Identifiant de l'application (par exemple JEFYADMIN)
     * @param appliVersion Version de l'application normalis&eacute; x.y.z
     * @param appliVersionMajeure Partie Majeure de la version de l'application normalis&eacute;
     * @param appliVersionMineure Partie Mineure de la version de l'application normalis&eacute;
     * @param appliVersionPatch Partie Patch de la version de l'application normalis&eacute;
     * @param appliBdVersion Version de la base de donn&eacute;es associ&eacute;e &agrave; l'application normalis&eacute; Majeure.Mineure.patch
     * @param appliVersionDate Date de release de la version format dd/mm/yyyy 
     * @param commentaires Champ libre, facultatif
     * @throws Exception
     */
    public static void envoyerCollecte(final boolean showTrace, final String encoding, final CktlWebApplication app, final String appliId, final String appliVersion, 
            final Integer appliVersionMajeure, final Integer appliVersionMineure, final String appliVersionPatch, 
            final String appliBdVersion, final String appliVersionDate, final String commentaires) throws Exception {
        final _CocktailCollecte cocktailCollecte = new _CocktailCollecte(app);
        
        cocktailCollecte.showTrace = showTrace;
        cocktailCollecte.setEncoding(encoding);
        
        //verifier les parametres
        if (estVide(appliId)) {
            throw EXCEPTION_APPLIID_VIDE;
        }
        if (estVide(appliVersion)) {
            throw EXCEPTION_APPLIVERSION_VIDE;
        }
        if (appliVersionMajeure == null) {
            throw EXCEPTION_APPLIVERSIONMAJEURE_VIDE;
        }
        if (appliVersionMineure == null) {
            throw EXCEPTION_APPLIVERSIONMINEURE_VIDE;
        }
        if (estVide(appliVersionPatch)) {
            throw EXCEPTION_APPLIVERSIONPATCH_VIDE;
        }
        if (estVide(appliBdVersion)) {
            throw EXCEPTION_APPLIBDVERSION_VIDE;
        }
        if (estVide(appliVersionDate)) {
            throw EXCEPTION_APPLIVERSIONDATE_VIDE;
        }
        
        cocktailCollecte.getInfosCollectees().put(APPLI_ID_KEY, appliId);
        cocktailCollecte.getInfosCollectees().put(APPLI_VERSION_KEY, appliVersion);
        cocktailCollecte.getInfosCollectees().put(APPLI_VERSION_MAJEURE_KEY, appliVersionMajeure.toString());
        cocktailCollecte.getInfosCollectees().put(APPLI_VERSION_MINEURE_KEY, appliVersionMineure.toString());
        cocktailCollecte.getInfosCollectees().put(APPLI_VERSION_PATCH_KEY, appliVersionPatch);
        cocktailCollecte.getInfosCollectees().put(APPLI_BD_VERSION_KEY, appliBdVersion);
        cocktailCollecte.getInfosCollectees().put(APPLI_VERSION_DATE_KEY, appliVersionDate);
        cocktailCollecte.getInfosCollectees().put(COMMENTAIRES_KEY, commentaires==null ? STR_NA : commentaires);
        
        
        
        //on met tout ca dans un trhead pour eviter les blocages
        Thread thread = new Thread() {
            public void run() {
                super.run();
                try {
                    cocktailCollecte.doTheJob();
                } catch (Exception e) {
                    e.printStackTrace();
                    //lastException = e;
                }
            }
        };
        
        thread.start();
    }
    
    
    private static boolean estVide(final String s) {
        return (s == null || s.length() == 0);
    }
    

    
    
    /**
     * Affiche toutes les valeurs collectees
     *
     */
    public void traceValeurs() {
        trace("");
        trace("Informations collectees");
        trace("-----------------------");
        final Iterator it = infosCollectees.keySet().iterator();
        while (it.hasNext()) {
            final String element = (String) it.next();
            trace(element + "=" + (String) infosCollectees.get(element));
        }
        trace("");
    }
    
    public void trace(final String s) {
        if (showTrace) {
            System.out.println(s);
        }
    }
    
    
    
    
    /**
     * Effectue les traitements (initialisation, envoi, etc.)
     * @throws Exception
     */
    protected void doTheJob() throws Exception {
        initCollectePushUrl();
        initInfos();
        traceValeurs();
        envoyerCollecte();
    }    
    



    /**
     * Initialise la propri&eacute;t&eacute; coktailCollectePushUrl en interrogeant l'url d&eacute;finie dans COCKTAIL_COLLECTE_REQUEST_URL
     * @throws Exception 
     *
     */
    private void initCollectePushUrl() throws Exception {
//        HttpURLConnection.setFollowRedirects(false);
        String res = null;
        try {
            final URL url = new URL(COCKTAIL_COLLECTE_REQUEST_URL);
            final URLConnection conn = url.openConnection();
            conn.connect();
            
            final BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            String str;
            while ((str = in.readLine()) != null) {
                res = (res != null ? res + str : str);
            }
            in.close();            
            if (res == null) {
                throw new Exception("Aucune donnee lue depuis l'url");
            }
            final String[] sp = res.split("#");
            if (sp.length != 2) {
                throw new Exception("Les donnees recuperees sont au mauvais format (type_collecte#url) (" + res + ")");
            }
            if (!TYPE_COLLECTE_POST.equals(sp[0])) {
                throw new Exception("Le type de collecte defini est errone (" + sp[0] + " au lieu de " + TYPE_COLLECTE_POST + ")");
            }
            
            setTypeCollecte(sp[0]);
            
            //Verifier si le deuxieme parametre est bien une url valide
            try {
                setCoktailCollectePushUrl(new URL(sp[1]));
            } catch (MalformedURLException e) {
                throw new Exception("l'URL recuperee n'est pas valide (" + sp[1]  + ")");
            }
            
            
        } catch (Exception e) {
            throw new Exception(STR_ERREUR_CONNEXION_COKTAIL_COLLECTE_URL + " : " + e.getMessage());
        }        
        
    }


    /**
     * Initialise toutes les informations collectees.
     * @throws Exception
     */
    protected void initInfos() throws Exception {
        initAppServerInfos();
        initNetInfos();
        initPlateformInfos();
        initAppliInfos();
        initJdbcInfos();
        initEtabInfos();
    }
    
    
    
    private void envoyerCollecte() throws Exception {
        if (TYPE_COLLECTE_POST.equals(getTypeCollecte())) {
            doPost();
        }
    }
    

    


    public final String getTypeCollecte() {
        return typeCollecte;
    }


    public final void setTypeCollecte(String typeCollecte) {
        this.typeCollecte = typeCollecte;
    }


    public final URL getCoktailCollectePushUrl() {
        return coktailCollectePushUrl;
    }


    public final void setCoktailCollectePushUrl(URL coktailCollectePushUrl) {
        this.coktailCollectePushUrl = coktailCollectePushUrl;
    }



    
    public String bdConnexionUrl(EOModel model) {
        NSDictionary vDico = model.connectionDictionary();
        String url = (String) vDico.valueForKey("URL");
        if (url == null) {
            url = STR_NA;
        }
        return url;
    }    
    
    public boolean checkBdConnection() {
        return CktlDataBus.isDatabaseConnected();
    }


    public final Map getInfosCollectees() {
        return infosCollectees;
    }

    
    
    protected void doPost() throws Exception{
        OutputStreamWriter writer = null;
        BufferedReader reader = null;
        try {
           String donnees = urlEncode();
           //trace("Donnees encodees = " + donnees);
           //creation de la connection
           final URLConnection conn = coktailCollectePushUrl.openConnection();
           conn.setDoOutput(true);
           //envoi de la requete
           writer = new OutputStreamWriter(conn.getOutputStream());
           writer.write(donnees);
           writer.flush();
           
           //lecture de la reponse
           reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
           String ligne;
           String res = null;
           while ((ligne = reader.readLine()) != null) {
               res = ligne;
               trace("Reponse Collecte = " + ligne);
              if (!RETOURPUSH_OK.equals(ligne)) {
                  throw new Exception (" Reponse=" + ligne);
              }
           }
           if (res == null) {
               throw new Exception (" Aucune reponse");
           }
           
           
        } catch (Exception e) {
           throw new Exception(STR_ERREUR_CONNEXION_COKTAIL_COLLECTE_REPONSE + e.getMessage());
        } finally {
           try { writer.close(); } catch (Exception e) { }
           try { reader.close(); } catch (Exception e) { }
        }
     }    
    
    
    private final String urlEncode() throws Exception {
        String res = "";
        final String egal = "=";
        final Iterator it = infosCollectees.keySet().iterator();
        while (it.hasNext()) {
            final String element = (String) it.next();
            try {
                res += URLEncoder.encode(element, getEncoding()) + egal + URLEncoder.encode((String) infosCollectees.get(element), getEncoding()) + "&";
            } catch (UnsupportedEncodingException e) {
                throw e;
            } 
        }
        return res;
    }


    public final void setEncoding(String encoding) {
        this.encoding = encoding;
    }


    public final String getEncoding() {
        return encoding;
    }

    /**
     * Execution des requetes pour eviter les erreurs.
     * La m&eacute;thode {@link CktlWebApplication#mainModelName()} de la classe <code>Application</code> doit etre surchargee.
     * 
     * <pre>
     * com.webobjects.jdbcadaptor.JDBCAdaptorException: Unable to determine the number of column returned:
     * Next exception:SQL State:null -- error code: 17144 -- msg: descripteur d'instruction non ex&eacute;cut&eacute;
     * </pre>
     * 
     * @param sqlQuery
     * @param arrayColumnName
     * @return le resultat de la requete
     */
    private NSDictionary _executeSQLQuery(String sqlQuery, NSArray arrayColumnName) {
    	NSArray resultDicos = CktlEOUtilities.rawRowsForSQL(_application.dataBus().editingContext(), _application.mainModelName(),
    			sqlQuery, arrayColumnName);
    	return (NSDictionary) resultDicos.objectAtIndex(0);
    }
    
}
