package org.cocktail.fwkcktlwebapp.server.version;


import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.StringTokenizer;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;

/**
 * <h1>Contr&ocirc;le de version des applications Cocktail</h1> <b>14/11/2013 : Les applications sont désormais construites avec Gradle et l'ensemble
 * des bibliothèques sont embarquées avec l'application. Le control de version est donc géré en amont de l'exécution. Les n° de version sont gérés par
 * le fichier gradle.properties, et un fichier version.properties est automatiquement intégré à l'archive lors du build (dans /Resources).
 * L'utilisation de cette classe devrait progressivement disparaire, elle reste néanmoins utile pour l'instant pour controler le n° de version de la
 * base.</b> <h3>Pr&eacute;ambule :</h3> <br/>
 * L'outil propos&eacute; va permettre aux d&eacute;veloppeurs d'applications :
 * <ul>
 * <li><a href="#ver">de d&eacute;crire une application ou un framework (nom, date, identifiant)</a></li>
 * <li><a href="#dep">de d&eacute;finir une liste de d&eacute;pendances (applications, frameworks, autres) dans des num&eacute;ros versions
 * pr&eacute;cis</a></li>
 * <li><a href="#col">d'envoyer ces informations automatiquement vers le serveur de collecte</a></li>
 * <li><a href="#aut">de cr&eacute;er des "versionneurs" pour d'autres types d'objets (jars, systemes, fichiers ...)</a></li>
 * </ul>
 * <br/>
 * <br/>
 * <h2><a name="ver"></a>Description de la version courante de l'application ou du framework</h2> Elle est g&eacute;n&eacute;ralement compos&eacute;e
 * des &eacute;l&eacute;ments suivants :
 * <ul>
 * <li>Un nom (d&eacute;fini par la m&eacute;thode {@link #name()}</code>)</li>
 * <li>Un identifiant de version : liste de num&eacute;ros s&eacute;par&eacute;s par des points (d&eacute;fini par les m&eacute;thodes
 * {@link #versionNumMaj()}, {@link #versionNumMin()}, {@link #versionNumPatch()} et {@link #versionNumBuild()})</li>
 * <li>Un date de publication (d&eacute;fini par la m&eacute;thode {@link #date()})</li>
 * <li>Eventuellement un commentaire (d&eacute;fini par la m&eacute;thode {@link #comment()})</li>
 * </ul>
 * Pour la mise en place, il suffit de cr&eacute;er une nouvelle classe dans le projet, qui doit h&eacute;riter de la classe abstraite
 * {@link A_CktlVersion}, et dans laquelle il faut surcharger les m&eacute;thodes indiqu&eacute;es.<br/>
 * <br/>
 * Un exemple de classe de description :
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="javaKeyword">package</code> org.cocktail.monappli.version;
 * <code class="javaKeyword">import</code> org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
 * 
 * <code class="commentJavaDoc">&#47;**
 *  * Classe descriptive de la version et dependances de l'application Mon Application
 *  *
 *  * @author Nom Prenom &lt;nom.prenom at cocktail.org&gt;
 *  *&#47;</code>
 * <code class="javaKeyword">public class</code> Version <code class="javaKeyword">extends</code> A_CktlVersion {
 *   
 *   <code class="comment">// nom de l'application</code>
 *   <code class="javaKeyword">public</code> String name() {
 *     <code class="javaKeyword">return</code> <code class="javaString">"Mon Application"</code>;
 *   }
 * 
 *   <code class="comment">// date de publication</code>
 *   <code class="javaKeyword">public</code> String date() {
 *     <code class="javaKeyword">return</code> <code class="javaString">"21/11/2007"</code>;
 *   }
 * 
 *   <code class="comment">// num&eacute;ro majeur</code>
 *   <code class="javaKeyword">public int</code> versionNumMaj() {
 *     <code class="javaKeyword">return</code> 2;
 *   }
 * 
 *   <code class="comment">// num&eacute;ro mineur</code>
 *   <code class="javaKeyword">public int</code> versionNumMin() {
 *     <code class="javaKeyword">return</code> 15;
 *   }
 * 
 *   <code class="comment">// num&eacute;ro de patch</code>
 *   <code class="javaKeyword">public int</code> versionNumPatch() {
 *     <code class="javaKeyword">return</code> 3;
 *   }
 * 
 *   <code class="comment">// num&eacute;ro de build</code>
 *   <code class="javaKeyword">public int</code> versionNumBuild() {
 *     <code class="javaKeyword">return</code> 6;
 *   }
 * 
 *   <code class="comment">// commentaire</code>
 *   <code class="javaKeyword">public</code> String comment() {
 *     <code class="javaKeyword">return</code> <code class="javaString">""</code>;
 *   }
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * <br/>
 * <h2><a name="dep"></a>D&eacute;finition des d&eacute;pendances</h2> Les applications reposent syst&eacute;matiquement sur d'autre briques
 * applicatives :
 * <ul>
 * <li>Frameworks g&eacute;n&eacute;raux et frameworks m&eacute;tiers</li>
 * <li>Biblioth&egrave;ques sp&eacute;cifiques (archives jars)</li>
 * <li>Serveur de base de donn&eacute;es</li>
 * <li>User sp&eacute;cifique de la base de donn&eacute;es</li>
 * <li>Environnement d'&eacute;x&eacute;cution Java (JRE)</li>
 * <li>Environnement d'&eacute;x&eacute;cution WebObjects</li>
 * <li>...</li>
 * </ul>
 * Afin de garantir le bon fonctionnement d'une application, le d&eacute;veloppeur peut indiquer la liste des versions compatibles de ces briques.<br/>
 * La d&eacute;finition de cette liste se fait assez simplement, en pr&eacute;cisant les bornes min et max de fonctionnement. <br/>
 * <br/>
 * La classe {@link CktlVersionRequirements} est d&eacute;di&eacute;e pour cet usage.<br/>
 * <br/>
 * Pr&eacute;alablement &agrave; son utilisation, il faut savoir CRIWebApp fournit des classes java repr&eacute;sentatives des briques applicatives
 * potentiellement controlables.
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Brique</td>
 * <td>Description</td>
 * <td>Classe associ&eacute;e</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>JRE</td>
 * <td align="left">version de la JRE install&eacute;e sur le poste / le serveur d'application</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava</code></td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>WebObjects</td>
 * <td align="left">version de WebObjects install&eacute;e sur le poste / le serveur d'application</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects</code></td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>Serveur Oracle</td>
 * <td align="left">version de ORACLE install&eacute;e sur le serveur de base de donn&eacute;es</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleServer</code></td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>Framework FwkCktlWebApp</td>
 * <td align="left">version du framework CktlApp utilis&eacute; par l'application</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.Version</code></td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>Jasper Report</td>
 * <td align="left">version de n'importe quel jar (voir {@link CktlVersionJar})</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.CktlVersionJar</code></td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td>Jasper Report</td>
 * <td align="left">version de Jasper Report</td>
 * <td align="left"><code>org.cocktail.fwkcktlwebapp.server.version.CktlVersionJasperReport</code></td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * L'expression d'une demande de controle se fait alors en 1 seule ligne de code :
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="comment">// environnement webobjects 5.3 uniquement, arret de l'appli si ce n'est pas le cas</code>
 * <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects(), <code class="javaString">"5.3"</code>, <code class="javaString">"5.3"</code>, <code class="javaKeyword">true</code>);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="comment">// JRE a partir de 1.4.2 jusqu'a 1.4.x.y (x>=2), pas d'arret de l'appli si ce n'est pas le cas</code>
 * <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava(), <code class="javaString">"1.4.2"</code>, <code class="javaString">"1.4"</code>, <code class="javaKeyword">false</code>);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * L'utilisation n'est pas limit&eacute;e aux classes pr&eacute;d&eacute;finies dans CRIWebApp, l'outil permet aussi de controler les versions de tous
 * les frameworks utilis&eacute;s par l'application (sous r&eacute;serve que ces derniers impl&eacute;mentent une classe h&eacute;ritant de
 * <code>CktlVersion</code>), il faut juste pr&eacute;ciser la classe (la compl&eacute;tion automatique d'Eclipse pourra vous y aider).
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="comment">// a partir de CRIWebExt 3, pas d'arret de l'appli si ce n'est pas le cas</code>
 * <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> fr.univlr.cri.webext.Version(), <code class="javaString">"3"</code>, <code class="javaKeyword">null</code>, <code class="javaKeyword">false</code>);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * Le d&eacute;veloppeur peut &eacute;galement cr&eacute;er un controleur de version du ou des users Oracle utilis&eacute;s par l'application.<br/>
 * Plus de d&eacute;tails sont disponibles sur la page de documentation de {@link CktlVersionOracleUser}.<br/>
 * <br/>
 * Puis, pour rassembler l'ensemble des contr&ocirc;les &agrave; effectuer, il suffit de surcharger la m&eacute;thode {@link #dependencies()}</code>,
 * qui devra retourner la liste de tous les {@link CktlVersionRequirements} n&eacute;c&eacute;ssaires.
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="javaKeyword">public class</code> Version <code class="javaKeyword">extends</code> A_CktlVersion {
 * 
 *   [...]
 * 
 *   <code class="comment">// liste des dependances</code>
 *   <code class="javaKeyword">public</code> CktlVersionRequirements[] dependencies() {
 *     <code class="javaKeyword">return new</code> CktlVersionRequirements[]{
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.fwkcktlwebapp.server.version.Version(), <code class="javaString">"3"</code>, <code class="javaString">"3"</code>, <code class="javaKeyword">true</code>),
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> fr.univlr.cri.webext.Version(), <code class="javaString">"3"</code>, <code class="javaString">"3"</code>, <code class="javaKeyword">true</code>),
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.fwkcktlwebapp.server.version.CktlVersionWebObjects(), <code class="javaString">"5.3"</code>, <code class="javaKeyword">null</code>, <code class="javaKeyword">false</code>),
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.monappli.VersionOracleMonUser(), <code class="javaKeyword">null</code>, <code class="javaKeyword">null</code>, <code class="javaKeyword">false</code>),
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.frameworkmetier.Version(), <code class="javaKeyword">null</code>, <code class="javaString">"1.9"</code>, <code class="javaKeyword">true</code>),
 *       <code class="javaKeyword">new</code> CktlVersionRequirements(<code class="javaKeyword">new</code> org.cocktail.fwkcktlwebapp.server.version.CktlVersionJava(), <code class="javaString">"1.4.2"</code>, <code class="javaString">"1.4"</code>, <code class="javaKeyword">false</code>),
 *   }
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * <i><u>Remarque</u> : les frameworks m&eacute;tiers control&eacute;s peuvent aussi surcharger cette m&eacute;thode, la v&eacute;rification se fera
 * alors de fa&cedil;on r&eacute;cursive</i><br/>
 * <br/>
 * Derni&egrave;re &eacute;tape, pour faire la liaison avec l'application, il faut modifier la classe <code>Application</code> (qui doit
 * h&eacute;riter directement ou indirectement de {@link CktlWebApplication}), en surchargeant la m&eacute;thode
 * {@link CktlWebApplication#appCktlVersion()}, qui devra fournir une instance de la classe de version repr&eacute;sentative de l'application
 * d&eacute;velopp&eacute;e.
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * [...]
 *   
 * <code class="javaKeyword">public class</code> Application <code class="javaKeyword">extends</code> CktlApplication {
 *   
 *   [...]
 *   
 *   <code class="comment">// 1 seule instance</code>@
 *   <code class="javaKeyword">private</code> A_CktlVersion <code class="javaVariable">_appCktlVersion</code>;
 *   
 *   <code class="comment">// controle de versions</code>
 *   <code class="javaKeyword">public</code> A_CktlVersion appCktlVersion() {
 *     if (<code class="javaVariable">_appCktlVersion</code> == <code class="javaKeyword">null</code>) {
 *       <code class="javaVariable">_appCktlVersion</code> = <code class="javaKeyword">new</code> org.cocktail.monappli.version.Version();
 *     }
 *     <code class="javaKeyword">return</code> <code class="javaVariable">_appCktlVersion</code>;
 *   }
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * Lors du lancement de l'application, la proc&eacute;dure de contr&ocirc;le des version sera lanc&eacute;e (d&eacute;tails visibles dans les logs),
 * et si une des conditions n'est pas satisfaite (version incorrecte), alors l'application sera arret&eacute;e (appel &agrave; la m&eacute;thode
 * {@link WOApplication#terminate()}) et un message d'erreur sera affich&eacute;.<br/>
 * <br/>
 * <br/>
 * <h2><a name="col"></a>Collecte automatique</h2> Pour activer le service de collecte automatique, il faut surcharger la m&eacute;thode
 * {@link CktlWebApplication#appShouldSendCollecte()} de <code>Application</code>.<br/>
 * Si la version de la base de donn&eacute;es doit aussi &ecirc;tre envoy&eacute;e, alors il faudra aussi surcharger la m&eacute;thode
 * {@link CktlWebApplication#appCktlVersionDb()} de <code>Application</code>. Dans ce cas, l'instance retourn&eacute;e par
 * {@link CktlWebApplication#appCktlVersionDb()} est une classe qui h&eacute;rite de {@link CktlVersionOracleUser}.<br/>
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * [...]
 *   
 * <code class="javaKeyword">public class</code> Application <code class="javaKeyword">extends</code> CktlApplication {
 *   
 *   [...]
 *  
 *   <code class="comment">// activation du service de collecte automatique</code>
 *   <code class="javaKeyword">public boolean</code> appShouldSendCollecte() {
 *     <code class="javaKeyword">return true</code>;
 *   }
 *   
 *   <code class="comment">// 1 seule instance</code>
 *   <code class="javaKeyword">private</code> A_CktlVersion <code class="javaVariable">_appCktlVersionDb</code>;
 *   
 *   <code class="comment">// collecte : lecture de la version de la base installee</code>
 *   <code class="javaKeyword">public</code> A_CktlVersion appCktlVersionDb() {
 *     if (<code class="javaVariable">_appCktlVersionDb</code> == <code class="javaKeyword">null</code>) {
 *       <code class="javaVariable">_appCktlVersionDb</code> = <code class="javaKeyword">new</code> org.cocktail.monappli.version.MonUserOracleVersion();
 *     }
 *     <code class="javaKeyword">return</code> <code class="javaVariable">_appCktlVersionDb</code>;
 *   }
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * <br/>
 * <h2><a name="aut"></a>Cr&eacute;ation d'extracteurs de version personnalis&eacute;s</h2> On peut potentiellement lire la version tout et n'importe
 * quoi (ou presque), &agrave; partir du moment ou l'on sait comment acc&eacute;der aux num&eacute;ros de version de l'objet observ&eacute;.<br/>
 * Ce travail est r&eacute;alis&eacute; par la m&eacute;thode {@link #readVersionNumber()} qui doit &ecirc;tre surcharg&eacute;e par le
 * d&eacute;veloppeur.<br/>
 * Exemple d'extraction de la version du JRE
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="javaKeyword">package</code> org.cocktail.monappli;
 * <code class="javaKeyword">import</code> org.cocktail.fwkcktlwebapp.server.version.CktlVersion;
 * <code class="javaKeyword">import</code> com.webobjects.foundation.NSArray;
 * 
 * <code class="commentJavaDoc">&#47;**
 *  * Classe de lecture de la version de java installee sur la machine qui lance l'application
 *  *
 *  * @author Nom Prenom &lt;nom.prenom at cocktail.org&gt;
 *  *&#47;</code>
 * <code class="javaKeyword">public class</code> VersionJavaLecteur <code class="javaKeyword">extends</code> A_CktlVersion {
 * 
 *   <code class="javaKeyword">public</code> String comment() {
 *     <code class="javaKeyword">return</code> <code class="javaString">""</code>;
 *   }
 * 
 *   <code class="javaKeyword">public</code> String date() {
 *     <code class="javaKeyword">return</code> <code class="javaString">""</code>;
 *   }
 * 
 *   <code class="javaKeyword">public</code> String name() {
 *     <code class="javaKeyword">return</code> <code class="javaString">"Java Runtime"</code>;
 *   }
 *  
 *   <code class="javaKeyword">public</code> CktlVersionRequirements[] dependencies() {
 *     <code class="javaKeyword">return null</code>;
 *   }
 * 
 *   <code class="javaKeyword">public int</code> versionNumMaj() {
 *     <code class="javaKeyword">return</code> <code class="javaStatic">versionNumMaj</code>;
 *   }
 * 
 *   <code class="javaKeyword">public int</code> versionNumMin() {
 *     <code class="javaKeyword">return</code> <code class="javaStatic">versionNumMin</code>;
 *   }
 * 
 *   <code class="javaKeyword">public int</code> versionNumPatch() {
 *     <code class="javaKeyword">return</code> <code class="javaStatic">versionNumPatch</code>;
 *   }
 * 
 *   <code class="javaKeyword">public int</code> versionNumBuild() {
 *     <code class="javaKeyword">return</code> <code class="javaStatic">versionNumBuild</code>;
 *   }
 * 	
 *   <code class="javaKeyword">private final static</code> String <code class="javaStatic">SYSTEM_JAVAVERSION</code> = <code class="javaString">"java.version"</code>;
 *  
 *   <code class="javaKeyword">private static int</code> <code class="javaStatic">versionNumMaj</code>;
 *   <code class="javaKeyword">private static int</code> <code class="javaStatic">versionNumMin</code>;
 *   <code class="javaKeyword">private static int</code> <code class="javaStatic">versionNumPatch</code>;
 *   <code class="javaKeyword">private static int</code> <code class="javaStatic">versionNumBuild</code>;
 * 
 * <code class="commentJavaDoc">&#47;**
 *  * R&eacute;cup&egrave;re les informations sur la version de Java.
 *  * On extrait la propriete systeme "java.version", qui retourne
 *  * une chaine sous la forme "a.b.c_d".
 *  * @throws Exception
 *  *&#47;</code>
 *   <code class="javaKeyword">public void</code> readVersionNumber() {
 *     String systemJavaversionString = (String) System.getProperty(<code class="javaStatic">SYSTEM_JAVAVERSION</code>);
 *     NSArray systemJavaversionArray = NSArray.componentsSeparatedByString(systemJavaversionString, <code class="javaString">"."</code>);
 *     <code class="javaStatic">versionNumMaj</code> = Integer.parseInt((String) systemJavaversionArray.objectAtIndex(0));
 *     <code class="javaStatic">versionNumMin</code> = Integer.parseInt((String) systemJavaversionArray.objectAtIndex(1));
 *     String versionNumPatchString = (String) systemJavaversionArray.objectAtIndex(2);
 *     NSArray versionNumPatchArray = NSArray.componentsSeparatedByString(versionNumPatchString, <code class="javaString">"_"</code>);
 *     <code class="javaStatic">versionNumPatch</code> = Integer.parseInt((String) versionNumPatchArray.objectAtIndex(0));
 *     <code class="javaStatic">versionNumBuild</code> = Integer.parseInt((String) versionNumPatchArray.objectAtIndex(1));
 *   }    
 * }
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 */
public abstract class A_CktlVersion {

	/**
	 * Constructeur par defaut.<br/>
	 * Fait appel &agrave; la m&eacute;thode {@link #readVersionNumber()}
	 */
	public A_CktlVersion() {
		try {
			readVersionNumber();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Methode a surcharger pour faire la lecture personnalise
	 * des numeros de versions. 
	 * 
	 * @throws Exception
	 */
	public void readVersionNumber() throws Exception {
		
	}
	
	/** 
	 * Nom - ne doit pas changer au fil des versions.
	 * <b>Surcharge obligatoire</b>
	 *
	 * exemple : <code>return "Demande de Travaux";</code>
	 */
	public abstract String name();

	/** 
	 * Date de publication (au format <code></code>String)
	 * <i>Surcharge facultative</i>
	 * 
	 * exemple : <code>"12/10/2007"</code>
	 */
	public String date() {
		return "";
	}

	/**
	 * Commentaire/descriptif de cette version 
	 * <i>Surcharge facultative</i>
	 * 
	 * exemple : <code>"Correction du bug qui fait tout planter";</code>
	 */
	public String comment() {
		return "";
	}
	
	/** 
	 * Numero majeur : evolue lors de changement(s) important(s) 
	 */
	public abstract int versionNumMaj();
	
	/**
	 * Numero mineur : evolue lors d'ajout de nouvelles fonctionnalit&eacute;s et/
	 * ou changement de version de base de donnees. De ce fait, une version
	 * 1.2.1 est g&eacute;n&eacute;ralement incompatible avec une version 1.3.0 
	 */
	public abstract int versionNumMin();

	/** 
	 * Numero de patch : evolue lors de correction de bug, ajout de petites fonctionnalit&eacute;s 
	 */
	public abstract int versionNumPatch();
	
	/** Numero de build : changement minime d'une meme version 
	 * (ne donne pas forcement lieu a un deploiement) 
	 */
	public abstract int versionNumBuild();
	
	/** 
	 * Retourne l'identifiant de la version.
	 * Concat&egrave;ne les numeros {@link #versionNumMaj()}, {@link #versionNumMin()}, 
	 * {@link #versionNumPatch()}, {@link #versionNumBuild()}, en les separant
	 * par le caractere '.'. Ne sont pas affiches les zeros terminaux.
	 */
	public String version() {
		StringBuffer sb = new StringBuffer();
		boolean shouldIgnorePrevNumber = true;
		if (versionNumBuild() > 0) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumBuild());
		}
		if (shouldIgnorePrevNumber && versionNumPatch() > 0) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumPatch());
		}
		if (shouldIgnorePrevNumber && versionNumMin() > 0) {
			shouldIgnorePrevNumber = false;
		}
		if (!shouldIgnorePrevNumber) {
			sb.insert(0, "."+versionNumMin());
		}
		// on affiche toujours le numero majeur
		sb.insert(0, versionNumMaj());
		return sb.toString();
	}
	
	// une instance unique, utilisee seulement par l'application finale
	private static String htmlVersion;
	
	/**
	 * Donne le formatage HTML de <code>txtVersion()</code>.
	 * Utilise la mise en page avec la feuille de style assa.css
	 */
	public String htmlVersion() {
		if (htmlVersion == null) {
			String strDateNow = DateCtrl.dateToString(DateCtrl.now());
			htmlVersion = "<font class=\"textNote\"><b>" + txtVersion() + "</b><br/>" +
					"&copy; " + strDateNow.substring(strDateNow.length() - 4) + " Association Cocktail</font>";
		}
		return htmlVersion;
	}
	
	/**
	 * Retourne une chaine de caract&egrave;re format&eacute;e qui contient :
	 * <ul>
	 * 	<li>name()</li>
	 * 	<li>version()</li>
	 * 	<li>date() si definie</li>
	 * </ul>
	 */
	public String txtVersion() {
		return name() + " Version " + version() +
		(!StringCtrl.isEmpty(date()) ? " du " + date() : "") +
		(!StringCtrl.isEmpty(comment()) ? "(" + comment() +")": "");
	}
	
	/**
	 * L'affichage du copyright par defaut.
	 * "(c) " + annee en cours + " Association Cocktail"
	 */
	public String copyright() {
		String strDateNow = DateCtrl.dateToString(DateCtrl.now());
		return "(c) " + strDateNow.substring(strDateNow.length() - 4) + " Association Cocktail";
	}

	/**
	 * La liste des frameworks n&eacute;c&eacute;ssaires au fonctionnement
	 * de l'application.
	 */
	public abstract CktlVersionRequirements[] dependencies();
	
	// la liste des dependances deja verifiees (pour ne pas parcourir
	// plusieurs fois les memes branches inutilement)
	private static NSArray<String> listCheckedDepencies = new NSArray<String>();
	
	/**
	 * Verifie si les dependances avec les autres frameworks
	 * sont correctement respectees.
	 * @throws Exception
	 */
	public void checkDependencies() throws Exception {
		CktlVersionRequirements[] listDep = dependencies();
		if (listDep != null) {
			for (CktlVersionRequirements vd : listDep) {
				// on determine le noeud a parcourir en connaissant le pere et le fils
				String keyParentFilsDependencie = txtVersion() + "_" + vd.getVersionInstance().txtVersion();
				if (!listCheckedDepencies.containsObject(keyParentFilsDependencie)) {
					listCheckedDepencies = listCheckedDepencies.arrayByAddingObject(keyParentFilsDependencie);
					vd.check(name());
					vd.getVersionInstance().checkDependencies();
				}
			}
		}
	}

	/**
	 * Affichage standard toutes les versions disponibles
	 * sur la machine qui lance l'application.
	 */
	public void rawLogVersions() {
		StringBuffer sb = new StringBuffer("Versions disponibles\n" +
																			 "--------------------\n\n");
		NSArray<String> labels = getArrayTrace(this);
		for (int i = 0; i < labels.count(); i++) {
			sb.append("  > ").append((String) labels.objectAtIndex(i)).append("\n");
		}
		sb.append("\n");
		CktlLog.rawLog(sb.toString());
	}

	/**
	 * 
	 * @param version
	 */
	private NSArray<String> getArrayTrace(A_CktlVersion version) {
		NSArray<String> result = new NSArray<String>();
		CktlVersionRequirements[] listDep = version.dependencies();
		for (int i = 0; listDep != null && i < listDep.length; i++) {
			CktlVersionRequirements vcr = (CktlVersionRequirements) listDep[i];
			A_CktlVersion versionInstance = vcr.getVersionInstance();
			String label = versionInstance.txtVersion();
			if (!result.containsObject(label)) {
				result = result.arrayByAddingObject(label);
			}
			NSArray<String> localSubArray = getArrayTrace(versionInstance);
			for (int j = 0; j < localSubArray.count(); j++) {
				String subLabel = (String) localSubArray.objectAtIndex(j);
				if (!result.containsObject(subLabel)) {
					result = result.arrayByAddingObject(subLabel);
				}
			}
		}
		return result;
	}
	
	/**
	 * Type d'exception d&eacute;di&eacute;e aux erreurs de version.
	 */ 
	public static class CktlVersionException extends Exception {
		private static final long serialVersionUID = 1L;

		public CktlVersionException(String errorMessage) {
			super("ERREUR : " + errorMessage);
		}
	}
	
	/**
	 * D&eacute;claration puis comparaison des versions disponibles et demand&eacute;es.<br/>
	 * <br/>
	 * Les 2 bornes sont facultatives. Elles sont d&eacute;clar&eacute;es en <code>String</code> compos&eacute;es
	 * de num&eacute;ros s&eacute;par&eacute;s par le caract&egrave;re '.'.<br/>
	 * La comparaison se fait entre num&eacute;ros de m&ecirc;me poids (i.e. les num&eacute;ros majeurs avec les
	 * num&eacute;ros majeurs ...), du plus au moins important (de gauche &agrave; droite).<br/>
	 * Les bornes sont inclues lors de la comparaison.<br/>
	 * <br/>
	 * Selon le degr&egrave;s de pr&eacute;cision de la borne et selon s'il s'agit de la borne minimum ou maximum, 
	 * l'algorithme va la compl&eacute;ter jusqu'au 4&egrave;me num&eacute;ro :
	 * <ul>
	 * 	<li>Borne minimum : compl&eacute;tion avec des z&eacute;ros. 		Exemple : 1.4.2 --> 1.4.2.0</li>
	 * 	<li>Borne maximum : compl&eacute;tion avec des infinis. 	Exemple : 5.3 --> 5.3.&#156;.&#156;</li>
	 * </ul>
	 */
	public static class CktlVersionRequirements {
		
		private A_CktlVersion versionIntance;
		private int[] realNum;
		private String strNumMin;
		private String strNumMax;
		private int[] numMin;
		private int[] numMax;
		private boolean hasNumMin;
		private boolean hasNumMax;
		private boolean shouldTerminateOnError;
			
		/**
		 * @param versionIntance : l'instance de la classe decrivant la version demandee
		 * @param strVersionMin : version minimale. <em>null</em> si pas de version minimum requise.
		 * @param strVersionMax : version maximale. <em>null</em> si pas de version maxumim requise.
		 * @param shouldTerminateOnError : faut-il arreter l'application en cas de mauvaise version
		 */
		public CktlVersionRequirements(A_CktlVersion versionIntance, 
				String strVersionMin, String strVersionMax, boolean shouldTerminateOnError) {
			// decoupage de la chaine version minimale
			strNumMin = strVersionMin;
			hasNumMin = !StringCtrl.isEmpty(strNumMin);
			if (hasNumMin) {
				StringTokenizer stMin = new StringTokenizer(strNumMin, ".");
				numMin = new int[stMin.countTokens()];
				int i = 0;
				while (stMin.hasMoreElements()) {
					String elem = (String) stMin.nextElement();
					numMin[i++] = Integer.parseInt(elem);
				}
			}
			// decoupage de la chaine version maximale
			strNumMax = strVersionMax;
			hasNumMax = !StringCtrl.isEmpty(strNumMax);
			if (hasNumMax) {
				StringTokenizer stMax = new StringTokenizer(strNumMax, ".");
				numMax = new int[stMax.countTokens()];
				int i = 0;
				while (stMax.hasMoreElements()) {
					String elem = (String) stMax.nextElement();
					numMax[i++] = Integer.parseInt(elem);
				}
			} 
			this.versionIntance = versionIntance;
			// construire un tableau avec les numeros lus
			realNum = new int[4];
			realNum[0] = versionIntance.versionNumMaj();
			realNum[1] = versionIntance.versionNumMin();
			realNum[2] = versionIntance.versionNumPatch();
			realNum[3] = versionIntance.versionNumBuild();
			this.shouldTerminateOnError = shouldTerminateOnError;
		}

		private final static int V_BASE 		= 1000000;
		private final static int V_COMP_MAX 	=  999999;

		/**
		 * Effectue la controle de compatibilite
		 * 
		 * @throws CktlVersionException si l'application doit etre arretee
		 */
		public void check(String nom) throws CktlVersionException {
			boolean success = true;

			// on convertit les numeros de version en base 1000000
			// et on fait une simple comparaison de long
			// ex : 1.4.2 donne 000001 000004 000002 000000
			//      1.5   donne 000001 000005 000000 000000
			//      28.55 donne 000028 000055 000000 000000
			// la completion se fait selon la borne :
			// min : on complete par des 0
			// max : on complete par des 999999
			
			BigInteger vMin = new BigInteger("0");
			BigInteger vMax = new BigInteger("0");
			BigInteger vReal = new BigInteger("0");
			DecimalFormat df = new DecimalFormat("0");
			for (int i = 0; i < 4; i++) {
				BigInteger base = new BigInteger(df.format(java.lang.Math.pow(V_BASE, 3-i)));
				if (hasNumMin && numMin.length > i) {
					vMin = vMin.add((new BigInteger(Integer.toString(numMin[i]))).multiply(base));
				}
				if (hasNumMax && numMax.length > i) {
					vMax = vMax.add((new BigInteger(Integer.toString(numMax[i]))).multiply(base));
				} else {
					vMax = vMax.add((new BigInteger(Integer.toString(V_COMP_MAX))).multiply(base));
				}
				vReal = vReal.add((new BigInteger(Integer.toString(realNum[i]))).multiply(base));
			}
		
			success = ((vMin.compareTo(vReal) == -1 || vMin.compareTo(vReal) == 0) && (vReal.compareTo(vMax) == -1 || vReal.compareTo(vMax) == 0));
					
			
			// construction du message de log
			StringBuffer sb = new StringBuffer();
			sb.append("  > ").append(nom).append(" necessite ").append(versionIntance.name()).append(" (requis: ");
			if (hasNumMin) {
				sb.append(strNumMin).append("<=");
			}			
			sb.append("[v]");
			if (hasNumMax) {
				sb.append("<=").append(strNumMax);
			}
			sb.append(" , disponible: ").append(versionIntance.version()).append(") - ");
			if (success) {
				sb.append("OK");
			} else {
				if (shouldTerminateOnError) {
					sb.append("ERREUR");
				} else {
					sb.append("ATTENTION");
				}
			}

			//
			if (!success && shouldTerminateOnError) {
				// exception avec le message
				throw new CktlVersionException(sb.toString());
			} else {
				// affichage standard
				CktlLog.rawLog(sb.toString());
			}

		}
	
		private A_CktlVersion getVersionInstance()				{ return versionIntance; }

	}
	
	
}
