package org.cocktail.fwkcktlwebapp.server.version;


import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlEOUtilities;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * Classe qui effectue le contr&ocirc;le de version d'un user d'une base de donn&eacute;es ORACLE.<br/>
 * <br/>
 * Elle interroge une table qui contient la liste des versions d'un ou plusieurs user.<br/>
 * Cette table doit au moins avoir les attributs suivants :<br/>
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align="center" class="paramBoxHeader">
 * <td>Type Oracle</td><td>Description</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>VARCHAR2</code></td>
 * <td align="left">L'identifiant de version (num&eacute;ros separes par le caract&egrave;re '.')</td>
 * </tr>
 * <tr align="center" valign="top" class="paramBoxContents">
 * <td><code>DATE</code></td>
 * <td align="left">La date de diffusion de l'application (la derni&egrave;re date sera prise en compte)</td>
 * </tr>
 * </table>
 * </p>
 * <br/>
 * <font class="importantNote">Attention</font> : La m&eacute;thode <code>Application.mainModelName()</code> doit 
 * &ecirc;tre surcharg&eacute;e afin que la requ&ecirc;te SQL soit correctement appel&eacute;e.<br/>
 * <br/>
 * Exemple d'utilisation :
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 *<code class="javaKeyword">package</code> org.cocktail.application.version;
 *<code class="javaKeyword">import</code> fr.univlr.cri.webapp.VersionCocktailOracleUser;
 *
 *<code class="commentJavaDoc">&#47;**
 * * Classe de controle de la version du user Oracle de mon application a moi
 * *
 * * @author Nom Prenom &lt;nom.prenom at cocktail.org&gt;
 * *&#47;</code>
 *<code class="javaKeyword">public class</code> VersionOracleMonUser <code class="javaKeyword">extends</code> CktlVersionOracleUser {
 *
 *  <code class="comment">// nom de ce qui est controle (pour affichage dans les logs)</code>
 *  <code class="javaKeyword">public String</code> name() {
 *    <code class="javaKeyword">return</code> <code class="javaString">"MON_USER ORACLE user"</code>;
 *  }
 *
 *  <code class="comment">// la table contenant la liste des versions de user</code>
 *  <code class="javaKeyword">public String</code> dbUserTableName() {	
 *    <code class="javaKeyword">return</code> <code class="javaString">"MON_USER.DB_VERSION"</code>;
 *  }
 *  
 *  <code class="comment">// l'attribut contenant la date de la version</code>
 *  <code class="javaKeyword">public String</code> dbVersionDateColumnName() {
 *    <code class="javaKeyword">return</code> <code class="javaString">"DB_VERSION_DATE"</code>;
 *  }
 *  
 *  <code class="comment">// l'attribut contenant l'id de la version</code>
 *  <code class="javaKeyword">public String</code> dbVersionIdColumnName(){
 *    <code class="javaKeyword">return</code> <code class="javaString">"DB_VERSION_ID"</code>;
 *  }

 *  <code class="comment">// mettre ici les dependances du user (version oracle, autres version users ...)</code>
 *  <code class="javaKeyword">public</code> CktlVersionRequirements[] dependencies() {
 *    <code class="javaKeyword">return null</code>;
 *  }
 *}</pre></td></tr></table>
 * </p>
 */
public abstract class CktlVersionOracleUser extends A_CktlVersion {

	/** 
	 * La table de version &agrave; consulter.<br/>
	 * <br/>
	 * exemple : <code>"DT3.DB_VERSION"</code>;
	 */
	public abstract String dbUserTableName();

	/**
	 * La colonne de la table point&eacute;e par {@link #dbUserTableName()} qui contient
	 * l'identifiant de version.<br/>
	 * <br/>
	 * exemple : <code>return "DB_VERSION_ID"</code>
	 */
	public abstract String dbVersionIdColumnName();

	/**
	 * La colonne de la table point&eacute;e par {@link #dbUserTableName()} qui contient
	 * le date de version.<br/>
	 * <br/>
	 * exemple : <code>return "DB_VERSION_DATE"</code>
	 */
	public abstract String dbVersionDateColumnName();
	
	/**
	 * Une clause SQL suppl&eacute;mentaire a appliquer en plus de la derniere version en date (facultatif).<br/>
	 * <br/>
	 * exemple : <code>return "APP_ID = 12"</code><br/>
	 * permettra de retourner la ligne r&eacute;pondant a la restriction <code>"APP_ID = 12"</code> dont
	 * la date dans la colonne {@link #dbVersionDateColumnName()} est maximum.
	 */
	public String sqlRestriction() {
		return "";
	}

	public void readVersionNumber() throws Exception {
    CktlWebApplication criApp = (CktlWebApplication) CktlWebApplication.application();

    String restriction = sqlRestriction();
    String sqlQuery = "SELECT " + dbVersionIdColumnName() + ","+dbVersionDateColumnName() + " " +
			"FROM "+dbUserTableName()+" DB1 WHERE DB1." + dbVersionDateColumnName() + 
				" = (SELECT MAX(DB2." + dbVersionDateColumnName() + ") FROM "+dbUserTableName()+" DB2" +
						(!StringCtrl.isEmpty(restriction) ? " WHERE " + restriction : "") + 
			")";
    NSArray dbList =
    	CktlEOUtilities.rawRowsForSQL(criApp.dataBus().editingContext(), criApp.mainModelName(),
    			sqlQuery ,	new NSArray("dbVersionId").arrayByAddingObject("dbVersionDate"));
    if (dbList.count() == 0) {
    	CktlLog.rawLog("Erreur ! La version de la base de donnees ne peut pas etre determinee (table "+
    			dbUserTableName()+", attributs " + dbVersionIdColumnName() + "," + dbVersionDateColumnName() + ")");
    } else {
    	NSDictionary dbDico = (NSDictionary)dbList.objectAtIndex(0);
      String dbCurrentVersionString = dbDico.objectForKey("dbVersionId").toString();
      NSArray dbCurrentVersionArray = NSArray.componentsSeparatedByString(dbCurrentVersionString, ".");
      versionNumMaj = dbCurrentVersionArray.count() > 0 ? Integer.parseInt((String)dbCurrentVersionArray.objectAtIndex(0)) : 0;
      versionNumMin = dbCurrentVersionArray.count() > 1 ? Integer.parseInt((String)dbCurrentVersionArray.objectAtIndex(1)) : 0;
      versionNumPatch = dbCurrentVersionArray.count() > 2 ? Integer.parseInt((String)dbCurrentVersionArray.objectAtIndex(2)) : 0;
      versionNumBuild = dbCurrentVersionArray.count() > 3 ? Integer.parseInt((String)dbCurrentVersionArray.objectAtIndex(3)) : 0;
      // format 2007-24-11 00:00:00 Etc/GMT
      String dbCurentVersionDateString = dbDico.objectForKey("dbVersionDate").toString();
      NSArray dbCurrentVersionDateArray = NSArray.componentsSeparatedByString(dbCurentVersionDateString, " ");
      String dbCurrentVersionDateStringClean = dbCurrentVersionDateArray.objectAtIndex(0) + " " + dbCurrentVersionDateArray.objectAtIndex(1);
      versionDate = DateCtrl.dateToString(DateCtrl.stringToDate(dbCurrentVersionDateStringClean, "%Y-%d-%m %H:%M:%S"));
    }
	}
	
	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}
	
	public String date() {
		return versionDate;
	}
	
  private int versionNumMaj;
  private int versionNumMin;
  private int versionNumPatch;
  private int versionNumBuild;
  private String versionDate;
}
