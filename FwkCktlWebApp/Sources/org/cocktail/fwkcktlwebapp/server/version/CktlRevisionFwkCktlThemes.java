package org.cocktail.fwkcktlwebapp.server.version;

/**
 * Classe de contrôle de lecture du numéro de révision SVN
 * du framework Cocktail FwkCktlThemes
 */
public class CktlRevisionFwkCktlThemes 
	extends A_CktlRevisionFrameworkProperties {

	public String frameworkName() {
		return "FwkCktlThemes";
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

}
