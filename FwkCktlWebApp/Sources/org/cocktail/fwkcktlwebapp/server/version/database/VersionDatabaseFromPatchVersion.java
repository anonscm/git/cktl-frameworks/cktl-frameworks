package org.cocktail.fwkcktlwebapp.server.version.database;

import org.cocktail.fwkcktlwebapp.server.version.CktlVersionOracleUser;


/**
 * Vérification d'une version dans la table GRHUM.PATCH_VERSION.
 *
 */
public class VersionDatabaseFromPatchVersion extends CktlVersionOracleUser {

	private static String NAME = "BDD COCKTAIL";
	private static String DB_USER_TABLE_NAME = "GRHUM.PATCH_VERSION";
	private static String DB_VERSION_DATE_COLUMN_NAME = "PV_DT_INSTALL";
	private static String DB_VERSION_ID_COLUMN_NAME = "PV_VERSION";
	private static String DB_VERSION_SCHEMA_COLUMN_NAME = "PV_SCHEMA";
	private static final String ALL_SCHEMA = "BDD";
	
	@Override
	public String dbUserTableName() {
		return DB_USER_TABLE_NAME;
	}

	@Override
	public String dbVersionDateColumnName() {
		return DB_VERSION_DATE_COLUMN_NAME;
	}

	@Override
	public String dbVersionIdColumnName() {
		return DB_VERSION_ID_COLUMN_NAME;
	}

	@Override
	public String name() {
		return NAME + " SCHEMA " + schema();
	}

	@Override
	public String sqlRestriction() {
		return DB_VERSION_SCHEMA_COLUMN_NAME + " = '" + schema() + "'";
	}

	@Override
	public CktlVersionRequirements[] dependencies() {
		return null;
	}

	/**
	 * @return le nom du schéma surlequel on voit
	 */
	public String schema() {
		return ALL_SCHEMA;
	}
}
