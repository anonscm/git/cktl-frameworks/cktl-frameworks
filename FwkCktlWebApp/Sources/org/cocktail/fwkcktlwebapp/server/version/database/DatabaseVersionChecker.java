package org.cocktail.fwkcktlwebapp.server.version.database;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion.CktlVersionException;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion.CktlVersionRequirements;

import com.webobjects.appserver.WOApplication;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXApplication;
import er.extensions.foundation.ERXProperties;

/**
 * Vérification des version de base en fonction de la table
 * GRHUM.PATCH_VERSION.
 * 
 * Cette vérification doit être activée en renseignant les properties :
 * <ul>
 * 	<li><code>org.cocktail.fwkcktlwebapp.versiondatabase.enabled=true</code></li>
 *  <li><code>org.cocktail.fwkcktlwebapp.versiondatabase.schemas=(SCO_SCOLARITE:0.1.1, SCO_PRE_SCOLARITE:1.0.1)</code></li>
 * </ul>
 *
 */
public class DatabaseVersionChecker {

	private static final String VERSIONDATABASE_SCHEMAS = "org.cocktail.fwkcktlwebapp.versiondatabase.schemas";
	private static final String VERSIONDATABASE_ENABLED = "org.cocktail.fwkcktlwebapp.versiondatabase.enabled";

	/**
	 * @param nomAppli le nom de l'application pour laquelle on veut vérifier les versions
	 * @throws CktlVersionException lorsqu'une version nécessaire n'est pas présente et que l'on est en production
	 * 
	 */
	public void check(String nomAppli) throws CktlVersionException {
		if (versionDatabaseEnabled()) {
			boolean isDevMode = ERXApplication.isDevelopmentModeSafe();
			boolean isDbFlyway = ((CktlWebApplication) WOApplication.application()).isDbFlyway();
			boolean controlePassant = isDevMode || isDbFlyway;
			
			for (final Schema schema : schemas()) {
				VersionDatabaseFromPatchVersion versionDatabaseFromPatchVersion = new VersionDatabaseFromPatchVersion() {
					@Override
					public String schema() {
						return schema.getNom();
					}
				};
				new CktlVersionRequirements(versionDatabaseFromPatchVersion, schema.getVersion(), null, !controlePassant).check(nomAppli);;
			}
			
		}
	}
	
	private boolean versionDatabaseEnabled() {
		return ERXProperties.booleanForKeyWithDefault(VERSIONDATABASE_ENABLED, false);
	}

	@SuppressWarnings("unchecked")
	private List<Schema> schemas() {
		NSArray<String> schemasAsString = ERXProperties.arrayForKeyWithDefault(VERSIONDATABASE_SCHEMAS, NSArray.emptyArray());
		List<Schema> schemas = new ArrayList<Schema>();
		for (String schemaAsString : schemasAsString) {
			schemas.add(Schema.initFromString(schemaAsString));
		}
		return schemas;
	}

	private static class Schema {
		
		private String nom;
		private String version;

		public Schema(String nom, String version) {
			super();
			this.nom = nom;
			this.version = version;
		}

		public static Schema initFromString(String schemaAsStr) {
			int colonnIndex = schemaAsStr.indexOf(':');
			String nom = schemaAsStr.substring(0, colonnIndex);
			String version = schemaAsStr.substring(colonnIndex+1);
			Schema schema = new Schema(nom, version);
			return schema;
		}

		public String getNom() {
			return nom;
		}
		
		public String getVersion() {
			return version;
		}
		
	}
	
}
