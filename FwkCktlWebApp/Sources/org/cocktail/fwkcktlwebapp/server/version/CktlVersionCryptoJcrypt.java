package org.cocktail.fwkcktlwebapp.server.version;

import org.cocktail.crypto.jcrypt.Jcrypt;



/**
 * Classe de lecture de la version du jar Jcrypt
 */
public class CktlVersionCryptoJcrypt extends CktlVersionJar {
	
	public CktlVersionCryptoJcrypt() {
		super(Jcrypt.FQDN_CLASS_NAME);
	}
}
