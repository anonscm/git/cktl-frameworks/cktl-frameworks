package org.cocktail.fwkcktlwebapp.server.version;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;

/**
 * Classe de lecture d'un ou plusieurs numéros au sein
 * d'un fichier au format .plist xml dans un framework WebObjects
 * 
 * @author ctarade
 */
public abstract class A_CktlVersionFrameworkPlist 
	extends A_CktlVersion {

	private int versionNumMaj;
	private int versionNumMin;
	private int versionNumPatch;
	private int versionNumBuild;

	/**
	 * Le nom du framework
	 */
	public abstract String frameworkName();

	/**
	 * Le nom du fichier plist dans le framework
	 * (inclure l'extension .plist, exemple : "Info.plist"
	 */
	public abstract String plistFileName();
	
	/**
	 * La clé contenant la proprieté à contrôler
	 */
	public abstract String plistKeyName();

	/**
	 * Classe effectuant la lecture du numéro de version
	 */
	public void readVersionNumber() throws Exception {
		String cFBundleShortVersionString = ERXProperties.versionStringForFrameworkNamed(frameworkName());
		NSArray cFBundleShortVersionArray = NSArray.componentsSeparatedByString(cFBundleShortVersionString, ".");		
		if (cFBundleShortVersionArray.count()>0) {
			versionNumMaj = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(0));
		} else {
			versionNumMaj = new Integer(0).intValue();
		}
		if (cFBundleShortVersionArray.count()>1) {
			versionNumMin = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(1));
		} else {
			versionNumMaj = new Integer(0).intValue();
		}
		if (cFBundleShortVersionArray.count()>2) {
			versionNumPatch = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(2));
		} else {
			versionNumPatch = new Integer(0).intValue();
		}
		if (cFBundleShortVersionArray.count()>3) {
			String versionNumBuildStr = (String)cFBundleShortVersionArray.objectAtIndex(3);
			if (StringUtils.isNumeric(versionNumBuildStr)) {
				versionNumBuild = Integer.parseInt(versionNumBuildStr);
			} else {
				CktlLog.log("> " + frameworkName() + ": Impossible de convertir la version de build \"" + 
								versionNumBuildStr + "\" en int");
			}
		} else {
			versionNumBuild = new Integer(0).intValue();
		}
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}
	
	public int versionNumBuild() {
		return versionNumPatch;
	}

	public int versionNumPatch() {
		return versionNumBuild;
	}

}
