package org.cocktail.fwkcktlwebapp.server.version;



import java.util.StringTokenizer;

/**
 * Classe de lecture de la version de la JRE
 */
public class CktlVersionJava extends A_CktlVersion {
	
	public String comment() {
		return "";
	}

	public String date() {
		return "";
	}

	public String name() {
		return "Java Runtime";
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}
	
	
	private static final String SYSTEM_JAVAVERSION = "java.version";

  private static int versionNumMaj;
  private static int versionNumMin;
  private static int versionNumPatch;
  private static int versionNumBuild;

  /**
   * Récupère les informations sur la version de Java.
   * On extrait la propriete systeme "java.version", qui retourne
   * une chaine sous la forme "a.b.c_d" ou "a.b.c-d".
   * @throws Exception
   */
  public void readVersionNumber() {
    String systemJavaversionString = (String) System.getProperty(SYSTEM_JAVAVERSION);
    StringTokenizer tk = new StringTokenizer(systemJavaversionString, "._-");
    versionNumMaj = Integer.parseInt((String)tk.nextToken());
    versionNumMin = Integer.parseInt((String)tk.nextToken());
    versionNumPatch = Integer.parseInt((String)tk.nextToken());
    versionNumBuild = Integer.parseInt((String)tk.nextToken());
		/*
    String systemJavaversionString = (String) 	System.getProperty(SYSTEM_JAVAVERSION);
    NSArray systemJavaversionArray = NSArray.componentsSeparatedByString(systemJavaversionString, ".");
    versionNumMaj = Integer.parseInt((String)systemJavaversionArray.objectAtIndex(0));
    versionNumMin = Integer.parseInt((String)systemJavaversionArray.objectAtIndex(1));
    String versionNumPatchString = (String)systemJavaversionArray.objectAtIndex(2);
    NSArray versionNumPatchArray = NSArray.componentsSeparatedByString(versionNumPatchString, "_");
    versionNumPatch = Integer.parseInt((String)versionNumPatchArray.objectAtIndex(0));
    versionNumBuild = Integer.parseInt((String)versionNumPatchArray.objectAtIndex(1));
    */
  }    

}
