package org.cocktail.fwkcktlwebapp.server.version;

/**
 * Classe de lecture du numéro de révision SVN d'un 
 * framework WebObjects. Le fichier contenant le 
 * numéro de révision doit d'appeler "Properties" et
 * être dans le sous-répertoire Resources du projet.
 * La clé du numéro de version doit être "svn.revision"
 * et la valeur doit être un entier positif.
 * 
 * Le numéro de révision est positionné sur le numéro majeur
 * du numéro de version. Les autres numéros sont à 0.
 * 
 * @author ctarade
 */
public abstract class A_CktlRevisionFrameworkProperties 
	extends A_CktlVersionFrameworkProperties {

	/**
	 * Le nom du framework
	 */
	public abstract String frameworkName();

	/**
	 * Affichage du libellé révision SVN du framework
	 */
	public String name() {
		return frameworkName() + " revision SVN";
	}

	/**
	 * Le nom du fichier est toujours le même
	 */
	public String propertiesFileName() {
		return "Properties";
	}

	/**
	 * La clé du numéro de révision : on le positionne dans le numéro majeur
	 */
	public String propertiesVersionNumMajKeyName() {
		return "svn.revision";
	}

	/**
	 * pas utilisé
	 */
	public String propertiesVersionNumMinKeyName() {
		return null;
	}

	/**
	 * pas utilisé
	 */
	public String propertiesVersionNumPatchKeyName() {
		return null;
	}

	/**
	 * pas utilisé
	 */
	public String propertiesVersionNumBuildKeyName() {
		return null;
	}

}
