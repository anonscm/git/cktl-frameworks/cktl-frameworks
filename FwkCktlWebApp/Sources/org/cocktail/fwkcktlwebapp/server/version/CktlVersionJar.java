package org.cocktail.fwkcktlwebapp.server.version;

import com.webobjects.foundation.NSArray;

/**
 * Classe de lecture de la version d'un fichier Jar.<br/>
 * L'utilisateur doit preciser le nom du package et de la classe associee.<br/>
 * Le lecture du numero de version se fait via la methode {@link Package#getImplementationVersion()}.<br>
 * Exemple d'utilisation :
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example">
 * <tr>
 * <td>
 * 
 * <pre>
 * <code class="comment">// version de JasperReport</code>
 * <code class="javaKeyword">new</code> CktlVersionJar(<code class="javaString">"net.sf.jasperreports.engine.JasperReport"</code>);
 * </pre>
 * 
 * </td>
 * </tr>
 * </table>
 * </p>
 */
public class CktlVersionJar extends A_CktlVersion {

	private String fqdnClassName;

	public CktlVersionJar(String aFqdnClassName) {
		super();
		fqdnClassName = aFqdnClassName;
		try {
			localReadVersionNumber();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

	public String name() {
		return "Jar \"" + fqdnClassName + "\"";
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}

	private int versionNumMaj;
	private int versionNumMin;
	private int versionNumPatch;
	private int versionNumBuild;

	// la lecture reelle des parametres, qui a que fqdnClassName soit renseignee
	protected void localReadVersionNumber() throws ClassNotFoundException {
		final Package jarPackage = Class.forName(fqdnClassName).getPackage();
		final String jarVersion = jarPackage.getImplementationVersion();
		NSArray jarVersionArray = NSArray.componentsSeparatedByString(jarVersion, ".");
		if (jarVersionArray.count() > 0) {
			versionNumMaj = Integer.parseInt((String) jarVersionArray.objectAtIndex(0));
		}
		if (jarVersionArray.count() > 1) {
			versionNumMin = Integer.parseInt((String) jarVersionArray.objectAtIndex(1));
		}
		if (jarVersionArray.count() > 2) {
			versionNumPatch = Integer.parseInt((String) jarVersionArray.objectAtIndex(2));
		}
		if (jarVersionArray.count() > 3) {
			versionNumBuild = Integer.parseInt((String) jarVersionArray.objectAtIndex(3));
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see fr.univlr.cri.webapp.VersionCocktail#readVersionNumber()
	 */
	public void readVersionNumber() throws Exception {

	}

}
