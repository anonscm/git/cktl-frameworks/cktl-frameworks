package org.cocktail.fwkcktlwebapp.server.version.app;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionDigitsExtractor;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

/**
 * 
 * 
 * @author Alexis Tual
 *
 */
public class VersionAppFromVersionProperties implements VersionApp {

    private CktlWebApplication application;
    private VersionDigitsExtractor versionDigitsExtractor;
    private Properties versionProperties;

    /**
     * @param application l'application, nécessaire pour chercher les ressources
     */
    public VersionAppFromVersionProperties(CktlWebApplication application) {
        this.application = application;
    }

    private Properties versionProperties() {
        if (versionProperties == null) {
            InputStream is = application.resourceManager().inputStreamForResourceNamed("version.properties", "app", null);
            versionProperties = new Properties();
            try {
                versionProperties.load(is);
            } catch (IOException e) {
                CktlWebApplication.log.warn("Impossible de lire le fichier Resources/version.properties");
            }
        }
        return versionProperties;
    }

    public void setVersionProperties(Properties versionProperties) {
        this.versionProperties = versionProperties;
    }
    
    private VersionDigitsExtractor versionDigitsExtractor() {
        if (versionDigitsExtractor == null) {
            this.versionDigitsExtractor = new VersionDigitsExtractor(fullVersion());
        }
        return versionDigitsExtractor;
    }
    
    /**
     * @return la version complète
     */
    public String fullVersion() {
        return versionProperties().getProperty("version");
    }

    /** 
     * {@inheritDoc}
     */
    public int majVersion() {
        return versionDigitsExtractor().majorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int minVersion() {
        return versionDigitsExtractor().minorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int patchVersion() {
        return versionDigitsExtractor().patchVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int buildVersion() {
        return versionDigitsExtractor().buildVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public String dateVersion() {
        return versionProperties().getProperty("date");
    }

    public String projetVersion() {
    	return versionProperties().getProperty("projet");
    }
}
