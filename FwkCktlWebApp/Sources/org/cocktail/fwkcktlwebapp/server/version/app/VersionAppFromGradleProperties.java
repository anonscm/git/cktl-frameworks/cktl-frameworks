package org.cocktail.fwkcktlwebapp.server.version.app;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionDigitsExtractor;

import com.webobjects.foundation.NSBundle;

/**
 * Version d'appli se basant sur le gradle.properties.
 * Si le gradle.properties n'existe pas ou que l'attribut version = 
 * n'est pas présent, la version renvoyée sera 0.0.0.0
 * 
 * @author Alexis Tual
 *
 */
public class VersionAppFromGradleProperties implements VersionApp {

    public static final Logger LOG = Logger.getLogger(VersionAppFromGradleProperties.class);
    private String fullVersion;
    private VersionDigitsExtractor versionDigitsExtractor;
    
    /**
     * Constructeur par défaut
     */
    public VersionAppFromGradleProperties() {
        this.versionDigitsExtractor = new VersionDigitsExtractor(fullVersion());
    }
    
    private String fullVersion() {
        if (fullVersion == null) {
            fullVersion = "";
            InputStream inputStream = null;
            try {
                NSBundle mainBundle = NSBundle.mainBundle();
                URL bundlePath = mainBundle.bundlePathURL();
                String pathToGradleProperties = bundlePath.getPath() + File.separator + "gradle.properties";
                File gradleProperties = new File(pathToGradleProperties);
                if (gradleProperties.exists()) {
                    inputStream = new FileInputStream(gradleProperties);
                    Properties props = new Properties();
                    props.load(inputStream);
                    fullVersion = (String) props.get("version");
                }
				else {
					//Dans le cas de build avec bundle, le fichier gradle.properties ne se trouve pas dans le bundlePath
					//on remonte donc de deux répertoires pour accéder à la racine du projet (doit fonctionner dans la majorité des cas)
					String parentDirectory = gradleProperties.getParent();
					File dir = new File(parentDirectory);
					dir = dir.getParentFile().getParentFile();
					gradleProperties = new File(dir, "gradle.properties");
					inputStream = new FileInputStream(gradleProperties);
					Properties props = new Properties();
					props.load(inputStream);
					fullVersion = (String) props.get("version");
				}
            } catch (FileNotFoundException e) {
                LOG.warn("Impossible de lire le numéro de version dans le fichier gradle.properties" , e);
            } catch (IOException e) {
                LOG.warn("Impossible de lire le numéro de version dans le fichier gradle.properties" , e);
            } finally {
                if (inputStream != null) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        LOG.debug(e);
                    }
                }
            }
        }
        return fullVersion;
    }

    /** 
     * {@inheritDoc}
     */
    public String dateVersion() {
        return new SimpleDateFormat().format(new Date());
    }

    /** 
     * {@inheritDoc}
     */
    public int majVersion() {
        return versionDigitsExtractor.majorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int minVersion() {
        return versionDigitsExtractor.minorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int patchVersion() {
        return versionDigitsExtractor.patchVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int buildVersion() {
        return versionDigitsExtractor.buildVersion();
    }

}
