package org.cocktail.fwkcktlwebapp.server.version.app;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionDigitsExtractor;

import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;

/**
 * Affichage de la version de l'appli à l'aide
 * des informations présentes dans le Info.plist généré.
 * 
 * @author Alexis Tual
 *
 */
public class VersionAppFromInfoPlist implements VersionApp {
    
    private VersionDigitsExtractor versionDigitsExtractor;

    /**
     * Constructeur par défaut
     */
    public VersionAppFromInfoPlist() {
        this.versionDigitsExtractor = new VersionDigitsExtractor(fullVersion());
    }

    /** 
     * {@inheritDoc}
     */
    public String fullVersion() {
        NSBundle bundle = NSBundle.mainBundle();
        String version = "";
        if (bundle != null) {
            NSDictionary<String, Object> infos = bundle._infoDictionary();
            if (infos != null) {
                version = (String) infos.objectForKey(NSBundle.CFBUNDLESHORTVERSIONSTRINGKEY);
            }
        }
        return version;
    }

    /** 
     * {@inheritDoc}
     */
    public String dateVersion() {
        // TODO : à voir comment récupérer la date de version
        // dans le build
        return "";
    }

    /** 
     * {@inheritDoc}
     */
    public int majVersion() {
        return versionDigitsExtractor.majorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int minVersion() {
        return versionDigitsExtractor.minorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int patchVersion() {
        return versionDigitsExtractor.patchVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int buildVersion() {
        return versionDigitsExtractor.buildVersion();
    }
    
}
