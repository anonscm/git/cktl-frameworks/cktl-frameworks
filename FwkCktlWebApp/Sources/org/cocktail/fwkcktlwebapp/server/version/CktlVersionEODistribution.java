package org.cocktail.fwkcktlwebapp.server.version;

import java.io.IOException;

import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;

public class CktlVersionEODistribution extends A_CktlVersion {

	public String comment() {
		return null;
	}

	public String date() {
		return null;
	}

	public String name() {
		return APP_SERVER_NOM;
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}


	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}
	
  private static final String APP_SERVER_NOM = "EODitribution";
  private static final String WO_CFBUNDLESHORTVERSIONSTRING = "CFBundleShortVersionString";
  private static final String WO_BUILDVERSION = "BuildVersion";
  private static final String WO_FILE = "version.plist";
  private static final String WO_FRMK = "JavaEODistribution";

  private static int versionNumMaj;
  private static int versionNumMin;
  private static int versionNumPatch;
  private static int versionNumBuild;

  /**
   * Récupère les informations sur le serveur d'application.
   * @throws Exception 
   * @throws IOException 
   * @throws Exception
   */
  public void readVersionNumber() throws Exception {
      String cFBundleShortVersionString = ERXProperties.versionStringForFrameworkNamed(WO_FRMK);
      NSArray cFBundleShortVersionArray = NSArray.componentsSeparatedByString(cFBundleShortVersionString, ".");
      versionNumMaj = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(0));
      versionNumMin = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(1));
      versionNumPatch = Integer.parseInt((String)cFBundleShortVersionArray.objectAtIndex(2));
      // pas dispo dans ts les fwk
      versionNumBuild = 0;
  }    

}
