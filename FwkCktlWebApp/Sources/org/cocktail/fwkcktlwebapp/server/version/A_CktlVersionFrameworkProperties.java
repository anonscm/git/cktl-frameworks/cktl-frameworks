package org.cocktail.fwkcktlwebapp.server.version;

import java.util.Properties;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

/**
 * Classe de lecture d'un numéro de version au sein
 * d'un fichier {@link Properties} dans un framwork WebObjects
 * 
 * @author ctarade
 */
public abstract class A_CktlVersionFrameworkProperties
	extends A_CktlVersion {

	private int versionNumMaj;
	private int versionNumMin;
	private int versionNumPatch;
	private int versionNumBuild;
	
	/**
	 * Le nom du framework
	 */
	public abstract String frameworkName();

	/**
	 * Le nom du fichier {@link Properties} dans le framewor
	 */
	public abstract String propertiesFileName();
	
	/**
	 * La clé contenant la propriété du numéro majeur
	 */
	public abstract String propertiesVersionNumMajKeyName();

	/**
	 * La clé contenant la propriété du numéro mineur
	 */
	public abstract String propertiesVersionNumMinKeyName();

	/**
	 * La clé contenant la propriété du numéro du patch
	 */
	public abstract String propertiesVersionNumPatchKeyName();

	/**
	 * La clé contenant la propriété du numéro du build
	 */
	public abstract String propertiesVersionNumBuildKeyName();


	/**
	 * Classe effectuant la lecture du numéro de version
	 */
	public void readVersionNumber() throws Exception {
		java.io.InputStream is = CktlWebApplication.application().resourceManager().inputStreamForResourceNamed(
				propertiesFileName(), frameworkName(), null);
		Properties properties = new Properties(); 
		properties.load(is);
		versionNumMaj = versionNumMin = versionNumPatch = versionNumBuild = 0;
		Object propertiesValue = null;
		if (propertiesVersionNumMajKeyName() != null) {
			propertiesValue = properties.getProperty(propertiesVersionNumMajKeyName());
			if (propertiesValue != null) {
				versionNumMaj = Integer.parseInt(propertiesValue.toString());
			}
		}
		if (propertiesVersionNumMinKeyName() != null) {
			propertiesValue = properties.getProperty(propertiesVersionNumMinKeyName());
			if (propertiesValue != null) {
				versionNumMin = Integer.parseInt(propertiesValue.toString());
			}
		}
		if (propertiesVersionNumPatchKeyName() != null) {
			propertiesValue = properties.getProperty(propertiesVersionNumPatchKeyName());
			if (propertiesValue != null) {
				versionNumPatch = Integer.parseInt(propertiesValue.toString());
			}
		}
		if (propertiesVersionNumBuildKeyName() != null) {
			propertiesValue = properties.getProperty(propertiesVersionNumBuildKeyName());
			if (propertiesValue != null) {
				versionNumBuild = Integer.parseInt(propertiesValue.toString());
			}
		}
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}
	
	public int versionNumBuild() {
		return versionNumBuild;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}
	
}
