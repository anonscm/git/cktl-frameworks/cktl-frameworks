package org.cocktail.fwkcktlwebapp.server.version;

import org.cocktail.crypto.md5.MD5Crypt;



/**
 * Classe de lecture de la version du jar MD5Crypt
 */
public class CktlVersionCryptoMD5 extends CktlVersionJar {
	
	public CktlVersionCryptoMD5() {
		super(MD5Crypt.FQDN_CLASS_NAME);
	}

}
