package org.cocktail.fwkcktlwebapp.server.version;



import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

/**
 * Classe de lecture de la version du serveur ORACLE
 */
public class CktlVersionOracleServer extends A_CktlVersion {

	public String comment() {
		return "";
	}

	public String date() {
		return "";
	}

	public String name() {
		return "Serveur Oracle";
	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}
	
	
	private static final String ORACLE_SQL_BANNER = "select BANNER from v$version where banner like '%Oracle%'";
  
  private static int versionNumMaj;
  private static int versionNumMin;
  private static int versionNumPatch;
  private static int versionNumBuild;
  // la version d'oracle est sur 5 chiffres
  private static int versionNumExtra;

  /**
   * Récupère les informations sur le serveur d'application.
   * @throws Exception
   */
  public void readVersionNumber()  {
  	final NSDictionary res = ((CktlWebApplication) CktlWebApplication.application()).dataBus().executeSQLQuery(
  			ORACLE_SQL_BANNER, null);
    if (res.valueForKey("BANNER") != null) {
    	String bannerString = res.valueForKey("BANNER").toString();
    	NSArray bannerArray = NSArray.componentsSeparatedByString(bannerString, " ");
    	NSArray oracleVersionArray = null;
    	// on recherche une chaine contient 4 "."
    	for (int i = 0; i < bannerArray.count(); i++) {
				String bannerTockenString = (String) bannerArray.objectAtIndex(i);
				NSArray bannerTockenArray = NSArray.componentsSeparatedByString(bannerTockenString, ".");
				if (bannerTockenArray.count() == 5) {
					oracleVersionArray = bannerTockenArray;
					break;
				}
			}
    	if (oracleVersionArray != null) {
        versionNumMaj = Integer.parseInt((String)oracleVersionArray.objectAtIndex(0));
        versionNumMin = Integer.parseInt((String)oracleVersionArray.objectAtIndex(1));
        versionNumPatch = Integer.parseInt((String)oracleVersionArray.objectAtIndex(2));
        versionNumBuild = Integer.parseInt((String)oracleVersionArray.objectAtIndex(3));
        versionNumExtra = Integer.parseInt((String)oracleVersionArray.objectAtIndex(4));
    	}
    }
  }    

  /** 
   * Affichage du 5eme numero
   */
  public String version() {
  	return super.version() + "." + Integer.toString(versionNumExtra);
  }


}
