package org.cocktail.fwkcktlwebapp.server.version;

/**
 * Classe de lecture de la version de ServeurPlanningFwk
 */
public class CktlVersionServeurPlanningFwk 
	extends A_CktlVersionFrameworkPlist {

	private static final String WO_CFBUNDLESHORTVERSIONSTRING = "CFBundleShortVersionString";
	private static final String WO_FILE = "Info.plist";
	private static final String WO_FRMK = "ServeurPlanningFwk";
	
	public String name() {
		return "ServeurPlanningFwk";
	}

	public String frameworkName() {
		return WO_FRMK;
	}

	public String plistFileName() {
		return WO_FILE;
	}

	public String plistKeyName() {
		return WO_CFBUNDLESHORTVERSIONSTRING;
	}	
 
	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {	
				new CktlVersionRequirements(new org.cocktail.fwkcktlwebapp.server.version.Version(), "4.0.13", null, true)
		};
	}

}
