package org.cocktail.fwkcktlwebapp.server.version;

import org.apache.xerces.impl.Version;

import com.webobjects.foundation.NSArray;

public class CktlVersionXerces extends A_CktlVersion {

	public CktlVersionXerces() {
		super();
		try {
			localReadVersionNumber();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public CktlVersionRequirements[] dependencies() {
		return null;
	}

	public String name() {
		return "Xerces";
	}

	public int versionNumMaj() {
		return versionNumMaj;
	}

	public int versionNumMin() {
		return versionNumMin;
	}

	public int versionNumPatch() {
		return versionNumPatch;
	}

	public int versionNumBuild() {
		return versionNumBuild;
	}

	private int versionNumMaj;
	private int versionNumMin;
	private int versionNumPatch;
	private int versionNumBuild;

	// la lecture reelle des parametres, qui a que fqdnClassName soit renseignee
	protected void localReadVersionNumber() throws ClassNotFoundException {
		org.apache.xerces.impl.Version xversion = new Version();
		final String jarVersion = removeExtraPeriods(xversion.getVersion());
		NSArray jarVersionArray = NSArray.componentsSeparatedByString(jarVersion, ".");
		if (jarVersionArray.count() > 0) {
			versionNumMaj = Integer.parseInt((String) jarVersionArray.objectAtIndex(0));
		}
		if (jarVersionArray.count() > 1) {
			versionNumMin = Integer.parseInt((String) jarVersionArray.objectAtIndex(1));
		}
		if (jarVersionArray.count() > 2) {
			versionNumPatch = Integer.parseInt((String) jarVersionArray.objectAtIndex(2));
		}
		if (jarVersionArray.count() > 3) {
			versionNumBuild = Integer.parseInt((String) jarVersionArray.objectAtIndex(3));
		}
	}

	public void readVersionNumber() throws Exception {

	}

	private static String removeExtraPeriods(String s) {
		StringBuffer result = new StringBuffer(s.length());
		for (int i = 0; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c >= '0' && c <= '9')
				result.append(c);
			else if (c == '.') {
				result.append(c);
			}
		}
		return result.toString();
	}

}
