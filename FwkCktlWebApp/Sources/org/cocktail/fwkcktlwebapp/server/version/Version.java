package org.cocktail.fwkcktlwebapp.server.version;

/**
 * Classe descriptive de la version actuelle de FwkCktlWebApp
 */
public class Version extends A_CktlVersion {

	public String name() {
		return "FwkCktlWebApp";
	}

	public String date() {
		return "18/07/2011";
	}

	public String comment() {
		return "";
	}

	public int versionNumMaj() {
		return 4;
	}

	public int versionNumMin() {
		return 0;
	}

	public int versionNumPatch() {
		return 18;
	}

	public int versionNumBuild() {
		return 0;
	}

	public CktlVersionRequirements[] dependencies() {
		return new CktlVersionRequirements[] {
				new CktlVersionRequirements(new CktlVersionCryptoJcrypt(), null, null, true),
				new CktlVersionRequirements(new CktlVersionCryptoMD5(), null, null, true),
				new CktlVersionRequirements(new CktlVersionWonder(), null, null, true)
		};
	}

}
