package org.cocktail.fwkcktlwebapp.server.version;


/**
 * Classe de controle de la version du user Oracle de DT
 *
 * @author ctarade
 */
public class CktlVersionOracleUserGRHUM extends CktlVersionOracleUser {

  // nom de ce qui est controle (pour affichage dans les logs)
  public String name() {
    return "GRHUM ORACLE user";
  }

  // la table contenant la liste des versions de user
  public String dbUserTableName() {	
    return "GRHUM.DB_VERSION";
  }
  
  // l'attribut contenant la date de la version
  public String dbVersionDateColumnName() {
    return "DBV_DATE";
  }
  
  // l'attribut contenant l'id de la version
  public String dbVersionIdColumnName(){
    return "DBV_LIBELLE";
  }

  // mettre ici les dependances du user (version oracle, autres version users ...)
  public CktlVersionRequirements[] dependencies() {
    return null;
  }
}