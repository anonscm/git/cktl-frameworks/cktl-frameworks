package org.cocktail.fwkcktlwebapp.server.version;

/**
 * Controle de la version du framework Wonder
 */
public class CktlVersionWonder 
	extends A_CktlVersionFrameworkPlist {

	private static final String WO_CFBUNDLESHORTVERSIONSTRING = "CFBundleShortVersionString";
	private static final String WO_FILE = "Info.plist";
	private static final String WO_FRMK = "ERExtensions";
	
	public String name() {
		return "Wonder";
	}

	public String frameworkName() {
		return WO_FRMK;
	}

	public String plistFileName() {
		return WO_FILE;
	}

	public String plistKeyName() {
		return WO_CFBUNDLESHORTVERSIONSTRING;
	}	
 
	public CktlVersionRequirements[] dependencies() {
		return null;
	}

}
