package org.cocktail.fwkcktlwebapp.server;


import org.cocktail.fwkcktlwebapp.common.util.CktlMailMessage;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSData;

/**
 * Cette classe propose implementation des fonctions d'envoi
 * des messages mail. Ces fonctions correspondent a l'utilisation
 * la plus courante d'envoi des messages. L'implementation utilise
 * la classe {@link CktlMailMessage}.
 * 
 * <p>Si les fonctions proposees dans cette classe ne sont pas suffisantes,
 * on peut utiliser le classe {@link CktlMailMessage}.</p>
 * 
 * @see CktlMailMessage
 */
public class CktlMailBus {
	
  /**
   * Le gestionnaire de la configuration de l'application.
   */
  private CktlConfig config;
  
  /**
   * La derniere erreur survenue
   */
  private String errorMessage;
    
  /**
   * Creer un gestionnaire d'envoi des messages mail. L'envoi de messages
   * utilise certaines parametres de configuration.
   * 
   * @param config Le gestionnaire de la configuration de l'application.
   */
  public CktlMailBus(CktlConfig config) {
    this.config = config;
  }

  /**
   * Envoie un message mail avec un fichier attache. Renvoie <code>true</code> ou
   * <code>false</code> en fonction du succes d'envoi. Cet appel utilise le
   * parametre <code>GRHUM_HOST_MAIL</code> qui indique l'adresse du serveur
   * SMTP. Si une erreur survient, alors le message d'erreur est recuperable
   * via la methode {@link #getErrorMessage()}
   *
   * @param from L'adresse mail d'expediteur du message.
   * @param to L'adresse mail du destinataire du message.
   * @param cc Les adresses mail des destinataires auquels la copie du message
   *   sera envoyee. Plusieurs adresses peuvent etre separees par virgule. 
   * @param subject Le sujet de message.
   * @param msgText Le text du message. Le message est epediee en "text/plain".
   * @param filename Le nom du fichier attache au message, si il existe.
   * @param filedata Les contenu du fichier attache, si il existe.
   * 
   * @see #sendMail(String, String, String, String, String)
   */
  public boolean sendMail(String from, String to, String cc,
                          String subject, String msgText,
                          String filename, NSData filedata) {
  	errorMessage = StringCtrl.emptyString();
  	boolean success = false;

    try {
      CktlMailMessage mailBus = getServiceEnvoiMessage();
      if (isNomFichierValide(filename)) {
        mailBus.initMessage(from, to, subject, msgText, 
        		prepareNomFichierJoint(filename), 
        		prepareContenuFichierJoint(filedata));
      } else {
        mailBus.initMessage(from, to, subject, msgText);
      }
      mailBus.addCCs(CktlMailMessage.toArray(cc));
      mailBus.send();
      success = true;
    } catch (Throwable mex) {
     	errorMessage = mex.getLocalizedMessage();
      mex.printStackTrace();
    }
    return success;
  }

  public boolean sendMail(String from, String to, String cc,
                          String subject, String msgText,
                          String[] filenames)
  {
   	errorMessage = StringCtrl.emptyString();
   	boolean success = false;
    try {
      CktlMailMessage mailBus = getServiceEnvoiMessage();
      if ((filenames != null) && (filenames.length > 0)) {
        mailBus.initMessage(from, to, subject, msgText, filenames);
      } else {
        mailBus.initMessage(from, to, subject, msgText);
      }
      mailBus.addCCs(CktlMailMessage.toArray(cc));
      mailBus.send();
      success = true;
    } catch (Exception mex) {
     	errorMessage = mex.getLocalizedMessage();
      mex.printStackTrace();
    }
    return success;
  }

  /**
   * Envoie un message e-mail. Renvoie <code>true</code> ou <code>false</code> en
   * fonction du succes d'envoi.
   * 
   * @param from L'adresse mail d'expediteur du message.
   * @param to L'adresse mail du destinataire du message.
   * @param cc Les adresses mail des destinataires auquels la copie du message
   *   sera envoyee. Plusieurs adresses peuvent etre separees par virgule. 
   * @param subject Le sujet de message.
   * @param msgText Le text du message. Le message est epediee en "text/plain".
   * 
   * @see #sendMail(String, String, String, String, String, String, NSData)
   */
  public boolean sendMail(String from, String to, String cc,
                          String subject, String msgText)
  {
    return sendMail(from, to, cc, subject, msgText, null, null);
  }

  /**
   * Retourne le message d'erreur survenu lors de l'appel a une 
   * des methodes sendMail
   */
  public String getErrorMessage() {
  	return errorMessage;
  }

  protected CktlMailMessage getServiceEnvoiMessage() {
	return new CktlMailMessage(config.stringForKey(CktlConfig.CONFIG_GRHUM_HOST_MAIL_KEY));
  }
	
  private boolean isNomFichierValide(String filename) {
	return StringCtrl.normalize(filename).length() > 0;
  }

  private String[] prepareNomFichierJoint(String filename) {
	String[] fNames = new String[1];
	fNames[0] = StringCtrl.normalize(filename);
	return fNames;
  }
	
  private byte[][] prepareContenuFichierJoint(NSData filedata) {
	byte[][] fData = new byte[1][];
	fData[0] = filedata.bytes(0, filedata.length());
	return fData;
  }

}
