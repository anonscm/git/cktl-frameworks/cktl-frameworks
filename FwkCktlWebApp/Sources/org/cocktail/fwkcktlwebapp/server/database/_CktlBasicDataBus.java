package org.cocktail.fwkcktlwebapp.server.database;


import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.eoaccess.EOAdaptorChannel;
import com.webobjects.eoaccess.EOAdaptorContext;
import com.webobjects.eoaccess.EODatabaseContext;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EOSQLExpression;
import com.webobjects.eoaccess.EOSQLExpressionFactory;
import com.webobjects.eoaccess.EOStoredProcedure;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.eocontrol.EOObjectStore;
import com.webobjects.eocontrol.EOObjectStoreCoordinator;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Cette classe definit une interface de communication
 * avec une base de donnees. Elle fournit les methodes facilitant
 * la recherche et chargement des objets a partir de la base,
 * la creation des objets, l'execution des procedures stockees et
 * d'autres operations.
 * 
 * <p>Cette classe initialise aussi des objets de EOF pour la communication
 * avec une base de donnees (adaptor, adaptor channel, etc.). Ils sont
 * initialises en appelant la methode statique <code>initBus</code>.
 * Elle doit etre appelee un seule fois lors de l'execution d'une application.
 * <i>Cette methode ne devrait pas etre appelee explicitement, car elle est
 * implicitement appelee lors de l'initianlisation de l'application (sa methode
 * <code>initApplication</code>)</i>.
 * 
 * <p>Un objet de cette classe est toujours cree sur un <i>editingContext</i>
 * (par defaut, un editingContext d'une application ou d'une session). Si vous
 * souhaitez gerer les objets dans un EOEditingContext different de ceux proposes
 * par defaut, creez un nouvel objet "bus" avec un nouvel editingContexts.
 * 
 * @see CktlWebApplication
 */
public class _CktlBasicDataBus {
  /**
   * Le nom de la cle correspondant a la description d'une erreur. Cette valeur
   * est utilisee lors de l'execution des procedures stockes. Si l'execution
   * d'une procedure echoue, alors le dictionnaire retourne contient la
   * description d'une erreur. Elle est accessible via la cle <code>ERROR_KEY</code>. 
   */
  public static final String ERROR_KEY = "ERREUR";
  
  /**
   * Le message indique lorsque le message d'erreur original ne peut pas
   * etre detecte correctement. 
   */
  private final String UNKNOWN_ERROR_MESSAGE = "Le message d'erreur inconnu";
  
  /**
   * La transaction par defaut. Elle correspond a l'editing context utilise
   * par defaut par ce data-bus. Toutes les operations de commit, rollback
   * ou terminate sont ignorees pour cette transaction.
   */
  public static final Integer DefaultTransactionId = new Integer(0);
  
  /**
   * L'objet EODatabaseContext de communication avec une base de donnees.
   */
  private static EODatabaseContext databaseContext;
  
  /**
   * L'objet EOObjectStoreCoordinator de communication avec une base de donnees.
   */
  private static EOObjectStoreCoordinator objectStore;
  
  /**
   * L'objet EOEditingContext utilise pour gerer le graphe d'objets metiers. 
   */
  private EOEditingContext editingContext;
  
  /**
   * Le resultat de la derniere execution d'une procedure stockee.
   */
  private NSMutableDictionary executedProcResult;

  /**
   * Indique s'il faut remettre a jour les objets deja charges
   * dans la memoire a partir de la base de donnees. Cette valeur
   * est changee en appelant la methode <code>notRefreshFetchedObjects</code>.
   */
  private boolean refreshObjects;
  
  /**
   * L'objet EOFetchSpecification utilise pour initialiser la connexion
   * avec la base de donnees.
   */
  private static EOFetchSpecification initFetchSpec;

  /**
   * Le dictionnaire des transactions. Il contient les editingContext
   * identifies par un code unique.
   */
  public NSMutableDictionary transactions;
  
  /**
   * Les associations des objets et les qualifiers de recherche. Ce
   * dictionaire peut etre utilise pour memoriser les dernieres
   * conditions de recherche effectuees sur les tables, par exemple.
   */
  private static Hashtable objectQualifiers = new Hashtable();
  
  /**
   * Les associations des objets et les definitions de tri. Ce
   * dictionaire peut etre utilise pour memoriser les dernieres
   * conditions de tri associees aux tables, par exemple.
   */
  private static Hashtable objectSorts = new Hashtable();
  
  /**
   * Les associations des objets et l'utilisation de la clause usesDistinct.
   * Ce dictionaire est utilise pour memoriser les dernieres
   * valeurs de usesDistinct associees aux tables.
   */
  private static Hashtable objectUsesDistincts = new Hashtable();
  
  /**
   * Les associations des objets et les limitations d'objets. A 
   * retourner. Cedictionaire est utilise pour memoriser les dernieres
   * limites de donnees associees aux tables.
   */
  private static Hashtable objectFetchLimits = new Hashtable();
  
  /**
   * Un auditeur des changements d'etats des objets <code>CktlDataBus</code>.
   * Il est informe de debut et de la fin des operations avec une base
   * de donnees.
   */
  private static Listener busListener;

  /**
   * Le compteur interne indiquant l'etat d'occupation de l'objet bus.
   * Les objets n'' est pas occupe si le compteur est egale a 0.
   */
  private int workingStatus;

  /**
   * Indique si le tri des objets doit etre effectue par la base de donnees
   * (<i>false</i>) ou dans la memoire de l'application (<i>true</i>).
   */
  private boolean useMemorySort;
  
  /**
   * Definit si l'application doit etre notifiee (<i>false</i>) lorsqu'un 
   * fetch depasse le nombre maximal fixe, lors de l'appel a une methode
   * utilisant le parametre <code>fetchLimit</code>.
   */
  private boolean promptsAfterFetchLimit;  
  
  /**
   * Si la variable promptsAfterFetchLimit est vraie, alors lors du depassement
   * de la limite fixee par <code>fetchLimit</code> dans les methodes appropriees,
   * alors cette variable sera vraie.
   */
  private boolean fetchLimitWasExceeded;
  
  /**
   * Creer un nouvel objet "bus" qui utilisera l'editingContext donne
   * pour gerer le graphe d'objets metiers.
   * 
   * @param editingContext L'objet gestionnaire du graphe d'objets du
   *     nouveau "bus".
   */
  public _CktlBasicDataBus(EOEditingContext editingContext) {
    if (editingContext == null)
      throw new IllegalArgumentException("Le editing context d'un data bus ne peux pas etre null");
    this.editingContext = editingContext;
    executedProcResult = new NSMutableDictionary();
    refreshObjects = true;
    transactions = new NSMutableDictionary();
    transactions.setObjectForKey(editingContext(), DefaultTransactionId);
    workingStatus = 0;
    useMemorySort = false;
    promptsAfterFetchLimit= false;
    fetchLimitWasExceeded = false;
  }

  /**
   * Memorise une condition de recherche <code>qualifier</code> associee a
   * un objet <code>object</code>.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour memoriser les
   * conditions de recherche associees aux entites representant les tableaux.
   * Cette condition peut ensuite etre restauree a l'aide de la methode
   * <code>restoreQualifier</code>.</p>
   * 
   * @see #restoreQualifier(Object)
   */
  protected synchronized void saveQualifier(Object object, EOQualifier qualifier) {
    if (object != null) {
      if (qualifier == null) objectQualifiers.remove(object);
      else objectQualifiers.put(object, qualifier);
    }
  }

  /**
   * Memorise une definition de tri <code>sort</code> associee a
   * un objet <code>object</code>.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour memoriser le
   * definitions de tri associees aux entites representant les tableaux.
   * Cette definition peut ensuite etre restauree a l'aide de la methode
   * <code>restoreQualifier</code>.</p>
   * 
   * @see #restoreSort(Object)
   */
  protected synchronized void saveSort(Object anObject, NSArray sort) {
    if (anObject != null) {
      if (sort == null) objectSorts.remove(anObject);
      else objectSorts.put(anObject, sort);
    }
  }
  
  /**
   * Memorise une definition de limitation <code>fetchLimit</code> associee a
   * un objet <code>object</code>.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour memoriser le 
   * nombre maximum d'enregistrer a retourne lors d'une requete.
   * Cette definition peut ensuite etre restauree a l'aide de la methode
   * <code>restoreFetchLimit</code>.</p>
   * 
   * @see #restoreFetchLimit(Object)
   */
  protected synchronized void saveFetchLimit(Object anObject, int fetchLimit) {
    if (anObject != null) {
      if (fetchLimit <= 0) objectFetchLimits.remove(anObject);
      else objectFetchLimits.put(anObject, new Integer(fetchLimit));
    }
  }
  
  /**
   * Memorise une definition de distinction <code>usesDistinct</code> associee a
   * un objet <code>object</code>.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour indiquer si
   * les objets ont ete retournes avec le mots clef <code>DISTINCT</code>
   * dans la requete SQL generee. Cette methode est tres utile lors de 
   * l'utilisation de CLOB/BLOB/LONG dans oracle car il est interdit de
   * faire un <code>DISCTINCT</code> dans un <code>SELECT</code>.
   * Cette definition peut ensuite etre restauree a l'aide de la methode
   * <code>restoreUseDistinct</code>.</p>
   * 
   * @see #restoreUsesDistinct(Object)
   */
  protected synchronized void saveUsesDistinct(Object anObject, boolean usesDistinct) {
    if (anObject != null) {
    	objectUsesDistincts.put(anObject, new Boolean(usesDistinct));
    }
  }

  /**
   * Restore une condition de recherche associee a un objet
   * <code>object</code>. Retourne la valeur <i>null</i> si aucune
   * condition n'a ete associee a l'objet.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour retaurer les
   * conditions de recherche associees aux entites representant les tableaux.
   * La condition peut etre ainsi restauree si elle etait precedemment
   * memorisee a l'aide de la methode <code>saveQualifier</code>.
   * 
   * @see #saveQualifier(Object, EOQualifier)
   */
  protected synchronized EOQualifier restoreQualifier(Object anObject) {
    if (anObject == null) return null;
    return (EOQualifier)objectQualifiers.get(anObject);
  }

  /**
   * Restore une definitin de tri associee a un objet <code>object</code>.
   * Retourne la valeur <i>null</i> si aucune definition n'a ete associee
   * a l'objet.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour retaurer les
   * definitions de tri associees aux entites representant les tableaux.
   * La definition peut etre ainsi restauree si elle etait precedemment
   * memorisee a l'aide de la methode <code>saveSort</code>.
   * 
   * @see #saveSort(Object, NSArray)
   */
  protected synchronized NSArray restoreSort(Object anObject) {
    if (anObject == null) return null;
    return (NSArray)objectSorts.get(anObject);
  }
  
  /**
   * Restore une limite maximum d'enregistrements associee a un 
   * objet <code>object</code>. Retourne la valeur <i>null</i>
   *  si aucune definition n'a ete associee a l'objet.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour retaurer le
   * maximum d'objets a fetcher aux entites representant les tableaux.
   * La definition peut etre ainsi restauree si elle etait precedemment
   * memorisee a l'aide de la methode <code>saveFetchLimit</code>.
   * 
   * @see #saveFetchLimit(Object, int)
   */
  protected synchronized int restoreFetchLimit(Object anObject) {
    if (anObject == null) return 0;
    Integer fetchLimit = (Integer)objectFetchLimits.get(anObject);
    return (fetchLimit != null ? fetchLimit.intValue() : 0);
  }
  
  /**
   * Restaure si le fetch associe a un objet <code>object</code> doit 
   * utiliser usesDistinct. Retourne la valeur <i>true</i>
   * si aucune definition n'a ete associee a l'objet.
   * 
   * <p>Par exemple, cette methode peut etre utilisee pour indiquer si
   * les objets ont ete retournes avec le mots clef <code>DISTINCT</code>
   * dans la requete SQL generee. Cette methode est tres utile lors de 
   * l'utilisation de CLOB/BLOB/LONG dans oracle car il est interdit de
   * faire un <code>DISCTINCT</code> dans un <code>SELECT</code>.
   * La definition peut etre ainsi restauree si elle etait precedemment
   * memorisee a l'aide de la methode <code>saveUseDistinct</code>.
   * 
   * @see #saveUsesDistinct(Object, boolean)
   */
  protected synchronized boolean restoreUsesDistinct(Object anObject) {
    if (anObject == null) return true;
    Boolean usesDistinct = (Boolean)objectUsesDistincts.get(anObject);
    return (usesDistinct != null ? usesDistinct.booleanValue() : true);
  }
  
/* ===== Les objets de la communication avec la base de donne ===== */
  
  /**
   * Initialise les objets EOF utilises pour la communication
   * avec une base de donnees. Tous les objets "bus" partagent
   * la meme connexion a une base et les memes objets de son gestion.
   * Cette methode donc ne devrait etre appele qu'une seule fois,
   * typiquement lors de l'initialisation de l'application.
   * 
   * <p><i>La methode</i> est appelee implicitement lors de
   * l'initialisation de l'application et ne devrait pas etre appelle
   * implicitement. 
   * 
   * <p>La procédure d'initialisation des objets EOF nécessite
   * le nom d'une entité d'un des models utilises par l'application.
   * Par défaut, la premiere entite du premier model est prise. 
   */
  public static void initBus(String entityName) {
    initFetchSpec = new EOFetchSpecification(entityName, null, null);
    resetBus();
  }

  /**
   * Initialise les elements du la connexion a la base de donnees.
   * La methode est appellee au moment d'initialisation d'un objet bus.
   * Elle est aussi appellee lorsqu'une operation avec la base de donnees
   * echoue apres la rupture de connexion avec la base.
   */
  private static void resetBus() {
    EOEditingContext eocontext = new EOEditingContext();
    objectStore = (EOObjectStoreCoordinator)eocontext.rootObjectStore();
    databaseContext = (EODatabaseContext)objectStore.objectStoreForFetchSpecification(initFetchSpec);
  }
  
  /**
   * Test si la connexion avec la base de donnees est deja etablie.
   * 
   * @see #initBus(String) initBus()
   */
  public static boolean isDatabaseConnected() {
    return (databaseContext != null);
  }

  /**
   * Retourne l'objet EOAdaptorContext de la gestion d'acces
   * a la base de donnees.
   * 
   * @return EOAdaptorContext
   */
  public static EOAdaptorContext adaptorContext() {
    return databaseContext.adaptorContext();
  }

  /**
   * Retourne l'objet EODatabaseContext de la gestion d'acces
   * a la base de donnees.
   * 
   * @return EODatabaseContext
   */
  public static EODatabaseContext databaseContext() {
    return databaseContext;
  }

  /**
   * Retourne l'objet objectStore de la gestion d'acces
   * a la base de donnes. L'objet reellement retourne est
   * celui EOObjectStoreCoordinator. 
   * 
   * @return EOObjectStoreCoordinator
   */
  public static EOObjectStoreCoordinator objectStore() {
    return objectStore;
  }

  /**
   * Return l'objet <code>EOAdaptorChannel</code> d'une connexion en cours.
   */
  protected static EOAdaptorChannel adaptorChannel() {
    EOAdaptorChannel adaptorChannel = databaseContext.availableChannel().adaptorChannel();
    if (!adaptorChannel.isOpen()) adaptorChannel.openChannel();
    return adaptorChannel;
  }
  
  /**
   * Renvoie la reference vers un objet EOEditingContext.
   * Il est utilise pour gerer les objets metiers par cette objet "bus".
   * 
   * @return EOEditingContext
   */
  public EOEditingContext editingContext() {
    return editingContext;
  }

  /**
   * Definit s'il faut recharger les objets a partir de la base
   * de donnees si ils ont deja ete charges dans la memoire. Par defaut,
   * tous les objets sont remis a jour.
   * 
   * <p>Cette methode doit etre appellee juste avant l'appel d'une des
   * methodes <code>fetchObject</code>, <code>fetchTable</code> ou
   * <code>fetchArray</code>. Une fois une methode <code>fetch</code>
   * appellees, la valeur par defaut est retablie pour les prochaines
   * appels aux <code>fetch</code> (les objets seront a nouveau remis a jour).
   */
  public void setRefreshFetchedObjects(boolean refresh) {
    refreshObjects = refresh;
  }
  
  /**
   * Indique s'il faut recharger les objets a partir de la base
   * de donnees si ils ont deja ete charges dans la memoire. Par defaut,
   * cette methode retourne la valeur <i>true</i> (les objets sont remis
   * a jours).
   */
  protected boolean refreshFetchedObjects() {
    return refreshObjects;
  }

  /**
   * Definit si le tri des objets doit etre effectue par la base de donnees
   * (<i>false</i>) ou dans la memoire de l'application (<i>false</i>).
   */
  public void setUseMemorySort(boolean use) {
    useMemorySort = use;
  }

  /**
   * Definit si l'application doit etre notifiee (<i>false</i>) lorsqu'un 
   * fetch depasse le nombre maximal fixe, lors de l'appel a une methode
   * utilisant le parametre <code>fetchLimit</code>. Si la valeur est vraie,
   * alors l'editing context capte l'evenement et met a jour la variable
   * fetchLimitWasExceeded, disponible par la methode getFetchLimitWasExceeded()
   */
  public void setPromptsAfterFetchLimit(boolean use) {
    promptsAfterFetchLimit = use;
    if (promptsAfterFetchLimit)
      editingContext.setMessageHandler(this);
  }
  
  /**
   * Permet de savoir si la limite maximum d'enregistrements retournes a ete 
   * atteinte. Cette valeur n'a de sens que si setPromptsAfterFetchLimit est
   * a true et que l'utilisateur utilise un fetch avec le parametre 
   * <code>fetchLimit</code> > 0.
   * 
   * Cette variable est remise a <code>false</code> avant chaque fetch.
   */
  public boolean getFetchLimitWasExceeded() {
    return fetchLimitWasExceeded;
  }
  
  /**
   * Methode surchargee de EOEditingContext qui permet de receptionner les
   * notifications en cas de depassement de fetchLimit. Cette version n'est
   * utilisee que si setPromptsAfterFetchLimit(true) est definit. lors de son
   * appel (fait automatiquement par WO), alors la methode getFetchLimitWasExceeded()
   * retournera true.
   */
  public boolean editingContextShouldContinueFetching(EOEditingContext context, int count, 
      int originalLimit, EOObjectStore objectStore) {
    fetchLimitWasExceeded = true;
    return false;
  } 
  
  /**
   * Creer un nouvel qualifier correspondant a la condition donnee dans
   * la chaine de caracteres <code>condition</code>. C'est un raccourcie
   * de l'expression :
   * <center><code>EOQualifier.qualifierWithQualifierFormat(condition, null);</code></center>
   * 
   * <p>Si la chaine condition est <i>null</i> ou vide, alors la methode
   * retourne <i>null</i>.</p>
   */
  public static EOQualifier newCondition(String condition) {
    if ((condition == null) || (condition.length() == 0)) return null;
    return EOQualifier.qualifierWithQualifierFormat(condition, null);
  }
  
  /**
   * Creer un nouvel qualifier correspondant a la condition donnee dans
   * la chaine de caracteres <code>condition</code>. C'est un raccourcie
   * de l'expression :
   * <center><code>EOQualifier.qualifierWithQualifierFormat(condition, arguments);</code></center>
   */
  public static EOQualifier newCondition(String condition, NSArray arguments) {
    return EOQualifier.qualifierWithQualifierFormat(condition, arguments);
  }

  /**
   * Execute une requete definie par la <code>condition</code>.
   * Retourne un tableau des objets de sont resultat.
   * 
   * @param nomTable Le nom de la table dont les objets sont selectionnes.
   * @param condition La condition de selection des objets. Si la condition
   * est <i>null</i>, tous les objets sont selectionnes.
   * @param arguments Le tableau des parametres pour la condition de selection.
   *   Cette valeur peut etre <i>null</i>.
   * @param sort Le tableau des definitions de tri des objets. Cette valeur
   *   peut etre <i>null</i>. 
   */
  public NSArray fetchArray(String nomTable, String condition,
                            NSArray arguments, NSArray sort)
  {
    EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(condition, arguments);
    return fetchArray(editingContext, nomTable, qualifier, sort, true, refreshObjects);
  }

  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat.
   */
  public NSArray fetchArray(String nomTable,
                            EOQualifier qualifier,
                            NSArray sort)
  {
    return fetchArray(editingContext, nomTable, qualifier, sort, true, refreshObjects);
  }

  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat.
   */
  public NSArray fetchArray(EOEditingContext econtext, String nomTable,
                            EOQualifier qualifier, NSArray sort)
  {
    return fetchArray(econtext, nomTable, qualifier, sort, true, refreshObjects);
  }


  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat.
   */
  public NSArray fetchArray(EOEditingContext econtext, String nomTable,
                            EOQualifier qualifier, NSArray sort,
                            boolean usesDistinct)
  {
     return fetchArray(econtext, nomTable, qualifier, sort, usesDistinct, refreshObjects);
  }
  
  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat. Le nombre d'enregistrements
   * est limite a fetchLimit
   */
  public NSArray fetchArray(EOEditingContext econtext, String nomTable,
                            EOQualifier qualifier, NSArray sort,
                            boolean usesDistinct, int fetchLimit)
  {
     return fetchArray(econtext, nomTable, qualifier, sort, usesDistinct, refreshObjects, fetchLimit);
  }

  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat.
   */
  public NSArray fetchArray(EOEditingContext econtext, String nomTable,
                            EOQualifier qualifier, NSArray sort,
                            boolean usesDistinct, boolean forceRefresh)
  {
    return fetchArray(econtext, nomTable, qualifier, sort, usesDistinct, forceRefresh, 0);
  }

  /**
   * Execute une requete definie par la condition <code>qualifier</code>.
   * Retourne un tableau des objets de sont resultat. Le nombre d'enregistrements
   * maximum retournes est limite au nombre <code>fetchLimit</code>.
   */
  public NSArray fetchArray(EOEditingContext econtext, String nomTable,
                            EOQualifier qualifier, NSArray sort,
                            boolean usesDistinct, boolean forceRefresh,
                            int fetchLimit)
  {
    NSArray objects;
    startsWorking();
    synchronized(econtext) {
      try {
        EOFetchSpecification myFetch = new EOFetchSpecification(nomTable, qualifier, sort);
        myFetch.setRefreshesRefetchedObjects(forceRefresh);
        myFetch.setUsesDistinct(usesDistinct);
        fetchLimitWasExceeded = false;
        if (fetchLimit > 0) {
          myFetch.setFetchLimit(fetchLimit);
          myFetch.setPromptsAfterFetchLimit(promptsAfterFetchLimit);
        }
        // econtext.lock();
        objects = econtext.objectsWithFetchSpecification(myFetch);
        // econtext.unlock();
        // On refait le sort dans la memoire, si c'est necessaire
        if (useMemorySort && (sort != null) && (objects.count() > 0))
          objects = EOSortOrdering.sortedArrayUsingKeyOrderArray(objects, sort);
      } catch(Throwable ex) {
        throwError(ex);
        objects = new NSArray();
      }
      refreshObjects = true;
    }
    stopsWorking();
    return objects;
  }
    
  /**
   * Retourne l'objet selectionne dans la base suivant les conditions
   * donnees. Si plusieurs objects sont selectionnes, le premier est renvoie
   * comme resultat.
   * 
   * @param nomTable Le nom de l'entite dans laquelle l'objet est selectionne.
   * @param condition Les conditions que doit satisfaire l'objet selectionne.
   * @param arguments Les arguments pour la condition des selection.
   * 
   * @return L'objet selectionne ou <i>null</i> si aucun objet n'a ete trouve.
   */
  public EOEnterpriseObject fetchObject(String nomTable,
                                        String condition, NSArray arguments)
 {
    EOQualifier qualifier = EOQualifier.qualifierWithQualifierFormat(condition, arguments);
    return fetchObject(nomTable, qualifier);
  }
 
  /**
   * Retourne l'objet selectionne dans la base suivant les conditions
   * donnees. Si plusieurs objects sont selectionnes, le premier est renvoie
   * comme resultat.
   * 
   * @param nomTable Le nom de l'entite dans laquelle l'objet est selectionne.
   * @param qualifier Les conditions que doit satisfaire l'objet selectionne.
   * 
   * @return L'objet selectionne ou <i>null</i> si aucun objet n'a ete trouve.
   */
  public EOEnterpriseObject fetchObject(String nomTable, EOQualifier qualifier) {
    return fetchObject(editingContext(), nomTable, qualifier);
  }
  
  /**
   * Retourne l'objet selectionne dans la base suivant les conditions
   * donnees. Si plusieurs objects sont selectionnes, le premier est renvoie
   * comme resultat. L'objet est selectionne dans l'editing context indique.
   *
   * @param ec Le editing context dans lequel l'objets sont selectionnes. 
   * @param nomTable Le nom de l'entite dans laquelle l'objet est selectionne.
   * @param qualifier La condition que l'objet selectionne doit satisfaire.
   * 
   * @return L'objet selectionne ou <i>null</i> si aucun objet n'a ete trouve.
   */
  public EOEnterpriseObject fetchObject(EOEditingContext ec,
                                        String nomTable,
                                        EOQualifier qualifier)
  {
    NSArray objects = fetchArray(ec, nomTable, qualifier, null, true);
    if (objects.count() > 0) {
      return (EOEnterpriseObject)objects.objectAtIndex(0);    
    }
    return null;
  }

  /**
   * Commence une transaction avec la base de donnees. Une suite des appels
   * aux procedures stockees sera interpretee comme une seule transaction.
   * 
   * <p>Utilisez les methodes <code>commitTransaction</code> ou
   * <code>rollbackTransaction</code> pour finir la transaction.</p>
   * 
   * <i>Cette methode permet de gerer les transaction avec la base de donnees.
   * Utilisez la methode beginECTransaction, pour avoir l'effet des
   * transactions au niveau de graphe d'objets.</i>
   * 
   * @see #commitTransaction()
   * @see #rollbackTransaction()
   * @see #beginECTransaction()
   */
  public void beginTransaction() {
    for(int i=0; (i<10 && adaptorContext().hasOpenTransaction()); i++) {
      try { wait(1000); } catch(InterruptedException ex) { }
    }
    if (!adaptorContext().hasOpenTransaction())
      adaptorContext().beginTransaction();
  }

  /**
   * Annule la transaction avec la base de donnees. Toutes les modifications
   * apportees par les procedures stockees sont annulees.
   * 
   * <p>L'appel de cette methode doit etre precedee par l'appel a
   * <code>beginTransaction</code>.</p>
   * 
   * @see #beginTransaction()
   */
  public void rollbackTransaction() {
    adaptorContext().rollbackTransaction();
  }

  /**
   * Valide et termine une transaction avec la base de donnees. Toutes les
   * modifications apportees par les procedure stockees sont effectivement
   * enregistrees.
   * 
   * <p>L'appel de cette methode doit etre precedee par l'appel a
   * <code>beginTransaction</code>.</p>
   * 
   * @see #beginTransaction()
   */
  public void commitTransaction() {
    adaptorContext().commitTransaction();
  }

  /**
   * Execute une procedure stockee. Ses parametres sont sous forme d'un
   * dictionnaire. Le resultat indique le succes d'execution de la procedure.
   * L'appel suivant de la methode <code>executedProcResult</code> renvoie le
   * resultat de l'execution.
   */
  public boolean executeProcedure(String procedure, NSDictionary dicoData) {
    return executeProcedure(executedProcResult, procedure, dicoData);
  }

  /**
   * Execute une procedure stockee. Ses parametres sont sous forme d'un
   * dictionnaire. Le resultat indique le succes d'execution de la procedure.
   * 
   * <p>Le resultat de l'execution de la procedure est enregistre dans le
   * dictionnaire <code>procResult</code>. Une copie du resultat est egalement
   * enregistree dans le dictionnaire retourne par la methode
   * <code>executedProcResult</code>.</p>
   * 
   * @see #executeProcedure(String, NSDictionary)
   */
  public boolean executeProcedure(NSMutableDictionary procResult,
                                  String procedure, NSDictionary dicoData)
  {
    boolean noError = true;
    startsWorking();
    EOAdaptorChannel adaptorChannel = adaptorChannel();
    executedProcResult.removeAllObjects();
    try {
      EOStoredProcedure myProcedure = EOModelGroup.defaultGroup().storedProcedureNamed(procedure);
      adaptorChannel.executeStoredProcedure(myProcedure, dicoData);
      executedProcResult.addEntriesFromDictionary(adaptorChannel.returnValuesForLastStoredProcedureInvocation());
    } catch (Throwable e) {
      e.printStackTrace();
      noError = false;
      // On memorise l'erreur
      String exMessage = CktlLog.getMessageForException(e);
      if (exMessage == null) exMessage = UNKNOWN_ERROR_MESSAGE;
      executedProcResult.setObjectForKey(exMessage, ERROR_KEY);
    }
    // On fait une copie du resultat
    if ((procResult != null) && (procResult != executedProcResult))
      procResult.addEntriesFromDictionary(executedProcResult);
    stopsWorking();
    return noError;
  }

  /**
   * Return le resultat de la derniere execution de la procedure stockee sous
   * forme d'un dictionnaire. Dans le cas d'une echec, le dictionaire
   * contient le message d'erreur accessible via la cle dont la valeur est
   * disponible dans la variable statique <code>ERROR_KEY</code>.
   * 
   * @see #ERROR_KEY
   */
  public NSDictionary executedProcResult() {
    return executedProcResult;
  }

  /**
   * Execute une requete SQL. La requete est envoyee directement dans la base
   * de donnees, sans passer par le modele de donnees de l'application.
   * Le parametre "attributes" indique l'ordre des attributs de resultat.
   * Le resultat d'execution de la procedure est renvoie sous forme
   * d'un dictionnaire.
   */
  public NSDictionary executeSQLQuery(String sentence, NSArray attributes) {
    NSDictionary result;

    startsWorking();
    // Version pour WO 5.x
    try {
      EOSQLExpressionFactory sqlFactory = adaptorContext().adaptor().expressionFactory();
      EOSQLExpression sql = sqlFactory.expressionForString(sentence);
      EOAdaptorChannel adaptorChannel = adaptorChannel();
      adaptorChannel.evaluateExpression(sql);
      adaptorChannel.setAttributesToFetch(attributes);
      result = adaptorChannel.fetchRow();
      adaptorChannel.closeChannel();
    } catch(Exception ex) {
      result = null;
      throwError(ex);
    }
    stopsWorking();
    return result;
  }

  /**
   * Cree et renvoie un nouvel objet dans l'entite en utilisant le
   * editing context donne.
   */
/*  public EOEnterpriseObject newObjectInEntity(String entityName, EOEditingContext econtext) {
    EOEnterpriseObject anObject;
    EOClassDescription aClassDesc = EOClassDescription.classDescriptionForEntityName(entityName);
    if (aClassDesc == null) return null;
    anObject = (EOEnterpriseObject) aClassDesc.createInstanceWithEditingContext(econtext, null);
    if (anObject == null) return null;
    econtext.insertObject(anObject);
    return anObject;
}*/
  public EOEnterpriseObject newObjectInEntity(String entityName, EOEditingContext econtext) {
    return EOUtilities.createAndInsertInstance(econtext, entityName);
  }
  
  /**
   * Supprime dans une entite <code>tableName</code> tous les les objets
   * correspondant a la condition <code>condition</code>.
   * 
   * @param transactionId L'identifiant d'une transaction ou <i>null</i> si
   *   aucune transaction n'est utilisee.
   * @param tableName Le nom de l'entite representant la table dans une base
   *   de donnees.
   * @param condition La condition que les objets supprimees doivent respecter.
   * 
   * @return Le nombre d'objets supprimes ou -1 si une erreur a survenue.
   * 
   * @see #beginECTransaction()
   */
  public int deleteFromTable(Integer transactionId,
                             String tableName,
                             EOQualifier condition)
  {
    int count = 0;
    startsWorking();
    EOEditingContext ec = econtextForTransaction(transactionId, true);
    try {
      NSArray objects = fetchArray(ec, tableName, condition, null);
      for(; count<objects.count(); count++)
        ec.deleteObject((EOEnterpriseObject)objects.objectAtIndex(count));
      if (transactionId == null) ec.saveChanges();
    } catch(Throwable ex) {
      count = -1;
      throwError(ex);
    }
    stopsWorking();
    return count;
  }

  /**
   * Renvoie la nouvelle valeur de la cle primaire pour une entite.
   * L'entite avec ce nom doit etre presente dans le model de donnees.
   * Cette methode utilise le result de la methode
   * <code>getSequenceNameForEntity</code>.
   * 
   * @see #getSequenceNameForEntity(EOEntity)
   */
  /*
  public synchronized Number getNewPrimaryKeyForEntity(String entityName) {
    EOEntity entity = EOModelGroup.defaultGroup().entityNamed(entityName);
    CktlLog.trace("Entity : "+entity);
    if (entity == null) return null;
    String sequenceName = getSequenceNameForEntity(entity);
    CktlLog.trace("Sequence name : "+sequenceName);
    if (sequenceName == null) return null;
    EOAttribute attribute = (EOAttribute)entity.primaryKeyAttributes().objectAtIndex(0);
    CktlLog.trace("Attribute : "+sequenceName);
    NSDictionary pKey = executeSQLQuery("SELECT "+sequenceName+".NEXTVAL FROM dual", new NSArray(attribute));
    CktlLog.trace("Query result : "+pKey);
    if (pKey == null) return null;
    Number pKeyValue = (Number)pKey.objectForKey(attribute.name());
    CktlLog.trace("PK Value : "+pKeyValue);
    // if (pKeyValue == null) return null;
    return pKeyValue;
  }
  */
  public synchronized Number getNewPrimaryKeyForEntity(String entityName) {
    CktlLog.trace("PK for entity : "+entityName);
    EOEntity entity = EOModelGroup.defaultGroup().entityNamed(entityName);
    if (entity == null) return null;
    String sequenceName = getSequenceNameForEntity(entity);
    if (sequenceName == null) return null;
    String sqlExpression = "SELECT "+sequenceName+".NEXTVAL FROM dual";
    CktlLog.trace("SQL Expression : "+sqlExpression);
    NSArray rawRows = EOUtilities.rawRowsForSQL(editingContext(),
                                                entity.model().name(),
                                                sqlExpression);
    if ((rawRows == null) || (rawRows.count() == 0)) return null;
    NSDictionary pKey = (NSDictionary)rawRows.objectAtIndex(0);
    if (pKey == null) return null;
    Number pKeyValue = null;
    java.util.Enumeration values = pKey.objectEnumerator();
    if (values.hasMoreElements())
      pKeyValue = (Number)values.nextElement();
    CktlLog.trace("PK Value : "+pKeyValue);
    return pKeyValue;
  }

  /**
   * Renvoie le nom de la sequence correspondant a une entite donnee.
   * La entite doit etre incluse dans le model de donnees.
   */
  public String getSequenceNameForEntity(EOEntity entity) {
    return null;
  }
  
  /**
   * Retourne l'objet correspondant a la valeur <code>null</code> dans
   * l'implementation actuelle de EOF.
   * 
   * <p><i>Cette methode est introduite afin de garder la compatibilite de code
   * entre les versions 4.5.x et 5.x de WebObjects.</i></p> 
   */
  public static Object nullValue() {
    // La version pour WO 5.x
    return EOKeyValueCoding.NullValue;
  }
  
  /**
   * 
   */
  public static Object valueIfNull(Object value) {
    if (value == null) return nullValue();
    return value;
  }

  /**
   * 
   */  
  public static boolean isNullValue(Object value) {
    return ((value == null) || (value == nullValue()));
  }
  
  /**
   * Cree une nouvelle trasaction "editingContext". Retourne l'identifiant
   * de la transaction cree.
   * 
   * <p>Une transaction creee correspond a un objet
   * <code>EOEditingContext</code> dans lequel les modifitions d'objets
   * sont enregistrees et sauvegardees dans un base de donnees. Le editing
   * context correspondant peut etre obtenue via l'appel a une methode
   * <code>econtextForTransaction</code>.</p>
   * 
   * <p><i>Cette methode permet de gerer les transactions au niveau
   * d'un editing context. Pour gerer les transaction au niveau de la
   * base de donnees directement, il faut tiliser la methode
   * <code>beginTransaction</code></i>.</p>
   * 
   * @see #rollbackECTrancsacition(Integer)
   * @see #commitECTrancsacition(Integer)
   * @see #econtextForTransaction(Integer)
   * @see #econtextForTransaction(Integer, boolean)
   * @see #beginTransaction()
   */
  public Integer beginECTransaction() {
    Integer id;
    synchronized(transactions) {
      NSArray allKeys = transactions.allKeys();
      for (int i=allKeys.count(); ;i++) {
        id = new Integer(i);
        if (!allKeys.containsObject(id)) break;
      }
      transactions.setObjectForKey(new EOEditingContext(), id);
    }
    return id;
  }
  
  /**
   * Annule la transaction commencee dans le graphe d'objets. Cette appel
   * annunle les modifications apportees au graphe d'objet et supprime la
   * reference vers l'objet <code>EOEditingContext</code> correspondant.
   * 
   * <p>Cet appel termine automatiquement la transaction et toutes les
   * futures modifications des objets obtenus dans le cadre de cette transaction
   * sont interdites.</p>
   * 
   * @param transactionId L'identifiant de la transaction. Il est obtenu suite a l'appel
   * a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #commitECTrancsacition(Integer)
   * @see #rollbackECTrancsacition(Integer, boolean)
   */
  public void rollbackECTrancsacition(Integer transactionId) {
    rollbackECTrancsacition(transactionId, true);
  }
    
  /**
   * Annule la transaction commencee dans le graphe d'objets. Cette appel
   * annunle les modifications apportees au graphe d'objet et supprime la
   * reference vers l'objet <code>EOEditingContext</code> correspondant.
   * 
   * @param transactionId L'identifiant de la transaction. Il est obtenu
   *   suite a l'appel a la methode <code>beginECTransaction</code>.
   * @param terminate Indique si la transaction doit etre definitivement
   *   terminee. Si ce parametre indique <i>true</i>, alors toutes les 
   *   futures operations sur les objets obtenus dans le cadre de cette
   *   transaction sont interdites.
   * 
   * @see #beginECTransaction()
   * @see #rollbackECTrancsacition(Integer)
   * @see #commitECTrancsacition(Integer, boolean)
   */
  public void rollbackECTrancsacition(Integer transactionId, boolean terminate) {
    synchronized(transactions) {
      EOEditingContext ec = econtextForTransaction(transactionId);
      if (ec != null) ec.revert();
      if (terminate) terminateTransaction(transactionId);
    }
  }
  
  /**
   * Valide la transaction commencee dans le graphe d'objets. Cette appel
   * repercute dans la base de donnees toutes les modifications apportees
   * sur le graphe d'objets.
   * 
   * <p>Retourne <i>true</i> dans le cas de succes et <i>false</i> dans le
   * cas contraire. Dans le cas de succes la reference vers l'objet
   * <code>EOEditingContext</code> correspondant est supprimee et la
   * transaction est definitivement terminee.  
   *  
   * @param transactionId L'identifiant de la transaction. Il est obtenu
   *   suite a l'appel a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #commitECTrancsacition(Integer, boolean)
   * @see #rollbackECTrancsacition(Integer)
   */
  public boolean commitECTrancsacition(Integer transactionId) {
    return commitECTrancsacition(transactionId, true);
  }

  /**
   * Valide la transaction commencee dans le graphe d'objets. Cet appel
   * repercute dans la base de donnees toutes les modifications apportees
   * sur le graphe d'objets. Le parametre <code>terminate</code> indique si les
   * la transaction doit etre termine et donc interdite aux futures operations
   * de mise a jour.
   * 
   * <p>Retourne <i>true</i> dans le cas de succes et <i>false</i> dans le
   * cas contraire. Dans le cas de succes la reference vers l'objet
   * <code>EOEditingContext</code> correspondant est supprimee.  
   *  
   * @param transactionId L'identifiant de la transaction. Il est obtenu suite
   *   a l'appel a la methode <code>beginECTransaction</code>.
   * @param terminate Indique si la transaction doit etre definitivement
   *   terminee. Si la valeur du parametre est <i>true</i>, alors les
   *   references vers les objets obtenus dans le cadre de cette transaction
   *   sont supprimees. 
   * 
   * @see #beginECTransaction()
   * @see #commitECTrancsacition(Integer)
   * @see #rollbackECTrancsacition(Integer, boolean)
   */
  public boolean commitECTrancsacition(Integer transactionId, boolean terminate) {
    boolean result = false;
    startsWorking();
    synchronized(transactions) {
      EOEditingContext ec = econtextForTransaction(transactionId);
      try {
        if (ec != null) ec.saveChanges();
        result = true;
      } catch(Throwable ex) {
        if (ec != null) ec.revert();
        throwError(ex);
      }
      if (terminate) terminateTransaction(transactionId);
    }
    stopsWorking();
    return result;
  }

  /**
   * Enleve la transaction avec l'identifiant <code>transactionId</code> de
   * la liste des transactions ouvertes.
   * 
   * <p>Cet appel libere toutes les ressources liees a la transaction. Apres
   * cet appel, les operations sur les objets obtenus dans le cadre de la
   * transaction ne sont plus autorisees et peuvent produire des erreurs.</p>
   * 
   * <p>Attention, la transaction par defaut ne peut jamais etre terminee.
   * Dans ce cas, l'appel a cette methode est ignore.</p>
   */
  public void terminateTransaction(Integer transactionId) {
    if (transactionId != DefaultTransactionId)
      transactions.removeObjectForKey(transactionId);
  }
  
  /**
   * Retourne l'objet <code>EOEditingContext</code> correspondant a
   * la transaction avec l'identifiant <code>id</code>. Il est
   * cree suite a l'appel a la methode <code>beginECTransaction</code>.
   * La valeur <i>null</i> est retournee si la transactio n'existe pas.
   * 
   * @param id L'identifiant de la transaction. Il est obtenu suite a l'appel
   * a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #econtextForTransaction(Integer, boolean) 
   */
  protected EOEditingContext econtextForTransaction(Integer id) {
    return (EOEditingContext)transactions.objectForKey(id);
  }

  /**
   * Retourne l'objet <code>EOEditingContext</code> correspondant a
   * la transaction avec l'identifiant <code>id</code>. Il est
   * cree suite a l'appel a la methode <code>beginECTransaction</code>.
   * 
   * <p>Si la valeur createIfNull est egale a <i>true</i> et la transactio
   * n'existe pas, un nouveau objet <code>EOEditingContext</code> est cree.
   * <i>Attention, ceci ne cree pas d'une nouvelle transaction !</i></p>
   * 
   * @param id L'identifiant de la transaction. Il est obtenu suite a l'appel
   * a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #econtextForTransaction(Integer)
   */
  protected EOEditingContext econtextForTransaction(Integer id, boolean createIfNull) {
    EOEditingContext ec = null;
    if (id != null) ec = (EOEditingContext)transactions.objectForKey(id);
    if ((ec == null) && createIfNull) ec = new EOEditingContext();
    return ec;
  }
  
  /**
   * Enregistre un objet auditeur des changement des etats des objets
   * <code>CktlDataBus</code>. Il ne peut exister qu'un seule auditeur
   * dans l'application. Different objets de <code>CktlDataBus</code> et de
   * ses sous-classes partagent le meme auditeur.
   * 
   * @param listener Un auditeur implementant l'interface
   * <code>_CktlBasicDataBus.Listener</code>.
   * 
   * @see #removeDataBusListener()
   * @see Listener
   */
  public static void setDataBusListener(Listener listener) {
    busListener = listener;
  }
  
  /**
   * Supprime la reference vers un auditeur des changements d'etats
   * des objets <code>_CktlBasicDataBus</code> precedament enregistre.
   * 
   * @see #setDataBusListener(_CktlBasicDataBus.Listener)
   * @see Listener
   */
  public static void removeDataBusListener() {
    busListener = null;
  }
  
  /**
   * Informe un auditeur qu'une operation avec la base de donnees
   * va commencer. 
   * 
   * @see Listener#startsWorking()
   */
  protected void startsWorking() {
    workingStatus++;
    if (busListener != null) busListener.startsWorking();
  }
  
  /**
   * Informe un auditeur qu'une operation avec la base de donnees
   * vient juste de s'est terminee.
   * 
   * @see Listener#stopsWorking()
   */
  protected void stopsWorking() {
    workingStatus--;
    if ((busListener != null) && (workingStatus == 0))
      busListener.stopsWorking();
  }
  
  /**
   * Indique qu'une erreur c'est produit lors de l'execution d'une operation
   * avec la base de donnees.
   * 
   * @see Listener#throwsError(Throwable, String)
   */
  public void throwError(Throwable ex) {
    if (ex != null) {
      ex.printStackTrace();
      if (busListener != null)
        busListener.throwsError(ex, CktlLog.getMessageForException(ex));
    }
  }
  
  /**
   * Cette interface propose des methodes qui indiquent le changement
   * d'etats des objets <code>CktlDataBus</code>. Un objet qui ecoute les
   * evenements d'un "bus" peut s'enregistrer a l'aide des methodes
   * <code>setDataBusListener</code> et <code>removeDataBusListener</code>.
   * 
   * @see _CktlBasicDataBus#setDataBusListener(_CktlBasicDataBus.Listener)
   * @see _CktlBasicDataBus#removeDataBusListener()
   */
  public interface Listener {
    /**
     * Indique qu'un objet <code>CktlDataBus</code> commence une operation
     * avec une base de donnees. Il passe donc a l'etat <i>occupe</i>.
     */
    public void startsWorking();
    
    /**
     * Indique qu'un objet <code>CktlDataBus</code> a termine une operation
     * avec une base de donnees. Il passe donc a l'etat <i>libre</i>.
     *
     */
    public void stopsWorking();
    
    /**
     * Indique qu'une erreur c'est produit lors de l'execution d'une operation
     * avec la base de donnees.
     * 
     * <p>Cette methode est appelee lorsqu'une exception est levee pendant
     * l'execution d'une operation et elle est renvoye dans le parametre
     * <code>ex</code>. Un message supplementaire peut etre donnee dans le
     * parametre <code>message</code>.</p> 
     */
    public void throwsError(Throwable ex, String message);
  }
}
