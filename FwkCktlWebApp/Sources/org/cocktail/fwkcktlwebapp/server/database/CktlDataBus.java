package org.cocktail.fwkcktlwebapp.server.database;


import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Cette classe definit une interface de communication
 * avec une base de donnees. Elle fournit les methodes facilitant
 * la recherche et chargement des objets a partir de la base,
 * la creation des objets, l'execution des procedures stockees et
 * d'autres operations.
 * 
 * <p>Cette classe initialise aussi des objets de EOF pour la communication
 * avec une base de donnees (adaptor, adaptor channel, etc.). Ils sont
 * initialises en appelant la methode statique <code>initBus</code>.
 * Elle doit etre appelee un seule fois lors de l'execution d'une application.
 * <i>Cette methode ne devrait pas etre appelee explicitement, car elle est
 * implicitement appelee lors de l'initianlisation de l'application (sa methode
 * <code>initApplication</code>)</i>.
 * 
 * <p>Un objet de cette classe est toujours cree sur un <i>editingContext</i>
 * (par defaut, un editingContext d'une application ou d'une session). Si vous
 * souhaitez gerer les objets dans un EOEditingContext different de ceux proposes
 * par defaut, creez un nouvel objet "bus" avec un nouvel editingContexts.
 * 
 * @see CktlWebApplication
 */
public class CktlDataBus extends _CktlBasicDataBus {
  /**
   * Creer un nouvel objet "bus" qui utilisera l'editingContext donne
   * pour gerer le graphe d'objets metiers.
   * 
   * @param editingContext L'objet gestionnaire du graphe d'objets du
   *     nouveau "bus".
   */
  public CktlDataBus(EOEditingContext editingContext) {
    super(editingContext);
  }

/* ===== Les objets de la communication avec la base de donne ===== */
  /**
   * Retourne le editing context utilise par la table donnee. Si le
   * editing context ne peut pas etre determine, celui par defaut
   * est retourne (methode <code>editingContext</code>).
   * 
   * @see #editingContext()
   */
  protected EOEditingContext econtextForTable(WODisplayGroup table) {
    EOEditingContext econtext = null;
    if (table.dataSource() != null) {
      econtext = table.dataSource().editingContext();
    }
    if (econtext == null) econtext = editingContext();
    return econtext;
  }
  
  /**
   * Efface le contenu actuel d'un WODisplayGroup.
   * L'appel a cette methode n'affecte pas le contenu de la base de donnees, 
   * les objets ne sont pas effectivement effacees dans la base. Toutes
   * les modifications des objets dans l'editing context sont annulees.
   * 
   * @param aTable Le display group dont le contenu sera efface.
   */
  public void clearTable(WODisplayGroup aTable) {
    econtextForTable(aTable).revert();
    aTable.setObjectArray(null);
  }
  
  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param condition La condition de selection des objets dans l'entite.
   * @param arguments Les arguments pour la condition de selection des objets.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   */
  public void fetchTable(WODisplayGroup table,
                         String condition, NSArray arguments,
                         NSArray sort,  boolean selectFirst)
  {
    fetchTable(table,
               table.dataSource().classDescriptionForObjects().entityName(),
               EOQualifier.qualifierWithQualifierFormat(condition, arguments),
               sort, selectFirst, true);
  }

  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   */
  public void fetchTable(WODisplayGroup table,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst)
  {
    fetchTable(table,
               table.dataSource().classDescriptionForObjects().entityName(),
               qualifier, sort, selectFirst, true);
  }

  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * La methode permet de selectionner les objets dans un editingContext different
   * de celui de l'objet "bus". Elle permet aussi indiquer si la selection doit
   * etre unique. Cette condition peut etre utiliser pour selectionner les objets
   * dans les entites ayant les attributs sur lesquels on ne peut pas definir
   * la condition <code>UNIQUE</code>.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   * @param usesDistinct Indique si la selection des objets doit etre unique
   *   (sans doublons).
   */
  public void fetchTable(WODisplayGroup table, String nomTable,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst, boolean usesDistinct)
  {
    saveQualifier(table, qualifier);
    saveSort(table, sort);
    table.setSelectsFirstObjectAfterFetch(selectFirst);
    table.setObjectArray(fetchArray(econtextForTable(table),
                                    nomTable, qualifier, sort, usesDistinct));
  }

  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param condition La condition de selection des objets dans l'entite.
   * @param arguments Les arguments pour la condition de selection des objets.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le premier
   *   objet de toute la liste d'objets.
   * @param fetchLimit Le nombre maximum d'enregistrements a retourner
   */
  public void fetchTable(WODisplayGroup table,
                         String condition, NSArray arguments,
                         NSArray sort,  boolean selectFirst,
                         int fetchLimit)
  {
    fetchTable(table,
               table.dataSource().classDescriptionForObjects().entityName(),
               EOQualifier.qualifierWithQualifierFormat(condition, arguments),
               sort, selectFirst, true, fetchLimit);
  }

  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   * @param usesDistinct Indique s'il faut utiliser la clause DISTINCT dans le fetch
   * @param fetchLimit Le nombre maximum d'enregistrements a retourner
   */
  public void fetchTable(WODisplayGroup table,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst, boolean usesDistinct, int fetchLimit)
  {
    fetchTable(table,
               table.dataSource().classDescriptionForObjects().entityName(),
               qualifier, sort, selectFirst, usesDistinct, fetchLimit);
  }

  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   * @param fetchLimit Le nombre maximum d'enregistrements a retourner
   */
  public void fetchTable(WODisplayGroup table,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst, int fetchLimit)
  {
    fetchTable(table,
               table.dataSource().classDescriptionForObjects().entityName(),
               qualifier, sort, selectFirst, fetchLimit);
  }
  
  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * La methode permet de selectionner les objets dans un editingContext different
   * de celui de l'objet "bus". Elle permet aussi indiquer si la selection doit
   * etre unique. Cette condition peut etre utiliser pour selectionner les objets
   * dans les entites ayant les attributs sur lesquels on ne peut pas definir
   * la condition <code>UNIQUE</code>.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   * @param fetchLimit Le nombre maximum d'enregistrements a retourner
   */
  public void fetchTable(WODisplayGroup table, String nomTable,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst, int fetchLimit)
  {
    saveQualifier(table, qualifier);
    saveSort(table, sort);
    saveFetchLimit(table, fetchLimit);
    table.setSelectsFirstObjectAfterFetch(selectFirst);
    table.setObjectArray(fetchArray(econtextForTable(table),
                                    nomTable, qualifier, sort, true, fetchLimit));
  }
  
  /**
   * Recupere un ensemble d'objets de la base de donnees correspondant
   * a la condition donnee. Les selectionnes sont places dans un display group.
   * La methode permet de selectionner les objets dans un editingContext different
   * de celui de l'objet "bus". Elle permet aussi indiquer si la selection doit
   * etre unique. Cette condition peut etre utiliser pour selectionner les objets
   * dans les entites ayant les attributs sur lesquels on ne peut pas definir
   * la condition <code>UNIQUE</code>.
   * 
   * @param table Le display group ou les objets selectionnes
   *     doivent etre places.
   * @param qualifier La condition de selection des objets dans l'entite.
   * @param sort L'ordre de tri des objets dans le resultat.
   * @param selectFirst Indique s'il faut selectionner automatiquement le promier
   *   objet de toute la liste d'objets.
   * @param usesDistinct Indique si la selection des objets doit etre unique
   *   (sans doublons).
   * @param fetchLimit Le nombre maximum d'enregistrements a retourner
   */
  public void fetchTable(WODisplayGroup table, String nomTable,
                         EOQualifier qualifier, NSArray sort,
                         boolean selectFirst, boolean usesDistinct,
                         int fetchLimit)
  {
    saveQualifier(table, qualifier);
    saveSort(table, sort);
    saveUsesDistinct(table, usesDistinct);
    saveFetchLimit(table, fetchLimit);
    table.setSelectsFirstObjectAfterFetch(selectFirst);
    table.setObjectArray(fetchArray(econtextForTable(table),
                                    nomTable, qualifier, sort, usesDistinct, fetchLimit));
  }
  
  /**
   * Rafraichie le contenue d'un display group <code>aTable</code> contenant
   * les objets d'une entite. Utilise la condition, le trie et le fetchlimit
   * avec lesquels les objets ont ete charges precedement.
   */
  public void refetchTable(WODisplayGroup aTable) {
    fetchTable(aTable, restoreQualifier(aTable),
               restoreSort(aTable), aTable.selectsFirstObjectAfterFetch(),
               restoreUsesDistinct(aTable), restoreFetchLimit(aTable));
  }
 
  /**
   * Rafraichie le contenue d'une table. Les objets sont selectionnes
   * en utilisant la condition et le trie avec lesquels les objets ont ete
   * charges precedement. La selection des objets est gardee, si possible.
   */
  public void refetchWithSameSelection(WODisplayGroup aTable) {
    NSArray indexes;

    // Detecter, quel sont les indices a selectionner
    if (aTable.displayedObjects().count()==0) return;
    startsWorking();
    indexes = aTable.selectionIndexes();
    if (indexes.count() == 0) indexes = new NSArray(new Integer(0));
    // Rafrechir les donnees
    refetchTable(aTable);
    // Selectionner les objets
    if (aTable.displayedObjects().count() <=
        ((Number)indexes.objectAtIndex(0)).intValue())
      indexes = new NSArray(new Integer(aTable.displayedObjects().count()-1));
    else
      indexes = new NSArray(indexes.objectAtIndex(0));
    aTable.setSelectionIndexes(indexes);
    stopsWorking();
  }
  
  
  // nouveautes issues de DTWeb
  
  /**
   * 
   */
  private int lastFreeTransactionId;

  
  /**
   * Cree une nouvelle transaction et son "editingContext". Retourne
   * l'identifiant de la transaction creee.
   * 
   * <p>Une transaction creee correspond a un objet <code>EOEditingContext</code>
   * dans lequel les modifitions d'objets sont enregistrees et sauvegardees
   * dans un base de donnees. Le <i>editingContext</i> correspondant peut etre
   * obtenu via l'appel a une methode <code>econtextForTransaction</code>.</p>
   * 
   * <p><i>Cette methode permet de gerer les transactions au niveau
   * d'un editing context. Pour gerer les transaction au niveau de la
   * base de donnees directement, il faut tiliser la methode
   * <code>beginTransaction</code></i>.</p>
   * 
   * @see #rollbackECTrancsacition(Integer)
   * @see #commitECTrancsacition(Integer)
   * @see #econtextForTransaction(Integer)
   * @see #econtextForTransaction(Integer, boolean)
   * @see #beginTransaction()
   */
  public Integer beginECTransaction() {
    return createECTransaction(false);
  }
    
  /**
   * Annunle la transaction commencee dans le graphe d'objets. Cette appel
   * annunle les modifications apportees au graphe d'objet et supprime la
   * reference vers l'objet <code>EOEditingContext</code> correspondant.
   * 
   * <p>Cet appel termine automatiquement la transaction et toutes les
   * futures modifications des objets obtenus dans le cadre de cette transaction
   * sont interdites.</p>
   * 
   * @param transactionId L'identifiant de la transaction. Il est obtenu suite 
   * a l'appel a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #commitECTrancsacition(Integer)
   * @see #rollbackECTrancsacition(Integer, boolean)
   */
  public void rollbackECTrancsacition(Integer transactionId) {
    rollbackECTrancsacition(transactionId, true);
  }
    
  /**
   * Annunle la transaction commencee dans le graphe d'objets. Cette appel
   * annunle les modifications apportees au graphe d'objet et supprime la
   * reference vers l'objet <code>EOEditingContext</code> correspondant.
   * 
   * @param transactionId L'identifiant de la transaction. Il est obtenu
   *   suite a l'appel a la methode <code>beginECTransaction</code>.
   * @param terminate Indique si la transaction doit etre definitivement
   *   terminee. Si ce parametre indique <i>true</i>, alors toutes les 
   *   futures operations sur les objets obtenus dans le cadre de cette
   *   transaction sont interdites.
   * 
   * @see #beginECTransaction()
   * @see #rollbackECTrancsacition(Integer)
   * @see #commitECTransaction(Integer, boolean)
   */
  public void rollbackECTrancsacition(Integer transactionId, boolean terminate) {
    synchronized(transactions) {
      EOEditingContext ec = econtextForTransaction(transactionId);
      if (ec != null) ec.revert();
      if (terminate) terminateECTransaction(transactionId);
    }
  }

  /**
   * Valide la transaction commencee dans le graphe d'objets. Cette appel
   * repercute dans la base de donnees toutes les modifications apportees
   * sur le graphe d'objets.
   * 
   * <p>Retourne <i>true</i> dans le cas de succes et <i>false</i> dans le
   * cas contraire. Dans le cas de succes la reference vers l'objet
   * <code>EOEditingContext</code> correspondant est supprimee et la
   * transaction est definitivement terminee.  
   *  
   * @param transactionId L'identifiant de la transaction. Il est obtenu
   *   suite a l'appel a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #commitECTransaction(Integer, boolean)
   * @see #rollbackECTrancsacition(Integer)
   */
  public boolean commitECTransaction(Integer transactionId) {
    return commitECTransaction(transactionId, true);
  }

  /**
   * Valide la transaction commencee dans le graphe d'objets. Cet appel
   * repercute dans la base de donnees toutes les modifications apportees
   * sur le graphe d'objets. Le parametre <code>terminate</code> indique si les
   * la transaction doit etre termine et donc interdite aux futures operations
   * de mise a jour.
   * 
   * <p>Retourne <i>true</i> dans le cas de succes et <i>false</i> dans le
   * cas contraire. Dans le cas de succes la reference vers l'objet
   * <code>EOEditingContext</code> correspondant est supprimee.  
   *  
   * @param transId L'identifiant de la transaction. Il est obtenu suite
   *   a l'appel a la methode <code>beginECTransaction</code>.
   * @param terminate Indique si la transaction doit etre definitivement
   *   terminee. Si la valeur du parametre est <i>true</i>, alors les
   *   references vers les objets obtenus dans le cadre de cette transaction
   *   sont supprimees. 
   * 
   * @see #beginECTransaction()
   * @see #commitECTransaction(Integer)
   * @see #rollbackECTrancsacition(Integer, boolean)
   */
  public boolean commitECTransaction(Integer transId, boolean terminate) {
    boolean result = false;
    startsWorking();
    synchronized(transactions) {
      EOEditingContext ec = econtextForTransaction(transId);
//      Object originalDelegate = null;
      try {
//        if (useOperationOrder) {
//          originalDelegate = databaseContext.delegate(); 
//          databaseContext.setDelegate(
//              new DatabaseOperationDelegate(dbopsForTransaction(transId)));
//        }
        if (ec != null) ec.saveChanges();
        result = true;
      } catch(Throwable ex) {
        if (ec != null) ec.revert();
        throwError(ex);
      }
//      if (useOperationOrder)
//        databaseContext.setDelegate(originalDelegate);
      if (terminate)
        terminateECTransaction(transId);
    }
    stopsWorking();
    return result;
  }

  /**
   * Enleve la transaction avec l'identifiant <code>transactionId</code> de
   * la liste des transactions ouvertes.
   * 
   * <p>Cet appel libere toutes les ressources liees a la transaction. Apres
   * cet appel, les operations sur les objets obtenus dans le cadre de la
   * transaction ne sont plus autorisees et peuvent produire des erreurs.</p>
   * 
   * <p>Attention, la transaction par defaut ne peut jamais etre terminee.
   * Dans ce cas, l'appel a cette methode est ignore.</p>
   */
  public void terminateECTransaction(Integer transId) {
    if (transId != DefaultTransactionId) {
      EOEditingContext ec = econtextForTransaction(transId);
      ec.undoManager().removeAllActions();
      // Ajouter ec.dispose() pour WO 5
      transactions.removeObjectForKey(transId);
//      transactionOps.removeObjectForKey(transId);
    }
  }
  
  /**
   * Retourne l'objet <code>EOEditingContext</code> correspondant a
   * la transaction avec l'identifiant <code>id</code>. Il est
   * cree suite a l'appel a la methode <code>beginECTransaction</code>.
   * La valeur <i>null</i> est retournee si la transactio n'existe pas.
   * 
   * @param id L'identifiant de la transaction. Il est obtenu suite a l'appel
   * a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #econtextForTransaction(Integer, boolean) 
   */
  protected EOEditingContext econtextForTransaction(Integer id) {
    return (EOEditingContext)transactions.objectForKey(id);
  }

  /**
   * Retourne l'objet <code>EOEditingContext</code> correspondant a
   * la transaction avec l'identifiant <code>id</code>. Il est
   * cree suite a l'appel a la methode <code>beginECTransaction</code>.
   * 
   * <p>Si la valeur createIfNull est egale a <i>true</i> et la transactio
   * n'existe pas, un nouveau objet <code>EOEditingContext</code> est cree.
   * <i>Attention, ceci ne cree pas d'une nouvelle transaction !</i></p>
   * 
   * @param transId L'identifiant de la transaction. Il est obtenu suite a l'appel
   * a la methode <code>beginECTransaction</code>.
   * 
   * @see #beginECTransaction()
   * @see #econtextForTransaction(Integer)
   */
  protected EOEditingContext econtextForTransaction(Integer transId, boolean createIfNull) {
    EOEditingContext ec = null;
    if (transId != null) ec = (EOEditingContext)transactions.objectForKey(transId);
    if ((ec == null) && createIfNull) ec = new EOEditingContext();
    return ec;
  }

  /**
   * Cree une transaction dans le 
   */
  protected Integer getTransaction(Integer transId) {
    // Si la transaction n'existe pas, on cree une trans. locale
    if (transId == null)
      return createECTransaction(true);
    else // sinon, on utilise deja existante
      return transId;
  }

  /**
   * 
   */
  private synchronized Integer createECTransaction(boolean isInternal) {
    Integer id;
    int i;
    synchronized(transactions) {
      NSArray allKeys = transactions.allKeys();
      for (i = lastFreeTransactionId; ;i++) {
        if (isInternal)
          id = new Integer(-i);
        else
          id = new Integer(i);
        if (!allKeys.containsObject(id)) break;
      }
      if (lastFreeTransactionId == Integer.MAX_VALUE)
        lastFreeTransactionId = 1;
      else
        lastFreeTransactionId = i+1;
      transactions.setObjectForKey(new EOEditingContext(), id);
//      transactionOps.setObjectForKey(new NSMutableArray(), id);
    }
    return id;
  }
}
