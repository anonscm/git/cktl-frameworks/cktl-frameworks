package org.cocktail.fwkcktlwebapp.server;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.metier.DBATabColumns;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.appserver.ERXDirectAction;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXModelGroup;
import er.extensions.eof.ERXQ;

public class CktlWebAppTools extends ERXDirectAction {

	public CktlWebAppTools(WORequest r) {
		super(r);
	}

	public WOActionResults verifierEOModelsAction() {
		WOResponse response = new WOResponse();
		String content = "";
		CktlLog.log("Début de la vérification ");
		EOEditingContext ec = ERXEC.newEditingContext();
		ERXModelGroup modelGroup = (ERXModelGroup) ERXModelGroup.defaultGroup();
		NSArray<EOModel> eomodels = (NSArray<EOModel>) modelGroup.models();
		for (EOModel erxModel : eomodels) {
			if (!erxModel.name().equalsIgnoreCase("DBA") && !erxModel.name().equalsIgnoreCase("erprototypes")) {
//				CktlLog.log("Traitement du modèle " + erxModel.name());
				content += "Traitement de l'eomodel : "+erxModel.name()+"\n";
				NSArray<EOEntity> entities = erxModel.entities();
				for (EOEntity erxEntity : entities) {
//					CktlLog.log("Traitement de l'entité " + erxEntity.name());
					String owner = null;
					String tableName = null;
					String externalName = erxEntity.externalName();
					if (externalName != null) {
						int index = externalName.indexOf(".");
						if (index > -1) {
							owner = erxEntity.externalName().substring(0, index);
							tableName = externalName.substring(index + 1, externalName.length());
							NSArray<EOAttribute> attributes = erxEntity.attributes();
							for (EOAttribute eoAttribute : attributes) {
								String columnName = eoAttribute.columnName();
								if (columnName != null) {
									EOQualifier qualifier = ERXQ.and(ERXQ.equals(DBATabColumns.OWNER_KEY, owner.toUpperCase()),
											ERXQ.equals(DBATabColumns.TABLE_NAME_KEY, tableName.toUpperCase()),
											ERXQ.equals(DBATabColumns.COLUMN_NAME_KEY, columnName.toUpperCase()));
									try {
										DBATabColumns colonne = DBATabColumns.fetchByQualifier(ec, qualifier);
										if (colonne != null) {
											String dataType = colonne.dataType();
											String attDataType = eoAttribute.externalType();
											if (dataType.equalsIgnoreCase(attDataType)) {
												if (eoAttribute.valueClassName().equalsIgnoreCase("NSString")) {
													Integer dataLength = colonne.dataLength();
													int attDataLength = eoAttribute.width();
													if (dataLength.intValue() != attDataLength) {
														CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " / " + eoAttribute.name() + " : "
																+ "Width error : " + dataLength.intValue() + " / " + attDataLength);
														content += "\t"+erxEntity.name() + " / " + eoAttribute.name() + " : Width error : " + dataLength.intValue() + " / " + attDataLength+"\n";
													}
												} else if (attDataType.equalsIgnoreCase("Number")) {
													Integer dataPrecision = colonne.dataPrecision();
													int attDataPrecision = eoAttribute.precision();
													if (dataPrecision != null && dataPrecision.intValue() != attDataPrecision) {
														CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " / " + eoAttribute.name() + " : "
																+ "Precision error : " + dataPrecision.intValue() + " / " + attDataPrecision);
														content += "\t"+erxEntity.name() + " / " + eoAttribute.name() + " : Precision error : " + dataPrecision.intValue() + " / " + attDataPrecision+"\n";
													}
													Integer dataScale = colonne.dataScale();
													int attDataScale = eoAttribute.scale();
													if (dataScale != null && dataScale.intValue() != attDataScale) {
														CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " / " + eoAttribute.name() + " : "
																+ "Scale error : " + dataScale.intValue() + " / " + attDataScale);
														content += "\t"+erxEntity.name() + " / " + eoAttribute.name() + " : Scale error : " + dataScale.intValue() + " / " + attDataScale+"\n";
													}
												}
											} else {
												CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " / " + eoAttribute.name() + " : "
														+ "External Type error : " + dataType + " / " + attDataType);
												content += "\t"+erxEntity.name() + " / " + eoAttribute.name() + " : External Type error : " + dataType + " / " + attDataType+"\n";
											}
										} else {
											// CktlLog.log(erxModel.name()+" / "+erxEntity.name()+" / "+eoAttribute.name()+" : "+"Pas de colonne : "+owner+
											// " / "+tableName+ " / "+columnName);
										}
									} catch (IllegalStateException e) {
										CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " / " + eoAttribute.name() + " : " + "Pas de colonne : "
												+ owner + " / " + tableName + " / " + columnName);
									}
								}
							}
						} else {
							CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " : " + "Préfixe manquant sur le nom de la table");
							content += "\t"+erxEntity.name() + " : Préfixe manquant sur le nom de la table"+"\n";
						}
					} else {
						CktlLog.log(erxModel.name() + " / " + erxEntity.name() + " : " + "Pas d'external name");
						content += "\t"+erxEntity.name() + " : Pas d'external name"+"\n";
					}
				}
				content += "\n";
			}

		}
		ec.dispose();
		CktlLog.log("Fin de la vérification ");
		content += "\nVérification des eomodels finalisée.\n";
		response.setContent(content);
		return response;
	}

}
