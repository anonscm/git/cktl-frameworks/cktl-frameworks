package org.cocktail.fwkcktlwebapp.server.filter;

import java.util.ArrayList;
import java.util.List;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

public class FilterChain {

	private List<Filter> filters;
	
	public FilterChain() {
		this.filters = new ArrayList<Filter>();
	}
	
	public void doPreFilter(WORequest request) {
		for (Filter filter : filters) {
			filter.doPreFilter(request);
		}
	}
	
	public void doPostFilter(WOResponse response) {
		for (Filter filter : filters) {
			filter.doPostFilter(response);
		}
	}
	
	public void registerFilter(Filter filter) {
		this.filters.add(filter);
	}
	
}
