package org.cocktail.fwkcktlwebapp.server.filter;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

public abstract class AbstractDefaultFilter implements Filter {

	public void doPreFilter(WORequest request) {
	}

	public void doPostFilter(WOResponse response) {
	}

}
