package org.cocktail.fwkcktlwebapp.server.filter;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

public interface Filter {

	void doPreFilter(WORequest request);
	void doPostFilter(WOResponse response);
}
