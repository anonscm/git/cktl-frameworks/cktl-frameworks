package org.cocktail.fwkcktlwebapp.server;


import javax.print.attribute.standard.MediaSize.Other;

import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;

/**
 * Cette classe permet d'identifier le navigateur de client connecte a
 * l'application. L'dentifincation contient les informations sur le
 * nom, la version et la plate-forme de navigateur.
 */
public class CktlBrowserDetector {
  
  /**
   * Identifiant des navigateurs <i>Netscape</i> et <i>Mozilla</i>.
   */
  public static final String BROWSER_NETSCAPE = "Netscape";
  
  /**
   * Identifiant des navigateur <i>Microsoft Internet Explorer</i>.
   */
  public static final String BROWSER_MSIE = "MSIE";
  
  /**
   * Identifiant des navigateurs <i>iCab</i>.
   */
  public static final String BROWSER_ICAB = "iCab";
  
  /**
   * Identifiant de plate-forme <i>Windows</i>.
   */
  public static final String OS_WINDOWS = "Windows";
  
  /**
   * Identifiant de plate-forme <i>Mac</i> et <i>MacOS X</i>.
   */
  public static final String OS_MAC = "Mac";
  
  /**
   * Identifiant de plate-forme <i>Linux</i>.
   */
  public static final String OS_LINUX = "Linux";
  
  /**
   * Identifiant d'une valeur indefinie.
   */
  public static final String OTHER = "Other";
  
  /**
   * Valeur d'une plate-forme detectee.
   */
  private String os;
  
  /**
   * Valeur de la version de navigateur detectee.
   */
  private String version;
  
  /**
   * Valeur de nom de navigateur detectee.
   */
  private String browser;
  
  /**
   * Valeur de la version de navigateur detectee en format numeric.
   * Elle est egale a -1.0 si la version n'a pas pu etre detectee.
   */
  private float versionNumber;
  
  
//  private boolean browserVerOveride;
  
  /**
   * Object <code>WOContext</code> utilise pour l'analyser les informations.
   */
  private WOContext context;
  
  /**
   * Cree un nouveau objet "detecteur" et annalyse les donnees.
   * <code>context</code> contient les informations utilisees pour l'analyse.
   * @param context
   */
  public CktlBrowserDetector(WOContext context) {
    this.context = context;
    analyze();
  }

  /**
   * Effectue l'analyse d'informations. Utilise le valeur "User-Agent" d'une
   * requet HTTP extraite de l'objet {@link WOContext}
   *
   */
  private void analyze() {
    WORequest r = context.request();
    String userAgent = r.headerForKey("User-Agent");
    os = version = browser = OTHER;
    
    // TODO Verifier d'autres navigateurs : Opera, Safari
    if (userAgent.indexOf("Mozilla") != -1) {
      browser = BROWSER_NETSCAPE;
    } else if (userAgent.indexOf("iCab") != -1) {
      browser = BROWSER_ICAB;
    } else if (userAgent.indexOf("MSIE") != -1) {
      browser = BROWSER_MSIE;
    }

    // TODO Etendre l'ensemble des plateformes
    if (userAgent.indexOf("Win") != -1) {
      os = OS_WINDOWS;
    } else if (userAgent.indexOf("Mac") != -1) {
      os = OS_MAC;
    } else if (userAgent.indexOf("Linux") != -1) {
      os = OS_LINUX;
    }

   // TODO Refaire la detection de la version de navigateur
    if (userAgent.indexOf("7.0") != -1)
      version = "7.0";
    else if (userAgent.indexOf("7.1") != -1)
      version = "7.1";
    else if (userAgent.indexOf("6.0") != -1)
      version = "6.0";
    else if (userAgent.indexOf("5.0") != -1)
      version = "5.0";
    else if (userAgent.indexOf("5.5") != -1)
      version = "5.5";
    else if (userAgent.indexOf("4.0") != -1)
      version = "4.0";
    else if (userAgent.indexOf("4.5") != -1)
      version = "4.5";
    else if (userAgent.indexOf("4.7") != -1)
      version = "4.7";
    else if (userAgent.indexOf("3.0") != -1)
      version = "3.0";
      
    if (version.equals(OTHER))
      versionNumber = -1.0f;
    else
      versionNumber = Float.valueOf(version).floatValue();
  }

  /**
   * Retourne la version du navigateur detecte.
   * Retourne la valeur {@link Other} si la version n'a pas
   * pu etre detectee.
   */
  public String getVersion() {
    return version;
  }

  /**
   * Retourne le nom du navigateur detecte. Si le nom est detecte
   * correctement, la valeur retournee est egale a une des constantes
   * <code>BROWSER_XXX</code>. Retourne la valeur {@link Other} si
   * le nom n'a pas pu etre detecte.
   */
  public String getBrowser() {
    return browser;
  }

  /**
   * Retourne le nom de la plate-forme du navigateur detecte. Si la plate-forme
   * est detectee correctement, la valeur retournee est egale a une des
   * constantes <code>OS_XXX</code>. Retourne la valeur {@link Other}
   * si la plate-forme n'a pas pu etre detectee.
   */
  public String getOS() {
    return os;
  }
  
  /**
   * Retourne la valeur numerique de la version du navigateur detecte.
   * Retourne la valeur -1.0 si la version n'a pa pu etre detecte
   * correctement. 
   */
  public float getVersionNumber() {
    return versionNumber;
  }
  
//  public boolean getBrowserVerOveride() {
//    return browserVerOveride;
//  }
//
//  public void setBrowserVerOveride(boolean value) {
//    browserVerOveride = value;
//  }

//  public boolean isValidBrowser() {
//    if (browserVerOveride == false) {
//      return false;
//    } else
//      if (getBrowser().equals("Netscape")
//        && (getVersion().equals("7.0"))
//        || (getBrowser().equals("MSIE"))
//        && (getVersion().equals("6.0") || getVersion().equals("7.0"))) {
//      return true;
//    } else {
//      return false;
//    }
//  }
}
