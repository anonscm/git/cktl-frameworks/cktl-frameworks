package org.cocktail.fwkcktlwebapp.server;

import com.webobjects.eocontrol.EOObjectStore;

import er.extensions.eof.ERXEC;

/**
 * Cette classe represente un editing context en lecture seule (read-only). Il est utilise comme editing context par defaut dans une application Web.
 * Il empeche la sauvegarde dans la base de donnees de toutes les modifications apportes aux objets().
 * <p>
 * Cette classe est introduite afin d'assurer que les objets charges a partir d'une base de donnees dans un context commun ne seront pas modifies par
 * differentes processus de l'application
 * </p>
 * <p>
 * <i><b>Attention !</b> Le <code>CktlROEditingContext</code> n'est pas un <code>EOSharedEditingContext</code></i>.
 * </p>
 */
public class CktlROEditingContext extends ERXEC {

	/**
	 * Cree un nouvel "editing context". Ce constructeur est reecrit afin de garder la compatibilite avec les constructeurs de
	 * <code>EOEditingContext</code>.
	 */
	public CktlROEditingContext() {
		super();
	}

	/**
	 * Cree un nouvel "editing context". Ce constructeur est reecrit afin de garder la compatibilite avec les constructeurs de
	 * <code>EOEditingContext</code>.
	 */
	public CktlROEditingContext(EOObjectStore objectStore) {
		super(objectStore);
	}

	/**
	 * Cette methode surcharge celle de la classe <code>EOEditingContext</code>. Chaque appel a cette methode annule toutes le modifications faites
	 * sur ses objest et leve une exception <code>IllegalStateException</code>.
	 */
	public void saveChanges() {
		super.revert();
		throw new IllegalStateException("Saving changes in CktlROEditingContext is not allowed (read-only editing context)");
	}
}
