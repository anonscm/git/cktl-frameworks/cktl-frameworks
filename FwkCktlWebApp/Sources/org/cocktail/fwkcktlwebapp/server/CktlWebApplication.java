package org.cocktail.fwkcktlwebapp.server;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.Vector;

import org.cocktail.fwkcktlacces.server.FwkCktlAcces;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlTimeoutPage;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.util.EOModelCtrl;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion;
import org.cocktail.fwkcktlwebapp.server.version.A_CktlVersion.CktlVersionException;
import org.cocktail.fwkcktlwebapp.server.version.database.DatabaseVersionChecker;

import com.google.inject.Module;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModel;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eoaccess.EORelationship;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.woinject.InjectableApplication;

import er.extensions.appserver.ERXApplication;
import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEC.Factory;
import er.extensions.eof.ERXModelGroup;
import er.extensions.foundation.ERXProperties;
import er.extensions.foundation.ERXStringUtilities;

/**
 * Propose une implementation des fonctionnalites par defaut pour une application Web. Cette classe etend la classe {@link ERXApplication} et elle est
 * orientee a etre utilise avec les applications Web. Elle peut egalement etre utilisee dans les applications Java-Client.
 * <p>
 * Cette classe execute automatiquement des opreations lors de l'initialisation de l'application&nbsp;:
 * <ul>
 * <li>l'etablissement d'une connexion a la base de donnees et creation de gestionnaire de communication avec la base (methode {@link #dataBus()})</li>
 * <li>la creation de gestionnaire de configuration de l'application ({@link #config()})</li>
 * <li>gestionnaire d'envoi des messages e-mail ({@link #mailBus()})</li>
 * <li>gestionnaire de depot des documents ({@link #gedBus()})</li>
 * </ul>
 * </p>
 * <p>
 * Tous les gestionnaires sont initialises en utilisant leur implementation par defaut. Dans le cas ou vous souhaitez utiliser votre propre
 * implementation ou configuration des gestionnaires, vous pouvez redefinir les methode de type <code>getNewXXX</code> (par exemple,
 * {@link #getNewGedBus(CktlConfig)}). Ces methodes doivent retourner une instance de gestionnaire qui ensuite est utilisee pour la classe
 * {@link CktlWebApplication}.
 * </p>
 * <p>
 * L'initialisation de l'application est effectuee par la methode {@link #initApplication()}. Elle est appele par le constructeur de l'application.
 * Votre application peut surcharger cette methode, mais dans ce cas il est fortemment recommende faire appel a la methode originale. Autrement dit,
 * votre methode initApplication() doit contenir un appel <code>super.initApplication()</code>.
 * </p>
 * <p>
 * L'application {@link CktlWebApplication} permet egalement l'initialisation "automatique" de dictionnaire de connexion a la base de donnees a l'aide
 * d'un serveur d'authentification.
 * </p>
 * <h3>Configuration d'une application</h3>
 * <p>
 * La configuration de l'application est accessible via la methode {@link #config()}. Consultez la documentation de la classe {@link CktlConfig} pour
 * plus d'informations sur la gesiton de configuration.
 * </p>
 * <p>
 * La configuration de l'application est enregistree dans un fichier. Par defaut, lors d'initialisation l'application cherhe le fichier dans le
 * ressources de l'application portant le meme nom que l'application et avec une extension ".config". Le nom de fichier peut etre redefini a l'aide de
 * la methode {@link #configFileName()}. Il est fortemment conseille de redefinir la methode configFileName dans votre classe de l'application en
 * retournant le nom du fichier de configuration.
 * </p>
 * <p>
 * Si au moment d'execution, l'application ne trouve pas le fichier de configuration meme s'il existe dans les ressources, vous pouvez le preciser a
 * l'aide d'un argument de la ligne de commandes <code>CRIAppConfig</code>. Ce parametre doit indiquer le chemin absolu d'acces au fichier de
 * configuration.
 * <h3>Les parametres de configuration</h3>
 * <p>
 * La configuration de l'application est constitue des couples cle-valeur. Les parametres suivants sont utilisee par l'application et peut etre
 * definis dans la configuration de l'application&nbsp;:
 * </p>
 * <dt> <dt><code>APP_ID</code></dt> <dd>l'identifiant unique de l'application. Cette valeur peut etre utilisee pour initialiser la connexion a la
 * base de donnees. Par exemple, "DT".</dd> <dt><code>APP_ALIAS</code></dt> <dd>le alias de l'application. Cette valeur peut etre choisie librement et
 * elle indique le description plus detaillees d'une application. Par exemple, "DT Windows".</dd> <dt><code>APP_ADMIN_MAIL</code></dt> <dd>l'adresse
 * de l'administrateur de l'application. Une liste des adresse e-mail peut etre donnee, separees par une virgule. Dans ce cas, la premiere adresse est
 * consideree comme etant la principale adresse. Cette valeur remplace l'ancienne valeur <code>ADMIN_MAIL</code>.</dd> <dt><code>APP_USE_GEDFS</code></dt>
 * <dd>indique si l'application utilise le gestionnaire des documents. Les valeurs possibles sont YES ou NO. La valeur par defaut est NO.</dd> <dt>
 * <code>APP_USE_MAIL</code></dt> <dd>indique si l'application utilise le gestionnaire d'envoi des messages mail. Les valeurs possibles sont YES ou
 * NO. La valeur par defaut est YES.</dd> <dt><code>APP_URL</code></dt> <dd>le URL d'acces a l'application, si celle-ci existe.</dd> <dt>
 * <code>APP_ACCEPT_LOGINS</code></dt> <dd>la liste des logins, separes par une virgule, qui peuvent se connecter a l'application.</dd> <dt>
 * <code>DEFAULT_TIME_ZONE</code></dt> <dt><code>NET_PROXY_HOST</code></dt> <dt><code>NET_PROXY_PORT</code></dt> <dt><code>NET_NO_PROXY_HOSTS</code></dt>
 * <dt><code>MAIN_WEB_SITE_URL</code></dt> <dt><code>MAIN_LOGO_URL</code></dt> <dt><code>SAUT_URL</code></dt> <dt><code>SAUT_ID_TRANSLATION</code></dt>
 * <dt><code>HTML_IMAGES_ROOT</code></dt> <dt><code>ACCEPT_EMPTY_PASSWORD</code></dt> </dl>
 * <p>
 * Parametre de ligne de commande : <code>CRIAppConfig</code>
 * </p>
 */
public abstract class CktlWebApplication
		extends InjectableApplication {

	private static String defaultAppURL;
	private CktlConfig config;
	private CktlDataBus dataBus;
	private CktlGedBus gedBus;
	private CktlMailBus mailBus;
	private NSMutableDictionary sautTranslation;
	private CktlResourceManager appResourceManager;
	private ModuleRegister moduleRegister;
	
    /**
     * Est-ce que la base de données est une base de données migrée avec flyway ou avec des scripts SQL livrés ?
     * <ul>
     * <li>si non, la base est construite avec des scripts SQL livrés et donc qui mettent à jour les tables DB_VERSION. On peut contrôler les versions des BDD et on empeche le démarrage de l'application si les tests échouent.
     * <li>si oui, la base est construite avec flyway et les tables DB_VERSION ne sont pas mises à jour. Les contrôles peuvent tout de même être effectués mais ne doivent pas empecher le démarrage de l'application si les tests échouent.
     * </ul>
     */
    private Boolean dbFlyway = null;
    
	/**
	 * Cree et initialise une instance de l'application.
	 */
	public CktlWebApplication() {
		// On indique q'on n' pas encore de la connexion a la BD
		dataBus = null;
		gedBus = null;
		mailBus = null;
		initApplication();
	}

	public static CktlWebApplication application() {
		return (CktlWebApplication) WOApplication.application();
	}

	public ModuleRegister getModuleRegister() {
		if (moduleRegister == null) {
			moduleRegister = new AllModulesRegister();
		}
		return moduleRegister;
	}

	@Override
	protected Module[] modules() {
		List<? extends Module> modules = moduleRegister.modules();
		return (Module[]) modules.toArray(new Module[0]);
	}

	/**
	 * Definit la TimeZone par defaut de l'application. On utilise le parametre <code>DEFAULT_TIME_ZONE</code> (.config de l'application ou base de
	 * donnees), ou "CEST" si ce n'est pas defini ou incorrect.
	 * <p>
	 * Voir NSTimeZone.knownTimeZoneNames() pour la liste des valeurs possible pour <code>DEFAULT_TIME_ZONE</code>.
	 */
	private void initTimeZone() {
		String tzs = config().stringForKey(CktlConfig.CONFIG_DEFAULT_TIME_ZONE_KEY);
		if (tzs == null) {
			tzs = CktlConfig.DEFAULT_TIME_ZONE_VALUE;
		}
		try {
			NSTimeZone tz = NSTimeZone.timeZoneWithName(tzs, true);
			NSTimeZone.setDefaultTimeZone(tz);
			NSTimeZone.setDefault(tz);
			java.util.TimeZone.setDefault(tz);
		} catch (Throwable ex) {
			System.err.println("Error while setting the time zone to \"" + tzs + "\"");
			System.err.println("The time zone was set to \"" + CktlConfig.DEFAULT_TIME_ZONE_VALUE + "\"");
			ex.printStackTrace();
			NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(CktlConfig.DEFAULT_TIME_ZONE_VALUE, true));
		}
	}

	/**
	 * Verifie les parametres des reseau correspondant &agrave; la cle <code>propertyKey</code> et ellimine les definition en double. Ce type des
	 * definitions est possible sur certaines plateformes Java, ce qui produi une erreur lors de l'execution de l'application.
	 * 
	 * @see #initNetSettings()
	 */
	private void filterNetSettings(String propertyKey) {
		String settings = StringCtrl.normalize(System.getProperty(propertyKey));
		if (settings.length() > 0) {
			Vector v = StringCtrl.toVector(settings, "|", true);
			Vector vRes = new Vector();
			StringBuffer sb = new StringBuffer();
			for (int i = 0; i < v.size(); i++) {
				if (!vRes.contains(v.elementAt(i))) {
					vRes.addElement(v.elementAt(i));
					if (sb.length() > 0) {
						sb.append("|");
					}
					sb.append(v.elementAt(i));
				}
			}
			System.setProperty(propertyKey, sb.toString());
		}
	}

	/**
	 * Change les parametres de connexion a la base de donnees en y ajoutant les informations sur l'utilisateur et l'instance de la base de donnees.
	 */
	private NSDictionary resetDictionary(NSDictionary originalDico,
			NSDictionary addDico) {
		NSMutableDictionary newDico = new NSMutableDictionary();
		if (originalDico != null) {
			newDico.addEntriesFromDictionary(originalDico);
		}
		if (addDico != null) {
			newDico.addEntriesFromDictionary(addDico);
		}
		return newDico;
	}

	/**
	 * Verifie si le dictionaire de connexion a la base de donnees est complet. Il est considere incomplet, s'il est null ou si le parametre
	 * "password" n'est pas defini.
	 */
	private boolean isConnectionDicoComplete(NSDictionary dico) {
		if (dico == null) {
			return false;
		}
		if (StringCtrl.normalize((String) dico.valueForKey("password")).length() == 0) {
			return false;
		}
		if ((StringCtrl.normalize((String) dico.valueForKey("userName")).length() == 0)
				&& (StringCtrl.normalize((String) dico.valueForKey("username")).length() == 0)) {
			return false;
		}
		// Sinon, OK
		return true;
	}

	/**
	 * Definie les parametres de connexion a la base de donnees pour tous les models.
	 */
	private void resetAllModels() {
		NSArray allModels = EOModelGroup.defaultGroup().models();
		EOModel aModel;
		NSDictionary modelConnDico, modelUserDico, aDico;
		if (allModels.count() == 0) {
			return;
		}
		for (int i = 0; i < allModels.count(); i++) {
			aModel = (EOModel) allModels.objectAtIndex(i);
			modelConnDico = aModel.connectionDictionary();
			modelUserDico = aModel.userInfo();
			// Si le dictionaire n'est pas defini, alors on le definira
			if (!isConnectionDicoComplete(modelConnDico)) {
				// S'il faut utiliser le dictionaire par defaut (application)
				if ((modelUserDico == null)
						|| (StringCtrl.normalize((String) modelUserDico.valueForKey("modelId")).length() == 0)) {
					aDico = defaultConnectionDictionary();
				} else {
					// Sinon, on utilisera le dictionaire propre au model
					aDico = connectionDictionaryForID((String) modelUserDico.valueForKey("modelId"));
					// S'il n'est pas defini alors on prend selui de l'application
					if (aDico.count() == 0) {
						aDico = defaultConnectionDictionary();
					}
				}
				aModel.setConnectionDictionary(resetDictionary(modelConnDico, aDico));
			}
		}
	}

	/**
	 * La methode qui commence la boucle principale d'execution de l'application. Ne pas reecrire cette methode. Surchargez la methode
	 * {@link #startRunning()}
	 */
	public final void run() {
		startRunning();

		// activation du filtrage de l'acces à l'application via le fwkcktlacces
		FwkCktlAcces.addAccessControlForRequestHandler(FwkCktlAcces.CKTL_DEFAULT_REQUEST_HANDLER);

		super.run();
	}

	/**
	 * Cette methode est appelee avant le demarrage de l'application, apres sont initialisation. Reecrire cette methode si vous voulez effectuer
	 * certaines actions juste avant l'execution de l'application.
	 */
	public void startRunning() {
	}

	/* ==== Les methodes pour la gestion de base de donnees ==== */

	/**
	 * Indique si l'application doit utiliser par defaut l'acces a une base de donnees. La valeur par defaut est <code>true</code>.
	 */
	public boolean useDatabase() {
		return true;
	}

	/**
	 * Retourne l'objet de la gestion d'acces a la base de donnees. Utilise la methode {@link #getNewDataBus(EOEditingContext)} pour creer un nouveau
	 * objet, si l'acces n'a pas ete initialise. Aucun objet n'est renvoye si la methode {@link #useDatabase()} retourne <code>false</code>.
	 */
	public CktlDataBus dataBus() {
		if (useDatabase() && (dataBus == null)) {
			dataBus = getNewDataBus(ERXEC.newEditingContext());
		}
		return dataBus;
	}

	public CktlDataBus getNewDataBus(EOEditingContext ec) {
		return new CktlDataBus(ec);
	}

	/**
	 * Retourne la valeur de configuration <code>APP_USE_GEDFS</code> si elle est definie, sinon <code>false</code>.
	 */
	public boolean useGed() {
		String useValue = config().stringForKey(CktlConfig.CONFIG_APP_USE_GEDFS_KEY);
		if (useValue != null) {
			return StringCtrl.toBool(useValue);
		}
		return false;
	}

	public synchronized CktlGedBus gedBus() {
		return gedBus;
	}

	public synchronized CktlGedBus gedBus(int noIndividu) {
		if (gedBus != null) {
			gedBus.setNoIndividu(noIndividu);
		}
		return gedBus;
	}

	public CktlGedBus getNewGedBus(CktlConfig config) {
		return new CktlGedBus(config);
	}

	/**
	 * Retourne la valeur de configuration <code>APP_USE_MAIL</code> si elle est definie, sinon <code>false</code>.
	 */
	public boolean useMail() {
		String useValue = config().stringForKey(CktlConfig.CONFIG_APP_USE_MAIL_KEY);
		if (useValue != null) {
			return StringCtrl.toBool(useValue);
		}
		return true;
	}

	public CktlMailBus mailBus() {
		return mailBus;
	}

	public CktlMailBus getNewMailBus(CktlConfig config) {
		return new CktlMailBus(config);
	}

	/**
	 * Retourne l'adresse IP du client, ou la valeur "inconnue" si elle ne peut pas etre detectee.
	 */
	public String getRequestIPAddress(WORequest request) {
		String addr = request.headerForKey("REMOTE_ADDR");
		if (addr == null)
			addr = request.headerForKey("x-webobjects-remote-addr");
		if (addr == null)
			addr = request.headerForKey("REMOTE_HOST");
		if (addr == null)
			addr = "inconnue";
		return addr;
	}

	public CktlResourceManager appResources() {
		if (appResourceManager == null)
			appResourceManager = new CktlResourceManager();
		return appResourceManager;
	}

	/**
	 * Returne l'objet permettant d'acceder aux parameters de l'application. Voir la definition de la classe {@link CktlConfig}.
	 */
	public CktlConfig config() {
		if (config == null) {
			config = getNewConfig(configFilePath());
		}
		return config;
	}

	/**
	 * Renvoie le chemin d'acces au fichier de configuration. Le nom de fichier de configuration est donnes par la methode {@link #configFileName()}
	 */
	public String configFilePath() {
		String path = System.getProperty("CRIAppConfig");
		if ((path == null) && (configFileName() != null))
			path = appResources().pathForResource(configFileName());
		return path;
	}

	/**
	 * Renvoie le nom de fichier de configuration de l'application. Null si aucun fichier ne doit pas etre utilise. Le nom par defaut pour un fichier
	 * de configuration est <nom_application>.config Reecrire cet methode si vous voulez utiliser un autre fichier. Le resultat de cette methode est
	 * ignore si le fichier de configuration est precise via le parametre <code>CRIAppConfig</code> de la ligne de commande de lancement de
	 * l'application.
	 */
	public String configFileName() {
		return name() + ".config";
	}

	/**
	 * Cree et retour une nouvelle instance de gestionnaire de configuration {@link CktlConfig} qui gere les parametres de configuration enregistres
	 * dans le fichier <code>filePath</code>.
	 * <p>
	 * Cette methode retourne une instance qui considere le fichier filePath comme etant enregistre en format Java-Properties. Vous pouvez redefinir
	 * cette methode dans votre application pour proposer une autre de gestionnaire de configuration, ou pour le parametrer autrement (par exemple,
	 * gerer la configuration XML).
	 * </p>
	 */
	public CktlConfig getNewConfig(String filePath) {
		return new CktlConfig(filePath);
	}

	/**
	 * Renvoie le nom de la table avec les parametres de configuration. Null si la base de donnees ne doit pas etre utilisee. La valeur par defaut est
	 * <code>EOGrhumParameteres.ENTITY_NAME</code>. Reecrire cet methode si vous voulez indiquer un nom defferent de la table. La table doit contenir
	 * au moin deux champs: "paramKey" (cle) et "paramValue" (valeurs). La valeur de cette methode est utilisee pour initialiser la configuration de
	 * l'application.
	 */
	public String configTableName() {
		return EOGrhumParametres.ENTITY_NAME;
	}

	/**
	 * Retourne l'addrese e-mail pour le conctact. Par defaut, c'est le parametre <code>APP_ADMIN_MAIL</code> dans la configuration de l'application
	 * (fichier .config).
	 */
	public String contactMail() {
		String mail = config().stringForKey(CktlConfig.CONFIG_APP_ADMIN_MAIL_KEY);
		if (mail == null)
			mail = config().stringForKey(CktlConfig.CONFIG_ADMIN_MAIL_KEY);
		Vector mails = StringCtrl.toVector(mail, ",");
		if (mails.size() > 0) {
			return (String) mails.elementAt(0);
		} else {
			return null;
		}
	}

	/**
	 * Page principale ou la connexion est redirigee apres que la session de travail soit terminee. Par defaut, c'est le parametre
	 * <code>MAIN_WEB_SITE_URL</code> dans la configuration de l'application (fichier .config).
	 */
	public String mainWebSiteURL() {
		return config().stringForKey(CktlConfig.CONFIG_MAIN_WEB_SITE_URL_KEY);
	}

	/**
	 * URL de l'image de logo du site. Par defaut, c'est le parametre <code>MAIN_LOGO_URL"</code> dans la configuration de l'application (fichier
	 * .config).
	 */
	public String mainLogoURL() {
		return config().stringForKey(CktlConfig.CONFIG_MAIN_LOGO_URL_KEY);
	}

	/**
	 * Blocage du Logo de l'établissement si le parametre <code>MAIN_LOGO_URL"</code> dans la configuration de l'application (fichier .config) est
	 * vide ou inexistant.
	 */

	public boolean isMainLogoURLBlocked() {
		if (mainLogoURL() == null
				|| mainLogoURL().equals("")
				|| mainLogoURL().equals(" ")) {
			return false;
		}
		return true;
	}

	/**
	 * Renvoie le nom d'une table dans le model de donnees de l'application. Elle est utilisee pour initialiser les objets d'acces a la base de
	 * donnees.
	 * <p>
	 * Par defaut, cette methode cherche une table dans le model dont le nom est donne par la methode {@link #mainModelName()}. Si aucun model
	 * n'existe avec ce nom ou s'il ne contient aucune table, alors le premier model (non vide) du group de tous les models de l'application est pris.
	 * </p>
	 * <p>
	 * Cette methode est utilisee pour les besoins internes d'initialisation de l'application. Elle peut etre redefinie pour simplifier la procedure
	 * de recherche de nom d'une table.
	 * </p>
	 * <p>
	 * <i>Aucune donnee ne sera chargee a partir de la table indiquee lors de l'initialisation de l'application</i>.
	 * </p>
	 */
	public String mainEntityName() {
		EOModel mainModel = null;
		if (mainModelName() != null)
			mainModel = EOModelGroup.defaultGroup().modelNamed(mainModelName());
		// Si on n'a rien trouve, alors on initialise a partir de n'import
		// quelle entite
		if ((mainModel == null) || (mainModel.entities().count() == 0)) {
			NSArray allModels = EOModelGroup.defaultGroup().models();
			for (int i = 0; i < allModels.count(); i++) {
				mainModel = (EOModel) allModels.objectAtIndex(i);
				if (mainModel.entities().count() > 0)
					break;
			}
		}
		// Si on a trouve un model avec les entites, alors on return son nom
		if ((mainModel != null) && (mainModel.entities().count() > 0))
			return ((EOEntity) mainModel.entities().objectAtIndex(0)).name();
		else
			return null;
	}

	/**
	 * Renvoie le nom du model principal de donnees pour l'application. Il est utilise pour initialiser l'objet EOModel de l'application. Par defaut,
	 * le model a le meme nom que l'application.
	 * <p>
	 * Cette methode peut etre redefinie si le nom de modele est diferent du nom de l'application.
	 * </p>
	 * <p>
	 * Cette methode doit renvoyer le nom de l'application sans extention <code>.eomodeld</code>.
	 * </p>
	 */
	public String mainModelName() {
		return name();
	}

	/**
	 * Renvoie le dictionnaire de la connexion a la base de donnees. Il peut etre compose de : "password", "userName", "serverId", "URL". Par defaut,
	 * il est initialise en comuniquant avec le service d'authentification. Cette initialisation utilise le parametre <code>SAUT_URL</code> de la
	 * configuration et l'identificateur l'application ou du model.
	 */
	public NSMutableDictionary connectionDictionaryForID(String id) {
		NSMutableDictionary dico = new NSMutableDictionary();
		String sautURL;
		if (id == null)
			return dico;
		if (config() == null)
			return dico;
		sautURL = config().stringForKey(CktlConfig.CONFIG_SAUT_URL_KEY);
		if (sautURL == null)
			return dico;
		SAUTClient client = new SAUTClient(sautURL);
		id = translateModelId(id);
		String sDico = client.requestDecryptedConnectionInfo(id, config().stringForKey(CktlConfig.CONFIG_APP_ALIAS_KEY));
		// NSLog.out.appendln("SAUT-ULR : "+appConfig().valueForKey("SAUT_URL"));
		// NSLog.out.appendln("Dico : "+sDico);
		Properties props = SAUTClient.toProperties(sDico);
		if (props != null) {
			addKeyFromProperties(props, "userName", dico, "username");
			addKeyFromProperties(props, "userName", dico, "userName");
			addKeyFromProperties(props, "password", dico, "password");
			addKeyFromProperties(props, "serverId", dico, "serverId");
			addKeyFromProperties(props, "URL", dico, "URL");
		}
		return dico;
	}

	/**
	 * Ajoute une valeur dans le dictionaire a partir des proprietes (Java) donnees. Ne fait rien si la propriete donnee n'est pas trouvee.
	 */
	private void addKeyFromProperties(Properties props, String key,
			NSMutableDictionary dico, String withKey) {
		if (props.getProperty(key) != null)
			dico.takeValueForKey(props.getProperty(key).trim(), withKey);
	}

	/**
	 * Renvoie le dictionnaire de la connexion a la base de donnees pour une application. Le doctionnaire correspond au parametre <code>APP_ID</code>
	 * de la configuration (voir la methode {@link #configFileName()}). Voir aussi la methode {@link #connectionDictionaryForID(String)}.
	 */
	public NSMutableDictionary defaultConnectionDictionary() {
		return connectionDictionaryForID(config().stringForKey(CktlConfig.CONFIG_APP_ID_KEY));
	}

	/**
	 * Renome l'identifiant <code>id</code> de connexion a la base de donnees. Le dictionnaire de translation se trouve dans la valeur
	 * <code>SAUT_ID_TRANSLATION</code> de configuration. Cette valeur a la forme :
	 * <p>
	 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example">
	 * <tr>
	 * <td>
	 * 
	 * <pre>
	 *   SAU_ID_TRANSLATION=oldID1:newId1,oldID2:newID2,...
	 * </pre>
	 * 
	 * </td>
	 * </tr>
	 * </table>
	 */
	private String translateModelId(String id) {
		String newId;
		if (id == null)
			return null;
		// On initialise le dictionnaire de translations, s'il le faut
		if (sautTranslation == null) {
			sautTranslation = new NSMutableDictionary();
			String trans = StringCtrl.normalize(config().stringForKey(CktlConfig.CONFIG_SAUT_ID_TRANSLATION_KEY));
			if (trans.length() > 0) {
				String oldId;
				int idx;
				// Les couples oldId:newId
				NSArray ids = NSArray.componentsSeparatedByString(trans, ",");
				for (int i = 0; i < ids.count(); i++) {
					oldId = (String) ids.objectAtIndex(i);
					idx = oldId.indexOf(":");
					if (idx >= 0) {
						newId = oldId.substring(idx + 1);
						oldId = oldId.substring(0, idx);
						if ((oldId.length() > 0) && (newId.length() > 0))
							sautTranslation.setObjectForKey(newId, oldId);
					}
				}
			}
		}
		// On essaie de traduire
		newId = (String) sautTranslation.objectForKey(id);
		if (newId != null)
			return newId;
		else
			return id;
	}

	/**
	 * Retourne le mot de passe d'administrateur pour l'application. Par defaut, cette methode recupere la valeur de parametre de configuration
	 * <code>APP_ADMIN_PASSWORD</code>. On considere que le mot de passe n'est pas defini si cette valeur est soit <code>null</code>, vide ou la
	 * longeur de mot de passe est inferieure a 2 symboles.
	 * <p>
	 * <i>Cette methode doit toujours renvoyer le mot de passe crypte !</i>.
	 * </p>
	 */
	public String getRootPassword() {
		String rootPass = StringCtrl.normalize(config().stringForKey(CktlConfig.CONFIG_APP_ADMIN_PASSWORD_KEY));
		if ((rootPass == null) || (rootPass.length() < 2))
			return null;
		return rootPass;
	}

	/**
	 * Renvoie <code>true</code> si la connexion a l'application accepte les mots de passes vides. Dans ce cas, l'identification d'utilisateur se fait
	 * uniquement via son "login".
	 * <p>
	 * La valeur par defaut est <code>false</code>. Cette valuer peut etre changee en definissant le parametre de configuration
	 * <code>ACCEPT_EMPTY_PASSWORD</code>.
	 * </p>
	 */
	public boolean isAcceptEmptyPassword() {
		return config().booleanForKey(CktlConfig.CONFIG_ACCEPT_EMPTY_PASSWORD_KEY);
	}

	/**
	 * Teste si l'utilisateur donne peut se connecter a l'application. Voir la variable de configuration <code>APP_ACCEPT_LOGINS</code>.
	 */
	public boolean acceptLoginName(String loginName) {
		String allowedLogins = config().stringForKey(CktlConfig.CONFIG_APP_ACCEPT_LOGINS_KEY);
		if (allowedLogins == null)
			allowedLogins = config().stringForKey(CktlConfig.CONFIG_ACCEPT_LOGINS_KEY);
		if (allowedLogins == null)
			return true;
		// Il faut modifier ce test, il n'est pas correct
		return (allowedLogins.indexOf(loginName) >= 0);
	}

	/**
	 * Renvoie le URL de connexion a l'application. La valeur est detectee en faisant appel aux methodes {@link #webserverConnectURL()} ou
	 * {@link #directConnectURL()} de {@link WOApplication}.
	 * <p>
	 * Attention, si l'application est executee en mode "deploiement", le resultat des appels aux methodes ci-dessus dependent du parametre
	 * <code>WOAdaptorURL</code>.
	 * </p>
	 * 
	 * @see #getApplicationInstanceURL(WOContext)
	 */
	public String getApplicationURL(WOContext context) {
		String applicationURL;
		// Si le context est disponible, on l'utilise pour determiner l'url
		if (context != null) {
			if (context.request().isUsingWebServer())
				applicationURL = webserverConnectURL();
			else
				applicationURL = directConnectURL();
		} else { // Sinon, on essaie de le trouver...
					// ...en testant si WebConnect repond
			if (defaultAppURL == null) {
				defaultAppURL = webserverConnectURL();
				// ...sinon, on prend direct connect
				if (!checkConnection(defaultAppURL))
					defaultAppURL = directConnectURL();
			}
			applicationURL = defaultAppURL;
		}
		// On ellimine le tout ce qui suit ".woa/"
		int i = applicationURL.indexOf(".woa");
		if (i >= 0)
			applicationURL = applicationURL.substring(0, i) + ".woa";
		return applicationURL;
	}

	/**
	 * Retourne le URL de connexion a l'instance en cours d'execution. Cette methode retourne le meme URL que la methode
	 * {@link #getApplicationURL(WOContext)}, mais complete par le numero d'instance, si possible.
	 * 
	 * @see #getApplicationURL(WOContext)
	 */
	public String getApplicationInstanceURL(WOContext context) {
		// On prend d'abord le URL par defaut
		String applicationURL = getApplicationURL(context);
		String applicationInstanceURL = applicationURL;
		// Le URL par defaut ne tien pas compte de numero d'instance
		// On essaye de l'ajouter
		if ((applicationURL != null) && (context != null)) {
			// Le numero de l'instance peut se trouver dans URL apres ".woa"
			String req = context.request().applicationURLPrefix();
			int i = req.indexOf(".woa");
			if (i >= 0)
				req = req.substring(0, i) + ".woa";
			i = applicationURL.indexOf(req);
			if (i >= 0) {
				applicationInstanceURL = applicationURL.substring(0, i);
				req = context.request().applicationURLPrefix();
				if (!req.startsWith("/") && (applicationInstanceURL.length() > 0))
					applicationInstanceURL += "/";
				applicationInstanceURL += req;
			}
		}
		return applicationInstanceURL;
	}

	/**
	 * Verifie si la connexion a l'adresse <code>link</code> fonctionne. Cette methode essaie d'etablire une connexion a l'adresse donnee et retourne
	 * <code>true</code> dans le cas si la connexion repond, sinon <code>false</code>.
	 */
	private boolean checkConnection(String link) {
		try {
			URL url = new URL(link);
			URLConnection con = url.openConnection();
			con.connect();
			return true;
		} catch (Exception e) {
			//      e.printStackTrace();
			return false;
		}
	}

	/**
	 * Retourne le URL complet de l'action de <i>login</i> dans l'application. Cette valeur doit etre utilisee comme une reference HREF de lien de
	 * connexion a l'application sur la page d'acceuil, par exemple.
	 */
	public String getLoginActionURL(WOContext context) {
		return CktlWebAction.getDefaultLoginActionURL(context);
	}

	/**
	 * Retourne le URL d'une image. Le URL est construit un connectant la valuer du parametre <code>HTML_IMAGES_ROOT</code> de la configuration de
	 * l'application avec le nom de fichier de l'image. Le nom de l'image <code>imageName</code> seul est retourne si le parametre
	 * <code>HTML_IMAGES_ROOT</code> n'est pas defini.
	 * 
	 * @see #getImageURL(String, String)
	 */
	public String getImageDefaultURL(String imageName) {
		String imgRoot = StringCtrl.normalize(config().stringForKey(CktlConfig.CONFIG_HTML_IMAGES_ROOT_KEY));
		if (imgRoot.length() > 0)
			return FileCtrl.normalizeDirName(imgRoot) + imageName;
		return imageName;
	}

	/**
	 * Construit le URL d'une image.
	 * 
	 * @param imageURLKey
	 * @param imageName
	 * @return URL d'une image ou <code>null</code> si aucun URL n'a pas ete trouve.
	 * @see #getImageDefaultURL(String)
	 */
	public String getImageURL(String imageURLKey, String imageName) {
		String url = null;
		if (imageURLKey != null)
			url = config().stringForKey(imageURLKey);
		if ((url == null) && (imageName != null)) {
			url = getImageDefaultURL(imageName);
		}
		return url;
	}

	/**
	 * Retourne la page qui est affichee lorsqu'la session est terminee a cause de time-out. Si cette methode retourne <code>null</code>, alors la
	 * page par defaut de WebObjects est utilisee. Pour redefinir la page time-out, la classe Application peut surcharger la methode
	 * getSessionTimeoutPage:
	 * <p>
	 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example">
	 * <tr>
	 * <td>
	 * 
	 * <pre>
	 * <code class="comment">// A inclure dans la classe Application</code>
	 * public WOComponent getSessionTimeoutPage(WOContext context) {
	 *   return getMyTimeoutPage(context);
	 * }
	 * </pre>
	 * 
	 * </td>
	 * </tr>
	 * </table>
	 * <p>
	 * Par defaut, cette methode retourne la page de timeout definit par le composant {@link CktlTimeoutPage}, si elle est disponibles.
	 * </p>
	 * 
	 * @param context Le context dans lequel la page de time-out est creee.
	 */
	public WOComponent getSessionTimeoutPage(WOContext context) {
		return getDefaultTimeoutPage(context);
	}

	/**
	 * Retourne la page de "Session Timeout" propose par defaut. C'est la page portant le nom {@link CktlTimeoutPage}. Retourne <code>null</code> si
	 * elle n'existe pas ou ne peut pas etre trouvee.
	 * 
	 * @param context Le context dans lequel la page de time-out est creee.
	 */
	private WOComponent getDefaultTimeoutPage(WOContext context) {
		try {
			return pageWithName(CktlTimeoutPage.class.getName(), context);
		} catch (Throwable ex) {
			ex.printStackTrace();
			return null;
		}
	}

	/**
	 * Gere la presentation de la page "Session Time-Out". Methode de la {@link WOApplication}. Ne pas surcharger cette methode. Utilisez plutot la
	 * methode {@link #getSessionTimeoutPage(WOContext)}.
	 */
	public WOResponse handleSessionRestorationErrorInContext(WOContext aContext) {
		WOComponent responsePage = getSessionTimeoutPage(aContext);
		if (responsePage == null)
			return super.handleSessionRestorationErrorInContext(aContext);
		return responsePage.generateResponse();
	}

	/**
	 * Renvoie une chaine de caracteres svec les messages logs de l'application. Les messages sont pris dans le fichier des logs de l'application. Il
	 * est indique a l'aide de parametre "<code>-WOOutputPath</code>" lors du lancement de l'application.
	 * <p>
	 * Retourne une chaine avec un message d'erreur si les logs ne peuvent pas etre accedes pour une raison quelconque (le parametre
	 * <code>WOOutputPath</code> n'existe pas, le fichier des logs ne peut pas etre consulte, etc...).
	 * </p>
	 */
	public String getLogs() {
		String logsFile = config().stringForKey("WOOutputPath");
		if (logsFile == null) {
			return "L'emplacement des logs de l'application n'a pas pu etre detecte (WOOutputPath)";
		} else {
			try {
				ByteArrayOutputStream out = new ByteArrayOutputStream();
				FileInputStream in = new FileInputStream(logsFile);
				StreamCtrl.writeContentToStream(in, out, FileCtrl.getFileSize(logsFile));
				in.close();
				out.flush();
				out.close();
				return out.toString();
			} catch (Throwable ex) {
				return "Une erreur c'est produite lors de l'acces aux logs de l'applications:\n" + ex.getMessage();
			}
		}
	}

	// fusion des 2 anciennes classes en une seule

	/**
	 * Initialise les parametres proxy de connextion au reseau. Permet de definir les domaines pour lesquels le proxy ne sera jamais utilise. Les
	 * valeurs de configuration suivantes sont utilisees :
	 * <ul>
	 * <li><code>NET_PROXY_HOST</code> - l'adresse du serveur proxy. Ce parametre correspond a la propriete de Java <code>http.proxyHost</code>.</li>
	 * <li><code>NET_PROXY_PORT</code> - le numero du serveur proxy. Ce parametre correspond a la propriete de Java <code>http.proxyPort</code>.</li>
	 * <li><code>NET_NO_PROXY_HOSTS</code> - la liste des adresses pour lesquelles le serveur proxy ne doit pas etre utilise. Les elements de la liste
	 * peuvent etre separes par les symboles "|" ou ",". Les adresses peuvent contenir le symbole * pour indiquer une plage d'adresses. Ce parametre
	 * correspond aux proprietes de Java <code>http.nonProxyHosts</code> et <code>ftp.nonProxyHosts</code> (connexions HTTP et FTP).</li>
	 * </ul>
	 * Si un parametre a la valeur <code>null</code> (une chaine de caracteres), alors la valeur de la propriete correspondante est supprimee. Si le
	 * parametre n'est pas defini, la propriete n'est pas modifiee.
	 * 
	 * @see #filterNetSettings(String)
	 */
	private void initNetSettings() {
		setSystemProperty(
				"http.proxyHost", config().stringForKey(CktlConfig.CONFIG_NET_PROXY_HOST_KEY), "null");
		setSystemProperty(
				"http.proxyPort", config().stringForKey(CktlConfig.CONFIG_NET_PROXY_PORT_KEY), "null");
		String value = filterProxyHostsList(config().stringForKey(CktlConfig.CONFIG_NET_NO_PROXY_HOSTS_KEY));
		setSystemProperty(
				"http.nonProxyHosts", value, "null");
		setSystemProperty(
				"ftp.nonProxyHosts", value, "null");
	}

	/**
	 * Format correctement la liste des adresse pour lesquelles le proxy ne peut pas etre utilisee. Cette methode remplace les separateurs "|" et ","
	 * par le seul separateur "|". Elle ellimine egalement tous les doublons, s'ils se trouvent dans la chaine.
	 */
	private String filterProxyHostsList(String hosts) {
		if (hosts == null)
			return null;
		// On decompose la chaine. "," et "|" sont consideres comme les separateurs
		StringTokenizer st = new StringTokenizer(hosts, ",|");
		Vector hv = new Vector();
		String value;
		// On ellimine les doublons (java n'aime pas ca)
		while (st.hasMoreTokens()) {
			value = StringCtrl.normalize(st.nextToken());
			if ((value.length() > 0) && (!hv.contains(value)))
				hv.addElement(value);
		}
		// On recontruit la chaine correcte
		StringBuffer hl = new StringBuffer();
		for (int i = 0; i < hv.size(); i++) {
			if (hl.length() > 0)
				hl.append("|");
			hl.append(hv.elementAt(i));
		}
		return hl.toString();
	}

	/**
	 * Associe la valeur <code>value</code> a la propriete du systeme Java <code>propName</code>. Si <code>nullValue</code> n'est pas
	 * <code>null</code> alors la definition de la propriete est supprimee si <code>value</code> est egale a <code>nullValue</code>. Si
	 * <code>value</code> est <code>null</code>, aucune modification n'est faite.
	 */
	private void setSystemProperty(String propName, String value,
			String nullValue) {
		try {
			if (value != null) {
				if ((nullValue != null) && value.equals(nullValue))
					System.setProperty(propName, StringCtrl.emptyString());
				else
					System.setProperty(propName, value);
			}
		} catch (Throwable ex) {
			StringBuffer em = new StringBuffer();
			em.append("Setting error for system property ").append(propName).append(" : ");
			em.append(ex.getClass().getName()).append(" : ").append(ex.getMessage());
			CktlLog.rawLog(em.toString());
		}
	}

	/**
	 * Effectue les operations d'initialisation de l'application. Cette methode est executee qu'une seule fois. Si une classe surchage cette methode,
	 * elle doit contenir l'appel a <code>super().initApplication</code>. <br/>
	 * Cette methode effectue les appels suivant, dans l'ordre :
	 * <ul>
	 * <li>{@link #checkAppParams()}</li>
	 * <li>{@link #initNetSettings()}</li>
	 * </ul>
	 * Si un des param&egrave;tres obligatoires d&eacute;finit dans link {@link #configMandatoryKeys()} est absent, alors l'application est
	 * arret&eacute;e. De plus, si la m&eacute;thode {@link #appCktlVersion()} est surcharg&eacute;e et retourne une instance de {@link A_CktlVersion}
	 * , alors le controle de version s'op&egrave;re.<br/>
	 * <br/>
	 * 
	 * @see #initNetSettings()
	 */
	public void initApplication() {
		setPageRefreshOnBacktrackEnabled(true);
		// Definie temporairement le time zone, pour ne pas avoir les messages d'erreurs
		// au debut d'initialisation. Sera change a la fin de l'initialisation -
		// appel a resetTimeZone()
		NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName(
				CktlConfig.DEFAULT_TIME_ZONE_VALUE, true));
		// Les objets de la connection avec la base de donnees
		setEcFactory();
		EOEditingContext ec = ERXEC.newEditingContext();
		((ERXEC) ec).setCoalesceAutoLocks(false);
		((ERXEC) ec).setUseAutoLock(false);
		ec.lock();
		try {
			if (useDatabase()) {
				resetAllModels();
				if (mainEntityName() != null) {
					CktlDataBus.initBus(mainEntityName());
					//
					dataBus = getNewDataBus(ec);
				}
				config().setDatabase(dataBus, configTableName());
			}
			if (useGed()) {
				gedBus = getNewGedBus(config());
			}
			if (useMail()) {
				mailBus = getNewMailBus(config());
			}
			// Les parametres des connexions reseau
			initNetSettings();
			// Les parametres de timeZone
			initTimeZone();

			// analyse des modeles
			EOModelCtrl.scanModels();
			// arrete de l'application si manque des parametres
			if (!checkAppParams()) {
				CktlLog.rawLog("Il manque des parametres obligatoire de l'application - ARRET");
				System.exit(-1);
				return;
			}
			checkDependencies();

			// initialisation de la classe DateCtrl
			DateCtrl.initStaticFields(
					config().stringForKey(CktlConfig.CONFIG_APP_DATECTRL_ADDITIONAL_HOLIDAY_KEY),
					config().stringForKey(CktlConfig.CONFIG_APP_DATECTRL_IGNORING_HOLIDAY_KEY));
			// fix des fonctionnalites buggees de WO
			fixWOBugs();
			fixEOFBugs();
			// fix pour que les WebServerResources marchent en javaclient
			//		if (isDirectConnectEnabled()) {
			//			registerRequestHandler(new CktlERXStaticResourceRequestHandler(), "wr");
			//		}
			initJMXBeans();
			// ... On unlock l'ec ayant servi éventuellement
		} finally {
			ec.unlock();
		}
	}

	// ----
	// VERSIONNING
	// ----

	/**
	 * Effectuer le controle de version
	 */
	private void checkDependencies() {
		if (appCktlVersion() == null) {
			CktlLog.rawLog("ATTENTION ! La methode Application.appCktlVersion() n'a pas"
					+ " ete surchargee ... controle de versions impossible !");
		} else {
			try {
				CktlLog.rawLog("Controle des versions\n---------------------\n");
				checkDatabaseVersion();
				appCktlVersion().checkDependencies();
				CktlLog.rawLog("\n");
			} catch (CktlVersionException e) {
				e.printStackTrace();
				System.exit(-1);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	private void checkDatabaseVersion() throws CktlVersionException {
		new DatabaseVersionChecker().check(name());
	}

	/**
	 * Une instance de la classe heritee de {@link A_CktlVersion} descriptive de l'application courante.<br/>
	 * <br/>
	 * <b>Methode a surcharger pour effectuer le contr&ocirc;le de version</b>
	 */
	public A_CktlVersion appCktlVersion() {
		return null;
	}

	/**
	 * Une instance descriptive de la version de la base de donnee courante.<br/>
	 * <br/>
	 * <b>Methode utilis&eacute;e pour l'envoi de la collecte</b><br/>
	 * <br/>
	 * Si cette m&eacute;thode n'est pas surcharg&eacute;e, alors la version de base de donn&eacute;es d&eacute;clar&eacute;e dans la collecte sera
	 * {@link #appCktlVersion()}</code>
	 */
	public A_CktlVersion appCktlVersionDb() {
		return null;
	}

	/**
	 * Retourne la version de l'application. Donne par defaut la version issue de {@link A_CktlVersion}
	 */
	public String version() {
		return (appCktlVersion() != null ? appCktlVersion().version() : null);
	}

	/**
	 * Retrourne le message de copyright de l'application. Donne par defaut le copyright issus de {@link A_CktlVersion}
	 */
	public String copyright() {
		return (appCktlVersion() != null ? appCktlVersion().copyright() : null);
	}

	// affichages - logs

	/**
	 * Affiche des informations sur l'ensemble de ce qui est
	 */
	public void rawLogVersionInfos() {
		if (appCktlVersion() != null) {
			appCktlVersion().rawLogVersions();
		}
	}

	/**
	 * Affiche des informations sur l'application :
	 * <ul>
	 * <li>Le nom de l'application (celui de {@link #appCktlVersion()} si d&eacute;finie, sinon {@link #name})</li>
	 * <li>Date, version, commentaire ... si {@link #appCktlVersion()} est d&eacute;finie</li>
	 * <li>Chemin et nom du fichier de config utilis&eacute;</li>
	 * </ul>
	 */
	public void rawLogAppInfos() {
		final StringBuffer sb = new StringBuffer("Informations application\n------------------------\n\n");
		if (appCktlVersion() != null) {
			sb.append("  > Application : ").append(appCktlVersion().name()).append("\n");
			if (!StringCtrl.isEmpty(appCktlVersion().date())) {
				sb.append("  > Date : ").append(appCktlVersion().date()).append("\n");
			}
			sb.append("  > Version : ").append(appCktlVersion().version()).append("\n");
			if (!StringCtrl.isEmpty(appCktlVersion().comment())) {
				sb.append("  > Commentaire : ").append(appCktlVersion().comment()).append("\n");
			}
		} else {
			sb.append("  > Application : ").append(name()).append("\n");
		}
		if (configFilePath() != null) {
			sb.append("  > Config : ").append(configFilePath()).append("\n");
		}
		sb.append("\n");
		CktlLog.rawLog(sb.toString());
	}

	/**
	 * Afficher des informations sur les modeles utilis&eacute;s par l'application. Pour chacun d'entre eux s'affiche :
	 * <ul>
	 * <li>L'url de connexion a la base de donn&eacute;es</li>
	 * <li>Le nom de l'instance associ&eacute;e</li>
	 * <li>Le user ORACLE qui utilise la connexion</li>
	 * <li>Le chemin sur le filesystem du modele</li>
	 * </ul>
	 */
	public void rawLogModelInfos() {
		final NSMutableDictionary mdlsDico = EOModelCtrl.getModelsDico();
		final StringBuffer sb = new StringBuffer("Informations modeles\n--------------------\n\n");
		final Enumeration mdls = mdlsDico.keyEnumerator();
		while (mdls.hasMoreElements()) {
			final String mdlName = (String) mdls.nextElement();
			EOModel model = (EOModel) mdlsDico.objectForKey(mdlName);
			sb.append("  > Modele ").append(mdlName).append(" :\n");
			sb.append("    * Connexion base de donnees : ").append(EOModelCtrl.bdConnexionUrl(model)).append("\n");
			sb.append("    * Instance : ").append(EOModelCtrl.bdConnexionServerId(model)).append("\n");
			sb.append("    * User base de donnees : ").append(EOModelCtrl.bdConnexionUser(model)).append(" (modelId SAUT : ").append(model.userInfo().valueForKey("modelId")).append(")\n");
			sb.append("    * Chemin : ").append(model.pathURL()).append("\n");
			sb.append("\n");
		}
		sb.append("\n");
		CktlLog.rawLog(sb.toString());
	}

	// controles

	/**
	 * Verification que tous les modeles pointent vers le meme SID de base de donn&eacute;es.<br/>
	 * 
	 * @return <code>true</code> si tout est OK. <code>false</code> si les instances diff&eacute;rent.
	 */
	public boolean checkModel() {
		String sid = null;
		boolean erreurSid = false;
		EOModelGroup vModelGroup = EOModelGroup.defaultGroup();
		for (int i = 0; i < vModelGroup.models().count(); i++) {
			EOModel tmpEOModel = (EOModel) vModelGroup.models().objectAtIndex(i);
			if (!tmpEOModel.name().equalsIgnoreCase("DBA") && !tmpEOModel.name().equalsIgnoreCase("erprototypes") && !tmpEOModel.name().equalsIgnoreCase("FwkCktlB2bCxml")) {
				if (sid == null) {
					sid = EOModelCtrl.bdConnexionServerId(tmpEOModel);
				} else if (EOModelCtrl.bdConnexionServerId(tmpEOModel) == null
						|| !sid.toUpperCase().equals(EOModelCtrl.bdConnexionServerId(tmpEOModel).toUpperCase())) {
					erreurSid = true;
				}
			}
		}
		if (erreurSid) {
			CktlLog.rawLog("Les modeles pointent vers differentes instances de base de donnees (cf. ci-dessus)"
					+ " ceci peut causer des problemes d'incoherence lors de l'execution - ERREUR");
			return false;
		}
		return true;
	}

	/**
	 * Definit la liste des parametres obligatoires d&eacute;finies dans la config de l'application, c'est &agrave; dire
	 * r&eacute;cup&eacute;r&eacute;es via la m&eacute;thode {@link #config()}<br/>
	 * Si au moins un des param&egrave;tres est absent, alors la m&eacute;thode {@link #terminate()} est appel&eacute;e. <br/>
	 * exemple : return new String[] {"GEDFS_SERVICE_HOST", "GEDFS_SERVICE_PORT"};
	 * 
	 * @return un tableau de <code>String</code>
	 */
	public String[] configMandatoryKeys() {
		return null;
	}

	/**
	 * Definit la liste des parametres facultatifs d&eacute;finis dans la config de l'application, c'est &agrave; dire r&eacute;cup&eacute;r&eacute;es
	 * via la m&eacute;thode {@link #config()} <br/>
	 * exemple : return new String[] {"APP_ADMIN_MAIL"};
	 * 
	 * @return un tableau de {@link String}
	 */
	public String[] configOptionalKeys() {
		return null;
	}

	private static final String SB_SEPARATOR = ", ";

	/**
	 * Controle et affiche les parametres obligatoires et facultatifs de l'application, via les m&eacute;thodes {@link #configMandatoryKeys()} et
	 * {@link #configOptionalKeys()}.
	 * 
	 * @return <code>false</code> si une des variables obligatoires est absente, <code>true</code> sinon
	 */
	private boolean checkAppParams() {

		StringBuffer sb = new StringBuffer("Controle de la presence des parametres de l'application\n"
				+ "-------------------------------------------------------\n\n");

		// variables obligatoires
		StringBuffer sbMandatoryMissing = new StringBuffer();
		boolean hasMandatoryMissing = false;
		Hashtable hMandatoryFound = new Hashtable();
		String[] keys = configMandatoryKeys();
		if (keys != null && keys.length > 0) {
			for (int i = 0; i < keys.length; i++) {
				String key = keys[i];
				Object value = config().valueForKey(key);
				if (config().valueForKey(key) == null) {
					sbMandatoryMissing.append(key).append(SB_SEPARATOR);
					hasMandatoryMissing = true;
				} else {
					hMandatoryFound.put(key, value);
				}
			}
			// enlever le dernier separateur et ajouter le message d'erreur
			if (sbMandatoryMissing.length() > 0) {
				int sbSize = sbMandatoryMissing.length();
				sbMandatoryMissing.replace(sbSize - SB_SEPARATOR.length(), sbSize, "");
				sbMandatoryMissing.insert(0, "  > Parametres OBLIGATOIRES absents : ");
				sbMandatoryMissing.append(" - ERREUR");
			} else {
				sbMandatoryMissing.insert(0, "  > Tous les parametres OBLIGATOIRES sont presents - OK");
			}
			sb.append(sbMandatoryMissing).append("\n");
			sb.append(getFormatted(hMandatoryFound));
		}

		// variables facultatives
		StringBuffer sbOptionalMissing = new StringBuffer();
		Hashtable hOptionalFound = new Hashtable();
		keys = configOptionalKeys();
		if (keys != null && keys.length > 0) {
			for (int i = 0; i < keys.length; i++) {
				String key = (String) keys[i];
				Object value = config().valueForKey(key);
				if (config().valueForKey(key) == null) {
					sbOptionalMissing.append(key).append(SB_SEPARATOR);
				} else {
					hOptionalFound.put(key, value);
				}
			}
			// enlever le dernier separateur et ajouter le message d'erreur
			if (sbOptionalMissing.length() > 0) {
				int sbSize = sbOptionalMissing.length();
				sbOptionalMissing.replace(sbSize - SB_SEPARATOR.length(), sbSize, "");
				sbOptionalMissing.insert(0, "  > Parametres FACULTATIFS absents : ");
				sbOptionalMissing.append(" - ATTENTION");
			} else {
				sbOptionalMissing.insert(0, "  > Tous les parametres FACULTATIFS sont presents - OK");
			}
			sb.append(sbOptionalMissing).append("\n");
			sb.append(getFormatted(hOptionalFound));
		}
		CktlLog.rawLog(sb.toString());

		return !hasMandatoryMissing;
	}

	private static String createKeyValue(final String key, final Object value, int keyLength) {
		if (key.length() > keyLength) {
			keyLength = key.length();
		}
		return StringCtrl.extendWithChars(key, " ", keyLength, false) + " = " + value;
	}

	/**
	 * Afficher le contenu format cl&eacute; = valeur d'une {@link Hashtable}
	 */
	private static StringBuffer getFormatted(Hashtable hashtable) {
		// determiner la taille de cle la plus long
		int maxLength = 0;
		Enumeration enumKeys = hashtable.keys();
		NSArray arrKeys = new NSArray();
		while (enumKeys.hasMoreElements()) {
			String key = (String) enumKeys.nextElement();
			arrKeys = arrKeys.arrayByAddingObject(key);
			if (key.length() > maxLength) {
				maxLength = key.length();
			}
		}

		// affichage
		StringBuffer sb = new StringBuffer();
		// classement alphabetique
		try {
			arrKeys = arrKeys.sortedArrayUsingComparator(NSComparator.AscendingCaseInsensitiveStringComparator);
		} catch (ComparisonException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < arrKeys.count(); i++) {
			String key = (String) arrKeys.objectAtIndex(i);
			String value = (String) hashtable.get(key);
			sb.append("    * ").append(createKeyValue(key, value, maxLength)).append("\n");
		}
		sb.append("\n");
		return sb;
	}

	/**
	 * Effectue les traitements qui sont buggees dans certaines versions de WebObjects ... - mot de passe de la Direct Action "wa/WOEventDisplay" et
	 * "wa/WOEventSetup" qui n'est pas pris en compte avec le parametre <em>EOEventLoggingPassword</em>
	 */
	private void fixWOBugs() {
		String eventLogginPassword = System.getProperty("EOEventLoggingPassword");
		if (!StringCtrl.isEmpty(eventLogginPassword)) {
			com.webobjects.eocontrol.EOEventCenter.setPassword(eventLogginPassword);
		}
	}

	/**
	 * Bug pouvant arriver lors d'enregistrements mettant en jeu des relations vers des sub entities (en tout cas héritage vertical). Pour plus de
	 * détails, voir : http://lists.apple.com/archives/webobjects-dev/2013/May/msg00248.html
	 */
	private void fixEOFBugs() {
		for (EOModel model : ERXModelGroup.defaultGroup().models()) {
			for (EOEntity entity : model.entities()) {
				for (EORelationship relationship : entity.relationships()) {
					EOEntity destination = relationship.destinationEntity();
					if (destination != null && destination.parentEntity() != null) {
						relationship.anyInverseRelationship();
					}
				}
			}
		}
	}

	/**
	 * Initialisation d'un MBean pour accéder aux statistiques d'utilisation de l'application. Attention cette fonctionnalité n'est disponible qu'en
	 * wo54.
	 */
	private void initJMXBeans() {
		if (ERXApplication.isWO54()) {
			try {
				String mbeanClassName = "com.webobjects.appserver.WOStatisticsStore";
				registerMBean((Object) statisticsStore(), getJMXDomain(), mbeanClassName);
			} catch (Exception iae) {
				CktlLog.log("WARNING: impossible de demarer le JMX monitoring à cause de " + iae.getMessage() + "\n");
				iae.printStackTrace();
			}
		}
	}

    /**
     * Est-ce que la base de données est une base de données migrée avec flyway ou avec des scripts SQL livrés ?
     * <ul>
     * <li>si non, la base est construite avec des scripts SQL livrés et donc qui mettent à jour les tables DB_VERSION. On peut contrôler les versions des BDD et on empeche le démarrage de l'application si les tests échouent.
     * <li>si oui, la base est construite avec flyway et les tables DB_VERSION ne sont pas mises à jour. Les contrôles peuvent tout de même être effectués mais ne doivent pas empecher le démarrage de l'application si les tests échouent.
     * </ul>
     * 
     * @return oui, la base est construite avec flyway, non sinon
     */
	public boolean isDbFlyway() {
		if (dbFlyway == null) {
			dbFlyway = config().booleanForKey(CktlConfig.CONFIG_ORG_COCKTAIL_DB_FLYWAY);
		}
		
		return dbFlyway;
	}

	/**
	 * On initialize la factory d'ec éventuellement renseignée.
	 */
	private void setEcFactory() {
		String paramKey = "er.extensions.ERXEC.factoryClassName";
		String className = ERXProperties.stringForKey(paramKey);
		try {
			if (!ERXStringUtilities.stringIsNullOrEmpty(className)) {
				Class<?> factoryClazz = Class.forName(className);
				Factory factory = (Factory) factoryClazz.newInstance();
				ERXEC.setFactory(factory);
			}
		} catch (Exception e) {
			log.warn("Erreur lors de l'instantiation de la factory " + className, e);
		}
	}

}
