package org.cocktail.fwkcktlwebapp.server;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * 
 * Extraction dans une classe de la generation des URL de validation CAS.
 * Permet de fixer la faille :
 * https://lists.wisc.edu/read/messages?id=33836937
 * 
 * Correctif inspiré librement de :
 * 
 * https://github.com/Jasig/java-cas-client/commit/ae37092100c8eaec610dab6d83e5e05a8ee58814
 * 
 * @author alexistual
 *
 */
public class CasUrlGenerator {

	public final static String ACTION_PARAM_TICKET = "ticket";
	public final static String ACTION_PARAM_SERVICE = "service";
	
	/**
	 * @param casValidateUrl l'URL de validation des tickets CAS
	 * @param ticket le ticket
	 * @param serviceEncoded l'URL du service, encodé au préalablement (code existant)...
	 * 
	 * @return l'URL complete de validation du ticket en fonction du service
	 */
	public String generateCompleteValidateUrl(String casValidateUrl, String ticket, String serviceUrlEncoded) {
		String ticketEncoded = encode(ticket);
		StringBuffer sb = new StringBuffer();
		sb.append(casValidateUrl).append("?" + ACTION_PARAM_SERVICE + "=");
		sb.append(serviceUrlEncoded).append("&" + ACTION_PARAM_TICKET + "=").append(ticketEncoded);
		return sb.toString();
	}

	/**
	 * @param chaine la chaine à encoder
	 * @return la chaine encodé
	 */
	public String encode(String chaine) {
		String result = chaine;
		try {
			result = URLEncoder.encode(chaine, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			throw new RuntimeException("Impossible d'encoder la chaine : " + chaine, ex);
		}
		return result;
	}
	
}
