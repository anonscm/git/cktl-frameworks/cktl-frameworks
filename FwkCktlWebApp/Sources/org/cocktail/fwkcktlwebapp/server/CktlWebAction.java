package org.cocktail.fwkcktlwebapp.server;

/*
 * Copyright CRI - Universite de La Rochelle, 2002-2005 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.CktlXMLNode;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;
import org.cocktail.fwkcktlwebapp.server.components.CktlJnlpDownload;
import org.cocktail.fwkcktlwebapp.server.util.CktlJnlpGenerator;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOCookie;
import com.webobjects.appserver.WORedirect;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSComparator;
import com.webobjects.foundation.NSComparator.ComparisonException;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXDirectAction;
import er.extensions.crypting.ERXCrypterInterface;
import er.extensions.crypting.ERXCrypto;

/**
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public abstract class CktlWebAction
		extends ERXDirectAction {
	public final static Logger logger = Logger.getLogger(CktlWebAction.class);
	/**
	 * La reference vers l'application en cours.
	 */
	public static CktlWebApplication cktlApp = (CktlWebApplication) WOApplication.application();

	/**
	 * Indique explicitement si le service CAS doit etre utilise. Si cette valeur n'est pas definie, alors la configuration de l'application est
	 * utilisee.
	 */
	private static Boolean useCasService;

	/**
	 * Le URL de serveur de CAS. En general, c'est un URL HTTPS.
	 */
	private static String casServiceURL;

	/**
	 * Le URL de l'action de login de CAS.
	 */
	private static String casLoginURL;

	/**
	 * Le URL de l'action de la deconexion de CAS.
	 */
	private static String casLogoutURL;
	
	/**
	 * L'URL de l'action de la déconnexion lorsqu'on est authentifié par la méthode SQL
	 */
	private static String sqlLogoutURL;

	/**
	 * Le URL de l'action de la validation des tickets de connexion de CAS.
	 */
	private static String casValidateURL;

	//  /**
	//   * Le URL de l'application local. Il est utilise pour recevoir les
	//   * reponses de CAS.
	//   */
	//  private String localServiceURL;

	/**
	 * Le URL ou le client doit etre redirige apres la deconnexion de service CAS. Si se parametre n'est pas defini, le client restera sur la page de
	 * "logout" affichee par le serveur CAS.
	 */
	private String appLogoutURL;

	private String actionClassName;

	//  // Pour les anciennes versions de Java : com.sun.net.ssl.HostnameVerifier;
	//  // Pour 1.4+ : javax.net.ssl.HostnameVerifier;
	//  private static HostnameVerifier httpsVerifier;

	/**
	 * Definition des parametres pour le support de SSL/HTTPS dans le Java.
	 */
	static {
		System.setProperty("java.protocol.handler.pkgs",
				"com.sun.net.ssl.internal.www.protocol");
		java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
		// Pour les anciennes version Java, il faudrait utiliser
		// les memes classes du package com.sun.net.ssl
		//
		// Redefinit la gestion de la verification lors d'une connexion HTTPS.
		// La nouvelle verification permet d'eviter une exception lorsqu'une
		// connexion est faite sur un site qui est different de celui indique
		// dans le sertificat.
		//     javax.net.ssl.HttpsURLConnection.
		//       setDefaultHostnameVerifier(new HostnameVerifier() {
		//          public boolean verify(String urlHostName, javax.net.ssl.SSLSession sslSession) {
		//            StringBuffer sb = new StringBuffer("ATTENTION ! Le nom du serveur de connexion https (");
		//            sb.append(urlHostName).append(") ne correpond pas au serveur de sertificat (");
		//            sb.append(sslSession.getPeerHost()).append(")");
		//            LRLog.log(sb.toString());
		//            return true;
		//          }
		//       });
	}

	private final static String ACTION_PARAM_WOSID = "wosid";

	private final static CasUrlGenerator CAS_URL_GENERATOR = new CasUrlGenerator();
	
	/**
	 * Cree une nouvelle instance de l'objet direct action.
	 */
	public CktlWebAction(WORequest aRequest) {
		super(aRequest);
		actionClassName();
	}

	/**
	 * Retourne l'objet session en cours. Si la session n'existe pas elle sera creee.
	 */
	public CktlWebSession cktlSession() {
		return (CktlWebSession) session();
	}

	/**
	 * Definit explicitement si le service CAS doit etre utilise dans l'application. Suite a cette appel, les definitions des variables de la
	 * configuration <code>APP_USE_CAS</code> et <code>CAS_USE_CAS</code> seront ignorees.
	 */
	public static void setUseCasService(boolean useCas) {
		useCasService = new Boolean(useCas);
	}

	/**
	 * Indique si le service CAS doit etre utilise dans l'application. Si la methode <code>setUseCasService</code> a ete appellee precedement, alors
	 * la valeur definie par celle-ci sera utilisee. Sinon, les variables <code>APP_USE_CAS</code> et <code>CAS_USE_CAS</code> de l'environement de
	 * configuration de l'application seront utilisee. Dans ce cas, la methode retourne <i>true</i> seulement si <i>les deux</i> sont definies a
	 * <i>YES</i>.
	 */
	public static boolean useCasService() {
		if (useCasService == null)
			return cktlApp.config().booleanForKey(CktlConfig.CONFIG_APP_USE_CAS_KEY) &&
					cktlApp.config().booleanForKey(CktlConfig.CONFIG_CAS_USE_CAS_KEY);
		else
			return useCasService.booleanValue();
	}

	/**
	 * Definit le URL d'acces au service CAS. Cette definition surcharge la definition donnee avec le parametre de configuration
	 * <code>CAS_SERVICE_URL</code>, si elle etait disponible.
	 */
	public static void setCasServiceURL(String url) {
		casServiceURL = url;
	}

	/**
	 * Retourne le URL du service CAS. Le URL est determine suivant ces etapes&nbsp;:
	 * <dl>
	 * <li>definie localement a l'aide de la methode <b>setCasServiceURL</b>, sinon</li>
	 * <li>definie dans la configuration de l'application comme parametre <code>CAS_SERVICE_URL</code>
	 * <li>
	 * </dl>
	 */
	public static String casServiceURL() {
		String u = casServiceURL;
		if (u == null)
			u = cktlApp.config().stringForKey(CktlConfig.CONFIG_CAS_SERVICE_URL_KEY);
		return normalizeURL(u);
	}

	/**
	 * Definit le URL de "login" du service CAS. Cette definition surcharge la definition donnee avec le parametre de configuration
	 * <code>CAS_LOGIN_URL</code>, si elle etait disponible.
	 */
	public static void setCasLoginURL(String url) {
		casServiceURL = url;
	}

	/**
	 * Retourne le URL de "login" du serveur CAS. Le URL est determine suivant ces etapes&nbsp;:
	 * <dl>
	 * <li>definie localement a l'aide de la methode statique <b>setCasLoginURL</b>, sinon</li>
	 * <li>definie dans la configuration de l'application comme parametre <code>CAS_LOGIN_URL</code>, sinon
	 * <li>
	 * <li>genere a partir de l'appel a <code>casServiceURL</code> en y ajoutant "/login"</li>
	 * </dl>
	 */
	public static String casLoginURL() {
		return getURLForCASAction(casLoginURL, CktlConfig.CONFIG_CAS_LOGIN_URL, "login");
	}

	/**
	 * Definit le URL de "logout" du service CAS. Cette definition surcharge la definition donnee avec le parametre de configuration
	 * <code>CAS_LOGOUT_URL</code>, si elle etait disponible.
	 */
	public static void setCasLogoutURL(String url) {
		casServiceURL = url;
	}

	/**
	 * Retourne le URL de "logout" du service de CAS. Le URL est determine suivant ces etapes&nbsp;:
	 * <dl>
	 * <li>definie localement a l'aide de la methode statique <b>setCasLogoutURL</b>, sinon</li>
	 * <li>definie dans la configuration de l'application comme parametre <code>CAS_LOGOUT_URL</code>, sinon
	 * <li>
	 * <li>genere a partir de l'appel a <code>casServiceURL</code> en y ajoutant "/logout"</li>
	 * </dl>
	 */
	public static String casLogoutURL() {
		return getURLForCASAction(casLogoutURL, CktlConfig.CONFIG_CAS_LOGOUT_URL, "logout");
	}
	
	/**
	 * Definit le URL de "validation" du service CAS. Cette definition surcharge la definition donnee avec le parametre de configuration
	 * <code>CAS_VALIDATE_URL</code>, si elle etait disponible.
	 */
	public static void setCasValidateURL(String url) {
		casServiceURL = url;
	}

	/**
	 * Retourne le URL de "validation" du service de CAS. Le URL est determine suivant ces etapes&nbsp;:
	 * <dl>
	 * <li>definie localement a l'aide de la methode statique <b>setCasLogoutURL</b>, sinon</li>
	 * <li>definie dans la configuration de l'application comme parametre <code>CAS_VALIDATE_URL</code>, sinon
	 * <li>
	 * <li>genere a partir de l'appel a <code>casServiceURL</code> en y ajoutant "/serviceValidate"</li>
	 * </dl>
	 */
	public static String casValidateURL() {
		return getURLForCASAction(casValidateURL, CktlConfig.CONFIG_CAS_VALIDATE_URL, "proxyValidate");
	}

	/**
	 * Definit le URL ou l'utilisateur sera redirige apres la deconnection de l'application. En general, ce URL doit indiquer le chemin d'acces a la
	 * page d'acceuil de l'application.
	 */
	public void setAppLogoutURL(String url) {
		appLogoutURL = url;
	}

	/**
	 * Retourne le URL d'acces a l'application. Il peut etre defini explicitement suite à l'appel de la methode <code>setAppLogoutURL</code> ou
	 * detecte automatiquement en appelant la methode <code>getApplicationURL</code> de la classe <code>CktlApplication</code>.
	 */
	public String appLogoutURL() {
		if (appLogoutURL != null)
			return appLogoutURL;
		else
			return cktlApp.getApplicationURL(context());
	}

	private String actionClassName() {
		if (actionClassName == null) {
			actionClassName = getClass().getName();
			CktlLog.trace("Action class complete name : " + actionClassName);
			int i = actionClassName.lastIndexOf(".");
			if (i >= 0)
				actionClassName = actionClassName.substring(i + 1);
			if (actionClassName.equals("DirectAction"))
				actionClassName = StringCtrl.emptyString();
		}
		CktlLog.trace("Action class : \"" + actionClassName + "\"");
		return actionClassName;
	}

	/**
	 * Retourne le URL complet de l'action de <i>login</i> dans l'application. Cette valeur doit etre utilisee comme une reference HREF de lien de
	 * connexion a l'application sur la page d'acceuil, par exemple.
	 */
	public static String getDefaultLoginActionURL(WOContext context) {
		CktlLog.trace(null);
		return getLoginActionURL(context, false, null, false, null);
	}

	/**
   * 
   */
	public static String getLoginActionURL(WOContext context,
			boolean useContext,
			String actionClass,
			boolean keepSession,
			NSDictionary actionParams) {
		// CktlLog.trace("componentActionURL : "+context.componentActionURL());
		// CktlLog.trace("request.adaptorPrefix : "+context.request().adaptorPrefix());
		// CktlLog.trace("request.applicationName : "+context.request().applicationName());
		// CktlLog.trace("request.applicationNumber : "+context.request().applicationNumber());
		// CktlLog.trace("request.applicationURLPrefix : "+context.request().applicationURLPrefix());
		// CktlLog.trace("request.queryString : "+context.request().queryString());
		// CktlLog.trace("request.requestHandlerKey : "+context.request().requestHandlerKey());
		// CktlLog.trace("request.requestHandlerPath : "+context.request().requestHandlerPath());
		// CktlLog.trace("request.requestHandlerPathArray() : "+context.request().requestHandlerPathArray());
		// CktlLog.trace("request.uri : "+context.request().uri());
		StringBuffer url = new StringBuffer();
		url.append(normalizeURL(cktlApp.getApplicationURL(context)));
		// TODO Verifier la constructoin de URL si on n'utilise pas
		// des classes de direct action
		if (useContext && (context != null)) {
			WORequest req = context.request();
			if (req.requestHandlerKey() != null && !req.requestHandlerKey().equals(""))
				url.append(req.requestHandlerKey()).append("/");
			else
				url.append("wa/");
			if (req.requestHandlerPathArray() != null && req.requestHandlerPathArray().count() > 0) {
				String handlerClass = (String) req.requestHandlerPathArray().objectAtIndex(0);
				if (isCASClass(handlerClass)) {
					url.append(handlerClass).append("/");
				}

			}
			url.append("casLogin");
		}
		else {
			url.append("wa/");
			if ((actionClass != null) && isCASClass(actionClass)) {
				//RP : dans le cas ou on a une sous-classe de DirectAction qui est dans un package, on ne conserve que le nom de la classe
				url.append(actionClass.substring(actionClass.lastIndexOf(".") + 1)).append("/");
			}
			url.append("casLogin");
		}
		boolean hasSession = false;
		if (context != null)
			hasSession = context.hasSession();
		CktlLog.trace("hasSession : " + hasSession);
		String sessionID = null;
		if (hasSession && keepSession) {
			sessionID = context.session().sessionID();
			CktlLog.trace("sessionID : " + sessionID);
			//      params.append("wosid=").append(context.session().sessionID());
		}
		String params = getParamsString(sessionID, actionParams);
		if (params.length() > 0)
			url.append("?").append(params);
		CktlLog.trace("loginActionURL : " + url.toString());
		return url.toString();
	}

	private static boolean isCASClass(String className) {
		try {
			Class cls = Class.forName(className);
			return (CktlWebAction.class.isAssignableFrom(cls));
		} catch (Throwable ex) {
			if (logger.isDebugEnabled()) {
				logger.debug(ex.getMessage());
			}
		}
		return false;
	}

	/**
	 * Retourne une URL pour une action CAS demande. Cette methode est reutilise par les autres methodes : <code>casLoginURL</code>,
	 * <code>casLogoutURL</code>, <code>casValidateURL</code>.
	 */
	private static String getURLForCASAction(String localValue,
			String configKey,
			String defaultName) {
		String u = localValue;
		if (u == null)
			u = cktlApp.config().stringForKey(configKey);
		if (u == null)
			u = casServiceURL() + defaultName;
		return u;
	}

	/**
	 * Retourne l'URL locale a l'application que le serveur CAS doit contacter pour retourner la reponse d'authentification.
	 */
	private String getCasCallBackURL(NSDictionary actionParams) {
		String applicationURL = cktlApp.getApplicationURL(context());
		String params;
		StringBuffer action = new StringBuffer(applicationURL);
		// On construit le URL d'acces a l'action de callBack
		if (!applicationURL.endsWith("/")) {
			action.append("/");
		}
		action.append("wa/");
		if (actionClassName().length() > 0) {
			action.append(actionClassName()).append("/");
		}
		action.append("casCallBack");
		// On construit la ligne des parametres
		params = getParamsString(request().sessionID(), actionParams);
		// En fin, on fussionne les deux et on encode le resultat
		if (params.length() > 0) {
			action.append("?").append(params);
		}
		applicationURL = action.toString();
		CktlLog.trace("callbackURL.uncoded : " + applicationURL, true);
		applicationURL = CAS_URL_GENERATOR.encode(applicationURL);
		CktlLog.trace("callbackURL.coded : " + applicationURL, true);
		return applicationURL;
	}

	/**
	 * Retourne le URL local a l'application que le serveur CAS doit contacter pour returner la reponse d'authentification. Utilisé par le nouveau ZAP
	 * proxy CAS (version 0.5.0.0) (le login pass est tape dans ZAP au lieu de CAS)
	 */
	public final WOActionResults getCasCallBackURLAction() {
		WOResponse response = new WOResponse();
		response.setContent(getCasCallBackURL(null));
		return response;
	}

	/**
	 * Retourne la page qui sert de point d'entree principale dans l'application.
	 * <p>
	 * Si la configuration de l'application indique que le service CAS est disponible, alors cette action effectue automatiquement l'authentification
	 * CAS. Pour ceci, la configuration de l'application doit comporter deux parametre definis a <i>true</i> (<i>YES</i>)&nbsp;:
	 * <ul>
	 * <li><code>CAS_USE_CAS</code>&nbsp;: indique que le service CAS est disponible dans le systeme d'information</li>
	 * <li><code>APP_USE_CAS</code>&nbsp;: indique que la gestion de CAS est ajoute dans l'application.</li>
	 * </ul>
	 * <p>
	 * Si un de ces parametres n'est pas defini, la methode permet de retourner la page de connexion "classique" en appelant la methode
	 * <code>noCasPage</code>.
	 * </p>
	 * <p>
	 * <i>Cette methode ne peux pas etre surchargee&nbsp;!</i>
	 * </p>
	 */
	public final WOActionResults casLoginAction() {
		NSDictionary actionParams = getParamsFromRequest(request(), null);
		CktlLog.trace("actionParams : " + actionParams);
		if (useCasService()) {
			CktlLog.trace("request.sessionID : " + request().sessionID());
			CktlLog.trace("context.hasSession : " + context().hasSession());
			StringBuffer sb = new StringBuffer();
			sb.append(casLoginURL()).append("?" + CasUrlGenerator.ACTION_PARAM_SERVICE + "=").append(getCasCallBackURL(actionParams));
			return pageForURL(sb.toString());
		}
		else {
			existingSession();
			WOActionResults noCasPage = loginNoCasPage(actionParams);
			if (noCasPage == null)
				noCasPage =
						loginCasFailurePage(
								"La page de connexion a l'application n'existe pas !", "NO_LOGIN_PAGE");
			return noCasPage;
		}
	}

	/**
	 * Traite la reponse de serveur d'authentification. Recupere le "ticket" de CAS et ensuite l'utilise pour recuperer la reponse validee de serveur.
	 * <p>
	 * <i>Cette methode ne peux pas etre surchargee&nbsp;!</i>
	 * </p>
	 */
	public final WOActionResults casCallBackAction() {
		try {
			String ticket = (String) request().formValueForKey(CasUrlGenerator.ACTION_PARAM_TICKET);
			NSDictionary actionParams = getParamsFromRequest(request(),
					new NSArray<String>(new String[] {
							CasUrlGenerator.ACTION_PARAM_TICKET, CasUrlGenerator.ACTION_PARAM_SERVICE
					}));
			CktlLog.trace("actionParams : " + actionParams);
			String casValidateURL = casValidateURL();
			String serviceURL = getCasCallBackURL(actionParams);
			String casCompleteValidateUrl = CAS_URL_GENERATOR.generateCompleteValidateUrl(casValidateURL, ticket, serviceURL);
			String validateResponse = getContentForSecureURL(casCompleteValidateUrl);
			// Analyse
			CASAnswerParser parser = new CASAnswerParser(validateResponse);
			CktlLog.trace("context.hasSession (before existingSession) : " + context().hasSession());
			existingSession();
			CktlLog.trace("context.hasSession (after existingSession) : " + context().hasSession());
			if (parser.isSuccess())
				return loginCasSuccessPage(parser.netid(), actionParams);
			else
				return loginCasFailurePage(parser.errorMessage(), parser.errorCode());
		} catch (IOException ex) {
			ex.printStackTrace();
			return loginCasFailurePage("CALLBACK_ERROR", CktlLog.getMessageForException(ex));
		}
	}

	/**
	 * Termine la session de connexion avec l'application et le service CAS. Retourne le composant d'acces a la page d'accueil de l'application.
	 */
	public final WOActionResults casLogoutAction() {
		return casLogoutComponent();
	}

	/**
	 * Effectue le traitement necessaire pour ajouter les informations dans un navigateur Web permettant d'effectuer authentification CAS
	 * "automatique".
	 * <p>
	 * Cette methode modifie la reponse envoyee au navigateur afin de reutiliser un ticket CAS deja existant. Le ticket CAS doit etre passe comme
	 * parametre CGI "CASTGC".
	 * </p>
	 * <p>
	 * Cette action peut etre utilisee par une tiers application de l'authentification et de lancement des applications.
	 * </p>
	 */
	public WOActionResults ssoAction() {
		WOResponse response = new WOResponse();
		StringBuffer body = new StringBuffer();
		body.append("<html><head><meta http-equiv=\"refresh\" content=\"0;url=");
		body.append(cktlApp.getApplicationInstanceURL(context()));
		body.append("\"></head></html>");
		response.setHeader("text/html", "Content-type");
		response.setHeader(Integer.toString(body.length()), "Content-Length");
		String castgc = (String) context().request().formValueForKey("CASTGC");
		if (castgc != null) {
			addSSOCookie(response, castgc, casDomain(false));
			addSSOCookie(response, castgc, casDomain(true));
		}
		//
		response.setContent(body.toString());
		return response;
	}

	/**
	 * Ajoute un cookie "CASTGC" dans la reponse <code>response</code> envoye au navigateur Web. Le cookie a la valeur <code>castgc</code> et il est
	 * defini sur le domaine <code>domain</code>.
	 */
	private void addSSOCookie(WOResponse response, String castgc, String domain) {
		WOCookie cookie = new WOCookie("CASTGC", castgc, "/cas", domain, -1, true);
		cookie.setExpires(null);
		response.addCookie(cookie);
		CktlLog.trace("Added Cookie : \n  " + cookie.headerString());
	}

	/**
	 * Retourne le nom (symbolique) du serveur sur lequel s'execute l'application WebObjects.
	 */
	public String fullHostName() {
		return getDomainFromURL(cktlApp.getApplicationURL(context()), false);
	}

	/**
	 * Retourne le nom (symbolique) de serveur CAS. Si la parametre <code>partial</code> est <em>false</em>, alors le nom ne comporte que le domaine.
	 * <p>
	 * Par exemple, si le nom du serveur est "www.univ-lr.fr" alors le nom partiel serait ".univ-lr.fr".
	 * </p>
	 */
	public String casDomain(boolean partial) {
		return getDomainFromURL(casLoginURL(), partial);
	}

	/**
	 * Retourne le nom du server extrait a partir de <code>url</code> donne. Si le parametre <code>partial</code> est <em>true</em> le nom ne comporte
	 * que le nom du domaine.
	 */
	private String getDomainFromURL(String url, boolean partial) {
		if (url != null) {
			int i = url.indexOf("://");
			if (i > 0)
				url = url.substring(i + 3);
			i = url.indexOf(":");
			if (i > 0)
				url = url.substring(0, i);
			i = url.indexOf("/");
			if (i > 0)
				url = url.substring(0, i);
			i = url.indexOf(".");
			if ((i > 1) && partial)
				url = url.substring(i);
		}
		return url;
	}

	/**
	 * Termine la session de connexion avec l'application et le service CAS. Retourne le composant d'acces a la page d'accueil de l'application.
	 */
	public WOComponent casLogoutComponent() {
		String u = appLogoutURL();
		if (useCasService()) {
			StringBuffer sb = new StringBuffer(casLogoutURL());
			if (u != null)
				sb.append("?" + CasUrlGenerator.ACTION_PARAM_SERVICE + "=").append(u);
			u = sb.toString();
		}
		if (context().hasSession())
			context().session().terminate();
		return pageForURL(u);
	}

	/**
	 * Termine la session de connexion avec l'application et retourne le composant d'acces a la page d'accueil de l'application. La session de
	 * connextion avec le service CAS n'est pas termine.
	 */
	public WOActionResults sessionLogoutAction() {
		String id = context().request().sessionID();
		if (id != null) {
			CktlWebSession session =
					(CktlWebSession) cktlApp.sessionStore().checkOutSessionWithID(id, context().request());
			if (session != null)
				session.terminate();
		}
		return pageForURL(appLogoutURL());
	}

	/**
	 * Retourne le composant de redirection vers l'URL donne. Retourne <i>null</i> si <code>url</code> n'est pas defini.
	 */
	protected WOComponent pageForURL(String url) {
		if (url == null)
			return null;
		WORedirect page = (WORedirect) cktlApp.pageWithName("WORedirect", context());
		page.setUrl(url);
		return page;
	}

	/**
	 * Retorne le contenu du document qui correspond a la connexion via URL <code>httpsURL</code>. Ce URL doit utiliser le protocol HTTPS.
	 */
	private String getContentForSecureURL(String httpsURL) throws IOException {
		BufferedReader r = null;
		StringBuffer resp = new StringBuffer();
		try {
			URL u = new URL(httpsURL);
			URLConnection uc = u.openConnection();
			uc.setRequestProperty("Connection", "close");
			r = new BufferedReader(new InputStreamReader(uc.getInputStream()));
			String line;
			while ((line = r.readLine()) != null)
				resp.append(line).append("\n");
		} finally {
			try {
				if (r != null)
					r.close();
			} catch (IOException ex) {
			}
		}
		return resp.toString();
	}

	/**
	 * Retourne le forme normalise d'un URL. Le URL doit se terminer avec le symbole "/".
	 */
	private static String normalizeURL(String url) {
		if (url == null)
			url = "/";
		if (!url.endsWith("/"))
			url += "/";
		return url;
	}

	/**
   *  
   */
	protected static NSDictionary<String, String> getParamsFromRequest(
			WORequest request, NSArray<String> ignoreParams) {
		NSMutableDictionary<String, String> params = new NSMutableDictionary<String, String>();
		NSArray<String> keys = request.formValueKeys();
		String key;
		for (int i = 0; i < keys.count(); i++) {
			key = keys.objectAtIndex(i);
			if ((ignoreParams == null) || (!ignoreParams.containsObject(key)))
				params.setObjectForKey((String) request.formValueForKey(key), key);
		}
		return params;
	}

	/**
   * 
   */
	private static String getParamsString(String sessionID, NSDictionary actionParams) {
		StringBuffer sb = new StringBuffer();
		// On memorise le ID de la session
		if (sessionID != null) {
			// on fait attention a ne pas mettre en double session id ! pour eviter
			// les erreurs CAS, ex : ticket 'ST-1531967-W6Bq1gVCuaKLIwTY4K0h' does not match supplied service
			if (actionParams != null && !actionParams.containsKey(ACTION_PARAM_WOSID)) {
				sb.append(ACTION_PARAM_WOSID + "=").append(sessionID);
			}
		}
		// Les autres parametres de l'action
		if (actionParams != null) {
			NSArray keys = actionParams.allKeys();
			// classement alphabetique pour eviter les erreurs lors du casCallBack
			// (les parametres peuvent etre dans un ordre differents d'un appel a un autre)
			try {
				keys = keys.sortedArrayUsingComparator(NSComparator.AscendingCaseInsensitiveStringComparator);
			} catch (ComparisonException e) {
				e.printStackTrace();
			}
			;
			Object key;
			for (int i = 0; i < keys.count(); i++) {
				if (sb.length() > 0)
					sb.append("&");
				key = keys.objectAtIndex(i);
				sb.append(key).append("=").append(actionParams.objectForKey(key));
				CktlLog.trace("add param " + key + "=" + actionParams.objectForKey(key));
			}
		}
		return sb.toString();
	}

	/**
	 * Retourner la page suite a l'authentification avec succes par le CAS.
	 * <p>
	 * Le parametre <code>netid</code> est l'idetifiant de l'utilisateur reconnu par le serveur CAS. Typiquement, il s'agit de <i>login</i> de
	 * l'utilisateur.
	 * <p>
	 * C'est a la charge de l'utilisateur de se servire de cet identifiant pour recuperer les informations complementaires sur l'utilisateur et les
	 * enregistrer dans la session de connexion avec l'applciation en cours. Dans ce cas, on peut faire l'appel a la classe
	 * <code>database.CktlUserInfo</code>.
	 * </p>
	 */
	public abstract WOActionResults loginCasSuccessPage(String netid, NSDictionary actionParams);

	/**
	 * Retourner la page suite a l'echec de l'authentification par le CAS.
	 * <p>
	 * Les parametres <code>errorMessage</code> et <code>errorCode</code> donnent la description de l'erreur.
	 * </p>
	 * <p>
	 * C'est a la charge de l'utilisateur de generer une page affichant le message d'erreur ou effectuer d'autres traitements.
	 * </p>
	 */
	public abstract WOActionResults loginCasFailurePage(String errorMessage, String errorCode);

	/**
	 * Retourner la page d'entree/login dans l'application dans le cas ou le service CAS n'est pas utilise. Dans ce cas, cette page peut propose sa
	 * propre page de la gestion de login/mot de passe d'utilisateur.
	 * <p>
	 * Le service CAS est utilise lorsque la configuration de l'application contient le parametre APP_USE_CAS=YES. Sinon, le service CAS n'est pas
	 * utilise.
	 * </p>
	 */
	public abstract WOActionResults loginNoCasPage(NSDictionary actionParams);

	/**
	 * Analyseur d'une reponse de serveur CAS. On suppose que le document contient la reponse de CAS version 1 ou 2.
	 * <p>
	 * Le code de cette classe est base sur le code source de client Java disponible sur le site Web de CAS.
	 * </p>
	 */
	private static class CASAnswerParser {
		// Les definitions des balises de document XML - la reponse CAS
		private static String AUTHENTICATION_SUCCESS = "cas:authenticationSuccess";
		private static String AUTHENTICATION_FAILURE = "cas:authenticationFailure";
		private static String PROXY_GRANTING_TICKET = "cas:proxyGrantingTicket";
		private static String USER = "cas:user";

		/**
		 * Indicateur si l'authentification et l'analyse se deroule avec succes.
		 */
		private boolean authenticationSuccess = false;

		/**
		 * Identifiant d'utilisateur sur le reseau. Typiquement, il s'agit de son <i>login</i>.
		 */
		private String netid;

		/**
		 * Identifiant de Proxy Ticket Granting. Voir la doc de CAS 2.
		 */
		private String pgtIou;

		/**
		 * Le code de message d'erreur si elle existe.
		 */
		private String errorCode;

		/**
		 * Le contenu de message d'erreur si elle existe.
		 */
		private String errorMessage;

		/**
		 * Cree une instance de l'analysuer de la reponse CAS <i>answerContent</i>. L'analyse de la reponse CAS est effectuee dans le constructeur,
		 * pendant la creation de l'objet.
		 */
		public CASAnswerParser(String answerContent) {
			if (answerContent.contains("<cas:serviceResponse"))
				parseV2Answer(answerContent);
			else
				parseV1Answer(answerContent);
		}

		/**
		 * Effectue l'analyse de la reponse de CAS, version 2. La reponse est en format XML.
		 */
		private void parseV2Answer(String answerContent) {
			CktlXMLNode rootNode = CktlXMLNode.parse(answerContent);
			CktlXMLNode node = rootNode.findChild(AUTHENTICATION_FAILURE, true);
			// Si le noeud "echec" existe, alors c'est une erreur
			if (node != null) {
				authenticationSuccess = false;
				errorCode = (String) node.getAttributes().get("code");
				errorMessage = node.getCharacters();
				return;
			}
			else {
				// Sinon, on verifie que la reponse "succes" existe bien
				node = rootNode.findChild(AUTHENTICATION_SUCCESS, true);
				// ...et on recupere les infos sur la connexion
				if (node != null) {
					authenticationSuccess = true;
					node = rootNode.findChild(USER, true);
					if (node != null)
						netid = node.getCharacters();
					node = rootNode.findChild(PROXY_GRANTING_TICKET, true);
					if (node != null)
						pgtIou = node.getCharacters();
					return;
				}
			}
			// Sinon, il y a un probleme plus general
			errorCode = "NO_ANSWER";
			errorMessage = "No indication of success or failure from CAS";
			authenticationSuccess = false;
		}

		/**
		 * Effectue l'analyse de la reponse de CAS, version12. La reponse est en format text (premiere ligne "yes/no", deuxiemme ligne "identifiant".
		 */
		private void parseV1Answer(String answerContent) {
			authenticationSuccess = false;
			if (answerContent != null) {
				StringTokenizer st = new StringTokenizer(answerContent);
				if (st.hasMoreTokens()) {
					String line = st.nextToken().toLowerCase();
					if (line.equals("yes")) {
						if (st.hasMoreTokens()) {
							authenticationSuccess = true;
							netid = st.nextToken();
							return;
						}
					}
					else if (line.equals("no")) {
						errorCode = "NO_USER";
						errorMessage = "User not recognized by CAS";
					}
				}
			}
			// Sinon, utilisateur ne peut pas etre reconu
			errorCode = "NO_ANSWER";
			errorMessage = "No indication of success or failure from CAS";
		}

		/**
		 * Indique si l'authentification de l'utilisateur a reussi.
		 */
		public boolean isSuccess() {
			return authenticationSuccess;
		}

		/**
		 * Retourne le code de l'erreur ou <i>null</i> si l'authentification de l'utilisateur reussi.
		 */
		public String errorCode() {
			return errorCode;
		}

		/**
		 * Retourne la description de l'erreur ou <i>null</i> si l'authentification de l'utilisateur reussi.
		 */
		public String errorMessage() {
			return errorMessage;
		}

		/**
		 * Retourne l'identifiant de l'utilisateur reconnu par CAS ou <i>null</i> si l'authentification echoue. Typiquement, l'identifiant correspond
		 * au login de l'utilisateur.
		 */
		public String netid() {
			return netid;
		}

		/**
		 * Retourne le Proxy Granting Ticket ou <i>null</i> si l'authentification echoue.
		 */
		public String pgtIou() {
			return pgtIou;
		}

		//    /**
		//     * Affiche les infos sur l'authentification sur la sortie standard de
		//     * l'application.
		//     */
		//    public String toString() {
		//      StringBuffer sb = new StringBuffer("CAS Parser Info - ");
		//      if (isSuccess()) {
		//        sb.append("Success {NetID : ").append(netid()).append("}");
		//      } else {
		//        sb.append("Failure {Error Code : "+errorCode());
		//        sb.append(", Error Message : "+errorMessage()).append("}");
		//      }
		//      return sb.toString();
		//    }
	}

	private final static String DA_ENCRYPT_TEXT_KEY = "text";
	private final static String DA_ENCRYPT_CRYPTER_KEY = "crypter";

	/**
	 * Methode permettant d'encrypter une chaine via la classe {@link ERXCrypto}.
	 * 
	 * @param text chaine a encrypter (obligatoire)
	 * @param crypter algorithme d'encryption a utiliser (facultatif, a defaut c'est {@link ERXCrypto#BLOWFISH} qui est utilisé).
	 * @return
	 */
	public WOActionResults encryptAction() {
		NSDictionary<String, String> actionParams = getParamsFromRequest(request(), null);

		String text = (String) actionParams.valueForKey(DA_ENCRYPT_TEXT_KEY);

		if (!StringCtrl.isEmpty(text)) {

			String crypter = (String) actionParams.valueForKey(DA_ENCRYPT_CRYPTER_KEY);

			ERXCrypterInterface crypterInstance = null;

			if (!StringCtrl.isEmpty(crypter)) {
				crypterInstance = ERXCrypto.crypterForAlgorithm(crypter);
			}
			else {
				crypterInstance = ERXCrypto.defaultCrypter();
			}

			String encrypted = crypterInstance.encrypt(text);

			String str = "l'encryption donne '" + encrypted + "'";

			CktlLog.log(str);
		}

		CktlAlertPage page = (CktlAlertPage) cktlApp.pageWithName("CktlAlertPage", context());
		page.showMessage(null, "Encryption de chaine de caractere",
				"Veuillez consulter la console pour connaitre le resultat de l'operation",
				null, null, null, CktlAlertPage.INFO, null);
		return page;

	}

	/**
	 * @return Une page permettant de générer un fichier jnlp.
	 * @throws Exception s'il y a un problème
	 */
	public WOActionResults jnlpAction() throws Exception {
		return cktlApp.pageWithName(CktlJnlpDownload.class, context());

	}

	/**
	 * @return Le fichier jnlp généré avec les valeurs récupérées automatiquement
	 * @throws Exception s'il y a un problème
	 */
	public WOActionResults jnlpDefaultAction() throws Exception {
		CktlJnlpGenerator cktlJnlpGenerator = new CktlJnlpGenerator(context(), null, null);
		return cktlJnlpGenerator.genereJnlpResponse(null, null);
	}

}
