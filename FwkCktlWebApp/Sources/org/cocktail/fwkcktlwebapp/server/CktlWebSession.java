package org.cocktail.fwkcktlwebapp.server;


import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.database.CktlUserInfoDB;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORedirect;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.appserver.ERXSession;
import er.extensions.foundation.ERXProperties;

/**
 * Propose les methodes qui peuvent communement etre utilisees dans les
 * sessions des applications Web. Cette classe permet d'initialiser et
 * d'acceder aux differents gestionnaires : l'acces a la base de donnes,
 * l'envoi des messages mails, le depot des documents.
 *
 * <p>Cette classe propose egalement les methodes facilitant la fermeture
 * d'une session et l'acces aux pages d'accueil de l'application ou de
 * site Web principal.</p> 
 */
public class CktlWebSession
  extends ERXSession {
  /**
   * Le raccourcie de reference vers l'application en cours.
   */
  protected final static CktlWebApplication cktlApp = (CktlWebApplication)WOApplication.application();
  
  /**
   * L'objet de la gestion d'acces a la base de donnees.
   */
  private CktlDataBus dataBus;
  
  /**
   * L'objet de la gestion d'acces au serveur de sauvegarde des documents.
   */
  private CktlGedBus gedBus;
  
  /**
   * L'objet de la gestion d'envoi des messages mail.
   */
  private CktlMailBus mailBus;
  
  /**
   * L'objet permettant d'acceder aux informations sur l'utilisateur qui a
   * ouvert la session. Cette informations doit etre initialisee explicitement.
   */
  private CktlUserInfo connectedUserInfo;
  
  /**
   * Le cache des pages sauvegardees dans cette session.
   */
  private NSMutableDictionary woComponents;

  /**
   * Cree une nouvelle session et effectue son initialisation.
   */
  public CktlWebSession() {
    super();
    initSession();
  }

  // Methode pour WO 5.x uniquement.
  /**
   * Cree une nouvelle session.
   */
  public CktlWebSession(String aSessionID) {
    super();
    initSession();
  }

  /**
   * Effectue l'initialisation de la session. Cette methode cree les objets
   * de communication avec la base de donnees ({@link #dataBus()}), de la
   * gestion de depot des documents ({@link #gedBus()}) et d'envoi des
   * messages mail ({@link #mailBus()}).
   * 
   * <p>Cette methode est appelle par le constructeur de la session.</p>  
   */
  public void initSession() {
    woComponents = new NSMutableDictionary();
    dataBus = (cktlApp.useDatabase())?getNewDataBus():null;
    gedBus = (cktlApp.useGed())?getNewGedBus():null;
    mailBus = (cktlApp.useMail())?getNewMailBus():null;
    setStoresIDsInCookies(ERXProperties.booleanForKeyWithDefault("org.cocktail.fwkcktlwebapp.cktlwebsession.storesIDsInCookies", false));
    setStoresIDsInURLs(ERXProperties.booleanForKeyWithDefault("org.cocktail.fwkcktlwebapp.cktlwebsession.storesIDsInURLs", true));

  }
  
  /**
   * Renvoie l'objet de la gestion d'acces a la base de donnees.
   * 
   * <p>Ne redefinissez pas cette methode. Utilisez plutot la methode
   * {@link #getNewDataBus()}, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   * 
   * @see #getNewDataBus()
   */
  public CktlDataBus dataBus() {
    return dataBus;    
  }
  
  /**
   * Cree et retourne une nouvelle instance de gestionnaire d'acces a la base
   * de donnees utilise par la session.
   * 
   * <p>Cette methode peut etre redefinie, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   */
  protected CktlDataBus getNewDataBus() {
    return new CktlDataBus(defaultEditingContext());
  }
  
  /**
   * Renvoie l'objet de la gestion d'envoi des messages mail.
   * 
   * <p>Ne redefinissez pas cette methode. Utilisez plutot la methode
   * {@link #getNewMailBus()}, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   * 
   * @see #getNewMailBus()
   */
  public CktlMailBus mailBus() {
    return mailBus;
  }
  
  /**
   * Cree et retourne une nouvelle instance de gestionnaire d'envoi des
   * messages mail utilise par la session.
   * 
   * <p>Cette methode peut etre redefinie, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   */
  protected CktlMailBus getNewMailBus() {
    return new CktlMailBus(cktlApp.config());
  }

  /**
   * Renvoie l'objet de la gestion de depot des documents.
   * 
   * <p>Ne redefinissez pas cette methode. Utilisez plutot la methode
   * {@link #getNewGedBus()}, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   * 
   * @see #getNewGedBus()
   */
  public CktlGedBus gedBus() {
    if ((gedBus != null) && (connectedUserInfo() != null)) {
      gedBus.setNoIndividu(connectedUserInfo().noIndividu().intValue());
    }
    return gedBus;
  }
  
  /**
   * Renvoie l'objet de la gestion de depot des documents. Le gestionnaire
   * est initialise pour indiquer que le document est depose par la personne
   * dont le numero d'individu est <code>noIndividu</code>. 
   */
  public CktlGedBus gedBus(int noIndividu) {
    if (gedBus != null) {
    	gedBus.setNoIndividu(noIndividu);
    }
    return gedBus;
  }
  
  /**
   * Cree et retourne une nouvelle instance de gestionnaire de depot
   * des documents utilise par la session.
   * 
   * <p>Cette methode peut etre redefinie, si vous souhaitez utiliser votre
   * implementation de gestionnaire.</p>
   */
  protected CktlGedBus getNewGedBus() {
    return new CktlGedBus(cktlApp.config());
  }

  /**
   * Memorise dans la session les informations sur l'utilisateur actuellement
   * connecte a l'application. L'objet <code>userInfo</code> doit deja etre
   * initialise.
   */
  public void setConnectedUserInfo(CktlUserInfo userInfo) {
    connectedUserInfo = userInfo;
  }

  /**
   * Enregistre l'utilisateur avec le login <code>userLogin</code> comme
   * etant connecte a l'application. Cette methode recupere les informations
   * sur l'utilisateur a partir de la base de donnees et ensuite les enregistre
   * dans la session.
   * Dans le cas d'echec, retourne le message d'erreur. Retourne <i>null</i>
   * si la connexion reussi.
   */
  public String setConnectedUser(String userLogin) {
    CktlUserInfoDB ui = new CktlUserInfoDB(cktlApp.dataBus());
    ui.compteForLogin(userLogin, null, true);
    if (!ui.hasError()) {
      setConnectedUserInfo(ui);
      return null;
    }
    return ui.errorMessage();
  }
  
  /**
   * Retourne l'objet contenant les informations sur l'utilisateur actuellement
   * connecte a l'application. Cette methode retourne les informations qui
   * sont precedement memorisees a l'aide de la methode
   * {@link #setConnectedUserInfo(database.CktlUserInfo)}. Retourne <code>null</code>, si aucune
   * information n'a precedement ete memorisee.
   */
  public CktlUserInfo connectedUserInfo() {
    return connectedUserInfo;
  }
  
  /**
   * Retourne une instance de la page avec le nom <code>name</code>.
   * 
   * <p>Cette methode fonctionne comme la methode {@link WOComponent#pageWithName(String)}.
   * Mais en plus, cette methode
   * memorise chaque composant ainsi creee dans le cache de la session.
   * Les appels suivants de cette methode avec le meme nom du composant
   * retourneront la meme instance de l'objet, sans le recreer.</p>  
   * 
   * <p>Cette methode peut etre utilisee pour memoriser et reutiliser les
   * composants et leurs etats : la selection des elements des formulaires,
   * contenu d'une page, etc...</p>
   */
  public WOComponent getSavedPageWithName(String name) {
    WOComponent aComponent = (WOComponent) woComponents.objectForKey(name);
    if (aComponent == null) {
      aComponent = cktlApp.pageWithName(name, context());
      if (aComponent != null) {
        woComponents.setObjectForKey(aComponent, name);
      }
    } else {
      aComponent.ensureAwakeInContext(context());
    }
    return aComponent;
  }
  
/* ==== Pour la connexion et la deconnexion ==== */

  /**
   * Ferme la session en cours et retourne le composant correspondant a la
   * page d'accueil de l'application. Elle fait appel a la methode
   * {@link CktlWebApplication#getApplicationURL(WOContext)} pour
   * determiner le URL de la page d'accueil.
   * 
   * <p>Cette methode peut etre utilisee comme la methode/action de lien
   * de fermeture de l'application.</p>
   * 
   * @see CktlWebApplication#getApplicationURL(WOContext)
   */
  public WOComponent logout() {
    WORedirect nextPage = (WORedirect) cktlApp.pageWithName(
    		WORedirect.class.getName(), context());
    nextPage.setUrl(cktlApp.getApplicationURL(context()));
    terminate();
    return nextPage;
  }

  /**
   * Ferme la session en cours et redirige vers la page principale de site Web.
   * Elle fait appel a la methode  {@link CktlWebApplication#mainWebSiteURL()} 
   * pour determiner le URL de la page d'accueil de site.
   * 
   * <p>Cette methode peut etre utilisee comme la methode/action de lien
   * de fermeture de l'application. Par exemple, le lien associe au logo de site
   * Web.</p>
   * 
   * @see CktlWebApplication#mainWebSiteURL()
   */
  public WOComponent goToMainSite() {
    WORedirect nextPage = (WORedirect) cktlApp.pageWithName(
    		WORedirect.class.getName(), context());
    String url = cktlApp.mainWebSiteURL();
    if (StringCtrl.normalize(url).length() == 0) {
      url = cktlApp.getApplicationURL(context());
    }
    nextPage.setUrl(url);
    terminate();
    return nextPage;
  }
  
	public NSMutableDictionary getWoComponents() {
		return woComponents;
	}

}
