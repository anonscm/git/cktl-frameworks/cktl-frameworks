package org.cocktail.fwkcktlwebapp.common;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;

/**
 * Assure la communication entre la barre de lancement des applications
 * (<i>Plateau</i>) et les applications voulant connaitre le login de
 * l'utilisateur.
 * 
 * <p>Principes&nbsp;:
 * <ul>
 *   <li>la barre contacte le serveur CAS pour authentifier l'utilisateur
 *   est recupere son login&nbsp;;</li>
 *   <li>lors de lancement d'une application, le plateau lui passe comme
 *   parametre un ticket unique (<code>LRAppDockTicket</code>) et le numero
 *   du port TCP/IP (<code>LRAppDockPort</code>)&nbsp;;</li>
 *   <li>l'application contacte la barre au port indique et lui revoie le
 *   ticket&nbsp;;</li>
 *   <li>la barre verifie la validite du ticket et renvoie le login
 *   de l'utilisateur&nbsp;;</li>
 *   <li>le ticket ne peut etre utilise qu'une seule fois.</li>
 * </ul>
 * </p>
 * 
 * <p>Pour communiquer avec la barre et recuperer le nom d'utilisateur,
 * il faut utiliser une des methodes statiques <code>getNetID</code>.
 * Le schema general pourrait etre le suivant&nbsp;:
 * <ul>
 * <li>recuperer les parametre passes en ligne de commande
 * (<code>LRAppDockTicket</code> et <code>LRAppDockPort</code>)&nbsp;;</li>
 * <li>s'ils sont definis, appeler une des methodes statiques <em>getNetID</em>
 * (le paramere <code>host</code> peut etre <em>null</em>)&nbsp;;</li>
 * <li>si la methode retourne le nom d'utilisateur (login), l'utiliser pour
 * pour se connecter a l'application&nbsp;;</li>
 * <li>si l'authentification via le Plateau echoue, utiliser la methode
 * d'authentification classique pour l'application (paneau de login/mot de
 * passe).</li>
 * </ul>
 * </p>
 * 
 * @see #getNetID(String, int, long)
 * @see #getNetID(String, String, String)
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 * @author Marc-Henri DELAVAUD-BOISIS <marc-henri.delavaud-boisis at univ-lr.fr>
 */
public class CktlDockClient {
  
  /** L'adresse de la barre des applications */
  private String dockHost;
  
  /** Le numero de port de la barre des applications */
  private int dockPort;
  
  /**
   * Le ticket que l'application doit utiliser pour recuperer le login
   * de l'utilisateur aupres de la barre.
   */
  private long dockTicket; 
  
  /** Le dernier message d'erreur */
  private String lastErrorMessage;
  
  /**
   * Cree une instance de client pour recupere le login de l'utilisateur
   * a partir de la barre des applications. La barre est disponible sur le
   * serveur <code>host</code> et elle ecute les connexions sur le port avec le
   * numero <code>port</code>. Le <code>ticket</code> est le ticket que
   * l'application doit envoyer au serveur pour obtenir le login.
   * 
   * <p>Si le parametre <code>host</code> est <code>null</code>, alors l'adresse
   * du poste local est utilisee (le dock d'application s'execute en local).</p>
   * 
   * @throws UnknownHostException
   */
	public CktlDockClient(String host, int port, long ticket)
    throws UnknownHostException
  {
    if (host == null)
      this.dockHost = InetAddress.getLocalHost().getHostAddress();
    else
		  this.dockHost = host;
    this.dockPort = port;
    this.dockTicket = ticket;
	}

  /**
   * Cree une instance de client pour recupere le login de l'utilisateur
   * a partir de la barre des applications. La barre est disponible sur le
   * serveur <code>host</code> et elle ecute les connexions sur le port avec le
   * numero <code>port</code>. Le <code>ticket</code> est le ticket que
   * l'application doit envoyer au serveur pour obtenir le login.
   * 
   * <p>Si le parametre <code>host</code> est <code>null</code>, alors l'adresse
   * du poste local est utilisee (la barre s'execute en local).</p>
   * 
   * @throws UnknownHostException
   * @throws NumberFormatException
   * 
   * @see #CktlDockClient(String, int, long)
   */
  public CktlDockClient(String host, String port, String ticket)
    throws NumberFormatException, UnknownHostException
  {
    this(host,
        Integer.valueOf(port).intValue(),
        Long.valueOf(ticket).longValue());
  }

  /**
   * Return le login de l'utilisateur executant l'application. Le login est
   * recupere en se connectant a l'application Plateau qui se trouve sur le
   * serveur <code>host</code> et le numero du port <code>port</code> et en
   * utilisant le <code>ticket</code>. Le parametre <code>host</code> peut etre
   * <code>null</code> auquel cas on considere que le Plateau s'execute en local
   * (<em>localhost</em>).
   * 
   * <p>La methode retourne <em>null</em> si l'authentification via Plateau
   * echoue (le Plateau indisponible, utilisateur inconnu,...).</p>
   */
  public static String getNetID(String host, String port, String ticket) {
		//masquage de l'erreur lorsque le jnlp n'est pas lancé via ZAP 
		if ("*".equals(ticket) && "*".equals(port)) {
			return null;
		}
		try {
      return new CktlDockClient(host, port, ticket).getNetID();
    } catch(Exception ex) {
      traceError(ex);
    }
    return null;
  }
  
  /**
   * Return le login de l'utilisateur executant l'application. Le login est
   * recupere en se connectant a l'application Plateau sur le serveur
   * <code>host</code> et le numero du port <code>port</code> et en utilisant
   * le <code>ticket</code>. Le parametre <code>host</code> peut etre
   * <code>null</code> auquel cas on considere que le Plateau s'execute en local
   * (<em>localhost</em>).
   * 
   * <p>La methode retourne <code>null</code> si l'authentification via Plateau
   * echoue (le Plateau indisponible, utilisateur inconnu,...).</p>
   */
  public static String getNetID(String host, int port, long ticket) {
    try {
      return new CktlDockClient(host, port, ticket).getNetID();
    } catch(Exception ex) {
      traceError(ex);
    }
    return null;
  }

  /**
   * Returne le nom de l'utilisateur (login) connu dans la barre des
   * applications. Returne <code>null</code> si la barre ne peut pas retourner le
   * login ou ne peut pas etre contacte pour d'autres raisons. 
   * 
   * @throws IOException
   * @throws UnknownHostException
   */
  public String getNetID() {
    String netID = null; 
    Socket socket = null;
    DataInputStream din = null;
    DataOutputStream dout = null;
    try {
      // On ouvre une connexion
      socket = new Socket(dockHost, dockPort);
      din = new DataInputStream(socket.getInputStream());
      dout = new DataOutputStream(socket.getOutputStream());
      // On envoie d'abord
      dout.writeBytes("RAWGET /"+Long.toString(dockTicket)+"\n"); // !!!!
      // dout.writeLong(dockTicket);
      // Ensuite, on attends la reponse
      netID = StreamCtrl.readStringFromStream(din);
      // Si le login est vide, alors on remet a null
      // koz l'application s'attend a ca dans ce cas
      if (netID.length() == 0) netID = null;
    } catch(Exception ex) {
      lastErrorMessage = CktlLog.getMessageForException(ex);
      traceError(ex);
    }
    // On ferme toutes les connexions
    StreamCtrl.forceClose(dout);
    StreamCtrl.forceClose(din);
    StreamCtrl.forceClose(socket);
    // Et c'est tout
    return netID;
  }
  
  /**
   * Verfie si des methodes on survenu lors de la recuperation de login
   * de l'utilisateur.
   */
  public boolean hasErrors() {
    return (lastErrorMessage != null);
  }
  
  /**
   * Retourne le dernier message d'erreur survenu ou <code>null</code> si le login
   * a ete recupere sans erreurs. 
   */
  public String getLastErrorMessage() {
    return lastErrorMessage;
  }
  
  /**
   * Affiche le message d'erreur <code>message</code> sur la sortie standard.
   * Le message est affiche uniquement si le niveau de logs est mit a
   * <code>CktlLog.LEVEL_DEBUG</code>.
   */
  private static void traceError(Throwable ex) {
    // A remplacer per System.out si LRLog n'est pas disponible.
    CktlLog.rawLog("[ZAP] "+CktlLog.getMessageForException(ex));
    ex.printStackTrace();
//    LRGUI.errorMessage("Erreur : \n"+message);
  }
}
