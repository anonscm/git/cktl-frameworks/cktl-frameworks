package org.cocktail.fwkcktlwebapp.common;



import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

/**
 * Cette classe propose les methode permettant de gerer les informations sur
 * un utilisateur&nbsp;: nom, prenom, e-mail, login, etc...
 * 
 * <p>Les informations sur un utilisateur peuvent etre initialisees a partir
 * d'une chaine de caracteres (<code>takeFromString</code>) ou des
 * dictionnaires (<code>takeFromDictionary</code> ou
 * <code>takeFromHashtable</code>). Elles peuvent egalement etre initialisees
 * a partir d'une source de donnees en appelant les methodes
 * <code>individuForXXX</code> ou <code>compteForXXX</code>.</p>
 */
public abstract class CktlUserInfo implements UserInfo {
    
  /**
   * La constante indiquant le statut d'utilisateur "personnel".
   * @see #userStatus()
   */
  public static final int STATUS_PERSONNEL = 1;

  /**
   * La constante indiquant le statut d'utilisateur "etudiant".
   * @see #userStatus()
   */
  public static final int STATUS_ETUDIANT = 2;

  /**
   * La constante indiquant le statut d'utilisateur "personne exterieure".
   * @see #userStatus()
   */
  public static final int STATUS_EXTERIEUR = 3;
  
  /**
   * La constante indiquant le statut d'utilisateur "inconnu".
   * @see #userStatus()
   */
  public static final int STATUS_INCONNU = 4;
  
  /**
   * La constant indiquant le code d'une erreur : aucune erreur.
   */
  public static final int ERROR_NONE = 0;

  /**
   * La constant indiquant le code d'une erreur : l'enregistrement sur
   * un individu demande n'existe pas.
   */
  public static final int ERROR_INDIVIDU = 1;

  /**
   * La constant indiquant le code d'une erreur : l'enregistrement de compte
   * demande n'existe pas.
   * 
   * <p>Si le compte est initialise a partir de login, alors cette erreur
   * peut etre interpretee comme "login inconnu".</p>
   */
  public static final int ERROR_COMPTE = 2;

  /**
   * La constant indiquant le code d'une erreur : l'enregistrement de compte
   * avec le mot de passe donne n'existe pas.
   * 
   * <p>Si le compte est initialise a partir de login et de mot de passe,
   * alors cette erreur peut etre interpretee comme "mot de passe
   * incorrect".</p>
   */
  public static final int ERROR_PASSWORD = 3;
  
  /**
   * La constant indiquant le code d'une erreur : une erreur s'est produite lors
   * de la communication avec la base de donnees.
   * 
   * <p>Cette erreur peut survenir lors de la communication avec la base
   * de donnees, par exemple.</p>
   */
  public static final int ERROR_SOURCE = 4;
  
  /**
   * Indique si le mot de passe doit etre considere comme etant crypte.
   */
  private boolean forceCryptedPass = false;
  
  /**
   * Indique s'il faut accepter le mot de passe vide comme un mot de passe
   * valide.
   */
  private boolean acceptEmptyPass = false;
  
  /**
   * Indique le mot de passe de super utilisateur (mot de passe "magique").
   */
  private String rootPass = null;
  
  /**
   * Le nom de l'utilisateur.
   */
  protected String nom;
  
  /**
   * Le prenom de l'utilisateur.
   */
  protected String prenom;
  
  /**
   * Le numero d'individu de l'utilisateur.
   */
  protected Number noIndividu;

  /**
   * Le identificateur de l'utilisateur.
   */
  protected Number persId;
  
  /**
   * L'adrese mail de l'utilisateur.
   */
  protected String email;
  
  /**
   * Le nom de utilisateur (login).
   */
  protected String login;
  
  /**
   * Le numero de compte d'utilisateur.
   */
  protected Number noCompte;
  
  /**
   * L'identifiant de reseau sur lequel l'utilisateur possede un compte.
   */
  protected String vLan;
  
  /**
   * Le nom de domaine d'utilisateur (la valeur apres "@" dans le mail
   * d'utilisateur).
   */
  protected String domaine;
  
  /**
   * Le dictionnaire contenant la liste des services auxquels l'utilisateur
   * appartient. Le dictionnaire contient les codes (cle) et les noms
   * des services (valeurs).
   */
  protected NSMutableDictionary services;
  
  /**
   * Le code de derniere erreur survenu.
   */
  protected int errorCode;
  
  /**
   * Le message de derniere erreure survenue.
   */
  protected String errorMessage;
  
  /**
   * Indique l'etat des calculs en cours. La valeur supperieur indique que
   * les calculs sont en cours d'execution.
   */
  private int currentState;
  
  /**
   * Cree un nouvel objet representant les informations sur un utlisateur.
   * Toutes les informations sont initialisees aux valeurs nulles.
   * 
   * <p>Attention, ce constructeur est propose pour permettre l'heritage
   * de la classe <code>database.CktlUserInfo</code>. Consultez la documentation
   * des classes heritant de <code>database.CktlUserInfo</code> pour connaitre le
   * constructeur qu'il est recommande d'utiliser.</p>
   */
  public CktlUserInfo() {
    currentState = 0;
    startProcessing(true);
  }
  
  /**
   * Marque le debut d'un traitement en cours. Si cette methode est appellee
   * pour la premiere fois ou si le parametre <code>resetOnly</code> est
   * <i>true</i>, alors toutes les informations sur l'utilsateur contenues
   * dans cette objet sont remises a zero.
   * 
   * <p>Chaque appel a cette methode doit etre suivi de l'appel
   * <code>astopProcessing</code>, sauf si <code>resetOnly</code> est
   * <i>true</i>.</p>
   * 
   * @see #stopProcessing()
   */
  protected void startProcessing(boolean resetOnly) {
    if (resetOnly || (currentState == 0)) {
      nom = prenom = null;
      noIndividu = persId = noCompte = null;
      email = login = domaine = vLan = null;
      errorCode = ERROR_NONE;
      errorMessage = null;
      services = new NSMutableDictionary();
    }
    if (!resetOnly) currentState++;
  }
  
  /**
   * Marque la fin d'un traitement en cours. Chaque appel a cette methode doit
   * etre precede de l'appel a <code>startProcessing</code> correspondant.
   * 
   * @see #startProcessing(boolean)
   */
  protected void stopProcessing() {
    if (currentState > 0) currentState--;
  }
  
  /**
   * Retourne le code du message d'erreur survenue suite a l'execution
   * de la derniere operation. Retourne une des valeurs
   * <code>ERROR_<i>XXX</i></code>.
   * 
   * <p>Si la derniere operation est executee sans erreures, cette methode
   * retourne la valeur <code>ERROR_NONE</code>.</p>
   * 
   * @see #errorMessage()
   */
  public int errorCode() {
    return errorCode;
  }
  
  /**
   * Retourne le message d'erreur survenue suite a l'execution de la derniere
   * operation.
   *  
   * <p>Si la derniere operation est executee sans erreures, cette methode
   * retourne <i>null</i>.</p>
   * 
   * @see #errorCode()
   */
  public String errorMessage() {
    return errorMessage;
  }

  /**
   * Memorise le message d'erreur de l'exception <code>ex</code>.
   * Affiche le message d'erreur dans la sortie standard de l'application.
   *
   * @see #errorMessage()
   */
  public void takeError(Throwable ex) {
    if (ex != null) {
      errorMessage = CktlLog.getMessageForException(ex);
      ex.printStackTrace();
    }
  }
  
  /**
   * Definit si tous les mot de passes doivent etre consideres comme etant
   * cryptes. Si les mots de passes ne le sont pas consideres ainsi, lors de
   * test de sa validite, on compare sa valeur cryptee et non-cryptee.
   * 
   * <p>La valeur par defaut est <i>false</i>.</p>
   */
  public void setForceCryptedPass(boolean forceCrypted) {
    forceCryptedPass = forceCrypted;
  }
  
  /**
   * Indique si tous les mot de passe doivent etre consideres comme etant
   * cryptes.
   * 
   * @see #setForceCryptedPass(boolean)
   */
  public boolean forceCryptedPass() {
    return forceCryptedPass;
  }

  /**
   * Definit si le mot de passe vide doit etre accepte comme un mot
   * de passe valide. Cette methode peut etre appellee dans les systemes
   * ou l'identification par le mot de passe uniquement est utilisee.
   * 
   * <p>La valeur par defaut est <i>false</i>.</p>
   */
  public void setAcceptEmptyPass(boolean acceptEmpty) {
    acceptEmptyPass = acceptEmpty;
  }

  /**
   * Indique si le mot de passe vide doit etre accepte comme un mot
   * de passe valide.
   * 
   * @see #setAcceptEmptyPass(boolean)
   */
  public boolean acceptEmptyPass() {
    return acceptEmptyPass;
  }
  
  /**
   * Definit le mot de passe d'administrateur. Il est utilise lorsque les
   * informations sont initialisees a partir d'un login et le mot de passe
   * d'utilisateur. Dans ce cas, le mot de passe saisi sera accepte s'il est
   * aussi egale a celui d'administrateur, meme s'il n'est pas egale au vrai
   * mot de passe d'utilisateur.
   * 
   * <p>La valeur par defaut est <i>null</i> (aucun mot de passe
   * administrateur).</p>
   */
  public void setRootPass(String rootPass) {
    this.rootPass = rootPass; 
  }
  
  /**
   * Donne le mot de passe d'administrateur.
   * 
   * @see #setRootPass(String)
   */
  public String rootPass() {
    return rootPass;
  }
  
  /**
   * Retourne le nom d'utilisateur. Retourne la valeur <i>null</i> si les
   * informations sur l'utilisateur n'ont pas pu etre initialisees.
   */
  public String nom() {
    return nom;
  }

  /**
   * Retourne le prenom d'utilisateur. Retourne la valeur <i>null</i> si les
   * informations sur l'utilisateur n'ont pas pu etre initialisees.
   */
  public String prenom() {
    return prenom;
  }

  /**
   * Retourne le nom suivit de prenom d'utilisateur. Retourne la valeur
   * <i>null</i> si les informations sur l'utilisateur n'ont pas pu etre
   * initialisees.
   */
  public String nomEtPrenom() {
    if ((nom != null) && (prenom != null))
      return (new StringBuffer(nom)).append(" ").append(prenom).toString();
    else
      return null;
  }
  
  /**
   * Retourne le numero d'individu de l'utilisateur. Retourne la valeur
   * <i>null</i> si les informations sur l'utilisateur n'ont pas pu etre
   * initialisees.
   */
  public Number noIndividu() {
    return noIndividu;
  }

  /**
   * Retourne le "code de peronne" de l'utilisateur. Retourne la valeur
   * <i>null</i> si les informations sur l'utilisateur n'ont pas pu etre
   * initialisees.
   */
  public Number persId() {
    return persId;
  }

  /**
   * Retourne le email l'adresse email de la personne. Retourne la valeur
   * <i>null</i> si les informations sur le compte d'utilisateur n'ont
   * pas pu etre initialisees.
   */
  public String email() {
    return email;
  }

  /**
   * Retourne le login de la personne. Retourne la valeur <i>null</i> si les
   * informations sur le compte d'utilisateur n'ont pas pu etre initialisees.
   */
  public String login() {
    return login;
  }

  /**
   * Retourne le numero de compte de la personne. Retourne la valeur
   * <i>null</i> si les informations sur le compte d'utilisateur n'ont
   * pas pu etre initialisees.
   */
  public Number noCompte() {
    return noCompte;
  }

  /**
   * Retourne le code de reseau auquel appartient le compte de la personne.
   * Retourne la valeur <i>null</i> si les informations sur le compte
   * d'utilisateur n'ont pas pu etre initialisees.
   */
  public String vLan() {
    return vLan;
  }

  /**
   * Retourne le nom de domaine du compte de la personne. Retourne la valeur
   * <i>null</i> si les informations sur le compte d'utilisateur n'ont
   * pas pu etre initialisees.
   */
  public String domaine() {
    return domaine;
  }
  
  /**
   * Retourne le dictionnaire des tous les services auxquels l'utilisateur
   * appertient. Le dictionnaire contient les couples : le code de service
   * (la cle) et le nom de service (la valeur).
   * 
   * <p>Retourne le dictionnaire vide si les informations sur l'utilisateur
   * n'ont pas pu etre initialisees ou si l'utilisateur n'appartient a aucun
   * service.</p>
   */
  public NSDictionary services() {
    return services;
  }

  /**
   * Retourne le code du service auquel l'utilisateur appartient. Si
   * l'utilisateur appartient a plusieurs services, alors le code du premier
   * service retrouve est retourne.
   * 
   * <p>Retourne la valeur <i>null</i> si les informations sur l'utilisateur
   * n'ont pas pu etre initialisees ou si l'utilsateur n'appartient a aucun
   * service.</p>
   */
  public String codeService() {
    if (services.count() == 0) return null;
    return (String)services.allKeys().objectAtIndex(0);
  }
  
  /**
   * Retourne le nom du service auquel l'utilisateur appartient. Si
   * l'utilisateur appartient a plusieurs services, alors le nom du premier
   * service retrouve est retourne.
   * 
   * <p>Retourne la valeur <i>null</i> si les informations sur l'utilisateur
   * n'ont pas pu etre initialisees ou si l'utilsateur n'appartient a aucun
   * service.</p>
   */
  public String nomService() {
    if (services.count() == 0) return null;
    return (String)services.allValues().objectAtIndex(0);
  }
  
  /**
   * Initialise les informations sur l'utilisateur a partir d'une chaine
   * de caracteres. Les elements dans cette chaine doivent representer
   * les entres d'un dictionnaire.
   */
  public void takeFromString(String dicoString) {
    if (dicoString == null) return;
    ByteArrayInputStream in = new ByteArrayInputStream(dicoString.getBytes());
    Properties dico = new Properties();
    Properties dicoServices;
    try {
      dico.load(in);
      dicoString = (String)dico.get("services");
      if (dicoString != null) {
        StringCtrl.replace(dicoString, ";", "\n");
        in = new ByteArrayInputStream(dicoString.getBytes());
        dicoServices = new Properties();
        dicoServices.load(in);
        dico.put("services", dicoServices);
      }
      takeFromHashtable(dico);
    } catch (IOException e) {
      // e.printStackTrace();
    }
  }
  
  /**
   * Retourne les informations sur un utilisateur sous forme d'un dictionaire.
   * Retourne <i>null</i> si aucune information n'est disponible.
   */
  public String toString() {
    StringBuffer sb = new StringBuffer();
    Hashtable dico = toHashtable();
    Enumeration en = dico.keys();
    Object key;
    while(en.hasMoreElements()) {
      key = (String)en.nextElement();
      if (!key.equals("services")) {
        if (sb.length() > 0) sb.append("\n");
        sb.append(key).append("=").append(dico.get(key));
      }
    }
    sb.append("\n");
    dico = (Hashtable)dico.get("services");
    if ((dico != null) && (!dico.isEmpty())) {
      sb.append("services=");
      en = dico.keys();
      while(en.hasMoreElements()) {
        key = en.nextElement();
        sb.append(key).append("=").append(dico.get(key)).append(";");
      }
    }
    if (sb.length() == 0) return null;
    return sb.toString();
  }
  
  /**
   * Initialise les informations sur l'utilisateur a partir d'un dictionnaire
   * represente par un objet <code>Hashtable</code> de Java.
   * 
   * @see #takeFromDictionary(NSDictionary)
   */
  public void takeFromHashtable(Hashtable dico) {
  	nom = (String)dico.get("nom");
    prenom = (String)dico.get("prenom");
    noIndividu = (Number)dico.get("noIndividu");
    persId = (Number)dico.get("persId");
    email = (String)dico.get("email");
    login = (String)dico.get("login");
    noCompte = (Number)dico.get("noCompte");
    vLan = (String)dico.get("vLan");
    domaine = (String)dico.get("domaine");
    // Les services
    services.removeAllObjects();
    if (dico.get("services") == null) {
      services.removeAllObjects();
    } else {
      services = new NSMutableDictionary((Map) dico.get("services"), true);
    }
  }
  
  /**
   * Initialise les informations sur l'utilisateur a partir d'un dictionnaire
   * represente par un objet <code>NSDictionary</code>.
   * 
   * @see #takeFromHashtable(Hashtable)
   */
  public void takeFromDictionary(NSDictionary dico) {
  	this.takeFromHashtable(dico.hashtable());
  }
  
  /**
   * Retourne les informations sur l'utilisateur dans un dictionnaire
   * represente par un objet {@link Hashtable} de Java. Le dictionnaire
   * est vide si aucun information sur l'utilisateur n'a ete initialisee.
   * 
   * @see #takeFromHashtable(Hashtable)
   */
  public Hashtable toHashtable() {
    Hashtable dico = new Hashtable();
    if (nom !=null) dico.put("nom", nom);
    if (prenom != null) dico.put("prenom", prenom);
    if (noIndividu != null) dico.put("noIndividu", noIndividu);
    if (persId != null) dico.put("persId", persId);
    if (email != null) dico.put("email", email);
    if (login != null) dico.put("login", login);
    if (noCompte != null) dico.put("noCompte", noCompte);
    if (vLan != null) dico.put("vLan", vLan);
    if (domaine != null) dico.put("domaine", domaine);
    if (services.count() > 0) dico.put("services", services.hashtable());
    return dico;
  }

  /**
   * Indique si des erreur on survenu pendant la derniere operation. Cette
   * methode teste si le code de l'erreur est egale a la constant
   * <code>ERROR_NONE</code>.
   * 
   * @see #errorMessage()
   * @see #errorCode()
   */
  public boolean hasError() {
    return (errorCode != ERROR_NONE);
  }
  
  /**
   * Test si le mot de passe donne <code>enteredPass</code> correspond au
   * mot de passe reel <code>realPass</code>.
   * 
   * <p>Le mot de passe reel peut etre crypte ou non crypte. Si dans le
   * systeme en cours, le mot de passe reel est toujours crypte, alors
   * le parametre <code>alwaysCrypted</code> doit etre <i>true</i>.</p>
   * 
   * <p>Le parametre <code>rootPass</code> indique le mot de passe
   * d'administrtateur. Ce mot de passe doit etre toujours crypte. Si le
   * mot de passe donne est egale a celui d'administrateur, alors il
   * est accepte comme le mot de passe d'utilisateur.</p>
   * 
   * <p>Si dans le systeme en cours, les mot de psse d'utilisateurs
   * ne sont pas disponibles, alors le parametre <code>acceptEmpty</code>
   * doit etre <i>true</i>. Dans ce cas le mot de passe donne
   * <code>enteredPass</code> est accepte meme s'il est vide ou <i>null</i>.</p>
   */
  public static boolean equalsToPass(String enteredPass, String realPass,
                                     String rootPass, boolean acceptEmpty,
                                     boolean alwaysCrypted) {
    if (realPass == null) return false;
    if (enteredPass == null) enteredPass = "";
    // Si on accepte les mots de passes vides...
    if (enteredPass.length() == 0) return acceptEmpty;
    // Tester si ceci n'est pas un mot de passe "root" (d'abord en crypt unix)
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, enteredPass, rootPass)) return true;
    // (puis en crypt MD5)
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_MD5, 	enteredPass, rootPass)) return true;
    // Verifier si le mot de passe est le mot de passe crypte
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, enteredPass, realPass)) return true;
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_MD5, enteredPass, realPass)) return true;
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_SHA1, enteredPass, realPass)) return true;
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_SHA256, enteredPass, realPass)) return true;
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_SHA384, enteredPass, realPass)) return true;
    if (CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_SHA512, enteredPass, realPass)) return true;
    
    // Enfin, on regarde si le mot de passe est en clair (si alwaysCrypted=false)
    return !alwaysCrypted && enteredPass.equals(realPass);
  }
 
/* === Les methodes abstraites a redefinir dans les sous-classes. === */

  /**
   * Indique si le compte de l'utilisateur est local par rapport au reseau
   * de l'etablissement.
   */
  public abstract boolean isNetLocal();
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau "etudiant".
   */
  public abstract boolean isNetEdutiant();
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau
   * "enseigniants/chercheurs".
   */
  public abstract boolean isNetRecherche();
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau
   * "administration".
   */
  public abstract boolean isNetAdmin();
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau "externe".
   * Il s'agit des comptes des personnes externes par rapport a l'etablissement.
   */
  public abstract boolean isNetExterne();

  /**
   * @return Retourne le status d'utilisateur. Cette valeur indique si l'utlisateur
   * appartient au personnel de l'universite, les etudiants, les personnes
   * externes ou bien son statut est inconnu. La valeur retournee correspond
   * a une des constantes <code>STATUS_<i>XXX</i></code>.
   */
  public abstract int userStatus();
  
  /**
   * @return Retourne la liste des status de l'utilisateur. Cette valeur indique si l'utlisateur
   * appartient au personnel de l'universite, les etudiants, les personnes
   * externes ou bien son statut est inconnu. La valeur retournee correspond
   * a une liste des constantes <code>STATUS_<i>XXX</i></code>.
   */
  public abstract ArrayList<Integer> listUserStatus();
  
  /**
   * Initialise les informations sur l'individu a partir de sont numero
   * <code>noIndividu</code>.
   * 
   * <p>Si la valeur <code>useCompte</code> est <i>true</i>, alors cette
   * methode tente d'initialiser les informations sur le comte d'utlisateur,
   * si celui-ci existe.</p>
   * 
   * @see #individuForPersId(Number, boolean)
   */
  public abstract void individuForNoIndividu(Number noIndividu, boolean useCompte);
  
  /**
   * Initialise les informations sur un individu a partir de sont code
   * personnel <code>persId</code>.
   * 
   * <p>Si la valeur <code>useCompte</code> est <i>true</i>, alors cette
   * methode tente d'initialiser les informations sur le comte d'utlisateur,
   * si celui-ci existe.</p>
   * 
   * @see #individuForNoIndividu(Number, boolean)
   */
  public abstract void individuForPersId(Number persId, boolean useCompte);

  /**
   * Initialise les informations sur le compte d'utilisateur a partir de son
   * login et son mot de passe. Si le mot de passe est <i>null</i>, alors
   * la recherche de compte se fait uniquement par le login. Les comptes sont
   * parcurus en suivant l'ordre de priorite des types des reseaux
   * (<code>Vlans</code>).
   * 
   * <p>Si <code>useIndividu</code> est <i>true</i>, alors les informations
   * sur l'utilisateur du compte seront egalement initialisees. Si le compte n'est 
   * pas trouvé, les informations de l'individu ne seront pas récupérées.
   * Pour éviter cela, utiliser plutôt les fonctions individuForPersId et 
   * individuForNoIndividu.</p>
   */
  public abstract void compteForLogin(String login, String pass, boolean useIndividu);
  
  /**
   * Initialise les informations sur le compte d'utilisateur a partir de
   * code personnel <code>presId</code>. Les comptes sont parcurus en suivant
   * l'ordre impose par les types des reseaux (<code>Vlans</code>.
   * 
   * <p>Si <code>useIndividu</code> est <i>true</i>, alors les informations
   * sur l'utilisateur du compte seront egalement initialisees. Si le compte n'est 
   * pas trouvé, les informations de l'individu ne seront pas récupérées.
   * Pour éviter cela, utiliser plutôt les fonctions individuForPersId et 
   * individuForNoIndividu.</p>
   */
  public abstract void compteForPersId(Number persId, boolean useIndividu);

  /**
   * Initialise la liste des services auxquels appartient l'utilisateur
   * avec le numero personnel <code>noIndividu</code>.
   */
  public abstract void servicesForNoIndividu(Number noIndividu);
}
