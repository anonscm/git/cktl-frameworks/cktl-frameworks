// _EOVCptFournis.java
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOVCptFournis.java instead.
package org.cocktail.fwkcktlwebapp.common.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

@SuppressWarnings("all")
public abstract class _EOVCptFournis extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCktlWebApp_VCptFournis";
	public static final String ENTITY_TABLE_NAME = "GRHUM.V_CPT_FOURNIS";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cptOrdre";

	public static final String CPT_LOGIN_KEY = "cptLogin";
	public static final String CPT_ORDRE_KEY = "cptOrdre";
	public static final String CPT_PASSWD_KEY = "cptPasswd";
	public static final String FOU_CODE_KEY = "fouCode";
	public static final String FOU_ORDRE_KEY = "fouOrdre";
	public static final String PERS_ID_KEY = "persId";
	public static final String PERS_LC_KEY = "persLc";
	public static final String PERS_LIBELLE_KEY = "persLibelle";
	public static final String PERS_TYPE_KEY = "persType";

	// Non visible attributes

	// Colkeys
	public static final String CPT_LOGIN_COLKEY = "CPT_LOGIN";
	public static final String CPT_ORDRE_COLKEY = "CPT_ORDRE";
	public static final String CPT_PASSWD_COLKEY = "CPT_PASSWD";
	public static final String FOU_CODE_COLKEY = "FOU_CODE";
	public static final String FOU_ORDRE_COLKEY = "FOU_ORDRE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PERS_LC_COLKEY = "PERS_LC";
	public static final String PERS_LIBELLE_COLKEY = "PERS_LIBELLE";
	public static final String PERS_TYPE_COLKEY = "PERS_TYPE";

	// Non visible colkeys

	// Relationships

	// Create / Init methods

	/**
	 * Creates and inserts a new EOVCptFournis with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cptOrdre
	 * @param fouCode
	 * @param fouOrdre
	 * @return EOVCptFournis
	 */
	public static EOVCptFournis create(EOEditingContext editingContext, java.math.BigDecimal cptOrdre, String fouCode, java.math.BigDecimal fouOrdre) {
		EOVCptFournis eo = (EOVCptFournis) createAndInsertInstance(editingContext);
		eo.setCptOrdre(cptOrdre);
		eo.setFouCode(fouCode);
		eo.setFouOrdre(fouOrdre);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOVCptFournis.
	 *
	 * @param editingContext
	 * @return EOVCptFournis
	 */
	public static EOVCptFournis create(EOEditingContext editingContext) {
		EOVCptFournis eo = (EOVCptFournis) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOVCptFournis localInstanceIn(EOEditingContext editingContext) {
		EOVCptFournis localInstance = (EOVCptFournis) localInstanceOfObject(editingContext, (EOVCptFournis) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOVCptFournis localInstanceIn(EOEditingContext editingContext, EOVCptFournis eo) {
		EOVCptFournis localInstance = (eo == null) ? null : (EOVCptFournis) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cptLogin() {
		return (String) storedValueForKey("cptLogin");
	}

	public void setCptLogin(String value) {
		takeStoredValueForKey(value, "cptLogin");
	}
	public java.math.BigDecimal cptOrdre() {
		return (java.math.BigDecimal) storedValueForKey("cptOrdre");
	}

	public void setCptOrdre(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "cptOrdre");
	}
	public String cptPasswd() {
		return (String) storedValueForKey("cptPasswd");
	}

	public void setCptPasswd(String value) {
		takeStoredValueForKey(value, "cptPasswd");
	}
	public String fouCode() {
		return (String) storedValueForKey("fouCode");
	}

	public void setFouCode(String value) {
		takeStoredValueForKey(value, "fouCode");
	}
	public java.math.BigDecimal fouOrdre() {
		return (java.math.BigDecimal) storedValueForKey("fouOrdre");
	}

	public void setFouOrdre(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "fouOrdre");
	}
	public java.math.BigDecimal persId() {
		return (java.math.BigDecimal) storedValueForKey("persId");
	}

	public void setPersId(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "persId");
	}
	public String persLc() {
		return (String) storedValueForKey("persLc");
	}

	public void setPersLc(String value) {
		takeStoredValueForKey(value, "persLc");
	}
	public String persLibelle() {
		return (String) storedValueForKey("persLibelle");
	}

	public void setPersLibelle(String value) {
		takeStoredValueForKey(value, "persLibelle");
	}
	public String persType() {
		return (String) storedValueForKey("persType");
	}

	public void setPersType(String value) {
		takeStoredValueForKey(value, "persType");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOVCptFournis.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOVCptFournis.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOVCptFournis)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOVCptFournis fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOVCptFournis fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOVCptFournis eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOVCptFournis)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOVCptFournis fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVCptFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOVCptFournis fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOVCptFournis eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOVCptFournis)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVCptFournis fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOVCptFournis fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOVCptFournis eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOVCptFournis ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOVCptFournis createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOVCptFournis.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOVCptFournis.ENTITY_NAME + "' !");
		}
		else {
			EOVCptFournis object = (EOVCptFournis) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOVCptFournis localInstanceOfObject(EOEditingContext ec, EOVCptFournis object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOVCptFournis " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOVCptFournis) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
