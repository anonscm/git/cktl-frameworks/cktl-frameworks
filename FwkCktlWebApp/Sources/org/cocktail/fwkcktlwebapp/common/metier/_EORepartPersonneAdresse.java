// _EORepartPersonneAdresse.java
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EORepartPersonneAdresse.java instead.
package org.cocktail.fwkcktlwebapp.common.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

@SuppressWarnings("all")
public abstract class _EORepartPersonneAdresse extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCktlWebApp_RepartPersonneAdresse";
	public static final String ENTITY_TABLE_NAME = "GRHUM.REPART_PERSONNE_ADRESSE";

	// Attributes

	public static final String ADR_ORDRE_KEY = "adrOrdre";
	public static final String E_MAIL_KEY = "eMail";
	public static final String PERS_ID_KEY = "persId";
	public static final String RPA_PRINCIPAL_KEY = "rpaPrincipal";
	public static final String RPA_VALIDE_KEY = "rpaValide";
	public static final String TADR_CODE_KEY = "tadrCode";

	// Non visible attributes

	// Colkeys
	public static final String ADR_ORDRE_COLKEY = "ADR_ORDRE";
	public static final String E_MAIL_COLKEY = "E_MAIL";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String RPA_PRINCIPAL_COLKEY = "RPA_PRINCIPAL";
	public static final String RPA_VALIDE_COLKEY = "RPA_VALIDE";
	public static final String TADR_CODE_COLKEY = "TADR_CODE";

	// Non visible colkeys

	// Relationships

	// Create / Init methods

	/**
	 * Creates and inserts a new EORepartPersonneAdresse with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param adrOrdre
	 * @param persId
	 * @param rpaPrincipal
	 * @param tadrCode
	 * @return EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse create(EOEditingContext editingContext, java.math.BigDecimal adrOrdre, java.math.BigDecimal persId, String rpaPrincipal, String tadrCode) {
		EORepartPersonneAdresse eo = (EORepartPersonneAdresse) createAndInsertInstance(editingContext);
		eo.setAdrOrdre(adrOrdre);
		eo.setPersId(persId);
		eo.setRpaPrincipal(rpaPrincipal);
		eo.setTadrCode(tadrCode);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EORepartPersonneAdresse.
	 *
	 * @param editingContext
	 * @return EORepartPersonneAdresse
	 */
	public static EORepartPersonneAdresse create(EOEditingContext editingContext) {
		EORepartPersonneAdresse eo = (EORepartPersonneAdresse) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext) {
		EORepartPersonneAdresse localInstance = (EORepartPersonneAdresse) localInstanceOfObject(editingContext, (EORepartPersonneAdresse) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EORepartPersonneAdresse localInstanceIn(EOEditingContext editingContext, EORepartPersonneAdresse eo) {
		EORepartPersonneAdresse localInstance = (eo == null) ? null : (EORepartPersonneAdresse) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public java.math.BigDecimal adrOrdre() {
		return (java.math.BigDecimal) storedValueForKey("adrOrdre");
	}

	public void setAdrOrdre(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "adrOrdre");
	}
	public String eMail() {
		return (String) storedValueForKey("eMail");
	}

	public void setEMail(String value) {
		takeStoredValueForKey(value, "eMail");
	}
	public java.math.BigDecimal persId() {
		return (java.math.BigDecimal) storedValueForKey("persId");
	}

	public void setPersId(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "persId");
	}
	public String rpaPrincipal() {
		return (String) storedValueForKey("rpaPrincipal");
	}

	public void setRpaPrincipal(String value) {
		takeStoredValueForKey(value, "rpaPrincipal");
	}
	public String rpaValide() {
		return (String) storedValueForKey("rpaValide");
	}

	public void setRpaValide(String value) {
		takeStoredValueForKey(value, "rpaValide");
	}
	public String tadrCode() {
		return (String) storedValueForKey("tadrCode");
	}

	public void setTadrCode(String value) {
		takeStoredValueForKey(value, "tadrCode");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EORepartPersonneAdresse.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EORepartPersonneAdresse.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EORepartPersonneAdresse)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EORepartPersonneAdresse fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EORepartPersonneAdresse fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EORepartPersonneAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EORepartPersonneAdresse)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EORepartPersonneAdresse fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EORepartPersonneAdresse fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EORepartPersonneAdresse eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EORepartPersonneAdresse)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORepartPersonneAdresse fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EORepartPersonneAdresse fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EORepartPersonneAdresse eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EORepartPersonneAdresse ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EORepartPersonneAdresse createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EORepartPersonneAdresse.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EORepartPersonneAdresse.ENTITY_NAME + "' !");
		}
		else {
			EORepartPersonneAdresse object = (EORepartPersonneAdresse) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EORepartPersonneAdresse localInstanceOfObject(EOEditingContext ec, EORepartPersonneAdresse object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EORepartPersonneAdresse " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EORepartPersonneAdresse) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
