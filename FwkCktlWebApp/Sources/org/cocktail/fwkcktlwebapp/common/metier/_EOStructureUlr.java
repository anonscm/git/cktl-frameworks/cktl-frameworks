// _EOStructureUlr.java
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOStructureUlr.java instead.
package org.cocktail.fwkcktlwebapp.common.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

@SuppressWarnings("all")
public abstract class _EOStructureUlr extends  EOGenericRecord {
	public static final String ENTITY_NAME = "FwkCktlWebApp_StructureUlr";
	public static final String ENTITY_TABLE_NAME = "GRHUM.STRUCTURE_ULR";

	// Attributes
	public static final String ENTITY_PRIMARY_KEY = "cStructure";

	public static final String C_STRUCTURE_KEY = "cStructure";
	public static final String C_STRUCTURE_PERE_KEY = "cStructurePere";
	public static final String C_TYPE_STRUCTURE_KEY = "cTypeStructure";
	public static final String GRP_ACCES_KEY = "grpAcces";
	public static final String GRP_OWNER_KEY = "grpOwner";
	public static final String GRP_RESPONSABILITE_KEY = "grpResponsabilite";
	public static final String LC_STRUCTURE_KEY = "lcStructure";
	public static final String LL_STRUCTURE_KEY = "llStructure";
	public static final String PERS_ID_KEY = "persId";
	public static final String REF_DECISION_KEY = "refDecision";

	// Non visible attributes

	// Colkeys
	public static final String C_STRUCTURE_COLKEY = "C_STRUCTURE";
	public static final String C_STRUCTURE_PERE_COLKEY = "C_STRUCTURE_PERE";
	public static final String C_TYPE_STRUCTURE_COLKEY = "C_TYPE_STRUCTURE";
	public static final String GRP_ACCES_COLKEY = "GRP_ACCES";
	public static final String GRP_OWNER_COLKEY = "GRP_OWNER";
	public static final String GRP_RESPONSABILITE_COLKEY = "GRP_RESPONSABILITE";
	public static final String LC_STRUCTURE_COLKEY = "LC_STRUCTURE";
	public static final String LL_STRUCTURE_COLKEY = "LL_STRUCTURE";
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String REF_DECISION_COLKEY = "REF_DECISION";

	// Non visible colkeys

	// Relationships
	public static final String TOS_REPART_TYPE_GROUPE_KEY = "tosRepartTypeGroupe";

	// Create / Init methods

	/**
	 * Creates and inserts a new EOStructureUlr with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param cStructure
	 * @param cTypeStructure
	 * @param persId
	 * @return EOStructureUlr
	 */
	public static EOStructureUlr create(EOEditingContext editingContext, String cStructure, String cTypeStructure, java.math.BigDecimal persId) {
		EOStructureUlr eo = (EOStructureUlr) createAndInsertInstance(editingContext);
		eo.setCStructure(cStructure);
		eo.setCTypeStructure(cTypeStructure);
		eo.setPersId(persId);
		return eo;
	}

	/**
	 * Creates and inserts a new empty EOStructureUlr.
	 *
	 * @param editingContext
	 * @return EOStructureUlr
	 */
	public static EOStructureUlr create(EOEditingContext editingContext) {
		EOStructureUlr eo = (EOStructureUlr) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public EOStructureUlr localInstanceIn(EOEditingContext editingContext) {
		EOStructureUlr localInstance = (EOStructureUlr) localInstanceOfObject(editingContext, (EOStructureUlr) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static EOStructureUlr localInstanceIn(EOEditingContext editingContext, EOStructureUlr eo) {
		EOStructureUlr localInstance = (eo == null) ? null : (EOStructureUlr) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String cStructure() {
		return (String) storedValueForKey("cStructure");
	}

	public void setCStructure(String value) {
		takeStoredValueForKey(value, "cStructure");
	}
	public String cStructurePere() {
		return (String) storedValueForKey("cStructurePere");
	}

	public void setCStructurePere(String value) {
		takeStoredValueForKey(value, "cStructurePere");
	}
	public String cTypeStructure() {
		return (String) storedValueForKey("cTypeStructure");
	}

	public void setCTypeStructure(String value) {
		takeStoredValueForKey(value, "cTypeStructure");
	}
	public String grpAcces() {
		return (String) storedValueForKey("grpAcces");
	}

	public void setGrpAcces(String value) {
		takeStoredValueForKey(value, "grpAcces");
	}
	public java.math.BigDecimal grpOwner() {
		return (java.math.BigDecimal) storedValueForKey("grpOwner");
	}

	public void setGrpOwner(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "grpOwner");
	}
	public String grpResponsabilite() {
		return (String) storedValueForKey("grpResponsabilite");
	}

	public void setGrpResponsabilite(String value) {
		takeStoredValueForKey(value, "grpResponsabilite");
	}
	public String lcStructure() {
		return (String) storedValueForKey("lcStructure");
	}

	public void setLcStructure(String value) {
		takeStoredValueForKey(value, "lcStructure");
	}
	public String llStructure() {
		return (String) storedValueForKey("llStructure");
	}

	public void setLlStructure(String value) {
		takeStoredValueForKey(value, "llStructure");
	}
	public java.math.BigDecimal persId() {
		return (java.math.BigDecimal) storedValueForKey("persId");
	}

	public void setPersId(java.math.BigDecimal value) {
		takeStoredValueForKey(value, "persId");
	}
	public String refDecision() {
		return (String) storedValueForKey("refDecision");
	}

	public void setRefDecision(String value) {
		takeStoredValueForKey(value, "refDecision");
	}

	public NSArray tosRepartTypeGroupe() {
		return (NSArray)storedValueForKey("tosRepartTypeGroupe");
	}

	public NSArray tosRepartTypeGroupe(EOQualifier qualifier) {
		return tosRepartTypeGroupe(qualifier, null, false);
	}

	public NSArray tosRepartTypeGroupe(EOQualifier qualifier, boolean fetch) {
		return tosRepartTypeGroupe(qualifier, null, fetch);
	}

	public NSArray tosRepartTypeGroupe(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
		NSArray results;
				if (fetch) {
			EOQualifier fullQualifier;
						EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe.TO_STRUCTURE_ULR_KEY, EOQualifier.QualifierOperatorEqual, this);
			
			if (qualifier == null) {
				fullQualifier = inverseQualifier;
			}
			else {
				NSMutableArray qualifiers = new NSMutableArray();
				qualifiers.addObject(qualifier);
				qualifiers.addObject(inverseQualifier);
				fullQualifier = new EOAndQualifier(qualifiers);
			}

						results = org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe.fetchAll(editingContext(), fullQualifier, sortOrderings);
					}
		else {
				results = tosRepartTypeGroupe();
		if (qualifier != null) {
			results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
		}
		if (sortOrderings != null) {
			results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
		}
				}
				return results;
	}
  
	public void addToTosRepartTypeGroupeRelationship(org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe object) {
		addObjectToBothSidesOfRelationshipWithKey(object, "tosRepartTypeGroupe");
	}

	public void removeFromTosRepartTypeGroupeRelationship(org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartTypeGroupe");
	}

	public org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe createTosRepartTypeGroupeRelationship() {
		EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkCktlWebApp_RepartTypeGroupe");
		EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
		editingContext().insertObject(eo);
		addObjectToBothSidesOfRelationshipWithKey(eo, "tosRepartTypeGroupe");
		return (org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe) eo;
	}

	public void deleteTosRepartTypeGroupeRelationship(org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe object) {
		removeObjectFromBothSidesOfRelationshipWithKey(object, "tosRepartTypeGroupe");
				editingContext().deleteObject(object);
			}

	public void deleteAllTosRepartTypeGroupeRelationships() {
		Enumeration objects = tosRepartTypeGroupe().immutableClone().objectEnumerator();
		while (objects.hasMoreElements()) {
			deleteTosRepartTypeGroupeRelationship((org.cocktail.fwkcktlwebapp.common.metier.EORepartTypeGroupe)objects.nextElement());
		}
	}

	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _EOStructureUlr.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _EOStructureUlr.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns EOStructureUlr)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static EOStructureUlr fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOStructureUlr fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		EOStructureUlr eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static EOStructureUlr fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static EOStructureUlr fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		EOStructureUlr eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (EOStructureUlr)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOStructureUlr fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static EOStructureUlr fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		EOStructureUlr eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet EOStructureUlr ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static EOStructureUlr createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_EOStructureUlr.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _EOStructureUlr.ENTITY_NAME + "' !");
		}
		else {
			EOStructureUlr object = (EOStructureUlr) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static EOStructureUlr localInstanceOfObject(EOEditingContext ec, EOStructureUlr object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The EOStructureUlr " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (EOStructureUlr) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
