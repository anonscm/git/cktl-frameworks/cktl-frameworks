// _DBATabColumns.java
/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to DBATabColumns.java instead.
package org.cocktail.fwkcktlwebapp.common.metier;

import java.util.NoSuchElementException;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

@SuppressWarnings("all")
public abstract class _DBATabColumns extends  EOGenericRecord {
	public static final String ENTITY_NAME = "DBATabColumns";
	public static final String ENTITY_TABLE_NAME = "SYS.DBA_TAB_COLUMNS";

	// Attributes

	public static final String COLUMN_NAME_KEY = "columnName";
	public static final String DATA_LENGTH_KEY = "dataLength";
	public static final String DATA_PRECISION_KEY = "dataPrecision";
	public static final String DATA_SCALE_KEY = "dataScale";
	public static final String DATA_TYPE_KEY = "dataType";
	public static final String OWNER_KEY = "owner";
	public static final String TABLE_NAME_KEY = "tableName";

	// Non visible attributes

	// Colkeys
	public static final String COLUMN_NAME_COLKEY = "COLUMN_NAME";
	public static final String DATA_LENGTH_COLKEY = "DATA_LENGTH";
	public static final String DATA_PRECISION_COLKEY = "DATA_PRECISION";
	public static final String DATA_SCALE_COLKEY = "DATA_SCALE";
	public static final String DATA_TYPE_COLKEY = "DATA_TYPE";
	public static final String OWNER_COLKEY = "OWNER";
	public static final String TABLE_NAME_COLKEY = "TABLE_NAME";

	// Non visible colkeys

	// Relationships

	// Create / Init methods

	/**
	 * Creates and inserts a new DBATabColumns with non null attributes and mandatory relationships.
	 *
	 * @param editingContext
	 * @param columnName
	 * @param dataLength
	 * @param dataPrecision
	 * @param dataScale
	 * @param dataType
	 * @param owner
	 * @param tableName
	 * @return DBATabColumns
	 */
	public static DBATabColumns create(EOEditingContext editingContext, String columnName, Integer dataLength, Integer dataPrecision, Integer dataScale, String dataType, String owner, String tableName) {
		DBATabColumns eo = (DBATabColumns) createAndInsertInstance(editingContext);
		eo.setColumnName(columnName);
		eo.setDataLength(dataLength);
		eo.setDataPrecision(dataPrecision);
		eo.setDataScale(dataScale);
		eo.setDataType(dataType);
		eo.setOwner(owner);
		eo.setTableName(tableName);
		return eo;
	}

	/**
	 * Creates and inserts a new empty DBATabColumns.
	 *
	 * @param editingContext
	 * @return DBATabColumns
	 */
	public static DBATabColumns create(EOEditingContext editingContext) {
		DBATabColumns eo = (DBATabColumns) createAndInsertInstance(editingContext);
		return eo;
	}

	// Utilities methods

	public DBATabColumns localInstanceIn(EOEditingContext editingContext) {
		DBATabColumns localInstance = (DBATabColumns) localInstanceOfObject(editingContext, (DBATabColumns) this);
		if (localInstance == null) {
			throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
		}
		return localInstance;
	}

	public static DBATabColumns localInstanceIn(EOEditingContext editingContext, DBATabColumns eo) {
		DBATabColumns localInstance = (eo == null) ? null : (DBATabColumns) localInstanceOfObject(editingContext, eo);
		if (localInstance == null && eo != null) {
			throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
		}
		return localInstance;
	}

	// Accessors methods

	public String columnName() {
		return (String) storedValueForKey("columnName");
	}

	public void setColumnName(String value) {
		takeStoredValueForKey(value, "columnName");
	}
	public Integer dataLength() {
		return (Integer) storedValueForKey("dataLength");
	}

	public void setDataLength(Integer value) {
		takeStoredValueForKey(value, "dataLength");
	}
	public Integer dataPrecision() {
		return (Integer) storedValueForKey("dataPrecision");
	}

	public void setDataPrecision(Integer value) {
		takeStoredValueForKey(value, "dataPrecision");
	}
	public Integer dataScale() {
		return (Integer) storedValueForKey("dataScale");
	}

	public void setDataScale(Integer value) {
		takeStoredValueForKey(value, "dataScale");
	}
	public String dataType() {
		return (String) storedValueForKey("dataType");
	}

	public void setDataType(String value) {
		takeStoredValueForKey(value, "dataType");
	}
	public String owner() {
		return (String) storedValueForKey("owner");
	}

	public void setOwner(String value) {
		takeStoredValueForKey(value, "owner");
	}
	public String tableName() {
		return (String) storedValueForKey("tableName");
	}

	public void setTableName(String value) {
		takeStoredValueForKey(value, "tableName");
	}


	// Finders

	// Fetching many (returns NSArray)
	
	public static NSArray fetchAll(EOEditingContext editingContext) {
		return _DBATabColumns.fetchAll(editingContext, (NSArray) null);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
		return _DBATabColumns.fetchAll(editingContext, null, sortOrderings);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchAll(editingContext, qualifier, null, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	}

	public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
		EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
		fetchSpec.setIsDeep(true);
		fetchSpec.setUsesDistinct(distinct);
		return (NSArray) editingContext.objectsWithFetchSpecification(fetchSpec);
	}

	// Fetching one (returns DBATabColumns)
	
	/**
	 * Renvoie un objet simple.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie l'objet correspondant a la paire cle/valeur
	 * @throws IllegalStateException  
	 */
	public static DBATabColumns fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws IllegalStateException {
		return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie l'objet correspondant au qualifier.
	 * Si plusieurs objets sont susceptibles d'etre trouves, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier).
	 * Une exception est declenchee si plusieurs objets sont trouves.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouves, une exception est declenchee.
	 * 		   Si aucun objet n'est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static DBATabColumns fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws IllegalStateException {
		NSArray eoObjects = fetchAll(editingContext, qualifier, null);
		DBATabColumns eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else if (count == 1) {
			eoObject = (DBATabColumns)eoObjects.objectAtIndex(0);
		}
		else {
			throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, String, Object, NSArray).
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, null si aucun trouve
	 */
	public static DBATabColumns fetchFirstByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		return fetchFirstByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), null);
	}

	/**
	 * Renvoie le premier objet simple trouve.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static DBATabColumns fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		return fetchFirstByQualifier(editingContext, qualifier, null);
	}

	/**
	 * Renvoie le premier objet simple trouve dans la liste triee.
	 * Pour recuperer un tableau, utiliser fetchAll(EOEditingContext, EOQualifier, NSArray).
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @param sortOrderings
	 * @return Renvoie le premier objet trouve correspondant au qualifier, null si aucun trouve
	 */
	public static DBATabColumns fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
		DBATabColumns eoObject;
		int count = eoObjects.count();
		if (count == 0) {
			eoObject = null;
		}
		else {
			eoObject = (DBATabColumns)eoObjects.objectAtIndex(0);
		}
		return eoObject;
	}  

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param keyName
	 * @param value
	 * @return Renvoie le premier objet trouve correspondant a la paire cle/valeur, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByKeyValue(EOEditingContext, String, Object)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static DBATabColumns fetchFirstRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) throws NoSuchElementException {
		return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}

	/**
	 * Renvoie le premier objet simple obligatoirement trouve.
	 * 
	 * @param editingContext
	 * @param qualifier
	 * @return Renvoie le premier objet trouve correspondant au qualifier, une exception si aucun trouve.
	 * Pour ne pas avoir d'exception, utiliser fetchFirstByQualifier(EOEditingContext, EOQualifier)
	 * @throws NoSuchElementException Si aucun objet trouve
	 */
	public static DBATabColumns fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) throws NoSuchElementException {
		DBATabColumns eoObject = fetchFirstByQualifier(editingContext, qualifier);
		if (eoObject == null) {
			throw new NoSuchElementException("Aucun objet DBATabColumns ne correspond au qualifier '" + qualifier + "'.");
		}
		return eoObject;
	}	

	// FetchSpecs...
	


	// Internal utilities methods for common use (server AND client)...

	private static DBATabColumns createAndInsertInstance(EOEditingContext ec) {
		EOClassDescription classDescription = EOClassDescription.classDescriptionForEntityName(_DBATabColumns.ENTITY_NAME);
		if(classDescription == null) {
			throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + _DBATabColumns.ENTITY_NAME + "' !");
		}
		else {
			DBATabColumns object = (DBATabColumns) classDescription.createInstanceWithEditingContext(ec, null);
			ec.insertObject(object);
			return object;
		}
	}

	private static DBATabColumns localInstanceOfObject(EOEditingContext ec, DBATabColumns object) {
		if(object != null && ec != null) {
			EOEditingContext otherEditingContext = object.editingContext();
			if(otherEditingContext == null) {
				throw new IllegalArgumentException("The DBATabColumns " + object + " is not in an EOEditingContext.");
			}
			else {
				com.webobjects.eocontrol.EOGlobalID globalID = otherEditingContext.globalIDForObject(object);
				return (DBATabColumns) ec.faultForGlobalID(globalID, ec);
			}
		}
		else {
			return null;
		}
	}

}
