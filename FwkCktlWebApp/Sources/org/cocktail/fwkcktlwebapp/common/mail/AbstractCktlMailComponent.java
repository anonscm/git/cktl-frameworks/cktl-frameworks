package org.cocktail.fwkcktlwebapp.common.mail;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOContext;

public abstract class AbstractCktlMailComponent extends CktlWebComponent {
	
	public AbstractCktlMailComponent(WOContext context) {
		super(context);
	}

	public abstract String objet();
	
}
