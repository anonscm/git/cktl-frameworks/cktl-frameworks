package org.cocktail.fwkcktlwebapp.common.mail;

import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXProperties;
import er.javamail.ERMailDeliveryHTML;

public class CktlMailFactory {
	

	public void sendMessage(NSArray<String> to, AbstractCktlMailComponent messageComponent) throws Exception {
		sendMessage(ERXProperties.stringForKey("cktlmailfactory.from"), to, messageComponent);
	}
		
	public void sendMessage(String from, NSArray<String> to, AbstractCktlMailComponent messageComponent) throws Exception {
		
		// Pas de destinataires, pas de mail !
		if(to == null || to.isEmpty()) {
			throw new Exception("La liste des destinataires est vide");
		}
		

		// Utilisation de ERMail pour l'envoi
		ERMailDeliveryHTML mail = new ERMailDeliveryHTML();
		 
		// Mise en place du composant HTML
		mail.setComponent(messageComponent);
		
		mail.newMail();
		mail.setFromAddress(from);
		mail.setSubject(messageComponent.objet());
		mail.setToAddresses(to);
		mail.sendMail();
		
	}
}
