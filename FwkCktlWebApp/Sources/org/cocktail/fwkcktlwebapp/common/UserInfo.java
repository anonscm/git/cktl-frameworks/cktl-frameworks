package org.cocktail.fwkcktlwebapp.common;

/**
 * 
 * Représente les informations minimales de l'utilisateur courant.
 * 
 * @author Alexis Tual
 *
 */
public interface UserInfo {

    /**
     * @return le persId de l'utilisateur courant
     */
    Number persId();

	/**
	   * @return Retourne le nom d'utilisateur. Retourne la valeur <i>null</i> si les
	   * informations sur l'utilisateur n'ont pas pu etre initialisees.
	   */
	   String nom();

	/**
	   * @return Retourne le prenom d'utilisateur. Retourne la valeur <i>null</i> si les
	   * informations sur l'utilisateur n'ont pas pu etre initialisees.
	   */
	   String prenom();

	/**
	   * @return Retourne le nom suivit de prenom d'utilisateur. Retourne la valeur
	   * <i>null</i> si les informations sur l'utilisateur n'ont pas pu etre
	   * initialisees.
	   */
	   String nomEtPrenom();

	/**
	   * @return Retourne le numero d'individu de l'utilisateur. Retourne la valeur
	   * <i>null</i> si les informations sur l'utilisateur n'ont pas pu etre
	   * initialisees.
	   */
	   Number noIndividu();
    
}
