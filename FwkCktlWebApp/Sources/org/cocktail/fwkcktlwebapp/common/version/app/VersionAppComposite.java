package org.cocktail.fwkcktlwebapp.common.version.app;



/**
 * Permet de déterminer les versions selon plusieurs sources. 
 * 
 * @author Alexis Tual
 *
 */
public class VersionAppComposite implements VersionApp {

    private VersionApp versionAppReference;
    
    /**
     * @param versionsApp les versions app
     */
    public VersionAppComposite(VersionApp...versionsApp) {
        setVersionAppReference(versionsApp);
    }

    private void setVersionAppReference(VersionApp...versionsApp) {
        for (VersionApp versionApp : versionsApp) {
            if (!hasNoVersionInfo(versionApp)) {
                versionAppReference = versionApp;
                break;
            }
        }
        if (versionAppReference == null) {
            throw new IllegalArgumentException(
                    "Aucune instance de VersionApp passée en paramètres ne permet de fournir les infos de version");
        }
    }
    
    private boolean hasNoVersionInfo(VersionApp versionApp) {
        return versionApp.majVersion() == 0 
                && versionApp.minVersion() == 0 
                && versionApp.patchVersion() == 0 
                && versionApp.buildVersion() == 0
                && versionApp.dateVersion() == null;
    }
    
    /** 
     * {@inheritDoc}
     */
    public int majVersion() {
        return versionAppReference.majVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int minVersion() {
        return versionAppReference.minVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int patchVersion() {
        return versionAppReference.patchVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int buildVersion() {
        return versionAppReference.buildVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public String dateVersion() {
        return versionAppReference.dateVersion();
    }

}
