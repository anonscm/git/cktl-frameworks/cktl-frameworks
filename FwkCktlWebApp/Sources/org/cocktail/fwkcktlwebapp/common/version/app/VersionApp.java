package org.cocktail.fwkcktlwebapp.common.version.app;


/**
 * 
 * Représente la version d'une application.
 * Ces informations, dans la mesure du possible, doivent être tirées
 * d'artefacts générés et non renseignées "à la main".
 * 
 * Les versions cocktail suivent le pattern <major>.<minor>.<patch>.<build>
 * Si une partie de ce pattern est omise dans l'artefact, les implémentations de 
 * {@link VersionApp} doivent remplacer les éléments manquants par 0.
 * 
 * @see http://semver.org/
 * 
 * @author Alexis Tual
 *
 */
public interface VersionApp {
    
    /**
     * @return la version majeure de l'appli, 0 si omise
     */
    int majVersion();
    
    /**
     * @return la version mineure de l'appli, 0 si omise
     */
    int minVersion();
    
    /**
     * @return la version patch de l'appli, 0 si omise
     */
    int patchVersion();
    
    /**
     * @return la version build de l'appli, 0 si omise
     */
    int buildVersion();
    
    /**
     * @return la date de la version, "" si omise
     */
    String dateVersion();
    
}
