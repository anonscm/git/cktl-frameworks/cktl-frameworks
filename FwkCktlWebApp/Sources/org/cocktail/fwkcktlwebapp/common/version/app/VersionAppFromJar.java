package org.cocktail.fwkcktlwebapp.common.version.app;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionDigitsExtractor;



/**
 * Récupération de la version à partir des informations présentes dans le manifest.mf du Jar.
 * 
 * @author Rodolphe Prin
 */
public class VersionAppFromJar implements VersionApp {
	private String className;
    private VersionDigitsExtractor versionDigitsExtractor;

    /**
	 * Constructeur par défaut
	 * 
	 * @param className nom de la classe contenue dans le jar à analyser.
	 */
	public VersionAppFromJar(String className) {
		this.className = className;
        this.versionDigitsExtractor = new VersionDigitsExtractor(fullVersion());
    }

    /** 
     * {@inheritDoc}
     */
    public String fullVersion() {
		Package jarPackage;
		String jarVersion = "0.0.0.0";
		try {
			jarPackage = Class.forName(className).getPackage();
			jarVersion = jarPackage.getImplementationVersion();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}

		return jarVersion;
    }

    /** 
     * {@inheritDoc}
     */
    public String dateVersion() {
        // TODO : à voir comment récupérer la date de version
        // dans le build
        return "";
    }

    /** 
     * {@inheritDoc}
     */
    public int majVersion() {
        return versionDigitsExtractor.majorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int minVersion() {
        return versionDigitsExtractor.minorVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int patchVersion() {
        return versionDigitsExtractor.patchVersion();
    }

    /** 
     * {@inheritDoc}
     */
    public int buildVersion() {
        return versionDigitsExtractor.buildVersion();
    }
    
}
