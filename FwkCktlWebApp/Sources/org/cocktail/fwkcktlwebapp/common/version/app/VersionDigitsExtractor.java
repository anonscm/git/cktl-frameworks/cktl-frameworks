package org.cocktail.fwkcktlwebapp.common.version.app;

import java.util.StringTokenizer;


/**
 * 
 * Extracteur des digits <major>.<minor>.<patch>.<build>
 * 
 * Si jamais la version données ne se conforte pas à ce 
 * pattern, on renvoie quoi qu'il arrive 4 digits (0.0.0.0 si aucun digit n'est lisible).
 * 
 * @see VersionDigitsExtractorTest
 * 
 * @author Alexis Tual
 *
 */
public class VersionDigitsExtractor {

    private String fullVersion;
    private int[] digits;
    
    /**
     * @param fullVersion la version textuelle complète.
     *          par exemple : 2.0.1.3
     */
    public VersionDigitsExtractor(String fullVersion) {
        this.fullVersion = fullVersion;
    }
    
    private int[] digits() {
        if (digits == null) {
            digits = extractDigits();
        }
        return digits;
    }
    
    /**
     * @return le premier digit correspondant à la version majeure
     */
    public int majorVersion() {
        return digits()[0];
    }
    
    /**
     * @return le second digit correspondant à la version mineure
     */
    public int minorVersion() {
        return digits()[1];
    }
    
    /**
     * @return le troisième digit correspondant à la version de patch
     */
    public int patchVersion() {
        return digits()[2];
    }
    
    /**
     * @return le dernier digit correspondant à la version de build
     */
    public int buildVersion() {
        return digits()[3];
    }
    
    protected int[] extractDigits() {
        int[] digitsTmp = new int[] {0, 0, 0, 0};
        if (fullVersion != null) {
            StringTokenizer tokenizer = new StringTokenizer(fullVersion, ".");
            int i = 0;
            while (tokenizer.hasMoreTokens()) {
                String token = tokenizer.nextToken();
                int digit = 0;
                try {
                    digit = Integer.valueOf(token);
                } catch (NumberFormatException e) {
					System.out.println("Impossible de parser le token '" + token + "' de la version " + fullVersion);
					e.printStackTrace();
                }
                digitsTmp[i] = digit;
                i++;
            }
        }
        return digitsTmp;
    }
    
}
