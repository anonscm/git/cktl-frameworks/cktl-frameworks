package org.cocktail.fwkcktlwebapp.common.test.integ;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 */
public class WebDriverFactory {
	private static WebDriverFactory webDriverFactory;
	private WebDriver sharedWebDriver;

	public static WebDriverFactory getWebDriverFactory() {
		if (webDriverFactory == null) {
			webDriverFactory = new WebDriverFactory();
		}
		return webDriverFactory;
	}

	public WebDriver getSharedWebDriver() {
		if (sharedWebDriver == null) {
			sharedWebDriver = new FirefoxDriver();
		}
		return sharedWebDriver;
	}

}
