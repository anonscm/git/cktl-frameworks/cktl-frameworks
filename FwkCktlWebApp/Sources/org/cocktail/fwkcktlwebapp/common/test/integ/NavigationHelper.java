package org.cocktail.fwkcktlwebapp.common.test.integ;

import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * fournit des fonctions utiles pour naviguer dans Girofle
 */
public class NavigationHelper {
	// Private fields
	private static NavigationHelper navigationHelper;
	// private WebDriver webDriver;

	// Public fields
	public static final String URL_ACCUEIL = System.getProperty("URL_ACCUEIL");

	// Properties
	public WebDriver getWebDriver() {
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();
	}

	/**
	 * renvoie l'assistant à la navigation
	 * @return instance de la classe NavigationHelper
	 */
	public static NavigationHelper getNavigationHelper() {
		if (navigationHelper == null) {
			navigationHelper = new NavigationHelper();
		}
		return navigationHelper;
	}

	// Public Methods
	/**
	 * Se connecte à une application
	 * @param identification : identification de l'utilisateur
	 * @param application titre de l'application
	 * @param motDePasse mot de passe de l'utilisateur
	 */
	public void connection(String identification, String application, String motDePasse) {
		getWebDriver().navigate().to(URL_ACCUEIL);
		getWebDriver().findElement(By.id("LoginId")).sendKeys(identification);
		getWebDriver().findElement(By.name("mot_de_passe")).sendKeys("motDePasse");
		getWebDriver().findElement(By.name("validerLogin")).click();
		assertTrue(getWebDriver().getPageSource().contains(application));
	}

	/**
	 * navigue vers une page en cliquant sur un lien
	 * @param linkText texte du lien
	 */
	public void navigateToPageByLink(String linkText) {
		String xpath = "//a[contains(text(),'" + linkText + "')]";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement link = getWebDriver().findElement(By.xpath(xpath));
		link.click();
	}

	/**
	 * @param label label du bouton
	 * @return le lien du cktlAjaxbutton à partir de son label
	 */
	public void clickOnCktlAjaxActionButton(String label) {
		String xpath = "//span[contains(text(),'"+label+"')]/../../../../../..";
		WebDriverWait wait = new WebDriverWait(getWebDriver(), 5);
		wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(xpath)));

		WebElement button = getWebDriver().findElement(By.xpath(xpath));
		button.click();
	}
	
}
