package org.cocktail.fwkcktlwebapp.common.test.integ;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

import cucumber.api.java.fr.Alors;
import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

/**
 * Steps Cucumber pour l'auhtentification.
 * Correspond au scénario suivant :
 * 
 * <pre>
 *   Soit Toto habilitée à utiliser MonAppli
 *   Lorsqu'elle se connecte avec le login toto et le mot de passe "titi"
 *   Alors elle se retrouve sur la page d'accueil et voit le texte "Bienvenue sur mon application"
 * </pre>
 * 
 * Il est nécessaire de préciser l'URL de la page d'accueil pour exécuter ce steps.
 * 
 * @author Alexis Tual
 *
 */
public class AuthentificationSteps {

    private WebDriver webDriver;
    private static final String URL_ACCUEIL = System.getProperty("URL_ACCUEIL");
    
    @cucumber.api.java.Before
    public void setUp() {
        this.webDriver = new HtmlUnitDriver();
    }
    
    @Soit("^(\\w+) habilité(?:e)? à utiliser (\\w+)$")
    public void habilite_a_utiliser(String identification, String application) {
        webDriver.navigate().to(URL_ACCUEIL);
    }
    
    @Lorsqu("^(?:il|elle) se connecte avec le login (\\w+) et le mot de passe \"([^\\\"]*)\"$")
    public void se_connecte(String login, String motDePasse) {
        webDriver.findElement(By.id("LoginId")).sendKeys(login);
        webDriver.findElement(By.name("mot_de_passe")).sendKeys(motDePasse);
        webDriver.findElement(By.name("validerLogin")).click();
    }
    
    @Alors("^(?:il|elle) se retrouve sur la page d'accueil et voit le texte \"([^\\\"]*)\"$")
    public void elle_se_retrouve_sur_la_page_d_accueil_et_voit_le_texte(String texte) throws IOException {
        assertTrue(webDriver.getPageSource().contains(texte));
    }
    
}
