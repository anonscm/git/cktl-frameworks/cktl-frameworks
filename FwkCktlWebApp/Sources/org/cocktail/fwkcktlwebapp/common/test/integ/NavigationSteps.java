package org.cocktail.fwkcktlwebapp.common.test.integ;

import org.openqa.selenium.WebDriver;

import cucumber.api.java.fr.Lorsqu;
import cucumber.api.java.fr.Soit;

public class NavigationSteps {

	public WebDriver getWebDriver() {
		return WebDriverFactory.getWebDriverFactory().getSharedWebDriver();
	}
	
	@Soit("^\"([^\"]*)\" utilisateur connecté à \"([^\"]*)\" avec le mot de passe \"([^\"]*)\"$")
	public void utilisateur_connecte_a_avec_le_mot_de_passe(String identification, String application, String motDePasse) throws Throwable {
		NavigationHelper.getNavigationHelper().connection(identification, application, motDePasse);
	}

	
	@Lorsqu("^(?:il|elle) arrive sur la page \"([^\"]*)\" en cliquant sur le lien  \"([^\"]*)\"$")
	public void elle_arrive_sur_la_page_en_cliquant_sur_le_lien(String pageTitre, String pageLien) throws Throwable {
		NavigationHelper.getNavigationHelper().navigateToPageByLink(pageLien);
	}
}
