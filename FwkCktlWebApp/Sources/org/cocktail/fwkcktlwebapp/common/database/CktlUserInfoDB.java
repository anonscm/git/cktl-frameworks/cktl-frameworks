package org.cocktail.fwkcktlwebapp.common.database;



import java.math.BigDecimal;
import java.util.ArrayList;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.CktlSort;
import org.cocktail.fwkcktlwebapp.common.CktlUserInfo;
import org.cocktail.fwkcktlwebapp.common.metier.EOAffectation;
import org.cocktail.fwkcktlwebapp.common.metier.EOCompte;
import org.cocktail.fwkcktlwebapp.common.metier.EOIndividuUlr;
import org.cocktail.fwkcktlwebapp.common.metier.EOStructureUlr;
import org.cocktail.fwkcktlwebapp.common.metier.EOTypeCryptage;
import org.cocktail.fwkcktlwebapp.common.metier.EOVlans;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlConfig;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.database.CktlDataBus;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.appserver.WOApplication;
import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;

/**
 * Cette classe propose les methode permettant de gerer les informations sur
 * un utilisateur&nbsp;: nom, prenom, e-mail, login, etc...
 * 
 * <p>Cette classe permet d'initialiser les informations a partir
 * de la base de donnees. Elle donne l'implementation des methodes
 * abstraites de la classe <code>database.CktlUserInfo</code>. Cette classe fait appel
 * aux tables de la base de donnees qui peuvent etre redefinies en appelant
 * la methode (statique) <code>setDataBaseModel</code>.</p>
 */
public class CktlUserInfoDB 
	extends CktlUserInfo {
  /**
   * La liste des codes des reseaux de type "administration de l'universite".
   */
	  private static NSArray VLansAdmin = new NSArray("P");
  
  /**
   * La liste des codes des reseaux de type "administration de l'universite".
   */
	  private static NSArray VLansRecherche = new NSArray("P");

  /**
   * la liste des condes des reseaux de type "etudiants".
   */
  private static NSArray VLansEtudiant = new NSArray("E");
  
//  /**
//   * La liste des codes des reseaux de type "personnes externes a
//   * l'etablissement".
//   */
//  private static NSArray VLansExterieur = LRArray.newArray("X");
//  
  /**
   * La liste des codes des reseaux a ignorer.
   */
  private static NSArray VLansIgnore = new NSArray(new String[] {"G", "D"});
  
  /**
   * Le nom du l'entite du model en cours correspondant a la table
   * <code>GRHUM.INDIVIDU_ULR</code>.
   */
  private static String TableIndividu = EOIndividuUlr.ENTITY_NAME;

  /**
   * Le nom du l'entite du model en cours correspondant a la table
   * <code>GRHUM.STRUCTURE</code>.
   */
  private static String TableStructure = EOStructureUlr.ENTITY_NAME;

  /**
   * Le nom du l'entite du model en cours correspondant a la table
   * <code>GRHUM.COPMPTE</code>.
   */
  private static String TableCompte = EOCompte.ENTITY_NAME;
  
  /**
   * Le nom du l'entite du model en cours correspondant a la table
   * <code>GRHUM.AFFECTATION</code>.
   */
  private static String TableAffectation = EOAffectation.ENTITY_NAME;

  /**
   * Le nom du l'entite du model en cours correspondant a la table
   * <code>GRHUM.V_LANS</code>.
   */
  private static String TableVLans = EOVlans.ENTITY_NAME;
  
  
  /**
   * L'objet de communication avec la base de donnees.
   */
  private _CktlBasicDataBus myDB;

  /**
   * La liste de tous les enregistrements contenant les definitions des
   * reseaux disponibles sur le systeme.
   */
  private static NSMutableArray allVLanRecords;
  
  /**
   * Indique si cette classe est utilisee dans une application qui supporte
   * la gestion de configuration "a la CRI".
   */
  private Boolean hasCktlConfig;

  /**
   * Cree un nouvel objet representant les informations sur un utlisateur.
   * Toutes les informations sont initialisees aux valeurs nulles.
   * 
   * <p>Attention, ce constructeur est propose pour permettre l'heritage
   * de la classe <code>database.CktlUserInfoDB</code>. Utilisez le constructeur prenant
   * comme parametre <code>_CktlBasicDataBus</code> si vous utlisez
   * l'implementation proposee par la classe <code>database.CktlUserInfoDB</code>.
   * 
   * @see #CktlUserInfoDB(_CktlBasicDataBus)
   */
  public CktlUserInfoDB() {
    super();
  }

  /**
   * Cree un nouvel objet representant les informations d'utilisateur.
   * Toutes les informations sont initialisees aux valeurs nulles.
   * 
   * <p>Une fois l'objet cree, il faut faire appel a une des methodes
   * <code>individuForXXX</code> ou <code>compteForXXX</code> pour initialiser
   * effectivement les informations.</p>
   * 
   * @param newDB L'objet de communication avec la base de donnees. Il est
   * utilise pour charger les valeurs a partir de la base de donnes.
   */
  public CktlUserInfoDB(_CktlBasicDataBus newDB) {
    this();
    myDB = newDB;
  }
  
  /**
   * Definit un objet <code>dataBus</code> qui sera utilise pour acceder a la
   * base de donnees. L'implementation donnee demande un objet
   * <code>_CktlBasicDataBus</code> et utilise ces methodes
   * <code>fetchObject</code> et <code>fetchArray</code> pour recuperer les
   * informations a partir de la base de donnees.
   */
  public void setDataBus(_CktlBasicDataBus dataBus) {
    myDB = dataBus;
  }

  /**
   * Definit la structure du model de la base donnees qui sera
   * utilisee pour executer les requetes d'acces a la base. Cette methode doit
   * etre appellee, si la structure de la base est differente de celle
   * propose par defaut (voir les constantes <code>Table<i>XXX</i></code>).
   * 
   * @param tblIndividuUlr Le nom de la table GRHUM.INDIVIDU_ULR dans le modele
   *   de l'application.
   * @param tblStructure Le nom de la table GRHUM.STRUCTURE dans le modele
   *   de l'application.
   * @param tblCompte Le nom de la table GRHUM.COMPTE dans le model
   *   de l'application.
   * @param tblAffectation Le nom de la table GRHUM.AFFECTATION dans le modele
   *   de l'application.
   * @param tblVLans Le nom de la table GRHUM.VLANS dans le modele de
   *   l'application.
   */
  public static void setDataBaseModel(String tblIndividuUlr,
                                      String tblStructure,
                                      String tblCompte,
                                      String tblAffectation,
                                      String tblVLans)
  {
    if (tblIndividuUlr != null) TableIndividu = tblIndividuUlr;
    if (tblStructure != null) TableStructure = tblStructure;
    if (tblCompte != null) TableCompte = tblCompte;
    if (tblAffectation != null) TableAffectation = tblAffectation;
    if (tblVLans != null) TableVLans = tblVLans;
  }
  
  /**
   * Initialise les informations sur un individu a partir de l'enregistrement
   * <code>rec</code>. Il represente un enregistrement de la table indiquee
   * par la variable <code>TableIndividu</code>.
   * 
   * <p>Si la valeur <code>useCompte</code> est <i>true</i>, alors cette
   * methode tente d'initialiser les informations sur le comte d'utlisateur,
   * si celui-ci existe.</p>
   * 
   * @see #individuForNoIndividu(Number, boolean)
   * @see #individuForPersId(Number, boolean)
   */
  public void individuForRecord(EOEnterpriseObject rec, boolean useCompte) {
    startProcessing(false);
    try {
      if (rec != null) {
        takeFromIndividuRec(rec);
        if (noIndividu() != null) {
        	servicesForNoIndividu(noIndividu());
        }
        if (useCompte) {
          compteForPersId((Number) rec.valueForKey(EOIndividuUlr.PERS_ID_KEY), false);
        }
      } else {
        errorCode = ERROR_INDIVIDU;
        errorMessage = "Individu n'existe pas";
      }
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
    stopProcessing();
  }
  
  /**
   * Initialise les informations sur le compte d'utilisateur a partir d'un
   * enregistrement de la table des comptes.
   * 
   * <p>Si <code>useIndividu</code> est <i>true</i>, alors les informations
   * sur l'utilisateur du compte seront egalement initialisees.</p>
   */
  public void compteForRecord(EOEnterpriseObject rec, boolean useIndividu) {
    startProcessing(false);
    try {
      if (rec != null) {
        takeFromCompteRec(rec);
        if (useIndividu) {
          //rec = myDB.fetchObject(TableRepartCompte, _CktlBasicDataBus.newCondition("cptOrdre="+noCompte()));
          if (rec != null) 
            individuForPersId((Number)rec.valueForKey(EOCompte.PERS_ID_KEY), false);
          else
            errorCode = ERROR_INDIVIDU;
            errorMessage = "Individu n'existe pas";
        }
      } else {
        errorCode = ERROR_COMPTE;
        errorMessage = "Compte n'existe pas ou l'erreur de login";
      }
    } catch(Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
    stopProcessing();
  }
  
  /**
   * Initialise les champs de l'objet en cours a partir de l'enregistrement
   * de la table d'individu.
   */
  protected void takeFromIndividuRec(EOEnterpriseObject rec) {
    if (rec != null) {
      nom = StringCtrl.normalize((String) rec.valueForKey(EOIndividuUlr.NOM_USUEL_KEY));
      prenom = StringCtrl.normalize((String) rec.valueForKey(EOIndividuUlr.PRENOM_KEY));
      noIndividu = (Number) rec.valueForKey(EOIndividuUlr.NO_INDIVIDU_KEY);
      persId = (Number) rec.valueForKey(EOIndividuUlr.PERS_ID_KEY);
    }
  }
  
  /**
   * Initialise les champs de l'objet en cours a partir de l'enregistrement
   * de la table de compte.
   */
  protected void takeFromCompteRec(EOEnterpriseObject rec) {
    if (rec != null) {
      noCompte = (Number) rec.valueForKey(EOCompte.CPT_ORDRE_KEY);
      login = (String) rec.valueForKey(EOCompte.CPT_LOGIN_KEY);
      domaine = (String) rec.valueForKey(EOCompte.CPT_DOMAINE_KEY);
      vLan = (String) rec.valueForKey(EOCompte.CPT_VLAN_KEY);
      email = (String) rec.valueForKey(EOCompte.CPT_EMAIL_KEY);
      if ((StringCtrl.normalize(email).length() == 0) 
    		  || (StringCtrl.normalize(domaine).length() == 0)) {
        email = null;
      } else {
        email = email + "@" + domaine;
      }
    }
  }
  
  /**
   * Recherche le compte de l'individu dont le code personnel est
   * <code>persId</code>. Les comptes sont parcurus en suivant l'ordre
   * de priorite des reseaux (<code>Vlans</code>). Le premier compte
   * valide retrouve est retourne.
   */
  public EOEnterpriseObject findCompteForPersId(Number persId) {
  	return findCompte(CktlDataBus.newCondition(EOCompte.PERS_ID_KEY+"=%@", new NSArray(persId)), null);
  }
  
  /**
   * Recherche le compte de l'individu avec le login et le mot de passe
   * donnee. Les comptes sont parcurus en suivant l'ordre de priorite des
   * reseaux (<code>Vlans</code>). Le premier compte valide retrouve est
   * retourne. Si le mot de passe est vide, le compte est recherche
   * uniquement suivant le login donne.
   */
  protected EOEnterpriseObject findCompteForLogin(String login, String pass) {
    return findCompte(_CktlBasicDataBus.newCondition(EOCompte.CPT_LOGIN_KEY + "='" + login + "'"), pass);
  }
  
  
  /**
   * Recherche le compte qui satisfait la condition <code>condition</code>.
   * Cette condition doit etre definit sur la table des comptes.
   * 
   * <p>Si plusieurs comptes sont disponibles, ils sont parcurus en suivant
   * l'ordre de priorite des types des reseaux (<code>Vlans</code>).</p>
   */
  protected EOEnterpriseObject findCompte(EOQualifier condition, String password) {
    NSArray comptes = myDB.fetchArray(TableCompte, condition, null);
    EOEnterpriseObject recCompte = null;
    NSArray vlanCodes = allVLanCodes();
    EOEnterpriseObject rec;
    // On cherche en suivant la priorite des VLans
    for (int i = 0; ((i < vlanCodes.count()) && (recCompte == null)); i++) {
      // On cherche le compte qui correspond a ce Vlan
      for (int j = 0; j < comptes.count(); j++) {
        rec = (EOEnterpriseObject) comptes.objectAtIndex(j);
        if (rec.valueForKey(EOCompte.CPT_VLAN_KEY).equals(vlanCodes.objectAtIndex(i))) {
          // S'il ne faut pas verifier le mot de passe, alors on l'a trouve
          if (password == null) {
            recCompte = rec;
          } else { 
            // Sinon, on verifie le mot de passe
            if (equalsToPass(password, (String) rec.valueForKey(EOCompte.CPT_PASSWD_KEY),
                             rootPass(), acceptEmptyPass(), forceCryptedPass())) {
              recCompte = rec;
            }
          }
          // On verifie la validite de compte
          if (recCompte != null) {
            if (!isCompteValide(recCompte)) {
            	recCompte = null;
            }
          }
          // En fin, si on l'a trouve, alors c'est fini
          if (recCompte != null) {
        	  break;
          }
        }
      }
    }
    // Verdict/Erreur : si le mot de passe est donnee...
    if (password != null) {
      // ...et si des comptes existent, mais aucun n'est selectionne...
      if ((comptes.count() > 0) && (recCompte == null)) {
       // ...alors c'est le pb du mot de passe
        errorCode = ERROR_PASSWORD;
        errorMessage = "Erreur de mot de passe";
      }
    }
    return recCompte;
  }
  
  /**
   * Retourne la liste de tous les enregistrements des definitions des types
   * des reseaux (<i>Vlans</i>). Cette liste est triee par la priorite des
   * reseaux.
   */
  protected NSArray allVLanRecords() {
    if (allVLanRecords == null) {
    	CktlSort sort = CktlSort.newSort(EOVlans.PRIORITE_KEY);
      StringBuffer condition = new StringBuffer();
      // On definit la condition pour ignorer les reseaux... a ignorer
      if ((VLansIgnore != null) && (VLansIgnore.count() > 0)) {
        for (int i = 0; i < VLansIgnore.count(); i++) {
          if (condition.length() > 0) {
        	  condition.append(" and ");
          }
          condition.append(EOVlans.C_VLAN_KEY  + "<>'").append(VLansIgnore.objectAtIndex(i)).append("'");
        }
      }
      // On va faire une copie sous forme des objets NSDictionary
      // pour ne pas avoir des problemes de editing context
      allVLanRecords = new NSMutableArray();
      NSArray records = myDB.fetchArray(TableVLans, _CktlBasicDataBus.newCondition(condition.toString()), sort);
      // TODO Remplacer par une implementation plus economique
      for (int i = 0; i < records.count(); i++) {
        allVLanRecords.addObject(((EOEnterpriseObject) records.objectAtIndex(i)).snapshot());
      }
    }
    return allVLanRecords;
  }
  
  /**
   * Retourne la liste des codes des definitions des types des reseaux
   * (<code>Vlans</code>). Cette liste est triee par la priorite des reseaux.
   */
  protected NSArray allVLanCodes() {
    return (NSArray) allVLanRecords().valueForKey(EOVlans.C_VLAN_KEY);
  }
  
  /**
   * Test si le mot de passe donnee est valide. Cette methode verifie les
   * dates de validite du compte.
   */
  protected boolean isCompteValide(EOEnterpriseObject recCompte) {
    // TODO Ajouter le test de la validite des comptes
    // On dit que ceci n'est pas necessaire
    return true;
  }
 
  /* === Le methodes abstraites de database.CktlUserInfo a redefinir === */

  /**
   * Indique si le compte de l'utilisateur est local par rapport au reseau
   * de l'etablissement. Cette methode compare le code de reseau de compte
   * courant, la valeur retournee par la methode <code>vLan</code>, avec ceux
   * donnes dans le parametre de configuration <code>GRHUM_VLAN_LOCAL</code>.
   * 
   * <p>Si ce parametre n'est pas defini, alors on teste si le reseau est
   * etudiant, recherche ou administration.</p>
   * 
   * @see #isNetEdutiant()
   * @see #isNetRecherche()
   * @see #isNetAdmin()
   */
  public boolean isNetLocal() {
    int resp = isNetDefinedAs(CktlConfig.CONFIG_GRHUM_VLAN_LOCAL_KEY);
    if (resp >= 0) { // Definit dans la configuration
      return (resp > 0);
    } else if (resp == -1) { // N'est pas defini dans la config
      return (isNetEdutiant() || isNetRecherche() || isNetAdmin());
    }
    // Sinon, vLan n'est pas donne
    return false; 
  }
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau "etudiants".
   * Cette methode compare le code de reseau de compte courant, la valeur
   * retournee par la methode <code>vLan</code>, avec ceux donnes dans le
   * parametre de configuration <code>GRHUM_VLAN_ETUD</code>.
   * 
   * <p>Si ce parametre n'est pas defini, alors le code "E" (etudiants) est
   * considere comme definissant les reseaux "etudiants".</p>
   */
  public boolean isNetEdutiant() {
    int resp = isNetDefinedAs(CktlConfig.CONFIG_GRHUM_VLAN_ETUD_KEY);
    if (resp >= 0) { // Definit dans la configuration
      return (resp > 0);
    } else if (resp == -1) { // N'est pas defini dans la config
      return VLansEtudiant.containsObject(vLan());
    }
    // Sinon, vLan n'est pas donne
    return false; 
  }
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau 
   * enseignants/chercheurs". Cette methode compare le code de reseau de compte
   * courant, la valeur retournee par la methode <code>vLan</code>, avec ceux
   * donnes dans le parametre de configuration <code>GRHUM_VLAN_RECHERCHE</code>.
   * 
   * <p>Si ce parametre n'est pas defini, alors le code "R" est considere comme
   * definissant les reseaux de recherche.</p>
   */
  public boolean isNetRecherche() {
    int resp = isNetDefinedAs(CktlConfig.CONFIG_GRHUM_VLAN_RECHERCHE_KEY);
    if (resp >= 0) { // Definit dans la configuration
      return (resp > 0);
    } else if (resp == -1) { // N'est pas defini dans la config
      return VLansRecherche.containsObject(vLan());
    }
    // Sinon, vLan n'est pas donne
    return false; 
  }
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau
   * "administration". Cette methode compare le code de reseau de compte
   * courant, la valeur retournee par la methode <code>vLan</code>, avec ceux
   * donnes dans le parametre de configuration <code>GRHUM_VLAN_ADMIN</code>.
   * 
   * <p>Si ce parametre n'est pas defini, alors le code "P" est considere comme
   * definissant les reseaux de recherche.</p>
   */
  public boolean isNetAdmin() {
    int resp = isNetDefinedAs(CktlConfig.CONFIG_GRHUM_VLAN_ADMIN_KEY);
    if (resp >= 0) { // Definit dans la configuration
      return (resp > 0);
    } else if (resp == -1) { // N'est pas defini dans la config
      return VLansAdmin.containsObject(vLan());
    }
    // Sinon, vLan n'est pas donne
    return false; 
  }
  
  /**
   * Indique si le compte de l'utilisateur appartient au reseau "externe".
   * Cette methode compare le code de reseau de compte
   * courant, la valeur retournee par la methode <code>vLan</code>, avec ceux
   * donnes dans le parametre de configuration <code>GRHUM_VLAN_EXTERNE</code>.
   * 
   * <p>Si ce parametre n'est pas defini, alors le code "X" est considere comme
   * definissant les reseaux de recherche.</p>
   */
  public boolean isNetExterne() {
    int resp = isNetDefinedAs(CktlConfig.CONFIG_GRHUM_VLAN_EXTERNE_KEY);
    if (resp >= 0) { // Definit dans la configuration
      return (resp > 0);
    } else if (resp == -1) { // N'est pas defini dans la config
      return VLansAdmin.containsObject(vLan());
    }
    // Sinon, vLan n'est pas donne
    return false; 
  }

  /**
   * Teste si le type reseau en cours correspond a la definition du reseau
   * donnee dans le parametre de configuration <code>netTypeParamName</code>.
   * Cette methode retourne les intiers :
   * <ul>
   * <li>"-2" : le compte en cours n'a pas d'informations sur le type de
   * reseau;</li>
   * <li>"-1" : le dype de reseau n'est pas defini;</li>
   * <li>"0" : la definition du compte en cours ne correspond pas a celle donnee
   * dans le parametre indique.</li>
   * <li>"1" : la definition du compte correspond bien a celle donnee dans le
   * parametre de configuraiton indique.</li>
   * </ul>
   */
  private int isNetDefinedAs(String netTypeParamName) {
    if (vLan() == null) return -2;
    String value =
      StringCtrl.normalize(configForKey(netTypeParamName));
    if (value.length() == 0) return -1;
    if (!value.startsWith(",")) value = ","+value;
    if (!value.endsWith(",")) value += ",";
    return ((value.indexOf(","+vLan()+",") >= 0)?1:0);
  }
  
  /**
   * On redefinit cette methode, car elle ne fonctionne pas correctement dans
   * certains etablissements. On va s'appuyer sur le parametrage des reseaux
   * dans l'application.
   * @see fr.univlr.cri.webapp.database.CktlUserInfoDB#userStatus()
   * @return status de l'utilisateur
   */
  public int userStatus() {
    if (hasStatusPersonnel()) {
      return STATUS_PERSONNEL;
    } else if (hasStatusEtudiant()) {
      return STATUS_ETUDIANT;
    } else if (hasStatusExterne()) {
      return STATUS_EXTERIEUR;
    } else {
      return STATUS_INCONNU;
    }
  }
  
  /**
   * @return la liste des status de l'utilisateur
   */
  public ArrayList<Integer> listUserStatus() {
	  ArrayList<Integer> status = new ArrayList<Integer>();

	  if (hasStatusPersonnel()) {
		  status.add(STATUS_PERSONNEL);
	  }
	  if (hasStatusEtudiant()) {
		  status.add(STATUS_ETUDIANT);
	  }
	  if (hasStatusExterne()) {
		  status.add(STATUS_EXTERIEUR);
	  }
	  
	  return status;
  }
  
  /**
   * @return true si l'utilisateur est un personnel
   */
  public Boolean hasStatusPersonnel() {
	  return isNetAdmin() || isNetRecherche();
  }
  
  /**
   * @return true si la personne est un étudiant
   */
  public Boolean hasStatusEtudiant() {
	  return isNetEdutiant();
  }
  
  /**
   * @return true si la personne est externe
   */
  public Boolean hasStatusExterne() {
	  return isNetExterne();
  }

  // Le commentaire sera copie de la super-classe.
  public void individuForNoIndividu(Number noIndividu, boolean useCompte) {
    try {
      EOEnterpriseObject rec =
        myDB.fetchObject(TableIndividu, _CktlBasicDataBus.newCondition(
        		EOIndividuUlr.NO_INDIVIDU_KEY + "=%@", new NSArray(noIndividu)));
      individuForRecord(rec, useCompte);
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      errorMessage = CktlLog.getMessageForException(e);
    }
  }
  
  // Le commentaire sera copie de la super-classe.
  public void individuForPersId(Number persId, boolean useCompte) {
    try {
      EOEnterpriseObject rec =
        myDB.fetchObject(TableIndividu, _CktlBasicDataBus.newCondition(
        		EOIndividuUlr.PERS_ID_KEY + "=%@", new NSArray(persId)));
      individuForRecord(rec, useCompte);
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
  }

  // Le commentaire sera copie de la super-classe.
  public void compteForLogin(String login, String pass, boolean useIndividu) {
    EOEnterpriseObject rec;
    startProcessing(false);
    try {
      rec = findCompteForLogin(login, pass);
      if (errorCode() == ERROR_NONE) {
    	  compteForRecord(rec, useIndividu);
      }
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
    stopProcessing();
  }
  
  // Le commentaire sera copie de la super-classe.
  public void compteForPersId(Number persId, boolean useIndividu) {
    EOEnterpriseObject rec;
    startProcessing(false);
    try {
      rec = findCompteForPersId(persId);
      if (errorCode() == ERROR_NONE) {
    	  compteForRecord(rec, useIndividu);
      }
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
    stopProcessing();
  }
  
  // Le commentaire sera copie de la super-classe.
  public void servicesForNoIndividu(Number noIndividu) {
    try {
      EOEnterpriseObject rec;
      EOQualifier condition;
      NSMutableArray conditionListe = new NSMutableArray();
      NSTimestamp dateCourante = new NSTimestamp();

      // Verifier l'affectation aux services
      conditionListe.addObject(EOQualifier.qualifierWithQualifierFormat(
      		EOAffectation.NO_DOSSIER_PERS_KEY+" = %@",new NSArray(noIndividu)));
      conditionListe.addObject(EOQualifier.qualifierWithQualifierFormat(
      		EOAffectation.D_DEB_AFFECTATION_KEY +" <= %@",new NSArray(dateCourante)));
      conditionListe.addObject(EOQualifier.qualifierWithQualifierFormat(
      		"(("+EOAffectation.D_FIN_AFFECTATION_KEY+" >= %@) or ("+EOAffectation.D_FIN_AFFECTATION_KEY+" = nil))",new NSArray(dateCourante)));
      condition = new EOAndQualifier(conditionListe);
      NSArray objects = myDB.fetchArray(TableAffectation, condition, null);
      if (objects.count()==0) return;

      // Recuperer la description de chaque service
      conditionListe.removeAllObjects();
      for (int i = 0; i < objects.count(); i++) {
        rec = (EOEnterpriseObject) objects.objectAtIndex(i);
        conditionListe.addObject(EOQualifier.qualifierWithQualifierFormat(
        		EOStructureUlr.C_STRUCTURE_KEY + "= %@",new NSArray(rec.valueForKey(EOStructureUlr.C_STRUCTURE_KEY))));
      }
      condition = new EOOrQualifier(conditionListe);
      NSArray sort = new NSArray(EOSortOrdering.sortOrderingWithKey(EOStructureUlr.LL_STRUCTURE_KEY, EOSortOrdering.CompareDescending));
      objects = myDB.fetchArray(TableStructure, condition, sort);
      // Memoriser toutes les definitions des services
      services.removeAllObjects();
      for (int i = 0; i < objects.count(); i++) {
        rec = (EOEnterpriseObject) objects.objectAtIndex(i);
        services.takeValueForKey(rec.valueForKey(EOStructureUlr.LL_STRUCTURE_KEY), (String) rec.valueForKey(EOStructureUlr.C_STRUCTURE_KEY));
      }
    } catch (Throwable e) {
      errorCode = ERROR_SOURCE;
      takeError(e);
    }
  }
  
  /**
   * Permet d'acceder a l'objet de gestion d'acces a la base de donnees.
   */
  protected _CktlBasicDataBus dataBus() {
    return myDB;
  }
  
  /**
   * Retourne la valeur de configuration correspondant au parametre
   * <code>key</code>. Cette methode essaye d'abord verifier si le parametre
   * est definit dans la configuration gerer par la methode <code>config</code>
   * de la classe <code>CktlApplication</code>. Sinon, elle cherche la
   * definition dans les proprietes <code>Systeme</code> Java, ce qui
   * correspond au fichier <i>Properties</i> d'une application WebObjects.
   */
  protected String configForKey(String key) {
    // On detecte si CktlApplication est utilise
    // On ne le fait qu'une seule fois
    if (hasCktlConfig == null) {
      hasCktlConfig =
        new Boolean(WOApplication.application() instanceof CktlWebApplication);
    }
    if (hasCktlConfig.booleanValue()) {
      return ((CktlWebApplication) WOApplication.application()).config().stringForKey(key);
    } else {
      return System.getProperty(key);
    }
  }

}
