package org.cocktail.fwkcktlwebapp.common.database;


import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eoaccess.EOAttribute;
import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eoaccess.EOModelGroup;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOKeyValueCoding;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSTimestamp;

/**
 * Simplifier l'acces aux atributs des objets EOF de differents types. Cette
 * classe etend la classe {@link EOGenericRecord} en proposant les
 * methodes qui effectuent le transtypage des valeurs lors de l'acces aux
 * attributs des objets EOF. Les methodes de cette classe permet de simplifier
 * l'ecriture de code. Elles n'ajoutent pas de fonctionnalites supplementaires
 * par rapport a celles offertes par {@link EOGenericRecord}.
 * 
 * <p>Si vous souhaitez utiliser cette classe pour instancier vos objets,
 * indiquez <code>org.cocktail.fwkcktlwebapp.server.database.CktlRecord</code> comme <i>Classe Name</i>
 * de vos entites dans votre <i>EOModel</i>.</p>
 * 
 * <p>Attention, la classe {@link CktlRecord} peut etre utilisee dans les
 * modeles ou d'habitude vous utilisez les instances de la classe
 * {@link EOGenericRecord}. Il n'est pas necessaire de faire reference
 * vers la classe {@link CktlRecord}, si dans un modele in instacie deja
 * ses propres classes metiers, car les methodes accesseurs aux attributs y
 * sont ajoutees par defaut.</p>
 * 
 * <p>Cette classe propose les methodes d'instance ainsi que les methodes
 * statiques. Voir les exemples ci-dessous sur leur utilisation.</p>
 * 
 * 
 * <h3>Attribus d'une instance</h3>
 * 
 * Les valeurs des attributs peuvent etre accedees via les methodes
 * <code>intForKey</code>, <code>stringForKey</code>, etc... On peut egalement
 * utiliser leurs equivalents <code>intForKeyPath</code>, etc...
 * 
 * <p>Ainsi, si <code>unObjet</code> est une instance de
 * <code>EOGenericRecord</code> et <code>attributInt</code> sont attribut
 * ayant les valeurs entieres, on devrait ecrire :</p>
 * <div class="example"><pre>
 * int intVal = ((Number)unObjet.valueForKey("attributInt")).intValue();</pre></div>
 * 
 * <p>Si <code>unObjet</code> est une instance de {@link CktlRecord}, alors
 * la meme expression s'ecrit plus simplement :</p>
 * <div class="example"><pre>
 * int intVal = unObjet.intForKey("attributInt");</pre></div>
 * 
 * 
 * <h3>Attributs d'une instance formates</h3>
 * 
 * Les methodes <code>stringNormalizedForKey</code>, <code>dateStringForKey</code>
 * et <code>dateTimeStringForKey</code> permettent d'acceder aux valeurs des
 * attributs en meme temps effectuant leur formattage. 
 * 
 * <p>Pour une instance de <code>EOGenericRecord</code> et un attribut
 * <code>NSTimeStamp</code>, on peut ecrire :</p>
 * <div class="example"><pre>
 * String dateStr = DateCtrl.dateToString((NSTimeStamp)unObjet.valueForKey("attributDate"),
 *                                        "%m/%d/%Y");</pre></div>
 * 
 * <p>En utilisant la classe {@link CktlRecord}, le meme resultat peut
 * etre obtenu :</p>
 * 
 * <div class="example"><pre>
 * String dateStr = unObjet.dateStringForKey("attributDate", "%m/%d/%Y");</pre></div>
 * 
 * <h3>Methodes statiques</h3>
 * 
 * Les methodes statiques <code>recordIntForKey</code>, <code>recordStringForKey</code>,
 * etc., permettent d'acceder aux valeurs d'un objet. Elles prennent comme
 * parametre un objet de la classe <code>Object</code>, mais on suppose qu'il
 * doit implementer l'interface <code>NSKeyValueCoding</code>.
 * 
 * <p>Un exemple de code souvent utilise avec <code>EOGenericRecord</code> :</p>
 * <div class="example"><pre>
 * NSArray objects; <code class="comment">// Une liste des objets EOF</code>
 * for(int i=0; i&lt;objects.count(); i++) {
 *   int intVal = ((Number)((EOGenericRecord)objects.objectAtIndex(i))
 *                    .valueForKey("attribInt")).intValue();
 *   <code class="comment">// On effetue le traitement sur la valeur obtenue...</code>
 * }</pre></div>
 * 
 * <p>En utilisant les methodes de la classe {@link CktlRecord}, on pourrait
 * ecrire :</p>
 * 
 * <div class="example"><pre>
 * NSArray objects; <code class="comment">// Une liste des objets EOF</code>
 * for(int i=0; i&lt;objects.count(); i++) {
 *   int intVal = CktlRecord.recordIntForKey(objects.objectAtIndex(i), "attribInt");
 *   <code class="comment">// On effectue le traitement sur la valeur obtenue...</code>
 * }</pre></div>
 */
public class CktlRecord
	extends EOGenericRecord {
  
  public CktlRecord() {
    super();
  }
/* Les methode pour acceder aux valeurs */
  
  public Number numberForKey(String key) {
    return (Number)storedValueForKey(key);
  }
  
  public int intForKey(String key) {
    return numberForKey(key).intValue();
  }
  
  public String stringForKey(String key) {
    return (String)storedValueForKey(key);
  }
  
  public NSTimestamp dateForKey(String key) {
    return (NSTimestamp)storedValueForKey(key);
  }

  public boolean boolForKey(String key) {
    return StringCtrl.toBool(storedValueForKey(key).toString());
  }
  
  public NSArray arrayForKey(String key) {
    return (NSArray)valueForKey(key);
  }
  
  public EOEnterpriseObject eoForKey(String key) {
    return (EOEnterpriseObject)valueForKey(key);
  }
  
  public CktlRecord recForKey(String key) {
    return (CktlRecord)valueForKey(key);
  }
  
/* Les methodes pour acceder aux valeurs via keyPath */
  
  public Number numberForKeyPath(String keyPath) {
    return (Number)valueForKeyPath(keyPath);
  }
  
  public int intForKeyPath(String keyPath) {
    return numberForKeyPath(keyPath).intValue();
  }
  
  public String stringForKeyPath(String keyPath) {
    return (String)valueForKeyPath(keyPath);
  }
  
  public NSTimestamp dateForKeyPath(String keyPath) {
    return (NSTimestamp)valueForKeyPath(keyPath);
  }
  
  public boolean boolForKeyPath(String keyPath) {
    return StringCtrl.toBool(valueForKeyPath(keyPath).toString());
  }
  
  public NSArray arrayForKeyPath(String key) {
    return (NSArray)valueForKeyPath(key);
  }
  
  public EOEnterpriseObject eoForKeyPath(String key) {
    return (EOEnterpriseObject)valueForKeyPath(key);
  }
  
  public CktlRecord recForKeyPath(String key) {
    return (CktlRecord)valueForKeyPath(key);
  }

/* Les methodes qui permettent de formater les donnees */

  /**
   * Retourne la valeur normalize de la valeur de l'attribut <code>key</code>.
   * On suppose que la valeur de <code>key</code> est une chaine de caracteres.
   * Si la valeur est <code>null</code>, alors elle est convertie en une chaine vide.
   * Cette methode ne retourne donc jamais la valeur <code>null</code>.
   */
  public String stringNormalizedForKey(String key) {
    return StringCtrl.normalize(stringForKey(key));
  }
  
  /**
   * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et
   * la convertie en une chaine de caracteres en suivant le format donne
   * dans <code>formatter</code>.
   */
  public String dateStringForKey(String key, String formatter) {
    return DateCtrl.dateToString(dateForKey(key), formatter);
  }
  
  /**
   * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et
   * la convertie en une chaine de caracteres. Le format de la date
   * utilise est "%d/%m/%Y" (par exemple, 25/03/2004).
   */
  public String dateStringForKey(String key) {
    return dateStringForKey(key, "%d/%m/%Y");
  }
  
  /**
   * Recupere la valeur <i>date</i> de l'attribut <code>key</code> et
   * la convertie en une chaine de caracteres. Le format de la date
   * utilise est "%d/%m/%Y %H:%M" (par exemple, 25/03/2004 13:35).
   */
  public String dateTimeStringForKey(String key) {
    return dateStringForKey(key, "%d/%m/%Y %H:%M");
  }
  
  /**
   * Retourne la longeur maximal en octets de la valeur autorisee pour
   * l'attribut <code>attributeName</code> de cet enregistrement.
   * 
   * <p>Retourne -1 si l'attribut n'existe pas dans cet enregistrement ou
   * s'il est de type numerique ou date.</p>
   */
  public int maxLengthForAttribute(String attributeName) {
    return maxLengthForAttribute(entityName(), attributeName);
  }
  
/* Les methodes permttant d'extraire les valeurs des attributs a partir des objets */

  public static Object recordValueForKey(Object record, String key) {
    return ((NSKeyValueCoding)record).valueForKey(key);
  }
  
  public static String recordStringForKey(Object record, String key) {
    return (String)((NSKeyValueCoding)record).valueForKey(key);
  }

  public static Number recordNumberForKey(Object record, String key) {
    return (Number)((NSKeyValueCoding)record).valueForKey(key);
  }

  public static int recordIntForKey(Object record, String key) {
    return ((Number)((NSKeyValueCoding)record).valueForKey(key)).intValue();
  }

  public static NSTimestamp recordDateForKey(Object record, String key) {
    return (NSTimestamp)((NSKeyValueCoding)record).valueForKey(key);
  }

  public static boolean recordBoolForKey(Object record, String key) {
    String value = ((NSKeyValueCoding)record).valueForKey(key).toString();
    return StringCtrl.toBool(value.toUpperCase());
  }

  public static NSArray recordArrayForKey(Object record, String key) {
    return (NSArray)((NSKeyValueCoding)record).valueForKey(key);
  }
  
  public static EOEnterpriseObject recordEOForKey(Object record, String key) {
    return (EOEnterpriseObject)((NSKeyValueCoding)record).valueForKey(key);
  }

  public static CktlRecord recordRecForKey(Object record, String key) {
    return (CktlRecord)((NSKeyValueCoding)record).valueForKey(key);
  }
  
/* D'autres methodes */
  /**
   * Retourne l'objet correspondant a la valeur <code>null</code> dans
   * l'implementation actuelle de EOF.
   */
  public static Object nullValue() {
    // La version pour WO 5.x
    return EOKeyValueCoding.NullValue;
  }
    
  /**
   * 
   */
  public static Object valueIfNull(Object value) {
    if (value == null) return nullValue();
    return value;
  }
  
  /**
   * Teste si la valeur donnee correspond a la valeur NULL. Elle l'est si
   * <code>value</code> est egale a <code>null</code> ou
   * {@link NSKeyValueCoding#NullValue}.
   */  
  public static boolean isNullValue(Object value) {
    return ((value == null) || (value == nullValue()));
  }
  
  /**
   * Retourne la longeur maximal en octets de la valeur autorisee pour
   * l'attribut <code>attributeName</code> de la table <code>tableName</code>.
   * 
   * <p>Retourne -1 si la table ou l'attribut n'existe pas ou si l'attribut est 
   * de type numerique ou date.</p>
   */
  public static int maxLengthForAttribute(String tableName, String attributeName) {
    EOEntity entity = EOModelGroup.defaultGroup().entityNamed(tableName);
    if (entity != null) {
      EOAttribute attribute = entity.attributeNamed(attributeName);
      if ((attribute != null) && (attribute.width() > 0))
        return attribute.width(); 
    }
    return -1;
  }
  
  // methodes rajoutees de DTWeb (classe DTRecord)
  
  /**
   * 
   */
  public static Hashtable recordToHashtable(EOEnterpriseObject rec,
                                            boolean keepEmpty)
  {
    return keyValueToHashtable(rec, rec.attributeKeys(), keepEmpty);
  }

  /**
   * 
   */
  public static Hashtable dicoToHashtable(NSDictionary dico,
                                          boolean keepEmpty)
  {
    return keyValueToHashtable(dico, dico.allKeys(), keepEmpty);
  }

  /**
   * 
   */
  public static Hashtable keyValueToHashtable(NSKeyValueCoding dico,
                                              NSArray keys,
                                              boolean keepEmpty)
  {
    Hashtable result = new Hashtable();
    Object value;
    for(int i=0; i<keys.count(); i++) {
      value = dico.valueForKey((String)keys.objectAtIndex(i));
      if (CktlRecord.isNullValue(value)) {
        if (keepEmpty)
          result.put(keys.objectAtIndex(i), "<null>");
      } else {
        result.put(keys.objectAtIndex(i), value);
      }
    }
    return result;
  }
}