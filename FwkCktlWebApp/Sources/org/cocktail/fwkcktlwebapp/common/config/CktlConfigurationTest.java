package org.cocktail.fwkcktlwebapp.common.config;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public interface CktlConfigurationTest {
	/**
	 * Méthode d'exécution du test
	 * @throws CktlConfigurationTestException exception en cas d'échec du test
	 */
	void run() throws CktlConfigurationTestException;
}
