package org.cocktail.fwkcktlwebapp.common.config;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author Julien Lafourcade
 *
 */
public class CktlConfigurationTestRunner {
	
	private List<CktlConfigurationTest> tests;
	
	/**
	 * Contructeur qui prend une liste de tests en paramètre
	 * @param tests la liste de tests
	 */
	public CktlConfigurationTestRunner(List<CktlConfigurationTest> tests) {
		this.setTests(tests);
	}

	public List<CktlConfigurationTest> getTests() {
		return tests;
	}

	public void setTests(List<CktlConfigurationTest> tests) {
		this.tests = tests;
	}

	/**
	 * Lance la série de tests et renvoie les resultats sous forme d'une liste d'exceptions
	 * @return la liste des exceptions levées par les test qui ont échoués
	 */
	public List<CktlConfigurationTestException> runTests() {
		List<CktlConfigurationTestException> resultats = new ArrayList<CktlConfigurationTestException>();
		for (CktlConfigurationTest test : getTests()) {
			try {
				test.run();
			} catch (CktlConfigurationTestException e) {
				resultats.add(e);
			}
		}
		return resultats;
	}

}
