package org.cocktail.fwkcktlwebapp.common.config;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class CktlConfigurationTestException extends Exception {

	private static final long serialVersionUID = 4653468129746568156L;
	
	/**
	 * 
	 * @param string Le message d'erreur
	 */
	public CktlConfigurationTestException(String string) {
		super(string);
	}

	

}
