package org.cocktail.fwkcktlwebapp.common.css;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.lesscss.LessCompiler;
import org.lesscss.LessException;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSBundle;


/**
 * 
 * Cette classe va nous servir à compiler les fichiers less de nos applications
 * 
 * @author Julien Lafourcade
 *
 */
public class CktlLessCompiler {

	/**
	 * Lance une compilation
	 * @param lessInputFileName le fichier LESS à compiler ("application.less" ou autre). Il doit se trouver au premier niveau dans le répertoire LESS des WSR de l'appli
	 * @param outputFolder le repertoire de destination. Généralement "styles" ou "css"
	 * @param cssOutputFileName le nom du fichier compilé ("application.css" ou autre)
	 * @throws IOException erreur lors de la lecture ou de l'écriture d'un fichier 
	 * @throws URISyntaxException erreur à la génération des chemins des fichiers
	 * @throws LessException erreur lors de la compilation du fichier LESS
	 */
	public void compile(String lessInputFileName, String outputFolder, String cssOutputFileName) throws IOException, URISyntaxException, LessException {

		// Recuperation des bundles des frameworks
		NSArray<NSBundle> frameworkBundles = NSBundle.frameworkBundles();

		// Creation d'un dossier temporaire pour travailler
		File tmpRoot = null;

		tmpRoot = File.createTempFile("less", "");

		tmpRoot.delete();
		tmpRoot.mkdir();

		// Recopie des ressources LESS des frameworks
		for (NSBundle bundle : frameworkBundles) {
			File destDir = new File(tmpRoot, bundle.name());
			URL lessUrl = bundle.pathURLForResourcePath("less");
			if (lessUrl != null) {
				File srcDir = new File(lessUrl.toURI());
				FileUtils.copyDirectory(srcDir, destDir);
			}

		}

		// Recopie des ressources LESS de l'application
		File srcDir = new File(mainBundle().pathURLForResourcePath("less").toURI());
		FileUtils.copyDirectory(srcDir, tmpRoot);

		// Compilation du fichier LESS
		LessCompiler lessCompiler = new LessCompiler();
		File stylesDir = new File(mainBundle().pathURLForResourcePath(outputFolder).toURI());
		lessCompiler.compile(new File(tmpRoot, lessInputFileName), new File(stylesDir, cssOutputFileName));
	}

	private NSBundle mainBundle() {
		return NSBundle.mainBundle();
	}

}
