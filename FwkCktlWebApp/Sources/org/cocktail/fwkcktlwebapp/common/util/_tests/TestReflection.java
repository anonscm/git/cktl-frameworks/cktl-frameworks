package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.lang.reflect.Method;

import org.cocktail.fwkcktlwebapp.common.util._imports.FileClassLoader;


/**
 * Test des mecanismes de Java Reflection.
 */
public class TestReflection {
  public static void main(String[] args) {
//    testClassLoader();
    testNoParameters();
//    testStrings();
//    testIntegers();
//    testInts();
//    testClassInfo();
  }

  private static void testClassLoader() {
    try {
      ClassLoader classLoader =
        new FileClassLoader("/System/Library/Java/");
      Class nSWorkspace = classLoader.loadClass("com.apple.cocoa.application.NSWorkspace");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
  }

  private static void testNoParameters() {
    System.out.println("*** Test Reflection : No Parameters method ***");
    try {
      Class testClass = Class.forName("TestReflectionClass");
      Object testInstance = testClass.newInstance();
      Method testMethod = testClass.getDeclaredMethod("methodNoParameters",
                      (Class<?>) null);
      testMethod.invoke(testInstance, (Object[]) null);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private static void testStrings() {
    System.out.println("*** Test Reflection : String method ***");
    try {
      Class testClass = Class.forName("TestReflectionClass");
      Object testInstance = testClass.newInstance();
      Method testMethod = testClass.getDeclaredMethod("methodStrings",
                            new Class[]{String.class, String.class, String.class});
      testMethod.invoke(testInstance, new Object[]{"Test value 1", null, "value 2"});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  private static void testIntegers() {
    System.out.println("*** Test Reflection : Integer method ***");
    try {
      Class testClass = Class.forName("TestReflectionClass");
      Object testInstance = testClass.newInstance();
      Method testMethod = testClass.getDeclaredMethod("methodIntegers",
                            new Class[]{Integer.class, Integer.class, Integer.class});
      testMethod.invoke(testInstance, new Object[]{new Integer(1), null, null});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  private static void testInts() {
    System.out.println("*** Test Reflection : int method ***");
    try {
      Class testClass = Class.forName("TestReflectionClass");
      Object testInstance = testClass.newInstance();
      Method testMethod = testClass.getDeclaredMethod("methodInts",
                            new Class[]{Integer.class, int.class, int.class});
      testMethod.invoke(testInstance, new Object[]{new Integer(1), new Integer(Integer.MAX_VALUE), new Integer(Integer.MIN_VALUE)});
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void testClassInfo() {
    System.out.println("*** Test Reflection : ClassInfo ***");
    try {
      Class testClass = Class.forName("TestReflectionClass");
      System.out.println("Class Name : "+testClass.getName());
      Method[] classMethods = testClass.getMethods();
      System.out.println("All methods :");
      if (classMethods == null || classMethods.length == 0) {
        System.out.println("  [No public methods]");
      } else {
        for(int i=0; i<classMethods.length; i++) {
          System.out.println("  "+classMethods[i].getName());
          System.out.println("   - parameters");
          Class[] parameters = classMethods[i].getParameterTypes();
          if (parameters == null || parameters.length == 0) {
            System.out.println("    > no parameters");
          } else {
            for(int j=0; j<parameters.length; j++)
              System.out.println("    > "+parameters[j].getName());
          }
          System.out.println("   - result");
          System.out.println("    > "+classMethods[i].getReturnType());
        }
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }
}
