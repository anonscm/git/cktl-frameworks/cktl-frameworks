package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Vector;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLNode;

public class TestXMLparse {
  public static void main(String args[]) {
    testXParce2();
  }

  private static void testXParce() {
    int[] occur = {1, 1, 1, 1};
    CktlXMLNode xmlRoot;
    CktlXMLNode node;
//    String xmlFile = "D:/Devel/CRI/06_Eclipse/WO5/SIXCore/files/data/BulletinPaye.xml";
    String xmlFile = "D:/TempStuff/test.xml";
    String findPath = "ListeAffaire/MAFWREPG";
    String childrenPath = "MAFWREPG";
    String childName = "devise";
    
    System.out.println("Parsing file : "+xmlFile);
    try {
      BufferedReader r = new BufferedReader(new FileReader(xmlFile));
      String line;
      StringBuffer content = new StringBuffer();
      while((line = r.readLine()) != null) {
        content.append(line);
      }
      xmlRoot = CktlXMLNode.parse(content.toString());
//      node = xmlRoot.findFirst(findPath);
//      node = xmlRoot.find(findPath, occur);
      xmlRoot.find("ListeAffaire", "1");
      node = xmlRoot.find(findPath, "1, 1");
      System.out.print("Value for \""+findPath+"\" : ");
      if(node == null) {
        System.out.println("not found");
      } else {
        System.out.println(node.getCharacters());
      }
      System.out.println("Children for \""+childrenPath+"\" : ");
      node = xmlRoot.findFirst(childrenPath);
      if (node == null) {
        System.out.println("  not found");
      } else {
        Vector ch = node.getChildren();
        if (ch.size() == 0) {
          System.out.println("  no children");
        } else {
          for(int i=0; i<ch.size(); i++)
            System.out.println("  "+((CktlXMLNode)ch.elementAt(i)).getName());
        }
      }
      System.out.println("Children with name : \""+childName+"\" : ");
      node = xmlRoot.findChild(childName, true);
      if (node == null) {
        System.out.println("  not found");
      } else {
        System.out.println("  found");
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  private static void testXParce2() {
    int[] occur = {1, 1, 1, 1};
    CktlXMLNode xmlRoot;
    CktlXMLNode node;
    String xmlFile = "D:/TempStuff/test.xml";
    String findPath = "ListeAffaire/MAFWREPG";
    String childrenPath = "MAFWREPG";
    
    System.out.println("Parsing file : "+xmlFile);
    try {
      BufferedReader r = new BufferedReader(new FileReader(xmlFile));
      String line;
      StringBuffer content = new StringBuffer();
      while((line = r.readLine()) != null) {
        content.append(line);
      }
      xmlRoot = CktlXMLNode.parse(content.toString());
      xmlRoot = xmlRoot.find("ListeAffaire", "1");
//      node = xmlRoot.findFirst(findPath);
//      node = xmlRoot.find(findPath, occur);
      Vector childrenNodes = xmlRoot.getChildren();
      System.out.println("Root children count : "+childrenNodes.size());
      for(int i=0; i<childrenNodes.size(); i++) {
        node = (CktlXMLNode)childrenNodes.elementAt(i);
        System.out.println("Child "+i+" : "+node.getName());
        showChilds(node);
      }
    } catch(Exception e) {
      e.printStackTrace();
    }
  }
  
  private static void showChilds(CktlXMLNode node) {
    Vector children = node.getChildren();
    System.out.println("Children for node \""+node.getName()+"\" : "+children.size());
    for(int i=0; i<children.size(); i++) {
      node = (CktlXMLNode)children.elementAt(i);
      System.out.println(" > Child "+i+" : "+node.getName());
    }
  }

}
