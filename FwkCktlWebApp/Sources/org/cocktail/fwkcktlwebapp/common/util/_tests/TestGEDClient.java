package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.cocktail.fwkcktlwebapp.common.util.GEDClient;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class TestGEDClient {
  private static final String APP_CODE = "COUR";
  private static final String CAT_CODE = "COUR";

/* ===== Test de client ====== */
  public static void main(String[] args) {
    System.out.println("Starting GED client...");
    GEDClient client = new GEDClient("193.51.217.177", 9241, 2356, APP_CODE);
    // -- Enregistrement
    testEnregistrerFichier(client, args[0]);
    // testEnregistrerFichierNom(client, args[0], "DocumentTest.bin.1");
    // testEnregistrerFichierStream(client, args[0], "DocumentTest.bin.2");
    // testEnregistrerFichierBytes(client, args[0], "DocumentTest.bin.3");
    // testEnregistrerReference(client, args[0]);
    // -- Remplacement
    // testRemplacerFichier(client, args[0], args[1]);
    // testRemplacerFichierNom(client, args[0], "DocumentTest.bin.4", args[1]);
    // testRemplacerFichierStream(client,  args[0], "DocumentTest.bin.5", args[1]);
    // testRemplacerReference(client, args[0], 45);
    // -- Inspection
    // testInspecter(client, args[0]);
    // -- Suppression
    // testSupprimer(client, args[0]);
  }

  private static int stringToInt(String value, int defValue) {
    try {
      return Integer.valueOf(value).intValue();
    } catch(Exception ex) {
      ex.printStackTrace();
      return defValue;
    } 
  }
  
/* ==== Tests d'enregistrement ==== */  
  private static void testEnregistrerFichier(GEDClient client, String fileName) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerDocument(fileName, "Test document", CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }
  
  private static void testEnregistrerFichierNom(GEDClient client, String fileName, String nom) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerDocument(fileName, "Test document", nom, CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

  private static void testEnregistrerFichierStream(GEDClient client, String fileName, String nom) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    File f = new File(fileName);
    try {
      FileInputStream in = new FileInputStream(f);
      if (client.enregistrerDocument(in, "Test document", f.length(), nom, CAT_CODE) != -1) {
        System.out.println("OK. Server answer:\n  "+client.description());
      } else {
        System.out.println("FAIL. Message:\n  "+client.lastMessage());
      }
    } catch(IOException ex) {
      ex.printStackTrace();
    }
  }

  private static void testEnregistrerFichierBytes(GEDClient client, String text, String nom) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    byte[] b = text.getBytes();
    if (client.enregistrerDocument(b, "Test document", nom, CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

  private static void testEnregistrerReference(GEDClient client, String reference) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerReference(reference, "Test reference", CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

/* ==== Tests de remplacement des fichiers ==== */
  private static void testRemplacerFichier(GEDClient client, String fileName, String docId) {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerDocument(fileName, "Test document", StringCtrl.toInt(docId, -1))) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

  private static void testRemplacerFichierNom(GEDClient client, String fileName, String nom, String docId) {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerDocument(fileName, "Test document", nom, StringCtrl.toInt(docId, -1))) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

  private static void testRemplacerFichierStream(GEDClient client, String fileName, String nom, String docId) {
    System.out.println("Test de remplacement...");
    client.reset();
    File f = new File(fileName);
    try {
      FileInputStream in = new FileInputStream(f);
      if (client.remplacerDocument(in, "Test document", f.length(), nom, StringCtrl.toInt(docId, -1))) {
        System.out.println("OK. Server answer:\n  "+client.description());
      } else {
        System.out.println("FAIL. Message:\n  "+client.lastMessage());
      }
    } catch(IOException ex) {
      ex.printStackTrace();
    }
  }

  private static void testRemplacerReference(GEDClient client, String reference, int docId) {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerReference(reference, "Test reference", docId)) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

/* ==== Tests de suppression des fichiers ==== */
  private static void testSupprimer(GEDClient client, String documentId) {
    System.out.println("Test de suppression...");
    client.reset();
    if (client.supprimerDocument(stringToInt(documentId, -1))) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }

/* ==== Tests d'inspection des fichiers ==== */
  private static void testInspecter(GEDClient client, String documentId) {
    System.out.println("Test d'inspection...");
    client.reset();
    if (client.inspecterDocument(stringToInt(documentId, -1))) {
      System.out.println("OK. Server answer:\n  "+client.description());
    } else {
      System.out.println("FAIL. Message:\n  "+client.lastMessage());
    }
  }
}
