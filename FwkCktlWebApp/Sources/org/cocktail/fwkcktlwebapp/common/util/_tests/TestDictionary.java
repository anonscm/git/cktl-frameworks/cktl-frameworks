package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.Properties;

/**
 * 
 */
public class TestDictionary {

  public static void main(String args[]) throws IOException {
    testDicoLoad();
  }

	private static void testDicoLoad() throws IOException {
    Hashtable dico = new Hashtable();
    System.out.println("-- Test for dico --");
    dico.put("key1", "value1");
    dico.put("key2", "value2");
    dico.put("key3", "value3 complex value");
    System.out.println("Dico :\n"+dico);
    
    Properties props = new Properties();
    System.out.println("-- Test for properties --");
    props.put("propetie1", "prop_value1");
    props.put("prop2_dico", dico.toString());
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    props.save(out, null);
    System.out.println("Properties :\n"+new String(out.toByteArray()));
    
    System.out.println("-- Load form dictionary --");
    Properties props2 = new Properties();
    ByteArrayInputStream in = new ByteArrayInputStream(dico.toString().getBytes());
    props.load(in);
    System.out.println("Properties from dicotionary :\n"+props2.toString());
	}
}
