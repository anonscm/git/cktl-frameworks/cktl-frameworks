package org.cocktail.fwkcktlwebapp.common.util._tests;
/*
 * Created on 13 mars 2003
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.MalformedURLException;
import java.util.Vector;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * @author astockus
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestStringCtrl {

  public static void main(String[] args) {
  	// testBasicString();
    // testCryptHTML();
    // testCrypter();
//     testStripTag();
    // testExtendWithChars();
    // textCut();
    // testGet0Int();
    // testCapitalizeWords();
    // testReplace();
//    test();
//    testLike();
//    testTextBetweenTag();
//    testNumberClass();
//    testPathURL();
//    testSortStrings();
    testBoolean();
  }
  
  private static void testBoolean() {
    System.out.println("*** Test Boolean expressions ***");
    System.out.println("-- expressions : vrai --");
    System.out.println("  oui : "+StringCtrl.toBool("oui"));
    System.out.println("  yes : "+StringCtrl.toBool("yes"));
    System.out.println("  O : "+StringCtrl.toBool("O"));
    System.out.println("  Y : "+StringCtrl.toBool("Y"));
    System.out.println("  vrai : "+StringCtrl.toBool("vrai"));
    System.out.println("  true : "+StringCtrl.toBool("true"));
    System.out.println("  1 : "+StringCtrl.toBool("1"));
    System.out.println("  OK : "+StringCtrl.toBool("OK"));
    System.out.println("-- expressions : faux --");
    System.out.println("  OKK : "+StringCtrl.toBool("OKK"));
    System.out.println("  11 : "+StringCtrl.toBool("11"));
    System.out.println("  yy : "+StringCtrl.toBool("yy"));
    System.out.println("  oo : "+StringCtrl.toBool("oo"));
  }

  private static void testSortStrings() {
    System.out.println("*** Test Sort Strings ***");
    String fileToSort = "E:/six1";
    Vector strings = new Vector();
    try {
      BufferedReader r = new BufferedReader(new FileReader(fileToSort));
      String line;
      while((line = r.readLine()) != null)
        strings.addElement(line);
      System.out.println("-- File read. Content :");
      for(int i=0; i<strings.size(); i++)
        System.out.println(strings.elementAt(i));
      System.out.println("-- Sorting...");
      strings = StringCtrl.sortedStrings(strings, true, false);
      System.out.println("-- Sorted. Content :");
      for(int i=0; i<strings.size(); i++)
        System.out.println(strings.elementAt(i));
    } catch(Exception ex) {
    }
  }

  private static void testPathURL() {
    System.out.println("*** Test Path URL ***");
    String filePath = "D:/Shared/Doc/JavaTutorial/tutorial/index.html";
    try {
      System.out.println("Original Path : "+filePath);
      filePath = new File(filePath).toURL().toString();
      System.out.println("File URL : "+filePath);
      if (filePath.startsWith("file:")) {
        filePath = filePath.substring(5);
        for(;filePath.startsWith("/"); filePath = filePath.substring(1));
        filePath = "/"+filePath;
      }
      System.out.println("File Path : "+filePath);
      System.out.println("Exists ? : "+new File(filePath).exists());
    } catch (MalformedURLException e) {
      e.printStackTrace();
    }
  }

  private static void testNumberClass() {
    System.out.println("Test number class");
    Integer integer = new Integer(0);
    Float flt = new Float(0.0);
    System.out.println("  Integer instanceof Number : "+(integer instanceof Number));
    System.out.println("  Float instanceof Number : "+(integer instanceof Number));
  }

  private static void testTextBetweenTag() {
    String text = "Sans vouloir faire de concurrence au forum Petite annonce :\n<a href=\"http://webannonce.free.fr\" target=\"_blank\">webannonce.free.fr</a> ";
    System.out.println("Text :\n---\n"+text+"\n----");
    System.out.println("Stripped :\n---\n"+StringCtrl.textBetweenTag(text, "a", true)+"\n----");
  }

  private static void testLike() {
    System.out.println("String Test : like ");
    String chain = "SIX.java";
    String pattern = "SIX.*";
    System.out.println("Chain : "+chain);
    System.out.println("Pattern : "+pattern);
    System.out.println("Is like ? : "+StringCtrl.like(chain, pattern));
  }

  private static void test() {
    String s = "ABC:DEF";
    String key = ":";
    int idx = s.indexOf(key);
    System.out.println("String = \""+s+"\", key = \""+key+"\"");
    System.out.println("Prefix : "+s.substring(0, idx));
    System.out.println("Suffix : "+s.substring(idx+1));
  }
  
  private static void testReplace() {
    String s = "ACHATS D'ETUDES ET PRESTATIONS DE SERVICES";
    String what = "ES";
    String byWhat = "IS";
    System.out.println("--- Test String Replace ---");
    System.out.println("Original str : "+s);
    System.out.println("        what : "+what);
    System.out.println("      byWhat : "+byWhat);
    System.out.println("Replaced str :"+StringCtrl.replace(s, what, byWhat));
  }

  private static void testCapitalizeWords() {
    String words = "ACHATS D'ETUDES ET PRESTATIONS DE SERVICES";
    System.out.println("Test Capitalize Words");
    System.out.println("Initial sentence :");
    System.out.println("  "+words);
    System.out.println("Result sentence :");
    System.out.println("  "+StringCtrl.capitalizeWords(words));
  }

  private static void textCut() {
    String toCut = "AAABBB";
    System.out.println("Test Cut :");
    System.out.println("  String to cut : "+toCut);
    System.out.println("  Cutted string : "+
      StringCtrl.cut(toCut, 2, false));
  }

  private static void testExtendWithChars() {
    String toExtend = "AAABBB";
    System.out.println("Test Extends :");
    System.out.println("  String to extend : "+toExtend);
    System.out.println("  Extended string : "+
      StringCtrl.extendWithChars(toExtend, "0", 25, true));
  }

  private static void testGet0Int() {
    int i = 215;
    System.out.println("Test Get0Int :");
    System.out.println("  Int to extend : "+i);
    System.out.println("  Extended int : "+
      StringCtrl.get0Int(i, 4));
  }

  private static void testBasicString() {
  	String str = "ShadowBox/ShadowBox_files";
  	String strBasic = StringCtrl.toBasicString(str);
  	
  	System.out.println("Original : "+str);
    System.out.println("Basic : "+strBasic);
  }
  
  private static void testCryptHTML() {
    String htmlChain = "astockus@univ-lr.fr";
    System.err.println("Original HTML chain = "+htmlChain);
    System.err.println("Crypted HTML chain = "+StringCtrl.codeHTML(htmlChain));
  }
  
  private static void testCrypter() {
    String msgToCrypt = "<a href=\"mailto:astockus@univ-lr.fr\">envoyer un mail</a>";
    System.out.println("testCrypter() original : "+msgToCrypt);
    System.out.println("testCrypter() crypted : "+encryptIt(msgToCrypt));
  }
  
  private static String encryptIt(String _msg) {
    String key = "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz1029384756><#].@";
    // boolean DECRYPT = false;
    String _xmsg = "";
    int _loc = -1;
    int mcH =  key.length() / 2;
    int dv;

    for (int x = 0; x < _msg.length(); x++) {
      if (((int)_msg.charAt(x)) == 13 || ((int)_msg.charAt(x)) == 10) {
        _xmsg += " ";
      } else {
        _loc = key.indexOf(_msg.charAt(x));
        if(_loc==-1) {
          _xmsg+= (_msg.charAt(x) == '\"'?"\\\"":String.valueOf(_msg.charAt(x)));
        } else if (_loc > mcH) {
          dv = _loc - mcH;
          _xmsg += key.charAt(33 - dv);
        } else {
          dv = mcH - _loc;
          // if (key.charAt(33+dv)=='<'&& DECRYPT) _xmsg+="<"; else
          _xmsg += key.charAt(33 + dv);
        }
      }
    }
    return _xmsg;
  }
  
  private static void testStripTag() {
    String fileName = "E:/TableTest.htm";
    try {
      BufferedReader r = new BufferedReader(new FileReader(fileName));
      String line;
      StringBuffer sb = new StringBuffer();
      while((line = r.readLine()) != null) {
        sb.append(line).append("\n");
      }
      line = StringCtrl.textBetweenTag(sb.toString(), "meta", true);
      System.out.println("\n\n-------- Result ------\n"+line);
    } catch (Exception e) {
      e.printStackTrace();
    }
    
  }
}
