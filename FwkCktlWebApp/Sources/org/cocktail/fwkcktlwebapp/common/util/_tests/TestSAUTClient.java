package org.cocktail.fwkcktlwebapp.common.util._tests;

import org.cocktail.fwkcktlwebapp.common.util.SAUTClient;


public class TestSAUTClient {
//  private static final String SAUT_URL = "http://www.univ-lr.fr/cgi-bin/WebObjects/SAUT.woa/wa/";
//   private static final String SAUT_URL = "http://localhost:9242/cgi-bin/WebObjects.exe/SAUT.woa/wa";
  //private static final String SAUT_URL = "http://10.1.11.128:4444/cgi-bin/WebObjects/ServAut.woa/wa";
//  private static final String SAUT_URL = "http://pixar.univ-lr.fr:4444/cgi-bin/WebObjects/ServAut.woa/wa";
  private static final String SAUT_URL = "http://www.univ.u-3mrs.fr/cgi-bin/WebObjects/ServAut.woa/wa/";
  
  public static void main(String[] args) {
    String bd_id = "GRHUM";
    String usr_login = "astockus";
    String usr_pass = "bidule";
    String alias = "Test du client SAUT"; // "Appli test";
//    if (args.length != 1) {
//      System.out.println("Utilisation: TestSAUTClient <application_id");
//      System.exit(0); 
//    }
    SAUTClient saut = new SAUTClient(SAUT_URL);
    saut.setNoProxyHosts("*.univ-lr.fr");
    System.out.println("--- Test de connexion a la base ---");
    String dico = saut.requestDecryptedConnectionInfo(bd_id, alias);
    System.out.println("BD ID : "+bd_id);
    System.out.println("Reponse : \n----\n"+dico+"\n----");
    System.out.println("--- Test de l'utilisateur ---");
    dico = saut.requestDecryptedUserInfo(usr_login, usr_pass, alias);
    System.out.println("Login : "+usr_login);
    System.out.println("Reponse : \n----\n"+dico+"\n----");
    System.out.println("Hashtable : \n----\n"+SAUTClient.toProperties(dico)+"\n----");
  }
}
