package org.cocktail.fwkcktlwebapp.common.util._tests;


import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;

//import fr.univlr.cri.util.wo4.*;

/**
 * @author astockus
 *
 * To change the template for this generated type comment go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
public class TestDateCtrl {

  public static void main(String args[]) {
//     testCurrentDate();
    testHoliday();
//    testWeekNumber();
//    testValide();
//     testTimeZonesWO5(); 
//    testTimeZonesWO4();
//    testDateCompletion();
  }

//  private static void testTimeZonesWO5() {
//    String sDate = "10/10/2003";
//    System.out.println("Test Time Zones :");
//    //
//    NSTimestamp t;
//    //
//    NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", true));
//    System.out.println("TZ -> Europe/Paris : "+NSTimeZone.defaultTimeZone());
//    // System.out.println("  DateCtrl.now() : "+DateCtrl.now());
//    t = new NSTimestamp();
//    System.out.println("  new NSTimestamp() : "+t);
//    System.out.println("  dateToString() : "+DateCtrl.dateToString(t, "%d/%m/%Y %H:%M"));
//    System.out.println("  toLocalDate() : "+DateCtrl.toLocalDate(t));
//    System.out.println("  toLocalDate(toLocalDate()) : "+DateCtrl.toLocalDate(DateCtrl.toLocalDate(t)));
//    System.out.println("  now() : "+DateCtrl.now());
//    System.out.println("  stringToDate() "+sDate+" : "+DateCtrl.stringToDate("10/10/2003"));
//    System.out.println("  dateToString(stringToDate()) "+sDate+" : "+
//      DateCtrl.dateToString(DateCtrl.stringToDate("10/10/2003"), "%d/%m/%Y %H:%M:%S"));
//    
//    
//    
//    NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("GMT", true));
//    System.out.println("\nTZ -> GMT : "+NSTimeZone.defaultTimeZone());
//    t = new NSTimestamp();
//    System.out.println("  t : "+t);
//    //
//    System.out.println("\nTZ With Name : "+NSTimeZone.timeZoneWithName("Europe/Paris", true));
//    NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", true));
//    System.out.println("\n  String to Date : ");
//    sDate = "10/11/2003";
//    System.out.println("  t : "+DateCtrl.stringToDate(sDate));
//  }

//  private static void testTimeZonesWO4() {
//    System.out.println("Test Time Zones :");
//    //
//    NSGregorianDate t = new NSGregorianDate();
//    System.out.println("  TZ With Name : "+new NSTimeZone("Europe/Paris", true));
//    NSTimeZone.setDefaultTimeZone(new NSTimeZone("Europe/Paris", true));
//    System.out.println("  DateCtrl.now() : "+DateCtrl.now());
//    System.out.println("  t : "+t);
//    NSTimeZone.setDefaultTimeZone(new NSTimeZone("GMT", true));
//    System.out.println("  TZ With Name : "+new NSTimeZone("GMT", true));
//    System.out.println("  t : "+t);
//    System.out.println("  toLocal(t) : "+DateCtrl.toLocalDate(t));
//    System.out.println("  toLocal(toLocal(t)) : "+DateCtrl.toLocalDate(DateCtrl.toLocalDate(t)));
//    System.out.println("  NSTimezone.defaultTimeZone() : "+NSTimeZone.defaultTimeZone());
//  }

  private static void testDateCompletion() {
    System.out.println("Test date completion ");
    System.out.println(  "10 : "+DateCtrl.dateCompletion("10"));
    System.out.println(  "10/11 : "+DateCtrl.dateCompletion("10/11"));
  }

  private static void testValide() {
    String sDate = "31/11/2003";
    System.out.println("Test date valide :");
    System.out.println("  Date : "+sDate+", is valide : "+DateCtrl.isValid(sDate));
  }

  private static void testCurrentDate() {
    System.out.println("DateCtrl.getCurrentDateTime() : "+DateCtrl.currentDateTimeString());
    System.out.println("TZ -> GMT");
    NSTimeZone.setDefault(NSTimeZone.timeZoneWithName("GMT", true));
    System.out.println("DateCtrl.getCurrentDateTime() : "+DateCtrl.currentDateTimeString());
    System.out.println("TZ -> Reunion");
    NSTimeZone.setDefault(NSTimeZone.timeZoneWithName("Indian/Reunion", true));
    System.out.println("DateCtrl.getCurrentDateTime() : "+DateCtrl.currentDateTimeString());
  }
  
  private static void testHoliday() {
    NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("Europe/Paris", true));
    NSTimestamp date = DateCtrl.stringToDate("23/05/2004 00:00", "%d/%m/%Y %H:%M");
    
    System.out.println("Date : "+date+", is holiday : "+DateCtrl.isHolidayFR(date));
  }

  private static void testWeekNumber() {
    NSTimeZone.setDefaultTimeZone(NSTimeZone.timeZoneWithName("WEST", true));
    NSTimestamp date = DateCtrl.stringToDate("14/01/2003");
    
    System.out.println("Date : "+date+", week no. : "+DateCtrl.weekNumber(date));
  }
}
