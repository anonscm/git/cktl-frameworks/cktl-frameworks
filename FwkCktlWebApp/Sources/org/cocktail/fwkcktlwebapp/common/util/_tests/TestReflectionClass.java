package org.cocktail.fwkcktlwebapp.common.util._tests;

public class TestReflectionClass {
  public void methodNoParameters() {
    System.out.println("-- TestReflectionClass.methodNoParameters --");
  }
  
  public void methodStrings(String arg1, String arg2, String arg3) {
    System.out.println("-- TestReflectionClass.methodStrings --");
    System.out.println("  arg1 : "+arg1);
    System.out.println("  arg2 : "+arg2);
    System.out.println("  arg3 : "+arg3);
  }
  
  public void methodIntegers(Integer arg1, Integer arg2, Integer arg3) {
    System.out.println("-- TestReflectionClass.methodInts --");
    System.out.println("  arg1 : "+arg1);
    System.out.println("  arg2 : "+arg2);
    System.out.println("  arg3 : "+arg3);
  }
  
  public void methodInts(Integer arg1, int arg2, int arg3) {
    System.out.println("-- TestReflectionClass.methodInts --");
    System.out.println("  arg1 : "+arg1);
    System.out.println("  arg2 : "+arg2);
    System.out.println("  arg3 : "+arg3);
  }
}
