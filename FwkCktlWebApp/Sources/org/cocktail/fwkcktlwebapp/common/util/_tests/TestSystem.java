package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Random;

import org.cocktail.fwkcktlwebapp.common.util.SystemCtrl;


/**
 * Test des trucs de systeme.
 */
public class TestSystem {
  public static void main(String args[]) throws InterruptedException {
    showEnv();
//    openFile();
//    execCommad();
    timeStamp();
  }
  
  private static void timeStamp() throws InterruptedException {
    System.out.println("*** Test Timestamp ***");
    Random random = new Random((new Date()).getTime());
    System.out.println("Timestamp : "+random.nextLong());
    random = new Random((new Date()).getTime());
    System.out.println("Timestamp : "+random.nextLong());
    System.out.println("Timestamp : "+random.nextLong());
    System.out.println("Timestamp : "+random.nextLong());
    System.out.println("Done.");
  }

  private static void execCommad() {
    String command = "C:\\Programs\\Java\\j2sdk\\bin\\java.exe -version";
    System.out.println("*** Test Exec Command ***");
    System.out.println("Command : "+command);
    try {
      String result = SystemCtrl.execCommand(command);
      System.out.println(result);
    } catch(Exception e) {
      e.printStackTrace();
    }
    System.out.println("Done.");
  }

  private static void openFile() {
//    String filePath = "E:/DocumentTest.pdf";
//    String filePath = "/Users/astockus/MyDevel/DocumentTest.pdf";
    String filePath = "file:///D:/Shared/Doc/:j2sdk142/relnotes/features.html";
//	String filePath = "file:/mnt/win_d/Shared/Doc/j2sdk142/relnotes/license.html";
//    String filePath = "http://www.google.com/";
    System.out.println("*** Open File ***");
    System.out.println("File Path : "+filePath);
    String result = SystemCtrl.openFile(filePath);
    if (result == null)
      System.out.println(" -> OK");
    else
      System.out.println(" -> Error :\n"+result);
  }

  private static void showEnv() {
    System.out.println("*** Test Environement ***");
    System.out.println("-- Java Info --");
    showProperties(SystemCtrl.javaEnv());
//    System.out.println("OS Name : "+SystemCtrl.osName());
//    System.out.println("OS Version : "+SystemCtrl.osVersion());
    // On lance le procesus avec la commande set
    System.out.println("-- System Info --");
    showProperties(SystemCtrl.systemEnv());
    // Le repertoire des fichiers temporaires
    System.out.println("-- Repertoire temporaire --");
    System.out.println(SystemCtrl.tempDir());
    System.out.println("-- Variables d'environement --");
    System.out.println(" windir : "+SystemCtrl.systemVariable("windir"));
    System.out.println(" HOME : "+SystemCtrl.systemVariable("HOME"));
    System.out.println(" HOMEPATH : "+SystemCtrl.systemVariable("HOMEPATH"));
  }
  
  private static void showProperties(Hashtable props) {
    Enumeration enumeration = props.keys();
    Object key;
    while(enumeration.hasMoreElements()) {
      key = enumeration.nextElement();
      System.out.println(key+"="+props.get(key));
    }
  }
  
  
}
