package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.util.CktlXMLWriter;


/**
 * @author Administrateur
 *
 * To change the template for this generated type comment go to
 * Window&gt;Preferences&gt;Java&gt;Code Generation&gt;Code and Comments
 */
public class TestXMLWriter {
  public static void main(String[] args) {
    // testXMLFile();
    testXMLString();
  }
  
	private static void testXMLString() {
    System.out.println("Test d'ecriture d'une chaine de carateres XML");
    try {
       // Le fichier XMl
       StringWriter xmlString = new StringWriter();
       CktlXMLWriter w = new CktlXMLWriter(xmlString);
       w.setEscapeSpecChars(true);
       w.setDTD("presonne", "schema.dtd");
       w.setIdentChars("  ");
       // w.setCompactMode(true);
       w.setNamespace("six", "http://www.univ-lr.fr/CRI/Apps/SIX");
       performWriterXML(w);
       // Terminer l'ecriture du document XML
       w.close();
       System.out.println("La chaine XML est cree :");
       System.out.println("-------------------");
       System.out.println(xmlString.toString());
       System.out.println("-------------------");
     } catch(Exception ex) {
         ex.printStackTrace();
         System.out.println("Erreur d'ecriture de XML :\n"+ex.getMessage());
     }
	}

	private static void testXMLFile() {
    System.out.println("Test d'ecriture d'un fichier XML");
    try {
       // Le fichier XMl
       String xmlFileName = "C:/Temp/Test.xml";
       CktlXMLWriter w = new CktlXMLWriter(xmlFileName);
       w.setEscapeSpecChars(true);
       performWriterXML(w);
       // Terminer l'ecriture du document XML
       w.close();
       System.out.println("Le document XML est cree :\n  "+xmlFileName);
     } catch(Exception ex) {
         ex.printStackTrace();
         System.out.println("Erreur d'ecriture d'un document XML :\n"+ex.getMessage());
     }
  }
  
  private static void performWriterXML(CktlXMLWriter w) throws IOException {
    // On initialise le document
    w.startDocument();
    w.writeComment("Exemple d'un document XML");
    w.startElement("personne");
    w.writeElement("nom", "John");
    w.writeElement("prenom", "Doe");
    Hashtable addrType = new Hashtable();
    addrType.put("type", "personnelle");
    w.startElement("adresse", addrType);
    w.writeElement("ville", "La Rochelle");
    w.writeElement("cp", "17000");
    w.endElement(); // "adresse"
    w.startElement("commentaire");
    w.writeCharacters("9;=A'?(\"`<>");
    w.endElement(); // "commentaire"
    w.endElement(); // "personne"
    // Il est recommende de finir le document par l'appel suivant
    w.endDocument();
  }
}
