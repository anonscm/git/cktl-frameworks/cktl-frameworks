package org.cocktail.fwkcktlwebapp.common.util._tests;

import org.cocktail.fwkcktlwebapp.common.util.CRIpto;

/*
 * Copyright (C) 2003 Universite de La Rochelle
 *
 * This file is part of CRIUtil.
 *
 * CRIUtil is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIUtil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

public class TestCrypt {

  public static void main(String args[]) {
  	//testCrypt();
  	//testPass();
  	//testCryptDico();
    //testStringToHex();
    //testDecrypt();
  }

  /**
   * 
   */
  private static void testDecrypt() {
    // System.out.println(CRIpto.decrypt("C5515AD500723FBB19A40F428B95397F2630DF83BD71C38DAB8D3AA62A5A914418C5FDC255FADE26039BBCFADC96942A36438C728DA8DE18031D6DEF4BD6E36E5478F0B937D310B804352EACBECDE6EFB9BB1497368B1EC8859CD454629F91B01E981D2909B121CAE25FC8D72E8EC66D138663DB08CF2476"));
    System.out.println(CRIpto.decrypt("C5515AD500723FBB19A40F428B95397F2630DF83BD71C38DAB8D3AA62A5A914418C5FDC255FADE26039BBCFADC96942A36438C728DA8DE18031D6DEF4BD6E36E5478F0B937D310B804352EACBECDE6EFB9BB1497368B1EC8859CD454629F91B01E981D2909B121CAE25FC8D72E8EC66D138663DB08CF2476"));
  }

  private static void testStringToHex() {
    String ori = "2PtiC";
    StringBuffer hex = new StringBuffer();
    String s;
    
    for(int i=0; i<ori.length(); i++) {
      s = Integer.toHexString(ori.charAt(i)).toUpperCase();
      if (s.length() < 0) hex.append("0");
      hex.append(s);
    }
    System.out.println("Original : "+ori);
    System.out.println("hex : " +hex);
  }

  private static void testCryptDico() {
    System.out.println("Test Cript : crypt/decrypt()");
    String chaine = "userName=grhum\nusername=grhum\npassword=clement\nserverId=GEST\nURL=jdbc:oracle:thin:@127.0.0.1:1521:gest\nsautEndMark=END";
    String crpt;
    System.out.println("Chaine : "+chaine);
    crpt = CRIpto.crypt(chaine);
    System.out.println("Crypted : "+crpt);
    System.out.println("Decrypted : "+CRIpto.decrypt(crpt));
  }

  private static void testPass() {
    System.out.println("Test crypt password");
    String pass = "thepass";
    String pass2 = "theotherpass";
    //String crypted = CRIpto.passCrypt(pass);
    //System.out.println("Passzword : "+pass);
    //System.out.println("Crypted : "+crypted);
    //System.out.println("Is same with '"+pass+"' : "+CRIpto.isSamePass(pass, crypted));
    //System.out.println("Is same with '"+pass2+"' : "+CRIpto.isSamePass(pass2, crypted));
  }

  /**
   * 
   */
  private static void testCrypt() {
    System.out.println("Test Cript : crypt/decrypt()");
    String chaine = "ABCFTRGS5454";
    String crpt;
    System.out.println("Chaine : "+chaine);
    crpt = CRIpto.crypt(chaine);
    System.out.println("Crypted : "+crpt);
    System.out.println("Decrypted : "+CRIpto.decrypt(crpt));
  }
}
