package org.cocktail.fwkcktlwebapp.common.util._tests;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Date;

import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.GEDClient;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;


/*
 * Resultats de tests :
 * 
 * elea - HP-UX - WO 4.5.1 - jdk 1.1.8
 *   create> 0min. 33sec. 348msec.
 *    clean> 0min. 11sec. 386msec.
 *   delete> 0min. 0sec. 911msec.
 * 
 * vinzed2n - FreeBSD - /usr/local/jdk1.4.2/
 *   create> 1min. 31sec. 832msec.
 *    clean> 1min. 22sec. 579msec.
 *   delete> 0min. 4sec. 5msec.
 * 
 * vinzed2n - FreeBSD - /usr/local/linux-sun-jdk1.4.2
 *   create> 1min. 33sec. 14msec.
 *    clean> 1min. 22sec. 119msec.
 *   delete> 0min. 3sec. 986msec.
 * 
 * skinner - MacOS - jdk 1.4.2
 *   create> 1min. 25sec. 523msec.
 *    clean> 1min. 10sec. 1msec.
 *   delete> 0min. 3sec. 465msec.
 * 
 * pixar - MacOS - jdk 1.4.2
 *   create> 3min. 31sec. 324msec.
 *    clean> 2min. 8sec. 105msec.
 *   delete> 0min. 10sec. 525msec.
 * 
 * manzana - Win XP - jdk 1.4.2
 *   create> 3min. 24sec. 775msec.
 *    clean> 3min. 46sec. 646msec.
 *   delete> 0min. 10sec. 816msec.
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */


public class TestGEDFSClient {
  private static final String APP_CODE = "COUR"; // Code de l'application
  private static final String CAT_CODE = "TEST"; // Code de la categorie
  private static final String GEDFS_HOST = "pixar.univ-lr.fr"; // Voir GRHUM_PARAMETRES
  // private static final String GEDFS_HOST = "www.univ-lr.fr"; // Voir GRHUM_PARAMETRES
  private static int GEDFS_PORT = 9251; // Voir GRHUM_PARAMETRES
  private static int NO_INDIVIDU = 2356; // Voir GRHUM.INDIVIDU_ULR
  private static int documentPereId = -1;
  
  /* ===== Test de client ====== */
  public static void main(String[] args) {
    System.out.println("Starting GED client...");
    GEDClient client =
      new GEDClient(GEDFS_HOST, GEDFS_PORT, NO_INDIVIDU, APP_CODE);
    long beg = new Date().getTime();
    // -- Enregistrement
//    testEnregistrerFichier(client, "D:/DocumentTest.pdf");
    // testEnregistrerFichierNom(client, args[0], "DocumentTest.bin.1");
    // testEnregistrerFichierStream(client, args[0], "DocumentTest.bin.2");
    // testEnregistrerFichierBytes(client, args[0], "DocumentTest.bin.3");
    // testEnregistrerReference(client, args[0]);
    // -- Remplacement
//     testRemplacerFichier(client, "E:/DocumentTest.ps", "17675");
    // testRemplacerFichierNom(client, args[0], "DocumentTest.bin.4", args[1]);
    // testRemplacerFichierStream(client,  args[0], "DocumentTest.bin.5", args[1]);
    // testRemplacerReference(client, args[0], 45);
    // -- Inspection
//     testInspecter(client, "33759");
    // -- Manipulations des repertoires
    // testReserverRoot(client);
//    testEnregistrerDir(client, "D:/TempStuff/DocForGEDFS");
//    testViderDir(client, "33759");
    // -- Suppression
    testSupprimer(client, "33759");
    //
    //
     long dur = new Date().getTime() - beg;
     long min = dur/60000;
     long sec = (dur - (min*60000))/1000;
     long millis = dur - ((min*60000)) - (sec*1000);
     System.out.println("Duree : "+dur);
     System.out.println("Duree d'operation : "+min+"min. "+sec+"sec. "+millis+"msec.");
  }

  private static void showUsage() {
    System.out.println("Utilisation : TestGEDFSClient <option>");
    System.out.println("Ici <options> est une des :");    
    System.out.println("  -ef <nom_fichier>");    
  }
  
  private static int stringToInt(String value, int defValue) {
    try {
      return Integer.valueOf(value).intValue();
    } catch (Exception ex) {
      ex.printStackTrace();
      return defValue;
    }
  }

  /* ==== Tests d'enregistrement ==== */
  private static void testEnregistrerFichier(GEDClient client, String fileName)
  {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerDocument(fileName, "Test document", CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testEnregistrerFichierNom(GEDClient client, String fileName,
                                                String nom)
  {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerDocument(fileName, "Test document", nom, CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testEnregistrerFichierStream(GEDClient client, String fileName,
                                                   String nom)
  {
    System.out.println("Test d'enregistrement...");
    client.reset();
    File f = new File(fileName);
    try {
      FileInputStream in = new FileInputStream(f);
      if (client.enregistrerDocument(in, "Test document", f.length(), nom, CAT_CODE) != -1) {
        System.out.println("OK. Server answer:\n  " + client.description());
      } else {
        System.out.println("FAIL. Message:\n  " + client.lastMessage());
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private static void testEnregistrerFichierBytes(GEDClient client, String text,
                                                  String nom)
  {
    System.out.println("Test d'enregistrement...");
    client.reset();
    byte[] b = text.getBytes();
    if (client.enregistrerDocument(b, "Test document", nom, CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testEnregistrerReference(GEDClient client, String reference) {
    System.out.println("Test d'enregistrement...");
    client.reset();
    if (client.enregistrerReference(reference, "Test reference", CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

/* ==== Tests de remplacement des fichiers ==== */
  
  private static void testRemplacerFichier(GEDClient client, String fileName, String docId) {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerDocument(fileName, "Test document", StringCtrl.toInt(docId, -1))) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testRemplacerFichierNom(GEDClient client, String fileName,
                                              String nom, String docId)
  {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerDocument(fileName, "Test document", nom, StringCtrl.toInt(docId, -1))) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testRemplacerFichierStream(GEDClient client, String fileName,
                                                 String nom, String docId)
  {
    System.out.println("Test de remplacement...");
    client.reset();
    File f = new File(fileName);
    try {
      FileInputStream in = new FileInputStream(f);
      if (client.remplacerDocument(in, "Test document", f.length(), nom, StringCtrl.toInt(docId, -1))) {
        System.out.println("OK. Server answer:\n  " + client.description());
      } else {
        System.out.println("FAIL. Message:\n  " + client.lastMessage());
      }
    } catch (IOException ex) {
      ex.printStackTrace();
    }
  }

  private static void testRemplacerReference(GEDClient client, String reference,
                                             int docId)
  {
    System.out.println("Test de remplacement...");
    client.reset();
    if (client.remplacerReference(reference, "Test reference", docId)) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  /* ==== Tests de suppression des fichiers ==== */
  private static void testSupprimer(GEDClient client, String documentId) {
    System.out.println("Test de suppression...");
    client.reset();
    if (client.supprimerDocument(stringToInt(documentId, -1))) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  /* ==== Tests d'inspection des fichiers ==== */
  private static void testInspecter(GEDClient client, String documentId) {
    System.out.println("Test d'inspection...");
    client.reset();
    if (client.inspecterDocument(stringToInt(documentId, -1))) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  /* ==== Manipulation des repertoires ==== */
  private static void testReserverRoot(GEDClient client) {
    System.out.println("Test de la reservation...");
    client.reset();
    if (client.reserverDocument(CAT_CODE) != -1) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static void testViderDir(GEDClient client, String documentId) {
    System.out.println("Test de suppression...");
    client.reset();
    if (client.viderRepertoire(stringToInt(documentId, -1))) {
      System.out.println("OK. Server answer:\n  " + client.description());
    } else {
      System.out.println("FAIL. Message:\n  " + client.lastMessage());
    }
  }

  private static boolean saveFileInDir(
    GEDClient client,
    String fileName,
    String rootDir,
    int pereId) {
    String name;
    String subDir;

    client.reset();
    File f = new File(fileName);
    try {
      name = FileCtrl.getFileName(fileName);
      subDir = StringCtrl.getPrefix(fileName, FileCtrl.SEPARATOR + name);
      subDir = StringCtrl.getSuffix(subDir, FileCtrl.normalizeDirName(rootDir));
      System.out.println("saveFileDir.subDir : " + subDir);
      FileInputStream in = new FileInputStream(fileName);
      if (client.enregistrerDocument(in, "Test de depot d'un repertoire",
            FileCtrl.getFileSize(fileName), FileCtrl.getFileName(fileName),
            subDir, pereId, CAT_CODE) != -1) {
        System.out.println("OK. Server answer:\n  " + client.description());
      } else {
        System.out.println("FAIL. Message:\n  " + client.lastMessage());
        return false;
      }
    } catch (IOException ex) {
      ex.printStackTrace();
      return false;
    }
    return true;
  }

  private static boolean saveDirInDir(
    GEDClient client,
    String dirName,
    String rootDir,
    int parentId) {
    String subName;
    File f = new File(dirName);
    String[] allFiles = f.list();
    for (int i = 0; i < allFiles.length; i++) {
      subName = FileCtrl.normalizeDirName(dirName) + allFiles[i];
      f = new File(subName);
      if (f.isDirectory()) {
        if (!saveDirInDir(client, subName, rootDir, parentId)) {
          System.out.println(
            "Erreur de sauvegarder de repertoire : " + subName);
          return false;
        }
      } else {
        if (!saveFileInDir(client, subName, rootDir, parentId)) {
          System.out.println("Erreur de sauvegarder de fichier : " + subName);
          return false;
        }
      }
    }
    return true;
  }

  private static void testEnregistrerDir(GEDClient client, String dirName) {
    int parentId;
    System.out.println("Test d'enregistrement de repertoire...");
    client.reset();
    File f = new File(dirName);
    if (!f.isDirectory()) {
      System.out.println("\"" + dirName + "\" n'est pas un repertoire");
      return;
    }
    if ((parentId = client.reserverDocument(CAT_CODE)) == -1) {
      System.out.println("Le repertoire racine ne peut pas etre reserve");
      return;
    }
    System.out.println("Le ID du document racine : "+parentId);
    if (!saveDirInDir(client, dirName, dirName, parentId))
      System.out.println("-> Echeque");
    else
      System.out.println("-> Sauvegarde OK");
  }
}
