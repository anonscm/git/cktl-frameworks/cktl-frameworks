package org.cocktail.fwkcktlwebapp.common.util._tests;
/*
 * Created on 12 mars 2003
 *
 * To change this generated comment go to 
 * Window>Preferences>Java>Code Generation>Code and Comments
 */

import java.io.File;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import javax.swing.Icon;
import javax.swing.UIManager;

import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;

public class TestFileCtrl {

  public static void main(String[] args) {
//  	 testDeleteTree();
//  	 testCleanTree();
//     testMakeDirs();
    // testIcones();
//    testListeDir();
//    testDeleteFiles();
//    testFileName();
//    testFileSize();
    testFindFile();
  }
  
  private static void testFileSize() {
    String fileURL = "http://www.rgagnon.com/javadetails/java-0298.html";
    URL url;
    URLConnection conn;
    int size;
    System.getProperties().put("http.proxyHost", "wwwcache.univ-lr.fr");
    System.getProperties().put("http.proxyPort", "3128");
    System.getProperties().put("http.nonProxyHosts", "*.univ-lr.fr");
    System.getProperties().put("ftp.nonProxyHosts", "*.univ-lr.fr");
    try {
      url = new URL(fileURL);
      conn = url.openConnection();
      size = conn.getContentLength();
      if(size < 0)
         System.out.println("Could not determine file size.");
      else
        System.out.println(fileURL + "\nSize: " + size);
      conn.getInputStream().close();
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  private static void testFileName() {
    String filePath = "E:/Users/arunas/Devel/06_Eclipse/WO5/SIX/build";
    System.out.println("File Path : "+filePath);
    System.out.println("File Name : "+FileCtrl.getFileName(filePath));
    System.out.println("File Name (no ext) : "+FileCtrl.getFileName(filePath, true));
  }

  private static void testDeleteFiles() {
    System.out.println("Test FileCtrl : deleteFiles");
    String files = "E:/Temp/subDir/DocumentTest.*";
    System.out.println("Files : "+files);
    if (FileCtrl.deleteFiles(files))
      System.out.println("-> OK");
    else
      System.out.println("-> Fail ! Some failes wehe no deleted.");
  }

  private static void testListeDir() {
    System.out.println("Test FileCtrl : list Dir ");
    String dir = "C:/Data/";
    String pattern = "*";
    System.out.println("File separator : "+File.separatorChar);
    System.out.println("Dir : "+dir);
    System.out.println("Pattern : "+pattern);
    System.out.println("-- List with FileCtrl --");
    showArray(FileCtrl.listDir(dir, pattern, true));
//    System.out.println("-- List with File.list (no pattern) --");
//    showArray((new File(dir)).list());
  }

  private static void testIcones() {
    Hashtable defaults = UIManager.getDefaults();
    Enumeration keys = defaults.keys();
    Object key;
    Object value;
    while(keys.hasMoreElements()) {
      key = keys.nextElement();
      value = defaults.get(key);
      if (value instanceof Icon)
        System.out.println(key +" = "+defaults.get(key));
    }
    System.out.println("\nFileView.fileIcon : "+UIManager.getIcon("FileView.fileIcon"));
    System.out.println("FileView.directoryIcon : "+UIManager.getIcon("FileView.directoryIcon"));
  }

  public static void testDeleteTree() {
    System.out.println("Test FileCtrl : deleteDir");
    String dir = "E:/Temp/subDir";
    System.out.println("Delete tree : "+dir);
  	if (FileCtrl.deleteDir(dir, true))
  	  System.out.println("-> OK");
  	else
      System.out.println("-> Fail !");
  }

  public static void testCleanTree() {
    System.out.println("Test FileCtrl : cleanDir");
    String dir = "E:/Temp/subDir/dir1/dir2";
    System.out.println("Clean in dir : " + dir);
    if (FileCtrl.cleanDir(dir))
      System.out.println("-> OK");
    else
      System.out.println("-> Fail !");
  }
  
  public static void testMakeDirs() {
    System.out.println("Test FileCtrl : makeDirs");
    String path = "E:/Temp/subDir/dir1/dir2/dir_3";
    String subPath = "E:/Temp/";
    System.out.println("Path : "+path);
    System.out.println("Must exists : "+subPath);
    if (FileCtrl.makeDirs(path, subPath))
      System.out.println("-> OK");
    else
      System.out.println("-> Fail");
  }
  
  private static void showArray(String[] sArray) {
    if (sArray == null) {
      System.out.println("No elements");
    } else {
      System.out.println(sArray.length + " elements in array");
      for(int i=0; i<sArray.length; i++)
        System.out.println("  "+sArray[i]);
    }
  }
  
  private static void testFindFile() {
    System.out.println("*** Test Find File ***");
    String fileName = "*.jar";
    String rootDir = "C:/Apple/Library/Frameworks";
    Vector files = FileCtrl.findFiles(rootDir, fileName, true);
    System.out.println("Directory : "+rootDir);
    System.out.println("File : "+fileName);
    System.out.println("-- Files -- ");
    if (files.size() == 0) {
      System.out.println("  No files");
    } else {
      for(int i=0; i<files.size(); i++)
        System.out.println("  "+files.elementAt(i));
    }
  }
}
