package org.cocktail.fwkcktlwebapp.common.util._tests;

import org.cocktail.fwkcktlwebapp.common.util.CryptoCtrl;

/**
 * Classe de test des classes de cryptage
 */
public class TestCocktailCrypt {

	public static void main(String args[]) throws Exception {
		//testCrypt();
		testPass();
	}

	private static void testPass() throws Exception {
		System.out.println("Test match crypt password");
		String pass = "toto";
		// 
		String cryptedUnix = (String) CryptoCtrl.cryptPass(CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, pass);
		//
		String cryptedMd5 = (String) CryptoCtrl.cryptPass(CryptoCtrl.JAVA_METHOD_CRYPT_MD5, pass);
		//
		System.out.println("Password : "+pass+ "(unix="+cryptedUnix+", md5=" + cryptedMd5+")");
		System.out.println("Is same unix with '"+pass+"' : "+CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_UNIX, pass, cryptedUnix));
		System.out.println("Is same md5 with '"+pass+"' : "+CryptoCtrl.equalsToCryptedPass(CryptoCtrl.JAVA_METHOD_CRYPT_MD5, pass, cryptedMd5));
	}
}
