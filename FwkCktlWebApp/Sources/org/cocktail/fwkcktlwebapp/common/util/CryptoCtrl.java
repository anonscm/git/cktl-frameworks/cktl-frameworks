package org.cocktail.fwkcktlwebapp.common.util;


import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.cocktail.crypto.jcrypt.Jcrypt;
import org.cocktail.crypto.md5.MD5Crypt;
import org.cocktail.crypto.sha.Sha1;
import org.cocktail.crypto.sha.Sha256;
import org.cocktail.crypto.sha.Sha384;
import org.cocktail.crypto.sha.Sha512;
import org.cocktail.fwkcktlwebapp.common.CktlLog;

/**
 * Ensemble d'outils dedi&eacute;s au cryptage de mots de passes.
 * Elle n'est constituee que des methodes statiques.<br/>
 * <br/>
 * CRIWebApp propose 2 algorithmes de base : le cryptage UNIX et le cryptage MD5.<br/>
 * <br/>
 * Il est possible d'utiliser d'autres methodes de cryptage que celles
 * fournies avec CRIWebApp. Pour cela, il faut que l'application aie dans
 * son classpath une classe respectant le mod&egrave;le suivant :<br/>
 * <br/>
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 bgcolor=black width=95% align=center class="example"><tr>
 * <td><pre>
 *<code class="javaKeyword">package</code> mon.package.crypto.nomcryptage;
 *
 *<code class="commentJavaDoc">&#47;**
 * * Classe de gestion du cryptage MonCryptage
 * *
 * * @author Nom Prenom &lt;nom.prenom at cocktail.org&gt;
 * *&#47;</code>
 *<code class="javaKeyword">public class</code> MonCryptage {
 *  
 *  <code class="comment">// Effectue le cryptage d'une chaine en clair uncrypted</code>
 *  <code class="javaKeyword">public final static</code> String crypt(String uncrypted);
 *  
 *  <code class="comment">// Effectue le cryptage d'une chaine en clair uncrypted avec la cl&eacute; salt</code>
 *  <code class="javaKeyword">public final static</code> String crypt(String uncrypted, String salt);
 *  
 *  <code class="comment">// Donne la cl&eacute; de d&eacute;cryptage d'un mot de passe crypt&eacute;</code>
 *  <code class="javaKeyword">public final static</code> String getSalt(String crypte);
 *  
 *  <code class="comment">// Effectue la correspondance entre un mot de passe en clair uncrypted et un mot de passe crypt&eacute; crypted</code>
 *  <code class="javaKeyword">public final static boolean</code> match(String uncrypted);
 *}</pre></td></tr></table>
 * </p>
 * <br/>
 * Il suffit alors de passer le chemin de cette classe aux m&eacute;thodes qui la n&eacute;c&eacute;ssitent,
 * avec en param&egrave;tre la chaine de caract&egrave;re suivante (pour notre exemple) : <code class="javaString">"mon.package.crypto.nomcryptage.MonCryptage.crypt"</code><br/>
 * <br/>
 * @see #cryptPass(String, String)
 * @see #equalsToCryptedPass(String, String, String)
 * @see #JAVA_METHOD_CRYPT_UNIX
 * @see #JAVA_METHOD_CRYPT_MD5
 */
public class CryptoCtrl {

	/** cryptage UNIX : le nom de la methode de cryptage */
	public final static String JAVA_METHOD_CRYPT_UNIX 		= Jcrypt.FQDN_CLASS_NAME + ".crypt";
	/** cryptage MD5 : le nom de la methode de cryptage */
	public final static String JAVA_METHOD_CRYPT_MD5 			= MD5Crypt.FQDN_CLASS_NAME + ".crypt";
	public final static String JAVA_METHOD_CRYPT_SHA1 			= Sha1.FQDN_CLASS_NAME + ".crypt";
	public final static String JAVA_METHOD_CRYPT_SHA256 			= Sha256.FQDN_CLASS_NAME + ".crypt";
	public final static String JAVA_METHOD_CRYPT_SHA384 			= Sha384.FQDN_CLASS_NAME + ".crypt";
	public final static String JAVA_METHOD_CRYPT_SHA512 			= Sha512.FQDN_CLASS_NAME + ".crypt";

	/** le nom de la methode comparaison dans une classe de cryptage */
	private final static String METHOD_NAME_MATCH	= ".match";
	
  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>CryptoCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public CryptoCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }
	
	/**
	 * Tester la correspondance entre un mot de passe en clair <code>uncrypted</code> et 
	 * un mot de passe crypt&eacute; <code>crypted</code>, selon un algorithme de cryptage, 
	 * definit par son package, sa classe et la methode <code>javaMethodCrypt</code>.<br/>
	 * <br/>
	 * La methode match(String, String) de la m&ecir;me classe que celle de <code>javaMethodCrypt</code>
	 * est appel&eacute;e pour faire cette comparaison.<br/>
	 *  <br/>
   * En cryptage UNIX, Le mot de passe crypte <code>crypted</code> doit avoir plus de deux caracteres, 
   * car deux premier caracteres sont utilise comme la cle de sont cryptage.<br/>
   * <br/>
   * En cryptage MD5, Le mot de passe crypte <code>crypted</code> doit contenir au moins 3 caracteres "$", 
   * avec entre le 2eme et le 3eme d'autres caracteres, cette zone servant de cle de cryptage.
   *
   * @param javaMethodCrypt : l'algorithme de cryptage
   * @param uncrypted : la chaine en clair
   * @param crypted : la chaine cryptee	 
   * @return <code>true</code> si le mot de passe <code>uncrypted</code> correspond &agrave; <code>crypted</code>.
   * 
   * @see #JAVA_METHOD_CRYPT_UNIX
   * @see #JAVA_METHOD_CRYPT_MD5
   */
	public static boolean equalsToCryptedPass(String javaMethodCrypt, String uncrypted, String crypted) {
		if (!StringCtrl.isEmpty(javaMethodCrypt) && 
				!StringCtrl.isEmpty(uncrypted) && 
				!StringCtrl.isEmpty(crypted)) {
			try {
				String className = getClass(javaMethodCrypt);
				String javaMethodMatch = className + METHOD_NAME_MATCH;
				CktlLog.rawLog("Comparaison de chaines avec la methode " + javaMethodMatch, CktlLog.LEVEL_DEBUG);
				return ((Boolean) getInvokeResult(
						javaMethodMatch, 
						new Class[]{String.class, String.class}, 
						new String[]{uncrypted, crypted})).booleanValue();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
  /**
   * Effectue le cryptage d'un mot de passe <code>uncrypted</code> via une m&eacute;thode 
   * de cryptage donn&eacute;e <code>javaMethodCrypt</code>.<br/>
   *  <br/>
   * @param javaMethodCrypt : la methode de cryptage a utiliser
   * @param uncrypted : le chaine en clair qu'il faut crypter
   * 
   * @return la chaine cryptee
   * 
   * @see #JAVA_METHOD_CRYPT_UNIX
   * @see #JAVA_METHOD_CRYPT_MD5
   */
  public static String cryptPass(String javaMethodCrypt, String uncrypted) throws Exception {
		String crypted = null;
  	if (StringCtrl.isEmpty(javaMethodCrypt)) {
  		throw new Exception("Classe Java non definie pour le type de cryptage " + javaMethodCrypt);
  	}
  	CktlLog.rawLog("Cryptage avec la methode " + javaMethodCrypt, CktlLog.LEVEL_DEBUG);
  	crypted = (String) getInvokeResult(javaMethodCrypt, new Class[]{String.class}, new String[]{uncrypted});
  	return crypted;
  }	
  
  /**
   * Methode interne pour recuperer le nom d'une classe
   * @param fqdnMethod
   * @return le nom de la classe
   */
  private static String getClass(String fqdnMethod) {
  	return fqdnMethod.substring(0, fqdnMethod.lastIndexOf(".") );
  }
  
  /**
   * Methode interne pour simplifier l'usage de reflect
   * @return l'instance de l'objet
   * @throws ClassNotFoundException 
   * @throws NoSuchMethodException 
   */
  private static Object getInvokeResult(
  		String fqdnMethod, Class[] paramClasses, String[] paramValues) throws Exception {
  	Object invokedResult = null;
  	String className = getClass(fqdnMethod);
  	String methodName = fqdnMethod.substring(fqdnMethod.lastIndexOf(".")+1 );
  	try {
  		Class myClass = Class.forName(className);
  		Method methodForCrypt = myClass.getMethod(methodName, paramClasses);
  		invokedResult = methodForCrypt.invoke(myClass, (Object[]) paramValues);
  	} catch (ClassNotFoundException e) {
  		throw new ClassNotFoundException("La classe " + className + " n'a pas ete trouvee. Assurez-vous que cette classe est presente dans le classPath." );
  	}	catch (NoSuchMethodException e1) {
  		throw new NoSuchMethodException("La methode " + methodName + " n'a pas ete trouvee. Assurez-vous que cette methode est presente dans la classe " + className );               
  	} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
  	return invokedResult;
  }
}
