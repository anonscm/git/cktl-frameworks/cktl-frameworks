package org.cocktail.fwkcktlwebapp.common.util;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import com.webobjects.eocontrol._EOCheapCopyMutableArray;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

/**
 * Ensemble d'utilitaires dedies a la gestion des types de donnees <code>NSArray</code> et <code>NSMutableArray</code>.
 */

public class NSArrayCtrl {

	/**
	 * Supprimer tous les doublons d'un tableau. L'algorithme se base sur la methode {@link NSArray#containsObject(Object)} de {@link NSArray}
	 * 
	 * @param <T>
	 * @return le tableau sans doublons
	 */
	public static <T> NSArray<T> removeDuplicate(NSArray<T> array) {
		NSMutableArray<T> myArray = new NSMutableArray<T>(array);
		removeDuplicatesInNSArray(myArray);
		return myArray.immutableClone();
	}

	/**
	 * "Remettre a plat" un {@link NSArray}. On peut se retrouver face a des objets recuperes via {@link NSKeyValueCoding}, qui comporte des tableaux
	 * de tableaux de tableaux ... Cette methode ramene tous les objets contenus dans ces tableaux imbriques au niveau 0, en supprimant les tableaux
	 * vides.
	 * 
	 * @return le tableau transforme
	 */
	public static NSArray flattenArray(NSArray arrays) {
		NSArray arrayPlat = new NSArray();
		for (int i = 0; i < arrays.count(); i++) {
			Object anObject = arrays.objectAtIndex(i);
			if (anObject.getClass() == NSArray.class ||
					anObject.getClass() == NSMutableArray.class ||
					anObject.getClass() == _EOCheapCopyMutableArray.class) {
				if (((NSArray) anObject).count() > 0)
					arrayPlat = arrayPlat.arrayByAddingObjectsFromArray(flattenArray((NSArray) anObject));
			}
			else
				arrayPlat = arrayPlat.arrayByAddingObject(anObject);
		}
		return arrayPlat;
	}

	/**
	 * Teste si la liste donnees est vide. Elle l'est si la raference est <code>null</code> ou si la liste n'a aucun element.
	 * 
	 * @param <T>
	 */
	public static <T> boolean isEmpty(NSArray<T> anArray) {
		return ((anArray == null) || (anArray.count() == 0));
	}

	/**
	 * Effectue une intersection entre plusieurs NSArray.
	 * 
	 * @param <T>
	 * @param lesTableaux
	 * @return L'intersection des tableaux.
	 */
	public static <T> NSArray<T> intersectionOfNSArray(ArrayList<NSArray<T>> lesTableaux) {
		int nbTabs = lesTableaux.size();
		if (nbTabs == 0) {
			return NSArray.emptyArray();
		}
		final NSArray<T> tab1 = lesTableaux.get(0);
		if (nbTabs == 1) {
			return tab1;
		}

		final NSMutableArray<T> res = new NSMutableArray<T>();
		for (int i = 0; i < tab1.count(); i++) {
			//            for (int i = 1; i < tab1.count(); i++) {
			boolean cond = true;
			for (Iterator<NSArray<T>> iter = lesTableaux.iterator(); iter.hasNext() && cond;) {
				cond = cond && iter.next().containsObject(tab1.objectAtIndex(i));
			}
			if (cond) {
				res.addObject(tab1.objectAtIndex(i));
			}
		}
		return res.immutableClone();
	}

	/**
	 * @param <T>
	 * @param array1
	 * @param array2
	 * @return
	 */
	public static <T> NSArray<T> intersectionOfNSArray(NSArray<T> array1, NSArray<T> array2) {
		ArrayList<NSArray<T>> list = new ArrayList<NSArray<T>>(2);
		list.add(array1);
		list.add(array2);
		return intersectionOfNSArray(list);
	}

	/**
	 * Renvoi le complément de ensembleReference dans ensemble. (cad Tous les éléments de ensemble qui ne font pas partie de l'intersection entre
	 * ensembleReference et ensemble)
	 * 
	 * @param <T>
	 * @param ensembleReference
	 * @param ensembleTeste
	 * @return
	 */
	public static <T> NSArray<T> complementOfNSArray(NSArray<T> ensembleReference, NSArray<T> ensemble) {
		final ArrayList<NSArray<T>> tabs = new ArrayList<NSArray<T>>(2);
		tabs.add(ensembleReference);
		tabs.add(ensemble);
		final NSArray<T> intersect = intersectionOfNSArray(tabs);
		final NSMutableArray<T> res = new NSMutableArray<T>();
		final Enumeration<T> iterator = ensemble.objectEnumerator();
		while (iterator.hasMoreElements()) {
			final T element = iterator.nextElement();
			if (intersect.indexOfObject(element) == NSArray.NotFound) {
				res.addObject(element);
			}
		}
		return res;
	}

	/**
	 * Renvoi un tableau contenant les objets de ensemble1 qui ne sont pas dans ensemble2 et les objets de enemble2 qui ne sont pas dans ensemble1.
	 * (l'union du complément de 1 dans 2 et du complément de 1 dans 2).
	 * 
	 * @param <T>
	 * @param ensemble1
	 * @param ensemble2
	 * @return
	 */
	public static <T> NSArray<T> extOfNSArrays(NSArray<T> ensemble1, NSArray<T> ensemble2, boolean distinct) {
		final NSArray<T> fermeture1 = complementOfNSArray(ensemble1, ensemble2);
		final NSArray<T> fermeture2 = complementOfNSArray(ensemble2, ensemble1);
		@SuppressWarnings("unchecked")
		final NSArray<T> res = unionOfNSArrays(new NSArray[] {
				fermeture1, fermeture2
		});
		if (distinct) {
			return getDistinctsOfNSArray(res);
		}
		return res;
	}

	/**
	 * Renvoie l'union de tous les tableaux (sans effectuer de distinct)
	 * 
	 * @param lesTableaux
	 * @return
	 */
	public static <E> NSArray<E> unionOfNSArrays(ArrayList<NSArray<? extends E>> lesTableaux) {
		int nbTabs = lesTableaux.size();
		if (nbTabs == 0) {
			return NSArray.emptyArray();
		}
		//		final NSArray<? extends E> tab1 = lesTableaux.get(0);
		//		if (nbTabs == 1) {
		//			final NSMutableArray<E> tab2 = new NSMutableArray<E>();
		//			tab2.addObjectsFromArray(tab1);
		//			return tab2.immutableClone();
		//		}

		final NSMutableArray<E> res = new NSMutableArray<E>();
		for (Iterator<NSArray<? extends E>> iter = lesTableaux.iterator(); iter.hasNext();) {
			res.addObjectsFromArray(iter.next());
		}
		return res.immutableClone();
	}

	/**
	 * @param <T>
	 * @param lesTableaux
	 * @return
	 */
	public static <T> NSArray<T> unionOfNSArrays(NSArray<? extends T>[] lesTableaux) {
		int nbTabs = lesTableaux.length;
		if (nbTabs == 0) {
			return NSArray.emptyArray();
		}
		//		final NSMutableArray<T> tab1 = new NSMutableArray<T>();
		//		tab1.addObjectsFromArray(lesTableaux[0]);
		//		if (nbTabs == 1) {
		//			return tab1.immutableClone();
		//		}

		final NSMutableArray<T> res = new NSMutableArray<T>();
		for (int i = 0; i < lesTableaux.length; i++) {
			res.addObjectsFromArray(lesTableaux[i]);
		}
		return res.immutableClone();
	}

	/**
	 * Supprime les doublons dans un NSMutableArray, les objets conservés sont ceux avec le plus petit index.
	 * 
	 * @param <T>
	 * @param array
	 */
	public static <T> void removeDuplicatesInNSArray(NSMutableArray<T> array) {
		int i = array.count() - 1;
		while (i >= 0) {
			final Object obj = array.objectAtIndex(i);
			int found = array.indexOfObject(obj);
			if (found != NSArray.NotFound && found != i) {
				array.removeObjectAtIndex(i);
			}
			i--;
		}
	}

	/**
	 * @param <T>
	 * @param array
	 * @return un nouveau NSArray contenant les éléments de array sans les doublons.
	 */
	public static <T> NSArray<T> getDistinctsOfNSArray(NSArray<T> array) {

		final NSMutableArray<T> res = new NSMutableArray<T>();
		for (int i = 0; i < array.count(); i++) {
			final T element = (T) array.objectAtIndex(i);
			if (res.indexOfObject(element) == NSArray.NotFound) {
				res.addObject(element);
			}
		}
		return res.immutableClone();
	}
}
