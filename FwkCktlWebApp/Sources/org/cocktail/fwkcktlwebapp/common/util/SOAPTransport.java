package org.cocktail.fwkcktlwebapp.common.util;


import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;

/**
 * Permet d'executer une requette SOAP en l'envoyant au serveur et recuperer
 * le resultat de sont execution.
 * 
 * <p>Un exemple d'utilisation de la classe&nbsp;:</p>
 * 
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * String result = null; <code class="comment">// Ce sera le resultat</code>
 * SOAPQuery query = ...; <code class="comment">// Initialisation d'une requette SOAP</code>
 * SOAPTransport soapBus = new SOAPTransport(urlDeServeur);
 * <code class="comment">// S'il le faut, on definit les params de proxy</code>
 * soapBus.setNoProxyHosts(noProxyHosts);
 * <code class="comment">// Banzaii...</code>
 * if (soapBus.sendMessage(query)) 
 *   result = soapBus.output();</code></pre></td></tr></table>
 * 
 * <p>Voir la documentation de la classe <code>SOAPQuery</code> pour un exemple
 * de la creation d'une requette SOAP.</p>
 * 
 * @see SOAPQuery
 */
public class SOAPTransport {
  /**
   * Le nom de serveur ou se trouve le serveur SOAP (WebService).
   */
  protected String host;
  
  /**
   * Le numero de port d'ecute du serveur SOAP.
   */
  protected int port;
  
  /**
   * Resultat de l'execution de la requette SOAP.
   */
  protected String output;
  
  /**
   * Le URL d'acces au serveur SOAP.
   */
  protected String queryURL;
  
  /**
   * La liste des serveurs/domaines qui ne doivent pas etre accedes via
   * un serveur proxy.
   */
  private String noProxyHosts;
   
  /**
   * Cree l'objet qui permettra de se connecter au serveur avec l'adresse
   * <code>host</code> et la requette <code>query</code>. Les trois parametres
   * sont convertis en un URL&nbsp;:
   * <div class="example">"http://"+host+":"+port+query</div>
   */
  public SOAPTransport(String host, int port, String query) {        
    this("http://"+host+":"+port+query);
  }

  /**
   * Creer un objet qui permettra de se connecter au serveur avec l'URL
   * <code>queryURL</code>.
   */
  public SOAPTransport(String queryURL) {
    this.queryURL = queryURL;
    this.noProxyHosts = null;
  }
  
  /**
   * Definie les domaines pour lesquels il ne faut pas utiliser le serveur
   * proxy. La chaine peut contenir plusieurs domaines separes par "|".
   * Par exemple, "*.foo.com|localhost".
   */
  public void setNoProxyHosts(String noProxyHosts) {
    this.noProxyHosts = noProxyHosts;
  }
  
  /**
   * Envoie la requette input au serveur dont l'URL etait indique a la
   * creation de l'objet. Retourne <i>true</i> ou <i>false</i> pour indiquer
   * si la requette etait executee avec succes.
   * 
   * @see #output()
   */
  public boolean sendMessage(String input) {
    String str;
    byte[] content = input.getBytes();
    URL url;
    URLConnection urlConn;
	  OutputStream out;
	  BufferedReader in;

    try {
      // Pour etre sur qu'on n'utilise pas le proxy
      // Sinon, la connexion peut lever une exception "IOException" ou "FileNotFound"
      if (noProxyHosts != null) {
        System.getProperties().put("proxySet", "false");
        System.getProperties().put("http.nonProxyHosts", noProxyHosts);
        System.getProperties().put("ftp.nonProxyHosts", noProxyHosts);
      }
      // Construction de la connexion
      url = new URL(queryURL);
      urlConn = url.openConnection();
      urlConn.setDoInput(true);
      urlConn.setDoOutput(true);
      urlConn.setUseCaches(false);
      urlConn.setRequestProperty("Content-Type", "text/xml");
      // Envoi de la requete HTTP
      out = urlConn.getOutputStream();
      out.write(content);
      out.flush();
      out.close();

      // Lecture de la reponse
      in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
      output = "";
      while ((str = in.readLine()) != null) {
        output += str;
      }
      in.close();
      output = output.trim();
      if (output.length() == 0) {
        output = "Response is empty";
        return false;
      }
    } catch(Exception ex) {
      ex.printStackTrace();
      output = ex.getMessage();
      return false;
    }
    return true;
  }
  
  /**
   * Retourne le resultat d'execution d'une requette.
   * 
   * @see #sendMessage(String) 
   */
  public String output() {
    return output;
  }
}
