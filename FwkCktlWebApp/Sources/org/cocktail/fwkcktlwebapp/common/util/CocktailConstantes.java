package org.cocktail.fwkcktlwebapp.common.util;

import java.awt.Color;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.Format;
import java.text.NumberFormat;
import java.util.Locale;

public class CocktailConstantes {

//protected	static final EOClientResourceBundle resourceBundle = new EOClientResourceBundle();
	
	public static final String[] LISTE_MOIS = new String[]{"JANVIER", "FEVRIER", "MARS", "AVRIL", "MAI", "JUIN",
		"JUILLET", "AOUT", "SEPTEMBRE", "OCTOBRE", "NOVEMBRE", "DECEMBRE"};

	public static final String [] LETTRES_ALPHABET = new String[] {"A", "B", "C", "D", "E", "F", "G", "H",
		"I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};

	public static final Integer[] LISTE_ANNEES_DESC = new Integer[]{new Integer("2015"), new Integer("2014"), new Integer("2013"), new Integer("2012"),
		new Integer("2011"), new Integer("2010"), new Integer("2009"), new Integer("2008"), new Integer("2007"), 
				new Integer("2006"), new Integer("2005"), new Integer("2004")};
	public static final Integer[] LISTE_ANNEES_ASC = new Integer[]{new Integer("2004"), new Integer("2005"),
		new Integer("2006"), new Integer("2007"), new Integer("2008"), new Integer("2009"), new Integer("2010"), 
				new Integer("2011"), new Integer("2012"), new Integer("2013"), new Integer("2014"), new Integer("2015")};

	public static final String SAUT_DE_LIGNE = "\n";
	public static final String TABULATION = "\t";
	public static final String SEPARATEUR_EXP = ";";
	
	public static final String VRAI = "O";
	public static final String FAUX = "N";

	public static final String MONSIEUR = "M.";
	public static final String MADAME = "MME";
	public static final String MADEMOISELLE = "MLLE";

	public static final String STRING_EURO = " \u20ac";
	
	// COULEURS
	public static Color BG_COLOR_WHITE = new Color(255,255,255);
	public static Color BG_COLOR_BLACK = new Color(0,0,0);
	public static Color BG_COLOR_GREY = new Color(100, 100, 100);
	public static Color BG_COLOR_RED = new Color(255,0,0);
	public static Color BG_COLOR_GREEN = new Color(0,255,0);
	public static Color BG_COLOR_BLUE = new Color(0,0,255);
	public static Color BG_COLOR_YELLOW = new Color(255,255,0);
	public static Color BG_COLOR_LIGHT_GREY = new Color(100,100,100);
	public static Color BG_COLOR_CYAN = new Color(224, 255, 255);
	public static Color BG_COLOR_PINK = new Color(248,192,218);

	public static Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	public static Color COULEUR_FOND_ACTIF = new Color(255, 255, 255);
	 
	public static final Color COLOR_INACTIVE_BACKGROUND = new Color(222, 222, 222);
	public static final Color COLOR_ACTIVE_BACKGROUND = new Color(0, 0, 0);

    public static final Color COLOR_BKG_TABLE_VIEW = new Color(230, 230, 230);
    public static final Color COLOR_SELECTED_ROW = new Color(127, 155, 165);

    public static final Color COLOR_SELECT_NOMENCLATURES = new Color(100, 100, 100);
    public static final Color COLOR_FOND_NOMENCLATURES = new Color(220, 220, 220);
    public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240, 240, 240);
	
    // FORMATS 
    public static final Format FORMAT_EUROS     = NumberFormat.getCurrencyInstance(Locale.FRANCE);
    public static final Format FORMAT_NUMBER    = NumberFormat.getNumberInstance();
	public static final Format FORMAT_DECIMAL   = new DecimalFormat("#,##0.00");
    public static final Format FORMAT_DATESHORT = DateFormat.getDateInstance(DateFormat.SHORT, Locale.FRANCE);
}
