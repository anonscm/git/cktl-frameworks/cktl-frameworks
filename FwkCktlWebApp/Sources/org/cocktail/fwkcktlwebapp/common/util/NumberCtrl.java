/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlwebapp.common.util;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Vector;

import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class NumberCtrl {
	
	/** */
	public static final String[] listeChiffres = {"0","1","2","3","4","5","6","7","8","9"};

	public static final String[] listeNumber = {"0","1","2","3","4","5","6","7","8","9",".",",","-"};
	
	
	public static BigDecimal computeSumForKeyBigDecimal(NSArray eo, String keyBigDecimal) {
		if (eo == null || eo.count() == 0)
			return new BigDecimal(0);
		return (BigDecimal) eo.valueForKeyPath("@sum." + keyBigDecimal);
	}

	public static boolean inferieur(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = true;

		if (resultat == 0)
			reponse = false;

		if (resultat == 1)
			reponse = false;

		return reponse;
	}

	public static boolean inferieurOuEgal(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = true;

		if (resultat == 0)
			reponse = true;

		if (resultat == 1)
			reponse = false;

		return reponse;
	}

	public static boolean superieur(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = false;

		if (resultat == 0)
			reponse = false;

		if (resultat == 1)
			reponse = true;

		return reponse;
	}

	public static boolean superieurOuEgal(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = false;

		if (resultat == 0)
			reponse = true;

		if (resultat == 1)
			reponse = true;

		return reponse;
	}

	public static boolean egal(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = false;

		if (resultat == 0)
			reponse = true;

		if (resultat == 1)
			reponse = false;

		return reponse;
	}

	public static boolean different(BigDecimal a, BigDecimal b) {
		boolean reponse = false;

		int resultat = a.compareTo(b);

		if (resultat == -1)
			reponse = true;

		if (resultat == 0)
			reponse = false;

		if (resultat == 1)
			reponse = true;

		return reponse;
	}

	/**
	 * Calcule la somme de toutes les valeurs contenues dans un champ des EOEnterpriseObjects contenues dans le tableau.
	 * 
	 * @param array Tableau d'EOEnterpriseObjects
	 * @param keyName Nom de l'attribut qui contient les valeurs e sommer (le champ doit etre un BigDecimal)
	 * @return Le resultat de la somme
	 */
	public static final BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
		return calcSommeOfBigDecimals(array.vector(), keyName);
	}

	public static final BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
		BigDecimal res = new BigDecimal(0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}
	
	/** */
	public static boolean estUnChiffre(String chaine)
	{
		NSArray chiffres = new NSArray(listeChiffres);

		if (chiffres.containsObject(chaine))	return true;

		return false;
	}

	/** */
		public static Number max(Number n1, Number n2)
		{
			if (n1.intValue() >= n2.intValue())
				return n1;

			return n2;		
		}

	/** Chaine ne contenant que des chiffres, . ou , */
	public static boolean isANumber(String chaine)
	{
		NSArray nombre = new NSArray(listeNumber);

		for (int i=0;i< chaine.length();i++)
		{
			if (!nombre.containsObject(""+chaine.charAt(i)))
				return false;
		}

		return true;
	}
}
