package org.cocktail.fwkcktlwebapp.common.util;


import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Permet de decomposer une chaine de caracteres en sous-chaines. Chaque
 * sous-chaine est delimitee par un separateur.
 * 
 * <p>La classe propose les fonctionnalites similaires a celles de la classe
 * {@link StringTokenizer}. Une difference est que le
 * delimiteur peut etre compose de plusieurs caracteres. Ainsi, on ne peut
 * indiquer qu'un seul delimiteur pour un objet "tokenizer".
 * 
 * <p>Voir la documentation de la classe {@link StringTokenizer}
 * pour plus d'informations.
 */
public class CktlStringTokenizer
  implements Enumeration
{
  /**
   * Indique si les delimiteurs doivent etre retournes parmis les autres
   * elements.
   */
  private boolean returnTokens;
  
  /**
   * Indique le delimiteur par defaut.
   */
  private String delimiter;
  
  /**
   * La chaine en cour.
   */
  private String string;
  
  /**
   * Cree un objet "tokenizer" pour decomposer une chaine de caracteres.
   * La chaine est composee des elements separes l'un de l'autre par la chaine
   * du delimiteur <code>delim</code>.
   *
   * @param str La chaine a decomposer.
   * @param delim La chaine delimitant les elements.
   * @param returnTokens Si <code>true</code> les delimiteurs seront renvoyes parmi
   *     les autres elements. Sinon, les delimiteurs sont ignores.
   */
  public CktlStringTokenizer(String str, String delim, boolean returnTokens) {
    this.string = str;
    this.delimiter = delim;
    this.returnTokens = returnTokens;
  }

  /**
   * Cree un objet "tokenizer" pour decomposer une chaine de caracteres.
   * La chaine est composee des elements separes l'un de l'autre par la chaine
   * du delimiteur <code>delim</code>. Les delimiteurs eux-memes sont ignores
   * (le fonctionnement par defaut).
   * 
   * @param str La chaine a decomposer.
   * @param delim La chaine delimitant les elements.
   */
  public CktlStringTokenizer(String str, String delim) {
    this(str, delim, false);
  }
  
  /**
   * Cree un tokenizer pour decomposer une chaine de caracteres.
   * La chaine est decomposee dans les elements separes par
   * les espaces (separateur par defaut).
   * 
   * @param str La chaine a decomposer.
   */
  public CktlStringTokenizer(String str) {
    this(str, " ", false);
  }

  /**
   * Retourne le nombre des elements restant dans la chaine.
   * Les entrees du delimiteur ne sont pas comptees.
   * 
   * @return Le nombre des elements dans la chaine.
   */
  public int countTokens() {
    int index = -1;
    int count = 0;

    if (!hasMoreTokens()) return 0;
    do {
    index = string.indexOf(delimiter, index+delimiter.length()+1);
      if (index >= 0) count++;
    } while(index != -1);
    return count;
  }

  /**
   * Test si la chaine contient encore des elements.
   * Cette methode correspond a d'implementation de l'interface
   * {@link Enumeration}
   * 
   * @return <code>true</code> si la liste d'elements n'est pas vide,
   *     <code>false</code> dans le cas contraire.
   */
  public boolean hasMoreElements() {
    return hasMoreTokens();
  }
  
  /**
   * Test si la chaine contient encore des elements.
   * 
   * @return <code>true</code> si la liste d'elements n'est pas vide,
   *     <code>false</code> dans le cas contraire.
   */
  public boolean hasMoreTokens() {
    return (string != null);
  }

  /**
   * Retourne l'element suivant en tant qu'un objet. Cette methode
   * implemente l'interface Enumeration.
   * 
   * @return L'element suivant. L'objet retourne est un objet String.
   * @exception NoSuchElementException L'exception est levee s'il
   *     ne reste plus d'elements.
   */
  public Object nextElement()
    throws NoSuchElementException
  {
    return nextToken();
  }
  
  /**
   * Retourn l'element suivant en tant qu'une chaine de caracteres.
   * 
   * @return La chaine de caracteres suivante.
   * @exception NoSuchElementException S'il n'existe plus d'elements.
   */
  public String nextToken()
    throws NoSuchElementException
  {
    return nextToken(delimiter);
  }
  
  /**
   * Retourne l'element suivant en tant qu'une chaine de caracteres.
   * Pour cet appel, le nouveau element est retrouve en utilisant
   * le nouveau delimiteur <i>delim</i>.
   * 
   * @param delim Le delimiteur utilise pour cette appel de la methode
   *     (un delimiteur temporaire).
   * @return La chaine de caracteres suivante.
   * @exception NoSuchElementException Il n'existe plus d'elements.
   */
  public String nextToken(String delim)
    throws NoSuchElementException
  {
    String token;
    int i;
    if(!hasMoreTokens())
      throw new NoSuchElementException("No more tokens.");
    i = string.indexOf(delim);
    if (i == -1) {
      token = string;
      string = null;
    } else {
      if (i == 0) token = "";
      else token = string.substring(0, i);
      string = string.substring(i+delim.length());
      if (string == null) string = "";
    }
    return token;
  }
}
