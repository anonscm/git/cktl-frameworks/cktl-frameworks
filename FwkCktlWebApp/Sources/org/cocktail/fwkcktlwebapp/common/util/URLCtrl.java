package org.cocktail.fwkcktlwebapp.common.util;


import java.io.UnsupportedEncodingException;

/**
 * Propose des methodes relatives a la gestions des URL et des requetes HTTP.
 * Toutes les methodes de cette classe sont statiques.
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public class URLCtrl {
  
  /**
   * Encode la chaine de caracteres <code>content</code> en utilisant
   * l'encodage UTF-8. La chaine encode peut etre transmises via les requettes
   * HTTP, par exemple.
   * 
   * <p>Pour decoder la chaine, il faut utiliser la methode <code>decode</code>.
   * </p>
   * 
   * @param content
   *          La chaine a encode.
   * @return La chaine encode.
   * @see #decode(String)
   */
  public static String encode(String content) {
    try {
      return URLEncoder.encode(content, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      return content;
    }
  }
  
  /**
   * Decode la chaine de caracteres <code>content</code> precedement encodee
   * avec la methode <code>encode</code>. L'encodage de caracteres utilise
   * est "UTF-8".
   * 
   * @param content La chaine a decoder.
   * @return La chaine decodee.
   * @see #encode(String)
   */
  public static String decode(String content) {
    try {
      return URLDecoder.decode(content, "UTF-8");
    } catch (UnsupportedEncodingException ex) {
      return content;
    }
  }
  
  /**
   * Modifie le lien d'acces a une application WebObject <code>woURL</code> en
   * lui donnant la forme&nbsp;:
   * <div align="center"><code>
   * .../cgi-bin/WebObjects/UneApplication.woa
   * </code></div>
   * 
   * Si le lien contient des informations apres ".woa" (ex., acces a une directe
   * action), toutes ces informations sont "coupees". Si le lien ne contient pas
   * d'extention ".woa", mais inclue "cgi-bin" et "WebObjects", cette extention
   * est ajoutee. Aucune modification n'est faite dans le cas contraire.
   * 
   * @param woURL
   *          Le lien d'acces a une application WebObjects.
   * @return Le lien "normalise".
   */
  public static String getWOAppAccessURL(String woURL) {
    int idx; 
    // Si URL contient le "woa", on coupe tout ce qui suit
    idx = woURL.indexOf(".woa");
    if (idx > 0) {
      woURL = woURL.substring(0, idx+4);
    } else {
      idx = woURL.indexOf("cgi-bin");
      if ((idx > 0) && (woURL.indexOf("/WebObjects") > idx)) {
        woURL += ".woa";
      }
    }
    return woURL;
  }
  
  /**
   * Modifie le lien <code>anURL</code> en y ajoutant le symbole "/" s'il
   * indique un repertoire. On considere que lien pointe vers un repertoire, si
   * le nom apres "/" contient le sybole "." (le nom de fichier avec une
   * extention).
   * 
   * @param anURL
   *          Le URL a modifier si necessaire.
   * @param forceSlash
   *          Indique s'il faut ajouter le "/" meme si le lien peut pointer vers
   *          un fichier.
   * @return Le URL modifie.
   */
  public static String normalizeURL(String anURL, boolean forceSlash) {
    // Rien, si le symbole y est deja
    if (anURL.endsWith("/"))
      return anURL;
    if (!forceSlash) {
      // On ne fait rien, si c'est le symbole
      if (anURL.lastIndexOf('/') < anURL.lastIndexOf('.'))
        return anURL;
    }
    // Sinon, on ajoute
    return anURL+"/";
  }
}
