package org.cocktail.fwkcktlwebapp.common.util;


import java.io.ByteArrayInputStream;
import java.util.Hashtable;
import java.util.Properties;
import java.util.Vector;

/**
 * Permet de communiquer avec le serveur d'authentification et d'autentifier
 * les clients d'un systeme d'information : applications, utilisateurs,...
 * 
 * <p>La communication entre le client et le serveur est base sur le
 * protocole HTTP/SOAP.</p>
 * 
 * <p>Les methodes de cette classe sont utilisees lors de l'initialisation de
 * l'application.</p>
 * 
 * @see SOAPTransport
 * @see SOAPParser
 * @see SOAPQuery
 * @see CRIpto
 */
public class SAUTClient {
  // TODO Ajouter la verification si le service SAUT est disponible
  private static String MsgCommError =
    "errno=100\nmsg=Erreur de communication avec le serveur";
  
  // Les variables permettant de construire le URL de connexion
  // aux services du serveur SAUT
  private static final String FAULT_CODE_ID = "faultcode";
  private static final String FAULT_STRING_ID = "faultstring";
  private static final String DB_CONNECTION_ACTION = "bd";
  private static final String DB_CONNECTION_ACTION_ID = "ClientID";
  private static final String DB_CONNECTION_ACTION_ALIAS = "ClientAlias";
  private static final String DB_CONNECTION_ACTION_RESULT = "ConnectionDictionary";
  private static final String USER_ID_ACTION = "usr";
  private static final String USER_ID_ACTION_LOGIN = "Login";
  private static final String USER_ID_ACTION_PASS = "Password";
  private static final String USER_ID_ACTION_ALIAS = "Alias";
  private static final String USER_ID_ACTION_RESULT = "UserDictionary";
  
  /**
   * Le URL d'acces au serveur d'authentification.
   */
  private String sautURL;
  
  /**
   * La liste des domaines pour lesquels les connexions HTTP et FTP
   * n'utiliseront pas du serveur proxy.
   */
  private String noProxyHosts;
  
  /**
  * Cree l'objet client. Prend comme parametre le URL de connexion
  * au serveur d'authentification. Typiquement, le URL est celui
  * de DirectAction d'une application WebObject. Cette valeur
  * est disponible dans la table GRHUM.GRHUM_PARAMETRES avec
  * la cle SAUT_URL. Cette valeur devrait etre donnee dans le
  * fichier de configuration de l'application car initialement
  * l'acces a la base de donnees n'est pas initialise.
  */
  public SAUTClient(String newSautURL) {
    sautURL = newSautURL;
    if (!sautURL.endsWith("/")) sautURL += "/";
    this.noProxyHosts = null;
  }
 
  /**
   * Definie les domaines pour lesquels il ne faut pas utiliser le serveur
   * proxy. La chaine peut contenir plusieurs domaines separes par "|".
   * Par exemple, "*.foo.com|localhost".
   * 
   * <p>Les definitions proxy seront ignorees pour les connexions 
   * HTTP et FTP.</p>
   */
  public void setNoProxyHosts(String noProxyHosts) {
    this.noProxyHosts = noProxyHosts;
  }
  
  /**
   * Convertit les infos (le dictionnaire) en objet <code>Properties</code>.
   * Les infos sont constituees des couples: (cle, valeur).
   * Renvoit <i>null</i> si la conversion ne peut pas etre faite.
   */
  public static Properties toProperties(String info) {
    Properties props = new Properties();
    try {
      props.load(new ByteArrayInputStream(info.getBytes()));
      return props;
    } catch(Exception ex) {
      ex.printStackTrace();
      return null;
    }
  }

  /**
   * Communique avec le serveur represente par le URL
   * <code>actionURL</code> et renvoie la reponse du serveur.
   */
  private String resultForAction(String actionURL, String query) {
    try {
      SOAPTransport soapBus = new SOAPTransport(actionURL);
      soapBus.setNoProxyHosts(noProxyHosts);
      if (soapBus.sendMessage(query)) return soapBus.output();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }
  
  /**
   * Extrait la valeur d'un champ donne a partir de document XML.
   * 
   * @param xmlResult Le contenu du document XML.
   * @param fieldId Le nom du champ dont la valeur doit etre extraite.
   * 
   * @see SOAPParser
   */
  private String extractResult(String xmlResult, String fieldId) {
    String result;
    if (xmlResult == null) {
      result = MsgCommError;
    } else {
      Vector args = new Vector();
      args.addElement(fieldId);
      args.addElement(FAULT_CODE_ID);
      args.addElement(FAULT_STRING_ID);
      Hashtable dico = SOAPParser.parseDocumentWithArguments(xmlResult, args);
      if (dico.get(FAULT_CODE_ID) != null) {
        result = "errno="+dico.get(FAULT_CODE_ID)+"\nmsg="+dico.get(FAULT_STRING_ID);
      } else {
        result = (String)dico.get(fieldId);
        if (result == null)
          result = MsgCommError;
        else
          result = StringCtrl.trimText(result);
      }
    }
    return result;
  }

  /**
   * Recoit et decripte le dictionnaire de connexion a la base de donnes
   * correspondant a l'identifiant <code>modId</code>. La reponse est une
   * chaine cryptee. Utilisez la methode <code>requestDecryptedConnectionInfo</code>
   * pour obtenir le dictionnaire decripte.
   * 
   * <p>Le parametre <code>alias</code> indique le context dans lequel le
   * dictionnaire de connexion a la base sont demandees.</p>
   * 
   * @see #requestDecryptedConnectionInfo(String, String)
   */
  public String requestConnectionInfo(String modId, String alias) {
    SOAPQuery rpc = new SOAPQuery();
    rpc.makeMethod(DB_CONNECTION_ACTION, sautURL+USER_ID_ACTION);
    rpc.appendMethodArgument(DB_CONNECTION_ACTION, DB_CONNECTION_ACTION_ID, modId);
    if ((alias != null) && (alias.length() > 0))
      rpc.appendMethodArgument(DB_CONNECTION_ACTION, DB_CONNECTION_ACTION_ALIAS, alias);
    String result = resultForAction(sautURL+DB_CONNECTION_ACTION, rpc.makeXMLQuery());
    return extractResult(result, DB_CONNECTION_ACTION_RESULT);
  }

  /**
   * Recoit et decripte le dictionnaire de connexion a la base de donnes
   * correspondant a l'identifiant <code>modId</code>.
   * 
   * <p>Le dictionnaire est retourne comme une chaine composee des couples
   * <code>cle=valeur</code>, ou chaque couple est inscrite sur une nouvelle
   * ligne. Cette valeur peut ensuite etre convertie en un dictionnaire
   * a l'aide de la methode <code>toProperties</code>.</p>
   * 
   * <p>Le dictionnaire contient au moins deux valeurs definies
   * par les cles <code>errno</code> et <code>msg</code>. errno indique
   * le code de l'erreur et <code>msg</code> contient le message de l'erreur.
   * Dans le cas de succes d'operation, <code>errno</code> est egale 0.</p>
   * 
   * <p>Le parametre supplementaire <code>alias</code> indique le context dans
   * lequel le dictionnaire de connexion a la base sont demandees (le nom du
   * programme, par exemple). Cette valeur peut etre choisie librement.</p>
   * 
   * @see #toProperties(String)
   */
  public String requestDecryptedConnectionInfo(String modId, String alias) {
    String response = requestConnectionInfo(modId, alias);
    if (response.startsWith("errno"))
      return response;
    else
      return CRIpto.decrypt(response);
  }

  /**
   * Recoit les informations sur l'utilisateur avec le login et le mot de passe
   * donnes.
   * 
   * <p>Le dictionnaire est renvoye sous une forme d'une chaine cryptee.
   * Utilisez plutot la methode <code>requestDecryptedUserInfo</code> pour
   * obtenir la reponse decriptee.</p>
   * 
   * <p>Le parametre <code>alias</code> indique le context dans lequel les
   * informations sur l'utilisateur sont demandees.</p>
   * 
   * @see #requestDecryptedUserInfo(String, String, String)
   */
  public String requestUserInfo(String login, String passe, String alias) {
    SOAPQuery rpc = new SOAPQuery();
    rpc.makeMethod(USER_ID_ACTION, sautURL+USER_ID_ACTION);
    rpc.appendMethodArgument(USER_ID_ACTION, USER_ID_ACTION_LOGIN, CRIpto.crypt(login));
    rpc.appendMethodArgument(USER_ID_ACTION, USER_ID_ACTION_PASS, CRIpto.crypt(passe));
    if (alias != null)
      rpc.appendMethodArgument(USER_ID_ACTION, USER_ID_ACTION_ALIAS, CRIpto.crypt(alias));
    String result = resultForAction(sautURL+USER_ID_ACTION, rpc.makeXMLQuery());
    return extractResult(result, USER_ID_ACTION_RESULT);
  }

  /**
   * Recoit et decripte les infos sur un utilisateur ayant le login et le mot
   * de passe donnes.
   * 
   * <p>Les informations sont composees des couples <code>cle=valeur</code>
   * et chaque couple y est inscrite sur une nouvelle ligne. Cette information
   * peut ensuite etre convertie en un dictionnaire a l'aide de la methode
   * <code>toProperties</code>.</p>
   * 
   * <p>Le dictionnaire contient au moins deux valeurs definies
   * par les cles <code>errno</code> et <code>msg</code>. errno indique
   * le code de l'erreur et <code>msg</code> contient le message de l'erreur.
   * Dans le cas de succes d'operation, <code>errno</code> est egale 0.</p>
   * 
   * <p>Le parametre supplementaire <code>alias</code> indique le context dans
   * lequel les inforamtions sur un utilisateur sont demandees (le nom du
   * programme, par exemple). Cette valeur peut etre choisie librement.</p>
   * 
   * @see #toProperties(String)
   */
  public String requestDecryptedUserInfo(String login, String pass, String alias) {
   String response = requestUserInfo(login, pass, alias);
    if (response.startsWith("errno"))
      return response;
    else
      return CRIpto.decrypt(response);
  }
  
}
