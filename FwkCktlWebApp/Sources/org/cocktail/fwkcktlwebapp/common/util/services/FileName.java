package org.cocktail.fwkcktlwebapp.common.util.services;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * Classe de gestion des noms de fichier
 */
public class FileName {
	
	
	private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd_HH-mm-ss";
	
	protected Date getTimeStamp() {
		return new Date();
	}
	
	/**
	   * Genere un nom de fichier à partir du nom
	   * et du format de date passe en parametre a partir de la date du jour
	   * @throws ParseException 
	   * 
	   */
	public String getNameWithCurrentDate(String name) throws ParseException {
		return getNameWithCurrentDate(name, DEFAULT_DATE_FORMAT);   
	}
	
	/**
	   * Genere un nom de fichier à partir du nom
	   * et du format de date passe en parametre a partir de la date du jour
	   * @throws ParseException 
	   * 
	   */
	public String getNameWithCurrentDate(String name, String dateFormat) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String stringDate = sdf.format(getTimeStamp()).trim();
		return getNameWithDate(name, stringDate, dateFormat);   
	}

	
	/**
	   * Genere un nom de fichier à partir du nom, de la date 
	   * et du format de date passe en parametre
	   * @throws ParseException 
	   * 
	   */
	public String getNameWithDate(String name, String date, String dateFormat) throws ParseException {
		if (dateFormat == null) {
			dateFormat = DEFAULT_DATE_FORMAT;
		}
		
		Date pvd = parseValidDate(date, dateFormat);
		
		if (date == null || pvd == null) {
			return getNameWithCurrentDate(name, dateFormat);
		}

		dateFormat = dateFormat.replace("/", "-");
		dateFormat = dateFormat.replace("\\", "-");
		SimpleDateFormat sdf = new SimpleDateFormat(dateFormat);
		String stringDate = sdf.format(pvd).trim();
		return stringDate + "-" + name;   
		
	}
	
	/**
	   * Vérifie la validité d'une date a partir
	   * du format de date passe en parametre
	   * @throws ParseException 
	   * 
	   */
	protected Date parseValidDate(String dateToValidate, String dateFromat) throws ParseException {
 
		SimpleDateFormat sdf = new SimpleDateFormat(dateFromat);
		sdf.setLenient(false);
		return sdf.parse(dateToValidate);
	}

}
