package org.cocktail.fwkcktlwebapp.common.util;


import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;

/**
 * Permet de decrir un document lors de son transfert entre le client et le
 * serveur des fichiers GED. Elle contient les informations sur le document,
 * sur l'operation que l'on souhaite effectuer et indique l'etat de sont
 * execution. Les informations sur le document sont acessibles via les champs
 * publiques de cette classe.
 * 
 * <p>Voir les explications et les exemples donnes pour la classe
 * <code>GEDClient</code>.
 * 
 * @see GEDClient
 */
public class GEDDescription
  implements Serializable
{
//  /**
//   * La taille du tampont utilise pour le transfert de document.
//   */
//  public static final int BUFFER_SIZE = 512;
  
  /**
   * Le code indiquant le succes d'une operation.
   */
  public static final int OK = 1;
  
  /**
   * Le code indiquant l'echeque d'une operation.
   */
  public static final int ERROR = 0;
  
  /**
   * Le code indiquant la presence d'un message d'avertissement.
   */
  public static final int WARNING = 2;

  /**
   * Le code de l'operation de sauvegarde d'un document.
   */
  public static final int OP_SAVE = 0;
  
  /**
   * Le code de l'operation de remplacement d'un document.
   */
  public static final int OP_REPLACE = 1;
  
  /**
   * Le code de l'operation de la suppression d'un document.
   */
  public static final int OP_DELETE = 2;
  

  /**
   * Le code de l'operation de la lecture d'un document.
   */
  public static final int OP_READ = 3;
  
  /**
   * Le code de l'operation de l'inspection d'un document.
   */
  public static final int OP_INSPECT = 4;
  
  /**
   * Le code de l'operation de test de l'existence d'un document.
   */
  public static final int OP_EXISTS = 5;
  
  /**
   * Le code de l'operation de reservation de repertoire.
   */
  public static final int OP_RESERVE = 6;

  /**
   * Le code de l'operation de la suppression du contenue
   * d'un repertoire racine.
   */
  public static final int OP_CLEAN = 7;

  /**
   * Le code du type "fichier".
   */
  public static final String REF_TYPE_FILE = "F";
  
  /**
   * Le code du type "lien URL".
   */
  public static final String REF_TYPE_URL = "U";
  
  /**
   * Le code du type "racine d'arborescence".
   */
  public static final String REF_TYPE_ROOT = "R";
  
  /**
   * Le code du type "document temporaire". Ce type
   * est considere comme etant de la meme classe que
   * <code>REF_TYPE_FILE</code>, i.e., un fichier.
   */
  public static final String REF_TYPE_TEMP = "T";

  /**
   * Le code du type "document supprime".
   */
  public static final String REF_TYPE_DELETED = "D";

  /**
   * Contient la reference vers le document disponible
   * sur le serveur. Typiquement, c'est un lien URL d'acces
   * au document.
   */
  public String reference;
  
  /**
   * Le nom du document. C'est le nom de fichier dans
   * lequel le document est enregistre.
   */
  public String name;
  
  /**
   * Le repertoire du document. Ce repertoire decrit le
   * chemin du fichier vis a vis de la racine de stockage
   * des tous les autres fichier de la meme categorie. La valeur
   * est vide ("") si le fichier n'est pas enregistre dans un
   * sous-repertoire.
   * 
   * <p>Cette valeur est prise en compte uniquement lors de l'enregistrement
   * lors de l'enregistrement d'un document avec un document pere (une
   * arborescence des fichiers).  
   */
  public String directory;
  
  /**
   * Le titre de document. Il permet d'associer une description
   * au document lorsqu'il est enregistre sur le serveur.
   */
  public String title;
  
  /**
   * L'identificateur unique de document. Le serveur associe un
   * identificateur unique a chaque document qu'il enregistre.
   * Ensuite, le document est accede et manipule uniquement via son
   * identificateur.
   * 
   * <p>L'identificateur est toujours renvoie comme le résultat
   * d'une opération de dépot d'un document sur le serveur de fichiers GED.
   * Les applications utilisant ce service doit memoriser cet identificateur
   * afin de pouvour acceder les fichiers ulterieurement. 
   */
  public int documentId;
  
  /**
   * L'identificateur du document pere d'un document en cours. Dans le cas
   * general, cette valeur est egale a -1 (aucun document pere). Elle est
   * utilisee lors de depot d'un ensemble des documents de la meme
   * arborescences (depot d'un repertoire). Les documents ayant le meme
   * document pere partagent tous le meme repertoire de stockage sur le
   * serveur. 
   */
  public int parentDocumentId;
  
  /**
   * La taille d'un document en octets.
   */
  public long size;
  
  /**
   * Le code de la categorie dans laquelle le document est enregistre.
   */
  public String category;
  
  /**
   * Le type du fichier (i.e., son type MIME).
   */
  public String refType;
  
  /**
   * L'identifiant de l'application qui a deposee le document.
   */
  public String applicationId;
  
  /**
   * L'identifiant de la personne qui depose un document ou souhaite
   * effectuer une autre operation. C'est un numero d'individu
   * dans l'annuaire du systeme d'information (noIndividu).
   */
  public int userId;
  
  /**
   * Le code de l'operation que l'on souhaite effectuer avec le document
   * decrit dans cette description. Ce code correspond a une des valeurs
   * definies par les constantes <code>OP_XXX</code>. 
   */
  public int operationId;
  
  /**
   * L'etat de l'operation. Indique le succes (constante <code>OK</code>)
   * ou l'echeque (constante <code>FAIL</code>) de l'operation.
   */
  public int status;
  
  /**
   * Le message supplementaire envoye du serveur vers le client.
   * Dans le cas de l'echeque, il contient le message d'erreur survenue
   * sur le serveur.
   */
  public String message;

  /**
   * L'entete HTTP contenant la description du document. Utilise dans le
   * cas ou le client fonctionne comme un proxy d'accès aux documents stockes
   * sur le serveur.
   * 
   * <p>Cette information n'est pas utilisee dans l'implementation actuelle.
   */
  public String header;
  
  /**
   * Le code numerique d'une erreur. Aucune erreur, si la valeur est 0.
   * 
   * <p>Cette information n'est pas normalisee dans l'implementation actuelle
   * de service GED.
   */
  public int errorCode;
  
  /**
   * Le code du groupe auquel le document est associe.
   *  
   * <p>Cette information n'est pas utilisee dans l'implementation actuelle.
   */
  public int groupId;
  
  /**
   * Description des droits d'acces au document.
   * 
   * <p>Cette information n'est pas utilisee dans l'implementation actuelle.
   */
  public String rights;
  
  /**
   * Le chemin sur le systeme de fichiers.
   * 
   * <p><i>Cette valeur est utilisee pour les besoins internes du serveur et
   * elle ne devrait pas etre manipulee par les clients. La valeur de cet
   * atribut n'est pas transmise entre le serveur et le client.</i>
   */
  public transient String fileReference;

  /**
   * Le URL d'acces au document.
   * 
   * <p><i>Cette valeur est utilisee pour les besoins internes du serveur et
   * elle ne devrait pas etre manipulee par les clients. La valeur de cet
   * atribut n'est pas transmise entre le serveur et le client.</i>
   */
  public transient String urlReference;
  
  /**
   * Construit une nouvelle description.
   */
  public GEDDescription() {
    reference = StringCtrl.emptyString();
    name = StringCtrl.emptyString();
    directory = StringCtrl.emptyString();
    title = StringCtrl.emptyString();
    documentId = -1;
    parentDocumentId = -1;
    size = 0;
    category = StringCtrl.emptyString();
    refType = REF_TYPE_FILE;
    applicationId = StringCtrl.emptyString();
    userId = -1;
    operationId = OP_SAVE;
    status = OK;
    message = StringCtrl.emptyString();
    header = StringCtrl.emptyString();
    errorCode = 0;
    groupId = -1;
    rights = StringCtrl.emptyString();
    fileReference = null;
    urlReference = null;
  }

//  /**
//   * Lit une chaine de caractere a partir d'un stream. Le stream commence
//   * par la taille de la chaine (un entier) suivi des caracteres de la
//   * chaine.
//   * 
//   * @param dataIn Le DataInputStream a partir de lequel la chaine est lue. 
//   * @exception IOException 
//   */
//  private String readStringFromStream(DataInputStream dataIn)
//    throws IOException
//  {
//    byte[] bytes;
//    int i = dataIn.readInt();
//    if (i > 0) {
//      bytes = new byte[i];
//      dataIn.read(bytes);
//      return new String(bytes);
//    } else {
//      return "";
//    }
//  }
//
//  /**
//   * Ecrit une chaine de caracteres dans un stream d'octets. On ecrit
//   * la longeur de la chaine (un entier) suivi des ses caracteres.s
//   *   
//   * @param aString La chaine a enregistrer dans un stream. 
//   * @param dataOut Le stream.
//   * @exception IOException 
//   */
//  private void writeStringToStream(String aString, DataOutputStream dataOut)
//    throws IOException
//  {
//    if ((aString == null) || (aString.length() == 0)) {
//      dataOut.writeInt(0);
//    } else {
//      byte[] bytes = aString.getBytes();
//      dataOut.writeInt(bytes.length);
//      dataOut.write(bytes, 0, bytes.length);
//    }
//  }
//
  /**
   * Lit le contenu de la description a partir d'un
   * stream.
   * 
   * @param in Le stream.
   * @return boolean Indique le succes (<code>true</code>) ou
   *   l'echeque (<code>false</code>) de la lecture. 
   */
  public boolean readFromStream(InputStream in) {
    try {
      DataInputStream dataIn = new DataInputStream(in);
      documentId = dataIn.readInt();
      parentDocumentId = dataIn.readInt();
      size = dataIn.readLong();
      reference = StreamCtrl.readStringFromStream(dataIn);
      name = StreamCtrl.readStringFromStream(dataIn);
      directory = StreamCtrl.readStringFromStream(dataIn);
      title = StreamCtrl.readStringFromStream(dataIn);
      category = StreamCtrl.readStringFromStream(dataIn);
      refType = StreamCtrl.readStringFromStream(dataIn);
      applicationId = StreamCtrl.readStringFromStream(dataIn);
      userId = dataIn.readInt();
      operationId = dataIn.readInt();
      status = dataIn.readInt();
      message = StreamCtrl.readStringFromStream(dataIn);
      header = StreamCtrl.readStringFromStream(dataIn);
      errorCode = dataIn.readInt();
      groupId = dataIn.readInt();
      rights = StreamCtrl.readStringFromStream(dataIn);
      // rajout richard le 16/10/2007
      //fileReference = StreamCtrl.readStringFromStream(dataIn);
      //urlReference = StreamCtrl.readStringFromStream(dataIn);
      return true;
    } catch(Exception ex) {
      ex.printStackTrace();
      return false;
    }
  }

  /**
   * Ecrit le contenu de la description dans un stream.
   * 
   * @param out La stream.
   * @return boolean Indique le succes (<code>true</code>) ou
   *   l'echeque (<code>false</code>) de la lecture. 
   */
  public boolean writeToStream(OutputStream out) {
    try {
      DataOutputStream dataOut = new DataOutputStream(out);
      dataOut.writeInt(documentId);
      dataOut.writeInt(parentDocumentId);
      dataOut.writeLong(size);
      StreamCtrl.writeStringToStream(reference, dataOut);
      StreamCtrl.writeStringToStream(name, dataOut);
      StreamCtrl.writeStringToStream(directory, dataOut);
      StreamCtrl.writeStringToStream(title, dataOut);
      StreamCtrl.writeStringToStream(category, dataOut);
      StreamCtrl.writeStringToStream(refType, dataOut);
      StreamCtrl.writeStringToStream(applicationId, dataOut);
      dataOut.writeInt(userId);
      dataOut.writeInt(operationId);
      dataOut.writeInt(status);
      StreamCtrl.writeStringToStream(message, dataOut);
      StreamCtrl.writeStringToStream(header, dataOut);
      dataOut.writeInt(errorCode);
      dataOut.writeInt(groupId);
      StreamCtrl.writeStringToStream(rights, dataOut);
      // rajouts richard le 16/10/2007
      //StreamCtrl.writeStringToStream(fileReference, dataOut);
      //StreamCtrl.writeStringToStream(urlReference, dataOut);
      dataOut.flush();
      return true;
    } catch(Exception ex) {
      ex.printStackTrace();
      return false;
    }
  }

  /**
   * Test si la description peut etre accompagnee de donnees.
   * La description a les donnees associees si elle represente une
   * operation d'enregistrement ou de remplacement de donnees et
   * elles sont du type "fichier". Lors de l'envoie de cette
   * description, elle doit etre suivie des donnees - le contenu
   * du document concerne.
   *  
   * @return <code>true</code> si la description doit etre suivie de
   * donnees.
   */
  public boolean hasData() {
    return (isFile() && ((operationId == OP_SAVE) || (operationId == OP_REPLACE)));
  }

  /**
   * Teste si la reference donnee represente le fichier.
   * 
   * @param referenceType Le type de la reference.
   * @return <code>true</code> si le type correspond a <code>REF_TYPE_FILE</code>
   *   ou <code>REF_TYPE_TEMP</code>. 
   */
  public static boolean isFile(String referenceType) {
    return (referenceType.equals(REF_TYPE_FILE) || referenceType.equals(REF_TYPE_TEMP));
  }

  /**
   * Teste si la description est une description d'un fichier.
   * 
   * @return <code>true</code> si le type de la description est
   *     <code>REF_TYPE_FILE</code> ou <code>REF_TYPE_TEMP</code>.
   */
  public boolean isFile() {
    return isFile(refType);
  }
  

  /**
   * Teste si la reference donnee represente la racine d'une arborescence.
   * 
   * @param referenceType Le type de la reference.
   * @return <code>true</code> si le type correspond a <code>REF_TYPE_ROOT</code>. 
   */
  public static boolean isRoot(String referenceType) {
    return (referenceType.equals(REF_TYPE_ROOT));
  }

  /**
   * Teste si la description est une description d'une recine d'arborescence.
   * 
   * @return <code>true</code> si le type de la description est
   *   <code>REF_TYPE_ROOT</code>.
   */
  public boolean isRoot() {
    return isRoot(refType);
  }

  /**
   * Teste si l'operation decrite par la description correspond a l'envoie
   * de donnees du client vers le serveur.
   * 
   * @return <code>true</code> si le type de l'operation est un des
   *     <code>OP_SAVE</code>, <code>OP_REPLACE</code> ou
   *     <code>OP_DELETE</code>. 
   */
  public boolean isPutOperation() {
    return ((operationId == OP_SAVE) ||
            (operationId == OP_REPLACE) ||
            (operationId == OP_DELETE) ||
            (operationId == OP_RESERVE) ||
            (operationId == OP_CLEAN));
  }

  /**
   * Retourne une chaine de caracteres avec le contenu de la description.
   */
  public String toString() {
    StringBuffer sb = new StringBuffer("GEDConnectionDescription(");
    sb.append("ref=").append(reference).append(", ");
    sb.append("titre=").append(title).append(", ");
    sb.append("docId=").append(documentId).append(", ");
    sb.append("parentDocId=").append(parentDocumentId).append(", ");
    sb.append("size=").append(size).append(", ");
    sb.append("category=").append(category).append(", ");
    sb.append("appId=").append(applicationId).append(", ");
    sb.append("userId=").append(userId).append(", ");
    sb.append("opId=").append(operationId).append(", ");
    sb.append("status=").append(status).append(", ");
    sb.append("message=").append(message).append(", ");
    sb.append("header=").append(header);//.append(", ");
		// rajout richard le 16/10/2007
    //sb.append("fileReference=").append(fileReference).append(", ");
    //sb.append("urlReference=").append(urlReference);
    sb.append(")");
    return sb.toString();
  }

  /**
   * Renvoie le resume d'information sur un document.
   * 
   * @return Cette implementation renvoie toujours une chaine vide.
   */
  public String documentDescription() {
    // Ajouter le code pour la description d'un document.
    return "";
  }
  
  /**
   * Renvoie le nom du document represente par le fichier ou la reference.
   * Permet d'acceder de maniere transparente au nom du document reference
   * par cette description.
   * 
   * @return String La reference contenant le nom du document.
   */
  public String getReferenceName() {
    if (isFile())
      return name;
    else
      return reference;
  }
}
