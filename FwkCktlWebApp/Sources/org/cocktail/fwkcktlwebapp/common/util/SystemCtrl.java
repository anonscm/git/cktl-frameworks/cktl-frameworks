package org.cocktail.fwkcktlwebapp.common.util;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.util._imports.BrowserBasic;


/**
 * Permet d'acceder aux informations relatives au systeme d'execution de
 * l'application Java, telles que les variables d'environement.
 * 
 * <p><i>Toutes les methodes de cette classe sont statiques.</i></p> 
 */
public class SystemCtrl {
  
  /**
   * Identifiant d'un "autre" system d'information. Typiquement, c'est un
   * systeme de type "Unix".
   */
  public static final int SYSTEM_OTHER = 0;
  
  /**
   * Identifiant de systeme d'exploitation de type Windows 9x (95, 98, ME).
   */
  public static final int SYSTEM_WIN_9X = 1;
  
  /**
   * Identifiant de systeme d'exploitation de type Windows NT (NT, 2000, XP).
   */
  public static final int SYSTEM_WIN_NT = 2;
  
  /**
   * Identifiant de systeme d'exploitation de type Mac OS X.
   */
  public static final int SYSTEM_MAC_OS = 3;
  
  /**
   * Les definitions des variables d'environement.
   */
  private static Hashtable sysEnvDico;
  
  /**
   * L'identifiant de systeme d'exploitation en cours.
   */
  private static int systemId = -1;
  
  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>StringCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public SystemCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }
  
  /**
   * Retourne l'identifiant de systeme d'exploitation en cours. La valeur
   * retournee est egale a une des constantes <code>SYSTEM_XXX</code>.
   */
  public static int systemId() {
    // Identification n'est pas encore faite.
    if (systemId == -1) {
      if (osName().startsWith("Windows 9") || osName().startsWith("Windows ME"))
        systemId = SYSTEM_WIN_9X; 
      else if (osName().startsWith("Windows"))
        systemId = SYSTEM_WIN_NT;
      else if (osName().startsWith("Mac OS"))
        systemId = SYSTEM_MAC_OS;
      else
        systemId = SYSTEM_OTHER;
    }
    return systemId;
  }
  
  /**
   * Retourne le dictionnaires avec proprietes Java. Cette appel est equivalent
   * a : <code>System.getProperties()</code>.
   */
  public static Hashtable javaEnv() {
    return System.getProperties();
  }
  
  /**
   * Retourne le dictionnaire contenant les definitions des variables
   * d'environement. Attention, ces definitions dependent de l'utilisateur
   * ayant actuellement ouvert une session de travail dans un systeme
   * d'environement.
   * 
   * <p>L'appel a cette methode retourne <i>null</i> si le systeme
   * d'exploitation ne possede pas les variables d'environement ou s'il
   * n'est pas reconnu par cette implementation de la methode.</p>
   *  
   */
  public static Hashtable systemEnv() {
    if (sysEnvDico == null) {
      sysEnvDico = new Hashtable();
      try {
        sysEnvDico = stringToDico(execCommand(getSystemEnvCommand()));
      } catch(Exception ex) { }
    }
    return sysEnvDico;
  }

  /**
   * Returne une expression qui doit etre executee dans le systeme
   * d'exploitation en cours afin d'execute la commande <code>command</code>.
   * Par exemple, dans le systeme d'exploitation de type Windows, une
   * commande doit toujours etre executee par un interpretateur de commandes.
   */
  private static String commandForCommand(String command) {
    if (systemId() == SYSTEM_WIN_9X)
      return "command /C "+command;
    else if (systemId() == SYSTEM_WIN_NT)
      return "cmd /Y /C "+command;
//      return "start /B /WAIT cmd /C "+command;
    else
      return command;
  }
  
  /**
   *  
   */
  private static String getContentFromStream(InputStream stream)
    throws IOException
  {
    String line;
    StringBuffer result = new StringBuffer();
    BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
    while((line = reader.readLine()) != null)
      result.append(line).append("\n");
    line = result.toString();
    if ((line.length() <= 1) && line.endsWith("\n"))
      return StringCtrl.emptyString();
    else
      return line;
  }

  /**
   * Execute la commande <code>command</code> dans le systeme d'exploitation
   * en cours. Returne dans une chaine de caracteres le contenu de la sortie
   * standard produit par cette commande.
   */
  public static String execCommand(String command)
    throws IOException
  {
    String line;
//    System.out.println("commandForCommand : "+commandForCommand(command));
    Process proc = Runtime.getRuntime().exec(commandForCommand(command));
    line = getContentFromStream(proc.getInputStream());
    if (line.length() == 0)
      line = getContentFromStream(proc.getErrorStream());
    return line;
  }
  
  /**
   * Retourne la valeur de la variable d'environement <code>variable</code>.
   * Retourne <i>null</i> si la variable n'est pas definie ou si la definition
   * des variables d'environement ne peux pas etre determinee.
   */
  public static String systemVariable(String variable) {
    return (String)systemEnv().get(variable);
  }
  
  /**
   * Convertie les definitions des variables passees dans le flux <code>in</code>
   * en un dictionnaire. On suppose que le flux <code>in</code> contient
   * les definitions ayant la forme "<code>variable=valeur</code>" et chaque
   * definition est donnees sur une nouvelle ligne.
   */
  private static Hashtable stringToDico(String lines) {
    Hashtable dico = new Hashtable();
    try {
      BufferedReader reader = new BufferedReader(new StringReader(lines));
      String line, key, value;
      int i;
      while((line = reader.readLine()) != null) {
        i = line.indexOf("=");
        if (i>0) {
          key = line.substring(0, i);
          value = line.substring(i+1);
          dico.put(key, value);
        }
      }
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    return dico;
  }
  
  /**
   * Retourne la commande qui doit etre execute dans le systeme d'exploitation
   * en cours pour obtenir la definition des variables d'environement. C'est
   * la commande "set" pour les systemes de type Windows, et "env" pour
   * d'autres systemes.
   */
  private static String getSystemEnvCommand() {
    if ((systemId() == SYSTEM_WIN_9X) || (systemId() == SYSTEM_WIN_NT))
      return "set";
    else
      return "env";
  }
  
  /**
   * Retourne le nom de systeme d'exploitation en cours. Cette methode est
   * le raccourci de l'appel : <code>System.getProperty("os.name")</code>
   */
  public static String osName() {
    return System.getProperty("os.name");
  }
  
  /**
   * Retourne la version de systeme d'exploitation en cours. Cette methode est
   * le raccourci de l'appel : <code>System.getProperty("os.version")</code>
   */
  public static String osVersion() {
    return System.getProperty("os.version");
  }
  
  /**
   * Retourne le nom de l'utilisateur actuallement ayant ouvert une session
   * de travail avec un systeme d'exploitation. Cette methode est le raccourci
   * de l'appel : <code>System.getProperty("user.name")</code>
   */
  public static String userName() {
    return System.getProperty("user.name"); 
  }
  
  /**
   * Retourne le repertoire des fichiers temporaires. Retourne <i>null</i>
   * si ce repertoire ne peux pas etre determine.
   * 
   * <p>Le chemin retourne est en format de systeme d'exploitation, i.e.,
   * le separateurs des noms des repertoires et des fichiers sont ceux
   * utilises dans le systeme. Sous les systemes de type Unix c'est le
   * symbole "/" et sous les systemes Windows ceci est "\".</p>
   * 
   * @see #tempDir()
   */
   public static String systemTempDir() {
    // On teste d'abord le repertoire defini dans la variable d'environement 
    String tmp = System.getProperty("java.io.tmpdir");
    if (tmp == null) tmp = systemVariable("TMP");
    if (tmp == null) tmp = systemVariable("TEMP");
    if (tmp != null) return tmp;
    // Sinon, on essaie de trouver le repertoire dans l'emplacement
    // "classique"
    // Win 9x : C:\Temp ou C:\Windows\Temp
    if ((systemId() == SYSTEM_WIN_9X) || (systemId() == SYSTEM_WIN_NT)) {
      // Le repertoire dans la racine
      if (testTempDir("C:\\Temp")) return "C:\\Temp";
      // Sinon, dans le repertoire de systeme
      tmp = systemVariable("windir");
      if (tmp == null) tmp = systemVariable("SystemRoot");
      if (tmp == null) tmp = "C:\\Windows\\";
      tmp = normalizePath(tmp)+"Temp";
      if (testTempDir(tmp)) return tmp;
      // Sous les OS NT-like, le repertoire systeme peut etre different
      tmp = "C:\\WINNT\\Temp";
      if (testTempDir(tmp)) return tmp;
    } else {
      // Sinon, on suppose que c'est un systeme de type UNIX
      tmp = "/tmp";
      if (testTempDir(tmp)) return tmp;
      tmp = "/var/tmp";
      if (testTempDir(tmp)) return tmp;
    }
    // Sinon, on n'a rien trouve
    return null;
  }

  /**
   * Retourne le repertoire des fichiers temporaires. Retourne <i>null</i>
   * si ce repertoire ne peux pas etre determine.
   * 
   * <p>Le chemin retourne utilise toujours le symbole "/" comme le separateur
   * des noms des repertoires et des fichiers.</p>
   * 
   * @see #systemTempDir()
   */  
  public static String tempDir() {
    return StringCtrl.replace(systemTempDir(), "\\", "/");
  }
  
  /**
   * Convertie une expression de chemin en son format normalise. Si
   * le chemin designe un repertoire, alors il doit se terminer par
   * le symbole "/" ou "\" (dependamment de OS).
   */
  private static String normalizePath(String path) {
    File f = new File(path);
    if (f.isDirectory()) {
      if (!path.endsWith(File.separator)) path += File.separator;
    }
    return path;
  }
  
  /**
   * Test si le repertoire donnee peut etre considere comme le repertoire
   * temporaire. Il doit exister et l'utilisateur doit avoir les droits
   * d'ecriture dans ce repertoire.
   */
  private static boolean testTempDir(String path) {
    File f = new File(path);
    if (f.exists()) {
      path = normalizePath(f.getAbsolutePath()) + "tmp_SystemCtrl_testTempDir";
      try {
        f = new File(path);
        if (f.exists()) f.delete();
        BufferedWriter w = new BufferedWriter(new FileWriter(path));
        w.write("This is a test file.");
        w.close();
        f.delete();
        return true;
      } catch(Throwable ex) { }
    }
    return false;
  }
  
  /**
   * Ouvre le document dont le chemin est <code>filePath</code>. Si le chemin
   * correspond a un URL, alors le document sera ouvert avec le navigateur
   * Web disponible par defaut dans le system. Si <code>filePath</code>
   * correspond au chemin dans le systeme de fichier, alors le document sera
   * ouvert avec le programme associe, si celui-ci peut etre detecte.
   *    
   * <p>Retourne <i>null</i> si le document est ouvert avec succes ou le
   * message d'erreur si l'ouverture echoue.</p>
   * 
   * @see #openFileInSystem(String)
   * @see #openFileInBrowser(String)
   */
  public static String openFile(String filePath) {
    if (filePath.indexOf(":/") > 1) // C'est une URL
      return openFileInBrowser(filePath);
    else // sinon, c'est le nom d'un fichier
      return openFileInSystem(filePath);
  }
  
  /**
   * Ouvre le document dont le chemin est <code>path</code> avec le programme
   * qui lui est associe dans le systeme d'exploitation. Le parametre
   * <code>path</code> doit indique le chemin d'acces absolu.
   * 
   * <p>Retourne <i>null</i> si le document est ouvert avec succes ou le
   * message d'erreur si l'ouverture echoue.</p>
   * 
   * <p>Cette methode peut fonctionner uniquement dans certains systemes
   * d'exploitation. Dans le cas, si le programme associe ne peux pas
   * etre detecte dans le systeme en cours, cette methode essaie de l'ouvrir
   * avec le navigateur Web par defaut.</p>
   * 
   * <p>Il est fortemment conseille d'utiliser le symbole "/" comme separateur
   * des noms des fichiers dans le chemin.</p>
   * 
   * @see #openFile(String)
   */
  public static String openFileInSystem(String path) {
    String result = null;
    try {
      File f = new File(path);
      if (!f.exists()) {
        result = "Fichier n'existe pas :\n"+path;
      } else {
        path = f.getAbsolutePath();
        if ((systemId() == SYSTEM_WIN_9X) || (systemId() == SYSTEM_WIN_NT)) {
//          // Methode No. 1
//          path = "file:///"+StringCtrl.replace(path, "\\", "/");
//          // Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler "+path);
//          Runtime.getRuntime().exec("rundll32 url.dll "+path);
          // Methode No. 2
          execCommand("start "+path);
        } else if (systemId() == SYSTEM_MAC_OS) {
          execCommand("open "+path);
        } else {
          // Sinon, on essaie l'ouverture dans un navigateur
          path = StringCtrl.replace(path, "\\", "/");
          if (!path.startsWith("/")) path = "/"+path;
          path = "file://"+path;
          result = openFileInBrowser(path);
//          result = "Fichier ne peut pas etre ouvert dans ce systeme d'exploitation :\n"+path;
        }
      }
    } catch(Throwable ex) {
      result = getExceptionMessage(ex);
    }
    return result;
  }
  
  /**
   * Ouvre le URL donne dans le navigateur par defaut de systeme
   * d'exploitation.
   * 
   * <p>Retourne <i>null</i> si le document est ouvert avec succes ou le
   * message d'erreur si l'ouverture echoue.</p>
   * 
   * @see #openFile(String)
   */
  public static String openFileInBrowser(String url) {
    try {
      BrowserBasic.displayURL(url);
    } catch(Exception ex) {
      return getExceptionMessage(ex);
    }
    return null;
  }
  
  /**
   * Retourne le message d'erreur de l'exception <code>ex</code>.
   * Le message est precede de nom de la classe de l'exception.
   */
  private static String getExceptionMessage(Throwable ex) {
    String message = ex.getClass().getName();
    if (ex.getMessage() != null) message += " : "+ex.getMessage();
    return message;
  }
}
