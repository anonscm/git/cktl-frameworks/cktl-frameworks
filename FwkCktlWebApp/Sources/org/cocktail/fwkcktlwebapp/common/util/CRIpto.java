package org.cocktail.fwkcktlwebapp.common.util;


import java.math.BigInteger;

import org.cocktail.fwkcktlwebapp.common.util._imports.TEA;


/** 
 * Crypte et decrypte les chaines de caracteres. Elle sert d'intermediaire
 * entre un algorithme concret de cryptage et les applications. Implementation
 * actuelle de la classe utilise le <i>Tiny Encryption Algorithm</i>.
 * 
 * <p>Cette classe est constituee des methodes statiques uniquement.
 */
public class CRIpto {
  /**
   * La chaine indiquant la fin de la chaine originale. Elle est utilisee
   * pour elliminer les symboles supplementaires ajoutes par l'algorinthme
   * de cryptage.
   */
  private static String EndMark = "\n#CRIptoEndMark#";
   
  /**
   * La cle du cryptage.
   */
  private static String KeyString = "8442BBC6A0D970D1362EEDBD149BA3F";

  /**
   * La representation binaire de la cle du cryptage.
   */
  private static final byte key[] = new BigInteger(KeyString, 16).toByteArray();

  /**
   * L'implementation de l'algorithme de cryptage.
   */
  private static TEA teaCoder = new TEA(key);

  /**
   * C'est un constructeur prive afin d'empecher la creation
   * d'une instance de cette classe. La creation d'une instance n'est
   * pas necessaire, car cette classe ne possede que les methodes statiques. 
   */
  private CRIpto() {
  }

  /**
   * Crypte la chaine de caracteres en utilisant la cle par defaut.
   * 
   * @param chaine La chaine a crypter. 
  */
  public static String crypt(String chaine) {
    if ((chaine == null) || (chaine.length() == 0)) return null;
    chaine += EndMark;
    chaine = teaCoder.padPlaintext(chaine);
    byte[] plainSource = chaine.getBytes();
    int[] enc = teaCoder.encode(plainSource, plainSource.length);
    return teaCoder.binToHex(enc);
  }

  /**
   * Decryte la chaine de caracteres donnee en utilisant la cle par defaut.
   * 
   * @param chaine La chaine a decrypter. 
   */
  public static String decrypt(String chaine) {
    if ((chaine == null) || (chaine.length() == 0)) return null;
    int[] enc = teaCoder.hexToBin(chaine);
    byte[] dec = teaCoder.decode(enc);
    chaine = new String(dec);
    if (chaine.lastIndexOf(EndMark) >= 0)
      return chaine.substring(0, chaine.lastIndexOf(EndMark));
    else
      return chaine;
  }

  /**
   * Retourne la cle pour le cryptage d'un mot de passe. La cle
   * est compose de deux caracteres choisis aletoirement.
   */
  private static String getNewSalt(int charNumber) {
    StringBuffer salt = new StringBuffer();
    int iA = 'A', iZ = 'Z';
    int ia = 'a'; // iz = 'z';
    int l = iZ-iA;
    int p;
    char c;
    // On genere autant de caracteres qu'il est demande
    for(int i=1; i<=charNumber; i++) {
      // On evite de generer deux memes caracteres de suite
      do {
        p = (int)(l*2*Math.random());
        if (p < l) c = (char)(iA+p);
          else c = (char)(ia+p-l);
      } while ((salt.length() != 0) &&
               (salt.charAt(salt.length()-1) == c));
      salt.append(c);
    }
    return salt.toString();
  }
}
