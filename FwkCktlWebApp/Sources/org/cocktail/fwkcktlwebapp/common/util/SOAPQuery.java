package org.cocktail.fwkcktlwebapp.common.util;


import java.io.StringWriter;
import java.util.Enumeration;
import java.util.Hashtable;

import org.apache.xerces.dom.DocumentImpl;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Permet de creer un document XML correspondant a une requette SOAP.
 * 
 * <p>Un exemple de la creation d'une requette SOAP&nbsp;:</p>
 * 
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * <code class="comment">// On cree et on parametrise la requette</code>
 * SOAPQuery soap = new SOAPQuery();
 * soap.makeMethod("maMethode", "http://www.univ-lr.fr/apps/soap/maMethode");
 * soap.appendMethodArgument("maMethode", "argument1", "valeur1");
 * soap.appendMethodArgument("maMethode", "argument2", "valeur2");
 * <code class="comment">// On genere sa representation textuelle</code>
 * String soapQuery = rpc.makeXMLQuery</code></pre></td></tr></table>
 * 
 * <p>Une fois la requette creee, elle peut etre executee a l'aide d'une
 * instance de la classe <code>SOAPTransport</code>.</p>
 * 
 * @see SOAPTransport
 */
public class SOAPQuery {
  /**
   * L'objet representant une enveloppe SOAP (l'entete).
   */
  private Element envelope;
  
  /**
   * L'objet representant le corps SOAP.
   */
  private Element body;
  
  /**
   * La liste des methodes qui doivent etre executees suite a la requette SOAP.
   */
  private Hashtable  methods;
  
  /**
   * La representation du document XML entier.
   */
  private Document document;

  /**
   * Creer une nouvelle requette SOAP.
   */
  public SOAPQuery() {
    document = new DocumentImpl();
    makeEnvelope();
    makeBody();
    methods = new Hashtable();
  }
  
  /**
   * Cree la definition de l'enveloppe de la requette SOAP.
   */
  private void makeEnvelope() {
    envelope = document.createElement("SOAP-ENV:Envelope");
    envelope.setAttribute("xmlns:SOAP-ENV",
                          "http://schemas.xmlsoap.org/soap/envelope/");
    envelope.setAttribute("SOAP-ENV:encodingStyle",
                          "http://schemas.xmlsoap.org/soap/encoding/");
  }
  
  /**
   * Cree la definition du corps de la requette SOAP.
   */
  private void makeBody() {
    body = document.createElement("SOAP-ENV:Body");
  }
  
  /**
   * Enregistre la methode <code>methodName</code> pour la requette SOAP.
   * Le parametre <code>uri</code> l'espace de noms pour la methode.
   * 
   * <p>La methode retourne un element qui correspond a la methode. Le
   * resultat est <i>null</i> si la methode demandee est deja enregistree dans
   * la requette.</p>
   * 
   * @see #appendMethodArgument(String, String, String)
   */
  public Element makeMethod(String methodName, String uri) {
    Object aMethod = methods.get(methodName);
    if (aMethod != null) {
      System.out.println("The method with name "+methodName+" is already created in this document.");
      return null;
    }
    Element method = document.createElement("m:"+methodName);
    method.setAttribute("xmlns:m", uri);
    methods.put(methodName, method);
    return method;
  }
  
  /**
   * Ajoute l'attribut <code>argName</code> avec sa valeur <code>argValue</code>
   * dans la liste des arguments de la methode <code>methodName</code>. La
   * methode <code>methodName</code> doit etre precedemment enregistree
   * dans la requette.
   * 
   * @see #makeMethod(String, String)
   */
  public void appendMethodArgument(String methodName, String argName, String argValue) {
    Element method = (Element)methods.get(methodName);
    if (method == null) {
      System.out.println("The method with the name "+methodName+" is not found in this document");
      return;
    }
    Element argument = document.createElement(argName);
    argument.appendChild(document.createTextNode(argValue));
    method.appendChild(argument);
  }
  
  /**
   * Genere la representation de la requette SOAP. Les methodes et les
   * arguments doivent precedement etre eregistres a l'aide des methodes
   * <code>makeMethod</code> et <code>appendMethodArgument</code>.
   * 
   * @see #makeMethod(String, String)
   * @see #appendMethodArgument(String, String, String)
   */
  public String makeXMLQuery() {
    // On relit tous les elements du document
    document.appendChild(envelope);
    envelope.appendChild(body);
    for(Enumeration e=methods.keys(); e.hasMoreElements();) {
      body.appendChild((Element)methods.get(e.nextElement()));
    }
    // On genere une representation textuelle du document
    try {
      StringWriter stringOut = new StringWriter();
      XMLSerializer serial = new XMLSerializer(stringOut, new OutputFormat(document));
      serial.asDOMSerializer(); 
      serial.serialize(document.getDocumentElement());
      // System.out.println( "\n\n\nXML SOAP Document:\n\n" + stringOut.toString() + "\n");
      return stringOut.toString();
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    return null;
  }

//  /**
//   * Test le fonctionnement de la classe.
//   */
//  public static void main(String[] args) {
//    SOAPQuery rpc = new SOAPQuery();
//    rpc.makeMethod("AMethod", "http://www.univ-lr.fr/apps/soap/bd");
//    rpc.appendMethodArgument("AMethod", "Argument", "aValue");
//    System.out.println("XML SOAP Query :\n"+rpc.makeXMLQuery()+"\n");
//  }
}
