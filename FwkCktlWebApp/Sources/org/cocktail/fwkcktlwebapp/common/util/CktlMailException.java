package org.cocktail.fwkcktlwebapp.common.util;


/**
 * Represente les erreures survenues lors de l'envoie ou l'analyse des
 * messages mails. Elle est levee lors de l'utilisation de la classe
 * {@link CktlMailMessage}
 * 
 * @see CktlMailMessage
 */
public class CktlMailException extends Exception {
  
  /**
   * Cree une nouvelle exception avec le message indique.
   *
   * @param message Le message pour la nouvelle exception.
   */
  public CktlMailException(String message) {
    super(message);
  }

  /**
   * Creer une nouvelle exception a partir de l'exception
   * ou l'erreur donnee. La nouvelle exception reprend le
   * le message de l'exception donnee. Si son message est
   * vide, le nom de la classe de l'exception est utilise
   * comme le nouveau message.
   *
   * @param ex Une exception ou une erreure.
   */
  public static CktlMailException createException(Throwable ex) {
    if ((ex.getMessage() != null) && (ex.getMessage().length() > 0))
      return new CktlMailException(ex.getMessage());
    else
      return new CktlMailException(ex.getClass().getName());
  }
}
