package org.cocktail.fwkcktlwebapp.common.util._imports;


import java.util.Hashtable;

/**
 * A node of an XML DOM tree;
 *
 * @author    Michael Claéen
 * @version   $Revision: 1.2 $
 */
public class XNode {
// The original name of this class was Node. It has been changed to avoid
// confusion with other classes of this or other packages.

  public String type;
  public String name;
  public String value;
  public Hashtable attributes;
  public int uid;
  public XJSArray contents;
  public XJSArray index;

  // node types
  public static final String Element = "element";
  public static final String CharData = "chardata";
  public static final String PI = "pi";
  public static final String Comment = "comment";

/////////////////////////
//// the object constructors for the hybrid DOM

  /**
   * factory method for element nodes
   *
   * @return    a Node of type element
   */
  static XNode createElement() {
    XNode n = new XNode();
  	n.type = Element;
	  n.name = new String();
  	n.attributes = new Hashtable();
	  n.contents = new XJSArray();
  	n.uid = Xparse.count++;
	  Xparse.index.setElementAt(n, n.uid);
    return n;
  }

  /**
   * factory method for the root element
   *
   * @return    a rootelement Node
   */
  static XNode createRootelement() {
    return createElement();
  }

  /**
   * factory method for chardata nodes
   *
   * @return    a chardata Node
   */
  static XNode createChardata() {
    XNode n = new XNode();
  	n.type = CharData;
	  n.value = new String();
    return n;
  }

  /**
   * factory method for PI nodes
   *
   * @return    a PI Node
   */
  static XNode createPi() {
    XNode n = new XNode();
	  n.type = PI;
  	n.value = new String();
    return n;
  }

  /**
   * factory method for comment nodes
   *
   * @return    a comment Node
   */
  static XNode createComment()
  {
    XNode n = new XNode();
	  n.type = Comment;
  	n.value = new String();
    return n;
  }

  /**
   * returns the character data in the first child element;
   * returns nonsense if the first child element ist not chardata
   *
   * @return    the characters following an element
   */
  public String getCharacters() {
    return ((XNode)contents.elementAt(0)).value;
  }

  /**
   * find the node matching a certain occurrence of the path description
   *
   * @param     path an XPath style expression without leading slash
   * @param     occur array indicating the n'th occurrence of a node matching each simple path expression
   *
   * @return    the n'th Node matching the path description
   */
  public XNode find(String path, int[] occur) {
    XNode n = this;
    XJSArray a = new XJSArray();
    a.split(path, "/");
    int i = 0;
    while (i < a.length()) {
      n = findChildElement(n, (String)a.elementAt(i), occur[i]);
      if (n == null) return null;
      i++;
    }
    return n;
  }

  /**
   * 
   */
  public XNode find(String path, String occur) {
    XJSArray a = new XJSArray();
    a.split(occur, ",");
    int[] occurs = new int[a.length()];
    for (int i=0; i<a.length(); i++)
      occurs[i] = Integer.parseInt(((String)a.elementAt(i)).trim());
    return find(path, occurs);
  }
  
  /**
   * 
   */
  public XNode findFirst(String path) {
    XNode n = this;
    XJSArray a = new XJSArray();
    a.split(path, "/");
    int i = 0;
    while (i < a.length()) {
      n = findChildElement(n, (String)a.elementAt(i), 1);
      if (n == null) return null;
      i++;
    }
    return n;
  }
  
  /**
   * find the child node matching a certain occurrence of a simple path description
   *
   * @param     parent the parent node to start from
   * @param     simplePath one element of an XPath style expression
   * @param     occur the n'th occurance of a node matching the path expression
   *
   * @return    the n'th child Node matching the simple path description
   */
  XNode findChildElement(XNode parent, String simplePath, int occur) {
    XJSArray a = parent.contents;
    XNode n;
    int  found = 0;
    int i = 0;
    String tag;
    do {
      n = (XNode)a.elementAt(i);
      ++i;
      tag = (n.name != null) ? n.name : "";
      int colonPos = tag.indexOf(':');
      tag = (colonPos == -1) ? tag : tag.substring(colonPos + 1);
      if (simplePath.equals(tag)) ++found;
    } while (i < a.length() && found < occur);
    return (found == occur) ? n : null;
  }

}

