package org.cocktail.fwkcktlwebapp.common.util._imports;


import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.lang.reflect.Method;
import java.net.URL;
import java.text.MessageFormat;
import java.util.StringTokenizer;
import java.util.Vector;

// The places where I did some modifications are marked by
// "Arunas was here" comment
/**
 * This class is a reduced version of the original class Browser proposed
 * by Stephen Ostermiller (see the comment bellow). I dropped out all the
 * methods related to GUI definition as well as browser management
 * (javascripting). The code which is present in this version
 * is Java 1.1 compatible (one think that I've needed). It also contains
 * some small corrections to original code.
 * 
 * <p>To use this class, just call its static method <code>displayURL</code>.
 * 
 * <p><i>Original comment from the class <code>Browser</code></i><br>
 * Allows URLs to be opened in the system browser on Windows and Unix.
 * More information about this class is available from <a target="_top" href=
 * "http://ostermiller.org/utils/Browser.html">ostermiller.org</a>.
 * </p>
 * @author Stephen Ostermiller http://ostermiller.org/contact.pl?regarding=Java+Utilities
 * @since ostermillerutils 1.00.00
 * 
 */
public class BrowserBasic {

  // Arunas was here  
  /**
   * Marks if the <code>init</code> method has already been called.
   */
  private static boolean initialized = false;
  
  /**
   * A list of commands to try in order to display the url.
   * The url is put into the command using MessageFormat, so
   * the URL will be specified as {0} in the command.
   * Some examples of commands to try might be:<br>
   * <code>rundll32 url.dll,FileProtocolHandler {0}</code></br>
   * <code>netscape {0}</code><br>
   * These commands are passed in order to exec until something works
   * when displayURL is used.
   *
   * @since ostermillerutils 1.00.00
   */
  protected static Vector exec = new Vector();

  /**
   * Determine appropriate commands to start a browser on the current
   * operating system.  On windows: <br>
   * <code>rundll32 url.dll,FileProtocolHandler {0}</code></br>
   * On other operating systems, the "which" command is used to
   * test if Mozilla, netscape, and lynx(xterm) are available (in that
   * order).
   *
   * @since ostermillerutils 1.00.00
   */
  protected static void init() {
    initDefaultCommands();
    // Arunas was here : note that initialisation has already be done.
    initialized = true;
  }

  /**
   * Retrieve the default commands to open a browser for this system.
   *
   * @since ostermillerutils 1.00.00
   */
  protected static void initDefaultCommands() {
    exec.removeAllElements();
    if (System.getProperty("os.name").startsWith("Windows")) {
      exec.addElement("rundll32 url.dll,FileProtocolHandler {0}");
    } else if (System.getProperty("os.name").startsWith("Mac")) {
      if (hasCommand("open")) {
          // Arunas was here : here and later, I will use addElement method
          // of Vector class to be Java 1.1 compatible
        exec.addElement("open {0}");
      }
    } else {
      if (hasCommand("firebird")) {
        exec.addElement("firebird -remote openURL({0})");
        exec.addElement("firebird {0}");
      }
      if (hasCommand("mozilla")) {
        exec.addElement("mozilla -remote openURL({0})");
        exec.addElement("mozilla {0}");
        }
      // Arunas was here : sometimes, some popular browsers are not in
      // the default path. Lets check in another "default" location
      if (hasCommand("/opt/mozilla/mozilla")) {
        exec.addElement("/opt/mozilla/mozilla -remote openURL({0})");
        exec.addElement("/opt/mozilla/mozilla {0}");
      }
      if (hasCommand("opera")) {
        exec.addElement("opera -remote openURL({0})");
        exec.addElement("opera {0}");
      }
      if (hasCommand("galeon")) {
        exec.addElement("galeon {0}");
      }
      if (hasCommand("konqueror")) {
        exec.addElement("konqueror {0}");
      }
      if (hasCommand("netscape")) {
        exec.addElement("netscape -remote openURL({0})");
        exec.addElement("netscape {0}");
      }
      // Arunas was here : another popular location
      if (hasCommand("/opt/netscape/netscape")) {
        exec.addElement("/opt/netscape/netscape -remote openURL({0})");
        exec.addElement("/opt/netscape/netscape {0}");
      }
      if (hasCommand("xterm")) {
        if (hasCommand("lynx"))
          exec.addElement("xterm -e lynx {0}");
      }
    }
  }

  /**
   * Display a URL in the system browser.
   *
   * <p>Browser.init() should be called before calling this function or
   * Browser.exec should be set explicitly.</p>
   *
   * <p>For security reasons, the URL will may not be passed directly to the
   * browser as it is passed to this method.  The URL may be made safe for
   * the exec command by URLEncoding the URL before passing it.</p>
   *
   *  <p>Note by Arunas : You don't need to call the <code>init</code>
   * method. It is declared private in this implementation and it is
   * called implicitely the first time you use <code>displayURL</code>
   * method.</p>
   * 
   * @param url the url to display
   * @throws IOException if the url is not valid or the browser fails to star
   *
   * @since ostermillerutils 1.00.00
   */
  public static void displayURL(String url) throws IOException {
    if (!initialized) init();
    //
    if (exec.size() == 0) {
      if (System.getProperty("os.name").startsWith("Mac")) {
        boolean success = false;
        try {
          Class nSWorkspace;
          if (new File("/System/Library/Java/com/apple/cocoa/application/NSWorkspace.class").exists()) {
            // Mac OS X has NSWorkspace, but it is not in the classpath, add it.
            // Arunas was here : the orinial code used the
            // java.net.URLClassLoader class witch is not present on Java 1.1.
            // We use here another implementation of a class loader.
            // For more information, see Java Tip 39 on JavaWorld :
            // http://www.javaworld.com/javaworld/javatips/jw-javatip39.html
            ClassLoader classLoader =
              new FileClassLoader("/System/Library/Java/");
            nSWorkspace = classLoader.loadClass("com.apple.cocoa.application.NSWorkspace");
          } else {
            nSWorkspace = Class.forName("com.apple.cocoa.application.NSWorkspace");
          }
          Method sharedWorkspace = nSWorkspace.getMethod("sharedWorkspace", new Class[]{});
          Object workspace = sharedWorkspace.invoke(null, new Object[]{});
          Method openURL = nSWorkspace.getMethod("openURL",
                                         new Class[]{Class.forName("java.net.URL")});
          success = ((Boolean)openURL.invoke(workspace,
                                        new Object[] {new java.net.URL(url)})).booleanValue();
          //success = com.apple.cocoa.application.NSWorkspace.sharedWorkspace().openURL(new java.net.URL(url));
        } catch (Exception x) {
        }
        if (!success) {
          try {
            Class mrjFileUtils = Class.forName("com.apple.mrj.MRJFileUtils");
            Method openURL = mrjFileUtils.getMethod("openURL",
                                            new Class[] {Class.forName("java.lang.String")});
            openURL.invoke(null, new Object[] { url });
            //com.apple.mrj.MRJFileUtils.openURL(url);
          } catch (Exception x) {
            System.err.println(x.getMessage());
            throw new IOException("Filed to open given URL");
          }
        }
      } else {
        throw new IOException("No command was found to open given URL");
      }
    } else {
      // for security, see if the url is valid.
      // this is primarily to catch an attack in which the url
      // starts with a - to fool the command line flags, bu
      // it could catch other stuff as well, and will throw a
      // MalformedURLException which will give the caller of this
      // function useful information.
      new URL(url);
      // escape any weird characters in the url.  This is primarily
      // to prevent an attacker from putting in spaces
      // that might fool exec into allowing
      // the attacker to execute arbitrary code.
      StringBuffer sb = new StringBuffer(url.length());
      for (int i = 0; i < url.length(); i++) {
        char c = url.charAt(i);
        if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')
            || (c >= '0' && c <= '9') || c == '.' || c == ':'
            || c == '&' || c == '@' || c == '/' || c == '?'
            || c == '%' || c == '+' || c == '=' || c == '#'
            || c == '-' || c == '\\')
        {
          //characters that are necessary for URLs and should be safe
          //to pass to exec.  Exec uses a default string tokenizer with
          //the default arguments (whitespace) to separate command line
          //arguments, so there should be no problem with anything bu
          //whitespace.
          sb.append(c);
        } else {
          c = (char) (c & 0xFF); // get the lowest 8 bits (URLEncoding)
          if (c < 0x10) {
            sb.append("%0" + Integer.toHexString(c));
          } else {
            sb.append("%" + Integer.toHexString(c));
          }
        }
      }
      String[] messageArray = new String[1];
      messageArray[0] = sb.toString();
      String command = null;
      boolean found = false;
      // try each of the exec commands until something works
      try {
        for (int i = 0; (i < exec.size()) && (!found); i++) {
          try {
            // stick the url into the command
            command = MessageFormat.format((String)exec.elementAt(i), (Object[]) messageArray);
            // parse the command line.
            Vector argsVector = new Vector();
            // Arunas was here : I did not have the BrowserCommandLexer class
            // used in the original version, so I used the StringTokenizer.
            // It seems to work too.
            StringTokenizer lex = new StringTokenizer(command);
            while (lex.hasMoreTokens())
              argsVector.addElement(lex.nextToken());
            // Arunas was here : the original method toArray does not exists
            // in Vector's Java 1.1 implementation. So, I use another method.
            String[] args = new String[argsVector.size()];
            argsVector.copyInto(args);// vectorToArray(argsVector);
            // the windows url protocol handler doesn't work well with file URLs.
            // Correct those problems here before continuing
            // Java File.toURL() gives only one / following file: bu
            // we need two.
            // If there are escaped characters in the url, we will have
            // to create an Internet shortcut and open that, as the command
            // line version of the rundll doesn't like them.
            if (args[0].equals("rundll32")
                && args[1].equals("url.dll,FileProtocolHandler"))
            {
              if (args[2].startsWith("file:/")) {
                if (args[2].charAt(6) != '/') {
                  args[2] = "file://" + args[2].substring(6);
                }
                if (args[2].charAt(7) != '/') {
                  args[2] = "file:///" + args[2].substring(7);
                }
              }
            }
            // start the browser
            Process p = Runtime.getRuntime().exec(args);
            // give the browser a bit of time to fail.
            // I have found that sometimes sleep doesn't work
            // the first time, so do it twice.  My tests
            // seem to show that 1000 milliseconds is enough
            // time for the browsers I'm using.
            for (int j = 0; j < 2; j++) {
            try {
                Thread.sleep(1000);
              } catch (InterruptedException inte) { }
            }
            // 
            if (p.exitValue() == 0) {
              if (args[0].equals("open")) {
                // Arunas was here : 
                // We have a special case with MacOS "open" command.
                // It always says OK, but in the case of an error,
                // it shows it on the standard error output.
                found = (inputStreamContent(p.getErrorStream()).length() == 0);
              } else {
                // this is a weird case.  The browser exited after
                // a couple seconds saying that it successfully
                // displayed the url.  Either the browser is lying
                // or the user closed it *really* quickly.  Oh well.
                found = true;
              }
            }
          } catch (IOException x) {
            // the command was not a valid command.
            System.err.println("An error occured while opening the URL : " + x.getMessage());
          }
        }
        if (!found) {
          // we never found a command that didn't terminate with an error.
          throw new IOException("The URL could not be opened");
        }
      } catch (IllegalThreadStateException e) {
        // the browser is still running.  This is a good sign.
        // lets just say that it is displaying the url right now!
      }
    }
  }

  // Arunas was here : some additional methods
  
  /**
   * Reads the input stream <code>in</code> and returns its
   * content in a string object. Returns an empty string if the
   * input stream has no content or it is null.
   * 
   * @author Arunas STOCKUS
   */
  private static String inputStreamContent(InputStream in) {
    StringBuffer output = new StringBuffer();
    if (in != null) {
      try {
        BufferedReader r = new BufferedReader(new InputStreamReader(in));
        String line;
        while((line = r.readLine()) != null)
          output.append(line).append("\n");
      } catch(Exception ex) { }                
    }
    return output.toString().trim();
  }
  
  /**
   * Tests if the givent command exists on the system.
   * 
   * @author Arunas STOCKUS
   */
  private static boolean hasCommand(String command) {
    try {
      Process p = Runtime.getRuntime().exec("which "+command);
      return (p.waitFor() == 0);
    } catch (Exception e) {
      return false;
    }
  }
}
