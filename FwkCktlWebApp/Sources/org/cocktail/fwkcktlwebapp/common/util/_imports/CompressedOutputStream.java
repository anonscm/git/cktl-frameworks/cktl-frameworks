package org.cocktail.fwkcktlwebapp.common.util._imports;


import java.io.FilterOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.zip.Deflater;

/**
 * Create a compressed (deflater) output stream.
 * 
 * @version $Revision: 1.13 $ $Date: 2003/06/10 13:39:38 $
 * 
 * @author Patrice Espié
 * 
 * @see fr.univlr.cri._imports.CompressedInputStream
 * 
 * @copyright Copyright (c) 2002, 2003 Patrice Espié - all rights reserved
 * 
 * @contact <a href='mailto:dev@patrice-espie.org'>dev@patrice-espie.org</a>
 * 
 * @licensing
 *  <i>This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.</i>
 *
 *  <p><i>This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 *  Lesser General Public License for more details.
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA</i></p> 
 */
public class CompressedOutputStream extends FilterOutputStream
{
    private byte buffer[];
    private byte compBuffer[];
    private int writeIndex;
    private Deflater deflater;

    public CompressedOutputStream(OutputStream out)
    {
        super(out);
        buffer = new byte[0x7d000];
        compBuffer = new byte[(int)((double)buffer.length * 1.2D)];
        writeIndex = 0;
        deflater = new Deflater(9);
    }

    public void write(byte b[])
        throws IOException
    {
        write(b, 0, b.length);
    }

    public void write(byte b[], int off, int len)
        throws IOException
    {
        int written = 0;
        do
        {
            if(written >= len)
                break;
            int toWrite = Math.min(len - written, buffer.length - writeIndex);
            System.arraycopy(b, off + written, buffer, writeIndex, toWrite);
            written += toWrite;
            writeIndex += toWrite;
            if((double)writeIndex >= (double)buffer.length * 0.80000000000000004D)
                flush();
        } while(true);
    }

    public void write(int b)
        throws IOException
    {
        if((double)writeIndex >= (double)buffer.length * 0.80000000000000004D)
            flush();
        buffer[writeIndex++] = (byte)b;
    }

    public void flush()
        throws IOException
    {
        int compSize = 0;
        boolean sendCompressed;
        if(writeIndex > 150)
        {
            deflater.reset();
            deflater.setInput(buffer, 0, writeIndex);
            deflater.finish();
            if(compBuffer.length < writeIndex * 2 + 40960)
                compBuffer = new byte[writeIndex * 2 + 40960];
            compSize = deflater.deflate(compBuffer);
            if(compSize <= 0)
                throw new IOException("Compression exception");
            sendCompressed = compSize < writeIndex;
        } else
        {
            sendCompressed = false;
        }
        if(sendCompressed)
        {
            super.out.write(1);
            super.out.write(writeIndex >> 24 & 0xff);
            super.out.write(writeIndex >> 16 & 0xff);
            super.out.write(writeIndex >> 8 & 0xff);
            super.out.write(writeIndex & 0xff);
            super.out.write(compSize >> 24 & 0xff);
            super.out.write(compSize >> 16 & 0xff);
            super.out.write(compSize >> 8 & 0xff);
            super.out.write(compSize & 0xff);
            super.out.write(compBuffer, 0, compSize);
            super.out.flush();
            writeIndex = 0;
        } else
        if(writeIndex > 0)
        {
            super.out.write(0);
            super.out.write(writeIndex >> 24 & 0xff);
            super.out.write(writeIndex >> 16 & 0xff);
            super.out.write(writeIndex >> 8 & 0xff);
            super.out.write(writeIndex & 0xff);
            super.out.write(buffer, 0, writeIndex);
            super.out.flush();
            writeIndex = 0;
        }
    }
}
