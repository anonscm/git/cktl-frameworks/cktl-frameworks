package org.cocktail.fwkcktlwebapp.common.util;


import java.util.Hashtable;
import java.util.Vector;

import org.cocktail.fwkcktlwebapp.common.util._imports.XNode;
import org.cocktail.fwkcktlwebapp.common.util._imports.Xparse;


/**
 * Permet d'effectuer une simple analyse d'un document XML.
 * 
 * <p>Une instance de cette classe represente un noeud d'un document XML.
 * La classe propose egalement les methodes (<code>parse</code>) permettant
 * d'analyser un document et acceder a son noeud racine.</p>
 * 
 * <p>Cette classe est principalement dediee a l'analyse des documents
 * XML simples avec peut des ressources necessaires. Pour une analyse complete
 * et conforme au standards XML, utilisez un analyseur comme Xerces de Apache,
 * par exemple.</p>
 * 
 * <h3>Un exemple</h3>
 * 
   * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
   * <code class="comment">// Supposons que cette methode existe...</code>
   * String xmlContent = readContentFromFile("C:/Temp/xmlFile.xml")
   * <code class="comment">// L'analyse et la reference vers le noeud racine</code>
   * CktlXMLNode rootNode = CktlXMLNode.parse(xmlContent);
   * <code class="comment">// Retrouver la valeur de la premiere occurence
   * // qui correspond au chemin donne</code>
   * CktlXMLNode node = rootNode.find("bulletins/bulletin/salarie/nom", "1, 1, 1, 1");
   * if (node == null)
   *   System.out.println("Pas de noeud !");
   * else
   *   System.out.println("Valeur : "+node.getCharacters());
   * <code class="comment">// Retrouver un element avec le nom donne</code>
   * node = xmlRoot.findChild("total", true);
   * if (node == null)
   *    System.out.println("  Noeud "+total+" n'est pas trouve");
   * else
   *   System.out.println("  Noeud " +total+" existe");</code></pre></td></tr></table>
   */
public class CktlXMLNode {
  /**
   * La vraie implementation de noeud.
   */
  private XNode node;
  
  /**
   * Cree un objet correspondant a un nouvel noeud. Ce constructeur doit
   * rester prive, car les noeuds ne sont creees que par l'analyseur.
   */
  private CktlXMLNode(XNode node) {
    this.node = node;
  }
  
  /**
   * Effectue l'analyse de document XML dont le contenu se trouve dans
   * <code>xmlText</code>. Retourn le raference vers le noeud representant
   * la racine de document.
   * 
   * <p>Utiliser ensuite les methodes find pour acceder a d'autres noeuds
   * du document.</p>
   */
  public static CktlXMLNode parse(String xmlText) {
    XNode node = new Xparse().parse(xmlText);
    if (node != null)
      return new CktlXMLNode(node); 
    else
      return null;
  }
  
  /**
   * Retourne le dictionnaire avec les definitions des attributs de noeud.
   */
  public Hashtable getAttributes() {
    return node.attributes;
  }
  
  /**
   * Retourne le text definit entre les balises ouvrante et fermante du
   * noeud en cours.
   */
  public String getCharacters() {
    return node.getCharacters();
  }
  
  /**
   * Retourne le nom du noeud en cours.
   */
  public String getName() {
    return node.name;
  }
  
  /**
   * Retourne la liste de tous les noeuds fils de ce noeud.
   * Retourne la liste vide si le noeud n'as pas de fils.
   */
  public Vector getChildren() {
    Vector v = new Vector();
    XNode n;
    if ((node != null) && (node.contents != null)) {
      for (int i=0; i<node.contents.length(); i++) {
        n = (XNode)node.contents.elementAt(i);
        if (n.type == XNode.Element)
          v.addElement(new CktlXMLNode(n));
      }
    }
    return v;
  }
  
  /**
   * Retrouve le noeud qui se trouve a l'indroit indique par l'expression
   * de chemin <code>path</code>. Le parametre <code>occur</code> indique
   * le(s) numero(s) d'occurences des noeuds.
   * 
   * <p>Retourne le noeud trouve ou <i>null</i> si aucun noeud ne correspond
   * au chemin donnee.</p>
   * 
   * <p>Exemple :<br>
   * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
   * int[] occur = {1, 3, 1};<br>
   * CktlXMLNode node = rootNode.find("/liste/salarie/nom", occur);
   * if (node == null)
   *   System.out.println("Pas de noeud !");
   * else
   *   System.out.println("Valeur : "+node.getCharacters());</code></pre></td></tr></table>
   * 
   * Dans cette exemple, on cherche le nom de troisiemme salarie dans la liste
   * de tous les salaries. On suppose donc, que dans le document XML, le
   * noeud <i>liste</i> est dans la racine et il peut avoir plusieurs noeuds
   * fils <i>salarie</i>.
   * </p>
   * 
   * @see #find(String, String)
   * @see #findFirst(String)
   */
  public CktlXMLNode find(String path, int[] occur) {
    return new CktlXMLNode(node.find(path, occur));
  }

  /**
   * Retrouve le noeud qui se trouve a l'endroi indique par l'expression
   * de chemin <code>path</code>. Le parametre <code>occur</code> indique le(s)
   * numero(s) d'occurences des noeuds. Ils doivent etre separes par les
   * virgules dans la chaine <code>occur</code>.
   * 
   * <p>Retourne le noeud trouve ou <i>null</i> si aucun noeud ne correspond
   * au chemin donne.</p>
   * 
   * 
   * @see #find(String, int[])
   * @see #findFirst(String)
   */
  public CktlXMLNode find(String path, String occur) {
    Vector v = StringCtrl.toVector(occur, ",");
    int[] occurs = new int[v.size()];
    for (int i=0; i<v.size(); i++)
      occurs[i] = Integer.parseInt(((String)v.elementAt(i)).trim());
    return this.find(path, occurs);
  }
  
  /**
   * Retrouve le noeud qui se trouve a l'indroit indique par l'expression
   * de chemin <code>path</code>. Le noeud correspondant a la premiere
   * occurence des fils est retourne.
   * 
   * @see #find(String, String)
   * @see #find(String, int[])
   */
  public CktlXMLNode findFirst(String path) {
    Vector v = StringCtrl.toVector(path, "/");
    int[] occurs = new int[v.size()];
    for (int i=0; i<v.size(); i++)
      occurs[i] = 1;
    return this.find(path, occurs);
  }
  
  /**
   * Cherche parmis les fils du noeud en cours celui ayant le
   * nom <code>tagName</code>. Retourn l'objet correspondant ou
   * <i>null</i> si aucun fils avec le nom demande n'est trouve.
   * 
   * <p>Cette methode est recursivement appelee sur les fils de noeud,
   * si le parametre <code>recursive</code> est defini a <i>true</i>.</p>
   */
  public CktlXMLNode findChild(String tagName, boolean recursive) {
    Vector children = getChildren();
    CktlXMLNode n;
    for(int i=0; i<children.size(); i++) {
      n = (CktlXMLNode)children.elementAt(i);
      if (n.getName().equals(tagName)) return n;
      if (recursive) {
        n = n.findChild(tagName, recursive);
        if (n != null) return n;
      }
    }
    return null;
  }
  
  /**
   * Teste l'egalite de l'objet avec <code>object</code>.
   */
  public boolean equals(Object object) {
    if (object instanceof XNode)
      return object.equals(node);
    else
      return super.equals(object);
  }
  
  @Override
	public int hashCode() {
		return super.hashCode();
	}
}
