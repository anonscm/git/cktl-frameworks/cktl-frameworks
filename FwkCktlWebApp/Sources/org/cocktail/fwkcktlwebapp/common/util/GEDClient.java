package org.cocktail.fwkcktlwebapp.common.util;


import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Fournit une interface de communication avec le serveur de fichier GED.
 * Elle contient l'ensemble de methodes permettant d'effectuer les operations
 * (elementaires) de manipulation de fichiers.
 * 
 * 
 * <h3>Principe d'utilisation du service GED</h3>
 * 
 * Lors de la gestion des documents (des fichiers), l'application du
 * client n'accede pas au stockage physique des documents. Le client
 * ne doit donc pas manipuler explicitement les notions comme
 * "chemin d'acces a un fichier", "sous-repertoires", ect... Ceci est gere
 * par le serveur GED de maniere transparente.
 * 
 * <p>Toute operation sur un document s'effectue via son identifiant.
 * A chaque document nouvellement enregistre, le serveur GED associe
 * un identifiant unique et le communique au client. L'application
 * client doit le memoriser afin de pouvoir acceder au document
 * ulterieurement.
 * 
 * 
 * <h3>Les classes pour les <i>applications client</i> GED</h3>
 * 
 * <p>La communication entre le client et le serveur GED repose
 * sur la classe <code>GEDDescription</code> qui definit le protocole de
 * communication. Elle contient l'ensemble des informations sur le document
 * et decrit les operations que l'on souhaite effectuer.
 * 
 * <p>L'application client ne manipule pas cette classe explicitement. Un
 * objet de cette classe et cree et parametre, ainsi que les operations
 * sont definies lors des appels aux methodes de <code>GEDClient</code>.
 * En revanche, l'application client peut utiliser l'objet
 * <code>GEDDescription</code> renvoyer par le client GED pour analyser
 * le resultat d'une operation.
 * 
 * <p>L'objet <code>GEDDescription</code> de la derniere operation est
 * renvoye par la methode <code>description()</code> de la classe
 * <code>GEDClient</code>. Apres une operation, l'objet
 * <code>GEDDescription</code> contient les informations principales
 * suivantes (les attributs de l'objet)&nbsp;:
 * <ul>
 * <li><code>reference</code> - un lien URL d'acces au document;</li>
 * <li><code>documentId</code> - l'identifiant du document;</li>
 * <li><code>parentDocumentId</code> - l'identifiant du document pere;</li>
 * <li><code>message</code> - la description d'erreur survenue sur
 *   le serveur;</li>
 * </ul>
 * 
 * 
 * <h3>Gestion des repertoires de documents</h3>
 * 
 * Les documents individuels sont en general enregistres sur le serveur
 * GED sans prendre en compte leur repertoires d'origine. Le serveur
 * GED fournit aussi les methodes permettant d'enregistres les
 * arborescences des documents. Cette fonctionnalite peut etre
 * utilisee pour enregistrer des parties des sites Web, par exemple. 
 * 
 * <p>Les arborescences des fichiers sont enregistrees en utilisant
 * le concepte de "document pere". Le serveur permet de reserver
 * un espace qui pourra etre partage par plusieurs fichiers. Il est
 * appele <i>document pere</i> ou encore <i>racine d'arborescence</i>
 * et correspond a un type special de documents sur le serveur.
 * Le document pere possede un identifiant et peut etre manipule
 * comme tous les autres documents sur le serveur.
 * 
 * <p>Les documents d'utilisateur pevent ensuite etre enregistres
 * en precisant le meme document pere. Ils seront ainsi place
 * dans une sous-arborescence du pere. 
 * 
 * <p>Plus d'information sur la manipulation des arborescences
 * de documents est donnee dans les exemples ci-dessous.
 * 
 * 
 * <h3>Chemins et noms des fichiers</h3>
 * 
 * Dans les chemins des fichiers, les deux symboles "\" et
 * "/" sont reconnus comme les separaters des fichiers.
 * 
 * <p>Le client (et le serveur GED) verifie si les noms des
 * fichiers ne contient pas des caracteres speciaux. Les seuls
 * caracteres autorises dans les noms des fichiers sont les
 * lettres non accentuees, les chiffres et les symboles "_"
 * (souligne), "." (point) et "-" (tire).
 * 
 * <p>Si on enregistre un fichier individuel et des caracteres
 * non-autorises sont present dans son nom, ils sont remplaces
 * par le symbole "_".
 * 
 * <p>Si on enregistre une arborescence contenant des fichier
 * ou des repertoires avec des caracteres non-autorise,
 * l'operation d'enregistrement est refusee. Ceci evite d'enregistrer
 * les references incorrectes entre les fichiers lors de depot
 * des sites Web, par exemple. 
 * 
 * 
 * <h3>Exemples</h3>
 * 
 * <h4>Enregistrer un fichier</h4>
 * 
 * On fait appel aux methodes <code>enregistrerDocument</code> pour
 * enregistrer un fichier.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * <code class="comment">// Le fichier local a enregistrer</code>
 * String fileName = "/MyDocuments/ASauver/mon_document.doc";
 * <code class="comment">// Supposons que les fonctions suivantes existent...</code>
 * String gedHost = getGEDHost();
 * int gedPort = getGEDPort();
 * int userId = getNoIndividu();
 * String appCode = getApplicationId();
 * <code class="comment">// Creer un objet client...</code>
 * GEDClient client = new GEDClient(gedHost, gedPort, userId, appCode);
 * client.reset();
 * int documentId = client.enregistrerDocument(fileName, "Titre document", "CATEGORIE_TEST");
 * if (documentId == -1)
 *   System.out.println("Erreur. Message : "+client.lastMessage());
 * else
 *   System.out.println("OK. Reference : "+client.description().reference);
 * <code class="comment">// documentId est un identifiant a memoriser</code></pre></td></tr></table>
 *
 *
 * <h4>Consulter les informations sur un document</h4>
 * 
 * On utilise la methode <code>inspecterDocument</code> pour obnenir
 * les informations sur un document. Elles sont renvoies dans un objet
 * <code>GEDDescription</code> accessible via la methode
 * <code>description</code> de client.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * <code class="comment">// L'identifiant du document</code>
 * int documentId = gedSavedDocumentId();
 * <code class="comment">// Initialiser les parametres pour le client...</code>
 * <code class="comment">// ...</code>
 * GEDClient client = new GEDClient(gedHost, gedPort, userId, appCode);
 * client.reset();
 * if (client.inspecterDocument(documentId))
 *   System.out.println("OK. Reference URL : " + client.description().reference);
 * else
 *   System.out.println("Erreur. Message: " + client.lastMessage());</pre></td></tr></table>
 * 
 * 
 * <h4>Enregistrer une arborescence</h4>
 * 
 * Pour enregistrer une arborescence de documents, on reserve d'abord
 * un espace dans une categorie souhaitee (appel a la methode
 * <code>reserverDocument()</code>). On utilise ensuite les methodes
 * <code>enregistrerDocument()</code> prenant l'identifiant du document
 * pere parmis les parametres.
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * try {
 *   int documentId, documentPereId;
 *   <code class="comment">// Les fichiers a enregistrer</code>
 *   String fileName1 = "/docs/ex1/remarques.html";
 *   String fileName1 = "/docs/ex1/images/logo.gif";
 *   <code class="comment">// Les sous-repertoires dans lesquels on les placera sur le serveur</code>
 *   String subDir1 = "ex1";
 *   String subDir2 = "ex1/images";
 *   FileInputStrem fin1 = new FileInputStream(fileName1);
 *   FileInputStrem fin2 = new FileInputStream(fileName2);
 *   <code class="comment">// La taille des fichiers en octets </code>
 *   int sizeFile1 = FileCtrl.fileSize(fileName1);
 *   int sizeFile2 = FileCtrl.fileSize(fileName2);
 *   String categorie = "CATEGORIE_TEST";
 *   <code class="comment">// Initialiser les parametres pour le client...</code>
 *   <code class="comment">// ...</code>
 *   GEDClient client = new GEDClient(gedHost, gedPort, userId, appCode);
 *   client.reset();
 *   <code class="comment">// Reserver un nouvel espace sur le serveur</code>
 *   documentPereId = client.reserverDocument(categorie);
 *   if (documentPereId == -1) return; <code class="comment">// Echeque...</code>
 *   <code class="comment">// Enregistrer le premier document</code>
 *   documentId = client.enregistrerDocument(fin1, "Test depot repertoire", sizeFile1,
 *     FileCtrl.getFileName(fileName1), subDir1, documentPereId, categorie);
 *   if (documentId == -1) return; <code class="comment">// Echeque...</code>
 *   <code class="comment">// Enregistrer le deuxieme document</code>
 *   documentId = client.enregistrerDocument(fin2, "Test depot repertoire", sizeFile2,
 *     FileCtrl.getFileName(fileName2), subDir2, documentPereId, categorie);
 *   if (documentId == -1) return; <code class="comment">// Echeque...</code>
 * } catch(Exception ex) {
 *   <code class="comment">// Traitement d'erreur...</code>
 *   return;
 * }
 * <code class="comment">// Memoriser les indentifiants des documents enregistres...</code></pre></td></tr></table>
 * 
 * 
 * <h4>Supprimer une arborescence</h4>
 * 
 * Pour supprimer une arborescence, on doit d'abord vider son contenu
 * (appel de la methode <code>viderRepertoire()</code>). On supprime ensuite
 * le document pere (appel de la methode <code>supprimerDocument()</code>).
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * int documentPereId; 
 * <code class="comment">// Le id de document appartenant a l'arborescence</code>
 * int documentId = ...;
 * <code class="comment">// Initialisation de client...</code>
 * <code class="comment">// ...</code>
 * GEDClient client = new GEDClient(gedHost, gedPort, userId, appCode);
 * client.reset();
 * <code class="comment">// Interroger le document pour obtenir le ID de son document pere</code>
 * if (!client.inspecterDocument(documentId)) return; <code class="comment">// Echeque...</code>
 * <code class="comment">// L'identifiant de la racine de l'arborescence</code>
 * documentPereId = client.description().parentDocumentId;
 * <code class="comment">// Supprimer le contenu de l'arborescence</code>
 * if (!client.viderRepertoire(documentPereId)) return; <code class="comment">// Echeque...</code>
 * <code class="comment">// On supprime la racine elle-meme</code>
 * if (!client.supprimerDocument(documentPereId)) return; <code class="comment">// Echeque...</code>
 * <code class="comment">// OK, s'est fini</code></pre></td></tr></table>
 * 
 * <p><i>Videz toujours le contenu d'une arborescence avant de supprimer
 * le document pere !</i>
 *
 * 
 * <h4>Remplacer un document</h4>
 * 
 * Le documents individuels sont remplaces a l'aide des methodes
 * <code>remplacerDocument()</code>.
 * 
 * <p>Pour remplacer les documents d'une arborescence, on vide d'abord
 * son contenu (methode <code>viderRepertoire()</code>) et on enregistre
 * une nouvelle arborescence en utilisant l'identifiant du meme document
 * pere. Le code serait en principe le meme que celui de l'exemple de
 * la suppression d'une arborescence. 
 *
 *  
 * @see GEDDescription
 */
public class GEDClient {
  ///** Le timeout de communication avec le serveur GEDFS en secondes (10sec.) */
  //private static final int GED_CONNECTION_TIMEOUT = 10000;
  /** Le timeout de communication avec le serveur GEDFS en secondes (120sec.) */
  private static final int GED_CONNECTION_TIMEOUT = 120000;
  
//  /**
//   * Le socket de communication avec le serveur de fichiers GED.
//   */
//  private Socket connectionSocket;
  
  /**
   * Le stream de lecture des informations envoyees par le serveur
   * GED. 
   */
  private InputStream in;
  
  /**
   * Le stream d'ecriture des informations vers le serveur GED.
   */
  private OutputStream out;
  
  /**
   * La description du document et des operations. 
   */
  private GEDDescription description;
  
  /**
   * Le stream representant la source du document. 
   */
  private InputStream sourceStream;
  
  /**
   * Le nom ou l'adresse IP du serveur GED. 
   */
  private String gedServerName;
  
  /**
   * Le numero du port de connexion au serveur GED.
   */
  private int gedServerPort;
  
  /**
   * Le ID d'utilisateur (NO_INDIVIDU) souhaitant 
   * d'effectuer les operations avec le serveur GED.
   */
  private int defaultUserId;
  
  /**
   * L'identifiant de l'application dans le cadre de laquelle
   * le client GED est utilise.
   */
  private String applicationId;
  
  /**
   * Indique si le client GED va liberer les ressources
   * automatiquement a la fin d'une opration. Typiquement,
   * il s'agit de la fermeture des streams ouverts pour la
   * lecture/ecriture de donnees.
   * 
   * <p>La valeur par defaut est <em>true</em>.
   */
  private boolean autoRelease;
  
/* ===== Initialisation et configuration ===== */

  /**
   * Creation de client GED. Le client est cree pour communiquer
   * avec un seul serveur GED et a partir d'une seule application.
   * Les parametres <code>gedServerName</code>, <code>gedServerPort</code>
   * et <code>applicationCode</code> ne peuvent pas etre modifies par la suite.
   * 
   * <p>Le parametre <code>userId</code> est modifiable via l'appel a la
   * methode <code>setUserInfo</code>.
   * 
   * <p>Le meme client GED peut etre utilise pour gerer l'acces a plusieurs
   * documents sur le serveur GED.
   * 
   * @param gedServerName Le nom ou l'adresse IP du serveur GED. 
   * @param gedServerPort Le numero du port pour se connecter au serveur GED.
   * @param userId Le numero d'individu souhaitant acceder au serveur GED. Ce
   *    parametre peut etre egale a -1 s'il s'agit d'accès annonyme.
   * @param applicationId L'identifiant de l'application dans laquelle on utilise
   *    le client GED. Ce code doit etre connu par le serveur GED.
   */  
  public GEDClient(String gedServerName, int gedServerPort,
                   int userId, String applicationId)
  {
    this.defaultUserId = userId;
    this.applicationId = applicationId;
    this.gedServerName = gedServerName;
    this.gedServerPort = gedServerPort;
    this.autoRelease = true;
    reset();
  }

  /**
   * Etablie la configuration par defaut du client GED. Une nouvelle description
   * de document est creee, les streams d'acces aux documents sont fermes et le
   * code de l'application et d'utilisateurs sont remis aux valeurs donnees
   * lors de la creation du client.
   */
  public void reset() {
    description = new GEDDescription();
    StreamCtrl.forceClose(sourceStream);
    sourceStream = null;
    description.applicationId = applicationId;
    description.userId = defaultUserId;
  }
  
/* ==== Interface Enregistrement ==== */  

  /**
   * Enregistre le fichier local. Utilise le meme nom, que celui du fichier.
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param cheminFichier 
   * @param titre 
   * @param categorie 
   */
  public int enregistrerDocument(String cheminFichier,
                                 String titre,
                                 String categorie)
  {
    try {
      return enregistrerDocument(cheminFichier, titre, FileCtrl.getFileName(cheminFichier), categorie);
    } catch(Exception ex) {
      ex.printStackTrace();
      description.message = ex.getMessage();
      return -1;
    }
  }

  /**
   * Enregistre le fichier local avec un nouveau nom.
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param cheminFichier 
   * @param titre 
   * @param nomDocument 
   * @param categorie 
   */
  public int enregistrerDocument(String cheminFichier,
                                 String titre,
                                 String nomDocument,
                                 String categorie)
  {
    try {
      File f = new File(cheminFichier);
      if (!f.exists()) {
        description.message = "Fichier local \""+cheminFichier+"\" n'existe pas";
        return -1;
      }
      return enregistrerDocument(new FileInputStream(cheminFichier), titre, f.length(), nomDocument, categorie);
    } catch(Exception ex) {
      ex.printStackTrace();
      description.message = ex.getMessage();
      return -1;
    }
  }

  /**
   * Enregistre le document represente par un tableau d'octets.
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param donnees 
   * @param titre 
   * @param nomDocument 
   * @param categorie 
   */
  public int enregistrerDocument(byte[] donnees,
                                 String titre,
                                 String nomDocument,
                                 String categorie)
  {
    return enregistrerDocument(new ByteArrayInputStream(donnees), titre, donnees.length, nomDocument, categorie);
  }
  
  private final static int MAX_SIZE_TITRE 				= 256;
  private final static int MAX_SIZE_NOM_DOCUMENT 	= 128;
  
  /**
   * Enregistre document a partir d'un flot d'octets indique.
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param stream 
   * @param titre 
   * @param streamTaille 
   * @param nomDocument 
   * @param categorie 
   */
  public int enregistrerDocument(InputStream stream,
                                 String titre,
                                 long streamTaille,
                                 String nomDocument,
                                 String categorie)
  {
    description.operationId = GEDDescription.OP_SAVE;
    description.reference = StringCtrl.emptyString();
    
    // on recoupe le nom de fichier a 128 caracteres
    String name = StringCtrl.toBasicString(nomDocument);
    if (!StringCtrl.isEmpty(name) && name.length() > MAX_SIZE_NOM_DOCUMENT) {
    	name = name.substring(0, MAX_SIZE_NOM_DOCUMENT);
    }
    description.name = name;
    description.directory = StringCtrl.emptyString();
    // on recoupe le titre a 256 caracteres
    String title = titre;
    if (!StringCtrl.isEmpty(title) && title.length() > MAX_SIZE_TITRE) {
    	title = title.substring(0, MAX_SIZE_TITRE);
    }
    description.title = title;
    description.documentId = -1;
    description.parentDocumentId = -1;
    description.size = streamTaille;
    description.refType = GEDDescription.REF_TYPE_FILE;
    description.category = categorie;
    description.userId = defaultUserId;
    sourceStream = stream;
    if (send())
      return description.documentId;
    else
      return -1;
  }

  /**
   * 
   * @param stream
   * @param titre
   * @param streamTaille
   * @param nomDocument
   * @param repertoire
   * @param documentPereId
   * @param categorie
   * @return int
   */
  public int enregistrerDocument(InputStream stream,
                                 String titre,
                                 long streamTaille,
                                 String nomDocument,
                                 String repertoire,
                                 int documentPereId,
                                 String categorie)
  {
    description.operationId = GEDDescription.OP_SAVE;
    description.reference = StringCtrl.emptyString();
    // on recoupe le nom de fichier a 128 caracteres
    String name = StringCtrl.toBasicString(nomDocument);
    if (!StringCtrl.isEmpty(name) && name.length() > MAX_SIZE_NOM_DOCUMENT) {
    	name = name.substring(0, MAX_SIZE_NOM_DOCUMENT);
    }
    description.name = name;
    description.directory = repertoire;
    // on recoupe le titre a 256 caracteres
    String title = titre;
    if (!StringCtrl.isEmpty(title) && title.length() > MAX_SIZE_TITRE) {
    	title = title.substring(0, MAX_SIZE_TITRE);
    }
    description.title = title;
    description.documentId = -1;
    description.parentDocumentId = documentPereId;
    description.size = streamTaille;
    description.refType = GEDDescription.REF_TYPE_FILE;
    description.category = categorie;
    description.userId = defaultUserId;
    sourceStream = stream;
    // Verifier si le nom du repertoire ne comporte pas des symboles incorects
    if (!FileCtrl.isBasicPath(repertoire)) {
      description.message = "Le nom du repertoire \""+repertoire+"\" contient des symboles qui ne sont pas acceptes par le serveur.";
      description.status = GEDDescription.ERROR;
      return -1;
    }
    // L'envoi des donnees... 
    if (send())
      return description.documentId;
    else
      return -1;
  }

  /**
   * Enregistre une reference vers un document (typiquement, une URL).
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param reference 
   * @param titre 
   * @param categorie 
   */
  public int enregistrerReference(String reference, String titre, String categorie)
  {
    description.operationId = GEDDescription.OP_SAVE;
    description.reference = reference;
    description.name = StringCtrl.emptyString();
    description.directory = StringCtrl.emptyString();
    // on recoupe le titre a 256 caracteres
    String title = titre;
    if (!StringCtrl.isEmpty(title) && title.length() > MAX_SIZE_TITRE) {
    	title = title.substring(0, MAX_SIZE_TITRE);
    }
    description.title = title;
    description.documentId = -1;
    description.parentDocumentId = -1;
    description.size = 0;
    description.refType = GEDDescription.REF_TYPE_URL;
    description.category = categorie;
    description.userId = defaultUserId;
    sourceStream = null;
    if (send())
      return description.documentId;
    else
      return -1;
  }

/* ==== Interface Remplacement ==== */  

  /**
   * Remplace le document avec identifiant <code>documentId</code>
   * par le document enregistre dans un fichier local
   * <code>cheminFichier</code>. Le nouveau document recoit
   * un nouveau <code>titre</code>. Retourne <i>true</i>
   * l'operation est effectuee avec succes et <i>false</i> dans
   * le cas contraire.
   */
  public boolean remplacerDocument(String cheminFichier,
                                   String titre,
                                   int documentId)
  {
    try {
      return remplacerDocument(cheminFichier, titre, FileCtrl.getFileName(cheminFichier), documentId);
    } catch(Exception ex) {
      ex.printStackTrace();
      description.message = ex.getMessage();
      return false;
    }
  }

  /**
   * Remplace le document avec l'identifiant <code>documentId</code>
   * par le document enregistre dans un fichier local
   * <code>cheminFichier</code>. Le document est remplace avec un
   * nouveau <code>tire</code> et nom <code>nomDocument</code>.
   * Retourne <i>true</i> l'operation est effectuee avec succes
   * et <i>false</i> dans le cas contraire.
   */
  public boolean remplacerDocument(String cheminFichier,
                                   String titre,
                                   String nomDocument,
                                   int documentId)
  {
    try {
      File f = new File(cheminFichier);
      if (!f.exists()) {
        description.message = "Fichier local \""+cheminFichier+"\" n'existe pas";
        description.status = GEDDescription.ERROR;
        return false;
      }
      return remplacerDocument(new FileInputStream(cheminFichier), titre, f.length(), nomDocument, documentId);
    } catch(Exception ex) {
      ex.printStackTrace();
      description.message = ex.getMessage();
      return false;
    }
  }

  /**
   * Remplace le document avec l'identifiant <code>documentId</code>
   * par un document dont le contenu est donne dans un tableau d'octets
   * <code>donnees</code>. Le document est enregistre avec un nouveau
   * <code>titre</code> et nom <code>nomDocument</code>. Retourne <i>true</i>
   * l'operation est effectuee avec succes et <i>false</i> dans
   * le cas contraire.
   */
  public boolean remplacerDocument(byte[] donnees,
                                   String titre,
                                   String nomDocument,
                                   int documentID)
  {
    return remplacerDocument(new ByteArrayInputStream(donnees), titre, donnees.length, nomDocument, documentID);
  }

  /**
   * Remplace le document avec l'identifiant <code>documentId</code>
   * par le document dont le contenu est lu a partir d'un flux d'octets
   * <code>stream</code>.
   */
  public boolean remplacerDocument(InputStream stream,
                                   String titre,
                                   long streamTaille,
                                   String nomDocument,
                                   int documentId)
  {
    description.operationId = GEDDescription.OP_REPLACE;
    description.reference = StringCtrl.emptyString();
    // on recoupe le nom de fichier a 128 caracteres
    String name = StringCtrl.toBasicString(nomDocument);
    if (!StringCtrl.isEmpty(name) && name.length() > MAX_SIZE_NOM_DOCUMENT) {
    	name = name.substring(0, MAX_SIZE_NOM_DOCUMENT);
    }
    description.name = name;
    description.directory = StringCtrl.emptyString();
    // on recoupe le titre a 256 caracteres
    String title = titre;
    if (!StringCtrl.isEmpty(title) && title.length() > MAX_SIZE_TITRE) {
    	title = title.substring(0, MAX_SIZE_TITRE);
    }
    description.title = title;
    description.documentId = documentId;
    description.parentDocumentId = -1;
    description.size = streamTaille;
    description.refType = GEDDescription.REF_TYPE_FILE;
    description.userId = defaultUserId;
    sourceStream = stream;
    return send();
  }

  /**
   * Enregistre une reference vers un document (typiquement, une URL).
   * Returne le code de document enregistre ou -1 dans le cas d'echec.
   * 
   * @param reference 
   * @param titre 
   * @param documentId 
   */
  public boolean remplacerReference(String reference, String titre, int documentId)
  {
    description.operationId = GEDDescription.OP_REPLACE;
    description.reference = reference;
    description.name = StringCtrl.emptyString();
    description.directory = StringCtrl.emptyString();
    // on recoupe le titre a 256 caracteres
    String title = titre;
    if (!StringCtrl.isEmpty(title) && title.length() > MAX_SIZE_TITRE) {
    	title = title.substring(0, MAX_SIZE_TITRE);
    }
    description.title = title;
    description.documentId = documentId;
    description.parentDocumentId = -1;
    description.size = 0;
    description.refType = GEDDescription.REF_TYPE_URL;
    description.userId = defaultUserId;
    sourceStream = null;
    return send();
  }
  
/* ==== Interface Suppression ==== */  
  
  /**
   * Supprime le document avec l'identificateur donnee.
   * 
   * @param documentId 
   */
  public boolean supprimerDocument(int documentId) {
    description.operationId = GEDDescription.OP_DELETE;
    description.documentId = documentId;
    description.parentDocumentId = -1;
    return send();
  }

  /**
   * Supprime les documents dont le document pere a l'identificateur
   * donne. Le document pere represente le pere d'une arborescence
   * des documents enregistree sur le serveur.
   * 
   * <p>Cette methode ne devrait pas appele que pour les arborescences
   * des documents. Utilisez la methode <code>supprimerDocument()</code>
   * pour supprimer les documents individuels.
   * 
   * @param documentPereId Le identifiant du document pere d'une arborescence
   *     des documents. 
   * @return Renvoie <code>true</code> si l'arborecence a ete supprimee avec
   *     succes et <code>false</code> si elle ne peut pas etre supprimee
   *     ou l'identificateur ne prepresente pas d'arborescence. 
   */
  public boolean viderRepertoire(int documentPereId) {
    description.operationId = GEDDescription.OP_CLEAN;
    description.documentId = documentPereId;
    description.parentDocumentId = -1;
    description.refType = GEDDescription.REF_TYPE_ROOT;
    return send();
  }

/* ==== Interface Inspection ==== */  

  /**
   * @param documentId 
   */
  public boolean inspecterDocument(int documentId) {
    description.operationId = GEDDescription.OP_INSPECT;
    description.documentId = documentId;
    return receive();
  }

/* ==== Fonctions supplementaires pour la gestion de documents ==== */

  /**
   * Demande le serveur de reserver un endroit pour placer
   * une arborescences des documents ensuite enregistres. Cet appel
   * creer un document fictif du cote de serveur qui peut etre
   * considere comme le document pere des documents enregistes
   * par la suite.
   * 
   * <p>L'appel de cette methode devrait etre suivit des appelts des
   * methodes <code>enregistreDocument</code> pour deposer plusieurs
   * documents. 
   * 
   * @return Renvoie l'identificateur du document "racine" reserve.
   *    -1 si la reservation ne peut pas etre faite.
   * @see #enregistrerDocument(InputStream, String, long, String, String, int, String) enregistrerDocument()
   */
  public int reserverDocument(String categorie) {
    description.operationId = GEDDescription.OP_RESERVE;
    description.reference = StringCtrl.emptyString();
    description.name = StringCtrl.emptyString();
    description.title = "Racine pour "+description.applicationId;
    description.documentId = -1;
    description.parentDocumentId = -1;
    description.size = 0;
    description.refType = GEDDescription.REF_TYPE_ROOT;
    description.category = categorie;
    description.userId = defaultUserId;
    sourceStream = null;
    if (send())
      return description.documentId;
    else
      return -1;
  }


/* ==== Configuration plus fine des communications ==== */

  /**
   * Definie une nouvelle description de la communication.
   * 
   * @param newDescription 
   */
  public void setDescription(GEDDescription newDescription) {
    this.description = newDescription;
  }

  /**
   * Definie un nouveau code d'utilisateur pour les
   * documents qui seront ensuite enregistres.
   * Utiliser cette methode si le code d'utilisateur
   * est different de celui donnees a la creation du client.
   * 
   * @param newUserId 
   */
  public void setUserId(int newUserId) {
    defaultUserId = newUserId;
  }

  /**
   * Definie si le client va automatiquement liberer les
   * ressources apres le transfert de donnees. Par exemple,
   * le fichier ouvert pour le transfert sera automatiquement
   * ferme a la fin de transfert. Les ressources ne seront pas
   * liberees dans le cas contraire. Faire l'appel a la methode
   * "reset" pour liberer les ressources explicitement.
   * 
   * @param autoRelease 
   */
  public void setAutoRelease(boolean autoRelease) {
    this.autoRelease = autoRelease;
  }
  
  /**
   * Accede a l'actuelle description d'une communication.
   */
  public GEDDescription description() {
    return description;
  }

  /**
   * Definie un nouvelle source des donnees.
   * 
   * @param newSourceStream 
   */  
  public void setSourceStream(InputStream newSourceStream) {
    this.sourceStream = newSourceStream;
  }

/* ==== Communicatrion avec le serveur ==== */

  /**
   * Envoi d'une commande vers le serveur.
   * Utiliser dans le cas d'une commande de type "put" (OP_SAVE, OP_REPLACE, OP_DELETE).
   */  
  public boolean send() {
    DataOutputStream dataOut = null;
    DataInputStream dataIn = null;
    int bytesRead, bytesTotal;
    byte bBuffer[] = new byte[StreamCtrl.BUFFER_SIZE];
    Socket connectionSocket = null;
    boolean result = false;
    
    try {
      // System.out.println();
      connectionSocket = new Socket(gedServerName, gedServerPort);
      connectionSocket.setSoTimeout(GED_CONNECTION_TIMEOUT); // timeout de 5 sec. 
      in = connectionSocket.getInputStream();
      out = connectionSocket.getOutputStream();
      dataOut = new DataOutputStream(out);
      dataIn = new DataInputStream(in);
      description.writeToStream(dataOut);
      if (description.hasData()) {
        bytesRead = 1;
        bytesTotal = 0;
        while(bytesRead > 0) {
          bytesRead = sourceStream.read(bBuffer, 0, StreamCtrl.BUFFER_SIZE);
          if (bytesRead > 0) {
            bytesTotal += bytesRead;
            dataOut.write(bBuffer, 0, bytesRead);
            if ((bytesTotal >= description.size) && (bytesRead != 0)) {
              bytesRead = 0;
            }
          }
        }
        dataOut.flush();
        if (autoRelease) StreamCtrl.forceClose(sourceStream);
      }
      description.readFromStream(dataIn);
      result = (description.status == GEDDescription.OK);
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    // Fermet toutes les communications
    StreamCtrl.forceClose(dataIn);
    StreamCtrl.forceClose(dataOut);
    StreamCtrl.forceClose(connectionSocket);
    return result;
  }

  /**
   * Envoie une commande de type "get" au serveur et recupere les
   * informations demandees. Les commandes correspondantes sont: 
   * OP_READ, OP_INSPECT, OP_EXISTS.
   */
  public boolean receive() {
    DataOutputStream dataOut = null;
    DataInputStream dataIn = null;
    Socket connectionSocket = null;
    boolean result = false;
    
    try {
      connectionSocket = new Socket(gedServerName, gedServerPort);
      connectionSocket.setSoTimeout(GED_CONNECTION_TIMEOUT); // timeout de 5 secondes
      in = connectionSocket.getInputStream();
      out = connectionSocket.getOutputStream();
      dataOut = new DataOutputStream(out);
      dataIn = new DataInputStream(in);
      description.writeToStream(dataOut);
      dataOut.flush();
      description.readFromStream(dataIn);
      // System.out.println("receive() description:\n  "+description);
      if (description.operationId == GEDDescription.OP_READ)
        sourceStream = in;
      result = (description.status == GEDDescription.OK);
    } catch(Exception ex) {
      ex.printStackTrace();
    }
    StreamCtrl.forceClose(dataIn);
    StreamCtrl.forceClose(dataOut);
    StreamCtrl.forceClose(connectionSocket);
    return result;
  }
  
  /**
   * Le code de la reponse de serveur.
   * C'est une des valeurs: GEDDescription.OK ou GEDDescription.FAIL;
   */
  public int lastResponseCode() {
    if (description == null) return -1;
    else return description.status;
  }

  /**
   * Le message de la reponse du serveur.
   * C'est la valeur qui se trouve dans le champs "message" de
   * GEDDecription.
   */
  public String lastMessage() {
    if (description == null) return StringCtrl.emptyString();
    else return description.message;
  }
  
  /**
   * Retourne le flot d'octets du document demande.
   */
  public InputStream dataStream() {
    if ((!description.isPutOperation()) && (description.status == GEDDescription.OK))
      return sourceStream;
    else
      return null;
  }
}
