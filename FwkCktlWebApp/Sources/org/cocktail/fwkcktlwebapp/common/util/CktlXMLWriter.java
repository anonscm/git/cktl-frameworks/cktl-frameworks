package org.cocktail.fwkcktlwebapp.common.util;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Definit les methodes facilitant la creation des documents XML. Elle gere
 * l'enchainement et l'identation corrects des balises XML.
 * 
 * <h3>Exemple</h3>
 * 
 * Un exemple de code pour creer un document XML&nbsp;:
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr>
 * <td><pre>
 * try {
 *   <code class="comment">// Le fichier XMl</code>
 *   String xmlFileName = "/Temp/Test.xml";
 *   CktlXMLWriter w = new CktlXMLWriter(xmlFileName);
 *   <code class="comment">// On initialise le document</code>
 *   w.startDocument();
 *   w.writeComment("Exemple d'un document XML");
 *   w.startElement("personne");
 *   w.writeElement("nom", "John");
 *   w.writeElement("prenom", "Doe");
 *   Hashtable addrType = new Hashtable();
 *   addrType.put("type", "personnelle");
 *   w.startElement("adresse", addrType);
 *   w.writeElement("ville", "La Rochelle");
 *   w.writeElement("cp", "17000");
 *   w.endElement(); <code class="comment">// "adresse"</code>
 *   w.startElement("commentaire");
 *   w.writeCharacters("C'est du bidon, ca !");
 *   w.endElement(); <code class="comment">// "commentaire"</code>
 *   w.endElement(); <code class="comment">// "personne"</code>
 *   <code class="comment">// Il est recommende de finir le document par l'appel suivant</code>
 *   w.endDocument();
 *   <code class="comment">// Terminer l'ecriture du document XML</code>
 *   w.close();
 * } catch(Exception ex) {
 *     ex.printStackTrace();
 *     System.out.println("Erreur d'ecriture d'un document XML :\n"+ex.getMessage());
 * }</pre></td></tr></table>
 * 
 * <p>Ce code produit le document suivant&nbsp;:</p>
 * 
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr>
 * <td><pre>
 * &lt;?xml version="1.0" encoding="UTF-8"?&gt;
 * 
 * &lt;!-- Exemple d'un document XML --&gt;
 * &lt;personne&gt;
 *   &lt;nom&gt;John&lt;/nom&gt;
 *   &lt;prenom>Doe&lt;/prenom&gt;
 *   &lt;adresse type="personnelle"&gt;
 *     &lt;ville&gt;La Rochelle&lt;/ville&gt;
 *     &lt;cp&gt;17000&lt;/cp&gt;
 *   &lt;/adresse&gt;
 *   &lt;commentaire&gt;
 *     C'est du bidon, ca !
 *   &lt;/commentaire&gt;
 * &lt;/personne&gt;</pre></td></tr></table>
 */
public class CktlXMLWriter {
  
  /**
   * Le jeu de caracteres utilise par defaut. La presente implementation
   * definie la valeur "UTF-8".
   */
  public static final String DEFAULT_ENCODING = "UTF-8";
  
  /**
   * La chaine de caracteres utiliser pour l'identation des elements
   * de document XML. La presente implementation definie la valeur "  "
   * (deux espaces).
   */
  public static final String DEFAUL_IDENT_CHARS = "  ";
  
  /**
   * Cet objet represente le destination ou le contenu de document est ecrit.
   */
  private BufferedWriter writer;

  /**
   * Le dictionnaire pour la gestion interne des balises XML.
   * Il contient les couples (identifiant, objet LocalXMLElement).
   */  
  private Hashtable elements;
  
  /**
   * L'identifiant de la balise XML en cours de l'ecriture.
   */
  private Object currentElementId;
  
  /**
   * Les caracteres utilises pour ajouter l'identation des elements XML.
   */
  private String identChars;
  
  /**
   * La chaine qui indique le jeu de caracteres dans le document XML.
   */
  private String encoding;
  
  /**
   * La reference vers le schema DTD associe au document XML cree.
   */
  private String dtdReference;
  
  /**
   * Le URI de XML Namespace qui sera utilisee lors de la creation
   * des balises XML. 
   */
  private String xmlnsURI;
  
  /**
   * Le nom de XML Namespace qui sera utilise lors de la creation
   * des balises XML.
   */
  private String xmlnsQName;
  
  /**
   * Reference vers un objet de la classe SpecChars. Une seule instance
   * de cette classe est utilisee.
   */
  private static SpecChars specCharsClass;
  
  /**
   * Indique si la nouvelle balise doit toujours commencer par une
   * nouvelle ligne.
   */
  private boolean useCompactMode;
  
  /**
   * Indique si les caracteres speciaux, comme les lettres accentués, doivent
   * etre rempalces par leur codes dans le document XML.
   */
  private boolean escapeSpecChars;
  
  /**
   * La liste des caracteres qui doivent etre remplaces par leur codes
   * dans le texte du document genere.
   */
  private String escapeChars;
  
  /**
   * Cree un nouveau createur XML qui va enregistre le document XML dans
   * la destination representee par <code>w</code>.
   * 
   * <p>Cette methode convertie l'objet <code>w</code> en un objet
   * <code>BufferedWriter</code>. Vous n'etes donc pas besoin de faire
   * cette conversion explicitement avant d'appeler ce constructeur.</p>
   * 
   * <p>Pour enregistrer le contenu de document dans une chaine
   * de caracteres, utlisez un objet <code>StringWtiter</code></p>.
   */
  public CktlXMLWriter(Writer w) {
     writer = new BufferedWriter(w);
     elements = new Hashtable();
     currentElementId = null;
     dtdReference = "";
     identChars = DEFAUL_IDENT_CHARS;
     encoding = DEFAULT_ENCODING;
     xmlnsURI = xmlnsQName = null;
     useCompactMode = false;
     escapeSpecChars = true;
     escapeChars = "<>";
  }
  
  /**
   * Cree un nouveau createur XML qui va enregistrer le document XML dans
   * la destination represente par le flux de sortie <code>out</code>.
   * 
   * @throws IOException
   */
  public CktlXMLWriter(OutputStream out) throws IOException {
    this(new OutputStreamWriter(out));
  }
  
  /**
   * Cree un nouveau createur XML qui va enregistrer le document XML dans
   * le fichier <code>filePath</code>.
   * 
   * @throws IOException
   */
  public CktlXMLWriter(String filePath) throws IOException {
    this(new FileWriter(filePath));
  }
  
  /**
   * Definie les caracteres qui seront utiliser pour l'identation des
   * elements de document XML. Aucune identation ne sera utilisee si
   * la chaine <code>newIdentChars</code> est vide ou <i>null</i>.
   * 
   * <p>Par defaut, les caracteres definis par la constante
   * <code>DEFAUL_IDENT_CHARS</code> sont utilises.</p>
   * 
   * <p>Cette methode doit etre appellee avant l'appel de la methode
   * <code>startDocument</code>.</p>
   * 
   * @see #DEFAUL_IDENT_CHARS
   * @see #startDocument()
   */
  public void setIdentChars(String newIdentChars) {
    if (newIdentChars == null) identChars = "";
      else identChars = newIdentChars;
  }

  /**
   * Indique si le document XML doit etre genere en mode "compact". En mode
   * compact, les elements suivants sont ignores :
   * <ul>
   * <li>les pasages a la ligne. Sinon, chaque balise est ecrite sur
   * une nouvelle ligne&nbsp;;</li>
   * <li>les indentations. La definition de la methode <code>setIdentChar</code>
   * est ignoree&nbsp;;</li>
   * <li>les commentaires. Les appels a la methode <code>writeComment</code>
   * sont ignores.</li>
   * </ul>
   * 
   * <p>Cette methode doit etre appellee avant l'appel de la methode
   * <code>startDocument</code>.</p>
   *
   * <p>En mode compact, le document XML est moin lisible, mais sont traitement
   * peut etre plus performant pour un analyseur XML.</p>
   * 
   * <p>Par defaut, le mode compact n'est pas active (<i>false</i>).</p>
   * 
   * @see #startDocument()
   */
  public void setUseCompactMode(boolean useCompactMode) {
    this.useCompactMode = useCompactMode;
  }
  
  /**
   * Definie le jeu de caracteres qui sera utilise dans le document XML cree.
   * Aucune definition sur le codage ne sera incluse dans le document si
   * le parametre <code>encoding</code> est vide ou <i>null</i>.
   * 
   * <p>Par defaut, le document utilise le jeu de caracteres defini
   * par la constante <code>DEFAULT_ENCODING</code>.</p>
   * 
   * <p>Cette methode doit etre appellee avant l'appel de la methode
   * <code>startDocument</code>.</p>
   * 
   * @see #DEFAULT_ENCODING
   * @see #startDocument()
   */
  public void setEncoding(String encoding) {
    this.encoding = StringCtrl.normalize(encoding);
  }
  
  /**
   * Definie les parametres de la reference d'un schema DTD qui sera
   * utilise pour la validation du document XML. Le parametre
   * <code>dtdXMLRoot</code> definie l'element racine du document XML,
   * est <code>dtdFileRef</code> la reference vers le fichier de schema
   * DTD. La reference generee aura la forme&nbsp;:
   * 
   * <table border=0 cellspacing=0 cellpadding=5 align="center" class="example"><tr><td><pre>
   * &lt;!DOCTYPE dtdXMLRoot SYSTEM "dtdFileRef"&gt;</pre></td></tr></table>
   * 
   * <p>La reference DTD n'est pas incluse dans le document XML, si au moins
   * un des parametres <code>dtdFileRef</code> ou <code>dtdXMLRoot</code>
   * est vide. Par defaut, aucune reference DTD n'est utilisee.
   * 
   * <p>Cette methode doit etre appellee avant l'appel de la methode
   * <code>startDocument</code>.</p>
   * 
   * @see #startDocument()
   * @see #setDTD(String)
   */
  public void setDTD(String dtdXMLRoot, String dtdFileRef) {
    dtdXMLRoot = StringCtrl.normalize(dtdXMLRoot);
    dtdFileRef = StringCtrl.normalize(dtdFileRef);
    if ((dtdXMLRoot.length() > 0) && (dtdFileRef.length() > 0))
      dtdReference = "<!DOCTYPE "+dtdXMLRoot+" SYSTEM \""+dtdFileRef+"\">";
    else
      dtdReference = "";
  }
  
  /**
   * Definie une reference DTD preformatee. Elle indique le schema DTD qui
   * sera utilise pour la validation du document XML cree.
   * 
   * <p>La reference DTD n'est pas incluse dans le document XML, si le
   * parametres <code>dtdReference</code> est vide. Par defaut,
   * aucune reference n'est utilisee.
   * 
   * <p>Cette methode doit etre appellee avant l'appel de la methode
   * <code>startDocument</code>.</p>
   * 
   * @see #startDocument()
   * @see #setDTD(String, String)
   */
  public void setDTD(String dtdReference) {
    this.dtdReference = StringCtrl.normalize(dtdReference);
  }
  
  /**
   * Definit l'espace des noms utilise dans le document XML (XML
   * Namespace). Le parametre <code>qName</code> indique le nom qui est
   * associe a la reference (unique) <code>uri</code>. Chaque balise du
   * document XML sera ensuite prefixee de "qName:". Aucune definition des
   * noms ne sera utilisee si un des parametres <code>uri</code> ou
   * <code>qName</code> est vide ou <i>null</i>.
   *  
   */
  public void setNamespace(String qName, String uri) {
    if ((StringCtrl.normalize(qName).length() > 0) &&
        (StringCtrl.normalize(uri).length() > 0))
    {
      this.xmlnsQName = qName;
      this.xmlnsURI = uri;
    } else {
      this.xmlnsURI = this.xmlnsQName = null;
    }
  }
  
  /**
   * Definit si les caracteres speciaux, comme des lettres accentues, doivent
   * etre remplaces par leur codes dans le document XML.
   * 
   * <p>Dans certains cas, les caracteres accentues ne sont pas correctement
   * affiches dans les documents generes a partir de donnees XML. Dans ce
   * cas, ils doivent etre remplaces par leur codes. Un code a une forme
   * &amp;#xC8;.</p>
   * 
   * <p>Le remplacement des caracteres ne s'applique qu'au texte du document
   * XML. Le noms des balises ou de leurs attribuits ne sont pas modifies.</p>
   * 
   * <p>Par defaut, le remplacement des caracteres est actif (<i>true</i>).</p>
   * 
   * @see #writeCharacters(String)
   * @see #writeElement(String, Dictionary, String)
   * @see #setCharsToEscape(String)
   */
  public void setEscapeSpecChars(boolean escape) {
    escapeSpecChars = escape;
  }
  
  /**
   * Definit la suite des caracteres qui doivent etre remplaces dans le
   * document XML, en plus des caracteres dont les codes depassent 128.
   * Par exemple, si <code>chars</code> est egale a "&lt;&gt;&amp;", alors
   * ces trois caracteres seront remplaces, en plus des lettres accentues.
   * 
   * <p>La valeur par defaut est "&lt;&gt;"</p>
   * 
   * @see #setEscapeSpecChars(boolean)
   */
  public void setCharsToEscape(String chars) {
    escapeChars = chars;
  }
  
  /**
   * Retourne une instance de la classe <code>SpecChars</code>. Cette
   * classe propose les constantes des symboles a utiliser dans les
   * documents XML.
   */
  public SpecChars specCharsClass() {
    if (specCharsClass == null) specCharsClass = new SpecChars();
    return specCharsClass;
  }

  /**
   * Ecrit la chaine de caracteres <code>line</code> dans la destination
   * et ajoute un passage a la ligne.
   * 
   * @throws IOException
   */
  private void writeLn(String line) throws IOException {
    if (line != null) writer.write(line);
    if (!useCompactMode) writer.newLine();
  }
  
  /**
   * Ecrit la chaine de caracteres <code>line</code> dans la destination
   * sans ajouter le passage a la nouvelle ligne.
   * 
   * @throws IOException
   */
  private void write(String line) throws IOException {
    writer.write(line);
  }
  
  /**
   * Ajoute le nom de l'element <code>name</code> dans la chaine construite
   * dans le buffeur <code>line</code>. Si l'espace des noms est defini,
   * alors un prefix "&lt;espace_noms&gt;:" est ajoute au nom de l'element. 
   */
  private StringBuffer appendElementName(StringBuffer line, String name) {
    if (xmlnsQName != null) line.append(xmlnsQName).append(":");
    return line.append(name);
  }
  
  /**
   * Prepare l'ensemble des attributs <code>attributes</code> en y ajoutant
   * les definitions supplementaires. Cette methode ajoute une definition
   * d'espace des noms si l'element en cours d'ecriture correspond a
   * l'element racine.
   */
  private Dictionary prepareAttributes(Dictionary attributes) {
    if (xmlnsQName != null) {
      if (currentElementId == null) {
        if (attributes == null) attributes = new Hashtable();
        attributes.put("xmlns:"+xmlnsQName, xmlnsURI);
      }
    }
    return attributes;
  }
  
  /**
   * Commence l'ecriture d'un document XML. Dans le cas generale, le
   * nouveau document commence par les expressions&nbsp;:
   * <table border=0 cellspacing=0 cellpadding=5 align="center" class="example"><tr><td><pre>
   * &lt;?xml version="1.0" encoding="some-encoding"?&gt;
   * &lt;!DOCTYPE xml-root-element SYSTEM "dtd-schema-file"&gt;</pre></td></tr></table>
   * 
   * <p>La definition du jeu de caracteres n'est pas inclue, s'il est defini a
   * <i>vide</i>. La definition DOCTYPE n'est pas genere si au moins un
   * des <code>xml-root-element</code> ou <code>dtd-schema-file</code> est
   * vide. Le jeu de caracteres et la reference DTD sont respectivement
   * definis par les methodes <code>setEncoding</code> et
   * <code>setDTD</code>.</p>
   * 
   * @throws IOException
   * 
   * @see #endDocument()
   * @see #setEncoding(String)
   * @see #setDTD(String, String)
   */
  public void startDocument() throws IOException {
    if (encoding.length() == 0)
      writeLn("<?xml version=\"1.0\"?>");
    else
      writeLn("<?xml version=\"1.0\" encoding=\""+encoding+"\"?>");
    if (dtdReference.length() > 0) writeLn(dtdReference);
    writeLn(null);
  }
  
  /**
   * Termine l'ecriture d'un document XML. Une exception
   * <code>IllegalStateException</code> est levee si les balises non-terminees
   * sont detectees.
   *  
   * @throws IllegalStateException
   * 
   * @see #endElement()
   */
  public void endDocument() throws IllegalStateException {
    if (currentElementId != null) {
      LocalXMLElement element = (LocalXMLElement)elements.get(currentElementId);
      throw new IllegalStateException("The element \""+
                                      element.name+"\" is started but not ended");
    }
  }
  
  /**
   * Commence l'ecriture de la balise <code>name</code>. Elle sera ferme
   * lors de l'appel ulterieure de la methode <code>endElement</code>.
   *
   * @throws IOException
   * 
   * @see #endElement()
   */
  public void startElement(String name) throws IOException {
    startElement(name, null);
  }
  
  /**
   * Commence l'ecriture de la balise <code>name</code> avec l'ensemble
   * des attributs <code>attributes</code>. Elle sera ferme
   * lors de l'appel ulterieure de la methode <code>endElement</code>.
   * Le dictionnaire des attributs contient les couples :
   * "nom d'attribut" (cle) - "valeur d'attribut" (valeur).
   * 
   * @throws IOException
   * 
   * @see #endElement()
   */
  public void startElement(String name, Dictionary attributes)
    throws IOException
  {
    LocalXMLElement element = new LocalXMLElement();
    // On prepare un nouvel element
    element.parentId = currentElementId;
    element.id = getNewId();
    element.level = getNextLevel(currentElementId);
    element.name = name;
    // ...on l'ecrit...
    attributes = prepareAttributes(attributes);
    StringBuffer line = new StringBuffer();
    line.append(getIdentForLevel(element.level)).append("<");
    appendElementName(line, element.name);
    line.append(getAttributesLine(attributes)).append(">");
    writeLn(line.toString());
    // ...et on le memorise...
    elements.put(element.id, element);
    currentElementId = element.id;
  }
  
  /**
   * Termine l'ecriture d'une balise XML precedemment cree suite
   * a l'appel d'une methode <code>startElement</code>.
   * 
   * <p>L'arborescence interne des balises XML est gere pour assurer
   * la creation correcte de document suite a plusieurs appels
   * consecutifs des methodes <code>startElement</code> et
   * <code>endElement</code>.</p>
   *  
   * @throws IOException
   * 
   * @see #startElement(String)
   * @see #startElement(String, Dictionary)
   */
  public void endElement() throws IOException {
    if (currentElementId == null) return;
    LocalXMLElement element = (LocalXMLElement)elements.get(currentElementId);
    elements.remove(element.id);
    currentElementId = element.parentId;
    StringBuffer line = new StringBuffer();
    line.append(getIdentForLevel(element.level)).append("</");
    appendElementName(line, element.name).append(">");
    writeLn(line.toString());
  }
  
  /**
   * Ecrit la definition d'une balise <code>name</code> avec le text
   * <code>chars</code> entre ses balises ouvrante et fermante.
   * 
   * @throws IOException
   * 
   * @see #writeElement(String, Dictionary, String)
   * @see #startElement(String)
   * @see #endElement()
   */
  public void writeElement(String name, String chars)
    throws IOException
  {
    writeElement(name, null, chars);
  }
  
  /**
   * Ecrit la definition d'une balise <code>name</code> avec le text
   * <code>chars</code> entre ses balises ouvrante et fermante.
   * Le parametre <code>attributes</code> contient les definitions
   * des attributs de la balise : les couples "nom d'attribut" (cle) -
   * "valeur d'attribut" (valeur).
   * 
   * @throws IOException
   * 
   * @see #writeElement(String, Dictionary, String)
   * @see #startElement(String)
   * @see #endElement()
   */
  public void writeElement(String name, Dictionary attributes, String chars)
    throws IOException
  {
    int level = getNextLevel(currentElementId);
    StringBuffer line = new StringBuffer();
    attributes = prepareAttributes(attributes);
    line.append(getIdentForLevel(level)).append("<");
    appendElementName(line, name);
    line.append(getAttributesLine(attributes)).append(">");
    if (escapeSpecChars) chars = escapeSpecChars(chars);
    line.append(chars).append("</");
    appendElementName(line, name).append(">");
    writeLn(line.toString());
  }

  /**
   * Ecrit la definition d'une balise <code>name</code> vide (une
   * balise ayant la forme <code>&lt;name.../&gt</code>).
   * Le parametre <code>attributes</code> contient les definitions
   * des attributs de la balise : les couples "nom d'attribut" (cle) -
   * "valeur d'attribut" (valeur).
   * 
   * @throws IOException
   * 
   * @see #writeElement(String, Dictionary, String)
   * @see #startElement(String)
   * @see #endElement()
   */
  public void writeElement(String name, Dictionary attributes)
    throws IOException
  {
    int level = getNextLevel(currentElementId);
    StringBuffer line = new StringBuffer();
    attributes = prepareAttributes(attributes);
    line.append(getIdentForLevel(level)).append("<");
    appendElementName(line, name);
    line.append(getAttributesLine(attributes)).append("/>");
    writeLn(line.toString());
  }

  /**
   * Ecrit le text qui sera insere entre les balises ouvrante et fermante
   * de la balise XML creee suite au dernier appel de
   * <code>startElement</code>.
   * 
   * @throws IOException
   * 
   * @see #startElement(String)
   * @see #startElement(String, Dictionary)
   */
  public void writeCharacters(String chars)
    throws IOException
  {
    int level = getNextLevel(currentElementId);
    if (escapeSpecChars) chars = escapeSpecChars(chars);
    writeLn(getIdentForLevel(level)+chars);
  }
  
  /**
   * Ecrit un commentaire dans le document XML.
   * 
   * @throws IOException
   * 
   * @see #writeString(String)
   */
  public void writeComment(String comment)
    throws IOException
  {
    if (!useCompactMode) {
      int level = getNextLevel(currentElementId);
      writeLn(getIdentForLevel(level)+"<!-- "+comment+" -->");
    }
  }
  
  /**
   * Ecrit une chaine arbitraire dans le document XML. L'utilisateur
   * est libre de choisir le format et le contenu du texte.
   *  
   * @throws IOException
   * 
   * @see #writeCharacters(String)
   * @see #writeComment(String)
   */
  public void writeString(String line) throws IOException {
    writeLn(line);
  }
  
  /**
   * Ferme le document en cours de creation. Suite a cette appel, aucune
   * autre ecriture n'est plus acceptee.
   * 
   * @throws IOException
   * 
   * @see #forceClose()
   */
  public void close() throws IOException {
    writer.flush();
    writer.close();
  }
  
  /**
   * Force la fermeture de flux de sortie. Cette methode peut etre appelle
   * suite aux problemes de creation du document XML.
   * 
   * @see #close()
   */
  public void forceClose() {
    try { close(); } catch(Throwable e) {
      try { writer.close(); } catch(Throwable e2) {}
    }
  }
  
  /**
   * Genere une chaine des definitions des attributs d'une balise.
   * Chaque couple (cle, valeur) du dictionnaire <code>attributes</code>
   * est transforme en une chaine "cle"="valeur".
   */
  private String getAttributesLine(Dictionary attributes) {
    StringBuffer line = new StringBuffer();
    if (attributes != null) {
      Enumeration keys = attributes.keys();
      Object attrName;
      while(keys.hasMoreElements()) {
        attrName = keys.nextElement();
        line.append(" ").append(attrName).append("=\"");
        line.append(attributes.get(attrName)).append("\"");
      }
    }
    return line.toString();
  }

  /**
   * Genere un nouvel identifiant pour une balise XML. Cet identifiant
   * est utilise par les methodes <code>startElement</code>. 
   */  
  private Object getNewId() {
    Object key;
    for(int i=0;true;i++) {
      key = new Integer(i);
      if(!elements.containsKey(key)) return key;
    }
  }
 
  /**
   * Renvoie le niveau d'identation pour la balise identifiee par
   * <code>elementId</code>. Le niveau est 0 si aucune balise
   * n'est indiquee.
   */
  private int getLevel(Object elementId) {
    if (elementId == null) return 0;
    LocalXMLElement element = (LocalXMLElement)elements.get(elementId);
    if (element == null) return 0;
    return element.level;
  }
 
  /**
   * Renvoie le niveau d'identation pour une balise "fils" de la
   * balise identifiee par <code>elementId</code>. Le niveau de chaque
   * fils est incremente. Le niveau est 0 si aucune balise n'est indiquee.
   */
  private int getNextLevel(Object elementId) {
    if (elementId != null)
      return getLevel(elementId)+1;
    else
      return 0;
  }
  
  /**
   * Convertie le niveau d'identation en caracteres d'identation.
   * Par defaut, on utilise les caracteres definis par la constante
   * DEFAUL_IDENT_CHARS. Les caracteres d'identation peuvent etre changes
   * a l'aide de la methode <code>setIdentChars</code>.
   * 
   * @see #DEFAUL_IDENT_CHARS
   * @see #setIdentChars(String)  
   */
  private String getIdentForLevel(int level) {
    if (useCompactMode) {
      return "";
    } else {
      StringBuffer indents = new StringBuffer(level*2);
      for(int i=0; i<level; i++) indents.append(identChars);
      return indents.toString();
    }
  }
  
  /**
   * Retourne une nouvelle chaine de caracteres dans laquelle tous les
   * caracteres speciaux de la <code>chaine</code> sont remplaces par leurs
   * codes. Les caracteres speciaux sont remplaces suivant la table des caracteres
   * et des codes donnee dans la classe <code>SpecChars</code>.
   * 
   * @see CktlXMLWriter.SpecChars#chars
   * @see CktlXMLWriter.SpecChars#codes
   */
  private String escapeSpecChars(String chaine) {
    if (chaine == null) return null;
    StringBuffer sb = new StringBuffer();
    char c;
    boolean mustEscape;
    for(int i=0; i<chaine.length(); i++) {
      c = chaine.charAt(i);
      mustEscape = (c > '\u007F');
      if ((!mustEscape) && (escapeChars != null))
        mustEscape = (escapeChars.indexOf(c) >= 0);
      if (mustEscape)
        sb.append("&#").append(Integer.toString(chaine.charAt(i))).append(';');
      else
        sb.append((char)chaine.charAt(i));
    }
    return sb.toString();
  }
  
//  /**
//   * Remplace dans la <code>chaine</code> toutes les chaines de caracteres
//   * <code>what</code> par <code>byWhat</code>.
//   */
//  private String replace(String chaine, String what, String byWhat) {
//    int e;
//    int s = 0;
//    StringBuffer newText = new StringBuffer();
//    while ((e = chaine.indexOf(what, s)) >= 0) {
//      newText.append(chaine.substring(s, e));
//      newText.append(byWhat);
//      s = e + what.length();
//    }
//    newText.append(chaine.substring(s));
//    return newText.toString();
//  }
  
  /**
   * Cette classe permet de gerer l'arborescence des balises XML en cours
   * d'ecriture. 
   */
  private class LocalXMLElement {
    /**
     * Le nom de la balise.
     */
    public String name;
    
    /**
     * L'identifiant interne de la balise.  
     */
    public Object id;
    
    /**
     * L'identifiant de la balise pere, si elle existe. Les balise
     * du premier niveau n'ont pas des balises "pere" (racine).
     * 
     * <p>Le document XML correcte ne contient qu'une seule balise racine.
     * Dans l'arborescence correcte des balise, il n'existe donc qu'un
     * seul element avec <code>parentId</code> egale a <i>null</i>.</p>
     */
    public Object parentId;
    
    /**
     * Le niveau de la balise. Chaque balise a le <i>niveau+1</i> par rapport
     * a sa balise pere. Le niveau de la balise racine est 0.
     */
    public int level;
  }
  
  /**
   * Cette classe definit des constantes pouvant etre utilisees dans un
   * document XML.
   */
  public static class SpecChars {
    /**
     * Le symbole de l'espace "insecable". Il est equivalent au symbole
     * &amp;nbsp; dans les documents HTML.  
     */
    public final static String nbsp =  "&#xA0;"; // "&#160;";
    
    /**
     * Le symbole d'euro.
     */
    public final static String euro = "&#x20AC;";
    
    /**
     * Le symbole de passage a la ligne. Il est equivalent a la balise
     * &lt;br&gt; dans les documents HTML.
     */
    public final static String br = "&#x2028;";
    
    /**
     * Le symbile "ampresand" (&amp;). 
     */
    public final static String amp = "&#x26;";
    
    /**
     * Le symbole "inferieur" (&lt;).
     */
    public final static String lt = "&#x3C;";
    
    /**
     * Le symbole "superieur" (&gt;).
     */
    public final static String gt = "&#x3E;";
    
    /**
     * Le symbole de guillements (&quot;).
     */
    public final static String quot = "&#x22;"; 
    
    /**
     * Le symbole de l'antislash (\).
     */
    public final static String backslash = "&#x2216;";
    
    /**
     * La table des caracteres speciaux. Cette table contient entre autres, les
     * lettres accentues.
     */
    public final static String[] chars =
    {
      "À", "à", "Á", "á", "Â", "â",
      "Ç", "ç",
      "È", "è", "É", "é", "Ê", "ê",
      "Î", "î", "Ï", "ï",
      "Ô", "ô",
      "Ù", "ù", "Û", "û", "Ü", "ü"
    };
    
    /**
     * La table des des codes des caracteres speciaux. Ils correspondent
     * aux caracteres donnes dans la table <code>chars</code>. 
     */
    public final static String[] codes =
     {
       "&#xC0;", "&#xE0;", "&#xC1;", "&#xE1;", "&#xC2;", "&#xE2;",
       "&#xC7;", "&#xE7;",
       "&#xC8;", "&#xE8;", "&#xC9;", "&#xE9;", "&#xCA;", "&#xEA;",
       "&#xCE;", "&#xEE;", "&#xCF;", "&#xEF;",
       "&#xD4;", "&#xF4;",
       "&#xD9;", "&#xF9;", "&#xDB;", "&#xFB;", "&#xDC;", "&#xFC;"
     };
  }
}
