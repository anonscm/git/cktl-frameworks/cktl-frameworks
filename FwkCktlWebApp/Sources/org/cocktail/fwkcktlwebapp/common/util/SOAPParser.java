package org.cocktail.fwkcktlwebapp.common.util;


import java.io.StringReader;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Vector;

import org.apache.xerces.parsers.SAXParser;
import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Effetue l'analyse d'une reponse SOAP - un document XML. Le resultat de
 * l'analyse est disponible dans un dictionnaire accessible via la methode
 * <code>result</code>.
 * 
 * <p>Uniquement les valeurs corrspondant aux balises donnees dans un vecteur
 * seront momorisees lors de l'analyse de la reponse.</p>
 * 
 * <p>Une reponse SOAP peut egalement etre analysee a l'aide d'une methode
 * statique <code>parseDocumentWithArguments</code>.</p>
 */
public class SOAPParser extends DefaultHandler {
  /**
   * Le resultat de l'analyse : les couples balises-valeurs.
   */
  protected Hashtable result = new Hashtable();
  
  /**
   * La liste des balises qui doivent etre reconnues. 
   */
  protected Vector acceptedTags;
  
  /**
   * La balise en cours d'analyse.
   */
  protected String currentTag;
  
  /**
   * Cree un nouvel objet d'analyse. Ce constructeur ne devrait pas etre
   * utilise, car il creer la liste vide des balises a reconnaitre (aucune
   * balise ne sera reconnue).
   * 
   * @see #parseDocumentWithArguments(String, Vector)
   */
  public SOAPParser() {
    this(new Vector());
  }

  /**
   * Cree un nouvel objet d'analyse d'une reponse SOAP. Le parametre
   * <code>tags</code> indique la liste des balises dont les valeurs doivent
   * etre memorisees.
   * 
   * @see #parseDocumentWithArguments(String, Vector)
   */
  public SOAPParser(Vector tags) { 
    this.result = new Hashtable();
    this.acceptedTags = tags;
  }
  
  /**
   * Retourne le resultat de l'analyse : les couples des balises et des valeurs
   * corrspondantes trouvees. Si aucune valeur n'est associee a un balise
   * demande, elle ne sera pas memorisee dans le resultat de traitement.
   * 
   * @see #parseDocumentWithArguments(String, Vector)
   */
  public Hashtable results() { 
    return result; 
  }
  
  /**
   * Effetue l'analyse du document XML <code>xmlDocument</code> et memorisant
   * les valeurs des balises de la liste <code>tags</code>.
   * 
   * <p>Le resultat comporte le dictionnaire des couples balise-valeur. Si
   * aucune valeur n'est associe a une balise de la liste <code>tags</code>,
   * elle n'est pas memorisee dans le resultat.</p>
   */
  public static Hashtable parseDocumentWithArguments(String xmlDocument, Vector tags) {
    SOAPParser parser = new SOAPParser(tags);
    try {
      parser.parseDocument(xmlDocument);
    } catch(Exception e) {
      e.printStackTrace();
    }
    return parser.results();
  } 
  
  /**
   * Declanche l'analyse du document XML <code>xmlDocument</code>.
   * 
   * @see #parseDocumentWithArguments(String, Vector)
   */
  public void parseDocument(String xmlDocument) throws Exception {
    SAXParser parser = new SAXParser();            
    InputSource source  = new InputSource(new StringReader(xmlDocument));
    
    parser.setContentHandler(this);
    parser.setFeature("http://xml.org/sax/features/namespaces", true);
    parser.setFeature("http://xml.org/sax/features/namespace-prefixes", true);
    parser.setFeature("http://apache.org/xml/features/validation/schema", true);
    parser.parse(source);
  }
      
  /**
   * Notifie l'analyseur qu'une nouvelle suite des donnees est detecte
   * dans le document XML.
   */
  public void characters(char[] ch, int start, int len) {
    for(Enumeration e = acceptedTags.elements(); e.hasMoreElements();) {
      if (((String)e.nextElement()).equals(currentTag)) {
        result.put(currentTag, new String(ch, start, len));
      }
    }
  }

  /**
   * Notifie l'analyseur que le debut de la balise une <code>localName</code>
   * est detecte.
   */
  public void startElement(String namespaceURI, String localName,
                           String qName, Attributes atts)
  {
    currentTag = localName;
  }

  /**
   * Notifie l'analyserur que la fin de la balise <code>localName</code>
   * est detectee.
   */
  public void endElement(String uri, String localName, String qName) {
    currentTag = null;
  }
}
