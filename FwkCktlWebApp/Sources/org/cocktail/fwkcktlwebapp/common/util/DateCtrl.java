/*
 * Copyright 2002-2004 CRI, Universite de La Rochelle
 *
 * This file is part of CRIUtil package.
 *
 * CRIUtil is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * CRIUtil is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Foobar; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
package org.cocktail.fwkcktlwebapp.common.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TimeZone;

import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Propose les methodes de manipulation des dates webobjects. Toutes les methodes de la classe sont statiques.
 */
public class DateCtrl extends JavaDateCtrl {

	/**
	 * Le separateur par defaut dans une expression de type <i>date</i>. La presente implementation utilise le separateur "/".
	 */
	public static final String DATE_SEPARATOR = "/";

	/**
	 * Le separateur par defaut dans une expression de type <i>heures</i>. La presente implementation utilise le separateur ":".
	 */
	public static final String TIME_SEPARATOR = ":";

	/**
	 * Le format par defaut pour une expression de <i>date</i>. La presente implementation utilise le format "%d/%m/%Y" (jour/mois/année).
	 */
	public static final String DEFAULT_FORMAT = "%d/%m/%Y";

	// la liste des jours feries supplementaires 
	private static NSArray additionalHolidayStrList;
	// la liste des jours feries dont il ne faut pas tenir compte 
	private static NSArray ignoringHolidayStrList;

	/**
	 * Le nombre de millisecondes par jour
	 */
	public static final long MILLISECONDE_PAR_JOUR = 24 * 60 * 60 * 1000;

	private static String JOURS = "jours";
	private static String SEMAINES = "semaines";
	private static String MOIS = "mois";

	/** initialisation si la classe est appelee "Application-less" */
	static {
		initStaticFields(null, null);
	}

	/**
	 * Cette methode est appelee au demarrage de l'application, elle permet d'ajuster la liste des jours feries donnes par la methode
	 * {@link #holidaysFR(int)}.
	 */
	public static void initStaticFields(String anAdditionalHolidayStrListStr, String anIgnoringHolidayStrListStr) {
		if (!StringCtrl.isEmpty(anAdditionalHolidayStrListStr)) {
			additionalHolidayStrList = NSArray.componentsSeparatedByString(anAdditionalHolidayStrListStr.trim(), ",");
		}
		else {
			additionalHolidayStrList = new NSArray();
		}
		if (!StringCtrl.isEmpty(anIgnoringHolidayStrListStr)) {
			ignoringHolidayStrList = NSArray.componentsSeparatedByString(anIgnoringHolidayStrListStr.trim(), ",");
		}
		else {
			ignoringHolidayStrList = new NSArray();
		}
	}

	/**
	 * <i>Il n'est pas necessaire de creer un objet de la classe <code>DateCtrl</code>, car toutes ses methodes sont statiques !</i>
	 */
	public DateCtrl() {
		// Ce constructeur est laisse juste pour pouvoir
		// ajouter un commentaire JavaDoc.
	}

	/**
	 * Complete une expression de la date. Le separateur et le format de la date comlete sont ceux par defaut : jour/mois/annee.
	 * <p>
	 * Les regles de la completion (x est la valeur donnee, <i>y</i> cette de la date completee) :
	 * <ul>
	 * <li>tous les separateurs sont ellimines. Par exemple, xx/xx est transforme en <i>xxxx</i> ;</li>
	 * <li><i>vide</i> - <i>yy/yy/yyyy</i> (y - jour, moi, annee en cours);</li>
	 * <li><i>x</i> - <i>0x/yy/yyyy</i> (y - moi, annee en cours);</li>
	 * <li><i>xx</i> - <i>xx/yy/yyyy</i> (y - moi, annee en cours);</li>
	 * <li><i>xxx</i> - <i>xx/0x/yyyy</i> (y - annee en cours);</li>
	 * <li><i>xxxx</i> - <i>xx/xx/yyyy</i> (y - annee en cours);</li>
	 * <li><i>xxxxx</i> - <i>xx/xx/yyyx</i> (y - annee en cours);</li>
	 * <li><i>xxxxxx</i> - <i>xx/xx/yyxx</i> (y - annee en cours);</li>
	 * <li><i>xxxxxxx</i> - <i>xx/xx/yxxx</i> (y - annee en cours);</li>
	 * <li><i>xxxxxxxx</i> (ou une valeur plus grande) - <i>xx/xx/xxxx</i> (y - annee en cours).</li>
	 * </ul>
	 * </p>
	 * <p>
	 * La chaine vide est retournee si la valeur de la date est incorrecte.
	 * </p>
	 */
	public static String dateCompletion(String uneDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		String maChaine, retour;

		String annee = String.valueOf(calendar.get(Calendar.YEAR));
		// On met le mois sur 2 caracteres dans tous les cas
		String mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
		if (mois.length() <= 1)
			mois = "0" + mois;
		String jour = String.valueOf(calendar.get(Calendar.DAY_OF_MONTH));
		if (jour.length() <= 1)
			jour = "0" + jour;

		// dd/mm/yyyy -> ddmmyyyy
		maChaine = "";
		if (uneDate != null) {
			StringTokenizer st = new StringTokenizer(uneDate, DATE_SEPARATOR);
			while (st.hasMoreTokens())
				maChaine += st.nextToken();
		}

		switch (maChaine.length()) {
		case 0:
			break;
		case 1:
			jour = "0" + maChaine;
			break;
		case 2:
			jour = maChaine;
			break;
		case 3:
			jour = maChaine.substring(0, 2);
			mois = "0" + maChaine.substring(2);
			break;
		case 4:
			jour = maChaine.substring(0, 2);
			mois = maChaine.substring(2);
			break;
		case 5:
			jour = maChaine.substring(0, 2);
			mois = maChaine.substring(2).substring(0, 2);
			annee = annee.substring(0, 3) + maChaine.substring(4);
			break;

		case 6:
			jour = maChaine.substring(0, 2);
			mois = maChaine.substring(2).substring(0, 2);
			annee = annee.substring(0, 2) + maChaine.substring(4);
			break;

		case 7:
			jour = maChaine.substring(0, 2);
			mois = maChaine.substring(2).substring(0, 2);
			annee = annee.substring(0, 1) + maChaine.substring(4);
			break;

		default:
			jour = maChaine.substring(0, 2);
			mois = maChaine.substring(2).substring(0, 2);
			annee = maChaine.substring(4).substring(0, 4);
		}
		retour = jour + DATE_SEPARATOR + mois + DATE_SEPARATOR + annee;

		if (!isValid(retour))
			retour = "";
		return retour;
	}

	/**
	 * Teste la validite de la date vis a vis du format par defaut.
	 * <p>
	 * Dans certains cas, les methodes de convertion des dates de WebObjects permet de traiter les dates incorrectes comme etant correctes. Par
	 * exemple, les jours "de trop" d'un mois sont reportes dans le mois suivant. Cette methode permet de traiter la validite d'une date en evitant ce
	 * type de formation.
	 * </p>
	 * 
	 * @see #DEFAULT_FORMAT
	 */
	public static boolean isValid(String dateString) {
		return dateToString(stringToDate(dateString)).equals(dateString);
	}

	/**
	 * Teste si deux dates representent le meme moment.
	 */
	public static boolean isSameDay(NSTimestamp date1, NSTimestamp date2) {
		return dateToString(date1).equals(dateToString(date2));
	}

	/**
	 * Teste si la date <i>date1</i> precede ou est egale a la date <i>date2</i>.
	 */
	public static boolean isBeforeEq(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		return (date1.getTime() <= date2.getTime());
	}

	/**
	 * Teste si la date <i>date1</i> precede strictement la date <i>date2</i>.
	 */
	public static boolean isBefore(NSTimestamp date1, NSTimestamp date2) {
		// WO4.5.x != WO5.x 
		return (date1.getTime() < date2.getTime());
	}

	/**
	 * Teste si la date <i>date1</i> succede ou est egale a la date la <i>date2</i>.
	 */
	public static boolean isAfterEq(NSTimestamp date1, NSTimestamp date2) {
		return isBeforeEq(date2, date1);
	}

	/**
	 * Teste si la date <i>date1</i> succede strictement la date <i>date2</i>.
	 */
	public static boolean isAfter(NSTimestamp date1, NSTimestamp date2) {
		return isBefore(date2, date1);
	}

	/**
	 * Teste si une date est comprise entre deux autres
	 * 
	 * @param dateAComparer date a comparer
	 * @param dateDeDebut date de debut
	 * @param dateDeFin date de fin
	 * @return boolean
	 */
	public static boolean isBetweenEq(Date dateAComparer, Date dateDeDebut, Date dateDeFin) {

		if (dateAComparer == null || dateDeDebut == null || dateDeFin == null) {
			return false;
		}
		return ((dateAComparer.before(dateDeFin) && dateAComparer.after(dateDeDebut)) || dateAComparer.equals(dateDeDebut) || dateAComparer.equals(dateDeFin));
	}

	/**
	 * Teste si une date est comprise strictement entre deux autres
	 * 
	 * @param dateAComparer date a comparer
	 * @param dateDeDebut date de debut
	 * @param dateDeFin date de fin
	 * @return boolean
	 */
	public static boolean isBetween(Date dateAComparer, Date dateDeDebut, Date dateDeFin) {

		if (dateAComparer == null || dateDeDebut == null || dateDeFin == null) {
			return false;
		}
		return (dateAComparer.before(dateDeFin) && dateAComparer.after(dateDeDebut));
	}

	/**
	 * Converti un objet date en un objet <code>String</code> suivant le format <code>dateFormat</code>. Le format doit correspondre au format accepte
	 * par <code>NSTimestampFormatter</code>.
	 * 
	 * @see #dateToString(NSTimestamp)
	 */
	public static String dateToString(NSTimestamp date, String dateFormat) {
		// WO4.5.x != WO5.x 
		String dateString;
		NSTimestampFormatter formatter = new NSTimestampFormatter(dateFormat);
		try {
			dateString = formatter.format(date);
		} catch (Exception ex) {
			dateString = "";
		}
		return dateString;
	}

	/**
	 * Converti un objet date en un objet <code>String</code>. La conversion utilise le format par defaut.
	 * 
	 * @see #dateToString(NSTimestamp, String)
	 * @see #DEFAULT_FORMAT
	 */
	public static String dateToString(NSTimestamp gregorianDate) {
		return dateToString(gregorianDate, DEFAULT_FORMAT);
	}

	/**
	 * Converti une chaine de caracteres <code>sDate</code> en un objet date. Le format de la date <code>sDate</code> doit correspondre au format
	 * <code>dateFormat</code>. Les format possibles sont definis dans la classe <code>NSTimestampFormatter</code>.
	 * <p>
	 * La date est convertie en utilisant le fuseau horaire <code>NSTimeZone.defaultTimeZone()</code>.
	 * </p>
	 * 
	 * @return La date ou null si la valeur <code>sDate</code> indique une date invalide.
	 * @see #stringToDate(String)
	 */
	public static NSTimestamp stringToDate(String sDate, String dateFormat) {
		// WO4.5.x != WO5.x 
		NSTimestamp date = null;
		NSTimestampFormatter formatter;
		if ((dateFormat == null) || (dateFormat.trim().length() == 0))
			return null;
		try {
			formatter = new NSTimestampFormatter(dateFormat);
			date = (NSTimestamp) formatter.parseObject(sDate);
			if (!sDate.equals(dateToString(date, dateFormat)))
				return null;
		} catch (Exception ex) {
		}
		return date;
	}

	/**
	 * Converti une chaine de caracteres <code>sDate</code> en un objet date. Le format de la date <code>sDate</code> doit correspondre au format par
	 * defaut.
	 * <p>
	 * La date est convertie en utilisant le fuseau horaire <code>NSTimeZone.defaultTimeZone()</code>.
	 * </p>
	 * 
	 * @return La date ou null si la valeur <code>sDate</code> indique une date invalide.
	 * @see #stringToDate(String, String)
	 * @see #DEFAULT_FORMAT
	 */
	public static NSTimestamp stringToDate(String dateString) {
		return stringToDate(dateString, DEFAULT_FORMAT);
	}

	/**
	 * Convertie le numero du jour de la semaine de la representation anglaise vers celle francaise.
	 */
	public static int getDayOfWeek(int dayOfWeek) {
		return (dayOfWeek == 0) ? (dayOfWeek + 6) : (dayOfWeek - 1);
	}

	/**
	 * Retourne le numero de jour de la semaine correspondant a la date indiquee. Lundi est le premier jour de la semaine.
	 */
	public static int getDayOfWeek(NSTimestamp date) {
		// WO4.5.x != WO5.x 
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.setFirstDayOfWeek(GregorianCalendar.MONDAY);
		return calendar.get(GregorianCalendar.DAY_OF_WEEK);
	}

	/**
	 * Retourne la representation du moment en cour sous forme d'une chaine de caracteres. Le format de la chaine est "jj/mm/AAAA HH:MM:SS".
	 * 
	 * @see #currentDateString()
	 */
	public static String currentDateTimeString() {
		java.util.Calendar cal = new GregorianCalendar();
		StringBuffer sb = new StringBuffer(currentDateString());
		sb.append(" ");
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.HOUR_OF_DAY), 2)).append(TIME_SEPARATOR);
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MINUTE), 2)).append(TIME_SEPARATOR);
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.SECOND), 2));
		return sb.toString();
	}

	/**
	 * Retourne la representation du moment en cour sous forme d'une chaine de caracteres. Le format de la chaine est "jj/mm/AAAA".
	 * 
	 * @see #currentDateTimeString()
	 */
	public static String currentDateString() {
		java.util.Calendar cal = new GregorianCalendar();
		StringBuffer sb = new StringBuffer("");
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.DAY_OF_MONTH), 2)).append(DATE_SEPARATOR);
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MONTH) + 1, 2)).append(DATE_SEPARATOR);
		if (cal.get(GregorianCalendar.YEAR) > 100)
			sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.YEAR) % 100, 2));
		else
			sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.YEAR), 2));
		return sb.toString();
	}

	/**
	 * Retourne la representation du moment en cour sous forme d'une chaine de caracteres. Le format de la chaine est "jjmmAAAAHHmm".
	 * 
	 * @see #currentDateTimeString()
	 */
	public static String currentDateHeureString() {
		java.util.Calendar cal = new GregorianCalendar();
		StringBuffer sb = new StringBuffer("");
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.DAY_OF_MONTH), 2));
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MONTH) + 1, 2));
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.YEAR), 2));
		sb.append('_');
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.HOUR_OF_DAY), 2));
		sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MINUTE), 2));
		return sb.toString();
	}

	/**
	 * Retourne la date du moment sous forme d'une chaine de caracteres. Le format de la chaine est "jjmmmmAAAA_HH'h'mm".
	 */
	public static String currentDateHeureVerbeux() {
		java.util.Calendar cal = new GregorianCalendar();
		return (new SimpleDateFormat("ddMMMMyyyy'_'HH'h'mm").format(cal.getTime()));
	}

	/**
	 * Retourne la date representant le moment en cours dans le fuseau horaire par defaut.
	 * 
	 * @see NSTimeZone#defaultTimeZone()
	 */
	public static NSTimestamp now() {
		//    return toLocalDate(new NSTimestamp());
		return new NSTimestamp();
	}

	// les prefix des jours feries recurents
	private final static String PREFIX_01_01 = "01/01";
	private final static String PREFIX_01_05 = "01/05";
	private final static String PREFIX_08_05 = "08/05";
	private final static String PREFIX_14_07 = "14/07";
	private final static String PREFIX_15_08 = "15/08";
	private final static String PREFIX_01_11 = "01/11";
	private final static String PREFIX_11_11 = "11/11";
	private final static String PREFIX_25_12 = "25/12";

	// les identifiants des jours feries non fixes
	private final static String ID_HOLIDAY_PAQUES = "PAQUES";
	private final static String ID_HOLIDAY_ASCENSION = "ASCENSION";
	private final static String ID_HOLIDAY_PENTECOTE = "PENTECOTE";

	// le cache des jours issus de la methode holidaysFR(int)
	private static NSMutableDictionary holidaysFRDico;

	/**
	 * Retourne un tableau de tous les jours feri&eacute;s en France de l'ann&eacute;e donn&eacute;e. Cette liste contient par defaut :
	 * <ul>
	 * <li>01/01/xxxx</li>
	 * <li>01/05/xxxx</li>
	 * <li>08/05/xxxx</li>
	 * <li>14/07/xxxx</li>
	 * <li>15/08/xxxx</li>
	 * <li>01/11/xxxx</li>
	 * <li>11/11/xxxx</li>
	 * <li>25/12/xxxx</li>
	 * <li>Les Lundis de P&acirc;ques</li>
	 * <li>Les Jeudis de l'Ascension (38 jours apres Paques)</li>
	 * <li>Les Lundis de Pentec&ocirc;te (except&eacute; pour les ann&eacute;es suivantes : 2004,2005,2006,2007)</li>
	 * </ul>
	 * <br/>
	 * Cette liste peut &ecirc;tre adapt&eacute;e gr&acirc;ce 2 param&egrave;tres de configuration de l'application (fichier .config,
	 * <em>GRHUM.GRHUM.PARAMETRES</em> le cas &eacute;ch&eacute;ant) :
	 * <ul>
	 * <li><b><em>APP_DATECTRL_ADDITIONAL_HOLIDAY</em></b> : la liste des jours feri&eacute;s suppl&eacute;mentaires &agrave; retourner.</li>
	 * <li><b><em>APP_DATECTRL_IGNORING_HOLIDAY</em></b> : la liste des jours feri&eacute;s dont il faut pas tenir compte (parmi la liste
	 * donn&eacute;e plus haut)</li>
	 * </ul>
	 * <br/>
	 * Chacun de ces parametres peut contenir une liste de dates, un jour et un mois (pour les jours feries recurrents, format DD/MM) ou bien des
	 * identifiants de jour feries (pour la suppression : <b><em>PAQUES</em></b>, <b><em>ASCENSION</em></b> et <b><em>PENTECOTE</em></b>), le tout
	 * s&eacute;par&eacute; par des virgules. <br/>
	 * <br/>
	 * <u>Exemple de fichier de config :</u> <br/>
	 * <br/>
	 * <div class="example"> &lt;...&gt;<br/>
	 * <br/>
	 * #* Les jours supplementaires : le 10/12/2008 et tous les 1er fevrier<br/>
	 * APP_DATECTRL_ADDITIONAL_HOLIDAY=10/12/2008, 01/02<br/>
	 * <br/>
	 * #* Les jours feries a ignorer : tous les 1er janvier et toutes les pentecote<br/>
	 * APP_DATECTRL_IGNORING_HOLIDAY=01/01, PENTECOTE<br/>
	 * <br/>
	 * &lt;...&gt; </div>
	 */
	public static NSArray holidaysFR(int year) {
		int jour, mois, nCycleLunaire, nBissextile, nLettDimanche, nC1, nC2, nC3;

		// instancier le cache des jours feries si besoin
		if (holidaysFRDico == null) {
			holidaysFRDico = new NSMutableDictionary();
		}
		// instancier le tableau pour l'annee definit si besoin
		String yearStr = Integer.toString(year);
		NSArray holidaysFR = (NSArray) holidaysFRDico.valueForKey(yearStr);
		// ce tableau a deja ete calcule
		if (holidaysFR != null) {
			return holidaysFR;
		}

		NSTimestamp leJourTemp;
		NSMutableArray result = new NSMutableArray();

		// supprimer les dates recurentes et les dates fixes
		String strDate = "";
		strDate = PREFIX_01_01 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_01_01) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_01_05 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_01_05) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_08_05 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_08_05) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_14_07 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_14_07) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_15_08 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_15_08) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_01_11 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_01_11) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_11_11 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_11_11) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}
		strDate = PREFIX_25_12 + "/" + Integer.toString(year);
		if (!ignoringHolidayStrList.containsObject(PREFIX_25_12) && !ignoringHolidayStrList.containsObject(strDate)) {
			result.addObject(stringToDate(strDate, DEFAULT_FORMAT));
		}

		// ajouter les dates recurentes et fixes
		for (int i = 0; i < additionalHolidayStrList.count(); i++) {
			boolean isRecurrentDate = false;
			String additionalHolidayStr = (String) additionalHolidayStrList.objectAtIndex(i);
			// completer par l'annee si besoin
			if (!StringCtrl.isEmpty(additionalHolidayStr) && (additionalHolidayStr.length() == 5)) {
				isRecurrentDate = true;
				additionalHolidayStr += "/" + Integer.toString(year);
			}
			// verifier que le format est le bon
			NSTimestamp date = stringToDate(additionalHolidayStr, DEFAULT_FORMAT);
			if (date != null) {
				boolean shouldAddDate = true;
				// le format est ok, pour les dates non recurrentes, on verifie si  
				// l'annee est la meme que <code>year</code>
				if (!isRecurrentDate) {
					GregorianCalendar gcDate = new GregorianCalendar();
					gcDate.setTime(date);
					shouldAddDate = (gcDate.get(GregorianCalendar.YEAR) == year);
				}
				if (shouldAddDate) {
					result.addObject(date);
				}
			}
		}

		// calcul du lundi de paques
		if ((year == 1954) || (year == 2049)) {
			jour = 18;
			mois = 4;
		}
		else {
			if ((year == 1981) || (year == 2076)) {
				jour = 19;
				mois = 4;
			}
			else {
				nCycleLunaire = year % 19;
				nBissextile = year % 4;
				nLettDimanche = year % 7;
				nC1 = ((nCycleLunaire * 19) + 24) % 30;
				nC2 = (nBissextile * 2 + nLettDimanche * 4 + nC1 * 6 + 5) % 7;
				nC3 = nC1 + nC2;

				if (nC3 <= 9) {
					jour = 22 + nC3;
					mois = 3;
				}
				else {
					jour = nC3 - 9;
					mois = 4;
				}
			}
		}
		leJourTemp = new NSTimestamp(year, mois, jour, 0, 0, 0, NSTimeZone.defaultTimeZone());

		// Lundi de Paques
		leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 1, 0, 0, 0);
		String leJourTempStr = dateToString(leJourTemp, DEFAULT_FORMAT);
		String leJourTempSansAnneeStr = leJourTempStr.substring(0, 5);
		if (!ignoringHolidayStrList.containsObject(leJourTempStr) &&
				!ignoringHolidayStrList.containsObject(leJourTempSansAnneeStr) &&
				!ignoringHolidayStrList.containsObject(ID_HOLIDAY_PAQUES)) {
			result.addObject(leJourTemp);
		}

		// Jeudi de l'ascension : 38 jours apres le lundi de paques
		leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 38, 0, 0, 0);
		leJourTempStr = dateToString(leJourTemp, DEFAULT_FORMAT);
		leJourTempSansAnneeStr = leJourTempStr.substring(0, 5);
		if (!ignoringHolidayStrList.containsObject(leJourTempStr) &&
				!ignoringHolidayStrList.containsObject(leJourTempSansAnneeStr) &&
				!ignoringHolidayStrList.containsObject(ID_HOLIDAY_ASCENSION)) {
			result.addObject(leJourTemp);
		}

		// Lundi de Pentecote : 11 jours apres l'ascension, excepte pour les annees (2004/2005, 2005/2006, 2006/2007, 2007/2008)
		leJourTemp = leJourTemp.timestampByAddingGregorianUnits(0, 0, 11, 0, 0, 0);
		leJourTempStr = dateToString(leJourTemp, DEFAULT_FORMAT);
		leJourTempSansAnneeStr = leJourTempStr.substring(0, 5);
		if (!ignoringHolidayStrList.containsObject(leJourTempStr) &&
				!ignoringHolidayStrList.containsObject(leJourTempSansAnneeStr) &&
				!ignoringHolidayStrList.containsObject(ID_HOLIDAY_PENTECOTE)) {
			if (year < 2005 || year > 2008) {
				result.addObject(leJourTemp);
			}
		}

		// sauvegarde de ce tableau dans le cache
		holidaysFRDico.setObjectForKey(result, yearStr);

		CktlLog.rawLog("holidaysFR(" + year + ") : " + result, CktlLog.LEVEL_DEBUG);

		return result;
	}

	/**
	 * Test si le jour donnee est un jour ferie en France.
	 */
	public static boolean isHolidayFR(NSTimestamp aDate) {
		GregorianCalendar aCalendar = new GregorianCalendar();
		aCalendar.setTime(aDate);
		NSArray dates = holidaysFR(aCalendar.get(Calendar.YEAR));
		for (int i = 0; i < dates.count(); i++) {
			if (isSameDay((NSTimestamp) dates.objectAtIndex(i), aDate))
				return true;
		}
		return false;
	}

	/**
	 * Retourne le numero de la semaine correspondant a la date donnee.
	 */
	public static int weekNumber(NSTimestamp aDate) {
		GregorianCalendar aCalendar = new GregorianCalendar();
		aCalendar.setTime(aDate);
		return aCalendar.get(Calendar.WEEK_OF_YEAR);
	}

	/**
	 * @return L'année en cours.
	 */
	public static Integer getCurrentYear() {
		return Integer.valueOf(new SimpleDateFormat("yyyy").format(new Date()));
	}

	/**
	 * Nettoie une date de ses heures, minutes, secondes en gardant seulement le jou, mois, année.
	 * 
	 * @param aDate une date
	 * @return la date nettoyee
	 */
	public static final Date getDateOnly(Date aDate) {
		final GregorianCalendar gc = new GregorianCalendar();
		gc.setTime(aDate);
		gc.set(Calendar.HOUR_OF_DAY, 0);
		gc.set(Calendar.MINUTE, 0);
		gc.set(Calendar.SECOND, 0);
		gc.set(Calendar.MILLISECOND, 0);
		return gc.getTime();
	}

	/**
	 * Renvoie la durée entre 2 dates formatées en nombre de jour, de semaines ou de mois
	 * 
	 * @param dateDebut : date de début
	 * @param dateFin : date de fin
	 * @return durée formatée
	 */
	public static String calculerDureeFormatteeEnJourSemaineMois(NSTimestamp dateDebut, NSTimestamp dateFin) {
		if ((dateDebut != null) && (dateFin != null)) {
			long nbJour = (dateFin.getTime() - dateDebut.getTime()) / MILLISECONDE_PAR_JOUR;
			String dureeRenvoye = null;

			if (nbJour < 7) {
				//si durée en dessous de 7 ==> calculer en jours
				dureeRenvoye = nbJour + " " + JOURS;
			} else if (nbJour < 30) {
				//si durée entre 7 et 30 ==> calculer en semaines
				dureeRenvoye = Math.round((nbJour / 7)) + " " + SEMAINES;
			} else {
				//sinon calculer en mois
				dureeRenvoye = Math.round((nbJour / 30)) + " " + MOIS;
			}

			return dureeRenvoye;
		}

		return "";
	}

	/**
	 * @param dateDebut la date de début
	 * @param dateFin la date de fin
	 * @return le nombre de jours entre les deux dates
	 */
	public static int nbJoursEntreDates(NSTimestamp dateDebut, NSTimestamp dateFin) {
		return Days.daysBetween(new DateTime(dateDebut), new DateTime(dateFin)).getDays();
	}

	/**
	 * Retourne une date de type {@link GregorianCalendar}
	 * 
	 * @param uneDate : date (en UTC)
	 * @return Date
	 */
	public static GregorianCalendar getGregorianDate(NSTimestamp uneDate) {
		GregorianCalendar date = new GregorianCalendar();
		date.setTime(uneDate);
		return date;
	}

	/**
	 * @param dateDebut : la date de début
	 * @param dateFin : la date de fin
	 * @return le nombre de jours entre les deux dates
	 */
	public static int nbJoursEntreDates(GregorianCalendar dateDebut, GregorianCalendar dateFin) {
		return Days.daysBetween(new DateTime(dateDebut), new DateTime(dateFin)).getDays();
	}

	/**
	 * Teste si la date <i>date1</i> est après la date <i>date2</i>
	 * 
	 * @param date1 : date 1 {@link GregorianCalendar}
	 * @param date2 : date 2 {@link GregorianCalendar}
	 * @return Vrai/Faux
	 */
	public static boolean isAfter(GregorianCalendar date1, GregorianCalendar date2) {
		return date1.after(date2);
	}

	/**
	 * Teste si la date <i>date1</i> est avant la date <i>date2</i>.
	 * 
	 * @param date1 : date 1 {@link GregorianCalendar}
	 * @param date2 : date 2 {@link GregorianCalendar}
	 * @return Vrai/Faux
	 */
	public static boolean isBefore(GregorianCalendar date1, GregorianCalendar date2) {
		return date1.before(date2);
	}

	public static final DateFormat DATE_FORMAT_SHORT = new SimpleDateFormat("dd/MM/yyyy");
	public static final DateFormat DATE_FORMAT_YYYY = new SimpleDateFormat("yyyy");

	/**
	 * @param date
	 * @return num&eacute;ro de l'ann&eacute;e
	 */
	public static int getYear(Date date) {
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.YEAR));
	}

	/**
	 * @param date
	 * @return num&eacute;ro du mois (0 si janvier)
	 */
	public static int getMonth(Date date) {
		java.util.Calendar cal = new GregorianCalendar();
		cal.setTime(date);
		return (cal.get(GregorianCalendar.MONTH));
	}

	/**
	 * Formatte une date avec un DateFormat (java)
	 * 
	 * @param date
	 * @param format
	 * @return La date formattée.
	 */
	public static String formatDate(Date date, DateFormat format) {
		return format.format(date);
	}

	/**
	 * @param date : date à formatter
	 * @return La date formattée avec DATE_FORMAT_SHORT
	 */
	public static String formatDateShort(Date date) {
		if (date != null) {
			return DATE_FORMAT_SHORT.format(date);
		}
		return "";
	}

	/**
	 * @param exercice
	 * @return Une liste d'objets Date (les 1ers jours de chaque mois de l'année)
	 */
	public static ArrayList getFirstDaysOfMonth(final int exercice) {
		ArrayList list = new ArrayList(12);
		for (int i = 0; i < 12; i++) {
			list.add(new GregorianCalendar(exercice, i, 1).getTime());
		}
		return list;
	}

	public static List<Date> getLastDaysOfMonth(final int exercice) {
		LinkedList<Date> list = new LinkedList<Date>();
		for (int i = 1; i <= 12; i++) {
			GregorianCalendar cal = new GregorianCalendar(exercice, i, 1);
			cal.add(Calendar.DAY_OF_MONTH, -1);
			list.add(cal.getTime());
		}
		return list;
	}

	public static Date getFirstDayOfYear(final Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		return cal.getTime();
	}

	public static Date getLastDayOfYear(final Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.MONTH, 11);
		return cal.getTime();
	}

	public static Date getFirstDayOfMonth(final Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		return cal.getTime();
	}

	public static Date getLastDayOfMonth(final Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		Date tmp = getFirstDayOfMonth(cal.getTime());
		cal = new GregorianCalendar();
		cal.setTime(tmp);
		cal.add(Calendar.DAY_OF_MONTH, -1);
		return cal.getTime();
	}

	/**
	 * Determine la date de la fin de l'année universitaire pour une date donnée. Par exemple : - le 01/09/2011 00h00 pour une date donnée au
	 * 01/12/2010. - le 01/09/2010 00h00 pour une date donnée au 01/06/2010.
	 * 
	 * @param date : date sur laquelle se base la determination
	 * @return la date de fin d'année universitaire par rapport à la date donnée
	 */
	public static Date getEndDateOfUniversityYear(final Date date) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.setTime(date);
		// 1er septembre de l'année de la date donnée
		cal.clear(Calendar.MINUTE);
		cal.clear(Calendar.SECOND);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MONTH, Calendar.SEPTEMBER);
		cal.setTime(getFirstDayOfMonth(cal.getTime()));

		NSTimestamp endDate = new NSTimestamp(cal.getTime());
		if (isAfterEq(new NSTimestamp(date), endDate)) {
			// Si la date donnée a déjà entammé l'année universitaire alors on ajoute 1 an au 1er septembre.
			endDate = endDate.timestampByAddingGregorianUnits(1, 0, 0, 0, 0, 0);
		}
		return endDate;
	}

	public static Date getLastDayOfYear(final int exercice) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(Calendar.DAY_OF_MONTH, 31);
		cal.set(Calendar.MONTH, 11);
		cal.set(Calendar.YEAR, exercice);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		return cal.getTime();
	}

	public static Date getFirstDayOfYear(final int exercice) {
		GregorianCalendar cal = new GregorianCalendar();
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.MONTH, 0);
		cal.set(Calendar.YEAR, exercice);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		return cal.getTime();
	}

	/**
	 * Ajoute des jours, heures, minutes, secondes à une date.
	 * 
	 * @param aDate La date de depart
	 * @param aDay Nombre de jours à rajouter
	 * @param hours Nombre d'heures à rajouter
	 * @param minutes Nombre de minutes à rajouter
	 * @param seconds Nombre de secondes à rajouter
	 * @return La nouvelle date
	 */
	public static Date addDHMS(Date aDate, int aDay, int hours, int minutes, int seconds) {
		if (aDate == null) {
			return null;
		}
		GregorianCalendar myCalendar = new GregorianCalendar();
		myCalendar.setTime(aDate);
		myCalendar.add(Calendar.DAY_OF_MONTH, aDay);
		myCalendar.add(Calendar.HOUR, hours);
		myCalendar.add(Calendar.MINUTE, minutes);
		myCalendar.add(Calendar.SECOND, seconds);
		return myCalendar.getTime();
	}

	/**
	 * @return la date du jour (sans heures, minutes, secondes, ...).
	 */
	public static NSTimestamp getDateJour() {
		return new NSTimestamp(getDateOnly(new Date()));
	}

	public static String getAnnee(Date date) {
		return DATE_FORMAT_YYYY.format(date);
	}

	//	DATE COMPLETION : un string en entree est transforme en date; Si retour "" -> ERREUR , sinon renvoi de la date formattee avec des /
	public static String dateCompletion2(String dateString)
	{
		GregorianCalendar calendar = new GregorianCalendar(TimeZone.getTimeZone("Europe/Paris"));
		NSTimestamp dateJour = new NSTimestamp();
		calendar.setTime(dateJour);
		int i;
		String chiffres = "", resultat = "";
		String jour = "", mois = "", annee = "";

		for (i = 0; i < dateString.length(); i++)
		{
			if (NumberCtrl.estUnChiffre("" + dateString.charAt(i)))
				chiffres = chiffres + dateString.charAt(i);
		}
		// chiffres : String contenant tous les chiffres de la chaine entree
		// On regarde maintenant la taille de cette chaine
		// Si 1 ou 2 : on complete par le mois et l'annee en cours; Si 3 : on regarde si le mois ou le jour sur 1 caractere;

		// Cas de 1 chiffre : 1 journee, completee par le mois en cours et l'annee en cours
		if ((chiffres.length() == 1))
		{
			jour = chiffres;
			mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
		}

		// Cas de 2 chiffres : 1 journee, completee par le mois en cours et l'annee en cours
		if ((chiffres.length() == 2))
		{
			jour = chiffres;
			mois = String.valueOf(calendar.get(Calendar.MONTH) + 1);
			annee = String.valueOf(calendar.get(Calendar.YEAR));
			resultat = formatteNo(jour) + "/" + formatteNo(mois) + "/" + formatterAnnee(annee);

			if (!isValid(resultat)) // On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
			}
		}

		// Cas de 3 chiffres : 1 jour et 1 mois , reste a determiner
		if ((chiffres.length() == 3))
		{
			// L'annee est l'annee en cours
			annee = String.valueOf(calendar.get(Calendar.YEAR));

			mois = chiffres.substring(2).substring(0, 1); // mois sur 1 caractere
			jour = chiffres.substring(0, 2);
			resultat = formatteNo(jour) + "/" + formatteNo(mois) + "/" + formatterAnnee(annee);

			if (!isValid(resultat)) // On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 2);
			}
		}

		// Cas de 4 chiffres : 1 jour et 1 mois, ou 1 jour, 1 mois et une annee
		if ((chiffres.length() == 4))
		{
			jour = chiffres.substring(0, 2);
			mois = chiffres.substring(2).substring(0, 2);
			annee = String.valueOf(calendar.get(Calendar.YEAR));

			resultat = formatteNo(jour) + "/" + formatteNo(mois) + "/" + formatterAnnee(annee);

			if (!isValid(resultat))
			{
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
				annee = chiffres.substring(2).substring(0, 2);
			}
		}

		// Cas de 5 chiffres : 1 annee sur 2 caracteres
		if ((chiffres.length() == 5))
		{
			// On regarde si les 4 derniers chiffres correspondent a une annee
			annee = chiffres.substring(3).substring(0, 2);

			// Reste 3 caracteres pour le mois et le jour
			mois = chiffres.substring(2).substring(0, 1); // mois sur 1 caractere
			jour = chiffres.substring(0, 2);
			resultat = formatteNo(jour) + "/" + formatteNo(mois) + "/" + formatterAnnee(annee);

			if (!isValid(resultat)) // On teste si la date est valide avec le mois sur 1 caractere
			{
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 2);
			}
		}

		// Cas de 6 chiffres : 2 pour le jour, le mois et 2 pour l'annee, ou 1 pour le jour, 1 pour le mois et 4 pour l'annee
		if ((chiffres.length() == 6))
		{
			// On regarde si les 4 derniers chiffres correspondent a une annee
			String localAnnee = chiffres.substring(2).substring(0, 4);

			if (((Number) new Integer(localAnnee)).intValue() > 1900) // L'annee est sur 4 chiffres
			{
				jour = chiffres.substring(0, 1);
				mois = chiffres.substring(1).substring(0, 1);
				annee = chiffres.substring(2).substring(0, 4);
			}
			else // L'annee est sur 2 chiffres
			{
				jour = chiffres.substring(0, 2);
				mois = chiffres.substring(2).substring(0, 2);
				annee = chiffres.substring(4).substring(0, 2);
			}
		}

		// Cas de 7 chiffres : 4 chiffres pour l'annee - A choisir 2 pour le jour ou 2 pour le mois selon la saisie ...
		if ((chiffres.length() == 7))
		{
			annee = chiffres.substring(3).substring(0, 4);

			// On teste si le mois est valide sur 1 ou 2 caracteres
			mois = chiffres.substring(1).substring(0, 2); // mois sur 2 caracteres
			if (((Number) new Integer(mois)).intValue() <= 12) // mois valide
				jour = chiffres.substring(0, 1);
			else
			{
				jour = chiffres.substring(0, 2);
				mois = chiffres.substring(2).substring(0, 1);
			}
		}

		// Cas de 8 chiffres : toute la date est donnee, on verifie si le mois, le jour et l'annee sont correts
		if ((chiffres.length() == 8)) {
			jour = chiffres.substring(0, 2);
			mois = chiffres.substring(2).substring(0, 2);
			annee = chiffres.substring(4).substring(0, 4);
			;
		}

		resultat = formatteNo(jour) + "/" + formatteNo(mois) + "/" + formatterAnnee(annee);

		if (!isValid(resultat))
			return "";
		return resultat;
	}

	/** GET MOIS : renvoie le numero formatte sur 2 caracteres */
	public static String formatteNo(String numero)
	{
		if (numero.length() == 1)
			return "0" + numero;

		return "" + numero;
	}

	public static String formatterAnnee(String annee)
	{
		if (annee.length() == 2) {
			if (((Number) new Integer(annee)).intValue() < 30) // valeur à changer en avançant dans le temps
				return "20" + annee;
			else
				return "19" + annee;
		}

		return annee;
	}
}