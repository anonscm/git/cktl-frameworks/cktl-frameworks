package org.cocktail.fwkcktlwebapp.common.util;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Facilite la manipulations des fichiers et des nom des fichiers. Elle ne
 * contient que des methodes statiques et aucune instance de cette classe
 * ne doit pas etre creee.
 */
public class FileCtrl {
  /**
   * Le symbole de separateur des fichiers. Dans cette implementation
   * c'est le symbole "/".
   */
  public static final String SEPARATOR = "/";
  
  /**
   * <i>Il n'est pas necessaire de creer un objet de la classe
   * <code>FileCtrl</code>, car toutes ses methodes sont statiques !</i>
   */
  public FileCtrl() {
    // Ce constructeur est laisse juste pour pouvoir
    // ajouter un commentaire JavaDoc.
  }
  
  /**
   * Teste si le fichier ou le repertoire existe.
   * <br>C'est un racourci de l'appel :
   * <div align="center"><code>(new File(path)).exists()</code></div>
   */
  public static boolean existsFile(String path) {
    try {
      return (new File(path)).exists();
    } catch(Throwable e) {
      e.printStackTrace();
      return false;
    }
  }

  /**
   * Supprime le fichier ou le repertoire.
   * 
   * @param filePath Le chemin (absolu) du fichier a supprimer.
   * @return Retourne <i>true</i> si l'operation s'est deroulee
   *   avec succes.
   * @see #deleteDir(String, boolean) deleteDir()
   */
  public static boolean deleteFile(String filePath) {
    try {
      return (new File(filePath)).delete();
    } catch(Throwable ex) {
      ex.printStackTrace();
      return false;
    }
  }

  /**
   * Supprime tous les fichiers qui correspond au schema
   * <code>filesPattern</code>. Si le schema definit un chemin complet,
   * tous les fichier d'un repertoire indique par ce chemin seront
   * supprimes.
   * 
   * <p>Par exemnpe, si le schema est <code>/var/tmp/*.zip</code>, alors
   * tous les fichiers avec l'extension ".zip" seront supprimes dans
   * le repertoire <code>/var/tmp</code>. Atention, cette methode n'effetue
   * par de ls suppression recursive. Les fichier ".zip" ne seront pas
   * supprimes dans le sous-repertoires du <code>/var/tmp</code>.</p>
   * 
   * <p>Retourne <i>true</i> si la suppression peut etre faite, et
   * <i>false</i> dans le cas contraire.</p>
   */
  public static boolean deleteFiles(String filesPattern) {
    boolean noErrors = true;
    try {
      String[] files = listDir(getParentDirectory(filesPattern),
                               getFileName(filesPattern), true);
      if (files != null) {
        for(int i=0; i<files.length; i++) {
          if (!deleteFile(files[i])) noErrors = false;
        }
      }
    } catch(Throwable ex) {
      ex.printStackTrace();
      noErrors = false;
    }
    return noErrors;
  }

  /**
   * Supprime le repertoire. Si le parametre <code>recursive</code>
   * est <i>true</i>, supprime les fichier et les sous repertoires.
   * La suppression echoue si le chemin n'indique pas de repertoire
   * ou l'utilisateur n'as pas les droits requises pour la
   * suppression.  
   * 
   * @param dirPath Le chemin (absolu) du repertoire a supprimer.
   * @param recursive <i>true</i> s'il faut supprimer les sous
   *   repertoires, <i>false</i> s'il faut ignorer l'operation.
   * @return Retourne <i>true</i> si l'operation s'est deroulee
   *   avec succes.
   * 
   * @see #deleteFile(String) deleteFile()
   * @see #cleanDir(String) cleanDir()
   */
  public static boolean deleteDir(String dirPath, boolean recursive) {
    File f;
    String[] subFiles;
    try {
      f = new File(dirPath);
      
      if (!f.isDirectory()) return false;
      subFiles = f.list();
      if (!recursive && (subFiles.length > 0)) return false;
      if (recursive)
        return cleanDir(dirPath, true);
      else
        return deleteFile(dirPath);
    } catch(Throwable e) {
      e.printStackTrace();
    }
    return false;
  }

  /**
   * Supprime les elements de systeme de fichiers designes par le chemin
   * <code>path</code>. Si <code>path</code> indique un fichier, il sera
   * supprime. S'il indique un repertoire, il sera supprime avec tous
   * ses fichiers et ses sous-repertoires.
   * 
   * <p>Retourne <i>true</i> si la suppression a pu etre faite, et
   * <i>false</i> dans le cas contraire. Si l'element indique n'existe pas,
   * l'operation de suppression est consideree comme reusssie.</p>
   */
  public static boolean deletePath(String path) {
    if (existsFile(path)) {
      if (isDirectory(path))
        return deleteDir(path, true);
      else
        return deleteFile(path);
    } else {
      return true;
    }
  }

  /**
   * Vide le contenu d'un repertoire en supprimant tous les
   * les fichier et les sous repertoires. La suppression echoue
   * si le chemin n'indique pas de repertoire ou l'utilisateur
   * n'as pas les droits requises pour la suppression.  
   * 
   * <p>Le repertoire <code>dirPath</code> lui-meme n'est pas supprime.
   * 
   * @param dirPath Le chemin (absolu) du repertoire a supprimer.
   * @return Retourne <i>true</i> si l'operation s'est deroulee
   *   avec succes.
   * 
   * @see #deleteFile(String) deleteFile()
   * @see #deleteDir(String, boolean) deleteDir()
   */
  public static boolean cleanDir(String dirPath) {
  	return cleanDir(dirPath, false);
  }

  /**
   * Supprime le contenu d'un repertoire. Permet d'indiquer si
   * l'on souhaite de supprimer le repertoire lui-meme.
   * 
   * <p>Cette methode est appele par les methodes <code>deleteDir</code>
   * et <code>cleanDir</code>.
   * 
   * @param dirPath Le chemin (absolu) du repertoire a supprimer.
   * @param deleteMainDir <i>true</i> si l'on souhaite de supprimer
   *   le repertoire <code>dirPath</code> lui-meme.
   * @return Retourne <i>true</i> si l'operation s'est deroulee
   *   avec succes.
   * 
   * @see #deleteFile(String) deleteFile()
   * @see #deleteDir(String, boolean) deleteDir()
   * @see #cleanDir(String) cleanDir()
   */
  private static boolean cleanDir(String dirPath,
                                  boolean deleteDir)
  {
    File f;
    String[] subFiles;
    String absPath, path;
    boolean hasError = false;
    
    try {
      f = new File(dirPath);
      if (!f.isDirectory()) return false;

      subFiles = f.list();
      absPath = normalizeDirName(dirPath);
      for (int i = 0; i < subFiles.length; i++) {
        path = absPath + subFiles[i];
        f = new File(path);
        if (f.isDirectory()) {
          if (!cleanDir(path, true)) hasError = true;
        } else {
          if (!deleteFile(path)) hasError = true;
        }
      }
      if (deleteDir) {
        if (!(new File(dirPath)).delete()) hasError = true;
      }
      return (!hasError);
    } catch (Throwable ex) {
      ex.printStackTrace();
      return false;
    }
  }
  
  /**
   * Retroune la liste des fichiers et des sous-repertoires du repertoire
   * <code>dirPath</code>. Retroune <i>null</i> si le chemin indique ne
   * correspond pas a un repertoire ou si l'utilisateur n'a pas les droits
   * requis pour lire son contenu.
   * 
   * @param dirPath Le chemin du repertoire.
   */
  public static String[] listDir(String dirPath) {
    return listDir(dirPath, "*", false);
  }
  
  /**
   * Retroune la liste des noms des fichiers et des sous-repertoires du
   * repertoire <code>dirPath</code> corrspondant au schema
   * <code>fileNamePattern</code>. Le parametre <code>useFullPath</code>
   * indique si le resultat est constitue uniquement des noms des fichiers
   * ou de leur chemins d'acces complets.
   * 
   * <p>La methode retourne <i>null</i> si le chemin indique ne
   * correspond pas a un repertoire ou si l'utilisateur n'a pas les droits
   * requis pour lire son contenu.</p>
   * 
   * <p>Le schema <code>fileNamePath</code> peut comporter les symboles
   * "*" (plusieurs caracteres dans le nom) et "?" (un caractere).</p>
   * 
   * @param dirPath Le chemin du repertoire.
   * @param fileNamePattern Le schema que les noms des fichiers et des
   *   repertoires doivent respecter. Par exemple, "*.zip" indique les
   *   fichiers et repertoires dont le nom se termine par ".zip".
   * @param useFullPath Indique si le tableau retourne sera constitue
   *   des noms des fichiers (<i>false</i>), ou des chemins d'acces
   *   complet (<i>true</i>).
   * 
   * @see #listDir(String)
   * @see #findFiles(String, String, boolean)
   */
  public static String[] listDir(String dirPath,
                                 String fileNamePattern,
                                 boolean useFullPath)
  {
    File dir;
    String[] files;
    
    try {
      dir = new File(dirPath);
      if (dir.isDirectory()) {
        if (fileNamePattern == null) fileNamePattern = "*";
        files = dir.list(new FileListFilter(fileNamePattern));
        if (useFullPath) {
          for(int i=0; i<files.length; i++)
            files[i] = normalizeDirName(dirPath)+files[i]; 
        }
        return files; 
      }
    } catch(Throwable ex) {
      ex.printStackTrace();
    }
    return null;
  }

  /**
   * Retourne la liste des chemins absolus des fichiers avec le nom
   * <code>filePattern</code>. Les fichiers sont recherches dans le repertoire
   * <code>dirPath</code>.
   * 
   * <p>Le parametre <code>filePath</code> indique le nom de fichier exacte
   * ou une partie de nom. Dans ce dernier cas, il peut contenir les symboles
   * "*" (plusieurs caracteres) et "?" (un seul caractere).</p>
   * 
   * <p>Le parametre <code>recursive</code> indique si les fichiers doivent
   * etre recherches uniquement dans le repertoire <code>dirPath</code>
   * (<i>false</i>) ou dans les sous repertoires (<i>true</i>).</p>
   * 
   * <p>Si aucun fichier n'est trouve, la methode retourne la liste vide.</p>
   * 
   * @see #listDir(String, String, boolean)
   */
  public static Vector findFiles(String dirPath,
                                 String filePattern,
                                 boolean recursive)
  {
    Vector files = new Vector();
    Vector subFiles = new Vector();
    String[] dirContent = listDir(dirPath, "*", true);
    if (dirContent != null) {
      for(int i=0; i<dirContent.length; i++) {
        // Si le nom correspond, alors on le memorise
        if (StringCtrl.like(getFileName(dirContent[i]), filePattern))
          files.addElement(dirContent[i]);
        // Sinon, on cherche dans les sous repertoires
        if (recursive && isDirectory(dirContent[i])) {
          Vector v = findFiles(dirContent[i], filePattern, recursive);
          for(int j=0; j<v.size(); j++)
            subFiles.addElement(v.elementAt(j));
        }
      }
      // On ajoute les fichiers des sous repertoires apres ceux de repertoire
      for(int i=0; i<subFiles.size(); i++)
        files.addElement(subFiles.elementAt(i));
    }
    return files;
  }
  
  /**
   * Change le nom d'un fichier ou repertoire. Cette methode peut
   * aussi etre utilisee pour deplacer les fichiers.
   */
  public static boolean renameFile(String oldPath, String newPath) {
    try {
      File fileSource = new File(oldPath);
      File fileDest = new File(newPath);
      return fileSource.renameTo(fileDest);
    } catch(Throwable ex) {
      ex.printStackTrace();
    }
    return false;
  }

  /**
   * Cree un nouveau repertoire correspondant au chemin
   * indique. Retourne <i>true</i> si la creation se deroule
   * avec succes.
   * <br>C'est un racourcie de l'appel :
   * <div align="center"><code>(new File(dirName)).mkdir()</code></div>
   */
  public static boolean makeDir(String dirName) {
    try {
      return (new File(dirName)).mkdir();
    } catch(Throwable ex) {
      ex.printStackTrace();
    }
    return false;
  }

  /**
   * Creer les repertoires indiques par le chemin. Si le
   * chemin contient les sous-repertoires qui n'existent pas,
   * ils sont aussi crees. La creation de ne sera pas autorisee
   * si les repertoires dans <code>path</code> ne constituent
   * pas les sous-repertoires du <code>mustExistPath</code>
   *
   * @param path Le chemin des repertoires a creer.
   * @param mustExistPath Le chemin des repertoires qui doivent
   *   deja exister. <i>null</i> si l'existence des repertoires
   *   peut etre ignoree. 
   */
  public static boolean makeDirs(String path, String mustExistPath) {
    Vector vpath;
    String subPath;

    if (mustExistPath != null) {
      if (!(existsFile(mustExistPath) && path.startsWith(mustExistPath)))
        return false;
    }
    if (path.startsWith("/") || path.startsWith("\\"))
      subPath = SEPARATOR;
    else
      subPath = "";
    vpath = pathToVector(path);
    try {
      for (int i = 0; i < vpath.size(); i++) {
      	if ((subPath.length() > 0) && (!subPath.equals(SEPARATOR)))
      	  subPath += SEPARATOR;
        subPath += (String)vpath.elementAt(i);
        if (!existsFile(subPath)) {
          if (!(new File(subPath)).mkdir())
            return false;
        }
      }
    } catch (Throwable ex) {
      ex.printStackTrace();
      return false;
    }
    return true;
  }

  /**
   * Retourne la taille du fichier en octets.
   * <br>C'est un racourci de l'appel :
   * <div align="center"><code>(new File(fileName)).length()</code></div>
   */
  public static long getFileSize(String fileName) {
    try {
      if (existsFile(fileName))
        return (new File(fileName)).length();
    } catch(Throwable e) {
      e.printStackTrace();
    }
    return -1L;
  }

  /**
   * Retourne le chemin absolu du fichier.
   * <br>C'est un racourci de l'appel :
   * <div align="center"><code>(new File(fileName)).getAbsolutePath()</code></div>
   */
  public static String getFullPath(String fileName) {
    try {
      return (new File(fileName)).getAbsolutePath();
    } catch(Throwable e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * Renvoie le nom de fichier indique par le chemin. Les deux symboles
   * "\\" et "/" sont consideres comme les separateurs des fichiers.
   * Cette methode retourne le nom de fichier avec son extention.
   */
  public static String getFileName(String path) {
    return getFileName(path, false);
  }

  /**
   * Renvoie le nom de fichier indique par le chemin. Les deux symboles
   * "\\" et "/" sont consideres comme les separateurs des fichiers.
   * La parametre stripExtention permet d'indiquer si l'extention de fichier
   * doit (<i>false</i>) ou ne doit pas (<i>true</i>) etre retournee avec le
   * nom de fichier. Si le parametre<code>stripExtention</code> est
   * <i>true</i>, alors tout ce qui suite le symbole '.' dans le nom de
   * fichier sera supprime (le '.' inclu).
   */
  public static String getFileName(String path, boolean stripExtention) {
    StringTokenizer st;
    try {
      path = path.trim();
      // Si ceci n'est pas le fichier.
      if (path.endsWith("/") || path.endsWith("\\")) return "";
      st = new StringTokenizer(path, "/\\");
      while(st.hasMoreTokens()) {
        path = st.nextToken();
      }
      if (path != null) {
        if (stripExtention && path.indexOf('.') >= 0)
          path = path.substring(0, path.indexOf('.'));
        return path;
      } 
    } catch(Throwable e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * Renvoie le nom de repertoire parent de l'element indique par le chemin.
   * Les deux symboles "\\" et "/" sont consideres
   * comme les separateurs des fichiers.
   */
  public static String getParentDirectory(String fullPath) {
    StringTokenizer st;
    String path = fullPath;
    int i;
    
    try {
      if (path.endsWith("/") || path.endsWith("\\")) path = path.substring(0, path.length()-1);
      st = new StringTokenizer(path, "/\\");
      while(st.hasMoreTokens()) path = st.nextToken();
      if ((path == null) || (path.length() == 0)) {
        path = fullPath;
      } else {
        i = fullPath.lastIndexOf(path);
        if (i > 0) path = fullPath.substring(0, i);
      }
      if ((path.length() > 1) && (path.endsWith("/") || path.endsWith("\\")))
        path = path.substring(0, path.length()-1);
      return path;
    } catch(Throwable e) {
      e.printStackTrace();
    }
    return "";
  }

  /**
   * Genere un nouveau nom du fichier dans le repertoire donne.
   * Le format de nom est <code>AAMMJJhhmm.i</code>, ou
   * <code>i</code> est un index (0, 1, ...). Cette methode
   * permet de trouver un nom de fichier unique.
   * 
   * @param dir Le repertoire dans lequel le nouveau nom
   *   doit etre creee.
   */
  public static String getNewNameInDir(String dir) {
    String s = currentDateForName().substring(2);
    int i = 0;
    for(;existsFile(dir+s+"."+i); i++);
    return s+"."+i;
  }

  /**
   * Genere un nouveau nom dans le repertoire donnee.
   * Utilise le prefix indique pour le nom du fichier.
   * Ajoute un indice supplementaire ".i" (i est un entier)
   * si le fichier avec le prefix donne existe deja.
   */
  public static String getNewNameInDir(String dir, String defaultPrefix) {
    if ((dir == null) || (dir.length()== 0 )) {
      dir = "";
    } else {
      // if (!dir.endsWith(File.separator)) dir += File.separator;
      dir = normalizeDirName(dir);
    }
    defaultPrefix += "."+Long.toString((new Date()).getTime());
    if (existsFile(dir+defaultPrefix)) {
      int i;
      for(i=0;existsFile(dir+defaultPrefix+"."+i); i++);
      return defaultPrefix+"."+i;
    } else {
      return defaultPrefix;
    }
  }

  /**
   * Genere une chaine de caracteres representant la date
   * actuelle. Le format de la chaine est <code>AAMMJJhhmm</code>.
   * Cette expression est utilisee pour generer les noms
   * de nouveaux fichiers ou repertoires.
   * 
   * @see #getNewNameInDir(String)
   */
  private static String currentDateForName() {
    java.util.Calendar cal = new GregorianCalendar();
    StringBuffer sb = new StringBuffer("");
    sb.append(cal.get(GregorianCalendar.YEAR));
    if (GregorianCalendar.JANUARY == 0)
      sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MONTH)+1, 2));
    else
      sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MONTH), 2));
    sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.DAY_OF_MONTH), 2));
    sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.HOUR_OF_DAY), 2));
    sb.append(StringCtrl.get0Int(cal.get(GregorianCalendar.MINUTE), 2));
    return sb.toString();
  }
  
  /**
   * Genere un nouveau nom de fichier en se basant sur le nom d'un
   * fichier donnee. Le nouveau nom est genere en ajoutant
   * le prefix et/ou suffixe donnes. Le nom est cree dans le
   * repertoire du fichier d'origine.
   * 
   * @param fileName Le nom du fichier existant a partir de lequel
   *   le nouveau nom sera cree.
   * @param prefix Le prefix a ajouter au nouveau nom cree. Il est ignore
   *   si cette valeur est <code>null</code>.
   * @param suffix Le suffix a ajouter au nouveau nom cree. Il est ignore
   *   si cette valeur est <code>null</code>.
   * @param unique <i>true</i> si le nouveau nom doit etre unique dans le
   *   repertoire du fichier d'origine. L'indice ".i" (i - un entier)
   *   sera ajouter si necessaire au nouveu nom.
   * @return Le chemin absolu du nouveau nom de fichier.
   * 
   * @see #getNewNameFromName(String, String, String)
   */
  public static String getNewNameFromName(String fileName, String prefix, String suffix, boolean unique) {
    String dir = getParentDirectory(fileName);
    String newName = "";
    if (prefix != null) newName = prefix;
    newName += getFileName(fileName);
    if (suffix != null) newName += suffix;
    // if (!dir.endsWith(File.separator)) dir += File.separator;
    dir = normalizeDirName(dir);
    if (unique)
      return dir+getNewNameInDir(dir, newName);
    else
      return dir+newName;
  }
  
  /**
   * Genere un nouveau nom de fichier en se basant sur le nom d'un
   * fichier donnee. Le nouveau nom est genere en ajoutant
   * le prefix et/ou suffixe donnes. Le nom est cree dans le
   * repertoire du fichier d'origine.
   * 
   * <p>Le nouveau nom genere sera un nom unique.
   * 
   * @param fileName Le nom du fichier existant a partir de lequel
   *   le nouveau nom sera cree.
   * @param prefix Le prefix a ajouter au nouveau nom cree. Il est ignore
   *   si cette valeur est <code>null</code>.
   * @param suffix Le suffix a ajouter au nouveau nom cree. Il est ignore
   *   si cette valeur est <code>null</code>.
   * @return Le chemin absolu du nouveau nom de fichier.
   * 
   * @see #getNewNameFromName(String, String, String, boolean)
   */
  public static String getNewNameFromName(String fileName, String prefix, String suffix) {
    return getNewNameFromName(fileName, prefix, suffix, true);
  }
  
  /**
   * Renvoie le nom du repertoire donnee en une forme normalizee.
   * Le nom du repertoire doit toujours finir par le symbole de
   * separateur des fichiers (voir l'attribur <code>SEPARATOR</code>).
   * 
   * @param dirName Le nom d'un repertoire.
   * @return String Le nom de repertoire normalize.
   * @see #SEPARATOR
   */
  public static String normalizeDirName(String dirName) {
    if (dirName == null)
      return null;
    if (!(dirName.endsWith("/") || dirName.endsWith("\\")))
      return dirName+SEPARATOR;
    return dirName;
  }
  
  /**
   * Test si le nom de fichier ne contient pas de caracteres speciaux.
   * Tous les caracteres differents des lettres non-accentues, des chifres
   * et des symboles ".", "-" et "_" sont consideres comme speciaux.
   * 
   * <p>Cette methode peut etre utilisee pour assurer la compatibilite
   * des noms de fichiers entre differents systemes d'exploitation utilisant
   * les codages differents.
   * 
   * @param fileName Le nom du fichier
   * @return <i>true</i> si le nom de fichier ne contient pas de caracteres speciaux.
   * @see StringCtrl#toBasicString(String)
   * @see #isBasicPath(String)
   */
  public static boolean isBasicName(String fileName) {
  	return StringCtrl.toBasicString(fileName).equals(fileName);
  }
  
  /**
   * Test si le chemin donnee ne contient pas de caracteres speciaux.
   * Tous les caracteres differents des lettres non-accentues, des chifres
   * et symboles ".", "-" et "_" sont consideres comme speciaux. Les deux
   * caracteres "\" et "/" sont acceptes comme separateurs des fichiers.
   * 
   * <p>Cette methode peut etre utilisee pour assurer la compatibilite
   * des noms des fichiers et des repertoires entre differents systemes
   * d'exploitation utilisant les codages differents.
   * 
   * @param path Le chemin.
   * @return <i>true</i> si auncun element dans le chemin ne contient pas
   *   de caracteres speciaux.
   * @see #isBasicName(String)
   */
  public static boolean isBasicPath(String path) {
  	Vector vpath = pathToVector(path);
    for (int i = 0; i < vpath.size(); i++) {
      if (!isBasicName((String) vpath.elementAt(i)))
        return false;
    }
  	return true;
  }
  
  /**
   * Converti le chemin en un vecteur comportant les
   * elements du chemin. Les elements correspondent aux
   * noms des repertoires et des fichiers. Les deux
   * symboles "\" et "/" sont acceptes comme les
   * separateurs des fichiers.
   *  
   * @param path Le chemin.
   * @return Un vecteur des elements du chemin.
   */
  public static Vector pathToVector(String path) {
  	Vector vpath = new Vector();
  	StringTokenizer st = new StringTokenizer(path, "\\/");
  	while(st.hasMoreElements()) {
      vpath.addElement(st.nextToken());
  	}
  	return vpath;
  }

  /**
   * Test si le chemin <code>path</code> est le chemin absolu.
   */
  public static boolean isAbsolutePath(String path) {
    try {
      return (new File(path)).isAbsolute();
    } catch(Throwable e) { }
    return false;
  }
  
  /**
   * Test si l'element de systeme des fichiers indique par le chemin
   * <code>path</code> est un repertoire.
   */
  public static boolean isDirectory(String path) {
    try {
      return (new File(path)).isDirectory();
    } catch(Throwable e) { }
    return false;
  }
  
  /**
   * La classe permettant de filtrer les noms des fichiers
   * dans un repertoire. Cette classe utilise la definition d'un filtre
   * qui peut comporter les symboles '*' et '?' dans les noms des fichiers.
   * Le symbole '*' represente 0 ou plusieurs symboles, et '?' - un seul
   * symbole. Voir la documentation du constructeur de la classe pour
   * plus d'informations.
   */  
  public static class FileListFilter implements FilenameFilter {
    
    /**
     * La definition de filtre.
     */
    private String pattern;
    
    /**
     * Definit un objet qui effectuera la selection des noms des fichiers
     * suivant le filtre <code>pattern</code>. Le filtre peut comporter les
     * symboles :
     * <ul>
     * <li>'*' - 0 ou plusieurs symboles&nbsp;;</li>
     * <li>'?' - un symbole.</li>
     * </ul>
     * 
     * Par exemple, la valeur "File.pas" est acceptee par le filtre "File.*",
     * mais sera refusee par "File.?". Le filtre "*.*" accepte tous les
     * fichiers dont le nom comporte ".".
     * 
     * <p>Acun nom de fichier ne sera pas accepte si <code>pattern</code>
     * est <i>null</i>.</p>  
     */
    public FileListFilter(String pattern) {
      this.pattern = pattern;
    }

    /**
     * Teste si le nom de fichier est accepte par le filtre.
     */
    public boolean accept(File dir, String fileName) {
      return StringCtrl.like(fileName, pattern);
    }
  }

	private static final int DEFAULT_BUFFER_SIZE = 1024 * 4;
	
	public static String fileToString(final File file) {
		final char[] buffer = new char[DEFAULT_BUFFER_SIZE];
		final StringBuilder out = new StringBuilder();
		try {
			final InputStream is = openInputStream(file);
			final Reader in = new InputStreamReader(is, "UTF-8");
			try {
				for (;;) {
					int rsz = in.read(buffer, 0, buffer.length);
					if (rsz < 0)
						break;
					out.append(buffer, 0, rsz);
				}
			} finally {
				try {
					in.close();
				} catch (Exception e) {
					// Quiet
				}
			}
		} catch (UnsupportedEncodingException ex) {
			ex.printStackTrace();
			return null;
		} catch (IOException ex) {
			ex.printStackTrace();
			return null;
		}
		return out.toString();
	}
 	
	public static FileInputStream openInputStream(File file) throws IOException {
		if (file.exists()) {
			if (file.isDirectory()) {
				throw new IOException("File '" + file + "' exists but is a directory");
			}
			if (!file.canRead()) {
				throw new IOException("File '" + file + "' cannot be read");
			}
		} else {
			throw new FileNotFoundException("File '" + file + "' does not exist");
		}
		return new FileInputStream(file);
	}
}
