package org.cocktail.fwkcktlwebapp.common.util;


import java.util.Date;
import java.util.Random;

/**
 * Permet de generer une cle aleatoire d'une longeur demandee.
 * 
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public class RandomKeyGenerator {

  /** L'alfabet dans lequel dont les valeurs seront utilisees pour la cle */
  private static final String ALPHABET =
    "123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
  /** L'instance de generateur initialisee au chargement de la classe */
  private static Random random = new Random((new Date()).getTime());
  
  /**
   * Retourne une cle generee aleatoirement. La cle va comporter
   * <code>len</code> caracteres.
   */
  public static String getNewKey(int len) {
    StringBuffer key = new StringBuffer(len);
    for (int i=0; i<len; i++) {
      key.append(ALPHABET.charAt(Math.abs(random.nextInt())%ALPHABET.length()));
    }
    return key.toString();
  }
}
