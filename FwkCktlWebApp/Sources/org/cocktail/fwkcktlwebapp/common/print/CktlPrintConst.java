package org.cocktail.fwkcktlwebapp.common.print;


/**
 * Contient les definitions des constantes partagees par differentes
 * classes du systeme d'impression XML.
 */
public final class CktlPrintConst {

  /**
   * Le code indiquant le succes d'une operation.
   */
  public static final int SUCCESS = 0;
  
  /**
   * Le code indiquant l'echeque d'une operation.
   */
  public static final int ERROR = 1;
  
  /**
   * Le code indiquant la presence d'un message d'avertissement suite
   * a une operation.
   */
  public static final int WARNING = 2;

  /**
   * Le code indiquant qu'une operation est en cours d'execution.
   */
  public static final int IN_PROGRESS = 3;
  
  /**
   * Le code indiquant que l'operation demande n'existe pas.
   */
  public static final int NOT_FOUND = 4;
  
  /**
   * Le code de format de donnees PDF.
   */
  public static final byte FORMAT_PDF = 100;

  /**
   * Le code de format de donnees Postscript.
   */
  public static final byte FORMAT_PS = 101;
  
  /**
   * Le code de format de donnees PCL.
   */
  public static final byte FORMAT_PCL = 102;
  
  /**
   * Le code de format de donnees Text.
   */
  public static final byte FORMAT_TXT = 103;

  /**
   * Code d'erreur : aucune erreur.
   */
  public static final byte ERR_OK = 0;

  /**
   * Code d'erreur : une operation inconnue ou non implementee.
   */
  public static final byte ERR_UNIMPLEMENTED = 1;

  /**
   * Code d'erreur : la maquette demande n'existe pas.
   */
  public static final byte ERR_NO_TEMPLATE = 2;

  /**
   * Code d'erreur : la tache d'impression demandee n'existe pas.
   */
  public static final byte ERR_NO_JOB = 3;

  /**
   * Code d'erreur : la tache d'impression demandee est en cours
   * d'impression.
   */
  public static final byte ERR_JOB_BUSY = 4;

  /**
   * Code d'erreur : des erreurs ont survenues lors de l'execution d'une
   * tache d'impression.
   */
  public static final byte ERR_JOB_ERROR = 5;

  /**
   * Code d'erreur : le format d'impression demande n'est pas supportee
   * par l'implementation de service d'impression.
   */
  public static final byte ERR_BAD_FORMAT = 6;

  /**
   * Code d'erreur : une erreur est survenue lors transfert de donnees entre
   * le client et le service d'impression.
   */
  public static final byte ERR_TRANSFER = 7;

  /**
   * Code d'erreur : une erreur est survenue lors d'une operation avec
   * le systeme de fichiers (le sauvegarde ou la lecture d'un fichier).
   */
  public static final byte ERR_FILE = 8;

  /**
   * Code d'erreur : erreur lors d'etablissement de connexion avec le service
   * d'impression.
   */
  public static final byte ERR_CONNECT = 9;
  
  /**
   * Code d'erreur : le client n'a pas ete initialise ou l'implementation
   * de client SIX n'a pas ete trouvee.
   */
  public static final byte ERR_NO_CLIENT = 10;

  /**
   * Code d'erreur : une erreur inconnue.
   */
  public static final byte ERR_UNKNOWN = 126;
  
  /**
   * La cle utilisee pour fournir la definition du nom du service d'impression
   * en cours. La valeur avec cette cle doit etre definie dans le dictionaire
   * retourne par la methode <code>checkService</code> de l'implementation
   * de {@link CktlPrinter}.
   */
  public static final String SERVICE_NAME_KEY = "xml.print.service.name";
  
  /**
   * La cle utilisee pour fournir la definition de la version du service
   * d'impression en cours. La valeur avec cette cle doit etre definie dans
   * le dictionaire retourne par la methode {@link CktlPrinter#checkService()} de
   * l'implementation de  {@link CktlPrinter}.
   */
  public static final String SERVICE_VERSION_KEY = "xml.print.service.version";
  
  /**
   * La cle utilisee pour fournir la definition de la descriontion
   * supplementaire du service d'impression en cours. La valeur avec cette
   * cle doit etre definie dans le dictionaire retourne par la methode
   * {@link CktlPrinter#checkService()} de l'implementation de  {@link CktlPrinter}.
   */
  public static final String SERVICE_DESCRIPTION_KEY = "xml.print.service.description";
  
  /**
   * La definition de la cle dont la valeur est recherchee dans les
   * proprietes de Java pour detecter la classe implementant l'interface
   * du client d'impression XML.
   */
  public static final String XML_PRINTER_DRIVER_KEY = "fr.univlr.xml.printer.driver";
  
  /**
   * Constructeur prive, car cette classe ne contient que les
   * definitions des constantes et les methodes statiques. Aucun
   * objet de la classe ne doit pas etre cree.
   */
  private CktlPrintConst() { }
  
  /**
   * Retourne l'extention de fichier correspondant au
   * <code>format</code>. Le parametre doit correspondre
   * a une des constantes <code>FORMAT_XXX</code>. Retourne
   * la chaine vide si le parametre format ne correspond a
   * aucun des formats connus. La valeur returnee est en
   * majuscules.
   */
  public static String extForFormat(int format) {
    switch(format) {
      case FORMAT_PDF : return "PDF";
      case FORMAT_PS : return "PS";
      case FORMAT_PCL : return "PCL";
      case FORMAT_TXT : return "TXT";
      default : return "";
    }
  }
  /**
   * Retourne le code MIME de document correspondant au <code>format</code>.
   * Le parametre doit correspondre a une des constantes
   * <code>FORMAT_XXX</code>. Retourne la code "application/octet-stream"
   * si le parametre <code>format</code> ne correspond a aucun des
   * formats connus.
   */
  public static String mimeForFormat(int format) {
    switch(format) {
      case FORMAT_PDF : return "application/pdf";
      case FORMAT_PS : return "application/postscript";
      case FORMAT_PCL : return "application/octet-stream";
      case FORMAT_TXT : return "text/plain";
      default : return "application/octet-stream";
    }
  }
  
  /**
   * Teste si le code de format est valide. Le format est valide
   * s'il correspond a une des constantes <code>FORMAT_XXX</code>.
   */
  public static boolean isFormatValid(int format) {
    switch(format) {
      case FORMAT_PDF : 
      case FORMAT_PS : 
      case FORMAT_PCL : 
      case FORMAT_TXT : return true;
      default : return false;
    }
  }
}
