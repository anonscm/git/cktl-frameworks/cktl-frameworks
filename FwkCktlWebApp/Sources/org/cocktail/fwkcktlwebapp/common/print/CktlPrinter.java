package org.cocktail.fwkcktlwebapp.common.print;


import java.io.InputStream;
import java.util.Dictionary;
import java.util.Hashtable;

/**
 * Definit une interface pour la gestion d'impression des documents.
 * 
 * <h3>Types d'impression</h3>
 * 
 * Trois types d'operations sont considerees comme <i>impression</i>&nbsp;:
 * <ol>
 * <li><i>impression dans un fichier</i> - un document (PDF) est genere a
 * partir des donnees utilisateur envoyees sous forme d'un fichier XML.
 * Il peut ensuite etre recupere et traite par le client : sauvegarde
 * dans un fichier, visualisation a l'aide d'un visualiseur (PDF), impression
 * sur une imprimante, etc...;</li>
 * <li><i>impression sur une imprimante</i> - le document genere a partir
 * des donnees XML est directement imprime sur une imprimante. Aucun fichier
 * intermediaire ne peut pas etre recupere;</li>
 * <li><i>impression mixte</i> - le document genere est imprime sur une
 * imprimante et en meme temps un document est cree. Il peut ensute
 * etre recupere.</li>
 * </ol>
 * 
 * Les operations d'impression peuvent etre effectuees de deux manieres&nbsp;:
 * <ol>
 * <li><i>impression immediate</i> - le client recoit immediatement
 * la reponse et/ou le resultat d'impression. Ceci implique que l'execution
 * de client sera bloquee jusqu'a la fin de l'operation d'impression;</li>
 * <li><i>impression differee</i> - l'impression est deposee sous forme
 * d'une demande qui sera traitee une fois que les ressources necessaires
 * seront disponibles. L'execution de client ne sera pas bloquee suite
 * a l'appel a une operation d'impression. Le client devra interroger
 * le pilote ulterieurement pour connaitre l'etat de sa demande et/ou
 * recuperer le resultat d'impression.<br>
 * L'impression differee peut etre utilisee pour imprimer des documents
 * importants, dont la creation peut necessite des delais importants.</li>
 * </ol>
 * 
 * <p><i>L'implementation actuelle d'impression ne propose que
 * l'impression dans un fichier. Ni l'impression sur une imprimante, ni
 * l'impression mixte ne sont pas encore implementees.</i></p>
 * 
 * 
 * <h3>Implementation concrete</h3>
 * 
 * <p>La classe qui effectue une impression des documents doit heriter
 * de la classe {@link CktlPrinter} et implementer les methodes qui
 * y sont definies. Cette classe doit avoir un constructeur qui ne prend
 * pas des perametres. Toute initialisation d'un objet de cette classe
 * est faite dans la methode {@link #initPrinter(Hashtable)} en utilisant
 * les parametres passes dans un dictionnaire.</p>
 * 
 * 
 * <h3>Initialisation et configuration</h3>
 * 
 * <p>Il est prevus que le systeme d'impression soit utilise de maniere
 * a pouvoir changer dynamiquement le pilote d'impression sans apporter
 * les modifications au code Java de l'application cliente. La classe
 * concrete du pilote est donc indiquee au niveau de la configuration
 * de l'environement de l'application et non au niveau de code.</p>
 * 
 * <p>Pour cette raison, il est conseille d'utiliser la metode statique
 * <code>newDefaultInstance</code> pour creer une instance de pilote.
 * Cette methode prend comme parametre un dictionnaire des couples
 * CLE=VALEUR avec la configuration de pilote. Le dictionnaire peut
 * avoir au moins une definition avec la cle <code>XML_PRINTER_DRIVER</code>
 * dont la valeur indique le nom de la classe de pilote a utiliser.
 * Si cette valeur n'est pas definie, le nom de la classe est recherche
 * dans les proprietes du systeme Java avec le nom
 * <code>fr.univlr.xml.printer.driver</code> (voir la constante
 * <code>XML_PRINTER_DRIVER_KEY</code> de la classe {@link CktlPrintConst}).
 * Par defaut, la valeur de ce parametre est definie a
 * <code>fr.univlr.cri.print.SIXPrinter</code>.</p>
 * 
 * <p>Le dictionnaire des parametres peut avoir d'autres definitions,
 * en fonction de l'implementation concrete. Adressez vous a la
 * documentation de la classe de votre pilote.</p>
 * 
 * 
 * <h3>Exemples</h3>
 * 
 * On suppose que pour ces exemples les parametres d'impression seront
 * initialises par la methode <code>initParams</code> et ils vont
 * contenir une definition&nbsp;:
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 align=center class="example"><tr><td><pre>
 * XML_PRINTER_DRIVER="fr.univlr.cri.print.SIXPrinter"</pre></td></tr></table>
 * </p>
 * 
 * 
 * <h4>Impression immediate</h4>
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * <code class="comment">// On suppose que cette methode existe dans mon application</code>
 * Hashtable params = myInitParams();
 * <code class="comment">// On cree et on initialise le pilote d'impression</code>
 * CktlPrinter printer;
 * try {
 *   printer = CktlPrinter.newDefaultInstance(params);
 * } catch (ClassNotFoundException e) {
 *   <code class="comment">// Le pilote n'a pas ete trouve</code>
 *   System.out.println("Erreur : "+e.getMessage());
 *   return;
 * }
 * <code class="comment">// C'est ce qu'on va imprimer</code>
 * String dataFile = "/MyDocuments/donnees.xml";
 * String resultFile = "/MyDocuments/impression.pdf";
 * String maquetteID = "MaquetteTest";
 * <code class="comment">// On fait appel a l'impression. On restera bloque jusqu'a la</code>
 * <code class="comment">// fin de l'impression.</code>
 * printer.printFileImmediate(maquetteID, dataFile, resultFile);
 * <code class="comment">// On verifie si l'operation est OK</code>
 * if (printer.hasSuccess()) {
 *   System.out.println("OK. Reponse de pilote :\n  "+printer.getMessage());
 * } else {
 *   System.out.println("FAIL ! Erreur :\n  "+printer.getMessage());
 * }</pre></td></tr></table>
 * 
 * 
 * <h4>Impression differee</h4>
 * 
 * <p>
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * <code class="comment">// On suppose que cette methode existe dans mon application</code>
 * Hashtable params = myInitParams();
 * <code class="comment">// On cree le pilote d'impression</code>
 * CktlPrinter printer;
 * try {
 *   printer = CktlPrinter.newDefaultInstance(params);
 * } catch (ClassNotFoundException e) {
 *   <code class="comment">// Le pilote n'a pas ete trouve</code>
 *   System.out.println("Erreur : "+e.getMessage());
 *   return;
 * }
 * <code class="comment">// C'est ce qu'on va imprimer</code>
 * String dataFile = "/MyDocuments/donnees.xml";
 * String resultFile = "/MyDocuments/impression.pdf";
 * String maquetteID = "MaquetteTest";
 * String receiptUrl = "http://...";
 * <code class="comment">// On recupere la definition pour la validation des donnees</code>
 * String dtdReference = printer.getValidationReference(maquetteID);
 * preparerDonnees(dataFile, dtdReference); <code class="comment">// Supposons que ceci existe</code>
 * <code class="comment">// On indique combient du temps on souhaite garder les resultats
 * // d'impression apres la de creation du document. Ici, 120 sec = 2 min.</code>
 * printer.setJobAliveTimeout(120);
 * <code class="comment">// Indique s'il faut utiliser la validation DTD des donnees.
 * // On la fait, si la definition DTD a ete donnee.</code>
 * printer.setUseValidation(dtdReference != null);
 * <code class="comment">// On indique le URL ou l'accise de reception sera envoye.</code>
 * printer.setReceiptURL(receiptUrl);
 * <code class="comment">// On effectue l'impression differee. Les donnees seront deposees
 * // et l'operation terminee avant la fin de creation du document.
 * // On recupere l'identifiant pour recupere le document ultrieurement.</code>
 * int jobId = printer.printFileDiffered(maquetteID, dataFile);
 * <code class="comment">// On continue s'il n'y a pas d'erreurs.</code>
 * if (printer.hasSuccess()) {
 *   <code class="comment">// OK. On attend la fin de creation du document...
 *   // L'etat de la tache d'impression jobId sera verifie
 *   // toutes les 2 secondes</code>
 *   printer.waitForJob(jobId, 2);
 * }
 * <code class="comment">// Si tout est OK, on recupere le resultat et
 * // on l'enregistre dans le fichier designe par resultFile</code>
 * if (printer.hasSuccess()) {
 *   printer.getPrintResult(jobId, resultFile);
 * }
 * <code class="comment">// Si c'est toujours OK, on clore l'impression. La tache et les fichiers
 * // d'impression seront supprimes. Sans cette appel, la tache avec
 * // "jobId" et les fichiers resteront disponibles pendant
 * // "setJobAliveTimeout" secondes (voir l'appel ci-dessus).</code>
 * if (printer.hasSuccess()) {
 *   printer.closePrint(jobId);
 * }
 * <code class="comment">// Le verdict...</code>
 * if (printer.hasSuccess()) {
 *   System.out.println("OK. Reponse de pilote :\n  "+printer.getMessage());
 * } else {
 *   System.out.println("FAIL ! Erreur :\n  "+printer.getMessage());
 * }</pre></td></tr></table>
 * 
 * @see CktlPrintConst
 * @see SIXPrinter
 */
public abstract class CktlPrinter {
  // Initialisation de pilote par defaut
  // On le fait lorsque la definition de la classe est charge dans la memoire
  static {
    // on ajoute la definition seulement si ce n'est pas encore fait
    if (System.getProperty(CktlPrintConst.XML_PRINTER_DRIVER_KEY) == null) {
      System.getProperties().put(CktlPrintConst.XML_PRINTER_DRIVER_KEY,
                                 SIXPrinter.class.getName());
    }
  }
  
  /**
   * Cree et initialise une instance de pilote d'impression en utilisant
   * le dictionnaire des parametres <code>params</code>. Une instance
   * d'un pilote est creee en suivant le schema suivant :
   * <ol>
   * <li>si le dictionnaire <code>params</code> contient la defintion
   * <code>XML_PRINTER_DRIVER</code>, alors une instance de cette classe
   * est creee;</li>
   * <li>si la definition n'est pas trouvee dans <code>params</code>, alors
   * on cherche la definition <code>fr.univlr.xml.printer.driver</code>
   * dans les proprietes de systeme Java
   * (<code>System.getProperties</code>, voir egalement la constante
   * <code>XML_PRINTER_DRIVER_KEY</code> de la classe {@link CktlPrintConst}).
   * Si cette definition existe, une instance de la classe est creee. Par
   * defaut, ce parametre indique la classe
   * <code>fr.univlr.cri.print.SIXPrinter</code>;</li>
   * <li>si aucun instance de pilote ne peut etre creee, une exception
   * <code>ClassNotFoundException</code> est levee;</li>
   * <li>sinon, le pilote est initialise en appelant sa methode
   * <code>initPrinter</code> avec le dictionnaire <code>params</code>
   * comme parametre.</li>
   * </ol>
   * 
   * @param params Le dictionnaire des couples CLE=VALEUR definissant
   *   les parametres pour l'initialisation de l'instance de pilote.
   * 
   * @return Une instance de pilote.
   * 
   * @throws ClassNotFoundException
   * 
   * @see #initPrinter(Hashtable)
   * @see CktlPrintConst#XML_PRINTER_DRIVER_KEY
   */
  public final static CktlPrinter newDefaultInstance(Hashtable params)
    throws ClassNotFoundException
  {
    // D'abord dans les parametres d'utilisateurs
    String className = (String)params.get("XML_PRINTER_DRIVER");
    // Sinon, dans la config du systeme
    if (className == null) {
      // On fait dans try pour eviter security exceptions
      try {
          className = (String)System.getProperties().get(CktlPrintConst.XML_PRINTER_DRIVER_KEY);
      } catch(Throwable e) {
        className = null;
      }
    }
    if (className == null)
      throw new ClassNotFoundException("Le pilote d'impression par defaut n'a pas pu etre initialise (XML_PRINTER_DRIVER, "+
                                        CktlPrintConst.XML_PRINTER_DRIVER_KEY+")");
    return newInstance(className, params);
  }

  /**
   * Cree et initialise une instance de pilote d'impression en utilisant
   * le dictionnaire des parametres <code>params</code>. Le parametre
   * <code>className</code> indique le nom de la classe de pilote. Elle
   * doit heriter de la classe {@link CktlPrinter}.
   * 
   * <p>Cette methode definie explicitement la classe a utiliser
   * pour la gestion d'impresisons. Il est conseille ne pas utiliser
   * cette methode dans votre code, mais faire appel a la methode
   * <code>newDefaultInstance</code> avec les parametres appropries.</p>
   * 
   * @param className Le nom de la classe de pilote.
   * @param params Le dictionnaire des couples CLE=VALEUR definissant
   *   les parametres pour l'initialisation de l'instance de pilote.
   * 
   * @return Une instance de pilote.
   * 
   * @throws ClassNotFoundException
   * 
   * @see #newDefaultInstance(Hashtable)
   * @see #initPrinter(Hashtable)
   */
  public final static CktlPrinter newInstance(String className, Hashtable params)
    throws ClassNotFoundException
  {
    try {
      CktlPrinter printer = (CktlPrinter)Class.forName(className).newInstance();
      printer.initPrinter(params);
      return printer;
    } catch(Exception e) {
      throw new ClassNotFoundException("Le pilote d'impression \""+className+
                        "\" n'a pas pu etre initialise : "+e.getMessage());
    }
  }
  
  /**
   * Initialise l'instance de pilote selon les parametres de configuration
   * <code>config</code>.
   * 
   * <p>Cette methode ne doit pas etre appelee explicitement, car elle est
   * implicitement appelee par les methodes <code>newDefaultInstance</code>
   * ou <code>newInstance</code>.</p>
   * 
   * @see #newDefaultInstance(Hashtable)
   */
  public abstract void initPrinter(Hashtable config);
  
  
  /**
   * Teste si le service d'impression utilise par ce client est disponible.
   * 
   * <p>Si le service est disponible, alors cette methode retourne un
   * dictionnaire avec sa description. Les informations disponibles
   * dans le dictionnaire peut dependre de son implementation concrete.
   * On suppose que au moins les valeus definies par les constantes
   * <code>SERVICE_XXX</code> definies dans {@link CktlPrintConst}
   * doivent etre definies. L'implementation actuelle definit les
   * constantes suivantes&nbsp;:
   * <ul>
   * <li><code>SERVICE_NAME_KEY</code> - le nom de service
   * d'impresion&nbsp;;</li>
   * <li><code>SERVICE_VERSION_KEY</code> - la version de
   * service&nbsp;;</li>
   * <li><code>SERVICE_DESCRIPTION_KEY</code> - le description plus
   * detaillee du service.</li>
   * </ul>
   * </p>
   * 
   * Si le service d'impression n'est pas disponible pour le client, cette
   * methode retourne <i>null</i>.
   * 
   * @see CktlPrintConst
   */
  public abstract Dictionary checkService();

  /**
   * Teste si la maquette avec l'identifiant <code>reportId</code> est
   * accessible pour le client. Retourne <i>true</i> si la maquette existe,
   * et <i>false</i> dans le cas contraire.
   */
  public abstract boolean checkTemplate(String reportId);

  /**
   * Effectue la creation d'un document a partir des donnees
   * enregistrees dans un fichier. L'appel a la methode va bloquer
   * l'execution de programme jusqu'a la fin de la tache d'impression.
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataFile Le chemin du fichier contenant les donnees pour
   *   la creation d'un document.
   * 
   * @return Retourne le flux avec le contenu du document genere.
   *   Returne <i>null</i> si le document ne peux pas etre genere ou une
   *   erreur s'est produite.
   */
  public abstract InputStream printFileImmediate(String reportId, String dataFile);
  
  /**
   * Effectue la creation d'un document a partir des donnees
   * envoyees dans un flux d'octets. L'appel a la methode va bloquer
   * l'execution de programme jusqu'a la fin de la tache d'impression.
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataStream Le flux d'octets avec le contenu des donnees
   *   pour la creation d'un document.
   * @param size La taille en octets du contenu des donnees.
   * 
   * @return Retourne le flux avec le contenu du document genere.
   *   Returne <i>null</i> si le document ne peux pas etre genere ou une
   *   erreur s'est produite.
   */
  public abstract InputStream printFileImmediate(String reportId, InputStream dataStream, long size);
  
  /**
   * Effectue la creation d'un document a partir des donnees
   * enregistrees dans un fichier et ensuite enregistre le resultat
   * dans un fichier local. L'appel a la methode va bloquer l'execution
   * de programme jusqu'a la fin de la tache d'impression.
   * 
   * <p>Suite a cette appel, l'appel a la methode <code>hasSuccess</code>
   * renvoie <i>false</i>, si la creation d'un document echoue.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataFile Le chemin du fichier contenant les donnees pour
   *   la creation d'un document.
   * @param resultFile Le chemin de fichier ou le document genere sera
   *   enregistre.
   */
  public abstract void printFileImmediate(String reportId, String dataFile, String resultFile);
  
  /**
   * Effectue la creation d'un document a partir des donnees
   * envoyees dans un flux d'octets et ensuite enregistre le resultat
   * dans un fichier local. L'appel a la methode va bloquer l'execution
   * de programme jusqu'a la fin de la tache d'impression.
   * 
   * <p>Suite a cette appel, l'appel a la methode <code>hasSuccess</code>
   * renvoie <i>false</i>, si la creation de document echoue.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataStream Le flux d'octets avec le contenu des donnees
   *   pour la creation d'un document.
   * @param size La taille en octets du contenu des donnees.
   * @param resultFile Le chemin de fichier ou le document genere sera
   *   enregistre.
   */
  public abstract void printFileImmediate(String reportId, InputStream dataStream,
                                          long size, String resultFile);
                                 
  /**
   * Effectue la creation d'un document a partir des donnees
   * enregistrees dans un fichier.
   * 
   * <p>L'appel a la methode est non-bloquante. Une tache d'impression est
   * demarree et son identifiant est renvoie comme resultat. L'etat de
   * la tache d'impression peut ensuite etre consulte en appelant la methode
   * <code>getPrintStatus</code>. Le resultat d'impression est recuperem
   * a l'aide de la methode <code>getPrintResult</code>.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataFile Le chemin du fichier contenant les donnees pour
   *   la creation d'un document.
   * 
   * @return Retourne l'identifiant de la tache d'impression ou -1 si une
   *   erreur est survenue lors de l'appel a la methode.
   * 
   * @see #getPrintStatus(int)
   * @see #getPrintResult(int)
   * @see #getPrintResult(int, String)
   */
  public abstract int printFileDiffered(String reportId, String dataFile);
  
  /**
   * Effectue la creation d'un document a partir des donnees
   * envoyees dans un flux d'octets.
   * 
   * <p>L'appel a la methode est non-bloquante. Une tache d'impression est
   * demarree et son identifiant est renvoie comme resultat. L'etat de
   * la tache d'impression peut ensuite etre consulte en appelant la methode
   * <code>getPrintStatus</code>. Le resultat d'impression est recuperem
   * a l'aide de la methode <code>getPrintResult</code>.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document.
   * @param dataStream Le flux d'octets avec le contenu des donnees
   *   pour la creation d'un document.
   * @param size La taille en octets du contenu des donnees.
   * 
   * @return Retourne l'identifiant de la tache d'impression ou -1 si une
   *   erreur est survenue lors de l'appel a la methode.
   * 
   * @see #getPrintStatus(int)
   * @see #getPrintResult(int)
   * @see #getPrintResult(int, String)
   */
  public abstract int printFileDiffered(String reportId, InputStream dataStream,
                                        long size);
  
  /**
   * Attend la fin d'execution d'une tache d'impression <code>jobId</code>.
   * Cette methode suspend l'execution de programme jusqu'a ce que la
   * tache d'impression soit terminee.
   * 
   * <p>Si l'impression est effectuee par un service des impressions,
   * cette methode interroge le service d'impression sur l'etat de
   * l'impression. Dans ce cas, le deuxieme parametre <code>sec</code>
   * indique le delai en secondes entre deux interrogations du service.</p>
   * 
   * <p>La methode <code>waitForJob</code> peut etre appelee suite a l'appel
   * a une des methodes <code>prinFileDiffered</code>.</p>
   * 
   * <p>Suite a l'appel a la methode <code>waitForJob</code>, la methode
   * <code>hasSuccess</code> renvoie <i>false</i>, si la creation de document
   * echue ou si la tache d'impression <code>jobId</code> n'existe pas.</p>
   * 
   * @param jobId L'identifiant de la tache d'impression cree suite a l'appel
   *   a la methode <code>printFileDiffered</code>.
   * @param sec Le delai en secondes entre deux interrogations de service
   *   d'impression. Cette valeur doit etre superieur a 0.
   */
  public abstract void waitForJob(int jobId, int sec);

  /**
   * Retourne le flux avec le contenu du resultat de la creation d'un
   * document PDF correspodant a la tache d'impression <code>printJobId</code>.
   * La tache de creation d'un document est demarree suite a l'appel a
   * une des methodes <code>printFileDiffered</code>.
   * 
   * <p>Retourne la valeur <i>null</i> si l'impression a eu des erreurs ou
   * si la tache avec l'identifiant <code>printJobId</code> n'existe plus.</p>
   * 
   * @see #getPrintResult(int, String)
   * @see #printFileDiffered(String, InputStream, long)
   * @see #printFileDiffered(String, String)
   */
  public abstract InputStream getPrintResult(int printJobId);
  
  /**
   * Enregistre dans le fichier local <code>resultFile</code> le resultat
   * de la creation d'un document correspodant a la tache d'impression
   * <code>printJobId</code>. La tache de creation d'un document est
   * demarree suite a l'appel a une des methodes
   * <code>printFileDiffered</code>.
   * 
   * <p>L'appel a la methode <code>hasSuccess</code> de return <i>false</i>
   * si l'impression a eu des erreurs ou si la tache avec l'identifiant
   * <code>printJobId</code> n'existe plus.</p>
   * 
   * @see #getPrintResult(int)
   * @see #printFileDiffered(String, InputStream, long)
   * @see #printFileDiffered(String, String)
   */
  public abstract void getPrintResult(int printJobId, String resultFile);
  
  /**
   * Recupere les informations sur l'etat d'une tache d'impression avec
   * l'identifiant <code>printJobId</code>. La valeur retournee par cette
   * methode peut etre comparee a une des constantes definies dans la
   * classe {@link CktlPrintConst}. L'appel suivant aux methodes
   * <code>has...</code>, <code>is...</code> ou <code>getMessage</code>
   * prend en compte le resultat de cette appel. 
   * 
   * <p>La tache de creation d'un document est demarree suite a l'appel
   * a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see CktlPrintConst
   * @see #hasError()
   * @see #hasWarning()
   * @see #hasSuccess()
   * @see #isPrintError()
   * @see #isPrintWarning()
   * @see #isPrintSuccess()
   * @see #isPrintExists()
   * @see #isPrintInProgress()
   */
  public abstract int getPrintStatus(int printJobId);
  
  /**
   * Retourne l'expression de validation qui peut etre utilisee pour valider
   * les donnees avant la creation d'un document. En general, une expression
   * de validation correspond a&nbsp;: 
   * <br>&nbsp;<br>
   * <table border=0 cellspacing=0 cellpadding=5 align=center class="example"><tr><td><pre>
   * &lt;!DOCTYPE xml-root-element SYSTEM "chemin-fichier-dtd"&gt;</pre></td></tr></table>
   * 
   * <p>C'est le programme de client qui est responsable d'utiliser
   * cette expression lors de preparation des donnees pour l'impression.</p>
   */
  public abstract String getValidationReference(String reportId);
 
  /**
   * Informe le pilote que la tache d'impression avec l'identifiant
   * <code>printJobId</code> peut etre terminee et supprimee du cache des
   * taches.
   * 
   * <p>La tache de creation d'un document est demarree suite a l'appel
   * a une des methodes <code>printFileDiffered</code> ou
   * <code>printFileImmedaite</code>.</p>
   * 
   * @see #cancelPrint(int)
   */
  public abstract void closePrint(int printJobId);
  
  /**
   * Informe le pilote que la tache d'impression avec l'identifiant
   * <code>printJobId</code> peut doit etre annulee. Si la tache d'impression
   * est en cours, elle sera terminee et ensuite supprime de l'ensemble
   * des taches.
   * 
   * <p>La tache de creation d'un document est demarree suite a l'appel
   * a une des methodes <code>printFileDiffered</code> ou
   * <code>printFileImmedaite</code>.</p>
   * 
   * @see #closePrint(int)
   */
  public abstract void cancelPrint(int printJobId);

  /**
   * Indique s'il le format des donnees doit etre valider avant la creation
   * d'un document.
   * 
   * <p>Dans le cas des donnees XML, elles sont validees en utilisant
   * un schema DTD defini avec la maquette d'impression. Dans ce cas,
   * le document XML doit inclure une reference "DTD" obtenu a l'aide
   * de l'appel a la methode <code>getDTDReference</code>.</p>
   * 
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>.</p>
   *  
   * @see #getValidationReference(String) 
   */  
  public abstract void setUseValidation(boolean useValidation);
  
  /**
   * Indique le delai en secondes qu'un document sera concerve son
   * impression. Durant ce temps, le client peut redemander les details
   * sur l'etat de la tache d'impression ou redemander de recevoir le
   * document correspondant.
   *  
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>.</p>
   */
  public abstract void setJobAliveTimeout(long timeout);
  
  /**
   * Indique le URL ou l'accuse de reception sera envoye un fois que
   * l'impression de document est terminee. Aucun AR n'est envoye si
   * la chaine <code>url</code> est vide ou <i>null</i>.
   * 
   * <p>Dans le fonctinnement par defaut, le numero de la tache d'impression
   * est ajoute a la fin de la chaine <code>url</code> et ensuite un appel
   * est fait a cette URL.</p>
   * 
   * <p>Par exemple, soit l'impression est faite par une application
   * WebObjects MonAppli. Soit cette application implemente une
   * direct action <code>printAR</code> pour etre informee de la fin
   * d'une de ses impressions. Soit l'impression est identifiee via
   * le parametre <code>job</code>. Alors le URL peut etre le suivant :
   * 
   * <table border=0 cellspacing=0 cellpadding=5 align=center class="example"><tr><td><pre>
   * http://.../WebObjects/MonAppli.woa/wa/printAR?job=</pre></td></tr></table>
   * </p>
   * 
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>.</p>
   */
  public abstract void setReceiptURL(String url);
  
  /**
   * Definit le format de document qui doit etre cree. La valeur
   * <code>format</code> doit correspondre a une des constantes
   * <code>FORMAT_XXX</code> de la classe {@link CktlPrintConst}.
   * 
   * <p>PDF est le format par defaut pour le document produit. Appeler
   * cette methode uniquement s'il faut redefinir le format.</p>
   * 
   * @see CktlPrintConst
   */
  public abstract void setResultFormat(byte format);
  
  /**
   * Retablie la configuration par defaut de pilote. La configuration
   * de pilote sera celle qu'il avait suite a l'appel a la methode
   * <code>newDefaultInstance</code> ou <code>newInstance</code>.
   * 
   * @see #newDefaultInstance(Hashtable)
   * @see #newInstance(String, Hashtable)
   */
  public abstract void reset();
  
  /**
   * Termine l'operation en cours et libere les ressources occupees
   * par le client.
   * 
   * <p><i>Il n'est pas necessaire d'appeler cette methode
   * explicitement.</i></p>
   */
  public abstract void terminate();

  /**
   * Test si une erreur est survenue lors de l'execution de la derniere
   * operation. Dans ce cas, la methode <code>getMessage</code> renvoie
   * le message d'erreur.
   * 
   * @see #getMessage()
   * @see #hasWarning()
   * @see #hasSuccess()
   */  
  public abstract boolean hasError();
  
  /**
   * Test si un avertissement a ete enregistree lors de l'execution
   * de la derniere operation. Dans ce cas, la methode <code>getMessage</code>
   * retourne le message d'avertissement.
   * 
   * <p>Meme s'il existe des avertissements, l'operation est consideree
   * comme  etant executee avec succes et l'appel a la methode
   * <code>hasSuccess</code> retourne <i>true</i>.</p>
   * 
   * @see #getMessage()
   * @see #hasError()
   * @see #hasSuccess()
   */
  public abstract boolean hasWarning();
  
  /**
   * Test si la derniere operation a ete effectuee avec succes.
   * 
   * @see #hasError()
   * @see #hasWarning()
   */  
  public abstract boolean hasSuccess();
  
  /**
   * Test si une tache de creation d'un document est en cours
   * d'execution. Cette methode peut etre appellee suite a l'appel
   * a la methode <code>getPrintStatus</code>.
   * 
   * <p>La tache de creation d'un document est demarree suite a l'appel
   * a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #getPrintStatus(int)
   */
  public abstract boolean isPrintInProgress();
  
  /**
   * Test si une tache de creation d'un document existe.
   * Cette methode peut etre appellee suite a l'appel a la methode
   * <code>getPrintStatus</code>.
   * 
   * <p>La tache de creation d'un document est demarree et creee suite a
   * l'appel a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #getPrintStatus(int)
   */
  public abstract boolean isPrintExists();
  
  /**
   * Test si une erreur est survenue lors de generation d'un document.
   * Cette methode peut etre appellee suite a l'appel a la methode
   * <code>getPrintStatus</code>.
   * 
   * <p>La difference entre cette methode et la methode
   * <code>hasError</code>, est qu'elle ne reflete que le processus
   * de creation d'un document. Elle ne prend pas en compte les
   * erreurs de transfert des donnees, la communication avec la base de
   * donnees, etc... Toutes ces erreur sont prises en compte par la methode
   * <code>hasError</code>.</p>
   * 
   * <p>La tache de creation d'un document est demarree et creee suite a
   * l'appel a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #hasError()
   * @see #isPrintSuccess()
   * @see #isPrintWarning()
   */
  public abstract boolean isPrintError();
  
  /**
   * Test si un avertissement est enregistree lors de generation d'un
   * document. Cette methode peut etre appellee suite a l'appel
   * a la methode <code>getPrintStatus</code>.
   * 
   * <p>La difference entre cette methode et la methode
   * <code>hasWarning</code>, est qu'elle ne reflete que le processus
   * de creation d'un document. Elle ne prend pas en compte les
   * avertissements generes au cours de transfert des donnees,
   * la communication avec la base de donnees, etc... Tous ces avertissement
   * sont pris en compte par la methode <code>hasWarning</code>.</p>
   * 
   * <p>La tache de creation d'un document est demarree et creee suite a
   * l'appel a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #hasWarning()
   * @see #isPrintError()
   * @see #isPrintSuccess()
   */
  public abstract boolean isPrintWarning();
  
  /**
   * Test si l'operation de generation d'un document a ete executee
   * avec succes. Cette methode peut etre appellee suite a l'appel
   * a la methode <code>getPrintStatus</code>.
   * 
   * <p>La difference entre cette methode et la methode
   * <code>hasSuccess</code>, est qu'elle ne reflete que le processus
   * de creation d'un document. L'etat de transfert de transfert des
   * donnees, de la communication avec la base de donnees, etc., est pris
   * en compte par la methode <code>hasSuccess</code>.</p>
   * 
   * <p>La tache de creation d'un document est demarree et creee suite a
   * l'appel a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #hasSuccess()
   * @see #isPrintError()
   * @see #isPrintWarning()()
   */
  public abstract boolean isPrintSuccess();
  
  /**
   * Renvoie le message correspondant a la derniere operation.
   */
  public abstract String getMessage();
  
  /**
   * Renvoie le code de l'erreur survenu suite a l'execution de la derniere
   * operation. La valeur retournee correspond a une des constantes
   * <code>ERR_XXX</code> de la classe {@link CktlPrintConst}. Retourne
   * <code>ERR_NONE</code> si aucune erreur n'est survenue.
   * 
   * @see CktlPrintConst
   */
  public abstract byte getErrorCode();
  
  /**
   * Retourne la taille en octets du contenu des donnees creees suite
   * a l'appel de la derniere operation. Par exemple, si la derniere
   * operation genere un document, cette methode renvoie la taille
   * du document.
   * 
   * <p>La methode renvoie -1 si aucun document n'a ete cree suite a la
   * derniere operation ou la taille de son contenu ne peut pas etre
   * determinee.</p> 
   */
  public abstract long getContentSize();
  
  /**
   * Retourne le nom qui est associe aux donnees creees suite a la derniere
   * operation. Par exemple, si la derniere operation correspond a la
   * creation d'un document, alors cette methode retourne le nom du
   * fichier (temporaire) dans lequel le document est enregistre.
   * 
   * <p>La methode retourne <i>null</i> si aucun fichier n'est cree ou
   * aucun nom n'est donne au nouveau document.</p>
   */
  public abstract String getContentName();

  /**
   * Retourne l'identifiant de la derniere operation de creation
   * de document. Cette methode peut etre appele immediatement apres une
   * des methodes <code>printXXX</code>.
   * 
   * <p>La methode retourne -1 si la derniere operation executee n'est
   * pas l'operation de creation d'un document.</p>
   */
  public abstract int getJobID();
}
