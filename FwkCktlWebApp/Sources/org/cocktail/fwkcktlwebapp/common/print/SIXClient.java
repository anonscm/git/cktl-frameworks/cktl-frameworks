package org.cocktail.fwkcktlwebapp.common.print;


import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Dictionary;

import org.cocktail.fwkcktlwebapp.common.util.FileCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;
import org.cocktail.fwkcktlwebapp.common.util._imports.CompressedInputStream;
import org.cocktail.fwkcktlwebapp.common.util._imports.CompressedOutputStream;

import fr.univlr.cri.print.SIXPrinter;

/**
 * Implemente les methodes de communication de avec le service d'impression
 * XML. Cette implementation peut etre consideree comme etant de "bas niveau"
 * et elle n'est pas destinee a etre utilise directement dans les applications. 
 * 
 * Lors de l'implementation de l'impression XML dans les applications, ils
 * est conseille d'utiliser les classes implementant l'interface
 * {@link CktlPrinter}, et notamment la classe <code>SIXPrinter</code>. La
 * classe <code>SIXClient</code> peut etre directement utilisee uniquement
 * dans les cas ou les fontionnalites proposees par les implementation de plus
 * haut niveau ne sont pas suffisantes.
 * 
 * <h3>Principe de fontionement</h3>
 * 
 * Le client <code>SIXClient</code> utilise une connexion basee sur le
 * TCP/IP pour echanger les donnees avec le serveur d'impression. Il utilise
 * egalement la classe <code>SIXDescription</code> qui represente l'entete
 * du protocol de communiqation.
 * 
 * <p>Le contenu de la classe <code>SIXDescription</code> est envoye en
 * entete de chaque requete de client et de chaque reponse de serveur.
 * L'entete peut etre suivie d'un flux d'octet des donnees ou du resultat
 * de traitement.</p> 
 * 
 * @see SIXPrinter
 * @see CktlPrinter
 * @see SIXDescription
 */
public class SIXClient {
  /**
   * La chaine vide afin d'eviter la miltiplication de la creation des objets
   * String.
   */
  private static String EmptyString = "";
  
  /**
   * Le socket de communication avec le serveur d'inmpression SIX.
   */
  private Socket connectionSocket;
  
  /**
   * Le flux de reception a partir du serveur.
   */
  private InputStream connectionIn;
  
  /**
   * Le flux d'envoi vers le serveur.
   */
  private OutputStream connectionOut;
  
  /**
   * L'adresse internet du serveur SIX. 
   */
  private String serverHost;
  
  /**
   * Le numero de port d'ecute du serveur SIX.
   */
  private int serverPort;
  
  /**
   * La description de la communication en cours.
   */
  private SIXDescription description;
  
  /**
   * Indique s'il faut utiliser la compression du contenu des documents.
   */
  private boolean useContentCompression;
  
  /**
   * Indique s'il faut utiliser la validation DTD des documents XML.
   */
  private boolean useValidation;
  
  /**
   * Le delai en secondes que le resultat d'impression sera garde sur
   * le serveur.
   */
  private long jobAliveTimeout;
  
  /**
   * Le URL ou la confirmation d'impression sera envoyee.
   * Cette valeur est prise en compte uniquement lors de l'impression
   * differee.
   */
  private String receiptURL;
  
  /**
   * Indique s'il faut afficher les details des messages d'erreurs et
   * des exceptions.
   */
  private boolean logErrors;
  
  /**
   * Le flux representatant la source des donnees envoyees au serveur.
   */
  private InputStream dataStream;
  
  /**
   * La taille de contenu des donnees lu a partir de <code>dataStream</code>.
   * La taille est indiquee en octets. 
   */
  private long dataSize;
  
  /**
   * Le format de document qui doit etre produit.
   */
  private byte resultFormat;
  
  /**
   * Indique s'il faut fermer le flux representant les donnees XML. Ceci
   * est necessaire lorsqu'un flux est ouvert en local.
   * 
   */
  private boolean closeDataSource;
  
  /**
   * Cree un nouveau client d'impression SIX. Le <code>sixServerHost</code>
   * indique l'adresse Internet du serveur d'impression et
   * <code>sixServerPort</code> le numero de son port d'ecure.
   */
  public SIXClient(String sixServerHost, int sixServerPort) {
    this.serverHost = sixServerHost;
    this.serverPort = sixServerPort;
    reset();
  }

  /**
   * Retablie les parametres par defaut du client SIX. Cet appel termine
   * toutes les communication avec le serveur SIX, si elles sont ouvertes.
   */  
  public void reset() {
    terminate();
    resetHeader();
    this.logErrors = false;
    this.receiptURL = EmptyString;
    this.useContentCompression = false;
    this.useValidation = false;
    this.jobAliveTimeout = SIXDescription.DEFAULT_ALIVE_TIMEOUT;
    this.dataStream = null;
    this.dataSize = SIXDescription.SIZE_NO_CONTENT;
    this.resultFormat = CktlPrintConst.FORMAT_PDF;
    this.closeDataSource = false;
  }

  /**
   * Annule tous les parametrages de l'entete SIXDescription.
   */  
  private void resetHeader() {
    this.description = new SIXDescription();
  }

  /**
   * Initialise les objets de communication avec les service d'impression.
   */
  private boolean prepareDialog() {
    resetHeader();
    try {
      initConnection();
      return true;
    } catch(IOException ex) {
      logError(ex);
      markExceptionError(ex, CktlPrintConst.ERR_CONNECT);
    }
    return false;
  }

  /**
   * Indique s'il faut utiliser la compresion des donnees lors de
   * la communication avec le serveur d'impression.
   * 
   * Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>. Par defaut, la compression n'est pas utilisee. 
   */  
  public void setUseContentCompression(boolean useCompression) {
    this.useContentCompression = useCompression;
  }

  /**
   * Indique s'il faut valider les donnees envoy&eacute;es au serveur SIX.
   * Les donnees XML sont validees par le serveur SIX en utilisant
   * un schema DTD defini avec la maquette d'impression. Dans ce cas,
   * le document XML doit inclure une reference "DTD" obtenu a l'aide
   * de l'appel a la methode <code>getDTDReference</code>.
   * 
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>. Par defaut, la validation n'est pas utilisee.</p>
   *  
   * @see #getDTDReference(String)  
   */  
  public void setUseValidation(boolean useValidation) {
    this.useValidation = useValidation;
  }
  
  /**
   * Indique le delai en secondes qu'un document sera concerve sur
   * le serveur apres son impression. Durant ce temps, le client peut
   * re-contacter le serveur pour obtenir les details sur l'etat d'une tache
   * d'impression ou redemander de recevoir ce document.
   *  
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>. Le delai par defaut est defini par la constante
   * SIXDescription.DEFAULT_ALIVE_TIMEOUT (10 min).</p>
   * 
   * @see SIXDescription#DEFAULT_ALIVE_TIMEOUT
   */
  public void setJobAliveTimeout(long timeout) {
    this.jobAliveTimeout = timeout;
  }
  
  /**
   * Indique le URL ou l'accuse de reception sera envoye un fois que
   * l'impression de document est terminee. Aucun AR n'est envoye si
   * la chaine <code>url</code> est vide ou <i>null</i>.
   * 
   * <p>Le serveur SIX ajoute le numero de la tache d'impression a la fin
   * de la chaine <code>url</code> et ensuite fait appel a cette URL.</p>
   * 
   * <p>Par exemple, soit l'impression est faite par une application
   * WebObjects MonAppli. Soit cette application implemente une
   * direct action <code>printAR</code> pour etre informee de la fin
   * d'une de ses impressions. Soit l'impression est identifiee via
   * le parametre <code>job</code>. Alors le URL de AR envoye au serveur
   * peut etre suivant :
   * 
   * <table border=0 cellspacing=0 cellpadding=5 align=center class="example"><tr><td><pre>
   * http://.../WebObjects/MonAppli.woa/wa/printAR?job=</pre></td></tr></table>
   * </p>
   * 
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>. Par defaut, aucun AR n'est envoye. Le AR n'est
   * pas non plus envoye si le mode d'impression est <i>immediate</i>.</p>
   */
  public void setReceiptURL(String url) {
    this.receiptURL = url;
  }
  
  /**
   * Definit le format de document qui doit etre cree. La valeur
   * <code>format</code> doit correspondre a une des constantes
   * <code>FORMAT_XXX</code> de la classe {@link CktlPrintConst}.
   * 
   * <p>PDF est le format par defaut pour le document produit. Appeler
   * cette methode uniquement s'il faut redefinir le format.</p>
   * 
   * @see CktlPrintConst
   */
  public void setResultFormat(byte format) {
    this.resultFormat = format;
  }
  
  /**
   * Indique s'il le client doit afficher les messages d'erreurs sur la
   * sortie standard. Les messages peuvent etre affiches suite aux exceptions
   * survenues pendant le fonctionnement du client.
   * 
   * <p>Cette definition sera valable jusqu'au prochain appel a la methode
   * <code>reset</code>. Par defaut, les messages ne sont pas affichees.</p>
   */
  public void setLogError(boolean logErrors) {
    this.logErrors = logErrors;
  }
  
  /**
   * Definit la source de donnees XML. Les donnees seront lues
   * a partir de flux <code>dataStream</code> et envoyees au serveur
   * lors de l'opration d'impression executee ensuite. Si la valeur
   * <code>dataStream</code> est <i>null</i>, alors aucune donnees
   * n'est envoyee.
   * 
   * @param dataStream Le flux des donnees XML.
   * @param dataSize La taille en octets de contenu des donnees. Ce parametre
   *   peut avoir la valeur <code>SIZE_UNDEFINED</code> de
   *   <code>SIXDescription</code> si la taille est inconnue.
   */
  public void setDataSource(InputStream dataStream, long dataSize) {
    this.dataStream = dataStream;
    if (dataStream == null)
      this.dataSize = SIXDescription.SIZE_NO_CONTENT;
    else
      this.dataSize = dataSize;
  }
  
  /**
   * Definit le chemin de fichier avec les donnees XML. Elles seront lues
   * a partir du fichier <code>xmlFilePath</code> et envoyees au serveur
   * lors de l'opration d'impression executee ensuite. Si la valeur
   * <code>xmlFilePath</code> est <i>null</i>, alors aucune donnees
   * n'est envoyee.
   */
  public void setDataSource(String xmlFilePath) {
    try {
      if (xmlFilePath == null) {
        setDataSource(null, -1);
      } else {
        setDataSource(new FileInputStream(xmlFilePath),
                      FileCtrl.getFileSize(xmlFilePath));
        closeDataSource = true;
      }
    } catch (FileNotFoundException e) {
      logError(e);
      markExceptionError(e, CktlPrintConst.ERR_FILE);
    } 
  }
  
  /**
   * Definit le contenu des donnees XML. Les donnees stockes dans le
   * tableau seront envoyees au serveur lors de l'opration d'impression
   * executee ensuite. Si la valeur <code>dataContent</code> est
   * <i>null</i>, alors aucune donnees n'est envoyee.
   */
  public void setDataSource(byte[] dataContent) {
    if (dataContent == null) 
      setDataSource(null, -1);
    else
      setDataSource(new ByteArrayInputStream(dataContent),
                    dataContent.length);
  }
  
  /**
   * Effectue la creation d'un fichier PDF a partir des donnees
   * envoyees dans un flux d'octets. L'appel a la methode va bloquer
   * l'execution de programme jusqu'a la fin de la tache d'impression.
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document PDF. Cette maquette doit etre enregistree
   *   dans la base de donnees du serveur.
   * 
   * @return Retourne le flux avec le contenu du document PDF genere.
   *   Returne <i>null</i> si le document ne peux pas etre genere ou une
   *   erreur s'est produite.
   */
  public InputStream printFileImmediate(String reportId)
  {
    if (prepareDialog()) {
      // Preparer la description du contenu
      description.operationId = SIXDescription.OP_PRINT_FILE;
      description.printImmediate = true;
      description.contentCompressed = useContentCompression;
      description.templateId = reportId;
      description.validate = useValidation;
      description.jobAliveTimeout = jobAliveTimeout;
      description.receiptURL = EmptyString; // Pas de confirmation en immediat
      description.contentSize = dataSize;
      description.resultFormat = resultFormat;
      try {
        // Effectuer le transfert
        if (sendHeader()) {
          sendContent(dataStream);
          if (closeDataSource) StreamCtrl.forceClose(dataStream);
          if (description.hasSuccess() && receiveHeader())
            return receiveContent();
        }
      } catch (IOException e) {
        logError(e);
        markExceptionError(e, CktlPrintConst.ERR_TRANSFER);
      }
      terminate();
    }
    return null;
  }
  
  /**
   * Effectue la creation d'un fichier PDF a partir des donnees
   * envoyees dans un flux d'octets et enregistre le resultat dans un
   * fichier local. L'appel a la methode sera bloque jusqu'a la fin
   * de la tache d'impression.
   * 
   * <p>L'appel a la methode <code>hasSuccess</code> de
   * <code>SIXDescription</code> renvoie <i>false</i>, si la creation
   * de PDF echoue.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document PDF. Cette maquette doit etre enregistree
   *   dans la base de donnees du serveur.
   * @param resultFile Le chemin de fichier ou le document genere sera
   *   enregistre.
   */
  public void printFileImmediate(String reportId, String resultFile) {
    InputStream resultIn = printFileImmediate(reportId);
    if (resultIn != null) {
      try {
        StreamCtrl.saveContentToFile(resultIn, resultFile, description.contentSize);
      } catch (IOException e) {
        logError(e);
        markExceptionError(e, CktlPrintConst.ERR_FILE);
      }
    }
    terminate();
  }
  
  /**
   * Effectue la generation d'un fichier PDF a partir des donnees
   * envoyees dans un flux d'octets.
   * 
   * <p>L'appel a la methode est non-bloquante. Une tache d'impression est
   * enregistree sur le serveur et son identifiant est renvoie comme
   * resultat. L'etat de la tache d'impression peut ensuite etre consulte
   * en appelant la methode <code>getPrintStatus</code>. Le resultat
   * d'impression est recuperem a l'aide de la methode
   * <code>getPrintResult</code>.</p>
   * 
   * @param reportId L'identifiant de la maquette a utiliser pour la
   *   creation d'un document PDF. Cette maquette doit etre enregistree
   *   dans la base de donnees du serveur.
   * 
   * @return Retourne l'identifiant de la tache d'impression ou -1 si une
   *   erreur est survenue lors de l'appel a la methode.
   * 
   * @see #getPrintStatus(int)
   * @see #getPrintResult(int)
   * @see #getPrintResult(int, String)
   */
  public int printFileDiffered(String reportId) {
    int id = -1;
    if (prepareDialog()) {
      // Preparer la description du contenu
      description.operationId = SIXDescription.OP_PRINT_FILE;
      description.printImmediate = false;
      description.contentCompressed = useContentCompression;
      description.templateId = reportId;
      description.validate = useValidation;
      description.jobAliveTimeout = jobAliveTimeout;
      description.receiptURL = receiptURL;
      description.contentSize = dataSize;
      description.resultFormat = resultFormat;
      try {
        // Effectuer le transfert
        if (sendHeader()) {
          sendContent(dataStream);
          if (closeDataSource) StreamCtrl.forceClose(dataStream);
          if (description.hasSuccess() && receiveHeader())
            id = description.jobId;
        }
      } catch (IOException e) {
        logError(e);
        markExceptionError(e, CktlPrintConst.ERR_TRANSFER);
      }
      terminate();
    }
    return id;
  }
  
  /**
   * Retourne le flux avec le contenu du resultat de la creation d'un
   * document PDF correspodant a la tache d'impression <code>printJobId</code>.
   * La tache de creation d'un document est enregistree sur le serveur suite
   * a l'appel a une des methodes <code>printFileDiffered</code>.
   * 
   * <p>Retourne la valeur <i>null</i> si l'impression a eu des erreurs ou
   * si la tache avec l'identifiant <code>printJobId</code> n'existe plus.</p>
   * 
   * @see #getPrintResult(int, String) 
   */
  public InputStream getPrintResult(int printJobId) {
    if (prepareDialog()) {
      description.operationId = SIXDescription.OP_GET;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      description.contentCompressed = useContentCompression;
      description.jobId = printJobId;
      if (sendHeader()) {
        if (receiveHeader())
          return receiveContent();
      }
      terminate();
    }
    return null;
  }

  /**
   * Enregistre dans le fichier local <code>resultFile</code> le resultat
   * de la creation d'un document PDF correspodant a la tache d'impression
   * <code>printJobId</code>. La tache de creation d'un document est
   * enregistree sur le serveur suite a l'appel a une des methodes
   * <code>printFileDiffered</code>.
   * 
   * <p>L'appel a la methode <code>hasSuccess</code> de
   * <code>SIXDescription</code> return <i>false</i> si l'impression a
   * eu des erreurs ou si la tache avec l'identifiant <code>printJobId</code>
   * n'existe plus.</p>
   * 
   * @see #getPrintResult(int)
   * @see SIXDescription#hasSuccess()
   */
  public void getPrintResult(int printJobId, String resultFile) {
    InputStream resultIn = getPrintResult(printJobId);
    if (resultIn != null) {
      try {
        StreamCtrl.saveContentToFile(resultIn, resultFile, description.contentSize);
      } catch (IOException e) {
        logError(e);
        markExceptionError(e, CktlPrintConst.ERR_FILE);
      }
    }
    terminate();
  }
  
  /**
   * Recupere les informations sur l'etat d'une tache d'impression avec
   * l'identifiant <code>printJobId</code>. L'etat est enregstre dans
   * un objet <code>SIXDescription</code> accessible via la methode
   * <code>description</code>.
   * 
   * <p>Une tache de creation d'un document est enregistree sur le serveur
   * suite a l'appel a une des methodes <code>printFileDiffered</code>.</p>
   * 
   * @see #description()
   * @see SIXDescription
   */
  public void getPrintStatus(int printJobId) {
    if (prepareDialog()) {
      description.operationId = SIXDescription.OP_CHECK;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      description.jobId = printJobId;
      if (sendHeader()) receiveHeader();
      terminate();
    }
  }

  /**
   * Retourne l'expression de validation DTD correspondant a la maquette
   * reportId enregistree sur le serveur. C'est une expression XML qui relie
   * un un schema DTD a un document XML :
   * <br>&nbsp;<br>
   * <table border=0 cellspacing=0 cellpadding=5 align=center class="example"><tr><td><pre>
   * &lt;!DOCTYPE xml-root-element SYSTEM "chemin-fichier-dtd"&gt;</pre></td></tr></table>
   * 
   * <p>La chaine de caracteres envoyee par cette methode peut ensuite etre
   * incluse dans un document XML genere afin de pouvoir effectuer
   * sa validation sur le serveur.</p>
   */
  public String getDTDReference(String reportId) {
    String dtd = EmptyString;
    if (prepareDialog()) {
      description.templateId = reportId;
      description.operationId = SIXDescription.OP_DTD;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      if (sendHeader()) {
        if (receiveHeader())
          dtd = description.dtd;
      }
      terminate();
    }
    return dtd;
  }
  
  /**
   * Test si la maquette avec l'dentifiant <code>reportId</code> est
   * enregistree aupres de la base des maquettes de serveur. Retourne
   * <i>true</i> si la maquette est connue, et <code>false</code> dans
   * le cas contraire.
   */
  public boolean checkTemplate(String reportId) {
    boolean success = false;
    if (prepareDialog()) {
      description.templateId = reportId;
      description.operationId = SIXDescription.OP_TEMPLATE;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      if (sendHeader()) {
        if (receiveHeader())
          success = description.hasSuccess();
      }
      terminate();
    }
    return success;
  }

  /**
   * Test la disponibilite de service d'impression SIX. Retourne le dictionaire
   * avec la description du service.
   * 
   * <p>Retourne <i>null</i> si le service est indisponible.</p>
   */
  public Dictionary checkService() {
    Dictionary dico = null;
    if (prepareDialog()) {
      description.operationId = SIXDescription.OP_SERVICE;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      if (sendHeader()) {
        if (receiveHeader())
          dico = description.complementDico();
      }
      terminate();
    }
    return dico;
  }

  /**
   * Informe le serveur que la tache d'impression avec l'identifiant
   * <code>printJobId</code> peut etre terminee et supprimee du cache
   * des taches geres par le serveur. 
   */
  public void closePrint(int printJobId) {
    if (prepareDialog()) {
      description.operationId = SIXDescription.OP_CLOSE;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      description.jobId = printJobId;
      if (sendHeader()) receiveHeader();
      terminate();
    }
  }

  /**
   * Informe le serveur que la tache d'impression avec l'identifiant
   * <code>printJobId</code> peut etre terminee et supprimee du cache
   * des taches geres par le serveur. 
   */
  public void cancelPrint(int printJobId) {
    if (prepareDialog()) {
      description.operationId = SIXDescription.OP_CANCEL;
      description.contentSize = SIXDescription.SIZE_NO_CONTENT;
      description.jobId = printJobId;
      if (sendHeader()) {
        receiveHeader();
      }
      terminate();
    }
  }

  /**
   * Retourne l'objet <code>SIXDescription</code> qui contient la desription
   * d'une operation d'impression en cours.
   */
  public SIXDescription description() {
    return description;
  }

  /**
   * Initialise une communication avec le serveur d'impression, ouvre les
   * flots d'entree et de sortie pour le transfert des donnees. 
   */  
  private void initConnection() throws IOException {
    if (isConnectionOpen()) terminate();
    connectionSocket = new Socket(serverHost, serverPort);
    connectionIn = connectionSocket.getInputStream();
    connectionOut = connectionSocket.getOutputStream();
  }
  
  /**
   * Termine la communication avec les serveur d'impression. Tous les
   * flots d'entree et de sortie sont fermes.
   * 
   * <p><i>Cette appel n'annule pas des taches d'impression crees pendant
   * la connexion en cours ! Faites appel a la methode
   * <code>closePrint</code> pour terminer une tache d'impression.</i></p>
   * 
   * @see #closePrint(int)
   */
  public void terminate() {
    StreamCtrl.forceClose(connectionSocket);
    StreamCtrl.forceClose(connectionIn);
    StreamCtrl.forceClose(connectionOut);
    connectionSocket = null;
    connectionIn = null;
    connectionOut = null;
  }
  
  /**
   * Envoie le contenue de la description <code>SIXDescription</code> au
   * serveur d'impression.
   */
  private boolean sendHeader() {
    return description.writeToStream(connectionOut);
  }
  
  /**
   * Envoie le contenue des donnees au serveur d'impression.
   */
  private void sendContent(InputStream dataContent) throws IOException {
    if ((!description.hasContent()) || (dataContent == null)) return;
    if (description.contentCompressed) 
      StreamCtrl.writeContentToStream(dataContent,
                           new CompressedOutputStream(connectionOut),
                           description.contentSize);
    else
      StreamCtrl.writeContentToStream(dataContent,
                           connectionOut,
                           description.contentSize);
  }
  
  /**
   * Recoit le contenu de l'objet <code>SIXDescription</code> a partir de
   * serveur d'impression.
   */
  private boolean receiveHeader() {
    return description.readFromStream(connectionIn);
  }

  /**
   * Recoit le flux avec le contenu d'un document PDF genere et envoye par
   * de serveur d'impression. Retourne <i>null</i> si aucun document
   * n'est envoye. 
   */  
  private InputStream receiveContent() {
    if (description.contentSize == SIXDescription.SIZE_NO_CONTENT)
      return null;
    else if (description.contentCompressed)
      return new CompressedInputStream(connectionIn);
    else
      return connectionIn;
  }

  /**
   * Test si la connexion avec le serveur d'impression est etablie.
   */  
  public boolean isConnectionOpen() {
    return connectionSocket != null;
  }

  /**
   * Enregistre le message d'erreur correspondant a l'exception
   * <code>ex</code> dans un objet <code>SIXDescription</code>
   * actuellement gere par le client.
   */  
  private void markExceptionError(Throwable ex, byte errorCode) {
    description.message = ex.getClass().getName();
    if (ex.getMessage() != null)
      description.message += " : "+ex.getMessage();
    description.status = CktlPrintConst.ERROR;
    description.errorCode = errorCode;
  }
  
  /**
   * Affiche le message d'erreur de l'exception <code>e</code>. Aucun
   * message n'est affiche si c'etait interdi via l'appel a la methode
   * <code>setLogError</code>.
   */
  private void logError(Throwable e) {
    if (logErrors && (e != null))
      e.printStackTrace();
  }
}
