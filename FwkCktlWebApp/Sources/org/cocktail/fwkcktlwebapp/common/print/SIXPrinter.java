package org.cocktail.fwkcktlwebapp.common.print;


import java.io.InputStream;
import java.util.Dictionary;
import java.util.Hashtable;

import org.cocktail.fwkcktlwebapp.common.print.CktlPrintConst;
import org.cocktail.fwkcktlwebapp.common.print.CktlPrinter;
import org.cocktail.fwkcktlwebapp.common.print.SIXClient;
import org.cocktail.fwkcktlwebapp.common.print.SIXDescription;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * La classe-pilote d'impression sur le service SIX. Elle defini le pilote
 * de communication avec SIX et implemente les methodes de
 * {@link CktlPrinter}.
 * 
 * <p>Le pilote <code>SIXPrinter</code> utilise les parametres de la
 * configuration suivants (voir la methode <code>initPrinter</code>)&nbsp;:
 * 
 * <p>
 * <table width="95%" border="0" cellspacing="2" cellpadding="3" align="center">
 * <tr align=center class="paramBoxHeader">
 * <td>Parametre</td><td>Definition</td><td>Description</td>
 * </tr>
 * <tr class="paramBoxContents" valign="top">
 * <td align="center"><code>SIX_SERVICE_HOST</code></td>
 * <td align="center">obligatoire</td>
 * <td>Le nom ou l'adresse IP du serveur sur lequel s'execute le
 *   service d'impression SIX.</td>
 * </tr>
 * <tr class="paramBoxContents" valign="top">
 * <td align="center"><code>SIX_SERVICE_PORT</code></td>
 * <td align="center">obligatoire</td>
 * <td>Le numero de port d'ecute des connexions SIX. Cette valeur
 *   est utilisee par le client SIX pour etablir la connexion avec le serveur
 *   et transferer les donnees.</td>
 * </tr>
 * <tr class="paramBoxContents" valign="top">
 * <td align="center"><code>SIX_USE_COMPRESSION</code></td>
 * <td align="center">optionnel</td>
 * <td>Indique s'il faut utiliser la compression des donnees lors de
 *   leur transfert entre le client et le serveur SIX. Par defaut, la
 *   compression n'est pas utilisee. Les valeurs "OUI", "TRUE", "YES" et "1"
 *   sont considerees comme <i>true</i>. Toutes les autres valeurs sont
 *   considerees comme <i>false</i>.</td>
 * </tr>
 * </table>
 * </p>
 * 
 * <p><i><b>Attention !</b> La casse des noms des parametres doit etre
 * respectee !</i>. Ces parametres doivent etre passes dans un dictionnaire
 * pour la methode <code>newDefaultInstance</code> definie dans
 * {@link CktlPrinter}.</p>
 * 
 * <p>Une instance de cette classe ne doit pas etre cree directement.
 * Uitlisez les methodes statiques de la classe <{@link CktlPrinter}
 * pour creer une instance de ce pilote. Adressez-vous a la documentation
 * de la classe {@link CktlPrinter} pour les exemples de code.</p>
 * 
 * <p>Cette classe utilise les classes <code>SIXClient</code> et
 * <code>SIXDescription</code> pour donner l'implementation des methodes
 * abstraites de {@link CktlPrinter}.</p>
 * 
 * @see CktlPrinter
 * @see SIXClient
 * @see SIXDescription
 * @see CktlPrinter#newDefaultInstance(Hashtable)
 */
public class SIXPrinter extends CktlPrinter {
  private SIXClient client;
  private Hashtable config;
  
  public SIXPrinter() {
    client = null;
  }
  
  public void initPrinter(Hashtable config) {
    this.config = config;
    this.client = new SIXClient((String)config.get("SIX_SERVICE_HOST"),
                        StringCtrl.toInt((String)config.get("SIX_SERVICE_PORT"),
                        -1));
    this.reset();
  }

  public Dictionary checkService() {
    return (client==null)?null:client.checkService();
  }
  
  public boolean checkTemplate(String reportId) {
    return (client == null)?false:client.checkTemplate(reportId);
  }
  
  public InputStream printFileImmediate(String reportId, String dataFile) {
    if (client != null) {
      client.setDataSource(dataFile);
      if (!client.description().hasError())
        return client.printFileImmediate(reportId);
    }
    return null;
  }
  
  public InputStream printFileImmediate(String reportId, InputStream dataStream, long size) {
    if (client != null) {
      client.setDataSource(dataStream, size);
      if (!client.description().hasError())
        return client.printFileImmediate(reportId);
    }
    return null;
  }
  
  public void printFileImmediate(String reportId, String dataFile, String resultFile) {
    if (client != null) {
      client.setDataSource(dataFile);
      if (!client.description().hasError()) {
        client.printFileImmediate(reportId, resultFile);
      }
    }
  }

//  public void printFileImmediate(String reportId, String dataFile, String resultFile) {
//    if (client != null) {
//      client.setDataSource(dataFile);
//      if (!client.description().hasError())
//        client.printFileImmediate(reportId, resultFile);
//    }
//  }

  public void printFileImmediate(String reportId, InputStream dataStream, long size, String resultFile) {
    if (client != null) {
      client.setDataSource(dataStream, size);
      if (!client.description().hasError())
        client.printFileImmediate(reportId, resultFile);
    }
  }
  
  public int printFileDiffered(String reportId, String dataFile) {
    if (client != null) {
      client.setDataSource(dataFile);
      if (!client.description().hasError())
        return client.printFileDiffered(reportId);
    }
    return -1;
  }
  
  public int printFileDiffered(String reportId, InputStream dataStream, long size) {
    if (client != null) {
      client.setDataSource(dataStream, size);
      if (!client.description().hasError())
        return client.printFileDiffered(reportId);
    }
    return -1;
  }
  
  public void waitForJob(int jobId, int sec) {
    if (sec <= 0)
      throw new IllegalArgumentException("Le parametre \"sec\" doit etre superieur a 0");
    int waitInterval = sec*1000;
    synchronized(this) {
      do {
        try { wait(waitInterval); } catch(Exception e) {}
        getPrintStatus(jobId);
      } while(isPrintInProgress());
    }
  }

  public InputStream getPrintResult(int printJobId) {
    return (client == null)?null:client.getPrintResult(printJobId);
  }
  
  public void getPrintResult(int printJobId, String resultFile) {
    if (client != null)
      client.getPrintResult(printJobId, resultFile);
  }
  
  public int getPrintStatus(int printJobId) {
    if (client == null) return CktlPrintConst.ERROR;
    client.getPrintStatus(printJobId);
  	return client.description().jobStatus;
  }
  
  public String getValidationReference(String reportId) {
    return (client == null)?"":client.getDTDReference(reportId);
  }

  public void closePrint(int printJobId) {
    if (client != null)
      client.closePrint(printJobId);
  }
  
  public void cancelPrint(int printJobId) {
    if (client != null)
      client.cancelPrint(printJobId);
  }

  public void setUseContentCompression(boolean useCompression) {
    if (client != null)
      client.setUseContentCompression(useCompression);
  }
  
  public void setUseValidation(boolean useValidation) {
    if (client != null)
      client.setUseValidation(useValidation);
  }
  
  public void setJobAliveTimeout(long timeout) {
    if (client != null)
      client.setJobAliveTimeout(timeout);
  }
  
  public void setReceiptURL(String url) {
    if (client != null)
      client.setReceiptURL(url);
  }
  
  public void setResultFormat(byte format) {
    if (client != null)
      client.setResultFormat(format);
  }

  public void reset() {
    if (client != null) {
      client.reset();
      client.setUseContentCompression(
        StringCtrl.toBool((String)config.get("SIX_USE_COMPRESSION")));
    } 
  }
  
  public void terminate() {
    if (client != null) client.terminate();
  }

  public boolean hasError() {
    return (client == null)?true:client.description().hasError();
  }
  
  public boolean hasWarning() {
    return (client == null)?false:client.description().hasWarning();
  }
  
  public boolean hasSuccess() {
    return (client == null)?false:client.description().hasSuccess();
  }
  
  public boolean isPrintInProgress() {
    if (client == null) return false;
      else return (client.description().jobStatus == CktlPrintConst.IN_PROGRESS);
  }

  public boolean isPrintExists() {
    if (client == null) return false;
      else return (client.description().jobStatus != CktlPrintConst.NOT_FOUND);
  }

  public boolean isPrintError() {
    if (client == null) return true;
      else return (client.description().jobStatus == CktlPrintConst.ERROR);
  }

  public boolean isPrintWarning() {
    if (client == null) return false;
    else return (client.description().jobStatus == CktlPrintConst.WARNING);
  }

  public boolean isPrintSuccess() {
    if (client == null) return false;
      else return ((client.description().jobStatus == CktlPrintConst.SUCCESS) ||
                   (client.description().jobStatus == CktlPrintConst.WARNING));
  }

  public String getMessage() {
    if (client == null) return "Le client n'est pas initialise";
      else return client.description().getMessage();
  }

  public byte getErrorCode() {
    if (client == null) return CktlPrintConst.ERR_NO_CLIENT;
      else return client.description().errorCode;
  }
  
  public long getContentSize() {
    if (client == null) return -1;
    long size = client.description().contentSize; 
    if ((size == SIXDescription.SIZE_NO_CONTENT) ||
        (size == SIXDescription.SIZE_UNDEFINED))
      return -1;
    return size;
  }

  public String getContentName() {
    if (client == null) return null;
    return client.description().contentName;
  }

  public int getJobID() {
    if (client == null) return -1;
      else return client.description().jobId;
  }
}
