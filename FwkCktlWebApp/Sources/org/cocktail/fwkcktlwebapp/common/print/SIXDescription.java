package org.cocktail.fwkcktlwebapp.common.print;


import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.Properties;

import org.cocktail.fwkcktlwebapp.common.util.StreamCtrl;


/**
 * Represente la description d'un etat d'une operation d'impression. Elle
 * comporte egalement les definitions des contantes utilisees lors de
 * l'impression.
 * 
 * <p>Cette classe represente egalement une entete du protocole utilise
 * pour la communication entre le client et le serveur SIX.</p> 
 * 
 * <p>Tous les champs de cette classe sont publiques. Il n'est pas conseille
 * de les modifier directement, mais utiliser les classes "clientes" pour
 * la gestion des champs de cette classe.</p>
 * 
 * @see SIXClient
 */
public class SIXDescription
  implements Serializable
{
  /**
   * Le temps que le resultat d'une impression est gardee sur
   * le serveur d'impression. La valeur est en secondes.
   * Cette valeur est utilisee pour le champ <code>jobAliveTimeout</code>.
   */
  public static final long DEFAULT_ALIVE_TIMEOUT = 600;
  
  /**
   * Le code d'une operation : creation du fichier PDF.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_PRINT_FILE = 0;
  
  /**
   * Le code d'une operation : impression sur une imprimante.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_PRINT_PRINTER = 1;
  
  /**
   * Le code d'une operation : creation du fichier PDF et
   * impression sur une imprimante.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_PRINT_MIXED = 2;
  
  /**
   * Le code d'une operation : verification d'etat d'une impression.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_CHECK = 3;

  /**
   * Le code d'une operation : recuperation du resultat d'une operation
   * d'impression.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_GET = 4;
  
  /**
   * Le code d'une operation : recuperation d'une definitino DTD.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_DTD = 5;

  /**
   * Le code d'une operation : fermeture d'une tache d'impression.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_CLOSE = 6;
  
  /**
   * Le code d'une operation : annulation d'une tache d'impression.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_CANCEL = 7;
  
  /**
   * Le code d'une operation : verification si une maquette existe.
   * Cette valeur est utilisee pour le champ <code>operationId</code>.
   */
  public static final int OP_TEMPLATE = 8;
  
  /**
   * Le code d'une operatino : verification de la disponibilite de service.
   */
  public static final int OP_SERVICE = 9;
  
  /**
   * Indique que les donnees ne seront pas envoyees suite a l'envoi
   * d'une entete pendant la communication entre le client et le serveur.
   * Cette valeur est utilisee pour le champ <code>contentSize</code>.
   */
  public static final long SIZE_NO_CONTENT = -1;
  
  /**
   * Indique que la taille des donnees envoyees lors de la communication
   * entre le client et le serveur est inconnue.
   * Cette valeur est utilisee pour le champ <code>contentSize</code>.
   */
  public static final long SIZE_UNDEFINED = Long.MIN_VALUE;
  
  /**
   * Le code de l'operation en cours. C'est une des constantes
   * <code>OP_XXX</code>.
   */
  public int operationId;
  
  /**
   * L'identifiant de l'imprimante sur laquelle le document sera imprime.
   */
  public String printerId;
  
  /**
   * Le URL ou un accuse de reception sera envoye a la fin d'une tache
   * d'impression.
   */
  public String receiptURL;
  
  /**
   * L'identifiant de la maquette qui sera utilisee pour la creation
   * d'un document PDF ou l'impression sur une imprimante.
   */
  public String templateId;
  
  /**
   * La taille en octets des donnees echangees entre le serveur et le client.
   * Cette valeur ne comprend pas la taille de l'entete. Ce champ peut
   * egalement contenir une des valeurs <code>SIZE_NO_CONTENT</code> ou
   * <code>SIZE_UNDEFINED</code>.
   */
  public long contentSize;

  /**
   * L'identifiant d'une tache d'impression pour une operation en cours.
   */
  public int jobId;
  
  /**
   * L'etat d'une tache d'impression en cours.
   */
  public int jobStatus;
  
  /**
   * Le delai en secondes qu'une tache d'impression terminee sera gardee
   * dans le cache du serveur.
   */
  public long jobAliveTimeout;
  
  /**
   * L'etat de l'operation d'impression en cours.
   */
  public int status;
  
  /**
   * Le message correspondant a l'etat actuel d'une operation d'impression.
   * Dans le cas d'une erreur, ce champs contient le message d'erreur. 
   */
  public String message;
  
  /**
   * Le nom du fichier correspondant au resultat d'impression. 
   */
  public String contentName;
  
  /**
   * La definition pour la validation DTD.
   */
  public String dtd;
  
  /**
   * Indication si le serveur doit effectuer la validation des donnees
   * avant la creation du resultat.
   */
  public boolean validate;
  
  /**
   * Indique si les donnees doivent etre compressees lors de leur
   * transfert entre le client et le serveur.
   */
  public boolean contentCompressed;
  
  /**
   * Indique si le resultat d'impression doit etre immediatement retourne
   * au client (<i>true</i>) ou si une tache d'impression doit etre
   * enregistree sur le serveur pour une impression ulterieure.
   */
  public boolean printImmediate;
  
  /**
   * Le format de document qui doit etre cree. Cette valeur doit
   * correspondre a une des valeurs definies par les constantes
   * <code>FORMAT_XXX</code> de la classe {@link CktlPrintConst}.
   */
  public byte resultFormat; 
  
  /**
   * Le code de l'erreur.
   */
  public byte errorCode;
  
  /**
   * Les informations complementaires. A utiliser par les versions
   * ulterieures de SIXDescription afin de concerver la compatibilite
   * avec sa premiere version.
   */
  public String complement;
  
  /**
   * Construit une nouvelle description et initialise les valeurs par defaut.
   */
  public SIXDescription() {
    status = CktlPrintConst.SUCCESS;
    operationId = OP_PRINT_FILE;
    printerId = "";
    receiptURL = "";
    templateId = "";
    contentSize = SIZE_NO_CONTENT;
    jobId = -1;
    jobStatus = CktlPrintConst.IN_PROGRESS;
    jobAliveTimeout = DEFAULT_ALIVE_TIMEOUT;
    message = "";
    contentName = "";
    dtd = "";
    validate = false;
    contentCompressed = false;
    printImmediate = true;
    resultFormat = CktlPrintConst.FORMAT_PDF;
    complement = "";
    errorCode = CktlPrintConst.ERR_OK; 
  }

  /**
   * Lit le contenu de la description a partir d'un flux <code>in</code>.
   * Retourne <i>true</i> dans le cas de succes, et <i>false</i> dans
   * le cas contraire.
   */
  public boolean readFromStream(InputStream in) {
    try {
      DataInputStream dataIn = new DataInputStream(in);
      // Les donnees envoyees par la version 1 de SIXDescription
      operationId = dataIn.readInt();
      printerId = StreamCtrl.readStringFromStream(dataIn);
      receiptURL = StreamCtrl.readStringFromStream(dataIn);
      templateId = StreamCtrl.readStringFromStream(dataIn);
      contentSize = dataIn.readLong();
      jobId = dataIn.readInt();
      jobStatus = dataIn.readInt();
      jobAliveTimeout = dataIn.readLong();
      status = dataIn.readInt();
      message = StreamCtrl.readStringFromStream(dataIn);
      contentName = StreamCtrl.readStringFromStream(dataIn);
      dtd = StreamCtrl.readStringFromStream(dataIn);
      validate = dataIn.readBoolean();
      contentCompressed = dataIn.readBoolean();
      printImmediate = dataIn.readBoolean();
      resultFormat = dataIn.readByte();
      errorCode = dataIn.readByte();
      complement = StreamCtrl.readStringFromStream(dataIn);
      return true;
    } catch(Throwable ex) {
      ex.printStackTrace();
      message = ex.getMessage();
      status = CktlPrintConst.ERROR;
      errorCode = CktlPrintConst.ERR_TRANSFER;
      return false;
    }
  }

  /**
   * Ecrit le contenu de la description dans un flux <i>out</i>.
   * Retourne <i>true</i> dans le cas de succes, et <i>false</i> dans
   * le cas contraire.
   */
  public boolean writeToStream(OutputStream out) {
    try {
      DataOutputStream dataOut = new DataOutputStream(out);
      dataOut.writeInt(operationId);
      StreamCtrl.writeStringToStream(printerId, dataOut);
      StreamCtrl.writeStringToStream(receiptURL, dataOut);
      StreamCtrl.writeStringToStream(templateId, dataOut);
      dataOut.writeLong(contentSize);
      dataOut.writeInt(jobId);
      dataOut.writeInt(jobStatus);
      dataOut.writeLong(jobAliveTimeout);
      dataOut.writeInt(status);
      StreamCtrl.writeStringToStream(message, dataOut);
      StreamCtrl.writeStringToStream(contentName, dataOut);
      StreamCtrl.writeStringToStream(dtd, dataOut);
      dataOut.writeBoolean(validate);
      dataOut.writeBoolean(contentCompressed);
      dataOut.writeBoolean(printImmediate);
      dataOut.writeByte(resultFormat);
      dataOut.writeByte(errorCode);
      StreamCtrl.writeStringToStream(complement, dataOut);
      dataOut.flush();
      return true;
    } catch(Throwable ex) {
      ex.printStackTrace();
      message = ex.getMessage();
      status = CktlPrintConst.ERROR;
      errorCode = CktlPrintConst.ERR_TRANSFER;
      return false;
    }
  }

  /**
   * Convertie les informations complementaires enregistrees dans le champ
   * en un dictionaire Java. Retourne le dictionaire vide si aucune si le
   * champ complementaire ne contient aucune information.
   */
  public Dictionary complementDico() {
    Hashtable dico = new Properties();
    if (complement.length() > 0) {
      try {
        ((Properties)dico).load(new ByteArrayInputStream(complement.getBytes()));
      } catch(IOException e) { }
    }
    return dico;
  }
  
  /**
   * Test si l'operation en cours correspond a une operation d'impression
   * sur une imprimante ou de la creation d'un document PDF.
   */
  public boolean isPrintOperation() {
    return ((operationId == OP_PRINT_FILE) ||
            (operationId == OP_PRINT_PRINTER) ||
            (operationId == OP_PRINT_MIXED));
  }

  /**
   * Test si l'operation en cours est une operation de recuperation
   * d'un resultat d'un impression.
   */
  public boolean isGetOperation() {
    return ((isPrintOperation() && printImmediate) ||
            (operationId == OP_GET));
  }
  
  /**
   * Test si les donnees sont transferees avec cette descrioption.
   */
  public boolean hasContent() {
    return ((contentSize == SIZE_UNDEFINED) || (contentSize > 0));
  }

  /**
   * Test si la derniere operation a ete effectuee avec succes.
   */  
  public boolean hasSuccess() {
    return ((status == CktlPrintConst.SUCCESS) ||
            (status == CktlPrintConst.WARNING));
  }
  
  /**
   * Test si une tache d'impression correspondant au champ <code>jobId</code>
   * est en cours d'execution.
   */
  public boolean isPrintJobInProgress() {
    return (jobStatus == CktlPrintConst.IN_PROGRESS);
  }
  
  /**
   * Renvoie le message correspondant a la derniere operation. Si une erreur
   * survient pendant l'execution de l'operation, cette methode renvoie
   * le message d'erreur.
   */
  public String getMessage() {
    if ((message == null) || (message.length() == 0))
      return "Success";
    else
      return message;
  }

  /**
   * Test si une erreur est survenue lors de l'execution de l'operation
   * en cours. Dans ce cas, la methode <code>getMessage</code> renvoie
   * le message d'erreur.
   * 
   * @see #getMessage()
   */  
  public boolean hasError() {
    return (status == CktlPrintConst.ERROR);
  }
  
  /**
   * Test si un avertissement a ete enregistree lors de l'execution
   * de l'operation en cours. Dans ce cas, la methode <code>getMessage</code>
   * retourne le message d'avertissement.
   * 
   * <p>Meme s'il existe des avertissements, l'operation est consideree
   * comme  etant executee avec succes et l'appel a la methode
   * <code>hasSuccess</code> retourne <i>true</i>.</p>
   * 
   * @see #getMessage()
   */
  public boolean hasWarning() {
    return (status == CktlPrintConst.WARNING);
  }

  /**
   * Test si la taille de donnees trasnferees entre le client et le serveur
   * est connue.
   */
  public boolean isContentSizeKnown() {
    return !((contentSize == SIZE_UNDEFINED) ||
             (contentSize == SIZE_NO_CONTENT));
  }
  
  /**
   * Retourne la presentation d'un objet SIXDescription comme une chaine
   * de caracteres.
   */
  public String toString() {
    StringBuffer sb = new StringBuffer("SIXDescription(");
    sb.append("operation=").append(operationId).append(", ");
    sb.append("printerId=").append(printerId);
    sb.append(")");
    return sb.toString();
  }
}
