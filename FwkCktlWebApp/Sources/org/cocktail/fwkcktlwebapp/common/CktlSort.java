package org.cocktail.fwkcktlwebapp.common;


import java.util.StringTokenizer;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSSelector;

/**
 * Gere les definitions de tri d'objets pour les requetes EOF. Les methodes
 * de cette classe permettent de simlifier la definition de tri et creer
 * les tableaux <code>NSArray</code> contenant les instances de la classe
 * <code>EOSortOrdering</code>.
 * 
 * <p>Typiquement, on utilise le code suivant pour cree une defintion :
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * NSMutableArray sort = new NSMutableArray();
 * sort.addObject(EOSortOrdering.sortOrderingWithKey("attribut1", EOSortOrdering.CompareAscending);
 * sort.addObject(EOSortOrdering.sortOrderingWithKey("attribut2", EOSortOrdering.CompareDescending);
 * sort.addObject(EOSortOrdering.sortOrderingWithKey("attribut3", EOSortOrdering.CompareDescending);
 * EOQualifier qualif = ... <code class="comment">// Definition de condition de selection;</code>
 * <code class="comment">// On suppose qu'on utilise une classe de type CktlDataBus</code>
 * NSArray objects = myDataBus.fetchArray("MaTable", qualif, sort);</code></pre></td></tr></table>
 * </p>
 * 
 * <p>
 * La meme solution utilisant la classe <code>CktlSort</code> serait :
 * <table border=0 cellspacing=0 cellpadding=5 width=95% align=center class="example"><tr><td><pre>
 * EOQualifier qualif = ... <code class="comment">// Definition de condition de selection</code>
 * <code class="comment">// On suppose qu'on utilise une classe de type CktlDataBus</code>
 * NSArray objects = myDataBus.fetchArray("MaTable", qualif,
 *    CktlSort.newSort("attribut1").arrayByAddingObject("attribut2, atribut3", CktlSort.Descending).getSort());</code></pre></td></tr></table>
 * </p>
 * 
 * <p>La plupart des methodes de cette classe retournent les references vers
 * leur instance de l'objet CktlSort. Leurs appels peuvent donc etre enchaines
 * l'un apres l'autre.</p>
 *
 * @author Arunas STOCKUS <arunas.stockus at univ-lr.fr>
 */
public class CktlSort extends NSMutableArray {
  /**
   * La contant indiquant le tri dans l'ordre croissant. Elle correspond
   * a la constante <code>EOSortOrdering.CompareAscending</code>.
   */
  public static final int Ascending = 0; // EOSortOrdering.CompareAscending;
  
  /**
   * La contant indiquant le tri dans l'ordre decroissant. Elle correspond
   * a la constante <code>EOSortOrdering.CompareDescending</code>.
   */
  public static final int Descending = 1; // EOSortOrdering.CompareDescending;
  
  /**
   * La contant indiquant le tri dans l'ordre croissant en ignorant la case
   * des caracteres. Elle correspond a la constante
   * <code>EOSortOrdering.CompareCaseInsensitiveAscending</code>.
   */
  public static final int AscendingNoCase = 2; // EOSortOrdering.CompareCaseInsensitiveAscending;

  /**
   * La contant indiquant le tri dans l'ordre decroissant en ingnorant la case
   * des caracteres. Elle correspond a la constante
   * <code>EOSortOrdering.CompareCaseInsensitiveDescending</code>.
   */
  public static final int DescendingNoCase = 3; // EOSortOrdering.CompareCaseInsensitiveDescending;
  

  
  /**
   * Cree une nouvelle instance d'une definition de tri vide.
   */
  public CktlSort() {
    super();
  }
  
  /**
   * Retourne un objet NSSeletor correspondant a l'ordre de tri
   * <code>order</code>. Ce parametre doit contenir une des constantes
   * Ascending, Descending, AscendingNoCase ou DescendingNoCas.
   */
  private static NSSelector getSelectorForOrdre(int order) {
    switch (order) {
      case Descending:
        return EOSortOrdering.CompareDescending;
      case AscendingNoCase:
        return EOSortOrdering.CompareCaseInsensitiveAscending;
      case DescendingNoCase:
        return EOSortOrdering.CompareCaseInsensitiveDescending;
      default: // Par defaut, on considere que c'est croissant
        return EOSortOrdering.CompareAscending;
    }
  }
  
  /**
   * Cree et retourne une nouvelle instance d'une definition de tri.
   * Cette definition initialement est vide.
   * 
   * @see #newSort(String)
   */
  public static CktlSort newSort() {
    return new CktlSort();
  }
  
  /**
   * Cree et retourne une nouvelle instance d'une definition de tri. Elle
   * est initialisee a la definition de tri par un ou plusieurs
   * attributs <code>sortKeys</code> dans l'ordre croissant. Si
   * <code>sortKeys</code> contient plusieurs attributs, alors ils
   * doivent etre separes par une virgule.
   */
  public static CktlSort newSort(String sortKeys) {
    return newSort().add(sortKeys);
  }
  
  /**
   * Cree et retourne une nouvelle instance d'une definition de tri. Elle
   * est initialisee a la definition de tri par un ou plusieurs attributs
   * <code>keys</code>. Le parametre <code>order</code> indique l'ordre
   * de tri : croissant, decroissant. Voir les constantes
   * <code>CompareXXX</code> de la classe <code>EOSortOrdering</code>.
   * 
   * <p>Si le parametre <code>keys</code> contient plusieurs attributs,
   * alors ils doivent etre separes par une virgule.</p>
   * 
   * @see EOSortOrdering
   */
  public static CktlSort newSort(String keys, int order) {
    return newSort().add(keys, order);
  }

  /**
   * Cree et retourne une nouvelle instance d'une definition de tri. Elle
   * est initialisee a la definition de tri par un ou plusieurs attributs
   * <code>keys</code>. Le parametre <code>order</code> indique l'ordre
   * de tri : croissant, decroissant. Voir les constantes
   * <code>AscendingXXX</code> et <code>DescendingXXX</code> pour les valeurs
   * possibles de ce parametre.
   * 
   * <p>Si le parametre <code>keys</code> contient plusieurs attributs,
   * alors ils doivent etre separes par une virgule.</p>
   * 
   * @see EOSortOrdering
   */
  public static CktlSort newSort(String keys, NSSelector order) {
    return newSort().add(keys, order);
  }

  /**
   * Ajoute une nouvelle definition de tri par un ou plusieurs attributs
   * <code>keys</code> dans l'ordre croissant. Si <code>keys</code>
   * contient plusieurs attributs, alors ils doivent etre separes par les
   * virgules. 
   * 
   * @see #add(String, NSSelector)
   */
  public CktlSort add(String keys) {
    return add(keys, Ascending);
  }

  /**
   * Ajoute une nouvelle definition de tri par un ou plusieurs attributs
   * <code>keys</code>. Le parametre <code>order</code> indique
   * l'ordre de tri : croissant, decroissant. Voir les constantes
   * <code>AscendingXXX</code> et <code>DescendingXXX</code> pour les valeurs
   * possibles de ce parametre.
   * 
   * <p>Si le parametre <code>keys</code> contient plusieurs attributs,
   * alors ils doivent etre separes par une virgule.</p>
   * 
   * @see #add(String)
   * @see #add(String, NSSelector)
   * @see EOSortOrdering
   */
  public CktlSort add(String keys, int order) {
    return add(keys, getSelectorForOrdre(order));
  }

  /**
   * Ajoute une nouvelle definition de tri par un ou plusieurs attributs
   * <code>keys</code>. Le parametre <code>order</code> indique
   * l'ordre de tri : croissant, decroissant. Voir les constantes
   * <code>CompareXXX</code> de la classe <code>EOSortOrdering</code>.
   * 
   * <p>Si le parametre <code>keys</code> contient plusieurs attributs,
   * alors ils doivent etre separes par une virgule.</p>
   * 
   * @see #add(String)
   * @see #add(String, int)
   * @see EOSortOrdering
   */
  public CktlSort add(String keys, NSSelector order) {
    if (StringCtrl.normalize(keys).length() >= 0) {
      StringTokenizer st = new StringTokenizer(keys, ",");
      while(st.hasMoreTokens())
        addObject(
            EOSortOrdering.sortOrderingWithKey(st.nextToken().trim(), order));
    }
    return this;
  }
  
  /**
   * Trie les objets de tableau <code>objects</code> suivant les cles
   * (attributs) donnees dans la chaine <code>keys</code>. Si plusieurs
   * attributs doivent etre utilises, ils doivent etre separes par
   * une virgule.
   * 
   * <p>Le parametre <code>order</code> indique l'ordre de tri.</p>
   * 
   * <p>Le resultat du tri est enregistre dans le meme tableau
   * <code>objects</code>.</p>
   */
  public static void sortArray(NSMutableArray objects, String keys, int order) {
    // On ne tri pas si les cles de tri ne sont pas donnees
    if (StringCtrl.normalize(keys).length() >= 0) {
      EOSortOrdering.sortArrayUsingKeyOrderArray(
          objects, CktlSort.newSort(keys, order));
    }
  }

  /**
   * Trie les objets de tableau <code>objectArray</code> suivant les cles
   * (attributs) donnees dans la chaine <code>keys</code>. Si plusieurs
   * attributs doivent etre utilises, ils doivent etre separes par
   * une virgule.
   * 
   * <p>Les objets sont tries dans l'ordre croissant.</p>
   * 
   * <p>Le resultat du tri est enregistre dans le meme tableau
   * <code>objectArray</code>.</p>
   */
  public static void sortArray(NSMutableArray objectArray, String keys) {
    sortArray(objectArray, keys, Ascending);
  }

  /**
   * Trie les objets de tableau <code>objects</code> suivant les cles
   * (attributs) donnees dans la chaine <code>keys</code>. Si plusieurs
   * attributs doivent etre utilises, ils doivent etre separes par
   * une virgule.
   * 
   * <p>Le parametre <code>order</code> indique l'ordre de tri (par exemple,
   * croissant ou decroissant). S'il est <code>null</code>, alors l'ordre
   * par defaut est utilise (decroissant).</p>
   * 
   * <p>Le resultat de tri est retourne dans un nouveau tableau.</p>
   */
  public static NSArray sortedArray(NSArray objects, String keys, int order)
  {
    if (StringCtrl.normalize(keys).length() == 0)
      return objects;
    else
      return EOSortOrdering.sortedArrayUsingKeyOrderArray(
          objects, CktlSort.newSort(keys, order));
  }

  /**
   * Trie les objets de tableau <code>objects</code> suivant les cles
   * (attributs) donnees dans la chaine <code>keys</code>. Si plusieurs
   * attributs doivent etre utilises, ils doivent etre separes par
   * une virgule.
   * 
   * <p>Les objets sont tries dans l'ordre croissant.</p>
   * 
   * <p>Le resultat de tri est retourne dans un nouveau tableau.</p>
   */
  public static NSArray sortedArray(NSArray objects, String keys) {
    return sortedArray(objects, keys, Ascending);
  }

}
