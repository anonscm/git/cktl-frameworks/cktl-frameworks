package org.cocktail.fwkcktlwebapp.common;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * Cette classe gere l'affichage des messages sur la sortie standard de l'application (les logs). Toutes les methodes de la classe sont statiques et
 * aucune instance ne doit pas etre creee. <br/>
 * A terme cette classe ne doit plus être utilisée, et les appels à cette classe doivent être remplacés par le système Log4j en créant un logger pour
 * chaque classe contenant des logs : <br/>
 * <br/>
 * <code>
 * 
 * public final static Logger logger = Logger.getLogger(Toto.class);<br/>
 * <br/>
 * public void fonction() {<br/>
 * 	[...]<br/>
 * if(logger.isDebugEnabled() { // cette condition n'est pas necessaire mais est sensée améliorer les perfs <br/>
 * 	logger.debug("L'utilisateur est passé par là");<br/>
 * }</br>
 * 	[...]<br/>
 * 
 *  logger.error(e);<br/>
 * }<br/>
 * </code>
 */
public class CktlLog {

	/**
	 * La constante de niveau de verbosite. Elle indique que <i>tous</i> les appels d'affichage des messages seront ignores.
	 */
	public static final int LEVEL_SILENT = 0;

	/**
	 * La constante de niveau de verbosite. Elle indique les appels aux methodes " <code>log</code>
	 * " uniquement seront traites. Les appels aux methodes " <code>trace</code>" sont ignores (les messages "debug").
	 */
	public static final int LEVEL_BASIC = 1;

	/**
	 * La constante de niveau de verbosite. Elle indique que les appels a tous les methodes seront traites, y compris les appels aux "
	 * <code>trace</code> " (les messages "debug).
	 */
	public static final int LEVEL_DEBUG = 2;

	/**
	 * Le niveau de verbosite en cours.
	 */
	private static int currentLevel = LEVEL_BASIC;

	/**
	 * Le prefix pour les messages affiches avec la methode <code>trace</code>.
	 */
	private static String tracePrefix = "++ ";

	/**
	 * Le nom de la classe en cours.
	 */
	private static String className;

	/**
	 * Le constructeur est prive pour empecher la creation d'une instance de la classe. Toutes ces methodes sont statiques et aucune instance ne doit
	 * pas etre creee.
	 */
	private CktlLog() {
	}

	/**
	 * Definit le niveau de la verbosite de <code>CktlLog</code>. Tous les appels des methodes avec le niveau inferieur a <code>level</code> seront
	 * ignores.
	 * <p>
	 * La valeur <code>level</code> doit correspondre a une des constantes LEVEL_<i>XXX</i>. Cette valeur est interpretee de maniere suivante&nbsp;:
	 * <ul>
	 * <li> {@link #LEVEL_SILENT} - <i>tous</i> les appels d'affichage des messages seront ignores&nbsp;;</li>
	 * <li> {@link #LEVEL_BASIC} - les appels aux methodes "<code>log</code>" uniquement seront traites. Les appels aux methodes "<code>trace</code>"
	 * sont ignores (les messages "debug")&nbsp;;</li>
	 * <li> {@link #LEVEL_DEBUG} - les appels a tous les methodes seront traites, y compris les appels aux "<code>trace</code>" (les messages "debug")</li>
	 * </ul>
	 * </p>
	 * <p>
	 * Une seule constante doit etre utilise, i.e., la combinaisont des constantes n'est pas possible. Par defaut, le niveau de verbosite est
	 * {@link #LEVEL_BASIC} .
	 * </p>
	 */
	public static void setLevel(int level) {
		CktlLog.currentLevel = level;
	}

	/**
	 * Connaitre la valeur actuelle du niveau de verbosite de <code>CktlLog</code> .
	 */
	public static int getLevel() {
		return CktlLog.currentLevel;
	}

	/**
	 * Definit le prefix pour les messages affiches avec la methode <code>trace</code>. Attention, si le prefix doit etre separe du message par un
	 * espace, celui-la doit etre inclu dans <code>prefix</code>.
	 */
	public static void setTracePrefix(String prefix) {
		CktlLog.tracePrefix = prefix;
	}

	/**
	 * Effectue l'affichage effective du message sur la sortie standard.
	 */
	/*
	 * private synchronized static void printMessage(String message, int level) { // Modifier cette methode si on souhaite utiliser une autre //
	 * classe pour l'affichage des message. Cette implementation // utilise NSLog (WO5) ou System.out (WO45). // if (currentLevel >= level)
	 * System.out.println(message); if (currentLevel >= level) NSLog.out.appendln(message); }
	 */

	/**
	 * Affiche un message sur la sortie standard sans aucun pre-formatage.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_BASIC}.
	 */
	public static void rawLog(String message) {
		printMessage(message, LEVEL_BASIC);
	}

	/**
	 * <p>
	 * La valeur <code>level</code> indique le niveau de la verbosite dans lequel le message doit etre affiche.
	 */
	public static void rawLog(String message, int level) {
		printMessage(message, level);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le temps de l'appel de cette methode.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_BASIC}.
	 */
	public static void log(String message) {
		log(message, null);
	}

	/**
	 * Affiche un message et un commentaire sur la sortie standard. Le message sera precede par le temps de l'appel de cette methode. Le commentaire
	 * sera affiche sur une nouvelle ligne et precede par le symbole "&gt;&gt; ".
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_BASIC}.
	 */
	public static void log(String message, String comment) {
		// prinMessage fait la meme chose, mais on le refait ici pour
		// eviter de faire les calculs inutils
		if (currentLevel < LEVEL_BASIC)
			return;
		printMessage("[" + DateCtrl.currentDateTimeString() + "] " + message, LEVEL_BASIC);
		if (comment != null)
			printMessage(StringCtrl.trimText(StringCtrl.quoteText(comment, ">> ")), LEVEL_BASIC);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le nom de la classe et de la methode qui a appele la methode
	 * <code>trace</code>.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_DEBUG}.
	 */
	public static void trace(String message) {
		trace(message, false);
	}

	/**
	 * Affiche un message sur la sortie standard. Le message sera precede par le nom de la classe et de la methode qui a appele la methode
	 * <code>trace</code>. Si <code>includeOrigin</code> est <code>true</code>, alors le nom de la methode appelant cette-derniere sera egalement
	 * incluse.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_DEBUG}.
	 */
	public static void trace(String message, boolean withOrigin) {
		// prinMessage fait la meme chose, mais on le refait ici pour
		// eviter de faire les calculs inutils
		if (currentLevel < LEVEL_DEBUG)
			return;
		StringBuffer msb = new StringBuffer();
		msb.append(tracePrefix).append(getCallMethod(withOrigin));
		if (message != null)
			msb.append(" : ").append(message);
		printMessage(msb.toString(), LEVEL_DEBUG);
	}

	/**
	 * Affiche la valeur <code>varValue</code> associee a la valeur <code>varName</code> sur la sortie standard. Affichage a la forme "
	 * <code>variable&nbsp;=&nbsp;valeur</code>".
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_DEBUG}.
	 */
	public static void trace(String varName, Object varValue) {
		trace(varName + " = " + varValue.toString(), false);
	}

	/**
	 * Affiche un message sur la sortie standard sans aucune modification de son format.
	 * <p>
	 * Le niveau de verbosite utilise par cette methode est {@link #LEVEL_DEBUG}.
	 */
	public static void rawTrace(String message) {
		rawLog(message, LEVEL_DEBUG);
	}

	/**
	 * Retourne le message d'erreur de l'exception ou de l'erreur <code>ex</code>. Le message sera suivie par le nom de la classe de l'exception
	 * <code>ex</code>.
	 * <p>
	 * Dans cerains cas une exception ne contient aucun message d'erreur. Dans ce cas, le nom de la classe de l'exception ex seul est retourne.
	 * </p>
	 */
	public static String getMessageForException(Throwable ex) {
		String message = null;
		if (ex != null) {
			message = ex.getClass().getName();
			if (ex.getMessage() != null)
				message += " : " + ex.getMessage();
		}
		return message;
	}

	/**
	 * Retourne une chaine indiquant une classe et une methode de l'enchainement des appels des methodes. Toutes les appels relatives a la classe
	 * <code>CktlLog</code> sont ignores. On retourne le nom de la derniere methode qui a fait appel a une methode de <code>CktlLog</code>. Si
	 * <code>withOrigin</code> est <i>true</i> alors le nom de la methode appelant cette derniere sera egalement inclu.
	 * <p>
	 * Cette methode retourne <i>null</i> si le nom de la methode ne peut pas etre detecte.
	 */
	protected static String getCallMethod(boolean withOrigin) {
		StringWriter sw = new StringWriter();
		(new Throwable()).printStackTrace(new PrintWriter(sw));
		String stack = sw.toString();
		// On detecte le dernier appel a "CktlLog"
		int startPos = stack.lastIndexOf(className()) + className().length();
		// De la, on trouve la methode appelante
		StringBuffer methodName = new StringBuffer();
		methodName.append(extractMethodName(stack, startPos));
		if (withOrigin) {
			methodName.append("\n   ").append(tracePrefix);
			methodName.append(extractMethodName(stack, startPos + 3 + methodName.length()));
		}
		return methodName.toString();
	}

	/**
	 * Retourne le nom de la methode qui se situe dans la position <code>startPos</code> dans l'arborescence des appels des mesthodes
	 * <code>stack</code> ("stack trace"). Retourne null si aucun nom de la metode ne peut pas etre trouvee dans la position indiquee.
	 */
	private static String extractMethodName(String stack, int startPos) {
		startPos = stack.indexOf("at ", startPos) + 3;
		int endPos = stack.indexOf(")", startPos) + 1;
		if (endPos <= startPos)
			return null;
		return stack.substring(startPos, endPos);
	}

	/**
	 * Retourne le nom de la classe dans laquelle se trouve cette methode statique. Cette methode permet de recuperer le nom de la classe meme si son
	 * nom est change ou si une autre classe herite de celle-ci.
	 * <p>
	 * Cette methode permettra d'obtenir le nom de la classe en cours dans le context statique, car les appels aux methodes de type
	 * <code>getClass().getName()</code> ne peuvent pas etre faits dans ce cas.
	 * </p>
	 * <p>
	 * L'implementation de cette methode utilise une classe <i>anonyme</i> interne qui herite de la classe {@link SecurityManager} et fait appel a sa
	 * methode <code>getClassContext</code>.
	 * </p>
	 * <p>
	 * Pour assurer le fonctionnement correct de la methode, elle ne doit pas etre redefinie dans le sous-classes de <code>CktlLog</code>. Pour cette
	 * raison, la methode est declaree <i>final</i>.
	 * </p>
	 */
	public static final String className() {
		if (className == null) {
			// On va utiliser la classe anonyme qui herite de SecurityManages
			className = (new SecurityManager() {
				// La nouvelle methode de la classe anonyme. Elle retourne le nom
				// de la classe ("CktlLog").
				public String getClassName() {
					// [0] - la class anonyme, [1] - la classe englobante
					return getClassContext()[1].getName();
				}
			}).getClassName();
		}
		return className;
	}

	// /**
	// * Retourne le nom de la classe dans laquelle se trouve cette methode
	// * statique. Cette methode permet de recuperer le nom de la classe meme
	// * si son nom est change ou si une autre classe herite de celle-ci.
	// */
	// public static final String className() {
	// if (className == null) className = (new
	// CurrentClassGetter()).getClassName();
	// return className;
	// }
	//
	// /**
	// * Cette classe permettra d'obtenir le nom de la classe en cours dans
	// * les methodes statiques. On ne peut plus faire l'appel a la methode
	// * <code>getClass().getName()</code> dans le context statique.
	// *
	// * <p>Cette methode permettra garder le meme code meme si le nom
	// * de la classe <code>CktlLog</code> est modifie (modification du code
	// * ou l'heritage).
	// */
	// private static class CurrentClassGetter extends SecurityManager {
	// public String getClassName() {
	// return getClassContext()[1].getName();
	// }
	// }

	/**
	 * Interface a definir pour effectuer des logs personnalises. Appeler ensuite {@link #setMyLRLogWriter(ICktlLogWriter)} pour parametrer
	 * {@link CktlLog} avec ce nouveau writer
	 */
	public static interface ICktlLogWriter {
		void appendln(String message);
	}

	private final static Logger logger = Logger.getLogger("CktlLog");

	/**
	 * Le CktlLogWriter par defaut qui utilise NSLog.appendln
	 */
	private static class DefaultCktlLogWriter implements ICktlLogWriter {
		public void appendln(String message) {
			// NSLog.out.appendln(message);
			logger.info(message);
		}
	}

	private static ICktlLogWriter myCktlLogWriter = new DefaultCktlLogWriter();

	/**
	 * @deprecated Redefinir le writter de log par defaut.
	 */
	public static void setMyLRLogWriter(ICktlLogWriter aCktlLogWriter) {
		myCktlLogWriter = aCktlLogWriter;
	}

	/**
	 * Effectue l'affichage du message sur la sortie.
	 */
	private synchronized static void printMessage(String message, int level) {
		if (currentLevel >= level) {
			myCktlLogWriter.appendln(message);
		}
	}
}
