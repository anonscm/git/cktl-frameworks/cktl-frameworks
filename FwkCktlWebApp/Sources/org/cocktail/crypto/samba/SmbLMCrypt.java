package org.cocktail.crypto.samba;

public class SmbLMCrypt extends SmbEncrypt {
	
	static public final String crypt(String password) {
		return LANMANHash(password);
	}

	/**
	 * Effectue la correspondance entre un mot de passe en clair <code>uncrypted</code>
	 * et un mot de passe crypté <code>crypted</code>
	 * 
	 * @param uncrypted
	 * @param crypted
	 * 
	 * @return <em>true</em> si les 2 mots de passes concordent
	 */
	public static boolean match(String uncrypted, String crypted) {
		try {
			return crypted.equals(crypt(uncrypted));
		} catch (Exception e) {
			// le motif de la cle de decryptage n'a pas ete trouve
		}
		return false;
	}
}
