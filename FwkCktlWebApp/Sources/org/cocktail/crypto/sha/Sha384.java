package org.cocktail.crypto.sha;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author jlafourc
 *
 */
public final class Sha384 {
	public final static String FQDN_CLASS_NAME = "org.cocktail.crypto.sha.Sha384";
	private static ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha384Digest());
	
	private Sha384() { }
	
	/**
	 * @param original la chaine originale
	 * @return la chaine cryptée avec SHA-384
	 */
	public static String crypt(String original) {
		return DigestUtils.sha384Hex(original);
	}
	
	/** 
	 * @param uncrypted la chaine non cryptée
	 * @param crypted la chaine crytée
	 * @return true si le mot de passe non-crypté une fois cryté est égal au crypté
	 */
	public static boolean match(String uncrypted, String crypted) {
		return shaUtils.match(uncrypted, crypted);
	}

}
