package org.cocktail.crypto.sha;

import java.security.MessageDigest;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 
 * @author jlafourc
 *
 */
public final class Sha1 {
	public final static String FQDN_CLASS_NAME = "org.cocktail.crypto.sha.Sha1";

	private static ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha1Digest());
	
	private Sha1() { }
	
	/**
	 * @param original la chaine originale
	 * @return la chaine cryptée avec SHA-1
	 */
	public static String crypt(String original) {
		return DigestUtils.sha1Hex(original);
	}
	
	/** 
	 * @param uncrypted la chaine non cryptée
	 * @param crypted la chaine crytée
	 * @return true si le mot de passe non-crypté une fois cryté est égal au crypté
	 */
	public static boolean match(String uncrypted, String crypted) {
		return shaUtils.match(uncrypted, crypted);
	}

}
