package org.cocktail.crypto.sha;

import java.security.MessageDigest;
import java.util.Arrays;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang3.ArrayUtils;

public class ShaUtils {

	private MessageDigest digest;

	public ShaUtils(MessageDigest digest) {
		this.digest = digest;
	}

	/**
	 * @param password le mot de passe en clair
	 * @param encryptedPassword le mdp en cryté en base64  : encodeBase64(digest(passwd + salt) + salt)
	 * @return true si ca matche 
	 */
	public boolean match(String password, String encryptedPassword) {
		// Décryptage depuis la base 64
		byte[] encryptedPasswordArray = new Base64().decode(StringUtils.getBytesUsAscii(encryptedPassword));
		// Récupération de la partie password
		byte[] passwordPayload = ArrayUtils.subarray(encryptedPasswordArray, 0, digest.getDigestLength());
		// Le reste correspond au salt
		byte[] saltPayload = ArrayUtils.subarray(encryptedPasswordArray, digest.getDigestLength(), encryptedPasswordArray.length);
		// On renvoie la comparaison des résultats cryptés
		return Arrays.equals(passwordPayload, crypt(password.getBytes(), saltPayload));
	}

	/**
	 * @param password le mdp à crypter
	 * @param salt le salt
	 * @return une chaine => encodeBase64(digest(passwd + salt) + salt)
	 */
	public String cryptString(String password, String salt) {
		byte[] cryptedPassword = crypt(password.getBytes(), salt.getBytes());
		return Base64.encodeBase64String(ArrayUtils.addAll(cryptedPassword, salt.getBytes()));
	}

	private byte[] crypt(byte[] password, byte[] salt) {
		digest.reset();
		digest.update(password);
		digest.update(salt);
		return digest.digest();
	}

}
