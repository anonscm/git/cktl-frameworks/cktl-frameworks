package org.cocktail.fwkcktlacces.server;

import java.net.HttpURLConnection;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.cocktail.fwkcktlacces.common.network.IP;
import org.cocktail.fwkcktlacces.common.network.Netmask;
import org.cocktail.fwkcktlacces.common.network.Subnet;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.components.CktlAlertPage;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;

/**
 * Cette classe centralise les operations de controle d'acces a l'application.
 * Elle est appelee par tous les handlers cocktails.
 * 
 * @author Ralph BULLET <ralph.bullet at cocktail.org>
 */
public class FwkCktlAccesController {

	public static boolean isInitialized = false;

	// private static Logger log = Logger.getLogger(FwkCktlAcces.class);
	private static CktlWebApplication application = (CktlWebApplication) WOApplication.application();

	private static final String IPV4_HOST_ADDRESS_PATTERN = "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}";
	private static final String IPV4_NETWORK_ADDRESS_PATTERN = IPV4_HOST_ADDRESS_PATTERN + "/[0-9]{1,2}";

	private static final Pattern ipv4HostAddressPattern = Pattern.compile(IPV4_HOST_ADDRESS_PATTERN + "$");
	private static final Pattern ipv4NetworkAddressPattern = Pattern.compile(IPV4_NETWORK_ADDRESS_PATTERN + "$");

	private static List<IP> authorizedHostsAddresses = null;
	private static List<Subnet> authorizedSubnets = null;

	private final static String HTML_TITRE_PAGE_ACCES_INTERDIT =
			"Acc&egrave;s interdit depuis cette machine";
	private final static String HTML_MESSAGE_DEFAULT_ACCES_INTERDIT =
			"Votre machine n'est pas autoris&eacute;e &agrave; acc&eacute;der cette application.<br>" +
					"Si vous pensez qu'il s'agit d'une erreur, veuillez contacter votre administrateur.";

	/**
	 * Initialisation des addresses ip et
	 */
	public static void initialize() {

		boolean isAfficherLog = true;
		StringBuffer sbLog = new StringBuffer();

		String strAuthorizedIpKey = application.config().stringForKey(FwkCktlAcces.AUTHORIZED_IP_KEY);

		if (StringCtrl.isEmpty(strAuthorizedIpKey)) {

			sbLog.append("Pas de filtrage par IP. Toutes les machines sont autorisees a acceder a l'application");
			isAfficherLog = false;

		} else {

			try {

				String[] list = strAuthorizedIpKey.split(",");
				authorizedHostsAddresses = new ArrayList<IP>();
				authorizedSubnets = new ArrayList<Subnet>();
				if (list.length > 0) {
					sbLog.append("adresses autorisees : ");
				}
				for (String networkOrHost : list) {
					networkOrHost = networkOrHost.replaceAll(" ", "");
					sbLog.append(networkOrHost + " ");
					Matcher matcher = ipv4HostAddressPattern.matcher(networkOrHost);
					if (matcher.matches()) {
						authorizedHostsAddresses.add(new IP(networkOrHost));
					} else {
						matcher = ipv4NetworkAddressPattern.matcher(networkOrHost);
						if (matcher.matches()) {
							String[] tmp = networkOrHost.split("/");
							Subnet subnet = new Subnet(new IP(tmp[0]), new Netmask(Integer.parseInt(tmp[1])));
							authorizedSubnets.add(subnet);
						} else {
							throw new UnknownHostException("Adresse d'hote ou de reseau invalide : " + networkOrHost);
						}
					}
				}

			}
			// L'addresse ip de la machine ou du reseau est invalide.
			catch (UnknownHostException uhe) {
				sbLog.append("Erreur lors du traitement : " + uhe.getMessage());
				// log.fatal("An error occured while establishing the ip filtering. "+uhe.getMessage());
				// log.fatal("Please correct your configuration");
			}

		}

		// log (affiché uniquement si on active cette fonctionnalité)
		if (isAfficherLog) {
			sbLog.insert(0, "\nControle des acces reseau a cette application\n---------------------------------------------\n  > ");
			sbLog.append("\n\n");
			CktlLog.rawLog(sbLog.toString());
		}

		isInitialized = true;
	}

	/**
	 * determine si l'utilisateur actuellement connecte peut executer la requete
	 * vers l'application
	 * 
	 * @param request
	 * @return <code>true</code> si l'utilisateur peut acceder a l'application et
	 *         <code>false</code> si acces refuse ou si une exceptiin a ete
	 *         declenchee
	 */
	public static boolean canExecuteRequest(WORequest request) {
		try {
			IP ip = new IP(request._originatingAddress().getHostAddress());
			return authorizedHostsAddresses == null || authorizedSubnets == null || authorizedHostsAddresses.contains(ip) || isInAuthorizedSubnets(ip);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * obtention du message d'acces refuse pour la requete en cours
	 * 
	 * @param request
	 */
	public static WOResponse getRestrictedAccessMessage(WORequest request) {
		CktlAlertPage cktlAlertPage = (CktlAlertPage) application.pageWithName(
				CktlAlertPage.class.getName(), new WOContext(request));
		cktlAlertPage.setTarget("_top");
		// String message =
		// "You cannot access to this application. If you think this is an error you can contact the administrator.";
		String message = HTML_MESSAGE_DEFAULT_ACCES_INTERDIT;
		if (application.config().get(FwkCktlAcces.AUTHORIZATION_DENIED_MESSAGE_KEY) != null) {
			message = application.config().stringForKey(FwkCktlAcces.AUTHORIZATION_DENIED_MESSAGE_KEY);
		}
		cktlAlertPage.showMessage(
				cktlAlertPage, HTML_TITRE_PAGE_ACCES_INTERDIT, message, null, null, null, CktlAlertPage.ERROR, null);
		WOResponse response = cktlAlertPage.generateResponse();
		response.setStatus(HttpURLConnection.HTTP_FORBIDDEN);
		return response;
	}

	private static boolean isInAuthorizedSubnets(IP ip) throws UnknownHostException {
		for (int i = 0; i < authorizedSubnets.size(); i++) {
			if (authorizedSubnets.get(i).contains(ip)) {
				return true;
			}
		}
		return false;
	}
}
