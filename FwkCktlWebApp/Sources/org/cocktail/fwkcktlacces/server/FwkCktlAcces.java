package org.cocktail.fwkcktlacces.server;

import org.cocktail.fwkcktlacces.server.handler.FwkCktlAccesRequestHandler;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

/**
 * Cette classe centralise les operations de controle d'acces a l'application.
 * Elle est appelee par tous les handlers cocktails.
 * 
 * Le filtrage se fait en modifiant le fichier de config via les paramètres suivants :
 * - AUTHORIZED_IP : liste des ips autorisées à accéder à l'application. Le format est le suivant :
 *   	liste d'ip (a.b.c.d) et/ou le masque a utiliser (nombre de bits), séparées par des virgules.
 *   	Exemple : 10.1.11.86, 10.1.11.0/24
 * - AUTHORIZATION_DENIED_MESSAGE : le message personnalisé qui sera affiché à l'utilisateur lui
 * 		signifiant que sa machine n'est pas autorisée à connecter l'application
 * 
 * @author Ralph BULLET <ralph.bullet at cocktail.org>
 */
public class FwkCktlAcces {
	
	//private static Logger log = Logger.getLogger(FwkCktlAcces.class);
	private static CktlWebApplication application = (CktlWebApplication)CktlWebApplication.application();
	
	//Nom des clefs associees aux handlers pour les handlers
	public static final String AJAX_REQUEST_HANDLER_KEY           = "ajax";
	public static final String AJAX_PUSH_REQUEST_HANDLER_KEY      = "push";
	public static final String WO_COMPONENT_REQUEST_HANDLER_KEY   = "wo";
	public static final String DIRECT_ACTION_REQUEST_HANDLER_KEY  = "wa";
	public static final String ERXADM_REQUEST_HANDLER_KEY         = "erxadm";
	public static final String MONITOR_REQUEST_HANDLER_KEY        = "womp";
	public static final String RESOURCE_REQUEST_HANDLER_KEY       = "wr";
	public static final String STATIC_RESOURCE_REQUEST_HANDLER_KEY= "_ws_";
	public static final String STREAM_ACTION_REQUEST_HANDLER_KEY  = "wis";
	public static final String ERREST_REQUEST_HANDLER_KEY         = "ra";
	
	//Les differents numeros de handlers sont obtenus en effectuant des decalages de bits a gauche
	//Cela permet de pouvoir les cumuler a l'aide de l'operateur ou booleen
	
	/** Controle sur les acces par defaut **/
	public static final int CKTL_DEFAULT_REQUEST_HANDLER           =    1;
	
	/** Controle d'acces sur les requetes Ajax **/
	public static final int CKTL_AJAX_REQUEST_HANDLER              = 1<<1;
	
	/** Controle d'acces sur les requetes Push **/
	public static final int CKTL_AJAX_PUSH_REQUEST_HANDLER         = 1<<2;
	
	/** Controle d'acces sur les requetes vers des composants **/
	public static final int CKTL_WO_COMPONENT_REQUEST_HANDLER      = 1<<3;
	
	/** Controle d'acces sur les requetes vers des direct action **/
	public static final int CKTL_DIRECT_ACTION_REQUEST_HANDLER     = 1<<4;
	
	/** Controle d'acces sur les requetes de type exadm **/
	public static final int CKTL_ERXADM_REQUEST_HANDLER            = 1<<5;
	
	/** Controle d'acces depuis le moniteur **/
	public static final int CKTL_MONITOR_REQUEST_HANDLER           = 1<<6;
	
	/** Controle d'acces vers les resources **/
	public static final int CKTL_RESOURCE_REQUEST_HANDLER          = 1<<7;
	
	/** Controle d'acces vers les ressources statiques **/
	public static final int CKTL_STATIC_RESOURCE_REQUEST_HANDLER   = 1<<8;
	
	/** Controle d'acces vers les flux **/
	public static final int CKTL_STREAM_ACTION_REQUEST_HANDLER     = 1<<9;
	
	/** Controle d'acces vers les requetes rest **/
	public static final int CKTL_ERREST_REQUEST_HANDLER            = 1<<10; 
	
	/** Activer les controles d'acces recommandes **/
	public static final int CKTL_RECOMMANDED_REQUEST_HANDLERS      = 0x1B;
	
	/** Activer tous les controles d'acces **/
	public static final int ALL_CKTL_REQUEST_HANDLERS              = 0xFFFFFFFF;
	
	/** Nom du parametre du controle d'acces dans le fichier de configuration **/
	public static final String AUTHORIZED_IP_KEY = "AUTHORIZED_IP";
	
	/** Nom du parametre contenant le message affiche lors du refus d'acces **/
	public static final String AUTHORIZATION_DENIED_MESSAGE_KEY = "AUTHORIZATION_DENIED_MESSAGE";
	
	/**
	 * Remplace un handler par son equivalent cocktail
	 * Il est possible de remplacer plusieurs handlers en ajoutant un ou entre ceux-ci ex CKTL_DIRECT_ACTION_REQUEST_HANDLER | CKTL_WO_COMPONENT_REQUEST_HANDLER
	 */
	public static void addAccessControlForRequestHandler(int cktRequestHandler){
		if((cktRequestHandler & CKTL_DEFAULT_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForDefaultRequestHandler();
		if((cktRequestHandler & CKTL_AJAX_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(AJAX_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_AJAX_PUSH_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(AJAX_PUSH_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_WO_COMPONENT_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(WO_COMPONENT_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_DIRECT_ACTION_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(DIRECT_ACTION_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_ERXADM_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(ERXADM_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_MONITOR_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(MONITOR_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_RESOURCE_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(RESOURCE_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_STATIC_RESOURCE_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(STATIC_RESOURCE_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_STREAM_ACTION_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(STREAM_ACTION_REQUEST_HANDLER_KEY);
		if((cktRequestHandler & CKTL_ERREST_REQUEST_HANDLER)!=0) FwkCktlAcces.addAccessControlForRequestHandler(ERREST_REQUEST_HANDLER_KEY);
	}
	
	public static void addAccessControlForRequestHandler(String key){
		if(!FwkCktlAccesController.isInitialized)FwkCktlAccesController.initialize();
		application.registerRequestHandler(new FwkCktlAccesRequestHandler(key), key);
	}
	
	public static void addAccessControlForDefaultRequestHandler(){
		if(!FwkCktlAccesController.isInitialized)FwkCktlAccesController.initialize();
		application.setDefaultRequestHandler(new FwkCktlAccesRequestHandler(application.defaultRequestHandler()));
	}
}
