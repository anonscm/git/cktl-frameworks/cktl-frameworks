package org.cocktail.fwkcktlacces.server.handler;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.vfs2.FileObject;
import org.apache.commons.vfs2.FileSystemException;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.VFS;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.URLDecoder;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WODynamicURL;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WORequestHandler;
import com.webobjects.appserver.WOResourceManager;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSBundle;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSNotificationCenter;

import er.extensions.foundation.ERXDictionaryUtilities;
import er.extensions.foundation.ERXProperties;

/**
 * Utiliser JarResourceRequestHandler à la place.
 */
@Deprecated
public class FwkCktlResourcesInJarRequestHandler extends WORequestHandler {

	private static Logger log = Logger.getLogger(FwkCktlResourcesInJarRequestHandler.class);

	private static WOApplication application = WOApplication.application();

	private String _documentRoot;

	private boolean _useRequestHandlerPath;

	private static FileSystemManager fsManager;

	private static final String URL_PATTERN = "(^.* url\\([\"']?)([^\\)]*)([\"']?\\).*$)";

	static {
		try {
			fsManager = VFS.getManager();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			fsManager = null;
		}
	}

	public FwkCktlResourcesInJarRequestHandler() {
		_documentRoot = null;
	}

	protected WOResponse _generateResponseForInputStream(InputStream is, long length, String type) {
		WOResponse response = application.createResponseInContext(null);
		if (is != null) {
			if (length != 0) {
				response.setContentStream(is, 50 * 1024, length);
			}
		} else {
			response.setStatus(404);
		}
		if (type != null) {
			response.setHeader(type, "content-type");
		}
		if (length != 0) {
			response.setHeader("" + length, "content-length");
		}
		return response;
	}

	private String documentRoot() {
		if (_documentRoot == null) {
			_documentRoot = ERXProperties.stringForKey("WODocumentRoot");
			if (_documentRoot == null) {
				NSBundle bundle = NSBundle.bundleForName("JavaWebObjects");
				@SuppressWarnings("unchecked")
				NSDictionary<String, String> dict = ERXDictionaryUtilities.dictionaryFromPropertyList("WebServerConfig", bundle);
				_documentRoot = dict.objectForKey("DocumentRoot");
			}
		}
		return _documentRoot;
	}

	public WOResponse handleRequest(WORequest request) {
		WOResponse response = null;
		InputStream is = null;
		long length = 0;
		String contentType = null;
		String uri = request.uri();
		String originalUri = uri;
		if (uri.charAt(0) == '/') {
			WOResourceManager rm = application.resourceManager();
			String documentRoot = documentRoot();
			File file = null;
			StringBuffer sb = new StringBuffer(documentRoot.length() + uri.length());
			String wodataKey = request.stringFormValueForKey("wodata");
			if (uri.startsWith("/cgi-bin") && wodataKey != null) {
				uri = wodataKey;
				if (uri.startsWith("file:")) {
					// remove file:/
					uri = uri.substring(5);
				} else {

				}
			} else {
				int index = uri.indexOf("/wodata=");

				if (index >= 0) {
					uri = uri.substring(index + "/wodata=".length());
				} else {
					sb.append(documentRoot);
				}
			}

			if (_useRequestHandlerPath) {
				try {
					WODynamicURL dynamicURL = new WODynamicURL(uri);
					String requestHandlerPath = dynamicURL.requestHandlerPath();
					if (requestHandlerPath == null || requestHandlerPath.length() == 0) {
						sb.append(uri);
					} else {
						sb.append("/");
						sb.append(requestHandlerPath);
					}
				} catch (Exception e) {
					throw new RuntimeException("Failed to parse URL '" + uri + "'.", e);
				}
			}
			else {
				sb.append(uri);
			}

			String path = sb.toString();
			try {
				path = path.replaceAll("\\?.*", "");
				if (request.userInfo() != null && !request.userInfo().containsKey("HttpServletRequest")) {
					/* PATH_INFO is already decoded by the servlet container */
					path = path.replace('+', ' ');
					path = URLDecoder.decode(path, "UTF-8");
				}

				if (path.startsWith("jar:file:") && uri.contains("!/") && (fsManager != null)) {
					try {
						FileObject jarFile = fsManager.resolveFile(uri);
						is = jarFile.getContent().getInputStream();
						length = jarFile.getContent().getSize();
						if (uri.endsWith(".css")) {
							StringBuilder builder = new StringBuilder();
							BufferedReader reader = new BufferedReader(new InputStreamReader(is));
							String line;
							while ((line = reader.readLine()) != null) {
								if (line.matches(URL_PATTERN)) {
									originalUri = URLDecoder.decode(originalUri, "UTF-8");
									originalUri = originalUri.substring(0, 1 + originalUri.lastIndexOf("/"));
									line = line.replaceAll(URL_PATTERN, "$1" + originalUri + "$2$3");
								}
								builder.append(line);
							}
							byte[] bytes = builder.toString().getBytes();
							is = new ByteArrayInputStream(bytes);
							length = bytes.length;
							System.out.flush();
						}
						contentType = rm.contentTypeForResourceNamed(path);
						log.debug("Reading file '" + jarFile.getName() + "' for uri: " + uri);
					} catch (FileSystemException e) {
						throw new RuntimeException("Failed use Commons VFS for uri '" + uri + "'.", e);
					}
				}
				else {
					file = new File(path);
					length = file.length();
					is = new FileInputStream(file);

					contentType = rm.contentTypeForResourceNamed(path);
					log.debug("Reading file '" + file + "' for uri: " + uri);
				}
			} catch (IOException ex) {
				if (!uri.toLowerCase().endsWith("/favicon.ico")) {
					log.info("Unable to get contents of file '" + file + "' for uri: " + uri);
				}
			}
		} else {
			log.error("Can't fetch relative path: " + uri);
		}
		response = _generateResponseForInputStream(is, length, contentType);
		NSNotificationCenter.defaultCenter().postNotification(WORequestHandler.DidHandleRequestNotification, response);
		response._finalizeInContext(null);
		return response;
	}
}
