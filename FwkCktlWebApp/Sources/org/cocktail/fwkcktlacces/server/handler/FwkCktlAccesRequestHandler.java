package org.cocktail.fwkcktlacces.server.handler;

import org.cocktail.fwkcktlacces.server.FwkCktlAccesController;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;

import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WORequestHandler;
import com.webobjects.appserver.WOResponse;

/**
 * RequestHandler prenant en compte la restriction d'acces
 * @author Ralph BULLET <ralph.bullet at cocktail.org>
 */
public class FwkCktlAccesRequestHandler extends WORequestHandler{

	//private static final Logger log  = Logger.getLogger(FwkCktlAccesRequestHandler.class);
	private static final CktlWebApplication application = (CktlWebApplication)CktlWebApplication.application();
	
	private WORequestHandler requestHandler = null;
	
	/**
	 * Cree un nouvel requestHandler qui "envelloppera" le request handler existant avec la clef
	 * donnee en parametre. Cela permet de lui rajouter le controle d'acces
	 * @param requestHandlerKey
	 */
	public FwkCktlAccesRequestHandler(String requestHandlerKey){
		this(application.requestHandlerForKey(requestHandlerKey));
	}
	
	/**
	 * Cree un nouvel requestHandler qui "envelloppera" le request handler donne en parametre
	 * de maniere a lui rajouter le controle d'acces
	 * @param requestHandlerKey
	 */
	public FwkCktlAccesRequestHandler(WORequestHandler requestHandler){
		super();
		if(requestHandler==null) {
			//log.fatal(String.format("requestHandler est a null"));
			CktlLog.rawLog(String.format("requestHandler est a null"));
		}
		this.requestHandler = requestHandler;
	}
	
	/**
	 * Effectue un controle d'acces puis appelle la methode handleRequest du handler qui
	 * s'etait enregistre pour la clef donne dans le constructeur
	 */
	public WOResponse handleRequest(WORequest request) {
		if(!FwkCktlAccesController.canExecuteRequest(request)){
			//log.info(String.format("Access forbidden for IP : %s ",request._originatingAddress().getHostAddress()));
			//log.info(String.format("Acces a l'application bloque pour l'IP : %s ",request._originatingAddress().getHostAddress()));
			CktlLog.rawLog(String.format("Acces a l'application bloque pour l'IP : %s ",request._originatingAddress().getHostAddress()));
			return FwkCktlAccesController.getRestrictedAccessMessage(request);
		}
		return requestHandler.handleRequest(request); //On laisse le handler executer la requete
	}
	
}
