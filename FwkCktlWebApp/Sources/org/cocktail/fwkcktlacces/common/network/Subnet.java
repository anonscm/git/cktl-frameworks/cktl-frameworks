package org.cocktail.fwkcktlacces.common.network;

import java.net.UnknownHostException;
import java.util.ArrayList;

public class Subnet implements Comparable<Subnet>, java.io.Serializable{

	private static final long serialVersionUID = -2545328641745226775L;
	private IP broadcast;
	private IP network;
	private Netmask netmask;
	private int EIGHT_BIT_MASK = 255;

	public Subnet(IP ip, Netmask mask) throws UnknownHostException {
		if(ip == null)throw new UnknownHostException("The IP is null");
		if(mask == null)throw new UnknownHostException("The netmask is null");
		netmask = mask;
		int a = ip.getA() & netmask.getA();
		int b = ip.getB() & netmask.getB();
		int c = ip.getC() & netmask.getC();
		int d = ip.getD() & netmask.getD();
		network = new IP(a,b,c,d);

		a = network.getA() ^ (~netmask.getA() & EIGHT_BIT_MASK);
		b = network.getB() ^ (~netmask.getB() & EIGHT_BIT_MASK);
		c = network.getC() ^ (~netmask.getC() & EIGHT_BIT_MASK);
		d = network.getD() ^ (~netmask.getD() & EIGHT_BIT_MASK);
		broadcast = new IP(a,b,c,d);
	}

	public boolean contains(IP ip) throws UnknownHostException{
		if(ip == null){
			return false;
		}

		int a = ip.getA() & netmask.getA();
		int b = ip.getB() & netmask.getB();
		int c = ip.getC() & netmask.getC();
		int d = ip.getD() & netmask.getD();
		IP inputNetwork = new IP(a,b,c,d);
		if(inputNetwork.equals(network)){
			return true;
		}
		else{
			return false;
		}
	}

	public int size(){
		return netmask.size();
	}

	public IP getBroadcast(){
		return broadcast;
	}

	public IP getNetwork(){
		return network;
	}

	public Netmask getNetmask(){
		return netmask;
	}

	public IP getNetmaskAsIP(){
		return netmask.getNetmask();
	}

	public int getCidr(){
		return netmask.getCidr();
	}

	public IP[] getRange(){

		int networkA = network.getA();
		int networkB = network.getB();
		int networkC = network.getC();
		int networkD = network.getD();

		ArrayList<IP> list = new ArrayList<IP>();

		int hosts = netmask.size();
		int current = 0;

		for(int a = networkA; a < 255; a++){
			for(int b = networkB; b < 255; b++){
				for(int c = networkC; c < 255; c++){
					for(int d = networkD; d < 255; d++){
						try{
							IP temp = new IP(a,b,c,d);
							if(!temp.equals(broadcast) && !temp.equals(network)){
								if(d != 0)
									list.add(new IP(a,b,c,d));
							}
						}catch(UnknownHostException uhe){
							uhe.printStackTrace();
						}
						if(current == hosts){
							break;
						}
						current++;
					}
					if(current == hosts){
						break;
					}
					networkD = 1;
				}
				if(current == hosts){
					break;
				}
				networkC = 1;
			}
			if(current == hosts){
				break;
			}
			networkB = 1;
		}
		return (IP[])list.toArray(new IP[0]);
	}

	public boolean equals(Subnet net){
		return compareTo(net) == 0;
	}
	
	public String toString(){
		String output = "";
		output += "Network: " + network + "\n";
		output += "Broadcast: " + broadcast + "\n";
		output += "Netmask: " + netmask.getNetmaskString();
		return output;
	}
	
	public boolean equals(Object o){
		if(!(o instanceof Subnet))return false;
		Subnet subnet = (Subnet)o;
		return this.compareTo(subnet) == 0;
	}

	@Override
	public int hashCode() {
		int result = 1;
		final int multiplier = 19;
		result = multiplier * result + network.hashCode();
		result = multiplier * result + broadcast.hashCode();
		return result;
	}

	public int compareTo(Subnet input) {
		
		
		if(input == null) return 1; 

		if(network == null && input.getNetwork() == null) return 0;

		if(network == null) return -1;


		if(!network.equals(input.getNetwork())) return network.compareTo(input.getNetwork());
		
		else{

			if(broadcast == null && input.getBroadcast() == null) return 0;

			if(broadcast == null) return -1;
			
			else return broadcast.compareTo(input.getBroadcast()); 
		}
	}

}

