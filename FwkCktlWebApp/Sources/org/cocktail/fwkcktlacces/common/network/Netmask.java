package org.cocktail.fwkcktlacces.common.network;

import java.net.UnknownHostException;
import java.util.StringTokenizer;

public class Netmask implements java.io.Serializable{

	private static final long serialVersionUID = -2395656340946482518L;
	private IP netmask;
	private int cidr = -1;

	public Netmask(IP ip) throws UnknownHostException{
		parseNetmask(ip.toString());
	}

	public Netmask(int a, int b, int c, int d) throws UnknownHostException{
		parseNetmask(a + "." + b + "." + c + "." + d);
	}

	public Netmask(int inputCidr) throws UnknownHostException {
		parseNetmaskFromCidr(inputCidr);
	}

	public Netmask(String netmask) throws UnknownHostException {
		if(netmask == null)throw new UnknownHostException("The netmask is null");
		netmask = netmask.trim();
		if(netmask.startsWith("/") || netmask.startsWith("\\")){
			netmask = netmask.substring(1);
		}
		if(netmask.length() <= 2){
			try{
				parseNetmaskFromCidr(Integer.parseInt(netmask));
			}catch(NumberFormatException nfe){
				throw new UnknownHostException
				("Not a valid cidr: " + netmask);
			}
		}
		else{
			parseNetmask(netmask);
		}
	}

	private void parseNetmask(String netmask) throws UnknownHostException{
		try{
			StringTokenizer toks = new StringTokenizer(netmask, ".");

			// Vérifie la validité du masque

			boolean end = false;

			int a = Integer.parseInt(toks.nextToken());
			if(!isValidRange(a)){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(a < 255){
				end = true;
			}
			int b = Integer.parseInt(toks.nextToken());
			if(!isValidRange(b)){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(end == true && b != 0){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(b < 255){
				end = true;
			}
			int c = Integer.parseInt(toks.nextToken());
			if(!isValidRange(c)){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(end == true && c != 0){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(c < 255){
				end = true;
			}
			int d = Integer.parseInt(toks.nextToken());
			if(!isValidRange(d)){
				throw new UnknownHostException
				("Invalid Netmask: "+netmask);
			}
			if(end == true && d != 0){
				throw new UnknownHostException
				("Invalid netmask: "+netmask);
			}

			//Obtention du cidr

			cidr = parseCidr(a);
			cidr += parseCidr(b);
			cidr += parseCidr(c);
			cidr += parseCidr(d);
			try{
				this.netmask = new IP(a,b,c,d);
			}catch(UnknownHostException uhe){
				//Cette exception en sera jamais generee car tous les tests ont
				// etes effectues lors de la creation du masque
			}
		}catch(NullPointerException npe){
			throw new UnknownHostException
			("Invalid netmask: "+netmask);
		}catch(NumberFormatException nfe){
			throw new UnknownHostException
			("Invalid netmask: "+netmask);
		}
	}

	private void parseNetmaskFromCidr(int cidr) throws UnknownHostException{
		this.cidr = cidr;

		int a = 0;
		int b = 0;
		int c = 0;
		int d = 0;

		if(cidr < 0 || cidr > 32 ){
			throw new UnknownHostException
			("The inputed cidr is out of range: " + cidr);
		}
		if(cidr >= 8){
			a = 255;
			if(cidr >= 16){
				b = 255;
				if(cidr >= 24){
					c = 255;
					if(cidr == 32){
						d = 255; 
					}
					else{
						d = ((1 << cidr%8) - 1) << (8 - cidr%8);
					}
				}
				else{
					c = ((1 << cidr%8) - 1) << (8 - cidr%8);
				}
			}
			else{
				b = ((1 << cidr%8) - 1) << (8 - cidr%8);
			}
		}
		else{
			a = ((1 << cidr%8) - 1) << (8 - cidr%8);
		}
		try{
			netmask = new IP(a,b,c,d);
		}catch(UnknownHostException uhe){
			//Cette exception en sera jamais generee car tous les tests ont
			// etes effectues lors de la creation du masque
		}
	}

	private int parseCidr(int netmask){
		if(netmask == 0){
			return 0;
		}
		else if(netmask == 128){
			return 1;
		}
		else if(netmask == 192){
			return 2;
		}
		else if(netmask == 224){
			return 3;
		}
		else if(netmask == 240){
			return 4;
		}
		else if(netmask == 248){
			return 5;
		}
		else if(netmask == 252){
			return 6;
		}
		else if(netmask == 254){
			return 7;
		}
		else if(netmask == 255){
			return 8;
		}
		else{
			return -1;
		}
	}

	private boolean isValidRange(int range){
		if(range == 255){
			return true;
		}
		for(int i = 1; i <= 8; i++){
			if(range == (256 - (int)Math.pow(2,i))){
				return true;
			}
		}
		return false;
	}

	public void setNetmask(String netmask) throws UnknownHostException {
		parseNetmask(netmask);
	}

	public void setCidr(String inputCidr) throws UnknownHostException {
		try{
			parseNetmaskFromCidr(Integer.parseInt(inputCidr));
		}catch(NumberFormatException nfe){
			throw new UnknownHostException
			("Not a valid cidr: " + inputCidr);
		}
	}

	public void setCidr(int inputCidr) throws UnknownHostException {
		parseNetmaskFromCidr(inputCidr);
	}

	public int getCidr(){
		return cidr;
	}

	public int size(){
		int value = (256 - netmask.getD());
		value *= (256 - netmask.getC());
		value *= (256 - netmask.getB());
		value *= (256 - netmask.getA());
		
		int unusable = 2;
		unusable *= (256 - netmask.getC());
		unusable *= (256 - netmask.getB());
		unusable *= (256 - netmask.getA());
		return (value - unusable);
	}

	public String toString(){
		String output = netmask.toString();
		output += "\n" + "/" + cidr;
		return output;
	}

	public String getNetmaskString(){
		return netmask.toString();
	}

	public IP getNetmask(){
		return netmask;
	}

	public boolean equals(Object o){
		if(!(o instanceof Netmask))return false;
		Netmask netmask = (Netmask)o;
		return this.equals(netmask);
	}
	
	public boolean equals(Netmask mask){
		return cidr == mask.getCidr();
	}

	@Override
	public int hashCode() {
		return cidr;
	}

	public int getA(){
		return netmask.getA();
	}

	public int getB(){
		return netmask.getB();
	}

	public int getC(){
		return netmask.getC();
	}

	public int getD(){
		return netmask.getD();
	}
}

