package org.cocktail.fwkcktlwebapp.common.config;


import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * 
 * @author Julien Lafourcade
 *
 */
public class CktlConfigurationTestRunnerTest {

	public static final String MESSAGE_ECHEC = "Le test n'a pas réussi";
	
	private CktlConfigurationTestRunner runner;
	
	/**
	 * Mise en place des données de tests
	 */
	@Before
	public void setUp() {
		List<CktlConfigurationTest> tests = new ArrayList<CktlConfigurationTest>();
		tests.add(new CktlConfigurationTestQuiReussi());
		tests.add(new CktlConfigurationTestQuiEchoue());
		runner = new CktlConfigurationTestRunner(tests);
	}
	
	/**
	 * Tests sur la méthode de lancement des tests
	 */
	@Test
	public void testRunTests() {
		List<CktlConfigurationTestException> resultats = runner.runTests();
		
		Assert.assertEquals(1, resultats.size());
		
		CktlConfigurationTestException resultat = resultats.get(0);
		Assert.assertTrue(MESSAGE_ECHEC.equals(resultat.getMessage()));
	}
	
	/**
	 * Le test qui echoue
	 * @author Julien Lafourcade
	 *
	 */
	public class CktlConfigurationTestQuiEchoue implements CktlConfigurationTest {

		/**
		 * cette méthode échoue
		 * @throws CktlConfigurationTestException echec
		 */
		public void run() throws CktlConfigurationTestException {
			throw new CktlConfigurationTestException(MESSAGE_ECHEC);
		}
		
	}
	
	/**
	 * Le test qui réussi
	 * @author Julien Lafourcade
	 *
	 */
	public class CktlConfigurationTestQuiReussi implements CktlConfigurationTest {

		/**
		 * cette méthode réussi
		 * @throws CktlConfigurationTestException echec
		 */
		public void run() throws CktlConfigurationTestException {
		}
		
	}

}
