package org.cocktail.fwkcktlwebapp.common.util.services;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.cocktail.fwkcktlwebapp.common.util.services.FileName;
import org.junit.Before;
import org.junit.Test;

public class FileNameTest {
	
	private static final SimpleDateFormat SDF = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
	FileName fileName = null;
	
	
	@Before
	public void setUpService() throws Exception {
		final Date date = SDF.parse("25/02/2014 23:15:06");
		fileName = new FileName() {
			@Override
			protected Date getTimeStamp() {
				return date;
			};
		};
	}
	
	/*----------------
	 * parseValidDate
	 -----------------*/
	
	@Test(expected=ParseException.class)
	public void isDateValidBadMonthTest() throws ParseException {
		fileName.parseValidDate("2014-14-14", "yyyy-MM-dd");
	}
	
	@Test(expected=ParseException.class)
	public void isDateValidBadDayTest() throws ParseException {
		fileName.parseValidDate("2014-01-32", "yyyy-MM-dd");
	}

	@Test(expected=ParseException.class)
	public void isDateValidBadYearTest() throws ParseException {
		fileName.parseValidDate("-2014-01-30", "yyyy-MM-dd");
	}
	
	@Test()
	public void isDateValidTest() throws ParseException {
	      assertNotNull(fileName.parseValidDate("2014-01-30", "yyyy-MM-dd"));
	}
	
	/*----------------
	 * getNameWithDate
	 -----------------*/
	
	@Test(expected=ParseException.class)
	public void getNameWithDateBadMonthTest() throws ParseException {
		fileName.getNameWithDate("test", "2014-14-14", "yyyy-MM-dd");
	}
	
	@Test(expected=ParseException.class)
	public void getNameWithDateBadDayTest() throws ParseException {
		fileName.getNameWithDate("test", "2014-12-35", "yyyy-MM-dd");
	}

	@Test(expected=ParseException.class)
	public void getNameWithDateBadYearTest() throws ParseException {
		fileName.getNameWithDate("test", "-2014-12-12", "yyyy-MM-dd");
	}
	
	@Test()
	public void getNameWithDateTest() throws ParseException {
	      assertEquals(fileName.getNameWithDate("test", "2012-02-29", "yyyy-MM-dd"), "2012-02-29-test");
	      assertEquals(fileName.getNameWithDate("test", "2014-12-31", "yyyy-MM-dd"), "2014-12-31-test");
	      assertEquals(fileName.getNameWithDate("test", "2014/02/14", "yyyy/MM/dd"), "2014-02-14-test");
	      assertEquals(fileName.getNameWithDate("test", "14/05/2014_15-00-00", "dd/MM/yyyy_HH-mm-ss"), "14-05-2014_15-00-00-test");
	}


	/*----------------
	 * getNameWithCurrentDate
	 -----------------*/
	@Test
	public void getNameWithCurrentDateTest() throws ParseException {
		assertTrue(fileName.getNameWithCurrentDate("test", "dd-MM-yyyy").equals("25-02-2014-test"));

	}
	
	@Test
	public void getNameWithCurrentDateWithoutFormatTest() throws ParseException {
		assertEquals(fileName.getNameWithCurrentDate("test"), "2014-02-25_23-15-06-test");
	}
 
	@Test
	public void getNameWithCurrentDateDateFormatIsNullTest() throws ParseException {
		assertEquals(fileName.getNameWithCurrentDate("test", "yyyy-MM-dd_HH"), "2014-02-25_23-test");
	}
	
	@Test
	public void getNameWithCurrentDateNameIsNullTest() throws ParseException {
		assertEquals(fileName.getNameWithCurrentDate(null, "dd-MM-yyyy"),"25-02-2014-null");
	}
	
	@Test
	public void fileNameDateFormatWithSlashesTest() throws ParseException {
		assertEquals(fileName.getNameWithCurrentDate("test", "yyyyyy/MM/dd...."),"002014-02-25....-test");
	}
	
}
