package org.cocktail.fwkcktlwebapp.common.version.app;

import static org.junit.Assert.*;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionDigitsExtractor;
import org.junit.Before;
import org.junit.Test;

public class VersionDigitsExtractorTest {

    private VersionDigitsExtractor versionDigitsExtractor;
    
    @Before
    public void setUp() throws Exception {
        
    }

    @Test
    public void testExtractDigitsNormal() {
        versionDigitsExtractor = new VersionDigitsExtractor("2.0.8.2");
        int[] results = versionDigitsExtractor.extractDigits();
        assertArrayEquals(new int[] { 2, 0, 8, 2 }, results);
    }
    
    @Test
    public void testExtractDigitsNull() {
        versionDigitsExtractor = new VersionDigitsExtractor(null);
        int[] results = versionDigitsExtractor.extractDigits();
        assertArrayEquals(new int[] { 0, 0, 0, 0 }, results);
    }
    
    @Test
    public void testExtractDigitsPasAssezLong() {
        versionDigitsExtractor = new VersionDigitsExtractor("2.0.8");
        int[] results = versionDigitsExtractor.extractDigits();
        assertArrayEquals(new int[] { 2, 0, 8, 0 }, results);
    }
    
    @Test
    public void testExtractDigitsLettres() {
        versionDigitsExtractor = new VersionDigitsExtractor("2.a.8b");
        int[] results = versionDigitsExtractor.extractDigits();
        assertArrayEquals(new int[] { 2, 0, 0, 0 }, results);
    }
    
    @Test
    public void testExtractDigitsCompletementFoireux() {
        versionDigitsExtractor = new VersionDigitsExtractor("2az.8b");
        int[] results = versionDigitsExtractor.extractDigits();
        assertArrayEquals(new int[] { 0, 0, 0, 0 }, results);
    }
    
}
