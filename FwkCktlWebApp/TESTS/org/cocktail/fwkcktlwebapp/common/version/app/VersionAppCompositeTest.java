package org.cocktail.fwkcktlwebapp.common.version.app;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlwebapp.common.version.app.VersionApp;
import org.cocktail.fwkcktlwebapp.common.version.app.VersionAppComposite;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

public class VersionAppCompositeTest {
    
    private VersionAppComposite versionAppComposite;
    
    @Before
    public void setUp() throws Exception {

        
    }
    
    @Test
    public void testFirstSource() {
        VersionApp first = createMockAppVersion(1, 0, 0, 0, "10/12/2013");
        VersionApp second = createMockAppVersion(2, 2, 0, 0, "10/10/2013");
        
        versionAppComposite = new VersionAppComposite(first, second);
        assertEquals(1, versionAppComposite.majVersion());
        assertEquals(0, versionAppComposite.minVersion());
        assertEquals(0, versionAppComposite.patchVersion());
        assertEquals(0, versionAppComposite.buildVersion());
        assertEquals("10/12/2013", versionAppComposite.dateVersion());
    }
    
    @Test
    public void testSecondSource() {
        VersionApp first = createMockAppVersion(0, 0, 0, 0, null);
        VersionApp second = createMockAppVersion(2, 2, 0, 0, "10/10/2013");
        
        versionAppComposite = new VersionAppComposite(first, second);
        assertEquals(2, versionAppComposite.majVersion());
        assertEquals(2, versionAppComposite.minVersion());
        assertEquals(0, versionAppComposite.patchVersion());
        assertEquals(0, versionAppComposite.buildVersion());
        assertEquals("10/10/2013", versionAppComposite.dateVersion());
    }
    
    private VersionApp createMockAppVersion(int majVersion, int minVersion, int patchVersion, int buildVersion, String dateVersion) {
        VersionApp mockAppVersion = Mockito.mock(VersionApp.class);
        when(mockAppVersion.majVersion()).thenReturn(majVersion);
        when(mockAppVersion.minVersion()).thenReturn(minVersion);
        when(mockAppVersion.patchVersion()).thenReturn(patchVersion);
        when(mockAppVersion.buildVersion()).thenReturn(buildVersion);
        when(mockAppVersion.dateVersion()).thenReturn(dateVersion);
        return mockAppVersion;
    }
    
}
