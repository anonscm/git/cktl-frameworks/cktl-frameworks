package org.cocktail.fwkcktlwebapp.server.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class CktlJnlpTemplateProcessorTest {
	private static final String JNLPTEMPLATECONTENT = "";

	@Test(expected = NullPointerException.class)
	public void processTemplateEmptyCGIParams() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put(CktlJnlpTemplateProcessor.CGI_APPLI_URL_KEY, "");
		params.put(CktlJnlpTemplateProcessor.WEB_APPLI_URL_KEY, "toto");
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		processor.processTemplate(JNLPTEMPLATECONTENT);
	}

	@Test(expected = NullPointerException.class)
	public void processTemplateEmptyWEBParams() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put(CktlJnlpTemplateProcessor.CGI_APPLI_URL_KEY, "toto");
		params.put(CktlJnlpTemplateProcessor.WEB_APPLI_URL_KEY, "");
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		processor.processTemplate(JNLPTEMPLATECONTENT);
	}

	@Test(expected = NullPointerException.class)
	public void processTemplateEmptyTemplate() throws Exception {
		Map<String, String> params = new HashMap<String, String>();
		params.put(CktlJnlpTemplateProcessor.CGI_APPLI_URL_KEY, "toto");
		params.put(CktlJnlpTemplateProcessor.WEB_APPLI_URL_KEY, "titi");
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		processor.processTemplate("");
	}

	@Test
	public void processTemplateErrorInTemplate() {
		Map<String, String> params = new HashMap<String, String>();
		params.put(CktlJnlpTemplateProcessor.CGI_APPLI_URL_KEY, "toto");
		params.put(CktlJnlpTemplateProcessor.WEB_APPLI_URL_KEY, "titi");
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		String error = "Aucune erreur";
		try {
			error = processor.processTemplate("toto");
		} catch (Exception e) {
			error = e.getMessage();
		}
		assertTrue(error.startsWith(CktlJnlpTemplateProcessor.CODEBASE_NOT_FOUND_MSG));

		try {
			error = processor.processTemplate(CktlJnlpTemplateProcessor.CODEBASE_FILTER);
		} catch (Exception e) {
			error = e.getMessage();
		}
		assertTrue(error.startsWith(CktlJnlpTemplateProcessor.APPLICATION_URL_FOUND_MSG));
	}

	@Test
	public void processWebUrl() throws Exception {
		String webAppliUrl = "http://toto/monAppli.woa";
		Map<String, String> params = new HashMap<String, String>();
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		String start = "xxxxxxxxxxxxxxxxxx";
		String end = "yyyyyyyyyyyyyyyyy";
		String res = processor.processWebAppliUrl(start + "codebase=\"*\"" + end, webAppliUrl);
		assertEquals(start + "codebase=\"" + webAppliUrl + "\"" + end, res);
	}

	@Test
	public void processCgiUrl() throws Exception {
		String cgiAppliUrl = "http://toto/cgi-bin/monAppli.woa";
		Map<String, String> params = new HashMap<String, String>();
		CktlJnlpTemplateProcessor processor = new CktlJnlpTemplateProcessor(params);
		String start = "xxxxxxxxxxxxxxxxxx";
		String end = "yyyyyyyyyyyyyyyyy";
		String content = start + "<argument>-applicationURL</argument>\n" +
				"    <argument>*</argument>\n" +
				"    <argument>-page</argument>\n" +
				"    <argument>JavaClient</argument>\n" + end;

		String expectedRes = start + "<argument>-applicationURL</argument>\n" +
				"    <argument>" + cgiAppliUrl + "</argument>\n" +
				"    <argument>-page</argument>\n" +
				"    <argument>JavaClient</argument>\n" + end;

		String res = processor.processCgiAppliUrl(content, cgiAppliUrl);
		assertEquals(expectedRes, res);

	}

}
