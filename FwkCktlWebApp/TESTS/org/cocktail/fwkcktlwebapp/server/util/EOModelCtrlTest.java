package org.cocktail.fwkcktlwebapp.server.util;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.Test;

import com.webobjects.eoaccess.EOModel;
import com.webobjects.foundation.NSDictionary;

public class EOModelCtrlTest {

    @Test
    public void bdConnexionUser() {
        
        NSDictionary<String, Object> dico = 
                        new NSDictionary<String, Object>(new Object[] {"utilisateur"}, 
                                        new String[] {EOModelCtrl.KEY_USERNAME});

        EOModel model = mock(EOModel.class);
        when(model.connectionDictionary()).thenReturn(dico);
        assertEquals("utilisateur", EOModelCtrl.bdConnexionUser(model));
    }

    @Test
    public void bdConnexionUserVide() {
        
        EOModel model = mock(EOModel.class);
        when(model.connectionDictionary()).thenReturn(new NSDictionary<String, Object>());
        assertNull(EOModelCtrl.bdConnexionUser(model));
    }
    
    @Test
    public void bdConnexionUrl() {
        NSDictionary<String, Object> dico = 
                        new NSDictionary<String, Object>(new Object[] {"url"}, 
                                        new String[] {EOModelCtrl.KEY_URL});

        EOModel model = mock(EOModel.class);
        when(model.connectionDictionary()).thenReturn(dico);
        
        assertEquals("url", EOModelCtrl.bdConnexionUrl(model));
    }
    
    @Test
    public void bdConnexionUrlVide() {
        NSDictionary<String, Object> dico = 
                        new NSDictionary<String, Object>();

        EOModel model = mock(EOModel.class);
        when(model.connectionDictionary()).thenReturn(dico);
        
        assertEquals(EOModelCtrl.DEFAULT_URL, EOModelCtrl.bdConnexionUrl(model));
    }
}
