package org.cocktail.fwkcktlwebapp.server;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * 
 * Test honteusement repris de : 
 * 
 * https://github.com/Jasig/java-cas-client/commit/ae37092100c8eaec610dab6d83e5e05a8ee58814
 * 
 * Pour fix le pb de sécurité potentiel :
 * 
 * https://lists.wisc.edu/read/messages?id=33836937
 * 
 * @author alexistual
 *
 */
public class CasUrlGeneratorTest {

	CasUrlGenerator casUrlGenerator;
	
	@Before
	public void setUp() {
		this.casUrlGenerator = new CasUrlGenerator();
	}
	
	@Test
	public void testGenerateCompleteValidateUrl() {
		String casValidateUrl = "http://cas.univ-foobar.fr/casValidate";
		String ticket = "ST-1-owKEOtYJjg77iHcCQpkl-cas01.example.org%26%73%65%72%76%69%63%65%3d%68%74%74%70%25%33%41%25%32%46%25%32%46%31%32%37%2e%30%2e%30%2e%31%25%32%46%62%6f%72%69%6e%67%25%32%46%23";
		String service = "foobar";
		String url = this.casUrlGenerator.generateCompleteValidateUrl(casValidateUrl, ticket, service);

		String encodedTicket = this.casUrlGenerator.encode(ticket);
		assertTrue(url.contains(encodedTicket));
		assertFalse(url.contains(ticket));
	}

}
