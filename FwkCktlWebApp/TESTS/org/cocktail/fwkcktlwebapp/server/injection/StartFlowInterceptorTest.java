package org.cocktail.fwkcktlwebapp.server.injection;

import static org.junit.Assert.*;

import org.cocktail.fwkcktlwebapp.server.injection.Dummy.GestionDummy;
import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.matcher.Matchers;

public class StartFlowInterceptorTest {

	private Injector injector;
	private FlowProvider flowProvider;

	@Before
	public void setUp() throws Exception {
		flowProvider = new DummyFlowProvider();
		injector = Guice.createInjector(new DummyModule(flowProvider));
	}

	@Test
	public void test() {
		Dummy dummy  = injector.getInstance(Dummy.class);
		dummy.startGestionDummy();

		DummyFlow flow = (DummyFlow) flowProvider.provide(GestionDummy.class.getName());
		assertTrue(flow.hasStarted());

	}

	public static class DummyModule extends AbstractModule {

		private FlowProvider flowProvider;

		public DummyModule(FlowProvider flowProvider) {
			this.flowProvider = flowProvider;
		}

		@Override
		protected void configure() {
			StartFlowInterceptor interceptor = new StartFlowInterceptor();
			interceptor.setFlowProvider(flowProvider);
			bindInterceptor(
					Matchers.any(),
					Matchers.annotatedWith(StartFlow.class),
					interceptor);

		}

	}

}
