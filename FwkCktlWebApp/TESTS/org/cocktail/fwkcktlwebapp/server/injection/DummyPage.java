package org.cocktail.fwkcktlwebapp.server.injection;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;
import org.cocktail.fwkcktlwebapp.server.injection.Dummy.GestionDummy;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSArray;

import er.extensions.foundation.ERXThreadStorage;

public class DummyPage extends CktlWebComponent {

	private static final long serialVersionUID = 1L;

	@InjectFlow(name = GestionDummy.class)
	private CktlFlow flow;

	public DummyPage(WOContext context) {
		super(context);
	}

	@Override
	public WOElement template() {
		return WOComponent.templateWithHTMLString(html(), wod(), NSArray.EmptyArray);
	}

	private String html() {
	    return "<webobject name = \"DummyCo\" />";
	}
	
	private String wod() {
	    return "DummyCo : DummyComponent { }";
	}

	public CktlFlow getFlow() {
        return flow;
    }
	
	public void setFlow(CktlFlow flow) {
        this.flow = flow;
	}
	
}
