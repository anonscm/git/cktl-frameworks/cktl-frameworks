package org.cocktail.fwkcktlwebapp.server.injection;

class Dummy {

	@InjectFlow(name = GestionDummy.class)
	protected Flow flow;

	@StartFlow(name = GestionDummy.class)
	public void startGestionDummy() {
	}

	@CurrentFlow
	protected Flow currentFlow;
	
	public static interface GestionDummy extends FlowName {
	}
}
