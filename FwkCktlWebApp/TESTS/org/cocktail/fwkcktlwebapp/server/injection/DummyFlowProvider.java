package org.cocktail.fwkcktlwebapp.server.injection;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlwebapp.server.injection.Dummy.GestionDummy;

class DummyFlowProvider implements FlowProvider {

    private Map<String, Flow> flows;

    public DummyFlowProvider() {
        this.flows = new HashMap<String, Flow>();
    }

    public Flow provide(String name) {
        Flow flow = null;
        if (name != null) {
            if (flows.containsKey(name)) {
                flow = flows.get(name);
            } else {
                flow = new DummyFlow();
                flows.put(name, flow);
            }
        } else {
            flow = provide(provideCurrentFlowName());
        }
        return flow;
    }

    public String provideCurrentFlowName() {
        return GestionDummy.class.getName();
    }

}
