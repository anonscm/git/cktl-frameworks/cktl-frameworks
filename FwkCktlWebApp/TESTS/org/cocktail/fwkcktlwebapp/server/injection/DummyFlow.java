package org.cocktail.fwkcktlwebapp.server.injection;

import java.util.Map;

class DummyFlow implements Flow {

	private boolean hasStarted = false;

	public void start() {
		this.hasStarted = true;
	}

	public boolean hasStarted() {
		return hasStarted;
	}

	public Map<String, Object> store() {
		return null;
	}

}
