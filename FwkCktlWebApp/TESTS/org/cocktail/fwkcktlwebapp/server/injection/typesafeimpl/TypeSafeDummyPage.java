package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import javax.inject.Inject;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.foundation.NSArray;

class TypeSafeDummyPage extends CktlWebComponent {

	private static final long serialVersionUID = 1L;

	@Inject @GestionDummy
	private TypeSafeFlow flow;

	public TypeSafeDummyPage(WOContext context) {
		super(context);
	}

	@Override
	public WOElement template() {
		return WOComponent.templateWithHTMLString(html(), wod(), NSArray.EmptyArray);
	}

	private String html() {
	    return "<webobject name = \"DummyCo\" />";
	}

	private String wod() {
	    return "DummyCo : TypeSafeDummyComponent { }";
	}

	public TypeSafeFlow getFlow() {
        return flow;
    }

	public void setFlow(TypeSafeFlow flow) {
        this.flow = flow;
	}

}
