package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.annotation.Annotation;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.injection.DummyPage;
import org.cocktail.fwkcktlwebapp.server.injection.InjectableApplicationTestCase;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Key;
import com.google.inject.Module;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation._NSUtilities;
import com.woinject.WOScopes;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXSession;
import er.extensions.appserver.ERXWOContext;

@Ignore
public class TypeSafeFlowIntegrationTest {

	private TypeSafeDummyPage page;
    private CktlWebApplication application;
    private WOContext testContext;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        InjectableApplicationTestCase.setUpBeforeClass(DummyApplication.class);
    }

    @Before
    public void setUp() throws Exception {
        testContext = new WOContext(new WORequest("GET", "/", "HTTP/1.1", null, null, null));
        ERXWOContext.setCurrentContext(testContext);
        application = (CktlWebApplication) ERXApplication.application();
        ERXSession session = (ERXSession) application._initializeSessionInContext(testContext);
        ERXSession.setSession(session);
        page = application.pageWithName(TypeSafeDummyPage.class, testContext);
    }

    @Test
    public void testFlowInjection() {
        application.injector().injectMembers(page);
        assertNotNull(page.getFlow());
    }

    @Test
    public void testFlowNameComponentNameRegistry() {
        TypeSafeFlowComponentRegistry registry = (TypeSafeFlowComponentRegistry)
        		application.injector().getInstance(TypeSafeFlowComponentRegistry.class);
        assertNotNull(registry);
        assertEquals(flowKeyFor(GestionDummy.class).toString(), registry.lookup(TypeSafeDummyPage.class));
    }

    @Test
    public void testCurrentFlowInjection() {
        page._awakeInContext(testContext);
        WOResponse response = page.generateResponse();
        assertTrue(response.contentString().contains("Flow"));
    }

    private Key<TypeSafeFlow> flowKeyFor(Class<? extends Annotation> flowAnnotation) {
    	return Key.get(TypeSafeFlow.class, flowAnnotation);
    }

    public static class DummySession extends CktlWebSession {
    }

    public static class DummyApplication extends CktlWebApplication {

        public DummyApplication() {
            super();
            _NSUtilities.setClassForName(DummySession.class, "Session");
        }

        @Override
        protected Module[] modules() {
            return new Module[] {
        		new AbstractModule() {
					@Override
					protected void configure() {
						install(new TypeSafeModule());
						bind(TypeSafeFlow.class).annotatedWith(GestionDummy.class).toProvider(TypeSafeFlowProvider.class).in(WOScopes.SESSION);
					}
				}
            };
        }

        @Override
        protected Class _sessionClass() {
            return DummySession.class;
        }
    }

}
