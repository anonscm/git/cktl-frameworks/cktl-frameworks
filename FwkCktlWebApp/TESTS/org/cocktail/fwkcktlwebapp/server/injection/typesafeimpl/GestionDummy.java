package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.google.inject.BindingAnnotation;

@Target({ FIELD })
@Retention(RUNTIME)
@BindingAnnotation
@BindingFlow
@interface GestionDummy {

}
