package org.cocktail.fwkcktlwebapp.server.injection.typesafeimpl;

import org.cocktail.fwkcktlwebapp.server.injection.CurrentFlow;

import com.google.inject.Inject;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

public class TypeSafeDummyComponent extends WOComponent {

    private static final long serialVersionUID = 1L;

    @Inject @CurrentFlow
    private TypeSafeFlow flow;

    public TypeSafeDummyComponent(WOContext context) {
        super(context);
    }

    @Override
    public WOElement template() {
        return WOComponent.templateWithHTMLString("", "", NSArray.EmptyArray);
    }

    @Override
    public void appendToResponse(WOResponse aResponse, WOContext aContext) {
        aResponse.setContent(flow.toString());
    }
}
