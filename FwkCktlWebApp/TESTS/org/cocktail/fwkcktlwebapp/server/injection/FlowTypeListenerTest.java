package org.cocktail.fwkcktlwebapp.server.injection;

import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.matcher.Matchers;

public class FlowTypeListenerTest {

	private Injector injector;

	@Before
	public void setUp() throws Exception {
		injector = Guice.createInjector(new DummyModule());
	}

	@Test
	public void testInjectFlow() {
		Dummy dummy = injector.getInstance(Dummy.class);
		assertNotNull(dummy.flow);
	}

	public static class DummyModule extends AbstractModule {

		private FlowTypeListener flowTypeListener;

		public DummyModule() {
		    this(new DummyFlowProvider());
		}

		public DummyModule(FlowProvider flowProvider) {
			this.flowTypeListener = new FlowTypeListener();
			flowTypeListener.setFlowProvider(flowProvider);
		}

		@Override
		protected void configure() {
			bindListener(Matchers.any(), flowTypeListener);
		}

	}
}
