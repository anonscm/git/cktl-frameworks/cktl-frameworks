package org.cocktail.fwkcktlwebapp.server.injection;

import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

import com.woinject.InjectableApplication;

import er.extensions.ERXExtensions;

public abstract class InjectableApplicationTestCase {
    
    public static void setUpBeforeClass(Class<? extends InjectableApplication> appClass) throws Exception {
        InjectableApplicationTestCase.initInjection(appClass);
        ERXExtensions.initApp(appClass, new String[] {"-Der.javamail.centralize=false"});
    }

    private static void initInjection(Class<? extends InjectableApplication> appClass) {
        try {
            ClassPool pool = ClassPool.getDefault();
            CtClass clazz = pool.get("com.webobjects.foundation._NSUtilities");
            CtMethod method = clazz.getDeclaredMethod("instantiateObject");
            method.insertBefore("{ if (" + appClass.getName() + ".class != $1 ) return com.webobjects.foundation.InstantiationInterceptor.instantiateObject($1, $2, $3, $4, $5); }");
            method = clazz.getDeclaredMethod("instantiateObjectWithConstructor");
            method.insertBefore("{ return com.webobjects.foundation.InstantiationInterceptor.instantiateObject($1, $2, $3, $4, $5); }");
            clazz.toClass();
        } catch (Exception e) {
            if (e != null && !e.getMessage().contains("frozen")) {
                throw new RuntimeException(e);
            }
        }
    }
    
}
