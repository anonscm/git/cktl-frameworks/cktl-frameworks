package org.cocktail.fwkcktlwebapp.server.injection;

import junit.framework.Assert;

import org.cocktail.fwkcktlwebapp.server.injection.Dummy.GestionDummy;
import org.junit.Before;
import org.junit.Test;

public class FlowMembersInjectorTest {

	private Flow flow;
	private FlowProvider provider;

	@Before
	public void setUp() {
		provider = new DummyFlowProvider();
		flow = provider.provide(GestionDummy.class.getName());
	}

	@Test
	public void testInjectFlowMembers() throws Exception {
		FlowMembersInjector<Dummy> injector = new FlowMembersInjector<Dummy>(
				provider,
				Dummy.class.getDeclaredField("flow"),
				GestionDummy.class.getName());
		Dummy dummy = new Dummy();
		injector.injectMembers(dummy);
		Assert.assertEquals(flow, dummy.flow);
	}

}
