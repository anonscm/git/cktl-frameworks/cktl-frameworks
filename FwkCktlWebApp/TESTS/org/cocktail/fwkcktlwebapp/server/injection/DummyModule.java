package org.cocktail.fwkcktlwebapp.server.injection;

import org.cocktail.fwkcktlwebapp.server.components.CktlWebComponent;

import com.google.inject.AbstractModule;
import com.google.inject.matcher.Matchers;
import com.woinject.WOScopes;

public class DummyModule extends AbstractModule {

	private FlowTypeListener flowTypeListener;
	private StartFlowInterceptor interceptor;
	private CktlFlowNameComponentRegistry flowNameClassNameRegistry;

	public DummyModule() {
	    this(new DummyFlowProvider(), null);
	}

	public DummyModule(FlowProvider flowProvider, CktlFlowNameComponentRegistry flowNameClassNameRegistry) {
		this.interceptor = new StartFlowInterceptor();
		interceptor.setFlowProvider(flowProvider);
		this.flowTypeListener = new FlowTypeListener();
		flowTypeListener.setFlowProvider(flowProvider);
		this.flowNameClassNameRegistry = flowNameClassNameRegistry;
		flowTypeListener.setFlowNameClassNameRegistry(flowNameClassNameRegistry);
	}

	@Override
	protected void configure() {
		bindInterceptor(
				Matchers.subclassesOf(CktlWebComponent.class),
				Matchers.annotatedWith(StartFlow.class),
				interceptor);
		bindListener(Matchers.any(), flowTypeListener);
		bind(CktlFlowNameComponentRegistry.class).toInstance(flowNameClassNameRegistry);
		bind(CktlFlow.class).annotatedWith(CurrentFlow.class).toProvider(CktlCurrentFlowProvider.class).in(WOScopes.REQUEST);
	}

}
