package org.cocktail.fwkcktlwebapp.server.injection;

import com.google.inject.Inject;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOElement;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSArray;

public class DummyComponent extends WOComponent {

    private static final long serialVersionUID = 1L;

    @Inject @CurrentFlow
    private CktlFlow flow;

    public DummyComponent(WOContext context) {
        super(context);
    }

    @Override
    public WOElement template() {
        return WOComponent.templateWithHTMLString("", "", NSArray.EmptyArray);
    }

    @Override
    public void appendToResponse(WOResponse aResponse, WOContext aContext) {
        aResponse.setContent(flow.toString());
    }
    
    public void setFlow(CktlFlow flow) {
        this.flow = flow;
    }

    public CktlFlow getFlow() {
        return flow;
    }
    
}
