package org.cocktail.fwkcktlwebapp.server.injection;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.CktlWebSession;
import org.cocktail.fwkcktlwebapp.server.injection.Dummy.GestionDummy;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.google.inject.Module;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation._NSUtilities;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXSession;
import er.extensions.appserver.ERXWOContext;

@Ignore
public class CktlFlowIntegrationTest {

    private DummyPage page;
    private CktlWebApplication application;
    private WOContext testContext;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        InjectableApplicationTestCase.setUpBeforeClass(DummyApplication.class);
    }

    @Before
    public void setUp() throws Exception {
        testContext = new WOContext(new WORequest("GET", "/", "HTTP/1.1", null, null, null));
        ERXWOContext.setCurrentContext(testContext);
        application = (CktlWebApplication)ERXApplication.application();
        ERXSession session = (ERXSession) application._initializeSessionInContext(testContext);
        ERXSession.setSession(session);
        page = application.pageWithName(DummyPage.class, testContext);
    }

    @Test
    public void testFlowInjection() {
        application.injector().injectMembers(page);
        assertNotNull(page.getFlow());
    }

    @Test
    public void testFlowNameComponentNameRegistry() {
        CktlFlowNameComponentRegistry registry =
                (CktlFlowNameComponentRegistry) application.injector().getInstance(CktlFlowNameComponentRegistry.class);
        assertNotNull(registry);
        assertEquals(GestionDummy.class.getName(), registry.flowName(DummyPage.class));
    }

    @Test
    public void testCurrentFlowInjection() {
        //application.injector().injectMembers(page);
        page._awakeInContext(testContext);
        WOResponse response = page.generateResponse();
        assertTrue(response.contentString().contains("Flow"));
    }

    public static class DummySession extends CktlWebSession {
    }

    public static class DummyApplication extends CktlWebApplication {

        public DummyApplication() {
            super();
            _NSUtilities.setClassForName(DummySession.class, "Session");
        }

        @Override
        protected Module[] modules() {
            return new Module[] {new DummyModule(new CktlFlowProvider(), new CktlFlowNameComponentRegistry())};
        }

        @Override
        protected Class _sessionClass() {
            return DummySession.class;
        }
    }

}
