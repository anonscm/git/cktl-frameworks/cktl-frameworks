package org.cocktail.fwkcktlwebapp.server.version.app;

import static org.junit.Assert.assertEquals;

import java.util.Properties;

import org.junit.Before;
import org.junit.Test;

public class VersionAppFromVersionPropertiesTest {

    private VersionAppFromVersionProperties versionAppFromVersionProperties;
    
    @Before
    public void setUp() throws Exception {
        versionAppFromVersionProperties = new VersionAppFromVersionProperties(null);
        Properties props = new Properties();
        props.setProperty("version", "1.2.3.4");
        props.setProperty("date", "2013-10-08 10:49:32");
        versionAppFromVersionProperties.setVersionProperties(props);
    }

    @Test
    public void testFullVersion() {
        assertEquals("1.2.3.4", versionAppFromVersionProperties.fullVersion());
    }
    
    @Test
    public void testDate() {
        assertEquals("2013-10-08 10:49:32", versionAppFromVersionProperties.dateVersion());
    }
    
}
