package org.cocktail.crypto.sha;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.junit.Test;

public class ShaUtilsTest {

	String password = "password";
	String salt = "5dfàà=4fgf";

	@Test
	public void testMatchRaw() {
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha1Digest());
		assertTrue(shaUtils.match("tests", "92A++6qoboOSWB7Y3geOfAVWbKuKdZWfdBDTCw=="));
	}

	@Test
	public void testMatchSha1() {
		String encryptedPassword = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha1(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha1Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testMatchSha256() {
		String encryptedPassword = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha256(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha256Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testMatchSha384() {
		String encryptedPassword = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha384(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha384Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testMatchSha512() {
		String encryptedPassword = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha512(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha512Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testCryptSha1() {
		String encryptedPasswordAttendu = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha1(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha1Digest());
		String encryptedPassword = shaUtils.cryptString(password, salt);
		assertEquals(encryptedPasswordAttendu, encryptedPassword);
	}

	@Test
	public void testCryptSha256() {
		String encryptedPasswordAttendu = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha256(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha256Digest());
		String encryptedPassword = shaUtils.cryptString(password, salt);
		assertEquals(encryptedPasswordAttendu, encryptedPassword);
	}

	@Test
	public void testCryptSha384() {
		String encryptedPasswordAttendu = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha384(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha384Digest());
		String encryptedPassword = shaUtils.cryptString(password, salt);
		assertEquals(encryptedPasswordAttendu, encryptedPassword);
	}

	@Test
	public void testCryptSha512() {
		String encryptedPasswordAttendu = Base64.encodeBase64String(ArrayUtils.addAll(DigestUtils.sha512(password + salt), salt.getBytes()));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha512Digest());
		String encryptedPassword = shaUtils.cryptString(password, salt);
		assertEquals(encryptedPasswordAttendu, encryptedPassword);
	}

	@Test
	public void testCryptSha1SaltVide() {
		String encryptedPassword = Base64.encodeBase64String(DigestUtils.sha1(password));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha1Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testCryptSha256SaltVide() {
		String encryptedPassword = Base64.encodeBase64String(DigestUtils.sha256(password));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha256Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testCryptSha384SaltVide() {
		String encryptedPassword = Base64.encodeBase64String(DigestUtils.sha384(password));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha384Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

	@Test
	public void testCryptSha512SaltVide() {
		String encryptedPassword = Base64.encodeBase64String(DigestUtils.sha512(password));
		ShaUtils shaUtils = new ShaUtils(DigestUtils.getSha512Digest());
		assertTrue(shaUtils.match(password, encryptedPassword));
	}

}
