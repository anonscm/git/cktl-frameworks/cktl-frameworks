// -------- VARIABLES GLOBALES ---------

var isIE = document.all?true:false;

// ces variables sont initialisees lors du premier appel au constructeur
var bgcolor, border, texte, close, url, imgClose, larg, haut, imgTransparent, title;

var isHidden = false;

function showsup()
{
	if(document.getElementById)
	{
		document.getElementById("CktlLayerDiv").style.width = larg;
		document.getElementById("CktlLayerDiv").style.height = haut;
		document.getElementById("CktlLayerDiv").innerHTML = html_haut+html_mil+html_bas;
		//TODO timeOut a mettre en param
		//setTimeout("closeSup()",30000);
	}
	isHidden = false;
}

function closeSup()
{
	if(document.getElementById)
	{
		document.getElementById("CktlLayerDiv").style.width = 1;
		document.getElementById("CktlLayerDiv").style.height = 1;
		document.getElementById("CktlLayerDiv").innerHTML = '';
	}
	isHidden = true;
}

/**
* determiner les valeurs de largeur et hauteur de la fenetre
*/
function fillSize() 
{
	if (isHidden) 
	{
		haut = 0;
		larg = 0;
	}
	else 
	{
		if (isIE) 
		{
			haut = document.body.clientHeight;
			larg = document.body.clientWidth;
		} 
		else 
		{
			haut = window.innerHeight;
			larg = window.innerWidth;
		}
		// TODO rajouter la gestion des ascenceurs
	}
	haut += "px";
	larg += "px";
}


/**
 * fonction appell�e lors du redimensionnement de la fenetre
 * la div qui masque le fond doit suivre la nouvelle zone. 
*/
function resizeWindow()
{
	fillSize();
	var div = document.getElementById("CktlLayerDiv");
	var table = document.getElementById("CktlLayerTable");
	if (div) 
	{
		div.style.height = haut;
		div.style.width = larg;
	}
	if (table)
	{
		table.style.height = haut;
		table.style.width = larg;
	}
}



function cktlLayer(abgcolor, aborder, atexte, aclose, aurl, w, h, aimgClose, aimgTransparent, atitle)
{
	bgcolor = abgcolor;
	border = aborder;
	texte = atexte;
	close = aclose;
	url =  aurl;
	imgClose =  aimgClose;
	imgTransparent = aimgTransparent;
	title = atitle;
	
	fillSize();

	html_haut ="<table id='CktlLayerTable' background='" + imgTransparent + "' width='"+larg+"' height='"+haut+"'><tr><td valign='middle' align='center'>";
	html_bas = "</td></tr></table>";

	if (title == null || title == '')
	{
		title = "&nbsp;Information";
	}		

	/* 
	A VOUS DE MODIFER CES LIGNES 
	N'OUBLIEZ UN LIEN DE CE TYPE :
	<A HREF=# onClick="closeSup();return(false)">texte ou image</A>
	POUR POUVOIR REFERMER LA FENETRE SI LE VISITEUR N'EST PAS INTERESSE
	*/
	html_mil = "<table style='background: "+border+"' cellpadding='2' cellspacing='0'>" + 	 
					"<tr>" + 
						"<td>" +
							"<table style='margin-left: auto;margin-right: auto;text-align: left;background: "+bgcolor+"' width='100%' cellpadding='0' cellspacing='0'>"+
								"<tr class=boxTitle>"+
									"<th align='left' width=99%>"+
										"<font class=titleInverse>" + title + "</font>" +
									"</th>"+
									"<td align='right'>"+
										"<a href='#' onclick='closeSup();return(false)' title='" + close + "'><img src='"+imgClose+"' border=0/></a>"+
									"</td>"+
								"</tr>"+
								"<tr>"+
									"<td style='width: "+w+"px;height: "+h+"px;' align='center' onmouseover=this.style.cursor='pointer'; onclick=";
	
									if (url == null || url == '') 
									{
										html_mil += "'closeSup();return(false)";
									}
									else
									{
										html_mil += "window.location.href='" +url;
									}
									html_mil += "'>" + texte+ "</td>";
					html_mil += "</tr>";
	html_mil += "</table></td></tr></table>";
	window.onload = showsup;
}