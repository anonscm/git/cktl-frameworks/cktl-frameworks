/* 
Fonctions Javascript permettant d'encoder une url 'mailto' contenant
des caracteres speciaux en mailto conforme.
*/

// CREATE A MAILTO URL OR MAILTO ANCHOR TAG
function createMailto(sourceForm, targetField, urlType) {

    // AUTHOR:  Robert W. Husted
    // COMPANY: Netscape Communications
    // DATE:    10/22/98

    var to      = sourceForm.to.value;
    var cc      = sourceForm.cc.value;
    var bcc     = sourceForm.bcc.value;
    var subject = sourceForm.subject.value;
    var body    = sourceForm.body.value;

    var urltext = "";

    // IF THE VALUE IS SET, INCLUDE IT IN THE URL
    if (to != "") {
        urltext += to;
    }
    else {
        alert("Sorry, you must fill out the 'To' field");
        sourceForm.to.focus();
        return(1);
    }
    if (cc != "") {
        urltext = addDelimiter(urltext);
        urltext += "CC=" + cc;
    }
    if (bcc != "") {
        urltext = addDelimiter(urltext);
        urltext += "BCC=" + bcc;
    }
    if (subject != "") {
        urltext = addDelimiter(urltext);
        urltext += "Subject=" + escape(subject);
    }
    if (body != "") {
        urltext = addDelimiter(urltext);
        urltext += "Body=" + escape(body);
    }

    if (urlType == "url") {
        urltext = "mailto:" + urltext;
    }
    else {
        urltext = "<A HREF=\"mailto:" + urltext + "\">linkText</A>";
    }

    // PUT THE NEW URL IN THE FORM FIELD
    targetField.value = urltext;

    // GIVE THE FIELD FOCUS AND HIGHLIGHT THE TEXT -- 
    // TO FACILITATE EASY COPYING AND PASTING OF THE NEW URL
    targetField.focus();
    targetField.select();
    return(1);
}


// ADD THE "?" OR "&" NAME/VALUE SEPARATOR CHARACTER
function addDelimiter(inputString) {

    var inString = inputString;

    // IF '?' NOT FOUND, THEN THIS IS THE FIRST NAME/VALUE PAIR
    if (inString.indexOf("?") == -1) {
        inString += "?";
    }
    // ELSE IT'S A SUBSEQUENT NAME/VALUE PAIR, SO ADD THE '&' CHARACTER
    else {
        inString += "&";
    }
    return inString;
}