/**
 * Methodes permettant la gestion du composant
 * pour le formatage des heures : CktlTimeField
 */

/**
 * @param tf : le textfield HTML
 * @param restrict24 : s'agit d'une heure au format "horaire"
 * si oui, la saisie est limitee de 0:00 a 23:59
 * si non, on peut saisir de -999:59 a +999:59 
 */
function formatter(tf, restrict24) {

	var tfValue = tf.value;
   	var length = tfValue.length;
   	var newTfValue = "";
   
   
   	// recuperation du signe (si pas de restriction sur 24h00)
   	var negatif = false;
   	if (restrict24 == false) {	   
		if (length > 0 && tfValue.indexOf("-") == 0) {
	   		negatif = true;
	   		tfValue = tfValue.substr(1, length);
	   }
	}      
   	// Suppression des caracteres indesirables
   	for(var i = 0; i < length; i++)
   	{
      var unChar = tfValue.charAt(i);
      if(":0123456789".indexOf(unChar) >= 0)
      {
         newTfValue += unChar;
      }
   }
   tfValue = newTfValue;
   tf.value = tfValue;
   length = tfValue.length;
   
   if(length > 0)
   {
      var decalage = 1; if (restrict24) {	decalage = 0; }
      var index = tfValue.indexOf(":");
      var heures = "";
      var minutes = "";
      if(index == - 1)
      {
         if(length <= 2+decalage)
         {
            heures = tfValue.substr(0, length);
            minutes = "0";
         }
         else 
         {
         	if(length == 3+decalage)
         	{
            	heures = tfValue.substr(0, 1+decalage);
            	minutes = tfValue.substr(1+decalage, 2+decalage);
         	}
         	else 
         	{
            	heures = tfValue.substr(0, 2+decalage);
            	minutes = tfValue.substr(2+decalage, 2);
         	}
      	}
      }
      else 
      {
         if(length > 1)
         {
            heures = tfValue.substr(0, index);
            if(length - 1 == index)
            {
               minutes = "00";
            }
            else 
            {
               minutes = tfValue.substr(index + 1, length - index);
            }
         }
         else 
         {
            if (restrict24 == true) {alert('Horaire invalide');} else {alert('Duree invalide');}
         }
      }
      if((restrict24 == true && parseInt(heures) > 23) ||(parseInt(minutes) > 59))
      {
            if (restrict24 == true) {alert('Horaire invalide');} else {alert('Duree invalide');}
      }
      else 
      {
         if(heures.length < 2 && parseInt(heures) < 10)
         {
            heures = "0".concat(parseInt(heures));
         }
         if (restrict24 == false && (heures.length < 3 && parseInt(heures) < 100))
         {
            heures = "0".concat(parseInt(heures));
         }
         if(minutes.length < 2 && parseInt(minutes) < 10)
         {
            minutes = minutes + "0";
         }
         tf.value = heures + ':' + minutes;
      }            
      if (negatif == true) {
      	tf.value = '-' + tf.value;
      }
   }   
}
