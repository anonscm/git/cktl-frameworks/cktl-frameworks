package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;

/**
 * Cherche les structures orgunit en explorant la hiérarchie des structures
 */
public class StructureExplorerService {
	private static final Integer MAX_LOOP_COUNTER = 100;

	List<IStructure> getListeStructuresParentes(IStructure structure) {

		List<IStructure> structures = new ArrayList<IStructure>();
		int n = 0;
		if (structure != null) {
			while (structure.toStructurePere() != null && n < MAX_LOOP_COUNTER) {
				structures.add(structure.toStructurePere());
				structure = structure.toStructurePere();
				n++;
			}
		}
		return structures;
	}

	/**
	 * @param structures liste des structures pour lesquelles on cherche la hiérarchie
	 * @return liste contenant les structures et leur hiérarchie
	 */
	public List<IStructure> getListeStructureAvecHierarchie(List<IStructure> structures) {
		List<IStructure> toutesStructures = new ArrayList<IStructure>();
		for (IStructure structure : structures) {
			ajouteStructure(structure, toutesStructures);
			for (IStructure structureParent : getListeStructuresParentes(structure)) {
				ajouteStructure(structureParent, toutesStructures);
			}
		}
		return toutesStructures;
	}

	private void ajouteStructure(IStructure structure, List<IStructure> structures) {
		if (!structures.contains(structure)) {
			structures.add(structure);
		}
	}
}
