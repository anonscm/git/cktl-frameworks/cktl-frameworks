package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.cocktail.fwkcktlcdmfrgenerator.serveur.FwkCktlCdmfrGeneratorParamManager;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlwebapp.common.CktlLog;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

/**
 * genere une extraction CDMFR
 */
public class GenerateurCdmfr {
	public static final String XML_ENCODING = "UTF-8";
	private String nomDossierSortie;
	private FwkCktlCdmfrGeneratorParamManager paramManager = new FwkCktlCdmfrGeneratorParamManager();
	private StructureExplorerService structureExplorerService = new StructureExplorerService();

	/**
	 * @param nomDossierSortie nom du dossier de sortie des fichiers xml
	 */
	public GenerateurCdmfr(String nomDossierSortie) {
		setNomDossierSortie(nomDossierSortie);
	}

	public String getNomDossierSortie() {
		return nomDossierSortie;
	}

	public void setNomDossierSortie(String nomDossierSortie) {
		this.nomDossierSortie = nomDossierSortie;
	}

	/**
	 * Génère un document xml
	 * @return document xml
	 * @throws ParserConfigurationException exception
	 */
	public Document getNewDocument() throws ParserConfigurationException {
		// Creation du constructeur de document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Creation du document
		Document document = builder.newDocument();
		return document;
	}

	protected void exportDocument(String fileName, DOMSource source) throws Exception {
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer();
		transformer.setOutputProperty("encoding", XML_ENCODING);
		transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		transformer.setOutputProperty(OutputKeys.INDENT, "yes");

		// generation du fichier XML resultat
		FileOutputStream fichierXMLresultat;
		fichierXMLresultat = new FileOutputStream(fileName);

		StreamResult destination = new StreamResult(fichierXMLresultat);
		transformer.transform(source, destination);
		fichierXMLresultat.close();
	}

	/**
	 * Fonction permettant de produire le fichier XML résultat : génération des différents éléments XML souhaités
	 * @param versionDiplome diplome traité
	 * @param isHabilitation <code> true </code> si l'on exporte le CDMFR en utilisant le descriptif d'habilitation, <code> false </code> si l'on exporte le CDMFR
	 *          en utilisant le descriptif de publication
	 */
	public void generateDiplome(IVersionDiplome versionDiplome, Boolean isHabilitation) {
		try {
			Element racine = generateDOMDiplome(versionDiplome, isHabilitation);
			DOMSource source = new DOMSource(racine);
			exportDocument(getNomFichier(versionDiplome.getDiplome()), source);
		} catch (Exception e) {
			CktlLog.log(e.getMessage());
			e.printStackTrace();
		}
	}

	private Element generateDOMDiplome(IVersionDiplome versionDiplome, Boolean isHabilitation) throws Exception {
		Document document = getNewDocument();
		IStructure structurePrincipale = getStructure(versionDiplome);
		List<IIndividu> contacts = new ArrayList<IIndividu>();
		List<IStructure> structures = new ArrayList<IStructure>();
		structures.add(structurePrincipale);
		ajouterStructures(structures, structureExplorerService.getListeStructuresParentes(structurePrincipale));
		int i = 1;

		CktlLog.log(i + ") Génération des cdmfr:habilitation");
		HabilitationInfoGenerator habilitationInfoGenerator = new HabilitationInfoGenerator(document, structurePrincipale, isHabilitation);
		Element racine = habilitationInfoGenerator.getElementRacine();
		habilitationInfoGenerator.generateHabilitationDiplome(racine, versionDiplome.getDiplome());
		i++;

		CktlLog.log(i + ") Génération des properties");
		PropertiesInfoGenerator propertiesInfoGenerator = new PropertiesInfoGenerator(document, structurePrincipale, isHabilitation);
		propertiesInfoGenerator.generateProperties(racine);
		i++;

	
		CktlLog.log(i + ") Génération des program");
		ProgramInfoGenerator programInfoGenerator = new ProgramInfoGenerator(document, structurePrincipale, isHabilitation);
		programInfoGenerator.generateProgramDiplome(racine, versionDiplome);
		ajouterContacts(contacts, programInfoGenerator.getContacts());
		ajouterStructures(structures, programInfoGenerator.getStructures());
		i++;

		CktlLog.log(i + ") Génération des course");
		CourseInfoGenerator courseInfoGenerator = new CourseInfoGenerator(document, structurePrincipale, isHabilitation);
		for (ILien lien : programInfoGenerator.getLienComposantCours()) {
			courseInfoGenerator.generateCourse(racine, lien);
		}
		ajouterContacts(contacts, courseInfoGenerator.getContacts());
		ajouterStructures(structures, programInfoGenerator.getStructures());
		i++;
		
		CktlLog.log(i + ") Génération des orgUnit");
		OrgUnitInfoGenerator orgInfoGenerator = new OrgUnitInfoGenerator(document, structurePrincipale, isHabilitation);
		orgInfoGenerator.generateOrgUnits(racine, structures);
		ajouterContacts(contacts, orgInfoGenerator.getContacts());
		i++;

		CktlLog.log(i + ") Génération des contacts");
		PersonInfoGenerator personInfoGenerator = new PersonInfoGenerator(document, structurePrincipale, isHabilitation);
		personInfoGenerator.generateAllPersons(racine, contacts);
		return racine;

	}

	private IStructure getStructure(IVersionDiplome versionDiplome) {
		EOEditingContext editingContext = ((EOComposant) versionDiplome).editingContext();
		IStructure structure = getStructureEtablissement(versionDiplome.getDiplome());
		if (structure == null) {
			structure = getStructureDefaut(editingContext);
		}
		if (structure == null) {
			structure = getStructureRacine(editingContext);
		}
		return structure;
	}

	private String getNomFichier(IDiplome diplome) {
		return getNomDossierSortie() + "diplome" + diplome.code() + getDateFormatter(new Date()) + ".xml";
	}

	/**
	 * Retourne la String de format des dates
	 * @param dt1 la date passée pour être formatée
	 * @return le format par défaut pour les dates
	 */
	private String getDateFormatter(Date dt1) {
		String dt = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");
		dt = sdf.format(dt1);
		return dt;
	}

	private IStructure getStructureEtablissement(IDiplome diplome) {
		if (diplome.structures() != null && diplome.structures().size() > 0) {
			return diplome.structures().get(0);
		}
		return null;
	}

	private IStructure getStructureDefaut(EOEditingContext editingContext) {
		String rne = paramManager.getDefaultEtablissementRne();
		EOStructure structure = EOStructure.fetchByQualifier(editingContext, EOStructure.C_RNE.eq(rne));
		return structure;
	}

	private IStructure getStructureRacine(EOEditingContext editingContext) {
		EOQualifier qualifier = ERXQ.and(EOStructure.TO_TYPE_STRUCTURE.dot(EOTypeStructure.C_TYPE_STRUCTURE).eq(EOTypeStructure.TYPE_STRUCTURE_E),
		    EOStructure.C_STRUCTURE_PERE.eq(EOStructure.C_STRUCTURE));
		EOStructure structure = EOStructure.fetchByQualifier(editingContext, qualifier);
		return structure;
	}

	protected void ajouterContacts(List<IIndividu> contacts, List<IIndividu> contactsAajouter) {
		for (IIndividu individu : contactsAajouter) {
			if (!contacts.contains(individu)) {
				contacts.add(individu);
			}
		}
	}
	
	protected void ajouterStructures(List<IStructure> structures, List<IStructure> structuresAajouter) {
		for (IStructure structure : structuresAajouter) {
			if (!structures.contains(structure)) {
				structures.add(structure);
			}
		}
	}
}
