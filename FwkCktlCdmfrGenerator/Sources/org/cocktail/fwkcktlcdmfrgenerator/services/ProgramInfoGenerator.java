package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.EOLien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptifChampTitre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVocation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CalculateurSommeCreditECTS;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.CalculateurSommeCreditECTSBase;
import org.cocktail.fwkcktlscolpeda.serveur.metier.maquette.services.RegleCalculCreditECTSPourMaquette;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.CalculateurSommeHeure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.utils.ICalculateurSommeHeures;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import com.webobjects.eocontrol.EOEditingContext;

/**
 * Extrait les informations liées au programme (en l'occurence une formation/diplome)
 */
public class ProgramInfoGenerator extends CommonInfoGenerator {
	private List<ILien> lienComposantCours;
	private List<IComposant> composantCours;

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public ProgramInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
		lienComposantCours = new ArrayList<ILien>();
		composantCours = new ArrayList<IComposant>();

	}

	public List<ILien> getLienComposantCours() {
		return lienComposantCours;
	}

	public List<IComposant> getComposantCours() {
		return composantCours;
	}

	/**
	 * Génère la balise "program" décrivant le diplôme <code>diplome</code>
	 * @param element element auquel on rattache les infos de type diplome
	 * @param versionDiplome la version de diplome pour laquelle on fait l'extraction
	 */
	public void generateProgramDiplome(Element element, IVersionDiplome versionDiplome) {
		if (versionDiplome == null) {
			return;
		}

		IDiplome diplome = versionDiplome.getDiplome();
		if (diplome != null) {
			initialiseCreditEtHeures(versionDiplome);
			ajouteLesStructures(diplome);
			Element p = initialiseElementProgram(diplome);
			p.appendChild(generateProgramDescription(diplome));
			p.appendChild(generateQualification(versionDiplome));
			p.appendChild(generateTeachingLangage(diplome));
			p.appendChild(generateLevel(diplome));
			p.appendChild(generateLevelCode(diplome));
			p.appendChild(generateLearningObjective(diplome));
			p.appendChild(generateAdmissionInfo(diplome));
			p.appendChild(generateRecommendedPreRequisites(diplome));
			p.appendChild(generateFormalPreRequisites(diplome));
			p.appendChild(generateTeachingPlace(diplome));
			p.appendChild(generateTargetGroup(diplome));
			p.appendChild(generateProgramDuration(diplome));
			p.appendChild(generateProgramStructure(diplome));
			p.appendChild(generateRegulations(diplome));
			p.appendChild(generateContacts(diplome));
			generateSubProgram(p, versionDiplome);
			element.appendChild(p);

		}

	}

	private void initialiseCreditEtHeures(IVersionDiplome versionDiplome) {
		if (versionDiplome.liensChildsWithoutDuplicate() == null || versionDiplome.liensChildsWithoutDuplicate().size() != 1) {
			return;
		}

		ILien lien = versionDiplome.liensChildsWithoutDuplicate().get(0);
		CalculateurSommeCreditECTS calculateurSommeCreditECTS = new CalculateurSommeCreditECTSBase(new RegleCalculCreditECTSPourMaquette());
		calculateurSommeCreditECTS.calcule(lien);

		ICalculateurSommeHeures calculateurSommeHeure = new CalculateurSommeHeure(getEditingContext(lien), null);
		calculateurSommeHeure.calculerSommeHeuresEtudiant(lien);
	}

	private EOEditingContext getEditingContext(ILien lien) {
		if (lien instanceof EOLien) {
			return ((EOLien) lien).editingContext();
		}
		return null;
	}

	String getDiplomeName(IDiplome diplome) {
		String libelle = diplome.libelle();
		if (diplome.gradeUniversitaire() != null) {
			libelle = diplome.gradeUniversitaire().libelle() + " " + libelle;
		}
		return libelle;
	}

	/**
	 * @param versionDiplome version de diplome sur laquelle on fait le calcul
	 * @return nombre de credits ECTS pour ce diplome
	 */
	Integer getNbCreditECTS(IVersionDiplome versionDiplome) {
		if (versionDiplome.liensChildsWithoutDuplicate() == null || versionDiplome.liensChildsWithoutDuplicate().size() != 1) {
			return null;
		}

		ILien lien = versionDiplome.liensChildsWithoutDuplicate().get(0);

		BigDecimal creditECTS = lien.getCreditECTSCalcule();
		if (creditECTS != null) {
			return creditECTS.intValue();
		}
		return null;
	}

	private Element initialiseElementProgram(IDiplome diplome) {

		String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_PROGRAM, diplome.code());
		String name = getDiplomeName(diplome);
		return initialiseElementProgram(ConstantesXML.TAG_PROGRAM, identifiant, name);
	}

	private Element initialiseElementProgram(String programTag, String identifiant, String name) {
		Element p = (Element) getDoc().createElement(programTag);
		p.setAttribute(ConstantesXML.TAG_ATTR_ID, identifiant);

		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_ID);
		getXmlProducer().ajouteTextNode(e, identifiant);
		p.appendChild(e);

		// Nom du diplome
		e = generateProgramName(name);

		p.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_CODE);
		// TODO aller rechercher les valeur du code
		p.appendChild(e);
		return p;
	}

	private Element generateProgramName(String name) {
		Element e;
		e = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_NAME);
		Element t = (Element) getDoc().createElement(ConstantesXML.TAG_CDMFR_TEXT);
		getXmlProducer().ajouteTextNode(t, name);
		e.appendChild(t);
		return e;
	}

	private Element generateProgramDescription(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_DESCRIPTION);
		getXmlProducer().ajouteStringAttribute(element, ConstantesXML.TAG_PROGRAM_DESCRIPTION_ATTR_NATURE, ConstantesXML.CONST_NATURE_PROGRAM_DIPLOME);
		String texteDescription = getDescriptifInfoGenerator().getLibelle(getComposantDescriptif(diplome));
		element.appendChild(getXmlProducer().creeInfoBlockAvecParagraphe(texteDescription));
		return element;
	}

	private Element generateQualification(IVersionDiplome versiondiplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_QUALIFICATION);

		Element child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_QUALIFICATION_NAME);
		element.appendChild(child);

		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_QUALIFICATION_DESCRIPTION);
		element.appendChild(child);

		child = (Element) getDoc().createElement(ConstantesXML.TAG_CREDITS);
		getXmlProducer().ajouteIntegerAttribute(child, ConstantesXML.TAG_ATTR_ECTS_CREDITS, getNbCreditECTS(versiondiplome));
		element.appendChild(child);

		IDiplome diplome = versiondiplome.getDiplome();
		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_DEGREE);
		TypeDiplomeSet typeDiplome = TypeDiplomeSet.getTypeDiplome(diplome.typeFormation());
		if (typeDiplome != TypeDiplomeSet.TYPE_DIPLOME_UNKNOWN) {
			getXmlProducer().ajouteStringAttribute(child, ConstantesXML.TAG_PROGRAM_ATTR_DEGREE, typeDiplome.getLibelle());
		}
		element.appendChild(child);

		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_PROFESSION);
		child.appendChild(getXmlProducer().creeInfoBlockAvecParagraphe(
		    getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_METIER)));
		element.appendChild(child);

		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_STUDY_QUALIFICATION);
		child.appendChild(getXmlProducer().creeInfoBlockAvecParagraphe(
		    getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_COMPETENCE)));
		element.appendChild(child);
		return element;
	}

	private Element generateTeachingLangage(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_TEACHING_LANGUAGE);
		getXmlProducer().ajouteStringAttribute(element, ConstantesXML.TAG_ATTR_PROGRAM_TEACHING_LANG, "Français");
		return element;
	}

	private Element generateLevel(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_LEVEL);
		element.setAttribute(ConstantesXML.TAG_PROGRAM_ATTR_LEVEL, diplome.gradeUniversitaire().type());

		IVocation vocation = diplome.vocation();
		if (vocation != null) {
			element.setAttribute(ConstantesXML.TAG_PROGRAM_ATTR_YIELDING_COMPETENCE, vocation.libelle());
			element.setAttribute(ConstantesXML.TAG_PROGRAM_ATTR_YIELDING_COMPETENCE_CODE, vocation.code());
		}

		return element;
	}

	private Node generateLevelCode(IDiplome diplome) {
		LevelSet level = getLevelSet(diplome);

		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_LEVEL_CODE);
		getXmlProducer().ajouteStringAttribute(element, ConstantesXML.TAG_PROGRAM_ATTR_CODE_SET, level.getCode());

		return element;
	}

	LevelSet getLevelSet(IDiplome diplome) {
		LevelSet level = LevelSet.LEVEL_Inconnu;

		Integer dureeDiplome = null;
		if (diplome.gradeUniversitaire() != null && diplome.gradeUniversitaire().nbUnitesTemps() != null) {
			dureeDiplome = diplome.gradeUniversitaire().nbUnitesTemps();
		}
		if (diplome.niveauAccesDiplome() != null && diplome.niveauAccesDiplome().niveau() != null) {
			if (dureeDiplome == null) { // unboxing
				dureeDiplome = 0;
			}
			dureeDiplome += diplome.niveauAccesDiplome().niveau();
		}

		if (dureeDiplome != null) {
			level = LevelSet.getLevelSet(dureeDiplome);
		}
		return level;
	}

	private Element generateLearningObjective(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_LEARNING_OBJECTIVES);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_CONNAISSANCE);
		if (text != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		return element;
	}

	/**
	 * @param diplome diplome correpondant au programme
	 * @param listeSpn liste des specialisations
	 * @return generation de la partie "Admission"
	 */
	private Element generateAdmissionInfo(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_ADMISSION_INFO);
		Element child = (Element) getDoc().createElement(ConstantesXML.TAG_ADMISSION_DESCRIPTION);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_RECRUTEMENT);
		if (text != null) {
			child.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		element.appendChild(child);

		// TODO information manquante
		Integer nbCapaciteMax = 0;
		if (nbCapaciteMax > 0) {
			child = (Element) getDoc().createElement(ConstantesXML.TAG_STUDENT_PLACES);
			getXmlProducer().ajouteTextNode(child, new Integer(nbCapaciteMax).toString());
			element.appendChild(child);
		}

		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_ECTS_REQUIRED);
		element.appendChild(child);

		child = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_STUDENT_STATUS);
		element.appendChild(child);

		return element;
	}

	private Element generateFormalPreRequisites(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_FORMAL_PRE_REQUISITES);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_PREREQUIS);
		if (text != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		return element;
	}

	private Element generateRecommendedPreRequisites(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_RECOMMENDED_PRE_REQUISITES);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_RECOMMANDATION);
		if (text != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		return element;
	}

	private Element generateTeachingPlace(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_TEACHING_PLACE);

		IStructure structure = getStructure();
		if (structure != null && structure.toRepartPersonneAdresses() != null && structure.toRepartPersonneAdresses().size() > 0) {
			IAdresse adresse = structure.toRepartPersonneAdresses().get(0).toAdresse();
			if (adresse != null) {
				generateAdresse(element, adresse, null);
			}
		}
		return element;
	}

	private Element generateTargetGroup(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_TARGET_GROUP);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_PUBLIC_SPEC);
		if (text != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		return element;
	}

	private Element generateProgramDuration(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_DURATION);
		if (diplome.gradeUniversitaire() != null && diplome.gradeUniversitaire().nbUnitesTemps() != null) {
			String str = String.format("%d année(s)", diplome.gradeUniversitaire().nbUnitesTemps());
			getXmlProducer().ajouteTextNode(element, str);
		}
		return element;
	}

	private Element generateProgramStructure(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_STRUCTURE);
		return element;
	}

	private Element generateRegulations(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_REGULATIONS);
		String text = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(diplome), IDescriptifChampTitre.DIP_MODALITE_EVALUATION);
		if (text != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(text, true));
		}
		return element;
	}

	private Element generateContacts(IDiplome diplome) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_CONTACTS);

		for (IResponsableComposant responsableComposant : diplome.responsablesComposant()) {
			if (responsableComposant.responsable() instanceof IIndividu) {
				Element child = generateContact(getStructure(), (IIndividu) responsableComposant.responsable(), ConstantesXML.ROLE_CONTACT_CONTACT);
				element.appendChild(child);
				ajouteIndividuAuxContacts((IIndividu) responsableComposant.responsable());
			}

			if (responsableComposant.responsable() instanceof IStructure) {
				Element child = generateContact(getStructure(), (IStructure) responsableComposant.responsable(), ConstantesXML.ROLE_CONTACT_CONTACT);
				element.appendChild(child);
				ajouteStructureAuxStructures((IStructure) responsableComposant.responsable());
			}
		}

		for (IStructure structure : diplome.structures()) {
			Element child = generateContact(getStructure(), structure, null);
			element.appendChild(child);
			ajouteStructureAuxStructures((IStructure) structure);
		}
		return element;
	}

	void generateSubProgram(Element p, IComposant composantParent) {
		List<? extends ILien> liensParents = composantParent.liensParentsWithoutDuplicate();
		ordonneLiens(liensParents);

		for (ILien lien : liensParents) {
			IComposant composant = lien.child();
			if (isElementStructure(composant)) {
				Element child = generateSubProgramElementStructure(lien);
				if (child != null) {
					p.appendChild(child);
					generateSubProgram(child, composant);
				}
			}
		}

		Element child = generateSubProgramCourse(composantParent);
		if (child != null) {
			p.appendChild(child);

		}
	}

	void ajouteLiensTypeCourse(IComposant composant, List<ILien> listeLiens) {
		listeLiens.addAll(getLiensTypeCourse(composant));
	}

	List<ILien> getLiensTypeCourse(IComposant composant) {
		List<ILien> liens = new ArrayList<ILien>();

		for (ILien lien : composant.liensParentsWithoutDuplicate()) {
			if (isElementCourse(lien.child())) {
				liens.add(lien);
			}
		}
		ordonneLiens(liens);
		return liens;
	}

	private void ordonneLiens(List<? extends ILien> liens) {
		Collections.sort(liens, new Comparator<ILien>() {
			public int compare(ILien lien1, ILien lien2) {
				return (lien1.ordre() - lien2.ordre());
			}
		});
	}

	/**
	 * @param composant composant testé
	 * @return <code> true </code> si le composant doit être extrait dans la balise subProgram
	 */
	private boolean isElementStructure(IComposant composant) {
		return ((composant instanceof IPeriode) || (composant instanceof IParcours) || (composant instanceof IRegroupement));
	}

	/**
	 * @param lien.child() composant testé
	 * @return <code> true </code> si le composant doit être extrait dans la balise subProgram
	 */
	private boolean isElementCourse(IComposant composant) {
		return ((composant instanceof IUE) || ((composant instanceof IEC)));
	}

	private Element generateSubProgramElementStructure(ILien lien) {
		IComposant composant = lien.child();
		ajouteLesStructures(composant);

		String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_SUBPROGRAM, composant.code());
		Element element = initialiseElementProgram(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM, identifiant, composant.libelle());

		if (lien.getCreditECTSCalcule() != null) {
			Element child = (Element) getDoc().createElement(ConstantesXML.TAG_CREDITS);
			getXmlProducer().ajouteBigDecimalAttribute(child, ConstantesXML.TAG_ATTR_ECTS_CREDITS, lien.getCreditECTSCalcule());
			element.appendChild(child);
		}

		return element;
	}

	private Element generateSubProgramCourse(IComposant composant) {
		if (!hasChildTypeCourse(composant)) {
			return null;
		}

		List<ILien> listeLiens = getLiensTypeCourse(composant);
		ajouteAuxComposantsCours(listeLiens);
		ajouteAuxComposantsCours(getAllDescendantTypeCours(listeLiens));

		return generateSubProgramElementCourse(listeLiens);
	}

	private List<ILien> getAllDescendantTypeCours(List<ILien> listeLiens) {
		List<ILien> liensDescendant = new ArrayList<ILien>();
		for (ILien lien : listeLiens) {
			remplitDescendantTypeCours(lien, liensDescendant);
		}
		return liensDescendant;
	}

	private void remplitDescendantTypeCours(ILien lien, List<ILien> listeLiens) {
		for (ILien lienchild : lien.child().liensParentsWithoutDuplicate()) {
			if (isElementCourse(lienchild.child())) {
				listeLiens.add(lienchild);
			}
			remplitDescendantTypeCours(lienchild, listeLiens);
		}

	}

	private void ajouteAuxComposantsCours(List<ILien> listeLiens) {
		for (ILien lien : listeLiens) {
			if (!getComposantCours().contains(lien.child())) {
				getComposantCours().add(lien.child());
				getLienComposantCours().add(lien);
			}
		}
	}

	private boolean hasChildTypeCourse(IComposant composant) {
		for (ILien lien : composant.liensParentsWithoutDuplicate()) {
			if (isElementCourse(lien.child())) {
				return true;
			}
		}
		return false;
	}

	private Element generateSubProgramElementCourse(List<ILien> liens) {
		if (liens.size() == 0) {
			return null;
		}

		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_PROGRAM_STRUCTURE);

		for (ILien lien : liens) {
			IComposant composant = lien.child();
			Element child = (Element) getDoc().createElement(ConstantesXML.TAG_REF_COURSE);
			String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_COURSE, composant.id().toString());
			child.setAttribute(ConstantesXML.TAG_ATTR_REF_ID, identifiant);
			element.appendChild(child);
		}

		return element;
	}

}
