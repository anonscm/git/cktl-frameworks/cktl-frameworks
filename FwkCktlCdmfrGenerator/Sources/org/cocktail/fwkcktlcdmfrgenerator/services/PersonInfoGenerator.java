package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.ICompte;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Extrait les informations liées à une personne (en l'occurence un ue ou un ec)
 */
public class PersonInfoGenerator extends CommonInfoGenerator {

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public PersonInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
	}

	/**
	 * @param elt élément de rattachement
	 * @param listePerson liste des personnes à exporter
	 */
	public void generateAllPersons(Element elt, List<IIndividu> listePerson) {
		for (IIndividu individuUlr : listePerson) {
			generatePerson(elt, individuUlr);
		}
	}

	/**
	 * @param elt élément de rattachement
	 * @param indPers individu dont on veut extraire les information
	 */
	public void generatePerson(Element elt, IIndividu indPers) {
		if (indPers != null) {
			Element c = (Element) getDoc().createElement(ConstantesXML.TAG_PERSON);
			c.setAttribute(ConstantesXML.TAG_ATTR_ID, generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_PERSON, getIndividuULRIdentifiant(indPers)));
			c.appendChild(generatePersonID(indPers));
			c.appendChild(generateNamePerson(indPers));
			c.appendChild(generateTitre(indPers));
			generateRoles(c, indPers);
			c.appendChild(generateAffiliation(indPers));
			generateContactsData(c, indPers);
			elt.appendChild(c);
		}
	}

	private Element generatePersonID(IIndividu indPers) {
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_PERSONID);
		getXmlProducer().ajouteTextNode(e, indPers.persId().toString());
		return e;
	}

	private Element generateNamePerson(IIndividu indPers) {
		Element c = (Element) getDoc().createElement(ConstantesXML.TAG_PERSON_NAME);

		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_NAME_GIVEN);
		e.appendChild((Text) getDoc().createTextNode(indPers.prenom()));
		c.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_NAME_FAMILY);
		e.appendChild((Text) getDoc().createTextNode(indPers.nomUsuel()));
		c.appendChild(e);
		return c;
	}

	private Element generateTitre(IIndividu indPers) {
		Element c = (Element) getDoc().createElement(ConstantesXML.TAG_TITRE);
		return c;
	}

	private void generateRoles(Element c, IIndividu indPers) {
		Element e;
		e = (Element) getDoc().createElement(ConstantesXML.TAG_ROLE);
		if (indPers.indQualite() != null) {
			Element t = (Element) getDoc().createElement("text");
			t.appendChild((Text) getDoc().createTextNode(indPers.indQualite()));
			e.appendChild(t);
		} else {
			Element t = (Element) getDoc().createElement("text");
			t.appendChild((Text) getDoc().createTextNode(ConstantesXML.ROLE_CONTACT));
			e.appendChild(t);
		}
		c.appendChild(e);
	}

	private Element generateAffiliation(IIndividu indPers) {
		Element c = (Element) getDoc().createElement(ConstantesXML.TAG_AFFILIATION);
		return c;
	}

	private void generateContactsData(Element eltCurr, IIndividu indPers) {
		// TODO à généraliser l'indivivdu peut avoir 0 ou plusieurs éléments de type contactData
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_CONTACT_DATA);

		Element n = (Element) getDoc().createElement(ConstantesXML.TAG_CONTACT_NAME);
		Element f = (Element) getDoc().createElement("text");
		f.appendChild((Text) getDoc().createTextNode(indPers.nomUsuel() + " " + indPers.prenom()));
		n.appendChild(f);

		generateAdrPerson(e, indPers);

		Element h = (Element) getDoc().createElement(ConstantesXML.TAG_HEURE_VISITE);
		e.appendChild(h);

		generateTelFaxPerson(e, indPers);
		generateEmailPerson(e, indPers);

		e.appendChild(n);
		eltCurr.appendChild(e);
	}

	private void generateAdrPerson(Element eltCurr, IIndividu indPers) {

		if (indPers.toAdresses() != null && indPers.toAdresses().size() > 0) {
			IAdresse adresse = indPers.toAdresses().get(0);
			generateAdresse(eltCurr, adresse, null);
		}
	}

	private void generateTelFaxPerson(Element eltCurr, IIndividu indPers) {
		boolean isTelInsere = false, isFaxInsere = false;

		for (ITelephone telephone : indPers.toTelephones()) {
			String numTel = telephone.noTelephone();
			numTel = numTel.replaceFirst("0", "+33.(0)");
			if (!isTelInsere && telephone.toTypeNoTel().cTypeNoTel().equals(EOTypeNoTel.TYPE_NO_TEL_TEL)) {
				Element e = (Element) getDoc().createElement(ConstantesXML.TAG_TELEPHONE);
				e.appendChild((Text) getDoc().createTextNode(numTel));
				eltCurr.appendChild(e);
				isTelInsere = true;
			} else if (!isFaxInsere && telephone.toTypeNoTel().cTypeNoTel().equals(EOTypeNoTel.TYPE_NO_TEL_FAX)) {
				Element e = (Element) getDoc().createElement(ConstantesXML.TAG_FAX);
				e.appendChild((Text) getDoc().createTextNode(numTel));
				eltCurr.appendChild(e);
				isFaxInsere = true;
			}
		}
	}

	private void generateEmailPerson(Element eltCurr, IIndividu indPers) {
		for (ICompte compte : indPers.toComptes()) {
			ICompteEmail compteEmail = getCompteEmailPrincipal(compte);
			if (compteEmail != null) {
				Element e = (Element) getDoc().createElement(ConstantesXML.TAG_EMAIL);
				String mail = compteEmail.cemEmail() + "@" + compteEmail.cemDomaine();
				getXmlProducer().ajouteTextNode(e, mail);
				eltCurr.appendChild(e);
				break;
			}
		}
	}

	private ICompteEmail getCompteEmailPrincipal(ICompte compte) {
		if (isCompteValide(compte)) {
			if (compte.toCompteEmails() != null && compte.toCompteEmails().size() > 0) {
				return compte.toCompteEmails().get(0);
			}
		}
		return null;
	}

	private boolean isCompteValide(ICompte compte) {
		if (compte != null && compte.toVlans() != null) {
			return compte.toVlans().cVlan().equals(EOVlans.VLAN_P) || compte.toVlans().cVlan().equals(EOVlans.VLAN_R);
		}
		return false;
	}
}
