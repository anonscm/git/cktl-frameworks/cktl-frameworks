package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptifChampTitre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposantRole;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkgspot.serveur.metier.eof.IImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.ILocal;
import org.cocktail.fwkgspot.serveur.metier.eof.IRepartBatImpGeo;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Extrait les informations liées à un "Cours" (en l'occurence un ue ou un ec)
 */
public class CourseInfoGenerator extends CommonInfoGenerator {

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public CourseInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
	}

	/**
	 * Génère la balise "program" décrivant l'élement de cours <code>ue</code>
	 * @param element element auquel on rattache les infos de type diplome
	 * @param lien le lien qui contient comme child l'élément pour lequels on fait l'extraction
	 */
	public void generateCourse(Element element, ILien lien) {
		if (lien == null) {
			return;
		}
		setComposantDescriptif(null);

		IComposant composantPedagogique = lien.child();
		if (composantPedagogique != null) {
			ajouteLesStructures(composantPedagogique);
			Element c = initialiseElementCourse(composantPedagogique);
			c.appendChild(generateDescription(composantPedagogique));
			c.appendChild(generateLearningObjectives(composantPedagogique));
			c.appendChild(generateCredits(lien.getCreditECTSCalcule(), lien.getSommeMinutesEnseigneesCalculee()));
			c.appendChild(generateAdmissionInfo(composantPedagogique));
			c.appendChild(generateRecommendedPreRequisites(composantPedagogique));
			c.appendChild(generateFormalPreRequisites(composantPedagogique));
			c.appendChild(generateTeachingPlace(composantPedagogique));
			c.appendChild(generatePublicCible(composantPedagogique));
			c.appendChild(generateFormOfTeaching(composantPedagogique));
			c.appendChild(generateBenefits(composantPedagogique));
			// TODO retrouver à quoi correspond le tag syllabus
//			c.appendChild(generateSyllabus(ue));
			c.appendChild(generateUniversalAdjustement(composantPedagogique));
			c.appendChild(generateContacts(composantPedagogique));
			generateECInstructionLanguage(c, composantPedagogique);
			generateECFormOfAssessment(c, composantPedagogique);
			generateECActivites(c, composantPedagogique);
			element.appendChild(c);
		}
	}

	private Element initialiseElementCourse(IComposant composant) {
		Element c = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE);
		String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_COURSE, composant.id().toString());
		c.setAttribute(ConstantesXML.TAG_ATTR_ID, identifiant);

		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_ID);
		getXmlProducer().ajouteTextNode(e, identifiant);
		c.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_NAME);
		Element t = getXmlProducer().ajouteTextElement(composant.libelle());
		e.appendChild(t);
		c.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_CODE);
		if (composant instanceof IUE) {
			e.setAttribute(ConstantesXML.TAG_ATTR_CODE_SET, ConstantesXML.CODE_SET_UE);
		} else if (composant instanceof IEC) {
			e.setAttribute(ConstantesXML.TAG_ATTR_CODE_SET, ConstantesXML.CODE_SET_EC);
		}
		getXmlProducer().ajouteTextNode(e, composant.code());
		c.appendChild(e);

		return c;
	}

	private Element generateDescription(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_DESCRIPTION);
		String description = getDescriptifInfoGenerator().getLibelle(getComposantDescriptif(composant));

		if (description != null) {
			// TODO clarifier pourquoi il y a deux tags infoblock
			Element infoBlock = (Element) getDoc().createElement(ConstantesXML.TAG_INFO_BLOCK_CDMFR);
			Element paragraph = (Element) getDoc().createElement(ConstantesXML.TAG_PARAGRAPHE);
			getXmlProducer().ajouteTextNode(paragraph, description);
			infoBlock.appendChild(paragraph);
			element.appendChild(infoBlock);
		}
		appendChildReference(element, composant);
		return element;
	}

	void appendChildReference(Element element, IComposant composant) {
		if (!(composant instanceof IUE) || !hasChildsTypeEC(composant)) {
			return;
		}
		Element elementContent = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_CONTENTS_EC);
		for (IComposant child : composant.childs()) {
			if (child instanceof IEC) {

				Element refCourse = (Element) getDoc().createElement(ConstantesXML.TAG_REF_COURSE);
				String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_COURSE, child.id().toString());
				refCourse.setAttribute(ConstantesXML.TAG_ATTR_REF_ID, identifiant);
				elementContent.appendChild(refCourse);
			}
		}
		element.appendChild(elementContent);
	}

	private boolean hasChildsTypeEC(IComposant composant) {
		for (IComposant child : composant.childs()) {
			if (child instanceof IEC) {
				return true;
			}
		}
		return false;
	}

	private Element generateLearningObjectives(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_LEARNING_OBJECTIVES);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_OBJECTIFS);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	private Element generateCredits(BigDecimal points, Integer horairesEtudiant) {
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_CREDITS);
		if (points != null && points.intValue() > 0) {
			e.setAttribute(ConstantesXML.TAG_ATTR_ECTS_CREDITS, points.toBigInteger().toString());
		}

		if (horairesEtudiant != null) {
			BigDecimal heures = BigDecimal.valueOf((long) horairesEtudiant).divide(BigDecimal.valueOf(60), 2, RoundingMode.HALF_UP);
			e.setAttribute(ConstantesXML.TAG_ATTR_TOTAL_WORK_LOAD, heures.toBigInteger().toString());
		}
		return e;
	}

	/**
	 * @param ueComp complement d'information à l'UE
	 * @return element concernant les condition d'accès à l'UE
	 */
	private Element generateAdmissionInfo(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_ADMISSION_INFO);
		Element child = (Element) getDoc().createElement(ConstantesXML.TAG_ADMISSION_DESCRIPTION);

		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_ADMISSION);
		if (texte != null) {
			child.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		element.appendChild(child);

		Integer nbPlaces = getNbPlacesMaxEc(composant);
		if (nbPlaces != null) {
			child = (Element) getDoc().createElement(ConstantesXML.TAG_STUDENT_PLACES);

			getXmlProducer().ajouteTextNode(child, nbPlaces.toString());
			element.appendChild(child);
		}
		return element;
	}

	private Integer getNbPlacesMaxEc(IComposant composant) {
		Integer nbPlacesMax = null;

		for (IComposant child : composant.childs()) {
			if (child instanceof IAP) {
				Integer seuil = ((IAP) child).seuil();
				if (seuil != null) {
					if (nbPlacesMax == null) {
						nbPlacesMax = seuil;
					}
					if (nbPlacesMax < seuil) {
						nbPlacesMax = seuil;
					}
				}
			}

		}
		return nbPlacesMax;
	}

	private Element generateRecommendedPreRequisites(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_RECOMMENDED_PRE_REQUISITES);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_RECOMMANDATION);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	private Element generateFormalPreRequisites(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_FORMAL_PRE_REQUISITES);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_PREREQUIS);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	private Element generateTeachingPlace(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_TEACHING_PLACE);

		IAdresse adresse = getAdresse(composant);
		if (adresse != null) {
			generateAdresse(element, adresse, null);
		}

		return element;
	}

	private IAdresse getAdresse(IComposant composant) {
		if (composant.implantationsGeos() != null && composant.implantationsGeos().size() > 0) {
			return (getPremiereAdresse(composant.implantationsGeos().get(0)));
		}

		return null;
	}

	private IAdresse getPremiereAdresse(IImplantationGeo composant) {
		if (composant != null && composant.toRepartBatImpGeos() != null && composant.toRepartBatImpGeos().size() > 0) {
			IRepartBatImpGeo repart = composant.toRepartBatImpGeos().get(0);

			ILocal local = repart.toLocal();
			if (local != null) {
				return local.toFwkpers_Adresse();
			}
		}
		return null;
	}

	private Element generatePublicCible(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_TARGET_GROUP);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_PUBLIC_CIBLE);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	private Element generateFormOfTeaching(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_FORM_OF_TEACHING);
		// TODO definir cette valeur
		String texte = null;
		if (texte != null) {
			getXmlProducer().ajouteStringAttribute(element, ConstantesXML.TAG_ATTR_TEACHING_METHOD, texte);
		}
		return element;
	}

	private Element generateBenefits(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_BENEFITS);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(composant), IDescriptifChampTitre.COMP_AIDES);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	private Element generateUniversalAdjustement(IComposant maquetteElement) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_UNIVERSAL_ADJUSTMENT);
		String texte = getDescriptifInfoGenerator().getContenuChamp(getComposantDescriptif(maquetteElement), IDescriptifChampTitre.COMP_BESOINS_PART);
		if (texte != null) {
			element.appendChild(getXmlProducer().creeInfoBlock(texte, true));
		}
		return element;
	}

	/**
	 * @param composant element de la maquette pour laquelle on recherche les contacts responsables
	 * @return element décrivant les contacts
	 */
	private Element generateContacts(IComposant composant) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_CONTACTS);

		for (IResponsableComposant responsable : composant.responsablesComposant()) {
			if (responsable.responsable() instanceof IIndividu) {
				IIndividu individu = (IIndividu) responsable.responsable();
				if (individu != null) {
					String role = null;
					if (responsable.roles() != null && responsable.roles().size() > 0) {
						IResponsableComposantRole responsableComposantRole = responsable.roles().get(0);
						role = responsableComposantRole.role().libelleLong();
					}
					Element pers = generateContact(getStructure(), individu, role);
					element.appendChild(pers);
					ajouteIndividuAuxContacts(individu);
				}
			}
		}

		return element;
	}

	private void generateECInstructionLanguage(Element eltCurr, IComposant composant) {
		String libelleLang = getLanguage(composant);
		if (libelleLang != null) {
			Element e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_INSTRUCTION_LANGUAGE);
			getXmlProducer().ajouteStringAttribute(e, ConstantesXML.TAG_COURSE_ATTR_TEACHING_LANGUAGE, libelleLang);
			eltCurr.appendChild(e);
		}
	}

	private String getLanguage(IComposant composant) {
		String libelleLang = null;
		if (composant.langues() != null && composant.langues().size() > 0) {
			libelleLang = composant.langues().get(0).llLangue();
		}

		return libelleLang;
	}

	private void generateECFormOfAssessment(Element eltCurr, IComposant composant) {

		List<IAE> aes = new ArrayList<IAE>();
		for (IComposant child : composant.childs()) {
			if (child instanceof IAE) {
				aes.add((IAE) child);
			}
		}
		if (aes.size() > 0) {
			Element e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_FORM_OF_ASSESSMENT);
			for (IAE ae : aes) {
				e.appendChild(getXmlProducer().creeInfoBlock(getTypeAEcode(ae) + " : " + ae.libelle(), true));
			}
			eltCurr.appendChild(e);
		}

	}

	private void generateECActivites(Element eltCurr, IComposant composant) {

		if (composant != null) {
			for (IComposant child : composant.childs()) {
				if (child instanceof IAP) {
					Element c = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY);
					Element e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_ID);
					getXmlProducer().ajouteTextNode(e, child.code());
					c.appendChild(e);
					e = (Element) getDoc().createElement(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_NAME);
					Element t = getXmlProducer().ajouteTextElement(getTypeAPcode((IAP) child) + " : " + child.libelle());
					e.appendChild(t);
					c.appendChild(e);
					eltCurr.appendChild(c);
				}
			}
		}
	}

	private String getTypeAPcode(IAP ap) {
		if (ap.typeAP() != null) {
			return ap.typeAP().code();
		}
		return null;
	}

	private String getTypeAEcode(IAE ae) {
		if (ae.typeAE() != null) {
			return ae.typeAE().code();
		}
		return null;
	}
}
