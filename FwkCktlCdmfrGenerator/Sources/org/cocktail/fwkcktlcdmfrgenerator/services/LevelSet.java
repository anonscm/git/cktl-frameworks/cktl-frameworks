package org.cocktail.fwkcktlcdmfrgenerator.services;

/**
 * Définition des niveaux d'études
 */
public enum LevelSet {
	LEVEL_BAC(0, "bac"), 
	LEVEL_BACplus1(1, "bac+1"), 
	LEVEL_BACplus2(2, "bac+2"), 
	LEVEL_BACplus3(3, "bac+3"), 
	LEVEL_BACplus4(4, "bac+4"), 
	LEVEL_BACplus5(5, "bac+5"), 
	LEVEL_BACplus6(6, "bac+6"),
	LEVEL_BACplus7(7, "bac+7"),
	LEVEL_BACplus8(8, "bac+8"),
	LEVEL_Inconnu(9, "Inconnu");

	private Integer level;
	private String code;

	public Integer getLevel() {
		return level;
	}

	public void setLevel(Integer level) {
		this.level = level;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	LevelSet(Integer level, String code) {
		setLevel(level);
		setCode(code);
	}

	/**
	 * @param level niveau d'étude
	 * @return le niveau avec code
	 */
	public static LevelSet getLevelSet(Integer level) {
		switch (level) {
		case 0:
			return LEVEL_BAC;
		case 1:
			return LEVEL_BACplus1;
		case 2:
			return LEVEL_BACplus2;
		case 3:
			return LEVEL_BACplus3;
		case 4:
			return LEVEL_BACplus4;
		case 5:
			return LEVEL_BACplus5;
		case 6:
			return LEVEL_BACplus6;
		case 7:
			return LEVEL_BACplus7;
		case 8:
			return LEVEL_BACplus8;
		default:
			return LEVEL_Inconnu;
		}
	}
}
