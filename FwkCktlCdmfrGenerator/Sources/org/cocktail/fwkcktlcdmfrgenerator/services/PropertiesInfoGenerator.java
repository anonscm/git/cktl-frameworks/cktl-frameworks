package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.text.SimpleDateFormat;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.webobjects.foundation.NSTimestamp;

/**
 * Extrait les informations liées aux propriétés génrales du document
 */
public class PropertiesInfoGenerator extends CommonInfoGenerator {

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public PropertiesInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
	}

	/**
	 * Génère la balise "properties"
	 * @param element element auquel on rattache les infos de type diplome
	 */
	public void generateProperties(Element element) {
		Element properties = (Element) getDoc().createElement(ConstantesXML.TAG_PROPERTIES);

		// min : 0
		Element dataSource = getDoc().createElement(ConstantesXML.TAG_PROPERTIES_DATA_SOURCE);
		dataSource.setTextContent(ConstantesXML.DATASOURCE_NAME);
		properties.appendChild(dataSource);

		// min : 0
		Element timeProduced = getDoc().createElement(ConstantesXML.TAG_PROPERTIES_DATE_TIME);
		timeProduced.setAttribute(ConstantesXML.TAG_PROPERTIES_ATTR_DATE, new SimpleDateFormat("yyyy-MM-dd").format(new NSTimestamp()));
		timeProduced.setAttribute("time", new SimpleDateFormat("H:m:s").format(new NSTimestamp()));
		properties.appendChild(timeProduced);

		element.appendChild(properties);
	}

}
