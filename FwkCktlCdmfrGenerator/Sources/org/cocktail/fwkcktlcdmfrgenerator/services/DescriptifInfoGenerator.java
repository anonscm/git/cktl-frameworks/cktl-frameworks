package org.cocktail.fwkcktlcdmfrgenerator.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;

/**
 * extrait les infos à partir d'un descriptif
 */
public class DescriptifInfoGenerator {

	/**
	 * @param descriptif descriptif dont on extrait le libelle
	 * @return texte qui contient la description de la formation
	 */
	public String getLibelle(IDescriptif descriptif) {
		if (descriptif != null) {
			return descriptif.libelle();
		}

		return null;
	}

	/**
	 * @param descriptif descriptif dont on extrait la valeur du champ
	 * @param codeTitreChamp code du titre du champ
	 * @return le contenue du champ
	 */
	public String getContenuChamp(IDescriptif descriptif, String codeTitreChamp) {
		if (descriptif != null && codeTitreChamp != null) {
			return descriptif.getContenuChamp(codeTitreChamp);
		}
		return null;
	}
}
