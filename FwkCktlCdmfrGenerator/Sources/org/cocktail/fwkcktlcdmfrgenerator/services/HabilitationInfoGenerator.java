package org.cocktail.fwkcktlcdmfrgenerator.services;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDomaine;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Génère les informations relatives à l'habilitation du diplome
 */
public class HabilitationInfoGenerator extends CommonInfoGenerator {

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public HabilitationInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
	}

	/**
	 * Génère la balise "habilitation" décrivant le diplôme <code>diplome</code>
	 * @param element element auquel on rattache les infos d'habiliation de type diplome
	 * @param diplome le diplome pour lequel on fait l'extraction
	 */
	public void generateHabilitationDiplome(Element element, IDiplome diplome) {
		Element habilitation = initialiseElementHabilitation(diplome);
		habilitation.appendChild(generateCohabilitation(diplome));
		habilitation.appendChild(generateOtherDiploma(diplome));
		habilitation.appendChild(generatePartnership());
		habilitation.appendChild(generateField(diplome));
		element.appendChild(habilitation);

	}

	private Element initialiseElementHabilitation(IDiplome diplome) {
		Element habilitation = (Element) getDoc().createElement(ConstantesXML.TAG_HABILITATION);
		Element habiliId = getDoc().createElement(ConstantesXML.TAG_HABILITATION_ID);
		String identifiant = generateIdentifiant(getStructure(), ConstantesXML.FAMILLE_PROGRAM, diplome.code());
		getXmlProducer().ajouteTextNode(habiliId, identifiant);
		habilitation.appendChild(habiliId);
		return habilitation;
	}

	private Element generateCohabilitation(IDiplome diplome) {
		Element cohabitation = getDoc().createElement(ConstantesXML.TAG_HABILITATION_COHABILITATION);

		Element exists = getDoc().createElement(ConstantesXML.TAG_HABILITATION_EXISTS);
		exists.setTextContent("true"); // TODO: fonction true ou false selon cohabitation ou non
		cohabitation.appendChild(exists);

		Element listOfOrgUnit = getDoc().createElement(ConstantesXML.TAG_HABILITATION_LIST_OF_ORG_UNIT); // TODO: creer fonction pr la boucle
		Element habOrgUnit = getDoc().createElement(ConstantesXML.TAG_HABILITATION_HAB_ORG_UNIT);

		Element refOrgUnit = getDoc().createElement(ConstantesXML.TAG_HABILITATION_REF_ORG_UNIT);
		IStructure structure = getStructure();
		if (structure != null) {
			getXmlProducer().ajouteTextNode(refOrgUnit, structure.cRne());
		}
		habOrgUnit.appendChild(refOrgUnit);

		for (IDomaine domaine : diplome.domaines()) {
			Element domainName = getDoc().createElement(ConstantesXML.TAG_HABILITATION_DOMAIN_NAME);
			Element fixedDomain = getDoc().createElement(ConstantesXML.TAG_HABILITATION_FIXED_DOMAIN);
			getXmlProducer().ajouteTextNode(fixedDomain, domaine.getLibelle());
			domainName.appendChild(fixedDomain);
			habOrgUnit.appendChild(domainName);
		}

		listOfOrgUnit.appendChild(habOrgUnit);
		cohabitation.appendChild(listOfOrgUnit);
		return cohabitation;
	}

	private Element generateOtherDiploma(IDiplome diplome) {
		Element otherDiploma = getDoc().createElement(ConstantesXML.TAG_HABILITATION_OTHER_DIPLOMA);

		Element refProgram = getDoc().createElement("cdmfr:refProgram");
		IStructure structure = getStructure();
		if (structure != null) {
			refProgram.setTextContent(generateIdentifiant(structure, ConstantesXML.FAMILLE_HABILITATION, "identifiantAModifier")); // TODO: modifier identifiant
		}
		otherDiploma.appendChild(refProgram);
		return otherDiploma;
	}

	private Element generatePartnership() {
		Element partnership = getDoc().createElement(ConstantesXML.TAG_HABILITATION_PARTNER_SHIP);
		Element training = getDoc().createElement(ConstantesXML.TAG_HABILITATION_TRAINING);
		training.setTextContent("training"); // TODO: training ?
		partnership.appendChild(training);
		return partnership;
	}

	private Element generateField(IDiplome diplome) {
		Element field = getDoc().createElement(ConstantesXML.TAG_HABILITATION_FIELD);
		Element fieldName = getDoc().createElement(ConstantesXML.TAG_HABILITATION_FIELD_NAME);
		Element fieldFree = getDoc().createElement(ConstantesXML.TAG_HABILITATION_FIELD_FREE);
		if (diplome.mention() != null) {
			getXmlProducer().ajouteTextNode(fieldFree, diplome.mention().libelle());
		}
		fieldName.appendChild(fieldFree);
		field.appendChild(fieldName);
		field.appendChild(generateSpeciality(diplome));
		return field;
	}

	private Element generateSpeciality(IDiplome diplome) {
		Element speciality = getDoc().createElement(ConstantesXML.TAG_HABILITATION_SPECIALITY);

		Element specialityId = getDoc().createElement(ConstantesXML.TAG_HABILITATION_SPECIALITY_ID);
		speciality.appendChild(specialityId);

		Element specialityName = getDoc().createElement(ConstantesXML.TAG_HABILITATION_SPECIALITY_NAME);
		if (diplome.specialite() != null) {
			getXmlProducer().ajouteTextNode(specialityName, diplome.specialite().libelle());
		}
		speciality.appendChild(specialityName);

		Element refProgram = getDoc().createElement(ConstantesXML.TAG_HABILITATION_REF_PROGRAM);
		IStructure structure = getStructure();
		if (structure != null) {
			getXmlProducer().ajouteTextNode(refProgram, generateIdentifiant(structure, ConstantesXML.FAMILLE_PROGRAM, diplome.code()));
		}
		speciality.appendChild(refProgram);

		return speciality;
	}
}
