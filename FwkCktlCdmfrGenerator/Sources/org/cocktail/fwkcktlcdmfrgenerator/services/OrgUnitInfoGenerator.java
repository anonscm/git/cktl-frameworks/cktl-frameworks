package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ISecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

/**
 * Extrait les informations liées à une organisation
 */
public class OrgUnitInfoGenerator extends CommonInfoGenerator {

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public OrgUnitInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		super(doc, structure, isHabilitation);
	}

	/**
	 * Extraction des éléments se rattachant aux strucures (orgUnit)
	 * @param  eltCurr element auquel on rattache les infos de type structure
	 * @param structures les structures que l'on traite
	 */
	public void generateOrgUnits(Element eltCurr, List<IStructure> structures) {
	  for (IStructure structure : structures) {
	    generateOrgUnit(eltCurr, structure);
    }	  
  }
	
	/**
	 * Extraction des éléments d'une strucure (orgUnit)
	 * @param eltCurr element auquel on rattache les infos de type
	 * @param structure orgUnit à générer
	 */
	public void generateOrgUnit(Element eltCurr, IStructure structure) {
		if (structure != null) {
			Element p = initialiseElementStructure(structure);
			p.appendChild(generateAcronym(structure));
			p.appendChild(generateOrgUnitKind(structure));
			p.appendChild(generateWebLink(structure));
			p.appendChild(generateAdmissionInfo(structure));
			p.appendChild(generateContacts(structure));
			p.appendChild(generateSearchWord(structure));
			eltCurr.appendChild(p);
		}
	}
	


	private Element initialiseElementStructure(IStructure structure) {
		String identifiant = generateIdentifiant(structure, ConstantesXML.FAMILLE_ORG_UNIT, structure.cRne());
		Element p = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT);
		getXmlProducer().ajouteStringAttribute(p, ConstantesXML.TAG_ATTR_ID, identifiant);

		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT_ID);
		getXmlProducer().ajouteTextNode(e, identifiant);
		p.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT_NAME);
		String name = structure.llStructure();
		Element t = getXmlProducer().ajouteTextElement(name);
		e.appendChild(t);
		p.appendChild(e);

		e = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT_CODE);
		getXmlProducer().ajouteStringAttribute(e, ConstantesXML.TAG_ATTR_CODE_SET, getOrgUnitCodeValueSet(structure));
		getXmlProducer().ajouteTextNode(e, getOrgUnitCodeValue(structure));
		p.appendChild(e);

		return p;
	}

	private String getOrgUnitCodeValueSet(IStructure structure) {
		if (structure.cRne() != null) {
			return ConstantesXML.CODE_SET_UAI; // le code UAI a remplace le code RNE depuis 1996. Il continue à être abusivement noté RNE dans le référentiel
		} else {
			return ConstantesXML.CODE_SET_ETABLISSEMENT;
		}
	}

	private String getOrgUnitCodeValue(IStructure structure) {
		if (structure.cRne() != null) {
			return structure.cRne();
		} else {
			return structure.lcStructure();
		}
	}

	private Element generateAcronym(IStructure structure) {
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT_ACRONYM);
		if (structure.lcStructure() != null) {
			getXmlProducer().ajouteTextNode(e, structure.lcStructure());
		} else if (structure.cRne() != null) {
			getXmlProducer().ajouteTextNode(e, structure.cRne());
		}
		return e;
	}

	private Element generateOrgUnitKind(IStructure structure) {
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_ORG_UNIT_KIND);
		getXmlProducer().ajouteStringAttribute(e, ConstantesXML.TAG_ATTR_UNIT_KIND, getTypeOrganisation(structure));
		if (structure.toTypeStructure() != null) {
			getXmlProducer().ajouteTextNode(e, structure.toTypeStructure().lTypeStructure());
		}
		return e;
	}

	private String getTypeOrganisation(IStructure structure) {
		if (structure.toTypeStructure() != null && EOTypeStructure.TYPE_STRUCTURE_E.equals(structure.toTypeStructure().cTypeStructure())) {
			return ConstantesXML.ATT_ORG_TYPE_UNIV;
		}
		return null;
	}

	private Element generateWebLink(IStructure structure) {
		String url = getURLForPersId(structure);
		return generateWeblink(url, null, true);
	}

	private String getURLForPersId(IStructure structure) {
		for (IRepartPersonneAdresse repartAdresse : structure.toRepartPersonneAdresses()) {
			if (repartAdresse.toAdresse() != null) {
				String hrefCurr = repartAdresse.toAdresse().adrUrlPere();
				if (hrefCurr != null) {
					return hrefCurr;
				}
			}
		}
		return null;
	}

	private Element generateAdmissionInfo(IStructure structure) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_ADMISSION_INFO);

		if (structure.grpEffectifs() != null) {
			Integer nbCapaciteMax = structure.grpEffectifs();
			if (nbCapaciteMax != null && nbCapaciteMax > 0) {
				Element child = (Element) getDoc().createElement(ConstantesXML.TAG_STUDENT_PLACES);
				getXmlProducer().ajouteTextNode(child, nbCapaciteMax.toString());
				element.appendChild(child);
			}
		}
		return element;
	}

	private Element generateContacts(IStructure structure) {
		Element element = (Element) getDoc().createElement(ConstantesXML.TAG_CONTACTS);
		IIndividu individu = structure.toResponsable();
		if (individu != null) {
			element.appendChild(generateContact(structure, individu, ConstantesXML.ROLE_CONTACT_RESPONSABLE));
			ajouteIndividuAuxContacts(individu);
		}

		for (ISecretariat secretaire : structure.toSecretariats()) {
			if (secretaire.toIndividu() != null) {
				element.appendChild(generateContact(structure, secretaire.toIndividu(), ConstantesXML.ROLE_CONTACT_SECRETAIRE));
				ajouteIndividuAuxContacts(secretaire.toIndividu());
			}
		}
		return element;
	}

	private Node generateSearchWord(IStructure structure) {
		Element e = (Element) getDoc().createElement(ConstantesXML.TAG_SEARCH_WORD);
		getXmlProducer().ajouteTextNode(e, structure.grpMotsClefs());
		return e;
	}

	
}
