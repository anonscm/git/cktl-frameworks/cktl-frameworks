package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.math.BigDecimal;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

/**
 * Produit des éléments XML conforme à ce qui est attendu par CDM Fr
 */
public class XMLProducer {
	private Document doc;
	private String lineSep = System.getProperty("line.separator");

	/**
	 * @param doc document xml dans lequel les éléments sont produits
	 */
	public XMLProducer(Document doc) {
		this.doc = doc;
	}

	/**
	 * @param element element auquel on ajoute le texte
	 * @param texte texte ajouté à un élement
	 */
	public void ajouteTextNode(Element element, String texte) {
		if (texte != null) {
			String[] lines = texte.split(lineSep);
			for (int i = 0; i < lines.length; i++) {
				element.appendChild((Text) doc.createTextNode(lines[i]));
				if (i != lines.length - 1) {
					element.appendChild((Text) doc.createTextNode(lineSep));
					element.appendChild((Element) doc.createElement(ConstantesXML.TAG_LIGNE_SEPARATEUR));
					element.appendChild((Text) doc.createTextNode(lineSep));
				}
			}
		}
	}

	/**
	 * <infoBlock>
	 * <p>
	 * texte
	 * </p>
	 * </infoBlock>
	 * @param texte mis en forme dans un paragraphe infoblock
	 * @return un element XML de type infoblock contenant le texte
	 */
	public Element creeInfoBlockAvecParagraphe(String texte) {
		Element infoBlock = (Element) doc.createElement(ConstantesXML.TAG_INFO_BLOCK);
		Element paragraph = (Element) doc.createElement(ConstantesXML.TAG_PARAGRAPHE);

		ajouteTextNode(paragraph, texte);
		infoBlock.appendChild(paragraph);
		return infoBlock;
	}

	/**
	 * Ajoute un attribut de type Integer
	 * @param element element auquel on ajoute l'attribut
	 * @param tagName nom du tag XML
	 * @param value valeur de l'attribut
	 */
	public void ajouteIntegerAttribute(Element element, String tagName, Integer value) {
		if (value != null) {
			element.setAttribute(tagName, value.toString());
		}
	}

	/**
	 * Ajoute un attribut de type String
	 * @param element element auquel on ajoute l'attribut
	 * @param tagName nom du tag XML
	 * @param value valeur de l'attribut
	 */
	public void ajouteStringAttribute(Element element, String tagName, String value) {
		if (value != null) {
			element.setAttribute(tagName, value);
		}
	}

	/**
	 * Ajoute un attribut de type BigDecimal
	 * @param element element auquel on ajoute l'attribut
	 * @param tagName nom du tag XML
	 * @param value valeur de l'attribut
	 */
	public void ajouteBigDecimalAttribute(Element element, String tagName, BigDecimal value) {
		if (value != null) {
			element.setAttribute(tagName, value.toString());
		}
	}

	/**
	 * <infoBlock> texte </infoBlock>
	 * @param texte mis en forme dans un paragraphe infoblock
	 * @param overrideTag <code> true </code> si on doit remplacer infoblock par p
	 * @return un element XML de type infoblock contenant le texte
	 */
	protected Element creeInfoBlock(String texte, Boolean overrideTag) {
		// On tient compte ici du fait que selon la version du schema CDMFR, infoblock peut
		// parfois être remplacé par un autre tag (en l'occurence <p>)
		String tagName = ConstantesXML.TAG_INFO_BLOCK;
		if (overrideTag) {
			tagName = ConstantesXML.TAG_INFO_BLOCK_SUBSTITUTE;
		}
		Element infoBlock = (Element) doc.createElement(tagName);
		ajouteTextNode(infoBlock, texte);
		return infoBlock;
	}

	/**
	 * @param texte texte que l'on ajoute
	 * @return ajoute un element avec la balise TEXT_ELEMENT avec le texte texte
	 */
	protected Element ajouteTextElement(String texte) {
		Element element = (Element) doc.createElement(ConstantesXML.TAG_TEXT_ELEMENT);
		ajouteTextNode(element, texte);
		return element;
	}
}
