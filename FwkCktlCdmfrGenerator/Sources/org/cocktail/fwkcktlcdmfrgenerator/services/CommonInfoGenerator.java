package org.cocktail.fwkcktlcdmfrgenerator.services;

import java.util.ArrayList;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.descriptif.service.DescriptifProvider;
import org.cocktail.fwkcktlscolpeda.serveur.metier.descriptif.service.DescriptifProviderImpl;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * Extrait les informations transverses : generation d'identifiant, adresse, téléphone
 */
public class CommonInfoGenerator {
	private Document doc;
	private DescriptifInfoGenerator descriptifInfoGenerator;
	private XMLProducer xmlProducer;
	private IStructure structure;
	private List<IIndividu> contacts;
	private IDescriptif composantDescriptif;
	private Boolean isHabilitation;
	private List<IStructure> structures;

	public static final String ERR_IDENTIFIANT_IMPOSSIBLE_A_GENERER = "identifiantImpossibleAGenerer";
	public static final String CODE_ISO_PAYS = "FR";
	public static final String TYPE_CODE_INSTITUTION_MESR = "UAI";
	public static final String TYPE_CODE_INSTITUTION_AUTRE = "SIRET";

	/**
	 * @param doc doc document xml auquel on va ajouter les éléments spécifiques
	 * @param structure structure de rattachement de l'élément traité
	 * @param isHabilitation <code> true </code> si l'on utilise le descriptif d'habilitation, <code> false </code> si l'on utilise le descriptif de publication
	 */
	public CommonInfoGenerator(Document doc, IStructure structure, Boolean isHabilitation) {
		setDoc(doc);
		setStructure(structure);
		setIsHabilitation(isHabilitation);
		xmlProducer = new XMLProducer(doc);
		descriptifInfoGenerator = new DescriptifInfoGenerator();
		contacts = new ArrayList<IIndividu>();
		structures = new ArrayList<IStructure>();
	}

	public List<IIndividu> getContacts() {
		return contacts;
	}

	public List<IStructure> getStructures() {
		return structures;
	}

	protected void setDoc(Document doc) {
		this.doc = doc;
	}

	protected Document getDoc() {
		return doc;
	}

	protected DescriptifInfoGenerator getDescriptifInfoGenerator() {
		return descriptifInfoGenerator;
	}

	protected XMLProducer getXmlProducer() {
		return xmlProducer;
	}

	public void setIsHabilitation(Boolean isHabilitation) {
		this.isHabilitation = isHabilitation;
	}

	public Boolean getIsHabilitation() {
		return isHabilitation;
	}

	/**
	 * @return intialise le fichier de résultats
	 */
	public Element getElementRacine() {
		Element racine = (Element) doc.createElement(ConstantesXML.ELEMENT_RACINE_CDM);
		racine.setAttribute("xmlns", ConstantesXML.ATT_SCHEMA);
		racine.setAttribute("xmlns:xsi", ConstantesXML.ATT_SCHEMA_URI);
		racine.setAttribute("xmlns:cdmfr", ConstantesXML.ATT_CDMFR);
		racine.setAttribute("xmlns:xhtml", ConstantesXML.ATT_XHTML);
		racine.setAttribute("xsi:schemaLocation", ConstantesXML.ATT_SCHEMA_LOCATION);
		racine.setAttribute("profile", ConstantesXML.ATT_PROFILE);
		racine.setAttribute("language", ConstantesXML.ATT_LANGUE_PAR_DEFAUT);
		return racine;
	}

	/**
	 * Retourne un identifiant valide au sens cdm-fr du terme, construit à partir du code rne ou du code siret
	 * @see https://cdm-fr.fr/QuickAppBuilder/BlobServlet?id=_QuickAppBuilder_WAR_QuickAppBuilder_INSTANCE_J6en_&row=2&krow=-1&col=4&file=RUR_CDM-fr.pdf
	 * @param structure structure de l'etablissement
	 * @param typeFamille famille de CDMFR désignant l'institution dans le codage choisi : OU, HA, CO, PR, PE ou LH
	 * @param identifiantLocal identifiant local de l'element
	 * @return identifiant à la norme cdm-fr
	 */
	public String generateIdentifiant(IStructure structure, String typeFamille, String identifiantLocal) {
		if (structure == null) {
			return ERR_IDENTIFIANT_IMPOSSIBLE_A_GENERER;
		}

		if (structure.cRne() != null) {
			return CODE_ISO_PAYS + TYPE_CODE_INSTITUTION_MESR + structure.cRne() + typeFamille + identifiantLocal;
		} else {
			return CODE_ISO_PAYS + TYPE_CODE_INSTITUTION_AUTRE + structure.siret() + typeFamille + identifiantLocal;
		}
	}

	/**
	 * @param elemtCurr element auquel au ajoute l'adresse
	 * @param adr : adresse
	 * @param texteComplementaire : texte complémentaire ajouté à l'adresse
	 */
	public void generateAdresse(Element elemtCurr, IAdresse adr, String texteComplementaire) {
		if (adr != null) {
			Element eltAdr = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE);

			Element element = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE_EXTADR);
			getXmlProducer().ajouteTextNode(element, texteComplementaire);
			eltAdr.appendChild(element);

			element = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE_STREET);
			String ad = "";
			if (adr.adrAdresse1() != null) {
				ad = adr.adrAdresse1();
			}
			if (adr.adrAdresse2() != null) {
				ad += " " + adr.adrAdresse2();
			}
			getXmlProducer().ajouteTextNode(element, ad);
			eltAdr.appendChild(element);

			element = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE_LOCALITY);
			getXmlProducer().ajouteTextNode(element, adr.ville());
			eltAdr.appendChild(element);

			element = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE_PCODE);
			getXmlProducer().ajouteTextNode(element, adr.codePostal());
			eltAdr.appendChild(element);

			element = (Element) doc.createElement(ConstantesXML.TAG_ADRESSE_COUNTRY);
			if (adr.toPays() != null) {
				getXmlProducer().ajouteTextNode(element, adr.toPays().llPays());
			}
			eltAdr.appendChild(element);

			elemtCurr.appendChild(eltAdr);
		}
	}

	protected Element generateContact(IStructure structure, IIndividu individu, String role) {
		Element child = (Element) doc.createElement(ConstantesXML.TAG_REF_PERSON);
		String id = generateIdentifiant(structure, ConstantesXML.FAMILLE_PERSON, getIndividuULRIdentifiant(individu));
		getXmlProducer().ajouteStringAttribute(child, ConstantesXML.TAG_ATTR_REF_ID, id);
		getXmlProducer().ajouteStringAttribute(child, ConstantesXML.TAG_ATTR_REF_ROLE, role);
		return child;
	}

	protected Element generateContact(IStructure structure, IStructure composante, String role) {
		Element child = (Element) doc.createElement(ConstantesXML.TAG_REF_ORG_UNIT);
		String id = generateIdentifiant(structure, ConstantesXML.FAMILLE_ORG_UNIT, composante.cRne());
		getXmlProducer().ajouteStringAttribute(child, ConstantesXML.TAG_ATTR_REF_ID, id);
		getXmlProducer().ajouteStringAttribute(child, ConstantesXML.TAG_ATTR_REF_ROLE, role);
		return child;
	}

	protected String getIndividuULRIdentifiant(IIndividu individu) {
		// TODO à modifier plus tard selon les specs
		return individu.persId().toString();
	}

	IDescriptif getComposantDescriptif(IComposant composant) {
		if (composantDescriptif == null) {
			DescriptifProvider descriptifProvider = new DescriptifProviderImpl();
			composantDescriptif = descriptifProvider.getDescriptif(composant, getIsHabilitation());
		}
		return composantDescriptif;
	}

	public void setComposantDescriptif(IDescriptif diplomeDescriptif) {
	  this.composantDescriptif = diplomeDescriptif;
  }
	
	public IStructure getStructure() {
		return structure;
	}

	public void setStructure(IStructure structure) {
		this.structure = structure;
	}

	/**
	 * Fonction permettant de faire un element XML <weblink>
	 * @param elemtCurr element sur lequel on insere le lien
	 * @param url url du lien à générer
	 * @param nomLien nom que l'on souhaite donner au lien url (texte affiche sur la page)
	 * @param estHomepage permet de savoir si le lien doit avoir la propriete homepage ou non
	 * @return true si un lien a bien ete genere, false sinon (pas d'url de specifie)
	 */
	protected Element generateWeblink(String url, String nomLien, boolean estHomepage) {

		Element element = (Element) doc.createElement(ConstantesXML.TAG_WEB_LINK);
		if (estHomepage) {
			getXmlProducer().ajouteStringAttribute(element, ConstantesXML.TAG_ATTR_WEB_LINK_ROLE, ConstantesXML.ATT_ROLE_WEBLINK_HOMEPAGE);
		}

		if (url != null) {
			Element elemtFils = doc.createElement(ConstantesXML.TAG_HREF);
			getXmlProducer().ajouteTextNode(elemtFils, url);
			element.appendChild(elemtFils);
		}

		if (nomLien != null) {
			Element elemtFils = doc.createElement(ConstantesXML.TAG_LINK_NAME);
			getXmlProducer().ajouteTextNode(elemtFils, nomLien);
			element.appendChild(elemtFils);
		}

		return element;
	}

	protected void ajouteIndividuAuxContacts(IIndividu individu) {
		if (!getContacts().contains(individu)) {
			getContacts().add(individu);
		}
	}
	
	protected void ajouteStructureAuxStructures(IStructure	structure) {
		if (!getStructures().contains(structure)) {
			getStructures().add(structure);
		}
	}

	protected void ajouteLesStructures(IComposant composant) {
		for (IStructure composantStructure : composant.structures()) {
			ajouteStructureAuxStructures(composantStructure);
		}
	}
}
