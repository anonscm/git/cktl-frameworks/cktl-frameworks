package org.cocktail.fwkcktlcdmfrgenerator.services;

import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;

/**
 * type de diplome attendus dans le schéma CDMFR.xsd
 */
public enum TypeDiplomeSet {
	TYPE_DIPLOME_UNKNOWN(0, "inconnu"), 
	TYPE_DIPLOME_LICENCE(1, "licence"), 
	TYPE_DIPLOME_LICENCE_PRO(2, "licencePro"), 
	TYPE_DIPLOME_MASTER(3, "master"), 
	TYPE_DIPLOME_PHD(4, "phD"), 
	TYPE_DIPLOME_PREPA(5, "préparation aux concours"), 
	TYPE_DIPLOME_DE(6, "diplôme d'état");

	private Integer grade;
	private String libelle;

	public Integer getGrade() {
		return grade;
	}

	public void setGrade(Integer grade) {
		this.grade = grade;
	}

	public String getLibelle() {
		return libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	private TypeDiplomeSet(Integer grade, String libelle) {
		setGrade(grade);
		setLibelle(libelle);
	}

	/**
	 * @param typeFormation  type de formation côté scolpeda
	 * @return  un type de diplome au format CDM-fr
	 */
	public static TypeDiplomeSet getTypeDiplome(ITypeFormation typeFormation) {
		if (typeFormation == null) {
			return TYPE_DIPLOME_UNKNOWN;
		}
		
		if (ITypeFormation.TF_LICENCE.equals(typeFormation.code())) {
			return TYPE_DIPLOME_LICENCE;
		}

		if (ITypeFormation.TF_LICENCE_PRO.equals(typeFormation.code())) {
			return TYPE_DIPLOME_LICENCE_PRO;
		}

		if (ITypeFormation.TF_MASTER.equals(typeFormation.code())) {
			return TYPE_DIPLOME_MASTER;
		}

		if (ITypeFormation.TF_DOCTORAT.equals(typeFormation.code())) {
			return TYPE_DIPLOME_PHD;
		}

		if (ITypeFormation.TF_PREAGR.equals(typeFormation.code()) || ITypeFormation.TF_PRECDIV.equals(typeFormation.code())
		    || ITypeFormation.TF_PRECENM.equals(typeFormation.code()) || ITypeFormation.TF_PRECENS.equals(typeFormation.code())) {
			return TYPE_DIPLOME_PREPA;
		}
		
		return TYPE_DIPLOME_UNKNOWN;
	}
}
