package org.cocktail.fwkcktlcdmfrgenerator.services;

/**
 * Définition des tags XML qui sont liés à la génération des blocs de type "program"
 */
public final class ConstantesXML {

	public static final String ELEMENT_RACINE_CDM = "CDM";
	public static final String ATT_SCHEMA_URI = "http://www.w3.org/2001/XMLSchema-instance";
	public static final String ATT_CDMFR = "http://cdm-fr.fr/2012/CDM-frSchema";
	public static final String ATT_XHTML = "http://www.w3.org/1999/xhtml";
	public static final String ATT_SCHEMA = "http://cdm-fr.fr/2012/CDM";
	public static final String ATT_SCHEMA_LOCATION = "http://cdm-fr.fr/2012/CDM http://cdm-fr.fr/2012/schemas/CDMFR.xsd";
	public static final String ATT_LANGUE_PAR_DEFAUT = "fr";
	public static final String ATT_PROFILE = "CDM-fr";
	public static final String ATT_ROLE_WEBLINK_HOMEPAGE = "homepage";

	public static final String ROLE_CONTACT_RESPONSABLE = "Responsable";
	public static final String ROLE_CONTACT_SECRETAIRE = "Secretaire";
	public static final String ROLE_CONTACT_ENSEIGNANT = "Enseignant";
	public static final String ROLE_CONTACT_CONTACT = "contact";

	public static final String FAMILLE_PROGRAM = "PR";
	public static final String FAMILLE_PERSON = "PE";
	public static final String FAMILLE_HABILITATION = "HA";
	public static final String FAMILLE_SUBPROGRAM = "SU";
	public static final String FAMILLE_COURSE = "CO";
	public static final String FAMILLE_ORG_UNIT = "OU";

	public static final String TAG_ADRESSE = "adr";
	public static final String TAG_ADRESSE_EXTADR = "extadr";
	public static final String TAG_ADRESSE_STREET = "street";
	public static final String TAG_ADRESSE_LOCALITY = "locality";
	public static final String TAG_ADRESSE_PCODE = "pcode";
	public static final String TAG_ADRESSE_COUNTRY = "country";
	public static final String TAG_TELEPHONE = "telephone";
	public static final String TAG_FAX = "fax";
	public static final String TAG_ATTR_ID = "id";
	public static final String TAG_INFO_BLOCK = "infoBlock";
	public static final String TAG_INFO_BLOCK_SUBSTITUTE = "p";
	public static final String TAG_INFO_BLOCK_CDMFR = "cdmfr:infoBlock";
	public static final String TAG_TEXT_ELEMENT = "text";

	public static final String TAG_CREDITS = "cdmfr:credits";
	public static final String TAG_ATTR_ECTS_CREDITS = "ECTScredits";
	public static final String TAG_ATTR_HOURS_PER_WEEKS = "hoursPerWeek";
	public static final String TAG_ATTR_TOTAL_WORK_LOAD = "totalWorkLoad";
	public static final String TAG_ADMISSION_INFO = "admissionInfo";
	public static final String TAG_ADMISSION_DESCRIPTION = "cdmfr:admissionDescription";
	public static final String TAG_FORMAL_PRE_REQUISITES = "formalPrerequisites";
	public static final String TAG_RECOMMENDED_PRE_REQUISITES = "recommendedPrerequisites";
	public static final String TAG_TEACHING_PLACE = "cdmfr:teachingPlace";
	public static final String TAG_TARGET_GROUP = "targetGroup";
	protected static final String TAG_FORM_OF_TEACHING = "formOfTeaching";
	public static final String TAG_LEARNING_OBJECTIVES = "learningObjectives";
	public static final String TAG_STUDENT_PLACES = "studentPlaces";
	public static final String TAG_CONTACTS = "contacts";
	public static final String TAG_REF_PERSON = "refPerson";
	public static final String TAG_REF_ORG_UNIT = "refOrgUnit";
	public static final String TAG_ATTR_REF_ID = "idRef";
	public static final String TAG_ATTR_REF_ROLE = "role";
	public static final String TAG_ATTR_CODE_SET = "codeSet";
	public static final String TAG_WEB_LINK = "cdmfr:webLink";
	public static final String TAG_ATTR_WEB_LINK_ROLE = "role";
	public static final String TAG_HREF = "href";
	public static final String TAG_LINK_NAME = "linkName";
	public static final String TAG_SEARCH_WORD = "searchword";
	public static final String TAG_ATTR_USER_DEFINED = "userDefined";
	public static final String TAG_PARAGRAPHE = "p";
	public static final String TAG_ATTR_TEACHING_METHOD = "method";
	public static final String TAG_REF_COURSE = "cdmfr:refCourse";

	public static final String TAG_HABILITATION = "cdmfr:habilitation";
	public static final String TAG_HABILITATION_ID = "cdmfr:habiliId";
	public static final String TAG_HABILITATION_COHABILITATION = "cdmfr:cohabilitation";
	public static final String TAG_HABILITATION_EXISTS = "cdmfr:exists";
	public static final String TAG_HABILITATION_LIST_OF_ORG_UNIT = "cdmfr:listOfOrgUnit";
	public static final String TAG_HABILITATION_HAB_ORG_UNIT = "cdmfr:habOrgUnit";
	public static final String TAG_HABILITATION_REF_ORG_UNIT = "cdmfr:refOrgUnit";
	public static final String TAG_HABILITATION_OTHER_DIPLOMA = "cdmfr:otherDiploma";
	public static final String TAG_HABILITATION_PARTNER_SHIP = "cdmfr:partnership";
	public static final String TAG_HABILITATION_TRAINING = "cdmfr:training";
	public static final String TAG_HABILITATION_SPECIALITY = "cdmfr:speciality";
	public static final String TAG_HABILITATION_SPECIALITY_NAME = "cdmfr:specialityName";
	public static final String TAG_HABILITATION_SPECIALITY_ID = "cdmfr:specialityId";
	public static final String TAG_HABILITATION_REF_PROGRAM = "cdmfr:refProgram";
	public static final String TAG_HABILITATION_FIELD = "cdmfr:field";
	public static final String TAG_HABILITATION_FIELD_NAME = "cdmfr:fieldName";
	public static final String TAG_HABILITATION_FIELD_FREE = "cdmfr:free";
	public static final String TAG_HABILITATION_DOMAIN_NAME = "cdmfr:domainName";
	public static final String TAG_HABILITATION_FIXED_DOMAIN = "cdmfr:fixedDomain";

	public static final String TAG_PROPERTIES = "properties";
	public static final String TAG_PROPERTIES_DATA_SOURCE = "datasource";
	public static final String TAG_PROPERTIES_DATE_TIME = "datetime";
	public static final String TAG_PROPERTIES_ATTR_DATE = "date";

	public static final String TAG_PROGRAM = "program";
	public static final String TAG_PROGRAM_ID = "programID";
	public static final String TAG_PROGRAM_NAME = "cdmfr:programName";
	public static final String TAG_PROGRAM_CODE = "cdmfr:programCode";
	public static final String TAG_PROGRAM_DESCRIPTION = "cdmfr:programDescription";
	public static final String TAG_PROGRAM_DESCRIPTION_ATTR_NATURE = "nature";
	public static final String TAG_PROGRAM_QUALIFICATION = "qualification";
	public static final String TAG_PROGRAM_QUALIFICATION_NAME = "qualificationName";
	public static final String TAG_PROGRAM_QUALIFICATION_DESCRIPTION = "cdmfr:qualificationDescription";
	public static final String TAG_PROGRAM_DEGREE = "cdmfr:degree";
	public static final String TAG_PROGRAM_PROFESSION = "cdmfr:profession";
	public static final String TAG_PROGRAM_STUDY_QUALIFICATION = "cdmfr:studyQualification";
	public static final String TAG_PROGRAM_TEACHING_LANGUAGE = "cdmfr:ppalTeachingLanguage";
	public static final String TAG_PROGRAM_LEVEL = "level";
	public static final String TAG_PROGRAM_ATTR_LEVEL = "level";
	public static final String TAG_PROGRAM_LEVEL_CODE = "levelCode";
	public static final String TAG_PROGRAM_ATTR_CODE_SET = "codeSet";
	public static final String TAG_PROGRAM_ATTR_YIELDING_COMPETENCE = "competenceYielding";
	public static final String TAG_PROGRAM_ATTR_YIELDING_COMPETENCE_CODE = "competenceYieldingCode";
	public static final String TAG_PROGRAM_ATTR_DEGREE = "degree";
	public static final String TAG_PROGRAM_ECTS_REQUIRED = "cdmfr:ectsRequired";
	public static final String TAG_PROGRAM_STUDENT_STATUS = "cdmfr:studentStatus";
	public static final String TAG_PROGRAM_STRUCTURE = "cdmfr:programStructure";
	public static final String TAG_PROGRAM_REGULATIONS = "regulations";
	public static final String TAG_PROGRAM_SUB_PROGRAM = "subProgram";
	public static final String TAG_CDMFR_TEXT = "cdmfr:text";
	public static final String TAG_ATTR_PROGRAM_TEACHING_LANG = "teachingLang";
	public static final String TAG_PROGRAM_DURATION = "programDuration";

	public static final String CONST_NATURE_PROGRAM_DIPLOME = "Diplôme";

	public static final String CODE_SET_UE = "UE";
	public static final String CODE_SET_EC = "EC";
	
	public static final String TAG_COURSE = "course";
	public static final String TAG_COURSE_ID = "courseID";
	public static final String TAG_COURSE_NAME = "courseName";
	public static final String TAG_COURSE_CODE = "courseCode";
	public static final String TAG_COURSE_BENEFITS = "benefits";
	public static final String TAG_COURSE_SYLLABUS = "syllabus";
	public static final String TAG_COURSE_UNIVERSAL_ADJUSTMENT = "universalAdjustment";
	public static final String TAG_COURSE_INSTRUCTION_LANGUAGE = "instructionLanguage";
	public static final String TAG_COURSE_ATTR_TEACHING_LANGUAGE = "teachingLang";
	public static final String TAG_COURSE_FORM_OF_ASSESSMENT = "formOfAssessment";
	public static final String TAG_COURSE_DESCRIPTION = "courseDescription";
	public static final String TAG_COURSE_TEACHING_ACTIVITY = "teachingActivity";
	public static final String TAG_COURSE_TEACHING_ACTIVITY_ID = "teachingActivityID";
	public static final String TAG_COURSE_TEACHING_ACTIVITY_NAME = "teachingActivityName";
	public static final String TAG_COURSE_CONTENTS_EC = "cdmfr:courseContentsEC";

	public static final String ROLE_CONTACT = "Contact";
	public static final String TAG_ROLE = "role";
	public static final String TAG_TITRE = "title";
	public static final String TAG_PERSONID = "personID";
	public static final String TAG_PERSON = "person";
	public static final String TAG_PERSON_NAME = "name";
	public static final String TAG_NAME_GIVEN = "given";
	public static final String TAG_NAME_FAMILY = "family";
	public static final String TAG_AFFILIATION = "affiliation";
	public static final String TAG_CONTACT_DATA = "contactData";
	public static final String TAG_CONTACT_NAME = "contactName";
	public static final String TAG_HEURE_VISITE = "visitHour";
	public static final String TAG_EMAIL = "email";

	public static final String CODE_SET_UAI = "codeUAI";
	public static final String CODE_SET_ETABLISSEMENT = "code établissement";;
	public static final String ATT_ORG_TYPE_UNIV = "universite";

	public static final String TAG_ORG_UNIT = "orgUnit";
	public static final String TAG_ORG_UNIT_ID = "orgUnitID";
	public static final String TAG_ORG_UNIT_NAME = "orgUnitName";
	public static final String TAG_ORG_UNIT_CODE = "orgUnitCode";
	public static final String TAG_ORG_UNIT_ACRONYM = "orgUnitAcronym";
	public static final String TAG_ORG_UNIT_KIND = "orgUnitKind";
	public static final String TAG_ATTR_UNIT_KIND = "orgType";

	public static final String TAG_LIGNE_SEPARATEUR = "br";
	public static final String DATASOURCE_NAME = "Suite Logicielle Cocktail (Sphère SVE v1.0)";

	/**
	 * constructeur privé pour empêcher l'instanciation de cette classe
	 */
	private ConstantesXML() {

	}
}
