package org.cocktail.fwkcktlcdmfrgenerator.serveur;

import org.cocktail.fwkcktlpersonne.common.metier.manager.CktlGrhumParamManager;

import er.extensions.eof.ERXEC;

/**
 *  Gestion des paramètres
 */
public class FwkCktlCdmfrGeneratorParamManager extends CktlGrhumParamManager {
	private static final String DEFAULT_ETABLISSEMENT_RNE = "GRHUM_DEFAULT_RNE";

	/**
	 * constructeur, initialisation des paramètres.
	 */
	public FwkCktlCdmfrGeneratorParamManager() {
		super(ERXEC.newEditingContext());
	}

	public String getDefaultEtablissementRne() {
		return getParam(DEFAULT_ETABLISSEMENT_RNE);
	}
}
