package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class XMLProducerTests {

	@Test
	public void testAjouteTextNodeAvec2lignes() throws Exception {
		// Arrange
		Document doc = getNewDocument();
		Element element = doc.createElement("TAG");
		String lineSep = System.getProperty("line.separator");
		String text = "ligne1" + lineSep + "ligne2";
		XMLProducer xmlProducer = new XMLProducer(doc);
		// Assert ready to test
		// Act
		xmlProducer.ajouteTextNode(element, text);
		// Assert
		// TODO vérifier le contenu html généré
		String expectedText = "ligne1" + lineSep + lineSep + "ligne2";
		assertThat(element.getTextContent()).isEqualTo(expectedText);
	}

	private Document getNewDocument() throws ParserConfigurationException {
		// Creation du constructeur de document
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();

		// Creation du document
		Document document = builder.newDocument();
		return document;
	}
}
