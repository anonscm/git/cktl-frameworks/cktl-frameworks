package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;

import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class PropertiesInfoGeneratorTests extends CDMTestCase {

	@Test
	public void testGenerateProperties() throws Exception {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementProgram");
		PropertiesInfoGenerator propertiesInfoGenerator = new PropertiesInfoGenerator(doc, null, null);
		// Assert ready to test
		// Act

		propertiesInfoGenerator.generateProperties(element);
		Element nodeProperties = (Element) element.getFirstChild();

		// Assert
		checkProperties(nodeProperties);
	}

	private void checkProperties(Element node) {
		assertThat(node.getNodeName()).isEqualTo(ConstantesXML.TAG_PROPERTIES);
		Element child = (Element) node.getElementsByTagName(ConstantesXML.TAG_PROPERTIES_DATA_SOURCE).item(0);
		assertThat(child.getTextContent()).isEqualTo(ConstantesXML.DATASOURCE_NAME);

		child = (Element) node.getElementsByTagName(ConstantesXML.TAG_PROPERTIES_DATE_TIME).item(0);
		String strDate = child.getAttribute(ConstantesXML.TAG_PROPERTIES_ATTR_DATE);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		try {
			dateFormat.parse(strDate);
		} catch (Exception e) {
			assertTrue(e.getMessage(), false);
		}

	}
}
