package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INiveauAccesDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposantRole;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRoleResponsable;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVocation;
import org.junit.Before;
import org.w3c.dom.Element;

import er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater;

public class CDMTestCase {

	protected GenerateurCdmfr generateur;

	@Before public void setUp() {
		generateur = new GenerateurCdmfr("");
	}

	protected IStructure getStructure(String codeRNE, String siret, String llStructure, String lcStructure) {
		IStructure structure = mock(IStructure.class);
		when(structure.cRne()).thenReturn(codeRNE);
		when(structure.siret()).thenReturn(siret);
		when(structure.llStructure()).thenReturn(llStructure);
		when(structure.lcStructure()).thenReturn(lcStructure);
		return structure;
	}

	protected IAdresse getAdresse(String rue, String ville, String codePostal, String url, IPays pays) {
		IAdresse adress = mock(IAdresse.class);
		when(adress.adrAdresse1()).thenReturn(rue);
		when(adress.ville()).thenReturn(ville);
		when(adress.codePostal()).thenReturn(codePostal);
		when(adress.toPays()).thenReturn(pays);
		when(adress.adrUrlPere()).thenReturn(url);
		return adress;
	}

	protected IPays getPays() {
		IPays pays = mock(IPays.class);
		when(pays.llPays()).thenReturn("France");
		return pays;
	}

	protected IIndividu getIndPers() {
		IIndividu individu = mock(IIndividu.class);
		when(individu.persId()).thenReturn(1);
		when(individu.prenom()).thenReturn("Alice");
		when(individu.nomUsuel()).thenReturn("Amstram");
		when(individu.indQualite()).thenReturn("Responsable");
		return individu;
	}
	
	protected IPersonne getPersonneIndividu(Integer persId, String prenom, String nom, String indQualite){
		EOIndividu personne = mock(EOIndividu.class);
		when(personne.persId()).thenReturn(persId);
		when(personne.prenom()).thenReturn(prenom);
		when(personne.nomUsuel()).thenReturn(nom);
		when(personne.indQualite()).thenReturn(indQualite);
		return personne;
	}
	
	protected IPersonne getPersonneStructure(String cRne, String siret, String llStructure, String lcStructure){
		EOStructure personne = mock(EOStructure.class);		
		when(personne.cRne()).thenReturn(cRne);
		when(personne.siret()).thenReturn(siret);
		when(personne.llStructure()).thenReturn(llStructure);
		when(personne.lcStructure()).thenReturn(lcStructure);
		
		return personne;
	}

	
	protected void checkTeachingPlace(Element node) {
		Element nodeTeachingPlace = (Element) node.getElementsByTagName(ConstantesXML.TAG_TEACHING_PLACE).item(0);
		assertThat(nodeTeachingPlace).isNotNull();

		Element nodeAdr = (Element) nodeTeachingPlace.getElementsByTagName(ConstantesXML.TAG_ADRESSE).item(0);
		// Element nodeAdrExtAdr = (Element) nodeAdr.getElementsByTagName(ListeCDM.TAG_ADRESSE_EXTADR).item(0);
		// assertThat(nodeAdrExtAdr.getTextContent()).isEqualTo("Rez-de-chaussée, Porte 212, Bat. Bat A");
		Element nodeAdrStreet = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_STREET).item(0);
		assertThat(nodeAdrStreet.getTextContent()).isEqualTo("Rue de l'université");
		Element nodeAdrPCode = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_PCODE).item(0);
		assertThat(nodeAdrPCode.getTextContent()).isEqualTo("64000");
		Element nodeAdrLocality = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_LOCALITY).item(0);
		assertThat(nodeAdrLocality.getTextContent()).isEqualTo("Pau");
		Element nodeAdrCountry = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_COUNTRY).item(0);
		assertThat(nodeAdrCountry.getTextContent()).isEqualTo("France");

	}

	protected void checkInfoBlock(Element node, String TAG_NAME, String texteValue, Boolean infoBlockOverrided) {
		Element childNode = (Element) node.getElementsByTagName(TAG_NAME).item(0);
		Element nodeInfoBlock = (Element) childNode.getElementsByTagName(getInfoBlockTagName(infoBlockOverrided)).item(0);
		assertThat(nodeInfoBlock.getTextContent()).isEqualTo(texteValue);
	}

	protected String getInfoBlockTagName(Boolean overrided) {
		if (overrided) {
			return ConstantesXML.TAG_INFO_BLOCK_SUBSTITUTE;
		} else {
			return ConstantesXML.TAG_INFO_BLOCK;
		}
	}
	
	protected void checkInfoBlockAvecParagraph(Element node, String TAG_NAME, String texteValue) {
		Element childNode = (Element) node.getElementsByTagName(TAG_NAME).item(0);
		Element nodeInfoBlock = (Element) childNode.getElementsByTagName(ConstantesXML.TAG_INFO_BLOCK).item(0);
		Element nodeParagraph = (Element) nodeInfoBlock.getElementsByTagName(ConstantesXML.TAG_PARAGRAPHE).item(0);
		assertThat(nodeParagraph.getTextContent()).isEqualTo(texteValue);
	}

	protected IDiplome getScolFormationDiplome(String libelle, String code, IGradeUniversitaire grade, IVocation vocation, INiveauAccesDiplome niveauAccesDiplome) {
		IDiplome diplome = mock(IDiplome.class);
		when(diplome.code()).thenReturn(code);
		when(diplome.libelle()).thenReturn(libelle);
		when(diplome.gradeUniversitaire()).thenReturn(grade);
		when(diplome.vocation()).thenReturn(vocation);
		when(diplome.niveauAccesDiplome()).thenReturn(niveauAccesDiplome);
		return diplome;
	}

	protected void checkContacts(Element nodeProgram,String tagName, String refPerson, String role) {
		Element nodeContacts = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_CONTACTS).item(0);
		Element nodeResponsable = (Element) nodeContacts.getElementsByTagName(tagName).item(0);
		assertThat(nodeResponsable.getAttribute(ConstantesXML.TAG_ATTR_REF_ID)).isEqualTo(refPerson);
		assertThat(nodeResponsable.getAttribute(ConstantesXML.TAG_ATTR_REF_ROLE)).isEqualTo(role);
	}

	protected IGradeUniversitaire getGradeUniversitaire(String libelle, String type, Integer nbUniteTemps) {
		IGradeUniversitaire gradeUniversitaire = mock(IGradeUniversitaire.class);
		when(gradeUniversitaire.libelle()).thenReturn(libelle);
		when(gradeUniversitaire.type()).thenReturn(type);
		when(gradeUniversitaire.nbUnitesTemps()).thenReturn(nbUniteTemps);
		return gradeUniversitaire;
	}
	
	protected INiveauAccesDiplome getNiveauAccesDiplome(Integer niveau){
		INiveauAccesDiplome niveauAccesDiplome = mock(INiveauAccesDiplome.class);
		when(niveauAccesDiplome.niveau()).thenReturn(niveau);
		return niveauAccesDiplome;
	}

	protected IDescriptif getDescriptif(String libelle) {
		IDescriptif descriptif = mock(IDescriptif.class);
		when(descriptif.libelle()).thenReturn(libelle);
		return descriptif;
	}

	protected IResponsableComposant getResponsable(IPersonne individu, String libelleRole) {
		IResponsableComposant responsableComposant = mock(IResponsableComposant.class);
		doReturn(individu).when(responsableComposant).responsable();
		if(libelleRole!=null){
			IRoleResponsable role= mock(IRoleResponsable.class);
			when(role.libelleLong()).thenReturn(libelleRole);
			
			IResponsableComposantRole responsableComposantRole = mock(IResponsableComposantRole.class);
			when(responsableComposantRole.role()).thenReturn(role);
			doReturn(Arrays.asList(responsableComposantRole)).when(responsableComposant).roles();
		}
	  return responsableComposant;
	}

	
	protected void checkWebLink(Element element, String url, String nomLien) {
		Element node = (Element) element.getElementsByTagName(ConstantesXML.TAG_WEB_LINK).item(0);
		assertThat(node.getAttribute(ConstantesXML.TAG_ATTR_WEB_LINK_ROLE)).isEqualTo(ConstantesXML.ATT_ROLE_WEBLINK_HOMEPAGE);
		if (url != null) {
			Element nodeURL = (Element) node.getElementsByTagName(ConstantesXML.TAG_HREF).item(0);
			assertThat(nodeURL.getTextContent()).isEqualTo(url);
		}
		if (nomLien != null) {
			Element nodeName = (Element) node.getElementsByTagName(ConstantesXML.TAG_LINK_NAME).item(0);
			assertThat(nodeName.getTextContent()).isEqualTo(nomLien);
		}
	}


}
