package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.EOVlans;
import org.cocktail.fwkcktlpersonne.common.metier.ICompte;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ICompteEmail;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITelephone;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeNoTel;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IVlans;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class PersonInfoGeneratorTests extends CDMTestCase {

	@Test
	public void generatePerson() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementPerson");
		
		IStructure structure = getStructure("codeRNE", null, null, null);
		PersonInfoGenerator personInfoGenerator = new PersonInfoGenerator(doc, structure, null);
		
		IIndividu individu = getIndPers();
		IPays pays = getPays();
		IAdresse adresse = getAdresse("Rue de l'université", "Pau", "64000", null, pays);
		doReturn(Arrays.asList(adresse)).when(individu).toAdresses();
		
		ITelephone telephone1 = getTelephone("0432765412", EOTypeNoTel.TYPE_NO_TEL_TEL);
		ITelephone telephone2 = getTelephone("0432765400",  EOTypeNoTel.TYPE_NO_TEL_FAX);
		doReturn(Arrays.asList(telephone1, telephone2)).when(individu).toTelephones();
		
		ICompte compte = getCompte(EOVlans.VLAN_P, "mail", "univ.fr");
		doReturn(Arrays.asList(compte)).when(individu).toComptes();
		
		// Assert ready to test
		// Act
		personInfoGenerator.generatePerson(element, individu);
		
		// Assert
		Element nodePerson = (Element) element.getFirstChild();
		checkIdentifiant(nodePerson);
		checkNom(nodePerson);
		checkTitre(nodePerson);
		checkRole(nodePerson);		
		checkAffiliation(nodePerson);
		checkContactData(nodePerson);
	}

	private void checkIdentifiant(Element nodePerson) {
		assertThat(nodePerson.getNodeName()).isEqualTo(ConstantesXML.TAG_PERSON);
		assertThat(nodePerson.getAttribute(ConstantesXML.TAG_ATTR_ID)).isEqualTo("FRUAIcodeRNEPE1");
		Element nodePersonId = (Element) nodePerson.getElementsByTagName(ConstantesXML.TAG_PERSONID).item(0);
		assertThat(nodePersonId.getTextContent()).isEqualTo(getIndPers().persId().toString());
	}

	private void checkNom(Element nodePerson) {
		Element nodeName = (Element) nodePerson.getElementsByTagName(ConstantesXML.TAG_PERSON_NAME).item(0);
		Element nodeNameGiven = (Element) nodeName.getElementsByTagName(ConstantesXML.TAG_NAME_GIVEN).item(0);
		assertThat(nodeNameGiven.getTextContent()).isEqualTo("Alice");
		Element nodeNameFamily = (Element) nodeName.getElementsByTagName(ConstantesXML.TAG_NAME_FAMILY).item(0);
		assertThat(nodeNameFamily.getTextContent()).isEqualTo("Amstram");
	}

	private void checkRole(Element nodePerson) {
		Element nodeRole = (Element) nodePerson.getElementsByTagName(ConstantesXML.TAG_ROLE).item(0);
		assertThat(nodeRole.getTextContent()).isEqualTo("Responsable");
	}

	private void checkTitre(Element nodePerson) {
		Element nodeTitre = (Element) nodePerson.getElementsByTagName(ConstantesXML.TAG_TITRE).item(0);
		assertThat(nodeTitre).isNotNull(); // TODO à préciser
	}

	private void checkAffiliation(Element nodePerson) {
		Element nodeAffiliation = (Element) nodePerson.getElementsByTagName(ConstantesXML.TAG_AFFILIATION).item(0);
		assertThat(nodeAffiliation).isNotNull(); // TODO à préciser
	}

	private void checkContactData(Element nodePerson) {
		NodeList listeContacts = nodePerson.getElementsByTagName(ConstantesXML.TAG_CONTACT_DATA);
		assertThat(listeContacts.getLength()).isEqualTo(1);
		Element nodeContact = (Element) listeContacts.item(0);
		Element nodeContactName = (Element) nodeContact.getElementsByTagName(ConstantesXML.TAG_CONTACT_NAME).item(0);
		assertThat(nodeContactName.getTextContent()).isEqualTo("Amstram Alice");

		Element nodeAdr = (Element) nodeContact.getElementsByTagName(ConstantesXML.TAG_ADRESSE).item(0);
//		Element nodeAdrExtAdr = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_EXTADR).item(0);
//		assertThat(nodeAdrExtAdr.getTextContent()).isEqualTo("Rez-de-chaussée, Porte 212, Bat. Bat A");
		Element nodeAdrStreet = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_STREET).item(0);
		assertThat(nodeAdrStreet.getTextContent()).isEqualTo("Rue de l'université");
		Element nodeAdrPCode = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_PCODE).item(0);
		assertThat(nodeAdrPCode.getTextContent()).isEqualTo("64000");
		Element nodeAdrLocality = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_LOCALITY).item(0);
		assertThat(nodeAdrLocality.getTextContent()).isEqualTo("Pau");
		Element nodeAdrCountry = (Element) nodeAdr.getElementsByTagName(ConstantesXML.TAG_ADRESSE_COUNTRY).item(0);
		assertThat(nodeAdrCountry.getTextContent()).isEqualTo("France");

		Element nodeTelephone = (Element) nodeContact.getElementsByTagName(ConstantesXML.TAG_TELEPHONE).item(0);
		assertThat(nodeTelephone.getTextContent()).isEqualTo("+33.(0)432765412");
		Element nodeFax = (Element) nodeContact.getElementsByTagName(ConstantesXML.TAG_FAX).item(0);
		assertThat(nodeFax.getTextContent()).isEqualTo("+33.(0)432765400");

		Element nodeEmail = (Element) nodeContact.getElementsByTagName(ConstantesXML.TAG_EMAIL).item(0);
		assertThat(nodeEmail.getTextContent()).isEqualTo("mail@univ.fr");

	}

	private ITelephone getTelephone(String numero, String codeTypeNoTel){
		ITelephone telephone = mock(ITelephone.class);
		when(telephone.noTelephone()).thenReturn(numero);
		ITypeNoTel typeNoTel = getTypeNoTel(codeTypeNoTel);
		when(telephone.toTypeNoTel()).thenReturn(typeNoTel);
		return telephone;
	}
	
	private ITypeNoTel getTypeNoTel(String code) {
	  ITypeNoTel typeNoTel = mock(ITypeNoTel.class);
	  when(typeNoTel.cTypeNoTel()).thenReturn(code);
	  return typeNoTel;
  }
	
	ICompte getCompte(String vlanCode, String email, String domaine){
		ICompte compte = mock(ICompte.class);
		IVlans vlan= mock(IVlans.class);
		when(vlan.cVlan()).thenReturn(vlanCode);
		when(compte.toVlans()).thenReturn(vlan);
		ICompteEmail compteEmail = mock(ICompteEmail.class);
		when(compteEmail.cemEmail()).thenReturn(email);
		when(compteEmail.cemDomaine()).thenReturn(domaine);
		doReturn(Arrays.asList(compteEmail)).when(compte).toCompteEmails();
		return compte;
	}
}
