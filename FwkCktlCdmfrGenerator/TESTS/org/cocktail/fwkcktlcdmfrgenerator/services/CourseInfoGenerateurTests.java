package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ILangue;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptifChampTitre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeAE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeAP;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkgspot.serveur.metier.eof.IImplantationGeo;
import org.cocktail.fwkgspot.serveur.metier.eof.ILocal;
import org.cocktail.fwkgspot.serveur.metier.eof.IRepartBatImpGeo;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class CourseInfoGenerateurTests extends CDMTestCase {
	@Test
	public void testGenerateCourseUE() throws Exception {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementCourse");

		IStructure structure = getStructure("codeRNE", null, null, null);
		CourseInfoGenerator courseInfoGenerator = spy(new CourseInfoGenerator(doc, structure, true));

		IUE ue = getUE(1, "nom de l'UE", "codeUE");
		IDescriptif descriptif = getDescriptif("texte description");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_OBJECTIFS)).thenReturn("texte learning objectives");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_ADMISSION)).thenReturn("texte admission");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_PREREQUIS)).thenReturn("formal prerequisites");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_RECOMMANDATION)).thenReturn("recommended prerequisites");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_PUBLIC_CIBLE)).thenReturn("texte target group");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_AIDES)).thenReturn("texte benefits");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_BESOINS_PART)).thenReturn("texte besoins particuliers");
		doReturn(descriptif).when(courseInfoGenerator).getComposantDescriptif(ue);

		ILien lienUe = getLienAvecCreditECTS(ue, new BigDecimal(30), 120 * 60);

		IPays pays = getPays();
		IAdresse adresse = getAdresse("Rue de l'université", "Pau", "64000", null, pays);
		ajouteAdresse(ue, adresse);

		IIndividu individu = getIndPers();
		IResponsableComposant responsableComposant = getResponsable(
		    getPersonneIndividu(individu.persId(), individu.prenom(), individu.nomUsuel(), individu.indQualite()), "Responsable");
		doReturn(Arrays.asList(responsableComposant)).when(ue).responsablesComposant();
		// Assert ready to test

		// Act
		courseInfoGenerator.generateCourse(element, lienUe);

		// Assert
		Element nodeCourse = (Element) element.getFirstChild();
		checkIdentifiant(nodeCourse, "FRUAIcodeRNECO1", "nom de l'UE", "UE", "codeUE");
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_DESCRIPTION, "texte description", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_LEARNING_OBJECTIVES, "texte learning objectives", true);
		checkCredit(nodeCourse, "30", "120");
		checkAdmissionInfo(nodeCourse, "texte admission", null);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_FORMAL_PRE_REQUISITES, "formal prerequisites", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_RECOMMENDED_PRE_REQUISITES, "recommended prerequisites", true);
		checkTeachingPlace(nodeCourse);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_TARGET_GROUP, "texte target group", true);
		checkFormOfTeaching(nodeCourse);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_BENEFITS, "texte benefits", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_UNIVERSAL_ADJUSTMENT, "texte besoins particuliers", true);
		checkContacts(nodeCourse, ConstantesXML.TAG_REF_PERSON, "FRUAIcodeRNEPE1", "Responsable");
	}

	@Test
	public void testGenerateCourseEC() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementCourse");

		IStructure structure = getStructure("codeRNE", null, null, null);
		CourseInfoGenerator courseInfoGenerator = spy(new CourseInfoGenerator(doc, structure, true));

		IEC ec = getEC(1, "nom de l'EC", "codeEC");

		IDescriptif descriptif = getDescriptif("texte description");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_OBJECTIFS)).thenReturn("texte learning objectives ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_ADMISSION)).thenReturn("texte admission ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_PREREQUIS)).thenReturn("formal prerequisites ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_RECOMMANDATION)).thenReturn("recommended prerequisites ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_PUBLIC_CIBLE)).thenReturn("texte target group ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_AIDES)).thenReturn("texte benefits ec");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.COMP_BESOINS_PART)).thenReturn("texte besoins particuliers ec");
		doReturn(descriptif).when(courseInfoGenerator).getComposantDescriptif(ec);

		ILangue langue = getLangue();
		doReturn(Arrays.asList(langue)).when(ec).langues();
		ITypeAE typeAECC = getTypeAE("CC", "Controle continu");
		ITypeAE typeAEEC = getTypeAE("EC", "Examen écrit");
		IAE ae1 = getAE("ae1", "Examen session1", typeAECC);
		IAE ae2 = getAE("ae2", "Examen session2", typeAEEC);

		ILien lienEc = getLienAvecCreditECTS(ec, new BigDecimal(10), 15 * 60);
		ITypeAP typeAP1 = getTypeAP("CM", "cours magistral");
		ITypeAP typeAP2 = getTypeAP("TD", "travaux dirigés");
		IAP ap1 = getAP(25, "libelle AP1", "codeAP1", typeAP1);
		IAP ap2 = getAP(23, "libelle AP2", "codeAP2", typeAP2);
		doReturn(Arrays.asList(ap1, ap2, ae1, ae2)).when(ec).childs();

		// Assert ready to test

		// Act
		courseInfoGenerator.generateCourse(element, lienEc);
		// Assert

		// Assert
		Element nodeCourse = (Element) element.getFirstChild();
		checkIdentifiant(nodeCourse, "FRUAIcodeRNECO1", "nom de l'EC", "EC", "codeEC");
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_DESCRIPTION, "texte description", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_LEARNING_OBJECTIVES, "texte learning objectives ec", true);
		checkCredit(nodeCourse, "10", "15");
		checkAdmissionInfo(nodeCourse, "texte admission ec", 25);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_FORMAL_PRE_REQUISITES, "formal prerequisites ec", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_RECOMMENDED_PRE_REQUISITES, "recommended prerequisites ec", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_TARGET_GROUP, "texte target group ec", true);
		checkFormOfTeaching(nodeCourse);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_BENEFITS, "texte benefits ec", true);
		checkInfoBlock(nodeCourse, ConstantesXML.TAG_COURSE_UNIVERSAL_ADJUSTMENT, "texte besoins particuliers ec", true);
		checkInstructionLanguage(nodeCourse, langue.llLangue());
		checkFormOfAssessment(nodeCourse);
		checkTeachingActivites(nodeCourse);
	}

	@Test
	public void getStructures() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementCourse");

		IStructure structure = getStructure("codeRNE", null, null, null);
		CourseInfoGenerator courseInfoGenerator = spy(new CourseInfoGenerator(doc, structure, true));

		IEC ec = getEC(1, "nom de l'EC", "codeEC");
		IDescriptif descriptif = getDescriptif("texte description");
		doReturn(descriptif).when(courseInfoGenerator).getComposantDescriptif(ec);
		ILien lienEc = getLienAvecCreditECTS(ec, new BigDecimal(10), 15 * 60);
		doReturn(Arrays.asList(structure)).when(ec).structures();
		// Assert ready to test

		// Act
		courseInfoGenerator.generateCourse(element, lienEc);
		List<IStructure> structures = courseInfoGenerator.getStructures();

		// Assert
		assertThat(structures.size()).isEqualTo(1);
		assertThat(structures.contains(structure)).isTrue();
	}

	@Test
	public void appendChildReference() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("UE");

		IStructure structure = getStructure("codeRNE", null, null, null);
		CourseInfoGenerator courseInfoGenerator = spy(new CourseInfoGenerator(doc, structure, true));

		IUE ue = getUE(1, "UE1", "UE1");
		IEC ec1 = getEC(2, "EC1", "EC1");
		IEC ec2 = getEC(3, "EC1", "EC1");
		doReturn(Arrays.asList(ec1, ec2)).when(ue).childs();

		// Act
		courseInfoGenerator.appendChildReference(element, ue);

		// Assert
		Element nodeContent = (Element) element.getElementsByTagName(ConstantesXML.TAG_COURSE_CONTENTS_EC).item(0);
		NodeList nodes = nodeContent.getElementsByTagName(ConstantesXML.TAG_REF_COURSE);
		assertThat(((Element) nodes.item(0)).getAttribute(ConstantesXML.TAG_ATTR_REF_ID)).isEqualTo("FRUAIcodeRNECO2");
		assertThat(((Element) nodes.item(1)).getAttribute(ConstantesXML.TAG_ATTR_REF_ID)).isEqualTo("FRUAIcodeRNECO3");
	}

	// Private Helpers
	private ITypeAE getTypeAE(String code, String libelle) {
		ITypeAE typeAE = mock(ITypeAE.class);
		when(typeAE.code()).thenReturn(code);
		when(typeAE.libelle()).thenReturn(libelle);
		return typeAE;
	}

	private IAE getAE(String code, String libelle, ITypeAE typeAE) {
		IAE ae = mock(IAE.class);
		when(ae.code()).thenReturn(code);
		when(ae.libelle()).thenReturn(libelle);
		when(ae.typeAE()).thenReturn(typeAE);
		return ae;
	}

	private void ajouteAdresse(IComposant composant, IAdresse adresse) {
		ILocal local = mock(ILocal.class);
		when(local.toFwkpers_Adresse()).thenReturn(adresse);

		IRepartBatImpGeo repart = mock(IRepartBatImpGeo.class);
		when(repart.toLocal()).thenReturn(local);

		IImplantationGeo implantationGeo = mock(IImplantationGeo.class);
		doReturn(Arrays.asList(repart)).when(implantationGeo).toRepartBatImpGeos();

		doReturn(Arrays.asList(implantationGeo)).when(composant).implantationsGeos();
	}

	private ILien getLienAvecCreditECTS(IComposant child, BigDecimal creditECTS, Integer horaireEnseigne) {
		ILien lien = mock(ILien.class);
		when(lien.getCreditECTSCalcule()).thenReturn(creditECTS);
		when(lien.getSommeMinutesEnseigneesCalculee()).thenReturn(horaireEnseigne);
		when(lien.child()).thenReturn(child);
		return lien;
	}

	private IUE getUE(Integer id, String libelle, String code) {
		IUE ue = mock(IUE.class);

		when(ue.id()).thenReturn(id);
		when(ue.libelle()).thenReturn(libelle);
		when(ue.code()).thenReturn(code);
		return ue;
	}

	private IEC getEC(Integer id, String libelle, String code) {
		IEC ec = mock(IEC.class);

		when(ec.id()).thenReturn(id);
		when(ec.libelle()).thenReturn(libelle);
		when(ec.code()).thenReturn(code);
		return ec;
	}

	private ITypeAP getTypeAP(String code, String libelle) {
		ITypeAP typeAP = mock(ITypeAP.class);
		when(typeAP.code()).thenReturn(code);
		when(typeAP.libelle()).thenReturn(libelle);
		return typeAP;
	}

	private IAP getAP(Integer seuil, String libelle, String code, ITypeAP typeAP) {
		IAP ap = mock(IAP.class);
		when(ap.libelle()).thenReturn(libelle);
		when(ap.code()).thenReturn(code);
		when(ap.seuil()).thenReturn(seuil);
		when(ap.typeAP()).thenReturn(typeAP);
		return ap;
	}

	private ILangue getLangue() {
		ILangue langue = mock(ILangue.class);
		when(langue.llLangue()).thenReturn("Espagnol");
		return langue;
	}

	private void checkIdentifiant(Element node, String strId, String courseName, String codeSet, String courseCode) {
		assertThat(node.getNodeName()).isEqualTo(ConstantesXML.TAG_COURSE);

		assertThat(node.getAttribute(ConstantesXML.TAG_ATTR_ID)).isEqualTo(strId);

		Element nodeCourseId = (Element) node.getElementsByTagName(ConstantesXML.TAG_COURSE_ID).item(0);
		assertThat(nodeCourseId.getTextContent()).isEqualTo(strId);

		Element nodeCourseName = (Element) node.getElementsByTagName(ConstantesXML.TAG_COURSE_NAME).item(0);
		assertThat(nodeCourseName.getTextContent()).isEqualTo(courseName);

		Element nodeCourseCode = (Element) node.getElementsByTagName(ConstantesXML.TAG_COURSE_CODE).item(0);
		assertThat(nodeCourseCode).isNotNull();
		assertThat(nodeCourseCode.getAttribute(ConstantesXML.TAG_ATTR_CODE_SET)).isEqualTo(codeSet);
		assertThat(nodeCourseCode.getTextContent()).isEqualTo(courseCode);
	}

	private void checkCredit(Element node, String creditECTS, String hoursPerWeek) {
		Element nodeCreditUE = (Element) node.getElementsByTagName(ConstantesXML.TAG_CREDITS).item(0);
		assertThat(nodeCreditUE).isNotNull();
		assertThat(nodeCreditUE.getAttribute(ConstantesXML.TAG_ATTR_ECTS_CREDITS)).isEqualTo(creditECTS);
		assertThat(nodeCreditUE.getAttribute(ConstantesXML.TAG_ATTR_TOTAL_WORK_LOAD)).isEqualTo(hoursPerWeek);
	}

	private void checkFormOfTeaching(Element node) {
		Element nodeFormOfTeaching = (Element) node.getElementsByTagName(ConstantesXML.TAG_FORM_OF_TEACHING).item(0);
		assertThat(nodeFormOfTeaching).isNotNull();
		// assertThat(nodeFormOfTeaching.getAttribute(ConstantesXML.TAG_ATTR_TEACHING_METHOD)).isEqualTo("texte form of teaching");
	}

	private void checkAdmissionInfo(Element nodeProgram, String texteAdmission, Integer nbPlaces) {
		Element nodeProgramAdmission = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_ADMISSION_INFO).item(0);
		Element nodeInfoBlock = (Element) nodeProgramAdmission.getElementsByTagName(ConstantesXML.TAG_INFO_BLOCK_SUBSTITUTE).item(0);
		assertThat(nodeInfoBlock.getTextContent()).isEqualTo(texteAdmission);

		if (nbPlaces != null) {
			Element nodeStudentPlaces = (Element) nodeProgramAdmission.getElementsByTagName(ConstantesXML.TAG_STUDENT_PLACES).item(0);
			assertThat(nodeStudentPlaces.getTextContent()).isEqualTo(nbPlaces.toString());
		}
	}

	private void checkInstructionLanguage(Element nodeProgram, String libelleLangue) {
		Element nodeInstructionLanguage = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_COURSE_INSTRUCTION_LANGUAGE).item(0);
		assertThat(nodeInstructionLanguage.getAttribute(ConstantesXML.TAG_COURSE_ATTR_TEACHING_LANGUAGE)).isEqualTo(libelleLangue);
	}

	private void checkFormOfAssessment(Element node) {
		Element nodeFormOfAssessment = (Element) node.getElementsByTagName(ConstantesXML.TAG_COURSE_FORM_OF_ASSESSMENT).item(0);
		Element nodeInfoBlock1 = (Element) nodeFormOfAssessment.getElementsByTagName(ConstantesXML.TAG_INFO_BLOCK_SUBSTITUTE).item(0);
		assertThat(nodeInfoBlock1.getTextContent()).isEqualTo("CC : Examen session1");
		Element nodeInfoBlock2 = (Element) nodeFormOfAssessment.getElementsByTagName(ConstantesXML.TAG_INFO_BLOCK_SUBSTITUTE).item(1);
		assertThat(nodeInfoBlock2.getTextContent()).isEqualTo("EC : Examen session2");
	}

	private void checkTeachingActivites(Element nodeCourse) {
		Element nodeTeachingActivity1 = (Element) nodeCourse.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY).item(0);
		Element nodeTeachingActivityID1 = (Element) nodeTeachingActivity1.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_ID).item(0);
		assertThat(nodeTeachingActivityID1.getTextContent()).isEqualTo("codeAP1");
		Element nodeTeachingActivityName1 = (Element) nodeTeachingActivity1.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_NAME).item(0);
		assertThat(nodeTeachingActivityName1.getTextContent()).isEqualTo("CM : libelle AP1");

		Element nodeTeachingActivity2 = (Element) nodeCourse.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY).item(1);
		Element nodeTeachingActivityID2 = (Element) nodeTeachingActivity2.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_ID).item(0);
		assertThat(nodeTeachingActivityID2.getTextContent()).isEqualTo("codeAP2");
		Element nodeTeachingActivityName2 = (Element) nodeTeachingActivity2.getElementsByTagName(ConstantesXML.TAG_COURSE_TEACHING_ACTIVITY_NAME).item(0);
		assertThat(nodeTeachingActivityName2.getTextContent()).isEqualTo("TD : libelle AP2");
	}
}
