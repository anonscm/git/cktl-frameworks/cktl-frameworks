package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDomaine;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IMention;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ISpecialite;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class HabilitationInfoGeneratorTests extends CDMTestCase {
	@Test
  public void testGenerateCdmfrHabilitation() throws Exception {
	  // Arrange 
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementHabilitation");
		IStructure structure = getStructure("codeRNE", null, null, null);
		HabilitationInfoGenerator habilitationInfogenerator = new HabilitationInfoGenerator(doc, structure, null);
	
		IGradeUniversitaire grade = getGradeUniversitaire("Master", "M", 1);
		IDiplome diplome = getScolFormationDiplome("libelle du diplôme", "dipCode", grade, null, null);
		doReturn(Arrays.asList(structure)).when(diplome).structures();
		IDomaine domaine = getDomaine("code", "domaine");
		doReturn(Arrays.asList(domaine)).when(diplome).domaines();
		ISpecialite specialite = getSpecialite("code","specialité");
		when(diplome.specialite()).thenReturn(specialite);
		IMention mention = getMention("code","field");
		when(diplome.mention()).thenReturn(mention);
	
	  
		// Act 
		habilitationInfogenerator.generateHabilitationDiplome(element, diplome);
		
	  // Assert
		Element nodeHabilitation = (Element) element.getFirstChild();
		//check
		checkIdentifiant(nodeHabilitation);
		checkCoHabilitation(nodeHabilitation);
		checkField(nodeHabilitation);
  }
		
	private IDomaine getDomaine(String code, String libelle) {
	  IDomaine domaine = mock(IDomaine.class);
	  when(domaine.getCode()).thenReturn(code);
	  when(domaine.getLibelle()).thenReturn(libelle);
	  return domaine;
  }
	private ISpecialite getSpecialite(String code, String libelle) {
		ISpecialite specialite = mock(ISpecialite.class);
	  when(specialite.code()).thenReturn(code);
	  when(specialite.libelle()).thenReturn(libelle);
	  return specialite;
  }
	private IMention getMention(String code, String libelle) {
		IMention mention = mock(IMention.class);
	  when(mention.code()).thenReturn(code);
	  when(mention.libelle()).thenReturn(libelle);
	  return mention;
  }

	private void checkField(Element nodeHabilitation) {
		Element nodeField = (Element) nodeHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_FIELD).item(0);
		Element nodeFieldName= (Element) nodeField.getElementsByTagName(ConstantesXML.TAG_HABILITATION_FIELD_NAME).item(0);
		Element nodeFieldFree= (Element) nodeFieldName.getElementsByTagName(ConstantesXML.TAG_HABILITATION_FIELD_FREE).item(0);
		assertThat(nodeFieldFree.getTextContent()).isEqualTo("field");			
		checkSpeciality(nodeField);
  }
	
	private void checkSpeciality(Element nodeHabilitation) {
		Element nodeSpeciality = (Element) nodeHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_SPECIALITY).item(0);
		Element nodeSpecialityID= (Element) nodeSpeciality.getElementsByTagName(ConstantesXML.TAG_HABILITATION_SPECIALITY_ID).item(0);
		assertThat(nodeSpecialityID).isNotNull();
	  
		Element nodeSpecialityName= (Element) nodeSpeciality.getElementsByTagName(ConstantesXML.TAG_HABILITATION_SPECIALITY_NAME).item(0);
		assertThat(nodeSpecialityName.getTextContent()).isEqualTo("specialité");
		
		Element nodeRefProgram= (Element) nodeSpeciality.getElementsByTagName(ConstantesXML.TAG_HABILITATION_REF_PROGRAM).item(0);
		assertThat(nodeRefProgram.getTextContent()).isEqualTo("FRUAIcodeRNEPRdipCode");			  
  }


	private void checkCoHabilitation(Element nodeHabilitation) {
		Element nodeCoHabilitation = (Element) nodeHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_COHABILITATION).item(0);
		Element nodeExists = (Element) nodeCoHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_EXISTS).item(0);		
		assertThat(nodeExists.getTextContent()).isEqualTo("true");
		
		Element listofOrgunit = (Element) nodeCoHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_LIST_OF_ORG_UNIT).item(0);
		Element habOrgUnit = (Element) listofOrgunit.getElementsByTagName(ConstantesXML.TAG_HABILITATION_HAB_ORG_UNIT).item(0);
		Element refOrgUnit = (Element) habOrgUnit.getElementsByTagName(ConstantesXML.TAG_HABILITATION_REF_ORG_UNIT).item(0);
		assertThat(refOrgUnit.getTextContent()).isEqualTo("codeRNE");
		
		Element  domainName = (Element) nodeCoHabilitation.getElementsByTagName(ConstantesXML.TAG_HABILITATION_DOMAIN_NAME).item(0);
		Element fixedDomain = (Element) domainName.getElementsByTagName(ConstantesXML.TAG_HABILITATION_FIXED_DOMAIN).item(0);
		assertThat(fixedDomain.getTextContent()).isEqualTo("domaine");
		
		
  }


	private void checkIdentifiant(Element node) {
		assertThat(node.getNodeName()).isEqualTo(ConstantesXML.TAG_HABILITATION);

		String strId = "FRUAIcodeRNEPRdipCode";
		
		Element nodeHabilitationId = (Element) node.getElementsByTagName(ConstantesXML.TAG_HABILITATION_ID).item(0);
		assertThat(nodeHabilitationId.getTextContent()).isEqualTo(strId);
	}
}
