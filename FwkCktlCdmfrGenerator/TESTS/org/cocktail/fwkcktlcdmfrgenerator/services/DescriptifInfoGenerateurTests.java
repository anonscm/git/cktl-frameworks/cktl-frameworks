package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.junit.Test;

public class DescriptifInfoGenerateurTests {

	@Test public void getLibelle() {
		// Arrange
		DescriptifInfoGenerator descriptifInfoGenerator = new DescriptifInfoGenerator();
		IDescriptif descriptif = mock(IDescriptif.class);
		when(descriptif.libelle()).thenReturn("description de la formation");
		// Act		
		assertThat(descriptifInfoGenerator.getLibelle(descriptif)).isEqualTo("description de la formation");
		assertThat(descriptifInfoGenerator.getLibelle(null)).isNull();
	}

	@Test public void getContenuChamp() {
		// Arrange
		DescriptifInfoGenerator descriptifInfoGenerator = new DescriptifInfoGenerator();
		IDescriptif descriptif = mock(IDescriptif.class);
		when(descriptif.getContenuChamp("titreChamp")).thenReturn("contenu du champ");
		// Assert		
		assertThat(descriptifInfoGenerator.getContenuChamp(descriptif, "titreChamp")).isEqualTo("contenu du champ");
		assertThat(descriptifInfoGenerator.getContenuChamp(descriptif, "titreinconnu")).isNull();
		assertThat(descriptifInfoGenerator.getContenuChamp(descriptif, null)).isNull();
		assertThat(descriptifInfoGenerator.getContenuChamp(null, "titreChamp")).isNull();
		
	}
}
