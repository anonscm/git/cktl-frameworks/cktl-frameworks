package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.doReturn;

import java.util.Arrays;

import javax.xml.parsers.ParserConfigurationException;

import org.cocktail.fwkcktlpersonne.common.metier.EOTypeStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.ITypeStructure;
import org.junit.Ignore;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

public class OrgUnitInfoGeneratorTests extends CDMTestCase {

	@Test
	public void generateOrgUnit() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementOrgUnit");
		IStructure structure = getStructure("codeRNE", null, "Université", "U");
		OrgUnitInfoGenerator listeOrgUnit = new OrgUnitInfoGenerator(doc, structure, null);



		ITypeStructure typeStructure = getTypeStructure();
		when(structure.toTypeStructure()).thenReturn(typeStructure);
		when(structure.grpMotsClefs()).thenReturn("search words");
		IAdresse adresse = getAdresse(null, null, null, "url1", null);
		IRepartPersonneAdresse repartPersonneAdresse = getRepartAdresse(adresse);
		doReturn(Arrays.asList(repartPersonneAdresse)).when(structure).toRepartPersonneAdresses();
		when(structure.grpEffectifs()).thenReturn(600);

		// Act
		listeOrgUnit.generateOrgUnit(element, structure);

		// Assert
		Element nodeOrgUnit = (Element) element.getFirstChild();
		checkIdentifiant(nodeOrgUnit);
		checkAcronym(nodeOrgUnit);
		checkUnitKind(nodeOrgUnit);
		checkWebLink(nodeOrgUnit, "url1", null);
		checkAdmissionInfo(nodeOrgUnit);
		checkSearchWord(nodeOrgUnit);
	}

	private IRepartPersonneAdresse getRepartAdresse(IAdresse adresse) {
		IRepartPersonneAdresse repartPersonneAdresse = mock(IRepartPersonneAdresse.class);
		when(repartPersonneAdresse.toAdresse()).thenReturn(adresse);
		return repartPersonneAdresse;
	}

	private void checkIdentifiant(Element node) {
		assertThat(node.getNodeName()).isEqualTo(ConstantesXML.TAG_ORG_UNIT);

		String strId = "FRUAIcodeRNEOUcodeRNE";
		assertThat(node.getAttribute(ConstantesXML.TAG_ATTR_ID)).isEqualTo(strId);

		Element nodeOrgUnitId = (Element) node.getElementsByTagName(ConstantesXML.TAG_ORG_UNIT_ID).item(0);
		assertThat(nodeOrgUnitId.getTextContent()).isEqualTo(strId);

		Element nodeOrgUnitName = (Element) node.getElementsByTagName(ConstantesXML.TAG_ORG_UNIT_NAME).item(0);
		assertThat(nodeOrgUnitName.getTextContent()).isEqualTo("Université");

		Element nodeOrgUnitCode = (Element) node.getElementsByTagName(ConstantesXML.TAG_ORG_UNIT_CODE).item(0);
		assertThat(nodeOrgUnitCode.getAttribute(ConstantesXML.TAG_ATTR_CODE_SET)).isEqualTo("codeUAI");
		assertThat(nodeOrgUnitCode).isNotNull();
	}

	private void checkAcronym(Element node) {
		Element nodeAcronym = (Element) node.getElementsByTagName(ConstantesXML.TAG_ORG_UNIT_ACRONYM).item(0);
		assertThat(nodeAcronym.getTextContent()).isEqualTo("U");
	}

	private ITypeStructure getTypeStructure() {
		ITypeStructure typeStructure = mock(ITypeStructure.class);
		when(typeStructure.lTypeStructure()).thenReturn("typeStructure");
		when(typeStructure.cTypeStructure()).thenReturn(EOTypeStructure.TYPE_STRUCTURE_E);
		return typeStructure;
	}

	private void checkUnitKind(Element node) {
		Element nodeUnitKind = (Element) node.getElementsByTagName(ConstantesXML.TAG_ORG_UNIT_KIND).item(0);
		assertThat(nodeUnitKind.getAttribute(ConstantesXML.TAG_ATTR_UNIT_KIND)).isEqualTo("universite");
		assertThat(nodeUnitKind.getTextContent()).isEqualTo("typeStructure");
	}

	private void checkAdmissionInfo(Element node) {
		Element nodeProgramAdmission = (Element) node.getElementsByTagName(ConstantesXML.TAG_ADMISSION_INFO).item(0);

		Element nodeStudentPlaces = (Element) nodeProgramAdmission.getElementsByTagName(ConstantesXML.TAG_STUDENT_PLACES).item(0);
		assertThat(nodeStudentPlaces.getTextContent()).isEqualTo("600");
	}

	private void checkSearchWord(Element node) {
		Element nodeSearchWord = (Element) node.getElementsByTagName(ConstantesXML.TAG_SEARCH_WORD).item(0);
		assertThat(nodeSearchWord.getTextContent()).isEqualTo("search words");
	}

}
