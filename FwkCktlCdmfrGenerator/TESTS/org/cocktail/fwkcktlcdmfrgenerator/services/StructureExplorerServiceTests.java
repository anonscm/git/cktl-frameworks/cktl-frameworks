package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.junit.Test;

public class StructureExplorerServiceTests {

	@Test
  public void getListeStructuresParentes() {
	  // Arrange
		StructureExplorerService structureExplorerService = new StructureExplorerService();
		IStructure structure = mock(IStructure.class);
		IStructure structureN1 = mock(IStructure.class);
		IStructure structureN2 = mock(IStructure.class);
		IStructure structureN3 = mock(IStructure.class);
		when(structure.toStructurePere()).thenReturn(structureN1);
		when(structureN1.toStructurePere()).thenReturn(structureN2);
		when(structureN2.toStructurePere()).thenReturn(structureN3);
	  // Assert ready to test
		
	  // Act
		List<IStructure> structuresParentes = structureExplorerService.getListeStructuresParentes(structure);
		
	  // Assert
		assertThat(structuresParentes.contains(structure)).isFalse();
		assertThat(structuresParentes.contains(structureN1)).isTrue();
		assertThat(structuresParentes.contains(structureN2)).isTrue();
		assertThat(structuresParentes.contains(structureN3)).isTrue();
		
  }
	
	@Test
  public void getListeStructuresParentes_null() {
	  // Arrange
		StructureExplorerService structureExplorerService = new StructureExplorerService();
		IStructure structure = null;
	  // Assert ready to test
	  // Act 
		List<IStructure> structuresParentes = structureExplorerService.getListeStructuresParentes(structure);
	  // Assert
		assertThat(structuresParentes.isEmpty()).isTrue();
  }
	
	@Test
	 public void getListeStructuresParentes_loop() {
	  // Arrange
		StructureExplorerService structureExplorerService = new StructureExplorerService();
		IStructure structure = mock(IStructure.class);
		IStructure structureN1 = mock(IStructure.class);
		when(structure.toStructurePere()).thenReturn(structureN1);
		when(structureN1.toStructurePere()).thenReturn(structure);
	  // Assert ready to test
	  // Act 
		List<IStructure> structuresParentes = structureExplorerService.getListeStructuresParentes(structure);
	  // Assert
		assertThat(structuresParentes.size()).isEqualTo(100);
  }
	
	@Test
  public void getListeStructureAvecHierarchie() {
	  // Arrange
		StructureExplorerService structureExplorerService = new StructureExplorerService();
		IStructure structure1 = mock(IStructure.class);
		IStructure structure2 = mock(IStructure.class);
		IStructure structure3 = mock(IStructure.class);
		IStructure structureN1 = mock(IStructure.class);
		IStructure structureN2 = mock(IStructure.class);
		IStructure structureN3 = mock(IStructure.class);
		IStructure structureN4 = mock(IStructure.class);
		when(structure1.toStructurePere()).thenReturn(structureN1);
		when(structureN1.toStructurePere()).thenReturn(structureN2);
		when(structureN2.toStructurePere()).thenReturn(structureN3);
		when(structure2.toStructurePere()).thenReturn(structureN1);
		when(structure3.toStructurePere()).thenReturn(structureN4);
	  // Assert ready to test
		
	  // Act
		List<IStructure> structuresParentes = structureExplorerService.getListeStructureAvecHierarchie(Arrays.asList(structure1, structure2, structure3));
		
	  // Assert
		assertThat(structuresParentes.size()).isEqualTo(7);
		assertThat(structuresParentes.contains(structure1)).isTrue();
		assertThat(structuresParentes.contains(structure2)).isTrue();
		assertThat(structuresParentes.contains(structure3)).isTrue();
		assertThat(structuresParentes.contains(structureN1)).isTrue();
		assertThat(structuresParentes.contains(structureN2)).isTrue();
		assertThat(structuresParentes.contains(structureN3)).isTrue();
		assertThat(structuresParentes.contains(structureN4)).isTrue();
  }
}
