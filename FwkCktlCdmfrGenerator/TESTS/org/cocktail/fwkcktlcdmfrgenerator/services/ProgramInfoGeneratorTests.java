package org.cocktail.fwkcktlcdmfrgenerator.services;

import static org.fest.assertions.api.Assertions.assertThat;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IPays;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IRepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.interfaces.IStructure;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptif;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDescriptifChampTitre;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IEC;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IGradeUniversitaire;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ILien;
import org.cocktail.fwkcktlscolpeda.serveur.metier.INiveauAccesDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IParcours;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IPeriode;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IRegroupement;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IResponsableComposant;
import org.cocktail.fwkcktlscolpeda.serveur.metier.ITypeFormation;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IUE;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVersionDiplome;
import org.cocktail.fwkcktlscolpeda.serveur.metier.IVocation;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class ProgramInfoGeneratorTests extends CDMTestCase {

	@Test
	public void getDiplomeName() throws Exception {
		// Arrange
		ProgramInfoGenerator program = spy(new ProgramInfoGenerator(null, null, null));
		IGradeUniversitaire gradeUniversitaire = getGradeUniversitaire("Master", "M", 2);
		IDiplome diplome = getScolFormationDiplome("libelle du diplôme", "dipCode", gradeUniversitaire, null, null);

		// Assert
		assertThat(program.getDiplomeName(diplome)).isEqualTo("Master libelle du diplôme");

		when(diplome.gradeUniversitaire()).thenReturn(null);
		assertThat(program.getDiplomeName(diplome)).isEqualTo("libelle du diplôme");
	}

	@Test
	public void getNbCreditECTS() throws Exception {
		// Arrange
		Document doc = generateur.getNewDocument();
		ProgramInfoGenerator program = new ProgramInfoGenerator(doc, null, null);

		IVersionDiplome versionDiplome = getVersionDiplome();
		ajouteLienAvecCreditECTS(null, Arrays.asList((IComposant) versionDiplome), Arrays.asList(new BigDecimal(180)));
		assertThat(program.getNbCreditECTS(versionDiplome)).isEqualTo(180);
		// Act
		versionDiplome = getVersionDiplome();
		BigDecimal credit = null;
		ajouteLienAvecCreditECTS(null, Arrays.asList((IComposant) versionDiplome), Arrays.asList(credit));
		// Assert
		assertThat(program.getNbCreditECTS(versionDiplome)).isNull();
	}
	
	@Test
  public void getLevelCode() throws Exception {
	  // Arrange
		Document doc = generateur.getNewDocument();
		ProgramInfoGenerator program = new ProgramInfoGenerator(doc, null, null);
		INiveauAccesDiplome niveauAccesDiplome =getNiveauAccesDiplome(3);
		IGradeUniversitaire gradeUniversitaire = getGradeUniversitaire("Master", "M", 2);
		IDiplome diplome = getScolFormationDiplome("libelle du diplôme", "dipCode", null, null, null);
	  	
		// Act -Assert
		assertThat( program.getLevelSet(diplome)).isEqualTo(LevelSet.LEVEL_Inconnu);
		when(diplome.gradeUniversitaire()).thenReturn(gradeUniversitaire);
		assertThat(program.getLevelSet(diplome)).isEqualTo(LevelSet.LEVEL_BACplus2);
		when(diplome.niveauAccesDiplome()).thenReturn(niveauAccesDiplome);
		assertThat(program.getLevelSet(diplome)).isEqualTo(LevelSet.LEVEL_BACplus5);
		when(diplome.gradeUniversitaire()).thenReturn(null);
		assertThat(program.getLevelSet(diplome)).isEqualTo(LevelSet.LEVEL_BACplus3);
	
  }

	@Test
	public void generateProgramDiplome() throws Exception {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementProgram");
		IStructure structure = getStructure("codeRNE", null, null, null);
		ProgramInfoGenerator programInfoGenerator = spy(new ProgramInfoGenerator(doc, structure, true));

		
		IGradeUniversitaire grade = getGradeUniversitaire("Master", "M", 2);
		INiveauAccesDiplome niveauAccesDiplome = getNiveauAccesDiplome(3);
		IVocation vocation = getVocation("PRO", "Professionnelle");
		ITypeFormation typeFormation = mock(ITypeFormation.class);
		when(typeFormation.code()).thenReturn(ITypeFormation.TF_MASTER);
		IDiplome diplome = getScolFormationDiplome("libelle du diplôme", "dipCode", grade, vocation, niveauAccesDiplome);
		when(diplome.typeFormation()).thenReturn(typeFormation);
		IVersionDiplome versionDiplome = getVersionDiplome();
		ajouteLienAvecCreditECTS(null, Arrays.asList((IComposant) versionDiplome), Arrays.asList(new BigDecimal(180)));
		when(versionDiplome.getDiplome()).thenReturn(diplome);
		doReturn(Arrays.asList(structure)).when(diplome).structures();
		IDescriptif descriptif = getDescriptif("texte description");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_METIER)).thenReturn("Métier visé");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_COMPETENCE)).thenReturn("poursuite des études");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_CONNAISSANCE)).thenReturn("texte learning objective");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_RECRUTEMENT)).thenReturn("texte admission");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_RECOMMANDATION)).thenReturn("recommended prerequisites");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_PREREQUIS)).thenReturn("formal prerequisites");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_PUBLIC_SPEC)).thenReturn("texte target group");
		when(descriptif.getContenuChamp(IDescriptifChampTitre.DIP_MODALITE_EVALUATION)).thenReturn("texte regulations");
		doReturn(descriptif).when(programInfoGenerator).getComposantDescriptif(diplome);
		IPays pays = getPays();
		IAdresse adresse = getAdresse("Rue de l'université", "Pau", "64000", null, pays);
		ajouteAdresse(structure, adresse);
		IIndividu individu = getIndPers();
		IPersonne personne = getPersonneIndividu(individu.persId(), individu.prenom(), individu.nomUsuel(), individu.indQualite());
		IResponsableComposant responsableComposant1 = getResponsable(personne, null);
		IStructure ufr1 = getStructure("1234578", null, "ufr1", "ufr1");		
		IResponsableComposant responsableComposant2 = getResponsable(getPersonneStructure(ufr1.cRne(), ufr1.siret(), ufr1.llStructure(), ufr1.lcStructure()), null);
		doReturn(Arrays.asList(responsableComposant1, responsableComposant2)).when(diplome).responsablesComposant();

		// structure
		// versionDiplome
		// ===>parcours
		// ===========>semestre
		// ===================>ue1
		// ===================>ue2
		IParcours parcours = getParcours("0", "parcours Name");
		ajouteLienAvecCreditECTS(versionDiplome, Arrays.asList((IComposant) parcours), Arrays.asList(new BigDecimal(10)));

		IPeriode semestre = getPeriode("1", "semestre 1", 1);
		ajouteLienAvecCreditECTS(parcours, Arrays.asList((IComposant) semestre), Arrays.asList(new BigDecimal(10)));

		IUE ue1 = getUE("1", 1);
		IUE ue2 = getUE("2", 2);
		ajouteLienAvecCreditECTS(semestre, Arrays.asList((IComposant) ue1, (IComposant) ue2), Arrays.asList(new BigDecimal(5), new BigDecimal(2)));

		// Assert ready to test
		assertThat(programInfoGenerator.getDiplomeName(diplome)).isNotNull();
		assertThat(programInfoGenerator.getLiensTypeCourse(semestre).size()).isEqualTo(2);

		// Act
		programInfoGenerator.generateProgramDiplome(element, versionDiplome);

		// Assert
		Element nodeProgram = (Element) element.getFirstChild();
		checkIdentifiant(nodeProgram);
		checkProgramDescription(nodeProgram);
		checkProgramQualification(nodeProgram);
		checkProgramTeachingLangage(nodeProgram);
		checkProgramLevel(nodeProgram);
		checkProgramLevelCode(nodeProgram);
		checkInfoBlock(nodeProgram, ConstantesXML.TAG_LEARNING_OBJECTIVES, "texte learning objective", true);
		checkAdmissionInfo(nodeProgram);
		checkInfoBlock(nodeProgram, ConstantesXML.TAG_FORMAL_PRE_REQUISITES, "formal prerequisites", true);
		checkInfoBlock(nodeProgram, ConstantesXML.TAG_RECOMMENDED_PRE_REQUISITES, "recommended prerequisites", true);
		checkTeachingPlace(nodeProgram);
		checkInfoBlock(nodeProgram, ConstantesXML.TAG_TARGET_GROUP, "texte target group", true);
		checkProgramDuration(nodeProgram);
		checkProgramStructure(nodeProgram);
		checkInfoBlock(nodeProgram, ConstantesXML.TAG_PROGRAM_REGULATIONS, "texte regulations", true);
		checkContacts(nodeProgram,ConstantesXML.TAG_REF_PERSON, "FRUAIcodeRNEPE1", "contact");
		checkContacts(nodeProgram,ConstantesXML.TAG_REF_ORG_UNIT, "FRUAIcodeRNEOU1234578", "contact");
		checkSubProgram(nodeProgram);
	}

	@Test
	public void generateSubProgram() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementProgram");
		IStructure structure = getStructure("codeRNE", null, null, null);
		ProgramInfoGenerator programInfoGenerator = spy(new ProgramInfoGenerator(doc, structure, true));

		// structure
		// semestre
		// ===>parcours1
		// ============>UE1
		// ============>UE2
		// ===============>EC1
		// ===============>EC2
		// ===>parcours2
		// ============>UE3
		// ===============>regroupement
		// ============================>EC3
		// ============================>EC4
		IPeriode semestre = getPeriode("semestre", "semestre", 1);
		IParcours parcours1 = getParcours("parcours1", "parcours1");
		IParcours parcours2 = getParcours("parcours2", "parcours2");
		IRegroupement regroupement = getRegroupement("regroupement", "regroupement");
		IUE ue1 = getUE("ue1", 101);
		IUE ue2 = getUE("ue2", 102);
		IUE ue3 = getUE("ue3", 103);
		IEC ec1 = getEC("ec1", 201);
		IEC ec2 = getEC("ec2", 202);
		IEC ec3 = getEC("ec3", 203);
		IEC ec4 = getEC("ec4", 204);
		ajouteLienAvecCreditECTS(null, Arrays.asList((IComposant) semestre), Arrays.asList(new BigDecimal(180)));
		ajouteLienAvecCreditECTS(semestre, Arrays.asList((IComposant) parcours1, (IComposant) parcours2), Arrays.asList(new BigDecimal(10), new BigDecimal(10)));
		ajouteLienAvecCreditECTS(parcours1, Arrays.asList((IComposant) ue1, (IComposant) ue2), Arrays.asList(new BigDecimal(10), new BigDecimal(10)));
		ajouteLienAvecCreditECTS(ue2, Arrays.asList((IComposant) ec1, (IComposant) ec2), Arrays.asList(new BigDecimal(10), new BigDecimal(10)));
		ajouteLienAvecCreditECTS(parcours2, Arrays.asList((IComposant) ue3), Arrays.asList(new BigDecimal(10)));
		ajouteLienAvecCreditECTS(ue3, Arrays.asList((IComposant) regroupement), Arrays.asList(new BigDecimal(10)));
		ajouteLienAvecCreditECTS(regroupement, Arrays.asList((IComposant) ec3, (IComposant) ec4), Arrays.asList(new BigDecimal(10), new BigDecimal(10)));

		// Act
		programInfoGenerator.generateSubProgram(element, semestre);
		List<ILien> liensCours = programInfoGenerator.getLienComposantCours();
		// Assert
		assertThat(liensCours.size()).isEqualTo(7);
		assertThat(liensCours.size()).isEqualTo(7);
		assertThat(liensCours.get(0).child()).isEqualTo(ue1);
		assertThat(liensCours.get(1).child()).isEqualTo(ue2);
		assertThat(liensCours.get(2).child()).isEqualTo(ec1);
		assertThat(liensCours.get(3).child()).isEqualTo(ec2);
		assertThat(liensCours.get(4).child()).isEqualTo(ue3);
		assertThat(liensCours.get(5).child()).isEqualTo(ec3);
		assertThat(liensCours.get(6).child()).isEqualTo(ec4);


		NodeList listSubProgram =  element.getElementsByTagName(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM);		
		assertThat(listSubProgram.getLength()).isEqualTo(2);
		
		Element nodeParcours1 = (Element) element.getElementsByTagName(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM).item(0);
		assertThat(nodeParcours1).isNotNull();
		Element nodeStructure = (Element) nodeParcours1.getElementsByTagName(ConstantesXML.TAG_PROGRAM_STRUCTURE).item(0);
		NodeList nodesRefCours =nodeStructure.getElementsByTagName( ConstantesXML.TAG_REF_COURSE);
		assertThat(nodesRefCours.getLength()).isEqualTo(2);
		assertThat( ((Element) nodesRefCours.item(0)).getAttribute(ConstantesXML.TAG_ATTR_REF_ID)  ).isEqualTo("FRUAIcodeRNECO101");
		assertThat( ((Element) nodesRefCours.item(1)).getAttribute(ConstantesXML.TAG_ATTR_REF_ID)  ).isEqualTo("FRUAIcodeRNECO102");
		
		Element nodeParcours2 = (Element) element.getElementsByTagName(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM).item(1);
		assertThat(nodeParcours2).isNotNull();
		nodeStructure = (Element) nodeParcours2.getElementsByTagName(ConstantesXML.TAG_PROGRAM_STRUCTURE).item(0);
		nodesRefCours = nodeStructure.getElementsByTagName( ConstantesXML.TAG_REF_COURSE);	
		assertThat(nodesRefCours.getLength()).isEqualTo(1);
		assertThat( ((Element) nodesRefCours.item(0)).getAttribute(ConstantesXML.TAG_ATTR_REF_ID)  ).isEqualTo("FRUAIcodeRNECO103");		
	}

	@Test
	public void getStructures() throws ParserConfigurationException {
		// Arrange
		Document doc = generateur.getNewDocument();
		Element element = doc.createElement("ElementProgram");
		ProgramInfoGenerator programInfoGenerator = spy(new ProgramInfoGenerator(doc, null, true));
		IStructure structure = mock(IStructure.class);
		IStructure structureParcours1 = mock(IStructure.class);
		IStructure structureParcours2 = mock(IStructure.class);

		IGradeUniversitaire grade = getGradeUniversitaire("Master", "M", 2);
		IVocation vocation = getVocation("PRO", "Professionnelle");
		IDiplome diplome = getScolFormationDiplome("libelle du diplôme", "dipCode", grade, vocation, null);
		IVersionDiplome versionDiplome = getVersionDiplome();
		when(versionDiplome.getDiplome()).thenReturn(diplome);
		doReturn(Arrays.asList(structure)).when(diplome).structures();
		IPeriode semestre = getPeriode("semestre", "semestre", 1);
		IParcours parcours1 = getParcours("parcours1", "parcours1");
		IParcours parcours2 = getParcours("parcours2", "parcours2");
		doReturn(Arrays.asList(structureParcours1, structureParcours2)).when(parcours1).structures();
		doReturn(Arrays.asList(structureParcours2)).when(parcours2).structures();
		ajouteLienAvecCreditECTS(diplome, Arrays.asList((IComposant) versionDiplome), null);
		ajouteLienAvecCreditECTS(versionDiplome, Arrays.asList((IComposant) semestre), null);
		ajouteLienAvecCreditECTS(semestre, Arrays.asList((IComposant) parcours1, (IComposant) parcours2), null);

		doReturn(null).when(programInfoGenerator).getComposantDescriptif(diplome);
		// Act
		programInfoGenerator.generateProgramDiplome(element, versionDiplome);
		List<IStructure> structures = programInfoGenerator.getStructures();
		// Assert
		assertThat(structures.size()).isEqualTo(3);
		assertThat(structures.contains(structure));
		assertThat(structures.contains(structureParcours1));
		assertThat(structures.contains(structureParcours2));
	}

	private void ajouteAdresse(IStructure structure, IAdresse adresse) {
		IRepartPersonneAdresse repartAdresse = mock(IRepartPersonneAdresse.class);
		when(repartAdresse.toAdresse()).thenReturn(adresse);
		doReturn(Arrays.asList(repartAdresse)).when(structure).toRepartPersonneAdresses();
	}

	private IVocation getVocation(String code, String libelle) {
		IVocation vocation = mock(IVocation.class);
		when(vocation.code()).thenReturn(code);
		when(vocation.libelle()).thenReturn(libelle);
		return vocation;
	}

	private void checkProgramDescription(Element nodeProgram) {
		Element nodeProgramDescription = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_DESCRIPTION).item(0);
		assertThat(nodeProgramDescription.getAttribute(ConstantesXML.TAG_PROGRAM_DESCRIPTION_ATTR_NATURE)).isEqualTo(ConstantesXML.CONST_NATURE_PROGRAM_DIPLOME);
		assertThat(nodeProgramDescription.getTextContent()).isEqualTo("texte description");

	}

	private void checkProgramDuration(Element nodeProgram) {
		Element nodeProgramDuration = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_DURATION).item(0);
		assertThat(nodeProgramDuration.getTextContent()).isEqualTo("2 année(s)");
	}

	private void checkSubProgram(Element nodeProgram) {
		// parcours
		Element nodeSubProgram = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM).item(0);
		Element nodeSubProgramId = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_ID).item(0);
		assertThat(nodeSubProgramId.getTextContent()).isEqualTo("FRUAIcodeRNESU0");

		Element nodeSubProgramName = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_NAME).item(0);
		Element nodeTextName = (Element) nodeSubProgramName.getElementsByTagName(ConstantesXML.TAG_CDMFR_TEXT).item(0);
		assertThat(nodeTextName.getTextContent()).isEqualTo("parcours Name");

		Element nodeSubProgramCode = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_CODE).item(0);
		assertThat(nodeSubProgramCode).isNotNull();

		Element nodeSubProgramCredits = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_CREDITS).item(0);
		assertThat(nodeSubProgramCredits.getAttribute(ConstantesXML.TAG_ATTR_ECTS_CREDITS)).isEqualTo("10");

		// check for semestre
		nodeSubProgram = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_SUB_PROGRAM).item(0);
		nodeSubProgramId = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_ID).item(0);
		assertThat(nodeSubProgramId.getTextContent()).isEqualTo("FRUAIcodeRNESU1");

		nodeSubProgramName = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_NAME).item(0);
		nodeTextName = (Element) nodeSubProgramName.getElementsByTagName(ConstantesXML.TAG_CDMFR_TEXT).item(0);
		assertThat(nodeTextName.getTextContent()).isEqualTo("semestre 1");

		nodeSubProgramCode = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_CODE).item(0);
		assertThat(nodeSubProgramCode).isNotNull();

		nodeSubProgramCredits = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_CREDITS).item(0);
		assertThat(nodeSubProgramCredits.getAttribute(ConstantesXML.TAG_ATTR_ECTS_CREDITS)).isEqualTo("10");

//		// check for structure
		Element nodeProgramStructure = (Element) nodeSubProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_STRUCTURE).item(0);
		assertThat(nodeProgramStructure).isNotNull();

		Element nodeRefUE1 = (Element) nodeProgramStructure.getElementsByTagName(ConstantesXML.TAG_REF_COURSE).item(0);
		assertThat(nodeRefUE1.getAttribute(ConstantesXML.TAG_ATTR_REF_ID)).isEqualTo("FRUAIcodeRNECO1");
		Element nodeRefUE2 = (Element) nodeProgramStructure.getElementsByTagName(ConstantesXML.TAG_REF_COURSE).item(1);
		assertThat(nodeRefUE2.getAttribute(ConstantesXML.TAG_ATTR_REF_ID)).isEqualTo("FRUAIcodeRNECO2");
	}

	private void checkProgramStructure(Element nodeProgram) {
		Element nodeProgramStructure = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_STRUCTURE).item(0);
		assertThat(nodeProgramStructure).isNotNull();
	}

	private void checkAdmissionInfo(Element nodeProgram) {
		Element nodeProgramAdmission = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_ADMISSION_INFO).item(0);
		Element nodeInfoBlock = (Element) nodeProgramAdmission.getElementsByTagName(getInfoBlockTagName(true)).item(0);
		assertThat(nodeInfoBlock.getTextContent()).isEqualTo("texte admission");
//		Element nodeStudentPlaces = (Element) nodeProgramAdmission.getElementsByTagName(ConstantesXML.TAG_STUDENT_PLACES).item(0);
//		assertThat(nodeStudentPlaces.getTextContent()).isEqualTo("25");
		Element nodeECTSRequired = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_ECTS_REQUIRED).item(0);
		assertThat(nodeECTSRequired).isNotNull();
		Element nodeStudentStatus = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_STUDENT_STATUS).item(0);
		assertThat(nodeStudentStatus).isNotNull();
	}

	private void checkProgramLevel(Element nodeProgram) {
		Element nodeProgramLevel = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_LEVEL).item(0);
		assertThat(nodeProgramLevel.getAttribute(ConstantesXML.TAG_PROGRAM_ATTR_LEVEL)).isEqualTo("M");

		assertThat(nodeProgramLevel.getAttribute(ConstantesXML.TAG_PROGRAM_ATTR_YIELDING_COMPETENCE)).isEqualTo("Professionnelle");
		assertThat(nodeProgramLevel.getAttribute(ConstantesXML.TAG_PROGRAM_ATTR_YIELDING_COMPETENCE_CODE)).isEqualTo("PRO");
		
		
	}
	
	private void checkProgramLevelCode(Element nodeProgram) {
		Element nodeLevelCode = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_LEVEL_CODE).item(0);
		assertThat(nodeLevelCode.getAttribute(ConstantesXML.TAG_PROGRAM_ATTR_CODE_SET)).isEqualTo("bac+5");
  }
	
	private void checkProgramTeachingLangage(Element nodeProgram) {
		Element nodeProgramLevel = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_TEACHING_LANGUAGE).item(0);
		assertThat(nodeProgramLevel.getAttribute(ConstantesXML.TAG_ATTR_PROGRAM_TEACHING_LANG)).isEqualTo("Français");
	}

	private void checkProgramQualification(Element nodeProgram) {
		Element nodeProgramQualification = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_QUALIFICATION).item(0);
		assertThat(nodeProgramQualification).isNotNull();

		Element nodeProgramQualificationName = (Element) nodeProgramQualification.getElementsByTagName(ConstantesXML.TAG_PROGRAM_QUALIFICATION_NAME).item(0);
		assertThat(nodeProgramQualificationName).isNotNull();

		Element nodeProgramQualificationDescription = (Element) nodeProgramQualification.getElementsByTagName(ConstantesXML.TAG_PROGRAM_QUALIFICATION_DESCRIPTION)
		    .item(0);
		assertThat(nodeProgramQualificationDescription).isNotNull();

		Element nodeProgramCredits = (Element) nodeProgramQualification.getElementsByTagName(ConstantesXML.TAG_CREDITS).item(0);
		assertThat(nodeProgramCredits).isNotNull();
		assertThat(nodeProgramCredits.getAttribute(ConstantesXML.TAG_ATTR_ECTS_CREDITS)).isEqualTo("180");

		Element nodeProgramDegree = (Element) nodeProgramQualification.getElementsByTagName(ConstantesXML.TAG_PROGRAM_DEGREE).item(0);
		assertThat(nodeProgramDegree).isNotNull();
		assertThat(nodeProgramDegree.getAttribute(ConstantesXML.TAG_PROGRAM_ATTR_DEGREE)).isEqualTo("master");

		checkInfoBlockAvecParagraph(nodeProgramQualification, ConstantesXML.TAG_PROGRAM_PROFESSION, "Métier visé");

		checkInfoBlockAvecParagraph(nodeProgramQualification, ConstantesXML.TAG_PROGRAM_STUDY_QUALIFICATION, "poursuite des études");
	}

	private void checkIdentifiant(Element nodeProgram) {
		assertThat(nodeProgram.getNodeName()).isEqualTo(ConstantesXML.TAG_PROGRAM);

		String strId = "FRUAIcodeRNEPRdipCode";
		assertThat(nodeProgram.getAttribute(ConstantesXML.TAG_ATTR_ID)).isEqualTo(strId);

		Element nodeProgramId = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_ID).item(0);
		assertThat(nodeProgramId.getTextContent()).isEqualTo(strId);

		Element nodeProgramName = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_NAME).item(0);
		Element nodeTextName = (Element) nodeProgramName.getElementsByTagName(ConstantesXML.TAG_CDMFR_TEXT).item(0);
		assertThat(nodeTextName.getTextContent()).isEqualTo("Master libelle du diplôme");

		Element nodeProgramCode = (Element) nodeProgram.getElementsByTagName(ConstantesXML.TAG_PROGRAM_CODE).item(0);
		assertThat(nodeProgramCode).isNotNull();
	}

	private IVersionDiplome getVersionDiplome() {
		IVersionDiplome versionDiplome = mock(IVersionDiplome.class);
		return versionDiplome;
	}

	private void ajouteLienAvecCreditECTS(IComposant parent, List<IComposant> childs, List<BigDecimal> listeCreditECTS) {

		List<ILien> liens = new ArrayList<ILien>();

		for (int i = 0; i < childs.size(); i++) {
			IComposant child = childs.get(i);
			ILien lien = mock(ILien.class);
			if (listeCreditECTS != null) {
				BigDecimal creditECTS = listeCreditECTS.get(i);
				when(lien.getCreditECTSCalcule()).thenReturn(creditECTS);
			}
			when(lien.child()).thenReturn(child);
			when(lien.parent()).thenReturn(parent);
			doReturn(Arrays.asList(lien)).when(child).liensChildsWithoutDuplicate();
			liens.add(lien);
		}

		if (parent != null) {
			doReturn(liens).when(parent).liensParentsWithoutDuplicate();
		}

	}

	private IParcours getParcours(String code, String libelle) {
		IParcours parcours = mock(IParcours.class);
		when(parcours.code()).thenReturn(code);
		when(parcours.libelle()).thenReturn(libelle);
		return parcours;
	}

	private IRegroupement getRegroupement(String code, String libelle) {
		IRegroupement regroupement = mock(IRegroupement.class);
		when(regroupement.code()).thenReturn(code);
		when(regroupement.libelle()).thenReturn(libelle);
		return regroupement;
	}

	private IPeriode getPeriode(String code, String libelle, Integer ordre) {
		IPeriode periode = mock(IPeriode.class);
		when(periode.code()).thenReturn(code);
		when(periode.libelle()).thenReturn(libelle);
		when(periode.ordre()).thenReturn(ordre);
		return periode;
	}

	private IUE getUE(String code, Integer id) {
		IUE ue = mock(IUE.class);
		when(ue.code()).thenReturn(code);
		when(ue.id()).thenReturn(id);
		return ue;
	}

	private IEC getEC(String code, Integer id) {
		IEC ec = mock(IEC.class);
		when(ec.code()).thenReturn(code);
		when(ec.id()).thenReturn(id);
		return ec;
	}

}
