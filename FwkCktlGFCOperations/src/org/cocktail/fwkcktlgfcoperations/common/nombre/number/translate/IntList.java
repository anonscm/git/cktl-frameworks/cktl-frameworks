package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


// Referenced classes of package nombre:
//            Vocab

public class IntList
{

    int hd;
    IntList tl;

    protected static IntList ofLetters(Vocab vocab, String s)
    {
        return ofLetters(vocab, s, s.length(), null);
    }

    protected static IntList ofLetters(Vocab vocab, String s, int i, IntList intlist)
    {
        if(i == 0)
        {
            return intlist;
        }
        int j = i - 1;
        if(Character.isLetter(s.charAt(j)))
        {
            for(; j > 0 && Character.isLetter(s.charAt(j - 1)); j--) { }
        }
        String s1 = s.substring(j, i);
        if(vocab.linkOfWord(s1) >= 0)
        {
            return ofLetters(vocab, s, j, intlist);
        }
        int k = vocab.intOfWord(s1);
        if(k == -1)
        {
            return null;
        } else
        {
            return ofLetters(vocab, s, j, new IntList(k, intlist));
        }
    }

    IntList(int i, IntList intlist)
    {
        hd = i;
        tl = intlist;
    }
}
