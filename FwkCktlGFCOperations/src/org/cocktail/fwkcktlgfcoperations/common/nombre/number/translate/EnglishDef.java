package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


// Referenced classes of package nombre:
//            Vocab, LanguageDef, Tree, IntList

public class EnglishDef
    implements LanguageDef
{

    private int language;
    private int countries[];
    private Vocab vocab;

    private void init()
    {
        language = 1;
        countries = new int[1];
        countries[0] = 3;
        vocab = new Vocab();
        vocab.addWord(1001, "zero", 0);
        vocab.addWord(1001, "one", 1);
        vocab.addWord(1001, "two", 2);
        vocab.addWord(1001, "three", 3);
        vocab.addWord(1001, "four", 4);
        vocab.addWord(1001, "five", 5);
        vocab.addWord(1001, "six", 6);
        vocab.addWord(1001, "seven", 7);
        vocab.addWord(1001, "eight", 8);
        vocab.addWord(1001, "nine", 9);
        vocab.addWord(1001, "ten", 10);
        vocab.addWord(1001, "eleven", 11);
        vocab.addWord(1001, "twelve", 12);
        vocab.addWord(1001, "thirteen", 13);
        vocab.addWord(1001, "fourteen", 14);
        vocab.addWord(1001, "fifteen", 15);
        vocab.addWord(1001, "sixteen", 16);
        vocab.addWord(1001, "seventeen", 17);
        vocab.addWord(1001, "eighteen", 18);
        vocab.addWord(1001, "nineteen", 19);
        vocab.addWord(1001, "twenty", 20);
        vocab.addWord(1001, "thirty", 30);
        vocab.addWord(1001, "forty", 40);
        vocab.addWord(1001, "fifty", 50);
        vocab.addWord(1001, "sixty", 60);
        vocab.addWord(1001, "seventy", 70);
        vocab.addWord(1001, "eighty", 80);
        vocab.addWord(1001, "ninety", 90);
        vocab.addWord(1001, "hundred", 100);
        vocab.addWord(1001, "thousand", 1000);
        vocab.addWord(1001, "million", 0xf4240);
        vocab.addWord(1001, "billion", 0x3b9aca00);
        vocab.addWord(1003, "first", 1);
        vocab.addWord(1003, "second", 2);
        vocab.addWord(1003, "third", 3);
        vocab.addWord(1003, "fourth", 4);
        vocab.addWord(1003, "fifth", 5);
        vocab.addWord(1003, "sixth", 6);
        vocab.addWord(1003, "seventh", 7);
        vocab.addWord(1003, "eighth", 8);
        vocab.addWord(1003, "ninth", 9);
        vocab.addWord(1003, "tenth", 10);
        vocab.addWord(1003, "eleventh", 11);
        vocab.addWord(1003, "twelfth", 12);
        vocab.addWord(1003, "thirteenth", 13);
        vocab.addWord(1003, "fourteenth", 14);
        vocab.addWord(1003, "fifteenth", 15);
        vocab.addWord(1003, "sixteenth", 16);
        vocab.addWord(1003, "seventeenth", 17);
        vocab.addWord(1003, "eighteenth", 18);
        vocab.addWord(1003, "nineteenth", 19);
        vocab.addWord(1003, "twentieth", 20);
        vocab.addWord(1003, "thirtieth", 30);
        vocab.addWord(1003, "fortieth", 40);
        vocab.addWord(1003, "fiftieth", 50);
        vocab.addWord(1003, "sixtieth", 60);
        vocab.addWord(1003, "seventieth", 70);
        vocab.addWord(1003, "eightieth", 80);
        vocab.addWord(1003, "ninetieth", 90);
        vocab.addWord(1003, "hundredth", 100);
        vocab.addWord(1003, "thousandth", 1000);
        vocab.addWord(1003, "millionth", 0xf4240);
        vocab.addWord(1003, "billionth", 0x3b9aca00);
        vocab.addWord(0, "nought", 0);
        vocab.addWord(0, "a", 1);
        vocab.addWord(0, "an", 1);
        vocab.addLanguage(0, "French");
        vocab.addLanguage(1, "English");
        vocab.addLanguage(2, "Spanish");
        vocab.addCountry(1, "Belgium");
        vocab.addCountry(0, "France");
        vocab.addCountry(2, "Swiss");
        vocab.addCountry(3, "UK");
        vocab.addCountry(4, "Spain");
        vocab.addMode(0, "Cardinal");
        vocab.addMode(1, "Ordinal");
        vocab.addMode(2, "Date");
        vocab.addLink(1, "and");
        vocab.addLink(2, "of");
        vocab.addLink(3, "-");
        vocab.addLink(4, " ");
        vocab.addLink(5, " and ");
        vocab.addLink(6, "-and-");
        vocab.addLink(7, " of ");
        vocab.addMisc("translate", "Translate");
        vocab.addMisc("reset", "Reset");
        vocab.addMisc("import", "Import");
    }

    public int getLanguage()
    {
        return language;
    }

    public int[] getCountries()
    {
        return countries;
    }

    public Vocab getVocab()
    {
        return vocab;
    }

    private Tree decompPlus(int i, int j, long l, int k)
    {
        Tree tree = decomp(i, j, (l / (long)k) * (long)k);
        if(l % (long)k != 0L)
        {
            tree = Tree.newPlus(tree, decomp(i, j, l % (long)k));
        }
        return tree;
    }

    private Tree decompMult(int i, int j, long l, int k)
    {
        Tree tree = Tree.newNum(k);
        tree = Tree.newMult(decomp(i, j, l / (long)k), tree);
        if(l % (long)k != 0L)
        {
            tree = Tree.newPlus(tree, decomp(i, j, l % (long)k));
        }
        return tree;
    }

    private Tree decomp(int i, int j, long l)
    {
        if(l <= 19L)
        {
            return Tree.newNum(l);
        }
        if(l < 100L && l % 10L == 0L)
        {
            return Tree.newNum(l);
        }
        if(l < 100L)
        {
            return decompPlus(i, j, l, 10);
        }
        if(l < 1000L)
        {
            return decompMult(i, j, l, 100);
        }
        if(l < 0xf4240L)
        {
            return decompMult(i, j, l, 1000);
        }
        if(l < 0x3b9aca00L)
        {
            return decompMult(i, j, l, 0xf4240);
        } else
        {
            return decompMult(i, j, l, 0x3b9aca00);
        }
    }

    public Tree ofLong(int i, int j, long l)
    {
        Tree tree;
        if(j == 2 && l >= 1000L && l < 2000L)
        {
            tree = decompMult(i, j, l, 100);
        } else
        {
            tree = decomp(i, j, l);
        }
        doLink(i, j, tree);
        return tree;
    }

    private Tree insertNum(Tree tree, long l)
    {
        switch(tree.oper)
        {
        case 1: // '\001'
            if(tree.left.value > l)
            {
                return Tree.newPlus(tree.left, insertNum(tree.right, l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }

        case 0: // '\0'
            if(tree.value > l)
            {
                return Tree.newPlus(tree, Tree.newNum(l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }

        case 2: // '\002'
            if(tree.value > l)
            {
                return Tree.newPlus(tree, Tree.newNum(l));
            } else
            {
                return Tree.newMult(tree, Tree.newNum(l));
            }
        }
        return null;
    }

    private Tree ofIntList(Tree tree, IntList intlist)
    {
        if(intlist == null)
        {
            return tree;
        } else
        {
            return ofIntList(insertNum(tree, intlist.hd), intlist.tl);
        }
    }

    private Tree ofIntList(IntList intlist)
    {
        return ofIntList(Tree.newNum(intlist.hd), intlist.tl);
    }

    public Tree ofLetters(String s)
    {
        IntList intlist = IntList.ofLetters(vocab, s);
        if(intlist == null)
        {
            return null;
        }
        if(intlist.tl == null || intlist.hd < 10 || intlist.hd > 19)
        {
            return ofIntList(intlist);
        }
        Tree tree = ofIntList(intlist.tl);
        if(tree.value > 99L)
        {
            return ofIntList(intlist);
        } else
        {
            return Tree.newPlus(Tree.newMult(Tree.newNum(intlist.hd), Tree.newNum(100L)), tree);
        }
    }

    private void doLink(int i, int j, Tree tree)
    {
        link(tree);
        if(j == 1)
        {
            linkOrdinal(tree);
        }
    }

    private void link(Tree tree)
    {
        switch(tree.oper)
        {
        default:
            break;

        case 1: // '\001'
            link(tree.left);
            link(tree.right);
            if(tree.left.value < 100L)
            {
                tree.link = 3;
                break;
            }
            if(tree.left.matchMultNum(1000L))
            {
                tree.link = 4;
            } else
            {
                tree.link = 5;
            }
            break;

        case 0: // '\0'
            link(tree.left);
            link(tree.right);
            tree.link = 4;
            break;

        case 2: // '\002'
            tree.link = 1001;
            break;
        }
    }

    private void linkOrdinal(Tree tree)
    {
        switch(tree.oper)
        {
        case 0: // '\0'
        case 1: // '\001'
            linkOrdinal(tree.right);
            break;

        case 2: // '\002'
            tree.link = 1003;
            break;
        }
    }

    public String toLetters(int i, int j, Tree tree)
    {
        if(j == 2 && tree.oper == 1 && tree.left.matchMultNum(100L) && tree.left.value >= 1000L && tree.left.value < 2000L && tree.right.value < 100L)
        {
            return toLetters(tree.left.left) + vocab.wordOfLink(4) + toLetters(tree.right);
        } else
        {
            return toLetters(tree);
        }
    }

    private String toLetters(Tree tree)
    {
        switch(tree.oper)
        {
        case 0: // '\0'
        case 1: // '\001'
            return toLetters(tree.left) + vocab.wordOfLink(tree.link) + toLetters(tree.right);

        case 2: // '\002'
            if(tree.value > 0x3b9aca00L)
            {
                return Long.toString(tree.value);
            }
            String s = vocab.wordOfInt(tree.link, (int)tree.value);
            if(s == null)
            {
                return Long.toString(tree.value);
            } else
            {
                return s;
            }
        }
        return null;
    }

    public EnglishDef()
    {
        init();
    }
}
