package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


public class Link
{

    static final int AND = 1;
    static final int OF = 2;
    static final int HYPHEN = 3;
    static final int SPACE = 4;
    static final int SPACE_AND_SPACE = 5;
    static final int HYPHEN_AND_HYPHEN = 6;
    static final int SPACE_OF_SPACE = 7;
    static final int SINGULAR = 1001;
    static final int PLURAL = 1002;
    static final int ORDINAL = 1003;
    static final int EXTRA1 = 1004;

    public Link()
    {
    }
}
