package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


public class Tree
{

    static final int MULT = 0;
    static final int PLUS = 1;
    static final int NUM = 2;
    int oper;
    long value;
    int link;
    Tree left;
    Tree right;

    static Tree newPlus(Tree tree, Tree tree1)
    {
        return new Tree(1, tree.value + tree1.value, 0, tree, tree1);
    }

    static Tree newMult(Tree tree, Tree tree1)
    {
        return new Tree(0, tree.value * tree1.value, 0, tree, tree1);
    }

    static Tree newNum(long l)
    {
        return new Tree(2, l, 0, null, null);
    }

    boolean matchNum(long l)
    {
        return oper == 2 && value == l;
    }

    boolean matchPlus()
    {
        return oper == 1;
    }

    boolean matchMult()
    {
        return oper == 0;
    }

    boolean matchMultNum(long l)
    {
        return oper == 0 && right.matchNum(l);
    }

    boolean matchNumMultNum(long l, long l1)
    {
        return oper == 0 && left.matchNum(l) && right.matchNum(l1);
    }

    long toLong()
    {
        return value;
    }

    String toArith()
    {
        switch(oper)
        {
        case 1: // '\001'
            if(left.matchMult() || right.matchMult())
            {
                return left.toArith() + " +" + right.toArith();
            } else
            {
                return left.toArith() + "+" + right.toArith();
            }

        case 0: // '\0'
            if(left.matchPlus() && right.matchPlus())
            {
                return "(" + left.toArith() + ")*(" + right.toArith() + ")";
            }
            if(left.matchPlus())
            {
                return "(" + left.toArith() + ")*" + right.toArith();
            }
            if(right.matchPlus())
            {
                return left.toArith() + "*(" + right.toArith() + ")";
            } else
            {
                return left.toArith() + "*" + right.toArith();
            }

        case 2: // '\002'
            if(value == 0xf4240L)
            {
                return "10^6";
            }
            if(value == 0x3b9aca00L)
            {
                return "10^9";
            } else
            {
                return Long.toString(value);
            }
        }
        return null;
    }

    long pivot()
    {
        switch(oper)
        {
        case 2: // '\002'
            return value;

        case 1: // '\001'
            return left.pivot();

        case 0: // '\0'
            return right.pivot();
        }
        return 0L;
    }

    private Tree(int i, long l, int j, Tree tree, Tree tree1)
    {
        oper = i;
        value = l;
        link = j;
        left = tree;
        right = tree1;
    }
}
