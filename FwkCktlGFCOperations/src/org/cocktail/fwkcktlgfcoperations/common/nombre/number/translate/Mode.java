package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


public class Mode
{

    public static final int CARDINAL = 0;
    public static final int ORDINAL = 1;
    public static final int DATE = 2;
    public static final int NBR = 3;

    public Mode()
    {
    }
}
