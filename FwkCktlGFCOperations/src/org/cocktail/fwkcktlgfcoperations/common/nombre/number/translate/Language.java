package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;


public class Language
{

    public static final int FRENCH = 0;
    public static final int ENGLISH = 1;
    public static final int SPANISH = 2;
    public static final int NBR = 3;

    public Language()
    {
    }
}
