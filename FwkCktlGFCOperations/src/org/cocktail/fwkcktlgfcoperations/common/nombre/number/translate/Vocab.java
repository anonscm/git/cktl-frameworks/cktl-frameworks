package org.cocktail.fwkcktlgfcoperations.common.nombre.number.translate;

import java.util.Hashtable;

public class Vocab
{

    private Hashtable int_to_word_singular;
    private Hashtable int_to_word_plural;
    private Hashtable int_to_word_ordinal;
    private Hashtable int_to_word_extra1;
    private Hashtable language_to_word;
    private Hashtable country_to_word;
    private Hashtable mode_to_word;
    private Hashtable link_to_word;
    private Hashtable misc_to_word;
    private Hashtable word_to_int;
    private Hashtable word_to_link;

    int intOfWord(String s)
    {
        Integer integer = (Integer)word_to_int.get(s);
        if(integer == null)
        {
            return -1;
        } else
        {
            return integer.intValue();
        }
    }

    int linkOfWord(String s)
    {
        Integer integer = (Integer)word_to_link.get(s);
        if(integer == null)
        {
            return -1;
        } else
        {
            return integer.intValue();
        }
    }

    String wordOfInt(int i, int j)
    {
        Integer integer = new Integer(j);
        String s;
        switch(i)
        {
        case 1001: 
            s = (String)int_to_word_singular.get(integer);
            break;

        case 1002: 
            s = (String)int_to_word_plural.get(integer);
            break;

        case 1003: 
            s = (String)int_to_word_ordinal.get(integer);
            break;

        case 1004: 
            s = (String)int_to_word_extra1.get(integer);
            break;

        default:
            s = null;
            break;
        }
        if(s == null)
        {
            return String.valueOf(j);
        } else
        {
            return s;
        }
    }

    String wordOfLanguage(int i)
    {
        Integer integer = new Integer(i);
        String s = (String)language_to_word.get(integer);
        if(s == null)
        {
            return "-";
        } else
        {
            return s;
        }
    }

    String wordOfCountry(int i)
    {
        Integer integer = new Integer(i);
        String s = (String)country_to_word.get(integer);
        if(s == null)
        {
            return "-";
        } else
        {
            return s;
        }
    }

    String wordOfMode(int i)
    {
        Integer integer = new Integer(i);
        String s = (String)mode_to_word.get(integer);
        if(s == null)
        {
            return "-";
        } else
        {
            return s;
        }
    }

    String wordOfLink(int i)
    {
        Integer integer = new Integer(i);
        String s = (String)link_to_word.get(integer);
        if(s == null)
        {
            return ".";
        } else
        {
            return s;
        }
    }

    String wordOfMisc(String s)
    {
        String s1 = (String)misc_to_word.get(s);
        if(s1 == null)
        {
            return s;
        } else
        {
            return s1;
        }
    }

    static char suppressAccent(char c)
    {
        switch(c)
        {
        case 224: 
        case 225: 
        case 226: 
        case 227: 
        case 228: 
            return 'A';

        case 231: 
            return 'c';

        case 232: 
        case 233: 
        case 234: 
        case 235: 
            return 'e';

        case 236: 
        case 237: 
        case 238: 
        case 239: 
            return 'i';

        case 241: 
            return 'n';

        case 242: 
        case 243: 
        case 244: 
        case 245: 
        case 246: 
            return 'o';

        case 249: 
        case 250: 
        case 251: 
        case 252: 
            return 'u';

        case 253: 
        case 255: 
            return 'y';

        case 229: 
        case 230: 
        case 240: 
        case 247: 
        case 248: 
        case 254: 
        default:
            return c;
        }
    }

    static String suppressAccent(String s)
    {
        char ac[] = s.toCharArray();
        for(int i = 0; i < ac.length; i++)
        {
            ac[i] = suppressAccent(ac[i]);
        }

        return String.valueOf(ac);
    }

    void addWord(int i, String s, int j)
    {
        Integer integer = new Integer(j);
        word_to_int.put(s, integer);
        word_to_int.put(suppressAccent(s), integer);
        switch(i)
        {
        case 1001: 
            int_to_word_singular.put(integer, s);
            break;

        case 1002: 
            int_to_word_plural.put(integer, s);
            break;

        case 1003: 
            int_to_word_ordinal.put(integer, s);
            break;

        case 1004: 
            int_to_word_extra1.put(integer, s);
            break;
        }
    }

    void addLanguage(int i, String s)
    {
        Integer integer = new Integer(i);
        language_to_word.put(integer, s);
    }

    void addCountry(int i, String s)
    {
        Integer integer = new Integer(i);
        country_to_word.put(integer, s);
    }

    void addMode(int i, String s)
    {
        Integer integer = new Integer(i);
        mode_to_word.put(integer, s);
    }

    void addLink(int i, String s)
    {
        Integer integer = new Integer(i);
        word_to_link.put(s, integer);
        word_to_link.put(suppressAccent(s), integer);
        link_to_word.put(integer, s);
    }

    void addMisc(String s, String s1)
    {
        misc_to_word.put(s, s1);
    }

    Vocab()
    {
        int_to_word_singular = new Hashtable();
        int_to_word_plural = new Hashtable();
        int_to_word_ordinal = new Hashtable();
        int_to_word_extra1 = new Hashtable();
        language_to_word = new Hashtable();
        country_to_word = new Hashtable();
        mode_to_word = new Hashtable();
        link_to_word = new Hashtable();
        misc_to_word = new Hashtable();
        word_to_link = new Hashtable();
        Integer integer = new Integer(0);
        word_to_link.put(" ", integer);
        word_to_link.put("\n", integer);
        word_to_link.put("\t", integer);
        word_to_link.put("\f", integer);
        word_to_link.put("\r", integer);
        word_to_int = new Hashtable();
    }
}
