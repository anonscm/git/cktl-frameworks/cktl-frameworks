package org.cocktail.fwkcktlgfcoperations.common.i18n;

public final class CodeMessage {
	
	public static final String BUD_ERR_BUDPOS = "budget.DroitBUDPOSAbsent";
	public static final String BUD_ERR_TRANCHE_VALIDEE = "budget.ActionAnnuleeCarTrancheValidee";
	public static final String BUD_ERR_TRANCHE_VERROUILLEE = "budget.ActionAnnuleeCarTrancheVerrouillee";
	public static final String BUD_ERR_TRANCHE_INTEGREE = "budget.SuppAnnuleeCarTrancheIntegree";
	public static final String BUD_ERR_INCOHERENCE_AE_CP = "budget.IncoherenceAeCp";
}
