/*
 * Created on 3 nov. 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package org.cocktail.fwkcktlgfcoperations.common.debug;

import java.io.PrintStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;


/**
 * Methodes utilitaires pour le debugging
 */
public class Debug 
{
	/**
	 * Imprime l'etat courant de la pile d'execution des methodes
	 * @param throwable La "throwable" source
	 * @param profondeur Nombre d'elements de la pile a considerer
	 * @param packagePath Seuls les elements de la pile qui contiennent cette chaine de caracteres sont retenus
	 * 							Par exemple: "fr.univlr.cri.convention.client"
	 * @param out Flux de sortie (par exemple: System.out)
	 */
	public static void printCurrentStackTrace(Throwable throwable, String packagePath, PrintStream out) {
		Throwable th;
		if (throwable == null) th = new Throwable();
		else th = throwable;
		
		//System.out.println("packagePath = "+packagePath);
		
		Object[] stackTraceElements = th.getStackTrace();
		
		StringBuffer buf = new StringBuffer();
		buf.append("DEBUG [");
		buf.append(new Date().toString());
		buf.append("]: CurrentStackTrace :\n");
		for (int i=0; i<stackTraceElements.length; i++) {
			String str = stackTraceElements[i].toString();
			if (packagePath==null || str.indexOf(packagePath)>=0) {
				buf.append(str);
				buf.append("\n");
			}
		}
		out.println();
		out.println(buf.toString());
		out.println();
	}
	
	/**
	 * Impression d'un qualifier un peu plus facile a lire
	 * 
	 * Exemple :
	 * 
	 * ((conSuppr = 'N') and (exercice = (fr.univlr.cri.convention.client.Exercice)'2005') and ((avenants.avtDateFin >= 
	 * (com.webobjects.foundation.NSTimestamp)'2005-12-13 23:00:00 Etc/GMT') or (avenants.avtDateFinExec >= (com.webobj
	 * ects.foundation.NSTimestamp)'2005-12-13 23:00:00 Etc/GMT')) and ((agentCreation.individuUlr.repartStructures.str
	 * uctureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'DIRECTION DES SYSTEMES D\'INFORMATION') or (agentCreation.
	 * individuUlr.repartStructures.structureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'LHB') or (agentCreation.in
	 * dividuUlr.repartStructures.structureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'Applications (Soft)')))
	 * 
	 * ..devient :
	 * 
	 * (
	 * (conSuppr = 'N')
	 * and 
	 * (exercice = (fr.univlr.cri.convention.client.Exercice)'2005')
	 * and 
	 * (
	 * (avenants.avtDateFin >= (com.webobjects.foundation.NSTimestamp)'2005-12-13 23:00:00 Etc/GMT')
	 * or 
	 * (avenants.avtDateFinExec >= (com.webobjects.foundation.NSTimestamp)'2005-12-13 23:00:00 Etc/GMT')
	 * )
	 * and 
	 * (
	 * (agentCreation.individuUlr.repartStructures.structureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'DIRECTION D
	 * ES SYSTEMES D\'INFORMATION')
	 * or 
	 * (agentCreation.individuUlr.repartStructures.structureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'LHB')
	 * or 
	 * (agentCreation.individuUlr.repartStructures.structureUlr = (fr.univlr.cri.grhum.client.StructureUlr)'Application
	 * s (Soft)')
	 * )
	 * )
	 * 
	 * @param qualifier Le qualifier a imprimer
	 */
	public static void printQualifier(final String name, final EOQualifier qualifier) {
		System.out.println();
		if (qualifier == null) {
			System.out.println(name+" == null");
		}
		else {
			System.out.println(name+" :");
			System.out.println(qualifier.toString()
				.replaceAll("\\(\\(\\(\\(", "\\(\n\\(\\(\\(").replaceAll("\\)\\)\\)\\)", "\\)\n\\)\\)\\)")	//quatre parentheses de suite
				.replaceAll("\\(\\(\\(", "\\(\n\\(\\(").replaceAll("\\)\\)\\)", "\\)\n\\)\\)")	//trois parentheses de suite
				.replaceAll("\\(\\(", "\\(\n\\(").replaceAll("\\)\\)", "\\)\n\\)")	//deux parentheses de suite
				.replaceAll(" and ","\nand \n").replaceAll(" or ","\nor \n"));
		}
		System.out.println();
	}
	
	/**
	 * Affichage d'un NSArray
	 * @param name
	 * @param array
	 */
	public static void printArray(final String name, final NSArray array) {
		if (array == null) {
			System.out.println(name+" == null");
		}
		else {
			if (array.count() < 1) {
				System.out.println(name+" : vide");
			}
			else {
				System.out.println(name+" : ");
				for (int i=0; i<array.count(); i++) {
					System.out.println("\t["+i+"] : "+array.objectAtIndex(i));
				}
			}
		}
		System.out.println();
	}
	/**
	 * Affichage d'un NSArray
	 * @param name
	 * @param array
	 */
	public static void printArray(final String name, final NSArray array, boolean displayClass) {
		if (array == null) {
			System.out.println(name+" == null");
		}
		else {
			if (array.count() < 1) {
				System.out.println(name+" : vide");
			}
			else {
				System.out.println(name+" : ");
				for (int i=0; i<array.count(); i++) {
					System.out.print("\t["+i+"] : "+array.objectAtIndex(i));
					if (displayClass) System.out.print("  ("+array.objectAtIndex(i).getClass()+")");
					System.out.println();
				}
			}
		}
		System.out.println();
	}
	
	/**
	 * Affichage d'un EOGenericRecord
	 * @param name
	 * @param record
	 */
	public static void printRecord(final String name, final EOGenericRecord record) {
		System.out.println();
		if (record == null)
			System.out.println(name + " == null");
		else {
			System.out.println(name+" :");
			System.out.println("\t[class] : "+record.getClass());
			System.out.println("\t[editingContext] : "+record.editingContext());
			NSArray keys = record.allPropertyKeys();
			for (int i=0; i<keys.count(); i++) {
				String key = (String) keys.objectAtIndex(i);
				Object obj = record.valueForKey(key);
				System.out.print("\t[");
				System.out.print(key);
				System.out.print("] : ");
				System.out.println(obj);
			}
		}
		System.out.println();
	}
	
	/**
	 * Affichage d'un EOGenericRecord
	 * @param name
	 * @param record
	 */
	public static void printDate(final Date date) {
		System.out.println(new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss - z ('GMT'Z)").format(date));
	}
	
	/**
	 * Affichage d'une Map
	 * @param name
	 * @param map
	 */
	public static void printMap(final String name, final Map map) {
		System.out.println();
		if (map == null)
			System.out.println(name + " == null");
		else {
			System.out.println(name+" :");
			for (Iterator iter = map.keySet().iterator(); iter.hasNext(); ) {
				Object key = iter.next();
				Object obj = map.get(key);
				System.out.println("\t["+key+"] : "+obj);
			}
		}
		System.out.println();
	}
	
	/**
	 * Affichage d'un NSDictionary
	 * @param name
	 * @param map
	 */
	public static void printMap(final String name, final NSDictionary map) {
		System.out.println();
		if (map == null)
			System.out.println(name + " == null");
		else {
			System.out.println(name+" :");
			for (int i=0; i<map.allKeys().count(); i++) {
				String key = (String) map.allKeys().objectAtIndex(i);
				Object obj = map.valueForKey(key);
				System.out.println("\t["+key+"] : "+obj);
			}
		}
		System.out.println();
	}
	
	
	public static void main(String[] args) {
		Debug.printDate(new Date());
	}
}
