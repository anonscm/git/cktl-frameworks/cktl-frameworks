package org.cocktail.fwkcktlgfcoperations.common.date;

import static org.apache.commons.lang.Validate.*;

import java.util.Calendar;
import java.util.Date;

import lombok.Getter;
@Getter
public class PeriodeTemps {

	private Date debut;
	private Date fin;
	
	public PeriodeTemps(Date debut, Date fin) {
		isTrue(debut != null || fin != null, "Une période de temps doit avoir au moins une borne de définie");
		this.debut = debut;
		this.fin = fin;
	}
	
	public boolean contient(int annee) {
		if (debut != null) {
			int anneeDebut = extraireAnnee(debut);
			if (annee < anneeDebut) {
				return false;
			}
		}
		
		if (fin != null) {
			int anneeFin = extraireAnnee(fin);
			if (annee > anneeFin) {
				return false;
			}
		}
		
		return true;
	}
	
	private int extraireAnnee(Date date) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
	    int annee = cal.get(Calendar.YEAR);
	    return annee;
	}
	
}
