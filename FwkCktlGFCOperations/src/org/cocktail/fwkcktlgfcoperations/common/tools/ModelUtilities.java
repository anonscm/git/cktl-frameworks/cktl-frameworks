
package org.cocktail.fwkcktlgfcoperations.common.tools;

import org.cocktail.fwkcktlgfcoperations.common.debug.Debug;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionCompositePrimaryKey;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionNullPrimaryKey;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionTemporaryPrimaryKey;

import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOGlobalID;
import com.webobjects.eocontrol.EOKeyGlobalID;
import com.webobjects.eocontrol.EOTemporaryGlobalID;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ModelUtilities 
{
	
	
	/**
	 * Constructeur.
	 */
	public ModelUtilities() {
		
	}
	
	
	/**
	 * <p>Tente d'obtenir la cle primaire d'un objet d'entreprise. 
	 * <p>Utilisation du {@link EOGlobalID} de l'objet. C'est l'editing context de l'objet lui-meme qui est utilise.
	 * @param eo Objet d'entreprise.
	 * @return La cle primaire.
	 * @throws ExceptionNullPrimaryKey 
	 * @throws ExceptionTemporaryPrimaryKey 
	 * @throws ExceptionCompositePrimaryKey 
	 */
	public Object primaryKeyForObject(final EOGenericRecord eo) throws ExceptionNullPrimaryKey, ExceptionTemporaryPrimaryKey, ExceptionCompositePrimaryKey {
		
		if (eo == null) 
			throw new NullPointerException("Objet metier null interdit.");
		
		EOGlobalID gId = eo.editingContext().globalIDForObject(eo);
		
		if (gId == null) 
			throw new ExceptionNullPrimaryKey(
					"En utilisant l'editing context "+eo.editingContext()+
					", le global id de l'objet metier suivant est null : "+eo.toString());
		
		if (gId instanceof EOTemporaryGlobalID) 
			throw new ExceptionTemporaryPrimaryKey(
					"Le global id de l'objet metier suivant est temporaire (EOTemporaryGlobalID) : "+eo.toString());
		
		EOKeyGlobalID myGID = (EOKeyGlobalID) gId;

		if (myGID.keyValuesArray().count() > 1) {
			Debug.printArray("Cles primaires trouvees", myGID.keyValuesArray());
			throw new ExceptionCompositePrimaryKey(
					"Le global id de l'objet metier '"+eo.toString()+"' suivant est composite.");
		}
		
		Object pk = myGID.keyValuesArray().objectAtIndex(0);
		
		return pk;
	}
}
