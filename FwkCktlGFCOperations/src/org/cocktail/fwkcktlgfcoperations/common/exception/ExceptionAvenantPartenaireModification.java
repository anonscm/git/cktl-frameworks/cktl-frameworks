package org.cocktail.fwkcktlgfcoperations.common.exception;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class ExceptionAvenantPartenaireModification extends Exception 
{
	
	public ExceptionAvenantPartenaireModification(final String message) {
		super(message);
	}

	public ExceptionAvenantPartenaireModification(final String message, final Throwable cause) {
		super(message, cause);
	}


}
