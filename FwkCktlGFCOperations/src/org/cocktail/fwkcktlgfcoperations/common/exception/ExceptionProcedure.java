/**
 * TheJavaClientUtilities
 * bgauthie
 * 2007
 *
 */
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionProcedure extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionProcedure() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionProcedure(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionProcedure(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionProcedure(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
