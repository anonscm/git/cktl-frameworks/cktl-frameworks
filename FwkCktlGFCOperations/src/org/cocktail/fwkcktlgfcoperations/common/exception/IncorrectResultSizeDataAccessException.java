package org.cocktail.fwkcktlgfcoperations.common.exception;

public class IncorrectResultSizeDataAccessException extends RuntimeException {

	/**
	 * Constructor for IncorrectResultSizeDataAccessException.
	 * @param expectedSize the expected result size
	 */
	public IncorrectResultSizeDataAccessException(int expectedSize) {
		super("Incorrect result size: expected " + expectedSize);
	}

	
}
