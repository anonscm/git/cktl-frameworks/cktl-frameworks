
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionNullPrimaryKey extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionNullPrimaryKey() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionNullPrimaryKey(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionNullPrimaryKey(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionNullPrimaryKey(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
