
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionClientSideRequest extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionClientSideRequest() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionClientSideRequest(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionClientSideRequest(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionClientSideRequest(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
