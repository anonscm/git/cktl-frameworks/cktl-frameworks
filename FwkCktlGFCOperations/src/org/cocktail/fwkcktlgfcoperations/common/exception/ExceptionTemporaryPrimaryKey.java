
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionTemporaryPrimaryKey extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionTemporaryPrimaryKey() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionTemporaryPrimaryKey(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionTemporaryPrimaryKey(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionTemporaryPrimaryKey(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
