/**
 * TheJavaClientUtilities
 * bgauthie
 * 2007
 *
 */
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionNotFound extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionNotFound() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionNotFound(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionNotFound(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionNotFound(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
