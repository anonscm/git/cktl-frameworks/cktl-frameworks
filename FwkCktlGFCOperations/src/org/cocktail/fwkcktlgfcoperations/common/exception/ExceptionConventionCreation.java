
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionConventionCreation extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionConventionCreation() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionConventionCreation(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionConventionCreation(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionConventionCreation(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
