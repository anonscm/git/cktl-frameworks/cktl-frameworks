
package org.cocktail.fwkcktlgfcoperations.common.exception;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ExceptionContrainteViolee extends Exception {

	/**
	 * Constructeur.
	 */
	public ExceptionContrainteViolee() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 */
	public ExceptionContrainteViolee(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param cause
	 */
	public ExceptionContrainteViolee(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param message
	 * @param cause
	 */
	public ExceptionContrainteViolee(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
