package org.cocktail.fwkcktlgfcoperations.common.exception;


/**
 * 
 * @author Michael Haller, Consortium Cocktail, 2008
 *
 */
public class ExceptionFinder extends Exception 
{
	public ExceptionFinder(final String message) {
		super(message);
	}

	public ExceptionFinder(final String message, final Throwable cause) {
		super(message, cause);
	}
}
