package org.cocktail.fwkcktlgfcoperations.common.text;

import java.util.Vector;


/** 
 * Classe complementaire du J2SDK sur la manipulation de chaines de caract�res
 *  Permet notamment de supprimer les accents d'une chaine de caract�res
 */
public abstract class StringOperation 
{
	public static final int DEFAULT_LINE_LENGTH = 75;
	
	/** Index du 1er caractere accentu� **/
	private static final int MIN = 192;
	/** Index du dernier caractere accentu� **/
	private static final int MAX = 255;
	/** Vecteur de correspondance entre accent / sans accent **/
	private static final Vector map = initMap();
	
	/**
	 * Retourne une chaine dont les lettres accentues sont remplacees par leur equivalent non accentue, 
	 * puis eventuellement transformees en majuscules.
	 * @param str Chaine d'entree.
	 * @param toUpperCase Mettre a <code>true</code> pour mettre les lettres en majuscules apres remplacement des lettres accentuees.
	 * @return La chaine resultat. <code>null</code> si la chaine d'entree est <code>null</code>.
	 */
	static public String removeAccents(final String str, final boolean toUpperCase) {
		
		if (str == null) return null;
		
		String s = str;
		s = s.replaceAll("[\u00E8\u00E9\u00EA\u00EB]",					"e");
		s = s.replaceAll("[\u00E9\u00FA\u00FB]",						"u");
		s = s.replaceAll("[\u00EC\u00ED\u00EE\u00EF]",					"i");
		s = s.replaceAll("[\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5]",		"a");
		s = s.replaceAll("[\u00E6]",									"ae");
		s = s.replaceAll("[\u00F2\u00F3\u00F4\u00F5\u00F6]",			"o");
		s = s.replaceAll("[\u00E7]",									"c");
		s = s.replaceAll("[\u00F1]",									"n");
		s = s.replaceAll("[\u00FD\u00FF]",								"y");
		s = s.replaceAll("[\u00C8\u00C9\u00CA\u00CB]",					"E");
		s = s.replaceAll("[\u00D9\u00DA\u00DB\u00DC]",					"U");
		s = s.replaceAll("[\u00CC\u00CD\u00CE\u00CF]",					"I");
		s = s.replaceAll("[\u00C0\u00C1\u00C2\u00C3\u00C4\u00C5]",		"A");
		s = s.replaceAll("[\u00C6]",									"AE");
		s = s.replaceAll("[\u00D2\u00D3\u00D4\u00D5\u00D6]",			"O");
		s = s.replaceAll("[\u00C7]",									"C");
		s = s.replaceAll("[\u00D1]",									"N");
		s = s.replaceAll("[\u00DD]",									"Y");
		
//		s = s.replaceAll("[����]","e");
//		s = s.replaceAll("[��]","u");
//		s = s.replaceAll("[��]","i");
//		s = s.replaceAll("[��]","a");
//		s = s.replaceAll("�","o");
//		s = s.replaceAll("�","c");
//		s = s.replaceAll("[����]","E");
//		s = s.replaceAll("[��]","U");
//		s = s.replaceAll("[��]","I");
//		s = s.replaceAll("[��]","A");
//		s = s.replaceAll("�","O");
//		s = s.replaceAll("�","C");

		if (toUpperCase) s = s.toUpperCase();
		
		return s;
	}
	/**
	 * Retourne une chaine dont les lettres accentues sont remplacees par leur equivalent non accentue, 
	 * puis eventuellement transformees en majuscules.
	 * @param str Chaine d'entree.
	 * @param replaceBy Chaine de caracetres par laquelle les lettres accentuees doivent etre remplacees
	 * @return La chaine resultat. <code>null</code> si la chaine d'entree est <code>null</code>. 
	 */
	static public String replaceAccents(final String str, final String replaceBy) {
		
		if (str == null) return null;
		if (replaceBy == null) return str;
		
		String s = str;
		s = s.replaceAll("[\u00E8\u00E9\u00EA\u00EB]",					replaceBy);
		s = s.replaceAll("[\u00E9\u00FA\u00FB]",						replaceBy);
		s = s.replaceAll("[\u00EC\u00ED\u00EE\u00EF]",					replaceBy);
		s = s.replaceAll("[\u00E0\u00E1\u00E2\u00E3\u00E4\u00E5]",		replaceBy);
		s = s.replaceAll("[\u00E6]",									replaceBy);
		s = s.replaceAll("[\u00F2\u00F3\u00F4\u00F5\u00F6]",			replaceBy);
		s = s.replaceAll("[\u00E7]",									replaceBy);
		s = s.replaceAll("[\u00F1]",									replaceBy);
		s = s.replaceAll("[\u00FD\u00FF]",								replaceBy);
		s = s.replaceAll("[\u00C8\u00C9\u00CA\u00CB]",					replaceBy);
		s = s.replaceAll("[\u00D9\u00DA\u00DB\u00DC]",					replaceBy);
		s = s.replaceAll("[\u00CC\u00CD\u00CE\u00CF]",					replaceBy);
		s = s.replaceAll("[\u00C0\u00C1\u00C2\u00C3\u00C4\u00C5]",		replaceBy);
		s = s.replaceAll("[\u00C6]",									replaceBy);
		s = s.replaceAll("[\u00D2\u00D3\u00D4\u00D5\u00D6]",			replaceBy);
		s = s.replaceAll("[\u00C7]",									replaceBy);
		s = s.replaceAll("[\u00D1]",									replaceBy);
		s = s.replaceAll("[\u00DD]",									replaceBy);
		
		return s;
	}
	
	/** 
	 * Initialisation du tableau de correspondance entre les caract�res accentu�s
	 * et leur homologues non accentu�s 
	 */
	private static Vector initMap() {  
		Vector result = new Vector();
		java.lang.String car = null;
		
		car = new java.lang.String("A");
		result.add( car );            /* '\u00C0'   �   alt-0192  */ 
		result.add( car );            /* '\u00C1'   �   alt-0193  */
		result.add( car );            /* '\u00C2'   �   alt-0194  */
		result.add( car );            /* '\u00C3'   �   alt-0195  */
		result.add( car );            /* '\u00C4'   �   alt-0196  */
		result.add( car );            /* '\u00C5'   �   alt-0197  */
		car = new java.lang.String("AE");
		result.add( car );            /* '\u00C6'   �   alt-0198  */
		car = new java.lang.String("C");
		result.add( car );            /* '\u00C7'   �   alt-0199  */
		car = new java.lang.String("E");
		result.add( car );            /* '\u00C8'   �   alt-0200  */
		result.add( car );            /* '\u00C9'   �   alt-0201  */
		result.add( car );            /* '\u00CA'   �   alt-0202  */
		result.add( car );            /* '\u00CB'   �   alt-0203  */
		car = new java.lang.String("I");
		result.add( car );            /* '\u00CC'   �   alt-0204  */
		result.add( car );            /* '\u00CD'   �   alt-0205  */
		result.add( car );            /* '\u00CE'   �   alt-0206  */
		result.add( car );            /* '\u00CF'   �   alt-0207  */
		car = new java.lang.String("D");
		result.add( car );            /* '\u00D0'   ?   alt-0208  */
		car = new java.lang.String("N");
		result.add( car );            /* '\u00D1'   �   alt-0209  */
		car = new java.lang.String("O");
		result.add( car );            /* '\u00D2'   �   alt-0210  */
		result.add( car );            /* '\u00D3'   �   alt-0211  */
		result.add( car );            /* '\u00D4'   �   alt-0212  */
		result.add( car );            /* '\u00D5'   �   alt-0213  */
		result.add( car );            /* '\u00D6'   �   alt-0214  */
		car = new java.lang.String("*");
		result.add( car );            /* '\u00D7'   ?   alt-0215  */
		car = new java.lang.String("0");
		result.add( car );            /* '\u00D8'   �   alt-0216  */
		car = new java.lang.String("U");
		result.add( car );            /* '\u00D9'   �   alt-0217  */
		result.add( car );            /* '\u00DA'   �   alt-0218  */
		result.add( car );            /* '\u00DB'   �   alt-0219  */
		result.add( car );            /* '\u00DC'   �   alt-0220  */
		car = new java.lang.String("Y");
		result.add( car );            /* '\u00DD'   ?   alt-0221  */
		car = new java.lang.String("?");
		result.add( car );            /* '\u00DE'   ?   alt-0222  */
		car = new java.lang.String("B");
		result.add( car );            /* '\u00DF'   �   alt-0223  */
		car = new java.lang.String("a");
		result.add( car );            /* '\u00E0'   �   alt-0224  */
		result.add( car );            /* '\u00E1'   �   alt-0225  */
		result.add( car );            /* '\u00E2'   �   alt-0226  */
		result.add( car );            /* '\u00E3'   �   alt-0227  */
		result.add( car );            /* '\u00E4'   �   alt-0228  */
		result.add( car );            /* '\u00E5'   �   alt-0229  */
		car = new java.lang.String("ae");
		result.add( car );            /* '\u00E6'   �   alt-0230  */
		car = new java.lang.String("c");
		result.add( car );            /* '\u00E7'   �   alt-0231  */
		car = new java.lang.String("e");
		result.add( car );            /* '\u00E8'   �   alt-0232  */
		result.add( car );            /* '\u00E9'   �   alt-0233  */
		result.add( car );            /* '\u00EA'   �   alt-0234  */
		result.add( car );            /* '\u00EB'   �   alt-0235  */
		car = new java.lang.String("i");
		result.add( car );            /* '\u00EC'   �   alt-0236  */
		result.add( car );            /* '\u00ED'   �   alt-0237  */
		result.add( car );            /* '\u00EE'   �   alt-0238  */
		result.add( car );            /* '\u00EF'   �   alt-0239  */
		car = new java.lang.String("d");
		result.add( car );            /* '\u00F0'   ?   alt-0240  */
		car = new java.lang.String("n");
		result.add( car );            /* '\u00F1'   �   alt-0241  */
		car = new java.lang.String("o");
		result.add( car );            /* '\u00F2'   �   alt-0242  */
		result.add( car );            /* '\u00F3'   �   alt-0243  */
		result.add( car );            /* '\u00F4'   �   alt-0244  */
		result.add( car );            /* '\u00F5'   �   alt-0245  */
		result.add( car );            /* '\u00F6'   �   alt-0246  */
		car = new java.lang.String("/");
		result.add( car );            /* '\u00F7'   �   alt-0247  */
		car = new java.lang.String("0");
		result.add( car );            /* '\u00F8'   �   alt-0248  */
		car = new java.lang.String("u");
		result.add( car );            /* '\u00F9'   �   alt-0249  */
		result.add( car );            /* '\u00FA'   �   alt-0250  */
		result.add( car );            /* '\u00FB'   �   alt-0251  */
		result.add( car );            /* '\u00FC'   �   alt-0252  */
		car = new java.lang.String("y");
		result.add( car );            /* '\u00FD'   ?   alt-0253  */
		car = new java.lang.String("?");
		result.add( car );            /* '\u00FE'   ?   alt-0254  */
		car = new java.lang.String("y");
		result.add( car );            /* '\u00FF'   �   alt-0255  */
		result.add( car );            /* '\u00FF'       alt-0255  */
		
		return result;
	}
	
	/** 
	 * Transforme une chaine pouvant contenir des accents dans une version sans accent
	 *  @param chaine Chaine a convertir sans accent
	 *  @return Chaine dont les accents ont �t� supprim�s
	 **/
	public static java.lang.String sansAccent(java.lang.String chaine) {
		java.lang.StringBuffer result  = new StringBuffer(chaine);
	
		for(int bcl = 0 ; bcl < result.length() ; bcl++) {
			int carVal = chaine.charAt(bcl);
			if (carVal>=MIN && carVal<=MAX) {  // Remplacement
				java.lang.String newVal = (java.lang.String)map.get( carVal - MIN );
				result.replace(bcl, bcl+1,newVal);
			}
		}
		return result.toString();
	}
	
	/**
	 * <p>A partir d'une chaine de caracteres, fournit une chaine de caracteres pouvant servir de motif de 
	 * recherche textuelle en base.
	 * <p>Exemple : "*service*intern*" est produit a partir de "service intern". 
	 * @param str La chaine de caractere d'entree.
	 * @param replaceAccents Les lettres accentuees sont egalement remplacees par une "*" si ce parametre est a <code>true</code>.
	 * @return Le motif correspondant, null si la chaine de caractere specifiee est null.
	 */
	public static String makePatternForSearching(final String str, final boolean replaceAccents) {
		
		if (str == null) return null;
		if ("".equals(str)) return "*";
		if ("*".equals(str)) return "*";
		
		String tmp = str.trim();
		if (replaceAccents)
			tmp = replaceAccents(tmp, "*");
			
		StringBuffer buf = new StringBuffer();
		buf.append("*");
		buf.append(tmp.replaceAll(" ", "*"));
		buf.append("*");
		
		return buf.toString();
	}
	/**
	 * <p>A partir d'une chaine de caracteres, fournit une chaine de caracteres pouvant servir de motif de 
	 * recherche textuelle en base.
	 * <p>Exemple : "*service*intern*" est produit a partir de "service intern". 
	 * Les lettres accentuees sont egalement remplacees par une "*".
	 * @param str La chaine de caractere d'entree.
	 * @return Le motif correspondant, null si la chaine de caractere specifiee est null.
	 */
	public static String makePatternForSearching(final String str) {
		
		return makePatternForSearching(str, true);
	}
	
	/**
	 * Produit a partir d'une chaine d'entree (mono ou multi-ligne) une chaine multi-ligne dont 
	 * chaque ligne a approximativement une longueur maximum voulue. 
	 * @param str Chaine d'entree (les sauts de ligne existants sont respectes).
	 * @param length Longueur approximative voulue (en nombre de caracteres) de chaque ligne.
	 * Un mot ne sera coupe en 2 que si aucun espace n'est trouve avant d'atteindre la longueur maximum.
	 * @return La chaine mutli-ligne dont chaque ligne a approximativement la longueur maximum voulue.
	 */
	public static String multiline(final String str, final int length) {
		if (str == null) return null;
				
		StringBuffer buf = new StringBuffer();
		int endIndex=length;
		String substr = str.substring(0);

		while (substr.length() >= length) {
			// on respecte chaque saut de ligne rencontre
			if (substr.indexOf("\n")!=-1 && substr.indexOf("\n")<length) {
				endIndex = substr.indexOf("\n") + 1;
				buf.append(substr.substring(0, endIndex));
				substr = substr.substring(endIndex);
			}
			else {
				endIndex = substr.indexOf(" ", length) + 1;
				if (endIndex <= 0) endIndex = length;	// pas d'espace trouve
				buf.append(substr.substring(0, endIndex));
				buf.append("\n");
				substr = substr.substring(endIndex);
			}
		}
		buf.append(substr);
		
		return buf.toString();
	}
	/**
	 * Produit a partir d'une chaine d'entree (mono ou multi-ligne) une chaine multi-ligne dont 
	 * chaque ligne a approximativement une longueur par defaut.
	 * @param str Chaine d'entree (les sauts de ligne existants sont respectes).
	 * @return La chaine mutli-ligne dont chaque ligne a approximativement la longueur maximum par defaut.
	 */
	public static String multiline(final String str) {
		return multiline(str, DEFAULT_LINE_LENGTH);
	}
	
    /**
     * Renvoie une chaine reformattee pour affichage dans des alertes javascript
     */
	public static String getMessageFormatte(String message) {    	
		if (message != null) {
			message = message.replaceAll("\n", " ");
			message = message.replaceAll("\"", "''");
			
			int index = message.indexOf("ORA-20001:");
			if (index > -1) {
				message = message.substring(index+10);
				index = message.indexOf("ORA-");
				if (index > -1) {
					message = message.substring(0, index);
				}
			}
		}
		return message;
    }

	public static void main(String[] args) {
//		System.out.println(StringOperation.indexOfNearestSpace("La convention a une date de fin ant\u00E9rieure \u00E0 l'exercice en cours. " +
//					"Voulez-vous cr\u00E9er une date de fin d'ex\u00E9cution", 0));
//		System.out.println("["+StringOperation.multiline("La convention a une date de fin ant�rieure � l'exercice en aaaaa zzzz eeeeeee rr tttttt.")+"]");
//		System.out.println("["+StringOperation.multiline("La convention a une date de fin ant\u00E9rieure \u00E0 l'exercice en cours." +
//					"Voulez-vous cr\u00E9er une date de fin d'ex\u00E9cution au 31 d\u00E9cembre N ? \nConfirmez-vous ce " +
//					"report de cr\u00E9dits pour la convention ? hfsdlkdf dslkfsdhjkf sdkfsd fbsdf sdfkbfds",60)+"]");
		System.out.println("["+StringOperation.multiline(
				"Une erreur est survenue : \n"+
				"Aucune donn�e n'a �t� \nrenvoy�e par le serveur :\n"+
				"Erreur lors du remplissage de la maquette: Could not load object from location: " +
				"/Developer/Bertrand/Buildings/Development/Cocowork.framework/Versions/A/WebServerResourcesGeneralitesTranches.jasper")+"]");
//		System.out.println(StringOperation.removeAccents("!'\u00E8\u00E9\u00EA\u00EB��!')�(�!�'kjhfg�)�!gdksjdfhg�)!'(�-��('!&-��(!bg\n", true));
//
//		System.out.println("["+
//				StringOperation.makePatternForSearching("   !'\u00E8\u00E9 \u00EA\u00EB��!') �(�!�'kjh\nfg �)�!gd ksjdfhg�)!'(�-��('!&-�   �(!bg\n", true)+
//				"]");
	}
}
