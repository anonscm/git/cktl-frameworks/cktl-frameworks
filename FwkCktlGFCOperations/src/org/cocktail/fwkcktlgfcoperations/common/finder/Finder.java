/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cocktail.fwkcktlgfcoperations.common.finder;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


/**
 *
 * @author Michael Haller
 */


public abstract class Finder {

	protected static final int DEFAULT_MINIMUM_LENGTH_FOR_STRING_CRITERIA = 3;
	
	protected static final String INFO_RECHERCHE_POSSIBLE = 
		"La recherche peut \u00EAtre lanc\u00E9e.";
	protected static final String INFO_EDITING_CONTEXT_REQUIS = "Editing context requis pour lancer la recherche";
	
	
	protected EOEditingContext ec;
	protected String entityName;
	
	protected NSMutableArray freeQualifiers;
	protected NSMutableArray optionalQualifiers;
	protected NSMutableArray mandatoryQualifiers;
	
	protected NSArray objects;
	
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 * @param entityName Nom de l'entite du model correspondant aux objets metier recherches.
	 */
	protected Finder(EOEditingContext ec, final String entityName) {
		
		this.ec = ec;
		this.entityName = entityName;

		this.freeQualifiers = new NSMutableArray();
		this.optionalQualifiers = new NSMutableArray();
		this.mandatoryQualifiers = new NSMutableArray();
		
		this.objects = new NSMutableArray();
	}
	
	/**
	 * Change l'editing context de travail.
	 * @param ec Nouvel editing contexte a utiliser.
	 */
	public void setEditingContext(final EOEditingContext ec) {
		this.ec = ec;
	}
	
	
	/**
	 * Supprime tous les criteres de recherches courants.
	 */
	public abstract void clearAllCriteria();
	
	/**
	 * Indique si les criteres necessaires ont ete fournis pour chercher.
	 * @return <code>true</code> si la recherche est possible, <code>false</code> sinon.
	 */
	public abstract boolean canFind();
	
	/**
	 * 
	 * @return
	 */
	public abstract String getCurrentWarningMessage();
	
	/**
	 * Ajout d'un qualifier libre.
	 * @param qualifier Qualifer supplementaire, un AND est fait avec ceux existants.
	 */
	public void addFreeQualifier(final EOQualifier qualifier) {
		if (qualifier != null) {
			this.freeQualifiers.addObject(qualifier);
		}
	}
	/**
	 * Ajout d'un qualifier "inevitable".
	 * @param qualifier Le qualifer a ajouter. Un AND sera fait avec les qualifiers existants.
	 */
	protected void addMandatoryQualifier(EOQualifier qualifier) {
		if (qualifier != null) {
			this.mandatoryQualifiers.addObject(qualifier);
		}
	}
	/**
	 * Ajout d'un qualifier optionnel au finder.
	 * @param qualifier Le qualifer a ajouter. Un AND sera fait avec les qualifiers existants.
	 */
	protected void addOptionalQualifier(EOQualifier qualifier) {
		if (qualifier != null) {
			this.optionalQualifiers.addObject(qualifier);
		}
	}
	
	/**
	 * Cree un qualifier a partir d'un format et d'un unique argument, si cet argument est non null.
	 * @param qualifierFormat Format du qualifier.
	 * @param qualifierArgument Argument du nouveau qualifier. Si cet argument est null, le qualifier est mis a null.
	 */
	protected EOQualifier createQualifier(
			final String qualifierFormat, 
			final Object qualifierArgument) {
		
		if (qualifierArgument != null) 
			return EOQualifier.qualifierWithQualifierFormat(qualifierFormat, new NSArray(qualifierArgument));
		else
			return null;
	}
	/**
	 * Cree un qualifier a partir d'un format et d'une liste de plusieurs arguments (si cette liste est non null et non vide).
	 * @param qualifierFormat Format du qualifier.
	 * @param qualifierArgument Argument du nouveau qualifier. Si cet argument est null, le qualifier est mis a null.
	 */
	protected EOQualifier createQualifier(
			final String qualifierFormat, 
			final NSArray qualifierArguments) {
		
		if (qualifierArguments != null && qualifierArguments.count()>0) 
			return EOQualifier.qualifierWithQualifierFormat(qualifierFormat, qualifierArguments);
		else
			return null;
	}

	/**
	 * Supprimer tous les qualifiers (sauf ceux obligatoires).
	 */
	public void removeQualifiers() {
		removeOptionalQualifiers();
		removeFreeQualifiers();
	}
	/**
	 * Supprimer les qualifiers libres.
	 */
	protected void removeFreeQualifiers() {
		this.optionalQualifiers.removeAllObjects();
	}
	/**
	 * Supprimer les qualifiers (sauf ceux obligatoires).
	 */
	protected void removeOptionalQualifiers() {
		this.optionalQualifiers.removeAllObjects();
	}
	/**
	 * Fournit le qualifier global courant de recherche (y compris ceux obligatoires).
	 * @return Le qualifier global, ou <code>null</code> si aucun critere present.
	 */
	public EOQualifier getGlobalQualifier() {
		NSMutableArray array = new NSMutableArray();
		if(this.mandatoryQualifiers!=null)
			array.addObjectsFromArray(this.mandatoryQualifiers);
		if(this.optionalQualifiers!=null)
			array.addObjectsFromArray(this.optionalQualifiers);
		if(this.freeQualifiers!=null)
			array.addObjectsFromArray(this.freeQualifiers);
		
		// System.out.println("count qualifier : "+array.count());
		if (array.count() > 0)
			return new EOAndQualifier(array); 
		else
			return null;
	}
	/**
	 * Fournit le nombre courant de qualifiers (sauf ceux obligatoires).
	 * @return Un entier, eventuellement nul.
	 */
	protected int getQualifiersCount() {
		return this.optionalQualifiers.count() + this.freeQualifiers.count();
	}
	
	
	/**
	 * Lance la recherche en base, avec les criteres courants..
	 * @return Liste des objets trouves.
	 * @throws ExceptionFinder Si criteres insuffisants ou si aucun objet trouve.
	 */
	public NSArray find() throws ExceptionFinder {
		
		if (!this.canFind()) {
			throw new ExceptionFinder(this.getCurrentWarningMessage());
		}
		objects = fetch();
		
		return objects;
	}
	/**
	 * Lance la recherche sans criteres, sauf ceux obligatoires.
	 * @return Liste des objets trouves.
	 * @throws ExceptionFinder 
	 */
	public NSArray findAll() throws ExceptionFinder {
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		// fetch
		objects = fetch();
		return objects;
	}
	
	/**
	 * Effectue le fetch en base.
	 * @throws ExceptionFinder Une erreur, ou rien trouve.
	 */
	private NSArray fetch() throws ExceptionFinder {

		// verif ec present
		if (ec == null) {
			throw new NullPointerException(INFO_EDITING_CONTEXT_REQUIS);
		}
		
		EOQualifier globalQualifier = getGlobalQualifier();
		
		// fetch
		try {
			objects = find(
					ec, 
					entityName, 
					globalQualifier,
					null);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new ExceptionFinder("Une erreur s'est produite lors du fetch : "+e.getMessage(), e);
		}
		
		return objects;
	}
	
	/**
	 * Retourne le nombre d'objets trouves.
	 * @return Entier eventuellement nul.
	 */
	public int getFoundObjectCount() {
		return this.objects.count();
	}
	
    public static NSArray find(EOEditingContext ec,String leNomTable, EOQualifier leQualifier, NSArray leSort) {
        EOFetchSpecification myFetch;
        myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
        return new NSArray(ec.objectsWithFetchSpecification(myFetch));
    }

}
