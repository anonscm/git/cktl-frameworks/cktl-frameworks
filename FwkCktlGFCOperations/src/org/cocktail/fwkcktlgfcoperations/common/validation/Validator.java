package org.cocktail.fwkcktlgfcoperations.common.validation;

public interface Validator<E> {

	boolean isValid(E entity);
	
}
