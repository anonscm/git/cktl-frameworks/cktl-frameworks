/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfcoperations.server;

import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametres;
import org.cocktail.fwkcktlpersonne.common.metier.EOGrhumParametresType;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eoaccess.EOEntity;
import com.webobjects.eocontrol.EOEditingContext;

import er.extensions.eof.ERXEC;
import er.extensions.eof.ERXEOAccessUtilities;

/**
 * Pompé de {@link org.cocktail.fwkcktlcompta.server.FwkCktlComptaParamManager} et
 * {@link org.cocktail.fwkcktlpersonne.common.FwkCktlPersonneParamManager}
 * 
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class FwkCktlGFCOperationsParamManager extends CktlParamManager {
	private EOEditingContext ec = ERXEC.newEditingContext();

	public static final String MULTI_LB_POUR_CONV_RA_AUTORISE = "org.cocktail.convention.ra.multilignesbudgetaires.autorise";

	public FwkCktlGFCOperationsParamManager() {
		getParamList().add(MULTI_LB_POUR_CONV_RA_AUTORISE);
		getParamComments().put(MULTI_LB_POUR_CONV_RA_AUTORISE, "Autoriser l'association de plusieurs lignes budgétaires pour une convention RA");
		getParamDefault().put(MULTI_LB_POUR_CONV_RA_AUTORISE, "NON");
		getParamTypes().put(MULTI_LB_POUR_CONV_RA_AUTORISE, EOGrhumParametresType.codeActivation);
	}

	/**
	 * Cree un nouveau parametre de type EOGrhumParametresType.codeActivation {@inheritDoc}
	 */
	@Override
	public void createNewParam(String key, String value, String comment) {
		createNewParam(key, value, comment, EOGrhumParametresType.codeActivation);

	}

	/** {@inheritDoc} */
	@Override
	public void createNewParam(String key, String value, String comment, String type) {
		EOGrhumParametres newParametre = EOGrhumParametres.creerInstance(ec);
		newParametre.setParamKey(key);
		newParametre.setParamValue(value);
		newParametre.setParamCommentaires(comment);
		newParametre.setToParametresTypeRelationship(EOGrhumParametresType.fetchByKeyValue(ec, EOGrhumParametresType.TYPE_ID_INTERNE_KEY, type));
		if (ec.hasChanges()) {
			EOEntity entityParameter = ERXEOAccessUtilities.entityForEo(newParametre);
			try {
				// Avant de sauvegarder les données, nous modifions le modèle
				// pour que l'on puisse avoir accès aussi en écriture sur les données
				entityParameter.setReadOnly(false);
				ec.saveChanges();
			} catch (Exception e) {
				log.warn("Erreur lors de l'enregistrement des parametres.");
				e.printStackTrace();
			} finally {
				entityParameter.setReadOnly(true);
			}
		}
	}

	/** {@inheritDoc} */
	@Override
	public String getParam(String key) {
		String res = getApplication().config().stringForKey(key);
		return res;
	}

}
