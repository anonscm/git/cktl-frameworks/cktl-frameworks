package org.cocktail.fwkcktlgfcoperations.server;

import org.cocktail.fwkcktlpersonne.common.metier.droits.AutorisationsCache;

public class FwkCktlGFCOperationsAutorisationCache extends AutorisationsCache {

	public FwkCktlGFCOperationsAutorisationCache(String appStrId, Integer persId) {
		super(appStrId, persId);
	}

	public FwkCktlGFCOperationsAutorisationCache(Integer persId) {
		this("GFCOPERATIONS", persId);
	}

	public boolean hasDroitConsultationContrat() {
		return hasDroitUtilisationOnFonction("CONV");
	}

}
