package org.cocktail.fwkcktlgfcoperations.server;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponible;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponibleFraisExclus;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculTvaService;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculTvaServiceImpl;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche.RechercheContratService;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche.RechercheContratServiceImpl;

import com.google.inject.AbstractModule;

/**
 * 
 * Module Guice.
 * Si on a besoin de définir un lien dans le fichier Properties : {@link #getImplementationClassFor(Class)}
 * 
 * @author Alexis Tual
 *
 */
public class FwkCktlGFCOperationsModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(RechercheContratService.class).to(RechercheContratServiceImpl.class);
        bind(CalculTvaService.class).to(CalculTvaServiceImpl.class);
        bind(CalculMontantParticipationDisponible.class).to(
//                getImplementationClassFor(CalculMontantParticipationDisponible.class)
        		CalculMontantParticipationDisponibleFraisExclus.class
                );
    }

    @SuppressWarnings("unchecked")
    private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
        try {
            String implementationClassName = System.getProperty(baseClass.getSimpleName());
            Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
            return clazz;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
        }
    }
    
}
