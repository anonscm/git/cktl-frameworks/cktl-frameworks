package org.cocktail.fwkcktlgfcoperations.server;

import org.cocktail.fwkcktlgfceos.server.GFCApplicationUser;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FwkCktlGFCOperationsApplicationUser extends GFCApplicationUser {

	private static final String TYAP_STR_ID_GFCOPERATIONS = "GFCOPERATIONS";
	public static NSArray fonctions = null;
	
	/** Super admin */
	public static final String FON_ID_SUPER_ADMIN = "SUPADMIN";
	private Boolean hasDroitSuperAdmin = null;

	/** Gestion / Operation */
	public static final String FON_ID_CREATION_CONTRAT = "CONC";
	private Boolean hasDroitCreationOperationsEtAvenants = null;
	public static final String FON_ID_CONSULTATION_CONTRAT = "CONV";
	private Boolean hasDroitConsultationOperationsEtAvenants = null;
	public static final String FON_ID_CONSULTATION_TOUS_CONTRATS = "CONVTOUS";
	private Boolean hasDroitConsultationTousLesOperationsEtAvenants = null;
	public static final String FON_ID_RECHERCHE_DETAILLEE_CONTRAT = "CONR";
	private Boolean hasDroitRechercheDetailleeOperationsEtAvenants = null;
	public static final String FON_ID_MODIFICATION_CONTRAT = "CONM";
	private Boolean hasDroitModificationOperationsEtAvenants = null;
	public static final String FON_ID_SUPPRESSION_CONTRAT = "CONS";
	private Boolean hasDroitSuppressionOperationsEtAvenants = null;
	public static final String FON_ID_VALIDATION_OPERATION = "OPEVALADM";
	private Boolean hasDroitValidationOperationEtAvenants = null;
	public static final String FON_ID_VALIDATION_FINANCIERE = "OPEVALFIN";
	private Boolean hasDroitValidationFinanciereOperationEtAvenants = null;
    public static final String FON_ID_GESTION_REGROUPEMENTS = "GRPG";
    private Boolean hasDroitGestionRegroupements = null;
    public static final String FON_ID_CREATION_OPERATIONS_IMPACT_FINANCIER = "OPEFIN";
    private Boolean hasDroitCreationOperationsImpactFinancier = null;

	/** Gestion / Partenaires */
	public static final String FON_ID_CONSULTATION_PARTENAIRES = "PARTV";
	private Boolean hasDroitConsultationPartenaires = null;
	public static final String FON_ID_MODIFICATION_PARTENAIRES = "PARTM";
	private Boolean hasDroitModificationPartenaires = null;

	/** Gestion / Document */
	public static final String FON_ID_CONSULTATION_DOCUMENT = "DOCV";
	private Boolean hasDroitConsultationDocuments = null;
	public static final String FON_ID_MODIFICATION_DOCUMENT = "DOCM";
	private Boolean hasDroitModificationDocuments = null;

	/** Gestion / Evenements */
	public static final String FON_ID_CONSULTATION_EVENEMENTS = "EVTV";
	private Boolean hasDroitConsultationEvenements = null;
	public static final String FON_ID_MODIFICATION_EVENEMENTS = "EVTM";
	private Boolean hasDroitModificationEvenements = null;

	/** Gestion / Tranches */
	public static final String FON_ID_CREATION_TRANCHES = "TRAC";
	private Boolean hasDroitCreationTranches = null;
	public static final String FON_ID_SUPPRESSION_TRANCHES = "TRAS";
	private Boolean hasDroitSuppressionTranches = null;
	public static final String FON_ID_MODIFICATION_TRANCHES = "TRAM";
	private Boolean hasDroitModificationTranches = null;

	/** Impressions */
	private static final String FON_ID_IMPRESSION_GENERALITES = "IMPGEN";
	private Boolean hasDroitImpressionGeneralites = null;

	/** Gestion budget */
	private static final String FON_ID_BUDPROP = "BUDPROP";
	private Boolean hasDroitPropositionBudget = null;
	private static final String FON_ID_BUDPOS = "BUDPOS";
	private Boolean hasDroitPositionnerBudget = null;

	/** Validation tranche */
	private static final String FON_ID_VALIDATION_TRANCHE = "TRAVAL";
	private Boolean hasDroitValidationTranche = null;

	/** Report des crédits **/
	private static final String FON_ID_REPORT_CREDITS = "REPC";
	private Boolean hasDroitReportCredits = null;

	public FwkCktlGFCOperationsApplicationUser(EOEditingContext ec, String tyapStrId, Integer persId) {
		super(ec, tyapStrId, persId);
	}

	public FwkCktlGFCOperationsApplicationUser(EOEditingContext ec, String tyapStrId, EOUtilisateur utilisateur) {
		super(ec, tyapStrId, utilisateur);
	}

	public FwkCktlGFCOperationsApplicationUser(EOEditingContext ec, EOUtilisateur utilisateur) {
		super(ec, utilisateur);
	}

	public FwkCktlGFCOperationsApplicationUser(EOEditingContext ec, Integer persId) {
		super(ec, persId);
	}

	public boolean hasDroitSuperAdmin() {
		if (hasDroitSuperAdmin == null) {
			hasDroitSuperAdmin = this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_SUPER_ADMIN, null);
		}
		return hasDroitSuperAdmin;
	}

    public boolean hasDroitCreationOperationsImpactFinancier() {
        if (hasDroitCreationOperationsImpactFinancier == null) {
            hasDroitCreationOperationsImpactFinancier = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CREATION_OPERATIONS_IMPACT_FINANCIER, null);
        }
        return hasDroitCreationOperationsImpactFinancier;        
    }
    
    public boolean hasDroitGestionRegroupements() {
        if (hasDroitGestionRegroupements == null) {
            hasDroitGestionRegroupements = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_GESTION_REGROUPEMENTS, null);
        }
        return hasDroitGestionRegroupements;        
    }
    
	public boolean hasDroitCreationOperationsEtAvenants() {
		if (hasDroitCreationOperationsEtAvenants == null) {
			hasDroitCreationOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CREATION_CONTRAT, null);
		}
		return hasDroitCreationOperationsEtAvenants;
	}

	public boolean hasDroitConsultationOperationsEtAvenants() {
		if (hasDroitConsultationOperationsEtAvenants == null) {
			hasDroitConsultationOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CONSULTATION_CONTRAT, null);
		}
		return hasDroitConsultationOperationsEtAvenants;
	}

	public boolean hasDroitConsultationTousLesOperationsEtAvenants() {
		if (hasDroitConsultationTousLesOperationsEtAvenants == null) {
			hasDroitConsultationTousLesOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CONSULTATION_TOUS_CONTRATS, null);
		}
		return hasDroitConsultationTousLesOperationsEtAvenants;
	}

	public boolean hasDroitValidationOperationsEtAvenants() {
		if (hasDroitValidationOperationEtAvenants == null) {
			hasDroitValidationOperationEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_VALIDATION_OPERATION, null);
		}
		return hasDroitValidationOperationEtAvenants;
	}
	
	public boolean hasDroitValidationFinanciereOperationsEtAvenants() {
		if (hasDroitValidationFinanciereOperationEtAvenants == null) {
			hasDroitValidationFinanciereOperationEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_VALIDATION_FINANCIERE, null);
		}
		return hasDroitValidationFinanciereOperationEtAvenants;
	}
	
	public boolean hasDroitSuppressionOperationsEtAvenants() {
		if (hasDroitSuppressionOperationsEtAvenants == null) {
			hasDroitSuppressionOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_SUPPRESSION_CONTRAT, null);
		}
		return hasDroitSuppressionOperationsEtAvenants;
	}

	public boolean hasDroitRechercheDetailleeOperationsEtAvenants() {
		if (hasDroitRechercheDetailleeOperationsEtAvenants == null) {
			hasDroitRechercheDetailleeOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_RECHERCHE_DETAILLEE_CONTRAT, null);
		}
		return hasDroitRechercheDetailleeOperationsEtAvenants;
	}

	public boolean hasDroitModificationOperationsEtAvenants() {
		if (hasDroitModificationOperationsEtAvenants == null) {
			hasDroitModificationOperationsEtAvenants = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_MODIFICATION_CONTRAT, null);
		}
		return hasDroitModificationOperationsEtAvenants;
	}

	public boolean hasDroitConsultationPartenaires() {
		if (hasDroitConsultationPartenaires == null) {
			hasDroitConsultationPartenaires = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CONSULTATION_PARTENAIRES, null);
		}
		return hasDroitConsultationPartenaires;
	}

	public boolean hasDroitModificationPartenaires() {
		if (hasDroitModificationPartenaires == null) {
			hasDroitModificationPartenaires = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_MODIFICATION_PARTENAIRES, null);
		}
		return hasDroitModificationPartenaires;
	}

	public boolean hasDroitConsultationDocuments() {
		if (hasDroitConsultationDocuments == null) {
			hasDroitConsultationDocuments = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CONSULTATION_DOCUMENT, null);
		}
		return hasDroitConsultationDocuments;
	}

	public boolean hasDroitModificationDocuments() {
		if (hasDroitModificationDocuments == null)
			hasDroitModificationDocuments = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_MODIFICATION_DOCUMENT, null);
		return hasDroitModificationDocuments;
	}

	public boolean hasDroitConsultationEvenements() {
		if (hasDroitConsultationEvenements == null) {
			hasDroitConsultationEvenements = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CONSULTATION_EVENEMENTS, null);
		}
		return hasDroitConsultationEvenements;
	}

	public boolean hasDroitModificationEvenements() {
		if (hasDroitModificationEvenements == null) {
			hasDroitModificationEvenements = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_MODIFICATION_EVENEMENTS, null);
		}
		return hasDroitModificationEvenements;
	}

	public boolean hasDroitCreationTranches() {
		if (hasDroitCreationTranches == null) {
			hasDroitCreationTranches = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_CREATION_TRANCHES, null);
		}
		return hasDroitCreationTranches;
	}

	public boolean hasDroitModificationTranches() {
		if (hasDroitModificationTranches == null) {
			hasDroitModificationTranches = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_MODIFICATION_TRANCHES, null);
		}
		return hasDroitModificationTranches;
	}

	public boolean hasDroitSuppressionTranches() {
		if (hasDroitSuppressionTranches == null) {
			hasDroitSuppressionTranches = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_SUPPRESSION_TRANCHES, null);
		}
		return hasDroitSuppressionTranches;
	}

	public boolean hasDroitImpressionGeneralites() {
		if (hasDroitImpressionGeneralites == null) {
			hasDroitImpressionGeneralites = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_IMPRESSION_GENERALITES, null);
		}
		return hasDroitImpressionGeneralites;
	}

	public boolean hasDroitValidationPrevisionBudget() {
		if (hasDroitPropositionBudget == null) {
			hasDroitPropositionBudget = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_BUDPROP, null);
		}
		return hasDroitPropositionBudget;
	}

	public boolean hasDroitPropositionBudget() {
		if (hasDroitPropositionBudget == null) {
			hasDroitPropositionBudget = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_BUDPROP, null);
		}
		return hasDroitPropositionBudget;
	}

	public boolean hasDroitPositionnerBudget() {
		if (hasDroitPositionnerBudget == null) {
			hasDroitPositionnerBudget = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_BUDPOS, null);
		}
		return hasDroitPositionnerBudget;
	}
	
	public boolean hasDroitValidationTranche() {
		if (hasDroitValidationTranche == null) {
			hasDroitValidationTranche = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_VALIDATION_TRANCHE, null);
		}
		return hasDroitValidationTranche;
	}

	public boolean hasDroitReportCredits() {
		if (hasDroitReportCredits == null) {
			hasDroitReportCredits = hasDroitSuperAdmin() || this.isFonctionAutoriseeByFonID(TYAP_STR_ID_GFCOPERATIONS, FON_ID_REPORT_CREDITS, null);
		}
		return hasDroitReportCredits;
	}
}
