package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import lombok.Getter;

@Getter
public class CategorieOperationBean {

	public static final String LL_CATEG_OPERATION = "llCategOperation";
	
    private String codeCategOperation;
    private String llCategOperation;
    
    public CategorieOperationBean(String codeCategOperation, String llCategOperation) {
        this.codeCategOperation = codeCategOperation;
        this.llCategOperation = llCategOperation;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codeCategOperation == null) ? 0 : codeCategOperation
						.hashCode());
		result = prime
				* result
				+ ((llCategOperation == null) ? 0 : llCategOperation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CategorieOperationBean other = (CategorieOperationBean) obj;
		if (codeCategOperation == null) {
			if (other.codeCategOperation != null)
				return false;
		} else if (!codeCategOperation.equals(other.codeCategOperation))
			return false;
		if (llCategOperation == null) {
			if (other.llCategOperation != null)
				return false;
		} else if (!llCategOperation.equals(other.llCategOperation))
			return false;
		return true;
	}
	
}
