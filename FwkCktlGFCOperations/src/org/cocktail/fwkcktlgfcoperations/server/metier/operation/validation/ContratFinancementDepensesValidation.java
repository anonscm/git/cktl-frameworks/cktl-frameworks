package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;
import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

public class ContratFinancementDepensesValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Verifie que le total des contributions des partenaires est superieur ou égal 
     * à la somme des budgets de depenses positionnés.
     * 
     * @param operation le contrat a valider.
     * @return true si le financement du contrat est valide, false sinon.
     */
    public boolean isSatisfiedBy(Operation operation) {
        if (operation == null) {
            return false;
        }
        
        BigDecimal totalContributionsAPositionner = operation.totalContributionsAPositionner();
        BigDecimal totalPositionneContrat = operation.totalPositionneOperation();
        return totalContributionsAPositionner.subtract(totalPositionneContrat).doubleValue() >= 0;
    } 
}
