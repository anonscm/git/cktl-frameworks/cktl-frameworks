package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 
 * Bean utilisé dans les filtres de recherche pour représenter les 
 * disciplines.
 * 
 * @author Alexis Tual
 *
 */
public class DisciplineBean {

    public static final String DISC_LIBELLE = "discLibelle";
    
    private Integer discOrdre;
    private String discLibelle;
    
    /**
     * @param discOrdre un discOrdre
     * @param discLibelle un libellé de discipline
     */
    public DisciplineBean(Integer discOrdre, String discLibelle) {
        this.discOrdre = discOrdre;
        this.discLibelle = discLibelle;
    }

    public String getDiscLibelle() {
        return discLibelle;
    }

    public Integer getDiscOrdre() {
        return discOrdre;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(discOrdre)
                .append(discLibelle)
                .toHashCode();
    }
    
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
        if (obj instanceof DisciplineBean) {
            DisciplineBean other = (DisciplineBean) obj;
            result = new EqualsBuilder()
                        .append(discOrdre, other.discOrdre)
                        .append(discLibelle, other.discLibelle)
                        .isEquals();
        }
        return result;
    }
    
}
