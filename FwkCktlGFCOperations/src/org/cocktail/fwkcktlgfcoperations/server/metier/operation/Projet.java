// Projet.java
// Created on Thu Feb 21 14:05:15 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class Projet extends EOProjet {

    public Projet() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public Groupe(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/
    static public Projet defaultProjet(EOEditingContext ec) {
    	Projet result = projetForPjtId(ec, new Integer(1));
    	return result; 
    }
    
    static public Projet projetForPjtId(EOEditingContext ec, Integer pjtId) {
    	NSArray result = Finder.find(ec, "OpeProjet", EOQualifier.qualifierWithQualifierFormat("pjtId = %@", new NSArray(pjtId)), null);
    	return result.count()==1 ? (Projet)result.objectAtIndex(0) : null; 
    }
    
    /**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee.
	 * @throws Exception Probleme d'instanciation.
	 */
	static public Projet instanciate(EOEditingContext ec) throws Exception {
		Projet unProjet = (Projet) Factory.instanceForEntity(ec, ENTITY_NAME);
		
		return unProjet;
	}
    
    

}
