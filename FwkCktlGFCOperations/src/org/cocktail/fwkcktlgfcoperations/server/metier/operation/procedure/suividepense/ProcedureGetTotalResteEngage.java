package org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suividepense;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Procedure qui calcule le total restant engage au titre d'une convention sur un exercice, 
 * pour un type de credit et une ligne de l'organigramme budgetaire.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public class ProcedureGetTotalResteEngage extends ProcedureSuiviDepense
{
	protected final static String PROCEDURE_NAME = "GetTotalResteEngage";
	
	
	/**
	 * Constructeur protected.
	 * @param ec Editing context a utiliser.
	 */
	public ProcedureGetTotalResteEngage(final EOEditingContext ec) { 
		super(ec, PROCEDURE_NAME);
	}
	
}
