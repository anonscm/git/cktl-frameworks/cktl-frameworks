package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import lombok.Getter;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec;

@Getter
public class BudgetCombinaisonAxesDepenseNotificationHandler {

	private boolean isEbEnErreur;
	private boolean isDestinationEnErreur; 
	private boolean isNatureEnErreur;
	private boolean isEnveloppeEnErreur;
			
	public BudgetCombinaisonAxesDepenseNotificationHandler() {
		isEbEnErreur = false;
		isDestinationEnErreur = false;
		isNatureEnErreur = false;
		isEnveloppeEnErreur = false;
	}
	
	public void marquerEbEnErreur() {
		isEbEnErreur = true;
	}
	
	public void marquerDestinationEnErreur() {
		isDestinationEnErreur = true;
	}
	
	public void marquerNatureEnErreur() {
		isNatureEnErreur = true;
	}

	public void marquerEnveloppeEnErreur() {
		isEnveloppeEnErreur = true;
	}
	
}
