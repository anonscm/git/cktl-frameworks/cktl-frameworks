
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModeGestion;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderModeGestion extends Finder {
	protected String mgLibelleCourt;
	
	protected EOQualifier mgLibelleCourtQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context de travail
	 */
	public FinderModeGestion(EOEditingContext ec) {
		super(ec, ModeGestion.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du parametre.
	 * @param mgLibelleCourt Libelle court
	 */
	public void setMgLibelleCourt(final String mgLibelleCourt) {
		this.mgLibelleCourtQualifier = createQualifier(
				"mgLibelleCourt = %@", 
				mgLibelleCourt);
		
		this.mgLibelleCourt = mgLibelleCourt;
	}
	
	/**
	 * 
	 */
	public void clearAllCriteria() {
		setMgLibelleCourt(null);
	}

	/**
	 * 
	 */
	public NSArray findWithLibelleCourt(final String libelleCourt) throws ExceptionFinder {
		removeOptionalQualifiers();
		setMgLibelleCourt(libelleCourt);
		addOptionalQualifier(this.mgLibelleCourtQualifier);
		
		return super.find();
	}

	/**
	 * 
	 */
	public NSArray find() throws ExceptionFinder {
		removeOptionalQualifiers();
		addOptionalQualifier(this.mgLibelleCourtQualifier);
		
		return super.find();
	}
	
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	/**
	 * 
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * 
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
