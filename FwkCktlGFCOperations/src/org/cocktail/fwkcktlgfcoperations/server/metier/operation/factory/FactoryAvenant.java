package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktlged.serveur.metier.EODocument;
import org.cocktail.fwkcktlgfceos.server.ApplicationUser;
import org.cocktail.fwkcktlgfceos.server.metier.EOTva;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.AvenantDocument;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModeGestion;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeAvenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core.FinderModeGestion;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Michael HALLER, Consortium Cocktail, 2008
 */
public class FactoryAvenant extends Factory {
	public static String CATEGORIE_GEDFS_CODE_DEFAUT = "TEST"; // TODO En faire un parametre ?

	/**
	 * @param ec {@link EOEditingContext}
	 * @param withtrace <code>true</code> pour activer la trace
	 */
	public FactoryAvenant(final EOEditingContext ec, final Boolean withtrace) {

		super(ec, withtrace.booleanValue());
	}

	/**
	 * Création d'un avenant.
	 * 
	 * @param operation Contrat de rattachement.
	 * @param index Numero d'index de l'avenant (0: avenant zéro, 1, 2, 3, etc)
	 * @param typeAvenant Type de l'avenant (initial, administratif, financier)
	 * @param modeGestion Mode de gestion de l'avenant, doit etre le meme pour tous les avenants d'un operation.
	 * @param referenceExterne Reference externe de l'avenant
	 * @param objet Objet de l'avenant
	 * @param observations Observations diverses
	 * @param dateDebut Date de debut de l'avenant
	 * @param dateFin Date de fin de l'avenant
	 * @param duree Durée de l'avenant
	 * @param dateDebutExecution Date de debut d'execution de la convention
	 * @param dateFinExecution Date de fin d'execution de la convention
	 * @param dateSignature Date de signature en CA de la convention
	 * @param montantGlobalHT Montant HT de l'avenant
	 * @param tauxTva Taux de TVA a appliquer au montant HT pour avoir le TTC
	 * @param creditsLimitatifs Indique si le montant des credits positionnes est indepassable au moment d'un depense.
	 * @param lucratif Indicateur de lucrativite de l'avenant
	 * @param tvaRecuperable Indicateur de recuperation (collecte) de la TVA
	 * @param utilisateur Utilisateur createur de l'avenant
	 * @return L'avenant cree.
	 * @throws ExceptionUtilisateur Si un paramètre obligatoire n'est pas fourni
	 * @throws ExceptionFinder Si le mode de gestion n'est pas trouvé
	 */
	public Avenant creerAvenant(final Operation operation, final Integer index, final TypeAvenant typeAvenant, final ModeGestion modeGestion,
			final String referenceExterne, final String objet, final String observations, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Integer duree, final NSTimestamp dateDebutExecution, final NSTimestamp dateFinExecution, final NSTimestamp dateSignature,
			final BigDecimal montantGlobalHT, final EOTva tauxTva, final Boolean creditsLimitatifs, final Boolean lucratif, final Boolean tvaRecuperable,
			final EOUtilisateur utilisateur) throws ExceptionUtilisateur, ExceptionFinder {

		trace("creerAvenant()");
		trace(operation);
		trace(index);
		trace(typeAvenant);
		trace(modeGestion);
		trace(referenceExterne);
		trace(objet);
		trace(observations);
		trace(dateDebut);
		trace(dateFin);
		trace(dateDebutExecution);
		trace(dateFinExecution);
		trace(dateSignature);
		trace(montantGlobalHT);
		trace(tauxTva);
		trace(creditsLimitatifs);
		trace(lucratif);
		trace(tvaRecuperable);
		trace(utilisateur);

		if (modeGestion == null) {
			throw new ExceptionUtilisateur("Le mode de gestion doit \u00EAtre fourni.");
		}

		Avenant avenant = creerAvenantVierge(operation, index, typeAvenant, utilisateur);

		modifierAvenant(avenant, modeGestion, operation.centreResponsabilite(), referenceExterne, objet, observations, dateDebut, dateFin, duree,
				dateDebutExecution, dateFinExecution, dateSignature, montantGlobalHT, tauxTva, creditsLimitatifs, lucratif, tvaRecuperable, null, null);

		return avenant;
	}

	/**
	 * Creation d'un avenant.
	 * 
	 * @param operation Contrat de rattachement.
	 * @param index Numero d'index de l'avenant (0: avenant zero, 1, 2, 3, etc)
	 * @param typeAvenant Le type d'avenant
	 * @param utilisateur Utilisateur createur de l'avenant
	 * @return L'avenant cree.
	 * @throws ExceptionUtilisateur Si le operation ou l'utilisateur ne sont pas fournis
	 * @throws ExceptionFinder Si le mode de gestion n'est pas trouvé
	 */
	public Avenant creerAvenantVierge(final Operation operation, Integer index, final TypeAvenant typeAvenant, final EOUtilisateur utilisateur)
			throws ExceptionUtilisateur, ExceptionFinder {

		if (operation == null) {
			throw new ExceptionUtilisateur("Le operation doit \u00EAtre fourni.");
		}

		if (utilisateur == null) {
			throw new ExceptionUtilisateur("L'utilisateur cr\u00E9ateur doit \u00EAtre fourni.");
		}

		if (index == null || index.intValue() < 0) {
			NSArray avenants = operation.avenantsDontInitialNonSupprimes();
			EOSortOrdering sortOrderingIndex = EOSortOrdering.sortOrderingWithKey(Avenant.AVT_INDEX_KEY, EOSortOrdering.CompareAscending);
			avenants = EOSortOrdering.sortedArrayUsingKeyOrderArray(avenants, new NSArray(sortOrderingIndex));
			index = (Integer) ((Avenant) avenants.lastObject()).avtIndex();
			index = Integer.valueOf(index.intValue() + 1);
		}
		Avenant avenant = Avenant.create(ec, null, null);

		avenant.setTypeAvenantRelationship(typeAvenant);
		avenant.setAvtIndex(index);
		avenant.setAvtMonnaie(Avenant.AVT_MONNAIE);
		avenant.setAvtMontantHt(BigDecimal.ZERO);
		avenant.setAvtSuppr(Avenant.AVT_SUPPR_NON);
		avenant.setAvtDateCreation(new NSTimestamp());
		avenant.setUtilisateurCreationRelationship(utilisateur);
		avenant.setUtilisateurModifRelationship(utilisateur);
		avenant.setOperationRelationship(operation);

		if (index.intValue() == 0) {
			operation.setAvenantZeroRelationship(avenant);
			operation.initDatesEtDuree(null, null, null);
		} else {
			// - Initialisation date debut : today
			NSTimestamp today = new NSTimestamp();
			avenant.setAvtDateDeb(today);
			// - Initialisation date de fin : fin du operation
			avenant.setAvtDateFin(operation.dateFin());
		}

		FinderModeGestion fmg = new FinderModeGestion(operation.editingContext());
		ModeGestion modeGestionInitial = (ModeGestion) fmg.findWithLibelleCourt("CS").lastObject();
		avenant.setModeGestionRelationship(modeGestionInitial);

		return avenant;
	}

	/**
	 * Modification d'un avenant
	 * 
	 * @param avenant Avenant a modifier.
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention
	 * @param referenceExterne Reference externe de l'avenant
	 * @param objet Objet de l'avenant
	 * @param observations Observations diverses
	 * @param dateDebut Date de debut de l'avenant
	 * @param dateFin Date de fin de l'avenant
	 * @param duree Durée de l'avenant
	 * @param dateDebutExecution Date de debut d'execution de la convention
	 * @param dateFinExecution Date de fin d'execution de la convention
	 * @param dateSignature Date de signature en CA de la convention
	 * @param montantGlobalHT Montant HT de l'avenant
	 * @param tauxTva Taux de TVA a appliquer au montant HT pour avoir le TTC
	 * @param creditsLimitatifs Indique si le montant des credits positionnes est indepassable au moment d'un depense.
	 * @param lucratif Indicateur de lucrativite de l'avenant
	 * @param tvaRecuperable Indicateur de recuperation (collecte) de la TVA
	 * @param utilisateur Utilisateur modificateur de l'avenant
	 * @param dateModification Pour forcer une date de modification de l'avenant.
	 * @throws ExceptionUtilisateur Si un des paramètres obligatoire n'est pas fourni
	 */
	public void modifierAvenant(final Avenant avenant, final ModeGestion modeGestion, final EOStructure centreResponsabiliteGestionnaire,
			final String referenceExterne, final String objet, final String observations, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Integer duree, final NSTimestamp dateDebutExecution, final NSTimestamp dateFinExecution, final NSTimestamp dateSignature,
			BigDecimal montantGlobalHT, final EOTva tauxTva, final Boolean creditsLimitatifs, final Boolean lucratif, final Boolean tvaRecuperable,
			final EOUtilisateur utilisateur, final NSTimestamp dateModification) throws ExceptionUtilisateur {

		if (avenant == null) {
			throw new ExceptionUtilisateur("L'avenant doit \u00EAtre fourni.");
		}
		if (modeGestion == null) {
			throw new ExceptionUtilisateur("Le mode de gestion doit \u00EAtre fourni.");
		}
		if (centreResponsabiliteGestionnaire == null) {
			throw new ExceptionUtilisateur("Le centre de responsabilit\u00E9 gestionnaire doit \u00EAtre fourni.");
		}
		if (objet == null) {
			throw new ExceptionUtilisateur("L'objet doit \u00EAtre fourni.");
		}
		if (objet.length() > Avenant.LONGUEUR_MAX_OBJET) {
			throw new ExceptionUtilisateur("L'objet ne doit pas d\u00E9passer " + Avenant.LONGUEUR_MAX_OBJET + " caract\u00E8res.");
		}
		if (montantGlobalHT != null && montantGlobalHT.signum() < 0) {
			throw new ExceptionUtilisateur("Le montant global ne doit pas \u00EAtre n\u00E9gatif.");
		}

		int longueur = Avenant.LONGUEUR_MAX_OBJET_COURT;
		if (objet.length() < Avenant.LONGUEUR_MAX_OBJET_COURT) {
			longueur = objet.length();
		}
		String objetCourt = objet.substring(0, longueur);

		avenant.setAvtObjet(objet);
		avenant.setAvtObjetCourt(objetCourt);
		avenant.setAvtObservations(observations);
		avenant.setAvtRefExterne(referenceExterne);
		avenant.setAvtDateSignature(dateSignature);
		avenant.setAvtDateDebExec(dateDebutExecution);
		avenant.setAvtDateFinExec(dateFinExecution);
		avenant.setModeGestionRelationship(modeGestion);
		avenant.setCentreResponsabiliteRelationship(centreResponsabiliteGestionnaire);

		if (montantGlobalHT == null) {
			montantGlobalHT = BigDecimal.ZERO;
		}
		avenant.setAvtMontantHt(montantGlobalHT);
		avenant.setAvtMontantTtc(calculerMontantTtc(montantGlobalHT, tauxTva));
		avenant.setAvtLimitatifBoolean(creditsLimitatifs);
		avenant.setAvtLucrativiteBoolean(lucratif);
		avenant.setAvtRecupTvaBoolean(tvaRecuperable);
		avenant.setUtilisateurModifRelationship(utilisateur);
		avenant.setAvtDateModif(dateModification);

		if (avenant.operation() != null && avenant.avtIndex().intValue() == 0 && avenant.operation().avenantsCount() == 0) {
			avenant.operation().initDatesEtDuree(dateDebut, dateFin, duree);
		} else {
			if (dateDebut != null) {
				avenant.setAvtDateDeb(dateDebut);
			}
			avenant.setAvtDateFin(dateFin);
		}
	}

	/**
	 * Suppression d'un avenant. L'avenant est seulement archive.
	 * 
	 * @param avenant Avenant a supprimer
	 * @throws Exception
	 */
	public void supprimerAvenant(Avenant avenant) {
		avenant.setAvtSuppr(Avenant.AVT_SUPPR_OUI);
	}

	/**
	 * Calcule un montant TTC a partir du montant HT et du taux de TVA.
	 * 
	 * @param montantHt Montant HT.
	 * @param tauxTva Taux de TVA. Peut etre null.
	 * @return Le montant TTC calcule.
	 */
	protected BigDecimal calculerMontantTtc(final BigDecimal montantHt, final EOTva tauxTva) {
		trace("calculerMontantTtc()");
		trace(montantHt);
		trace(tauxTva);

		if (montantHt != null && tauxTva != null && tauxTva.tvaTaux() != null) {
			BigDecimal coeffTva = new BigDecimal(1.0 + tauxTva.tvaTaux().doubleValue() / 100.0);
			BigDecimal result = montantHt.multiply(coeffTva);
			result.setScale(2, BigDecimal.ROUND_HALF_UP);
			return result;
		} else {
			return montantHt;
		}
	}

	/**
	 * @param avenant Un {@link Avenant}
	 * @return <code>true</code> si les champs pour la création de l'avenant sont valides
	 */
	public static boolean isValidAvenantCreation(Avenant avenant) {
		if (avenant == null || avenant.modeGestion() == null || avenant.centreResponsabilite() == null || avenant.avtObjet() == null
				|| avenant.avtObjet().length() > Avenant.LONGUEUR_MAX_OBJET || avenant.avtDateDeb() == null || avenant.avtMontantHt() != null
				&& avenant.avtMontantHt().signum() < 0) {
			return false;
		}
		return true;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param utilisateur Un {@link ApplicationUser}
	 * @param avenant Un {@link Avenant}
	 * @param document Un {@link EODocument}
	 * @return Un {@link AvenantDocument} ou <code>null</code> si un des paramètrs est <code>null</code>
	 */
	public AvenantDocument ajouterDocument(EOEditingContext edc, ApplicationUser utilisateur, Avenant avenant, EODocument document) {
		AvenantDocument avtDocument = null;

		if (edc != null && utilisateur != null && avenant != null && document != null) {
			avtDocument = (AvenantDocument) EOUtilities.createAndInsertInstance(edc, AvenantDocument.ENTITY_NAME);
			avenant.addToAvenantDocumentsRelationship(avtDocument);
			avtDocument.setDocumentRelationship(document);
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(ec, "edc");
			NSNotificationCenter.defaultCenter().postNotification("refreshDocumentsNotification", avtDocument, userInfo);
		}

		return avtDocument;
	}

	/**
	 * @param edc Un {@link EOEditingContext}
	 * @param avenantDocument Un {@link AvenantDocument}
	 * @param utilisateur Un {@link ApplicationUser}
	 * @param racine Non utilisé
	 */
	public void supprimerDocument(EOEditingContext edc, AvenantDocument avenantDocument, ApplicationUser utilisateur, Integer racine) {
		if (avenantDocument != null && utilisateur != null) {
			// Suppression du lien entre l'avenant et le document
			// EODocument document = avenantDocument.document();
			Avenant avenant = avenantDocument.avenant();
			avenant.removeFromAvenantDocumentsRelationship(avenantDocument);
			edc.deleteObject(avenantDocument);
		}
	}

	/**
	 * @param avenant Un {@link Avenant}
	 * @return Le répartoire GEDI
	 */
	public String repertoireGedi(Avenant avenant) {
		String repertoireGedi = null;
		Operation operation = avenant.operation();
		if (operation.numeroOperation().endsWith("????")) {
			// Le operation n'a pas encore ete enregistre ==> il ne possede pas encore de numero
			repertoireGedi = "TEMPO";
		} else {
			repertoireGedi = operation.numeroOperation() + "/";
			if (avenant.avtIndex().intValue() == 0) {
				repertoireGedi += "Documents principaux";
			} else {
				repertoireGedi += "Avenant nº " + avenant.avtIndex();
			}
		}

		return repertoireGedi;
	}
}
