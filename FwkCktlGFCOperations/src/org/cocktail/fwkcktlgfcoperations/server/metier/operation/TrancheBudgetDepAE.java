package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import static org.cocktail.fwkcktlgfceos.server.metier._EOEb.ORG_CR;
import static org.cocktail.fwkcktlgfceos.server.metier._EOEb.ORG_ETAB;
import static org.cocktail.fwkcktlgfceos.server.metier._EOEb.ORG_SOUSCR;
import static org.cocktail.fwkcktlgfceos.server.metier._EOEb.ORG_UB;
import static org.cocktail.fwkcktlgfceos.server.metier._EOEb.ORG_UNIV;

import java.math.BigDecimal;
import java.util.List;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgfceos.server.exception.IllegalArgumentException;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionApplication;
import org.cocktail.fwkcktlgfcoperations.common.nombre.NombreOperation;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperations;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsParamManager;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.BudgetCombinaisonAxesDepense;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.ConventionsDepensesService;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation.ContratFinancementDepensesValidation;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;

public class TrancheBudgetDepAE extends EOTrancheBudgetDepAE {

    public static final EOQualifier QUAL_NON_SUPPR = ERXQ.equals(TrancheBudgetDepAE.TB_SUPPR_KEY, "N");
    public static final ERXKey<BigDecimal> TOTAL_DEPENSES = new ERXKey<BigDecimal>("totalDepenses");
    public static final NSArray<EOSortOrdering> SORT_DETAILS_DEPENSES_AE = new NSArray<EOSortOrdering>(
    		ORGAN.dot(ORG_UNIV).asc(), ORGAN.dot(ORG_ETAB).asc(), ORGAN.dot(ORG_UB).asc(),  ORGAN.dot(ORG_CR).asc(),  ORGAN.dot(ORG_SOUSCR).asc(),
    		DESTINATION_DEPENSE.asc(), NATURE_DEPENSE.asc(), COMMENTAIRE.asc());
    public static final String MARQUEUR_SUPPRESSION_OUI = "O";

    private static final Logger LOG = Logger.getLogger(TrancheBudgetDepAE.class);
    
    public TrancheBudgetDepAE() {
        super();
    }

    public TrancheBudgetDepAE mergeAvecExistant() {
        TrancheBudgetDepAE result = this;
        
        EOQualifier qual = TRANCHE.eq(tranche())
        		// FIXME Comparer avec la section/natureRec
        		// .and(TYPE_CREDIT.eq(typeCredit()))
        		.and(ORGAN.eq(organ()))
        		.and(QUAL_NON_SUPPR);
        TrancheBudgetDepAE existant = TrancheBudgetDepAE.fetch(editingContext(), qual);
        if (existant != null) {
            LOG.info("Va merger une TrancheBudget avec celle existante");
            existant.setTbMontant(existant.tbMontant().add(this.tbMontant()));
            // On supprime l'objet courant
            this.tranche().removeFromTrancheBudgets(this);
            this.setTrancheRelationship(null);
            this.editingContext().deleteObject(this);
            
            result = existant;
        }    
        
        return result;
    }
    
    /** Reprise du mécanisme de budget présent dans Coconuts */
    public static TrancheBudgetDepAE creerDepenseAE(Tranche tranche, BudgetCombinaisonAxesDepense axes, String commentaire, BigDecimal montantAE, Integer persId) {
        if (tranche == null) {
            throw new IllegalArgumentException("La tranche est obligatoire");
        }
        
        TrancheBudgetDepAE trancheBudget = EOTrancheBudgetDepAE.create(tranche.editingContext(), null, null, null, null, null, null, null, null);
        trancheBudget.setTbSuppr("N");
        trancheBudget.setTbDateCreation(new NSTimestamp());
        trancheBudget.setTrancheRelationship(tranche);
        trancheBudget.setOrgan(axes.getEntiteBudgetaire());
        trancheBudget.setDestinationDepenseRelationship(axes.getDestination());
        trancheBudget.setNatureDepenseRelationship(axes.getNature());
        trancheBudget.setEnveloppeBudgetaireRelationship(axes.getEnveloppe());
        trancheBudget.setCommentaire(commentaire);
        trancheBudget.setTbMontant(montantAE);
        trancheBudget.setPersIdCreation(persId);
        return trancheBudget;
    }
    
    @Deprecated
    public static void supprimerTrancheBudget(EOEditingContext ec, TrancheBudgetDepAE trancheBudget, VCreditsPositionnes creditsDepensesPos) {
        // On vérifie avant que le montant dépensé soit égal à 0
        if (trancheBudget.totalDepenses().signum() > 0) {
            NSNumberFormatter formatter = new NombreOperation().getFormatterMoney();
            String msg =    "Op\u00E9ration impossible : \n" 
                            + "Des d\u00E9penses ont \u00E9t\u00E9 r\u00E9alis\u00E9es \u00E0 hauteur de "
                            + formatter.format(trancheBudget.totalDepenses()) + " sur ces cr\u00E9dits positionn\u00E9s.\n"
                            + "Il n'est donc pas permis de supprimer ces cr\u00E9dits.";
            throw new ValidationException(msg);
        }
        // Enfin là c'est safe, on historise et on marque si besoin la TrancheBudget à supprimer
        trancheBudget.setTbMontant(BigDecimal.valueOf(0));
        trancheBudget.supprimer(null);
    }

    @Deprecated
    public static void supprimerLigneCreditDepense(EOEditingContext ec, EOExercice exericeOuvertCourant, VCreditsPositionnes creditsDepensesPos, TrancheBudgetDepAE trancheBudget) {
        EOExerciceCocktail exerciceSelectionne = creditsDepensesPos.tranche().exerciceCocktail();
        supprimerTrancheBudget(ec, trancheBudget, creditsDepensesPos);
    }
    
    public static EOQualifier qualifierTranchesBudgetsActives(Tranche tranche, EOEb organ, EOTypeCredit typeCredit) {
        return TrancheBudgetDepAE.ORGAN.eq(organ).
        		// FIXME Ajouter nature/destination/enveloppe/...
                // and(TrancheBudget.TYPE_CREDIT.eq(typeCredit)).
                and(TrancheBudgetDepAE.TRANCHE.eq(tranche)).
                and(TrancheBudgetDepAE.QUAL_NON_SUPPR);
    }
    
    public static boolean existeAbsenceTrancheBudget(List<TrancheBudgetDepAE> tranchesBudgets) {
        return tranchesBudgets.size() < 1;
    }

    public static boolean existeDoublonsTranchesBudgets(List<TrancheBudgetDepAE> tranchesBudgets) {       
        return tranchesBudgets.size() > 1;    
    }
    
    /**
     * Traitement des doublons de TrancheBudget : 
     * On supprime toutes les tranches existantes 
     * et on recree une nouvelle tranche dont le montant est égal au montant positionné dans l'histo de la tranche.
     * @param tranchesBudgets les tranches budgets a supprimer.
     * @param montantCreditsPositionnes le montant de la nouvelle tranche budget remplacant les autres supprimées.
     * @param persId l'utilisateur a l'origine de l'operation.
     * @deprecated Faut arreter avec ces hacks
     */
    @Deprecated
     public static void corrigerDoublonsTranchesBudgets(List<TrancheBudgetDepAE> tranchesBudgets, BigDecimal montantCreditsPositionnes, Integer persId) {
        TrancheBudgetDepAE derniereTrancheBudget = tranchesBudgets.get(tranchesBudgets.size() - 1);
        
        TrancheBudgetDepAE newTrancheBudget = TrancheBudgetDepAE.creerDepenseAE(
            derniereTrancheBudget.tranche(),
            new BudgetCombinaisonAxesDepense(null, null, null, null),
            null,
            BigDecimal.ZERO,
            persId);
        
        newTrancheBudget.setTbMontant(montantCreditsPositionnes);
        newTrancheBudget.setOrgan(derniereTrancheBudget.organ());
        // FIXME Ajouter nature/destination/origine/enveloppe 
        // newTrancheBudget.setTypeCredit(derniereTrancheBudget.typeCredit());
        
        for (TrancheBudgetDepAE currentTrancheBudget : tranchesBudgets) {
            //currentTrancheBudget.supprimer(persId);
        }
    }
    
    /**
     * Supprime une tranche budget en poitionnant son montant a 0 et le flag 'Suppr' a Oui.
     * @param persId l'utilisateur a l'origine de l'operation.
     */
    @Deprecated
    public void supprimer(Integer persId) {
        setTbMontant(BigDecimal.ZERO);
        setTbSuppr(MARQUEUR_SUPPRESSION_OUI);
        setTbDateModif(new NSTimestamp());
        if (persId != null) {
            setPersIdModif(persId);
        }
    }
    
    public void marquerCommeSupprimer(Integer persId) {
    	setTbSuppr(MARQUEUR_SUPPRESSION_OUI);
        setTbDateModif(new NSTimestamp());
        if (persId != null) {
            setPersIdModif(persId);
        }
    }
    
    /**
     * <p>Fournit le total des depenses existantes sur les memes exercice, convention, type de credit et LB que cet objet. 
     * <p>Ce total des depenses est : total restant engage + total liquide.
     * <p>Fait appel a une procedure stoquee.
     * @return Le total des depenses ou <code>null</code> si probleme.
     * @throws ExceptionApplication 
     */
    @Deprecated
    public BigDecimal totalDepenses() {
        BigDecimal totalDepense=null;
        if (!isNewObject()) {
        	totalDepense = ConventionsDepensesService.creerNouvelleInstance()
        			.totalDepensesPourConventionSurExerciceTypeCreditEtOrgan(tranche().operation(), tranche().exercice(), typeCredit(), organ());
        }
        return totalDepense;
    }
    
    public EOTypeCredit typeCredit() {
		throw new UnsupportedOperationException("DEPRECATED : on n'utilise plus de type de crédit, mec.");
	}

	public BigDecimal totalResteEngage() {
        BigDecimal totalResteEngage=null;
        if (!isNewObject()) {
        	totalResteEngage = ConventionsDepensesService.creerNouvelleInstance().totalResteEngagePourConventionSurExerciceTypeCreditEtOrgan(tranche().operation(), tranche().exercice(), typeCredit(), organ());
        }
        return totalResteEngage;
    }

    /**
     * Fournit le "reste a depenser" par rapport au total positionne, 
     * cad la difference entre le total positionne et le total des depenses existantes.
     * @see #totalDepenses()
     * @return Le montant restant a depenser ou <code>null</code> si probleme lors de l'obtention du total des depenses.
     * @throws ExceptionApplication 
     */
    public BigDecimal resteADepenser() {
        BigDecimal totalDepenses = ConventionsDepensesService.creerNouvelleInstance().totalDepensesPlusResteEngagePourConventionSurExerciceTypeCreditEtOrgan(tranche().operation(), tranche().exercice(), typeCredit(), organ()); 
        if (totalDepenses != null)
            return tbMontant().subtract(totalDepenses);
        else
            return null;
    }
    
    public BudgetCombinaisonAxesDepense combinaisonAxes() {
    	return new BudgetCombinaisonAxesDepense(organ(), destinationDepense(), natureDepense(), enveloppeBudgetaire());
    }
 
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (!NombreOperation.montantEstPositif(tbMontant())) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, TB_MONTANT_KEY, tbMontant(), "tbMontantPositif");
        }
        
        if (tranche().operation().isModeRA()) {
	        // Si le crédit positionné dépasse le montant de la tranche
	        if (tbMontant().compareTo(tranche().montant()) == 1) {
	            throw ERXValidationFactory.defaultFactory().createCustomException(this, TB_MONTANT_KEY, tbMontant(), "MontantTooBig");
	        }
            // Si on est dans le cadre d'une convention RA, on vérifie qu'il n'y ait pas déjà une trancheBudget sur un autre
            // organ
	        // Si multi-ligne par conv. RA non activé
	        String isMultiLBAutorise = FwkCktlGFCOperations.paramManager.getParam(FwkCktlGFCOperationsParamManager.MULTI_LB_POUR_CONV_RA_AUTORISE);
			if (!CktlParamManager.booleanForValue(isMultiLBAutorise)) {
	            NSArray<EOEb> allOrgansDep = (NSArray<EOEb>) tranche().trancheBudgetsNonSupprimes().valueForKey(ORGAN_KEY);
	            allOrgansDep = ERXArrayUtilities.arrayWithoutDuplicates(allOrgansDep);
	            if (allOrgansDep.count() > 1) {
	                throw ERXValidationFactory.defaultFactory().createCustomException(this, "MultipleOrgansRA");
	            }
	        }
        } else {
            ContratFinancementDepensesValidation financementValidation = new ContratFinancementDepensesValidation();
    		if (!financementValidation.isSatisfiedBy(tranche().operation())) {
    		    BigDecimal totalContributionsAPositionner = tranche().operation().totalContributionsAPositionner();
                BigDecimal totalPositionneContrat = tranche().operation().totalPositionneOperation();
                
                ERXValidationException ex = ERXValidationFactory.defaultFactory().createCustomException(this, "MontantContributionsMoinsFgTropBasPourTotalPositionne");
                NSMutableDictionary<String, String> replacements = new NSMutableDictionary<String, String>();
                replacements.takeValueForKey(totalContributionsAPositionner, "contrib");
                replacements.takeValueForKey(totalPositionneContrat, "creditpos");
                ex.setContext(replacements);
                throw ex;
        	}
        }
    }
}
