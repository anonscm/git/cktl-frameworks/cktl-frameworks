// DO NOT EDIT.  Make changes to RepartOperationAxeStrategique.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartOperationAxeStrategique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartOperationAxeStrategique";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_OPERATION_AXE_STRAT";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> AXE_STRATEGIQUE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique>("axeStrategique");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation> OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation>("operation");

  // Attributes
  // Relationships
  public static final String AXE_STRATEGIQUE_KEY = AXE_STRATEGIQUE.key();
  public static final String OPERATION_KEY = OPERATION.key();

  private static Logger LOG = Logger.getLogger(EORepartOperationAxeStrategique.class);

  public RepartOperationAxeStrategique localInstanceIn(EOEditingContext editingContext) {
    RepartOperationAxeStrategique localInstance = (RepartOperationAxeStrategique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique axeStrategique() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique)storedValueForKey(EORepartOperationAxeStrategique.AXE_STRATEGIQUE_KEY);
  }
  
  public void setAxeStrategique(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique value) {
    takeStoredValueForKey(value, EORepartOperationAxeStrategique.AXE_STRATEGIQUE_KEY);
  }

  public void setAxeStrategiqueRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique value) {
    if (EORepartOperationAxeStrategique.LOG.isDebugEnabled()) {
      EORepartOperationAxeStrategique.LOG.debug("updating axeStrategique from " + axeStrategique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAxeStrategique(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique oldValue = axeStrategique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartOperationAxeStrategique.AXE_STRATEGIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartOperationAxeStrategique.AXE_STRATEGIQUE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation operation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation)storedValueForKey(EORepartOperationAxeStrategique.OPERATION_KEY);
  }
  
  public void setOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    takeStoredValueForKey(value, EORepartOperationAxeStrategique.OPERATION_KEY);
  }

  public void setOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    if (EORepartOperationAxeStrategique.LOG.isDebugEnabled()) {
      EORepartOperationAxeStrategique.LOG.debug("updating operation from " + operation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation oldValue = operation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartOperationAxeStrategique.OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartOperationAxeStrategique.OPERATION_KEY);
    }
  }
  

  public static RepartOperationAxeStrategique create(EOEditingContext editingContext, org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique axeStrategique, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation operation) {
    RepartOperationAxeStrategique eo = (RepartOperationAxeStrategique) EOUtilities.createAndInsertInstance(editingContext, EORepartOperationAxeStrategique.ENTITY_NAME);    
    eo.setAxeStrategiqueRelationship(axeStrategique);
    eo.setOperationRelationship(operation);
    return eo;
  }

  public static ERXFetchSpecification<RepartOperationAxeStrategique> fetchSpec() {
    return new ERXFetchSpecification<RepartOperationAxeStrategique>(EORepartOperationAxeStrategique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartOperationAxeStrategique> fetchAll(EOEditingContext editingContext) {
    return EORepartOperationAxeStrategique.fetchAll(editingContext, null);
  }

  public static NSArray<RepartOperationAxeStrategique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartOperationAxeStrategique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartOperationAxeStrategique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartOperationAxeStrategique> fetchSpec = new ERXFetchSpecification<RepartOperationAxeStrategique>(EORepartOperationAxeStrategique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartOperationAxeStrategique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartOperationAxeStrategique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartOperationAxeStrategique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartOperationAxeStrategique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartOperationAxeStrategique> eoObjects = EORepartOperationAxeStrategique.fetchAll(editingContext, qualifier, null);
    RepartOperationAxeStrategique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartOperationAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartOperationAxeStrategique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartOperationAxeStrategique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartOperationAxeStrategique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartOperationAxeStrategique eoObject = EORepartOperationAxeStrategique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartOperationAxeStrategique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartOperationAxeStrategique localInstanceIn(EOEditingContext editingContext, RepartOperationAxeStrategique eo) {
    RepartOperationAxeStrategique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}