// DO NOT EDIT.  Make changes to UtilisateurFonctionContrat.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOUtilisateurFonctionContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeUtilisateurFonctionContrat";
	public static final String ENTITY_TABLE_NAME = "GFC.UTILISATEUR_FONCT_CONTRAT";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation> CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation>("contrat");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOFonction> FONCTION = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOFonction>("fonction");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction> UTILISATEUR_FONCTION = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction>("utilisateurFonction");

  // Attributes
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String FONCTION_KEY = FONCTION.key();
  public static final String UTILISATEUR_FONCTION_KEY = UTILISATEUR_FONCTION.key();

  private static Logger LOG = Logger.getLogger(EOUtilisateurFonctionContrat.class);

  public UtilisateurFonctionContrat localInstanceIn(EOEditingContext editingContext) {
    UtilisateurFonctionContrat localInstance = (UtilisateurFonctionContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation contrat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation)storedValueForKey(EOUtilisateurFonctionContrat.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    takeStoredValueForKey(value, EOUtilisateurFonctionContrat.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    if (EOUtilisateurFonctionContrat.LOG.isDebugEnabled()) {
      EOUtilisateurFonctionContrat.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOUtilisateurFonctionContrat.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOUtilisateurFonctionContrat.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOFonction fonction() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOFonction)storedValueForKey(EOUtilisateurFonctionContrat.FONCTION_KEY);
  }
  
  public void setFonction(org.cocktail.fwkcktlgfceos.server.metier.EOFonction value) {
    takeStoredValueForKey(value, EOUtilisateurFonctionContrat.FONCTION_KEY);
  }

  public void setFonctionRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOFonction value) {
    if (EOUtilisateurFonctionContrat.LOG.isDebugEnabled()) {
      EOUtilisateurFonctionContrat.LOG.debug("updating fonction from " + fonction() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setFonction(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOFonction oldValue = fonction();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOUtilisateurFonctionContrat.FONCTION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOUtilisateurFonctionContrat.FONCTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction utilisateurFonction() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction)storedValueForKey(EOUtilisateurFonctionContrat.UTILISATEUR_FONCTION_KEY);
  }
  
  public void setUtilisateurFonction(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction value) {
    takeStoredValueForKey(value, EOUtilisateurFonctionContrat.UTILISATEUR_FONCTION_KEY);
  }

  public void setUtilisateurFonctionRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction value) {
    if (EOUtilisateurFonctionContrat.LOG.isDebugEnabled()) {
      EOUtilisateurFonctionContrat.LOG.debug("updating utilisateurFonction from " + utilisateurFonction() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurFonction(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction oldValue = utilisateurFonction();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOUtilisateurFonctionContrat.UTILISATEUR_FONCTION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOUtilisateurFonctionContrat.UTILISATEUR_FONCTION_KEY);
    }
  }
  

  public static UtilisateurFonctionContrat create(EOEditingContext editingContext, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation contrat, org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateurFonction utilisateurFonction) {
    UtilisateurFonctionContrat eo = (UtilisateurFonctionContrat) EOUtilities.createAndInsertInstance(editingContext, EOUtilisateurFonctionContrat.ENTITY_NAME);    
    eo.setContratRelationship(contrat);
    eo.setUtilisateurFonctionRelationship(utilisateurFonction);
    return eo;
  }

  public static ERXFetchSpecification<UtilisateurFonctionContrat> fetchSpec() {
    return new ERXFetchSpecification<UtilisateurFonctionContrat>(EOUtilisateurFonctionContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<UtilisateurFonctionContrat> fetchAll(EOEditingContext editingContext) {
    return EOUtilisateurFonctionContrat.fetchAll(editingContext, null);
  }

  public static NSArray<UtilisateurFonctionContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOUtilisateurFonctionContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<UtilisateurFonctionContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<UtilisateurFonctionContrat> fetchSpec = new ERXFetchSpecification<UtilisateurFonctionContrat>(EOUtilisateurFonctionContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<UtilisateurFonctionContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static UtilisateurFonctionContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOUtilisateurFonctionContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static UtilisateurFonctionContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<UtilisateurFonctionContrat> eoObjects = EOUtilisateurFonctionContrat.fetchAll(editingContext, qualifier, null);
    UtilisateurFonctionContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeUtilisateurFonctionContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static UtilisateurFonctionContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOUtilisateurFonctionContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static UtilisateurFonctionContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    UtilisateurFonctionContrat eoObject = EOUtilisateurFonctionContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeUtilisateurFonctionContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static UtilisateurFonctionContrat localInstanceIn(EOEditingContext editingContext, UtilisateurFonctionContrat eo) {
    UtilisateurFonctionContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}