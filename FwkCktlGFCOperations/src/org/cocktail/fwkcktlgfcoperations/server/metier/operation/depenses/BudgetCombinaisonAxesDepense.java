package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import static org.apache.commons.lang.Validate.notNull;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;

public class BudgetCombinaisonAxesDepense implements Comparable<BudgetCombinaisonAxesDepense> {

    private EOEb entiteBudgetaire;
    private EODestinationDepense destination;
    private EONatureDep nature;
    private EOEnveloppeBudgetaire enveloppe;

    public BudgetCombinaisonAxesDepense(EOEb entiteBudgetaire, EODestinationDepense destination, EONatureDep nature,
            EOEnveloppeBudgetaire enveloppe) {
        notNull(entiteBudgetaire);
        notNull(destination);
        notNull(nature);
        notNull(enveloppe);
        this.entiteBudgetaire = entiteBudgetaire;
        this.destination = destination;
        this.nature = nature;
        this.enveloppe = enveloppe;
    }

    public EOEb getEntiteBudgetaire() {
        return entiteBudgetaire;
    }

    public EODestinationDepense getDestination() {
        return destination;
    }

    public EONatureDep getNature() {
        return nature;
    }

    public EOEnveloppeBudgetaire getEnveloppe() {
        return enveloppe;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;

        result = prime * result;
        if (destination != null) {
            result += destination.hashCode();
        }

        result = prime * result;
        if (entiteBudgetaire != null) {
            result += entiteBudgetaire.hashCode();
        }

        result = prime * result;
        if (enveloppe != null) {
            result += enveloppe.hashCode();
        }

        result = prime * result;
        if (nature != null) {
            result += nature.hashCode();
        }
        return result;
    }

    @Override
	public String toString() {
    	String[] axesString = new String[] {entiteBudgetaire.getShortString(), destination.libelle(), nature.libelle()};
		return "[ Combinaison axes : " +  StringUtils.join(axesString, ", ") + "]";
	}

	@Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        BudgetCombinaisonAxesDepense other = (BudgetCombinaisonAxesDepense) obj;
        if (destination == null) {
            if (other.destination != null) {
                return false;
            }
        } else if (!destination.equals(other.destination)) {
            return false;
        }
        if (entiteBudgetaire == null) {
            if (other.entiteBudgetaire != null) {
                return false;
            }
        } else if (!entiteBudgetaire.equals(other.entiteBudgetaire)) {
            return false;
        }
        if (enveloppe == null) {
            if (other.enveloppe != null) {
                return false;
            }
        } else if (!enveloppe.equals(other.enveloppe)) {
            return false;
        }
        if (nature == null) {
            if (other.nature != null) {
                return false;
            }
        } else if (!nature.equals(other.nature)) {
            return false;
        }
        return true;
    }

    public int compareTo(BudgetCombinaisonAxesDepense other) {
        String eb1 = this.getEntiteBudgetaire().getLongString();
        String eb2 = other.getEntiteBudgetaire().getLongString();

        if (eb1.equals(eb2)) {
            String dest1 = this.getDestination().libelle();
            String dest2 = other.getDestination().libelle();
            if (dest1.equals(dest2)) {
                String nat1 = this.getNature().libelle();
                String nat2 = other.getNature().libelle();
                return nat1.compareTo(nat2);
            } else {
                return dest1.compareTo(dest2);
            }
        } else {
            return eb1.compareTo(eb2);
        }
    }

}
