package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche.finder;

import org.apache.commons.lang.Validate;
import org.cocktail.fwkcktlgfceos.server.finder.FinderDestinationDepense;
import org.cocktail.fwkcktlgfceos.server.finder.FinderExercice;
import org.cocktail.fwkcktlgfceos.server.finder.FinderNatureDepense;
import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FinderNomenclaturesTrancheDepense {

	public static NSArray<EODestinationDepense> listerDestinations(EOEditingContext edc, Tranche tranche) {
		Validate.notNull(tranche);
		NSArray<EODestinationDepense> listeDestinations = listerDestinationsRattacheesExerciceTranche(edc, tranche);
		if (listeDestinations.isEmpty()) {
			listeDestinations = listerDestinationsRattacheesDernierExerciceOuvertOuEnPreparation(edc);
		}
		return listeDestinations;
	}
	
	private static NSArray<EODestinationDepense> listerDestinationsRattacheesExerciceTranche(EOEditingContext edc, Tranche tranche) {
		Integer exerciceTranche = tranche.exerciceCocktail().exeExercice();
		NSArray<EODestinationDepense> listeDestinations = FinderDestinationDepense.findValides(edc, exerciceTranche);
		return listeDestinations;
	}
	
	private static  NSArray<EODestinationDepense> listerDestinationsRattacheesDernierExerciceOuvertOuEnPreparation(EOEditingContext edc) {
		EOExercice exer = FinderExercice.getDernierExerciceOuvertOuEnPreparation(edc);
		NSArray<EODestinationDepense> listeDestinations = FinderDestinationDepense.findValides(edc, exer.exeExercice());  
		return listeDestinations;
	}
	
	public static NSArray<EONatureDep> listerNatures(EOEditingContext edc, Tranche tranche) {
		Validate.notNull(tranche);
		NSArray<EONatureDep> listeNatures = listerNaturesRattacheesExerciceTranche(edc, tranche);
		if (listeNatures.isEmpty()) {
			listeNatures = listerNaturesRattacheesDernierExerciceOuvertOuEnPreparation(edc);
		}
		return listeNatures;
	}
	
	private static NSArray<EONatureDep> listerNaturesRattacheesExerciceTranche(EOEditingContext edc, Tranche tranche) {
		Integer exerciceTranche = tranche.exerciceCocktail().exeExercice();
		NSArray<EONatureDep> listeNatures = FinderNatureDepense.findByExercice(edc, exerciceTranche);
		return listeNatures;
	}
	
	private static  NSArray<EONatureDep> listerNaturesRattacheesDernierExerciceOuvertOuEnPreparation(EOEditingContext edc) {
		EOExercice exer = FinderExercice.getDernierExerciceOuvertOuEnPreparation(edc);
		NSArray<EONatureDep> listeNatures = FinderNatureDepense.findByExercice(edc, exer.exeExercice());  
		return listeNatures;
	}
}
