package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;

/**
 * <p>
 *  Interface de calcul du montant de la participation disponible pour un partenaire et une tranche<br/>
 *  2 implémentations actuelles disponibles, celle par défaut étant 
 *      <code>CalculMontantParticipationDisponibleFraisExclus</code>
 *  Pour changer d'implémentation, changer le paramètre 
 *      <code>CalculMontantParticipationDisponible</code> dans le fichier Properties.
 * </p>
 * @see CalculMontantParticipationDisponibleFraisExclus
 * @see CalculMontantParticipationDisponibleFraisInclus
 * 
 * @author Alexis Tual
 *
 */
public interface CalculMontantParticipationDisponible {

    /**
     * @param repartPartenaireTranche le partenaire et la tranche dont on veut calculer la participation
     * @return le calcul du montant de la participation disponible
     */
    public BigDecimal montantParticipationDisponible(RepartPartenaireTranche repartPartenaireTranche);
    
}
