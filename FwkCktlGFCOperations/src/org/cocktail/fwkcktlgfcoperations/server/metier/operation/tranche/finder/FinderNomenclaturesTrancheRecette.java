package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche.finder;

import org.cocktail.fwkcktlgfceos.server.finder.FinderExercice;
import org.cocktail.fwkcktlgfceos.server.finder.FinderNatureRecette;
import org.cocktail.fwkcktlgfceos.server.finder.FinderOrigineRecette;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureRec;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class FinderNomenclaturesTrancheRecette {

	public static NSArray<EONatureRec> listerNatures(EOEditingContext edc, Tranche tranche) {
		NSArray<EONatureRec> natureRecettes = FinderNatureRecette.findByExercice(edc, tranche.exerciceCocktail().exeExercice());
        if (natureRecettes.isEmpty()) {
            // pas de nature sur l'année de la tranche alors on va chercher l'année non fermée la plus récente
            EOExercice exer = FinderExercice.getDernierExerciceOuvertOuEnPreparation(edc);
            natureRecettes = FinderNatureRecette.findByExercice(edc, exer.exeExercice());
        }
        return natureRecettes;
	}
	
	public static NSArray<EOOrigineRecette> listerOrigines(EOEditingContext edc, Tranche tranche) {
		NSArray<EOOrigineRecette> origineRecettes = FinderOrigineRecette.findByExercice(edc, tranche.exerciceCocktail().exeExercice());
        if (origineRecettes.isEmpty()) {
            // pas de nature sur l'année de la tranche alors on va chercher l'année non fermée la plus récente
            EOExercice exer = FinderExercice.getDernierExerciceOuvertOuEnPreparation(edc);
            origineRecettes = FinderOrigineRecette.findByExercice(edc, exer.exeExercice());
        }
        return origineRecettes;
	}
	
}
