/**
 * 
 */
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionArgument;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.EOOperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Michael HALLER, Consortium Cocktail, 2008
 */
public class FactoryOperationPartenaire extends Factory {
	final static public Integer STRATEGIE_MONTANT_APPORTE_REMPLACER = new Integer(0);
	final static public Integer STRATEGIE_MONTANT_APPORTE_AJOUTER = new Integer(1);

	/**
	 * @param ec
	 * @param withtrace
	 */
	public FactoryOperationPartenaire(EOEditingContext ec, Boolean withtrace) {
		super(ec, withtrace.booleanValue());

	}

	/**
	 * Creation d'un nouveau partenariat (objet OperationPartenaire)
	 * 
	 * @param operation Operation concerne.
	 * @param personne Personne physique ou morale partenaire
	 * @param typePartenaire Type du partenariat
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public OperationPartenaire creerOperationPartenaire(final Operation operation, final IPersonne personne, final NSArray roles, final Boolean partenairePrincipal,
			final BigDecimal montantApporte) throws ExceptionUtilisateur, Exception {
		return creerOperationPartenaire(operation, personne, roles, null,// dateDebut,
				null,// dateFin,
				partenairePrincipal, montantApporte, null); // rasCommentaire
	}

	/**
	 * Creation d'un nouveau partenariat (objet OperationPartenaire)
	 * 
	 * @param operation Operation concerne.
	 * @param personne Personne physique ou morale partenaire
	 * @param typePartenaire Type du partenariat
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public OperationPartenaire creerOperationPartenaire(final Operation operation, final IPersonne personne, final NSArray roles, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final String rasCommentaire) throws ExceptionUtilisateur, Exception {
		return creerOperationPartenaire(operation, personne, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, rasCommentaire, null);
	}

	/**
	 * Creation d'un nouveau partenariat (objet OperationPartenaire)
	 * 
	 * @param operation Operation concerne.
	 * @param personne Personne physique ou morale partenaire
	 * @param typePartenaire Type du partenariat
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @param rasQuotite la quotite du repart association
	 * @return L'objet AvenantPartenaire cree.
	 * @throws InstantiationException
	 * @throws ExceptionArgument
	 */
	public OperationPartenaire creerOperationPartenaire(final Operation operation, final IPersonne personne, final NSArray roles, final NSTimestamp dateDebut, final NSTimestamp dateFin,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final String rasCommentaire, final BigDecimal rasQuotite) throws ExceptionUtilisateur, Exception {

		trace("creerOperationPartenaire()");
		// trace(operation);
		// trace(personne);
		trace(roles);
		trace(partenairePrincipal);
		trace(montantApporte);

		if (operation == null)
			throw new ExceptionUtilisateur("Le operation doit \u00EAtre fourni pour cr\u00E9er un partenariat.");
		if (personne == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour cr\u00E9er un partenariat.");

		NSArray<OperationPartenaire> operationPartenaires = operation.operationPartenaires(OperationPartenaire.OPERATION.eq(operation).and(OperationPartenaire.PERS_ID.eq(personne.persId())));
		OperationPartenaire operationPartenaire = null;
		if (operationPartenaires.isEmpty()) {
			operationPartenaire = (OperationPartenaire) Factory.instanceForEntity(ec, EOOperationPartenaire.ENTITY_NAME);
		} else {
			operationPartenaire = operationPartenaires.get(0);
		}

		ec.insertObject(operationPartenaire);

		modifierOperationPartenaire(operationPartenaire, operation, personne, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, STRATEGIE_MONTANT_APPORTE_REMPLACER,
				rasCommentaire, rasQuotite);

		NSArray tranches = operation.tranches();
		Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
		while (enumTranches.hasMoreElements()) {
			Tranche tranche = (Tranche) enumTranches.nextElement();
			RepartPartenaireTranche rpt = RepartPartenaireTranche.instanciate(ec);
			ec.insertObject(rpt);
			rpt.setMontantParticipation(BigDecimal.ZERO);
			operationPartenaire.addToContributionsRelationship(rpt);
			System.out.println();
			tranche.addToToRepartPartenaireTranchesRelationship(rpt);
		}

		return operationPartenaire;
	}

	/**
	 * Modif d'un partenariat.
	 * 
	 * @param operationPartenaire Partenariat a modifier.
	 * @param operation Operation concerne.
	 * @param partenaire Personne physique ou morale partenaire
	 * @param roles Liste des roles du partenaire
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte Strategie a appliquer concernant le montant apporte par le partenaire : remplacer l'ancien montant par celui
	 *            specifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER} ) ; augmenter le monatnt existant de celui
	 *            sepcifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER} ).
	 * @throws ExceptionUtilisateur ,Exception
	 * @throws ExceptionArgument Strategie
	 */
	public void modifierOperationPartenaire(final OperationPartenaire operationPartenaire, final Operation operation, final IPersonne partenaire, final NSArray roles,
			final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte) throws ExceptionUtilisateur, Exception {

		modifierOperationPartenaire(operationPartenaire, operation, partenaire, roles, null, null, partenairePrincipal, montantApporte, strategiePourMontantApporte, null);
	}

	/**
	 * Modif d'un partenariat.
	 * 
	 * @param operationPartenaire Partenariat a modifier.
	 * @param operation Operation concerne.
	 * @param partenaire Personne physique ou morale partenaire
	 * @param roles Liste des roles du partenaire
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte Strategie a appliquer concernant le montant apporte par le partenaire : remplacer l'ancien montant par celui
	 *            specifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER} ) ; augmenter le monatnt existant de celui
	 *            sepcifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER} ).
	 * @throws ExceptionUtilisateur ,Exception
	 * @throws ExceptionArgument Strategie
	 */
	public void modifierOperationPartenaire(final OperationPartenaire operationPartenaire, final Operation operation, final IPersonne partenaire, final NSArray roles,
			final NSTimestamp dateDebut, final NSTimestamp dateFin, final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte,
			final String rasCommentaire) throws ExceptionUtilisateur, Exception {
		modifierOperationPartenaire(operationPartenaire, operation, partenaire, roles, dateDebut, dateFin, partenairePrincipal, montantApporte, strategiePourMontantApporte, rasCommentaire, null);
	}

	/**
	 * Modif d'un partenariat.
	 * 
	 * @param operationPartenaire Partenariat a modifier.
	 * @param operation Operation concerne.
	 * @param partenaire Personne physique ou morale partenaire
	 * @param roles Liste des roles du partenaire
	 * @param partenairePrincipal <code>true</code> pour le declarer comme partenaire principal
	 * @param montantApporte Montant apporte par ce partenaire.
	 * @param strategiePourMontantApporte Strategie a appliquer concernant le montant apporte par le partenaire : remplacer l'ancien montant par celui
	 *            specifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_REMPLACER} ) ; augmenter le monatnt existant de celui
	 *            sepcifie (utiliser {@link FactoryAvenantPartenaire#STRATEGIE_MONTANT_APPORTE_AJOUTER} ).
	 * @param rasQuotite La quotite de la repart association
	 * @throws ExceptionUtilisateur ,Exception
	 * @throws ExceptionArgument Strategie
	 */
	public void modifierOperationPartenaire(final OperationPartenaire operationPartenaire, final Operation operation, final IPersonne partenaire, final NSArray roles,
			final NSTimestamp dateDebut, final NSTimestamp dateFin, final Boolean partenairePrincipal, final BigDecimal montantApporte, final Integer strategiePourMontantApporte,
			final String rasCommentaire, final BigDecimal rasQuotite) throws ExceptionUtilisateur, Exception {

		trace("modifierOperationPartenaire()");
		partenaire.persId();
		if (operationPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat doit \u00EAtre fourni pour modifier un partenariat.");
		if (operation == null)
			throw new ExceptionUtilisateur("Le operation doit \u00EAtre fourni pour modifier un partenariat.");
		if (partenaire == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour modifier un partenariat.");
		if (strategiePourMontantApporte == null)
			throw new ExceptionUtilisateur("La strat\u00E9gie pour le montant apport\u00E9 doit \u00EAtre fournie.");
		if (!strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_REMPLACER) && !strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_AJOUTER))
			throw new ExceptionUtilisateur("Strat\u00E9gie inconnue pour le montant apport\u00E9.");

		operationPartenaire.setOperationRelationship(operation);
		operationPartenaire.setPartenaire(partenaire);
		// operationPartenaire.setTypePartenaireRelationship(typePartenaire);

		operationPartenaire.setCpPrincipalBoolean(partenairePrincipal != null ? partenairePrincipal.booleanValue() : false);
		Integer persid = null;
		if (operation.utilisateurModif() != null)
			persid = operation.utilisateurModif().personne().persId();
		else
			persid = operation.utilisateurCreation().personne().persId();
		EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec, partenaire, operation.groupePartenaire(), persid, true);
		if (roles != null) {
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation association = (EOAssociation) enumRoles.nextElement();

				NSTimestamp aDebut = dateDebut;
				NSTimestamp aFin = dateFin;
				if (aDebut == null || aFin == null) {
					aDebut = operation.dateDebut();
					aFin = operation.dateFin();
				} else {
					if (dateDebut.before(operation.dateDebut())) {
						aDebut = operation.dateDebut();
					}
					if (aFin.after(operation.dateFin())) {
						aFin = operation.dateFin();
					}
				}

				partenaire.definitUnRole(ec, association, operation.groupePartenaire(), persid, aDebut, aFin, rasCommentaire, rasQuotite, null, true);
			}
		}
		BigDecimal nouveauMontant = null;

		if (strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_REMPLACER))
			nouveauMontant = montantApporte;
		else if (strategiePourMontantApporte.equals(STRATEGIE_MONTANT_APPORTE_AJOUTER))
			nouveauMontant = operationPartenaire.cpMontant().add(montantApporte);

		operationPartenaire.setCpMontant(nouveauMontant);
	}

	/**
	 * Suppression d'un partenariat.
	 * 
	 * @param operationPartenaire Partenariat a supprimer.
	 * @param persIdUtilisateur persId de la personne effectuant la suppression
	 * @throws Exception exception
	 * @throws ExceptionUtilisateur exception
	 */
	public void supprimerOperationPartenaire(final OperationPartenaire operationPartenaire, Integer persIdUtilisateur) throws Exception, ExceptionUtilisateur {

		if (operationPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat à supprimer n'est pas renseigne");

		trace("supprimerOperationPartenaire()");
		trace(operationPartenaire);

		EOEditingContext edc = operationPartenaire.editingContext();
		NSMutableArray persIds = new NSMutableArray();
		// Eliminer les contacts du partenaires
		NSArray contacts = operationPartenaire.operationPartContacts().immutableClone();
		if (contacts != null && contacts.count() > 0) {
			try {
				Enumeration enumContacts = contacts.objectEnumerator();
				while (enumContacts.hasMoreElements()) {
					OperationPartContact unContact = (OperationPartContact) enumContacts.nextElement();
					persIds.addObject(unContact.persIdContact());
					operationPartenaire.removeObjectFromBothSidesOfRelationshipWithKey(unContact, "operationPartContacts");
					edc.deleteObject(unContact);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		// Suppression des repartPartenaireTranches 
		NSArray contributions = operationPartenaire.contributions().immutableClone();
		Enumeration<RepartPartenaireTranche> enumContributions = contributions.objectEnumerator();
		while (enumContributions.hasMoreElements()) {
			RepartPartenaireTranche repartPartenaireTranche = (RepartPartenaireTranche) enumContributions.nextElement();
			operationPartenaire.removeFromContributionsRelationship(repartPartenaireTranche);
			ec.deleteObject(repartPartenaireTranche);
		}
		// NSArray tranches = operationPartenaire.operation().tranches();
		// Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
		// while (enumTranches.hasMoreElements()) {
		// Tranche tranche = (Tranche) enumTranches.nextElement();
		// NSArray rpt = tranche.toRepartPartenaireTranches();
		// EOKeyValueQualifier qualifier = new
		// EOKeyValueQualifier(RepartPartenaireTranche.OPERATION_PARTENAIRE_KEY,EOQualifier.QualifierOperatorEqual,operationPartenaire);
		// rpt = EOQualifier.filteredArrayWithQualifier(rpt, qualifier);
		// Enumeration<RepartPartenaireTranche> enumRpt =
		// rpt.immutableClone().objectEnumerator();
		// while (enumRpt.hasMoreElements()) {
		// RepartPartenaireTranche repartPartenaireTranche =
		// (RepartPartenaireTranche) enumRpt.nextElement();
		// tranche.removeObjectFromBothSidesOfRelationshipWithKey(repartPartenaireTranche,
		// Tranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
		// ec.deleteObject(repartPartenaireTranche);
		// }
		// }

		// Eliminer le partenaire du groupe des partenaires
		Operation operation = operationPartenaire.operation();
		// if (operationPartenaire.cpPrincipalBoolean()) {
		// operation.setEtablissementRelationship(null);
		// }
		EOStructure groupe = operation.groupePartenaire();
		EOStructureForGroupeSpec.supprimerPersonneDuGroupe(operation.editingContext(), operationPartenaire.partenaire(), groupe, persIdUtilisateur);

		Integer persid = null;
		if (operation.utilisateurModif() != null)
			persid = operation.utilisateurModif().personne().persId();
		else
			persid = operation.utilisateurCreation().personne().persId();

		persIds.addObject(operationPartenaire.persId());
		operation.removeObjectFromBothSidesOfRelationshipWithKey(operationPartenaire, "operationPartenaires");
		edc.deleteObject(operationPartenaire);

		if (operationPartenaire.partenaire().globalID().isTemporary()) {
			edc.forgetObject(operationPartenaire.partenaire());
		}

	}

	public void ajouterContact(OperationPartenaire operationPartenaire, IPersonne personne, NSArray roles) throws ExceptionUtilisateur, Exception {

		if (operationPartenaire == null)
			throw new ExceptionUtilisateur("Le partenariat auquel vous tentez d'ajouter un partenaire n'est pas renseigne");
		if (personne == null)
			throw new ExceptionUtilisateur("Le contact n'est pas renseigne");
		// if(typeContact==null)
		// throw new Exception("Il faut un type de contact");

		IPersonne partenaire = operationPartenaire.partenaire();
		//		JB : un stagiaire de FC est contact d'un operationPartenaire qui peut etre une personne physique
		//		if (partenaire.isStructure()) {
		Operation operation = operationPartenaire.operation();

		OperationPartContact cpc = (OperationPartContact) Factory.instanceForEntity(ec, OperationPartContact.ENTITY_NAME);

		ec.insertObject(cpc);

		cpc.setOperationPartenaireRelationship(operationPartenaire);
		cpc.setPersonne(personne);
		cpc.setPartenaire(partenaire);
		// cpc.setTypeContact(typeContact);

		Integer persid = null;
		if (operation.utilisateurModif() != null)
			persid = operation.utilisateurModif().personne().persId();
		else
			persid = operation.utilisateurCreation().personne().persId();

		operationPartenaire.addToOperationPartContactsRelationship(cpc);
		EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec, personne, operation.groupePartenaire(), persid, true);

		if (roles != null) {
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation association = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(ec, association, operation.groupePartenaire(), persid, operation.dateDebut(), operation.dateFin(), null, null, null, true);
			}
		}
		NSMutableDictionary userInfo = new NSMutableDictionary();
		userInfo.setObjectForKey(ec, "edc");
		NSNotificationCenter.defaultCenter().postNotification("refreshContactsNotification", cpc, userInfo);

		//		} else
		//			throw new ExceptionUtilisateur("Seules les personnes morales peuvent avoir des contacts");

	}

	/**
	 * Suppression d'un contact.
	 * 
	 * @param contact OperationPartContact a supprimer.
	 * @param partenaire OperationPartenaire a supprimer.
	 * @param persIdUtilisateur PersId de la personne effectuant la suppression
	 * @throws Exception exception
	 * @throws ExceptionUtilisateur exception
	 */
	public void supprimerContact(final OperationPartContact contact, OperationPartenaire partenaire, Integer persIdUtilisateur) throws Exception, ExceptionUtilisateur {

		if (contact == null)
			throw new ExceptionUtilisateur("Le contact doit \u00EAtre fourni pour le supprimer.");
		if (partenaire == null)
			throw new ExceptionUtilisateur("Le partenaire doit \u00EAtre fourni pour supprimer le contact.");

		trace("supprimerOperationPartContact()");
		trace(contact);

		// Eliminer le Contact du groupe des partenaires
		EOStructureForGroupeSpec.supprimerPersonneDuGroupe(ec, contact.personne(), partenaire.operation().groupePartenaire(), persIdUtilisateur);
		if (contact.personne().equals(partenaire.operation().centreResponsabilite())) {
			partenaire.operation().setCentreResponsabiliteRelationship(null, persIdUtilisateur);
		}
		partenaire.removeFromOperationPartContactsRelationship(contact);

		ec.deleteObject(contact);

		NSMutableDictionary userInfo = new NSMutableDictionary();
		userInfo.setObjectForKey(ec, "edc");
		NSNotificationCenter.defaultCenter().postNotification("refreshContactsNotification", null, userInfo);
	}

	/**
	 * Renvoie un Boolean pour indiquer si un partenaire joue un role dans le groupe des partenaires associe au operation
	 * 
	 * @param partenaire
	 * @param role
	 * @param operation
	 * @return
	 */
	public Boolean partenaireARoleDansOperation(IPersonne partenaire, EOAssociation role, Operation operation) {
		NSArray repartAssociations = partenaire.getRepartAssociationsInGroupe(operation.groupePartenaire(), null);
		if (repartAssociations == null)
			return Boolean.FALSE;
		if (repartAssociations.isEmpty())
			return Boolean.FALSE;
		Enumeration<EORepartAssociation> enumRepartAssociations = repartAssociations.objectEnumerator();
		while (enumRepartAssociations.hasMoreElements()) {
			EORepartAssociation repartAssociation = enumRepartAssociations.nextElement();
			if (repartAssociation.toAssociation().equals(role))
				return Boolean.TRUE;
		}

		return Boolean.FALSE;
	}

	protected boolean isAssociationsInRepartAssociation(EOAssociation role, EORepartAssociation repartAssociation) {
		return repartAssociation.toAssociation().equals(role);
	}

	/**
	 * Renvoie un Boolean pour indiquer si un partenaire joue un des roles dans le groupe des partenaires associe au operation
	 * 
	 * @param partenaire
	 * @param role
	 * @param operation
	 * @return
	 */
	public Boolean partenaireARolesDansOperation(IPersonne partenaire, NSArray<EOAssociation> roles, Operation operation) {
		NSArray repartAssociations = partenaire.getRepartAssociationsInGroupe(operation.groupePartenaire(), null);
		if (repartAssociations == null)
			return Boolean.FALSE;
		if (repartAssociations.isEmpty())
			return Boolean.FALSE;
		Enumeration<EORepartAssociation> enumRepartAssociations = repartAssociations.objectEnumerator();
		while (enumRepartAssociations.hasMoreElements()) {
			EORepartAssociation repartAssociation = enumRepartAssociations.nextElement();
			Enumeration<EOAssociation> lesRoles = roles.objectEnumerator();
			while (lesRoles.hasMoreElements()) {
				if (isAssociationsInRepartAssociation(lesRoles.nextElement(), repartAssociation))
					return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	/**
	 * Ajout d'un role a un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void ajouterLeRole(OperationPartenaire partenaire, EOAssociation role) {
		if (partenaire != null && role != null) {
			ajouterLesRoles(partenaire, new NSArray(role));
		}
	}

	public void ajouterLeRolePourLesDates(OperationPartenaire partenaire, EOAssociation role, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (partenaire != null && role != null) {
			ajouterLesRolesPourLesDates(partenaire, new NSArray(role), dateDebut, dateFin);
		}
	}

	public void ajouterLeRolePourLesDatesEtQuotite(OperationPartenaire partenaire, EOAssociation role, NSTimestamp dateDebut, NSTimestamp dateFin, BigDecimal quotite) {
		if (partenaire != null && role != null) {
			ajouterLesRolesPourLesDatesEtQuotite(partenaire, new NSArray(role), dateDebut, dateFin, quotite);
		}
	}

	/**
	 * Ajout de roles a un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void ajouterLesRoles(OperationPartenaire partenaire, NSArray roles) {
		if (partenaire != null && roles != null) {
			Operation operation = partenaire.operation();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, operation.groupePartenaire(), operation.utilisateurModif().personne().persId(), operation.dateDebut(),
						operation.dateFin(), null, null, null, true);
			}
		}
	}

	public void ajouterLesRolesPourLesDates(OperationPartenaire partenaire, NSArray roles, NSTimestamp dateDebut, NSTimestamp dateFin) {
		if (partenaire != null && roles != null) {
			Operation operation = partenaire.operation();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, operation.groupePartenaire(), operation.utilisateurModif().personne().persId(), dateDebut, dateFin, null,
						null, null, true);
			}
		}
	}

	public void ajouterLesRolesPourLesDatesEtQuotite(OperationPartenaire partenaire, NSArray roles, NSTimestamp dateDebut, NSTimestamp dateFin, BigDecimal quotite) {
		if (partenaire != null && roles != null) {
			Operation operation = partenaire.operation();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.definitUnRole(personne.editingContext(), unRole, operation.groupePartenaire(), operation.utilisateurModif().personne().persId(), dateDebut, dateFin, null,
						quotite, null, true);
			}
		}
	}

	/**
	 * Suppression du role d'un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param unRole
	 */
	public void supprimerLeRole(OperationPartenaire partenaire, EOAssociation unRole) {
		supprimerLesRoles(partenaire, new NSArray(unRole));
	}

	/**
	 * Suppression des roles d'un partenaire au sein du groupe des Partenaires
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void supprimerLesRoles(OperationPartenaire partenaire, NSArray roles) {
		if (partenaire != null && roles != null) {
			Operation operation = partenaire.operation();
			IPersonne personne = partenaire.partenaire();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.supprimerLesRoles(ec, unRole, operation.groupePartenaire(), operation.utilisateurModif().toPersonne().persId());
			}
		}
	}

	/**
	 * Suppression du roles d'un contact d'un partenaire
	 * 
	 * @param partenaire
	 * @param unRole
	 */
	public void supprimerLeRoleContact(OperationPartenaire partenaire, OperationPartContact cpc, EOAssociation unRole) {
		supprimerLesRolesContact(partenaire, cpc, new NSArray(unRole));
	}

	/**
	 * Suppression des roles d'un contact d'un partenaire
	 * 
	 * @param partenaire
	 * @param roles
	 */
	public void supprimerLesRolesContact(OperationPartenaire partenaire, OperationPartContact cpc, NSArray roles) {
		if (partenaire != null && cpc != null && roles != null) {
			Operation operation = partenaire.operation();
			IPersonne personne = cpc.personne();
			Enumeration<EOAssociation> enumRoles = roles.objectEnumerator();
			while (enumRoles.hasMoreElements()) {
				EOAssociation unRole = (EOAssociation) enumRoles.nextElement();
				personne.supprimerLesRoles(ec, unRole, operation.groupePartenaire(), operation.utilisateurModif().toPersonne().persId());
			}
		}
	}

}
