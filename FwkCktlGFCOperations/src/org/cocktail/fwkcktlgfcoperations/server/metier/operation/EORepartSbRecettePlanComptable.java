// DO NOT EDIT.  Make changes to RepartSbRecettePlanComptable.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartSbRecettePlanComptable extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartSbRecettePlanComptable";
	public static final String ENTITY_TABLE_NAME = "GFC.REPART_SB_RECETTE_PLANCO";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT_HT = new ERXKey<java.math.BigDecimal>("montantHt");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> SB_RECETTE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>("sbRecette");

  // Attributes
  public static final String MONTANT_HT_KEY = MONTANT_HT.key();
  // Relationships
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String SB_RECETTE_KEY = SB_RECETTE.key();

  private static Logger LOG = Logger.getLogger(EORepartSbRecettePlanComptable.class);

  public RepartSbRecettePlanComptable localInstanceIn(EOEditingContext editingContext) {
    RepartSbRecettePlanComptable localInstance = (RepartSbRecettePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montantHt() {
    return (java.math.BigDecimal) storedValueForKey(EORepartSbRecettePlanComptable.MONTANT_HT_KEY);
  }

  public void setMontantHt(java.math.BigDecimal value) {
    if (EORepartSbRecettePlanComptable.LOG.isDebugEnabled()) {
        EORepartSbRecettePlanComptable.LOG.debug( "updating montantHt from " + montantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartSbRecettePlanComptable.MONTANT_HT_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable)storedValueForKey(EORepartSbRecettePlanComptable.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, EORepartSbRecettePlanComptable.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable value) {
    if (EORepartSbRecettePlanComptable.LOG.isDebugEnabled()) {
      EORepartSbRecettePlanComptable.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbRecettePlanComptable.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbRecettePlanComptable.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette sbRecette() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette)storedValueForKey(EORepartSbRecettePlanComptable.SB_RECETTE_KEY);
  }
  
  public void setSbRecette(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    takeStoredValueForKey(value, EORepartSbRecettePlanComptable.SB_RECETTE_KEY);
  }

  public void setSbRecetteRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    if (EORepartSbRecettePlanComptable.LOG.isDebugEnabled()) {
      EORepartSbRecettePlanComptable.LOG.debug("updating sbRecette from " + sbRecette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSbRecette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette oldValue = sbRecette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbRecettePlanComptable.SB_RECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbRecettePlanComptable.SB_RECETTE_KEY);
    }
  }
  

  public static RepartSbRecettePlanComptable create(EOEditingContext editingContext, org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable planco, org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette sbRecette) {
    RepartSbRecettePlanComptable eo = (RepartSbRecettePlanComptable) EOUtilities.createAndInsertInstance(editingContext, EORepartSbRecettePlanComptable.ENTITY_NAME);    
    eo.setPlancoRelationship(planco);
    eo.setSbRecetteRelationship(sbRecette);
    return eo;
  }

  public static ERXFetchSpecification<RepartSbRecettePlanComptable> fetchSpec() {
    return new ERXFetchSpecification<RepartSbRecettePlanComptable>(EORepartSbRecettePlanComptable.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartSbRecettePlanComptable> fetchAll(EOEditingContext editingContext) {
    return EORepartSbRecettePlanComptable.fetchAll(editingContext, null);
  }

  public static NSArray<RepartSbRecettePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartSbRecettePlanComptable.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartSbRecettePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartSbRecettePlanComptable> fetchSpec = new ERXFetchSpecification<RepartSbRecettePlanComptable>(EORepartSbRecettePlanComptable.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartSbRecettePlanComptable> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartSbRecettePlanComptable fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbRecettePlanComptable.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbRecettePlanComptable fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartSbRecettePlanComptable> eoObjects = EORepartSbRecettePlanComptable.fetchAll(editingContext, qualifier, null);
    RepartSbRecettePlanComptable eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartSbRecettePlanComptable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbRecettePlanComptable fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbRecettePlanComptable.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbRecettePlanComptable fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartSbRecettePlanComptable eoObject = EORepartSbRecettePlanComptable.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartSbRecettePlanComptable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbRecettePlanComptable localInstanceIn(EOEditingContext editingContext, RepartSbRecettePlanComptable eo) {
    RepartSbRecettePlanComptable localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}