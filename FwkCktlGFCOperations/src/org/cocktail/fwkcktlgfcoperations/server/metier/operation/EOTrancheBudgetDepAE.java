// DO NOT EDIT.  Make changes to TrancheBudgetDepAE.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTrancheBudgetDepAE extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTrancheBudgetDepAE";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_TRANCHE_BUD_DEP_AE";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIF = new ERXKey<Integer>("persIdModif");
  public static final ERXKey<NSTimestamp> TB_DATE_CREATION = new ERXKey<NSTimestamp>("tbDateCreation");
  public static final ERXKey<NSTimestamp> TB_DATE_MODIF = new ERXKey<NSTimestamp>("tbDateModif");
  public static final ERXKey<java.math.BigDecimal> TB_MONTANT = new ERXKey<java.math.BigDecimal>("tbMontant");
  public static final ERXKey<String> TB_SUPPR = new ERXKey<String>("tbSuppr");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe>("budgetPrevisionOperationTrancheDepenseAes");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> DESTINATION_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("destinationDepense");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire> ENVELOPPE_BUDGETAIRE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire>("enveloppeBudgetaire");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> NATURE_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep>("natureDepense");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIF_KEY = PERS_ID_MODIF.key();
  public static final String TB_DATE_CREATION_KEY = TB_DATE_CREATION.key();
  public static final String TB_DATE_MODIF_KEY = TB_DATE_MODIF.key();
  public static final String TB_MONTANT_KEY = TB_MONTANT.key();
  public static final String TB_SUPPR_KEY = TB_SUPPR.key();
  // Relationships
  public static final String BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY = BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES.key();
  public static final String DESTINATION_DEPENSE_KEY = DESTINATION_DEPENSE.key();
  public static final String ENVELOPPE_BUDGETAIRE_KEY = ENVELOPPE_BUDGETAIRE.key();
  public static final String NATURE_DEPENSE_KEY = NATURE_DEPENSE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOTrancheBudgetDepAE.class);

  public TrancheBudgetDepAE localInstanceIn(EOEditingContext editingContext) {
    TrancheBudgetDepAE localInstance = (TrancheBudgetDepAE)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(EOTrancheBudgetDepAE.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.COMMENTAIRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOTrancheBudgetDepAE.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModif() {
    return (Integer) storedValueForKey(EOTrancheBudgetDepAE.PERS_ID_MODIF_KEY);
  }

  public void setPersIdModif(Integer value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating persIdModif from " + persIdModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.PERS_ID_MODIF_KEY);
  }

  public NSTimestamp tbDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetDepAE.TB_DATE_CREATION_KEY);
  }

  public void setTbDateCreation(NSTimestamp value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating tbDateCreation from " + tbDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.TB_DATE_CREATION_KEY);
  }

  public NSTimestamp tbDateModif() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetDepAE.TB_DATE_MODIF_KEY);
  }

  public void setTbDateModif(NSTimestamp value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating tbDateModif from " + tbDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.TB_DATE_MODIF_KEY);
  }

  public java.math.BigDecimal tbMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetDepAE.TB_MONTANT_KEY);
  }

  public void setTbMontant(java.math.BigDecimal value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating tbMontant from " + tbMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.TB_MONTANT_KEY);
  }

  public String tbSuppr() {
    return (String) storedValueForKey(EOTrancheBudgetDepAE.TB_SUPPR_KEY);
  }

  public void setTbSuppr(String value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepAE.LOG.debug( "updating tbSuppr from " + tbSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.TB_SUPPR_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(EOTrancheBudgetDepAE.DESTINATION_DEPENSE_KEY);
  }
  
  public void setDestinationDepense(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.DESTINATION_DEPENSE_KEY);
  }

  public void setDestinationDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("updating destinationDepense from " + destinationDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDestinationDepense(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = destinationDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepAE.DESTINATION_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepAE.DESTINATION_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire enveloppeBudgetaire() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire)storedValueForKey(EOTrancheBudgetDepAE.ENVELOPPE_BUDGETAIRE_KEY);
  }
  
  public void setEnveloppeBudgetaire(org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.ENVELOPPE_BUDGETAIRE_KEY);
  }

  public void setEnveloppeBudgetaireRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("updating enveloppeBudgetaire from " + enveloppeBudgetaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEnveloppeBudgetaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire oldValue = enveloppeBudgetaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepAE.ENVELOPPE_BUDGETAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepAE.ENVELOPPE_BUDGETAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EONatureDep natureDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EONatureDep)storedValueForKey(EOTrancheBudgetDepAE.NATURE_DEPENSE_KEY);
  }
  
  public void setNatureDepense(org.cocktail.fwkcktlgfceos.server.metier.EONatureDep value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.NATURE_DEPENSE_KEY);
  }

  public void setNatureDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDep value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("updating natureDepense from " + natureDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setNatureDepense(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EONatureDep oldValue = natureDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepAE.NATURE_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepAE.NATURE_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOTrancheBudgetDepAE.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepAE.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepAE.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOTrancheBudgetDepAE.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepAE.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepAE.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepAE.TRANCHE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> budgetPrevisionOperationTrancheDepenseAes() {
    return (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe>)storedValueForKey(EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> budgetPrevisionOperationTrancheDepenseAes(EOQualifier qualifier) {
    return budgetPrevisionOperationTrancheDepenseAes(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> budgetPrevisionOperationTrancheDepenseAes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> results;
      results = budgetPrevisionOperationTrancheDepenseAes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToBudgetPrevisionOperationTrancheDepenseAes(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe object) {
    includeObjectIntoPropertyWithKey(object, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
  }

  public void removeFromBudgetPrevisionOperationTrancheDepenseAes(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe object) {
    excludeObjectFromPropertyWithKey(object, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
  }

  public void addToBudgetPrevisionOperationTrancheDepenseAesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe object) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("adding " + object + " to budgetPrevisionOperationTrancheDepenseAes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToBudgetPrevisionOperationTrancheDepenseAes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
    }
  }

  public void removeFromBudgetPrevisionOperationTrancheDepenseAesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe object) {
    if (EOTrancheBudgetDepAE.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepAE.LOG.debug("removing " + object + " from budgetPrevisionOperationTrancheDepenseAes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromBudgetPrevisionOperationTrancheDepenseAes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe createBudgetPrevisionOperationTrancheDepenseAesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe) eo;
  }

  public void deleteBudgetPrevisionOperationTrancheDepenseAesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBudgetPrevisionOperationTrancheDepenseAesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe> objects = budgetPrevisionOperationTrancheDepenseAes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBudgetPrevisionOperationTrancheDepenseAesRelationship(objects.nextElement());
    }
  }


  public static TrancheBudgetDepAE create(EOEditingContext editingContext, Integer persIdCreation
, NSTimestamp tbDateCreation
, java.math.BigDecimal tbMontant
, org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense, org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire enveloppeBudgetaire, org.cocktail.fwkcktlgfceos.server.metier.EONatureDep natureDepense, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    TrancheBudgetDepAE eo = (TrancheBudgetDepAE) EOUtilities.createAndInsertInstance(editingContext, EOTrancheBudgetDepAE.ENTITY_NAME);    
        eo.setPersIdCreation(persIdCreation);
        eo.setTbDateCreation(tbDateCreation);
        eo.setTbMontant(tbMontant);
    eo.setDestinationDepenseRelationship(destinationDepense);
    eo.setEnveloppeBudgetaireRelationship(enveloppeBudgetaire);
    eo.setNatureDepenseRelationship(natureDepense);
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<TrancheBudgetDepAE> fetchSpec() {
    return new ERXFetchSpecification<TrancheBudgetDepAE>(EOTrancheBudgetDepAE.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TrancheBudgetDepAE> fetchAll(EOEditingContext editingContext) {
    return EOTrancheBudgetDepAE.fetchAll(editingContext, null);
  }

  public static NSArray<TrancheBudgetDepAE> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTrancheBudgetDepAE.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TrancheBudgetDepAE> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TrancheBudgetDepAE> fetchSpec = new ERXFetchSpecification<TrancheBudgetDepAE>(EOTrancheBudgetDepAE.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TrancheBudgetDepAE> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TrancheBudgetDepAE fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetDepAE.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetDepAE fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TrancheBudgetDepAE> eoObjects = EOTrancheBudgetDepAE.fetchAll(editingContext, qualifier, null);
    TrancheBudgetDepAE eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTrancheBudgetDepAE that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetDepAE fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetDepAE.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetDepAE fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TrancheBudgetDepAE eoObject = EOTrancheBudgetDepAE.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTrancheBudgetDepAE that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetDepAE localInstanceIn(EOEditingContext editingContext, TrancheBudgetDepAE eo) {
    TrancheBudgetDepAE localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}