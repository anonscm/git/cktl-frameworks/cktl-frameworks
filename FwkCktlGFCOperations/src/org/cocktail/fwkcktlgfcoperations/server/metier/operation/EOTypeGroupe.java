// DO NOT EDIT.  Make changes to TypeGroupe.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeGroupe extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTypeGroupe";
	public static final String ENTITY_TABLE_NAME = "GFC.TYPE_GROUPE";

  // Attribute Keys
  public static final ERXKey<String> TGPE_CODE = new ERXKey<String>("tgpeCode");
  // Relationship Keys

  // Attributes
  public static final String TGPE_CODE_KEY = TGPE_CODE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeGroupe.class);

  public TypeGroupe localInstanceIn(EOEditingContext editingContext) {
    TypeGroupe localInstance = (TypeGroupe)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tgpeCode() {
    return (String) storedValueForKey(EOTypeGroupe.TGPE_CODE_KEY);
  }

  public void setTgpeCode(String value) {
    if (EOTypeGroupe.LOG.isDebugEnabled()) {
        EOTypeGroupe.LOG.debug( "updating tgpeCode from " + tgpeCode() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeGroupe.TGPE_CODE_KEY);
  }


  public static TypeGroupe create(EOEditingContext editingContext) {
    TypeGroupe eo = (TypeGroupe) EOUtilities.createAndInsertInstance(editingContext, EOTypeGroupe.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<TypeGroupe> fetchSpec() {
    return new ERXFetchSpecification<TypeGroupe>(EOTypeGroupe.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeGroupe> fetchAll(EOEditingContext editingContext) {
    return EOTypeGroupe.fetchAll(editingContext, null);
  }

  public static NSArray<TypeGroupe> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeGroupe.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeGroupe> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeGroupe> fetchSpec = new ERXFetchSpecification<TypeGroupe>(EOTypeGroupe.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeGroupe> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeGroupe fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeGroupe.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeGroupe fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeGroupe> eoObjects = EOTypeGroupe.fetchAll(editingContext, qualifier, null);
    TypeGroupe eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeGroupe fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeGroupe.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeGroupe fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeGroupe eoObject = EOTypeGroupe.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTypeGroupe that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeGroupe localInstanceIn(EOEditingContext editingContext, TypeGroupe eo) {
    TypeGroupe localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}