// DO NOT EDIT.  Make changes to SbRecette.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOSbRecette extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeSbRecette";
	public static final String ENTITY_TABLE_NAME = "GFC.SB_RECETTE";

  // Attribute Keys
  public static final ERXKey<Long> CAN_ID = new ERXKey<Long>("canId");
  public static final ERXKey<Integer> CLIENT_PERS_ID = new ERXKey<Integer>("clientPersId");
  public static final ERXKey<String> SR_BLOCAGE = new ERXKey<String>("srBlocage");
  public static final ERXKey<NSTimestamp> SR_DATE_BLOC = new ERXKey<NSTimestamp>("srDateBloc");
  public static final ERXKey<NSTimestamp> SR_DATE_PREV = new ERXKey<NSTimestamp>("srDatePrev");
  public static final ERXKey<NSTimestamp> SR_DATE_REAL = new ERXKey<NSTimestamp>("srDateReal");
  public static final ERXKey<NSTimestamp> SR_DATE_VALID = new ERXKey<NSTimestamp>("srDateValid");
  public static final ERXKey<NSTimestamp> SR_DATE_VALID_PART = new ERXKey<NSTimestamp>("srDateValidPart");
  public static final ERXKey<String> SR_LIBELLE = new ERXKey<String>("srLibelle");
  public static final ERXKey<java.math.BigDecimal> SR_MNT_VALID_HT = new ERXKey<java.math.BigDecimal>("srMntValidHt");
  public static final ERXKey<java.math.BigDecimal> SR_MONTANT_HT = new ERXKey<java.math.BigDecimal>("srMontantHt");
  public static final ERXKey<String> SR_MOTIF_BLOC = new ERXKey<String>("srMotifBloc");
  public static final ERXKey<java.math.BigDecimal> SR_PCT_CONTRIB = new ERXKey<java.math.BigDecimal>("srPctContrib");
  public static final ERXKey<java.math.BigDecimal> SR_PCT_IMPOTS = new ERXKey<java.math.BigDecimal>("srPctImpots");
  public static final ERXKey<java.math.BigDecimal> SR_PCT_VALID_HT = new ERXKey<java.math.BigDecimal>("srPctValidHt");
  public static final ERXKey<String> SR_VALID = new ERXKey<String>("srValid");
  public static final ERXKey<String> SR_VALID_PART = new ERXKey<String>("srValidPart");
  public static final ERXKey<Integer> UTL_ORDRE_BLOC = new ERXKey<Integer>("utlOrdreBloc");
  public static final ERXKey<Integer> UTL_ORDRE_VALID = new ERXKey<Integer>("utlOrdreValid");
  public static final ERXKey<Integer> UTL_ORDRE_VALID_PART = new ERXKey<Integer>("utlOrdreValidPart");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> LOLF = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette>("lolf");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> SB_DEPENSES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>("sbDepenses");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTva> TVA = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTva>("tva");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String CAN_ID_KEY = CAN_ID.key();
  public static final String CLIENT_PERS_ID_KEY = CLIENT_PERS_ID.key();
  public static final String SR_BLOCAGE_KEY = SR_BLOCAGE.key();
  public static final String SR_DATE_BLOC_KEY = SR_DATE_BLOC.key();
  public static final String SR_DATE_PREV_KEY = SR_DATE_PREV.key();
  public static final String SR_DATE_REAL_KEY = SR_DATE_REAL.key();
  public static final String SR_DATE_VALID_KEY = SR_DATE_VALID.key();
  public static final String SR_DATE_VALID_PART_KEY = SR_DATE_VALID_PART.key();
  public static final String SR_LIBELLE_KEY = SR_LIBELLE.key();
  public static final String SR_MNT_VALID_HT_KEY = SR_MNT_VALID_HT.key();
  public static final String SR_MONTANT_HT_KEY = SR_MONTANT_HT.key();
  public static final String SR_MOTIF_BLOC_KEY = SR_MOTIF_BLOC.key();
  public static final String SR_PCT_CONTRIB_KEY = SR_PCT_CONTRIB.key();
  public static final String SR_PCT_IMPOTS_KEY = SR_PCT_IMPOTS.key();
  public static final String SR_PCT_VALID_HT_KEY = SR_PCT_VALID_HT.key();
  public static final String SR_VALID_KEY = SR_VALID.key();
  public static final String SR_VALID_PART_KEY = SR_VALID_PART.key();
  public static final String UTL_ORDRE_BLOC_KEY = UTL_ORDRE_BLOC.key();
  public static final String UTL_ORDRE_VALID_KEY = UTL_ORDRE_VALID.key();
  public static final String UTL_ORDRE_VALID_PART_KEY = UTL_ORDRE_VALID_PART.key();
  // Relationships
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String LOLF_KEY = LOLF.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String SB_DEPENSES_KEY = SB_DEPENSES.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TVA_KEY = TVA.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOSbRecette.class);

  public SbRecette localInstanceIn(EOEditingContext editingContext) {
    SbRecette localInstance = (SbRecette)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Long canId() {
    return (Long) storedValueForKey(EOSbRecette.CAN_ID_KEY);
  }

  public void setCanId(Long value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating canId from " + canId() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.CAN_ID_KEY);
  }

  public Integer clientPersId() {
    return (Integer) storedValueForKey(EOSbRecette.CLIENT_PERS_ID_KEY);
  }

  public void setClientPersId(Integer value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating clientPersId from " + clientPersId() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.CLIENT_PERS_ID_KEY);
  }

  public String srBlocage() {
    return (String) storedValueForKey(EOSbRecette.SR_BLOCAGE_KEY);
  }

  public void setSrBlocage(String value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srBlocage from " + srBlocage() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_BLOCAGE_KEY);
  }

  public NSTimestamp srDateBloc() {
    return (NSTimestamp) storedValueForKey(EOSbRecette.SR_DATE_BLOC_KEY);
  }

  public void setSrDateBloc(NSTimestamp value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srDateBloc from " + srDateBloc() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_DATE_BLOC_KEY);
  }

  public NSTimestamp srDatePrev() {
    return (NSTimestamp) storedValueForKey(EOSbRecette.SR_DATE_PREV_KEY);
  }

  public void setSrDatePrev(NSTimestamp value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srDatePrev from " + srDatePrev() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_DATE_PREV_KEY);
  }

  public NSTimestamp srDateReal() {
    return (NSTimestamp) storedValueForKey(EOSbRecette.SR_DATE_REAL_KEY);
  }

  public void setSrDateReal(NSTimestamp value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srDateReal from " + srDateReal() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_DATE_REAL_KEY);
  }

  public NSTimestamp srDateValid() {
    return (NSTimestamp) storedValueForKey(EOSbRecette.SR_DATE_VALID_KEY);
  }

  public void setSrDateValid(NSTimestamp value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srDateValid from " + srDateValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_DATE_VALID_KEY);
  }

  public NSTimestamp srDateValidPart() {
    return (NSTimestamp) storedValueForKey(EOSbRecette.SR_DATE_VALID_PART_KEY);
  }

  public void setSrDateValidPart(NSTimestamp value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srDateValidPart from " + srDateValidPart() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_DATE_VALID_PART_KEY);
  }

  public String srLibelle() {
    return (String) storedValueForKey(EOSbRecette.SR_LIBELLE_KEY);
  }

  public void setSrLibelle(String value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srLibelle from " + srLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_LIBELLE_KEY);
  }

  public java.math.BigDecimal srMntValidHt() {
    return (java.math.BigDecimal) storedValueForKey(EOSbRecette.SR_MNT_VALID_HT_KEY);
  }

  public void setSrMntValidHt(java.math.BigDecimal value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srMntValidHt from " + srMntValidHt() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_MNT_VALID_HT_KEY);
  }

  public java.math.BigDecimal srMontantHt() {
    return (java.math.BigDecimal) storedValueForKey(EOSbRecette.SR_MONTANT_HT_KEY);
  }

  public void setSrMontantHt(java.math.BigDecimal value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srMontantHt from " + srMontantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_MONTANT_HT_KEY);
  }

  public String srMotifBloc() {
    return (String) storedValueForKey(EOSbRecette.SR_MOTIF_BLOC_KEY);
  }

  public void setSrMotifBloc(String value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srMotifBloc from " + srMotifBloc() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_MOTIF_BLOC_KEY);
  }

  public java.math.BigDecimal srPctContrib() {
    return (java.math.BigDecimal) storedValueForKey(EOSbRecette.SR_PCT_CONTRIB_KEY);
  }

  public void setSrPctContrib(java.math.BigDecimal value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srPctContrib from " + srPctContrib() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_PCT_CONTRIB_KEY);
  }

  public java.math.BigDecimal srPctImpots() {
    return (java.math.BigDecimal) storedValueForKey(EOSbRecette.SR_PCT_IMPOTS_KEY);
  }

  public void setSrPctImpots(java.math.BigDecimal value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srPctImpots from " + srPctImpots() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_PCT_IMPOTS_KEY);
  }

  public java.math.BigDecimal srPctValidHt() {
    return (java.math.BigDecimal) storedValueForKey(EOSbRecette.SR_PCT_VALID_HT_KEY);
  }

  public void setSrPctValidHt(java.math.BigDecimal value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srPctValidHt from " + srPctValidHt() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_PCT_VALID_HT_KEY);
  }

  public String srValid() {
    return (String) storedValueForKey(EOSbRecette.SR_VALID_KEY);
  }

  public void setSrValid(String value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srValid from " + srValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_VALID_KEY);
  }

  public String srValidPart() {
    return (String) storedValueForKey(EOSbRecette.SR_VALID_PART_KEY);
  }

  public void setSrValidPart(String value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating srValidPart from " + srValidPart() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.SR_VALID_PART_KEY);
  }

  public Integer utlOrdreBloc() {
    return (Integer) storedValueForKey(EOSbRecette.UTL_ORDRE_BLOC_KEY);
  }

  public void setUtlOrdreBloc(Integer value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating utlOrdreBloc from " + utlOrdreBloc() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.UTL_ORDRE_BLOC_KEY);
  }

  public Integer utlOrdreValid() {
    return (Integer) storedValueForKey(EOSbRecette.UTL_ORDRE_VALID_KEY);
  }

  public void setUtlOrdreValid(Integer value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating utlOrdreValid from " + utlOrdreValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.UTL_ORDRE_VALID_KEY);
  }

  public Integer utlOrdreValidPart() {
    return (Integer) storedValueForKey(EOSbRecette.UTL_ORDRE_VALID_PART_KEY);
  }

  public void setUtlOrdreValidPart(Integer value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
        EOSbRecette.LOG.debug( "updating utlOrdreValidPart from " + utlOrdreValidPart() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbRecette.UTL_ORDRE_VALID_PART_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail)storedValueForKey(EOSbRecette.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOSbRecette.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette lolf() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette)storedValueForKey(EOSbRecette.LOLF_KEY);
  }
  
  public void setLolf(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette value) {
    takeStoredValueForKey(value, EOSbRecette.LOLF_KEY);
  }

  public void setLolfRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating lolf from " + lolf() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setLolf(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette oldValue = lolf();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.LOLF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.LOLF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOSbRecette.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOSbRecette.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer)storedValueForKey(EOSbRecette.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOSbRecette.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOSbRecette.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOSbRecette.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTva tva() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTva)storedValueForKey(EOSbRecette.TVA_KEY);
  }
  
  public void setTva(org.cocktail.fwkcktlgfceos.server.metier.EOTva value) {
    takeStoredValueForKey(value, EOSbRecette.TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTva value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating tva from " + tva() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTva(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTva oldValue = tva();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.TVA_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.TVA_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit)storedValueForKey(EOSbRecette.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOSbRecette.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbRecette.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbRecette.TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)storedValueForKey(EOSbRecette.SB_DEPENSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier) {
    return sbDepenses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier, boolean fetch) {
    return sbDepenses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.SB_RECETTE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sbDepenses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSbDepenses(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    includeObjectIntoPropertyWithKey(object, EOSbRecette.SB_DEPENSES_KEY);
  }

  public void removeFromSbDepenses(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    excludeObjectFromPropertyWithKey(object, EOSbRecette.SB_DEPENSES_KEY);
  }

  public void addToSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("adding " + object + " to sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToSbDepenses(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOSbRecette.SB_DEPENSES_KEY);
    }
  }

  public void removeFromSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    if (EOSbRecette.LOG.isDebugEnabled()) {
      EOSbRecette.LOG.debug("removing " + object + " from sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromSbDepenses(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOSbRecette.SB_DEPENSES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense createSbDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOSbRecette.SB_DEPENSES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense) eo;
  }

  public void deleteSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOSbRecette.SB_DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSbDepensesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> objects = sbDepenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSbDepensesRelationship(objects.nextElement());
    }
  }


  public static SbRecette create(EOEditingContext editingContext, String srLibelle
, java.math.BigDecimal srMontantHt
, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche, org.cocktail.fwkcktlgfceos.server.metier.EOTva tva) {
    SbRecette eo = (SbRecette) EOUtilities.createAndInsertInstance(editingContext, EOSbRecette.ENTITY_NAME);    
        eo.setSrLibelle(srLibelle);
        eo.setSrMontantHt(srMontantHt);
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    eo.setTvaRelationship(tva);
    return eo;
  }

  public static ERXFetchSpecification<SbRecette> fetchSpec() {
    return new ERXFetchSpecification<SbRecette>(EOSbRecette.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<SbRecette> fetchAll(EOEditingContext editingContext) {
    return EOSbRecette.fetchAll(editingContext, null);
  }

  public static NSArray<SbRecette> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOSbRecette.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<SbRecette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<SbRecette> fetchSpec = new ERXFetchSpecification<SbRecette>(EOSbRecette.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<SbRecette> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static SbRecette fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOSbRecette.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SbRecette fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<SbRecette> eoObjects = EOSbRecette.fetchAll(editingContext, qualifier, null);
    SbRecette eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeSbRecette that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SbRecette fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOSbRecette.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SbRecette fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    SbRecette eoObject = EOSbRecette.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeSbRecette that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SbRecette localInstanceIn(EOEditingContext editingContext, SbRecette eo) {
    SbRecette localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}