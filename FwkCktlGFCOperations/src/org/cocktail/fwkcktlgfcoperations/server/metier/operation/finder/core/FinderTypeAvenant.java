
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeAvenant;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderTypeAvenant extends Finder 
{
	protected EOQualifier taCodeQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context de travail.
	 */
	public FinderTypeAvenant(EOEditingContext ec) {
		super(ec, TypeAvenant.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du critere.
	 * @param taCode Id interne du type d'avenant.
	 */
	public void setTaCode(final String taCode) {
		this.taCodeQualifier = createQualifier(
				"taCode = %@", 
				taCode);
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#clearAllCriteria()
	 */
	public void clearAllCriteria() {
		this.taCodeQualifier = null;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray findWithCode(final String taCode) throws ExceptionFinder {
		
		if (taCode == null)
			throw new NullPointerException("Un code est requis.");
		
		removeOptionalQualifiers();
		setTaCode(taCode);
		addOptionalQualifier(taCodeQualifier);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray find() throws ExceptionFinder {
		clearAllCriteria();
		addOptionalQualifier(this.taCodeQualifier);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
