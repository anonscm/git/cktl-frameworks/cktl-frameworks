package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;

import com.webobjects.eocontrol.EOEditingContext;

public class AvenantTypeStat extends EOAvenantTypeStat {

	public AvenantTypeStat() {
		super();
	}

	public static AvenantTypeStat instanciate(EOEditingContext ec) throws Exception {
		return (AvenantTypeStat) Factory.instanceForEntity(ec, ENTITY_NAME);
	}
}
