package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgfceos.server.exception.IllegalArgumentException;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.BudgetCombinaisonAxesDepense;

import com.webobjects.foundation.NSTimestamp;

public class TrancheBudgetDepCP extends EOTrancheBudgetDepCP {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(TrancheBudgetDepCP.class);
    
    public static TrancheBudgetDepCP creerDepenseCP(Tranche tranche, BudgetCombinaisonAxesDepense axes, BigDecimal montantCP, Integer persId) {
        if (tranche == null) {
            throw new IllegalArgumentException("La tranche est obligatoire");
        }
        
        TrancheBudgetDepCP depCP = EOTrancheBudgetDepCP.create(tranche.editingContext(), null, null, null, null, null, null, null, null);
        depCP.setTrancheRelationship(tranche);
        depCP.setOrgan(axes.getEntiteBudgetaire());
        depCP.setDestinationDepenseRelationship(axes.getDestination());
        depCP.setNatureDepenseRelationship(axes.getNature());
        depCP.setEnveloppeBudgetaireRelationship(axes.getEnveloppe());
        depCP.setMontantCp(montantCP);
        depCP.setDateCreation(new NSTimestamp());
        depCP.setPersIdCreation(persId);
        
        return depCP;
    }
    
    public void majDepenseCP(BigDecimal montantCP, Integer persId) {
    	setMontantCp(montantCP);
    	setPersIdModif(persId);
    	setDateModification(new NSTimestamp());
    }
}
