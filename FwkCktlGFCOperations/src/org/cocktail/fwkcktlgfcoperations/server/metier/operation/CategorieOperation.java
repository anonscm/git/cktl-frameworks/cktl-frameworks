package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.apache.log4j.Logger;

public class CategorieOperation extends EOCategorieOperation {
	
	public static final String CODE_AVEC_IMPACT_FINANCIER = "FIN";
	public static final String CODE_SANS_IMPACT_FINANCIER = "NFIN";
	public static final String CODE_COMPTE_TIERS = "TIE";
	
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(CategorieOperation.class);
    
    public CategorieOperation() {
    	super();
    }
    
}
