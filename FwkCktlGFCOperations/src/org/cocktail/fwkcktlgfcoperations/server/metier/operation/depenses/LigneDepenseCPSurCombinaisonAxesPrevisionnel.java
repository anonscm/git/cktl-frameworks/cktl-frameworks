package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP;

public class LigneDepenseCPSurCombinaisonAxesPrevisionnel implements LigneDepenseCPSurCombinaisonAxes {

    private Tranche tranche;
    private TrancheBudgetDepCP trancheDepCP;
    private BigDecimal montantAE;
    private BigDecimal montantCP;

    public LigneDepenseCPSurCombinaisonAxesPrevisionnel(Operation operation, BudgetCombinaisonAxesDepense combi, Tranche tranche, TrancheBudgetDepCP trancheDepCP) {
        this.tranche = tranche;
        this.trancheDepCP = trancheDepCP;
        initMontantAE(tranche, combi);
        initMontantCP(trancheDepCP);
    }

    private void initMontantAE(Tranche tranche, BudgetCombinaisonAxesDepense combinaison) {
        this.montantAE = tranche.totalDepenseAESurCombinaison(combinaison);
    }
    
    private void initMontantCP(TrancheBudgetDepCP trancheDepCP) {
    	if (trancheDepCP != null) {
    		this.montantCP = trancheDepCP.montantCp();
    	}
    }

    public Tranche tranche() {
        return this.tranche;
    }

    public BigDecimal montantAE() {
        return montantAE;
    }

    public BigDecimal montantCP() {
        return montantCP;
    }
    
    public void setMontantCP(BigDecimal value) {
        montantCP = value;
    }

    public boolean isPrevisionnel() {
        return true;
    }

	public boolean isMontantCPMisAJour() {
		return isNouvelleSaisie() || existeVariationMontantCP();
	}
	
    public boolean isMontantCPEfface() {
        return trancheDepCP != null && montantCP() == null;
    }
	
	private boolean existeVariationMontantCP() {
		return trancheDepCP != null && (montantCP() == null || montantCP().compareTo(trancheDepCP.montantCp()) != 0);
	}
	
	public boolean isNouvelleSaisie() {
		return trancheDepCP == null && montantCP() != null;
	}

	public TrancheBudgetDepCP getTrancheDepCP() {
		return trancheDepCP;
	}

	public void setTrancheDepCP(TrancheBudgetDepCP trancheDepCP) {
		this.trancheDepCP = trancheDepCP;
	}
}
