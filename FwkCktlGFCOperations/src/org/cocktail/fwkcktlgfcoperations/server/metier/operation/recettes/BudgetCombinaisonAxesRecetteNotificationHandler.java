package org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes;

import lombok.Getter;

@Getter
public class BudgetCombinaisonAxesRecetteNotificationHandler {

	private BudgetCombinaisonAxesRecetteValidationResults validationResults;
			
	public BudgetCombinaisonAxesRecetteNotificationHandler() {
		validationResults = new BudgetCombinaisonAxesRecetteValidationResults();
	}
	
	public void marquerEbEnErreur() {
		validationResults.setEbEnErreur(true);
	}
	
	public void marquerNatureEnErreur() {
		validationResults.setNatureEnErreur(true);
	}
	
	public void marquerOrigineEnErreur() {
		validationResults.setOrigineEnErreur(true);
	}
}
