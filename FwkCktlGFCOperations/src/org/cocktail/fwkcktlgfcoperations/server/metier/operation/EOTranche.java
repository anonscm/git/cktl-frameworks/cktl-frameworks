// DO NOT EDIT.  Make changes to Tranche.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTranche extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTranche";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_TRANCHE_BUD";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIF = new ERXKey<Integer>("persIdModif");
  public static final ERXKey<Integer> PERS_ID_VALID = new ERXKey<Integer>("persIdValid");
  public static final ERXKey<java.math.BigDecimal> REPORT_NMOINS1 = new ERXKey<java.math.BigDecimal>("reportNmoins1");
  public static final ERXKey<java.math.BigDecimal> REPORT_NPLUS1 = new ERXKey<java.math.BigDecimal>("reportNplus1");
  public static final ERXKey<NSTimestamp> TRA_DATE_CREATION = new ERXKey<NSTimestamp>("traDateCreation");
  public static final ERXKey<NSTimestamp> TRA_DATE_MODIF = new ERXKey<NSTimestamp>("traDateModif");
  public static final ERXKey<NSTimestamp> TRA_DATE_VALID = new ERXKey<NSTimestamp>("traDateValid");
  public static final ERXKey<Integer> TRA_ORDRE = new ERXKey<Integer>("traOrdre");
  public static final ERXKey<String> TRA_SUPPR = new ERXKey<String>("traSuppr");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> HISTO_CREDIT_POSITIONNES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne>("histoCreditPositionnes");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation> OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation>("operation");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> SB_DEPENSES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>("sbDepenses");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> SB_RECETTES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>("sbRecettes");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> TO_REPART_PARTENAIRE_TRANCHES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>("toRepartPartenaireTranches");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> TRANCHE_BUDGET_DEPS_CP = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP>("trancheBudgetDepsCP");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> TRANCHE_BUDGET_RECS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec>("trancheBudgetRecs");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> TRANCHE_BUDGETS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE>("trancheBudgets");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat> TYPE_ETAT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat>("typeEtat");

  // Attributes
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIF_KEY = PERS_ID_MODIF.key();
  public static final String PERS_ID_VALID_KEY = PERS_ID_VALID.key();
  public static final String REPORT_NMOINS1_KEY = REPORT_NMOINS1.key();
  public static final String REPORT_NPLUS1_KEY = REPORT_NPLUS1.key();
  public static final String TRA_DATE_CREATION_KEY = TRA_DATE_CREATION.key();
  public static final String TRA_DATE_MODIF_KEY = TRA_DATE_MODIF.key();
  public static final String TRA_DATE_VALID_KEY = TRA_DATE_VALID.key();
  public static final String TRA_ORDRE_KEY = TRA_ORDRE.key();
  public static final String TRA_SUPPR_KEY = TRA_SUPPR.key();
  // Relationships
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String HISTO_CREDIT_POSITIONNES_KEY = HISTO_CREDIT_POSITIONNES.key();
  public static final String OPERATION_KEY = OPERATION.key();
  public static final String SB_DEPENSES_KEY = SB_DEPENSES.key();
  public static final String SB_RECETTES_KEY = SB_RECETTES.key();
  public static final String TO_REPART_PARTENAIRE_TRANCHES_KEY = TO_REPART_PARTENAIRE_TRANCHES.key();
  public static final String TRANCHE_BUDGET_DEPS_CP_KEY = TRANCHE_BUDGET_DEPS_CP.key();
  public static final String TRANCHE_BUDGET_RECS_KEY = TRANCHE_BUDGET_RECS.key();
  public static final String TRANCHE_BUDGETS_KEY = TRANCHE_BUDGETS.key();
  public static final String TYPE_ETAT_KEY = TYPE_ETAT.key();

  private static Logger LOG = Logger.getLogger(EOTranche.class);

  public Tranche localInstanceIn(EOEditingContext editingContext) {
    Tranche localInstance = (Tranche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOTranche.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModif() {
    return (Integer) storedValueForKey(EOTranche.PERS_ID_MODIF_KEY);
  }

  public void setPersIdModif(Integer value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating persIdModif from " + persIdModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.PERS_ID_MODIF_KEY);
  }

  public Integer persIdValid() {
    return (Integer) storedValueForKey(EOTranche.PERS_ID_VALID_KEY);
  }

  public void setPersIdValid(Integer value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating persIdValid from " + persIdValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.PERS_ID_VALID_KEY);
  }

  public java.math.BigDecimal reportNmoins1() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.REPORT_NMOINS1_KEY);
  }

  public void setReportNmoins1(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating reportNmoins1 from " + reportNmoins1() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.REPORT_NMOINS1_KEY);
  }

  public java.math.BigDecimal reportNplus1() {
    return (java.math.BigDecimal) storedValueForKey(EOTranche.REPORT_NPLUS1_KEY);
  }

  public void setReportNplus1(java.math.BigDecimal value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating reportNplus1 from " + reportNplus1() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.REPORT_NPLUS1_KEY);
  }

  public NSTimestamp traDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_CREATION_KEY);
  }

  public void setTraDateCreation(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateCreation from " + traDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_CREATION_KEY);
  }

  public NSTimestamp traDateModif() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_MODIF_KEY);
  }

  public void setTraDateModif(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateModif from " + traDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_MODIF_KEY);
  }

  public NSTimestamp traDateValid() {
    return (NSTimestamp) storedValueForKey(EOTranche.TRA_DATE_VALID_KEY);
  }

  public void setTraDateValid(NSTimestamp value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traDateValid from " + traDateValid() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_DATE_VALID_KEY);
  }

  public Integer traOrdre() {
    return (Integer) storedValueForKey(EOTranche.TRA_ORDRE_KEY);
  }

  public void setTraOrdre(Integer value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traOrdre from " + traOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_ORDRE_KEY);
  }

  public String traSuppr() {
    return (String) storedValueForKey(EOTranche.TRA_SUPPR_KEY);
  }

  public void setTraSuppr(String value) {
    if (EOTranche.LOG.isDebugEnabled()) {
        EOTranche.LOG.debug( "updating traSuppr from " + traSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTranche.TRA_SUPPR_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail)storedValueForKey(EOTranche.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOTranche.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation operation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation)storedValueForKey(EOTranche.OPERATION_KEY);
  }
  
  public void setOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    takeStoredValueForKey(value, EOTranche.OPERATION_KEY);
  }

  public void setOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating operation from " + operation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation oldValue = operation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.OPERATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat)storedValueForKey(EOTranche.TYPE_ETAT_KEY);
  }
  
  public void setTypeEtat(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    takeStoredValueForKey(value, EOTranche.TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat value) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("updating typeEtat from " + typeEtat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeEtat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat oldValue = typeEtat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTranche.TYPE_ETAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTranche.TYPE_ETAT_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> histoCreditPositionnes() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne>)storedValueForKey(EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier) {
    return histoCreditPositionnes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier, boolean fetch) {
    return histoCreditPositionnes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> histoCreditPositionnes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = histoCreditPositionnes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToHistoCreditPositionnes(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public void removeFromHistoCreditPositionnes(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
  }

  public void addToHistoCreditPositionnesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to histoCreditPositionnes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToHistoCreditPositionnes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    }
  }

  public void removeFromHistoCreditPositionnesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from histoCreditPositionnes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromHistoCreditPositionnes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne createHistoCreditPositionnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne) eo;
  }

  public void deleteHistoCreditPositionnesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.HISTO_CREDIT_POSITIONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllHistoCreditPositionnesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.HistoCreditPositionne> objects = histoCreditPositionnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteHistoCreditPositionnesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)storedValueForKey(EOTranche.SB_DEPENSES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier) {
    return sbDepenses(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier, boolean fetch) {
    return sbDepenses(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> sbDepenses(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sbDepenses();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSbDepenses(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.SB_DEPENSES_KEY);
  }

  public void removeFromSbDepenses(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.SB_DEPENSES_KEY);
  }

  public void addToSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToSbDepenses(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    }
  }

  public void removeFromSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from sbDepenses relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromSbDepenses(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense createSbDepensesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.SB_DEPENSES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense) eo;
  }

  public void deleteSbDepensesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_DEPENSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSbDepensesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> objects = sbDepenses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSbDepensesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> sbRecettes() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>)storedValueForKey(EOTranche.SB_RECETTES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> sbRecettes(EOQualifier qualifier) {
    return sbRecettes(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> sbRecettes(EOQualifier qualifier, boolean fetch) {
    return sbRecettes(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> sbRecettes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = sbRecettes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToSbRecettes(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.SB_RECETTES_KEY);
  }

  public void removeFromSbRecettes(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.SB_RECETTES_KEY);
  }

  public void addToSbRecettesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to sbRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToSbRecettes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    }
  }

  public void removeFromSbRecettesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from sbRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromSbRecettes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette createSbRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.SB_RECETTES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette) eo;
  }

  public void deleteSbRecettesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.SB_RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllSbRecettesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> objects = sbRecettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteSbRecettesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> toRepartPartenaireTranches() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)storedValueForKey(EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier) {
    return toRepartPartenaireTranches(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier, boolean fetch) {
    return toRepartPartenaireTranches(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> toRepartPartenaireTranches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = toRepartPartenaireTranches();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToToRepartPartenaireTranches(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public void removeFromToRepartPartenaireTranches(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
  }

  public void addToToRepartPartenaireTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to toRepartPartenaireTranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToToRepartPartenaireTranches(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    }
  }

  public void removeFromToRepartPartenaireTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from toRepartPartenaireTranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromToRepartPartenaireTranches(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche createToRepartPartenaireTranchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche) eo;
  }

  public void deleteToRepartPartenaireTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TO_REPART_PARTENAIRE_TRANCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllToRepartPartenaireTranchesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> objects = toRepartPartenaireTranches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteToRepartPartenaireTranchesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> trancheBudgetDepsCP() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP>)storedValueForKey(EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> trancheBudgetDepsCP(EOQualifier qualifier) {
    return trancheBudgetDepsCP(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> trancheBudgetDepsCP(EOQualifier qualifier, boolean fetch) {
    return trancheBudgetDepsCP(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> trancheBudgetDepsCP(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = trancheBudgetDepsCP();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTrancheBudgetDepsCP(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
  }

  public void removeFromTrancheBudgetDepsCP(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
  }

  public void addToTrancheBudgetDepsCPRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to trancheBudgetDepsCP relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTrancheBudgetDepsCP(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
    }
  }

  public void removeFromTrancheBudgetDepsCPRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from trancheBudgetDepsCP relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTrancheBudgetDepsCP(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP createTrancheBudgetDepsCPRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP) eo;
  }

  public void deleteTrancheBudgetDepsCPRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_DEPS_CP_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTrancheBudgetDepsCPRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP> objects = trancheBudgetDepsCP().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTrancheBudgetDepsCPRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> trancheBudgetRecs() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec>)storedValueForKey(EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier) {
    return trancheBudgetRecs(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier, boolean fetch) {
    return trancheBudgetRecs(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> trancheBudgetRecs(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = trancheBudgetRecs();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTrancheBudgetRecs(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public void removeFromTrancheBudgetRecs(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
  }

  public void addToTrancheBudgetRecsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to trancheBudgetRecs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTrancheBudgetRecs(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    }
  }

  public void removeFromTrancheBudgetRecsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from trancheBudgetRecs relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTrancheBudgetRecs(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec createTrancheBudgetRecsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec) eo;
  }

  public void deleteTrancheBudgetRecsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGET_RECS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTrancheBudgetRecsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec> objects = trancheBudgetRecs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTrancheBudgetRecsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> trancheBudgets() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE>)storedValueForKey(EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> trancheBudgets(EOQualifier qualifier) {
    return trancheBudgets(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> trancheBudgets(EOQualifier qualifier, boolean fetch) {
    return trancheBudgets(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> trancheBudgets(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE.TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = trancheBudgets();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTrancheBudgets(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE object) {
    includeObjectIntoPropertyWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public void removeFromTrancheBudgets(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE object) {
    excludeObjectFromPropertyWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
  }

  public void addToTrancheBudgetsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("adding " + object + " to trancheBudgets relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTrancheBudgets(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    }
  }

  public void removeFromTrancheBudgetsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE object) {
    if (EOTranche.LOG.isDebugEnabled()) {
      EOTranche.LOG.debug("removing " + object + " from trancheBudgets relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTrancheBudgets(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE createTrancheBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTranche.TRANCHE_BUDGETS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE) eo;
  }

  public void deleteTrancheBudgetsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTranche.TRANCHE_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTrancheBudgetsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE> objects = trancheBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTrancheBudgetsRelationship(objects.nextElement());
    }
  }


  public static Tranche create(EOEditingContext editingContext, Integer persIdCreation
, java.math.BigDecimal reportNmoins1
, java.math.BigDecimal reportNplus1
, NSTimestamp traDateCreation
, Integer traOrdre
, org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail, org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat typeEtat) {
    Tranche eo = (Tranche) EOUtilities.createAndInsertInstance(editingContext, EOTranche.ENTITY_NAME);    
        eo.setPersIdCreation(persIdCreation);
        eo.setReportNmoins1(reportNmoins1);
        eo.setReportNplus1(reportNplus1);
        eo.setTraDateCreation(traDateCreation);
        eo.setTraOrdre(traOrdre);
    eo.setExerciceCocktailRelationship(exerciceCocktail);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  public static ERXFetchSpecification<Tranche> fetchSpec() {
    return new ERXFetchSpecification<Tranche>(EOTranche.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext) {
    return EOTranche.fetchAll(editingContext, null);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTranche.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Tranche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Tranche> fetchSpec = new ERXFetchSpecification<Tranche>(EOTranche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Tranche> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Tranche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Tranche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Tranche> eoObjects = EOTranche.fetchAll(editingContext, qualifier, null);
    Tranche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Tranche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTranche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Tranche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Tranche eoObject = EOTranche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Tranche localInstanceIn(EOEditingContext editingContext, Tranche eo) {
    Tranche localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}