
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderTypeReconduction extends Finder 
{
	protected String trIdInterne;
	
	protected EOQualifier trIdInterneQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context de travail.
	 */
	public FinderTypeReconduction(EOEditingContext ec) {
		super(ec, TypeReconduction.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du critere.
	 * @param trIdInterne Id interne du type de reconduction.
	 */
	public void setTrIdInterne(final String trIdInterne) {
		this.trIdInterneQualifier = createQualifier(
				"trIdInterne = %@", 
				trIdInterne);
		
		this.trIdInterne = trIdInterne;
	}
	
	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#clearAllCriteria()
	 */
	public void clearAllCriteria() {
		setTrIdInterne(null);
	}


	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray findWithTrIdInterne(final String trIdInterne) throws ExceptionFinder {
		removeOptionalQualifiers();
		setTrIdInterne(trIdInterne);
		addOptionalQualifier(this.trIdInterneQualifier);
		
		return super.find();
	}

	
	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public NSArray find() throws ExceptionFinder {
		removeOptionalQualifiers();
		addOptionalQualifier(this.trIdInterneQualifier);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
