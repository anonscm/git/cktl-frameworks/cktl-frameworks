package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP;

/**
 * Classe qui nous permet de représenter le tableau des AE devant être couverts
 * par des CP
 *
 */
public class DepenseCPSurCombinaisonAxes {

    private final Operation operation;
    private final BudgetCombinaisonAxesDepense combinaisonAxe;
    private final EOExercice exerciceCourant;

    private Map<Integer, LigneDepenseCPSurCombinaisonAxes> mapTrancheAE;

    public DepenseCPSurCombinaisonAxes(Operation ope, BudgetCombinaisonAxesDepense combiAxe, EOExercice exeCourant) {
        this.operation = ope;
        this.combinaisonAxe = combiAxe;
        this.exerciceCourant = exeCourant;

        generateLignes();
    }

    private void generateLignes() {
        mapTrancheAE = new TreeMap<Integer, LigneDepenseCPSurCombinaisonAxes>();
        for (Tranche tranche : operation.tranchesOrdonneesParExerciceAsc()) {
            LigneDepenseCPSurCombinaisonAxes ligne;
            if (exerciceCourant.exeExercice().intValue() > tranche.exerciceCocktail().exeExercice()) {
                ligne = creerLigneCPConsomme(operation, combinaisonAxe, tranche);
            } else {
                ligne = creerLigneCPPrevisionnel(operation, combinaisonAxe, tranche);
            }
            mapTrancheAE.put(tranche.exerciceCocktail().exeExercice(), ligne);
        }
    }
    
    private LigneDepenseCPSurCombinaisonAxes creerLigneCPConsomme(Operation operation, BudgetCombinaisonAxesDepense combi, Tranche tranche) {
    	return new LigneDepenseCPSurCombinaisonAxesConsomme(operation, combi, tranche);	
    }
    
    private LigneDepenseCPSurCombinaisonAxes creerLigneCPPrevisionnel(Operation operation, BudgetCombinaisonAxesDepense combi, Tranche tranche) {
    	TrancheBudgetDepCP depCP = tranche.trancheBudgetsDepCP(combi);
    	return new LigneDepenseCPSurCombinaisonAxesPrevisionnel(operation, combi, tranche, depCP);
    }

    public List<LigneDepenseCPSurCombinaisonAxes> getLignes() {
        return new ArrayList<LigneDepenseCPSurCombinaisonAxes>(mapTrancheAE.values());
    }

    public BigDecimal getTotalMontantAE() {
        BigDecimal totalAE = BigDecimal.ZERO;
        for (LigneDepenseCPSurCombinaisonAxes ligneDepenseCPSurCombinaisonAxes : mapTrancheAE.values()) {
            if (ligneDepenseCPSurCombinaisonAxes.montantAE() != null) {
                totalAE = totalAE.add(ligneDepenseCPSurCombinaisonAxes.montantAE());
            }
        }
        return totalAE;
    }

    public BigDecimal getTotalMontantPaye() {
        BigDecimal totalPaye = BigDecimal.ZERO;
        for (LigneDepenseCPSurCombinaisonAxes ligneDepenseCPSurCombinaisonAxes : mapTrancheAE.values()) {
            if (ligneDepenseCPSurCombinaisonAxes.montantCP() != null) {
                totalPaye = totalPaye.add(ligneDepenseCPSurCombinaisonAxes.montantCP());
            }
        }
        return totalPaye;
    }

    /**
     * @param tranche
     *            Une tranche T
     * @return <pre>
     * ResteAPayer(T - 1) + MontantAE(T) - MontantPaye(T)
     * </pre>
     */
    public BigDecimal getResteAPayer(Tranche tranche) {
        BigDecimal resteAPayer = BigDecimal.ZERO;
        BigDecimal montantAE = BigDecimal.ZERO;
        BigDecimal montantPaye = BigDecimal.ZERO;

        // ResteAPayer(N-1) + MontantAE(N) - MontantPaye(N)
        BigDecimal resteAPayerNmoins1 = getResteAPayerPrecedent(tranche);

        LigneDepenseCPSurCombinaisonAxes ligne = mapTrancheAE.get(tranche.exerciceCocktail().exeExercice());
        if (ligne != null) {
            if (ligne.montantAE() != null) {
                montantAE = ligne.montantAE();
            }
            if (ligne.montantCP() != null) {
                montantPaye = ligne.montantCP();
            }
        }
        resteAPayer = resteAPayerNmoins1.add(montantAE).subtract(montantPaye);
        return resteAPayer;
    }

    private BigDecimal getResteAPayerPrecedent(Tranche tranche) {
        BigDecimal resteAPayerNmois1 = BigDecimal.ZERO;
        Integer trancheExer = tranche.exerciceCocktail().exeExercice().intValue();
        Tranche trancheNmoins1 = operation.trancheNonSupprPourExercice(trancheExer - 1);
        if (trancheNmoins1 != null) {
            resteAPayerNmois1 = getResteAPayer(trancheNmoins1);
        }
        return resteAPayerNmois1;
    }

    public List<LigneDepenseCPSurCombinaisonAxes> lignesMisesAJour() {
    	List<LigneDepenseCPSurCombinaisonAxes> lignesCP = new ArrayList<LigneDepenseCPSurCombinaisonAxes>();
    	for (LigneDepenseCPSurCombinaisonAxes currentLigne : getLignes()) {
    		if (currentLigne.isMontantCPMisAJour()) {
    			lignesCP.add(currentLigne);
    		}
    	}
    	
    	return lignesCP;
    }
    
    public List<LigneDepenseCPSurCombinaisonAxes> lignesMisesAJourSansSuppression() {
        List<LigneDepenseCPSurCombinaisonAxes> lignesCP = new ArrayList<LigneDepenseCPSurCombinaisonAxes>();
        for (LigneDepenseCPSurCombinaisonAxes currentLigne : getLignes()) {
            if (currentLigne.isMontantCPMisAJour() && !currentLigne.isMontantCPEfface()) {
                lignesCP.add(currentLigne);
            }
        }
        
        return lignesCP;
    }
    
    public List<LigneDepenseCPSurCombinaisonAxes> lignesPrevisionnellesAllouees() {
    	List<LigneDepenseCPSurCombinaisonAxes> lignesCP = new ArrayList<LigneDepenseCPSurCombinaisonAxes>();
    	for (LigneDepenseCPSurCombinaisonAxes currentLigne : getLignes()) {
    		if (currentLigne.isPrevisionnel() && currentLigne.montantCP() != null) {
    			lignesCP.add(currentLigne);
    		}
    	}
    	
    	return lignesCP;
    }
    
    public Operation operation() {
    	return operation;
    }
}
