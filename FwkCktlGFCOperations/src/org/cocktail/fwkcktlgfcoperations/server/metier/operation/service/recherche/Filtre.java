package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import lombok.Getter;
import lombok.Setter;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;

/**
 * 
 * Représente les critères de la recherche avancée.
 * 
 * @author Alexis Tual
 *
 */
@Getter
@Setter
public class Filtre {

    private ServiceGestionnaireBean selectedService;
    private DisciplineBean selectedDiscipline;
    private ModeGestionBean selectedModeDeGestion;
    private TypeContratBean selectedTypeContrat;
    private CategorieOperationBean selectedCategorieOperation;
    private TypeOperationBean selectedTypeOperation;
    private ModeDePilotageBean selectedModeDePilotage;
    
    private Integer filtreExeordre;
    private Integer filtreIndex;
    private String filtreRefExterne;
    private String filtreLibelle;
    private String filtreTypeClassificationContrat;
    private NSTimestamp filtreDateDebutMin;
    private NSTimestamp filtreDateDebutMax;
    private NSTimestamp filtreDateFinMin;
    private NSTimestamp filtreDateFinMax;
    private NSTimestamp filtreDateValidationAdmMin;
    private NSTimestamp filtreDateValidationAdmMax;

    /**
     * @return le qualifier correspondant aux critères.
     *         A noter que les critères null sont exclus du qualifier.
     * 
     */
    public EOQualifier qualifier() {
        NSArray<EOQualifier> qualifiers = qualifiersWithoutNullValue(
                ERXQ.equals(ResultatRechercheBean.DISCIPLINE_KEY, selectedDiscipline),
                ERXQ.equals(ResultatRechercheBean.MODE_GESTION_KEY, selectedModeDeGestion),
                ERXQ.equals(ResultatRechercheBean.TYPE_CONTRAT_KEY, selectedTypeContrat),
                ERXQ.equals(ResultatRechercheBean.CATEGORIE_OPERATION_KEY, selectedCategorieOperation),
                ERXQ.equals(ResultatRechercheBean.TYPE_OPERATION_KEY, selectedTypeOperation),
                ERXQ.equals(ResultatRechercheBean.MODE_PILOTAGE_KEY, selectedModeDePilotage),
                ERXQ.equals(ResultatRechercheBean.SERVICE_GEST_KEY, selectedService),
                ERXQ.equals(ResultatRechercheBean.EXE_ORDRE_KEY, filtreExeordre),
                ERXQ.equals(ResultatRechercheBean.INDEX_KEY, filtreIndex),
                ERXQ.between(ResultatRechercheBean.DATE_DEBUT_KEY, filtreDateDebutMin, filtreDateDebutMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_FIN_KEY, filtreDateFinMin, filtreDateFinMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_DEBUT_KEY, filtreDateDebutMin, filtreDateDebutMax, true),
                ERXQ.between(ResultatRechercheBean.DATE_VALIDATION_ADM_KEY, filtreDateValidationAdmMin, filtreDateValidationAdmMax, true),
                ERXQ.likeInsensitive(ResultatRechercheBean.TYPE_CLASSIFICATION_CONTRAT_KEY, filtreTypeClassificationContrat)
                );
        if (filtreLibelle != null) {
            qualifiers = qualifiers.arrayByAddingObject(ERXQ.contains(ResultatRechercheBean.LIBELLE_KEY, filtreLibelle));
        }
        if (filtreRefExterne != null) {
            qualifiers = qualifiers.arrayByAddingObject(ERXQ.contains(ResultatRechercheBean.REF_EXTERNE_KEY, filtreRefExterne));
        }
        EOAndQualifier andQual = new EOAndQualifier(qualifiers);
        return andQual;
    }
    
    protected NSArray<EOQualifier> qualifiersWithoutNullValue(EOQualifier... quals) {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        for (EOQualifier qual : quals) {
            if (qual != null) {
                if (qual instanceof EOKeyValueQualifier) {
                    Object object = ((EOKeyValueQualifier) qual).value();
                    if (object != null && !(object instanceof NSKeyValueCoding.Null)) {
                        qualifiers.addObject(qual);
                    }
                } else {
                    qualifiers.addObject(qual);
                }
            }
        }
        return qualifiers.immutableClone();
    }
}
