package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche;

import static org.cocktail.fwkcktlgfcoperations.common.i18n.CodeMessage.*;

import java.util.ArrayList;
import java.util.List;

import er.extensions.localization.ERXLocalizer;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class GestionBudgetSurTrancheNotificationHandler {

	private boolean errDroitPositionnerBudget;
	private boolean errTrancheValide;
	private boolean errTrancheVerrouillee;
	private boolean errTrancheIntegree;

	public List<String> errMessages() {
		List<String> erreurs = new ArrayList<String>();

		if (errDroitPositionnerBudget) {
			String msg = ERXLocalizer.defaultLocalizer().localizedStringForKeyWithDefault(BUD_ERR_BUDPOS);
			erreurs.add(msg);
		}
		
		if (errTrancheValide) {
			String msg = ERXLocalizer.defaultLocalizer().localizedStringForKeyWithDefault(BUD_ERR_TRANCHE_VALIDEE);
			erreurs.add(msg);
		}
		
		if (errTrancheVerrouillee) {
			String msg = ERXLocalizer.defaultLocalizer().localizedStringForKeyWithDefault(BUD_ERR_TRANCHE_VERROUILLEE);
			erreurs.add(msg);
		}
		
		if (errTrancheIntegree) {
			String msg = ERXLocalizer.defaultLocalizer().localizedStringForKeyWithDefault(BUD_ERR_TRANCHE_INTEGREE);
			erreurs.add(msg);
		}
		
		return erreurs;
	}
	
}
