package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;

import com.google.inject.Singleton;

/**
 * Calcul du montant de participation disponible en excluant les frais de gestion.
 * 
 * Exemple :
 * <pre>
 *        1 contribution de 1000 et 10 frais de gestion 
 *        => Montant participation = 990
 * </pre>
 * Ce mode de calcul est celui par défaut.
 * 
 * @see CalculMontantParticipationDisponible
 * 
 * @author Alexis Tual
 *
 */
@Singleton
public class CalculMontantParticipationDisponibleFraisExclus implements CalculMontantParticipationDisponible {
    
    /** 
     * {@inheritDoc}
     */
    public BigDecimal montantParticipationDisponible(RepartPartenaireTranche repartPartenaireTranche) {
        BigDecimal result = BigDecimal.ZERO;
        if (repartPartenaireTranche != null) {
            FraisGestion frais = repartPartenaireTranche.fraisGestion();
            BigDecimal montantContrib = repartPartenaireTranche.montantParticipation();
            if (frais != null) {
                result = montantContrib.subtract(repartPartenaireTranche.toutFraisAnnexesConfondusAsDecimal());
            } else {
                result = montantContrib;
            }
        }
        return result;
    }

}
