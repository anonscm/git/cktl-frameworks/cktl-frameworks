// DO NOT EDIT.  Make changes to TypePartenaire.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypePartenaire extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTypePartenaire";
	public static final String ENTITY_TABLE_NAME = "GFC.TYPE_PARTENAIRE";

  // Attribute Keys
  public static final ERXKey<String> TYPE_PART_ID_INTERNE = new ERXKey<String>("typePartIdInterne");
  public static final ERXKey<String> TYPE_PART_LIBELLE = new ERXKey<String>("typePartLibelle");
  // Relationship Keys

  // Attributes
  public static final String TYPE_PART_ID_INTERNE_KEY = TYPE_PART_ID_INTERNE.key();
  public static final String TYPE_PART_LIBELLE_KEY = TYPE_PART_LIBELLE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypePartenaire.class);

  public TypePartenaire localInstanceIn(EOEditingContext editingContext) {
    TypePartenaire localInstance = (TypePartenaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String typePartIdInterne() {
    return (String) storedValueForKey(EOTypePartenaire.TYPE_PART_ID_INTERNE_KEY);
  }

  public void setTypePartIdInterne(String value) {
    if (EOTypePartenaire.LOG.isDebugEnabled()) {
        EOTypePartenaire.LOG.debug( "updating typePartIdInterne from " + typePartIdInterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypePartenaire.TYPE_PART_ID_INTERNE_KEY);
  }

  public String typePartLibelle() {
    return (String) storedValueForKey(EOTypePartenaire.TYPE_PART_LIBELLE_KEY);
  }

  public void setTypePartLibelle(String value) {
    if (EOTypePartenaire.LOG.isDebugEnabled()) {
        EOTypePartenaire.LOG.debug( "updating typePartLibelle from " + typePartLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypePartenaire.TYPE_PART_LIBELLE_KEY);
  }


  public static TypePartenaire create(EOEditingContext editingContext, String typePartIdInterne
, String typePartLibelle
) {
    TypePartenaire eo = (TypePartenaire) EOUtilities.createAndInsertInstance(editingContext, EOTypePartenaire.ENTITY_NAME);    
        eo.setTypePartIdInterne(typePartIdInterne);
        eo.setTypePartLibelle(typePartLibelle);
    return eo;
  }

  public static ERXFetchSpecification<TypePartenaire> fetchSpec() {
    return new ERXFetchSpecification<TypePartenaire>(EOTypePartenaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypePartenaire> fetchAll(EOEditingContext editingContext) {
    return EOTypePartenaire.fetchAll(editingContext, null);
  }

  public static NSArray<TypePartenaire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypePartenaire.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypePartenaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypePartenaire> fetchSpec = new ERXFetchSpecification<TypePartenaire>(EOTypePartenaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypePartenaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypePartenaire fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypePartenaire.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypePartenaire fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypePartenaire> eoObjects = EOTypePartenaire.fetchAll(editingContext, qualifier, null);
    TypePartenaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTypePartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypePartenaire fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypePartenaire.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypePartenaire fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypePartenaire eoObject = EOTypePartenaire.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTypePartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypePartenaire localInstanceIn(EOEditingContext editingContext, TypePartenaire eo) {
    TypePartenaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}