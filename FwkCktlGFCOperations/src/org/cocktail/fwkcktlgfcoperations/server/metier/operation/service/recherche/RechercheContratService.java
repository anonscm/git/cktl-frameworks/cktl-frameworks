package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

/**
 * 
 * Service pour la recherche avancée de contrats.
 * 
 * 
 * @author Alexis Tual
 *
 */
public interface RechercheContratService {

    /**
     * @param ec un {@link EOEditingContext}
     * @param utlOrdre un utlOrdre
     * @param noIndividu un no d'individu
     * @param persId un persId
     * @param voirToutes false si on veut limiter les oeprations à celles de l'utilisateur correspondant
     *        aux ids donnés, true si on veut récupérer toutes les oeprations
     * @return la liste des oeprations de l'utilisateur ou toutes
     */
    NSArray<ResultatRechercheBean> operationsForUtilisateur(EOEditingContext ec, int utlOrdre, int noIndividu, int persId, boolean voirToutes);

    /**
     * @param resultats les résultats
     * @return la liste des disciplines correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<DisciplineBean> disciplinesFromResultats(NSArray<ResultatRechercheBean> resultats);

    /**
     * @param resultats les résultats
     * @return la liste des types de contrat correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<TypeContratBean> typesContratFromResultats(NSArray<ResultatRechercheBean> resultats);

    /**
     * @param resultats les résultats
     * @return la liste des categories d'operation correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<CategorieOperationBean> categoriesOperationFromResultats(NSArray<ResultatRechercheBean> resultats);

    /**
     * @param resultats les résultats
     * @return la liste des types d'operation correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<TypeOperationBean> typesOperationFromResultats(NSArray<ResultatRechercheBean> resultats);
    
    /**
     * @param resultats les résultats
     * @return la liste des modes de pilotage correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<ModeDePilotageBean> modesDePilotageFromResultats(NSArray<ResultatRechercheBean> resultats);
    
    /**
     * @param resultats les résultats
     * @return la liste des modes de gestion correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<ModeGestionBean> modesGestionFromResultats(NSArray<ResultatRechercheBean> resultats);

    /**
     * @param resultats les résultats
     * @return la liste des services gestionnaires correspondant aux {@link ResultatRechercheBean} donnés
     */
    NSArray<ServiceGestionnaireBean> servicesGestionnairesFromResultats(NSArray<ResultatRechercheBean> resultats);

    /**
     * @param resultats les résultat
     * @param filtre le filtre
     * @return les résultats filtrés
     */
    NSArray<ResultatRechercheBean> filtrerOperations(NSArray<ResultatRechercheBean> resultats, Filtre filtre);

}