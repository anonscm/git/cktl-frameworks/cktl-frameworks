package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche.finder;

import org.apache.commons.lang.Validate;
import org.cocktail.fwkcktlgfceos.server.finder.FinderEb;
import org.cocktail.fwkcktlgfceos.server.finder.FinderExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public class FinderEntiteBudgetaireTranche {
	
	/*
	 * Si besoin on peut contextualiser l'edc / interfacer l'entite / retourner une List<?>.
	 * On peut regarder du coté :
	 * -- via Constructeur
	 * -- via http://www.corej2eepatterns.com/ContextObject.htm
	 * -- via ThreadLocal
	 * -- via http://www.cs.wustl.edu/~schmidt/PDF/Context-Object-Pattern.pdf
	 */
	public static NSArray<EOEb> lister(EOEditingContext edc, Tranche tranche) {
		Validate.notNull(tranche);
		NSArray<EOEb> listeEbs = listerEbsRattacheesExerciceTranche(edc, tranche);
		if (listeEbs.isEmpty()) {
			listeEbs = listerEbsRattacheesDernierExerciceOuvertOuEnPreparation(edc);
		}
		return listeEbs;
	}
	
	private static NSArray<EOEb> listerEbsRattacheesExerciceTranche(EOEditingContext edc, Tranche tranche) {
		Integer exerciceTranche = tranche.exerciceCocktail().exeExercice();
		NSArray<EOEb> listeEbs = FinderEb.findByExercice(edc, exerciceTranche);
		return listeEbs;
	}
	
	private static NSArray<EOEb> listerEbsRattacheesDernierExerciceOuvertOuEnPreparation(EOEditingContext edc) {
		EOExercice exer = FinderExercice.getDernierExerciceOuvertOuEnPreparation(edc);
		NSArray<EOEb> listeEbs = FinderEb.findByExercice(edc, exer.exeExercice());  
		return listeEbs;
	}
	
}
