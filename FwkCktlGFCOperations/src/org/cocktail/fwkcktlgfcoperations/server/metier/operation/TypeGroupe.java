// TypeGroupe.java
// Created on Thu Feb 21 15:27:30 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;


public class TypeGroupe extends EOTypeGroupe {

    public TypeGroupe() {
        super();
    }

/*
    // If you implement the following constructor EOF will use it to
    // create your objects, otherwise it will use the default
    // constructor. For maximum performance, you should only
    // implement this constructor if you depend on the arguments.
    public TypeGroupe(EOEditingContext context, EOClassDescription classDesc, EOGlobalID gid) {
        super(context, classDesc, gid);
    }

    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

    public String tgpeCode() {
        return (String)storedValueForKey("tgpeCode");
    }

    public void setTgpeCode(String value) {
        takeStoredValueForKey(value, "tgpeCode");
    }
}
