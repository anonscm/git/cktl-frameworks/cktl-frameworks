// DO NOT EDIT.  Make changes to HistoCreditPositionne.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOHistoCreditPositionne extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeHistoCreditPositionne";
	public static final String ENTITY_TABLE_NAME = "GFC.HISTO_CREDIT_POSITIONNE";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> HCP_DATE = new ERXKey<NSTimestamp>("hcpDate");
  public static final ERXKey<java.math.BigDecimal> HCP_MONTANT = new ERXKey<java.math.BigDecimal>("hcpMontant");
  public static final ERXKey<String> HCP_SUPPR = new ERXKey<String>("hcpSuppr");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice> EXERCICE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExercice>("exercice");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String HCP_DATE_KEY = HCP_DATE.key();
  public static final String HCP_MONTANT_KEY = HCP_MONTANT.key();
  public static final String HCP_SUPPR_KEY = HCP_SUPPR.key();
  // Relationships
  public static final String EXERCICE_KEY = EXERCICE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOHistoCreditPositionne.class);

  public HistoCreditPositionne localInstanceIn(EOEditingContext editingContext) {
    HistoCreditPositionne localInstance = (HistoCreditPositionne)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp hcpDate() {
    return (NSTimestamp) storedValueForKey(EOHistoCreditPositionne.HCP_DATE_KEY);
  }

  public void setHcpDate(NSTimestamp value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
        EOHistoCreditPositionne.LOG.debug( "updating hcpDate from " + hcpDate() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionne.HCP_DATE_KEY);
  }

  public java.math.BigDecimal hcpMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOHistoCreditPositionne.HCP_MONTANT_KEY);
  }

  public void setHcpMontant(java.math.BigDecimal value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
        EOHistoCreditPositionne.LOG.debug( "updating hcpMontant from " + hcpMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionne.HCP_MONTANT_KEY);
  }

  public String hcpSuppr() {
    return (String) storedValueForKey(EOHistoCreditPositionne.HCP_SUPPR_KEY);
  }

  public void setHcpSuppr(String value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
        EOHistoCreditPositionne.LOG.debug( "updating hcpSuppr from " + hcpSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOHistoCreditPositionne.HCP_SUPPR_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExercice)storedValueForKey(EOHistoCreditPositionne.EXERCICE_KEY);
  }
  
  public void setExercice(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    takeStoredValueForKey(value, EOHistoCreditPositionne.EXERCICE_KEY);
  }

  public void setExerciceRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExercice value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
      EOHistoCreditPositionne.LOG.debug("updating exercice from " + exercice() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExercice(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExercice oldValue = exercice();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionne.EXERCICE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionne.EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOHistoCreditPositionne.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOHistoCreditPositionne.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
      EOHistoCreditPositionne.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionne.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionne.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer)storedValueForKey(EOHistoCreditPositionne.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOHistoCreditPositionne.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
      EOHistoCreditPositionne.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionne.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionne.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOHistoCreditPositionne.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOHistoCreditPositionne.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
      EOHistoCreditPositionne.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionne.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionne.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit)storedValueForKey(EOHistoCreditPositionne.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOHistoCreditPositionne.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    if (EOHistoCreditPositionne.LOG.isDebugEnabled()) {
      EOHistoCreditPositionne.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOHistoCreditPositionne.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOHistoCreditPositionne.TYPE_CREDIT_KEY);
    }
  }
  

  public static HistoCreditPositionne create(EOEditingContext editingContext, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche, org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit typeCredit) {
    HistoCreditPositionne eo = (HistoCreditPositionne) EOUtilities.createAndInsertInstance(editingContext, EOHistoCreditPositionne.ENTITY_NAME);    
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  public static ERXFetchSpecification<HistoCreditPositionne> fetchSpec() {
    return new ERXFetchSpecification<HistoCreditPositionne>(EOHistoCreditPositionne.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<HistoCreditPositionne> fetchAll(EOEditingContext editingContext) {
    return EOHistoCreditPositionne.fetchAll(editingContext, null);
  }

  public static NSArray<HistoCreditPositionne> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOHistoCreditPositionne.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<HistoCreditPositionne> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<HistoCreditPositionne> fetchSpec = new ERXFetchSpecification<HistoCreditPositionne>(EOHistoCreditPositionne.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<HistoCreditPositionne> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static HistoCreditPositionne fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionne.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionne fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<HistoCreditPositionne> eoObjects = EOHistoCreditPositionne.fetchAll(editingContext, qualifier, null);
    HistoCreditPositionne eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeHistoCreditPositionne that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionne fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOHistoCreditPositionne.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static HistoCreditPositionne fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    HistoCreditPositionne eoObject = EOHistoCreditPositionne.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeHistoCreditPositionne that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static HistoCreditPositionne localInstanceIn(EOEditingContext editingContext, HistoCreditPositionne eo) {
    HistoCreditPositionne localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}