package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeAvenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core.FinderCategorieOperation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core.FinderTypeOperation;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableDictionary;

public class OperationBuilder {

	private static final String GROUPE_ANNUAIRE_PARTENARIAT = "ANNUAIRE_PARTENARIAT";
	
	private EOEditingContext ec;
	private FactoryOperation contratFactory;
	private FactoryAvenant avenantFactory;
	
	private Operation operation;
	private OperationPartenaire partenaireInternePrincipal;
	
	public OperationBuilder(final EOEditingContext ec) {
		this.ec = ec;
		this.contratFactory = new FactoryOperation(ec, false);
		this.avenantFactory = new FactoryAvenant(ec, false);
	}
	
	public Operation build() {
		return operation;
	}
	
	public OperationBuilder creerConventionVierge(final EOUtilisateur createur) throws Exception {
		checkUtilisateurValide(createur);
		
		TypeClassificationContrat typeContrat = findTypeClassificationConvention();
		operation = contratFactory.creerContratVierge(typeContrat, createur);
		avenantFactory.creerAvenantVierge(
				operation,
				Integer.valueOf(0),
				TypeAvenant.TypeAvenantContratInitial(ec),
				createur);
		
		this.ajouterGroupePartenaire(createur);
		
		return this;
	}
	
	/**
	 * 
	 * @param etablissement
	 * @return
	 */
	public OperationBuilder ajouterEtablissementCommePartenairePrincipal(EOStructure etablissement) throws Exception {
		if (operation == null || etablissement == null) {
			return this;
		}
		
		EOAssociation associationEtabInternePrincipal = findAssociationEtablissementInternePrincipalDeTypeConvention();
		
		FactoryConvention factoryConvention = new FactoryConvention(ec);
		partenaireInternePrincipal = factoryConvention.ajouterPartenaire(operation, etablissement, null, Boolean.TRUE, BigDecimal.valueOf(0));
		
		EOStructure groupePartenaire = operation.groupePartenaire();
		Integer persId = operation.persIdCreation();
				
		EOStructureForGroupeSpec.definitUnRole(ec, etablissement, associationEtabInternePrincipal, groupePartenaire, persId, null, null, null, null, null, true);
		
		operation.setEtablissementRelationship(etablissement, persId);
		
		return this;
	}
	
	public OperationBuilder ajouterServiceGestionnaireCommeContact(EOStructure serviceGestionnaire, Integer persIdUtilisateur) throws Exception {
		if (operation == null || partenaireInternePrincipal == null || serviceGestionnaire == null) {
			return this;
		}
		
		EOAssociation associationRepresentantGestionnairePartenariat = findAssociationRepresentantGestionnairePartenariat();
		FactoryOperationPartenaire fcp = new FactoryOperationPartenaire(ec, Boolean.TRUE);
		fcp.ajouterContact(partenaireInternePrincipal, serviceGestionnaire, new NSArray<EOAssociation>(associationRepresentantGestionnairePartenariat));

		operation.setCentreResponsabiliteRelationship(serviceGestionnaire, persIdUtilisateur);
		
		return this;
	}
	
	public OperationBuilder avecImpactFinancier() throws Exception {
		if (operation == null) {
			return this;
		}
		
		CategorieOperation categorieAvecImpactFinancier = findCategorieAvecImpactFinancier();
		operation.setCategorieOperation(categorieAvecImpactFinancier);

		return this;
	}

	public OperationBuilder operationInvestissement() throws Exception {
		if (operation == null) {
			return this;
		}
		
		if (!operation.estAvecImpactFinancier()) {
			throw new Exception("Une opération d'investissement doit être avec impact financier et non de catégorie égale à " + operation.categorieOperation().libelleLong());
		}
		
		TypeOperation typeInvestissement = findTypeInvestissement();
		operation.setTypeOperationRelationship(typeInvestissement);
		
		return this;
	}
	
	private OperationBuilder ajouterGroupePartenaire(EOUtilisateur createur) throws Exception {
		EOStructure groupePartenaire = findGroupePartenaire(createur);
		
		this.operation.setGroupePartenaireRelationship(groupePartenaire);
		
		return this;
	}
	
	private void checkUtilisateurValide(EOUtilisateur createur) throws Exception {
		if (createur == null || !createur.isValide()) {
			throw new Exception("Vous n'êtes pas un utlisateur valide (JEFY_ADMIN)");
		}
	}
	
	private EOStructure findGroupePartenaire(EOUtilisateur createur) throws Exception {
		String groupeLibelle = "Partenaires de l'acte en cours de creation par " + createur.getLogin() + " le " + DateCtrl.now();
		EOStructure groupePere = EOStructureForGroupeSpec.getGroupeForParamKey(ec, GROUPE_ANNUAIRE_PARTENARIAT);
		Integer persId = createur.personne().persId();
		EOTypeGroupe typeGroupePartenariat = (EOTypeGroupe) EOUtilities.objectMatchingKeyAndValue(ec, EOTypeGroupe.ENTITY_NAME, EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_PN);
		EOStructure groupe = EOStructureForGroupeSpec.creerGroupe(ec, persId, groupeLibelle, null, new NSArray(typeGroupePartenariat), groupePere);
		
		return groupe;
	}
	
	private TypeClassificationContrat findTypeClassificationConvention() {
		return (TypeClassificationContrat) EOUtilities.objectMatchingKeyAndValue(
				ec, 
				TypeClassificationContrat.ENTITY_NAME, 
				TypeClassificationContrat.TCC_CODE_KEY, 
				TypeClassificationContrat.TYPE_CLASSIFICATION_CODE_OPE);
	}
	
	private EOAssociation findAssociationEtablissementInternePrincipalDeTypeConvention() {
		NSMutableDictionary values = new NSMutableDictionary();
		values.setObjectForKey(EOTypeAssociation.TAS_CODE_CONV, EOAssociation.TO_TYPE_ASSOCIATION_KEY + "." + EOTypeAssociation.TAS_CODE_KEY);
		values.setObjectForKey("ETABINTPRINC", EOAssociation.ASS_CODE_KEY);
		EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
		EOAssociation associationEtabInternePrincipal = EOAssociation.fetchByQualifier(ec, qualifier);
		
		return associationEtabInternePrincipal;
	}
	
	private EOAssociation findAssociationRepresentantGestionnairePartenariat() {
		NSMutableDictionary values = new NSMutableDictionary();
		values.setObjectForKey("REPRGESTPART", EOAssociation.ASS_CODE_KEY);
		EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
		EOAssociation associationRepresentantGestionnairePartenariat = EOAssociation.fetchByQualifier(ec, qualifier);
		
		return associationRepresentantGestionnairePartenariat;
	}
	
	private CategorieOperation findCategorieAvecImpactFinancier() throws Exception {
		FinderCategorieOperation finderCategorie = new FinderCategorieOperation(ec);
		CategorieOperation categorieAvecImpactFinancier = finderCategorie.findByCode(CategorieOperation.CODE_AVEC_IMPACT_FINANCIER);
		return categorieAvecImpactFinancier;
	}
	
	private TypeOperation findTypeInvestissement() throws Exception {
		FinderTypeOperation finderType = new FinderTypeOperation(ec);
		TypeOperation typeInvestissement = finderType.findByCode(TypeOperation.OP_INVESTISSEMENT);
		
		return typeInvestissement;
	}

}
