// DO NOT EDIT.  Make changes to Parametre.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOParametre extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeParametre";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_PARAMETRES";

  // Attribute Keys
  public static final ERXKey<String> PARAM_COMMENTAIRES = new ERXKey<String>("paramCommentaires");
  public static final ERXKey<String> PARAM_KEY = new ERXKey<String>("paramKey");
  public static final ERXKey<String> PARAM_VALUE = new ERXKey<String>("paramValue");
  // Relationship Keys

  // Attributes
  public static final String PARAM_COMMENTAIRES_KEY = PARAM_COMMENTAIRES.key();
  public static final String PARAM_KEY_KEY = PARAM_KEY.key();
  public static final String PARAM_VALUE_KEY = PARAM_VALUE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOParametre.class);

  public Parametre localInstanceIn(EOEditingContext editingContext) {
    Parametre localInstance = (Parametre)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String paramCommentaires() {
    return (String) storedValueForKey(EOParametre.PARAM_COMMENTAIRES_KEY);
  }

  public void setParamCommentaires(String value) {
    if (EOParametre.LOG.isDebugEnabled()) {
        EOParametre.LOG.debug( "updating paramCommentaires from " + paramCommentaires() + " to " + value);
    }
    takeStoredValueForKey(value, EOParametre.PARAM_COMMENTAIRES_KEY);
  }

  public String paramKey() {
    return (String) storedValueForKey(EOParametre.PARAM_KEY_KEY);
  }

  public void setParamKey(String value) {
    if (EOParametre.LOG.isDebugEnabled()) {
        EOParametre.LOG.debug( "updating paramKey from " + paramKey() + " to " + value);
    }
    takeStoredValueForKey(value, EOParametre.PARAM_KEY_KEY);
  }

  public String paramValue() {
    return (String) storedValueForKey(EOParametre.PARAM_VALUE_KEY);
  }

  public void setParamValue(String value) {
    if (EOParametre.LOG.isDebugEnabled()) {
        EOParametre.LOG.debug( "updating paramValue from " + paramValue() + " to " + value);
    }
    takeStoredValueForKey(value, EOParametre.PARAM_VALUE_KEY);
  }


  public static Parametre create(EOEditingContext editingContext, String paramKey
, String paramValue
) {
    Parametre eo = (Parametre) EOUtilities.createAndInsertInstance(editingContext, EOParametre.ENTITY_NAME);    
        eo.setParamKey(paramKey);
        eo.setParamValue(paramValue);
    return eo;
  }

  public static ERXFetchSpecification<Parametre> fetchSpec() {
    return new ERXFetchSpecification<Parametre>(EOParametre.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Parametre> fetchAll(EOEditingContext editingContext) {
    return EOParametre.fetchAll(editingContext, null);
  }

  public static NSArray<Parametre> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOParametre.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Parametre> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Parametre> fetchSpec = new ERXFetchSpecification<Parametre>(EOParametre.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Parametre> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Parametre fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOParametre.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Parametre fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Parametre> eoObjects = EOParametre.fetchAll(editingContext, qualifier, null);
    Parametre eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeParametre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Parametre fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOParametre.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Parametre fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Parametre eoObject = EOParametre.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeParametre that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Parametre localInstanceIn(EOEditingContext editingContext, Parametre eo) {
    Parametre localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Parametre> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOParametre.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Parametre>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Parametre> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOParametre.ENTITY_NAME);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Parametre>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}