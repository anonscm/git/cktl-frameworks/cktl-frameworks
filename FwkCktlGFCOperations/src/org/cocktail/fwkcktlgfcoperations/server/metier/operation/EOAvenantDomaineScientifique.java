// DO NOT EDIT.  Make changes to AvenantDomaineScientifique.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAvenantDomaineScientifique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeAvenantDomaineScientifique";
	public static final String ENTITY_TABLE_NAME = "GFC.AVENANT_DOM_SCIENT";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> AVENANT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>("avenant");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique> DOMAINE_SCIENTIFIQUE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique>("domaineScientifique");

  // Attributes
  // Relationships
  public static final String AVENANT_KEY = AVENANT.key();
  public static final String DOMAINE_SCIENTIFIQUE_KEY = DOMAINE_SCIENTIFIQUE.key();

  private static Logger LOG = Logger.getLogger(EOAvenantDomaineScientifique.class);

  public AvenantDomaineScientifique localInstanceIn(EOEditingContext editingContext) {
    AvenantDomaineScientifique localInstance = (AvenantDomaineScientifique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant)storedValueForKey(EOAvenantDomaineScientifique.AVENANT_KEY);
  }
  
  public void setAvenant(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    takeStoredValueForKey(value, EOAvenantDomaineScientifique.AVENANT_KEY);
  }

  public void setAvenantRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    if (EOAvenantDomaineScientifique.LOG.isDebugEnabled()) {
      EOAvenantDomaineScientifique.LOG.debug("updating avenant from " + avenant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAvenant(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant oldValue = avenant();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantDomaineScientifique.AVENANT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantDomaineScientifique.AVENANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique)storedValueForKey(EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
  }
  
  public void setDomaineScientifique(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
    takeStoredValueForKey(value, EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
  }

  public void setDomaineScientifiqueRelationship(org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique value) {
    if (EOAvenantDomaineScientifique.LOG.isDebugEnabled()) {
      EOAvenantDomaineScientifique.LOG.debug("updating domaineScientifique from " + domaineScientifique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDomaineScientifique(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique oldValue = domaineScientifique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
    }
  }
  

  public static AvenantDomaineScientifique create(EOEditingContext editingContext, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant, org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique domaineScientifique) {
    AvenantDomaineScientifique eo = (AvenantDomaineScientifique) EOUtilities.createAndInsertInstance(editingContext, EOAvenantDomaineScientifique.ENTITY_NAME);    
    eo.setAvenantRelationship(avenant);
    eo.setDomaineScientifiqueRelationship(domaineScientifique);
    return eo;
  }

  public static ERXFetchSpecification<AvenantDomaineScientifique> fetchSpec() {
    return new ERXFetchSpecification<AvenantDomaineScientifique>(EOAvenantDomaineScientifique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AvenantDomaineScientifique> fetchAll(EOEditingContext editingContext) {
    return EOAvenantDomaineScientifique.fetchAll(editingContext, null);
  }

  public static NSArray<AvenantDomaineScientifique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAvenantDomaineScientifique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<AvenantDomaineScientifique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AvenantDomaineScientifique> fetchSpec = new ERXFetchSpecification<AvenantDomaineScientifique>(EOAvenantDomaineScientifique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AvenantDomaineScientifique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AvenantDomaineScientifique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantDomaineScientifique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantDomaineScientifique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AvenantDomaineScientifique> eoObjects = EOAvenantDomaineScientifique.fetchAll(editingContext, qualifier, null);
    AvenantDomaineScientifique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeAvenantDomaineScientifique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantDomaineScientifique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantDomaineScientifique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantDomaineScientifique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    AvenantDomaineScientifique eoObject = EOAvenantDomaineScientifique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeAvenantDomaineScientifique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantDomaineScientifique localInstanceIn(EOEditingContext editingContext, AvenantDomaineScientifique eo) {
    AvenantDomaineScientifique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}