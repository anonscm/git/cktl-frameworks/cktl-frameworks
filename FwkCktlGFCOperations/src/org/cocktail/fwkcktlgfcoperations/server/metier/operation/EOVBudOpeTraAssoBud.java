// DO NOT EDIT.  Make changes to VBudOpeTraAssoBud.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVBudOpeTraAssoBud extends  CktlServerRecord {
  public static final String ENTITY_NAME = "VBudOpeTraAssoBud";
	public static final String ENTITY_TABLE_NAME = "GFC.V_BUD_OPE_TRA_ASSO_BUD";

  // Attribute Keys
  public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String ID_OPE_OPERATION_KEY = ID_OPE_OPERATION.key();
  // Relationships
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOVBudOpeTraAssoBud.class);

  public VBudOpeTraAssoBud localInstanceIn(EOEditingContext editingContext) {
    VBudOpeTraAssoBud localInstance = (VBudOpeTraAssoBud)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(EOVBudOpeTraAssoBud.ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    if (EOVBudOpeTraAssoBud.LOG.isDebugEnabled()) {
        EOVBudOpeTraAssoBud.LOG.debug( "updating idOpeOperation from " + idOpeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOVBudOpeTraAssoBud.ID_OPE_OPERATION_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOVBudOpeTraAssoBud.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOVBudOpeTraAssoBud.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOVBudOpeTraAssoBud.LOG.isDebugEnabled()) {
      EOVBudOpeTraAssoBud.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVBudOpeTraAssoBud.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVBudOpeTraAssoBud.TRANCHE_KEY);
    }
  }
  

  public static VBudOpeTraAssoBud create(EOEditingContext editingContext, Integer idOpeOperation
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    VBudOpeTraAssoBud eo = (VBudOpeTraAssoBud) EOUtilities.createAndInsertInstance(editingContext, EOVBudOpeTraAssoBud.ENTITY_NAME);    
        eo.setIdOpeOperation(idOpeOperation);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VBudOpeTraAssoBud> fetchSpec() {
    return new ERXFetchSpecification<VBudOpeTraAssoBud>(EOVBudOpeTraAssoBud.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VBudOpeTraAssoBud> fetchAll(EOEditingContext editingContext) {
    return EOVBudOpeTraAssoBud.fetchAll(editingContext, null);
  }

  public static NSArray<VBudOpeTraAssoBud> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVBudOpeTraAssoBud.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VBudOpeTraAssoBud> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VBudOpeTraAssoBud> fetchSpec = new ERXFetchSpecification<VBudOpeTraAssoBud>(EOVBudOpeTraAssoBud.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VBudOpeTraAssoBud> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VBudOpeTraAssoBud fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVBudOpeTraAssoBud.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VBudOpeTraAssoBud fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VBudOpeTraAssoBud> eoObjects = EOVBudOpeTraAssoBud.fetchAll(editingContext, qualifier, null);
    VBudOpeTraAssoBud eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VBudOpeTraAssoBud that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VBudOpeTraAssoBud fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVBudOpeTraAssoBud.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VBudOpeTraAssoBud fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VBudOpeTraAssoBud eoObject = EOVBudOpeTraAssoBud.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VBudOpeTraAssoBud that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VBudOpeTraAssoBud localInstanceIn(EOEditingContext editingContext, VBudOpeTraAssoBud eo) {
    VBudOpeTraAssoBud localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}