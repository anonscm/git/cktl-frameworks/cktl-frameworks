// Avenant.java
// 
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement;
import org.cocktail.fwkcktlgfceos.server.metier.EOTva;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory.FactoryConvention;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.joda.time.DateTime;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationFactory;

public class Avenant extends EOAvenant {
    private static final BigDecimal BIGDECIMAL_100 = BigDecimal.TEN.multiply(BigDecimal.TEN);

    public static final String INITIAL = "Initial";

    /**
     * <p>
     * Nom de l'entite correspondante a cette classe metier.
     * <p>
     * C'est indispensable de "cacher"
     * <code>{@link EOAvenant#ENTITY_NAME}</code> ici car il y a 2 entites qui
     * pointent sur cette classe metier dans le modele : "OpeAvenant" et
     * "OpeAvenantZero", donc le template utilise par EOGenerator fait que
     * <code>{@link EOAvenant#ENTITY_NAME}</code> vaut
     * <code>OpeAvenantZero</code> au lieu de <code>OpeAvenant</code>.
     */
    public static final String ENTITY_NAME = "OpeAvenant";

    public static final String AVT_MONNAIE = "E";

    public static final String AVT_LUCRATIVITE_OUI = "O";
    public static final String AVT_LUCRATIVITE_NON = "N";

    public static final String AVT_RECUP_TVA_OUI = "O";
    public static final String AVT_RECUP_TVA_NON = "N";

    public static final String AVT_SUPPR_OUI = "O";
    public static final String AVT_SUPPR_NON = "N";

    public static final int LONGUEUR_MAX_REF_EXTERNE = 100;
    public static final int LONGUEUR_MAX_OBJET = 250;
    public static final int LONGUEUR_MAX_OBJET_COURT = 50;
    public static final int LONGUEUR_MAX_OBSERVATIONS = 700;

    public static final String AVT_LIMITATIF_OUI = "OUI";
    public static final String AVT_LIMITATIF_NON = null;

    public static final EOSortOrdering SORT_INDEX_ASC = AVT_INDEX.asc();

    public static final EOQualifier QUALIFIER_NON_SUPPR = AVT_SUPPR.eq(AVT_SUPPR_NON);
    public static final EOQualifier QUALIFIER_NON_INITIAL = AVT_INDEX.greaterThan(0);
    public static final EOQualifier QUALIFIER_NON_SUPPR_NON_INITIAL = ERXQ.and(QUALIFIER_NON_SUPPR, QUALIFIER_NON_INITIAL);
    public static final EOQualifier QUALIFIER_VALIDE = AVT_DATE_VALID_ADM.isNotNull();
    public static final EOQualifier QUALIFIER_VALIDE_FINANCIEREMENT = AVT_DATE_VALID_FIN.isNotNull();

    public NSMutableArray documents = null;

    private boolean shouldNotValidate;

    public Avenant() {
        super();
    }

    public boolean estValideFinancierement() {
    	return avtDateValidFin() != null;
    }
    
    /**
     * Valide financièrement un avenant
     */
    public void validerFinancierement() {
        if (avtDateValidFin() == null) {
            setAvtDateValidFin(new NSTimestamp());
        }
    }

    public void forcerMontantEtTauxTvaSiAvenantAdministratif() {
        TypeAvenant typeAvenant = typeAvenant();
        if (typeAvenant != null && typeAvenant.isAdministratif()) {
            setAvtMontantHt(BigDecimal.ZERO);
            setAvtMontantTtc(BigDecimal.ZERO);
            setTvaRelationship(EOTva.fetchTvaZero(editingContext()));
        }
    }

    public void validerAdministrativement(EOUtilisateur utilisateur) {
        EOUtilisateur utilisateurLocal = utilisateur.localInstanceIn(editingContext());
        if (!hasDroitValiderAdministrativement(utilisateurLocal)) {
            throw new ValidationException("Vous n'avez pas le droit de valider les avenants.");
        }
        setAvtDateValidAdm(new NSTimestamp());
        setUtilisateurValidAdmRelationship(utilisateurLocal);
        if (avtDateValidFin() == null) {
            setAvtDateValidFin(avtDateValidAdm());
        }
    }

    public void setShouldNotValidate(boolean shouldNotValidate) {
        this.shouldNotValidate = shouldNotValidate;
    }

    public Boolean avtLucrativiteBoolean() {
        return new Boolean(AVT_LUCRATIVITE_OUI.equals(avtLucrativite()));
    }

    public void setAvtLucrativiteBoolean(Boolean aValue) {
        if (aValue != null && aValue) {
            setAvtLucrativite(AVT_LUCRATIVITE_OUI);
        } else {
            setAvtLucrativite(AVT_LUCRATIVITE_NON);
        }
    }

    public Boolean avtRecupTvaBoolean() {
        return new Boolean(AVT_RECUP_TVA_OUI.equals(avtRecupTva()));
    }

    public void setAvtRecupTvaBoolean(Boolean aValue) {
        if (aValue != null && aValue) {
            setAvtRecupTva(AVT_RECUP_TVA_OUI);
        } else {
            setAvtRecupTva(AVT_RECUP_TVA_NON);
        }
    }

    public Boolean avtLimitatifBoolean() {
        return new Boolean(AVT_LIMITATIF_OUI.equals(avtLimitatif()));
    }

    public void setAvtLimitatifBoolean(Boolean aValue) {
        if (aValue != null && aValue) {
            setAvtLimitatif(AVT_LIMITATIF_OUI);
        } else {
            setAvtLimitatif(AVT_LIMITATIF_NON);
        }
    }

    public void setAvtDateDeb(NSTimestamp aValue) {
        super.setAvtDateDeb(aValue);
        if (isAvenantZero()) {
            operation().updateDuree();
            recalculerTranches();
        }
    }

    public void setAvtDateFin(NSTimestamp aValue) {
        super.setAvtDateFin(aValue);
        if (isAvenantZero()) {
            operation().updateDuree();
            recalculerTranches();
        }
    }

    public void setAvtMontantHt(BigDecimal aValue) {
        if (aValue != null && avtMontantHt() != null && avtDateValidAdm() != null) {
            if (operation().montantDisponibleTranches().doubleValue() - avtMontantHt().doubleValue() + aValue.doubleValue() < 0) {
                throw new ValidationException(
                        "Le Montant Ht ne peut etre modifie car le montant de la convention devient inferieur aux tranches deja positionnee");
            }
            if (operation().montantDisponiblePourPartenariat().doubleValue() - avtMontantHt().doubleValue() + aValue.doubleValue() < 0) {
                throw new ValidationException(
                        "Le Montant Ht ne peut etre modifie car le montant de la convention devient inferieur aux contributions de partenariat deja apportees");
            }
        }

        if (aValue != null) {
            super.setAvtMontantHt(aValue.setScale(2, BigDecimal.ROUND_HALF_UP));
        } else {
            super.setAvtMontantHt(null);
        }
        // Recalcul du montant TTC
        majMontantTTC();
        if (isAvenantZero()) {
            recalculerTranches();
        }
    }

    private Boolean gestionTrancheAuto() {
        // GBCP : Suppression de la gestion manuelle des tranches pour la GBCP -
        // On garde pour mémoire ou rétropédalage...
        // return ERXProperties.booleanForKeyWithDefault(
        // Constantes.GESTION_TRANCHE_AUTO_PARAM, true);
        return true;
    }

    public void recalculerTranches() {
        if (!gestionTrancheAuto()) {
            return;
        }
        boolean champsObligatoires = operation() != null && operation().dateDebut() != null && operation().dateFin() != null
                && operation().montantHt() != null;
        if (champsObligatoires && operation().montantHt().doubleValue() > 0) {
            FactoryConvention fc = new FactoryConvention(editingContext(), true);
            try {
                fc.genererTranches(operation(), operation().utilisateurCreation());
            } catch (ValidationException e) {
                throw e;
            } catch (Exception e) {
                throw new NSForwardException(e);
            }
        }
    }

    public void setAvtMontantTtc(BigDecimal aValue) {
        if (aValue != null) {
            super.setAvtMontantTtc(aValue.setScale(2, BigDecimal.ROUND_HALF_UP));
        } else {
            super.setAvtMontantTtc(null);
        }
    }

    public boolean isAvenantZero() {
        return operation() != null && operation().avenantZero() != null && this.equals(operation().avenantZero());
    }

    public boolean isSigne() {
        return avtDateValidAdm() != null;
    }

    /**
     * @see com.webobjects.eocontrol.EOCustomObject#validateForSave()
     */
    public void validateForSave() throws ValidationException {
        super.validateForSave();

        if (!shouldNotValidate) {
            validerChampsObligatoires();
            validerDates();
        }
        // Enfin, on recalcule les tranches et la durée de la convention
        operation().updateDuree();
    }

    private void validerDates() {
        // dates > 1900
        boolean datesOk = true;
        DateTime dateTime = new DateTime(1900, 1, 1, 0, 0);
        NSTimestamp mille900 = new NSTimestamp(dateTime.toDate());
        if (avtDateDeb() != null && avtDateDeb().before(mille900)) {
            setAvtDateDeb(null);
            datesOk = false;
        }
        if (avtDateFin() != null && avtDateFin().before(mille900)) {
            setAvtDateFin(null);
            datesOk = false;            
        }
        if (avtDateValidFin() != null && avtDateValidFin().before(mille900)) {
            setAvtDateValidFin(null);
            datesOk = false;
        }
        if (avtDateValidAdm() != null && avtDateValidAdm().before(mille900)) {
            setAvtDateValidAdm(null);
            datesOk = false;
        }
        if (!datesOk) {
            throw new ValidationException("Les dates doivent être supérieures au 01/01/1900.");            
        }

        // verif date fin >= date debut
        if (avtDateFin() != null && avtDateFin().before(avtDateDeb())) {
            String message;
            if (avtIndex().intValue() > 0) {
                message = "La date de fin de l'avenant n\u00BA" + avtIndex() + " ne peut \u00EAtre ant\u00E9rieure \u00E0 la date de d\u00E9but.";
            } else {
                message = "La date de fin du operation initial ne peut \u00EAtre ant\u00E9rieure \u00E0 la date de d\u00E9but.";
            }
            throw new ValidationException(message);
        }
        // Si date de signature est null mais que dateValidAdm renseignée,
        // pas normal !
        if (avtDateSignature() == null && avtDateValidAdm() != null) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionPasSigne");
        }
        // Si date de validation != null & operation date valid adm == null
        // pas bon !
        if (avtDateValidAdm() != null && operation().conDateValidAdm() == null) {
            setAvtDateValidAdm(null);
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionPasValide");
        }
    }

    private void validerChampsObligatoires() {
        // On vérifie que le type est bien spécifié
        if (typeAvenant() == null) {
            throw new ValidationException("Le type d'avenant doit \u00EAtre indiqu\u00E9.");
        }
        if (avtObjet() == null) {
            throw new ValidationException("Le libellé doit \u00EAtre indiqu\u00E9.");
        }
        // On vérifie que la date de début et la date de fin soient bien
        // renseignées
        // dans le cas d'un operation de type convention
        if (operation() != null && operation().typeClassificationContrat().isTypeOpe()) {
            if (avtDateDeb() == null || avtDateFin() == null) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, "ConventionNoDate");
            }
        }
    }

    /**
     * @return Cet objet au format texte.
     */
    public String toString() {
        if (avtIndex().intValue() > 0) {
            return "Avenant n\u00BA" + avtIndex() + " de la convention " + operation().toString();
        } else {
            return "Contrat initial de la convention " + operation().toString();
        }
    }

    @Override
    public void setModeGestionRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModeGestion aValue) {
        super.setModeGestionRelationship(aValue);
        if (aValue != null && aValue.isModeRA()) {
            setAvtLimitatifBoolean(Boolean.TRUE);
            setAvtLucrativiteBoolean(Boolean.FALSE);
        }
    }

    // pour le kvc
    public ModeGestion modeGestionRelationship() {
        return modeGestion();
    }

    /**
     * @return the documents
     */
    public NSMutableArray documents() {
        if (documents == null && operation().conIndex() != null) {
            // TODO Recuperer tous les documents associes a l'avenant a partir
            // du numero de operation
            // et du numero de l'avenant
        }
        return documents;
    }

    /**
     * @param documents
     *            the documents to set
     */
    public void setDocuments(NSMutableArray documents) {
        this.documents = documents;
    }

    public TypeStat typeOptionnel() {
        TypeStat typeOptionnel = null;
        NSArray avenantTypeStats = avenantTypeStats();
        if (avenantTypeStats != null && avenantTypeStats.count() > 0 && avenantTypeStats.lastObject() != null) {
            typeOptionnel = ((AvenantTypeStat) avenantTypeStats.lastObject()).typeStat();
        }
        return typeOptionnel;
    }

    public void setTypeOptionnel(TypeStat value) {
        if (value != null) {
            AvenantTypeStat avtTypeStat;
            try {
                avtTypeStat = AvenantTypeStat.instanciate(editingContext());
                editingContext().insertObject(avtTypeStat);
                avtTypeStat.setTypeStatRelationship(value);
                avtTypeStat.setAvenantRelationship(this);
                addToAvenantTypeStatsRelationship(avtTypeStat);
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } else {
            AvenantTypeStat avtTypeStat = (AvenantTypeStat) avenantTypeStats().lastObject();
            if (avtTypeStat != null) {
                removeFromAvenantTypeStatsRelationship(avtTypeStat);
                editingContext().deleteObject(avtTypeStat);
            }
        }
    }

    private void majMontantTTC() {
        setAvtMontantTtc(htToTtc(avtMontantHt(), tva()));
    }

    private BigDecimal htToTtc(BigDecimal montantHt, EOTva tva) {
        if (montantHt == null || tva == null) {
            return montantHt;
        }
        // HT x (1 + TVA / 100)
        return montantHt.multiply(new BigDecimal("1").add(tva.tvaTaux().divide(new BigDecimal("100"))));
    }

    @Override
    public void setTvaRelationship(EOTva value) {
        super.setTvaRelationship(value);
        majMontantTTC();
    }

    // Pour le kvc
    public EOTva tvaRelationship() {
        return tva();
    }

    @Override
    public EOTva tva() {
        EOTva tva = super.tva();
        if (tva == null && avtMontantHt() != null && avtMontantTtc() != null) {
            BigDecimal taux = avtMontantTtc().subtract(avtMontantHt());
            if (taux.doubleValue() > 0) {
                taux = taux.multiply(BIGDECIMAL_100);
                taux = taux.divide(avtMontantHt(), RoundingMode.HALF_UP);
                taux.setScale(2, RoundingMode.HALF_UP);
            } else {
                taux = BigDecimal.ZERO;
            }
            tva = (EOTva) EOUtilities.objectMatchingKeyAndValue(editingContext(), EOTva.ENTITY_NAME, EOTva.TVA_TAUX_KEY, taux);
            setTvaRelationship(tva);
        }
        return tva;
    }

    public NSArray<EOEvenement> evenements() {
        NSArray<EOEvenement> evts = (NSArray<EOEvenement>) avenantEvenements().valueForKey(EOAvenantEvenement.EVENEMENT_KEY);
        evts = ERXS.sorted(evts, ERXS.desc(EOEvenement.DATE_PREVUE_KEY).array());
        return evts;
    }

    public void removeFromEvenementsRelationship(EOEvenement evenement) {
        NSArray<AvenantEvenement> avEvts = avenantEvenements(EOAvenantEvenement.EVENEMENT.eq(evenement));
        for (AvenantEvenement avEvt : avEvts) {
            avEvt.setEvenementRelationship(null);
            EOEvenement.supprimerEvenement(evenement, editingContext());
            this.deleteAvenantEvenementsRelationship(avEvt);
        }
    }

    public void removeFromDomaineScientifiqueRelationship(EODomaineScientifique domaine) {
        NSArray<AvenantDomaineScientifique> domaines = avenantDomaineScientifiques(EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE.eq(domaine));

        for (AvenantDomaineScientifique ads : domaines) {
            ads.setDomaineScientifiqueRelationship(null);
            this.deleteAvenantDomaineScientifiquesRelationship(ads);
        }
    }

    public String libelleCourt() {
        String libelleCourt = null;

        if (avtIndex().intValue() > 0) {
            libelleCourt = String.valueOf(avtIndex());
        } else {
            libelleCourt = Avenant.INITIAL;
        }
        return libelleCourt;
    }

    @Override
    public EODomaineScientifique domaineScientifique() {
        if (avenantDomaineScientifiques().isEmpty()) {
            return null;
        } else {
            AvenantDomaineScientifique avenantDomaineScientifique = ERXArrayUtilities.firstObject(avenantDomaineScientifiques());
            return avenantDomaineScientifique.domaineScientifique();
        }
    }

    @Override
    public void setDomaineScientifique(EODomaineScientifique domaineScientifique) {
        if (domaineScientifique == null) {
            return;
        }

        if (avenantDomaineScientifiques().isEmpty()) {
            AvenantDomaineScientifique.create(editingContext(), this, domaineScientifique);
        } else {
            AvenantDomaineScientifique avenantDomaineScientifique = ERXArrayUtilities.firstObject(avenantDomaineScientifiques());
            avenantDomaineScientifique.setDomaineScientifiqueRelationship(domaineScientifique);
        }
    }

    public boolean hasDroitValiderAdministrativement(EOUtilisateur utilisateur) {
        FwkCktlGFCOperationsApplicationUser appUser = new FwkCktlGFCOperationsApplicationUser(editingContext(), utilisateur);
        boolean avenantEligible = avtDateSignature() != null && !isSigne();
        boolean droitsUtilisateur = appUser.hasDroitValidationOperationsEtAvenants();
        return avenantEligible && droitsUtilisateur;
    }
}
