package org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BudgetCombinaisonAxesRecetteValidationResults {

	private boolean isEbEnErreur;
	private boolean isNatureEnErreur;
	private boolean isOrigineEnErreur; 
	
}
