package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponible;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation.ContributionDoitExcederFraisGestionValidation;

import com.google.inject.Inject;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation.ValidationException;

import er.extensions.eof.ERXKey;
import er.extensions.validation.ERXValidationException;
import er.extensions.validation.ERXValidationFactory;

public class RepartPartenaireTranche extends EORepartPartenaireTranche {

    @Inject
    private CalculMontantParticipationDisponible calculMontantParticipationDisponible;
    
    public RepartPartenaireTranche() {
        super();
    }
    
	/**
	 * Fournit une instance de cette classe.
	 * @param ec Editing context a utliser.
	 * @return L'instance creee
	 * @throws InstantiationException Probleme d'instanciation.
	 */
	static public RepartPartenaireTranche instanciate(EOEditingContext ec) throws Exception {
		return (RepartPartenaireTranche) Factory.instanceForEntity(ec, ENTITY_NAME);
	}
	
	@Override
	public void validateForSave() throws ValidationException {
	    super.validateForSave();
	    tranche().checkMontantTranche();
	    checkMontantContributionAvecFrais();
	}

	public void checkMontantContributionAvecFrais() {
		ContributionDoitExcederFraisGestionValidation contributionExcedeFrais = new ContributionDoitExcederFraisGestionValidation(calculMontantParticipationDisponible);
	    if (!contributionExcedeFrais.isSatisfiedBy(this)) {
	    	NSMutableDictionary<String, String> replacements = new NSMutableDictionary<String, String>();
	    	throw buildValidationException("ContributionInferieurTotalFrais", replacements);
	    }
	}
	
    private ValidationException buildValidationException(String validationExceptionTemplate, NSMutableDictionary<String, String> replacements) {
    	ERXValidationException ex = ERXValidationFactory.defaultFactory().createCustomException(this, validationExceptionTemplate);
        ex.setContext(replacements);
        
        return ex;
    }
	
	public FraisGestion getOrCreateFraisGestion() {
	    FraisGestion frais = fraisGestions() != null ? fraisGestions().lastObject() : null;
	    if (frais == null) {
	        frais = FraisGestion.create(editingContext(), BigDecimal.ZERO, this);
	        frais.setChargesNonBudgetaires(BigDecimal.ZERO);
	        frais.setCoutsIndirectsNonImputes(BigDecimal.ZERO);
	    }
	    return frais;
	}
	
	public FraisGestion fraisGestion() {
	    FraisGestion frais = fraisGestions() != null ? fraisGestions().lastObject() : null;
	    return frais;
	}

	@Override
	public void setMontantParticipation(BigDecimal value) {
	    super.setMontantParticipation(value);
	    FraisGestion fraisGestion = getOrCreateFraisGestion();
	    if (fraisGestion != null) {
	        fraisGestion.recalculerFrais();
	    }
	}
	
	public static final ERXKey<BigDecimal> FRAIS_GESTIONS_AS_DECIMAL = new ERXKey<BigDecimal>("fraisGestionAsDecimal");
	public BigDecimal fraisGestionAsDecimal() {
	    FraisGestion fraisGestion = fraisGestion();
	    BigDecimal result = BigDecimal.ZERO;
	    if (fraisGestion != null && fraisGestion.montant() != null) {
	        result = fraisGestion.montant();
	    }
	    return result;
	}
	
	public BigDecimal toutFraisAnnexesConfondusAsDecimal() {
		return fraisGestionAsDecimal().add(coutsIndirectsAsDecimal()).add(chargesNonBudgetairesAsDecimal());
	}
	
	public static final ERXKey<BigDecimal> COUTS_INDIRECTS_AS_DECIMAL = new ERXKey<BigDecimal>("coutsIndirectsAsDecimal");
	public BigDecimal coutsIndirectsAsDecimal() {
		FraisGestion fraisGestion = fraisGestion();
		BigDecimal result = BigDecimal.ZERO;
		if (fraisGestion != null && fraisGestion.coutsIndirectsNonImputes() != null) {
			result = fraisGestion.coutsIndirectsNonImputes();
		}
		return result;
	}
	
	public static final ERXKey<BigDecimal> CHARGES_NON_BUDGETAIRES_AS_DECIMAL = new ERXKey<BigDecimal>("chargesNonBudgetairesAsDecimal");
	public BigDecimal chargesNonBudgetairesAsDecimal() {
		FraisGestion fraisGestion = fraisGestion();
		BigDecimal result = BigDecimal.ZERO;
		if (fraisGestion != null && fraisGestion.chargesNonBudgetaires() != null) {
			result = fraisGestion.chargesNonBudgetaires();
		}
		return result;
	}
	
	public static final ERXKey<BigDecimal> MONTANT_PARTICIPATION_DISPONIBLE = 
	        new ERXKey<BigDecimal>("montantParticipationDisponible");
	
	public void setCalculMontantParticipationDisponible(CalculMontantParticipationDisponible calculMontantParticipationDisponible) {
        this.calculMontantParticipationDisponible = calculMontantParticipationDisponible;
    }
	
	/**
	 * @return le montant de la participation disponible
	 */
	public BigDecimal montantParticipationDisponible() {
	   return calculMontantParticipationDisponible.montantParticipationDisponible(this);
	}
	
}
