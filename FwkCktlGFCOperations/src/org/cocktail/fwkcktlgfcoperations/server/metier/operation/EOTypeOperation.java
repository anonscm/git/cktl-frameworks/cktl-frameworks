// DO NOT EDIT.  Make changes to TypeOperation.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeOperation extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTypeOperation";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_TYPE_OPERATION";

  // Attribute Keys
  public static final ERXKey<String> CODE_TYPE_OPERATION = new ERXKey<String>("codeTypeOperation");
  public static final ERXKey<Integer> ID_OPE_CATEG_OPERATION = new ERXKey<Integer>("idOpeCategOperation");
  public static final ERXKey<Integer> ID_OPE_TYPE_OPERATION = new ERXKey<Integer>("idOpeTypeOperation");
  public static final ERXKey<Boolean> IS_INCLUS_TABX_BUDGETAIRES = new ERXKey<Boolean>("isInclusTabxBudgetaires");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation> CATEGORIE_OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation>("categorieOperation");

  // Attributes
  public static final String CODE_TYPE_OPERATION_KEY = CODE_TYPE_OPERATION.key();
  public static final String ID_OPE_CATEG_OPERATION_KEY = ID_OPE_CATEG_OPERATION.key();
  public static final String ID_OPE_TYPE_OPERATION_KEY = ID_OPE_TYPE_OPERATION.key();
  public static final String IS_INCLUS_TABX_BUDGETAIRES_KEY = IS_INCLUS_TABX_BUDGETAIRES.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  // Relationships
  public static final String CATEGORIE_OPERATION_KEY = CATEGORIE_OPERATION.key();

  private static Logger LOG = Logger.getLogger(EOTypeOperation.class);

  public TypeOperation localInstanceIn(EOEditingContext editingContext) {
    TypeOperation localInstance = (TypeOperation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeTypeOperation() {
    return (String) storedValueForKey(EOTypeOperation.CODE_TYPE_OPERATION_KEY);
  }

  public void setCodeTypeOperation(String value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
        EOTypeOperation.LOG.debug( "updating codeTypeOperation from " + codeTypeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeOperation.CODE_TYPE_OPERATION_KEY);
  }

  public Integer idOpeCategOperation() {
    return (Integer) storedValueForKey(EOTypeOperation.ID_OPE_CATEG_OPERATION_KEY);
  }

  public void setIdOpeCategOperation(Integer value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
        EOTypeOperation.LOG.debug( "updating idOpeCategOperation from " + idOpeCategOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeOperation.ID_OPE_CATEG_OPERATION_KEY);
  }

  public Integer idOpeTypeOperation() {
    return (Integer) storedValueForKey(EOTypeOperation.ID_OPE_TYPE_OPERATION_KEY);
  }

  public void setIdOpeTypeOperation(Integer value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
        EOTypeOperation.LOG.debug( "updating idOpeTypeOperation from " + idOpeTypeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeOperation.ID_OPE_TYPE_OPERATION_KEY);
  }

  public Boolean isInclusTabxBudgetaires() {
    return (Boolean) storedValueForKey(EOTypeOperation.IS_INCLUS_TABX_BUDGETAIRES_KEY);
  }

  public void setIsInclusTabxBudgetaires(Boolean value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
        EOTypeOperation.LOG.debug( "updating isInclusTabxBudgetaires from " + isInclusTabxBudgetaires() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeOperation.IS_INCLUS_TABX_BUDGETAIRES_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(EOTypeOperation.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
        EOTypeOperation.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeOperation.LIBELLE_LONG_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation categorieOperation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation)storedValueForKey(EOTypeOperation.CATEGORIE_OPERATION_KEY);
  }
  
  public void setCategorieOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation value) {
    takeStoredValueForKey(value, EOTypeOperation.CATEGORIE_OPERATION_KEY);
  }

  public void setCategorieOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation value) {
    if (EOTypeOperation.LOG.isDebugEnabled()) {
      EOTypeOperation.LOG.debug("updating categorieOperation from " + categorieOperation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCategorieOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation oldValue = categorieOperation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTypeOperation.CATEGORIE_OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTypeOperation.CATEGORIE_OPERATION_KEY);
    }
  }
  

  public static TypeOperation create(EOEditingContext editingContext, String codeTypeOperation
, Integer idOpeCategOperation
, Integer idOpeTypeOperation
, Boolean isInclusTabxBudgetaires
, String libelleLong
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation categorieOperation) {
    TypeOperation eo = (TypeOperation) EOUtilities.createAndInsertInstance(editingContext, EOTypeOperation.ENTITY_NAME);    
        eo.setCodeTypeOperation(codeTypeOperation);
        eo.setIdOpeCategOperation(idOpeCategOperation);
        eo.setIdOpeTypeOperation(idOpeTypeOperation);
        eo.setIsInclusTabxBudgetaires(isInclusTabxBudgetaires);
        eo.setLibelleLong(libelleLong);
    eo.setCategorieOperationRelationship(categorieOperation);
    return eo;
  }

  public static ERXFetchSpecification<TypeOperation> fetchSpec() {
    return new ERXFetchSpecification<TypeOperation>(EOTypeOperation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeOperation> fetchAll(EOEditingContext editingContext) {
    return EOTypeOperation.fetchAll(editingContext, null);
  }

  public static NSArray<TypeOperation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeOperation.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeOperation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeOperation> fetchSpec = new ERXFetchSpecification<TypeOperation>(EOTypeOperation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeOperation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeOperation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeOperation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeOperation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeOperation> eoObjects = EOTypeOperation.fetchAll(editingContext, qualifier, null);
    TypeOperation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTypeOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeOperation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeOperation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeOperation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeOperation eoObject = EOTypeOperation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTypeOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeOperation localInstanceIn(EOEditingContext editingContext, TypeOperation eo) {
    TypeOperation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}