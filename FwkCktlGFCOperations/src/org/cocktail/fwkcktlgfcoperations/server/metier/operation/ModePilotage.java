package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.apache.log4j.Logger;

public class ModePilotage extends EOModePilotage {
	
	public static final String CODE_SUIVI_SIMPLE = "SUIVI_SIMPLE";
	public static final String CODE_BUDGET_SPECIFIQUE = "BUDGET_SPECIFIQUE";
	public static final String CODE_PROGRAMMATION = "PROGRAMMATION";	
	
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(ModePilotage.class);
}
