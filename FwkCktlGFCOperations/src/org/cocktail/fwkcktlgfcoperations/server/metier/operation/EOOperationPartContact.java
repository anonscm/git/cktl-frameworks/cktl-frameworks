// DO NOT EDIT.  Make changes to OperationPartContact.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOOperationPartContact extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeOperationPartContact";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_OPERATION_PART_CONTACT";

  // Attribute Keys
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  public static final ERXKey<Integer> PERS_ID_CONTACT = new ERXKey<Integer>("persIdContact");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu> INDIVIDU = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOIndividu>("individu");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> OPERATION_PARTENAIRE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>("operationPartenaire");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> PARTENAIRE_STRUCTURE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("partenaireStructure");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact> TYPE_CONTACT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact>("typeContact");

  // Attributes
  public static final String PERS_ID_KEY = PERS_ID.key();
  public static final String PERS_ID_CONTACT_KEY = PERS_ID_CONTACT.key();
  // Relationships
  public static final String INDIVIDU_KEY = INDIVIDU.key();
  public static final String OPERATION_PARTENAIRE_KEY = OPERATION_PARTENAIRE.key();
  public static final String PARTENAIRE_STRUCTURE_KEY = PARTENAIRE_STRUCTURE.key();
  public static final String TYPE_CONTACT_KEY = TYPE_CONTACT.key();

  private static Logger LOG = Logger.getLogger(EOOperationPartContact.class);

  public OperationPartContact localInstanceIn(EOEditingContext editingContext) {
    OperationPartContact localInstance = (OperationPartContact)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer persId() {
    return (Integer) storedValueForKey(EOOperationPartContact.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
        EOOperationPartContact.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartContact.PERS_ID_KEY);
  }

  public Integer persIdContact() {
    return (Integer) storedValueForKey(EOOperationPartContact.PERS_ID_CONTACT_KEY);
  }

  public void setPersIdContact(Integer value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
        EOOperationPartContact.LOG.debug( "updating persIdContact from " + persIdContact() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartContact.PERS_ID_CONTACT_KEY);
  }

  public org.cocktail.fwkcktlpersonne.common.metier.EOIndividu individu() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOIndividu)storedValueForKey(EOOperationPartContact.INDIVIDU_KEY);
  }
  
  public void setIndividu(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    takeStoredValueForKey(value, EOOperationPartContact.INDIVIDU_KEY);
  }

  public void setIndividuRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOIndividu value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
      EOOperationPartContact.LOG.debug("updating individu from " + individu() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setIndividu(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOIndividu oldValue = individu();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartContact.INDIVIDU_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartContact.INDIVIDU_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire operationPartenaire() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire)storedValueForKey(EOOperationPartContact.OPERATION_PARTENAIRE_KEY);
  }
  
  public void setOperationPartenaire(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire value) {
    takeStoredValueForKey(value, EOOperationPartContact.OPERATION_PARTENAIRE_KEY);
  }

  public void setOperationPartenaireRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
      EOOperationPartContact.LOG.debug("updating operationPartenaire from " + operationPartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOperationPartenaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire oldValue = operationPartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartContact.OPERATION_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartContact.OPERATION_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure partenaireStructure() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOOperationPartContact.PARTENAIRE_STRUCTURE_KEY);
  }
  
  public void setPartenaireStructure(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOOperationPartContact.PARTENAIRE_STRUCTURE_KEY);
  }

  public void setPartenaireStructureRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
      EOOperationPartContact.LOG.debug("updating partenaireStructure from " + partenaireStructure() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPartenaireStructure(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = partenaireStructure();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartContact.PARTENAIRE_STRUCTURE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartContact.PARTENAIRE_STRUCTURE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact typeContact() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact)storedValueForKey(EOOperationPartContact.TYPE_CONTACT_KEY);
  }
  
  public void setTypeContact(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact value) {
    takeStoredValueForKey(value, EOOperationPartContact.TYPE_CONTACT_KEY);
  }

  public void setTypeContactRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact value) {
    if (EOOperationPartContact.LOG.isDebugEnabled()) {
      EOOperationPartContact.LOG.debug("updating typeContact from " + typeContact() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeContact(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContact oldValue = typeContact();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartContact.TYPE_CONTACT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartContact.TYPE_CONTACT_KEY);
    }
  }
  

  public static OperationPartContact create(EOEditingContext editingContext, Integer persId
, Integer persIdContact
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire operationPartenaire) {
    OperationPartContact eo = (OperationPartContact) EOUtilities.createAndInsertInstance(editingContext, EOOperationPartContact.ENTITY_NAME);    
        eo.setPersId(persId);
        eo.setPersIdContact(persIdContact);
    eo.setOperationPartenaireRelationship(operationPartenaire);
    return eo;
  }

  public static ERXFetchSpecification<OperationPartContact> fetchSpec() {
    return new ERXFetchSpecification<OperationPartContact>(EOOperationPartContact.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<OperationPartContact> fetchAll(EOEditingContext editingContext) {
    return EOOperationPartContact.fetchAll(editingContext, null);
  }

  public static NSArray<OperationPartContact> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOOperationPartContact.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<OperationPartContact> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<OperationPartContact> fetchSpec = new ERXFetchSpecification<OperationPartContact>(EOOperationPartContact.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<OperationPartContact> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static OperationPartContact fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperationPartContact.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OperationPartContact fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<OperationPartContact> eoObjects = EOOperationPartContact.fetchAll(editingContext, qualifier, null);
    OperationPartContact eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeOperationPartContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OperationPartContact fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperationPartContact.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OperationPartContact fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    OperationPartContact eoObject = EOOperationPartContact.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeOperationPartContact that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OperationPartContact localInstanceIn(EOEditingContext editingContext, OperationPartContact eo) {
    OperationPartContact localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}