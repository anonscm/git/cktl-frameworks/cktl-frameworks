package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;

public class FinderCategorieOperation extends Finder {

	/**
	 * Constructeur.
	 * @param ec Editing context de travail
	 */
	public FinderCategorieOperation(EOEditingContext ec) {
		super(ec, CategorieOperation.ENTITY_NAME);
		
	}
	
	public CategorieOperation findByCode(String code) throws ExceptionFinder {
		EOQualifier qualCode = ERXQ.equals(CategorieOperation.CODE_CATEGORIE_KEY, code);
		this.addMandatoryQualifier(qualCode);
		return findOne();
	}

	protected CategorieOperation findOne() throws ExceptionFinder {
		NSArray<CategorieOperation> categories = find();
		if (categories == null || categories.size() > 1) {
			throw new ExceptionFinder("FindOne attend un résultat de recherche unique. Found : " + categories);
		}
		
		return categories.lastObject();
	}
	
	@Override
	public void clearAllCriteria() {
	}

	@Override
	public boolean canFind() {
		return true;
	}

	@Override
	public String getCurrentWarningMessage() {
		return null;
	}
	
}
