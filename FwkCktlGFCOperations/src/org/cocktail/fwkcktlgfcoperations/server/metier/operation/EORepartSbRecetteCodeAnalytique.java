// DO NOT EDIT.  Make changes to RepartSbRecetteCodeAnalytique.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartSbRecetteCodeAnalytique extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartSbRecetteCodeAnalytique";
	public static final String ENTITY_TABLE_NAME = "GFC.REPART_SB_RECETTE_C_ANALYTIQUE";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT_HT = new ERXKey<java.math.BigDecimal>("montantHt");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique> CODE_ANALYTIQUE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique>("codeAnalytique");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> SB_RECETTE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>("sbRecette");

  // Attributes
  public static final String MONTANT_HT_KEY = MONTANT_HT.key();
  // Relationships
  public static final String CODE_ANALYTIQUE_KEY = CODE_ANALYTIQUE.key();
  public static final String SB_RECETTE_KEY = SB_RECETTE.key();

  private static Logger LOG = Logger.getLogger(EORepartSbRecetteCodeAnalytique.class);

  public RepartSbRecetteCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
    RepartSbRecetteCodeAnalytique localInstance = (RepartSbRecetteCodeAnalytique)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montantHt() {
    return (java.math.BigDecimal) storedValueForKey(EORepartSbRecetteCodeAnalytique.MONTANT_HT_KEY);
  }

  public void setMontantHt(java.math.BigDecimal value) {
    if (EORepartSbRecetteCodeAnalytique.LOG.isDebugEnabled()) {
        EORepartSbRecetteCodeAnalytique.LOG.debug( "updating montantHt from " + montantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartSbRecetteCodeAnalytique.MONTANT_HT_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique codeAnalytique() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique)storedValueForKey(EORepartSbRecetteCodeAnalytique.CODE_ANALYTIQUE_KEY);
  }
  
  public void setCodeAnalytique(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique value) {
    takeStoredValueForKey(value, EORepartSbRecetteCodeAnalytique.CODE_ANALYTIQUE_KEY);
  }

  public void setCodeAnalytiqueRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique value) {
    if (EORepartSbRecetteCodeAnalytique.LOG.isDebugEnabled()) {
      EORepartSbRecetteCodeAnalytique.LOG.debug("updating codeAnalytique from " + codeAnalytique() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCodeAnalytique(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique oldValue = codeAnalytique();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbRecetteCodeAnalytique.CODE_ANALYTIQUE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbRecetteCodeAnalytique.CODE_ANALYTIQUE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette sbRecette() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette)storedValueForKey(EORepartSbRecetteCodeAnalytique.SB_RECETTE_KEY);
  }
  
  public void setSbRecette(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    takeStoredValueForKey(value, EORepartSbRecetteCodeAnalytique.SB_RECETTE_KEY);
  }

  public void setSbRecetteRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    if (EORepartSbRecetteCodeAnalytique.LOG.isDebugEnabled()) {
      EORepartSbRecetteCodeAnalytique.LOG.debug("updating sbRecette from " + sbRecette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSbRecette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette oldValue = sbRecette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbRecetteCodeAnalytique.SB_RECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbRecetteCodeAnalytique.SB_RECETTE_KEY);
    }
  }
  

  public static RepartSbRecetteCodeAnalytique create(EOEditingContext editingContext, org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique codeAnalytique, org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette sbRecette) {
    RepartSbRecetteCodeAnalytique eo = (RepartSbRecetteCodeAnalytique) EOUtilities.createAndInsertInstance(editingContext, EORepartSbRecetteCodeAnalytique.ENTITY_NAME);    
    eo.setCodeAnalytiqueRelationship(codeAnalytique);
    eo.setSbRecetteRelationship(sbRecette);
    return eo;
  }

  public static ERXFetchSpecification<RepartSbRecetteCodeAnalytique> fetchSpec() {
    return new ERXFetchSpecification<RepartSbRecetteCodeAnalytique>(EORepartSbRecetteCodeAnalytique.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartSbRecetteCodeAnalytique> fetchAll(EOEditingContext editingContext) {
    return EORepartSbRecetteCodeAnalytique.fetchAll(editingContext, null);
  }

  public static NSArray<RepartSbRecetteCodeAnalytique> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartSbRecetteCodeAnalytique.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartSbRecetteCodeAnalytique> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartSbRecetteCodeAnalytique> fetchSpec = new ERXFetchSpecification<RepartSbRecetteCodeAnalytique>(EORepartSbRecetteCodeAnalytique.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartSbRecetteCodeAnalytique> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartSbRecetteCodeAnalytique fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbRecetteCodeAnalytique.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbRecetteCodeAnalytique fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartSbRecetteCodeAnalytique> eoObjects = EORepartSbRecetteCodeAnalytique.fetchAll(editingContext, qualifier, null);
    RepartSbRecetteCodeAnalytique eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartSbRecetteCodeAnalytique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbRecetteCodeAnalytique fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbRecetteCodeAnalytique.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbRecetteCodeAnalytique fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartSbRecetteCodeAnalytique eoObject = EORepartSbRecetteCodeAnalytique.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartSbRecetteCodeAnalytique that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbRecetteCodeAnalytique localInstanceIn(EOEditingContext editingContext, RepartSbRecetteCodeAnalytique eo) {
    RepartSbRecetteCodeAnalytique localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}