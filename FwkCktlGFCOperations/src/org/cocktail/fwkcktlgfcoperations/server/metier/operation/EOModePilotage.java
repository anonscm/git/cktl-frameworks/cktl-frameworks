// DO NOT EDIT.  Make changes to ModePilotage.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOModePilotage extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeModePilotage";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_MODE_PILOTAGE";

  // Attribute Keys
  public static final ERXKey<String> CODE = new ERXKey<String>("code");
  public static final ERXKey<Integer> ID_OPE_MODE_PILOTAGE = new ERXKey<Integer>("idOpeModePilotage");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  // Relationship Keys

  // Attributes
  public static final String CODE_KEY = CODE.key();
  public static final String ID_OPE_MODE_PILOTAGE_KEY = ID_OPE_MODE_PILOTAGE.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOModePilotage.class);

  public ModePilotage localInstanceIn(EOEditingContext editingContext) {
    ModePilotage localInstance = (ModePilotage)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String code() {
    return (String) storedValueForKey(EOModePilotage.CODE_KEY);
  }

  public void setCode(String value) {
    if (EOModePilotage.LOG.isDebugEnabled()) {
        EOModePilotage.LOG.debug( "updating code from " + code() + " to " + value);
    }
    takeStoredValueForKey(value, EOModePilotage.CODE_KEY);
  }

  public Integer idOpeModePilotage() {
    return (Integer) storedValueForKey(EOModePilotage.ID_OPE_MODE_PILOTAGE_KEY);
  }

  public void setIdOpeModePilotage(Integer value) {
    if (EOModePilotage.LOG.isDebugEnabled()) {
        EOModePilotage.LOG.debug( "updating idOpeModePilotage from " + idOpeModePilotage() + " to " + value);
    }
    takeStoredValueForKey(value, EOModePilotage.ID_OPE_MODE_PILOTAGE_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(EOModePilotage.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (EOModePilotage.LOG.isDebugEnabled()) {
        EOModePilotage.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EOModePilotage.LIBELLE_LONG_KEY);
  }


  public static ModePilotage create(EOEditingContext editingContext, String code
, Integer idOpeModePilotage
, String libelleLong
) {
    ModePilotage eo = (ModePilotage) EOUtilities.createAndInsertInstance(editingContext, EOModePilotage.ENTITY_NAME);    
        eo.setCode(code);
        eo.setIdOpeModePilotage(idOpeModePilotage);
        eo.setLibelleLong(libelleLong);
    return eo;
  }

  public static ERXFetchSpecification<ModePilotage> fetchSpec() {
    return new ERXFetchSpecification<ModePilotage>(EOModePilotage.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<ModePilotage> fetchAll(EOEditingContext editingContext) {
    return EOModePilotage.fetchAll(editingContext, null);
  }

  public static NSArray<ModePilotage> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOModePilotage.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<ModePilotage> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<ModePilotage> fetchSpec = new ERXFetchSpecification<ModePilotage>(EOModePilotage.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<ModePilotage> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static ModePilotage fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOModePilotage.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ModePilotage fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<ModePilotage> eoObjects = EOModePilotage.fetchAll(editingContext, qualifier, null);
    ModePilotage eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeModePilotage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ModePilotage fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOModePilotage.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static ModePilotage fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    ModePilotage eoObject = EOModePilotage.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeModePilotage that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static ModePilotage localInstanceIn(EOEditingContext editingContext, ModePilotage eo) {
    ModePilotage localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOModePilotage.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOModePilotage.ENTITY_NAME);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}