package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfceos.server.metier.EOTva;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionArgument;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionOperationImpossible;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.nombre.NombreOperation;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModeGestion;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeAvenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.NotificationCenter;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.NotificationCenter.Notification;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeGroupe;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Michael Haller, Consortium Cocktail, 2008
 */
public class FactoryConvention extends Factory {

    protected FactoryOperation contratFactory;
    protected FactoryAvenant avenantFactory;
    protected FactoryOperationPartenaire contratPartenaireFactory;
    protected FactoryTranche trancheFactory;
    protected NombreOperation nombreOperation;

    private static String TYPE_ASSOCIATION_CONVENTION = "Convention";

    /**
     * @param ec
     */
    public FactoryConvention(final EOEditingContext ec) {
        this(ec, false);
    }

    /**
     * @param ec
     * @param withlog
     */
    public FactoryConvention(final EOEditingContext ec, final Boolean withlog) {
        super(ec, withlog);

        contratFactory = new FactoryOperation(ec, withlog);
        avenantFactory = new FactoryAvenant(ec, withlog);
        contratPartenaireFactory = new FactoryOperationPartenaire(ec, withlog);
        trancheFactory = new FactoryTranche(ec, withlog);

        nombreOperation = new NombreOperation();
    }

    /**
     * Creation d'un objet Contrat, l'avenant 0 (contrat initial) est aussi
     * cree.
     * 
     * @param etablissementGestionnaire
     *            Etablissement gestionnaire de la convention (obligatoire)
     * @param centreResponsabiliteGestionnaire
     *            Centre de responsabilite gestionnaire de la convention
     *            (obligatoire)
     * @param typeContrat
     *            Type de la convention, ex: Convention de recherche
     *            (obligatoire)
     * @param modeGestion
     *            Mode de gestion de la convention, ex: Ressource affectee
     *            (obligatoire)
     * @param referenceExterne
     *            Reference externe de la convention.
     * @param objet
     *            Intitule de l'bjet de la convention (obligatoire)
     * @param observations
     *            Observation diverses
     * @param dateDebut
     *            Date de debut de la convention (obligatoire)
     * @param dateFin
     *            Date de fin de la convention (obligatoire)
     * @param dateDebutExecution
     *            Date de debut d'execution de la convention
     * @param dateFinExecution
     *            Date de fin d'execution de la convention
     * @param dateSignature
     *            Date de signature en CA de la convention
     * @param dateCloture
     *            Date de cloture de la convention
     * @param montantGlobalHT
     *            Montant global HT du cotrat initial (obligatoire)
     * @param tauxTva
     *            Taux de TVA pour le montant global.
     * @param creditsLimitatifs
     *            Indique si le montant des credits positionnes est indepassable
     *            au moment d'une depense.
     * @param lucratif
     *            Indicateur de lucrativite de la convention
     * @param tvaRecuperable
     *            Indicateur de recuperation (collecte) de la convention
     * @param typeReconduction
     *            Type de reconduction
     * @param naf
     *            code naf (domaine fonctionnel)
     * @param createur
     *            Utilisateur createur de la convention (obligatoire)
     * @param creerPartenaireAuto
     *            si true, ajoute le centre gestionnaire au partenaire
     * @return Le contrat correspondant a la convention creee. Ce contrat
     *         possede un avenant zero implicite.
     * @throws Exception
     */
    public Operation creerConvention(
            final TypeClassificationContrat typeClassificationContrat,
            final EOStructure etablissementGestionnaire,
            final EOStructure centreResponsabiliteGestionnaire,
            final TypeContrat typeContrat,
            final ModeGestion modeGestion,
            final String referenceExterne,
            final String objet,
            final String observations,
            final NSTimestamp dateDebut,
            final NSTimestamp dateFin,
            final Integer duree,
            final NSTimestamp dateDebutExecution,
            final NSTimestamp dateFinExecution,
            final NSTimestamp dateSignature,
            final NSTimestamp dateCloture,
            final BigDecimal montantGlobalHT,
            final EOTva tauxTva,
            final Boolean creditsLimitatifs,
            final Boolean lucratif,
            final Boolean tvaRecuperable,
            final TypeReconduction typeReconduction,
            final EONaf naf,
            final EOUtilisateur createur,
            final Boolean creerPartenaireAuto)
            throws Exception, ExceptionUtilisateur {

        // contrat
        Operation operation = contratFactory.creerContrat(
                typeClassificationContrat,
                etablissementGestionnaire,
                centreResponsabiliteGestionnaire,
                typeContrat,
                referenceExterne,
                objet,
                observations,
                dateCloture,
                typeReconduction,
                naf,
                createur);

        // avenant zero
        avenantFactory.creerAvenant(
                operation,
                new Integer(0),
                TypeAvenant.TypeAvenantContratInitial(ec),
                modeGestion,
                referenceExterne,
                objet,
                observations,
                dateDebut,
                dateFin,
                duree,
                dateDebutExecution,
                dateFinExecution,
                dateSignature,
                montantGlobalHT,
                tauxTva,
                creditsLimitatifs,
                lucratif,
                tvaRecuperable,
                createur);

        if (creerPartenaireAuto.booleanValue()) {
            // System.out.println("FACTORYcentreResponsabiliteGestionnaire:"+centreResponsabiliteGestionnaire);
            ajouterPartenaire(operation, (IPersonne) centreResponsabiliteGestionnaire, null, Boolean.FALSE, new BigDecimal(0));
        }
        return operation;
    }

    /**
     * Creation d'un objet Contrat, l'avenant 0 (contrat initial) est aussi
     * cree.
     * 
     * @param projet
     * @param etablissementGestionnaire
     *            Etablissement gestionnaire de la convention (obligatoire)
     * @param centreResponsabiliteGestionnaire
     *            Centre de responsabilite gestionnaire de la convention
     *            (obligatoire)
     * @param typeContrat
     *            Type de la convention, ex: Convention de recherche
     *            (obligatoire)
     * @param modeGestion
     *            Mode de gestion de la convention, ex: Ressource affectee
     *            (obligatoire)
     * @param referenceExterne
     *            Reference externe de la convention.
     * @param objet
     *            Intitule de l'bjet de la convention (obligatoire)
     * @param observations
     *            Observation diverses
     * @param dateDebut
     *            Date de debut de la convention (obligatoire)
     * @param dateFin
     *            Date de fin de la convention (obligatoire)
     * @param dateDebutExecution
     *            Date de debut d'execution de la convention
     * @param dateFinExecution
     *            Date de fin d'execution de la convention
     * @param dateSignature
     *            Date de signature en CA de la convention
     * @param dateCloture
     *            Date de cloture de la convention
     * @param montantGlobalHT
     *            Montant global HT du cotrat initial (obligatoire)
     * @param tauxTva
     *            Taux de TVA pour le montant global.
     * @param creditsLimitatifs
     *            Indique si le montant des credits positionnes est indepassable
     *            au moment d'une depense.
     * @param lucratif
     *            Indicateur de lucrativite de la convention
     * @param tvaRecuperable
     *            Indicateur de recuperation (collecte) de la convention
     * @param typeReconduction
     *            Type de reconduction
     * @param naf
     *            code naf (domaine fonctionnel)
     * @param createur
     *            Utilisateur createur de la convention (obligatoire)
     * @param creerPartenaireAuto
     *            si true, ajoute le centre gestionnaire au partenaire
     * @return Le contrat correspondant a la convention creee. Ce contrat
     *         possede un avenant zero implicite.
     * @throws Exception
     */
    public Operation creerConvention(
            final Projet projet,
            final TypeClassificationContrat typeClassificationContrat,
            final EOStructure etablissementGestionnaire,
            final EOStructure centreResponsabiliteGestionnaire,
            final TypeContrat typeContrat,
            final ModeGestion modeGestion,
            final String referenceExterne,
            final String objet,
            final String observations,
            final NSTimestamp dateDebut,
            final NSTimestamp dateFin,
            final Integer duree,
            final NSTimestamp dateDebutExecution,
            final NSTimestamp dateFinExecution,
            final NSTimestamp dateSignature,
            final NSTimestamp dateCloture,
            final BigDecimal montantGlobalHT,
            final EOTva tauxTva,
            final Boolean creditsLimitatifs,
            final Boolean lucratif,
            final Boolean tvaRecuperable,
            final TypeReconduction typeReconduction,
            final EONaf naf,
            final EOUtilisateur createur,
            final Boolean creerPartenaireAuto)
            throws Exception, ExceptionUtilisateur {

        // contrat
        Operation operation = contratFactory.creerContrat(
                projet,
                typeClassificationContrat,
                etablissementGestionnaire,
                centreResponsabiliteGestionnaire,
                typeContrat,
                referenceExterne,
                objet,
                observations,
                dateCloture,
                typeReconduction,
                naf,
                createur);

        // avenant zero
        avenantFactory.creerAvenant(
                operation,
                new Integer(0),
                TypeAvenant.TypeAvenantContratInitial(ec),
                modeGestion,
                referenceExterne,
                objet,
                observations,
                dateDebut,
                dateFin,
                duree,
                dateDebutExecution,
                dateFinExecution,
                dateSignature,
                montantGlobalHT,
                tauxTva,
                creditsLimitatifs,
                lucratif,
                tvaRecuperable,
                createur);

        if (creerPartenaireAuto.booleanValue())
            ajouterPartenaire(operation, (IPersonne) centreResponsabiliteGestionnaire, null, Boolean.FALSE, new BigDecimal(0));

        return operation;
    }

    /**
     * Creation d'une convention minimum, l'avenant 0 (contrat initial) est
     * aussi cree ainsi que le partenaire principal a partir de l'etablissement
     * de l'utilisateur
     * 
     * @param createur
     *            Utilisateur createur de la convention (obligatoire)
     * @return Le contrat correspondant a la convention creee. Ce contrat
     *         possede un avenant zero implicite.
     * @throws InstantiationException
     * @throws Exception
     */
    @Deprecated
    public Operation creerConventionVierge(final EOUtilisateur createur) throws Exception, ExceptionUtilisateur {
        return creerConventionVierge(createur, null);
    }

    /**
     * Creation d'une convention minimum, l'avenant 0 (contrat initial) est
     * aussi cree ainsi que le partenaire principal et le service gestionnaire
     * 
     * @param createur
     *            Utilisateur createur de la convention (obligatoire)
     * @param etablissementGestionnaire
     *            EOStructure service gestionnaire de la convention
     * @param serviceGestionnaire
     *            Utilisateur etablissement gestionnaire de la convention
     * @return Le contrat correspondant a la convention creee. Ce contrat
     *         possede un avenant zero implicite.
     * @throws InstantiationException
     * @throws Exception
     */
    @Deprecated
    public Operation creerConventionVierge(EOUtilisateur createur, EOStructure etablissementGestionnaire, EOStructure serviceGestionnaire)
            throws Exception, ExceptionUtilisateur {

        TypeClassificationContrat typeClassificationContrat = (TypeClassificationContrat) EOUtilities.objectMatchingKeyAndValue(ec,
                TypeClassificationContrat.ENTITY_NAME, TypeClassificationContrat.TCC_CODE_KEY, "OPE");
        Operation operation = contratFactory.creerContratVierge(typeClassificationContrat, createur);
        Avenant avenant = avenantFactory.creerAvenantVierge(
                operation,
                new Integer(0),
                TypeAvenant.TypeAvenantContratInitial(ec),
                createur);
        String groupeLibelle = "Partenaires de l'acte en cours de creation par " + createur.getLogin() + " le " + DateCtrl.now();
        EOStructure groupePere = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_PARTENARIAT");
        Integer persId = createur.personne().persId();
        EOTypeGroupe typeGroupePartenariat = (EOTypeGroupe) EOUtilities.objectMatchingKeyAndValue(ec, EOTypeGroupe.ENTITY_NAME,
                EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_PN);
        EOStructure groupe = EOStructureForGroupeSpec.creerGroupe(ec, persId, groupeLibelle, null, new NSArray(typeGroupePartenariat), groupePere);
        operation.setGroupePartenaireRelationship(groupe);

        if (etablissementGestionnaire != null) {
            // Ajout en tant que partenaire principal de la structure passee en
            // parametre
            OperationPartenaire partenaireInternePrincipal = ajouterPartenaire(operation, etablissementGestionnaire, null, Boolean.TRUE,
                    BigDecimal.valueOf(0));
            NSMutableDictionary values = new NSMutableDictionary();
            values.setObjectForKey("ETABINTPRINC", EOAssociation.ASS_CODE_KEY);
            EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
            EOAssociation associationEtabInternePrincipal = EOAssociation.fetchByQualifier(ec, qualifier);
            EOStructureForGroupeSpec.definitUnRole(ec, etablissementGestionnaire, associationEtabInternePrincipal, groupe, persId, null, null, null,
                    null, null, true);
            operation.setEtablissementRelationship(etablissementGestionnaire, createur.toPersonne().persId());
            if (serviceGestionnaire != null) {
                // Ajout du service gestionnaire en tant que contact de
                // l'etablissement
                FactoryOperationPartenaire fcp = new FactoryOperationPartenaire(ec, Boolean.TRUE);
                values = new NSMutableDictionary();
                values.setObjectForKey("REPRGESTPART", EOAssociation.ASS_CODE_KEY);
                qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
                EOAssociation associationRepresentantGestionnairePartenariat = EOAssociation.fetchByQualifier(ec, qualifier);

                fcp.ajouterContact(partenaireInternePrincipal, serviceGestionnaire, new NSArray(associationRepresentantGestionnairePartenariat));

                // EOStructureForGroupeSpec.affecterPersonneDansGroupe(ec,
                // etablissementGestionnaire, groupe, persId,
                // associationEtabInternePrincipal);
                operation.setCentreResponsabiliteRelationship(serviceGestionnaire, createur.toPersonne().persId());
            }
        }

        return operation;
    }

    /**
     * Creation d'une convention minimum, l'avenant 0 (contrat initial) est
     * aussi cree ainsi que le partenaire principal a partir de l'etablissement
     * de l'utilisateur
     * 
     * @param createur
     *            Utilisateur createur de la convention (obligatoire)
     * @return Le contrat correspondant a la convention creee. Ce contrat
     *         possede un avenant zero implicite.
     * @throws InstantiationException
     * @throws Exception
     */
    @Deprecated
    public Operation creerConventionVierge(EOUtilisateur createur, IPersonne etablissementPrincipal) throws Exception, ExceptionUtilisateur {
        TypeClassificationContrat typeClassificationContrat = (TypeClassificationContrat) EOUtilities.objectMatchingKeyAndValue(ec,
                TypeClassificationContrat.ENTITY_NAME, TypeClassificationContrat.TCC_CODE_KEY, "OPE");
        Operation operation = contratFactory.creerContratVierge(typeClassificationContrat, createur);
        Avenant avenant = avenantFactory.creerAvenantVierge(
                operation,
                new Integer(0),
                TypeAvenant.TypeAvenantContratInitial(ec),
                createur);

        // On teste que l'utilisateur est valide
        if (createur == null || createur.isValide() == false) {
            throw new Exception("Vous n'êtes pas un utlisateur valide (JEFY_ADMIN)");
        }
        String groupeLibelle = "Partenaires de l'acte en cours de creation par " + createur.getLogin() + " le " + DateCtrl.now();
        EOStructure groupePere = EOStructureForGroupeSpec.getGroupeForParamKey(ec, "ANNUAIRE_PARTENARIAT");
        Integer persId = createur.personne().persId();
        EOTypeGroupe typeGroupePartenariat = (EOTypeGroupe) EOUtilities.objectMatchingKeyAndValue(ec, EOTypeGroupe.ENTITY_NAME,
                EOTypeGroupe.TGRP_CODE_KEY, EOTypeGroupe.TGRP_CODE_PN);
        EOStructure groupe = EOStructureForGroupeSpec.creerGroupe(ec, persId, groupeLibelle, null, new NSArray(typeGroupePartenariat), groupePere);
        operation.setGroupePartenaireRelationship(groupe);
        if (etablissementPrincipal != null) {
            // Ajout en tant que partenaire principal de la structure passee en
            // parametre
            OperationPartenaire partenaireInternePrincipal = ajouterPartenaire(operation, etablissementPrincipal, null, Boolean.TRUE,
                    BigDecimal.valueOf(0));
            NSMutableDictionary values = new NSMutableDictionary();
            values.setObjectForKey(EOTypeAssociation.TAS_CODE_CONV, EOAssociation.TO_TYPE_ASSOCIATION_KEY + "." + EOTypeAssociation.TAS_CODE_KEY);
            values.setObjectForKey("PARTENAIRES", EOAssociation.ASS_CODE_KEY);
            EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
            EOAssociation associationPartenaires = EOAssociation.fetchByQualifier(ec, qualifier);
            values.setObjectForKey("ETABINTPRINC", EOAssociation.ASS_CODE_KEY);
            EOAssociation associationEtabInternePrincipal = EOAssociation.fetchByQualifier(ec, qualifier);
            EOStructureForGroupeSpec.definitUnRole(ec, etablissementPrincipal, associationEtabInternePrincipal, groupe, persId, null, null, null,
                    null, null, true);
            operation.setEtablissementRelationship((EOStructure) etablissementPrincipal, createur.toPersonne().persId());
        }

        return operation;
    }

    /**
     * Modification d'une convention
     * 
     * @param operation
     *            Contrat correspondant a la convention a modifier (obligatoire)
     * @param etablissementGestionnaire
     *            Etablissement gestionnaire de la convention (obligatoire)
     * @param centreResponsabiliteGestionnaire
     *            Centre de responsabilite gestionnaire de la convention
     *            (obligatoire)
     * @param typeContrat
     *            Type de la convention, ex: Convention de recherche
     *            (obligatoire)
     * @param modeGestion
     *            Mode de gestion de la convention, ex: Ressource affectee
     *            (obligatoire)
     * @param referenceExterne
     *            Reference externe de la convention.
     * @param objet
     *            Intitule de l'bjet de la convention (obligatoire)
     * @param observations
     *            Observation diverses
     * @param dateDebut
     *            Date de debut de la convention (obligatoire)
     * @param dateFin
     *            Date de fin de la convention (obligatoire)
     * @param dateDebutExecution
     *            Date de debut d'execution de la convention
     * @param dateFinExecution
     *            Date de fin d'execution de la convention
     * @param dateSignature
     *            Date de signature en CA de la convention
     * @param dateCloture
     *            Date de cloture de la convention
     * @param montantGlobalHT
     *            Montant global HT de la convention
     * @param tauxTva
     *            Taux de TVA pour le montant global.
     * @param creditsLimitatifs
     *            Indique si le montant des credits positionnes est indepassable
     *            au moment d'une depense.
     * @param lucratif
     *            Indicateur de lucrativite de la convention
     * @param tvaRecuperable
     *            Indicateur de recuperation (collecte) de la convention
     * @param typeReconduction
     *            Type de reconduction (ex: Expresse)
     * @param naf
     *            code naf (domaine fonctionnel)
     * @param utilisateur
     *            Utilisateur createur de la convention (obligatoire)
     * @throws ExceptionArgument
     */
    public void modifierConvention(
            final Operation operation,
            final TypeClassificationContrat typeClassificationContrat,
            final EOStructure etablissementGestionnaire,
            final EOStructure centreResponsabiliteGestionnaire,
            final TypeContrat typeContrat,
            final ModeGestion modeGestion,
            final String referenceExterne,
            final String objet,
            final String observations,
            final NSTimestamp dateDebut,
            final NSTimestamp dateFin,
            final Integer duree,
            final NSTimestamp dateDebutExecution,
            final NSTimestamp dateFinExecution,
            final NSTimestamp dateSignature,
            final NSTimestamp dateCloture,
            final BigDecimal montantGlobalHT,
            final EOTva tauxTva,
            final Boolean creditsLimitatifs,
            final Boolean lucratif,
            final Boolean tvaRecuperable,
            final TypeReconduction typeReconduction,
            final EONaf naf,
            final EOUtilisateur utilisateur,
            final NSTimestamp dateModification) throws ExceptionArgument, Exception, ExceptionUtilisateur {

        // contrat
        contratFactory.modifierContrat(
                operation,
                typeClassificationContrat,
                referenceExterne,
                etablissementGestionnaire,
                centreResponsabiliteGestionnaire,
                typeContrat,
                objet,
                observations,
                dateCloture,
                typeReconduction,
                naf,
                utilisateur,
                dateModification);

        // avenant zero
        avenantFactory.modifierAvenant(
                operation.avenantZero(),
                modeGestion,
                centreResponsabiliteGestionnaire,
                referenceExterne,
                objet,
                observations,
                dateDebut,
                dateFin,
                duree,
                dateDebutExecution,
                dateFinExecution,
                dateSignature,
                montantGlobalHT,
                tauxTva,
                creditsLimitatifs,
                lucratif,
                tvaRecuperable,
                utilisateur,
                dateModification);
    }

    /**
     * Suppression d'une convention. Elle n'est pas supprimee mais archivee
     * (ainsi que ses avenants).
     * 
     * @param operation
     *            Contrat correspondant a la convention a supprimer.
     * @throws Exception
     */
    public void supprimerConvention(
            final Operation operation) throws Exception, ExceptionUtilisateur {

        contratFactory.supprimerContrat(operation);
        contratFactory.supprimerAvenants(operation, true);
    }

    /**
     * Suppression d'une convention par un ADMIN. Elle n'est pas supprimee mais
     * archivee (ainsi que ses avenants).
     * 
     * @param operation
     *            Contrat correspondant a la convention a supprimer.
     * @throws Exception
     */
    public void supprimerConventionAdmin(
            final Operation operation) throws Exception, ExceptionUtilisateur {

        contratFactory.supprimerContratAdmin(operation);
        contratFactory.supprimerAvenantsAdmin(operation, true);
    }

    /**
     * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais
     * archives.
     * 
     * @param operation
     *            Contrat dont on veut supprimer les avenants.
     * @param avenantZeroAussi
     *            Mettre a <code>true</code> pour supprimer egalement l'avenant
     *            "zero" (= contrat initial).
     * @throws ExceptionArgument
     */
    public void supprimerAvenants(
            final Operation operation,
            final boolean avenantZeroAussi) throws ExceptionUtilisateur {

        trace("supprimerAvenants()");
        trace(operation);
        trace(new Boolean(avenantZeroAussi));

        // Verifier que la convention n'est pas validee
        if (operation.conDateValidAdm() != null) {
            throw new ExceptionUtilisateur(
                    "La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n" +
                            "Il n'est donc plus possible de la supprimer.");
        }

        for (int i = 0; i < operation.avenantsDontInitialNonSupprimes().count(); i++) {
            Avenant avenant = (Avenant) operation.avenantsDontInitialNonSupprimes().objectAtIndex(i);
            if (avenantZeroAussi || avenant.avtIndex().intValue() != 0) {
                this.avenantFactory.supprimerAvenant(avenant);
                // System.out.println("Avenant "+avenant.avtIndex()+" archiv\u00E9.");
            }
        }
    }

    /**
     * Invalide (refetch) la convention : le contrat et l'avenant zero.
     * 
     * @param operation
     *            Contrat correspondant a la convention.
     */
    public void invalidateConvention(final Operation operation) {
        NSMutableArray gids = new NSMutableArray();
        gids.addObject(ec.globalIDForObject(operation));
        gids.addObject(ec.globalIDForObject(operation.avenantZero()));
        ec.invalidateObjectsWithGlobalIDs(gids);
    }

    /**
     * Ajout d'un partenaire a une convention.
     * 
     * @param operation
     *            Contrat correspondant a la convention a laquelle on veut
     *            ajouter un partenaire.
     * @param personne
     *            Personne physique ou morale partenaire
     * @param typePartenaire
     *            Type du partenariat.
     * @param partenairePrincipal
     *            <code>true</code> pour le declarer comme partenaire principal
     * @param montantApporte
     *            Montant apporte par ce partenaire.
     * @return La partenariat cree.
     * @throws Exception
     */
    public OperationPartenaire ajouterPartenaire(
            final Operation operation,
            final IPersonne personne,
            final NSArray roles,
            final Boolean partenairePrincipal,
            final BigDecimal montantApporte) throws Exception, ExceptionUtilisateur {

        return ajouterPartenaireSurPeriode(
                operation,
                personne,
                roles,
                null,// dateDebut
                null,// dateFin
                partenairePrincipal,
                montantApporte,
                null); // rasCommentaire
    }

    /**
     * Ajout d'un partenaire a une convention.
     * 
     * @param operation
     *            Contrat correspondant a la convention a laquelle on veut
     *            ajouter un partenaire.
     * @param personne
     *            Personne physique ou morale partenaire
     * @param typePartenaire
     *            Type du partenariat.
     * @param partenairePrincipal
     *            <code>true</code> pour le declarer comme partenaire principal
     * @param montantApporte
     *            Montant apporte par ce partenaire.
     * @return La partenariat cree.
     * @throws Exception
     */
    public OperationPartenaire ajouterPartenaireSurPeriode(
            final Operation operation,
            final IPersonne personne,
            final NSArray roles,
            final NSTimestamp dateDebut,
            final NSTimestamp dateFin,
            final Boolean partenairePrincipal,
            final BigDecimal montantApporte,
            final String rasCommentaire) throws Exception, ExceptionUtilisateur {

        if (operation == null) {
            throw new ExceptionUtilisateur("La convention doit \u00EAtre fournie pour lui ajouter un partenaire.");
        }

        // On verifie que le partenariat n'existe pas deja
        System.out.println("ajouterPartenaireSurPeriode : " + personne.persId());

        NSArray partenariats = EOQualifier.filteredArrayWithQualifier(
                operation.operationPartenaires(),
                EOQualifier.qualifierWithQualifierFormat("persId = %@", new NSArray(personne.persId())));
        if (partenariats != null && partenariats.count() > 0) {
            throw new ExceptionUtilisateur("Ce partenariat existe d\u00E9j\u00E0.");
        }

        BigDecimal montant = montantApporte;
        if (montantApporte != null && operation.montantDisponiblePourPartenariat().compareTo(montantApporte) < 0) {
            // montant = contrat.montantDisponiblePourPartenariat();
            throw new ExceptionUtilisateur(
                    "La somme des montants apport\u00E9s par les partenaires ne peut d\u00E9passer le montant global de la " +
                            "convention (avenants compris).\n\n" +
                            "Le montant apport\u00E9 par le partenaire '" +
                            personne +
                            "' (" +
                            nombreOperation.getFormatterMoney().format(montantApporte) +
                            ") d\u00E9passe le montant non encore affect\u00E9 de cette convention qui est de " +
                            nombreOperation.getFormatterMoney().format(operation.montantDisponiblePourPartenariat()) +
                            ".\n");
        }

        OperationPartenaire contPartenaire = contratPartenaireFactory.creerOperationPartenaire(
                operation,
                personne,
                roles,
                dateDebut,
                dateFin,
                partenairePrincipal,
                montant,
                rasCommentaire);
        if (contPartenaire != null) {
            NSMutableDictionary userInfo = new NSMutableDictionary();
            userInfo.setObjectForKey(ec, "edc");
            NSNotificationCenter.defaultCenter().postNotification("refreshPartenairesNotification", contPartenaire, userInfo);
        }
        return contPartenaire;
    }

    /**
     * Supprime une tranche. Ele est seulement marquee comme telle.
     * 
     * @param tranche
     *            Tranche a supprimer.
     * @throws ExceptionUtilisateur
     */
    public void supprimerTranche(
            final Tranche tranche) throws ExceptionUtilisateur {

        if (tranche == null) {
            throw new ExceptionUtilisateur("La tranche \u00E0 supprimer est requise.");
        }

        tranche.setTraSuppr(Tranche.TRA_SUPPR_OUI);
    }

    /**
     * Instancie autant de tranches que necessaire pour une convention. Cad pour
     * les exercices non deja affectes.
     * 
     * @param operation Convention concernee.
     * @param createur  Utilisateur enregistre comme createur des tranches.
     * @return Liste des tranches creees.
     * @throws ExceptionFinder
     * @throws InstantiationException
     * @throws ExceptionOperationImpossible
     */
    public NSArray genererTranches(Operation operation, final EOUtilisateur createur) 
    		throws ExceptionFinder, Exception, ExceptionOperationImpossible {

        if (operation == null) {
            throw new NullPointerException("Une opération est requise.");
        }
        if (createur == null) {
            throw new NullPointerException("L\\\'utilisateur cr\u00E9ateur est requis.");
        }
        if (operation.dateDebut() == null) { 
            throw new NullPointerException("Une date de d\u00E9but est requise.");
        }
        if (operation.montantHt() == null || operation.montantHt().doubleValue() <= 0) {
            throw new NullPointerException("Un montant HT sup\u00E9rieur à 0 est requis.");
        }

        NSArray<EOExerciceCocktail> exercicesTous = exercicesCocktailCouvertParOperation(ec, operation);
        NSArray<EOExerciceCocktail> exercicesManquants = determinerExercicesCocktailManquants(operation, exercicesTous);
        NSArray<Tranche> nvllesTranchesGenerees = genererTranchesManquantes(operation, exercicesManquants, createur);

        return nvllesTranchesGenerees;
    }
    
    private NSArray<EOExerciceCocktail> exercicesCocktailCouvertParOperation(EOEditingContext edc, Operation operation) throws ExceptionFinder {
    	FinderExerciceCocktail fe = new FinderExerciceCocktail(edc);
    	if (operation.dateFin() == null) {
            return fe.findWithDatesDebutFin(operation.dateDebut(), operation.dateDebut());
        } 
    	
        // Recherche les exercices non deja utilises dans les tranches existantes
        return fe.findWithDatesDebutFin(operation.dateDebut(), operation.dateFin());
    }
    
    private NSArray<EOExerciceCocktail> determinerExercicesCocktailManquants(Operation operation, NSArray<EOExerciceCocktail> exercicesACouvrir) {
    	NSArray<Tranche> tranchesContrat = operation.tranches();
        NSArray<EOExerciceCocktail> exercicesTranches = (NSArray<EOExerciceCocktail>) tranchesContrat.valueForKey(Tranche.EXERCICE_COCKTAIL_KEY);
    	NSMutableArray<EOExerciceCocktail> exercicesManquants = new NSMutableArray<EOExerciceCocktail>();
        for (int i = 0; i < exercicesACouvrir.count(); i++) {
            if (!exercicesTranches.containsObject(exercicesACouvrir.objectAtIndex(i))) {
                exercicesManquants.addObject(exercicesACouvrir.objectAtIndex(i));
            }
        }
        
        return exercicesManquants.immutableClone();
    }
    
    private NSArray<Tranche> genererTranchesManquantes(Operation operation, NSArray<EOExerciceCocktail> exercicesManquants, EOUtilisateur createur) throws ExceptionUtilisateur, Exception {
    	NSMutableArray<Tranche> nvllesTranchesGenerees = new NSMutableArray<Tranche>();
        FactoryTranche ft = new FactoryTranche(operation.editingContext());
        for (int i = 0; i < exercicesManquants.count(); i++) {
        	Tranche currentTranche = ft.creerTranche(
                    operation,
                    (EOExerciceCocktail) exercicesManquants.objectAtIndex(i),
                    createur);

        	nvllesTranchesGenerees.addObject(currentTranche);
            NotificationCenter.instance().notifier(Notification.REFRESH_TRANCHES, operation.editingContext());
        }
        
        return nvllesTranchesGenerees;
    }

    public OperationPartenaire ajouterPartenaireContractant(Operation operation, IPersonne personneAAjouter, Boolean partenairePrincipal,
            BigDecimal montantApporte) throws ExceptionUtilisateur, Exception {
        // Tous les partenaires ont un role par defaut de partenaire contractant
        // dans le groupe Partenaire correspondant au contrat
        NSMutableDictionary values = new NSMutableDictionary();
        values.setObjectForKey(EOTypeAssociation.TAS_CODE_CONV, EOAssociation.TO_TYPE_ASSOCIATION_KEY + "." + EOTypeAssociation.TAS_CODE_KEY);
        values.setObjectForKey("PARTCONTRACT", EOAssociation.ASS_CODE_KEY);
        EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
        EOAssociation association = EOAssociation.fetchByQualifier(personneAAjouter.editingContext(), qualifier);
        return ajouterPartenaire(operation, personneAAjouter, new NSArray(association), partenairePrincipal, montantApporte);
    }

    public void supprimerPartenaire(Operation operation, OperationPartenaire partenaireASupprimer, Integer persIdUtilisateur) {
        // TODO Verifier avant de lancer la suppression du partenaire que le
        // contrat n'est pas deja engage
        if (operation != null && partenaireASupprimer != null) {
            // suppression du partenaire du groupe associe au contrat
            EOStructureForGroupeSpec.supprimerPersonneDuGroupe(operation.editingContext(), partenaireASupprimer.partenaire(),
                    operation.groupePartenaire(), persIdUtilisateur);
            operation.removeFromOperationPartenairesRelationship(partenaireASupprimer);
            // Suppression des repartPartenaireTranches
            NSArray tranches = operation.tranches();
            Enumeration<Tranche> enumTranches = tranches.objectEnumerator();
            while (enumTranches.hasMoreElements()) {
                Tranche tranche = (Tranche) enumTranches.nextElement();
                NSArray rpt = tranche.toRepartPartenaireTranches();
                EOKeyValueQualifier qualifier = new EOKeyValueQualifier(RepartPartenaireTranche.OPERATION_PARTENAIRE_KEY,
                        EOQualifier.QualifierOperatorEqual, partenaireASupprimer);
                rpt = EOQualifier.filteredArrayWithQualifier(rpt, qualifier);
                Enumeration<RepartPartenaireTranche> enumRpt = rpt.immutableClone().objectEnumerator();
                while (enumRpt.hasMoreElements()) {
                    RepartPartenaireTranche repartPartenaireTranche = (RepartPartenaireTranche) enumRpt.nextElement();
                    tranche.removeObjectFromBothSidesOfRelationshipWithKey(repartPartenaireTranche, RepartPartenaireTranche.OPERATION_PARTENAIRE_KEY);
                    ec.deleteObject(repartPartenaireTranche);
                }
            }

        }
    }

}
