// DO NOT EDIT.  Make changes to RepartSbDepensePlanComptable.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartSbDepensePlanComptable extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartSbDepensePlanComptable";
	public static final String ENTITY_TABLE_NAME = "GFC.REPART_SB_DEPENSE_PLANCO";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT_HT = new ERXKey<java.math.BigDecimal>("montantHt");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense> SB_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense>("sbDepense");

  // Attributes
  public static final String MONTANT_HT_KEY = MONTANT_HT.key();
  // Relationships
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String SB_DEPENSE_KEY = SB_DEPENSE.key();

  private static Logger LOG = Logger.getLogger(EORepartSbDepensePlanComptable.class);

  public RepartSbDepensePlanComptable localInstanceIn(EOEditingContext editingContext) {
    RepartSbDepensePlanComptable localInstance = (RepartSbDepensePlanComptable)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montantHt() {
    return (java.math.BigDecimal) storedValueForKey(EORepartSbDepensePlanComptable.MONTANT_HT_KEY);
  }

  public void setMontantHt(java.math.BigDecimal value) {
    if (EORepartSbDepensePlanComptable.LOG.isDebugEnabled()) {
        EORepartSbDepensePlanComptable.LOG.debug( "updating montantHt from " + montantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartSbDepensePlanComptable.MONTANT_HT_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable)storedValueForKey(EORepartSbDepensePlanComptable.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable value) {
    takeStoredValueForKey(value, EORepartSbDepensePlanComptable.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable value) {
    if (EORepartSbDepensePlanComptable.LOG.isDebugEnabled()) {
      EORepartSbDepensePlanComptable.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbDepensePlanComptable.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbDepensePlanComptable.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense sbDepense() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense)storedValueForKey(EORepartSbDepensePlanComptable.SB_DEPENSE_KEY);
  }
  
  public void setSbDepense(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense value) {
    takeStoredValueForKey(value, EORepartSbDepensePlanComptable.SB_DEPENSE_KEY);
  }

  public void setSbDepenseRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense value) {
    if (EORepartSbDepensePlanComptable.LOG.isDebugEnabled()) {
      EORepartSbDepensePlanComptable.LOG.debug("updating sbDepense from " + sbDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSbDepense(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense oldValue = sbDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartSbDepensePlanComptable.SB_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartSbDepensePlanComptable.SB_DEPENSE_KEY);
    }
  }
  

  public static RepartSbDepensePlanComptable create(EOEditingContext editingContext, org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptable planco, org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbDepense sbDepense) {
    RepartSbDepensePlanComptable eo = (RepartSbDepensePlanComptable) EOUtilities.createAndInsertInstance(editingContext, EORepartSbDepensePlanComptable.ENTITY_NAME);    
    eo.setPlancoRelationship(planco);
    eo.setSbDepenseRelationship(sbDepense);
    return eo;
  }

  public static ERXFetchSpecification<RepartSbDepensePlanComptable> fetchSpec() {
    return new ERXFetchSpecification<RepartSbDepensePlanComptable>(EORepartSbDepensePlanComptable.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartSbDepensePlanComptable> fetchAll(EOEditingContext editingContext) {
    return EORepartSbDepensePlanComptable.fetchAll(editingContext, null);
  }

  public static NSArray<RepartSbDepensePlanComptable> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartSbDepensePlanComptable.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartSbDepensePlanComptable> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartSbDepensePlanComptable> fetchSpec = new ERXFetchSpecification<RepartSbDepensePlanComptable>(EORepartSbDepensePlanComptable.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartSbDepensePlanComptable> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartSbDepensePlanComptable fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbDepensePlanComptable.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbDepensePlanComptable fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartSbDepensePlanComptable> eoObjects = EORepartSbDepensePlanComptable.fetchAll(editingContext, qualifier, null);
    RepartSbDepensePlanComptable eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartSbDepensePlanComptable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbDepensePlanComptable fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartSbDepensePlanComptable.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartSbDepensePlanComptable fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartSbDepensePlanComptable eoObject = EORepartSbDepensePlanComptable.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartSbDepensePlanComptable that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartSbDepensePlanComptable localInstanceIn(EOEditingContext editingContext, RepartSbDepensePlanComptable eo) {
    RepartSbDepensePlanComptable localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}