package org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes;

import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureRec;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureRecExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecetteExercice;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche.EntiteBudgetaireExerciceValidator;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEOControlUtilities;

public class BudgetCombinaisonAxesRecetteExerciceValidator {

	private EOEditingContext edc;
	private EOExercice exercice;
	private EntiteBudgetaireExerciceValidator ebValidator;
	private BudgetCombinaisonAxesRecetteNotificationHandler handler;
	
	public BudgetCombinaisonAxesRecetteExerciceValidator(EOEditingContext edc,
			EOExercice exercice, BudgetCombinaisonAxesRecetteNotificationHandler handler) {
		this.edc = edc;
		this.exercice = exercice;
		this.ebValidator = new EntiteBudgetaireExerciceValidator(edc, exercice);
		this.handler = handler;
	}
	
	public boolean isValid(BudgetCombinaisonAxesRecette axes) {
		boolean isEbValideSurExercice = ebValidator.isValid(axes.getEb());
		if (!isEbValideSurExercice) {
			handler().marquerEbEnErreur();
		}
		
		boolean isNatureValideSurExercice = isNatureValideSurExercice(axes.getNature());
		if (!isNatureValideSurExercice) {
			handler().marquerNatureEnErreur();
		}
		
		boolean isOrigineValideSurExercice = isOrigineValideSurExercice(axes.getOrigine());
		if (!isOrigineValideSurExercice) {
			handler().marquerOrigineEnErreur();
		}
		
		return isEbValideSurExercice && isNatureValideSurExercice && isOrigineValideSurExercice;
	}

	private boolean isNatureValideSurExercice(EONatureRec nature) {
		EOQualifier qualExisteSurExercice = EONatureRecExercice.ADM_NATURE_REC.eq(nature)
				.and(EONatureRecExercice.EXE_ORDRE.eq(exercice.exeExercice().intValue()));
		int count = ERXEOControlUtilities.objectCountWithQualifier(edc, EONatureRecExercice.ENTITY_NAME, qualExisteSurExercice);
		return count > 0;
	}
	
	private boolean isOrigineValideSurExercice(EOOrigineRecette origine) {
		boolean isValide = origine.estDansUnEtatValide();
		if (!isValide) {
			return false;
		}
		EOQualifier qualExisteSurExercice = EOOrigineRecetteExercice.ORIGINE_RECETTE.eq(origine)
				.and(EOOrigineRecetteExercice.EXE_ORDRE.eq(exercice.exeExercice().intValue()));
		int count = ERXEOControlUtilities.objectCountWithQualifier(edc, EOOrigineRecetteExercice.ENTITY_NAME, qualExisteSurExercice);
		return count > 0;
	}
	
	private BudgetCombinaisonAxesRecetteNotificationHandler handler() {
		return handler;
	}
}
