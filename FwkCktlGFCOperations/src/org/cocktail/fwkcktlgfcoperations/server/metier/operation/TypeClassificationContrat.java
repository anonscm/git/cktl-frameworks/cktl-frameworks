// TypeClassificationContrat.java
// Created on Thu Mar 12 16:18:38 Europe/Paris 2009 by Apple EOModeler Version 5.2

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionCompositePrimaryKey;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionNullPrimaryKey;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionTemporaryPrimaryKey;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.common.tools.ModelUtilities;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;


public class TypeClassificationContrat extends EOTypeClassificationContrat {

    public static final String TYPE_CLASSIFICATION_CODE_OPE = "OPE";
    public static final String TYPE_CLASSIFICATION_CODE_CONV = "CONV";
    public static final String TYPE_CLASSIFICATION_CODE_AAP = "AAP";
    public static final String TYPE_CLASSIFICATION_CODE_CONV_STAGE = "CONV_STAGE";
    
    public TypeClassificationContrat() {
        super();
    }
    
    public boolean isTypeOpe() {
        return TYPE_CLASSIFICATION_CODE_OPE.equals(tccCode());
    }
    
    /**
     * Tente de determine la cle primaire de ce TypeClassificationContrat.
     * @return Cle primaire de ce TypeClassificationContrat.
     * @throws ExceptionNullPrimaryKey 
     * @throws ExceptionTemporaryPrimaryKey 
     * @throws ExceptionCompositePrimaryKey 
     */
    public Number getPrimaryKey() throws ExceptionNullPrimaryKey, ExceptionTemporaryPrimaryKey, ExceptionCompositePrimaryKey {
    	return (Number) new ModelUtilities().primaryKeyForObject(this);
	}
    
    static public TypeClassificationContrat defaultTypeClassificationContrat(EOEditingContext ec) {
    	TypeClassificationContrat result = typeClassificationContratForCode(ec, "OPE");
    	return result; 
    }
    
    static public TypeClassificationContrat typeClassificationContratForCode(EOEditingContext ec, String code) {
    	NSArray result = Finder.find(ec, "OpeTypeClassificationContrat", EOQualifier.qualifierWithQualifierFormat("tccCode = %@", new NSArray(code)), null);
    	return result.count()==1 ? (TypeClassificationContrat)result.objectAtIndex(0) : null; 
    }


}
