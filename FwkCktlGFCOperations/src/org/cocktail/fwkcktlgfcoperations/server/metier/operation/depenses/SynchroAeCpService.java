package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;
import java.util.List;

import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepCP;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class SynchroAeCpService {

    private static SynchroAeCpService instance;
    
    public static SynchroAeCpService getInstance() {
        if (instance == null) {
            return new SynchroAeCpService();
        }
        return instance;
    }
    
    /**
     * Modifie/supprime le CP lié à une AE donnée
     * @param tranche
     * @param trancheDepAE
     */
    public void supprimerOuMettreAJourCPsPersonnel(EOEditingContext edc, Tranche tranche, TrancheBudgetDepAE trancheDepAE) {
        creerOuMettreAJourCPsPersonnel(edc, tranche, trancheDepAE, null, null, null);
    }
    
    /**
     * Gère la création/modification/suppression des CP de nature Personnel ; modifie l'editing context de la tranche sans le sauver
     * @param tranche
     * @param trancheDepAE
     * @param newAxes
     * @param montantAE
     * @param persId
     */
    public void creerOuMettreAJourCPsPersonnel(EOEditingContext edc, Tranche tranche, TrancheBudgetDepAE trancheDepAE, BudgetCombinaisonAxesDepense newAxes,
            BigDecimal montantAE, Integer persId) {
        boolean isMemeAxe = false;
        BigDecimal oldMontantAE = null;
        if (trancheDepAE != null) {
            EONatureDep oldNature = trancheDepAE.natureDepense();
            if (oldNature.isPersonnel()) {
                BudgetCombinaisonAxesDepense ancienAxes = trancheDepAE.combinaisonAxes();
                isMemeAxe = ancienAxes.equals(newAxes);
                TrancheBudgetDepCP oldCP = tranche.trancheBudgetsDepCP(ancienAxes);                
                oldMontantAE = trancheDepAE.tbMontant();
                if (oldCP != null) {
                    BigDecimal montantOldCP = oldCP.montantCp();
                    montantOldCP = montantOldCP.add(oldMontantAE.negate());
                    if (isMemeAxe) {
                        montantOldCP = montantOldCP.add(montantAE);
                    }
                    if (BigDecimal.ZERO.compareTo(montantOldCP) == 0) {
                        if (tranche.estBrouillon()) {
                            tranche.removeFromTrancheBudgetDepsCP(oldCP);
                            edc.deleteObject(oldCP);
                        }
                    } else {
                        oldCP.setMontantCp(montantOldCP);
                        oldCP.setPersIdModif(persId);
                        oldCP.setDateModification(new NSTimestamp());
                    }
                } else {
                    // anormal : pas de CP existante alors qu'il existait une AE de nature Personnel
                }
            }
        }
        if (newAxes != null && newAxes.getNature().isPersonnel() && !isMemeAxe) {
            TrancheBudgetDepCP newCP = tranche.trancheBudgetsDepCP(newAxes);
            if (newCP != null) {
                BigDecimal montantNewCP = newCP.montantCp();
                montantNewCP = montantNewCP.add(montantAE);
                newCP.setMontantCp(montantNewCP);
                newCP.setPersIdModif(persId);
                newCP.setDateModification(new NSTimestamp()); 
            } else {
                newCP = TrancheBudgetDepCP.creerDepenseCP(tranche, newAxes, montantAE, persId);
            }
        }
    }
    
    /**
     * 
     * @param edc editing context
     * @param operation operation contenant les tranches AE / CP.
     * @param combinaisonRecherchee combinaison sur laquelle doit s'effectuer le controle des orphelins.
     * @return le nombre d'orphelins supprimes.
     */
    public int supprimerCreditsPaiementsOrphelins(EOEditingContext edc, Operation operation, BudgetCombinaisonAxesDepense combinaisonRecherchee) {
    	if (isCombinaisonExisteEnAe(operation, combinaisonRecherchee)) {
    		return 0;
    	}
    	
    	return supprimerCreditsPaiements(edc, operation, combinaisonRecherchee);
    }
    
    private boolean isCombinaisonExisteEnAe(Operation operation, BudgetCombinaisonAxesDepense combinaisonRecherchee) {
    	NSMutableArray<TrancheBudgetDepAE> listeAeSurCombinaison = new NSMutableArray<TrancheBudgetDepAE>(
    			operation.tousLesAE(combinaisonRecherchee));
    	return listeAeSurCombinaison.count() > 0;
    }
    
    private int supprimerCreditsPaiements(EOEditingContext edc, Operation operation, BudgetCombinaisonAxesDepense combinaison) {
    	int nbSuppression = 0;
    	List<TrancheBudgetDepCP> listeCp = operation.tousLesCP(combinaison);
    	for (TrancheBudgetDepCP cp : listeCp) {
    		cp.tranche().removeFromTrancheBudgetDepsCP(cp);
    		edc.deleteObject(cp);
    		nbSuppression++;
    	}
    	
    	return nbSuppression;
    }
}
