
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.utilitaire;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionProcedure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSKeyValueCoding;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class ProcedureGetConOrdresPositPourLb extends ProcedureUtilitaire 
{
	protected final static String PROCEDURE_NAME = "GetConOrdresPositPourLb";
	
	protected final static String EXE_ORDRE_ARG_NAME = 	"010exeOrdre";
	protected final static String ORG_ID_ARG_NAME = 	"020orgId";
	
	protected final static String RETURN_VALUE_NAME = 	"030conOrdres";
	
	
	/**
	 * Constructeur.
	 * @param ec
	 * @param name
	 * @param orderedArgumentsNames
	 */
	public ProcedureGetConOrdresPositPourLb(EOEditingContext ec, String name, String[] orderedArgumentsNames) {
		super(
				ec, 
				PROCEDURE_NAME, 
				new String[] { EXE_ORDRE_ARG_NAME, ORG_ID_ARG_NAME });
	}

	
	/**
	 * Lance la procedure pour la convention et la ligne budgetaire specifies.
	 * @param exeOrdre Identifiant de l'exercice. Obligatoire.
	 * @param orgId Identifiant de la ligne budgetaire. 
	 * Peut etre <code>null</code> (SAUF pour les conventions gerees en Ressoures Affectees) pour totaliser qqsoit la LB.
	 * @return Le resultat renvoye par la procedure.
	 * @throws ExceptionProcedure Erreur lors de l'appel a la procedure stoquee.
	 */
	public BigDecimal execute(
			final Number exeOrdre, 
			final Number orgId) throws ExceptionProcedure {
		
		Object[] argumentsValues = new Object[] { exeOrdre, orgId };
		
		System.out.println("ProcedureGetConOrdresPositPourLb.execute() arguments = ");
		System.out.println("exeOrdre = "+exeOrdre);
		System.out.println("orgId = "+orgId);
		
		try {
			NSDictionary dico = super.execute(argumentsValues);
			Object resultat = dico.valueForKey(RETURN_VALUE_NAME);
			
			if (resultat == NSKeyValueCoding.NullValue)
				return new BigDecimal(0);
			else
				return (BigDecimal) resultat;
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ExceptionProcedure(e.getMessage());
		}
	}	
	/**
	 * Lance la procedure pour la convention et la ligne de l'organigramme budgetaire specifies.
	 * @param exercice L'exercice. Obligatoire. 
	 * @param organ La ligne budgetaire. 
	 * Peut etre <code>null</code> (SAUF pour les conventions gerees en Ressoures Affectees) pour totaliser qqsoit la LB.
	 * @return Le resultat renvoye par la procedure.
	 * @throws ExceptionProcedure Erreur lors de l'appel a la procedure stoquee.
	 */
	public BigDecimal execute(
			final EOExercice exercice, 
			final EOEb organ) throws Exception {

		Number exeOrdre=null;
		Number orgId=null;
		String info=null;
		try {
			info = "de l'exercice";
			exeOrdre = (Number) modelUtilities.primaryKeyForObject(exercice);
			info = "de la ligne budg\u00E9taire";
			orgId = organ!=null ? (Number) modelUtilities.primaryKeyForObject(organ) : null;
		} 
		catch (Exception e) {
			throw new Exception("Impossible d'obtenir l'identifiant "+info);
		}

		return execute(exeOrdre, orgId);
	}	
}
