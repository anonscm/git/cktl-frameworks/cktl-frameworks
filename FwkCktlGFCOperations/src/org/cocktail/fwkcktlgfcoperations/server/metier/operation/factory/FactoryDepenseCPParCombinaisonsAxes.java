package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;

import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetDepAE;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.BudgetCombinaisonAxesDepense;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.DepenseCPSurCombinaisonAxes;

public class FactoryDepenseCPParCombinaisonsAxes {

    public static TreeMap<BudgetCombinaisonAxesDepense, DepenseCPSurCombinaisonAxes> creer(Operation operation, EOExercice exer) {
        TreeMap<BudgetCombinaisonAxesDepense, DepenseCPSurCombinaisonAxes> mapCombi = new TreeMap<BudgetCombinaisonAxesDepense, DepenseCPSurCombinaisonAxes>();
        SortedSet<BudgetCombinaisonAxesDepense> listeCombi = new TreeSet<BudgetCombinaisonAxesDepense>();
        for (TrancheBudgetDepAE trancheBudAe : operation.toTranchesDepensesAE()) {
            BudgetCombinaisonAxesDepense combi = trancheBudAe.combinaisonAxes();
            listeCombi.add(combi);
        }
        for (BudgetCombinaisonAxesDepense combi : listeCombi) {
            DepenseCPSurCombinaisonAxes tableauDepCP = new DepenseCPSurCombinaisonAxes(operation, combi, exer);
            mapCombi.put(combi, tableauDepCP);
        }
        return mapCombi;
    }
}
