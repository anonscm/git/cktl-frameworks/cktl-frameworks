package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import java.math.BigDecimal;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.common.text.StringOperation;
import org.cocktail.fwkcktlgfcoperations.common.tools.ModelUtilities;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * Composant permettant de rechercher des conventions.
 * 
 * @author Michael Haller, Consortium Cocktail, 2009
 */
public class FinderConvention extends Finder
{
	protected Number exerciceCreation;
	protected Number contratIndex;
	protected String contratObjet;
	protected String contratReferenceExterne;
	protected TypeContrat typeContrat;
	protected Number conOrdre;
	protected TypeClassificationContrat typeClassificationContrat;
	protected Projet projet;
	
	protected EOQualifier qualifierExerciceCreation;
	protected EOQualifier qualifierContratIndex;
	protected EOQualifier qualifierContratObjet;
	protected EOQualifier qualifierContratReferenceExterne;
	protected EOQualifier qualifierTypeContrat;
	protected EOQualifier qualifierConOrdre;
	protected EOQualifier qualifierTypeClassificationContrat;
	protected EOQualifier qualifierProjet;
	
	
	/**
	 * 
	 * 
	 * @param ec
	 */
	public FinderConvention(EOEditingContext ec) {
		super(ec, Operation.ENTITY_NAME);
		
		this.addMandatoryQualifier(EOQualifier.qualifierWithQualifierFormat("conSuppr = %@", new NSArray("N")));
	}
	
	/**
	 * Change le critere
	 * @param exerciceCreation Exercice de creation de la convention. Ex: "2005" pour la convention "2005-0123".
	 */
	public void setExerciceCreation(final Number exerciceCreation) {
		this.qualifierExerciceCreation = createQualifier(
				"exeOrdre = %@",
				exerciceCreation);
		
		this.exerciceCreation = exerciceCreation;
	}
	/**
	 * Change le critere
	 * @param contratIndex Numero d'index de la convention. Ex: "123" pour la convention "2005-0123".
	 */
	public void setOperationIndex(final Number contratIndex) {
		this.qualifierContratIndex = createQualifier(
				"conIndex = %@",
				contratIndex);
		
		this.contratIndex = contratIndex;
	}
	/**
	 * Change le critere
	 * @param contratObjet Une partie de l'objet de la convention. Peut importe les maj/minuscules.
	 * @param wholeWord Mettre a true pour chercher la chaine de caractere specifiee exactement
	 */
	public void setOperationObjet(final String contratObjet, final boolean wholeWord) {
		this.qualifierContratObjet = createQualifier(
				"conObjet = %@",
				wholeWord ? contratObjet : StringOperation.makePatternForSearching(contratObjet, true));
		
		this.contratObjet = contratObjet;
	}
	/**
	 * Change le critere
	 * @param contratReferenceExterne Une partie de l'objet de la convention. Peut importe les maj/minuscules.
	 * @param wholeWord Mettre a true pour chercher la chaine de caractere specifiee exactement
	 */
	public void setOperationReferenceExterne(final String contratReferenceExterne, final boolean wholeWord) {
		this.qualifierContratReferenceExterne = createQualifier(
				"conReferenceExterne = %@",
				wholeWord ? contratReferenceExterne : StringOperation.makePatternForSearching(contratReferenceExterne, true));
		
		this.contratReferenceExterne = contratReferenceExterne;
	}
	/**
	 * Change le critere
	 * @param typeContrat Type de la convention. 
	 * Ex: new TypeContratFinder(ec).findWithTyconIdInterne(TypeContrat.TYCON_ID_INTERNE_CONVENTION_FORMATION)
	 */
	public void setTypeContrat(final TypeContrat typeContrat) {
		this.qualifierTypeContrat = createQualifier(
				"typeContrat = %@",
				typeContrat);
		
		this.typeContrat = typeContrat;
	}
	
	/**
	 * Change le critere
	 * @param typeClassification Type de classification de la convention. 
	 */
	public void setTypeClassificationContrat(final TypeClassificationContrat typeClassification) {
		this.qualifierTypeClassificationContrat = createQualifier(
				"typeClassificationContrat = %@",
				typeClassification);
		
		this.typeClassificationContrat = typeClassification;
	}
	
	/**
	 * Change le critere
	 * @param projet Projet/Groupe auquel appartient la convention. 
	 */
	public void setProjet(final Projet projet) {
		this.qualifierProjet = createQualifier(
				"projetContrat.projet = %@",
				projet);
		
		this.projet = projet;
	}

	/**
	 * <p>Change le critere de recherche courant.
	 * <p>Positionner ce critere "desactive" tous les criteres optionnels car il est suffisant pour qualifier un individu.
	 * En remettant ce critere a <code>null</code>, les criteres optionnels desactives sont reactives.
	 * @param persId Identifiant de la personne physique correspondant a l'individu cherche.
	 */
	protected void setConOrdre(final Number conOrdre) {
		this.qualifierConOrdre = createQualifier(
				"conOrdre = %@", 
				conOrdre);
		
		this.conOrdre = conOrdre;
	}
	
	/**
	 * Supprime tous les criteres de recherche courant.
	 */
	public void clearAllCriteria() {
		this.setExerciceCreation(null);
		this.setOperationIndex(null);
		this.setOperationObjet(null, false);
		this.setOperationReferenceExterne(null, false);
		this.setTypeContrat(null);
		this.setTypeClassificationContrat(null);
	}
	
	
	/**
	 * Recherche des conventions.
	 * @return Liste des conventions trouvees.
	 * @throws ExceptionFinder
	 */
	public NSArray find() throws ExceptionFinder {
		
		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();
		
		// nouveaux criteres 
		this.addOptionalQualifier(this.qualifierExerciceCreation);
		this.addOptionalQualifier(this.qualifierContratIndex);
		this.addOptionalQualifier(this.qualifierContratObjet);
		this.addOptionalQualifier(this.qualifierContratReferenceExterne);
		this.addOptionalQualifier(this.qualifierTypeContrat);
		this.addOptionalQualifier(this.qualifierTypeClassificationContrat);
		
		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder(
					"Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher une convention.");
		
		// fetch
		return super.find();
	}
	/**
	 * Lance la recherche de convention en utilisant le seul critere du CON_ORDRE.
	 * @return Liste des conventions trouvees.
	 * @throws ExceptionFinder 
	 */
	public Operation findWithConOrdre(final Number conOrdre) throws ExceptionFinder {

		if (conOrdre == null)
			throw new NullPointerException("L'identifiant CON_ORDRE est requis.");
			
		this.removeOptionalQualifiers();
		this.setConOrdre(conOrdre);
		this.addOptionalQualifier(this.qualifierConOrdre);
		
		return (Operation) super.find().lastObject();
	}
	
	/**
	 * Recherche les conventions visibles pour un utilisateur 
	 * @return Les contrat trouves. 
	 * @throws ExceptionFinder 
	 */
	public NSArray findForUser(final EOUtilisateur user) throws ExceptionFinder {
		
		try{

			NSArray allConventions=findAll();
			NSMutableArray result=new NSMutableArray();
			for (int i = 0; i < allConventions.count(); i++) {
				
				Operation contratCourant = (Operation) allConventions.objectAtIndex(i);
				
				if (contratCourant.isConsultablePar(user)) 
					result.addObject(contratCourant);
				
			}
			return new NSArray(result);
		}
		catch(Exception e) {
			e.printStackTrace();
			throw new ExceptionFinder(e.getMessage(),e);
		}
	}
	
	/**
	 * Recherche a partir d'un {@link EOGenericRecord}. 
	 * @return Le contrat trouve. 
	 * @throws ExceptionFinder 
	 */
	public Operation findFromGenericRecord(final EOGenericRecord contrat) throws ExceptionFinder {
		
		if (contrat == null)
			throw new NullPointerException("L'objet metier representant le contrat est requis.");
		
		Number id;
		try {
			id = (Number) new ModelUtilities().primaryKeyForObject(contrat);
		} 
		catch (Exception e) {
			e.printStackTrace();
			throw new ExceptionFinder(e.getMessage(), e);
		} 
		
		return findWithConOrdre(id);
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Recherche des conventions autorisees pour l'utilisateur du binding via un fetch SQL (rawRowSQL).<BR>
	 * Bindings pris en compte : utlOrdre (obligatoire), codeClassification (obligatoire), exeOrdre, index, objet, etabOrdre, conEtab, conCr, conNature, refExterne, orgId, modeGest, discOrdre, dateDeb, dateFin
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param bindings
	 *        dictionnaire contenant les bindings
	 * @return
	 *        un NSArray contenant des NSDictionary
	 *        {
	 *        	EXEORDRE = EXE_ORDRE; CONINDEX = CON_INDEX; NUMERO = CON_NUMERO; OBJET = LL_OPERATION;CONCR = CON_CR
	 *        }
	 * @throws Exception 
	 */
    public static final NSArray getRawRowConventionForUser(EOEditingContext ed, NSDictionary bindings) {
    	return EOUtilities.rawRowsForSQL(ed, "GFCOperations", constructionChaineConventionForUser(ed, false, bindings), null);
    }
    
    protected static String constructionChaineConventionForUser(EOEditingContext ed, boolean isForCount, NSDictionary bindings) {
    	
    	if (bindings == null)
    		throw new IllegalArgumentException("le binding 'utlOrdre' est obligatoire");
    	if (bindings.objectForKey("utlOrdre")==null)
    		throw new IllegalArgumentException("le binding 'utlOrdre' est obligatoire");
    	if (bindings.objectForKey("codeClassification")==null)
    		throw new IllegalArgumentException("le binding 'codeClassification' est obligatoire");
    	
    	Integer utlOrdre=(Integer)bindings.objectForKey("utlOrdre");
    	
    	String codeClassification=(String)bindings.objectForKey("codeClassification");
    	String sqlString = "";
    	
    	String sqlStringDroitUser;
    	
    	Boolean tous = (Boolean) bindings.objectForKey("tous");
    	Boolean admin = (Boolean) bindings.objectForKey("admin");
    	
    	if(tous == false && admin == false) {
	    	//Recuperation des conventions auxquelles l'utilisateur à droit de consultation 
	    	String selectClauseCreat = "select c.con_ordre as conOrdre";
	    	String fromClauseCreat = "from accords.contrat c";
	    	String whereClauseCreat = "where c.UTL_ORDRE_CREATION = "+utlOrdre;
	    	
	    	String selectClauseAdmin = "select c.con_ordre as conOrdre";
	    	String fromClauseAdmin = "from accords.contrat c, grhum.secretariat sec, jefy_admin.utilisateur u";
	    	String whereClauseAdmin = "where u.UTL_ORDRE="+utlOrdre+" and sec.C_STRUCTURE=c.CON_CR and sec.NO_INDIVIDU=u.NO_INDIVIDU";
	    	
	    	String selectClauseOrgan = "select c.con_ordre as conOrdre";
	    	String fromClauseOrgan = "from accords.contrat c, jefy_admin.utilisateur_organ uo";
	    	String whereClauseOrgan = "where uo.UTL_ORDRE="+utlOrdre+" and " +
	    	                                "uo.ORG_ID in (select tb.org_id from accords.tranche_budget tb, accords.tranche t where tb.tra_ordre = t.tra_ordre and t.con_ordre = c.con_ordre)";
	    	
	    	String selectClausePart = "select c.con_ordre as conOrdre";
	    	String fromClausePart = "from accords.contrat c, accords.CONTRAT_PARTENAIRE cp, jefy_admin.utilisateur u";
	    	String whereClausePart = "where u.UTL_ORDRE="+utlOrdre+" and cp.con_ordre=c.con_ordre and cp.pers_id=u.pers_id";
	    	
	    	String selectClauseContact = "select c.con_ordre as conOrdre";
	    	String fromClauseContact = "from accords.contrat c, accords.CONTRAT_PARTENAIRE cp,accords.CONTRAT_PART_CONTACT cpc, jefy_admin.utilisateur u";
	    	String whereClauseContact = "where u.UTL_ORDRE="+utlOrdre+" and cp.con_ordre=c.con_ordre and cpc.cp_ordre=cp.cp_ordre and cpc.pers_id_contact=u.pers_id";
	    	
	    	sqlStringDroitUser=selectClauseCreat+" "+fromClauseCreat+" "+whereClauseCreat+" union "+ selectClauseAdmin+" "+fromClauseAdmin+" "+whereClauseAdmin + " union "+ selectClauseOrgan+" "+fromClauseOrgan+" "+whereClauseOrgan+" union "+ selectClausePart+" "+fromClausePart+" "+whereClausePart+" union "+ selectClauseContact+" "+fromClauseContact+" "+whereClauseContact;
    	} else {
    		// On peut voir toutes les conventions
	    	String selectClauseAdmin = "select c.con_ordre as conOrdre";
	    	String fromClauseAdmin = "from accords.contrat c";

    		sqlStringDroitUser = selectClauseAdmin + " " + fromClauseAdmin;
    	}
    	
    	//Generation requete finale
    	String sqlSelectString= "select distinct c.con_ordre as conOrdre, c.exe_ordre as exeordre, c.con_index as conindex, " + //'"+ codeClassification +"' || ' - ' || " +
    			"c.EXE_ORDRE || ' - ' || TO_CHAR(c.CON_INDEX, '0999') as numero, c.LL_OPERATION as objet, s.ll_structure as concr, c.CON_DATE_VALID_ADM as datevalid";
    	
    	
    	String sqlFromString = " from ("+sqlStringDroitUser+") requete, accords.contrat c, grhum.STRUCTURE_ULR s, accords.TYPE_CLASSIFICATION_CONTRAT tcc";
		
    	
    	String sqlWhereString= " where c.con_ordre=requete.conOrdre and c.con_suppr='N' and c.con_cr=s.c_structure(+) and c.tcc_id=tcc.tcc_id and tcc.tcc_code ='"+codeClassification+"'";
    	
    	
    	String sqlOrderString= " order by numero desc";
    	
    	
    	// Filtres supplementaires
    	Number exeOrdre; 
    	BigDecimal index;
    	String objet;
    	String conEtab;
    	String conCr;
    	Integer conNature;
    	String refExterne;
    	Integer orgId;
    	
    	Integer modeGest;
    	Integer discOrdre;
    	
    	NSTimestamp dateDeb;
    	NSTimestamp dateFin;
    	NSTimestamp dateDebutMin;
    	NSTimestamp dateDebutMax;
    	NSTimestamp dateFinMin;
    	NSTimestamp dateFinMax;
    	NSTimestamp dateDebutExecMin;
    	NSTimestamp dateDebutExecMax;
    	NSTimestamp dateFinExecMin;
    	NSTimestamp dateFinExecMax;
    	NSTimestamp dateSignatureMin;
    	NSTimestamp dateSignatureMax;
    	
    	if (bindings.objectForKey("exeOrdre")!=null){
    		exeOrdre = (Number)bindings.objectForKey("exeOrdre");
    		sqlWhereString+=" and c.exe_ordre="+exeOrdre;
    	}
		
    	if (bindings.objectForKey("index")!=null){
    		index = (BigDecimal)bindings.objectForKey("index");
    		sqlWhereString+=" and c.con_index="+index;
    	}
    	
    	if (bindings.objectForKey("objet")!=null){
    		objet = (String)bindings.objectForKey("objet");
    		sqlWhereString+=" and upper(c.LL_OPERATION) like upper('%"+objet+"%')";
    	}
    	
    	if (bindings.objectForKey("conEtab")!=null){
    		conEtab = (String)bindings.objectForKey("conEtab");
    		sqlWhereString+=" and c.CON_ETABLISSEMENT = '"+conEtab+"'";
    	}
    	
    	if (bindings.objectForKey("conCr")!=null){
    		conCr = (String)bindings.objectForKey("conCr");
    		sqlWhereString+=" and c.CON_CR = '"+conCr+"'";
    	}
    	
    	if (bindings.objectForKey("conNature")!=null){
    		conNature = (Integer)bindings.objectForKey("conNature");
    		sqlWhereString+=" and c.con_nature="+conNature;
    	}
    	
    	if (bindings.objectForKey("refExterne")!=null){
    		refExterne = (String)bindings.objectForKey("refExterne");
    		sqlWhereString+=" and upper(c.CON_REFERENCE_EXTERNE) like upper('%"+refExterne+"%')";
    	}
    	
    	if (bindings.objectForKey("orgId")!=null){
    		orgId = (Integer)bindings.objectForKey("orgId");
    		sqlWhereString+=" and c.ORG_ID_COMPOSANTE="+orgId;
    	}
    	
    	modeGest=(Integer)bindings.objectForKey("modeGest");
    	discOrdre=(Integer)bindings.objectForKey("discOrdre");
    	dateDeb=(NSTimestamp)bindings.objectForKey("dateDeb");
    	dateFin=(NSTimestamp)bindings.objectForKey("dateFin");
    	
    	dateDebutMin=(NSTimestamp)bindings.objectForKey("DateDebutMin");
    	dateDebutMax=(NSTimestamp)bindings.objectForKey("DateDebutMax");
    	dateFinMin=(NSTimestamp)bindings.objectForKey("DateFinMin");
    	dateFinMax=(NSTimestamp)bindings.objectForKey("DateFinMax");
    	
    	dateDebutExecMin=(NSTimestamp)bindings.objectForKey("DateDebutExecMin");
    	dateDebutExecMax=(NSTimestamp)bindings.objectForKey("DateDebutExecMax");
    	dateFinExecMin=(NSTimestamp)bindings.objectForKey("DateFinExecMin");
    	dateFinExecMax=(NSTimestamp)bindings.objectForKey("DateFinExecMax");
    	
    	dateSignatureMin=(NSTimestamp)bindings.objectForKey("DateSignatureMin");
    	dateSignatureMax=(NSTimestamp)bindings.objectForKey("DateSignatureMax");
    	
    	
    	if(modeGest!=null || discOrdre!=null || dateDeb!=null  || dateFin!=null || 
    			dateDebutMin!=null  || dateDebutMax!=null || 
    			dateFinMin!=null  || dateFinMax!=null || 
    			dateDebutExecMin!=null  || dateDebutExecMax!=null || 
    			dateFinExecMin!=null  || dateFinExecMax!=null || 
    			dateSignatureMin!=null  || dateSignatureMax!=null ) {
    		sqlFromString += ", accords.avenant a ";
    		sqlWhereString += " and a.con_ordre=c.con_ordre and a.avt_suppr='N'";
    	}
    	
    	if(modeGest!=null)
    		sqlWhereString+=" and a.AVT_MODE_GEST="+modeGest;
    	
    	if(discOrdre!=null)
    		sqlWhereString+=" and a.DISC_ORDRE="+discOrdre;
    	
      	if (dateDebutMin != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_DEB,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('"+DateCtrl.dateToString(dateDebutMin)+"','dd/mm/yyyy')";
      	if (dateDebutMax != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_DEB,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('"+DateCtrl.dateToString(dateDebutMax)+"','dd/mm/yyyy')";
      	if (dateFinMin != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_FIN,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('"+DateCtrl.dateToString(dateFinMin)+"','dd/mm/yyyy')";
      	if (dateFinMax != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_FIN,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('"+DateCtrl.dateToString(dateFinMax)+"','dd/mm/yyyy')";

      	if (dateDebutExecMin != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_DEB_EXEC,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('"+DateCtrl.dateToString(dateDebutExecMin)+"','dd/mm/yyyy')";
      	if (dateDebutExecMax != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_DEB_EXEC,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('"+DateCtrl.dateToString(dateDebutExecMax)+"','dd/mm/yyyy')";
      	if (dateFinExecMin != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_FIN_EXEC,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('"+DateCtrl.dateToString(dateFinExecMin)+"','dd/mm/yyyy')";
      	if (dateFinExecMax != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_FIN_EXEC,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('"+DateCtrl.dateToString(dateFinExecMax)+"','dd/mm/yyyy')";

      	if (dateSignatureMin != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_SIGNATURE,'dd/mm/yyyy'),'dd/mm/yyyy')>=to_date('"+DateCtrl.dateToString(dateSignatureMin)+"','dd/mm/yyyy')";
      	if (dateSignatureMax != null)
      		sqlWhereString += " AND to_date(to_char(a.AVT_DATE_SIGNATURE,'dd/mm/yyyy'),'dd/mm/yyyy')<=to_date('"+DateCtrl.dateToString(dateSignatureMax)+"','dd/mm/yyyy')";

      	if(dateDeb!=null) {
    		GregorianCalendar myDate =  new GregorianCalendar();
    		myDate.setTime(dateDeb);
    		int year = myDate.get(GregorianCalendar.YEAR);
    		int month = myDate.get(GregorianCalendar.MONTH);
    		int day = myDate.get(GregorianCalendar.DAY_OF_MONTH);
    		
    		String dayString="";
    		if(day<10)
    			dayString+="0";
    		dayString+=day;
    		
    		String monthString="";
    		if(month<10)
    			monthString+="0";
    		monthString+=month;
    		
    		String dateString = dayString+"/"+monthString+"/"+year;
    		
    		sqlWhereString+=" and (a.AVT_DATE_FIN is null or a.AVT_DATE_FIN > TO_DATE('"+dateString+"','dd/mm/YYYY'))";
    	}
    	
    	if(dateFin!=null) {
    		GregorianCalendar myDate =  new GregorianCalendar();
    		myDate.setTime(dateFin);
    		int year = myDate.get(GregorianCalendar.YEAR);
    		int month = myDate.get(GregorianCalendar.MONTH);
    		int day = myDate.get(GregorianCalendar.DAY_OF_MONTH);
    		
    		String dayString="";
    		if(day<10)
    			dayString+="0";
    		dayString+=day;
    		
    		String monthString="";
    		if(month<10)
    			monthString+="0";
    		monthString+=month;
    		
    		String dateString = dayString+"/"+monthString+"/"+year;
    		
    		sqlWhereString+=" and a.AVT_DATE_DEB < TO_DATE('"+dateString+"','dd/mm/YYYY')";
    	}
    	
    	sqlString=sqlSelectString+sqlFromString+sqlWhereString+sqlOrderString;
    	
    	System.out.println("**************************** Requete generee ****************************");
    	System.out.println(sqlString);
    	System.out.println("*************************************************************************");
		
    	return sqlString;
			
    }
    
}
