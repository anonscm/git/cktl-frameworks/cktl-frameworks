package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche;

import java.math.BigDecimal;

import lombok.Getter;

import org.cocktail.fwkcktlgfceos.server.metier.EOVCptbudOpeSuiviGlobal;
import org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class TranchePourSuiviGlobal {

    private final EOVCptbudOpeSuiviGlobal infosSuivi;
    @Getter
    private final Tranche tranche;

    public TranchePourSuiviGlobal(Operation operation, Tranche tranche) {
        this.tranche = tranche;

        if (tranche.exercice() != null) {
            EOQualifier qual_idOpe = EOVCptbudOpeSuiviGlobal.ID_OPE_OPERATION.eq(operation.idOpeOperation());
            EOQualifier qual_exer = EOVCptbudOpeSuiviGlobal.TO_EXERCICE.eq(tranche.exercice());

            EOQualifier qualifier = ERXQ.and(qual_idOpe, qual_exer);
            infosSuivi = EOVCptbudOpeSuiviGlobal.fetchByQualifier(operation.editingContext(), qualifier);
        } else {
            infosSuivi = null;
        }

    }
    
    public boolean hasExercice() {
        return tranche.exercice() != null;
    }

    public boolean isExerciceClos() {
        // exercice clos = clos ou restreint
        return hasExercice() && (tranche.exercice().estClos() || tranche.exercice().estRestreint());
    }
    
    public boolean isTreso() {
        return tranche.isTreso();
    }

    public Integer exeExercice() {
        return tranche.exerciceCocktail().exeExercice();
    }

    public BigDecimal totalPositionneRec() {
        return tranche.totalPositionneRec();
    }

    public BigDecimal totalPositionneDepAe() {
        return tranche.totalPositionne();
    }

    public BigDecimal totalPositionneDepCp() {
        return tranche.totalPositionneDepCp();
    }

    public BigDecimal recRealisees() {
        if (infosSuivi == null) {
            return BigDecimal.ZERO;
        }
        if (infosSuivi.recDebit() == null) {
            return BigDecimal.ZERO;
        }
        return infosSuivi.recDebit();
    }

    public BigDecimal recAIntegrer() {
        BigDecimal recCredit = BigDecimal.ZERO;
        if ((infosSuivi != null) && (infosSuivi.recCredit() != null)) {
            recCredit = infosSuivi.recCredit();
        }
        return totalPositionneRec().subtract(recCredit);
    }

    public BigDecimal aeConsommees() {
        if (infosSuivi == null) {
            return BigDecimal.ZERO;
        }
        if (infosSuivi.aeDebit() == null) {
            return BigDecimal.ZERO;
        }
        return infosSuivi.aeDebit();
    }

    public BigDecimal aeAIntegrer() {
        BigDecimal aeCredit = BigDecimal.ZERO;
        if ((infosSuivi != null) && (infosSuivi.aeCredit() != null)) {
            aeCredit = infosSuivi.aeCredit();
        }
        return totalPositionneDepAe().subtract(aeCredit);
    }

    public BigDecimal cpConsommes() {
        if (infosSuivi == null) {
            return BigDecimal.ZERO;
        }
        if (infosSuivi.cpDebit() == null) {
            return BigDecimal.ZERO;
        }
        return infosSuivi.cpDebit();
    }

    public BigDecimal cpAIntegrer() {
        BigDecimal cpCredit = BigDecimal.ZERO;
        if ((infosSuivi != null) && (infosSuivi.cpCredit() != null)) {
            cpCredit = infosSuivi.cpCredit();
        }
        return totalPositionneDepCp().subtract(cpCredit);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (!obj.getClass().equals(this.getClass())) {
            return false;
        }
        TranchePourSuiviGlobal tpsg = (TranchePourSuiviGlobal) obj;
        return tranche.equals(tpsg.getTranche());
    }

    @Override
    public int hashCode() {
        return tranche.hashCode();
    }

}
