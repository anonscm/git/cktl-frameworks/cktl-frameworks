// DO NOT EDIT.  Make changes to AvenantEvenement.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAvenantEvenement extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeAvenantEvenement";
	public static final String ENTITY_TABLE_NAME = "GFC.AVENANT_EVT_EVENEMENT";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> AVENANT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>("avenant");
  public static final ERXKey<org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement> EVENEMENT = new ERXKey<org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement>("evenement");

  // Attributes
  // Relationships
  public static final String AVENANT_KEY = AVENANT.key();
  public static final String EVENEMENT_KEY = EVENEMENT.key();

  private static Logger LOG = Logger.getLogger(EOAvenantEvenement.class);

  public AvenantEvenement localInstanceIn(EOEditingContext editingContext) {
    AvenantEvenement localInstance = (AvenantEvenement)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant)storedValueForKey(EOAvenantEvenement.AVENANT_KEY);
  }
  
  public void setAvenant(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    takeStoredValueForKey(value, EOAvenantEvenement.AVENANT_KEY);
  }

  public void setAvenantRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    if (EOAvenantEvenement.LOG.isDebugEnabled()) {
      EOAvenantEvenement.LOG.debug("updating avenant from " + avenant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAvenant(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant oldValue = avenant();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantEvenement.AVENANT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantEvenement.AVENANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement() {
    return (org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement)storedValueForKey(EOAvenantEvenement.EVENEMENT_KEY);
  }
  
  public void setEvenement(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
    takeStoredValueForKey(value, EOAvenantEvenement.EVENEMENT_KEY);
  }

  public void setEvenementRelationship(org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement value) {
    if (EOAvenantEvenement.LOG.isDebugEnabled()) {
      EOAvenantEvenement.LOG.debug("updating evenement from " + evenement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEvenement(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement oldValue = evenement();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantEvenement.EVENEMENT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantEvenement.EVENEMENT_KEY);
    }
  }
  

  public static AvenantEvenement create(EOEditingContext editingContext, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant, org.cocktail.fwkcktlevenement.serveur.modele.EOEvenement evenement) {
    AvenantEvenement eo = (AvenantEvenement) EOUtilities.createAndInsertInstance(editingContext, EOAvenantEvenement.ENTITY_NAME);    
    eo.setAvenantRelationship(avenant);
    eo.setEvenementRelationship(evenement);
    return eo;
  }

  public static ERXFetchSpecification<AvenantEvenement> fetchSpec() {
    return new ERXFetchSpecification<AvenantEvenement>(EOAvenantEvenement.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AvenantEvenement> fetchAll(EOEditingContext editingContext) {
    return EOAvenantEvenement.fetchAll(editingContext, null);
  }

  public static NSArray<AvenantEvenement> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAvenantEvenement.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<AvenantEvenement> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AvenantEvenement> fetchSpec = new ERXFetchSpecification<AvenantEvenement>(EOAvenantEvenement.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AvenantEvenement> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AvenantEvenement fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantEvenement.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantEvenement fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AvenantEvenement> eoObjects = EOAvenantEvenement.fetchAll(editingContext, qualifier, null);
    AvenantEvenement eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeAvenantEvenement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantEvenement fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantEvenement.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantEvenement fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    AvenantEvenement eoObject = EOAvenantEvenement.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeAvenantEvenement that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantEvenement localInstanceIn(EOEditingContext editingContext, AvenantEvenement eo) {
    AvenantEvenement localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}