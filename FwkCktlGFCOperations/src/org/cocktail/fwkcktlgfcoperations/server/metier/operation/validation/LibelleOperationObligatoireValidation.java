package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;

import org.apache.commons.lang.StringUtils;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

public class LibelleOperationObligatoireValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;
    
    public boolean isSatisfiedBy(Operation operation) {
        if (operation == null) {
            return false;
        }
               
        String libelle = operation.libelle();
        return StringUtils.isNotBlank(libelle); 
    } 
}