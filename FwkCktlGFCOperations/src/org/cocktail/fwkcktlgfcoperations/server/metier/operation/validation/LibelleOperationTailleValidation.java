package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

public class LibelleOperationTailleValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    public boolean isSatisfiedBy(Operation operation) {
        if (operation == null || operation.libelle() == null) {
            return false;
        }
               
        return operation.libelle().length() <= Operation.LIBELLE_MAX_SIZE; 
    } 
}