package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;
import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponible;

public class ContributionDoitExcederFraisGestionValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;
    
    private CalculMontantParticipationDisponible calculMontantParticipationDisponible;
    
    public ContributionDoitExcederFraisGestionValidation(CalculMontantParticipationDisponible calculMontantParticipationDisponible) {
    	this.calculMontantParticipationDisponible = calculMontantParticipationDisponible;
    }
    
    public boolean isSatisfiedBy(RepartPartenaireTranche partenaireTranche) {
    	BigDecimal montantParticipationDisponible =
    			calculMontantParticipationDisponible.montantParticipationDisponible(partenaireTranche);
        return montantParticipationDisponible.signum() >= 0;
    } 
	
}
