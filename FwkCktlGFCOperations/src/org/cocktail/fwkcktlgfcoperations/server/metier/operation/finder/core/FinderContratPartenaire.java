package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Composant permettant de rechercher des structures.
 * 
 * @author Michael Haller, Consortium Cocktail, 2009
 */
public class FinderContratPartenaire extends Finder {
	protected Operation operation;
	protected IPersonne partenaire;
	protected TypePartenaire typePartenaire;
	protected Boolean partenairePrincipal;

	protected EOQualifier qualifierContrat;
	protected EOQualifier qualifierPartenaire;
	protected EOQualifier qualifierTypePartenaire;
	protected EOQualifier qualifierPartenairePrincipal;

	/**
	 * 
	 * @param ec
	 */
	public FinderContratPartenaire(EOEditingContext ec) {
		super(ec, OperationPartenaire.ENTITY_NAME);

		// this.addMandatoryQualifier(EOQualifier.qualifierWithQualifierFormat("conSuppr = %@",
		// new NSArray("N")));
	}

	/**
	 * Change le critere
	 */
	public void setContrat(final Operation operation) {
		this.qualifierContrat = createQualifier("contrat = %@", operation);

		this.operation = operation;
	}

	/**
	 * Change le critere
	 */
	public void setPartenaire(final IPersonne partenaire) {
		this.qualifierPartenaire = createQualifier("persId = %@", partenaire.persId());

		this.partenaire = partenaire;
	}

	/**
	 * Change le critere
	 */
	public void setPartenairePrincipal(final Boolean partenairePrincipal) {
		if (partenairePrincipal != null) {
			if (partenairePrincipal.booleanValue())
				this.qualifierPartenairePrincipal = OperationPartenaire.QUALIFIER_PARTENAIRE_PRINCIPAL;
			else
				this.qualifierPartenairePrincipal = OperationPartenaire.QUALIFIER_PARTENAIRE_NON_PRINCIPAL;
		}
		this.partenairePrincipal = partenairePrincipal;
	}

	/**
	 * Change le critere
	 */
	public void setTypePartenaire(final TypePartenaire typePartenaire) {
		this.qualifierTypePartenaire = createQualifier("typePartenaire = %@", typePartenaire);

		this.typePartenaire = typePartenaire;
	}

	/**
	 * Supprime tous les criteres de recherche courant.
	 */
	public void clearAllCriteria() {
		this.setContrat((Operation) null);
		this.setPartenaire((IPersonne) null);
		this.setPartenairePrincipal(null);
		this.setTypePartenaire(null);
	}

	/**
	 * Recherche des partenariats.
	 * 
	 * @return Liste des partenariats trouves.
	 * @throws ExceptionFinder
	 */
	public NSArray find() throws ExceptionFinder {

		// raz criteres de recherche visibles
		this.removeOptionalQualifiers();

		// nouveaux criteres
		this.addOptionalQualifier(this.qualifierContrat);
		this.addOptionalQualifier(this.qualifierPartenaire);
		this.addOptionalQualifier(this.qualifierPartenairePrincipal);
		this.addOptionalQualifier(this.qualifierTypePartenaire);

		// au moins un critere necessaire
		if (this.getQualifiersCount() < 1)
			throw new ExceptionFinder("Au moins un crit\u00E8re doit \u00EAtre fourni pour rechercher une convention.");

		// fetch
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
