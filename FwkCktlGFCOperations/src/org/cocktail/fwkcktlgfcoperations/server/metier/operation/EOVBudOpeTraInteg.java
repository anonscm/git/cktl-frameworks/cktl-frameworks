// DO NOT EDIT.  Make changes to VBudOpeTraInteg.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVBudOpeTraInteg extends  CktlServerRecord {
  public static final String ENTITY_NAME = "VBudOpeTraInteg";
	public static final String ENTITY_TABLE_NAME = "GFC.V_BUD_OPE_TRA_INTEG";

  // Attribute Keys
  public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String ID_OPE_OPERATION_KEY = ID_OPE_OPERATION.key();
  // Relationships
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOVBudOpeTraInteg.class);

  public VBudOpeTraInteg localInstanceIn(EOEditingContext editingContext) {
    VBudOpeTraInteg localInstance = (VBudOpeTraInteg)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(EOVBudOpeTraInteg.ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    if (EOVBudOpeTraInteg.LOG.isDebugEnabled()) {
        EOVBudOpeTraInteg.LOG.debug( "updating idOpeOperation from " + idOpeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOVBudOpeTraInteg.ID_OPE_OPERATION_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOVBudOpeTraInteg.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOVBudOpeTraInteg.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOVBudOpeTraInteg.LOG.isDebugEnabled()) {
      EOVBudOpeTraInteg.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVBudOpeTraInteg.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVBudOpeTraInteg.TRANCHE_KEY);
    }
  }
  

  public static VBudOpeTraInteg create(EOEditingContext editingContext, Integer idOpeOperation
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    VBudOpeTraInteg eo = (VBudOpeTraInteg) EOUtilities.createAndInsertInstance(editingContext, EOVBudOpeTraInteg.ENTITY_NAME);    
        eo.setIdOpeOperation(idOpeOperation);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VBudOpeTraInteg> fetchSpec() {
    return new ERXFetchSpecification<VBudOpeTraInteg>(EOVBudOpeTraInteg.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VBudOpeTraInteg> fetchAll(EOEditingContext editingContext) {
    return EOVBudOpeTraInteg.fetchAll(editingContext, null);
  }

  public static NSArray<VBudOpeTraInteg> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVBudOpeTraInteg.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VBudOpeTraInteg> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VBudOpeTraInteg> fetchSpec = new ERXFetchSpecification<VBudOpeTraInteg>(EOVBudOpeTraInteg.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VBudOpeTraInteg> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VBudOpeTraInteg fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVBudOpeTraInteg.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VBudOpeTraInteg fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VBudOpeTraInteg> eoObjects = EOVBudOpeTraInteg.fetchAll(editingContext, qualifier, null);
    VBudOpeTraInteg eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one VBudOpeTraInteg that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VBudOpeTraInteg fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVBudOpeTraInteg.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VBudOpeTraInteg fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VBudOpeTraInteg eoObject = EOVBudOpeTraInteg.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no VBudOpeTraInteg that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VBudOpeTraInteg localInstanceIn(EOEditingContext editingContext, VBudOpeTraInteg eo) {
    VBudOpeTraInteg localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}