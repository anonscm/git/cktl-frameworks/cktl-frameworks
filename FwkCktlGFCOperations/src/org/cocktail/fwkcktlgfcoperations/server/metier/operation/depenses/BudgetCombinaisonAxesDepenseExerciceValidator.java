package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense;
import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepenseExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDepExercice;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche.EntiteBudgetaireExerciceValidator;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEOControlUtilities;

public class BudgetCombinaisonAxesDepenseExerciceValidator {

	private EOEditingContext edc;
	private EOExercice exercice;
	private EntiteBudgetaireExerciceValidator ebValidator;
	private BudgetCombinaisonAxesDepenseNotificationHandler handler;
	
	public BudgetCombinaisonAxesDepenseExerciceValidator(EOEditingContext edc,
			EOExercice exercice, BudgetCombinaisonAxesDepenseNotificationHandler handler) {
		this.edc = edc;
		this.exercice = exercice;
		this.handler = handler;
		this.ebValidator = new EntiteBudgetaireExerciceValidator(edc, exercice);
	}
	
	public boolean isValid(BudgetCombinaisonAxesDepense axes) {
		boolean isEbValideSurExercice = ebValidator.isValid(axes.getEntiteBudgetaire());
		if (!isEbValideSurExercice) {
			handler().marquerEbEnErreur();
		}
		
		boolean isDestinationValideSurExercice = isDestinationValideSurExercice(axes.getDestination());
		if (!isDestinationValideSurExercice) {
			handler().marquerDestinationEnErreur();
		}
		
		boolean isNatureValideSurExercice = isNatureValideSurExercice(axes.getNature());
		if (!isNatureValideSurExercice) {
			handler().marquerNatureEnErreur();
		}
		
		boolean isEnveloppeValide = isEnveloppeValide(axes.getEnveloppe());
		
		return isEbValideSurExercice && isDestinationValideSurExercice && isNatureValideSurExercice && isEnveloppeValide;
	}
	
	private boolean isDestinationValideSurExercice(EODestinationDepense destination) {
		boolean isValide = destination.estDansUnEtatValide();
		if (!isValide) {
			return false;
		}
		
		EOQualifier qualExisteSurExercice = EODestinationDepenseExercice.DESTINATION_DEPENSE.eq(destination)
				.and(EODestinationDepenseExercice.EXERCICE.eq(exercice));
		int count = ERXEOControlUtilities.objectCountWithQualifier(edc, EODestinationDepenseExercice.ENTITY_NAME, qualExisteSurExercice);
		return count > 0;
	}
	
	private boolean isNatureValideSurExercice(EONatureDep nature) {
		EOQualifier qualExisteSurExercice = EONatureDepExercice.ADM_NATURE_DEP.eq(nature)
				.and(EONatureDepExercice.EXE_ORDRE.eq(exercice.exeExercice().intValue()));
		int count = ERXEOControlUtilities.objectCountWithQualifier(edc, EONatureDepExercice.ENTITY_NAME, qualExisteSurExercice);
		return count > 0;
	}
	
	private boolean isEnveloppeValide(EOEnveloppeBudgetaire enveloppe) {
		return enveloppe != null;
	}
	
	private BudgetCombinaisonAxesDepenseNotificationHandler handler() {
		return handler;
	}
}
