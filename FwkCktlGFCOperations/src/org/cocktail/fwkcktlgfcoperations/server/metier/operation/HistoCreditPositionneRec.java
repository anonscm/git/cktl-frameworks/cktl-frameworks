/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.validation.ERXValidationFactory;

@Deprecated
public class HistoCreditPositionneRec extends EOHistoCreditPositionneRec {

	public HistoCreditPositionneRec() {
		super();
	}

	public static HistoCreditPositionneRec creerHistoCreditPositionne(EOEditingContext editingContext,
			Tranche tranche,
			EOPlanComptableExer planco,
			EOTypeCredit typeCredit,
			EOEb organ,
			BigDecimal montant
			) {
		HistoCreditPositionneRec historique = HistoCreditPositionneRec.create(editingContext, null, null, null);
		historique.setHcprDate(new NSTimestamp());
		historique.setTrancheRelationship(tranche);
		historique.setTypeCreditRelationship(typeCredit);
		historique.setExerciceRelationship(tranche.exerciceCocktail().exercice());
		historique.setPlancoRelationship(planco);
		historique.setOrganRelationship(organ);
		historique.setHcprMontant(montant);
		return historique;
	}

	public static void checkPreCreation(VRecettesTranche recettes, BigDecimal montant) {
		if (recettes != null && montant != null && recettes.tranche().operation().isModeBudgetAvance()) {
			if (recettes.montantResteAPositionne().compareTo(montant) < 0) {
				throw ERXValidationFactory.defaultFactory().createCustomException(recettes, "MontantCreditDepasseRecettes");
			}
		}
	}

	private void fixHcprSuppr() {
		if (hcprSuppr() == null && hcprMontant() != null && hcprMontant().signum() > 0) {
			setHcprSuppr("N");
		}
	}

	@Override
	public void validateForInsert() throws ValidationException {
		super.validateForInsert();
		fixHcprSuppr();
	}

	@Override
	public void validateForUpdate() throws ValidationException {
		super.validateForUpdate();
		fixHcprSuppr();
	}

	@Override
	public void validateForSave() throws ValidationException {
		super.validateForSave();
		if (tranche().operation().isModeBudgetAvance() && planco() == null)
			throw ERXValidationFactory.defaultFactory().createCustomException(this, PLANCO_KEY, null, "PlancoMissing");
	}

}
