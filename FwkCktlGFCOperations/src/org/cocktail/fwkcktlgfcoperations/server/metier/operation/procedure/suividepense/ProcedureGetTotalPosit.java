package org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suividepense;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Procedure qui calcule le total des credits positionnes en depense dans Coconut
 * pour une convention, un exercice, un type de credit et une ligne de l'organigramme 
 * budgetaire.
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 */
public class ProcedureGetTotalPosit extends ProcedureSuiviDepense
{
	protected final static String PROCEDURE_NAME = "GetTotalPosit";
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context a utiliser.
	 */
	public ProcedureGetTotalPosit(final EOEditingContext ec) { 
		super(ec, PROCEDURE_NAME); 
	}
	
}
