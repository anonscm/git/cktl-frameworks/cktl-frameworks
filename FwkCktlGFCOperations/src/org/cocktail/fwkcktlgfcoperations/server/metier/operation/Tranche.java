package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseAe;
import org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.exception.IncorrectResultSizeDataAccessException;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses.BudgetCombinaisonAxesDepense;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory.FactoryTranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suividepense.ProcedureGetTotalDepense;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suividepense.ProcedureGetTotalLiquide;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSForwardException;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;
import er.extensions.eof.ERXQ;
import er.extensions.localization.ERXLocalizer;
import er.extensions.validation.ERXValidationFactory;

public class Tranche extends EOTranche {
	
    public static final ERXKey<Integer> CON_ORDRE = new ERXKey<Integer>("conOrdre");
    public static final ERXKey<String> TRA_MONTANT = new ERXKey<String>("montant");
    public static final String TRA_MONTANT_KEY = TRA_MONTANT.key();

    public static final String TRA_SUPPR_OUI = "O";
    public static final String TRA_SUPPR_NON = "N";

    public static final EOQualifier QUALIFIER_NON_SUPPR = EOQualifier.qualifierWithQualifierFormat("traSuppr='N'", null);

    public static final EOSortOrdering SORT_EXERCICE_ASC = EOSortOrdering.sortOrderingWithKey("exerciceCocktail.exeExercice",
            EOSortOrdering.CompareAscending);

    public static final ERXKey<BigDecimal> RESTE_A_POSITIONNE = new ERXKey<BigDecimal>("resteAPositionne");

    /**
     * Fournit une instance de cette classe.
     * 
     * @param ec
     *            Editing context a utliser.
     * @return L'instance creee
     * @throws InstantiationException
     *             Probleme d'instanciation.
     */
    public static Tranche instanciate(EOEditingContext ec) throws Exception {
        return (Tranche) Factory.instanceForEntity(ec, ENTITY_NAME);
    }
    
    @Deprecated
    public boolean isNatureDepense() {
    	throw new UnsupportedOperationException();
    }
    
    public BigDecimal montant() {
        BigDecimal traMontant = totalContributionsPlusReportNmoins1().subtract(reportNplus1());
        return traMontant;
    }

    public BigDecimal traMontantFraisInclus() {
        BigDecimal traMontant = totalContributions();
        // On ajoute le report de l'annee precedente et on soustrait celui de
        // l'année suivante
        traMontant = traMontant.add(reportNmoins1()).subtract(reportNplus1());
        return traMontant;
    }

    @Override
    public BigDecimal reportNmoins1() {
        // Verification car si on a 0 dans Oracle ca retourne null au lieu de 0
        BigDecimal reportNmoins1 = super.reportNmoins1();
        if (reportNmoins1 == null) {
            setReportNmoins1(BigDecimal.ZERO);
            return BigDecimal.ZERO;
        }
        return reportNmoins1;
    }

    @Override
    public BigDecimal reportNplus1() {
        // Verification car si on a 0 dans Oracle ca retourne null au lieu de 0
        BigDecimal reportNplus1 = super.reportNplus1();
        if (reportNplus1 == null) {
            setReportNplus1(BigDecimal.ZERO);
            return BigDecimal.ZERO;
        }
        return reportNplus1;
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_DISPONIBLES = new ERXKey<BigDecimal>("totalContributionsDisponibles");

    public BigDecimal totalContributionsDisponibles() {
        BigDecimal montant = valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.MONTANT_PARTICIPATION_DISPONIBLE));
        return montant;
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS = new ERXKey<BigDecimal>("totalContributions");

    public BigDecimal totalContributions() {
        BigDecimal montant = valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.MONTANT_PARTICIPATION));
        return montant;
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_PLUS_REPORTS = new ERXKey<BigDecimal>("totalContributionsPlusReports");

    public BigDecimal totalContributionsPlusReports() {
        BigDecimal montant = totalContributions().add(reportNmoins1());
        return montant;
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTRIBUTIONS_DISPONIBLES_PLUS_REPORT = new ERXKey<BigDecimal>("totalContributionsPlusReportNmoins1");

    public BigDecimal totalContributionsPlusReportNmoins1() {
        BigDecimal montant = totalContributionsDisponibles();
        return montant.add(reportNmoins1());
    }

    public static final ERXKey<BigDecimal> TOTAL_FRAIS_GESTION = new ERXKey<BigDecimal>("totalFraisGestion");

    public BigDecimal totalFraisGestion() {
        return valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.FRAIS_GESTIONS_AS_DECIMAL));
    }

    public static final ERXKey<BigDecimal> TOTAL_COUTS_INDIRECTS = new ERXKey<BigDecimal>("totalCoutsIndirects");

    public BigDecimal totalCoutsIndirects() {
        return valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.COUTS_INDIRECTS_AS_DECIMAL));
    }

    public static final ERXKey<BigDecimal> TOTAL_CHARGES_NON_BUDGETAIRES = new ERXKey<BigDecimal>("totalChargesNonBudgetaires");

    public BigDecimal totalChargesNonBudgetaires() {
        return valueForKey(TO_REPART_PARTENAIRE_TRANCHES.atSum(RepartPartenaireTranche.CHARGES_NON_BUDGETAIRES_AS_DECIMAL));
    }

    public static final ERXKey<BigDecimal> TOTAL_CONTIBUTIONS_TIERS = new ERXKey<BigDecimal>("totalContributionsTiers");

    public BigDecimal totalContributionsTiers() {
        return valueForKey(TO_REPART_PARTENAIRE_TRANCHES_TIERS.atSum(RepartPartenaireTranche.MONTANT_PARTICIPATION));
    }
    
    public BigDecimal totalDepenseAESurCombinaison(BudgetCombinaisonAxesDepense combinaison) {
    	NSArray<TrancheBudgetDepAE> listeAeSurCombinaison = ERXQ.filtered(
    			trancheBudgetsNonSupprimes(), 
    			restrictListeAESurCombinaisonAxesDepense(combinaison));
    	BigDecimal totalDepenseAESurCombinaison = null;
    	if (listeAeSurCombinaison != null && !listeAeSurCombinaison.isEmpty()) {
    	    totalDepenseAESurCombinaison = TrancheBudgetDepAE.TB_MONTANT.atSum().valueInObject(listeAeSurCombinaison);
    	}
    	return totalDepenseAESurCombinaison;
    }
    
    private EOQualifier restrictListeAESurCombinaisonAxesDepense(BudgetCombinaisonAxesDepense combinaison) {
    	EOQualifier qualCombi = TrancheBudgetDepAE.ORGAN.eq(combinaison.getEntiteBudgetaire())
        		.and(TrancheBudgetDepAE.DESTINATION_DEPENSE.eq(combinaison.getDestination()))
        		.and(TrancheBudgetDepAE.NATURE_DEPENSE.eq(combinaison.getNature()))
    			.and(TrancheBudgetDepAE.ENVELOPPE_BUDGETAIRE.eq(combinaison.getEnveloppe()));
    	return qualCombi;
    }
    
    public NSArray<TrancheBudgetDepAE> trancheBudgetsNonSupprimes(BudgetCombinaisonAxesDepense combinaison) {
    	NSArray<TrancheBudgetDepAE> listeAeSurCombinaison = ERXQ.filtered(
    			trancheBudgetsNonSupprimes(), 
    			restrictListeAESurCombinaisonAxesDepense(combinaison));
    	return listeAeSurCombinaison;
    }

    @Deprecated
    public void setTraNatureMontant(String aValue) {
        throw new IllegalAccessError("On ne doit plus utiliser cette méthode...");
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeDepenses() {
        BigDecimal total = new BigDecimal(0);
        for (Object depense : sbDepenses())
            total = total.add(((SbDepense) depense).sdMontantHt());
        return total;
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeRecettes() {
        BigDecimal total = new BigDecimal(0);
        for (Object recette : sbRecettes())
            total = total.add(((SbRecette) recette).srMontantHt());
        return total;
    }

    /**
     * @return
     */
    public BigDecimal balance() {
        return sommeRecettes().subtract(sommeDepenses());
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeDepensesNature() {
        NSArray<SbDepense> depenses = sbDepenses(SbDepense.PLANCO.isNotNull());
        return (BigDecimal) depenses.valueForKey("@sum.sdMontantHt");
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeDepensesDestination() {
        NSArray<SbDepense> depenses = sbDepenses(SbDepense.LOLF.isNotNull());
        return (BigDecimal) depenses.valueForKey("@sum.sdMontantHt");
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeRecettesNature() {
        NSArray<SbRecette> recettes = sbRecettes(SbDepense.PLANCO.isNotNull());
        return (BigDecimal) recettes.valueForKey("@sum.srMontantHt");
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal sommeRecettesDestination() {
        NSArray<SbRecette> recettes = sbRecettes(SbDepense.LOLF.isNotNull());
        return (BigDecimal) recettes.valueForKey("@sum.srMontantHt");
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal balanceRecettesNatureDestination() {
        return sommeRecettesNature().subtract(sommeRecettesDestination());
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal balanceDepensesNatureDestination() {
        return sommeDepensesNature().subtract(sommeDepensesDestination());
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal balanceRecettesDepensesNature() {
        return sommeRecettesNature().subtract(sommeDepensesNature());
    }

    /**
     * @return
     */
    @Deprecated
    public BigDecimal balanceRecettesDepensesDestination() {
        return sommeRecettesDestination().subtract(sommeDepensesDestination());
    }

    /**
     * @return
     */
    public NSArray<VDepensesTranche> sommeDepensesByNature() {
        ERXFetchSpecification<VDepensesTranche> fspec = VDepensesTranche.fetchSpec();
        fspec.setRefreshesRefetchedObjects(true);
        fspec.setQualifier(VDepensesTranche.TRANCHE.eq(this));
        return fspec.fetchObjects(editingContext());
    }

    public NSArray<VRecettesTranche> sommeRecettesByNature() {
        ERXFetchSpecification<VRecettesTranche> fspec = VRecettesTranche.fetchSpec();
        fspec.setRefreshesRefetchedObjects(true);
        fspec.setQualifier(VRecettesTranche.TRANCHE.eq(this));
        return fspec.fetchObjects(editingContext());
    }

    // -- Contrôles DEPENSES
    public boolean isMontantTrancheLtSommeDepensesDestOuNature() {
        return isMontantTrancheLtSommeDepensesDest() || isMontantTrancheLtSommeDepensesNature();
    }

    public boolean isMontantTrancheLtSommeDepensesDest() {
        BigDecimal sommeDepensesDest = sommeDepensesDestination();
        return montant().compareTo(sommeDepensesDest) < 0;
    }

    public boolean isMontantTrancheLtSommeDepensesNature() {
        BigDecimal sommeDepensesNature = sommeDepensesNature();
        return montant().compareTo(sommeDepensesNature) < 0;
    }

    // -- Contrôles RECETTES
    public boolean isMontantTrancheLtSommeRecettesDestOuNature() {
        return isMontantTrancheLtSommeRecettesNature() || isMontantTrancheLtSommeRecettesDest();
    }

    public boolean isMontantTrancheLtSommeRecettesNature() {
        BigDecimal sommeRecettesNature = sommeRecettesNature();
        return traMontantFraisInclus().compareTo(sommeRecettesNature) < 0;
    }

    public boolean isMontantTrancheLtSommeRecettesDest() {
        BigDecimal sommeRecettesDestination = sommeRecettesDestination();
        return traMontantFraisInclus().compareTo(sommeRecettesDestination) < 0;
    }

    // -- Contrôles pour avenants

    public boolean isMontantTrancheLtOrEqSommeDepensesNature() {
        BigDecimal sommeDepensesNature = sommeDepensesNature();
        return montant().compareTo(sommeDepensesNature) <= 0;
    }

    public boolean isMontantTrancheLtOrEqSommeDepensesDest() {
        BigDecimal sommeDepensesDest = sommeDepensesDestination();
        return montant().compareTo(sommeDepensesDest) <= 0;
    }

    public boolean isMontantTrancheLtOrEqSommeRecettesNature() {
        BigDecimal sommeRecettesNature = sommeRecettesNature();
        return traMontantFraisInclus().compareTo(sommeRecettesNature) <= 0;
    }

    public boolean isMontantTrancheLtOrEqSommeRecettesDest() {
        BigDecimal sommeRecettesDestination = sommeRecettesDestination();
        return traMontantFraisInclus().compareTo(sommeRecettesDestination) <= 0;
    }

    public boolean isMontantTrancheLtOrEqSommeRecettesDestEtNature() {
        BigDecimal sommeRecettesDestination = sommeRecettesDestination();
        BigDecimal sommeRecettesNature = sommeRecettesNature();
        return montant().compareTo(sommeRecettesDestination) <= 0 && montant().compareTo(sommeRecettesNature) <= 0;
    }

    public EOExercice exercice() {
        EOExercice exo = exerciceCocktail().exercice();
        return exo;
    }

    public TrancheBudgetDepCP trancheBudgetsDepCP(BudgetCombinaisonAxesDepense combinaison) {
    	EOQualifier qualCombinaison = restrictListeCPSurCombinaisonAxesDepense(combinaison);
    	NSArray<TrancheBudgetDepCP> depensesCP = trancheBudgetDepsCP(qualCombinaison);
    	if (depensesCP.count() == 0) {
        	return null;
    	}
    	
    	if (depensesCP.count() > 1) {
    		throw new IncorrectResultSizeDataAccessException(1);
    	}
    	
    	return depensesCP.lastObject();
    }
    
    private EOQualifier restrictListeCPSurCombinaisonAxesDepense(BudgetCombinaisonAxesDepense combinaison) {
    	EOQualifier qualCombi = TrancheBudgetDepAE.ORGAN.eq(combinaison.getEntiteBudgetaire())
        		.and(TrancheBudgetDepCP.DESTINATION_DEPENSE.eq(combinaison.getDestination()))
        		.and(TrancheBudgetDepCP.NATURE_DEPENSE.eq(combinaison.getNature()))
    			.and(TrancheBudgetDepCP.ENVELOPPE_BUDGETAIRE.eq(combinaison.getEnveloppe()));
    	return qualCombi;
    }

    public static final String TRANCHE_BUDGETS_NON_SUPP_KEY = "trancheBudgetsNonSupprimes";

    public NSArray<TrancheBudgetDepAE> trancheBudgetsNonSupprimes() {
        return trancheBudgets(TrancheBudgetDepAE.QUAL_NON_SUPPR);
    }

    public NSArray<TrancheBudgetDepAE> trancheBudgetsNonSupprimes(NSArray<EOSortOrdering> sorts) {
        return trancheBudgets(TrancheBudgetDepAE.QUAL_NON_SUPPR, sorts, false);
    }

    public static final String TRANCHE_BUDGETS_REC_NON_SUPP_KEY = "trancheBudgetsRecNonSupprimes";

    /**
     * @return les {@link TrancheBudgetRec} non supprimés
     */
    public NSArray<TrancheBudgetRec> trancheBudgetsRecNonSupprimes() {
        return trancheBudgetRecs(TrancheBudgetRec.QUAL_NON_SUPPR);
    }

    /**
     * @param sorts
     *            - les tris a appliquer
     * @return les {@link TrancheBudgetRec} non supprimés
     */
    public NSArray<TrancheBudgetRec> trancheBudgetsRec(NSArray<EOSortOrdering> sorts) {
        return EOSortOrdering.sortedArrayUsingKeyOrderArray(trancheBudgetRecs(), sorts);
    }

    public BigDecimal totalPositionne() {
        return (BigDecimal) trancheBudgetsNonSupprimes().valueForKey("@sum." + TrancheBudgetDepAE.TB_MONTANT_KEY);
    }

    public BigDecimal totalPositionneDepCp() {
        return (BigDecimal) trancheBudgetDepsCP().valueForKey("@sum." + TrancheBudgetDepCP.MONTANT_CP_KEY);
    }
    
    public BigDecimal totalPositionneRec() {
        return (BigDecimal) trancheBudgetsRecNonSupprimes().valueForKey("@sum." + TrancheBudgetRec.TBR_MONTANT_KEY);
    }
    
    public BigDecimal totalPositionneRecTitre() {
        return (BigDecimal) trancheBudgetsRecNonSupprimes().valueForKey("@sum." + TrancheBudgetRec.TBR_MONTANT_TITRE_KEY);
    }

    public BigDecimal resteAPositionne() {
        if (reportNplus1().signum() == 1) {
            return BigDecimal.ZERO;
        } else {
            return montant().subtract(totalPositionne());
        }
    }

    public BigDecimal resteAPositionneRec() {
        if (operation().isModeRA()) {
            return montant().subtract(totalPositionneRec());
        } else {
            return operation().totalContributions().subtract(operation().totalPositionneRecOperation());
        }
    }

    public void checkMontantTranche() {
        if (operation().isModeRA()) {
            // Si le montant de la tranche est inférieur au total des crédits
            // positionnés avant le report
            if (reportNplus1().signum() == 0 && montant().compareTo(totalPositionne()) == -1) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, montant(),
                        "TotalTrancheTropBasPourCredits");
            }
            // Si le montant de la tranche est inférieur au total des dépenses
            // avant le report
            if (reportNplus1().signum() == 0 && isMontantTrancheLtSommeDepensesDestOuNature()) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, montant(),
                        "TotalTrancheTropBasPourDepenses");
            }
            // Si le montant de la tranche est inférieur au total des crédits
            // recettes positionnés
            if (reportNplus1().signum() == 0 && montant().compareTo(totalPositionneRec()) == -1) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, montant(),
                        "TotalTrancheTropBasPourCreditsRec");
            }
            // Si le montant de la tranche est inférieur au total des recettes
            if (reportNplus1().signum() == 0 && isMontantTrancheLtSommeRecettesDestOuNature()) {
                throw ERXValidationFactory.defaultFactory().createCustomException(this, Tranche.TRA_MONTANT_KEY, montant(),
                        "TotalTrancheTropBasPourRecettes");
            }
            // FIXME Ajouter la vérification lorsque reportNplus1().signum() !=
            // 0
        }
    }

    public BigDecimal totalConsomme(boolean inclureResteEngage) {
        BigDecimal montantConsomme = null;
        try {
            if (inclureResteEngage) {
                montantConsomme = new ProcedureGetTotalDepense(editingContext()).execute(exerciceCocktail().exercice(), operation(), null, null);
            } else {
                montantConsomme = new ProcedureGetTotalLiquide(editingContext()).execute(exerciceCocktail().exercice(), operation(), null, null);
            }
        } catch (Exception e) {
            throw NSForwardException._runtimeExceptionForThrowable(e);
        }
        return montantConsomme;
    }

    public BigDecimal montantReportable(BigDecimal montantConsomme) {
        BigDecimal montantReportable = BigDecimal.ZERO;
        if (montant() != null) {
            montantReportable = montant().subtract(montantConsomme);
        }
        return montantReportable;
    }

    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        checkMontantTranche();
    }

    @Deprecated
    @Override
    public void validateForDelete() throws ValidationException {
        super.validateForDelete();
        if (!sbDepenses().isEmpty()) {
            NSMutableDictionary<String, String> stringContainer = new NSMutableDictionary<String, String>(
                    exerciceCocktail().exeExercice().toString(), "tranche");
            String msg = ERXLocalizer.defaultLocalizer().localizedTemplateStringForKeyWithObject("DepensesExistantes", stringContainer);
            throw new ValidationException(msg);
        }
    }

    public void valider(EOUtilisateur validateur) {
    	setTypeEtatRelationship(EOTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ETAT_VALIDE));
        setPersIdValid(validateur.personne().persId());
        setTraDateValid(new NSTimestamp());
    }

    public void devalider(EOUtilisateur validateur) {
        this.setPersIdModif(validateur.personne().persId());
        this.setTraDateModif(new NSTimestamp());
        this.setTypeEtatRelationship(EOTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ETAT_A_VALIDER));
    }

    public boolean isValide() {
        boolean estValidee = EOTypeEtat.ETAT_VALIDE.equals(typeEtat().tyetLibelle());
        return estValidee;
    }

    public String libelleTranche() {
        return exerciceCocktail().exeExercice().toString();
    }

    public boolean isTrancheSourceReportCredit() {
        NSArray<Tranche> tranchesSources = operation().tranches(EOExercice.EXE_ETAT_RESTREINT, EOExercice.EXE_ETAT_OUVERT);
        return tranchesSources.containsObject(this);
    }

    public static void effectuerReportDeCredits(Tranche tranche, BigDecimal montant, EOEditingContext edc, EOUtilisateur utilisateur)
            throws Exception {
        Tranche suivante = tranche.trancheExerciceSuivant();
        // GBCP : Suppression de la gestion manuelle des tranches pour la GBCP -
        // On garde pour mémoire ou rétropédalage...
        // Boolean gestionAutomatiqueDesTranches =
        // ERXProperties.booleanForKeyWithDefault(Constantes.GESTION_TRANCHE_AUTO_PARAM,
        // true);
        Boolean gestionAutomatiqueDesTranches = true;
        if (suivante == null) {
            if (gestionAutomatiqueDesTranches) {
                throw new Exception("La tranche " + (tranche.exerciceCocktail().exeExercice() + 1) + " n'existe pas. Veuillez créer"
                        + " un avenant allant jusqu'à l'année " + (tranche.exerciceCocktail().exeExercice() + 1)
                        + " avant d'effectuer le report des crédits.");
            } else {
                FactoryTranche factory = new FactoryTranche(edc);
                EOExerciceCocktail exerciceSuivant = EOExerciceCocktail.fetchFirstByQualifier(edc,
                        ERXQ.equals(EOExerciceCocktail.EXE_EXERCICE_KEY, tranche.exerciceCocktail().exeExercice() + 1));
                suivante = factory.creerTranche(tranche.operation(), exerciceSuivant, utilisateur);
            }
        }

        BigDecimal montantDuReport;

        if (montant == null) {
            montantDuReport = tranche.montant().subtract(tranche.totalConsomme(true));
        } else {
            montantDuReport = montant;
        }
        tranche.setReportNplus1(montantDuReport);
        suivante.setReportNmoins1(montantDuReport);

    }

    public Tranche trancheExerciceSuivant() {
        EOQualifier qualifier = Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exerciceCocktail().exeExercice() + 1)
                .and(Tranche.QUALIFIER_NON_SUPPR).and(Tranche.OPERATION.eq(operation()));
        return Tranche.fetch(editingContext(), qualifier);
    }

    public Tranche trancheExercicePrecedent() {
        EOQualifier qualifier = Tranche.EXERCICE_COCKTAIL.dot(EOExerciceCocktail.EXE_EXERCICE_KEY).eq(exerciceCocktail().exeExercice() - 1)
                .and(Tranche.QUALIFIER_NON_SUPPR).and(Tranche.OPERATION.eq(operation()));
        return Tranche.fetch(editingContext(), qualifier);
    }

    public void checkSuppressionPossible() throws ValidationException {
        if (sbRecettes().isEmpty()) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesRecettes");
        }
        if (sbDepenses().isEmpty()) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesDepenses");
        }
        if (trancheBudgets(TrancheBudgetDepAE.QUAL_NON_SUPPR).isEmpty()) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesCreditPositionnesDepenses");
        }
        if (trancheBudgetRecs(TrancheBudgetRec.QUAL_NON_SUPPR).isEmpty()) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, "ContientDesCreditPositionnesRecettes");
        }
    }

    @Override
    public void delete() {
        Tranche precedente = trancheExercicePrecedent();
        if (precedente != null) {
            precedente.setReportNplus1(BigDecimal.ZERO);
        }
        super.delete();
    }

    public static final ERXKey<BigDecimal> TO_REPART_PARTENAIRE_TRANCHES_TIERS = new ERXKey<BigDecimal>("toRepartPartenaireTrancheTiers");

    /**
     * @param uneTranche
     * @return La liste des partenaires externe à l'établissement, en se basant
     *         sur {@link IPersonne#isInterneEtablissement()}
     */
    public NSArray<RepartPartenaireTranche> toRepartPartenaireTrancheTiers() {
        List<RepartPartenaireTranche> result = new ArrayList<RepartPartenaireTranche>();
        for (RepartPartenaireTranche rpt : this.toRepartPartenaireTranches()) {
            IPersonne partenaire = rpt.operationPartenaire().partenaire();
            if (!partenaire.isInterneEtablissement()) {
                result.add(rpt);
            }
        }
        return new NSArray<RepartPartenaireTranche>(result);
    }

    public boolean hasDetailsDepensesAE() {
        return trancheBudgets() != null && trancheBudgetsNonSupprimes().count() > 0;
    }

    public void supprimerPrevisionDepenseAE(TrancheBudgetDepAE trancheBudget) {
    	removeFromTrancheBudgetsRelationship(trancheBudget);
    }

    /**
     * indique si la tranche est une tranche de trésorerie uniquement
     * i.e si elle est postérieure à la date de fin d'opération
     */
    public boolean isTreso() {
        return exerciceCocktail().premierJanvier().compareTo(operation().dateFin()) > 0;
    }

    /**
     * La tranche est-elle intégrée ou liée à un exercice clos ?
     */
    public boolean estVerrouillee() {
        EOExercice exercice = exercice();
        boolean estExerciceClos = false;
        if (exercice != null) {
            estExerciceClos = exercice.estClos();
        }
        return estExerciceClos || estIntegreeDansUnBudgetNonOuvert();
    }

    /**
     * 
     * @return l'état
     */
    private boolean estIntegreeDansUnBudgetNonOuvert() {
        int nb = estIntegreeDansUnBudgetNonOuvertCache.getUnchecked("vide");
        return nb > 0;
    }

    private Integer estIntegreeDansUnBudgetNonOuvertInternal() {
        EOQualifier qualExisteTranche = VBudOpeTraInteg.TRANCHE.eq(this);
        return ERXEOControlUtilities.objectCountWithQualifier(editingContext(), VBudOpeTraInteg.ENTITY_NAME, qualExisteTranche);
    }
    
    private LoadingCache<String, Integer> estIntegreeDansUnBudgetNonOuvertCache = CacheBuilder
            .newBuilder()
            .maximumSize(1000)
            .expireAfterWrite(10, TimeUnit.SECONDS)
            .build(new CacheLoader<String, Integer>() {
                @Override
                public Integer load(String key) throws Exception {
                    return estIntegreeDansUnBudgetNonOuvertInternal();
                }
            });
    

    /**
     * La tranche est-elle déjà utilisée dans la cadre d'budget validé
     * 
     * @return l'état
     * @deprecated
     */
    public boolean estAssocieeAUnBudgetValide() {
        EOEditingContext ec = editingContext();
        EOQualifier qualTranche = VBudOpeTraAssoBud.TRANCHE.eq(this);
        int count = ERXEOControlUtilities.objectCountWithQualifier(ec, VBudOpeTraAssoBud.ENTITY_NAME, qualTranche);
        boolean estAssociee = (count >= 1);
        return estAssociee;
    }
    
    public boolean estBrouillon() {
        return !isValide() && estNonIntegree();
    }

    private boolean estNonIntegree() {
    	EOEditingContext ec = editingContext();
    	EOQualifier qualCountDepensesIntegrees = TrancheBudgetDepAE.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_AES.dot(EOBudgetPrevisionOperationTrancheDepenseAe.PRIMARY_KEY).isNotNull()
    			.and(TrancheBudgetDepAE.TRANCHE.eq(this));
    	int countDepensesIntegrees = ERXEOControlUtilities.objectCountWithQualifier(ec, TrancheBudgetDepAE.ENTITY_NAME, qualCountDepensesIntegrees);
    	
    	EOQualifier qualCountRecettesIntegrees = TrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES.dot(EOBudgetPrevisionOperationTrancheRecette.PRIMARY_KEY).isNotNull()
    			.and(TrancheBudgetRec.TRANCHE.eq(this));
    	int countRecettesIntegrees = ERXEOControlUtilities.objectCountWithQualifier(ec, TrancheBudgetRec.ENTITY_NAME, qualCountRecettesIntegrees);
    	
    	int count = countDepensesIntegrees + countRecettesIntegrees;
    	return count == 0;
    }
}
