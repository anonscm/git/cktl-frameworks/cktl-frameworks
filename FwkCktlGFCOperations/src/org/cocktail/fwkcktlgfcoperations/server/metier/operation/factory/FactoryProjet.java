package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet;

import com.webobjects.eocontrol.EOEditingContext;

public class FactoryProjet extends Factory {
	
	/**
	 * Constructeur.
	 * @param ec
	 */
	public FactoryProjet(EOEditingContext ec) {
		super(ec);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Constructeur.
	 * @param ec
	 * @param withlog
	 */
	public FactoryProjet(EOEditingContext ec, Boolean withlog) {
		super(ec, withlog);
		// TODO Auto-generated constructor stub
	}
	
	/**
	 * Creation d'un projet de contrats.
	 */
	public Projet creerProjet(Projet projetPere, TypeProjet typeProjet, String libelleProjet)  throws Exception, ExceptionUtilisateur {
		
		Projet newProject=Projet.instanciate(ec);
		ec.insertObject(newProject);
		
		newProject.setTypeProjetRelationship(typeProjet);
		newProject.setProjetPereRelationship(projetPere);
		newProject.setExerciceCocktailRelationship(new FinderExerciceCocktail(ec).findExerciceCocktailCourant());
		newProject.setPjtLibelle(libelleProjet);
		
		return newProject;
		
	}
	
	public ProjetContrat ajouterContrat(Projet projet, Operation operation)  throws Exception, ExceptionUtilisateur {
		
		ProjetContrat newProjetContrat = ProjetContrat.instanciate(ec);
		ec.insertObject(newProjetContrat);
		newProjetContrat.setContratRelationship(operation);
		operation.addToProjetContratRelationship(newProjetContrat);
		newProjetContrat.setProjetRelationship(projet);
		projet.addToProjetContratsRelationship(newProjetContrat);
		
		return newProjetContrat;
		
	}
	
	
	
	

}
