package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import lombok.Getter;

@Getter
public class TypeOperationBean {

	public static final String CODE_TYPE_OPERATION = "codeTypeOperation";
	
	private String codeTypeOperation;
	private String llTypeOperation;
	
	public TypeOperationBean(String codeTypeOperation, String llTypeOperation) {
        this.codeTypeOperation = codeTypeOperation;
        this.llTypeOperation = llTypeOperation;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codeTypeOperation == null) ? 0 : codeTypeOperation
						.hashCode());
		result = prime * result
				+ ((llTypeOperation == null) ? 0 : llTypeOperation.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TypeOperationBean other = (TypeOperationBean) obj;
		if (codeTypeOperation == null) {
			if (other.codeTypeOperation != null)
				return false;
		} else if (!codeTypeOperation.equals(other.codeTypeOperation))
			return false;
		if (llTypeOperation == null) {
			if (other.llTypeOperation != null)
				return false;
		} else if (!llTypeOperation.equals(other.llTypeOperation))
			return false;
		return true;
	}
	
}
