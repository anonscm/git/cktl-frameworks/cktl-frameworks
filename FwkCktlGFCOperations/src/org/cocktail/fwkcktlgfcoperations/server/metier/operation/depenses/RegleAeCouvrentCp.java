package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

public class RegleAeCouvrentCp {

	private DepenseCPSurCombinaisonAxes cpSurCombinaison;
	
	public RegleAeCouvrentCp(DepenseCPSurCombinaisonAxes cpSurCombinaison) {
		this.cpSurCombinaison = cpSurCombinaison;
	}
	
	public boolean isValid(Tranche tranche) {
		BigDecimal resteAPayer = cpSurCombinaison.getResteAPayer(tranche);
		if (resteAPayer.signum() == -1) {
			return false;
		}
		return true;
	}
	
}
