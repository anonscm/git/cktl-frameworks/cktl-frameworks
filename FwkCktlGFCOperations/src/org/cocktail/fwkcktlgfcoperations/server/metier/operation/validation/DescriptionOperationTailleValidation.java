package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

public class DescriptionOperationTailleValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    public boolean isSatisfiedBy(Operation operation) {
        if (operation == null || operation.description() == null) {
            return true;
        }
               
        return operation.description().length() <= Operation.DESCRIPTION_MAX_SIZE; 
    } 
}