// DO NOT EDIT.  Make changes to FluxDepenseRececette.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOFluxDepenseRececette extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeFluxDepenseRececette";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_FLUX_DEP_REC";

  // Attribute Keys
  public static final ERXKey<String> CODE_FLUX_DEP_REC = new ERXKey<String>("codeFluxDepRec");
  public static final ERXKey<Integer> ID_OPE_FLUX_DEP_REC = new ERXKey<Integer>("idOpeFluxDepRec");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  // Relationship Keys

  // Attributes
  public static final String CODE_FLUX_DEP_REC_KEY = CODE_FLUX_DEP_REC.key();
  public static final String ID_OPE_FLUX_DEP_REC_KEY = ID_OPE_FLUX_DEP_REC.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOFluxDepenseRececette.class);

  public FluxDepenseRececette localInstanceIn(EOEditingContext editingContext) {
    FluxDepenseRececette localInstance = (FluxDepenseRececette)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeFluxDepRec() {
    return (String) storedValueForKey(EOFluxDepenseRececette.CODE_FLUX_DEP_REC_KEY);
  }

  public void setCodeFluxDepRec(String value) {
    if (EOFluxDepenseRececette.LOG.isDebugEnabled()) {
        EOFluxDepenseRececette.LOG.debug( "updating codeFluxDepRec from " + codeFluxDepRec() + " to " + value);
    }
    takeStoredValueForKey(value, EOFluxDepenseRececette.CODE_FLUX_DEP_REC_KEY);
  }

  public Integer idOpeFluxDepRec() {
    return (Integer) storedValueForKey(EOFluxDepenseRececette.ID_OPE_FLUX_DEP_REC_KEY);
  }

  public void setIdOpeFluxDepRec(Integer value) {
    if (EOFluxDepenseRececette.LOG.isDebugEnabled()) {
        EOFluxDepenseRececette.LOG.debug( "updating idOpeFluxDepRec from " + idOpeFluxDepRec() + " to " + value);
    }
    takeStoredValueForKey(value, EOFluxDepenseRececette.ID_OPE_FLUX_DEP_REC_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(EOFluxDepenseRececette.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (EOFluxDepenseRececette.LOG.isDebugEnabled()) {
        EOFluxDepenseRececette.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EOFluxDepenseRececette.LIBELLE_LONG_KEY);
  }


  public static FluxDepenseRececette create(EOEditingContext editingContext, String codeFluxDepRec
, Integer idOpeFluxDepRec
, String libelleLong
) {
    FluxDepenseRececette eo = (FluxDepenseRececette) EOUtilities.createAndInsertInstance(editingContext, EOFluxDepenseRececette.ENTITY_NAME);    
        eo.setCodeFluxDepRec(codeFluxDepRec);
        eo.setIdOpeFluxDepRec(idOpeFluxDepRec);
        eo.setLibelleLong(libelleLong);
    return eo;
  }

  public static ERXFetchSpecification<FluxDepenseRececette> fetchSpec() {
    return new ERXFetchSpecification<FluxDepenseRececette>(EOFluxDepenseRececette.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<FluxDepenseRececette> fetchAll(EOEditingContext editingContext) {
    return EOFluxDepenseRececette.fetchAll(editingContext, null);
  }

  public static NSArray<FluxDepenseRececette> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOFluxDepenseRececette.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<FluxDepenseRececette> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<FluxDepenseRececette> fetchSpec = new ERXFetchSpecification<FluxDepenseRececette>(EOFluxDepenseRececette.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<FluxDepenseRececette> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static FluxDepenseRececette fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOFluxDepenseRececette.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FluxDepenseRececette fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<FluxDepenseRececette> eoObjects = EOFluxDepenseRececette.fetchAll(editingContext, qualifier, null);
    FluxDepenseRececette eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeFluxDepenseRececette that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FluxDepenseRececette fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOFluxDepenseRececette.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FluxDepenseRececette fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    FluxDepenseRececette eoObject = EOFluxDepenseRececette.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeFluxDepenseRececette that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FluxDepenseRececette localInstanceIn(EOEditingContext editingContext, FluxDepenseRececette eo) {
    FluxDepenseRececette localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette> fetchFetchAll(EOEditingContext editingContext, NSDictionary<String, Object> bindings) {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOFluxDepenseRececette.ENTITY_NAME);
    fetchSpec = fetchSpec.fetchSpecificationWithQualifierBindings(bindings);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
  public static NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette> fetchFetchAll(EOEditingContext editingContext)
  {
    EOFetchSpecification fetchSpec = EOFetchSpecification.fetchSpecificationNamed("FetchAll", EOFluxDepenseRececette.ENTITY_NAME);
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette>)editingContext.objectsWithFetchSpecification(fetchSpec);
  }
  
}