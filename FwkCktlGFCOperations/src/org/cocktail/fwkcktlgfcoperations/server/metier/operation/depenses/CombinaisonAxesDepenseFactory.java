package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import org.cocktail.fwkcktlgfceos.server.finder.FinderEnveloppeBudgetaire;
import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureDep;

import com.webobjects.eocontrol.EOEditingContext;

public class CombinaisonAxesDepenseFactory {

	public static BudgetCombinaisonAxesDepense create(EOEditingContext edc,
			EOEb eb, EODestinationDepense destination, EONatureDep nature) {
		EOEnveloppeBudgetaire envStd = FinderEnveloppeBudgetaire.findEnveloppeStandard(edc);
		return new BudgetCombinaisonAxesDepense(eb, destination, nature, envStd);
	}
	
}
