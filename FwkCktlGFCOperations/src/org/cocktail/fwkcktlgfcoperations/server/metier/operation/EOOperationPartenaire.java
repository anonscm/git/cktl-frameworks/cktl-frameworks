// DO NOT EDIT.  Make changes to OperationPartenaire.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOOperationPartenaire extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeOperationPartenaire";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_OPERATION_PARTENAIRE";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> CP_DATE_SIGNATURE = new ERXKey<NSTimestamp>("cpDateSignature");
  public static final ERXKey<java.math.BigDecimal> CP_MONTANT = new ERXKey<java.math.BigDecimal>("cpMontant");
  public static final ERXKey<String> CP_PRINCIPAL = new ERXKey<String>("cpPrincipal");
  public static final ERXKey<String> CP_REFERENCE_EXTERNE = new ERXKey<String>("cpReferenceExterne");
  public static final ERXKey<Integer> PERS_ID = new ERXKey<Integer>("persId");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> CONTRIBUTIONS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>("contributions");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation> OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation>("operation");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> OPERATION_PART_CONTACTS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact>("operationPartContacts");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire> TYPE_PARTENAIRE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire>("typePartenaire");

  // Attributes
  public static final String CP_DATE_SIGNATURE_KEY = CP_DATE_SIGNATURE.key();
  public static final String CP_MONTANT_KEY = CP_MONTANT.key();
  public static final String CP_PRINCIPAL_KEY = CP_PRINCIPAL.key();
  public static final String CP_REFERENCE_EXTERNE_KEY = CP_REFERENCE_EXTERNE.key();
  public static final String PERS_ID_KEY = PERS_ID.key();
  // Relationships
  public static final String CONTRIBUTIONS_KEY = CONTRIBUTIONS.key();
  public static final String OPERATION_KEY = OPERATION.key();
  public static final String OPERATION_PART_CONTACTS_KEY = OPERATION_PART_CONTACTS.key();
  public static final String TYPE_PARTENAIRE_KEY = TYPE_PARTENAIRE.key();

  private static Logger LOG = Logger.getLogger(EOOperationPartenaire.class);

  public OperationPartenaire localInstanceIn(EOEditingContext editingContext) {
    OperationPartenaire localInstance = (OperationPartenaire)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp cpDateSignature() {
    return (NSTimestamp) storedValueForKey(EOOperationPartenaire.CP_DATE_SIGNATURE_KEY);
  }

  public void setCpDateSignature(NSTimestamp value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
        EOOperationPartenaire.LOG.debug( "updating cpDateSignature from " + cpDateSignature() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartenaire.CP_DATE_SIGNATURE_KEY);
  }

  public java.math.BigDecimal cpMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOOperationPartenaire.CP_MONTANT_KEY);
  }

  public void setCpMontant(java.math.BigDecimal value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
        EOOperationPartenaire.LOG.debug( "updating cpMontant from " + cpMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartenaire.CP_MONTANT_KEY);
  }

  public String cpPrincipal() {
    return (String) storedValueForKey(EOOperationPartenaire.CP_PRINCIPAL_KEY);
  }

  public void setCpPrincipal(String value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
        EOOperationPartenaire.LOG.debug( "updating cpPrincipal from " + cpPrincipal() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartenaire.CP_PRINCIPAL_KEY);
  }

  public String cpReferenceExterne() {
    return (String) storedValueForKey(EOOperationPartenaire.CP_REFERENCE_EXTERNE_KEY);
  }

  public void setCpReferenceExterne(String value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
        EOOperationPartenaire.LOG.debug( "updating cpReferenceExterne from " + cpReferenceExterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartenaire.CP_REFERENCE_EXTERNE_KEY);
  }

  public Integer persId() {
    return (Integer) storedValueForKey(EOOperationPartenaire.PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
        EOOperationPartenaire.LOG.debug( "updating persId from " + persId() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperationPartenaire.PERS_ID_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation operation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation)storedValueForKey(EOOperationPartenaire.OPERATION_KEY);
  }
  
  public void setOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    takeStoredValueForKey(value, EOOperationPartenaire.OPERATION_KEY);
  }

  public void setOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("updating operation from " + operation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation oldValue = operation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartenaire.OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartenaire.OPERATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire typePartenaire() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire)storedValueForKey(EOOperationPartenaire.TYPE_PARTENAIRE_KEY);
  }
  
  public void setTypePartenaire(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire value) {
    takeStoredValueForKey(value, EOOperationPartenaire.TYPE_PARTENAIRE_KEY);
  }

  public void setTypePartenaireRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire value) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("updating typePartenaire from " + typePartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypePartenaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypePartenaire oldValue = typePartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperationPartenaire.TYPE_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperationPartenaire.TYPE_PARTENAIRE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> contributions() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)storedValueForKey(EOOperationPartenaire.CONTRIBUTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> contributions(EOQualifier qualifier) {
    return contributions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> contributions(EOQualifier qualifier, boolean fetch) {
    return contributions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> contributions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.OPERATION_PARTENAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = contributions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToContributions(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    includeObjectIntoPropertyWithKey(object, EOOperationPartenaire.CONTRIBUTIONS_KEY);
  }

  public void removeFromContributions(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    excludeObjectFromPropertyWithKey(object, EOOperationPartenaire.CONTRIBUTIONS_KEY);
  }

  public void addToContributionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("adding " + object + " to contributions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToContributions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.CONTRIBUTIONS_KEY);
    }
  }

  public void removeFromContributionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("removing " + object + " from contributions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromContributions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.CONTRIBUTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche createContributionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperationPartenaire.CONTRIBUTIONS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche) eo;
  }

  public void deleteContributionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.CONTRIBUTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContributionsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> objects = contributions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContributionsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> operationPartContacts() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact>)storedValueForKey(EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> operationPartContacts(EOQualifier qualifier) {
    return operationPartContacts(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> operationPartContacts(EOQualifier qualifier, boolean fetch) {
    return operationPartContacts(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> operationPartContacts(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact.OPERATION_PARTENAIRE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = operationPartContacts();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOperationPartContacts(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact object) {
    includeObjectIntoPropertyWithKey(object, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
  }

  public void removeFromOperationPartContacts(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact object) {
    excludeObjectFromPropertyWithKey(object, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
  }

  public void addToOperationPartContactsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact object) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("adding " + object + " to operationPartContacts relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToOperationPartContacts(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
    }
  }

  public void removeFromOperationPartContactsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact object) {
    if (EOOperationPartenaire.LOG.isDebugEnabled()) {
      EOOperationPartenaire.LOG.debug("removing " + object + " from operationPartContacts relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromOperationPartContacts(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact createOperationPartContactsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact) eo;
  }

  public void deleteOperationPartContactsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperationPartenaire.OPERATION_PART_CONTACTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOperationPartContactsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartContact> objects = operationPartContacts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOperationPartContactsRelationship(objects.nextElement());
    }
  }


  public static OperationPartenaire create(EOEditingContext editingContext, String cpPrincipal
, Integer persId
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation operation) {
    OperationPartenaire eo = (OperationPartenaire) EOUtilities.createAndInsertInstance(editingContext, EOOperationPartenaire.ENTITY_NAME);    
        eo.setCpPrincipal(cpPrincipal);
        eo.setPersId(persId);
    eo.setOperationRelationship(operation);
    return eo;
  }

  public static ERXFetchSpecification<OperationPartenaire> fetchSpec() {
    return new ERXFetchSpecification<OperationPartenaire>(EOOperationPartenaire.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<OperationPartenaire> fetchAll(EOEditingContext editingContext) {
    return EOOperationPartenaire.fetchAll(editingContext, null);
  }

  public static NSArray<OperationPartenaire> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOOperationPartenaire.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<OperationPartenaire> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<OperationPartenaire> fetchSpec = new ERXFetchSpecification<OperationPartenaire>(EOOperationPartenaire.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<OperationPartenaire> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static OperationPartenaire fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperationPartenaire.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OperationPartenaire fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<OperationPartenaire> eoObjects = EOOperationPartenaire.fetchAll(editingContext, qualifier, null);
    OperationPartenaire eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeOperationPartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OperationPartenaire fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperationPartenaire.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OperationPartenaire fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    OperationPartenaire eoObject = EOOperationPartenaire.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeOperationPartenaire that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OperationPartenaire localInstanceIn(EOEditingContext editingContext, OperationPartenaire eo) {
    OperationPartenaire localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}