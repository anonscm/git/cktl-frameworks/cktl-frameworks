// DO NOT EDIT.  Make changes to TrancheBudgetRec.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTrancheBudgetRec extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTrancheBudgetRec";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_TRANCHE_BUD_REC";

  // Attribute Keys
  public static final ERXKey<String> COMMENTAIRE = new ERXKey<String>("commentaire");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<NSTimestamp> TBR_DATE_CREATION = new ERXKey<NSTimestamp>("tbrDateCreation");
  public static final ERXKey<NSTimestamp> TBR_DATE_MODIF = new ERXKey<NSTimestamp>("tbrDateModif");
  public static final ERXKey<java.math.BigDecimal> TBR_MONTANT = new ERXKey<java.math.BigDecimal>("tbrMontant");
  public static final ERXKey<java.math.BigDecimal> TBR_MONTANT_TITRE = new ERXKey<java.math.BigDecimal>("tbrMontantTitre");
  public static final ERXKey<String> TBR_SUPPR = new ERXKey<String>("tbrSuppr");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette>("budgetPrevisionOperationTrancheRecettes");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureRec> NATURE_REC = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureRec>("natureRec");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette> ORIGINE_RECETTE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette>("origineRecette");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette> SECTION_RECETTE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette>("sectionRecette");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String COMMENTAIRE_KEY = COMMENTAIRE.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String TBR_DATE_CREATION_KEY = TBR_DATE_CREATION.key();
  public static final String TBR_DATE_MODIF_KEY = TBR_DATE_MODIF.key();
  public static final String TBR_MONTANT_KEY = TBR_MONTANT.key();
  public static final String TBR_MONTANT_TITRE_KEY = TBR_MONTANT_TITRE.key();
  public static final String TBR_SUPPR_KEY = TBR_SUPPR.key();
  // Relationships
  public static final String BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY = BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES.key();
  public static final String NATURE_REC_KEY = NATURE_REC.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String ORIGINE_RECETTE_KEY = ORIGINE_RECETTE.key();
  public static final String SECTION_RECETTE_KEY = SECTION_RECETTE.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOTrancheBudgetRec.class);

  public TrancheBudgetRec localInstanceIn(EOEditingContext editingContext) {
    TrancheBudgetRec localInstance = (TrancheBudgetRec)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String commentaire() {
    return (String) storedValueForKey(EOTrancheBudgetRec.COMMENTAIRE_KEY);
  }

  public void setCommentaire(String value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating commentaire from " + commentaire() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.COMMENTAIRE_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOTrancheBudgetRec.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(EOTrancheBudgetRec.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.PERS_ID_MODIFICATION_KEY);
  }

  public NSTimestamp tbrDateCreation() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_CREATION_KEY);
  }

  public void setTbrDateCreation(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateCreation from " + tbrDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_CREATION_KEY);
  }

  public NSTimestamp tbrDateModif() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetRec.TBR_DATE_MODIF_KEY);
  }

  public void setTbrDateModif(NSTimestamp value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrDateModif from " + tbrDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_DATE_MODIF_KEY);
  }

  public java.math.BigDecimal tbrMontant() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MONTANT_KEY);
  }

  public void setTbrMontant(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMontant from " + tbrMontant() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MONTANT_KEY);
  }

  public java.math.BigDecimal tbrMontantTitre() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetRec.TBR_MONTANT_TITRE_KEY);
  }

  public void setTbrMontantTitre(java.math.BigDecimal value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrMontantTitre from " + tbrMontantTitre() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_MONTANT_TITRE_KEY);
  }

  public String tbrSuppr() {
    return (String) storedValueForKey(EOTrancheBudgetRec.TBR_SUPPR_KEY);
  }

  public void setTbrSuppr(String value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
        EOTrancheBudgetRec.LOG.debug( "updating tbrSuppr from " + tbrSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetRec.TBR_SUPPR_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EONatureRec natureRec() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EONatureRec)storedValueForKey(EOTrancheBudgetRec.NATURE_REC_KEY);
  }
  
  public void setNatureRec(org.cocktail.fwkcktlgfceos.server.metier.EONatureRec value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.NATURE_REC_KEY);
  }

  public void setNatureRecRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureRec value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating natureRec from " + natureRec() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setNatureRec(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EONatureRec oldValue = natureRec();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.NATURE_REC_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.NATURE_REC_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOTrancheBudgetRec.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette origineRecette() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette)storedValueForKey(EOTrancheBudgetRec.ORIGINE_RECETTE_KEY);
  }
  
  public void setOrigineRecette(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.ORIGINE_RECETTE_KEY);
  }

  public void setOrigineRecetteRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating origineRecette from " + origineRecette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrigineRecette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette oldValue = origineRecette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.ORIGINE_RECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.ORIGINE_RECETTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette sectionRecette() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette)storedValueForKey(EOTrancheBudgetRec.SECTION_RECETTE_KEY);
  }
  
  public void setSectionRecette(org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.SECTION_RECETTE_KEY);
  }

  public void setSectionRecetteRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating sectionRecette from " + sectionRecette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSectionRecette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette oldValue = sectionRecette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.SECTION_RECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.SECTION_RECETTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOTrancheBudgetRec.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOTrancheBudgetRec.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetRec.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetRec.TRANCHE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> budgetPrevisionOperationTrancheRecettes() {
    return (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette>)storedValueForKey(EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> budgetPrevisionOperationTrancheRecettes(EOQualifier qualifier) {
    return budgetPrevisionOperationTrancheRecettes(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> budgetPrevisionOperationTrancheRecettes(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> results;
      results = budgetPrevisionOperationTrancheRecettes();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToBudgetPrevisionOperationTrancheRecettes(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette object) {
    includeObjectIntoPropertyWithKey(object, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
  }

  public void removeFromBudgetPrevisionOperationTrancheRecettes(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette object) {
    excludeObjectFromPropertyWithKey(object, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
  }

  public void addToBudgetPrevisionOperationTrancheRecettesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette object) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("adding " + object + " to budgetPrevisionOperationTrancheRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToBudgetPrevisionOperationTrancheRecettes(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
    }
  }

  public void removeFromBudgetPrevisionOperationTrancheRecettesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette object) {
    if (EOTrancheBudgetRec.LOG.isDebugEnabled()) {
      EOTrancheBudgetRec.LOG.debug("removing " + object + " from budgetPrevisionOperationTrancheRecettes relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromBudgetPrevisionOperationTrancheRecettes(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette createBudgetPrevisionOperationTrancheRecettesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette) eo;
  }

  public void deleteBudgetPrevisionOperationTrancheRecettesRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetRec.BUDGET_PREVISION_OPERATION_TRANCHE_RECETTES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBudgetPrevisionOperationTrancheRecettesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheRecette> objects = budgetPrevisionOperationTrancheRecettes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBudgetPrevisionOperationTrancheRecettesRelationship(objects.nextElement());
    }
  }


  public static TrancheBudgetRec create(EOEditingContext editingContext, Integer persIdCreation
, NSTimestamp tbrDateCreation
, java.math.BigDecimal tbrMontant
, java.math.BigDecimal tbrMontantTitre
, org.cocktail.fwkcktlgfceos.server.metier.EONatureRec natureRec, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette origineRecette, org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette sectionRecette, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    TrancheBudgetRec eo = (TrancheBudgetRec) EOUtilities.createAndInsertInstance(editingContext, EOTrancheBudgetRec.ENTITY_NAME);    
        eo.setPersIdCreation(persIdCreation);
        eo.setTbrDateCreation(tbrDateCreation);
        eo.setTbrMontant(tbrMontant);
        eo.setTbrMontantTitre(tbrMontantTitre);
    eo.setNatureRecRelationship(natureRec);
    eo.setOrganRelationship(organ);
    eo.setOrigineRecetteRelationship(origineRecette);
    eo.setSectionRecetteRelationship(sectionRecette);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<TrancheBudgetRec> fetchSpec() {
    return new ERXFetchSpecification<TrancheBudgetRec>(EOTrancheBudgetRec.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext) {
    return EOTrancheBudgetRec.fetchAll(editingContext, null);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTrancheBudgetRec.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TrancheBudgetRec> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TrancheBudgetRec> fetchSpec = new ERXFetchSpecification<TrancheBudgetRec>(EOTrancheBudgetRec.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TrancheBudgetRec> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TrancheBudgetRec fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetRec.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetRec fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TrancheBudgetRec> eoObjects = EOTrancheBudgetRec.fetchAll(editingContext, qualifier, null);
    TrancheBudgetRec eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTrancheBudgetRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetRec fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetRec.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetRec fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TrancheBudgetRec eoObject = EOTrancheBudgetRec.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTrancheBudgetRec that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetRec localInstanceIn(EOEditingContext editingContext, TrancheBudgetRec eo) {
    TrancheBudgetRec localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}