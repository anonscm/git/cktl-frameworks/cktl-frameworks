package org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang.Validate;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EONatureRec;
import org.cocktail.fwkcktlgfceos.server.metier.EOOrigineRecette;
import org.cocktail.fwkcktlgfceos.server.metier.EOSectionRecette;

/**
 * @author bourges
 *
 */
@Getter
@Setter
public final class BudgetCombinaisonAxesRecette {

    private EOEb eb;
    private EONatureRec nature;
    private EOOrigineRecette origine;
    private EOSectionRecette section;

    @Builder
    private BudgetCombinaisonAxesRecette(EOEb eb, EONatureRec nature, EOOrigineRecette origine, EOSectionRecette section) {
        Validate.notNull(eb, "L'entité budgétaire est obligatoire.");
        Validate.notNull(nature, "La nature est obligatoire.");
        Validate.notNull(origine, "L'origine est obligatoire.");
        Validate.notNull(section, "La section est obligatoire.");

        this.eb = eb;
        this.nature = nature;
        this.origine = origine;
        this.section = section;
    }
}
