package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEbExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfcoperations.common.validation.Validator;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXEOControlUtilities;

public class EntiteBudgetaireExerciceValidator implements Validator<EOEb> {

	private EOEditingContext edc;
	private EOExercice exercice;
	
	public EntiteBudgetaireExerciceValidator(EOEditingContext edc, EOExercice exercice) {
		this.edc = edc;
		this.exercice = exercice;
	}
	
	public boolean isValid(EOEb eb) {
		EOQualifier qualExisteEbSurExercice = EOEbExercice.ORGAN.eq(eb)
				.and(EOEbExercice.EXE_ORDRE.eq(exercice.exeExercice().intValue()));
		int countEb = ERXEOControlUtilities.objectCountWithQualifier(edc, EOEbExercice.ENTITY_NAME, qualExisteEbSurExercice);
		return countEb > 0;
	}
}
