// DO NOT EDIT.  Make changes to TrancheBudgetDepCP.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTrancheBudgetDepCP extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTrancheBudgetDepCP";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_TRANCHE_BUD_DEP_CP";

  // Attribute Keys
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<java.math.BigDecimal> MONTANT_CP = new ERXKey<java.math.BigDecimal>("montantCp");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIF = new ERXKey<Integer>("persIdModif");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp>("budgetPrevisionOperationTrancheDepenseCps");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> DESTINATION_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("destinationDepense");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire> ENVELOPPE_BUDGETAIRE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire>("enveloppeBudgetaire");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep> NATURE_DEPENSE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EONatureDep>("natureDepense");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String MONTANT_CP_KEY = MONTANT_CP.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIF_KEY = PERS_ID_MODIF.key();
  // Relationships
  public static final String BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY = BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS.key();
  public static final String DESTINATION_DEPENSE_KEY = DESTINATION_DEPENSE.key();
  public static final String ENVELOPPE_BUDGETAIRE_KEY = ENVELOPPE_BUDGETAIRE.key();
  public static final String NATURE_DEPENSE_KEY = NATURE_DEPENSE.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOTrancheBudgetDepCP.class);

  public TrancheBudgetDepCP localInstanceIn(EOEditingContext editingContext) {
    TrancheBudgetDepCP localInstance = (TrancheBudgetDepCP)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetDepCP.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepCP.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.DATE_CREATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(EOTrancheBudgetDepCP.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepCP.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.DATE_MODIFICATION_KEY);
  }

  public java.math.BigDecimal montantCp() {
    return (java.math.BigDecimal) storedValueForKey(EOTrancheBudgetDepCP.MONTANT_CP_KEY);
  }

  public void setMontantCp(java.math.BigDecimal value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepCP.LOG.debug( "updating montantCp from " + montantCp() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.MONTANT_CP_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOTrancheBudgetDepCP.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepCP.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModif() {
    return (Integer) storedValueForKey(EOTrancheBudgetDepCP.PERS_ID_MODIF_KEY);
  }

  public void setPersIdModif(Integer value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
        EOTrancheBudgetDepCP.LOG.debug( "updating persIdModif from " + persIdModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.PERS_ID_MODIF_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(EOTrancheBudgetDepCP.DESTINATION_DEPENSE_KEY);
  }
  
  public void setDestinationDepense(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.DESTINATION_DEPENSE_KEY);
  }

  public void setDestinationDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("updating destinationDepense from " + destinationDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setDestinationDepense(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = destinationDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepCP.DESTINATION_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepCP.DESTINATION_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire enveloppeBudgetaire() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire)storedValueForKey(EOTrancheBudgetDepCP.ENVELOPPE_BUDGETAIRE_KEY);
  }
  
  public void setEnveloppeBudgetaire(org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.ENVELOPPE_BUDGETAIRE_KEY);
  }

  public void setEnveloppeBudgetaireRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("updating enveloppeBudgetaire from " + enveloppeBudgetaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEnveloppeBudgetaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire oldValue = enveloppeBudgetaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepCP.ENVELOPPE_BUDGETAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepCP.ENVELOPPE_BUDGETAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EONatureDep natureDepense() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EONatureDep)storedValueForKey(EOTrancheBudgetDepCP.NATURE_DEPENSE_KEY);
  }
  
  public void setNatureDepense(org.cocktail.fwkcktlgfceos.server.metier.EONatureDep value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.NATURE_DEPENSE_KEY);
  }

  public void setNatureDepenseRelationship(org.cocktail.fwkcktlgfceos.server.metier.EONatureDep value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("updating natureDepense from " + natureDepense() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setNatureDepense(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EONatureDep oldValue = natureDepense();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepCP.NATURE_DEPENSE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepCP.NATURE_DEPENSE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOTrancheBudgetDepCP.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepCP.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepCP.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOTrancheBudgetDepCP.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOTrancheBudgetDepCP.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOTrancheBudgetDepCP.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOTrancheBudgetDepCP.TRANCHE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> budgetPrevisionOperationTrancheDepenseCps() {
    return (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp>)storedValueForKey(EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> budgetPrevisionOperationTrancheDepenseCps(EOQualifier qualifier) {
    return budgetPrevisionOperationTrancheDepenseCps(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> budgetPrevisionOperationTrancheDepenseCps(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> results;
      results = budgetPrevisionOperationTrancheDepenseCps();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToBudgetPrevisionOperationTrancheDepenseCps(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp object) {
    includeObjectIntoPropertyWithKey(object, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
  }

  public void removeFromBudgetPrevisionOperationTrancheDepenseCps(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp object) {
    excludeObjectFromPropertyWithKey(object, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
  }

  public void addToBudgetPrevisionOperationTrancheDepenseCpsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp object) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("adding " + object + " to budgetPrevisionOperationTrancheDepenseCps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToBudgetPrevisionOperationTrancheDepenseCps(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
    }
  }

  public void removeFromBudgetPrevisionOperationTrancheDepenseCpsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp object) {
    if (EOTrancheBudgetDepCP.LOG.isDebugEnabled()) {
      EOTrancheBudgetDepCP.LOG.debug("removing " + object + " from budgetPrevisionOperationTrancheDepenseCps relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromBudgetPrevisionOperationTrancheDepenseCps(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp createBudgetPrevisionOperationTrancheDepenseCpsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
    return (org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp) eo;
  }

  public void deleteBudgetPrevisionOperationTrancheDepenseCpsRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOTrancheBudgetDepCP.BUDGET_PREVISION_OPERATION_TRANCHE_DEPENSE_CPS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllBudgetPrevisionOperationTrancheDepenseCpsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfceos.server.metier.EOBudgetPrevisionOperationTrancheDepenseCp> objects = budgetPrevisionOperationTrancheDepenseCps().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteBudgetPrevisionOperationTrancheDepenseCpsRelationship(objects.nextElement());
    }
  }


  public static TrancheBudgetDepCP create(EOEditingContext editingContext, NSTimestamp dateCreation
, java.math.BigDecimal montantCp
, Integer persIdCreation
, org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense destinationDepense, org.cocktail.fwkcktlgfceos.server.metier.EOEnveloppeBudgetaire enveloppeBudgetaire, org.cocktail.fwkcktlgfceos.server.metier.EONatureDep natureDepense, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    TrancheBudgetDepCP eo = (TrancheBudgetDepCP) EOUtilities.createAndInsertInstance(editingContext, EOTrancheBudgetDepCP.ENTITY_NAME);    
        eo.setDateCreation(dateCreation);
        eo.setMontantCp(montantCp);
        eo.setPersIdCreation(persIdCreation);
    eo.setDestinationDepenseRelationship(destinationDepense);
    eo.setEnveloppeBudgetaireRelationship(enveloppeBudgetaire);
    eo.setNatureDepenseRelationship(natureDepense);
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<TrancheBudgetDepCP> fetchSpec() {
    return new ERXFetchSpecification<TrancheBudgetDepCP>(EOTrancheBudgetDepCP.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TrancheBudgetDepCP> fetchAll(EOEditingContext editingContext) {
    return EOTrancheBudgetDepCP.fetchAll(editingContext, null);
  }

  public static NSArray<TrancheBudgetDepCP> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTrancheBudgetDepCP.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TrancheBudgetDepCP> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TrancheBudgetDepCP> fetchSpec = new ERXFetchSpecification<TrancheBudgetDepCP>(EOTrancheBudgetDepCP.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TrancheBudgetDepCP> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TrancheBudgetDepCP fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetDepCP.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetDepCP fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TrancheBudgetDepCP> eoObjects = EOTrancheBudgetDepCP.fetchAll(editingContext, qualifier, null);
    TrancheBudgetDepCP eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTrancheBudgetDepCP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetDepCP fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTrancheBudgetDepCP.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TrancheBudgetDepCP fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TrancheBudgetDepCP eoObject = EOTrancheBudgetDepCP.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTrancheBudgetDepCP that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TrancheBudgetDepCP localInstanceIn(EOEditingContext editingContext, TrancheBudgetDepCP eo) {
    TrancheBudgetDepCP localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}