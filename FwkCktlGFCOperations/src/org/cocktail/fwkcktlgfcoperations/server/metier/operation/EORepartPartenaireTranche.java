// DO NOT EDIT.  Make changes to RepartPartenaireTranche.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartPartenaireTranche extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartPartenaireTranche";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_REPART_PARTENAIRE";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> MONTANT_PARTICIPATION = new ERXKey<java.math.BigDecimal>("montantParticipation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> FRAIS_GESTIONS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion>("fraisGestions");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> OPERATION_PARTENAIRE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>("operationPartenaire");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String MONTANT_PARTICIPATION_KEY = MONTANT_PARTICIPATION.key();
  // Relationships
  public static final String FRAIS_GESTIONS_KEY = FRAIS_GESTIONS.key();
  public static final String OPERATION_PARTENAIRE_KEY = OPERATION_PARTENAIRE.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EORepartPartenaireTranche.class);

  public RepartPartenaireTranche localInstanceIn(EOEditingContext editingContext) {
    RepartPartenaireTranche localInstance = (RepartPartenaireTranche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal montantParticipation() {
    return (java.math.BigDecimal) storedValueForKey(EORepartPartenaireTranche.MONTANT_PARTICIPATION_KEY);
  }

  public void setMontantParticipation(java.math.BigDecimal value) {
    if (EORepartPartenaireTranche.LOG.isDebugEnabled()) {
        EORepartPartenaireTranche.LOG.debug( "updating montantParticipation from " + montantParticipation() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartPartenaireTranche.MONTANT_PARTICIPATION_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire operationPartenaire() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire)storedValueForKey(EORepartPartenaireTranche.OPERATION_PARTENAIRE_KEY);
  }
  
  public void setOperationPartenaire(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire value) {
    takeStoredValueForKey(value, EORepartPartenaireTranche.OPERATION_PARTENAIRE_KEY);
  }

  public void setOperationPartenaireRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire value) {
    if (EORepartPartenaireTranche.LOG.isDebugEnabled()) {
      EORepartPartenaireTranche.LOG.debug("updating operationPartenaire from " + operationPartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOperationPartenaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire oldValue = operationPartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartPartenaireTranche.OPERATION_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartPartenaireTranche.OPERATION_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EORepartPartenaireTranche.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EORepartPartenaireTranche.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EORepartPartenaireTranche.LOG.isDebugEnabled()) {
      EORepartPartenaireTranche.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartPartenaireTranche.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartPartenaireTranche.TRANCHE_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> fraisGestions() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion>)storedValueForKey(EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> fraisGestions(EOQualifier qualifier) {
    return fraisGestions(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> fraisGestions(EOQualifier qualifier, boolean fetch) {
    return fraisGestions(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> fraisGestions(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion.REPART_PARTENAIRE_TRANCHE_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = fraisGestions();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFraisGestions(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion object) {
    includeObjectIntoPropertyWithKey(object, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
  }

  public void removeFromFraisGestions(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion object) {
    excludeObjectFromPropertyWithKey(object, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
  }

  public void addToFraisGestionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion object) {
    if (EORepartPartenaireTranche.LOG.isDebugEnabled()) {
      EORepartPartenaireTranche.LOG.debug("adding " + object + " to fraisGestions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToFraisGestions(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
    }
  }

  public void removeFromFraisGestionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion object) {
    if (EORepartPartenaireTranche.LOG.isDebugEnabled()) {
      EORepartPartenaireTranche.LOG.debug("removing " + object + " from fraisGestions relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromFraisGestions(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion createFraisGestionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion) eo;
  }

  public void deleteFraisGestionsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EORepartPartenaireTranche.FRAIS_GESTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFraisGestionsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FraisGestion> objects = fraisGestions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFraisGestionsRelationship(objects.nextElement());
    }
  }


  public static RepartPartenaireTranche create(EOEditingContext editingContext, java.math.BigDecimal montantParticipation
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire operationPartenaire, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    RepartPartenaireTranche eo = (RepartPartenaireTranche) EOUtilities.createAndInsertInstance(editingContext, EORepartPartenaireTranche.ENTITY_NAME);    
        eo.setMontantParticipation(montantParticipation);
    eo.setOperationPartenaireRelationship(operationPartenaire);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<RepartPartenaireTranche> fetchSpec() {
    return new ERXFetchSpecification<RepartPartenaireTranche>(EORepartPartenaireTranche.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartPartenaireTranche> fetchAll(EOEditingContext editingContext) {
    return EORepartPartenaireTranche.fetchAll(editingContext, null);
  }

  public static NSArray<RepartPartenaireTranche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartPartenaireTranche.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartPartenaireTranche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartPartenaireTranche> fetchSpec = new ERXFetchSpecification<RepartPartenaireTranche>(EORepartPartenaireTranche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartPartenaireTranche> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartPartenaireTranche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartPartenaireTranche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartPartenaireTranche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartPartenaireTranche> eoObjects = EORepartPartenaireTranche.fetchAll(editingContext, qualifier, null);
    RepartPartenaireTranche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartPartenaireTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartPartenaireTranche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartPartenaireTranche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartPartenaireTranche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartPartenaireTranche eoObject = EORepartPartenaireTranche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartPartenaireTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartPartenaireTranche localInstanceIn(EOEditingContext editingContext, RepartPartenaireTranche eo) {
    RepartPartenaireTranche localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}