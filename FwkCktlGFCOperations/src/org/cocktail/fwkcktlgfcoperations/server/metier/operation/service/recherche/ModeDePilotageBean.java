package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import lombok.Getter;

@Getter
public class ModeDePilotageBean {

	public static final String CODE_MODE_PILOTAGE = "codeModePilotage";
	
	private String codeModePilotage;
	private String llModePilotage;
	
	public ModeDePilotageBean(String codeModePilotage, String llModePilotage) {
        this.codeModePilotage = codeModePilotage;
        this.llModePilotage = llModePilotage;
    }

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((codeModePilotage == null) ? 0 : codeModePilotage.hashCode());
		result = prime * result
				+ ((llModePilotage == null) ? 0 : llModePilotage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ModeDePilotageBean other = (ModeDePilotageBean) obj;
		if (codeModePilotage == null) {
			if (other.codeModePilotage != null)
				return false;
		} else if (!codeModePilotage.equals(other.codeModePilotage))
			return false;
		if (llModePilotage == null) {
			if (other.llModePilotage != null)
				return false;
		} else if (!llModePilotage.equals(other.llModePilotage))
			return false;
		return true;
	}
}
