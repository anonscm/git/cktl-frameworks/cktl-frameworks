/*
 * Copyright COCKTAIL (www.cocktail.org), 2001, 2010 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionApplication;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionProcedure;
import org.cocktail.fwkcktlgfcoperations.common.nombre.NombreOperation;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperations;
import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsParamManager;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suivirecette.ProcedureGetTotalRecette;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes.BudgetCombinaisonAxesRecette;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation.ContratFinancementRecettesValidation;
import org.cocktail.fwkcktlwebapp.server.CktlParamManager;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;
import er.extensions.foundation.ERXArrayUtilities;
import er.extensions.validation.ERXValidationFactory;

/**
 * Détail de recette pour une tranche d'opération
 * @author bourges
 *
 */
public class TrancheBudgetRec extends EOTrancheBudgetRec {

    public static final EOQualifier QUAL_NON_SUPPR = ERXQ.equals(TrancheBudgetRec.TBR_SUPPR_KEY, "N");
    private static final Logger LOG = Logger.getLogger(TrancheBudgetRec.class);
    
    public TrancheBudgetRec() {
        super();
    }
    
    /**
     * @param tranche
     * @param exericeOuvertCourant
     * @param axes
     * @param commentaire
     * @param montantTitre
     * @param montantBudgetaire
     * @param persId
     * @return {@link TrancheBudgetRec} créé
     */
    public static TrancheBudgetRec creer(Tranche tranche, EOExercice exericeOuvertCourant, BudgetCombinaisonAxesRecette axes, String commentaire,
            BigDecimal montantTitre, BigDecimal montantBudgetaire, Integer persId) {
        Validate.notNull(tranche, "La tranche est obligatoire");

        TrancheBudgetRec trancheBudgetRec = EOTrancheBudgetRec.create(tranche.editingContext(), null, null, null, null, null, null, null, null, null);
        trancheBudgetRec.setTbrSuppr("N"); 
        trancheBudgetRec.setTrancheRelationship(tranche);
        trancheBudgetRec.setTbrMontant(montantBudgetaire);
        trancheBudgetRec.setTbrMontantTitre(montantTitre);
        trancheBudgetRec.setCommentaire(commentaire);
        trancheBudgetRec.setOrgan(axes.getEb());
        trancheBudgetRec.setNatureRec(axes.getNature());
        trancheBudgetRec.setOrigineRecette(axes.getOrigine());
        trancheBudgetRec.setSectionRecette(axes.getSection());        
        trancheBudgetRec.setTbrDateCreation(new NSTimestamp());
        trancheBudgetRec.setPersIdCreation(persId);
        return trancheBudgetRec;
    }
    
    /**
     * @param trancheBudgetRec
     * @param exericeOuvertCourant
     * @param axes
     * @param commentaire
     * @param montantTitre
     * @param montantBudgetaire
     * @param persId
     * @return {@link TrancheBudgetRec} modifié
     */
    public static TrancheBudgetRec maj(TrancheBudgetRec trancheBudgetRec, BudgetCombinaisonAxesRecette axes, String commentaire,
            BigDecimal montantTitre, BigDecimal montantBudgetaire, Integer persId) {
        Validate.notNull(trancheBudgetRec, "Détail de recette est obligatoire");
        trancheBudgetRec.setTbrSuppr("N"); 
        trancheBudgetRec.setTbrMontant(montantBudgetaire);
        trancheBudgetRec.setTbrMontantTitre(montantTitre);
        trancheBudgetRec.setCommentaire(commentaire);
        trancheBudgetRec.setOrgan(axes.getEb());
        trancheBudgetRec.setNatureRec(axes.getNature());
        trancheBudgetRec.setOrigineRecette(axes.getOrigine());
        trancheBudgetRec.setSectionRecette(axes.getSection());        
        trancheBudgetRec.setTbrDateModif(new NSTimestamp());
        trancheBudgetRec.setPersIdModification(persId);
        return trancheBudgetRec;
    }
    
    @Deprecated
    public static void supprimerLigneCreditRecette(EOEditingContext ec, EOExercice exericeOuvertCourant, VCreditsPositionnesRec creditsRecettesPos) {
        EOExerciceCocktail exerciceSelectionne = creditsRecettesPos.tranche().exerciceCocktail();
        // On va chercher la tranche budget correspondante
        EOQualifier qual = TrancheBudgetRec.ORGAN.eq(creditsRecettesPos.organ())
        		// FIXME Ajouter nature/section/origine ? OUI si cette méthode est encore utilisée, si non, on jette tout
        		// .and(TrancheBudgetRec.TYPE_CREDIT.eq(creditsRecettesPos.typeCredit()))
        		.and(TrancheBudgetRec.TRANCHE.eq(creditsRecettesPos.tranche()))
        		.and(TrancheBudgetRec.QUAL_NON_SUPPR);
        
        NSArray<TrancheBudgetRec> tranchesBudgetRec = TrancheBudgetRec.fetchAll(ec, qual, null);
        if (tranchesBudgetRec.count() < 1)
            throw new IllegalStateException("Aucune TrancheBudgetRec correspondant au cr\u00E9dit positionn\u00E9 " +
                                            "n'a \u00E9t\u00E9 trouv\u00E9.");
        else if (tranchesBudgetRec.count() > 1)
            throw new IllegalStateException("Plusieurs TrancheBudgetRec correspondants au cr\u00E9dit positionn\u00E9 " +
                                            "ont \u00E9t\u00E9 trouv\u00E9s.");
        TrancheBudgetRec trancheBudgetRec = tranchesBudgetRec.lastObject();
        supprimerTrancheBudgetRec(ec, trancheBudgetRec, creditsRecettesPos);
    }

    public static void supprimerTrancheBudgetRec(EOEditingContext ec, TrancheBudgetRec trancheBudgetRec, VCreditsPositionnesRec creditsRecettesPosRec) {
        trancheBudgetRec.setTbrMontant(BigDecimal.valueOf(0));
        trancheBudgetRec.setTbrSuppr("O");
        trancheBudgetRec.setTbrDateModif(new NSTimestamp());
    }
    

    /**
     * <p>Fournit le total des depenses existantes sur les memes exercice, convention, type de credit et LB que cet objet. 
     * <p>Ce total des depenses est : total restant engage + total liquide.
     * <p>Fait appel a une procedure stoquee.
     * @return Le total des depenses ou <code>null</code> si probleme.
     * @throws ExceptionApplication 
     */
    public BigDecimal totalRecettes() {
        BigDecimal totalRecette=null;
        if (!isNewObject()) {
            try {
                Number exeOrdre = (Number)ERXEOControlUtilities.primaryKeyObjectForObject(tranche().exercice());
                Number conOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(tranche().operation());
//                Number tcdOrdre = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(typeCredit());
                Number tcdOrdre = null;
                Number orgId = (Number) ERXEOControlUtilities.primaryKeyObjectForObject(organ());

                totalRecette = new ProcedureGetTotalRecette(editingContext()).execute(
                                exeOrdre, 
                                conOrdre, 
                                tcdOrdre, 
                                orgId);
            } 
            catch (ExceptionProcedure e) {
                throw new RuntimeException(
                                "Une erreur est survenue lors de l'appel \u00E0 la procedure stoqu\u00E9e :\n" +
                                                e.getMessage(), e);
            }
            catch (Exception e) {
                throw new RuntimeException(
                                "Impossible d'obtenir la cl\u00E9 primaire d'un objet :\n" +
                                                e.getMessage(), e);
            }
        }
        return totalRecette;
    }
    
    public BigDecimal totalTrancheRecettes() {
    	return tbrMontant().add(tbrMontantTitre());
    }

    /**
     * Fournit le "reste a recetter" par rapport au total positionne, 
     * cad la difference entre le total positionne et le total des recettes existantes.
     * @see #totalRecettes()
     * @return Le montant restant a recetter ou <code>null</code> si probleme lors de l'obtention du total des recettes.
     * @throws ExceptionApplication 
     */
    public BigDecimal resteARecetter() {
        BigDecimal totalRecettes = totalRecettes();
        if (totalRecettes != null)
            return tbrMontant().subtract(totalRecettes);
        else
            return null;
    }
    
    public BudgetCombinaisonAxesRecette combinaisonAxes() {
    	return BudgetCombinaisonAxesRecette.builder()
    			.eb(organ())
    			.nature(natureRec())
    			.origine(origineRecette())
    			.section(sectionRecette())
    			.build();
    }
    
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (!NombreOperation.montantEstPositif(tbrMontantTitre())) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, TBR_MONTANT_TITRE_KEY, tbrMontantTitre(), "tbrMontantTitrePositif");
        }
        if (!NombreOperation.montantEstPositif(tbrMontant())) {
            throw ERXValidationFactory.defaultFactory().createCustomException(this, TBR_MONTANT_KEY, tbrMontant(), "tbrMontantPositif");
        }
        
        // Si une convention RA 
        // Si le crédit positionné dépasse le montant de la tranche
        if (tranche().operation().isModeRA()) {
          if (tbrMontant().compareTo(tranche().montant()) == 1) {
              throw ERXValidationFactory.defaultFactory().createCustomException(this, TBR_MONTANT_KEY, tbrMontant(), "MontantTooBig");
          }
        } else {
            ContratFinancementRecettesValidation financementValidation = new ContratFinancementRecettesValidation();
			if (!financementValidation.isSatisfiedBy(tranche().operation())) {
				throw ERXValidationFactory.defaultFactory().createCustomException(this, "MontantContributionsTropBasPourTotalPositionne");
			}
        }
        // Si on est dans le cadre d'une convention RA, on vérifie qu'il n'y ait pas déjà une trancheBudget sur un autre
        // organ
        // Si multi-ligne par conv. RA non autorisé
        String isMultiLBAutorise = FwkCktlGFCOperations.paramManager.getParam(FwkCktlGFCOperationsParamManager.MULTI_LB_POUR_CONV_RA_AUTORISE);
		if (!CktlParamManager.booleanForValue(isMultiLBAutorise)) {
	        if (tranche().operation().isModeRA()) {
	            NSArray<EOEb> allOrgansRec = (NSArray<EOEb>) tranche().trancheBudgetsRecNonSupprimes().valueForKey(ORGAN_KEY);
	            allOrgansRec = ERXArrayUtilities.arrayWithoutDuplicates(allOrgansRec);
	            if (allOrgansRec.count() > 1) {
	                throw ERXValidationFactory.defaultFactory().createCustomException(this, "MultipleOrgansRA");
	            }
	        }
        }
    }
}
