// DO NOT EDIT.  Make changes to SbDepense.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOSbDepense extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeSbDepense";
	public static final String ENTITY_TABLE_NAME = "GFC.SB_DEPENSE";

  // Attribute Keys
  public static final ERXKey<Long> CAN_ID = new ERXKey<Long>("canId");
  public static final ERXKey<Integer> CE_ORDRE = new ERXKey<Integer>("ceOrdre");
  public static final ERXKey<Integer> FOURNIS_PERS_ID = new ERXKey<Integer>("fournisPersId");
  public static final ERXKey<String> SD_AUTO = new ERXKey<String>("sdAuto");
  public static final ERXKey<NSTimestamp> SD_DATE_PREV = new ERXKey<NSTimestamp>("sdDatePrev");
  public static final ERXKey<Integer> SD_ETAT = new ERXKey<Integer>("sdEtat");
  public static final ERXKey<String> SD_LIBELLE = new ERXKey<String>("sdLibelle");
  public static final ERXKey<java.math.BigDecimal> SD_MONTANT_HT = new ERXKey<java.math.BigDecimal>("sdMontantHt");
  public static final ERXKey<Integer> SDT_ORDRE = new ERXKey<Integer>("sdtOrdre");
  public static final ERXKey<Integer> SR_ORDRE_OWNER = new ERXKey<Integer>("srOrdreOwner");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense> LOLF = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense>("lolf");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organ");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette> SB_RECETTE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette>("sbRecette");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTva> TVA = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTva>("tva");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit> TYPE_CREDIT = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit>("typeCredit");

  // Attributes
  public static final String CAN_ID_KEY = CAN_ID.key();
  public static final String CE_ORDRE_KEY = CE_ORDRE.key();
  public static final String FOURNIS_PERS_ID_KEY = FOURNIS_PERS_ID.key();
  public static final String SD_AUTO_KEY = SD_AUTO.key();
  public static final String SD_DATE_PREV_KEY = SD_DATE_PREV.key();
  public static final String SD_ETAT_KEY = SD_ETAT.key();
  public static final String SD_LIBELLE_KEY = SD_LIBELLE.key();
  public static final String SD_MONTANT_HT_KEY = SD_MONTANT_HT.key();
  public static final String SDT_ORDRE_KEY = SDT_ORDRE.key();
  public static final String SR_ORDRE_OWNER_KEY = SR_ORDRE_OWNER.key();
  // Relationships
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String LOLF_KEY = LOLF.key();
  public static final String ORGAN_KEY = ORGAN.key();
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String SB_RECETTE_KEY = SB_RECETTE.key();
  public static final String TRANCHE_KEY = TRANCHE.key();
  public static final String TVA_KEY = TVA.key();
  public static final String TYPE_CREDIT_KEY = TYPE_CREDIT.key();

  private static Logger LOG = Logger.getLogger(EOSbDepense.class);

  public SbDepense localInstanceIn(EOEditingContext editingContext) {
    SbDepense localInstance = (SbDepense)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Long canId() {
    return (Long) storedValueForKey(EOSbDepense.CAN_ID_KEY);
  }

  public void setCanId(Long value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating canId from " + canId() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.CAN_ID_KEY);
  }

  public Integer ceOrdre() {
    return (Integer) storedValueForKey(EOSbDepense.CE_ORDRE_KEY);
  }

  public void setCeOrdre(Integer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating ceOrdre from " + ceOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.CE_ORDRE_KEY);
  }

  public Integer fournisPersId() {
    return (Integer) storedValueForKey(EOSbDepense.FOURNIS_PERS_ID_KEY);
  }

  public void setFournisPersId(Integer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating fournisPersId from " + fournisPersId() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.FOURNIS_PERS_ID_KEY);
  }

  public String sdAuto() {
    return (String) storedValueForKey(EOSbDepense.SD_AUTO_KEY);
  }

  public void setSdAuto(String value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdAuto from " + sdAuto() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SD_AUTO_KEY);
  }

  public NSTimestamp sdDatePrev() {
    return (NSTimestamp) storedValueForKey(EOSbDepense.SD_DATE_PREV_KEY);
  }

  public void setSdDatePrev(NSTimestamp value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdDatePrev from " + sdDatePrev() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SD_DATE_PREV_KEY);
  }

  public Integer sdEtat() {
    return (Integer) storedValueForKey(EOSbDepense.SD_ETAT_KEY);
  }

  public void setSdEtat(Integer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdEtat from " + sdEtat() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SD_ETAT_KEY);
  }

  public String sdLibelle() {
    return (String) storedValueForKey(EOSbDepense.SD_LIBELLE_KEY);
  }

  public void setSdLibelle(String value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdLibelle from " + sdLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SD_LIBELLE_KEY);
  }

  public java.math.BigDecimal sdMontantHt() {
    return (java.math.BigDecimal) storedValueForKey(EOSbDepense.SD_MONTANT_HT_KEY);
  }

  public void setSdMontantHt(java.math.BigDecimal value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdMontantHt from " + sdMontantHt() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SD_MONTANT_HT_KEY);
  }

  public Integer sdtOrdre() {
    return (Integer) storedValueForKey(EOSbDepense.SDT_ORDRE_KEY);
  }

  public void setSdtOrdre(Integer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating sdtOrdre from " + sdtOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SDT_ORDRE_KEY);
  }

  public Integer srOrdreOwner() {
    return (Integer) storedValueForKey(EOSbDepense.SR_ORDRE_OWNER_KEY);
  }

  public void setSrOrdreOwner(Integer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
        EOSbDepense.LOG.debug( "updating srOrdreOwner from " + srOrdreOwner() + " to " + value);
    }
    takeStoredValueForKey(value, EOSbDepense.SR_ORDRE_OWNER_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail)storedValueForKey(EOSbDepense.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOSbDepense.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense lolf() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense)storedValueForKey(EOSbDepense.LOLF_KEY);
  }
  
  public void setLolf(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    takeStoredValueForKey(value, EOSbDepense.LOLF_KEY);
  }

  public void setLolfRelationship(org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating lolf from " + lolf() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setLolf(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense oldValue = lolf();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.LOLF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.LOLF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organ() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOSbDepense.ORGAN_KEY);
  }
  
  public void setOrgan(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOSbDepense.ORGAN_KEY);
  }

  public void setOrganRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating organ from " + organ() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrgan(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organ();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.ORGAN_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer)storedValueForKey(EOSbDepense.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOSbDepense.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette sbRecette() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette)storedValueForKey(EOSbDepense.SB_RECETTE_KEY);
  }
  
  public void setSbRecette(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    takeStoredValueForKey(value, EOSbDepense.SB_RECETTE_KEY);
  }

  public void setSbRecetteRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating sbRecette from " + sbRecette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setSbRecette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.SbRecette oldValue = sbRecette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.SB_RECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.SB_RECETTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOSbDepense.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOSbDepense.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.TRANCHE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTva tva() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTva)storedValueForKey(EOSbDepense.TVA_KEY);
  }
  
  public void setTva(org.cocktail.fwkcktlgfceos.server.metier.EOTva value) {
    takeStoredValueForKey(value, EOSbDepense.TVA_KEY);
  }

  public void setTvaRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTva value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating tva from " + tva() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTva(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTva oldValue = tva();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.TVA_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.TVA_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit)storedValueForKey(EOSbDepense.TYPE_CREDIT_KEY);
  }
  
  public void setTypeCredit(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    takeStoredValueForKey(value, EOSbDepense.TYPE_CREDIT_KEY);
  }

  public void setTypeCreditRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit value) {
    if (EOSbDepense.LOG.isDebugEnabled()) {
      EOSbDepense.LOG.debug("updating typeCredit from " + typeCredit() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeCredit(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit oldValue = typeCredit();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOSbDepense.TYPE_CREDIT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOSbDepense.TYPE_CREDIT_KEY);
    }
  }
  

  public static SbDepense create(EOEditingContext editingContext, String sdLibelle
, java.math.BigDecimal sdMontantHt
, org.cocktail.fwkcktlgfceos.server.metier.EOEb organ, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche, org.cocktail.fwkcktlgfceos.server.metier.EOTva tva) {
    SbDepense eo = (SbDepense) EOUtilities.createAndInsertInstance(editingContext, EOSbDepense.ENTITY_NAME);    
        eo.setSdLibelle(sdLibelle);
        eo.setSdMontantHt(sdMontantHt);
    eo.setOrganRelationship(organ);
    eo.setTrancheRelationship(tranche);
    eo.setTvaRelationship(tva);
    return eo;
  }

  public static ERXFetchSpecification<SbDepense> fetchSpec() {
    return new ERXFetchSpecification<SbDepense>(EOSbDepense.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<SbDepense> fetchAll(EOEditingContext editingContext) {
    return EOSbDepense.fetchAll(editingContext, null);
  }

  public static NSArray<SbDepense> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOSbDepense.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<SbDepense> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<SbDepense> fetchSpec = new ERXFetchSpecification<SbDepense>(EOSbDepense.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<SbDepense> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static SbDepense fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOSbDepense.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SbDepense fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<SbDepense> eoObjects = EOSbDepense.fetchAll(editingContext, qualifier, null);
    SbDepense eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeSbDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SbDepense fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOSbDepense.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static SbDepense fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    SbDepense eoObject = EOSbDepense.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeSbDepense that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static SbDepense localInstanceIn(EOEditingContext editingContext, SbDepense eo) {
    SbDepense localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}