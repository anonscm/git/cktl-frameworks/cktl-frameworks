package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

public interface LigneDepenseCPSurCombinaisonAxes {

    BigDecimal montantAE();
    BigDecimal montantCP();
    void setMontantCP(BigDecimal value);
    boolean isPrevisionnel();
    boolean isMontantCPMisAJour();
    boolean isMontantCPEfface();
    boolean isNouvelleSaisie();
    Tranche tranche();
}
