package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.apache.log4j.Logger;

public class FluxDepenseRececette extends EOFluxDepenseRececette {
	

	public static final String CODE_FLUX_DEPENSES = "DEP";
	public static final String CODE_FLUX_RECETTES = "REC";
	public static final String CODE_FLUX_DEPENSES_RECETTES = "DEPREC";
	
	private static final long serialVersionUID = 1L;
	
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(FluxDepenseRececette.class);
}
