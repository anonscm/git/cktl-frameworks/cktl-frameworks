// DO NOT EDIT.  Make changes to CategorieOperation.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOCategorieOperation extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeCategOperation";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_CATEG_OPERATION";

  // Attribute Keys
  public static final ERXKey<String> CODE_CATEGORIE = new ERXKey<String>("codeCategorie");
  public static final ERXKey<String> LIBELLE_LONG = new ERXKey<String>("libelleLong");
  // Relationship Keys

  // Attributes
  public static final String CODE_CATEGORIE_KEY = CODE_CATEGORIE.key();
  public static final String LIBELLE_LONG_KEY = LIBELLE_LONG.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOCategorieOperation.class);

  public CategorieOperation localInstanceIn(EOEditingContext editingContext) {
    CategorieOperation localInstance = (CategorieOperation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String codeCategorie() {
    return (String) storedValueForKey(EOCategorieOperation.CODE_CATEGORIE_KEY);
  }

  public void setCodeCategorie(String value) {
    if (EOCategorieOperation.LOG.isDebugEnabled()) {
        EOCategorieOperation.LOG.debug( "updating codeCategorie from " + codeCategorie() + " to " + value);
    }
    takeStoredValueForKey(value, EOCategorieOperation.CODE_CATEGORIE_KEY);
  }

  public String libelleLong() {
    return (String) storedValueForKey(EOCategorieOperation.LIBELLE_LONG_KEY);
  }

  public void setLibelleLong(String value) {
    if (EOCategorieOperation.LOG.isDebugEnabled()) {
        EOCategorieOperation.LOG.debug( "updating libelleLong from " + libelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EOCategorieOperation.LIBELLE_LONG_KEY);
  }


  public static CategorieOperation create(EOEditingContext editingContext, String codeCategorie
, String libelleLong
) {
    CategorieOperation eo = (CategorieOperation) EOUtilities.createAndInsertInstance(editingContext, EOCategorieOperation.ENTITY_NAME);    
        eo.setCodeCategorie(codeCategorie);
        eo.setLibelleLong(libelleLong);
    return eo;
  }

  public static ERXFetchSpecification<CategorieOperation> fetchSpec() {
    return new ERXFetchSpecification<CategorieOperation>(EOCategorieOperation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<CategorieOperation> fetchAll(EOEditingContext editingContext) {
    return EOCategorieOperation.fetchAll(editingContext, null);
  }

  public static NSArray<CategorieOperation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOCategorieOperation.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<CategorieOperation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<CategorieOperation> fetchSpec = new ERXFetchSpecification<CategorieOperation>(EOCategorieOperation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<CategorieOperation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static CategorieOperation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOCategorieOperation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static CategorieOperation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<CategorieOperation> eoObjects = EOCategorieOperation.fetchAll(editingContext, qualifier, null);
    CategorieOperation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeCategOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static CategorieOperation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOCategorieOperation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static CategorieOperation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    CategorieOperation eoObject = EOCategorieOperation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeCategOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static CategorieOperation localInstanceIn(EOEditingContext editingContext, CategorieOperation eo) {
    CategorieOperation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}