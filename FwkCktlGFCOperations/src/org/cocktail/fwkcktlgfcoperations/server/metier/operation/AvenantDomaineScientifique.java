package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;

public class AvenantDomaineScientifique extends EOAvenantDomaineScientifique {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(AvenantDomaineScientifique.class);
    
    public void remove(EODomaineScientifique domaine) {
    	removeObjectFromBothSidesOfRelationshipWithKey(domaine, EOAvenantDomaineScientifique.DOMAINE_SCIENTIFIQUE_KEY);
    }
    
}
