package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.apache.log4j.Logger;

public class TypeOperation extends EOTypeOperation {
	
	public static final String OP_INVESTISSEMENT = "INV";
	public static final String OP_RECHERCHE = "REC";
	public static final String OP_FORMATION_CONTINUE = "FOR";
	public static final String OP_ENSEIGNEMENT = "ENS";
	public static final String OP_AUTRES_AVEC_IMPACT_FINANCIER = "AUT";
	
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(TypeOperation.class);
}
