package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEnterpriseObject;

public interface IRepartMontant extends EOEnterpriseObject {

    public BigDecimal montantHt();
    public void setMontantHt(BigDecimal value);
    public EOEnterpriseObject nomenclature();
    public void setNomenclature(EOEnterpriseObject value);
    
}
