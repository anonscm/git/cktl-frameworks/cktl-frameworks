// DO NOT EDIT.  Make changes to VRecettesTranche.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOVRecettesTranche extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeVRecettesTranche";
	public static final String ENTITY_TABLE_NAME = "GFC.V_RECETTES_TRANCHE";

  // Attribute Keys
  public static final ERXKey<Integer> EXE_ORDRE = new ERXKey<Integer>("exeOrdre");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer> PLANCO = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer>("planco");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranche");

  // Attributes
  public static final String EXE_ORDRE_KEY = EXE_ORDRE.key();
  public static final String MONTANT_KEY = MONTANT.key();
  // Relationships
  public static final String PLANCO_KEY = PLANCO.key();
  public static final String TRANCHE_KEY = TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOVRecettesTranche.class);

  public VRecettesTranche localInstanceIn(EOEditingContext editingContext) {
    VRecettesTranche localInstance = (VRecettesTranche)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer exeOrdre() {
    return (Integer) storedValueForKey(EOVRecettesTranche.EXE_ORDRE_KEY);
  }

  public void setExeOrdre(Integer value) {
    if (EOVRecettesTranche.LOG.isDebugEnabled()) {
        EOVRecettesTranche.LOG.debug( "updating exeOrdre from " + exeOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOVRecettesTranche.EXE_ORDRE_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(EOVRecettesTranche.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (EOVRecettesTranche.LOG.isDebugEnabled()) {
        EOVRecettesTranche.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, EOVRecettesTranche.MONTANT_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer planco() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer)storedValueForKey(EOVRecettesTranche.PLANCO_KEY);
  }
  
  public void setPlanco(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    takeStoredValueForKey(value, EOVRecettesTranche.PLANCO_KEY);
  }

  public void setPlancoRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer value) {
    if (EOVRecettesTranche.LOG.isDebugEnabled()) {
      EOVRecettesTranche.LOG.debug("updating planco from " + planco() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setPlanco(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer oldValue = planco();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVRecettesTranche.PLANCO_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVRecettesTranche.PLANCO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche)storedValueForKey(EOVRecettesTranche.TRANCHE_KEY);
  }
  
  public void setTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    takeStoredValueForKey(value, EOVRecettesTranche.TRANCHE_KEY);
  }

  public void setTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche value) {
    if (EOVRecettesTranche.LOG.isDebugEnabled()) {
      EOVRecettesTranche.LOG.debug("updating tranche from " + tranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche oldValue = tranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOVRecettesTranche.TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOVRecettesTranche.TRANCHE_KEY);
    }
  }
  

  public static VRecettesTranche create(EOEditingContext editingContext, Integer exeOrdre
, org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer planco, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche tranche) {
    VRecettesTranche eo = (VRecettesTranche) EOUtilities.createAndInsertInstance(editingContext, EOVRecettesTranche.ENTITY_NAME);    
        eo.setExeOrdre(exeOrdre);
    eo.setPlancoRelationship(planco);
    eo.setTrancheRelationship(tranche);
    return eo;
  }

  public static ERXFetchSpecification<VRecettesTranche> fetchSpec() {
    return new ERXFetchSpecification<VRecettesTranche>(EOVRecettesTranche.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<VRecettesTranche> fetchAll(EOEditingContext editingContext) {
    return EOVRecettesTranche.fetchAll(editingContext, null);
  }

  public static NSArray<VRecettesTranche> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOVRecettesTranche.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<VRecettesTranche> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<VRecettesTranche> fetchSpec = new ERXFetchSpecification<VRecettesTranche>(EOVRecettesTranche.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<VRecettesTranche> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static VRecettesTranche fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOVRecettesTranche.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VRecettesTranche fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<VRecettesTranche> eoObjects = EOVRecettesTranche.fetchAll(editingContext, qualifier, null);
    VRecettesTranche eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeVRecettesTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VRecettesTranche fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOVRecettesTranche.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static VRecettesTranche fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    VRecettesTranche eoObject = EOVRecettesTranche.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeVRecettesTranche that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static VRecettesTranche localInstanceIn(EOEditingContext editingContext, VRecettesTranche eo) {
    VRecettesTranche localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}