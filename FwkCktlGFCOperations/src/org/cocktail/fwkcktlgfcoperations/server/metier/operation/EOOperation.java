// DO NOT EDIT.  Make changes to Operation.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOOperation extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeOperation";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_OPERATION";

  // Attribute Keys
  public static final ERXKey<String> ANCIENNE_REFERENCE = new ERXKey<String>("ancienneReference");
  public static final ERXKey<String> AVIS_DEFAVORABLE = new ERXKey<String>("avisDefavorable");
  public static final ERXKey<String> AVIS_FAVORABLE = new ERXKey<String>("avisFavorable");
  public static final ERXKey<NSTimestamp> CON_DATE_APUREMENT = new ERXKey<NSTimestamp>("conDateApurement");
  public static final ERXKey<NSTimestamp> CON_DATE_CLOTURE = new ERXKey<NSTimestamp>("conDateCloture");
  public static final ERXKey<NSTimestamp> CON_DATE_CREATION = new ERXKey<NSTimestamp>("conDateCreation");
  public static final ERXKey<NSTimestamp> CON_DATE_FIN_PAIEMENT = new ERXKey<NSTimestamp>("conDateFinPaiement");
  public static final ERXKey<NSTimestamp> CON_DATE_MODIF = new ERXKey<NSTimestamp>("conDateModif");
  public static final ERXKey<NSTimestamp> CON_DATE_VALID_ADM = new ERXKey<NSTimestamp>("conDateValidAdm");
  public static final ERXKey<Integer> CON_DUREE = new ERXKey<Integer>("conDuree");
  public static final ERXKey<Integer> CON_DUREE_MOIS = new ERXKey<Integer>("conDureeMois");
  public static final ERXKey<String> CON_GROUPE_BUD = new ERXKey<String>("conGroupeBud");
  public static final ERXKey<Integer> CON_INDEX = new ERXKey<Integer>("conIndex");
  public static final ERXKey<Integer> CON_NATURE = new ERXKey<Integer>("conNature");
  public static final ERXKey<String> CON_REFERENCE_EXTERNE = new ERXKey<String>("conReferenceExterne");
  public static final ERXKey<String> CON_SUPPR = new ERXKey<String>("conSuppr");
  public static final ERXKey<String> CONTEXTE = new ERXKey<String>("contexte");
  public static final ERXKey<NSTimestamp> DATE_CREATION = new ERXKey<NSTimestamp>("dateCreation");
  public static final ERXKey<NSTimestamp> DATE_MIGRATION = new ERXKey<NSTimestamp>("dateMigration");
  public static final ERXKey<NSTimestamp> DATE_MODIFICATION = new ERXKey<NSTimestamp>("dateModification");
  public static final ERXKey<String> DESCRIPTION = new ERXKey<String>("description");
  public static final ERXKey<Boolean> EST_CONVENTION_RA = new ERXKey<Boolean>("estConventionRA");
  public static final ERXKey<Boolean> EST_FLECHEE = new ERXKey<Boolean>("estFlechee");
  public static final ERXKey<Boolean> EST_INCLUSE_PPI_ETABLISSEMENT = new ERXKey<Boolean>("estInclusePPIEtablissement");
  public static final ERXKey<Boolean> EST_INCLUSE_TABLEAUX_BUDGETAIRES = new ERXKey<Boolean>("estIncluseTableauxBudgetaires");
  public static final ERXKey<Boolean> EXISTE_CONTRAINTE_ELIGIBILITE_DEPENSES = new ERXKey<Boolean>("existeContrainteEligibiliteDepenses");
  public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
  public static final ERXKey<String> LIBELLE = new ERXKey<String>("libelle");
  public static final ERXKey<String> MOTIFS_AVIS = new ERXKey<String>("motifsAvis");
  public static final ERXKey<Integer> PERS_ID_CREATION = new ERXKey<Integer>("persIdCreation");
  public static final ERXKey<Integer> PERS_ID_MODIFICATION = new ERXKey<Integer>("persIdModification");
  public static final ERXKey<String> REMARQUES = new ERXKey<String>("remarques");
  public static final ERXKey<Integer> TR_ORDRE = new ERXKey<Integer>("trOrdre");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> AVENANTS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>("avenants");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> AXE_STRATEGIQUES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique>("axeStrategiques");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation> CATEGORIE_OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation>("categorieOperation");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> CENTRE_RESPONSABILITE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("centreResponsabilite");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf> CODE_NAF = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EONaf>("codeNaf");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> ETABLISSEMENT = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("etablissement");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette> FLUX_DEPENSE_RECECETTE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette>("fluxDepenseRececette");
  public static final ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure> GROUPE_PARTENAIRE = new ERXKey<org.cocktail.fwkcktlpersonne.common.metier.EOStructure>("groupePartenaire");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> INDICATEURS_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat>("indicateursContrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage> MODE_PILOTAGE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage>("modePilotage");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> OPERATION_PARTENAIRES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>("operationPartenaires");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb> ORGAN_COMPOSANTE = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOEb>("organComposante");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> PROJET_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>("projetContrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> REPART_INDICATEURS_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat>("repartIndicateursContrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> TRANCHES = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>("tranches");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat> TYPE_CLASSIFICATION_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat>("typeClassificationContrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat> TYPE_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat>("typeContrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation> TYPE_OPERATION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation>("typeOperation");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction> TYPE_RECONDUCTION = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction>("typeReconduction");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> UTILISATEUR_CREATION = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur>("utilisateurCreation");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> UTILISATEUR_MODIF = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur>("utilisateurModif");
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur> UTILISATEUR_VALID_ADM = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur>("utilisateurValidAdm");

  // Attributes
  public static final String ANCIENNE_REFERENCE_KEY = ANCIENNE_REFERENCE.key();
  public static final String AVIS_DEFAVORABLE_KEY = AVIS_DEFAVORABLE.key();
  public static final String AVIS_FAVORABLE_KEY = AVIS_FAVORABLE.key();
  public static final String CON_DATE_APUREMENT_KEY = CON_DATE_APUREMENT.key();
  public static final String CON_DATE_CLOTURE_KEY = CON_DATE_CLOTURE.key();
  public static final String CON_DATE_CREATION_KEY = CON_DATE_CREATION.key();
  public static final String CON_DATE_FIN_PAIEMENT_KEY = CON_DATE_FIN_PAIEMENT.key();
  public static final String CON_DATE_MODIF_KEY = CON_DATE_MODIF.key();
  public static final String CON_DATE_VALID_ADM_KEY = CON_DATE_VALID_ADM.key();
  public static final String CON_DUREE_KEY = CON_DUREE.key();
  public static final String CON_DUREE_MOIS_KEY = CON_DUREE_MOIS.key();
  public static final String CON_GROUPE_BUD_KEY = CON_GROUPE_BUD.key();
  public static final String CON_INDEX_KEY = CON_INDEX.key();
  public static final String CON_NATURE_KEY = CON_NATURE.key();
  public static final String CON_REFERENCE_EXTERNE_KEY = CON_REFERENCE_EXTERNE.key();
  public static final String CON_SUPPR_KEY = CON_SUPPR.key();
  public static final String CONTEXTE_KEY = CONTEXTE.key();
  public static final String DATE_CREATION_KEY = DATE_CREATION.key();
  public static final String DATE_MIGRATION_KEY = DATE_MIGRATION.key();
  public static final String DATE_MODIFICATION_KEY = DATE_MODIFICATION.key();
  public static final String DESCRIPTION_KEY = DESCRIPTION.key();
  public static final String EST_CONVENTION_RA_KEY = EST_CONVENTION_RA.key();
  public static final String EST_FLECHEE_KEY = EST_FLECHEE.key();
  public static final String EST_INCLUSE_PPI_ETABLISSEMENT_KEY = EST_INCLUSE_PPI_ETABLISSEMENT.key();
  public static final String EST_INCLUSE_TABLEAUX_BUDGETAIRES_KEY = EST_INCLUSE_TABLEAUX_BUDGETAIRES.key();
  public static final String EXISTE_CONTRAINTE_ELIGIBILITE_DEPENSES_KEY = EXISTE_CONTRAINTE_ELIGIBILITE_DEPENSES.key();
  public static final String ID_OPE_OPERATION_KEY = ID_OPE_OPERATION.key();
  public static final String LIBELLE_KEY = LIBELLE.key();
  public static final String MOTIFS_AVIS_KEY = MOTIFS_AVIS.key();
  public static final String PERS_ID_CREATION_KEY = PERS_ID_CREATION.key();
  public static final String PERS_ID_MODIFICATION_KEY = PERS_ID_MODIFICATION.key();
  public static final String REMARQUES_KEY = REMARQUES.key();
  public static final String TR_ORDRE_KEY = TR_ORDRE.key();
  // Relationships
  public static final String AVENANTS_KEY = AVENANTS.key();
  public static final String AXE_STRATEGIQUES_KEY = AXE_STRATEGIQUES.key();
  public static final String CATEGORIE_OPERATION_KEY = CATEGORIE_OPERATION.key();
  public static final String CENTRE_RESPONSABILITE_KEY = CENTRE_RESPONSABILITE.key();
  public static final String CODE_NAF_KEY = CODE_NAF.key();
  public static final String ETABLISSEMENT_KEY = ETABLISSEMENT.key();
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String FLUX_DEPENSE_RECECETTE_KEY = FLUX_DEPENSE_RECECETTE.key();
  public static final String GROUPE_PARTENAIRE_KEY = GROUPE_PARTENAIRE.key();
  public static final String INDICATEURS_CONTRAT_KEY = INDICATEURS_CONTRAT.key();
  public static final String MODE_PILOTAGE_KEY = MODE_PILOTAGE.key();
  public static final String OPERATION_PARTENAIRES_KEY = OPERATION_PARTENAIRES.key();
  public static final String ORGAN_COMPOSANTE_KEY = ORGAN_COMPOSANTE.key();
  public static final String PROJET_CONTRAT_KEY = PROJET_CONTRAT.key();
  public static final String REPART_INDICATEURS_CONTRAT_KEY = REPART_INDICATEURS_CONTRAT.key();
  public static final String TRANCHES_KEY = TRANCHES.key();
  public static final String TYPE_CLASSIFICATION_CONTRAT_KEY = TYPE_CLASSIFICATION_CONTRAT.key();
  public static final String TYPE_CONTRAT_KEY = TYPE_CONTRAT.key();
  public static final String TYPE_OPERATION_KEY = TYPE_OPERATION.key();
  public static final String TYPE_RECONDUCTION_KEY = TYPE_RECONDUCTION.key();
  public static final String UTILISATEUR_CREATION_KEY = UTILISATEUR_CREATION.key();
  public static final String UTILISATEUR_MODIF_KEY = UTILISATEUR_MODIF.key();
  public static final String UTILISATEUR_VALID_ADM_KEY = UTILISATEUR_VALID_ADM.key();

  private static Logger LOG = Logger.getLogger(EOOperation.class);

  public Operation localInstanceIn(EOEditingContext editingContext) {
    Operation localInstance = (Operation)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String ancienneReference() {
    return (String) storedValueForKey(EOOperation.ANCIENNE_REFERENCE_KEY);
  }

  public void setAncienneReference(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating ancienneReference from " + ancienneReference() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.ANCIENNE_REFERENCE_KEY);
  }

  public String avisDefavorable() {
    return (String) storedValueForKey(EOOperation.AVIS_DEFAVORABLE_KEY);
  }

  public void setAvisDefavorable(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating avisDefavorable from " + avisDefavorable() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.AVIS_DEFAVORABLE_KEY);
  }

  public String avisFavorable() {
    return (String) storedValueForKey(EOOperation.AVIS_FAVORABLE_KEY);
  }

  public void setAvisFavorable(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating avisFavorable from " + avisFavorable() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.AVIS_FAVORABLE_KEY);
  }

  public NSTimestamp conDateApurement() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_APUREMENT_KEY);
  }

  public void setConDateApurement(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateApurement from " + conDateApurement() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_APUREMENT_KEY);
  }

  public NSTimestamp conDateCloture() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_CLOTURE_KEY);
  }

  public void setConDateCloture(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateCloture from " + conDateCloture() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_CLOTURE_KEY);
  }

  public NSTimestamp conDateCreation() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_CREATION_KEY);
  }

  public void setConDateCreation(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateCreation from " + conDateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_CREATION_KEY);
  }

  public NSTimestamp conDateFinPaiement() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_FIN_PAIEMENT_KEY);
  }

  public void setConDateFinPaiement(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateFinPaiement from " + conDateFinPaiement() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_FIN_PAIEMENT_KEY);
  }

  public NSTimestamp conDateModif() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_MODIF_KEY);
  }

  public void setConDateModif(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateModif from " + conDateModif() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_MODIF_KEY);
  }

  public NSTimestamp conDateValidAdm() {
    return (NSTimestamp) storedValueForKey(EOOperation.CON_DATE_VALID_ADM_KEY);
  }

  public void setConDateValidAdm(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDateValidAdm from " + conDateValidAdm() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DATE_VALID_ADM_KEY);
  }

  public Integer conDuree() {
    return (Integer) storedValueForKey(EOOperation.CON_DUREE_KEY);
  }

  public void setConDuree(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDuree from " + conDuree() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DUREE_KEY);
  }

  public Integer conDureeMois() {
    return (Integer) storedValueForKey(EOOperation.CON_DUREE_MOIS_KEY);
  }

  public void setConDureeMois(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conDureeMois from " + conDureeMois() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_DUREE_MOIS_KEY);
  }

  public String conGroupeBud() {
    return (String) storedValueForKey(EOOperation.CON_GROUPE_BUD_KEY);
  }

  public void setConGroupeBud(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conGroupeBud from " + conGroupeBud() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_GROUPE_BUD_KEY);
  }

  public Integer conIndex() {
    return (Integer) storedValueForKey(EOOperation.CON_INDEX_KEY);
  }

  public void setConIndex(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conIndex from " + conIndex() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_INDEX_KEY);
  }

  public Integer conNature() {
    return (Integer) storedValueForKey(EOOperation.CON_NATURE_KEY);
  }

  public void setConNature(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conNature from " + conNature() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_NATURE_KEY);
  }

  public String conReferenceExterne() {
    return (String) storedValueForKey(EOOperation.CON_REFERENCE_EXTERNE_KEY);
  }

  public void setConReferenceExterne(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conReferenceExterne from " + conReferenceExterne() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_REFERENCE_EXTERNE_KEY);
  }

  public String conSuppr() {
    return (String) storedValueForKey(EOOperation.CON_SUPPR_KEY);
  }

  public void setConSuppr(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating conSuppr from " + conSuppr() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CON_SUPPR_KEY);
  }

  public String contexte() {
    return (String) storedValueForKey(EOOperation.CONTEXTE_KEY);
  }

  public void setContexte(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating contexte from " + contexte() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.CONTEXTE_KEY);
  }

  public NSTimestamp dateCreation() {
    return (NSTimestamp) storedValueForKey(EOOperation.DATE_CREATION_KEY);
  }

  public void setDateCreation(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating dateCreation from " + dateCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.DATE_CREATION_KEY);
  }

  public NSTimestamp dateMigration() {
    return (NSTimestamp) storedValueForKey(EOOperation.DATE_MIGRATION_KEY);
  }

  public void setDateMigration(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating dateMigration from " + dateMigration() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.DATE_MIGRATION_KEY);
  }

  public NSTimestamp dateModification() {
    return (NSTimestamp) storedValueForKey(EOOperation.DATE_MODIFICATION_KEY);
  }

  public void setDateModification(NSTimestamp value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating dateModification from " + dateModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.DATE_MODIFICATION_KEY);
  }

  public String description() {
    return (String) storedValueForKey(EOOperation.DESCRIPTION_KEY);
  }

  public void setDescription(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating description from " + description() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.DESCRIPTION_KEY);
  }

  public Boolean estConventionRA() {
    return (Boolean) storedValueForKey(EOOperation.EST_CONVENTION_RA_KEY);
  }

  public void setEstConventionRA(Boolean value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating estConventionRA from " + estConventionRA() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.EST_CONVENTION_RA_KEY);
  }

  public Boolean estFlechee() {
    return (Boolean) storedValueForKey(EOOperation.EST_FLECHEE_KEY);
  }

  public void setEstFlechee(Boolean value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating estFlechee from " + estFlechee() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.EST_FLECHEE_KEY);
  }

  public Boolean estInclusePPIEtablissement() {
    return (Boolean) storedValueForKey(EOOperation.EST_INCLUSE_PPI_ETABLISSEMENT_KEY);
  }

  public void setEstInclusePPIEtablissement(Boolean value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating estInclusePPIEtablissement from " + estInclusePPIEtablissement() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.EST_INCLUSE_PPI_ETABLISSEMENT_KEY);
  }

  public Boolean estIncluseTableauxBudgetaires() {
    return (Boolean) storedValueForKey(EOOperation.EST_INCLUSE_TABLEAUX_BUDGETAIRES_KEY);
  }

  public void setEstIncluseTableauxBudgetaires(Boolean value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating estIncluseTableauxBudgetaires from " + estIncluseTableauxBudgetaires() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.EST_INCLUSE_TABLEAUX_BUDGETAIRES_KEY);
  }

  public Boolean existeContrainteEligibiliteDepenses() {
    return (Boolean) storedValueForKey(EOOperation.EXISTE_CONTRAINTE_ELIGIBILITE_DEPENSES_KEY);
  }

  public void setExisteContrainteEligibiliteDepenses(Boolean value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating existeContrainteEligibiliteDepenses from " + existeContrainteEligibiliteDepenses() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.EXISTE_CONTRAINTE_ELIGIBILITE_DEPENSES_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(EOOperation.ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating idOpeOperation from " + idOpeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.ID_OPE_OPERATION_KEY);
  }

  public String libelle() {
    return (String) storedValueForKey(EOOperation.LIBELLE_KEY);
  }

  public void setLibelle(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating libelle from " + libelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.LIBELLE_KEY);
  }

  public String motifsAvis() {
    return (String) storedValueForKey(EOOperation.MOTIFS_AVIS_KEY);
  }

  public void setMotifsAvis(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating motifsAvis from " + motifsAvis() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.MOTIFS_AVIS_KEY);
  }

  public Integer persIdCreation() {
    return (Integer) storedValueForKey(EOOperation.PERS_ID_CREATION_KEY);
  }

  public void setPersIdCreation(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating persIdCreation from " + persIdCreation() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.PERS_ID_CREATION_KEY);
  }

  public Integer persIdModification() {
    return (Integer) storedValueForKey(EOOperation.PERS_ID_MODIFICATION_KEY);
  }

  public void setPersIdModification(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating persIdModification from " + persIdModification() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.PERS_ID_MODIFICATION_KEY);
  }

  public String remarques() {
    return (String) storedValueForKey(EOOperation.REMARQUES_KEY);
  }

  public void setRemarques(String value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating remarques from " + remarques() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.REMARQUES_KEY);
  }

  public Integer trOrdre() {
    return (Integer) storedValueForKey(EOOperation.TR_ORDRE_KEY);
  }

  public void setTrOrdre(Integer value) {
    if (EOOperation.LOG.isDebugEnabled()) {
        EOOperation.LOG.debug( "updating trOrdre from " + trOrdre() + " to " + value);
    }
    takeStoredValueForKey(value, EOOperation.TR_ORDRE_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation categorieOperation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation)storedValueForKey(EOOperation.CATEGORIE_OPERATION_KEY);
  }
  
  public void setCategorieOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation value) {
    takeStoredValueForKey(value, EOOperation.CATEGORIE_OPERATION_KEY);
  }

  public void setCategorieOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating categorieOperation from " + categorieOperation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCategorieOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation oldValue = categorieOperation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.CATEGORIE_OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.CATEGORIE_OPERATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure centreResponsabilite() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOOperation.CENTRE_RESPONSABILITE_KEY);
  }
  
  public void setCentreResponsabilite(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOOperation.CENTRE_RESPONSABILITE_KEY);
  }

  public void setCentreResponsabiliteRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating centreResponsabilite from " + centreResponsabilite() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCentreResponsabilite(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = centreResponsabilite();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.CENTRE_RESPONSABILITE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.CENTRE_RESPONSABILITE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EONaf codeNaf() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EONaf)storedValueForKey(EOOperation.CODE_NAF_KEY);
  }
  
  public void setCodeNaf(org.cocktail.fwkcktlpersonne.common.metier.EONaf value) {
    takeStoredValueForKey(value, EOOperation.CODE_NAF_KEY);
  }

  public void setCodeNafRelationship(org.cocktail.fwkcktlpersonne.common.metier.EONaf value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating codeNaf from " + codeNaf() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setCodeNaf(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EONaf oldValue = codeNaf();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.CODE_NAF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.CODE_NAF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure etablissement() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOOperation.ETABLISSEMENT_KEY);
  }
  
  public void setEtablissement(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOOperation.ETABLISSEMENT_KEY);
  }

  public void setEtablissementRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating etablissement from " + etablissement() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setEtablissement(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = etablissement();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.ETABLISSEMENT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.ETABLISSEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail)storedValueForKey(EOOperation.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOOperation.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette fluxDepenseRececette() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette)storedValueForKey(EOOperation.FLUX_DEPENSE_RECECETTE_KEY);
  }
  
  public void setFluxDepenseRececette(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette value) {
    takeStoredValueForKey(value, EOOperation.FLUX_DEPENSE_RECECETTE_KEY);
  }

  public void setFluxDepenseRececetteRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating fluxDepenseRececette from " + fluxDepenseRececette() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setFluxDepenseRececette(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette oldValue = fluxDepenseRececette();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.FLUX_DEPENSE_RECECETTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.FLUX_DEPENSE_RECECETTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlpersonne.common.metier.EOStructure groupePartenaire() {
    return (org.cocktail.fwkcktlpersonne.common.metier.EOStructure)storedValueForKey(EOOperation.GROUPE_PARTENAIRE_KEY);
  }
  
  public void setGroupePartenaire(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    takeStoredValueForKey(value, EOOperation.GROUPE_PARTENAIRE_KEY);
  }

  public void setGroupePartenaireRelationship(org.cocktail.fwkcktlpersonne.common.metier.EOStructure value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating groupePartenaire from " + groupePartenaire() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setGroupePartenaire(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlpersonne.common.metier.EOStructure oldValue = groupePartenaire();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.GROUPE_PARTENAIRE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.GROUPE_PARTENAIRE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage modePilotage() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage)storedValueForKey(EOOperation.MODE_PILOTAGE_KEY);
  }
  
  public void setModePilotage(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage value) {
    takeStoredValueForKey(value, EOOperation.MODE_PILOTAGE_KEY);
  }

  public void setModePilotageRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating modePilotage from " + modePilotage() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setModePilotage(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage oldValue = modePilotage();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.MODE_PILOTAGE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.MODE_PILOTAGE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOEb organComposante() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOEb)storedValueForKey(EOOperation.ORGAN_COMPOSANTE_KEY);
  }
  
  public void setOrganComposante(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    takeStoredValueForKey(value, EOOperation.ORGAN_COMPOSANTE_KEY);
  }

  public void setOrganComposanteRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOEb value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating organComposante from " + organComposante() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setOrganComposante(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOEb oldValue = organComposante();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.ORGAN_COMPOSANTE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.ORGAN_COMPOSANTE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat typeClassificationContrat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat)storedValueForKey(EOOperation.TYPE_CLASSIFICATION_CONTRAT_KEY);
  }
  
  public void setTypeClassificationContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat value) {
    takeStoredValueForKey(value, EOOperation.TYPE_CLASSIFICATION_CONTRAT_KEY);
  }

  public void setTypeClassificationContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating typeClassificationContrat from " + typeClassificationContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeClassificationContrat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat oldValue = typeClassificationContrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.TYPE_CLASSIFICATION_CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.TYPE_CLASSIFICATION_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat typeContrat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat)storedValueForKey(EOOperation.TYPE_CONTRAT_KEY);
  }
  
  public void setTypeContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat value) {
    takeStoredValueForKey(value, EOOperation.TYPE_CONTRAT_KEY);
  }

  public void setTypeContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating typeContrat from " + typeContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeContrat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat oldValue = typeContrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.TYPE_CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.TYPE_CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation typeOperation() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation)storedValueForKey(EOOperation.TYPE_OPERATION_KEY);
  }
  
  public void setTypeOperation(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation value) {
    takeStoredValueForKey(value, EOOperation.TYPE_OPERATION_KEY);
  }

  public void setTypeOperationRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating typeOperation from " + typeOperation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeOperation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation oldValue = typeOperation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.TYPE_OPERATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.TYPE_OPERATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction typeReconduction() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction)storedValueForKey(EOOperation.TYPE_RECONDUCTION_KEY);
  }
  
  public void setTypeReconduction(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction value) {
    takeStoredValueForKey(value, EOOperation.TYPE_RECONDUCTION_KEY);
  }

  public void setTypeReconductionRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating typeReconduction from " + typeReconduction() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeReconduction(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction oldValue = typeReconduction();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.TYPE_RECONDUCTION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.TYPE_RECONDUCTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateurCreation() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur)storedValueForKey(EOOperation.UTILISATEUR_CREATION_KEY);
  }
  
  public void setUtilisateurCreation(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOOperation.UTILISATEUR_CREATION_KEY);
  }

  public void setUtilisateurCreationRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating utilisateurCreation from " + utilisateurCreation() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurCreation(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur oldValue = utilisateurCreation();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.UTILISATEUR_CREATION_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.UTILISATEUR_CREATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateurModif() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur)storedValueForKey(EOOperation.UTILISATEUR_MODIF_KEY);
  }
  
  public void setUtilisateurModif(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOOperation.UTILISATEUR_MODIF_KEY);
  }

  public void setUtilisateurModifRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating utilisateurModif from " + utilisateurModif() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurModif(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur oldValue = utilisateurModif();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.UTILISATEUR_MODIF_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.UTILISATEUR_MODIF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateurValidAdm() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur)storedValueForKey(EOOperation.UTILISATEUR_VALID_ADM_KEY);
  }
  
  public void setUtilisateurValidAdm(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    takeStoredValueForKey(value, EOOperation.UTILISATEUR_VALID_ADM_KEY);
  }

  public void setUtilisateurValidAdmRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur value) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("updating utilisateurValidAdm from " + utilisateurValidAdm() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setUtilisateurValidAdm(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur oldValue = utilisateurValidAdm();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOOperation.UTILISATEUR_VALID_ADM_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOOperation.UTILISATEUR_VALID_ADM_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> avenants() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>)storedValueForKey(EOOperation.AVENANTS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> avenants(EOQualifier qualifier) {
    return avenants(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> avenants(EOQualifier qualifier, boolean fetch) {
    return avenants(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> avenants(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant.OPERATION_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = avenants();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToAvenants(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.AVENANTS_KEY);
  }

  public void removeFromAvenants(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.AVENANTS_KEY);
  }

  public void addToAvenantsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to avenants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAvenants(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.AVENANTS_KEY);
    }
  }

  public void removeFromAvenantsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from avenants relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAvenants(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.AVENANTS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant createAvenantsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.AVENANTS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant) eo;
  }

  public void deleteAvenantsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.AVENANTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAvenantsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> objects = avenants().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAvenantsRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> axeStrategiques() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique>)storedValueForKey(EOOperation.AXE_STRATEGIQUES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> axeStrategiques(EOQualifier qualifier) {
    return axeStrategiques(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> axeStrategiques(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> results;
      results = axeStrategiques();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToAxeStrategiques(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.AXE_STRATEGIQUES_KEY);
  }

  public void removeFromAxeStrategiques(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.AXE_STRATEGIQUES_KEY);
  }

  public void addToAxeStrategiquesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to axeStrategiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToAxeStrategiques(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.AXE_STRATEGIQUES_KEY);
    }
  }

  public void removeFromAxeStrategiquesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from axeStrategiques relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromAxeStrategiques(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.AXE_STRATEGIQUES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique createAxeStrategiquesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.AXE_STRATEGIQUES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique) eo;
  }

  public void deleteAxeStrategiquesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.AXE_STRATEGIQUES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllAxeStrategiquesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.AxeStrategique> objects = axeStrategiques().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteAxeStrategiquesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> indicateursContrat() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat>)storedValueForKey(EOOperation.INDICATEURS_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> indicateursContrat(EOQualifier qualifier) {
    return indicateursContrat(qualifier, null);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> indicateursContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> results;
      results = indicateursContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToIndicateursContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.INDICATEURS_CONTRAT_KEY);
  }

  public void removeFromIndicateursContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.INDICATEURS_CONTRAT_KEY);
  }

  public void addToIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to indicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToIndicateursContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.INDICATEURS_CONTRAT_KEY);
    }
  }

  public void removeFromIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from indicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromIndicateursContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.INDICATEURS_CONTRAT_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat createIndicateursContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.INDICATEURS_CONTRAT_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat) eo;
  }

  public void deleteIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.INDICATEURS_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllIndicateursContratRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> objects = indicateursContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteIndicateursContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> operationPartenaires() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>)storedValueForKey(EOOperation.OPERATION_PARTENAIRES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> operationPartenaires(EOQualifier qualifier) {
    return operationPartenaires(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> operationPartenaires(EOQualifier qualifier, boolean fetch) {
    return operationPartenaires(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> operationPartenaires(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire.OPERATION_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = operationPartenaires();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToOperationPartenaires(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.OPERATION_PARTENAIRES_KEY);
  }

  public void removeFromOperationPartenaires(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.OPERATION_PARTENAIRES_KEY);
  }

  public void addToOperationPartenairesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to operationPartenaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToOperationPartenaires(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.OPERATION_PARTENAIRES_KEY);
    }
  }

  public void removeFromOperationPartenairesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from operationPartenaires relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromOperationPartenaires(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.OPERATION_PARTENAIRES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire createOperationPartenairesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.OPERATION_PARTENAIRES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire) eo;
  }

  public void deleteOperationPartenairesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.OPERATION_PARTENAIRES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllOperationPartenairesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire> objects = operationPartenaires().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteOperationPartenairesRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrat() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)storedValueForKey(EOOperation.PROJET_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrat(EOQualifier qualifier) {
    return projetContrat(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrat(EOQualifier qualifier, boolean fetch) {
    return projetContrat(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = projetContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToProjetContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.PROJET_CONTRAT_KEY);
  }

  public void removeFromProjetContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.PROJET_CONTRAT_KEY);
  }

  public void addToProjetContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to projetContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToProjetContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.PROJET_CONTRAT_KEY);
    }
  }

  public void removeFromProjetContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from projetContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromProjetContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.PROJET_CONTRAT_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat createProjetContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.PROJET_CONTRAT_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat) eo;
  }

  public void deleteProjetContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.PROJET_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllProjetContratRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> objects = projetContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteProjetContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> repartIndicateursContrat() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat>)storedValueForKey(EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier) {
    return repartIndicateursContrat(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier, boolean fetch) {
    return repartIndicateursContrat(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> repartIndicateursContrat(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat.CONTRAT_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = repartIndicateursContrat();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToRepartIndicateursContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public void removeFromRepartIndicateursContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
  }

  public void addToRepartIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to repartIndicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToRepartIndicateursContrat(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
    }
  }

  public void removeFromRepartIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from repartIndicateursContrat relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromRepartIndicateursContrat(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat createRepartIndicateursContratRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat) eo;
  }

  public void deleteRepartIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.REPART_INDICATEURS_CONTRAT_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllRepartIndicateursContratRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat> objects = repartIndicateursContrat().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteRepartIndicateursContratRelationship(objects.nextElement());
    }
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> tranches() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>)storedValueForKey(EOOperation.TRANCHES_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> tranches(EOQualifier qualifier) {
    return tranches(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> tranches(EOQualifier qualifier, boolean fetch) {
    return tranches(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> tranches(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche.OPERATION_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = tranches();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTranches(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche object) {
    includeObjectIntoPropertyWithKey(object, EOOperation.TRANCHES_KEY);
  }

  public void removeFromTranches(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche object) {
    excludeObjectFromPropertyWithKey(object, EOOperation.TRANCHES_KEY);
  }

  public void addToTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("adding " + object + " to tranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToTranches(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOOperation.TRANCHES_KEY);
    }
  }

  public void removeFromTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche object) {
    if (EOOperation.LOG.isDebugEnabled()) {
      EOOperation.LOG.debug("removing " + object + " from tranches relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromTranches(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.TRANCHES_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche createTranchesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOOperation.TRANCHES_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche) eo;
  }

  public void deleteTranchesRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOOperation.TRANCHES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTranchesRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche> objects = tranches().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTranchesRelationship(objects.nextElement());
    }
  }


  public static Operation create(EOEditingContext editingContext, NSTimestamp dateCreation
, Boolean estConventionRA
, Boolean estFlechee
, Boolean estInclusePPIEtablissement
, Boolean estIncluseTableauxBudgetaires
, Boolean existeContrainteEligibiliteDepenses
, Integer idOpeOperation
, String libelle
, Integer persIdCreation
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation categorieOperation, org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail, org.cocktail.fwkcktlgfcoperations.server.metier.operation.FluxDepenseRececette fluxDepenseRececette, org.cocktail.fwkcktlgfcoperations.server.metier.operation.ModePilotage modePilotage, org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation typeOperation, org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur utilisateurCreation) {
    Operation eo = (Operation) EOUtilities.createAndInsertInstance(editingContext, EOOperation.ENTITY_NAME);    
        eo.setDateCreation(dateCreation);
        eo.setEstConventionRA(estConventionRA);
        eo.setEstFlechee(estFlechee);
        eo.setEstInclusePPIEtablissement(estInclusePPIEtablissement);
        eo.setEstIncluseTableauxBudgetaires(estIncluseTableauxBudgetaires);
        eo.setExisteContrainteEligibiliteDepenses(existeContrainteEligibiliteDepenses);
        eo.setIdOpeOperation(idOpeOperation);
        eo.setLibelle(libelle);
        eo.setPersIdCreation(persIdCreation);
    eo.setCategorieOperationRelationship(categorieOperation);
    eo.setExerciceCocktailRelationship(exerciceCocktail);
    eo.setFluxDepenseRececetteRelationship(fluxDepenseRececette);
    eo.setModePilotageRelationship(modePilotage);
    eo.setTypeOperationRelationship(typeOperation);
    eo.setUtilisateurCreationRelationship(utilisateurCreation);
    return eo;
  }

  public static ERXFetchSpecification<Operation> fetchSpec() {
    return new ERXFetchSpecification<Operation>(EOOperation.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Operation> fetchAll(EOEditingContext editingContext) {
    return EOOperation.fetchAll(editingContext, null);
  }

  public static NSArray<Operation> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOOperation.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Operation> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Operation> fetchSpec = new ERXFetchSpecification<Operation>(EOOperation.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Operation> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Operation fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperation.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Operation fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Operation> eoObjects = EOOperation.fetchAll(editingContext, qualifier, null);
    Operation eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Operation fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOOperation.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Operation fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Operation eoObject = EOOperation.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeOperation that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Operation localInstanceIn(EOEditingContext editingContext, Operation eo) {
    Operation localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}