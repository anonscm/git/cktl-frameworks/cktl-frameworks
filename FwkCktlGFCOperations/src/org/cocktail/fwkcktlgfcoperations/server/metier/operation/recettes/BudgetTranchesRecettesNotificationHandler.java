package org.cocktail.fwkcktlgfcoperations.server.metier.operation.recettes;

import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TrancheBudgetRec;

public class BudgetTranchesRecettesNotificationHandler {

	private Map<TrancheBudgetRec, BudgetCombinaisonAxesRecetteNotificationHandler> listeNotificationParTrancheRecette;
	
	public BudgetTranchesRecettesNotificationHandler() {
		listeNotificationParTrancheRecette = new HashMap<TrancheBudgetRec, BudgetCombinaisonAxesRecetteNotificationHandler>();
	}
	
}
