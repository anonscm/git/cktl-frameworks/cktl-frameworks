package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import java.math.BigDecimal;

/**
 * 
 * Service de calcul autour de la tva.
 * 
 * @author Alexis Tual
 *
 */
public interface CalculTvaService {

    /**
     * @param montantHt le montant ht hors taxe
     * @param taux le taux de tva
     * @return le montant ttc correspondant au montant ht
     */
    BigDecimal montantTtc(double montantHt, double taux);
    
    /**
     * @param montantTtc le montant ttc
     * @param taux le taux de tva
     * @return le montant ht correspondant au montant ttc initial
     */
    BigDecimal montantHt(double montantTtc, double taux);
    
}
