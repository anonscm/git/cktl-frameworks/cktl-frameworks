package org.cocktail.fwkcktlgfcoperations.server.metier.operation.depenses;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOVapiCptbudAllocConsoDep;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

import com.webobjects.eocontrol.EOQualifier;

import er.extensions.eof.ERXQ;

public class LigneDepenseCPSurCombinaisonAxesConsomme implements LigneDepenseCPSurCombinaisonAxes {

    private final EOVapiCptbudAllocConsoDep alloconso;
    private final Tranche tranche;

    public LigneDepenseCPSurCombinaisonAxesConsomme(Operation operation, BudgetCombinaisonAxesDepense combi, Tranche tranche) {
        this.tranche = tranche;

        EOQualifier qual_idOpe = EOVapiCptbudAllocConsoDep.ID_OPE_OPERATION.eq(operation.idOpeOperation());
        EOQualifier qual_combi = EOVapiCptbudAllocConsoDep.TO_EB.eq(combi.getEntiteBudgetaire())
                .and(EOVapiCptbudAllocConsoDep.TO_DESTINATION_DEPENSE.eq(combi.getDestination()))
                .and(EOVapiCptbudAllocConsoDep.TO_NATURE_DEP.eq(combi.getNature()));
        // qual enveloppe budgetaire dans combi ???

        EOQualifier qual_exer = EOVapiCptbudAllocConsoDep.TO_EXERCICE.eq(tranche.exercice());

        EOQualifier qualifier = ERXQ.and(qual_idOpe, qual_combi, qual_exer);
        this.alloconso = EOVapiCptbudAllocConsoDep.fetchByQualifier(operation.editingContext(), qualifier);

    }

    public Tranche tranche() {
        return this.tranche;
    }

    public BigDecimal montantAE() {
        if (alloconso == null) {
            return BigDecimal.ZERO;
        }
        if (alloconso.aeConsommees() == null) {
            return BigDecimal.ZERO;
        }
        return alloconso.aeConsommees();
    }

    public BigDecimal montantCP() {
        if (alloconso == null) {
            return BigDecimal.ZERO;
        }
        if (alloconso.cpConsommes() == null) {
            return BigDecimal.ZERO;
        }
        return alloconso.cpConsommes();
    }

    public void setMontantCP(BigDecimal value) {
        throw new IllegalAccessError("Le montant de CP consommé est en lecture seul pour cette tranche.");
    }

    public boolean isPrevisionnel() {
        return false;
    }

	public boolean isMontantCPMisAJour() {
		return false;
	}
	
	public boolean isNouvelleSaisie() {
		return false;
	}

    public boolean isMontantCPEfface() {
        return false;
    }
}
