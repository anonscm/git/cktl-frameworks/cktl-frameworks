
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import java.math.BigDecimal;
import java.util.Enumeration;

import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat;
import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.date.DateOperation;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionArgument;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.OperationPartenaire;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.NotificationCenter;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.NotificationCenter.Notification;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * 
 * 
 * @author Michael HALLER, Consortium Cocktail, 2008
 * 
 */
public class FactoryTranche extends Factory 
{

	/**
	 * Constructeur.
	 * @param ec
	 */
	public FactoryTranche(EOEditingContext ec) {
		super(ec);
	}

	/**
	 * Constructeur.
	 * @param ec
	 * @param withlog
	 */
	public FactoryTranche(EOEditingContext ec, Boolean withlog) {
		super(ec, withlog);
	}

	
	/**
	 * Creation d'une tranche annuelle.
	 * @param operation Contrat de rattachement.
	 * @param exercice Exercice correspondant. Une tranche annuelle <--> un exercice.
	 * @param montantDepenses Montant total des depenses prevues pour l'exercice correspondant. 
	 * @param montantRecettes Montant total des recettes prevues pour l'exercice correspondant. PAS ENCORE UTILISE : DEP = REC.
	 * @param createur Utilisateur createur de la tranche. PAS ENCORE UTILISE.
	 * @return La tranche creee.
	 * @throws Exception
	 */
	public Tranche creerTranche(
			 Operation operation,
			final EOExerciceCocktail exercice,
			final EOUtilisateur createur) throws Exception, ExceptionUtilisateur {

		if (operation == null) {
			throw new ExceptionUtilisateur("Un contrat est necessaire pour créer une tranche.");
		}
		if (exercice == null) {
			throw new ExceptionUtilisateur("Un exercice est necessaire pour créer une tranche.");
		}
		if (createur == null) {
			throw new ExceptionUtilisateur("L'utilisateur cr\u00E9ateur de la tranche est requis.");
		}
		
		Tranche tranche = Tranche.instanciate(ec);
		
		ec.insertObject(tranche);

		tranche.setExerciceCocktailRelationship(exercice);
		tranche.setPersIdCreation(createur.personne().persId());
		tranche.setPersIdModif(createur.personne().persId());
		
		tranche.setTraSuppr(Tranche.TRA_SUPPR_NON);
		tranche.setTraDateCreation(new NSTimestamp());
		
		tranche.setReportNmoins1(BigDecimal.ZERO);
		tranche.setReportNplus1(BigDecimal.ZERO);
		
		tranche.setTypeEtatRelationship(EOTypeEtat.getTypeEtat(tranche.editingContext(), EOTypeEtat.ETAT_A_VALIDER));
		
		// Creation des repartPartenaireTranche
		NSArray partenaires = operation.operationPartenaires();
		Enumeration<OperationPartenaire>enumPartenaires = partenaires.objectEnumerator();
		while (enumPartenaires.hasMoreElements()) {
			OperationPartenaire operationPartenaire = (OperationPartenaire) enumPartenaires.nextElement();
			RepartPartenaireTranche rpt = RepartPartenaireTranche.instanciate(ec);
			ec.insertObject(rpt);
			rpt.setMontantParticipation(BigDecimal.ZERO);
			operationPartenaire.addToContributionsRelationship(rpt);
			tranche.addToToRepartPartenaireTranchesRelationship(rpt);
		}
		operation.addToTranchesRelationship(tranche);
        NotificationCenter.instance().notifier(Notification.REFRESH_TRANCHES, operation.editingContext());
		return tranche;
	}

	/**
	 * Supprime une tranche. Ele est seulement marquee comme telle.
	 * @param tranche Tranche a supprimer.
	 */
	public void supprimerTranche(
			final Tranche tranche) throws ExceptionUtilisateur {

		if (tranche == null) {
			throw new ExceptionUtilisateur("La tranche \u00E0 supprimer est requise.");
		}
		
		tranche.setTraSuppr(Tranche.TRA_SUPPR_OUI);
	}

	/**
	 * Modification d'une tranche existante.
	 * @param operation Contrat de rattachement.
	 * @param exercice Exercice correspondant. Une tranche annuelle <--> un exercice.
	 * @param montantDepenses Montant total des depenses prevues pour l'exercice correspondant. 
	 * @param montantRecettes Montant total des recettes prevues pour l'exercice correspondant. PAS ENCORE UTILISE : DEP = REC.
	 * @param utilisateur Utilisateur modificateur de la tranche. PAS ENCORE UTILISE.
	 * @throws ExceptionArgument
	 */
	public void modifierTranche(
			final Tranche tranche,
			final Operation operation,
			final EOExerciceCocktail exercice,
			final BigDecimal montantDepenses,
			final BigDecimal montantRecettes,
			final EOUtilisateur utilisateur) throws Exception, ExceptionUtilisateur {

		if (operation == null) {
			throw new ExceptionUtilisateur("Un contrat est necessaire pour modifier la tranche.");
		}
		if (exercice == null) {
			throw new ExceptionUtilisateur("Un exercice est necessaire pour modifier la tranche.");
		}
		if (utilisateur == null) {
			throw new ExceptionUtilisateur("L'utilisateur modificateur de la tranche est requis pour modifier la tranche.");
		}
		if (montantDepenses == null) {
			throw new ExceptionUtilisateur("Un montant sup\u00E9rieur ou \u00E9gal \u00E0 z\u00E9ro est n\u00E9cessaire concernant les d\u00E9penses.");
		}
		if (tranche.isValide()) {
			throw new ExceptionUtilisateur(
					"Impossible de modifier cette tranche, elle a \u00E9t\u00E9 valid\u00E9e le "+
					DateOperation.print(tranche.traDateValid(), false)+
					".");
		}
		
		tranche.setOperationRelationship(operation);
		tranche.setExerciceCocktailRelationship(exercice);
		tranche.setTraDateModif(new NSTimestamp());
	}
}
