package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.CategorieOperation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeOperation;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXQ;


public class FinderTypeOperation extends Finder {

	public FinderTypeOperation(EOEditingContext ec) {
		super(ec, TypeOperation.ENTITY_NAME);
	}
	
	public NSArray<TypeOperation> findByCategorie(CategorieOperation categorie) throws ExceptionFinder {
		if (categorie == null) {
			return NSArray.emptyArray();
		}
		
		EOQualifier qualCodeCategorie = TypeOperation.CATEGORIE_OPERATION.dot(CategorieOperation.CODE_CATEGORIE).eq(categorie.codeCategorie());
		EOQualifier qualHorsInvestissement = TypeOperation.CODE_TYPE_OPERATION.isNot(TypeOperation.OP_INVESTISSEMENT);
		EOQualifier qualTypeOperation = ERXQ.and(qualCodeCategorie, qualHorsInvestissement);
		this.addMandatoryQualifier(qualTypeOperation);
		return (NSArray<TypeOperation>) find();
	}
	
	public TypeOperation findByCode(String code) throws ExceptionFinder {
		EOQualifier qualCode = ERXQ.equals(TypeOperation.CODE_TYPE_OPERATION_KEY, code);
		this.addMandatoryQualifier(qualCode);
		return findOne();
	}

	protected TypeOperation findOne() throws ExceptionFinder {
		NSArray<TypeOperation> types = find();
		if (types == null || types.size() > 1) {
			throw new ExceptionFinder("FindOne attend un résultat de recherche unique. Found : " + types);
		}
		
		return types.lastObject();
	}

	@Override
	public void clearAllCriteria() {
	}

	@Override
	public boolean canFind() {
		return true;
	}

	@Override
	public String getCurrentWarningMessage() {
		return null;
	}

}
