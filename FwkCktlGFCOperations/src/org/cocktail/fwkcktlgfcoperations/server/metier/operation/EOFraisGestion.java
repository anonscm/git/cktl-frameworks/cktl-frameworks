// DO NOT EDIT.  Make changes to FraisGestion.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOFraisGestion extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeFraisGestion";
	public static final String ENTITY_TABLE_NAME = "GFC.OPE_FRAIS_GESTION";

  // Attribute Keys
  public static final ERXKey<java.math.BigDecimal> CHARGES_NON_BUDGETAIRES = new ERXKey<java.math.BigDecimal>("chargesNonBudgetaires");
  public static final ERXKey<java.math.BigDecimal> COUTS_INDIRECTS_NON_IMPUTES = new ERXKey<java.math.BigDecimal>("coutsIndirectsNonImputes");
  public static final ERXKey<java.math.BigDecimal> MONTANT = new ERXKey<java.math.BigDecimal>("montant");
  public static final ERXKey<java.math.BigDecimal> MONTANT_POURCENTAGE_TRANCHE = new ERXKey<java.math.BigDecimal>("montantPourcentageTranche");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche> REPART_PARTENAIRE_TRANCHE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche>("repartPartenaireTranche");

  // Attributes
  public static final String CHARGES_NON_BUDGETAIRES_KEY = CHARGES_NON_BUDGETAIRES.key();
  public static final String COUTS_INDIRECTS_NON_IMPUTES_KEY = COUTS_INDIRECTS_NON_IMPUTES.key();
  public static final String MONTANT_KEY = MONTANT.key();
  public static final String MONTANT_POURCENTAGE_TRANCHE_KEY = MONTANT_POURCENTAGE_TRANCHE.key();
  // Relationships
  public static final String REPART_PARTENAIRE_TRANCHE_KEY = REPART_PARTENAIRE_TRANCHE.key();

  private static Logger LOG = Logger.getLogger(EOFraisGestion.class);

  public FraisGestion localInstanceIn(EOEditingContext editingContext) {
    FraisGestion localInstance = (FraisGestion)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public java.math.BigDecimal chargesNonBudgetaires() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.CHARGES_NON_BUDGETAIRES_KEY);
  }

  public void setChargesNonBudgetaires(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating chargesNonBudgetaires from " + chargesNonBudgetaires() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.CHARGES_NON_BUDGETAIRES_KEY);
  }

  public java.math.BigDecimal coutsIndirectsNonImputes() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.COUTS_INDIRECTS_NON_IMPUTES_KEY);
  }

  public void setCoutsIndirectsNonImputes(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating coutsIndirectsNonImputes from " + coutsIndirectsNonImputes() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.COUTS_INDIRECTS_NON_IMPUTES_KEY);
  }

  public java.math.BigDecimal montant() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.MONTANT_KEY);
  }

  public void setMontant(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating montant from " + montant() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.MONTANT_KEY);
  }

  public java.math.BigDecimal montantPourcentageTranche() {
    return (java.math.BigDecimal) storedValueForKey(EOFraisGestion.MONTANT_POURCENTAGE_TRANCHE_KEY);
  }

  public void setMontantPourcentageTranche(java.math.BigDecimal value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
        EOFraisGestion.LOG.debug( "updating montantPourcentageTranche from " + montantPourcentageTranche() + " to " + value);
    }
    takeStoredValueForKey(value, EOFraisGestion.MONTANT_POURCENTAGE_TRANCHE_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche repartPartenaireTranche() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche)storedValueForKey(EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
  }
  
  public void setRepartPartenaireTranche(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche value) {
    takeStoredValueForKey(value, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
  }

  public void setRepartPartenaireTrancheRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche value) {
    if (EOFraisGestion.LOG.isDebugEnabled()) {
      EOFraisGestion.LOG.debug("updating repartPartenaireTranche from " + repartPartenaireTranche() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setRepartPartenaireTranche(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche oldValue = repartPartenaireTranche();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOFraisGestion.REPART_PARTENAIRE_TRANCHE_KEY);
    }
  }
  

  public static FraisGestion create(EOEditingContext editingContext, java.math.BigDecimal montant
, org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche repartPartenaireTranche) {
    FraisGestion eo = (FraisGestion) EOUtilities.createAndInsertInstance(editingContext, EOFraisGestion.ENTITY_NAME);    
        eo.setMontant(montant);
    eo.setRepartPartenaireTrancheRelationship(repartPartenaireTranche);
    return eo;
  }

  public static ERXFetchSpecification<FraisGestion> fetchSpec() {
    return new ERXFetchSpecification<FraisGestion>(EOFraisGestion.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext) {
    return EOFraisGestion.fetchAll(editingContext, null);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOFraisGestion.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<FraisGestion> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<FraisGestion> fetchSpec = new ERXFetchSpecification<FraisGestion>(EOFraisGestion.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<FraisGestion> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static FraisGestion fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOFraisGestion.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FraisGestion fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<FraisGestion> eoObjects = EOFraisGestion.fetchAll(editingContext, qualifier, null);
    FraisGestion eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeFraisGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FraisGestion fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOFraisGestion.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static FraisGestion fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    FraisGestion eoObject = EOFraisGestion.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeFraisGestion that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static FraisGestion localInstanceIn(EOEditingContext editingContext, FraisGestion eo) {
    FraisGestion localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}