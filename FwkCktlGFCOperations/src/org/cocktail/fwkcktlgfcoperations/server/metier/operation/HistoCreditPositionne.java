

// HistoCreditPositionne.java
// 
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.validation.ERXValidationFactory;


@Deprecated
public class HistoCreditPositionne extends EOHistoCreditPositionne
{

    public HistoCreditPositionne() {
        super();
    }
    
    public static HistoCreditPositionne creerHistoCreditPositionne(EOEditingContext editingContext, 
                                              Tranche tranche,
                                              EOPlanComptableExer planco,
                                              EOTypeCredit typeCredit,
                                              EOEb organ,
                                              BigDecimal montant
                                              ) {
        HistoCreditPositionne historique = HistoCreditPositionne.create(editingContext, null, null, null);
        historique.setHcpDate(new NSTimestamp());
        historique.setTrancheRelationship(tranche);
        historique.setTypeCreditRelationship(typeCredit);
        historique.setExerciceRelationship(tranche.exerciceCocktail().exercice());
        historique.setPlancoRelationship(planco);
        historique.setOrganRelationship(organ);
        historique.setHcpMontant(montant);
        return historique;
    }
    
    public static void checkPreCreation(VDepensesTranche depenses, BigDecimal montant) {
        // On vérifie que le montant des crédits positionnés ne dépassera pas le montant des dépenses.
        if (depenses != null && montant != null && depenses.tranche().operation().isModeBudgetAvance()) {
            if (depenses.montantResteAPositionne().compareTo(montant) < 0) {
                throw ERXValidationFactory.defaultFactory().createCustomException(depenses, "MontantCreditDepasseDepenses");
            }
        }
    }
    
    private void fixHcpSuppr() {
        if(hcpSuppr() == null) {
            setHcpSuppr("N");
        }
    }
    
    @Override
    public void validateForInsert() throws ValidationException {
        super.validateForInsert();
        fixHcpSuppr();
    }
    
    @Override
    public void validateForUpdate() throws ValidationException {
        super.validateForUpdate();
        fixHcpSuppr();
    }
    
    @Override
    public void validateForSave() throws ValidationException {
        super.validateForSave();
        if (tranche().operation().isModeBudgetAvance() && planco() == null)
            throw ERXValidationFactory.defaultFactory().createCustomException(this, PLANCO_KEY, null, "PlancoMissing");
    }
    
}
