package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Implémentation de base du calcul de la tva.
 * 
 * @author Alexis Tual
 *
 */
public class CalculTvaServiceImpl implements CalculTvaService {

    private BigDecimal coeff(BigDecimal taux) {
        return BigDecimal.valueOf(1).add(taux.divide(BigDecimal.valueOf(100.0)));
    }
    
    /** 
     * {@inheritDoc}
     */
    public BigDecimal montantTtc(double montantHt, double taux) {
        BigDecimal montantHtBigD = BigDecimal.valueOf(montantHt);
        BigDecimal tauxBigD = BigDecimal.valueOf(taux);
        BigDecimal montantTtc = BigDecimal.ZERO;
        montantTtc.setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal coeffTva = coeff(tauxBigD);
        montantTtc = montantHtBigD.multiply(coeffTva);
        return montantTtc;
    }

    /**
     * {@inheritDoc}
     */
    public BigDecimal montantHt(double montantTtc, double taux) {
        BigDecimal montantTtcBigD = BigDecimal.valueOf(montantTtc);
        BigDecimal tauxBigD = BigDecimal.valueOf(taux);
        BigDecimal montantHt = BigDecimal.ZERO;
        montantHt.setScale(2, BigDecimal.ROUND_HALF_UP);
        BigDecimal coeffTva = coeff(tauxBigD);
        montantHt = montantTtcBigD.divide(coeffTva, 2, RoundingMode.HALF_UP);
        return montantHt;
    }

}
