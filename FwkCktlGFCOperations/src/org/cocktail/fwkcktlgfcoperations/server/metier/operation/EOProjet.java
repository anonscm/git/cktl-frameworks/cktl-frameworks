// DO NOT EDIT.  Make changes to Projet.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOProjet extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeProjet";
	public static final String ENTITY_TABLE_NAME = "GFC.PROJET";

  // Attribute Keys
  public static final ERXKey<String> PJT_LIBELLE = new ERXKey<String>("pjtLibelle");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail> EXERCICE_COCKTAIL = new ERXKey<org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail>("exerciceCocktail");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> PROJET_CONTRATS = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>("projetContrats");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet> PROJET_PERE = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet>("projetPere");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet> TYPE_PROJET = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet>("typeProjet");

  // Attributes
  public static final String PJT_LIBELLE_KEY = PJT_LIBELLE.key();
  // Relationships
  public static final String EXERCICE_COCKTAIL_KEY = EXERCICE_COCKTAIL.key();
  public static final String PROJET_CONTRATS_KEY = PROJET_CONTRATS.key();
  public static final String PROJET_PERE_KEY = PROJET_PERE.key();
  public static final String TYPE_PROJET_KEY = TYPE_PROJET.key();

  private static Logger LOG = Logger.getLogger(EOProjet.class);

  public Projet localInstanceIn(EOEditingContext editingContext) {
    Projet localInstance = (Projet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String pjtLibelle() {
    return (String) storedValueForKey(EOProjet.PJT_LIBELLE_KEY);
  }

  public void setPjtLibelle(String value) {
    if (EOProjet.LOG.isDebugEnabled()) {
        EOProjet.LOG.debug( "updating pjtLibelle from " + pjtLibelle() + " to " + value);
    }
    takeStoredValueForKey(value, EOProjet.PJT_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail exerciceCocktail() {
    return (org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail)storedValueForKey(EOProjet.EXERCICE_COCKTAIL_KEY);
  }
  
  public void setExerciceCocktail(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    takeStoredValueForKey(value, EOProjet.EXERCICE_COCKTAIL_KEY);
  }

  public void setExerciceCocktailRelationship(org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail value) {
    if (EOProjet.LOG.isDebugEnabled()) {
      EOProjet.LOG.debug("updating exerciceCocktail from " + exerciceCocktail() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setExerciceCocktail(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail oldValue = exerciceCocktail();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOProjet.EXERCICE_COCKTAIL_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOProjet.EXERCICE_COCKTAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet projetPere() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet)storedValueForKey(EOProjet.PROJET_PERE_KEY);
  }
  
  public void setProjetPere(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet value) {
    takeStoredValueForKey(value, EOProjet.PROJET_PERE_KEY);
  }

  public void setProjetPereRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet value) {
    if (EOProjet.LOG.isDebugEnabled()) {
      EOProjet.LOG.debug("updating projetPere from " + projetPere() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setProjetPere(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet oldValue = projetPere();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOProjet.PROJET_PERE_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOProjet.PROJET_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet typeProjet() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet)storedValueForKey(EOProjet.TYPE_PROJET_KEY);
  }
  
  public void setTypeProjet(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet value) {
    takeStoredValueForKey(value, EOProjet.TYPE_PROJET_KEY);
  }

  public void setTypeProjetRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet value) {
    if (EOProjet.LOG.isDebugEnabled()) {
      EOProjet.LOG.debug("updating typeProjet from " + typeProjet() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeProjet(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeProjet oldValue = typeProjet();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOProjet.TYPE_PROJET_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOProjet.TYPE_PROJET_KEY);
    }
  }
  
  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrats() {
    return (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)storedValueForKey(EOProjet.PROJET_CONTRATS_KEY);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrats(EOQualifier qualifier) {
    return projetContrats(qualifier, null, false);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrats(EOQualifier qualifier, boolean fetch) {
    return projetContrats(qualifier, null, fetch);
  }

  public NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> projetContrats(EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings, boolean fetch) {
    NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.PROJET_KEY, EOQualifier.QualifierOperatorEqual, this);
        
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray<EOQualifier> qualifiers = new NSMutableArray<EOQualifier>();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = projetContrats();
      if (qualifier != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat>)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToProjetContrats(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    includeObjectIntoPropertyWithKey(object, EOProjet.PROJET_CONTRATS_KEY);
  }

  public void removeFromProjetContrats(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    excludeObjectFromPropertyWithKey(object, EOProjet.PROJET_CONTRATS_KEY);
  }

  public void addToProjetContratsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    if (EOProjet.LOG.isDebugEnabled()) {
      EOProjet.LOG.debug("adding " + object + " to projetContrats relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        addToProjetContrats(object);
    }
    else {
        addObjectToBothSidesOfRelationshipWithKey(object, EOProjet.PROJET_CONTRATS_KEY);
    }
  }

  public void removeFromProjetContratsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    if (EOProjet.LOG.isDebugEnabled()) {
      EOProjet.LOG.debug("removing " + object + " from projetContrats relationship");
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        removeFromProjetContrats(object);
    }
    else {
        removeObjectFromBothSidesOfRelationshipWithKey(object, EOProjet.PROJET_CONTRATS_KEY);
    }
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat createProjetContratsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName( org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat.ENTITY_NAME );
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EOProjet.PROJET_CONTRATS_KEY);
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat) eo;
  }

  public void deleteProjetContratsRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EOProjet.PROJET_CONTRATS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllProjetContratsRelationships() {
    Enumeration<org.cocktail.fwkcktlgfcoperations.server.metier.operation.ProjetContrat> objects = projetContrats().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteProjetContratsRelationship(objects.nextElement());
    }
  }


  public static Projet create(EOEditingContext editingContext) {
    Projet eo = (Projet) EOUtilities.createAndInsertInstance(editingContext, EOProjet.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<Projet> fetchSpec() {
    return new ERXFetchSpecification<Projet>(EOProjet.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<Projet> fetchAll(EOEditingContext editingContext) {
    return EOProjet.fetchAll(editingContext, null);
  }

  public static NSArray<Projet> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOProjet.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<Projet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<Projet> fetchSpec = new ERXFetchSpecification<Projet>(EOProjet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<Projet> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static Projet fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOProjet.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Projet fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<Projet> eoObjects = EOProjet.fetchAll(editingContext, qualifier, null);
    Projet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeProjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Projet fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOProjet.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static Projet fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    Projet eoObject = EOProjet.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeProjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static Projet localInstanceIn(EOEditingContext editingContext, Projet eo) {
    Projet localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}