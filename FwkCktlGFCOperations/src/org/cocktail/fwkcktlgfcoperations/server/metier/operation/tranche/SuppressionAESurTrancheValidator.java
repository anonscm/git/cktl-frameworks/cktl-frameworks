package org.cocktail.fwkcktlgfcoperations.server.metier.operation.tranche;

import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;

public class SuppressionAESurTrancheValidator {

	private GestionBudgetSurTrancheNotificationHandler handler;
	private FwkCktlGFCOperationsApplicationUser user;
	
	public SuppressionAESurTrancheValidator(GestionBudgetSurTrancheNotificationHandler handler,
			FwkCktlGFCOperationsApplicationUser user) {
		this.handler = handler;
		this.user = user;
	}
	
	public boolean isValid(Tranche tranche) {
		boolean isValid = true;
        if (!hasDroitPositionnerBudget()) {
        	handler.setErrDroitPositionnerBudget(true);
            isValid = false;
        } else if (tranche.estVerrouillee()) {
            handler.setErrTrancheVerrouillee(true);
            isValid = false;
        } else if (!tranche.estBrouillon()) {
        	handler.setErrTrancheIntegree(true);
        	isValid = false;
        }
        
        return isValid;
	}
	
    private boolean hasDroitPositionnerBudget() {
        return user.hasDroitPositionnerBudget();
    }
}
