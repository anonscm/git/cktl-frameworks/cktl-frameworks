package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;
import java.math.RoundingMode;

import org.apache.log4j.Logger;

public class FraisGestion extends EOFraisGestion {
    @SuppressWarnings("unused")
    private static Logger log = Logger.getLogger(FraisGestion.class);
    

    public void recalculerFrais() {
        setFgPctIntermediaire(montantPourcentageTranche());
    }
    
    public BigDecimal getFgMontantIntermediaire() {
        return montant();
    }
    
    public void setFgMontantIntermediaire(BigDecimal value) {
    	if(value == null) {
    		value = BigDecimal.ZERO;
    	}
        super.setMontant(value);
        // On recalcule le pourcentage
//        BigDecimal pct = 
        BigDecimal montantParticip = repartPartenaireTranche().montantParticipation();
        if (montantParticip != null && montantParticip.compareTo(BigDecimal.ZERO) != 0) {
            BigDecimal pct = value.divide(montantParticip, 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100));
            setMontantPourcentageTranche(pct);
        } else {
            setMontant(BigDecimal.ZERO);
            setMontantPourcentageTranche(BigDecimal.ZERO);
        }
    }
    
    public BigDecimal getFgPctIntermediaire() {
        return montantPourcentageTranche();
    }
    
    public void setFgPctIntermediaire(BigDecimal value) {
    	if(value == null) {
    		value = BigDecimal.ZERO;
    	}
        super.setMontantPourcentageTranche(value);
        // On recalcule le montant 
        BigDecimal montantParticip = repartPartenaireTranche().montantParticipation();
        if (montantParticip != null) {
            BigDecimal montant = montantParticip.multiply(value).divide(BigDecimal.valueOf(100), 2, RoundingMode.HALF_UP);
            setMontant(montant);
        }
    }
    
    @Override
    public void validateForSave() throws ValidationException {
    	super.validateForSave();
    	repartPartenaireTranche().checkMontantContributionAvecFrais();
    }
}
