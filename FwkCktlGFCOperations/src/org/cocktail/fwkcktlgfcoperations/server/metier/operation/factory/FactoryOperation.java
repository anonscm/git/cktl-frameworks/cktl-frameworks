package org.cocktail.fwkcktlgfcoperations.server.metier.operation.factory;

import org.cocktail.fwkcktlgfceos.server.metier.EOUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionArgument;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionUtilisateur;
import org.cocktail.fwkcktlgfcoperations.common.tools.factory.Factory;
import org.cocktail.fwkcktlgfcoperations.server.metier.gfc.finder.core.FinderExerciceCocktail;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Projet;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartIndicateursContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Tranche;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeClassificationContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeReconduction;
import org.cocktail.fwkcktlpersonne.common.metier.EONaf;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Michael HALLER, Consortium Cocktail, 2008
 */
public class FactoryOperation extends Factory
{
	protected FactoryAvenant avenantFactory;

	/**
	 * @param ec
	 * @param withtrace
	 */
	public FactoryOperation(final EOEditingContext ec, final Boolean withtrace) {
		super(ec, withtrace);

		this.avenantFactory = new FactoryAvenant(ec, withtrace);
	}

	/**
	 * Creation d'un objet Contrat.
	 * 
	 * @param typeClassificationContrat
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'objet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat cree
	 * @throws InstantiationException
	 * @throws ExceptionFinder
	 */
	public Operation creerContrat(
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur) throws ExceptionUtilisateur, Exception {

		trace("creerContrat()");
		trace(typeContrat);
		trace(referenceExterne);
		trace(objet);
		trace(observations);
		trace(dateCloture);
		trace(typeReconduction);
		trace(createur);

		Operation operation = creerContratVierge(typeClassificationContrat, createur);

		modifierContrat(
				operation,
				typeClassificationContrat,
				referenceExterne,
				etablissementGestionnaire,
				centreResponsabiliteGestionnaire,
				typeContrat,
				objet,
				observations,
				dateCloture,
				typeReconduction,
				naf,
				null,
				null);

		return operation;
	}

	/**
	 * Creation d'un objet Contrat.
	 * 
	 * @param projet
	 * @param typeClassificationContrat
	 * @param etablissementGestionnaire Etablissement gestionnaire de la convention (obligatoire)
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite gestionnaire de la convention (obligatoire)
	 * @param typeContrat Type de la convention, ex: Convention de recherche (obligatoire)
	 * @param referenceExterne Reference externe de la convention.
	 * @param objet Intitule de l'objet de la convention (obligatoire)
	 * @param observations Observation diverses
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction
	 * @param naf code naf (domaine fonctionnel)
	 * @param createur Utilisateur createur de la convention (obligatoire)
	 * @return Le contrat cree
	 * @throws InstantiationException
	 * @throws ExceptionFinder
	 */
	public Operation creerContrat(
			final Projet projet,
			final TypeClassificationContrat typeClassificationContrat,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String referenceExterne,
			final String objet,
			final String observations,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur createur) throws ExceptionUtilisateur, Exception {

		Operation newContrat = creerContrat(typeClassificationContrat, etablissementGestionnaire, centreResponsabiliteGestionnaire, typeContrat, referenceExterne, objet, observations, dateCloture, typeReconduction, naf, createur);

		FactoryProjet fp = new FactoryProjet(ec);
		fp.ajouterContrat(projet, newContrat);

		return newContrat;
	}

	/**
	 * Creation d'un objet Contrat minimum.
	 * 
	 * @param createur Utilisateur createur de l'objet (obligatoire)
	 * @return Le contrat vierge cree
	 * @throws InstantiationException
	 * @throws ExceptionFinder
	 */
	public Operation creerContratVierge(final TypeClassificationContrat typeClassificationContrat, final EOUtilisateur createur) throws Exception, ExceptionUtilisateur {

		Operation operation = Operation.instanciate(ec);

		operation.setConGroupeBud(Operation.CON_GROUPE_BUD_NON);
		operation.setConSuppr(Operation.CON_SUPPR_NON);
		operation.setConDateCreation(getDateJour());
		operation.setUtilisateurCreationRelationship(createur);
		operation.setUtilisateurModifRelationship(createur);
		operation.setTypeClassificationContratRelationship(typeClassificationContrat);

		operation.setExerciceCocktailRelationship(new FinderExerciceCocktail(ec).findExerciceCocktailCourant());

		// TODO completer avec les evolutions demander sur l'ecran Generalites + nouveaux champs
		operation.setDateCreation(getDateJour());
		operation.setPersIdCreation(createur.personne().persId());
		operation.setEstFlechee(Boolean.FALSE);
		operation.setEstInclusePPIEtablissement(Boolean.FALSE);
		operation.setEstIncluseTableauxBudgetaires(Boolean.FALSE);
		operation.setExisteContrainteEligibiliteDepenses(Boolean.FALSE);
		operation.setEstConventionRA(Boolean.FALSE);
		
		return operation;
	}

	/**
	 * Modification d'un contrat
	 * 
	 * @param operation Contrat correspondant a la convention a modifier.
	 * @param referenceExterne Reference externe facultative.
	 * @param etablissementGestionnaire Etablissement gestionnaire du contrat.
	 * @param centreResponsabiliteGestionnaire Centre de responsabilite du contrat
	 * @param typeContrat Type de contrat (ex: Convention de formation)
	 * @param libelle Objet du contrat
	 * @param description Remarques eventuelles
	 * @param dateCloture Date de cloture de la convention
	 * @param typeReconduction Type de reconduction (ex: Expresse)
	 * @param naf code naf (domaine fonctionnel)
	 * @param utilisateur Utilisateur modificateur du contrat.
	 * @param dateModification Pour forcer une date de modification de l'avenant.
	 * @throws ExceptionUtilisateur, Exception
	 */
	public void modifierContrat(
			final Operation operation,
			final TypeClassificationContrat typeClassificationContrat,
			final String referenceExterne,
			final EOStructure etablissementGestionnaire,
			final EOStructure centreResponsabiliteGestionnaire,
			final TypeContrat typeContrat,
			final String libelle,
			final String description,
			final NSTimestamp dateCloture,
			final TypeReconduction typeReconduction,
			final EONaf naf,
			final EOUtilisateur utilisateur,
			final NSTimestamp dateModification) throws ExceptionUtilisateur, Exception {

		if (operation == null) {
			throw new ExceptionUtilisateur("Le contrat doit \u00EAtre fourni.");
		}
		if (typeClassificationContrat == null) {
			throw new ExceptionUtilisateur("Le type de classification doit \u00EAtre fourni.");
		}
		if (etablissementGestionnaire == null) {
			throw new ExceptionUtilisateur("L'\u00E9tablissement gestionnaire doit \u00EAtre fourni.");
		}
		if (centreResponsabiliteGestionnaire == null) {
			throw new ExceptionUtilisateur("Le centre de responsabilit\u00E9 gestionnaire doit \u00EAtre fourni.");
		}
		if (typeContrat == null) {
			throw new ExceptionUtilisateur("Le type de contrat doit \u00EAtre fourni.");
		}
		if (typeReconduction == null) {
			throw new ExceptionUtilisateur("Le type de reconduction doit \u00EAtre fourni.");
		}
		if (libelle == null) {
			throw new ExceptionUtilisateur("L'objet doit \u00EAtre fourni.");
		}
		if (libelle.length() > Operation.LIBELLE_MAX_SIZE) {
			throw new ExceptionUtilisateur("Le libellé ne doit pas d\u00E9passer " + Operation.LIBELLE_MAX_SIZE + " caract\u00E8res.");
		}

		operation.setTypeClassificationContratRelationship(typeClassificationContrat);
		operation.setConReferenceExterne(referenceExterne);
		operation.setEtablissementRelationship(etablissementGestionnaire, utilisateur.toPersonne().persId());
		operation.setCentreResponsabiliteRelationship(centreResponsabiliteGestionnaire, utilisateur.toPersonne().persId());
		operation.setTypeContratRelationship(typeContrat);
		operation.setLibelle(libelle);
		operation.setDescription(description);
		operation.setConDateCloture(dateCloture);
		operation.setTypeReconductionRelationship(typeReconduction);
		operation.setCodeNafRelationship(naf);
		operation.setUtilisateurModifRelationship(utilisateur);
		operation.setConDateModif(dateModification);
	}

	/**
	 * Suppression d'un contrat. Il n'est pas supprime mais archive. Les avenants associes (zero et autres) ne sont pas supprimes.
	 * 
	 * @param operation Contrat a supprimer.
	 * @throws ExceptionArgument
	 */
	public void supprimerContrat(
			final Operation operation) throws ExceptionUtilisateur {
		_supprimerContrat(operation, false);
	}

	/**
	 * Suppression d'un contrat par un ADMIN. Il n'est pas supprime mais archive. Les avenants associes (zero et autres) ne sont pas supprimes. Un
	 * contrat validé peut être supprimée.
	 * 
	 * @param operation Contrat a supprimer.
	 * @throws ExceptionArgument
	 */
	public void supprimerContratAdmin(
			final Operation operation) throws ExceptionUtilisateur {
		_supprimerContrat(operation, true);
	}

	/**
	 * Suppression d'un contrat. Il n'est pas supprime mais archive. Les avenants associes (zero et autres) ne sont pas supprimes.
	 * 
	 * @param operation Contrat a supprimer.
	 * @param isAdmin <code>true</code> si l'utilisateur est un admin
	 * @throws ExceptionArgument
	 */
	private void _supprimerContrat(final Operation operation, final boolean isAdmin) throws ExceptionUtilisateur {
		trace("supprimerContrat() :: isAdmin=", new Boolean(isAdmin));
		trace(operation);

		// Verifier que la convention n'est pas validee
		if (!isAdmin && operation.conDateValidAdm() != null) {
			throw new ExceptionUtilisateur("La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n"
					+ "Il n'est donc plus possible de la supprimer.");
		}
		// Vérifier qu'il n'y a pas de crédits positionnées
		for (Tranche uneTranche : operation.tranches(Tranche.QUALIFIER_NON_SUPPR)) {
			if ((uneTranche.trancheBudgetsNonSupprimes() != null && !uneTranche.trancheBudgetsNonSupprimes().isEmpty())
					|| (uneTranche.trancheBudgetsRecNonSupprimes() != null && !uneTranche.trancheBudgetsRecNonSupprimes().isEmpty())) {
				throw new ExceptionUtilisateur("Suppression impossible !\nLa convention s\u00E9lectionn\u00E9e a des cr\u00E9dits positionn\u00E9s en d\u00E9pense ou recette.\n"
						+ "Vous devez les supprimer avant de pouvoir supprimer la convention.");
			}
		}
		operation.setConSuppr("O");
	}

	/**
	 * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais archives.
	 * 
	 * @param operation Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @throws ExceptionArgument
	 */
	public void supprimerAvenants(
			final Operation operation,
			final boolean avenantZeroAussi) throws ExceptionUtilisateur {
		_supprimerAvenant(operation, avenantZeroAussi, false);
	}

	/**
	 * Suppression des avenants d'un contrat par un ADMIN. Il ne sont pas supprimes mais archives.
	 * 
	 * @param operation Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @throws ExceptionArgument
	 */
	public void supprimerAvenantsAdmin(
			final Operation operation,
			final boolean avenantZeroAussi) throws ExceptionUtilisateur {
		_supprimerAvenant(operation, avenantZeroAussi, true);
	}

	/**
	 * Suppression des avenants d'un contrat. Il ne sont pas supprimes mais archives.
	 * 
	 * @param operation Contrat dont on veut supprimer les avenants.
	 * @param avenantZeroAussi Mettre a <code>true</code> pour supprimer egalement l'avenant "zero" (= contrat initial).
	 * @param isAdmin <code>true</code> si l'utilisateur est un admin
	 * @throws ExceptionUtilisateur
	 */
	private void _supprimerAvenant(final Operation operation, final boolean avenantZeroAussi, final boolean isAdmin) throws ExceptionUtilisateur {
		trace("supprimerAvenants() :: isAdmin=", new Boolean(isAdmin));
		trace(operation);
		trace(new Boolean(avenantZeroAussi));

		// Verifier que la convention n'est pas validee
		if (!isAdmin && operation.conDateValidAdm() != null) {
			throw new ExceptionUtilisateur("La convention s\u00E9lectionn\u00E9e a \u00E9t\u00E9 valid\u00E9e administrativement.\n"
					+ "Il n'est donc plus possible de la supprimer.");
		}

		for (int i = 0; i < operation.avenantsDontInitialNonSupprimes().count(); i++) {
			Avenant avenant = (Avenant) operation.avenantsDontInitialNonSupprimes().objectAtIndex(i);
			if (avenantZeroAussi || avenant.avtIndex().intValue() != 0) {
				this.avenantFactory.supprimerAvenant(avenant);
			}
		}
	}

	public void activerIndicateur(final Operation operation, final IndicateursContrat indicateur) {
		if (!operation.indicateursContrat().contains(indicateur)) {
			try {
				RepartIndicateursContrat ric = RepartIndicateursContrat.instanciate(ec);
				ric.setContratRelationship(operation);
				ric.setIndicateursContratRelationship(indicateur);
				ec.insertObject(ric);
				operation.addToRepartIndicateursContratRelationship(ric);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
	}

	public void desactiverIndicateur(final Operation operation, final IndicateursContrat indicateur) {
		NSArray rics = operation.repartIndicateursContrat();
		NSArray indicateurs = (NSArray) rics.valueForKeyPath("IndicateursContrat");
		if (indicateurs != null && indicateurs.contains(indicateur)) {
			EOKeyValueQualifier qual = new EOKeyValueQualifier("IndicateursContrat", EOKeyValueQualifier.QualifierOperatorEqual, indicateur);
			RepartIndicateursContrat ric = (RepartIndicateursContrat) EOQualifier.filteredArrayWithQualifier(rics, qual).lastObject();
			if (ric != null) {
				operation.removeFromRepartIndicateursContratRelationship(ric);
				ec.deleteObject(ric);
			}
		}
	}
}
