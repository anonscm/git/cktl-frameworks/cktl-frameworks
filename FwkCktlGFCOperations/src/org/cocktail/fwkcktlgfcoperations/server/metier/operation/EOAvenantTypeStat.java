// DO NOT EDIT.  Make changes to AvenantTypeStat.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOAvenantTypeStat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeAvenantTypeStat";
	public static final String ENTITY_TABLE_NAME = "GFC.AVENANT_TYPE_STAT";

  // Attribute Keys
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant> AVENANT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant>("avenant");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat> TYPE_STAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat>("typeStat");

  // Attributes
  // Relationships
  public static final String AVENANT_KEY = AVENANT.key();
  public static final String TYPE_STAT_KEY = TYPE_STAT.key();

  private static Logger LOG = Logger.getLogger(EOAvenantTypeStat.class);

  public AvenantTypeStat localInstanceIn(EOEditingContext editingContext) {
    AvenantTypeStat localInstance = (AvenantTypeStat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant)storedValueForKey(EOAvenantTypeStat.AVENANT_KEY);
  }
  
  public void setAvenant(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    takeStoredValueForKey(value, EOAvenantTypeStat.AVENANT_KEY);
  }

  public void setAvenantRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant value) {
    if (EOAvenantTypeStat.LOG.isDebugEnabled()) {
      EOAvenantTypeStat.LOG.debug("updating avenant from " + avenant() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setAvenant(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant oldValue = avenant();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantTypeStat.AVENANT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantTypeStat.AVENANT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat typeStat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat)storedValueForKey(EOAvenantTypeStat.TYPE_STAT_KEY);
  }
  
  public void setTypeStat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat value) {
    takeStoredValueForKey(value, EOAvenantTypeStat.TYPE_STAT_KEY);
  }

  public void setTypeStatRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat value) {
    if (EOAvenantTypeStat.LOG.isDebugEnabled()) {
      EOAvenantTypeStat.LOG.debug("updating typeStat from " + typeStat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setTypeStat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat oldValue = typeStat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EOAvenantTypeStat.TYPE_STAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EOAvenantTypeStat.TYPE_STAT_KEY);
    }
  }
  

  public static AvenantTypeStat create(EOEditingContext editingContext, org.cocktail.fwkcktlgfcoperations.server.metier.operation.Avenant avenant, org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeStat typeStat) {
    AvenantTypeStat eo = (AvenantTypeStat) EOUtilities.createAndInsertInstance(editingContext, EOAvenantTypeStat.ENTITY_NAME);    
    eo.setAvenantRelationship(avenant);
    eo.setTypeStatRelationship(typeStat);
    return eo;
  }

  public static ERXFetchSpecification<AvenantTypeStat> fetchSpec() {
    return new ERXFetchSpecification<AvenantTypeStat>(EOAvenantTypeStat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<AvenantTypeStat> fetchAll(EOEditingContext editingContext) {
    return EOAvenantTypeStat.fetchAll(editingContext, null);
  }

  public static NSArray<AvenantTypeStat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOAvenantTypeStat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<AvenantTypeStat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<AvenantTypeStat> fetchSpec = new ERXFetchSpecification<AvenantTypeStat>(EOAvenantTypeStat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<AvenantTypeStat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static AvenantTypeStat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantTypeStat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantTypeStat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<AvenantTypeStat> eoObjects = EOAvenantTypeStat.fetchAll(editingContext, qualifier, null);
    AvenantTypeStat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeAvenantTypeStat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantTypeStat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOAvenantTypeStat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static AvenantTypeStat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    AvenantTypeStat eoObject = EOAvenantTypeStat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeAvenantTypeStat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static AvenantTypeStat localInstanceIn(EOEditingContext editingContext, AvenantTypeStat eo) {
    AvenantTypeStat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}