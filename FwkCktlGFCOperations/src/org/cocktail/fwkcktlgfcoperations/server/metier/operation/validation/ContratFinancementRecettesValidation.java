package org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation;

import java.io.Serializable;
import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation;

public class ContratFinancementRecettesValidation implements Serializable {

    /** Serial version UID. */
    private static final long serialVersionUID = 1L;

    /**
     * Verifie que le total des contributions des partenaires est superieur ou égal 
     * à la somme des budgets de recettes positionnés.
     * 
     * @param operation le contrat a valider.
     * @return true si le financement du contrat au niveau des recettes est valide, false sinon.
     */
    public boolean isSatisfiedBy(Operation operation) {
        if (operation == null) {
            return false;
        }
        
        BigDecimal totalContributions = operation.totalContributions();
        BigDecimal totalPositionneRecContrat = operation.totalPositionneRecOperation();
        return totalContributions.subtract(totalPositionneRecContrat).doubleValue() >= 0;
    } 
    
}
