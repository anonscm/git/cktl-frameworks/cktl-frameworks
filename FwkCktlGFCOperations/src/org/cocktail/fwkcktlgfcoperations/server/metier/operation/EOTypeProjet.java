// DO NOT EDIT.  Make changes to TypeProjet.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EOTypeProjet extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeTypeProjet";
	public static final String ENTITY_TABLE_NAME = "GFC.TYPE_PROJET";

  // Attribute Keys
  public static final ERXKey<String> TPJT_CODE = new ERXKey<String>("tpjtCode");
  // Relationship Keys

  // Attributes
  public static final String TPJT_CODE_KEY = TPJT_CODE.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOTypeProjet.class);

  public TypeProjet localInstanceIn(EOEditingContext editingContext) {
    TypeProjet localInstance = (TypeProjet)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String tpjtCode() {
    return (String) storedValueForKey(EOTypeProjet.TPJT_CODE_KEY);
  }

  public void setTpjtCode(String value) {
    if (EOTypeProjet.LOG.isDebugEnabled()) {
        EOTypeProjet.LOG.debug( "updating tpjtCode from " + tpjtCode() + " to " + value);
    }
    takeStoredValueForKey(value, EOTypeProjet.TPJT_CODE_KEY);
  }


  public static TypeProjet create(EOEditingContext editingContext) {
    TypeProjet eo = (TypeProjet) EOUtilities.createAndInsertInstance(editingContext, EOTypeProjet.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<TypeProjet> fetchSpec() {
    return new ERXFetchSpecification<TypeProjet>(EOTypeProjet.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<TypeProjet> fetchAll(EOEditingContext editingContext) {
    return EOTypeProjet.fetchAll(editingContext, null);
  }

  public static NSArray<TypeProjet> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOTypeProjet.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<TypeProjet> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<TypeProjet> fetchSpec = new ERXFetchSpecification<TypeProjet>(EOTypeProjet.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<TypeProjet> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static TypeProjet fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeProjet.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeProjet fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<TypeProjet> eoObjects = EOTypeProjet.fetchAll(editingContext, qualifier, null);
    TypeProjet eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeTypeProjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeProjet fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOTypeProjet.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static TypeProjet fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    TypeProjet eoObject = EOTypeProjet.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeTypeProjet that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static TypeProjet localInstanceIn(EOEditingContext editingContext, TypeProjet eo) {
    TypeProjet localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}