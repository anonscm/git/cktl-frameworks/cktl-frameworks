// DO NOT EDIT.  Make changes to RepartIndicateursContrat.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import com.webobjects.eoaccess.*;
import com.webobjects.eocontrol.*;
import com.webobjects.foundation.*;
import java.math.*;
import java.util.*;
import org.apache.log4j.Logger;

import er.extensions.eof.*;
import er.extensions.foundation.*;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

@SuppressWarnings("all")
public abstract class EORepartIndicateursContrat extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeRepartIndicateursContrat";
	public static final String ENTITY_TABLE_NAME = "GFC.REPART_INDICATEURS_CONTRAT";

  // Attribute Keys
  public static final ERXKey<Integer> IC_ID = new ERXKey<Integer>("icId");
  public static final ERXKey<Integer> ID_OPE_OPERATION = new ERXKey<Integer>("idOpeOperation");
  // Relationship Keys
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation> CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation>("contrat");
  public static final ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat> INDICATEURS_CONTRAT = new ERXKey<org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat>("indicateursContrat");

  // Attributes
  public static final String IC_ID_KEY = IC_ID.key();
  public static final String ID_OPE_OPERATION_KEY = ID_OPE_OPERATION.key();
  // Relationships
  public static final String CONTRAT_KEY = CONTRAT.key();
  public static final String INDICATEURS_CONTRAT_KEY = INDICATEURS_CONTRAT.key();

  private static Logger LOG = Logger.getLogger(EORepartIndicateursContrat.class);

  public RepartIndicateursContrat localInstanceIn(EOEditingContext editingContext) {
    RepartIndicateursContrat localInstance = (RepartIndicateursContrat)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public Integer icId() {
    return (Integer) storedValueForKey(EORepartIndicateursContrat.IC_ID_KEY);
  }

  public void setIcId(Integer value) {
    if (EORepartIndicateursContrat.LOG.isDebugEnabled()) {
        EORepartIndicateursContrat.LOG.debug( "updating icId from " + icId() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartIndicateursContrat.IC_ID_KEY);
  }

  public Integer idOpeOperation() {
    return (Integer) storedValueForKey(EORepartIndicateursContrat.ID_OPE_OPERATION_KEY);
  }

  public void setIdOpeOperation(Integer value) {
    if (EORepartIndicateursContrat.LOG.isDebugEnabled()) {
        EORepartIndicateursContrat.LOG.debug( "updating idOpeOperation from " + idOpeOperation() + " to " + value);
    }
    takeStoredValueForKey(value, EORepartIndicateursContrat.ID_OPE_OPERATION_KEY);
  }

  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation contrat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation)storedValueForKey(EORepartIndicateursContrat.CONTRAT_KEY);
  }
  
  public void setContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    takeStoredValueForKey(value, EORepartIndicateursContrat.CONTRAT_KEY);
  }

  public void setContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation value) {
    if (EORepartIndicateursContrat.LOG.isDebugEnabled()) {
      EORepartIndicateursContrat.LOG.debug("updating contrat from " + contrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setContrat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.Operation oldValue = contrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartIndicateursContrat.CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartIndicateursContrat.CONTRAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat indicateursContrat() {
    return (org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat)storedValueForKey(EORepartIndicateursContrat.INDICATEURS_CONTRAT_KEY);
  }
  
  public void setIndicateursContrat(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat value) {
    takeStoredValueForKey(value, EORepartIndicateursContrat.INDICATEURS_CONTRAT_KEY);
  }

  public void setIndicateursContratRelationship(org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat value) {
    if (EORepartIndicateursContrat.LOG.isDebugEnabled()) {
      EORepartIndicateursContrat.LOG.debug("updating indicateursContrat from " + indicateursContrat() + " to " + value);
    }
    if (er.extensions.eof.ERXGenericRecord.InverseRelationshipUpdater.updateInverseRelationships()) {
        setIndicateursContrat(value);
    }
    else if (value == null) {
        org.cocktail.fwkcktlgfcoperations.server.metier.operation.IndicateursContrat oldValue = indicateursContrat();
        if (oldValue != null) {
            removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EORepartIndicateursContrat.INDICATEURS_CONTRAT_KEY);
      }
    } else {
        addObjectToBothSidesOfRelationshipWithKey(value, EORepartIndicateursContrat.INDICATEURS_CONTRAT_KEY);
    }
  }
  

  public static RepartIndicateursContrat create(EOEditingContext editingContext) {
    RepartIndicateursContrat eo = (RepartIndicateursContrat) EOUtilities.createAndInsertInstance(editingContext, EORepartIndicateursContrat.ENTITY_NAME);    
    return eo;
  }

  public static ERXFetchSpecification<RepartIndicateursContrat> fetchSpec() {
    return new ERXFetchSpecification<RepartIndicateursContrat>(EORepartIndicateursContrat.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<RepartIndicateursContrat> fetchAll(EOEditingContext editingContext) {
    return EORepartIndicateursContrat.fetchAll(editingContext, null);
  }

  public static NSArray<RepartIndicateursContrat> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EORepartIndicateursContrat.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<RepartIndicateursContrat> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<RepartIndicateursContrat> fetchSpec = new ERXFetchSpecification<RepartIndicateursContrat>(EORepartIndicateursContrat.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<RepartIndicateursContrat> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static RepartIndicateursContrat fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartIndicateursContrat.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartIndicateursContrat fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<RepartIndicateursContrat> eoObjects = EORepartIndicateursContrat.fetchAll(editingContext, qualifier, null);
    RepartIndicateursContrat eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeRepartIndicateursContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartIndicateursContrat fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EORepartIndicateursContrat.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static RepartIndicateursContrat fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    RepartIndicateursContrat eoObject = EORepartIndicateursContrat.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeRepartIndicateursContrat that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static RepartIndicateursContrat localInstanceIn(EOEditingContext editingContext, RepartIndicateursContrat eo) {
    RepartIndicateursContrat localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}