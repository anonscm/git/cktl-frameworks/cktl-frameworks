
package org.cocktail.fwkcktlgfcoperations.server.metier.operation.finder.core;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.TypeContrat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;

/**
 * 
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 * 
 */
public class FinderTypeContrat extends Finder 
{
	protected String tyconIdInterne;
	
	protected EOQualifier tyconIdInterneQualifier;
	
	
	/**
	 * Constructeur.
	 * @param ec Editing context de travail.
	 */
	public FinderTypeContrat(EOEditingContext ec) {
		super(ec, TypeContrat.ENTITY_NAME);
		
	}

	/**
	 * Change la valeur du critere.
	 * @param tyconIdInterne Id interne du type de contrat.
	 */
	protected void setTyconIdInterne(final String tyconIdInterne) {
		this.tyconIdInterneQualifier = createQualifier(
				"tyconIdInterne = %@", 
				tyconIdInterne);
		
		this.tyconIdInterne = tyconIdInterne;
	}
	
	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#clearAllCriteria()
	 */
	public void clearAllCriteria() {
		setTyconIdInterne(null);
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#find()
	 */
	public TypeContrat findWithTyconIdInterne(final String tyconIdInterne) throws ExceptionFinder {
		removeOptionalQualifiers();
		setTyconIdInterne(tyconIdInterne);
		addOptionalQualifier(this.tyconIdInterneQualifier);
		
		return (TypeContrat) super.find().lastObject();
	}
	
//	/**
//	 * @see org.cocktail.cocowork.eof.client.commun.Finder#find()
//	 */
//	public NSArray find() throws FinderException {
//		removeOptionalQualifiers();
//		addOptionalQualifier(this.tyconIdInterneQualifier);
//		
//		return super.find();
//	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}

}
