package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public class ReportInfo {

    public static final String RETRAIT_REPART_SOURCE_KEY = "retraitRepartSource";
    public static final String AJOUT_REPART_DESTINATION_KEY = "ajoutRepartDestination";
    private RepartPartenaireTranche repartSource;
    private RepartPartenaireTranche repartDestination;
    private BigDecimal montantRepartSource;
    private BigDecimal montantRepartDestination;

    public ReportInfo(RepartPartenaireTranche repartSource, Tranche trancheDest) {
        super();
        this.repartSource = repartSource;
        this.montantRepartSource = repartSource.montantParticipation();
        this.repartDestination = montantRepartDestinationFromSource(repartSource, trancheDest);
        if (this.repartDestination != null) {
            this.montantRepartDestination = repartDestination.montantParticipation();
        } else {
            this.montantRepartDestination = BigDecimal.ZERO;
        }
    }

    private RepartPartenaireTranche montantRepartDestinationFromSource(RepartPartenaireTranche repartSource, Tranche trancheDestination) {
        Integer contribPersId = repartSource.operationPartenaire().persId();
        // On va chercher le repart partenaire sur la tranche destination
        EOQualifier qual = RepartPartenaireTranche.OPERATION_PARTENAIRE.dot(OperationPartenaire.PERS_ID).eq(contribPersId);
        NSArray<RepartPartenaireTranche> repartsDest = trancheDestination.toRepartPartenaireTranches(qual);
        return repartsDest.isEmpty() ? null : repartsDest.lastObject();
    }

    public void reporter() {
        repartDestination.setMontantParticipation(getMontantRepartDestination());
        repartSource.setMontantParticipation(getMontantRepartSource());
    }

    public String libelleContributeur() {
        return repartSource.operationPartenaire().partenaire().libelleEtId();
    }
    
    public BigDecimal retraitRepartSource() {
        return repartSource.montantParticipation().subtract(getMontantRepartSource()).abs();
    }
    
    public BigDecimal ajoutRepartDestination() {
        return repartDestination.montantParticipation().subtract(getMontantRepartDestination()).abs();
    }
    
    public BigDecimal getMontantRepartDestination() {
        return montantRepartDestination;
    }

    public void setMontantRepartDestination(BigDecimal montantRepartDestination) {
        this.montantRepartDestination = montantRepartDestination;
    }

    public BigDecimal getMontantRepartSource() {
        return montantRepartSource;
    }

    public void setMontantRepartSource(BigDecimal montantRepartSource) {
        if (montantRepartSource.compareTo(repartSource.montantParticipation()) == -1) {
            this.montantRepartSource = montantRepartSource;
        }  
    }

}