package org.cocktail.fwkcktlgfcoperations.server.metier.operation.procedure.suivirecette;

import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionApplication;

import com.webobjects.eocontrol.EOEditingContext;


/**
 * Procedure qui calcule le total des recettes au titre d'une convention sur un exercice, 
 * pour un type de credit et une ligne de l'organigramme budgetaire.
 * 
 */
public class ProcedureGetTotalRecette extends ProcedureSuiviRecette
{
	protected final static String PROCEDURE_NAME = "GetTotalRecette";
	
	
	/**
	 * Constructeur protected.
	 * @param ec Editing context a utiliser.
	 * @throws ExceptionApplication 
	 */
	public ProcedureGetTotalRecette(final EOEditingContext ec) throws ExceptionApplication { 
		super(ec, PROCEDURE_NAME);
		throw new ExceptionApplication("Ancienne utilisation de typeDeCredit à faire évoluer");
	}
	
}
