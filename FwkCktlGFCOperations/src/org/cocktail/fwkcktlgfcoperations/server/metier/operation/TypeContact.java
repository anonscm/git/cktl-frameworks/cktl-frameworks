

// TypeContact.java
// 

package org.cocktail.fwkcktlgfcoperations.server.metier.operation;



public class TypeContact extends EOTypeContact
{
    
    public static final String LC_RESP_SCIENTIFIQUE = "RS";
    public static final String LC_RESP_ADMINISTRATIF = "RA";
    
    public TypeContact() {
        super();
    }

/*
    // If you add instance variables to store property values you
    // should add empty implementions of the Serialization methods
    // to avoid unnecessary overhead (the properties will be
    // serialized for you in the superclass).
    private void writeObject(java.io.ObjectOutputStream out) throws java.io.IOException {
    }

    private void readObject(java.io.ObjectInputStream in) throws java.io.IOException, java.lang.ClassNotFoundException {
    }
*/

}
