package org.cocktail.fwkcktlgfcoperations.server.metier.personne.service;

import org.cocktail.fwkcktlgfcoperations.server.FwkCktlGFCOperationsApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class PersonneService {

	private EOEditingContext edc;
	
	public PersonneService(EOEditingContext edc) {
		this.edc = edc;
	}
	
	public EOStructure getServiceAffectation(Integer persId) {
		FwkCktlGFCOperationsApplicationUser opAppUser = new FwkCktlGFCOperationsApplicationUser(edc(), persId);
		NSArray services = opAppUser.getServices();
		if (services == null) {
			return null;
		}
		
		return (EOStructure) services.lastObject();
	}
	
	public EOStructure getEtablissementAffectation(Integer persId) {
		FwkCktlGFCOperationsApplicationUser opAppUser = new FwkCktlGFCOperationsApplicationUser(edc(), persId);
		NSArray etablissements = opAppUser.getEtablissementsAffectation();
		if (etablissements == null || etablissements.count() != 1) {
			throw new IllegalStateException("Impossible de détecter votre établissement d'affectation");
		}
		
		return (EOStructure) etablissements.lastObject();
	}
	
	private EOEditingContext edc() {
		return this.edc;
	}
}
