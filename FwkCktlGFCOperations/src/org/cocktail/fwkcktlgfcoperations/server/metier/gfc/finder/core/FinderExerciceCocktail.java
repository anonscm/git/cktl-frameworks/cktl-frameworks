package org.cocktail.fwkcktlgfcoperations.server.metier.gfc.finder.core;

import java.util.Calendar;
import java.util.GregorianCalendar;

import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionFinder;
import org.cocktail.fwkcktlgfcoperations.common.exception.ExceptionNotFound;
import org.cocktail.fwkcktlgfcoperations.common.finder.Finder;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;


/**
 * 
 * @author Bertrand GAUTHIER, Consortium Cocktail, 2007
 *
 */
public class FinderExerciceCocktail extends Finder
{
	protected Number exeExercice;
	protected Number exeOrdre;
	
	protected EOQualifier qualifierExeExercice;
	protected EOQualifier qualifierExeOrdre;
	

	/**
	 * Constructeur.
	 * @param ec
	 * @param entityName
	 */
	public FinderExerciceCocktail(EOEditingContext ec, String entityName) {
		super(ec, entityName);
	}
	/**
	 * Constructeur.
	 * @param ec
	 */
	public FinderExerciceCocktail(final EOEditingContext ec) {
		this(ec, EOExerciceCocktail.ENTITY_NAME);
		
	}

	/**
	 * Change le critere.
	 * @param exeExercice Annee de l'exercice (ex: 2006)
	 */
	protected void setExeExercice(final Number exeExercice) {
		this.qualifierExeExercice = createQualifier(
				"exeExercice = %@",
				exeExercice);
		this.exeExercice = exeExercice;
	}
	/**
	 * Change le critere.
	 * @param exeOrdre Identifiant de l'exercice
	 */
	protected void setExeOrdre(final Number exeOrdre) {
		this.qualifierExeOrdre = createQualifier(
				"exeOrdre = %@", 
				exeOrdre);
		this.exeOrdre = exeOrdre;
	}

	
	/**
	 * Supprime tous les criteres de recherche courants
	 */
	public void clearAllCriteria() {
		setExeExercice(null);
		setExeOrdre(null);
	}

    /**
     * Determine la liste des exercices EXISTANT dans JEFY_ADMIN.EXERCICE_COCKTAIL et correspondants a une periode de temps. 
     * @param ec Editing context a utiliser.
     * @param dateDebut Date de debut de periode ou null.
     * @param dateFin Date de fin de periode ou null.
     * @throws ExceptionFinder 
     */
    public NSArray findWithDatesDebutFin(
    		final NSTimestamp dateDebut, 
    		final NSTimestamp dateFin) throws ExceptionFinder {
    	
    	NSMutableArray qualifiers = new NSMutableArray();
    	GregorianCalendar cal;
    	
    	if (dateDebut != null) {
    		cal = new GregorianCalendar();
    		cal.setTime(dateDebut);
			qualifiers.addObject(
        		EOQualifier.qualifierWithQualifierFormat(
        				"exeExercice>=%@", 
        				new NSArray(new Integer(cal.get(GregorianCalendar.YEAR)))));
    	}
    	if (dateFin != null) {
    		cal = new GregorianCalendar();
    		cal.setTime(dateFin);
    		qualifiers.addObject(
	        		EOQualifier.qualifierWithQualifierFormat(
	        				"exeExercice<=%@", 
	        				new NSArray(new Integer(cal.get(GregorianCalendar.YEAR)))));
    	}

		clearAllCriteria();
		removeOptionalQualifiers();

    	if (qualifiers.count() > 0) 
    		addFreeQualifier(new EOAndQualifier(qualifiers));
		
    	return super.find();
    }
    
	/**
	 * Recherche l'exercice courant dans JEFY_ADMIN.EXERCICE_COCKTAIL, correspondant a l'annee civile courante.
	 * @return L'exercice trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExerciceCocktail findExerciceCocktailCourant() throws ExceptionFinder {
		
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeExercice(new Integer(new GregorianCalendar().get(Calendar.YEAR)));
		addOptionalQualifier(qualifierExeExercice);
		
		return (EOExerciceCocktail) super.find().lastObject();
	}
	/**
	 * Recherche l'exercice courant dans JEFY_ADMIN.EXERCICE_COCKTAIL, correspondant a l'annee civile courante, 
	 * avec erreur si rien n'est trouve.
	 * @return L'exercice trouve.
	 * @throws ExceptionNotFound Pas trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExerciceCocktail findMandatoryExerciceCocktailCourant() throws ExceptionFinder, ExceptionNotFound {

		EOExerciceCocktail eo = findExerciceCocktailCourant();
		if (eo == null)
			throw new ExceptionNotFound("L'exercice courant n'a pas \u00E9t\u00E9 trouv\u00E9.");
		
		return eo;
	}
	/**
	 * Recherche l'exercice courant dans JEFY_ADMIN.EXERCICE_COCKTAIL, correspondant a l'annee civile precedente.
	 * @return L'exercice trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExerciceCocktail findExerciceCocktailPrecedent() throws ExceptionFinder {
		
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeExercice(new Integer(new GregorianCalendar().get(Calendar.YEAR) - 1));
		addOptionalQualifier(qualifierExeExercice);
		
		return (EOExerciceCocktail) super.find().lastObject();
	}
	/**
	 * Recherche l'exercice precedent dans JEFY_ADMIN.EXERCICE_COCKTAIL, correspondant a l'annee civile precedente, 
	 * avec erreur si rien n'est trouve.
	 * @return L'exercice trouve.
	 * @throws ExceptionNotFound Pas trouve.
	 * @throws ExceptionFinder 
	 */
	public EOExerciceCocktail findMandatoryExerciceCocktailPrecedent() throws ExceptionFinder, ExceptionNotFound {

		EOExerciceCocktail eo = findExerciceCocktailPrecedent();
		if (eo == null) {
			throw new ExceptionNotFound("L'exercice precedent n'a pas \u00E9t\u00E9 trouv\u00E9.");
		}
		
		return eo;
	}

	public EOExerciceCocktail findExerciceCocktailSuivant(EOExerciceCocktail exerciceCocktail) throws ExceptionFinder {
		if (exerciceCocktail == null) {
			throw new ExceptionFinder("L'exercice cocktail de référence est obligatoire pour déterminer l'exercice suivant");
		}
		clearAllCriteria();
		removeOptionalQualifiers();
		
		setExeExercice(exerciceCocktail.exeExercice() + 1);
		addOptionalQualifier(qualifierExeExercice);
		
		return (EOExerciceCocktail) super.find().lastObject();
	}

	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public EOExerciceCocktail findWithExeExercice(final Number exeExercice) throws ExceptionFinder { 
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeExercice(exeExercice);
		addOptionalQualifier(this.qualifierExeExercice);
		
		return (EOExerciceCocktail) super.find().lastObject();
	}
	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public EOExerciceCocktail findWithExeOrdre(final Number exeOrdre) throws ExceptionFinder { 
		clearAllCriteria();
		removeOptionalQualifiers();
		setExeOrdre(exeOrdre);
		addOptionalQualifier(this.qualifierExeOrdre);
		
		return (EOExerciceCocktail) super.find().lastObject();
	}

	/**
	 * Recherche les exercices selon les criteres courants.
	 */
	public NSArray find() throws ExceptionFinder { 
		
		this.removeOptionalQualifiers();
		this.addOptionalQualifier(this.qualifierExeExercice);
		
		return super.find();
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#canFind()
	 */
	public boolean canFind() {
		// TODO Auto-generated method stub
		return true;
	}

	/**
	 * @see org.cocktail.javaclientutilities.finder.Finder#getCurrentWarningMessage()
	 */
	public String getCurrentWarningMessage() {
		// TODO Auto-generated method stub
		return null;
	}
}
