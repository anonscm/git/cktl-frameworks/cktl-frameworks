

// IndividuUlr.java
// 
package org.cocktail.fwkcktlgfcoperations.server.metier.grhum;

import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;



public class IndividuUlr extends EOIndividu
{

    
    public String nomEtPrenom() {
    	StringBuffer buf = new StringBuffer();
    	buf.append(this.nomUsuel());
    	buf.append(' ');
    	buf.append(this.prenom());
    	return buf.toString();
    }

}
