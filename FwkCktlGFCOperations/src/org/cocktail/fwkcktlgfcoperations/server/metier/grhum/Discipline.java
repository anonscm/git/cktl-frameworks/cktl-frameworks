// Discipline.java
// Created on Fri Dec 12 10:40:17 Europe/Paris 2008 by Apple EOModeler Version 5.2

package org.cocktail.fwkcktlgfcoperations.server.metier.grhum;

import er.extensions.eof.ERXKey;


public class Discipline extends EODiscipline {

    public static final ERXKey<Integer> DISC_ORDRE = new ERXKey<Integer>("discOrdre");
    
    public Discipline() {
        super();
    }
}
