// DO NOT EDIT.  Make changes to OpeDiscipline.java instead.
package org.cocktail.fwkcktlgfcoperations.server.metier.grhum;

import java.util.NoSuchElementException;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.database.CktlServerRecord;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXFetchSpecification;
import er.extensions.eof.ERXKey;

@SuppressWarnings("all")
public abstract class EOOpeDiscipline extends  CktlServerRecord {
  public static final String ENTITY_NAME = "OpeDiscipline";

  // Attribute Keys
  public static final ERXKey<String> DISC_LIBELLE_COURT = new ERXKey<String>("discLibelleCourt");
  public static final ERXKey<String> DISC_LIBELLE_EDITION = new ERXKey<String>("discLibelleEdition");
  public static final ERXKey<String> DISC_LIBELLE_LONG = new ERXKey<String>("discLibelleLong");
  // Relationship Keys

  // Attributes
  public static final String DISC_LIBELLE_COURT_KEY = DISC_LIBELLE_COURT.key();
  public static final String DISC_LIBELLE_EDITION_KEY = DISC_LIBELLE_EDITION.key();
  public static final String DISC_LIBELLE_LONG_KEY = DISC_LIBELLE_LONG.key();
  // Relationships

  private static Logger LOG = Logger.getLogger(EOOpeDiscipline.class);

  public OpeDiscipline localInstanceIn(EOEditingContext editingContext) {
    OpeDiscipline localInstance = (OpeDiscipline)EOUtilities.localInstanceOfObject(editingContext, this);
    if (localInstance == null) {
      throw new IllegalStateException("You attempted to localInstance " + this + ", which has not yet committed.");
    }
    return localInstance;
  }

  public String discLibelleCourt() {
    return (String) storedValueForKey(EOOpeDiscipline.DISC_LIBELLE_COURT_KEY);
  }

  public void setDiscLibelleCourt(String value) {
    if (EOOpeDiscipline.LOG.isDebugEnabled()) {
        EOOpeDiscipline.LOG.debug( "updating discLibelleCourt from " + discLibelleCourt() + " to " + value);
    }
    takeStoredValueForKey(value, EOOpeDiscipline.DISC_LIBELLE_COURT_KEY);
  }

  public String discLibelleEdition() {
    return (String) storedValueForKey(EOOpeDiscipline.DISC_LIBELLE_EDITION_KEY);
  }

  public void setDiscLibelleEdition(String value) {
    if (EOOpeDiscipline.LOG.isDebugEnabled()) {
        EOOpeDiscipline.LOG.debug( "updating discLibelleEdition from " + discLibelleEdition() + " to " + value);
    }
    takeStoredValueForKey(value, EOOpeDiscipline.DISC_LIBELLE_EDITION_KEY);
  }

  public String discLibelleLong() {
    return (String) storedValueForKey(EOOpeDiscipline.DISC_LIBELLE_LONG_KEY);
  }

  public void setDiscLibelleLong(String value) {
    if (EOOpeDiscipline.LOG.isDebugEnabled()) {
        EOOpeDiscipline.LOG.debug( "updating discLibelleLong from " + discLibelleLong() + " to " + value);
    }
    takeStoredValueForKey(value, EOOpeDiscipline.DISC_LIBELLE_LONG_KEY);
  }


  public static OpeDiscipline create(EOEditingContext editingContext, String discLibelleCourt
, String discLibelleLong
) {
    OpeDiscipline eo = (OpeDiscipline) EOUtilities.createAndInsertInstance(editingContext, EOOpeDiscipline.ENTITY_NAME);    
        eo.setDiscLibelleCourt(discLibelleCourt);
        eo.setDiscLibelleLong(discLibelleLong);
    return eo;
  }

  public static ERXFetchSpecification<OpeDiscipline> fetchSpec() {
    return new ERXFetchSpecification<OpeDiscipline>(EOOpeDiscipline.ENTITY_NAME, null, null, false, true, null);
  }

  public static NSArray<OpeDiscipline> fetchAll(EOEditingContext editingContext) {
    return EOOpeDiscipline.fetchAll(editingContext, null);
  }

  public static NSArray<OpeDiscipline> fetchAll(EOEditingContext editingContext, NSArray<EOSortOrdering> sortOrderings) {
    return EOOpeDiscipline.fetchAll(editingContext, null, sortOrderings);
  }

  public static NSArray<OpeDiscipline> fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray<EOSortOrdering> sortOrderings) {
    ERXFetchSpecification<OpeDiscipline> fetchSpec = new ERXFetchSpecification<OpeDiscipline>(EOOpeDiscipline.ENTITY_NAME, qualifier, sortOrderings);
    fetchSpec.setIsDeep(true);
    NSArray<OpeDiscipline> eoObjects = fetchSpec.fetchObjects(editingContext);
    return eoObjects;
  }

  public static OpeDiscipline fetch(EOEditingContext editingContext, String keyName, Object value) {
    return EOOpeDiscipline.fetch(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OpeDiscipline fetch(EOEditingContext editingContext, EOQualifier qualifier) {
    NSArray<OpeDiscipline> eoObjects = EOOpeDiscipline.fetchAll(editingContext, qualifier, null);
    OpeDiscipline eoObject;
    int count = eoObjects.count();
    if (count == 0) {
      eoObject = null;
    }
    else if (count == 1) {
      eoObject = eoObjects.objectAtIndex(0);
    }
    else {
      throw new IllegalStateException("There was more than one OpeDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OpeDiscipline fetchRequired(EOEditingContext editingContext, String keyName, Object value) {
    return EOOpeDiscipline.fetchRequired(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
  }

  public static OpeDiscipline fetchRequired(EOEditingContext editingContext, EOQualifier qualifier) {
    OpeDiscipline eoObject = EOOpeDiscipline.fetch(editingContext, qualifier);
    if (eoObject == null) {
      throw new NoSuchElementException("There was no OpeDiscipline that matched the qualifier '" + qualifier + "'.");
    }
    return eoObject;
  }

  public static OpeDiscipline localInstanceIn(EOEditingContext editingContext, OpeDiscipline eo) {
    OpeDiscipline localInstance = (eo == null) ? null : ERXEOControlUtilities.localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }
}