package org.cocktail.fwkcktlgfcoperations.server;

import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

/**
 * Framework principal de FwkCktlGfcOperations.
 * 
 * @see ERXFrameworkPrincipal
 * @author Alexis Tual
 *
 */
public class FwkCktlGFCOperations extends ERXFrameworkPrincipal {
	public static FwkCktlGFCOperationsParamManager paramManager = new FwkCktlGFCOperationsParamManager();

    static {
        setUpFrameworkPrincipalClass(FwkCktlGFCOperations.class);
    }
    
    @Override
    public void finishInitialization() {
        ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
        moduleRegister.addModule(new FwkCktlGFCOperationsModule());
    }
    
    @Override
    public void didFinishInitialization() {
    	super.didFinishInitialization();
		paramManager.checkAndInitParamsWithDefault();
    }
    
}
