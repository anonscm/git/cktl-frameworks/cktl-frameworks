package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponible;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponibleFraisExclus;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation.ContratFinancementDepensesValidation;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class ContratFinancementDepensesValidationTest extends BaseOperationTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("GFCOperations");
    
    @Test
    public void testIsSatisfiedByAvecSimuationDeConsommation() {
        // contrat a 1000 euros
        // 3 tranches :
        // 2013) 0   | 750 | 750  
        // 2014) 350 | 100 | 450
        // 2015) 100 | 150 | 250
        
        // Simuation de consommation
        // 2013) 400
        // 2014) 350
        
        // Budgets depenses positionnés (= tranches budgets)
        // 2013) 750
        // 2014) 450
        // 2015) 250
        
        Tranche tranche2013 = creerTranche(2013);
        Tranche tranche2014 = creerTranche(2014);
        Tranche tranche2015 = creerTranche(2015);
        
        tranche2013.setTraSuppr("N");
        tranche2014.setTraSuppr("N");
        tranche2015.setTraSuppr("N");
        
        tranche2013.setReportNmoins1(BigDecimal.valueOf(0));
        tranche2013.setReportNplus1(BigDecimal.valueOf(350));
        tranche2014.setReportNmoins1(BigDecimal.valueOf(350));
        tranche2014.setReportNplus1(BigDecimal.valueOf(100));
        tranche2015.setReportNmoins1(BigDecimal.valueOf(100));
        tranche2015.setReportNplus1(BigDecimal.valueOf(0));
        
        Operation operation = creerOperationAvecTranches(tranche2013, tranche2014, tranche2015);
        ec.insertSavedObject(operation);
        
        FraisGestion frais = new FraisGestion();
        frais.setMontant(BigDecimal.ZERO);
        frais.setMontantPourcentageTranche(BigDecimal.ZERO);
        frais.setCoutsIndirectsNonImputes(BigDecimal.ZERO);
        frais.setChargesNonBudgetaires(BigDecimal.ZERO);

        ec.insertSavedObject(frais);
        
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart3 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart4 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart5 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart6 = new RepartPartenaireTranche();
        ec.insertSavedObject(repart1);
        ec.insertSavedObject(repart2);
        ec.insertSavedObject(repart3);
        ec.insertSavedObject(repart4);
        ec.insertSavedObject(repart5);
        ec.insertSavedObject(repart6);
        
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart3.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart4.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart5.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart6.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);

        repart1.addToFraisGestionsRelationship(frais);
        repart2.addToFraisGestionsRelationship(frais);
        repart3.addToFraisGestionsRelationship(frais);
        repart4.addToFraisGestionsRelationship(frais);
        repart5.addToFraisGestionsRelationship(frais);
        repart6.addToFraisGestionsRelationship(frais);

        repart1.setMontantParticipation(BigDecimal.valueOf(750));
        repart2.setMontantParticipation(BigDecimal.valueOf(0));
        repart3.setMontantParticipation(BigDecimal.valueOf(100));
        repart4.setMontantParticipation(BigDecimal.valueOf(0));
        repart5.setMontantParticipation(BigDecimal.valueOf(150));
        repart6.setMontantParticipation(BigDecimal.valueOf(0));
        
        tranche2013.addToToRepartPartenaireTranches(repart1);
        tranche2013.addToToRepartPartenaireTranches(repart2);
        tranche2014.addToToRepartPartenaireTranches(repart3);
        tranche2014.addToToRepartPartenaireTranches(repart4);
        tranche2015.addToToRepartPartenaireTranches(repart5);
        tranche2015.addToToRepartPartenaireTranches(repart6);
        
        TrancheBudgetDepAE trancheBudget2013 = creerTrancheBudget(2013);
        TrancheBudgetDepAE trancheBudget2014 = creerTrancheBudget(2014);
        TrancheBudgetDepAE trancheBudget2015 = creerTrancheBudget(2015);

        trancheBudget2013.setTbMontant(BigDecimal.valueOf(750));
        trancheBudget2014.setTbMontant(BigDecimal.valueOf(450));
        trancheBudget2015.setTbMontant(BigDecimal.valueOf(250));
        
        tranche2013.addToTrancheBudgets(trancheBudget2013);
        tranche2014.addToTrancheBudgets(trancheBudget2014);
        tranche2015.addToTrancheBudgets(trancheBudget2015);
        
        ContratFinancementDepensesValidation validateurFinancement = new ContratFinancementDepensesValidation();
        assertTrue(validateurFinancement.isSatisfiedBy(operation));
    }
    
    @Test
    public void testIsSatisfiedByEnEchecAvecSimuationDeConsommation() {
        // contrat a 1000 euros
        // 3 tranches :
        // 2013) 0   | 750 | 750  
        // 2014) 350 | 100 | 450
        // 2015) 100 | 150 | 250
        
        // Simuation de consommation
        // 2013) 400
        // 2014) 350
        
        // Budgets depenses positionnés (= tranches budgets)
        // 2013) 750
        // 2014) 450
        // 2015) 251
        
        Tranche tranche2013 = creerTranche(2013);
        Tranche tranche2014 = creerTranche(2014);
        Tranche tranche2015 = creerTranche(2015);
        
        tranche2013.setTraSuppr("N");
        tranche2014.setTraSuppr("N");
        tranche2015.setTraSuppr("N");
        
        tranche2013.setReportNmoins1(BigDecimal.valueOf(0));
        tranche2013.setReportNplus1(BigDecimal.valueOf(350));
        tranche2014.setReportNmoins1(BigDecimal.valueOf(350));
        tranche2014.setReportNplus1(BigDecimal.valueOf(100));
        tranche2015.setReportNmoins1(BigDecimal.valueOf(100));
        tranche2015.setReportNplus1(BigDecimal.valueOf(0));
        
        Operation operation = creerOperationAvecTranches(tranche2013, tranche2014, tranche2015);
        ec.insertSavedObject(operation);
        
        FraisGestion frais = new FraisGestion();
        frais.setMontant(BigDecimal.ZERO);
        frais.setMontantPourcentageTranche(BigDecimal.ZERO);
        frais.setCoutsIndirectsNonImputes(BigDecimal.ZERO);
        frais.setChargesNonBudgetaires(BigDecimal.ZERO);
        ec.insertSavedObject(frais);
        
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart3 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart4 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart5 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart6 = new RepartPartenaireTranche();
        ec.insertSavedObject(repart1);
        ec.insertSavedObject(repart2);
        ec.insertSavedObject(repart3);
        ec.insertSavedObject(repart4);
        ec.insertSavedObject(repart5);
        ec.insertSavedObject(repart6);
        
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart3.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart4.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart5.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart6.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);

        repart1.addToFraisGestionsRelationship(frais);
        repart2.addToFraisGestionsRelationship(frais);
        repart3.addToFraisGestionsRelationship(frais);
        repart4.addToFraisGestionsRelationship(frais);
        repart5.addToFraisGestionsRelationship(frais);
        repart6.addToFraisGestionsRelationship(frais);

        repart1.setMontantParticipation(BigDecimal.valueOf(750));
        repart2.setMontantParticipation(BigDecimal.valueOf(0));
        repart3.setMontantParticipation(BigDecimal.valueOf(100));
        repart4.setMontantParticipation(BigDecimal.valueOf(0));
        repart5.setMontantParticipation(BigDecimal.valueOf(150));
        repart6.setMontantParticipation(BigDecimal.valueOf(0));
        
        tranche2013.addToToRepartPartenaireTranches(repart1);
        tranche2013.addToToRepartPartenaireTranches(repart2);
        tranche2014.addToToRepartPartenaireTranches(repart3);
        tranche2014.addToToRepartPartenaireTranches(repart4);
        tranche2015.addToToRepartPartenaireTranches(repart5);
        tranche2015.addToToRepartPartenaireTranches(repart6);
        
        TrancheBudgetDepAE trancheBudget2013 = creerTrancheBudget(2013);
        TrancheBudgetDepAE trancheBudget2014 = creerTrancheBudget(2014);
        TrancheBudgetDepAE trancheBudget2015 = creerTrancheBudget(2015);

        trancheBudget2013.setTbMontant(BigDecimal.valueOf(750));
        trancheBudget2014.setTbMontant(BigDecimal.valueOf(450));
        trancheBudget2015.setTbMontant(BigDecimal.valueOf(251));
        
        tranche2013.addToTrancheBudgets(trancheBudget2013);
        tranche2014.addToTrancheBudgets(trancheBudget2014);
        tranche2015.addToTrancheBudgets(trancheBudget2015);
        
        ContratFinancementDepensesValidation validateurFinancement = new ContratFinancementDepensesValidation();
        assertFalse(validateurFinancement.isSatisfiedBy(operation));
    }

    @Test
    public void testIsSatisfiedBySansConsommation() {
        // contrat a 1000 euros
        // 3 tranches :
        // 2013) 0   | 750 | 750  
        // 2014) 750 | 100 | 850
        // 2015) 850 | 150 | 1000
        
        // Budgets depenses positionnés (= tranches budgets) avec depassement cas invalide mais aujourd'hui non verifie.
        // 2013) 750
        // 2014) 850
        // 2015) 1000
        
        Tranche tranche2013 = creerTranche(2013);
        Tranche tranche2014 = creerTranche(2014);
        Tranche tranche2015 = creerTranche(2015);
        
        tranche2013.setTraSuppr("N");
        tranche2014.setTraSuppr("N");
        tranche2015.setTraSuppr("N");
        
        tranche2013.setReportNmoins1(BigDecimal.valueOf(0));
        tranche2013.setReportNplus1(BigDecimal.valueOf(750));
        tranche2014.setReportNmoins1(BigDecimal.valueOf(750));
        tranche2014.setReportNplus1(BigDecimal.valueOf(850));
        tranche2015.setReportNmoins1(BigDecimal.valueOf(850));
        tranche2015.setReportNplus1(BigDecimal.valueOf(0));
        
        Operation operation = creerOperationAvecTranches(tranche2013, tranche2014, tranche2015);
        ec.insertSavedObject(operation);
        
        FraisGestion frais = new FraisGestion();
        frais.setMontant(BigDecimal.ZERO);
        frais.setMontantPourcentageTranche(BigDecimal.ZERO);
        frais.setCoutsIndirectsNonImputes(BigDecimal.ZERO);
        frais.setChargesNonBudgetaires(BigDecimal.ZERO);
        ec.insertSavedObject(frais);
        
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart3 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart4 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart5 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart6 = new RepartPartenaireTranche();
        ec.insertSavedObject(repart1);
        ec.insertSavedObject(repart2);
        ec.insertSavedObject(repart3);
        ec.insertSavedObject(repart4);
        ec.insertSavedObject(repart5);
        ec.insertSavedObject(repart6);
        
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart3.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart4.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart5.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart6.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);

        repart1.addToFraisGestionsRelationship(frais);
        repart2.addToFraisGestionsRelationship(frais);
        repart3.addToFraisGestionsRelationship(frais);
        repart4.addToFraisGestionsRelationship(frais);
        repart5.addToFraisGestionsRelationship(frais);
        repart6.addToFraisGestionsRelationship(frais);

        repart1.setMontantParticipation(BigDecimal.valueOf(750));
        repart2.setMontantParticipation(BigDecimal.valueOf(0));
        repart3.setMontantParticipation(BigDecimal.valueOf(100));
        repart4.setMontantParticipation(BigDecimal.valueOf(0));
        repart5.setMontantParticipation(BigDecimal.valueOf(150));
        repart6.setMontantParticipation(BigDecimal.valueOf(0));
        
        tranche2013.addToToRepartPartenaireTranches(repart1);
        tranche2013.addToToRepartPartenaireTranches(repart2);
        tranche2014.addToToRepartPartenaireTranches(repart3);
        tranche2014.addToToRepartPartenaireTranches(repart4);
        tranche2015.addToToRepartPartenaireTranches(repart5);
        tranche2015.addToToRepartPartenaireTranches(repart6);
        
        TrancheBudgetDepAE trancheBudget2013 = creerTrancheBudget(2013);
        TrancheBudgetDepAE trancheBudget2014 = creerTrancheBudget(2014);
        TrancheBudgetDepAE trancheBudget2015 = creerTrancheBudget(2015);

        trancheBudget2013.setTbMontant(BigDecimal.valueOf(750));
        trancheBudget2014.setTbMontant(BigDecimal.valueOf(850));
        trancheBudget2015.setTbMontant(BigDecimal.valueOf(1000));
        
        tranche2013.addToTrancheBudgets(trancheBudget2013);
        tranche2014.addToTrancheBudgets(trancheBudget2014);
        tranche2015.addToTrancheBudgets(trancheBudget2015);
        
        ContratFinancementDepensesValidation validateurFinancement = new ContratFinancementDepensesValidation();
        assertTrue(validateurFinancement.isSatisfiedBy(operation));
    }

    
}
