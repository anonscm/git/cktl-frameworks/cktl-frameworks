package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponible;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.CalculMontantParticipationDisponibleFraisExclus;
import org.cocktail.fwkcktlgfcoperations.server.metier.operation.validation.ContratFinancementRecettesValidation;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class ContratFinancementRecettesValidationTest extends BaseOperationTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("GFCOperations");
    
    @Test
    public void testIsSatisfiedBySansConsommation() {
        // contrat a 1000 euros
        // 3 tranches :  
    	// exer report	|contrib	|total
        // 2013) 0   	| 750 		| 750  
        // 2014) 750 	| 100 		| 850
        // 2015) 850 	| 150 		| 1000
        
        // Budgets recettes positionnées (= tranches budgets) .
        // 2013) 600
        // 2014) 200
        // 2015) 200
        
        Tranche tranche2013 = creerTranche(2013);
        Tranche tranche2014 = creerTranche(2014);
        Tranche tranche2015 = creerTranche(2015);
        
        tranche2013.setTraSuppr("N");
        tranche2014.setTraSuppr("N");
        tranche2015.setTraSuppr("N");
                
        
        tranche2013.setReportNmoins1(BigDecimal.valueOf(0));
        tranche2013.setReportNplus1(BigDecimal.valueOf(750));
        tranche2014.setReportNmoins1(BigDecimal.valueOf(750));
        tranche2014.setReportNplus1(BigDecimal.valueOf(850));
        tranche2015.setReportNmoins1(BigDecimal.valueOf(850));
        tranche2015.setReportNplus1(BigDecimal.valueOf(0));
        
        Operation operation = creerOperationAvecTranches(tranche2013, tranche2014, tranche2015);
        ec.insertSavedObject(operation);
        
        FraisGestion frais = new FraisGestion();
        frais.setMontant(BigDecimal.ZERO);
        frais.setMontantPourcentageTranche(BigDecimal.ZERO);
        ec.insertSavedObject(frais);
        
        
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart3 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart4 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart5 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart6 = new RepartPartenaireTranche();
        ec.insertSavedObject(repart1);
        ec.insertSavedObject(repart2);
        ec.insertSavedObject(repart3);
        ec.insertSavedObject(repart4);
        ec.insertSavedObject(repart5);
        ec.insertSavedObject(repart6);
        
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart3.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart4.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart5.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart6.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);

        repart1.addToFraisGestionsRelationship(frais);
        repart2.addToFraisGestionsRelationship(frais);
        repart3.addToFraisGestionsRelationship(frais);
        repart4.addToFraisGestionsRelationship(frais);
        repart5.addToFraisGestionsRelationship(frais);
        repart6.addToFraisGestionsRelationship(frais);


        repart1.setMontantParticipation(BigDecimal.valueOf(750));
        repart2.setMontantParticipation(BigDecimal.valueOf(0));
        repart3.setMontantParticipation(BigDecimal.valueOf(100));
        repart4.setMontantParticipation(BigDecimal.valueOf(0));
        repart5.setMontantParticipation(BigDecimal.valueOf(150));
        repart6.setMontantParticipation(BigDecimal.valueOf(0));
        
        tranche2013.addToToRepartPartenaireTranches(repart1);
        tranche2013.addToToRepartPartenaireTranches(repart2);
        tranche2014.addToToRepartPartenaireTranches(repart3);
        tranche2014.addToToRepartPartenaireTranches(repart4);
        tranche2015.addToToRepartPartenaireTranches(repart5);
        tranche2015.addToToRepartPartenaireTranches(repart6);
        
        TrancheBudgetRec trancheBudgetRec2013 = creerTrancheBudgetRec(2013);
        TrancheBudgetRec trancheBudgetRec2014 = creerTrancheBudgetRec(2014);
        TrancheBudgetRec trancheBudgetRec2015 = creerTrancheBudgetRec(2015);

        trancheBudgetRec2013.setTbrMontant(BigDecimal.valueOf(600));
        trancheBudgetRec2014.setTbrMontant(BigDecimal.valueOf(200));
        trancheBudgetRec2015.setTbrMontant(BigDecimal.valueOf(200));
        
        tranche2013.addToTrancheBudgetRecs(trancheBudgetRec2013);
        tranche2014.addToTrancheBudgetRecs(trancheBudgetRec2014);
        tranche2015.addToTrancheBudgetRecs(trancheBudgetRec2015);

        ContratFinancementRecettesValidation validateurFinancement = new ContratFinancementRecettesValidation();
        assertTrue(validateurFinancement.isSatisfiedBy(operation));
    }

    @Test
    public void testIsSatisfiedByEnEchecSansConsommation() {
    	// contrat a 1000 euros
        // 3 tranches :  
    	// exer report	|contrib	|total
        // 2013) 0   	| 750 		| 750  
        // 2014) 750 	| 100 		| 850
        // 2015) 850 	| 150 		| 1000
        
        // Budgets recettes positionnées (= tranches budgets) .
        // 2013) 600
        // 2014) 200
        // 2015) 201    // dépassement
        
        Tranche tranche2013 = creerTranche(2013);
        Tranche tranche2014 = creerTranche(2014);
        Tranche tranche2015 = creerTranche(2015);
        
        tranche2013.setTraSuppr("N");
        tranche2014.setTraSuppr("N");
        tranche2015.setTraSuppr("N");
                      
        
        tranche2013.setReportNmoins1(BigDecimal.valueOf(0));
        tranche2013.setReportNplus1(BigDecimal.valueOf(750));
        tranche2014.setReportNmoins1(BigDecimal.valueOf(750));
        tranche2014.setReportNplus1(BigDecimal.valueOf(850));
        tranche2015.setReportNmoins1(BigDecimal.valueOf(850));
        tranche2015.setReportNplus1(BigDecimal.valueOf(0));
        
        Operation operation = creerOperationAvecTranches(tranche2013, tranche2014, tranche2015);
        ec.insertSavedObject(operation);
        
        FraisGestion frais = new FraisGestion();
        frais.setMontant(BigDecimal.ZERO);
        frais.setMontantPourcentageTranche(BigDecimal.ZERO);
        ec.insertSavedObject(frais);
        
        
        RepartPartenaireTranche repart1 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart2 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart3 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart4 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart5 = new RepartPartenaireTranche();
        RepartPartenaireTranche repart6 = new RepartPartenaireTranche();
        ec.insertSavedObject(repart1);
        ec.insertSavedObject(repart2);
        ec.insertSavedObject(repart3);
        ec.insertSavedObject(repart4);
        ec.insertSavedObject(repart5);
        ec.insertSavedObject(repart6);
        
        CalculMontantParticipationDisponible calculMontantParticipationDisponible = 
                new CalculMontantParticipationDisponibleFraisExclus();
        repart1.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart2.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart3.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart4.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart5.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);
        repart6.setCalculMontantParticipationDisponible(calculMontantParticipationDisponible);

        repart1.addToFraisGestionsRelationship(frais);
        repart2.addToFraisGestionsRelationship(frais);
        repart3.addToFraisGestionsRelationship(frais);
        repart4.addToFraisGestionsRelationship(frais);
        repart5.addToFraisGestionsRelationship(frais);
        repart6.addToFraisGestionsRelationship(frais);


        repart1.setMontantParticipation(BigDecimal.valueOf(750));
        repart2.setMontantParticipation(BigDecimal.valueOf(0));
        repart3.setMontantParticipation(BigDecimal.valueOf(100));
        repart4.setMontantParticipation(BigDecimal.valueOf(0));
        repart5.setMontantParticipation(BigDecimal.valueOf(150));
        repart6.setMontantParticipation(BigDecimal.valueOf(0));
        
        tranche2013.addToToRepartPartenaireTranches(repart1);
        tranche2013.addToToRepartPartenaireTranches(repart2);
        tranche2014.addToToRepartPartenaireTranches(repart3);
        tranche2014.addToToRepartPartenaireTranches(repart4);
        tranche2015.addToToRepartPartenaireTranches(repart5);
        tranche2015.addToToRepartPartenaireTranches(repart6);
        
        TrancheBudgetRec trancheBudgetRec2013 = creerTrancheBudgetRec(2013);
        TrancheBudgetRec trancheBudgetRec2014 = creerTrancheBudgetRec(2014);
        TrancheBudgetRec trancheBudgetRec2015 = creerTrancheBudgetRec(2015);

        trancheBudgetRec2013.setTbrMontant(BigDecimal.valueOf(600));
        trancheBudgetRec2014.setTbrMontant(BigDecimal.valueOf(200));
        trancheBudgetRec2015.setTbrMontant(BigDecimal.valueOf(201));
        
        tranche2013.addToTrancheBudgetRecs(trancheBudgetRec2013);
        tranche2014.addToTrancheBudgetRecs(trancheBudgetRec2014);
        tranche2015.addToTrancheBudgetRecs(trancheBudgetRec2015);

        ContratFinancementRecettesValidation validateurFinancement = new ContratFinancementRecettesValidation();
        assertFalse(validateurFinancement.isSatisfiedBy(operation));
    }
    
}
