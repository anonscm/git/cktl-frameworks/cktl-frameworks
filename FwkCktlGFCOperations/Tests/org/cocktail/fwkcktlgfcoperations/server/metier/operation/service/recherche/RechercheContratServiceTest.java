package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.recherche;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.text.SimpleDateFormat;

import org.apache.commons.lang.time.DateUtils;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;
import com.wounit.rules.MockEditingContext;

public class RechercheContratServiceTest {

    private RechercheContratServiceImpl contratService;
    private NSArray<ResultatRechercheBean> mockResultats;
    private ResultatRechercheBean res1;
    private ResultatRechercheBean res2;
    private ResultatRechercheBean res3;
    
    private NSTimestamp dateDebut1;
    private NSTimestamp dateDebut2;
    private NSTimestamp dateFin1;
    private NSTimestamp dateFin2;
    private NSTimestamp dateSignature1;
    private NSTimestamp dateSignature2;
    
    private static final int UTL_ORDRE = 999;
    private static final int NO_INDIVIDU = 888;
    private static final int PERS_ID = 777;

    @Rule
    public MockEditingContext editingContext = new MockEditingContext("GFCOperations");
    
    @Before
    public void setUp() throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        dateDebut1 = new NSTimestamp(sdf.parse("01/01/2012"));
        dateFin1 = new NSTimestamp(sdf.parse("31/12/2012"));
        dateSignature1 = new NSTimestamp(sdf.parse("01/01/2012"));
        dateDebut2 = new NSTimestamp(sdf.parse("01/01/2011"));
        dateFin2 = new NSTimestamp(sdf.parse("31/12/2013"));
        dateSignature2 = new NSTimestamp(sdf.parse("01/01/2011"));
        contratService = new RechercheContratServiceImpl();
        mockResultatsRecherche();
    }

    @Test
    public void testQualifierCreateurContrat() {
        EOQualifier qual = contratService.qualifierCreateurContrat(UTL_ORDRE);
        assertTrue("(utlOrdreCreation = 999)".equals(qual.toString()));
    }
    
    @Test
    public void testQualifierSecretaireContrat() {
        EOQualifier qual = contratService.qualifierSecretaireContrat(NO_INDIVIDU);
        assertTrue(qual.toString().contains("'(toSecretariats.noIndividu contains 888)'"));
    }
    
    @Test
    public void testQualifierPartenaireContrat() {
        EOQualifier qual = contratService.qualifierPartenaireContrat(PERS_ID);
        assertTrue(qual.toString().contains("'(persId contains 777)'"));
    }
    
    @Test
    public void testQualifierOrganContrat() {
        EOQualifier qual = contratService.qualifierOrganContrat(NO_INDIVIDU);
        assertTrue(qual.toString().contains("'(trancheBudgets.organ.utilisateurOrgans.utilisateur.noIndividu contains 888)'"));
    }
    
    @Test
    public void testQualifierForUtilisateurContratTousDroits() {
        EOQualifier qual = contratService.qualifierForUtilisateurContrat(999, 998, 997, true);
        assertTrue(qual == null);
    }
    
    @Test
    public void testQualifierForUtilisateurContrat() {
        EOQualifier qual = contratService.qualifierForUtilisateurContrat(999, 998, 997, false);
        assertEquals("((utlOrdreCreation = 999) or  <er.extensions.eof.qualifiers.ERXQualifierInSubquery> " +
        		"'(trancheBudgets.organ.utilisateurOrgans.utilisateur.noIndividu contains 998)' or  " +
        		"<er.extensions.eof.qualifiers.ERXQualifierInSubquery> '(persId contains 997)' or  " +
        		"<er.extensions.eof.qualifiers.ERXQualifierInSubquery> '(toSecretariats.noIndividu contains 998)')", qual.toString());
    }
    
    @Test
    public void testQualifierForOperations() {
        EOQualifier qual = contratService.qualifierForOperations();
        assertEquals("(((typeClassificationContrat.tccCode = 'CONV') or (typeClassificationContrat.tccCode = 'OPE')) and (conSuppr = 'N') and (avenants.avtIndex = 0))", qual.toString());
    }
    
    private void mockResultatsRecherche() {
        NSArray<Object> data1 = new NSArray<Object>(13, 2013, 2, "Test 1", "Ref1", "3", "RACINE ETAB", new NSTimestamp(), 2, "CS", 
                182, "SOCIOLOGIE DEMOGRAPHIE", "CONV_THESE", "Convention thèse", 
                "FIN", "Opération avec impact financier", "INV", "Opération d'investissement",
                "SUIVI_SIMPLE", "Suivi simple", dateDebut1, dateFin1, dateSignature1, "CONV");
        NSArray<Object> data2 = new NSArray<Object>(14, 2013, 3, "Test 2", "Ref2", "81", "SERVICE 1", new NSTimestamp(), 1, "RA", 
                186, "TECHNOLOGIE CHIMIQUE ", "CONT_RECH_PARTEN", "Contrat de recherche en partenariat", 
                "FIN", "Opération avec impact financier", "INV", "Opération d'investissement",
                "SUIVI_SIMPLE", "Suivi simple", dateDebut2, dateFin2, dateSignature2, "OPE");
        NSArray<Object> data3 = new NSArray<Object>(14, 2013, 3, "Test 2", "Ref3", "81", "SERVICE 1", new NSTimestamp(), 1, "RA", 
                186, "TECHNOLOGIE CHIMIQUE ", "CONT_RECH_PARTEN", "Contrat de recherche en partenariat",
                "FIN", "Opération avec impact financier", "INV", "Opération d'investissement",
                "SUIVI_SIMPLE", "Suivi simple", dateDebut2, dateFin2, dateSignature2, "OPE");
        NSDictionary<String, Object> row1 = new NSDictionary<String, Object>(data1, ResultatRechercheBean.ROW_KEYS);
        NSDictionary<String, Object> row2 = new NSDictionary<String, Object>(data2, ResultatRechercheBean.ROW_KEYS);
        NSDictionary<String, Object> row3 = new NSDictionary<String, Object>(data3, ResultatRechercheBean.ROW_KEYS);
        res1 = new ResultatRechercheBean(row1);
        res2 = new ResultatRechercheBean(row2);
        res3 = new ResultatRechercheBean(row3);
        mockResultats = new NSArray<ResultatRechercheBean>(res1, res2, res3);
    }
    
    @Test
    public void testDisciplinesFromResultats() {
        NSArray<DisciplineBean> disciplines = 
                contratService.disciplinesFromResultats(mockResultats);
        assertEquals(2, disciplines.size());
    }
    
    @Test
    public void testTypeContratFromResultats() {
        NSArray<TypeContratBean> typesContrat = 
                contratService.typesContratFromResultats(mockResultats);
        assertEquals(2, typesContrat.size());
    }
    
    @Test
    public void testModesGestionFromResultats() {
        NSArray<ModeGestionBean> modesGestion = 
                contratService.modesGestionFromResultats(mockResultats);
        assertEquals(2, modesGestion.size());
    }
    
    @Test
    public void testServicesGestionnairesFromResultats() {
        NSArray<ServiceGestionnaireBean> services = 
                contratService.servicesGestionnairesFromResultats(mockResultats);
        assertEquals(2, services.size());
    }
    
    @Test
    public void testFiltrerConventionsExeOrdre() {
        Filtre filtre = new Filtre();
        filtre.setFiltreExeordre(2013);
        assertArrayEquals(new ResultatRechercheBean[] {res1, res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsIndex() {
        Filtre filtre = new Filtre();
        filtre.setFiltreIndex(3);
        assertArrayEquals(new ResultatRechercheBean[] {res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsObjet() {
        Filtre filtre = new Filtre();
        filtre.setFiltreLibelle("Test 1");
        assertArrayEquals(new ResultatRechercheBean[] {res1}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsRefExterne() {
        Filtre filtre = new Filtre();
        filtre.setFiltreRefExterne("Ref2");
        assertArrayEquals(new ResultatRechercheBean[] {res2}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsService() {
        Filtre filtre = new Filtre();
        filtre.setSelectedService(res1.getServiceGestionnaire());
        assertArrayEquals(new ResultatRechercheBean[] {res1}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsTypeContrat() {
        Filtre filtre = new Filtre();
        filtre.setSelectedTypeContrat(res2.getTypeContrat());
        assertArrayEquals(new ResultatRechercheBean[] {res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsModeGestion() {
        Filtre filtre = new Filtre();
        filtre.setSelectedModeDeGestion(res2.getModeGestion());
        assertArrayEquals(new ResultatRechercheBean[] {res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsDiscipline() {
        Filtre filtre = new Filtre();
        filtre.setSelectedDiscipline(res2.getDiscipline());
        assertArrayEquals(new ResultatRechercheBean[] {res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsDateDebut() {
        Filtre filtre = new Filtre();
        filtre.setFiltreDateDebutMax(new NSTimestamp(DateUtils.addDays(dateDebut1, 1)));
        filtre.setFiltreDateDebutMin(new NSTimestamp(DateUtils.addDays(dateDebut1, -1)));
        assertArrayEquals(new ResultatRechercheBean[] {res1}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsDateFin() {
        Filtre filtre = new Filtre();
        filtre.setFiltreDateFinMin(new NSTimestamp(DateUtils.addDays(dateFin2, -1)));
        filtre.setFiltreDateFinMax(new NSTimestamp(DateUtils.addDays(dateFin2, 1)));
        assertArrayEquals(new ResultatRechercheBean[] {res2, res3}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConventionsDateSignature() {
        Filtre filtre = new Filtre();
        filtre.setFiltreDateValidationAdmMin(new NSTimestamp(DateUtils.addDays(dateSignature1, -1)));
        filtre.setFiltreDateValidationAdmMax(new NSTimestamp(DateUtils.addDays(dateSignature1, 1)));
        assertArrayEquals(new ResultatRechercheBean[] {res1}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
    @Test
    public void testFiltrerConvention() {
        Filtre filtre = new Filtre();
        filtre.setFiltreIndex(2);
        filtre.setFiltreExeordre(2013);
        filtre.setSelectedService(res1.getServiceGestionnaire());
        filtre.setSelectedTypeContrat(res1.getTypeContrat());
        filtre.setSelectedModeDeGestion(res1.getModeGestion());
        filtre.setSelectedDiscipline(res1.getDiscipline());
        assertArrayEquals(new ResultatRechercheBean[] {res1}, contratService.filtrerOperations(mockResultats, filtre).toArray());
    }
    
}
