package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.RepartPartenaireTranche;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.wounit.rules.MockEditingContext;

public class CalculMontantParticipationDisponibleFraisExclusTest {

    @Rule
    public MockEditingContext ec = new MockEditingContext("GFCOperations");
    private CalculMontantParticipationDisponibleFraisExclus calculMontantParticipationDisponibleFraisExclus;
    private RepartPartenaireTranche repartPartenaireTranche;
    private double montantParticipation = 1000d;
    private double montantFg = 100d;
    
    @Before
    public void setUp() throws Exception {
        calculMontantParticipationDisponibleFraisExclus = new CalculMontantParticipationDisponibleFraisExclus();
        repartPartenaireTranche = new RepartPartenaireTranche();
        ec.insertSavedObject(repartPartenaireTranche);
        repartPartenaireTranche.setMontantParticipation(BigDecimal.valueOf(montantParticipation));
        repartPartenaireTranche.fraisGestion().setMontant(BigDecimal.valueOf(montantFg));
    }

    @Test
    public void testMontantParticipationDisponibleFraisExclus() {
        BigDecimal montantParticipationDispo = 
                calculMontantParticipationDisponibleFraisExclus.montantParticipationDisponible(repartPartenaireTranche);
        Assert.assertEquals(montantParticipation - montantFg, montantParticipationDispo.doubleValue(), 0d);
    }

}
