package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlpersonne.common.metier.EODomaineScientifique;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import com.webobjects.foundation.NSTimestamp;
import com.wounit.annotations.Dummy;
import com.wounit.annotations.UnderTest;
import com.wounit.rules.MockEditingContext;

import er.extensions.foundation.ERXTimestampUtilities;

public class AvenantTest {

	//private EOStructure struct = new EOStructure();

	@Rule
	public MockEditingContext ec = new MockEditingContext("GFCOperations", "FwkCktlPersonne", "FwkCktlGFCEos", "FwkCktlDroitsUtils");

	@UnderTest
	private Avenant avenant;

	@Dummy
	private EODomaineScientifique domaineScientifique;

	@Dummy
	private ModeGestion modeGestionRA;

	@Before
	public void setupModeGestionRA() {
		modeGestionRA.setMgLibelleCourt(ModeGestion.MODE_GESTION_RA);
	}

	@Test
	public void testRecuperationDomaineScientifique() {
		avenant.setDomaineScientifique(domaineScientifique);
		assertEquals(domaineScientifique, avenant.domaineScientifique());
	}

	@Test
	public void testLibelleAvenantZero() {
		avenant.setAvtIndex(0);
		assertEquals(avenant.libelleCourt(), Avenant.INITIAL);
	}

	@Test
	public void testPassageModeGestionRA() {
		avenant.setModeGestionRelationship(modeGestionRA);
		assertTrue("L'avenant doit etre a credit limitatif", avenant.avtLimitatifBoolean());
		assertFalse("L'avenant ne doit pas etre lucratif", avenant.avtLucrativiteBoolean());
	}

	@Test
	public void testContratEstSigne() {
		avenant.setAvtDateValidAdm(ERXTimestampUtilities.today());
		assertTrue(avenant.isSigne());
	}

	@Test
	public void testRecuperationAvenantValides() {
		Operation operation = new Operation();
		Avenant avenantValide = new Avenant();
		avenantValide.setAvtDateValidAdm(new NSTimestamp());
		avenantValide.setAvtSuppr("N");
		operation.addToAvenantsRelationship(avenantValide);
		assertEquals(1, operation.avenantsValides().count());
	}

	@Test
	public void testRecuperationAvenantValidesEtNonSupprimes() {
		Operation operation = new Operation();
		Avenant avenantValide = new Avenant();
		avenantValide.setAvtDateValidAdm(new NSTimestamp());
		avenantValide.setAvtSuppr("O");
		operation.addToAvenantsRelationship(avenantValide);
		assertEquals("Les avenants supprimés ne doivent pas être présents", 0, operation.avenantsValides().count());
	}

}
