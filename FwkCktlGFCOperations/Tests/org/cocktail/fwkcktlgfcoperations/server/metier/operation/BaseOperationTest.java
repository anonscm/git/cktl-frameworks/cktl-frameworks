package org.cocktail.fwkcktlgfcoperations.server.metier.operation;

import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceos.server.metier.EOExerciceCocktail;

public abstract class BaseOperationTest {

    protected Operation creerOperationAvecTranches(Tranche...tranches) {
        Operation contrat = new Operation();
        for (Tranche tranche : tranches) {
            contrat.addToTranchesRelationship(tranche);
        }
        return contrat;
    }
    
    protected Tranche creerTranche(int exercice) {
        Tranche tranche = new Tranche();
        EOExerciceCocktail exerciceCktl = new EOExerciceCocktail();
        exerciceCktl.setExeExercice(exercice);
        tranche.setExerciceCocktailRelationship(exerciceCktl);
        return tranche;
    }
    
    protected TrancheBudgetDepAE creerTrancheBudget(int exercice) {
        TrancheBudgetDepAE trancheBudget = new TrancheBudgetDepAE();
        EOExercice exerciceCktl = new EOExercice();
        exerciceCktl.setExeExercice(Long.valueOf(exercice));
        trancheBudget.setTbSuppr("N");
        return trancheBudget;
    }
    
    protected TrancheBudgetRec creerTrancheBudgetRec(int exercice) {
        TrancheBudgetRec trancheBudgetRec = new TrancheBudgetRec();
        EOExercice exerciceCktl = new EOExercice();
        exerciceCktl.setExeExercice(Long.valueOf(exercice));
        trancheBudgetRec.setTbrSuppr("N");
        return trancheBudgetRec;
    }
    
}
