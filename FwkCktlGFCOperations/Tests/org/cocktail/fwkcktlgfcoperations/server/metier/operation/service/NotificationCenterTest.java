package org.cocktail.fwkcktlgfcoperations.server.metier.operation.service;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.cocktail.fwkcktlgfcoperations.server.metier.operation.service.NotificationCenter.Notification;
import org.junit.Before;
import org.junit.Test;

import com.webobjects.foundation.NSNotification;

public class NotificationCenterTest {
    
    NotificationCenter notificationCenter;
    Abonne abonne;

    public static class Abonne {
        private boolean aRafraichit = false;
        
        public void onRafraichir(NSNotification notif) {
            aRafraichit = true;
        }
        
        public boolean aRafraichit() {
            return aRafraichit;
        }
        
    }
    
    @Before
    public void setUp() throws Exception {
        this.notificationCenter = NotificationCenter.instance();
        this.abonne = new Abonne();
    }

    @Test
    public void testAbonnerEtNotifier() {
        notificationCenter.abonner(abonne, "onRafraichir", Notification.REFRESH_DEPENSES);
        assertFalse(abonne.aRafraichit());
        notificationCenter.notifier(Notification.REFRESH_DEPENSES);
        assertTrue(abonne.aRafraichit());
    }

    @Test
    public void testAbonnerEtNotifierAvecOrigine() {
        notificationCenter.abonner(abonne, "onRafraichir", Notification.REFRESH_DEPENSES, this);
        assertFalse(abonne.aRafraichit());
        notificationCenter.notifier(Notification.REFRESH_DEPENSES, this);
        assertTrue(abonne.aRafraichit());
    }
    
}
