/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.Date;

import org.apache.commons.lang.time.DurationFormatUtils;
import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlDataResponse;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSMutableArray;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class CktlAbstractReporter {
	public final static Logger logger = Logger.getLogger(CktlAbstractReporter.class);
	public static String NOPAGES_MSG = "Aucune donnée trouvée. Vérifiez éventuellement les critères.";

	public final static int EXPORT_FORMAT_XLS = 0;
	public final static int EXPORT_FORMAT_PDF = 1;
	public final static int EXPORT_FORMAT_HTML = 2;

	private NSMutableArray<ICktlReportingTaskListener> listeners = new NSMutableArray<ICktlReportingTaskListener>();

	private Throwable lastPrintError;
	private String resultFileName;
	private String resultMimeType;
	private CktlAbstractReportingTaskThread currentReportingTaskThread;

	private boolean started = false;
	private Date timeStarted = null;
	private Date timeFinished = null;

	public static final void saveInTempFile(StringWriter sw, final String fileName) throws Exception {
		//        return;

		String temporaryDir = System.getProperty("java.io.tmpdir");
		if (!temporaryDir.endsWith(File.separator)) {
			temporaryDir = temporaryDir + File.separator;
		}
		File f = new File(temporaryDir + fileName);
		if (f.exists()) {
			f.delete();
		}
		if (f.createNewFile()) {
			new FileWriter(f);
			BufferedWriter out = new BufferedWriter(new FileWriter(f));
			out.write(sw.toString());
			out.close();
		}
		logger.info("Le fichier " + f.getAbsolutePath() + " a ete cree.");
	}

	public Throwable getLastPrintError() {
		return lastPrintError;
	}

	public void setLastPrintError(Throwable lastPrintError) {
		this.lastPrintError = lastPrintError;
	}

	public abstract Boolean isResultReady();

	public String getResultFileName() {
		return resultFileName;
	}

	public void setResultFileName(String resultFileName) {
		this.resultFileName = resultFileName;
	}

	public String getResultMimeType() {
		return resultMimeType;
	}

	public void setResultMimeType(String resultMimeType) {
		this.resultMimeType = resultMimeType;
	}

	public void addListener(ICktlReportingTaskListener listener) {
		if (listener != null) {
			if (listeners.indexOfObject(listener) == NSArray.NotFound) {
				listeners.addObject(listener);
			}
		}
	}

	public void removeListener(ICktlReportingTaskListener listener) {
		if (listener != null) {
			if (listeners.indexOfObject(listener) != NSArray.NotFound) {
				listeners.removeObject(listener);
			}
		}
	}

	protected void notifyAfterReportExport() {
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterReportExport();

		}
	}

	protected void notifyAfterException(Throwable e) {
		timeFinished = new Date();
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterException(e);
		}
	}

	protected void notifyAfterPageExport(int currentPageNum, int pageCount) {
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterPageExport(currentPageNum, pageCount);

		}
	}

	protected void notifyAfterReportFinish() {
		this.timeFinished = new Date();
		for (int i = listeners.count() - 1; i >= 0; i--) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterReportFinish();
		}
	}

	protected void notifyAfterReportBuild(int pageTotalCount) {
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterReportBuild(pageTotalCount);

		}
	}

	protected void notifyAfterReportStarted() {
		this.timeStarted = new Date();
		this.timeFinished = null;
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterReportStarted();

		}
	}

	protected void notifyAfterReportCanceled() {
		timeFinished = new Date();
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterReportCanceled();

		}
	}

	protected void notifyAfterDataSourceCreated() {
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).afterDataSourceCreated();

		}
	}

	protected void notifyTrace(String s) {
		for (int i = 0; i < listeners.count(); i++) {
			((ICktlReportingTaskListener) listeners.objectAtIndex(i)).trace(s);

		}
		if (logger.isDebugEnabled()) {
			logger.debug(s);
		}
	}

	public NSMutableArray<ICktlReportingTaskListener> getListeners() {
		return listeners;
	}

	/**
	 * Utilisez plutôt stop() à la place.
	 * 
	 * @deprecated
	 */
	public final void printKillCurrentTask() {
		if (getCurrentReportingTaskThread() != null && getCurrentReportingTaskThread().isAlive() && !getCurrentReportingTaskThread().isInterrupted()) {
			getCurrentReportingTaskThread().interrupt();
			setCurrentReportingTaskThread(null);
		}
	}

	public CktlAbstractReportingTaskThread getCurrentReportingTaskThread() {
		return currentReportingTaskThread;
	}

	protected void setCurrentReportingTaskThread(CktlAbstractReportingTaskThread currentReportingTaskThread) {
		this.currentReportingTaskThread = currentReportingTaskThread;
	}

	/**
	 * Utilisez printDifferedGetDataResult à la place
	 * 
	 * @deprecated
	 * @return
	 * @throws Exception
	 */
	public final NSData printDifferedGetPdf() throws Throwable {
		if (getLastPrintError() != null) {
			throw getLastPrintError();
		}
		Thread.yield();
		if (getCurrentReportingTaskThread() != null) {
			return getCurrentReportingTaskThread().getDataResult();
		}
		return null;
	}

	public final NSData printDifferedGetDataResult() throws Throwable {
		logger.info("Print " + this.toString());
		if (getLastPrintError() != null) {
			throw getLastPrintError();
		}
		Thread.yield();
		if (getCurrentReportingTaskThread() != null) {
			return getCurrentReportingTaskThread().getDataResult();
		}
		return null;
	}

	public boolean isStarted() {
		return started;
	}

	public void setStarted(boolean started) {
		this.started = started;
		if (started) {
			notifyAfterReportStarted();
		}
	}

	public void stop() {
		if (getCurrentReportingTaskThread() != null && getCurrentReportingTaskThread().isAlive()) {
			getCurrentReportingTaskThread().interrupt();
		}
		if (getCurrentReportingTaskThread() == null || getCurrentReportingTaskThread().isInterrupted()) {
			notifyAfterReportCanceled();
		}

	}

	/**
	 * Genere une reponse http pour le téléchargement d'un fichier.
	 * 
	 * @param data
	 * @param fileName
	 * @param mimeType
	 * @return
	 */
	public static WOActionResults downloadFile(NSData data, String fileName, String mimeType) {
		CktlDataResponse resp = new CktlDataResponse();
		if (data != null) {
			resp.setHeader("maxage=1", "Cache-Control");
			resp.setHeader("public", "Pragma");
			resp.setContent(data, mimeType);
			resp.setFileName(fileName);
		}
		else {
			resp.setContent("");
			resp.setHeader("0", "Content-Length");
		}
		return resp.generateResponse();
	}

	/**
	 * @return la durée d'execution.
	 */
	public Long getRunningTime() {
		if (timeStarted != null && timeFinished != null) {
			return timeFinished.getTime() - timeStarted.getTime();
		}
		return null;
	}

	/**
	 * @return La duree d'execution sous forme de chaine de caracteres.
	 */
	public String getRunningTimeAsString() {
		String res = null;
		if (timeStarted != null && timeFinished != null) {
			return DurationFormatUtils.formatDuration(getRunningTime(), "m'mn' s's' SSS'ms'", false);
		}
		return res;
	}

}
