package org.cocktail.reporting.server.exception;

public class CktlLocationException extends Exception {

    private static final long serialVersionUID = 1L;

    public CktlLocationException() {
        super();
    }

    public CktlLocationException(String message, Throwable cause) {
        super(message, cause);
    }

    public CktlLocationException(String message) {
        super(message);
    }

    public CktlLocationException(Throwable cause) {
        super(cause);
    }
}
