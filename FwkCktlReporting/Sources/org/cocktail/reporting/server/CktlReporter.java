/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server;

import java.sql.Connection;
import java.util.Map;

import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;

import com.webobjects.foundation.NSData;

/**
 * Point d'entrée pour l'execution des reports.
 * 
 * @deprecated Utilisez directement les classes des packages (par exemple JrxmlReporter.printNow)
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReporter {

	public static final int FORMATPDF = CktlAbstractReporter.EXPORT_FORMAT_PDF;
	public static final int FORMATXLS = CktlAbstractReporter.EXPORT_FORMAT_XLS;
	public static final int FORMATHTML = CktlAbstractReporter.EXPORT_FORMAT_HTML;

	public static NSData printJasperReportsNow(Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> parameters, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Exception {
		JrxmlReporter reporter = new JrxmlReporter();
		try {
			return reporter.printNow(connection, sqlQuery, completeJasperFilePath, parameters, printFormat, printIfEmpty, listener);
		} catch (Throwable e) {
			throw new Exception(e);
		}

	}

}
