package org.cocktail.reporting.server.jrxml;

import java.io.File;
import java.util.Collection;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

public class JrxmlReporterWithBeanCollectionDataSource extends JrxmlReporter {

	/**
	 * 
	 * @param label libellé de la tache d'impression
	 * @param beanCollectionDataSource la source de données sous forme de beans
	 * @param completeFilePath le chemin absolu du du fichier jasper servant pour la génération
	 * @param params les paramètres à passer au jasper
	 * @param printFormat le format d'impression (example PDF)
	 * @param listener le listener qui est utilisé pour connaitre l'avancement de l'impression
	 * @return le  thread dans lequel se déroule l'impression
	 * @throws Exception en cas de problème 
	 */
	public CktlJrxmlReportingTaskThread printWithThread(String label, Collection<Object> beanCollectionDataSource, final String completeFilePath,
	    final Map<String, Object> params, final int printFormat, IJrxmlReportListener listener)
	    throws Exception {
		try {
			
			defaultReportListener = new DefaultJrxmlReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			
			if (listener != null) {
				addListener(listener);
			}
			
			setStarted(true);
			initSrcFile(completeFilePath, false);

			// Recupérer le chemin du repertoire du report
			final File f = new File(completeFilePath);
			
			String rep = f.getParent();
			
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			
			
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(beanCollectionDataSource);

			if (listener != null) {
				listener.afterDataSourceCreated();
			}
			
			setCurrentReportingTaskThread(new CktlJrxmlWithJsonReportingTaskThread(label, dataSource, completeFilePath, params, printFormat, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			// On lance la génération du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public class CktlJrxmlWithJsonReportingTaskThread extends CktlJrxmlReportingTaskThread {
		
		private JRBeanCollectionDataSource dataSource;

		/**
		 * 
		 * @param label libellé de la tache d'impression
		 * @param beanCollectionDataSource la source de données sous forme de beans
		 * @param jasperFileName le chemin absolu du du fichier jasper servant pour la génération
		 * @param parameters les paramètres à passer au jasper
		 * @param printFormat le format d'impression (example PDF)
		 * @param listener le listener qui est utilisé pour connaitre l'avancement de l'impression
		 */
		public CktlJrxmlWithJsonReportingTaskThread(String label, JRBeanCollectionDataSource dataSource, String jasperFileName, Map<String, Object> parameters,
		    int printFormat, IJrxmlReportListener listener) {
			super(label, null, null, jasperFileName, parameters, printFormat, true, Boolean.FALSE, listener);
			this.dataSource = dataSource;
		}

		protected JasperPrint fillReport() throws JRException {
			JasperPrint _printResult = JasperFillManager.fillReport(jasperReport, getParams(), dataSource);
			return _printResult;
		}

	}
}
