/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jrxml;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;

import net.sf.jasperreports.engine.JRBand;
import net.sf.jasperreports.engine.JRDataset;
import net.sf.jasperreports.engine.JRElement;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExpression;
import net.sf.jasperreports.engine.JRExpressionChunk;
import net.sf.jasperreports.engine.JRGroup;
import net.sf.jasperreports.engine.JRSection;
import net.sf.jasperreports.engine.JRSubreport;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.design.JRDesignDataset;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.log4j.Logger;

import er.extensions.foundation.ERXFileUtilities;

/**
 * Outil pour la compilation des reports Jasper. Les fichiers sources (.jrxml) doivent être présents au même endroit que le .jasper.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class JrxmlCompiler {
	public final static Logger logger = Logger.getLogger(JrxmlCompiler.class);
	static final String JASPER_FILE_EXTENSION = ".jasper";
	private static final String JRXML_FILE_EXTENSION = ".jrxml";

	/**
	 * Compile le report passé en paramètre et tous les sous-reports trouvé (attention les fichiers .jasper sont écrasés par des version plus
	 * récentes, une version de sauvegarde des précédents .jasper est créée).
	 * 
	 * @param filePath Chemin complet du fichier .jasper ou .jrxml (le .jrxml doit néanmoins être présent).
	 * @return
	 * @throws Exception
	 */
	public JasperReport compileRecursive(String filePath) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug("Compilation avec JasperReports version " + getInstalledJasperReportsVersion());
		}

		//si le fichier passé en parametre est un .jasper, on cherche le .jrxml correspondant
		if (filePath.endsWith(JASPER_FILE_EXTENSION)) {
			filePath = getSrcFileNameFromCompiledFileName(filePath);
		}

		//charger le fichier source
		File srcFile = new File(filePath);
		if (!srcFile.exists()) {
			throw new FileNotFoundException("Le fichier " + srcFile.getAbsolutePath() + " n'a pas été trouvé.");
		}

		if (logger.isDebugEnabled()) {
			logger.debug("Chargement du fichier jasper " + filePath);
		}
		try {
			JasperDesign jasperDesign = null;
			try {
				jasperDesign = JRXmlLoader.load(srcFile);
			} catch (Exception e) {
				e.printStackTrace();
				throw new Exception(
						"Impossible de charger le fichier "
								+ srcFile
								+ ". Dans le fichier properties de l'application, activez les logs Jasper en ajoutant \"log4j.logger.net.sf.jasperreports=DEBUG\". Analysez les logs serveur et assurez-vous qu'une ancienne version de xerces.jar n'est pas chargée avant la version compatible avec JasperReports dans le classpath de l'application.",
						e);
			}
			String directoryName = srcFile.getParent();
			ArrayList<JRSubreport> subReports = getSubReports(jasperDesign);

			Iterator<JRSubreport> it = subReports.iterator();
			while (it.hasNext()) {
				JRSubreport jrSubreport = (JRSubreport) it.next();
				JRExpression expr = jrSubreport.getExpression();
				if (expr != null) {
					if (expr.getChunks() != null) {
						JRExpressionChunk lastChunk = expr.getChunks()[expr.getChunks().length - 1];
						String fileName = fileNameFromJRExpressionChunk(lastChunk);
						fileName = (directoryName.endsWith(File.separator) ? directoryName : directoryName.concat(File.separator)) + fileName;
						compileRecursive(fileName);
					}
					else {
						if (logger.isDebugEnabled()) {
							logger.debug("Aucun chunk récupéré depuis " + expr.getText());
						}
					}
				}
				else {
					if (logger.isDebugEnabled()) {
						logger.debug("Aucune expression trouvée pour le subReport " + jrSubreport);
					}
				}
			}
			if (logger.isDebugEnabled()) {
				logger.debug("Compilation du fichier jasper " + filePath);
			}
			JasperReport res = JasperCompileManager.compileReport(jasperDesign);
			if (logger.isDebugEnabled()) {
				logger.debug("SUCCESS : Compilation effectuee : " + filePath);
			}

			String compiledFileName = getCompiledFileNameFromSrcFileName(filePath);
			File compiledFile = new File(compiledFileName);
			if (compiledFile.exists()) {

				File backupFile = new File(compiledFile.getAbsolutePath() + "-autocompile.bak");
				if (backupFile.exists()) {
					ERXFileUtilities.deleteFile(backupFile);
				}
				if (logger.isDebugEnabled()) {
					logger.debug("Sauvegarde du precedent fichier compile vers : " + backupFile.getAbsolutePath());
				}
				ERXFileUtilities.copyFileToFile(compiledFile, backupFile, true, false);

			}
			if (logger.isDebugEnabled()) {
				logger.debug("Sauvegarde du fichier compile vers : " + compiledFileName);
			}
			JRSaver.saveObject(res, compiledFileName);
			return res;

		} catch (Exception e) {
			throw e;
		}

	}

	private String fileNameFromJRExpressionChunk(JRExpressionChunk chunk) {
		if (logger.isDebugEnabled()) {
			logger.debug("Analyse du nom du sous-report a partir de l'expression : " + chunk.getText());
		}
		String s = chunk.getText();
		String res = null;
		int end = -1;
		int start = -1;

		int len = s.length();
		for (int i = 0; i < len; i++) {
			String c = s.substring(i, i + 1);
			if (c.equals("\"")) {
				if (start == -1) {
					start = i;
				}
				else {
					if (end != -1) {
						start = end;
					}
					end = i;
				}
			}
		}

		if (start > 0 && end > start - 2) {
			res = s.substring(start + 1, end);
		}
		if (logger.isDebugEnabled()) {
			logger.debug("Nom du sous-report trouvé : " + res);
		}
		return res;

	}

	public ArrayList<JRSubreport> getSubReports(JasperDesign jasperDesign) {
		ArrayList<JRSubreport> res = new ArrayList<JRSubreport>();
		JRDataset[] datasets = jasperDesign.getDatasets();
		for (int i = 0; datasets != null && i < datasets.length; ++i) {
			JRDesignDataset dataset = (JRDesignDataset) datasets[i];
			JRGroup[] groups = dataset.getGroups();
			if ((groups != null) && (groups.length > 0)) {
				//boolean isMainDataset = dataset.isMainDataset();
				for (int index = 0; index < groups.length; ++index) {
					JRGroup group = groups[index];
					res.addAll(getSubReports(group));
				}
			}
		}
		res.addAll(getSubReports(jasperDesign.getDetailSection()));
		return res;
	}

	public ArrayList<JRSubreport> getSubReports(JRGroup group) {
		ArrayList<JRSubreport> res = new ArrayList<JRSubreport>();
		res.addAll(getSubReports(group.getGroupHeaderSection()));
		res.addAll(getSubReports(group.getGroupFooterSection()));
		return res;
	}

	public ArrayList<JRSubreport> getSubReports(JRSection section) {
		ArrayList<JRSubreport> res = new ArrayList<JRSubreport>();
		JRBand[] bands = section.getBands();
		if ((bands != null)) {
			for (int k = 0; k < bands.length; ++k) {
				res.addAll(getSubReports(bands[k]));
			}
		}
		return res;
	}

	public ArrayList<JRSubreport> getSubReports(JRBand band) {
		ArrayList<JRSubreport> res = new ArrayList<JRSubreport>();
		JRElement[] elements = band.getElements();
		if ((elements != null)) {
			for (int k = 0; k < elements.length; ++k) {
				JRElement element = elements[k];
				if (element instanceof JRSubreport) {
					res.add((JRSubreport) element);
				}
			}
		}
		return res;
	}

	/**
	 * Compile uniquement le report en parametre (et non les sous-rapport). Le report n'est pas sauvegardé sur le disque.
	 * 
	 * @param jasperDesign
	 * @return
	 * @throws JRException
	 */
	public JasperReport compile(JasperDesign jasperDesign) throws JRException {
		return JasperCompileManager.compileReport(jasperDesign);
	}

	public static String getInstalledJasperReportsVersion() throws Exception {
		try {
			return net.sf.jasperreports.engine.JasperCompileManager.class.getPackage().getImplementationVersion();

		} catch (NullPointerException nex) {
			return new String("N/A");
		}
	}

	public static String getSrcFileNameFromCompiledFileName(String fileName) {
		return ERXFileUtilities.removeFileExtension(fileName).concat(JRXML_FILE_EXTENSION);
	}

	public static String getCompiledFileNameFromSrcFileName(String fileName) {
		return ERXFileUtilities.removeFileExtension(fileName).concat(JASPER_FILE_EXTENSION);
	}

}
