/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jrxml;

import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JRXmlDataSource;

import com.webobjects.foundation.NSData;

/**
 * Permet de gerer les editions JasperReport avec sources de données xml. Utilisation possible via un thread ou directement.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class JrxmlReporterWithXmlDataSource extends JrxmlReporter {
	public class CktlJrxmlWithXmlReportingTaskThread extends CktlJrxmlReportingTaskThread {
		private JRXmlDataSource _jrxmlds;

		public CktlJrxmlWithXmlReportingTaskThread(String label, JRXmlDataSource jrxmlds, String jasperFileName, Map<String, Object> parameters, int printFormat, Boolean printIfEmpty, IJrxmlReportListener listener) {
			super(label, null, null, jasperFileName, parameters, printFormat, printIfEmpty, Boolean.FALSE, listener);
			_jrxmlds = jrxmlds;
		}

		public CktlJrxmlWithXmlReportingTaskThread(String label, JRXmlDataSource jrxmlds, String jasperFileName, Map<String, Object> parameters, int printFormat, Boolean printIfEmpty, Boolean tryCompilationWhenError, IJrxmlReportListener listener) {
			super(label, null, null, jasperFileName, parameters, printFormat, printIfEmpty, tryCompilationWhenError, listener);
			_jrxmlds = jrxmlds;
		}

		protected JasperPrint fillReport() throws JRException {
			JasperPrint _printResult = JasperFillManager.fillReport(jasperReport, getParams(), _jrxmlds);
			return _printResult;
		}

	}

	public JrxmlReporterWithXmlDataSource() {
	}

	/**
	 * @deprecatecd
	 */
	public CktlJrxmlReportingTaskThread printWithThread(String label, InputStream xmlDataSource, final String xmlRecordPath, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		return printWithThread(label, xmlDataSource, xmlRecordPath, completeFilePath, params, printFormat, printIfEmpty, Boolean.FALSE, listener);
	}

	public CktlJrxmlReportingTaskThread printWithThread(String label, InputStream xmlDataSource, final String xmlRecordPath, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, boolean tryCompilationWhenError,
			IJrxmlReportListener listener)
			throws Throwable {
		try {
			defaultReportListener = new DefaultJrxmlReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			if (listener != null)
				addListener(listener);
			setStarted(true);
			initSrcFile(completeFilePath, tryCompilationWhenError);

			//Recupérer le chemin du repertoire du report
			final File f = new File(completeFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			//preparation de la datasource xml
			JRXmlDataSource jrxmlds = null;
			if(xmlRecordPath == null) {
			  jrxmlds = new JRXmlDataSource(xmlDataSource);
			} else {
			  jrxmlds = new JRXmlDataSource(xmlDataSource, xmlRecordPath);
			}
			if (listener != null)
				listener.afterDataSourceCreated();

			setCurrentReportingTaskThread(new CktlJrxmlWithXmlReportingTaskThread(label, jrxmlds, completeFilePath, params, printFormat, printIfEmpty, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la génération du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @deprecated
	 */
	public NSData printNow(String label, InputStream xmlDataSource, final String xmlRecordPath, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		return printNow(label, xmlDataSource, xmlRecordPath, completeFilePath, params, printFormat, printIfEmpty, Boolean.FALSE, listener);
	}

	public NSData printNow(String label, InputStream xmlDataSource, final String xmlRecordPath, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, Boolean tryCompilationWhenError, IJrxmlReportListener listener) throws Throwable {
		try {
			printWithThread(label, xmlDataSource, xmlRecordPath, completeFilePath, params, printFormat, printIfEmpty, listener);
			// on attend que le thread soit terminé
			getCurrentReportingTaskThread().join();
			return printDifferedGetDataResult();

		} catch (Throwable e) {
			e.printStackTrace();
			System.err.println("impression vide avec les parametres = " + params);
			throw e;
		}
	}

	/**
	 * Ne pas utiliser cette méthode, on est dans un report avec source de donnees xml.
	 */
	@Override
	public NSData printNow(String label, Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		try {
			throw new Exception(
					"Report avec source de données XML, utilisez la methode public CktlJrxmlReportingTaskThread printWithThread(String label, Connection connection, StringWriter xmlDataSourceWriter, final String xmlRecordPath, final String completeFilePath, final Map params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener)");

		} catch (Throwable e) {
			e.printStackTrace();
			throw e;
		}
	}

	/**
	 * Ne pas utiliser cette méthode, on est dans un report avec source de donnees xml.
	 */
	@Override
	public CktlJrxmlReportingTaskThread printWithThread(String label, Connection connection, String sqlQuery, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		try {
			throw new Exception(
					"Report avec source de données XML, utilisez la methode public NSData printNow(String label, Connection connection, StringWriter xmlDataSourceWriter, final String xmlRecordPath, final String completeFilePath, final Map params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener)");

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}
}
