/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jrxml;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRXmlDataSource;
import net.sf.jasperreports.engine.export.JRExportProgressMonitor;
import net.sf.jasperreports.engine.export.JRHtmlExporter;
import net.sf.jasperreports.engine.export.JRHtmlExporterParameter;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.engine.export.JRXlsExporterParameter;
import net.sf.jasperreports.engine.type.WhenNoDataTypeEnum;
import net.sf.jasperreports.engine.util.JRLoader;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.CktlAbstractReportingTaskThread;
import org.cocktail.reporting.server.CktlDefaultReportingTaskListener;
import org.cocktail.reporting.server.exception.CktlReportingException;
import org.cocktail.reporting.server.exception.CktlReportingExceptionRapportVide;

import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;

import er.extensions.appserver.ERXApplication;
import er.extensions.foundation.ERXProperties;

/**
 * Gestion des impressions avec JasperReport (sources de donn\u221a\u00a9es sql). <br>
 * Avec possibilit\u221a\u00a9 de compiler les reports :<br>
 * Le comportement est le suivant :
 * <ul>
 * <li>Si le fichier .jasper n'est pas pr\u221a\u00a9sent et que le fichier .jrxml est trouv\u221a\u00a9, le framework tente de compiler le report (et ses sous-reports)
 * avant de l'ex\u221a\u00a9cuter.</li>
 * <li>Si le fichier .jasper est pr\u221a\u00a9sent mais qu'il y a une exception lors de l'execution, si le fichier .jrxml est pr\u221a\u00a9sent, ce dernier est compil\u221a\u00a9,
 * le fichier .jasper initial est archiv\u221a\u00a9 et le est nouveau ex\u221a\u00a9cut\u221a\u00a9.</li>
 * </ul>
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class JrxmlReporter extends CktlAbstractReporter {
	public static final Logger logger = Logger.getLogger(JrxmlReporter.class);
	protected DefaultJrxmlReportListener defaultReportListener;

	/** Nom de la cle dans le report qui represente la requete sql (si necessaire) */
	public static final String SQLQUERY_KEY = "REQUETE_SQL";
	protected JasperReport jasperReport;
	private JRDataSource _jrxmlds;
	private JasperPrint printResult;
	private byte _modeBlank;
	private int pageCount = -1;
	private int currentPageNum = -1;

	public JrxmlReporter() {

	}

	/**
	 * @return La requ\u221a\u2122te sql trouv\u221a\u00a9e dans le jasperReport
	 */
	public String getQuery() {
		if (jasperReport.getQuery() != null) {
			return jasperReport.getQuery().getText();
		}
		else if (jasperReport.getMainDataset().getQuery() != null) {
			return jasperReport.getMainDataset().getQuery().getText();
		}
		return null;
	}

	/**
	 * Thread d'impression
	 * 
	 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
	 */
	public class CktlJrxmlReportingTaskThread extends CktlAbstractReportingTaskThread {

		private final int _printFormat;
		private final Boolean _printIfEmpty;
		private Boolean _tryCompilationWhenError;

		/**
		 * @param label Permet d'indiquer un label qui sera affect\u221a\u00a9 \u221a\u2020 la tache d'impression (peut \u221a\u2122tre utile dans le
		 *            cas de messages de progression).
		 * @param connection Connection \u221a\u2020 la base
		 * @param sqlQuery Requete sql. Null si celle-ci est embarqu\u221a\u00a9e dans le fichier jasper
		 * @param completeJasperFilePath Chemin d'acc\u221a\u00aes complet au report
		 * @param params Parametres \u221a\u2020 passer au jasper (HashMap)
		 * @param printFormat
		 * @param printIfEmpty Si FALSE, rien n'est imprimé. Si TRUE, le comportement indiqué dans le report est utilisé.
		 * @param tryCompilationWhenError Si true, dans le cas o\u221a\u03c0 l'ex\u221a\u00a9cution du .jasper renvoie une erreur, la compilation du
		 *            jrxml (s'il est pr\u221a\u00a9sent) sera tent\u221a\u00a9e.
		 * @param listener Listener qui recevra les notifications de progression de l'execution du report.
		 * @param listener
		 */
		public CktlJrxmlReportingTaskThread(String label, Connection connection, String sqlQuery, final String jasperFileName, final Map<String, Object> parameters, final int printFormat, final Boolean printIfEmpty, final Boolean tryCompilationWhenError, IJrxmlReportListener listener) {
			super(label, connection, sqlQuery, jasperFileName, parameters, listener);
			_printFormat = printFormat;
			_tryCompilationWhenError = tryCompilationWhenError;
			_printIfEmpty = printIfEmpty;
		}

		public void run() {
		    ERXApplication._startRequest();
			try {

				if (!_printIfEmpty.booleanValue()) {
					jasperReport.setWhenNoDataType(WhenNoDataTypeEnum.NO_PAGES);
				}

				notifyAfterReportStarted();
				setDataResult(null);
				try {
					if (logger.isDebugEnabled()) {
						if (getSqlQuery() != null && getSqlQuery().length() > 0) {
							logger.debug(getLabel() + " SQL (transmis) = " + getSqlQuery());
						}
						else {
							logger.debug(getLabel() + " SQL (integre) = " + getQuery());
						}
					}

					if (logger.isDebugEnabled()) {
						logger.debug(getLabel() + " report rempli - " + pageCount + " pages");
					}
					printResult = fillReport();
					pageCount = printResult.getPages().size();
					notifyAfterReportBuild(pageCount);
					Thread.yield();
					if (logger.isDebugEnabled()) {
						logger.debug(getLabel() + " report rempli - " + pageCount + " pages");
					}
					notifyTrace(getLabel() + " report rempli - " + pageCount + " pages");

					if (pageCount == 0) {
						notifyTrace("query recuperee dans le report avant exec = " + getQuery());
						throw new CktlReportingExceptionRapportVide(NOPAGES_MSG);
					}
				} catch (Throwable e) {
					throw e;
				}
				//setLastReport(getCurrentReport());
				Thread.yield();
				ByteArrayOutputStream s;
				switch (_printFormat) {
				case EXPORT_FORMAT_PDF:
					s = exportToPdf();
					break;

				case EXPORT_FORMAT_XLS:
					s = exportToXls();
					break;
				case EXPORT_FORMAT_HTML:
					s = exportToHtml();
					break;
				default:
					s = exportToPdf();
					break;
				}
				setDataResult(new NSData(s.toByteArray()));
				notifyAfterReportFinish();

			} catch (CktlReportingException e) {
				e.setFileName(getFileName());
				e.setQuery(getQuery());
				e.setParams(getParams());
				setLastPrintError(e);
				notifyAfterException(e);
			} catch (Throwable e) {
				CktlReportingException except = new CktlReportingException("Erreur lors de la cr\u221a\u00a9ation du rapport Jasper", e);
				except.setFileName(getFileName());
				except.setQuery(getQuery());
				except.setParams(getParams());
				setLastPrintError(except);
				notifyAfterException(except);
			} finally {
                ERXApplication._endRequest();
			}
		}

		protected JasperPrint fillReport() throws Exception {
			boolean compile = false;
			Exception ex;
			//On ajoute la requete SQL aux parametres si celle-ci n'est pas nulle
			if (getSqlQuery() != null) {
				getParams().put(SQLQUERY_KEY, getSqlQuery());
			}

			//parameters.put("net.sf.jasperreports.jdbc.fetch.size", new Integer(10000));
			try {
				JasperPrint _printResult = JasperFillManager.fillReport(jasperReport, getParams(), getConnection());
				return _printResult;
			} catch (Exception e) {
				e.printStackTrace();
				if (_tryCompilationWhenError) {
					compile = true;
					ex = e;
				}
				else {
					throw e;
				}
			}
			if (compile) {
				//si exception et jrxml pr\u221a\u00a9sent, on tente la compilation puis un nouveau remplissage
				if (_tryCompilationWhenError) {
					if (logger.isDebugEnabled()) {
						logger.debug("Le report a renvoy\u221a\u00a9 une erreur.");
					}
					String filePath = getFileName();
					if (filePath.endsWith(JrxmlCompiler.JASPER_FILE_EXTENSION)) {
						filePath = JrxmlCompiler.getSrcFileNameFromCompiledFileName(filePath);
					}
					File srcFile = new File(filePath);
					if (srcFile.exists()) {
						if (logger.isDebugEnabled()) {
							logger.debug("Tentative de compilation du report source " + srcFile);
						}
						JrxmlCompiler compiler = new JrxmlCompiler();
						jasperReport = compiler.compileRecursive(getFileName());
						try {
							JasperPrint _printResult = JasperFillManager.fillReport(jasperReport, getParams(), getConnection());
							return _printResult;
						} catch (Exception e) {
							throw new Exception("Erreur lors de l'execution du report compil\u221a\u00a9", e);
						}
					}
					else {
						if (logger.isDebugEnabled()) {
							logger.debug("Impossible de compiler le report source, le fichier " + srcFile + " n'a pas \u221a\u00a9t\u221a\u00a9 trouv\u221a\u00a9.");
						}
						throw new Exception("Impossible de compiler le report source, le fichier " + srcFile + " n'a pas \u221a\u00a9t\u221a\u00a9 trouv\u221a\u00a9. Erreur originale :" + ex.getMessage(), ex);
					}
				}
			}
			return null;
		}
	}

	public class DefaultJrxmlReportListener extends CktlDefaultReportingTaskListener implements IJrxmlReportListener {

	}

	public Boolean isResultReady() {
		return Boolean.valueOf(getCurrentReportingTaskThread() != null && getCurrentReportingTaskThread().getDataResult() != null);
	}

	/**
	 * @param connection Connection \u221a\u2020 la base
	 * @param sqlQuery Requete sql. Null si celle-ci est embarqu\u221a\u00a9e dans le fichier jasper
	 * @param completeJasperFilePath Chemin d'acc\u221a\u00aes complet au report
	 * @param params Parametres \u221a\u2020 passer au jasper (HashMap)
	 * @param printFormat
	 * @param printIfEmpty n/a.
	 * @param tryCompilationWhenError Si true, dans le cas o\u221a\u03c0 l'ex\u221a\u00a9cution du .jasper renvoie une erreur, la compilation du jrxml (s'il est pr\u221a\u00a9sent)
	 *            sera tent\u221a\u00a9e.
	 * @param listener Listener qui recevra les notifications de progression de l'execution du report.
	 * @return
	 * @throws Throwable
	 */
	public NSData printNow(Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, Boolean tryCompilationWhenError, IJrxmlReportListener listener) throws Throwable {
		return printNow(null, connection, sqlQuery, completeJasperFilePath, params, printFormat, printIfEmpty, tryCompilationWhenError, listener);
	}

	/**
	 * @param label Permet d'indiquer un label qui sera affect\u221a\u00a9 \u221a\u2020 la tache d'impression (peut \u221a\u2122tre utile dans le cas de messages de progression).
	 * @param connection Connection \u221a\u2020 la base
	 * @param sqlQuery Requete sql. Null si celle-ci est embarqu\u221a\u00a9e dans le fichier jasper
	 * @param completeJasperFilePath Chemin d'acc\u221a\u00aes complet au report
	 * @param params Parametres \u221a\u2020 passer au jasper (HashMap)
	 * @param printFormat
	 * @param printIfEmpty n/a.
	 * @param tryCompilationWhenError Si true, dans le cas o\u221a\u03c0 l'ex\u221a\u00a9cution du .jasper renvoie une erreur, la compilation du jrxml (s'il est pr\u221a\u00a9sent)
	 *            sera tent\u221a\u00a9e.
	 * @param listener Listener qui recevra les notifications de progression de l'execution du report.
	 * @return
	 * @throws Throwable
	 */
	public NSData printNow(String label, Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, Boolean tryCompilationWhenError, IJrxmlReportListener listener) throws Throwable {
		try {
			printWithThread(label, connection, sqlQuery, completeJasperFilePath, params, printFormat, printIfEmpty, tryCompilationWhenError, listener);
			// on attend que le thread soit termin\u221a\u00a9
			getCurrentReportingTaskThread().join();
			return printDifferedGetDataResult();

		} catch (Throwable e) {
			e.printStackTrace();
			System.err.println("impression vide avec les parametres = " + params);
			throw e;
		}
	}

	protected void initSrcFile(String jasperFileName, boolean tryCompilationWhenError) throws Exception {
		try {
			pageCount = -1;
			currentPageNum = -1;
			if (StringCtrl.isEmpty(jasperFileName)) {
				throw new Exception("Nom de fichier vide...");
			}
			final File f = new File(jasperFileName);
			if (!f.exists()) {
				if (logger.isDebugEnabled()) {
					logger.debug("Fichier " + jasperFileName + " non trouv\u221a\u00a9");
				}
				//Si le fichier compil\u221a\u00a9 n'existe pas, on v\u221a\u00a9rifie l'exitence du fichier source et on le compile.
				if (tryCompilationWhenError) {
					File sourceFile = new File(JrxmlCompiler.getSrcFileNameFromCompiledFileName(jasperFileName));
					if (logger.isDebugEnabled()) {
						logger.debug("Tentative de compilation de " + sourceFile + " ");
					}
					if (sourceFile.exists()) {
						JrxmlCompiler compiler = new JrxmlCompiler();
						jasperReport = compiler.compileRecursive(sourceFile.getAbsolutePath());
					}
					else {
						if (logger.isDebugEnabled()) {
							logger.debug("Fichier " + sourceFile + " non trouv\u221a\u00a9");
						}
						throw new Exception("Le fichier " + jasperFileName + " n'a pas \u221a\u00a9t\u221a\u00a9 trouv\u221a\u00a9, ni le fichier " + sourceFile + ".");
					}
				}
				else {
					throw new Exception("Le fichier " + jasperFileName + " n'a pas \u221a\u00a9t\u221a\u00a9 trouv\u221a\u00a9.");
				}

			}

			notifyTrace("Chargement de " + jasperFileName + " ...");
			jasperReport = (JasperReport) JRLoader.loadObjectFromFile(jasperFileName);

			if (jasperReport != null) {
				notifyTrace(jasperFileName + " charg\u221a\u00a9.");
			}
			else {
				throw new Exception("Impossible de charger le fichier jasper " + jasperFileName);
			}

		} catch (Exception e) {
			notifyAfterException(e);
			throw e;
		}
	}

	/**
	 * @deprecated
	 */
	public CktlJrxmlReportingTaskThread printWithThread(String label, Connection connection, String sqlQuery, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		return printWithThread(label, connection, sqlQuery, completeFilePath, params, printFormat, printIfEmpty, Boolean.FALSE, listener);
	}

	/**
	 * @param label
	 * @param connection
	 * @param sqlQuery
	 * @param completeFilePath
	 * @param params
	 * @param printFormat
	 * @param printIfEmpty
	 * @param tryCompilationWhenError
	 * @param listener
	 * @return
	 * @throws Throwable
	 */
	public CktlJrxmlReportingTaskThread printWithThread(String label, Connection connection, String sqlQuery, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, Boolean tryCompilationWhenError, IJrxmlReportListener listener)
			throws Throwable {
		try {
			defaultReportListener = new DefaultJrxmlReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			addListener(listener);

			setStarted(true);

			initSrcFile(completeFilePath, tryCompilationWhenError);

			//Recup\u221a\u00a9rer le chemin du repertoire du report
			final File f = new File(completeFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			setCurrentReportingTaskThread(new CktlJrxmlReportingTaskThread(label, connection, sqlQuery, completeFilePath, params, printFormat, printIfEmpty, tryCompilationWhenError, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la g\u221a\u00a9n\u221a\u00a9ration du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * Parametre le report pour generer ou non des pages suivant le cas : Cf. net.sf.jasperreports.engine.type.WhenNoDataTypeEnum
	 * 
	 * @param whenNoData
	 */
	public void setWhenNoDataType(final WhenNoDataTypeEnum whenNoData) {
		jasperReport.setWhenNoDataType(whenNoData);
	}

	public ByteArrayOutputStream exportToPdf() throws JRException {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		final JRExporter exporter = new JRPdfExporter();
		final JRExportProgressMonitor exportProgressMonitor = new JRExportProgressMonitor() {
			public void afterPageExport() {
				currentPageNum++;
				notifyAfterPageExport(currentPageNum, pageCount);
				Thread.yield();
			}
		};
		exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, exportProgressMonitor);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, getPrintResult());
		currentPageNum = 1;
		exporter.exportReport();
		notifyAfterReportExport();
		Thread.yield();
		return tmpStream;
	}

	public ByteArrayOutputStream exportToXls() throws JRException {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		final JRExporter exporter = new JRXlsExporter();
		final JRExportProgressMonitor exportProgressMonitor = new JRExportProgressMonitor() {
			public void afterPageExport() {
				currentPageNum++;
				notifyAfterReportExport();
				Thread.yield();
			}
		};

		exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, exportProgressMonitor);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, getPrintResult());
		if(ERXProperties.booleanForKeyWithDefault("org.cocktail.reporting.jrxml.xls.removespacebetweenrows", true)) {
			exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
		}
		exporter.setParameter(JRXlsExporterParameter.IS_WHITE_PAGE_BACKGROUND, Boolean.TRUE);

		currentPageNum = 1;
		exporter.exportReport();
		notifyAfterReportExport();
		Thread.yield();
		return tmpStream;
	}

	public ByteArrayOutputStream exportToHtml() throws JRException {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		final JRExporter exporter = new JRHtmlExporter();
		final JRExportProgressMonitor exportProgressMonitor = new JRExportProgressMonitor() {
			public void afterPageExport() {
				currentPageNum++;
				notifyAfterPageExport(currentPageNum, pageCount);
				Thread.yield();
			}
		};
		exporter.setParameter(JRExporterParameter.PROGRESS_MONITOR, exportProgressMonitor);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, tmpStream);
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, getPrintResult());
		exporter.setParameter(JRExporterParameter.IGNORE_PAGE_MARGINS, true);
		exporter.setParameter(JRHtmlExporterParameter.IS_USING_IMAGES_TO_ALIGN, false);
		exporter.setParameter(JRHtmlExporterParameter.HTML_HEADER, "");
		exporter.setParameter(JRHtmlExporterParameter.HTML_FOOTER, "");
		exporter.setParameter(JRHtmlExporterParameter.CHARACTER_ENCODING, "UTF8");
		exporter.setParameter(JRHtmlExporterParameter.ZOOM_RATIO, (float) 1.5);

		currentPageNum = 1;
		exporter.exportReport();
		notifyAfterReportExport();
		Thread.yield();
		return tmpStream;
	}

	public CktlJrxmlReportingTaskThread getCurrentReportingTaskThread() {
		return (CktlJrxmlReportingTaskThread) super.getCurrentReportingTaskThread();
	}

	public JasperPrint getPrintResult() {
		return printResult;
	}

	public void setPrintResult(JasperPrint printResult) {
		this.printResult = printResult;
	}

	/**
	 * @deprecated
	 * @param connection
	 * @param sqlQuery
	 * @param parametres
	 * @param completeJasperFilePath
	 * @param printIfEmpty
	 * @param listener
	 * @throws Exception
	 */
	public final void printByThread(Connection connection, String sqlQuery, NSDictionary parametres, String completeJasperFilePath, Boolean printIfEmpty, IJrxmlReportListener listener) throws Exception {
		try {
			HashMap params = null;
			if (parametres != null) {
				params = new HashMap(parametres.hashtable());
				System.out.println("parametres : " + params);
			}

			//getCurrentReportingTaskThread() = null;
			setCurrentReportingTaskThread(null);
			//lastReport = null;
			setLastPrintError(null);

			//Recup\u221a\u00a9rer le chemin du repertoire du report
			final File f = new File(completeJasperFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			setCurrentReportingTaskThread(new CktlJrxmlReportingTaskThread(null, connection, sqlQuery, completeJasperFilePath, params, EXPORT_FORMAT_PDF, printIfEmpty, Boolean.FALSE, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la g\u221a\u00a9n\u221a\u00a9ration du report et on s'en va...
			getCurrentReportingTaskThread().start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @deprecated
	 * @param connection
	 * @param sqlQuery
	 * @param params
	 * @param completeJasperFilePath
	 * @param printIfEmpty
	 * @param listener
	 * @throws Exception
	 */
	public final void printByThread(Connection connection, String sqlQuery, Map<String, Object> params, String completeJasperFilePath, Boolean printIfEmpty, IJrxmlReportListener listener) throws Exception {
		try {
			setCurrentReportingTaskThread(null);
			//	lastReport = null;
			setLastPrintError(null);

			//Recup\u221a\u00a9rer le chemin du repertoire du report
			final File f = new File(completeJasperFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			setCurrentReportingTaskThread(new CktlJrxmlReportingTaskThread(null, connection, sqlQuery, completeJasperFilePath, params, EXPORT_FORMAT_PDF, printIfEmpty, Boolean.FALSE, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la g\u221a\u00a9n\u221a\u00a9ration du report et on s'en va...
			getCurrentReportingTaskThread().start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @deprecated
	 * @param connection
	 * @param sqlQuery
	 * @param parametres
	 * @param completeJasperFilePath
	 * @param listener
	 * @throws Exception
	 */
	public final void printByThreadXls(Connection connection, String sqlQuery, NSDictionary parametres, String completeJasperFilePath, IJrxmlReportListener listener) throws Exception {
		try {
			//		    ZAbstractReport currentReport=null;
			Hashtable params = null;
			if (parametres != null) {
				params = parametres.hashtable();
				System.out.println("parametres : " + params);
			}

			setCurrentReportingTaskThread(null);
			//lastReport = null;
			setLastPrintError(null);

			//Recup\u221a\u00a9rer le chemin du repertoire du report
			final File f = new File(completeJasperFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			setCurrentReportingTaskThread(new CktlJrxmlReportingTaskThread(null, connection, sqlQuery, completeJasperFilePath, params, EXPORT_FORMAT_XLS, Boolean.FALSE, Boolean.FALSE, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la generation du report et on s'en va...
			//ZLogger.info(" Tache d'impression lancee pour" + realJasperFilName + " ...");
			getCurrentReportingTaskThread().start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @deprecated
	 * @param connection
	 * @param sqlQuery
	 * @param params
	 * @param completeJasperFilePath
	 * @param listener
	 * @throws Exception
	 */
	public final void printByThreadXls(Connection connection, String sqlQuery, Map<String, Object> params, String completeJasperFilePath, IJrxmlReportListener listener) throws Exception {
		try {

			setCurrentReportingTaskThread(null);
			//lastReport = null;
			setLastPrintError(null);

			//Recup\u221a\u00a9rer le chemin du repertoire du report
			final File f = new File(completeJasperFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);
			setCurrentReportingTaskThread(new CktlJrxmlReportingTaskThread(null, connection, sqlQuery, completeJasperFilePath, params, EXPORT_FORMAT_XLS, Boolean.FALSE, Boolean.FALSE, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la generation du report et on s'en va...
			//ZLogger.info(" Tache d'impression lancee pour" + realJasperFilName + " ...");
			getCurrentReportingTaskThread().start();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * @deprecated
	 */
	public NSData printNow(Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		return printNow(null, connection, sqlQuery, completeJasperFilePath, params, printFormat, printIfEmpty, Boolean.FALSE, listener);
	}

	/**
	 * @deprecated
	 */
	public NSData printNow(String label, Connection connection, String sqlQuery, final String completeJasperFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJrxmlReportListener listener) throws Throwable {
		return printNow(label, connection, sqlQuery, completeJasperFilePath, params, printFormat, printIfEmpty, Boolean.FALSE, listener);
	}

	/**
	 * Initialise la datasource \u221a\u2020 partir du flux xml
	 * 
	 * @param xmlDataStream Flux des datas xml
	 * @param xmlRecordPath Le recordPath
	 * @throws JRException
	 * @deprecated
	 */
	public void initDataSource(InputStream xmlDataStream, String xmlRecordPath) throws JRException {
		_jrxmlds = new JRXmlDataSource(xmlDataStream, xmlRecordPath);
		notifyTrace("ReportFactory.initDataSource() " + "Datasource XML creee avec recordpath : " + xmlRecordPath);
	}

	/**
	 * Initialise une datasource vide.
	 * 
	 * @deprecated
	 * @throws JRException
	 */
	public void initEmptyDataSource() throws JRException {
		_jrxmlds = new JREmptyDataSource();
		notifyTrace("ReportFactory.initDataSource() " + "Datasource vide cree");
	}

	/**
	 * @deprecated
	 * @throws Exception
	 */
	public void prepareDataSource() throws Exception {
		//		if (_mode == MODE_XML) {
		//			if (_xmlDataSourceWriter != null) {
		//				final byte[] xmlbytes = _xmlDataSourceWriter.toString().getBytes();
		//				final InputStream xmlStream = new ByteArrayInputStream(xmlbytes);
		//				initDataSource(xmlStream, _xmlRecordPath);
		//			}
		//			else {
		initEmptyDataSource();
		//			}
		//
		//		}
	}

	/**
	 * @deprecated
	 * @param parameters Parametres \u221a\u00d8\u00ac\u00f8\u00ac\u03a9 passer au report
	 * @throws JRException
	 */
	private void _printReport(final Map<String, Object> parameters) throws Exception {
		printResult = JasperFillManager.fillReport(jasperReport, parameters, _jrxmlds);
		pageCount = printResult.getPages().size();
		notifyAfterReportBuild(pageCount);
		Thread.yield();
		notifyTrace("ReportFactory.printReport() " + " report rempli - " + pageCount + " pages");

		if (pageCount == 0) {
			throw new Exception(NOPAGES_MSG);
		}

	}

	/**
	 * Remplit le report \u221a\u2020 partir d'une requete sql sp\u221a\u00a9cifie en parametre. Fonctionne \u221a\u00a9galement si la requete est incluse dans le report (dans ce cas,
	 * sp\u221a\u00a9cifier null pour le parametre sqlQuery).
	 * 
	 * @deprecated
	 * @param connection Connection \u221a\u2020 la base de donn\u221a\u00d8\u00ac\u00f8\u00ac\u03a9es.
	 * @param sqlQuery Requete SQL. Si la chaine n'est pas nulle, elle est ajout\u221a\u00a9e aux parametres pass\u221a\u00a9s au report (@see SQLQUERY_KEY)
	 * @param parameters Parametres \u221a\u2020 passer au report
	 * @throws JRException
	 */
	private void _printReport(final Connection connection, final String sqlQuery, final Map<String, Object> parameters) throws Exception {
		try {
			//On ajoute la requete SQL aux parametres si celle-ci n'est pas nulle
			if (sqlQuery != null) {
				parameters.put(SQLQUERY_KEY, sqlQuery);
			}

			//parameters.put("net.sf.jasperreports.jdbc.fetch.size", new Integer(10000));
			printResult = JasperFillManager.fillReport(jasperReport, parameters, connection);

			pageCount = printResult.getPages().size();
			notifyAfterReportBuild(pageCount);
			Thread.yield();
			notifyTrace("ReportFactory.printReport() " + " report rempli - " + pageCount + " pages");

			if (pageCount == 0) {
				if (jasperReport.getQuery() != null) {
					notifyTrace("query recuperee dans le report avant exec = " + jasperReport.getQuery().getText());
				}
				if (jasperReport.getMainDataset().getQuery() != null) {
					notifyTrace("query recuperee dans le report main dataset avant exec = " + jasperReport.getMainDataset().getQuery().getText());
				}

				throw new Exception(NOPAGES_MSG);
			}
		} catch (Exception e) {
			e.printStackTrace();
			notifyAfterException(e);
		}

	}

	//	private Integer getStateLock() {
	//		return stateLock;
	//	}

	/**
	 * @deprecated
	 * @param parameters
	 * @throws Exception
	 */
	public void printReport(final Map<String, Object> parameters) throws Exception {
		_printReport(parameters);
	}

	/**
	 * @deprecated
	 * @param connection
	 * @param sqlQuery
	 * @param parameters
	 * @throws Exception
	 */
	public void printReport(final Connection connection, final String sqlQuery, final Map<String, Object> parameters) throws Exception {
		_printReport(connection, sqlQuery, parameters);
	}

}
