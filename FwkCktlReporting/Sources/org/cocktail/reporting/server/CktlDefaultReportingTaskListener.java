/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server;

import org.apache.log4j.Logger;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlDefaultReportingTaskListener implements ICktlReportingTaskListener {
	public final static Logger logger = Logger.getLogger(CktlDefaultReportingTaskListener.class);

	private int _pageNum = -1;
	private int _pageCount = -1;
	private int _pageTotalCount = -1;
	private boolean dataSourceCreated = false;
	private boolean reportStarted = false;
	private boolean reportBuild = false;
	private boolean reportExported = false;
	private boolean reportCanceled = false;

	public synchronized int getPageCount() {
		return _pageCount;
	}

	public synchronized int getPageTotalCount() {
		return _pageTotalCount;
	}

	public synchronized int getPageNum() {
		return _pageNum;
	}

	public synchronized boolean isDataSourceCreated() {
		return dataSourceCreated;
	}

	public synchronized boolean isReportExported() {
		return reportExported;
	}

	public synchronized boolean isReportBuild() {
		return reportBuild;
	}

	public synchronized void afterDataSourceCreated() {
		if (logger.isDebugEnabled()) {
			logger.debug("afterDataSourceCreated");
		}
		dataSourceCreated = true;
		Thread.yield();
	}

	public synchronized void afterReportBuild(int pageCount) {
		if (logger.isDebugEnabled()) {
			logger.debug("afterReportBuild " + pageCount);
		}
		reportBuild = true;
		_pageTotalCount = pageCount;
		Thread.yield();
	}

	public synchronized void afterPageExport(int pageNum, int pageCount) {
		if (logger.isDebugEnabled()) {
			logger.debug("afterReportBuild " + pageNum + "/" + pageCount);
		}
		_pageCount = pageCount;
		_pageNum = pageNum;
		Thread.yield();
	}

	public synchronized void afterReportExport() {
		if (logger.isDebugEnabled()) {
			logger.debug("afterReportExport ");
		}
		reportExported = true;
		Thread.yield();
	}

	public synchronized void afterReportFinish() {
		if (logger.isDebugEnabled()) {
			logger.debug("afterReportFinish ");
		}
		Thread.yield();
	}

	public void trace(String s) {
		//System.out.println(s);
	}

	public void afterReportStarted() {
		if (logger.isDebugEnabled()) {
			logger.debug("afterReportStarted ");
		}
		reportStarted = true;
		Thread.yield();
	}

	public synchronized boolean isReportStarted() {
		return reportStarted;
	}

	public void setReportStarted(boolean reportStarted) {
		this.reportStarted = reportStarted;
	}

	public synchronized void afterException(Throwable e) {
		e.printStackTrace();
	}

	public void afterReportCanceled() {
		reportCanceled = true;
	}

}
