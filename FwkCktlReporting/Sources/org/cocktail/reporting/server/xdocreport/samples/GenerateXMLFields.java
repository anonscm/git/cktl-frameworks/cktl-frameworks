/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.xdocreport.samples;

import java.io.File;
import java.io.FileOutputStream;

import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

/**
 * Classe permettant de créer un fichier *.fields.xml, utilisé par la macro de XDocReport dans OpenOffice ou Word
 */
public final class GenerateXMLFields {

	private GenerateXMLFields() {
	}

	/**
	 * Permet de générer un fichier XML contenant tous les champs contenus dans les classes données, et de pouvoir
	 * l'importer dans OpenOffice, grâce aux macro de <code>fr.opensagres.xdocreport.openoffice.macro-xxx.oxt</code>
	 * 
	 * @param args
	 *            Triplets contenant :
	 *            <ul>
	 *            <li>La clé, identifiant la classe dans le document</li>
	 *            <li>Le nom de la classe</li>
	 *            <li>true ou false, si c'est une liste (cf. ODTReportingJavaMainListField, permet de faire des foreach)
	 *            </li>
	 *            </ul>
	 *            Exemple : <code>GenerateXMLFields.main("stage", ConventionBean.class.getName(), "false");</code><br>
	 *            <b>ATTENTION</b> : éviter de mettre un getEOCeQueTuVeux() dans le bean, sinon Java heap space...
	 * @throws Exception
	 *             Si erreur de lecture des Beans, ou d'ecriture du fichier
	 */
	public static void main(String... args) throws Exception {
		main("project", args);
	}

	/**
	 * Permet de générer un fichier XML contenant tous les champs contenus dans les classes données, et de pouvoir
	 * l'importer dans OpenOffice, grâce aux macro de <code>fr.opensagres.xdocreport.openoffice.macro-xxx.oxt</code>
	 * 
	 * @param name
	 *            Première partie du nom du fichier, qui sera de la forme : <code>name.fields.xml</code>
	 * @param args
	 *            Triplets contenant :
	 *            <ul>
	 *            <li>La clé, identifiant la classe dans le document</li>
	 *            <li>Le nom de la classe</li>
	 *            <li>true ou false, si c'est une liste (cf. ODTReportingJavaMainListField, permet de faire des foreach)
	 *            </li>
	 *            </ul>
	 *            Exemple : <code>GenerateXMLFields.main("stage", ConventionBean.class.getName(), "false");</code><br>
	 *            <b>ATTENTION</b> : éviter de mettre un getEOCeQueTuVeux() dans le bean, sinon Java heap space...<br>
	 *            Et si malgré tout, il y a un Java heap space, supprimer les variables EOObjects privées dans les beans
	 * @throws Exception
	 *             Si erreur de lecture des Beans, ou d'ecriture du fichier
	 */
	public static void main(String name, String... args) throws Exception {
		if (args.length == 0 || args.length % 3 != 0) {
			System.err.println("Usage :  ./GenerateXMLFields [name] <key className isList> ...");
			System.exit(-1);
		}
		// 1) Create FieldsMetadata by setting Velocity as template engine
		FieldsMetadata fieldsMetadata = new FieldsMetadata(TemplateEngineKind.Velocity.name());
		for (int i = 0; i < args.length; i++) {
			String key = args[i];
			String clazz = args[++i];
			boolean isList = Boolean.valueOf(args[++i]);
			// 2) Load fields metadata from Java Class
			fieldsMetadata.load(key, Class.forName(clazz), isList);
		}
		// 3) Generate XML fields in the file "project.fields.xml".
		// Extension *.fields.xml is very important to use it with MS Macro XDocReport.dotm
		// FieldsMetadata#saveXML is called with true to indent the XML.
		File xmlFieldsFile = new File(name + ".fields.xml");
		fieldsMetadata.saveXML(new FileOutputStream(xmlFieldsFile), true);
		System.out.println("Enregistrement du fichier réussi : " + xmlFieldsFile.getAbsolutePath());
	}
}
