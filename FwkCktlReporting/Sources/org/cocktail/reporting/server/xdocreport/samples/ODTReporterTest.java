/**
 * $Id$
 */
package org.cocktail.reporting.server.xdocreport.samples;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;

import org.cocktail.reporting.server.exception.CktlReportingException;
import org.cocktail.reporting.server.xdocreport.ODTReporter;
import org.cocktail.reporting.server.xdocreport.samples.model.MonObjet;

/**
 * @author Julien BLANDINEAU <jblandin@univ-lr.fr>
 *
 */
public class ODTReporterTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			// InputStream input = new FileInputStream("/Volumes/Donnees/Workspace/FwkCktlStages/Resources/modeles/ODTProjectWithVelocity.odt");
			InputStream input = ODTReporterTest.class.getResourceAsStream("ODTProjectWithVelocity.odt");
			
			ODTReporter project = new ODTReporter(input);
			project.addContextParam("project", new MonObjet("Un test !"));

			OutputStream resultPDF = new FileOutputStream("/tmp/ODTProjectWithVelocity_result.pdf");
			project.exportToPDF(null);

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		} catch (CktlReportingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
