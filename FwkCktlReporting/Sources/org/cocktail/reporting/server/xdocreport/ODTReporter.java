/**
 * $Id$
 */
package org.cocktail.reporting.server.xdocreport;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;

import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.CktlAbstractReportingTaskThread;
import org.cocktail.reporting.server.CktlDefaultReportingTaskListener;
import org.cocktail.reporting.server.exception.CktlReportingException;

import com.webobjects.foundation.NSData;

import fr.opensagres.xdocreport.converter.ConverterTypeTo;
import fr.opensagres.xdocreport.converter.ConverterTypeVia;
import fr.opensagres.xdocreport.converter.Options;
import fr.opensagres.xdocreport.converter.XDocConverterException;
import fr.opensagres.xdocreport.core.XDocReportException;
import fr.opensagres.xdocreport.document.IXDocReport;
import fr.opensagres.xdocreport.document.registry.XDocReportRegistry;
import fr.opensagres.xdocreport.template.IContext;
import fr.opensagres.xdocreport.template.TemplateEngineKind;
import fr.opensagres.xdocreport.template.formatter.FieldsMetadata;

/**
 * @author Julien BLANDINEAU <jblandin@univ-lr.fr>
 *
 */
public class ODTReporter extends CktlAbstractReporter {

	private InputStream modeleIS;
	private IXDocReport aReport;
	private IContext aContext;

	private DefaultODTReportListener defaultReportListener;

	private static Options optionsConvertToPDF = Options.getTo(ConverterTypeTo.PDF).via(ConverterTypeVia.ODFDOM);

	public ODTReporter() {

	}

	/**
	 * @param aModele
	 * @throws CktlReportingException
	 * @Deprecated
	 */
	@Deprecated
	public ODTReporter(InputStream aModele)
			throws CktlReportingException {
		modeleIS = aModele;

		try {
			aReport = XDocReportRegistry.getRegistry().loadReport(modeleIS, TemplateEngineKind.Velocity);
			aContext = aReport.createContext();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CktlReportingException(e.getMessage());
		}
	}

	/**
	 * Ajoute une donnée au {@link IContext}<br>
	 * Exemple : <br>
	 * <br>
	 * <code>public class MonObjet {<br>
	 * &nbsp;private String name;<br>
	 * &nbsp;public MonObject(String aName) { name = aName; }<br>
	 * &nbsp;public getName() { return name; } <br>
	 *  }<br>
	 *  <br>
	 *  ...
	 *  <br><br>
	 *  addContextParam("unObjet", new MonObjet("Le nom de mon objet !"));<br>
	 *  </code><br>
	 * Dans le fichier modèle ODT, on pourra alors écrire : "
	 * <code>Un objet : $unObjet.Name</code>"<br>
	 * <br>
	 * Le résultat : "<code>Un objet : Le nom de mon objet !</code>"<br>
	 * <br>
	 * A noter que le "<code>$unObjet.Name</code>" doit etre dans un champ de
	 * saisie Attention : l'objet donnée en paramètre doit être de type JavaBean
	 *
	 *
	 * @param key
	 *            La clé utilisée dans le modèle pour accéder à la donnée
	 * @param value
	 *            La donnée
	 */
	public void addContextParam(String key, Object value) {
		aContext.put(key, value);
	}

	/**
	 * Ajoute une liste de données au {@link IContext}
	 *
	 * @see #addContextParam(String, Object)
	 * @param liste
	 */
	public void addContextParams(Map<String, Object> liste) {
		if (liste != null && !liste.isEmpty())
			for (Map.Entry<String, Object> anEntry : liste.entrySet())
				addContextParam(anEntry.getKey(), anEntry.getValue());
	}

	/**
	 * Génère un fichier ODT
	 *
	 * @param out
	 *            Le flux de sortie
	 * @throws XDocReportException
	 * @throws IOException
	 */
	public ByteArrayOutputStream exportToODT(Map<String, Object> params) throws Exception {
		try {
			addContextParams(params);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			aReport.process(aContext, out);
			notifyAfterReportExport();
			Thread.yield();
			return out;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur lors de la génération du fichier", e);
		}
	}

	/**
	 * Génère un fichier PDF
	 *
	 * @param out
	 *            Le flux de sortie
	 * @throws XDocConverterException
	 * @throws XDocReportException
	 * @throws IOException
	 */
	public ByteArrayOutputStream exportToPDF(Map<String, Object> params) throws Exception {
		try {
			addContextParams(params);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			aReport.convert(aContext, optionsConvertToPDF, out);
			notifyAfterReportExport();
			Thread.yield();
			return out;
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("Erreur lors de l'export du fichier", e);
		}
	}

	@Override
	public Boolean isResultReady() {
		return Boolean.valueOf(getCurrentReportingTaskThread() != null && getCurrentReportingTaskThread().getDataResult() != null);
	}

	public class DefaultODTReportListener extends CktlDefaultReportingTaskListener {

	}

	public class CktlODTReportingTaskThread extends CktlAbstractReportingTaskThread {
		private List<String> fields;

		/**
		 * @param label
		 *            Permet d'indiquer un label qui sera affecté à la tache
		 *            d'impression (peut être utile dans le cas de messages de
		 *            progression).
		 *
		 * @param completeJasperFilePath
		 *            Chemin d'accès complet au report
		 * @param params
		 *            Parametres à passer au modèle (HashMap)
		 * @param listener
		 *            Listener qui recevra les notifications de progression de
		 *            l'execution du report.
		 */
		public CktlODTReportingTaskThread(String label, String completeJasperFilePath, Map<String, Object> params, List<String> fieldsAsList, int printFormat,
				CktlDefaultReportingTaskListener listener) {
			super(label, null, null, completeJasperFilePath, params, listener);
			fields = fieldsAsList;
		}

		@Override
		public void run() {
			try {
				notifyAfterReportStarted();
				setDataResult(null);
				loadReport();
				Thread.yield();
				ByteArrayOutputStream s = exportToPDF(getParams());
				setDataResult(new NSData(s.toByteArray()));
				notifyAfterReportFinish();
			} catch (Throwable e) {
				CktlReportingException except = new CktlReportingException("Erreur lors de la création du rapport Jasper", e);
				except.setFileName(getFileName());
				except.setParams(getParams());
				setLastPrintError(except);
				notifyAfterException(except);
			}
		}

		protected void loadReport() throws Exception {
			try {
				InputStream in = new FileInputStream(getFileName());
				aReport = XDocReportRegistry.getRegistry().loadReport(in, TemplateEngineKind.Velocity);
				aContext = aReport.createContext();
				if (fields != null && fields.size() > 0) {
					FieldsMetadata metadata = new FieldsMetadata();
					for (String field : fields) {
						metadata.addFieldAsList(field);
					}
					aReport.setFieldsMetadata(metadata);
				}
			} catch (FileNotFoundException e) {
				throw new Exception("Modèle de document non trouvé !", e);
			} catch (IOException e) {
				e.printStackTrace();
				throw new Exception("Erreur lors du chargement du modèle !", e);
			} catch (XDocReportException e) {
				e.printStackTrace();
				throw new Exception("Erreur lors de la création du contexte !", e);
			}
		}
	}

	public CktlODTReportingTaskThread printWithThread(String label, final String completeFilePath, final Map<String, Object> params, final List<String> fieldsAsList,
			final int printFormat, CktlDefaultReportingTaskListener listener) throws Throwable {
		try {
			defaultReportListener = new DefaultODTReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			addListener(listener);

			setStarted(true);

			// Recupérer le chemin du repertoire du report
			final File f = new File(completeFilePath);
			String rep = f.getParent();
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}

			setCurrentReportingTaskThread(new CktlODTReportingTaskThread(label, completeFilePath, params, fieldsAsList, printFormat, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);
			// On lance la génération du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();
		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public NSData printNow(String label, final String completeFilePath, final Map<String, Object> params, final List<String> fieldsAsList, final int printFormat,
			DefaultODTReportListener listener) throws Throwable {
		printWithThread(label, completeFilePath, params, fieldsAsList, printFormat, listener);
		getCurrentReportingTaskThread().join();
		return printDifferedGetDataResult();
	}

	@Override
	public CktlODTReportingTaskThread getCurrentReportingTaskThread() {
		// TODO Auto-generated method stub
		return (CktlODTReportingTaskThread) super.getCurrentReportingTaskThread();
	}
}
