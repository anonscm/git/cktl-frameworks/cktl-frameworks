/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

// TODO: Auto-generated Javadoc
/**
 * The Enum OOoDocumentFormat.
 */
public enum OOoDocumentFormat {
	
	/** The PDF. */
	PDF("PDF", "writer_pdf_Export", "application/pdf", ".pdf"),
	
	/** The XL s2 pdf. */
	XLS2PDF("PDF", "calc_pdf_Export", "application/pdf", ".pdf"), 
	
	/** The MSWORD. */
	MSWORD("MS-Word", "MS Word 97", "application/msword", ".doc"), 
	
	/** The MSEXCEL. */
	MSEXCEL("MS-Excel", "MS Excel 97", "application/msexcel", ".xls"), 
	
	/** The CSV. */
	CSV("CSV", "Text - txt - csv (StarCalc)","text/comma-separated-values", ".csv"), ;
	
	/** The name. */
	private String name;
	
	/** The code. */
	private String code;
	
	/** The mime. */
	private String mime;
	
	/** The ext. */
	private String ext;
	
	/** The values. */
	private static OOoDocumentFormat[] values = { PDF, XLS2PDF, MSWORD, MSEXCEL, CSV };

	/**
	 * Instantiates a new o oo document format.
	 *
	 * @param name the name
	 * @param code the code
	 * @param mime the mime
	 * @param ext the ext
	 */
	private OOoDocumentFormat(String name, String code, String mime,
			String ext) {
		this.name = name;
		this.code = code;
		this.mime = mime;
		this.ext = ext;
	}

	/**
	 * Gets the name.
	 *
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * Gets the format code.
	 *
	 * @return the format code
	 */
	public String getFormatCode() {
		return code;
	}

	/**
	 * Gets the mime.
	 *
	 * @return the mime
	 */
	public String getMime() {
		return mime;
	}

	/**
	 * Gets the ext.
	 *
	 * @return the ext
	 */
	public String getExt() {
		return ext;
	}

	/**
	 * Gets the values.
	 *
	 * @return the values
	 */
	public static OOoDocumentFormat[] getValues() {
		return values;
	}
}
