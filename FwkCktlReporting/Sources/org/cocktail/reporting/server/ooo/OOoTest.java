/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;

import com.sun.star.beans.PropertyValue;
import com.sun.star.uno.Exception;

// TODO: Auto-generated Javadoc
/**
 * Classe OOoTest pour présentation du package org.cocktail.reporting.ooo
 */
public class OOoTest {

	public OOoTest() {
	}

	/**
	 * The main method.
	 * 
	 * @param args
	 *            the arguments
	 */
	public static void main(String[] args) {

		// on instancie un OOoTest
		OOoTest t = new OOoTest();

		try {
			KillOpenOffice.killOpenOffice();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		String oooExeFolder = "";
		Hashtable<String, String> userFields = new Hashtable<String, String>();

		CalcDocument calc = null;
		WriterDocument doc = null;

		ArrayList<PropertyValue> calcProps = new ArrayList<PropertyValue>();
		ArrayList<PropertyValue> writerProps = new ArrayList<PropertyValue>();
		PropertyValue p = null;

		// L'Entreprise
		userFields.put("ENTREPRISE", "DELL SA");
		userFields.put("ADR_ENTREPRISE", "Rue sous le pont");
		userFields.put("CP_ENTREPRISE", "69000");
		userFields.put("VILLE_ENTREPRISE", "Lyon");
		userFields.put("PAYS_ENTREPRISE", "France");

		// Stagiaire
		userFields.put("NOM_STAGIAIRE", "Nom");
		userFields.put("PRENOM_STAGIAIRE", "Prenom");
		userFields.put("MAIL_ETUDIANT", "prenom.nom@universite.fr");
		userFields.put("TEL_ETUDIANT", "69.69");

		// les propriétés du classeur excel
		p = new PropertyValue();
		p.Name = "DocumentTitle";
		p.Value = "Classeur_1";
		calcProps.add(p);
		
		// les propriétés du classeur excel
		p = new PropertyValue();
		p.Name = "DocumentTitle";
		p.Value = "Doc1";
		writerProps.add(p);

		// les entetes de la feuille excel
		String[] entetes = { "NOM_STAGIAIRE", "PRENOM_STAGIAIRE",
				"MAIL_ETUDIANT", "TEL_ETUDIANT", "Acces", "ENTREPRISE",
				"ADR_ENTREPRISE", "CP_ENTREPRISE", "VILLE_ENTREPRISE",
				"PAYS_ENTREPRISE", "Destinataire_Convention",
				"Service_destinataire", "Fonction_destinataire",
				"Fonction_prec_destinataire", "Telephone_destinataire",
				"Adresse_destinataire", "Sujet", "Domaines",
				"Date_debut_stage", "Date_fin_stage", "Debut_interrruption",
				"Fin_interrruption", "Debut_etranger", "Fin_etranger",
				"Duree_hebdo", "Cas_particuliers", "Adresse_stage",
				"Montant_gratification", "Mode_versement", "Avantages" };

		// le tableau des données à inserer dans la feuille
		String[][] data = new String[2][entetes.length];

		System.out
				.println("Création d'un classeur excel et export au format CSV");
		try {
			// on instancie un objet CalcDocument correspondant à un classeur
			// excel/calc
			calc = new CalcDocument(oooExeFolder);

			// Option pour l'export CSV
			// séparteur colonne : ;
			// séparateur texte : "
			// @see
			// http://wiki.services.openoffice.org/wiki/Documentation/DevGuide/Spreadsheets/Filter_Options
			calc.addDocumentProperty("FilterOptions", "59,34,0,0,");

			// on ouvre le classeur
			// @see CalcDocument#openDocument
			calc.openDocument();
			// on se positionne sur la premiere feuille
			// @see CalcDocument#getSpreadsheet(int);
			calc.getSpreadsheet(0);

			data[0] = entetes;

			for (int e = 0; e < entetes.length; e++) {
				data[1][e] = userFields.get(entetes[e]);
			}

			// insertion des données dans le classeur excel
			t.insertArrayIntoCalc(calc, data);

			// sauvegarde du classeur au format CSV dans ~/Desktop/calc.csv
			calc.saveDocAs("~/Desktop/calc.csv", OOoDocumentFormat.CSV, true);

		} catch (Exception e) {
			e.printStackTrace();
		} catch (java.lang.Exception e) {
			e.printStackTrace();
		}

		// on instancie un objet WriterDocument correspondant à un document
		// word/writer
		try {
			doc = new WriterDocument(oooExeFolder);
			
			doc.setDocumentProperties(writerProps);
			
			doc.openDocument();
			
			doc.addLine("Ceci est ma première ligne !\n");
			
			doc.addLine("Ceci est ma deuxième ligne !\n");
			
			doc.saveDocAs("~/Desktop/writer.odt", OOoDocumentFormat.MSWORD, true);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		} catch (java.lang.Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.exit(0);
	}

	/**
	 * Insert des données contenues dans un tableau dans une feuille excel.
	 * 
	 * @param calc
	 *            le classeur excel
	 * @param tab
	 *            le tableau contenant les données
	 * @see CalcDocument#insertIntoCell(int, int, String)
	 */
	private void insertArrayIntoCalc(CalcDocument calc, Object[][] tab) {
		for (int l = 0; l < tab.length; l++) {
			for (int c = 0; c < tab[l].length; c++) {
				calc.insertIntoCell(c, l, (String) tab[l][c]);
			}
		}
	}

}
