/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

// TODO: Auto-generated Javadoc
/**
 * The Class KillOpenOffice.
 */
public class KillOpenOffice {
	/**
	 * Kill OpenOffice.
	 * 
	 * Supports Windows XP, Solaris (SunOS) and Linux. Perhaps it supports more
	 * OS, but it has been tested only with this three.
	 * 
	 * @throws IOException
	 *             Killing OpenOffice didn't work
	 */
	public static void killOpenOffice() throws IOException {
		String osName = System.getProperty("os.name");
		if (osName.startsWith("Windows")) {
			windowsKillOpenOffice();
		} else if (osName.startsWith("SunOS") || osName.startsWith("Linux")) {
			unixKillOpenOffice();
		} else {
			throw new IOException("Unknown OS, killing impossible");
		}
	}

	/**
	 * Kill OpenOffice on Windows XP.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static void windowsKillOpenOffice() throws IOException {
		Runtime.getRuntime().exec("tskill soffice");
	}

	/**
	 * Kill OpenOffice on Unix.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static void unixKillOpenOffice() throws IOException {
		Runtime runtime = Runtime.getRuntime();

		String pid = getOpenOfficeProcessID();
		if (pid != null) {
			while (pid != null) {
				String[] killCmd = { "/bin/bash", "-c", "kill -9 " + pid };
				runtime.exec(killCmd);

				// Is another OpenOffice prozess running?
				pid = getOpenOfficeProcessID();
			}
		}
	}

	/**
	 * Get OpenOffice prozess id.
	 *
	 * @return the open office process id
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	private static String getOpenOfficeProcessID() throws IOException {
		Runtime runtime = Runtime.getRuntime();

		// Get prozess id
		String[] getPidCmd = { "/bin/bash", "-c",
				"ps -e|grep soffice|awk '{print $1}'" };
		Process getPidProcess = runtime.exec(getPidCmd);

		// Read prozess id
		InputStreamReader isr = new InputStreamReader(getPidProcess
				.getInputStream());
		BufferedReader br = new BufferedReader(isr);

		return br.readLine();
	}
}