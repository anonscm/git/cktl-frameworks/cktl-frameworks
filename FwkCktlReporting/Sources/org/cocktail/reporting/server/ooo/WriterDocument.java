/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

import java.util.Enumeration;
import java.util.Hashtable;

import com.sun.star.beans.XPropertySet;
import com.sun.star.container.XEnumerationAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.lang.WrappedTargetException;
import com.sun.star.text.XTextDocument;
import com.sun.star.text.XTextFieldsSupplier;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.util.XRefreshable;

// TODO: Auto-generated Javadoc
/**
 * The Class WriterDocument.
 */
public class WriterDocument extends OOoDocument {

	XTextDocument writerDocument;

	/**
	 * Instantiates a new writer document.
	 * 
	 * @param oooExeFolder
	 *            the ooo exe folder
	 * @throws Exception
	 *             the exception
	 */
	public WriterDocument(String oooExeFolder) throws Exception {
		super(oooExeFolder);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new writer document.
	 * 
	 * @throws Exception
	 *             the exception
	 */
	public WriterDocument() throws Exception {
		// TODO Auto-generated constructor stub
		super();
	}

	/**
	 * Ouvre un document vide.
	 *
	 * @throws Exception the exception
	 */
	public void openDocument() throws Exception {
		super.openDocument(WRITER_FACTORY);
		setWriterDocument();
	}

	/* (non-Javadoc)
	 * @see org.cocktail.reporting.ooo.OOoDocument#openDocument(java.lang.String)
	 */
	public void openDocument(String docUrl) throws Exception {
		super.openDocument(docUrl);
		setWriterDocument();
	}

	/**
	 * Sets the writer document.
	 * @see com.sun.star.text.XTextDocument
	 */
	private void setWriterDocument() {
		this.writerDocument = (XTextDocument) UnoRuntime.queryInterface(
				com.sun.star.text.XTextDocument.class, getOooDocument());
	}

	/**
	 * Gets the writer document.
	 *
	 * @return the writer document
	 * @see com.sun.star.text.XTextDocument
	 */
	private XTextDocument getWriterDocument() {
		return this.writerDocument;
	}

	/**
	 * Feed doc template.
	 * 
	 * @param fields
	 *            the fields
	 * @throws Exception
	 *             the exception
	 * @throws WrappedTargetException
	 *             the wrapped target exception
	 */
	public void feedDocTemplate(Hashtable<String, String> fields)
			throws Exception, WrappedTargetException {
		// get XTextFieldsSupplier, XBookmarksSupplier interfaces
		XTextFieldsSupplier xTextFieldsSupplier = (XTextFieldsSupplier) UnoRuntime
				.queryInterface(XTextFieldsSupplier.class, getOooDocument());

		// access the TextFields and the TextFieldMasters collections
		XNameAccess xNamedFieldMasters = xTextFieldsSupplier
				.getTextFieldMasters();
		XEnumerationAccess xEnumeratedFields = xTextFieldsSupplier
				.getTextFields();

		// iterate over hashtable and insert values into field masters
		Enumeration<String> keys = fields.keys();
		Object fieldMaster = null;
		while (keys.hasMoreElements()) {
			// get column name
			String key = (String) keys.nextElement();

			// tests if the user key is present in the template
			if (xNamedFieldMasters
					.hasByName("com.sun.star.text.FieldMaster.User." + key)) {
				// access corresponding field master
				fieldMaster = xNamedFieldMasters
						.getByName("com.sun.star.text.FieldMaster.User." + key);

				// query the XPropertySet interface, we need to set the Content
				// property
				XPropertySet xPropertySet = (XPropertySet) UnoRuntime
						.queryInterface(XPropertySet.class, fieldMaster);

				// insert the column value into field master
				xPropertySet.setPropertyValue("Content", fields.get(key));
			}
		}
		// afterwards we must refresh the textfields collection
		XRefreshable xRefreshable = (XRefreshable) UnoRuntime.queryInterface(
				XRefreshable.class, xEnumeratedFields);
		xRefreshable.refresh();
	}

	/**
	 * Adds the line.
	 *
	 * @param line the line
	 * 
	 * a revoir
	 */
	public void addLine(String line) {
		getWriterDocument().getText().insertString(
				getWriterDocument().getText().getEnd(), line, false);
	}

}
