/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

import java.io.File;
import java.util.ArrayList;

import ooo.connector.BootstrapSocketConnector;
import ooo.connector.server.OOoServer;

import com.sun.star.beans.PropertyValue;
import com.sun.star.comp.helper.BootstrapException;
import com.sun.star.frame.XComponentLoader;
import com.sun.star.frame.XStorable;
import com.sun.star.io.IOException;
import com.sun.star.lang.XComponent;
import com.sun.star.lang.XMultiComponentFactory;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;
import com.sun.star.uno.XComponentContext;

// TODO: Auto-generated Javadoc
/**
 * The Class OOoDocument.
 */
public class OOoDocument {

	/** The ooo exe folder. */
	private String oooExeFolder;
	
	/** The ooo server. */
	private OOoServer oooServer = null;
	
	/** The ooo document. */
	private XComponent oooDocument;
	
	/** The context. */
	private XComponentContext context = null;
	
	/** The service manager. */
	private XMultiComponentFactory serviceManager = null;
	
	/** The properties. */
	private PropertyValue[] properties = null;
	
	private ArrayList<PropertyValue> propertiesArray = new ArrayList<PropertyValue>();

	/** The Constant CALC_FACTORY. */
	protected final static String CALC_FACTORY = "private:factory/scalc";
	
	/** The Constant WRITER_FACTORY. */
	protected final static String WRITER_FACTORY = "private:factory/swriter";

	/**
	 * Instantiates a new o oo document.
	 *
	 * @param oooExeFolder the ooo exe folder
	 * @throws Exception the exception
	 */
	public OOoDocument(String oooExeFolder) throws Exception {
		setOooExeFolder(oooExeFolder);
		setContext();
		setServiceManager();
	}

	/**
	 * Instantiates a new o oo document.
	 *
	 * @throws Exception the exception
	 */
	public OOoDocument() throws Exception {
		setOooExeFolder(null);
		setContext();
		setServiceManager();
	}

	/**
	 * Initialisation du répertoire d'installation d'OpenOffice.
	 *
	 * @param oooExeFolder le path vers le repertoire d'installation
	 */
	private void setOooExeFolder(String oooExeFolder) {
		if ((oooExeFolder == null) || (oooExeFolder.length() < 1))
			this.oooExeFolder = "/usr/lib/openoffice.org/program/";
		else {

			this.oooExeFolder = oooExeFolder;
		}
	}

	/**
	 * Gets the ooo exe folder.
	 *
	 * @return the ooo exe folder
	 */
	public String getOooExeFolder() {
		return this.oooExeFolder;
	}

	/**
	 * Retourne l'handler de l'instance d'OpenOffice. Si c'est le premier appel,
	 * une instance d'OpenOffice est initialisée
	 * 
	 * @return oooServer l'handler vesr le l'instance d'OpenOffice
	 */
	private OOoServer getOooServer() {
		if (oooServer == null) {
			ArrayList<String> options = new ArrayList<String>();

			options.add("-headless");
			options.add("-nologo");
			options.add("-nodefault");
			options.add("-norestore");
			options.add("-nocrashreport");
			options.add("-nolockcheck");
			options.add("-nolockcheck");

			oooServer = new OOoServer(oooExeFolder, options);
		}
		return oooServer;
	}

	/**
	 * Initialise le context.
	 *
	 * @throws Exception the exception
	 */
	private void setContext() throws Exception {
		if (context == null) {
			try {
				BootstrapSocketConnector bootstrapSocketConnector = new BootstrapSocketConnector(
						getOooServer());
				context = bootstrapSocketConnector.connect();
			} catch (BootstrapException e) {
				throw new Exception(e.getMessage());
			}
		}
	}

	/**
	 * Set remote service manager. We only need one instance regardless of the
	 * number of documents we create.
	 *
	 * @throws Exception the exception
	 */
	private void setServiceManager() throws Exception {
		System.out
				.println("OOoDoc: Start up or connect to the remote service manager.");
		if (context == null)
			setContext();
		serviceManager = context.getServiceManager();
	}

	/**
	 * Get remote service manager. We only need one instance regardless of the
	 * number of documents we create.
	 *
	 * @return serviceManager
	 * @throws Exception the exception
	 */
	public XMultiComponentFactory getServiceManager() throws Exception {
		if (context == null && serviceManager == null) {
			setServiceManager();
		}
		return this.serviceManager;
	}

	/**
	 * Sets the document properties.
	 *
	 * @param props the new document properties
	 */
	public void setDocumentProperties(ArrayList<PropertyValue> props) {
		if (this.propertiesArray.size() == 0)
			this.propertiesArray = props;
		else {
			for (int j = 0 ; j < props.size(); j++) {
				this.propertiesArray.add(props.get(j));
			}
		}
	}
	
	/**
	 * Add a document property.
	 *
	 * @param unNom the un nom
	 * @param uneValeur the une valeur
	 */
	public void addDocumentProperty(String unNom, Object uneValeur) {
		PropertyValue propValue = new PropertyValue();
        propValue.Name = unNom;
        propValue.Value = uneValeur;
        this.propertiesArray.add(propValue);
	}

	/**
	 * Gets the document properties.
	 *
	 * @return the document properties
	 */
	protected PropertyValue[] getDocumentProperties() {
		if (this.propertiesArray.size() == 0)
			properties = new PropertyValue[0];
		else {
			this.properties = new PropertyValue[this.propertiesArray.size()];
			this.propertiesArray.toArray(this.properties);
		}
			
		return this.properties;
	}

	/**
	 * Open document.
	 *
	 * @param docUrl l'url vers le fichier à charger, peut aussi prendre les valeur
	 * suivante : private:factory/scalc : pour un fichier calc vide
	 * private:factory/writer : pour un fichier writer vide une URL
	 * http:// ou file://(/)
	 * @throws Exception the exception
	 */
	public void openDocument(String docUrl) throws Exception {

		// get the service manager
		// Retrieve the Desktop object and get its XComponentLoader.
		Object desktop = getServiceManager().createInstanceWithContext(
				"com.sun.star.frame.Desktop", context);
		XComponentLoader loader = (XComponentLoader) UnoRuntime.queryInterface(
				XComponentLoader.class, desktop);

		setOooDocument(loader.loadComponentFromURL(docUrl, "_blank", 0,
				getDocumentProperties()));
	}

	/**
	 * Sets the ooo document.
	 *
	 * @param component the new ooo document
	 */
	private void setOooDocument(XComponent component) {
		this.oooDocument = component;
	}

	/**
	 * Gets the ooo document.
	 *
	 * @return the ooo document
	 */
	protected XComponent getOooDocument() {
		return this.oooDocument;
	}

	/**
	 * fonction retournant une URL valide pour l'ouverture d'un fichier.
	 *
	 * @param filePath le chemin vers le fichier à ouvrir (URL ou URI)
	 * @return Une url valide pour ouvrir un document OOo
	 */
	public String filePathToUrl(String filePath) {
		if (filePath.startsWith("http://")) {
			return filePath;
		} else {
			String path = new File(filePath).getAbsolutePath();
			StringBuffer sb = new StringBuffer();
			if (path.startsWith("/")) {
				sb.append("file://");
			} else {
				sb.append("file:///");
			}
			sb.append(path);
			for (int i = 0; i < sb.length(); i++) {
				char c = sb.charAt(i);
				switch (c) {
				case ' ':
					sb.replace(i, i + 1, "%");
					sb.insert(i + 1, "20");
					i += 2;
					break;
				case '\\':
					sb.replace(i, i + 1, "/");
					break;
				}
			}
			return sb.toString();
		}
	}

	/**
	 * Ouvre un document en tant que template (copie d'un autre document).
	 *
	 * @param pathTemplate le chemin vers le template à utliser
	 * @throws Exception the exception
	 */
	public void openDocumentFromTemplate(String pathTemplate) throws Exception {

		String docUrl = filePathToUrl(pathTemplate);

		// Define general document properties (see
		// com.sun.star.document.MediaDescriptor for the possibilities).
		ArrayList<PropertyValue> props = new ArrayList<PropertyValue>();
		PropertyValue p = null;
		if (docUrl != null) {
			// Enable the use of a template document.
			p = new PropertyValue();
			p.Name = "AsTemplate";
			p.Value = new Boolean(true);
			props.add(p);
		} else {
			throw new Exception("Chemin vers le template non valide!");
		}

		setDocumentProperties(props);

		openDocument(docUrl);
	}

	/**
	 * Sauve le document dans un fichier.
	 *
	 * @param saveFile Le chemin complet du fichier dans lequel sauver le docuement.
	 * Si null, la procédure ne fait rien.
	 * @param format Le format de sortie du document.
	 * @param overwrite Si vrai, et que le fichier existe déjà, il sera écrasé.
	 * Autrement, si le fichier existe déjà une exception sera levée.
	 * @throws Exception the exception
	 * @see OOoDocumentFormat
	 */
	public void saveDocAs(String saveFile, OOoDocumentFormat format,
			boolean overwrite) throws java.lang.Exception {
		if ((saveFile == null) || (saveFile.trim().length() == 0)) {
			return;
		}

		if (!overwrite) {
			File f = new File(saveFile);
			if (f.exists()) {
				throw new java.lang.Exception("File " + saveFile
						+ " already exists; overwriting disabled.");
			}
		}

		String saveFileURL = filePathToUrl(saveFile);

		System.out.println("Saving file as : " + saveFileURL + " - "
				+ format.getFormatCode());

		XStorable storable = (XStorable) UnoRuntime.queryInterface(
				XStorable.class, getOooDocument());

		addDocumentProperty("FilterName", format.getFormatCode());

		storable.storeToURL(saveFileURL, getDocumentProperties());
	}
	
	/**
	 * Retourne le document dans un flux.
	 *
	 * @param format Le format de fichier en sortie de flux
	 * @return the o oo output stream
	 * @throws IOException Signals that an I/O exception has occurred.
	 * Un flux
	 * @see OOoDocumentFormat
	 * @see OOoOutputStream
	 */
	public OOoOutputStream saveDocAsStream(OOoDocumentFormat format) throws IOException {

		OOoOutputStream output = new OOoOutputStream();
		
		addDocumentProperty("OutputStream", output);
		addDocumentProperty("FilterName", format.getFormatCode());
		
		XStorable xstorable = (XStorable) UnoRuntime.queryInterface(XStorable.class,getOooDocument());
		xstorable.storeToURL("private:stream", getDocumentProperties());
		
        return output;
	}

}
