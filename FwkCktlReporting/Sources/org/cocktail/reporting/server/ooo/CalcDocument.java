/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.ooo;

import com.sun.star.container.XIndexAccess;
import com.sun.star.container.XNameAccess;
import com.sun.star.sheet.XSpreadsheet;
import com.sun.star.sheet.XSpreadsheetDocument;
import com.sun.star.sheet.XSpreadsheets;
import com.sun.star.table.XCell;
import com.sun.star.uno.Exception;
import com.sun.star.uno.UnoRuntime;

// TODO: Auto-generated Javadoc
/**
 * The Class CalcDocument.
 *
 * @author bdelarbr
 */
public class CalcDocument extends OOoDocument {

	/** The current spread sheet. */
	private XSpreadsheet currentSpreadSheet = null;

	/**
	 * Instantiates a new calc document.
	 *
	 * @param oooExeFolder the ooo exe folder
	 * @throws Exception the exception
	 */
	public CalcDocument(String oooExeFolder) throws Exception {
		super(oooExeFolder);
		// TODO Auto-generated constructor stub
	}

	/**
	 * Instantiates a new calc document.
	 *
	 * @throws Exception the exception
	 */
	public CalcDocument() throws Exception {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * Ouvre un nouveau document calc vide.
	 *
	 * @throws Exception the exception
	 */
	public void openDocument() throws Exception {
		super.openDocument(CALC_FACTORY);
	}

	/**
	 * Ouvre un fichier calc à partir du path fournit.
	 *
	 * @param path the path
	 * @throws Exception the exception
	 * @path Le chemin vers le fichier cal à ouvrir
	 * @see org.cocktail.griotte.serveur.office.OOoDocument#openDocument(java.lang.String)
	 */
	public void openDocument(String path) throws Exception {
		String docUrl = filePathToUrl(path);
		super.openDocument(docUrl);
	}

	/**
	 * Retourne le classeur.
	 *
	 * @return le classeur du document ouvert
	 */
	private XSpreadsheetDocument getSpreadsheetDocument() {
		return (XSpreadsheetDocument) UnoRuntime.queryInterface(XSpreadsheetDocument.class, getOooDocument());
	}

	/**
	 * Retourne la feuille à l'index passé en paramètre.
	 *
	 * @param unIndex the un index
	 * @return the spreadsheet
	 */
	public XSpreadsheet getSpreadsheet(int unIndex) {

		// Collection of sheets
		XSpreadsheets xSheets = getSpreadsheetDocument().getSheets();
		XSpreadsheet xSheet = null;

		try {
			XIndexAccess xSheetsIA = (XIndexAccess) UnoRuntime.queryInterface(
					XIndexAccess.class, xSheets);
			xSheet = (XSpreadsheet) UnoRuntime.queryInterface(XSpreadsheet.class, xSheetsIA.getByIndex(unIndex));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setCurrentSpreadSheet(xSheet);

		return xSheet;
	}

	/**
	 * Retourne la feuille avec le nom spécifié.
	 * 
	 * @param unNom
	 *            Le nom de la feuille.
	 * @return L'interface XSpreadsheet de la feuille.
	 * */
	public XSpreadsheet getSpreadsheet(String unNom) {

		// Collection of sheets
		XSpreadsheets xSheets = getSpreadsheetDocument().getSheets();
		XSpreadsheet xSheet = null;

		try {
			XNameAccess xSheetsNA = (XNameAccess) UnoRuntime.queryInterface(
					XNameAccess.class, xSheets);
			xSheet = (XSpreadsheet) UnoRuntime.queryInterface(XSpreadsheet.class, xSheetsNA.getByName(unNom));
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		setCurrentSpreadSheet(xSheet);

		return xSheet;
	}

	/**
	 * Insert une nouvelle feuille vide avec le nom spécifié, à l'index
	 * spécifié.
	 * 
	 * @param unNom
	 *            Le nom de la nouvelle feuille.
	 * @param unIndex
	 *            L'index aux l'insérer.
	 * @return L'interface XSpreadsheet de la nouvelle feuille. La feuille
	 *         ajoutée est positionnée comme feuille courante
	 */
	public XSpreadsheet insertSpreadsheet(String unNom, int unIndex) {
		// Collection of sheets
		XSpreadsheets xSheets = getSpreadsheetDocument().getSheets();
		XSpreadsheet xSheet = null;

		xSheets.insertNewByName(unNom, new Integer(unIndex).shortValue());
		
		xSheet = getSpreadsheet(unNom);
		setCurrentSpreadSheet(xSheet);

		return xSheet;
	}

	/**
	 * Initialise la feuille courante par uneFeuille.
	 *
	 * @param uneFeuille la feuille a définir comme courante, si null c'est la première
	 * feuille du classeur qui est définit
	 */
	private void setCurrentSpreadSheet(XSpreadsheet uneFeuille) {
		if (uneFeuille == null) {
			this.currentSpreadSheet = getSpreadsheet(0);
		} else {
			this.currentSpreadSheet = uneFeuille;
		}
	}

	/**
	 * Retourne la feuille courante (dernière feuille sur laquelle nous avons
	 * travaillé).
	 *
	 * @return la feuille courante, si non initialisée, c'est la première
	 * feuille du classeur
	 */
	public XSpreadsheet getCurrentSpreadSheet() {
		if (this.currentSpreadSheet == null)
			setCurrentSpreadSheet(null);
		return this.currentSpreadSheet;
	}

	/**
	 * Insertion dans une cellule donnée d'une feuille donnée.
	 *
	 * @param CellX l'index de la colonne
	 * @param CellY l'index de la ligne
	 * @param theValue la valeur à jouter
	 * @param TT1 la feuille sur laquelle on travail
	 * @param flag un flag pour savoir sir c'est du texte (formule comprise) ou
	 * un nombre, flag = "V" pour un nombre, toute autre valeur
	 * conduire à un ajout en tant que texte
	 */
	public void insertIntoCell(int CellX, int CellY, String theValue,
			XSpreadsheet TT1, String flag) {

		XCell xCell = getCell(CellX, CellY, TT1);

		if (flag.equals("V")) {
			xCell.setValue((new Float(theValue)).floatValue());
		} else {
			xCell.setFormula(theValue);
		}

	}

	/**
	 * insertion de texte dans une cellule donnée de la feuille courante.
	 *
	 * @param CellX l'index de la colonne
	 * @param CellY l'index de la ligne
	 * @param theValue le texte a jouter (texte ou formule)
	 * @see CalcDocument#insertIntoCell(int, int, String, XSpreadsheet, String)
	 */
	public void insertIntoCell(int CellX, int CellY, String theValue) {
		insertIntoCell(CellX, CellY, theValue, getCurrentSpreadSheet(), "");
	}

	/**
	 * insertion d'un numérique dans une cellule donnée de la feuille courante.
	 *
	 * @param CellX l'index de la colonne
	 * @param CellY l'index de la ligne
	 * @param theValue la valeur à ajouter (cast en float)
	 * @see CalcDocument#insertIntoCell(int, int, String, XSpreadsheet, String)
	 */
	public void insertValueIntoCell(int CellX, int CellY, String theValue) {
		insertIntoCell(CellX, CellY, theValue, getCurrentSpreadSheet(), "V");
	}

	/**
	 * Retourne la cellule en CellX,CellY de la feuille TT1.
	 *
	 * @param CellX l'index de la colonne
	 * @param CellY l'index de la ligne
	 * @param TT1 Une interface d'une feuille
	 * @return L'interface XCell de la cellule
	 */
	private XCell getCell(int CellX, int CellY, XSpreadsheet TT1) {
		XCell xCell = null;
		try {
			xCell = TT1.getCellByPosition(CellX, CellY);
		} catch (com.sun.star.lang.IndexOutOfBoundsException ex) {
			ex.printStackTrace(System.err);
		}
		return xCell;
	}

	/**
	 * Retourne la cellule en CellX,CellY de la feuille courante.
	 *
	 * @param CellX l'index de la colonne
	 * @param CellY l'index de la ligne
	 * @return L'interface XCell de la cellule
	 * @see CalcDocument#getCell(int, int, XSpreadsheet)
	 */
	@SuppressWarnings("unused")
	private XCell getCell(int CellX, int CellY) {
		return getCell(CellX, CellY, getCurrentSpreadSheet());
	}
	
	/**
	 * Adds the header.
	 *
	 * @param entetes the entetes
	 */
	public void addHeader(String[] entetes) {
		for (int s = 0; s < entetes.length; s++){
			insertIntoCell(s, 0, entetes[s]);
		}
	}
}
