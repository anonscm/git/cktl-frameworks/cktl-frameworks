/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server;

import java.sql.Connection;
import java.util.Map;

import com.webobjects.foundation.NSData;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public abstract class CktlAbstractReportingTaskThread extends Thread {

	private Map<String, Object> params;
	private String sqlQuery;
	private String fileName;
	private NSData dataResult;
	private Connection connection;
	private String label;

	public CktlAbstractReportingTaskThread(String label, Connection connection, String sqlQuery, final String fileName, final Map<String, Object> parameters, ICktlReportingTaskListener listener) {
		super();
		this.label = label;
		this.connection = connection;
		this.params = parameters;
		this.sqlQuery = sqlQuery;
		setFileName(fileName);
	}

	public CktlAbstractReportingTaskThread(Connection connection, String sqlQuery, final String fileName, final Map<String, Object> parameters, ICktlReportingTaskListener listener) {
		this(null, connection, sqlQuery, fileName, parameters, listener);
	}

	public NSData getDataResult() {
		return dataResult;
	}

	public void setDataResult(NSData dataResult) {
		this.dataResult = dataResult;
	}

	public Map<String, Object> getParams() {
		return params;
	}

	public void setParams(Map<String, Object> params) {
		this.params = params;
	}

	public String getSqlQuery() {
		return sqlQuery;
	}

	public void setSqlQuery(String sqlQuery) {
		this.sqlQuery = sqlQuery;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Connection getConnection() {
		return connection;
	}

	public void setConnection(Connection connection) {
		this.connection = connection;
	}

	public String getLabel() {
		return (label == null ? getFileName() : label);
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
