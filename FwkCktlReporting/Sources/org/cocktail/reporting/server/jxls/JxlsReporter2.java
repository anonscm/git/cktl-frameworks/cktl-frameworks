/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jxls;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.report.ResultSetCollection;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.reporting.server.exception.CktlReportingExceptionRapportVide;

/**
 * Génération d'un fichier excel à partir d'une {@link List}<br>
 * Inspiré de {@link JxlsReporter} mais :
 * <ul>
 * <li>simplifié au minimum</li>
 * <li>sans référence à WO</li>
 * </ul>
 * @author bourges
 */
public class JxlsReporter2 {
	public final static Logger logger = Logger.getLogger(JxlsReporter2.class);

	public static int MAX_ROWS = 65535;
	public static String EXCEED_MAX_ROWS = "Le nombre de lignes dépasse " + MAX_ROWS + ". Impossible de générer un fichier Excel. Modifiez vos critères pour que la requête renvoie moins de lignes.";
	public static String NOPAGES_MSG = "Aucune donnée trouvée. Vérifiez éventuellement les critères.";

	public JxlsReporter2() {

	}

	public static byte[] printNow(Connection connection, String sqlQuery, final String templateFileName) throws Throwable {
        ResultSet rs = null;
        int pageCount = -1;
        if (logger.isDebugEnabled()) {
            logger.debug("sql = ");
            logger.debug(sqlQuery);
        }
        Validate.notEmpty(sqlQuery,"La requête sql est vide");
        if (sqlQuery.contains("{")) {
            throw new Exception("La requete contient des paramètres non renseignés.");
        }
        //Compter le nombre de rows
        String sqlcount = "select count(*) from (" + sqlQuery + ")";
        Statement stmt = connection.createStatement();
        rs = stmt.executeQuery(sqlcount);
        if (rs.next()) {
            pageCount = rs.getInt(1);
        }

        if (pageCount == 0) {
            throw new CktlReportingExceptionRapportVide(NOPAGES_MSG);
        }
        if (pageCount >= MAX_ROWS) {
            throw new CktlReportingExceptionRapportVide(EXCEED_MAX_ROWS);
        }
        if (logger.isDebugEnabled()) {
            logger.debug("nbRows = " + pageCount);
        }
        rs = stmt.executeQuery(sqlQuery);
        ResultSetCollection rsc = new ResultSetCollection(rs, pageCount, true);
        @SuppressWarnings("rawtypes")
        Map<String, ResultSetCollection> beans = new HashMap();
        beans.put("item", rsc);
        XLSTransformer transformer = new XLSTransformer();
        HSSFWorkbook resultWorkbook = (HSSFWorkbook) transformer.transformXLS(new FileInputStream(templateFileName), beans);
        ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
        resultWorkbook.write(tmpStream);
        return tmpStream.toByteArray();
	}
}
