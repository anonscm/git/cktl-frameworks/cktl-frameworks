/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jxls;

import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.jxls.transformer.XLSTransformer;

import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Génération d'un fichier excel à partir d'une {@link List}<br>
 * Inspiré de {@link JxlsReporter} mais :
 * <ul>
 * <li>simplifié au minimum</li>
 * <li>utilise un array en entrée et pas select</li>
 * </ul>
 * @author bourges
 */
public class JxlsReporterFromArray {
	public final static Logger logger = Logger.getLogger(JxlsReporterFromArray.class);

	private static int MAX_ROWS = 65535;
	private static String EXCEED_MAX_ROWS = "Le nombre de lignes dépasse " + MAX_ROWS + ". Impossible de générer un fichier Excel. Modifiez vos critères pour que la requête renvoie moins de lignes.";


	/**
	 * @param data - une liste de données à mettre dans un tableau excel
	 * @param templateFileName - le nom du fichier excel à utiliser comme template
	 * @return retourne fichier Excel généré
	 * @throws Throwable
	 */
	public static byte[] printNow(List<?> data, final String templateFileName) throws Throwable {
	    Validate.isTrue(data.size() < MAX_ROWS, EXCEED_MAX_ROWS);
        Map<String, List<?>> beans = new HashMap<String, List<?>>();
        beans.put("items", data);
        XLSTransformer transformer = new XLSTransformer();
        HSSFWorkbook resultWorkbook = (HSSFWorkbook) transformer.transformXLS(new FileInputStream(templateFileName), beans);
        ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
        resultWorkbook.write(tmpStream);
        return tmpStream.toByteArray();
	}

}
