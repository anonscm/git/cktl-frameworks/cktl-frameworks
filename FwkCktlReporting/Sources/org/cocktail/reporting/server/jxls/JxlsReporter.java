/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.reporting.server.jxls;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;

import net.sf.jxls.report.ResultSetCollection;
import net.sf.jxls.transformer.XLSTransformer;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.CktlAbstractReportingTaskThread;
import org.cocktail.reporting.server.CktlDefaultReportingTaskListener;
import org.cocktail.reporting.server.ICktlReportingTaskListener;
import org.cocktail.reporting.server.exception.CktlReportingException;
import org.cocktail.reporting.server.exception.CktlReportingExceptionRapportVide;

import com.webobjects.foundation.NSData;

/**
 * Gestion des impressions.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class JxlsReporter extends CktlAbstractReporter {
	public final static Logger logger = Logger.getLogger(JxlsReporter.class);

	public static int MAX_ROWS = 65535;
	public static String EXCEED_MAX_ROWS = "Le nombre de lignes dépasse " + MAX_ROWS + ". Impossible de générer un fichier Excel. Modifiez vos critères pour que la requête renvoie moins de lignes.";

	private DefaultJxlsReportListener defaultReportListener;
	private int pageCount = -1;
	private File srcFile;
	private HSSFWorkbook resultWorkbook;

	public JxlsReporter() {

	}

	public final class CktlJxlsReportingTaskThread extends CktlAbstractReportingTaskThread {

		private final int _printFormat;

		//private final byte _modeBlank;

		public CktlJxlsReportingTaskThread(String label, Connection connection, String sqlQuery, final String fileName, final Map<String, Object> parameters, final int printFormat, final Boolean printIfEmpty, ICktlReportingTaskListener listener) {
			super(label, connection, sqlQuery, fileName, parameters, listener);
			_printFormat = printFormat;
			//defaultReportListener = new DefaultJxlsReportListener();
		}

		public InputStream getSrcStream() throws FileNotFoundException {
			return new FileInputStream(getSrcFile());
		}

		public void run() {
			try {
				setDataResult(null);
				setResultWorkbook(null);
				ResultSet rs = null;
				try {
					if (logger.isDebugEnabled()) {
						logger.debug("sql = ");
						logger.debug(getSqlQuery());
					}
					if (StringCtrl.isEmpty(getSqlQuery())) {
						throw new Exception("La requête sql est vide");
					}

					if (getSqlQuery().contains("{")) {
						throw new Exception("La requete contient des paramètres non renseignés.");
					}

					//					CktlReportingSqlCtrl critSqlCtrl = new CktlReportingSqlCtrl(getSqlQuery());
					//					setFinalQuery(critSqlCtrl.prepare(getParams()));
					//res = critSqlCtrl.rawRowsForSQL(getOwnerComponent().edc(), EOCritere.POPUP_KEYS, getAppUser().getCktlReportRapportCriteresGlobal());

					//Compter le nombre de rows
					String sqlcount = "select count(*) from (" + getSqlQuery() + ")";
					Statement stmt = getConnection().createStatement();
					rs = stmt.executeQuery(sqlcount);
					setPageCount(0);
					if (rs.next()) {
						setPageCount(rs.getInt(1));
					}

					if (getPageCount() == 0) {
						throw new CktlReportingExceptionRapportVide(NOPAGES_MSG);
					}
					if (getPageCount() >= MAX_ROWS) {
						throw new CktlReportingExceptionRapportVide(EXCEED_MAX_ROWS);
					}
					if (logger.isDebugEnabled()) {
						logger.debug("nbRows = " + getPageCount());
					}
					rs = stmt.executeQuery(getSqlQuery());
					notifyAfterDataSourceCreated();
					ResultSetCollection rsc = new ResultSetCollection(rs, getPageCount(), true);
					@SuppressWarnings("rawtypes")
					Map beans = new HashMap();
					beans.put("item", rsc);
					XLSTransformer transformer = new XLSTransformer();
					Thread.yield();
					//transformer.transformXLS(getFileName(), beans, templateUrlResultat);

					HSSFWorkbook resultWorkbook = (HSSFWorkbook) transformer.transformXLS(getSrcStream(), beans);
					setResultWorkbook(resultWorkbook);
					notifyAfterReportBuild(getPageCount());
					Thread.yield();

					notifyTrace("report rempli - " + getPageCount() + " lignes");

				} catch (Throwable e) {
					if (rs != null) {
						rs.close();
					}
					//getConnection().close();
					throw e;
				}
				//setLastReport(getCurrentReport());
				Thread.yield();
				ByteArrayOutputStream s;
				switch (_printFormat) {
				case EXPORT_FORMAT_PDF:
					s = exportToPdf();
					break;

				case EXPORT_FORMAT_XLS:
					s = exportToXls();
					break;
				case EXPORT_FORMAT_HTML:
					s = exportToHtml();
					break;
				default:
					s = exportToXls();
					break;
				}
				setDataResult(new NSData(s.toByteArray()));

				notifyAfterReportFinish();

			} catch (CktlReportingException e) {
				e.setFileName(getFileName());
				e.setQuery(getSqlQuery());
				e.setParams(getParams());
				setLastPrintError(e);
				notifyAfterException(e);
			} catch (Throwable e) {
				CktlReportingException except = new CktlReportingException("Erreur lors de la création du rapport Jxls", e);
				except.setFileName(getFileName());
				except.setQuery(getSqlQuery());
				except.setParams(getParams());
				setLastPrintError(except);
				notifyAfterException(except);
				//e.printStackTrace();
			}
		}
	}

	public class DefaultJxlsReportListener extends CktlDefaultReportingTaskListener implements IJxlsReportListener {

	}

	public Boolean isResultReady() {
		return Boolean.valueOf(getCurrentReportingTaskThread() != null && getCurrentReportingTaskThread().getDataResult() != null);
	}

	public NSData printNow(String label, Connection connection, String sqlQuery, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, IJxlsReportListener listener) throws Throwable {
		try {
			printWithThread(label, connection, sqlQuery, completeFilePath, params, printFormat, printIfEmpty, listener);
			// on attend que le thread soit terminé
			getCurrentReportingTaskThread().join();
			return printDifferedGetDataResult();

		} catch (Throwable e) {
			e.printStackTrace();
			System.err.println("impression vide avec les parametres = " + params);
			throw e;
		}
	}

	public CktlJxlsReportingTaskThread printWithThread(String label, Connection connection, String sqlQuery, final String completeFilePath, final Map<String, Object> params, final int printFormat, final Boolean printIfEmpty, ICktlReportingTaskListener listener) throws Throwable {
		try {
			defaultReportListener = new DefaultJxlsReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			addListener(listener);
			setStarted(true);
			initSrcFile(completeFilePath);
			setCurrentReportingTaskThread(new CktlJxlsReportingTaskThread(label, connection, sqlQuery, completeFilePath, params, printFormat, printIfEmpty, listener));
			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			//On lance la génération du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	public void setWhenNoDataType(final byte whenNoData) {

	}

	public void prepareDataSource() throws Exception {

	}

	public ByteArrayOutputStream exportToPdf() {
		return null;
	}

	public ByteArrayOutputStream exportToXls() throws Exception {
		final ByteArrayOutputStream tmpStream = new ByteArrayOutputStream();
		getResultWorkbook().write(tmpStream);
		notifyAfterReportExport();
		Thread.yield();
		return tmpStream;
	}

	public ByteArrayOutputStream exportToHtml() {
		return null;
	}

	public CktlJxlsReportingTaskThread getCurrentReportingTaskThread() {
		return (CktlJxlsReportingTaskThread) super.getCurrentReportingTaskThread();
	}

	public File getSrcFile() {
		return srcFile;
	}

	public void setSrcFile(File srcFile) {
		this.srcFile = srcFile;
	}

	public HSSFWorkbook getResultWorkbook() {
		return resultWorkbook;
	}

	public void setResultWorkbook(HSSFWorkbook resultWorkbook) {
		this.resultWorkbook = resultWorkbook;
	}

	protected void initSrcFile(String fileName) throws Exception {
		try {
			pageCount = -1;
			if (StringCtrl.isEmpty(fileName)) {
				throw new Exception("Nom de fichier non spécifié");
			}

			final File f = new File(fileName);
			if (!f.exists()) {
				throw new Exception("Le fichier " + fileName + " n'existe pas.");
			}

			notifyTrace("Chargement de " + fileName + " ...");
			setSrcFile(f);

			notifyTrace(fileName + " chargé.");
		} catch (Exception e) {
			notifyAfterException(e);
			throw e;
		}
	}

	public int getPageCount() {
		return pageCount;
	}

	public void setPageCount(int pageCount) {
		this.pageCount = pageCount;
	}

}
