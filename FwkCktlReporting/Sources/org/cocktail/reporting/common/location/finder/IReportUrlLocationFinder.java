package org.cocktail.reporting.common.location.finder;

import java.net.URL;

import org.cocktail.reporting.server.exception.CktlLocationException;

public interface IReportUrlLocationFinder {

    URL search(String reportPath) throws CktlLocationException;
    
}
