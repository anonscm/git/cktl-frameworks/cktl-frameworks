package org.cocktail.reporting.common.location.finder;

import java.io.File;
import java.net.URL;

import org.apache.log4j.Logger;
import org.cocktail.reporting.server.exception.CktlLocationException;

import er.extensions.appserver.ERXApplication;
import er.extensions.appserver.ERXResourceManager;

/**
 * Recherche l'URL d'un rapport embarqué dans l'application.  
 */
public class AppReportUrlLocationFinder implements IReportUrlLocationFinder {

    private static final Logger LOG = Logger.getLogger(AppReportUrlLocationFinder.class);
    private static final String REPORTS_APP_DIR = "Reports";
    
    public URL search(String reportPath) throws CktlLocationException {
        String appPath = REPORTS_APP_DIR + File.separator + reportPath;
        ERXResourceManager rsm = (ERXResourceManager) ERXApplication.application().resourceManager();
        URL appUrl = rsm.pathURLForResourceNamed(appPath, "app", null);
        LOG.info("Recherche de rapport embarqué dans l'application : " + appPath + " " + (appUrl != null ? "trouvé" : "non trouvé"));
        return appUrl;
    }

}
