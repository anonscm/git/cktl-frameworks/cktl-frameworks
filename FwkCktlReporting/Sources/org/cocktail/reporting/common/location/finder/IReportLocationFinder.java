package org.cocktail.reporting.common.location.finder;

import java.net.URL;

import org.cocktail.reporting.server.exception.CktlLocationException;

public interface IReportLocationFinder {

    URL searchJasperLocation(String localLocation, String reportName) throws CktlLocationException;
    URL searchLocation(String localLocation, String reportName, String extension) throws CktlLocationException;

}
