package org.cocktail.reporting.common.location.finder;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.log4j.Logger;
import org.cocktail.reporting.server.exception.CktlLocationException;

/**
 * Recherche l'URL d'un rapport personnalisé par l'etablissement.  
 */
public class LocalReportUrlLocationFinder implements IReportUrlLocationFinder {

    private static final Logger LOG = Logger.getLogger(LocalReportUrlLocationFinder.class);
    
    private String localLocation;
    
    public LocalReportUrlLocationFinder(String localLocation) {
        this.localLocation = localLocation;
    }
    
    public URL search(String reportPath) throws CktlLocationException {
        if (localLocation == null) {
            LOG.info("Pas de recherche de rapports personnalisés car le paramètre 'org.cocktail.<appli>.reports.local.location' n'a pas été renseigné.");
            return null;
        }
        
        URL reportURL = null;
        String localPath = localLocation + File.separator + reportPath;
        try {
            File localFile = new File(localPath);
            if (localFile.exists()) {
                reportURL = localFile.toURI().toURL();
            }
        } catch (MalformedURLException mue) {
            throw new CktlLocationException(mue);
        }
        
        LOG.info("Recherche de rapport personnalisé : " + localPath + " " + (reportURL != null ? "trouvé" : "non trouvé"));
        return reportURL;
    }

}
