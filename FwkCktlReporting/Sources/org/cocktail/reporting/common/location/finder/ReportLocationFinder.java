package org.cocktail.reporting.common.location.finder;

import java.io.File;
import java.net.URL;

import org.cocktail.reporting.server.exception.CktlLocationException;

public class ReportLocationFinder implements IReportLocationFinder {
    
    private static final String JASPER = "jasper";
    
    /* (non-Javadoc)
     * @see org.cocktail.reporting.common.utils.IReportLocationFactory#createJasperLocation(java.lang.String, java.lang.String)
     */
    public URL searchJasperLocation(String localLocation, String reportName) throws CktlLocationException {
        return searchLocation(localLocation, reportName, JASPER);
    }
    
    /* (non-Javadoc)
     * @see org.cocktail.reporting.common.utils.IReportLocationFactory#createLocation(java.lang.String, java.lang.String, java.lang.String)
     */
    public URL searchLocation(String localLocation, String reportName, String extension) throws CktlLocationException {
        String reportPath = reportName + File.separator + reportName + "." + extension;

        IReportUrlLocationFinder localFinder = new LocalReportUrlLocationFinder(localLocation);
        IReportUrlLocationFinder appFinder = new AppReportUrlLocationFinder();
        
        // ATTENTION : l'ordre de passage des finders est important. 
        // On s'arrete au premier trouve.
        return searchUrl(reportPath, localFinder, appFinder);
    }
    
    private URL searchUrl(String reportPath, IReportUrlLocationFinder...finders) throws CktlLocationException {
        URL reportURL = null;
        
        for (IReportUrlLocationFinder finder : finders) {
            reportURL = finder.search(reportPath);
            if (reportURL != null) {
                break;
            }
        }
        
        if (reportURL == null) {
            throw new CktlLocationException(reportPath + " non trouvé");
        }

        return reportURL;
    }
    
}
