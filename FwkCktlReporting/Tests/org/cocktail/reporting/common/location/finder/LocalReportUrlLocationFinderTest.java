package org.cocktail.reporting.common.location.finder;

import static org.junit.Assert.*;

import java.io.File;
import java.net.URL;
import java.util.UUID;

import org.cocktail.reporting.common.location.finder.IReportLocationFinder;
import org.cocktail.reporting.common.location.finder.ReportLocationFinder;
import org.junit.Test;

public class LocalReportUrlLocationFinderTest {

    @Test
    public void testSearch() throws Exception {
        String reportName = "testCreateLocalLocation" + UUID.randomUUID().toString();
        
        String tmpDir = System.getProperty("java.io.tmpdir");
        File tmpReportDir = new File(tmpDir + File.separator + reportName);
        tmpReportDir.mkdir();
        
        File tmpJasperFile = new File(tmpReportDir, reportName + ".jasper");
        tmpJasperFile.createNewFile();
        
        IReportLocationFinder factory = new ReportLocationFinder();
        URL locationUrl = factory.searchJasperLocation(tmpDir, reportName);

        assertNotNull(locationUrl);
        assertEquals(tmpJasperFile.toURI().toURL(), locationUrl);
    }

}
