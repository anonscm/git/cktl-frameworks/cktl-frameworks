/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.controlers;

import java.util.Enumeration;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.RepartPartenaireTranche;
import org.cocktail.cocowork.server.metier.convention.TypeContact;
import org.cocktail.cocowork.server.metier.convention.TypePartenaire;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypeContact;
import org.cocktail.cocowork.server.metier.convention.finder.core.FinderTypePartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules.ConventionPartenairesEtContacts;
import org.cocktail.fwkcktlaccordsguiajax.serveur.VersionMe;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktlpersonne.common.eospecificites.EOStructureForGroupeSpec;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSPropertyListSerialization;

public class ConventionPartenairesEtContactsCtrl extends CtrlModule<ConventionPartenairesEtContacts> {

	private String selectedTab;
	private static NSArray individuOnglets = new NSArray(new String[]{"Contributions","Adresses","Telephones"});

	private NSArray lesEtablissements = null;
	private EOStructure unEtablissement;

	private NSArray lesCentres = null;
	private EOStructure unCentre;
	private EOStructure leCentreGestionnaire;
	
	private WODisplayGroup dgPartenaires, dgContacts, dgContributions;
	private NSMutableDictionary dicoPartenaires, dicoContacts, dicoContributions;
	
	private boolean refreshPartenaires;
	private boolean refreshContacts;
	private boolean refreshContributions;
	
	private ContratPartenaire unContratPartenaire;	
	private Boolean cpPrincipal;
	private IPersonne lePartenaireSelectionne;
	private NSArray lesStatuts;
	private TypePartenaire unStatut;
	
	private ContratPartContact unContratPartContact;
	private IPersonne leContactSelectionne;	
	private NSArray lesTypesContact;
	private TypeContact unTypeContact;
	private EORepartStructure unePersonneAffecteeSelectionnee;
	
	private RepartPartenaireTranche uneContribution;
	
    public ConventionPartenairesEtContactsCtrl(ConventionPartenairesEtContacts component) {
        super(component);
        initialiserDetailPartenaire();
        lesStatuts = FinderTypePartenaire.find(editingContext(), TypePartenaire.ENTITY_NAME, null, null);
        lesTypesContact = FinderTypeContact.find(editingContext(), TypeContact.ENTITY_NAME, null, null);
        refreshPartenaires = false;
        refreshContacts = false;
    }
    
	public void refreshPartenaires(NSNotification notification) {
		if (notification.userInfo() != null && notification.userInfo().containsKey("edc") && notification.object() != null) {
			EOEditingContext ed = (EOEditingContext)notification.userInfo().objectForKey("edc");
			if (ed.equals(editingContext())) {
				refreshPartenaires = true;
				dgPartenaires().setSelectedObject(notification.object());
			}
		} else {
			refreshPartenaires = true;
		}
	}
	public void refreshContacts(NSNotification notification) {
		if (notification.userInfo() != null && notification.userInfo().containsKey("edc") && notification.object() != null) {
			EOEditingContext ed = (EOEditingContext)notification.userInfo().objectForKey("edc");
			if (ed.equals(editingContext())) {
				refreshContacts = true;
				dgContacts().setSelectedObject(notification.object());
			}
		} else {
			refreshContacts = true;
		}
	}
	public void refreshContributions(NSNotification notification) {
		if (notification.userInfo() != null && notification.userInfo().containsKey("edc") && notification.object() != null) {
			EOEditingContext ed = (EOEditingContext)notification.userInfo().objectForKey("edc");
			if (ed.equals(editingContext())) {
				refreshContributions = true;
				dgContributions().setSelectedObject(notification.object());
			}
		} else {
			refreshContributions = true;
		}
	}
	
    public void setIsTabContactsSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "Contacts";
    }
    public boolean isTabContactsSelected() {
		return selectedTab == "Contacts";
	}

    public void setIsTabContributionsSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "Contributions";
    }
    public boolean isTabContributionsSelected() {
		return selectedTab == "Contributions";
	}

    public void setIsTabAdressesSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "Adresses";
    }
    public boolean isTabAdressesSelected() {
		return selectedTab == "Adresses";
	}

    public void setIsTabTelephonesSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "Telephones";
    }
    public boolean isTabTelephonesSelected() {
		return selectedTab == "Telephones";
	}

    public void setIsTabPersonnesAffecteesSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "PersonnesAffectees";
    }
    public boolean isTabPersonnesAffecteesSelected() {
		return selectedTab == "PersonnesAffectees";
	}

    public void setIsTabAdministratifsSelected(boolean isSelected) {
    	if (isSelected) selectedTab = "Administratifs";
    }
    public boolean isTabAdministratifsSelected() {
		return selectedTab == "Administratifs";
	}


    
    public WOActionResults initialiserDetailPartenaire() {
        if (lePartenaireSelectionne() != null && lePartenaireSelectionne().isStructure()) {
            selectedTab = "Contacts";
        } else {
            selectedTab = "Adresses";
        }
        return null;
    }

	/**
	 * @return the dgPartenaires
	 */
	public WODisplayGroup dgPartenaires() {
		if (dgPartenaires == null || refreshPartenaires) {
			refreshPartenaires = false;
			refreshContacts = true;
			if (dgPartenaires == null) {
				dgPartenaires = new WODisplayGroup();
				dgPartenaires.setDelegate(this);
//				EOSortOrdering cpPrincipalSortOrdering = EOSortOrdering.sortOrderingWithKey(ContratPartenaire.CP_PRINCIPAL_KEY, EOSortOrdering.CompareDescending);
//				dgPartenaires.setSortOrderings(new NSArray(cpPrincipalSortOrdering));
			}
			Contrat contrat = wocomponent().contrat();
			if (contrat != null) {
		        EOArrayDataSource ds = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(ContratPartenaire.class),editingContext());
		        ds.setArray(contrat.contratPartenaires());
		        dgPartenaires.setDataSource(ds);
		        dgPartenaires.setSelectsFirstObjectAfterFetch(true);
		        dgPartenaires.fetch();
		        //dgPartenaires.setNumberOfObjectsPerBatch(5);
		        // dgPartenaires.setSelectedObjects(null);
			}
		} else {
			Contrat contrat = wocomponent().contrat();
			if (contrat != null) {
		        ((EOArrayDataSource)(dgPartenaires.dataSource())).setArray(contrat.contratPartenaires());
//		        dgPartenaires.setSelectsFirstObjectAfterFetch(false);
		        dgPartenaires.fetch();
			}
		}
		return dgPartenaires;
	}

	/**
	 * @param dgPartenaires the dgPartenaires to set
	 */
	public void setDgPartenaires(WODisplayGroup dgPartenaires) {
		this.dgPartenaires = dgPartenaires;
	}

	public NSMutableDictionary dicoPartenaires() {
		if (dicoPartenaires == null) {
			NSData data = new NSData(wocomponent().application().resourceManager().bytesForResourceNamed("AvenantPartenaires.plist", VersionMe.APPLICATIONINTERNALNAME, NSArray.EmptyArray));
			dicoPartenaires = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoPartenaires;
	}

	public WOComponent initialiserDetailContact() {
		dgContacts = null;
		return null;
	}
	/**
	 * @return the dgContacts
	 */
	public WODisplayGroup dgContacts() {
 		if (dgContacts == null || refreshContacts ) {
 			refreshContacts = false;
 			if (dgContacts == null) {
 				dgContacts = new WODisplayGroup();
 			}
	        ContratPartenaire partenaire = (ContratPartenaire)dgPartenaires().selectedObject();
			if (partenaire != null) {
		        EOArrayDataSource ds = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(ContratPartContact.class),editingContext());
		        ds.setArray(partenaire.contratPartContacts());
		        dgContacts.setDataSource(ds);
		        dgContacts.fetch();
		        dgContacts.setSelectsFirstObjectAfterFetch(true);
			}
 		}
		return dgContacts;
	}

	/**
	 * @param dgContacts the dgContacts to set
	 */
	public void setDgContacts(WODisplayGroup dgContacts) {
		this.dgContacts = dgContacts;
	}

	public NSMutableDictionary dicoContacts() {
		if (dicoContacts == null) {
			NSData data = new NSData(wocomponent().application().resourceManager().bytesForResourceNamed("PartenaireContacts.plist", VersionMe.APPLICATIONINTERNALNAME, NSArray.EmptyArray));
			dicoContacts = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoContacts;
	}

	/**
	 * @return the dgContributions
	 */
	public WODisplayGroup dgContributions() {
// 		if (dgContributions == null || refreshContributions) {
// 			refreshContributions = false;
 			if (dgContributions == null) {
 				dgContributions = new WODisplayGroup();
 			}
	        ContratPartenaire partenaire = (ContratPartenaire)dgPartenaires().selectedObject();
			if (partenaire != null) {
		        EOArrayDataSource ds = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(RepartPartenaireTranche.class),editingContext());
		        ds.setArray(partenaire.contributions());
		        dgContributions.setDataSource(ds);
		        dgContributions.fetch();
		        dgContributions.setSelectsFirstObjectAfterFetch(true);
			}
// 		}
		return dgContributions;
	}

	/**
	 * @param dgContributions the dgContributions to set
	 */
	public void setDgContributions(WODisplayGroup dgContributions) {
		this.dgContributions = dgContributions;
	}

	public NSMutableDictionary dicoContributions() {
		if (dicoContributions == null) {
			NSData data = new NSData(wocomponent().application().resourceManager().bytesForResourceNamed("PartenaireContributions.plist", VersionMe.APPLICATIONINTERNALNAME, NSArray.EmptyArray));
			dicoContributions = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoContributions;
	}

	/**
	 * @return the lePartenaireSelectionne
	 */
	public IPersonne lePartenaireSelectionne() {
		lePartenaireSelectionne = null;
		ContratPartenaire contratPartenaire = (ContratPartenaire)dgPartenaires().selectedObject();
		if (contratPartenaire != null) {
			lePartenaireSelectionne = contratPartenaire.partenaire();
		}
		return lePartenaireSelectionne;
	}

	/**
	 * @param lePartenaireSelectionne the lePartenaireSelectionne to set
	 */
	public void setLePartenaireSelectionne(IPersonne lePartenaireSelectionne) {
		this.lePartenaireSelectionne = lePartenaireSelectionne;
	}

	public boolean isPartenaireSelectionneIsPersonneMorale() {
		boolean isPartenaireSelectionneIsPersonneMorale = false;
		
		if (lePartenaireSelectionne() != null && lePartenaireSelectionne().persType().equalsIgnoreCase("STR"))  {
			isPartenaireSelectionneIsPersonneMorale = true;
		}
		return isPartenaireSelectionneIsPersonneMorale;
	}

	/**
	 * @return the lesStatuts
	 */
	public NSArray lesStatuts() {
		return lesStatuts;
	}

	/**
	 * @param lesStatuts the lesStatuts to set
	 */
	public void setLesStatuts(NSArray lesStatuts) {
		this.lesStatuts = lesStatuts;
	}

	/**
	 * @return the unStatut
	 */
	public TypePartenaire unStatut() {
		return unStatut;
	}

	/**
	 * @param unStatut the unStatut to set
	 */
	public void setUnStatut(TypePartenaire unStatut) {
		this.unStatut = unStatut;
	}

	/**
	 * @return the lesTypesContact
	 */
	public NSArray lesTypesContact() {
		return lesTypesContact;
	}

	/**
	 * @param lesTypesContact the lesTypesContact to set
	 */
	public void setLesTypesContact(NSArray lesTypesContact) {
		this.lesTypesContact = lesTypesContact;
	}

	/**
	 * @return the unTypeContact
	 */
	public TypeContact unTypeContact() {
		return unTypeContact;
	}

	/**
	 * @param unTypeContact the unTypeContact to set
	 */
	public void setUnTypeContact(TypeContact unTypeContact) {
		this.unTypeContact = unTypeContact;
	}

	/**
	 * @return the leContactSelectionne
	 */
	public IPersonne leContactSelectionne() {
		leContactSelectionne = null;
		ContratPartContact contact = (ContratPartContact)dgContacts().selectedObject();
		if (contact != null) {
			leContactSelectionne = contact.personne();
		}
		return leContactSelectionne;
	}

	/**
	 * @param leContactSelectionne the leContactSelectionne to set
	 */
	public void setLeContactSelectionne(IPersonne leContactSelectionne) {
		this.leContactSelectionne = leContactSelectionne;
	}

	/**
	 * @return the unContratPartenaire
	 */
	public ContratPartenaire unContratPartenaire() {
		return unContratPartenaire;
	}

	/**
	 * @param unContratPartenaire the unContratPartenaire to set
	 */
	public void setUnContratPartenaire(ContratPartenaire unContratPartenaire) {
		this.unContratPartenaire = unContratPartenaire;
	}

	/**
	 * @return the unContratPartContact
	 */
	public ContratPartContact unContratPartContact() {
		return unContratPartContact;
	}

	/**
	 * @param unContratPartContact the unContratPartContact to set
	 */
	public void setUnContratPartContact(ContratPartContact unContratPartContact) {
		this.unContratPartContact = unContratPartContact;
	}

	/**
	 * @return the unePersonneAffecteeSelectionnee
	 */
	public EORepartStructure unePersonneAffecteeSelectionnee() {
		return unePersonneAffecteeSelectionnee;
	}

	/**
	 * @param unePersonneAffecteeSelectionnee the unePersonneAffecteeSelectionnee to set
	 */
	public void setUnePersonneAffecteeSelectionnee(EORepartStructure unePersonneAffecteeSelectionnee) {
		this.unePersonneAffecteeSelectionnee = unePersonneAffecteeSelectionnee;
	}

	/**
	 * @return the uneContribution
	 */
	public RepartPartenaireTranche uneContribution() {
		return uneContribution;
	}

	/**
	 * @param uneContribution the uneContribution to set
	 */
	public void setUneContribution(RepartPartenaireTranche uneContribution) {
		this.uneContribution = uneContribution;
	}

	public NSArray lesEtablissements() {
		NSArray<ContratPartenaire> lesPartenaires = dgPartenaires().allObjects();
		Enumeration<ContratPartenaire> enumLesPartenaires = lesPartenaires.objectEnumerator();
		NSMutableArray<EOStructure> tmp = new NSMutableArray<EOStructure>();
//		tmp.addObjectsFromArray(wocomponent().mySession().lesEtablissementsAffectation());
		tmp.addObjectsFromArray(wocomponent().etablissementsAffectation());
		while (enumLesPartenaires.hasMoreElements()) {
			ContratPartenaire contratPartenaire = (ContratPartenaire) enumLesPartenaires.nextElement();
			IPersonne personne = contratPartenaire.partenaire();
			if (personne.isStructure() && ((EOStructure)personne).isEtablissement() && !tmp.containsObject((EOStructure)personne)) {
				tmp.add(((EOStructure)personne));
			}
		}
		
		lesEtablissements = tmp;
		return lesEtablissements;
	}

	public void setLesEtablissements(NSArray lesEtablissements) {
		this.lesEtablissements = lesEtablissements;
	}

	public EOStructure unEtablissement() {
		return unEtablissement;
	}

	public void setUnEtablissement(EOStructure unEtablissement) {
		this.unEtablissement = unEtablissement;
	}

	public NSArray lesCentres() {
		Contrat contrat = wocomponent().contrat();
		ContratPartenaire lePartenaireInternePrincipal = null;

		if (lesCentres == null) {
			if (contrat.etablissement() != null) {
				lePartenaireInternePrincipal = contrat.partenaireForPersId(contrat.etablissement().persId());
			}
			if (lePartenaireInternePrincipal != null) {
				lesCentres = EOStructureForGroupeSpec.sharedInstance().getSousGroupesRecursif(editingContext(), contrat.etablissement(), EOStructureForGroupeSpec.QUAL_GROUPES_SERVICES);
				if (leCentreGestionnaire() != null && !lesCentres.containsObject(leCentreGestionnaire())) {
					lesCentres=lesCentres.arrayByAddingObject(leCentreGestionnaire);				
				}
			} else {
				lesCentres = null;
			}
		} else if (contrat.etablissement() == null) {
			lesCentres = null;
			setLeCentreGestionnaire(null);
		}
		return lesCentres;
	}

	public void setLesCentres(NSArray lesCentres) {
		this.lesCentres = lesCentres;
	}

	public EOStructure unCentre() {
		return unCentre;
	}
	public void setUnCentre(EOStructure unCentre) {
		this.unCentre = unCentre;
	}

	public void setLeCentreGestionnaire(EOStructure leCentreGestionnaire) {
		Contrat contrat = wocomponent().contrat();
		if (contrat != null) {
			contrat.setCentreResponsabiliteRelationship(leCentreGestionnaire, wocomponent().utilisateurPersId());
			dgPartenaires().setSelectionIndexes(new NSArray(0));
			dgPartenaires().updateDisplayedObjects();
			refreshContacts = true;
			
		}
	}
	
	public EOStructure leCentreGestionnaire() {
		Contrat contrat = wocomponent().contrat();
		if (contrat != null) {
			leCentreGestionnaire = contrat.centreResponsabilite();
			if (leCentreGestionnaire == null) {
				if (contrat.organComposante() != null) {
					leCentreGestionnaire = FinderOrgan.getStructureForOrgan(contrat.organComposante(), EOOrgan.ORG_NIV_2);
					contrat.setCentreResponsabiliteRelationship(leCentreGestionnaire);
				}
			}
		}
		return leCentreGestionnaire;
	}

	
	public String dialogTitleModifierPartenaire() {
		String dialogTitleModifierPartenaire = "Modifier le partenaire ";
		if (lePartenaireSelectionne() != null) {
			dialogTitleModifierPartenaire += lePartenaireSelectionne().persLibelleAffichage();
		}
		return dialogTitleModifierPartenaire;
	}

	public String dialogTitleModifierContact() {
		String dialogTitleModifierContact = "Modifier le Contact ";
		if (leContactSelectionne() != null) {
			dialogTitleModifierContact += leContactSelectionne().persLibelleAffichage();
		}
		return dialogTitleModifierContact;
	}

	public void displayGroupDidChangeSelectedObjects(WODisplayGroup group) {
		if (group.equals(dgPartenaires)) {
			NSMutableDictionary userInfo = new NSMutableDictionary();
			userInfo.setObjectForKey(wocomponent().session().sessionID(), "sessionID");
						
			NSNotificationCenter.defaultCenter().postNotification("refreshSecretairesEtMembresNotification", this, userInfo);
		}
	}

	/**
	 * @return the cpPrincipal
	 */
	public Boolean cpPrincipal() {
		return unContratPartenaire.cpPrincipalBoolean();
	}

	/**
	 * @param cpPrincipal the cpPrincipal to set
	 */
	public void setCpPrincipal(Boolean cpPrincipal) {
		if (Boolean.TRUE.equals(cpPrincipal)) {
			Contrat contrat = wocomponent().contrat();
			if (contrat != null) {
				ContratPartenaire oldPartenairePrincipal = contrat.partenairePrincipal();
				if (oldPartenairePrincipal != null) {
					oldPartenairePrincipal.setCpPrincipalBoolean(false);
				}
			}
		}
		this.cpPrincipal = cpPrincipal;
		unContratPartenaire.setCpPrincipalBoolean(cpPrincipal);
	}
	
	public boolean nePeutEditerPartenaires() {
	    return !wocomponent().applicationUser().hasDroitModificationPartenaires();
	}
	
}
