/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.ACktlAjaxAccordsComponent;
import org.cocktail.fwkcktlaccordsguiajax.components.GestionPartenaire;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eoaccess.EOUtilities;

import er.ajax.AjaxUtils;
import er.extensions.eof.ERXEC;

public class PartenaireModalWin extends ACktlAjaxAccordsComponent {
	
	ContratPartenaire partenaire = null;
	GestionPartenaire pageInModal;

	private Contrat contrat;

	
	public PartenaireModalWin(WOContext context) {
        super(context);
    }
    
	@Override
	public void appendToResponse(WOResponse response, WOContext context) {

		super.appendToResponse(response, context);

		AjaxUtils.addScriptResourceInHead(context, response, "prototype.js");
		AjaxUtils.addScriptResourceInHead(context, response, "FwkCktlThemes.framework", "scripts/window.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/strings.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/formatteurs.js");
		AjaxUtils.addScriptResourceInHead(context, response, "app", "scripts/cocolight.js");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/default.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/alert.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "themes/lighting.css");

		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommon.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlThemes.framework", "css/CktlCommonBleu.css");
		AjaxUtils.addStylesheetResourceInHead(context, response, "app", "styles/cocolight.css");
		
	}

	public WOActionResults ajouterOuModifierUnPartenaire() {
		pageInModal = (GestionPartenaire)pageWithName(GestionPartenaire.class.getName());
		pageInModal.setPartenaire(partenaire());
		IPersonne personne = null;
		if (partenaire() != null) {
			personne = (IPersonne)EOUtilities.localInstanceOfObject(ERXEC.newEditingContext(), partenaire.partenaire());
		}
		pageInModal.setSelectedPersonne(personne);
		pageInModal.setIsDirectAction(Boolean.TRUE);
//		session.setIndexModuleActifGestionPartenaire(null);
		// TODO : faire un binding ?
		return pageInModal;
	}

	public boolean showMessageSuccess() {
	    return pageInModal != null && pageInModal.isSuccess();
	}
	
	public boolean showMessageCancel() {
	    return pageInModal != null && !pageInModal.isSuccess();
	}
	
	/**
	 * @return the partenaire
	 */
	public ContratPartenaire partenaire() {
		return partenaire;
	}

	/**
	 * @param partenaire the partenaire to set
	 */
	public void setPartenaire(ContratPartenaire partenaire) {
		this.partenaire = partenaire;
	}
	
	/**
	 * @return the contrat
	 */
	public Contrat contrat() {
		return contrat;
	}

	/**
	 * @param contrat
	 *            the contrat to set
	 */
	public void setContrat(Contrat contrat) {
		this.contrat = contrat;
	}

}
