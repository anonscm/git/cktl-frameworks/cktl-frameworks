/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.Avenant;
import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.fwkcktlaccordsguiajax.components.ACktlAjaxAccordsComponent;
import org.cocktail.fwkcktlaccordsguiajax.components.assistants.Assistant;
import org.cocktail.fwkcktlaccordsguiajax.components.controlers.CtrlModule;
import org.cocktail.fwkcktlpersonne.common.PersonneApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ModuleAssistant extends ACktlAjaxAccordsComponent implements IModuleAssistant {
	private static final long serialVersionUID = -4145974597929479420L;

	public static final String BDG_editingContext = "editingContext";
	public static final String APPLICATION_USER_BDG = "applicationUser";
	public static final String IS_APP_DEBUG_MODE_BDG = "isAppInDebugMode";
	public static final String BDG_personne = "personne";

	
	public static final String MODULE_BDG = "module";
	
	
	public static String STYLE_BDG = "styleCss";
	public static String CLASS_BDG = "classeCss";
	public static String CONTRAT_BDG = "contrat";
	public static String AVENANT_BDG = "avenant";
	public static String ROLES_BDG = "roles";
	public static String CONTACT_BDG = "contact";
	public static String FORM_ID_BDG = "formID";
	public static String ASSISTANT_BDG = "assistant";
	

	private CtrlModule ctrl = null;
	
	private NSArray roles;
	private ContratPartContact contact;
	
	private IModuleAssistant module;

	public String updateContainerID;
	public String updateContainerIDOnSelectionnerPersonneInTableview;
	
	public String formID;
	
	private Boolean _disabled;
	
	private CocoworkApplicationUser applicationUser;
	
	private Contrat _contrat;
	private Avenant _avenant;
	
	public ModuleAssistant(WOContext context) {
		super(context);
	}

	public ModuleAssistant(WOContext context, CtrlModule controler) {
		super(context);
		ctrl = controler;
		_disabled = null;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		setModule(this);
		super.appendToResponse(response, context);
	}

	/**
	 * @return the ctrl
	 */
	public CtrlModule ctrl() {
		return ctrl;
	}

	/**
	 * @param ctrl the ctrl to set
	 */
	public void setCtrl(CtrlModule ctrl) {
		this.ctrl = ctrl;
	}

	/**
	 * @return the contrat
	 */
    public Contrat contrat() {
        if (_contrat == null) {
            Contrat contratFromParent = (Contrat) valueForBinding(CONTRAT_BDG);
            if (contratFromParent != null && contratFromParent.editingContext() != null)
                _contrat = contratFromParent.localInstanceIn(editingContext());
        }
        return _contrat;
    }
	
	/**
	 * @return the avenant
	 */
	public Avenant avenant() {
	    if (_avenant == null) {
	        Avenant avenantFromParent = (Avenant)valueForBinding(AVENANT_BDG);
	        _avenant = avenantFromParent.localInstanceIn(editingContext());
	    }
	    return _avenant;
	}

	public WOComponent submit() {		
		return null;
	}

	/**
	 * @return the styleCss
	 */
	public String styleCss() {
		return (String)valueForBinding(STYLE_BDG);
	}

	/**
	 * @return the classeCss
	 */
	public String classeCss() {
		return (String)valueForBinding(CLASS_BDG);
	}


	/**
	 * @return the assistant
	 */
	public Assistant assistant() {
		return (Assistant)valueForBinding(ASSISTANT_BDG);
	}

	/**
	 * @return the module
	 */
	public IModuleAssistant module() {
		return module;
	}

	/**
	 * @param module the module to set
	 */
	public void setModule(IModuleAssistant module) {
		this.module = module;
		setValueForBinding(module, MODULE_BDG);
	}
	/**
	 * @return the editingContext
	 */
	public EOEditingContext editingContext() {
		return (EOEditingContext)valueForBinding(BDG_editingContext);
	}

	public String disabledTFScript() {
		String disabledTFScript = "";
		
		if (disabled()) {
			//disabledTFScript = "$('FormGeneralites').disable();";
			disabledTFScript = "document.forms[0].disable();";
		}
		
		return disabledTFScript;
	}

	public boolean disabled() {
		return _disabled().booleanValue();
	}
	public Boolean enabled() {
		return !disabled();
	}

	public boolean isPrecedentDisabled() {
		return false;
	}
	public void onPrecedent() {
	}

	public boolean isSuivantDisabled() {
		return false;
	}
	public void onSuivant() {
	}

	public boolean isTerminerDisabled() {
		return true;
	}
	public boolean valider() {
		return true;
	}


	/**
	 * @return the utilisateurPersId
	 */
	public Integer utilisateurPersId() {
		return applicationUser().getPersId();
	}

//	/**
//	 * @param utilisateurPersId the utilisateurPersId to set
//	 */
//	public void setUtilisateurPersId(Integer utilisateurPersId) {
//		this.utilisateurPersId = utilisateurPersId;
//	}

	/**
	 * @return the roles
	 */
	public NSArray roles() {
		return roles;
	}

	/**
	 * @param roles the roles to set
	 */
	public void setRoles(NSArray roles) {
		this.roles = roles;
	}

	/**
	 * @return the contact
	 */
	public ContratPartContact contact() {
		return contact;
	}

	/**
	 * @param contact the contact to set
	 */
	public void setContact(ContratPartContact contact) {
		this.contact = contact;
	}

	public Boolean _disabled() {
		if (_disabled == null) {
			_disabled = contrat() == null || ((!applicationUser().hasDroitModificationContratsEtAvenants() ||
			!contrat().isModifiablePar(applicationUser().getUtilisateur())) && !applicationUser().hasDroitSuperAdmin());
		}
		return _disabled;
	}

	public boolean isInAssistant() {
		return assistant()!=null;
	}
	
	public boolean isStandAlone() {
		return assistant()==null;
	}
	
	/**
	 * @return the applicationUser
	 */
	public CocoworkApplicationUser applicationUser() {
//		CocoworkApplicationUser applicationUser = null;
		// FIXME: applicationUser est systematiquement recalcule ce qui provoque que tous les droits sont systematiquement recalcules
		if (applicationUser == null) {
			if (isStandAlone()) {
				if (hasBinding(APPLICATION_USER_BDG)&&valueForBinding(APPLICATION_USER_BDG)!=null) {
					PersonneApplicationUser appUser = (PersonneApplicationUser)valueForBinding(APPLICATION_USER_BDG);
					applicationUser = new CocoworkApplicationUser(editingContext(), appUser.getPersId());
				}
			} else {
				applicationUser = assistant().applicationUser();
			}
		}
		return applicationUser;
	}

	/**
	 * @param applicationUser the applicationUser to set
	 */
	public void setApplicationUser(CocoworkApplicationUser applicationUser) {
		if (isStandAlone()) {
			if (canSetValueForBinding(APPLICATION_USER_BDG)) {
				setValueForBinding(applicationUser, APPLICATION_USER_BDG);
			}
		} else {
			assistant().setApplicationUser(applicationUser);
		}
		this.applicationUser = applicationUser;
	}
	
	/**
	 * @param indexModuleActif the indexModuleActif to set
	 */
	public void setIndexModuleActif(Integer indexModuleActif) {
		if (isInAssistant()) {
			assistant().setIndexModuleActif(indexModuleActif);
		}
	}
	

	/**
	 * @return the isApplicationInDebugMode
	 */
	public boolean isApplicationInDebugMode() {
		boolean isApplicationInDebugMode = false;
		if (isStandAlone()) {
			if (hasBinding(IS_APP_DEBUG_MODE_BDG)) {
				isApplicationInDebugMode = booleanValueForBinding(IS_APP_DEBUG_MODE_BDG, false);
			}
		} else {
			isApplicationInDebugMode = assistant().isApplicationInDebugMode();
		}
		return isApplicationInDebugMode;
	}
	
	/**
	 * @return the personne
	 */
	public IPersonne personne() {
		return (IPersonne)valueForBinding(BDG_personne);
	}

	/**
	 * @param personne the personne to set
	 */
	public void setPersonne(IPersonne personne) {
		setValueForBinding(personne, BDG_personne);
	}
	
	@Override
	public void sleep() {
	    super.sleep();
	    _contrat = null;
	    _avenant = null;
	}
	
}
