/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.cocowork.server.metier.convention.factory.FactoryContratPartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.GestionContact;
import org.cocktail.fwkcktlaccordsguiajax.components.GestionPartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.assistants.Assistant;
import org.cocktail.fwkcktlaccordsguiajax.components.controlers.ConventionPartenairesEtContactsCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOTypeTel;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOComponent;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSSelector;

import er.extensions.eof.ERXEC;

/**
 * Composant de gestion des partenaires et des contacts d'une convention.
 * Ce composant peut être une étape d'un assistant (création d'une convention par exemple), mais aussi être utilisé en 
 * mode 'standalone' comme un composant indépendant (dans un onglet par exemple).
 * 
 * @binding etablissementsAffectation : obligatoire liste des établissements d'affectation.
 * 
 * @binding editingContext : facultatif editingContext à utiliser. (passé par l'assistant si le composant est dans un assistant) 
 * @binding applicationUser : obligatoire CocoworkApplicationUser représentant l'utilisateur manipulant les partenaires. (passé par l'assistant si le composant est dans un assistant) 
 * @binding isAppInDebugMode : facultatif flag disant si l'application est en mode debug ou non. (passé par l'assistant si le composant est dans un assistant) 
 * @binding updateContainerID : facultatif id du container à mettre à jour après avoir modifié, ajouté un partenaire ou un contact
 * 
 * @author Pierre-Yves MARIE <pierre-yves.marie at cocktail.org>
 *
 */
public class ConventionPartenairesEtContacts extends ModuleAssistant {
	private static final long serialVersionUID = -1392179663746384811L;
	
	public static final String BDG_ETABLISSEMENTS_AFFECTATION = "etablissementsAffectation";
	public static final String BDG_PARTENAIRE_INDEX_MODULE_ACTIF = "indexPartenaireModuleActif";
	public static final String BDG_CONTACT_INDEX_MODULE_ACTIF = "indexContactModuleActif";
	public static final String BDG_UPDATE_CONTAINER_ID = "updateContainerID";

	public ConventionPartenairesEtContactsCtrl ctrl = null;
	
	private NSArray etablissementsAffectation;
	
	public ConventionPartenairesEtContacts(WOContext context) {
        super(context);
    }
	
	@Override
	public boolean disabled() {
	    return super.disabled() || ctrl().nePeutEditerPartenaires();
	}
	
	public ConventionPartenairesEtContactsCtrl ctrl() {
		if (ctrl == null) {
	        ctrl = new ConventionPartenairesEtContactsCtrl(this);
	        Class[] notificationArray = new Class[] { NSNotification.class };
	        NSNotificationCenter.defaultCenter().addObserver(ctrl, new NSSelector("refreshContacts",notificationArray), "refreshContactsNotification", null);
	        NSNotificationCenter.defaultCenter().addObserver(ctrl, new NSSelector("refreshPartenaires",notificationArray), "refreshPartenairesNotification", null);
		}
		return ctrl;
	}

	public void setCtrl(ConventionPartenairesEtContactsCtrl ctrl) {
		this.ctrl = ctrl;
	}

	@Override
	public boolean valider() {
		boolean validate = false;
		Contrat contrat = contrat();
		
		if (contrat != null && contrat.etablissement() != null && 
			contrat.centreResponsabilite() != null) {
			if (contrat.contratPartenaires().count()<=1) {
				((Assistant)parent()).setFailureMessage("La convention doit avoir au moins deux partenaires.");
			} else {
				validate = true;
				((Assistant)parent()).setFailureMessage(null);
			}
		} else {
			((Assistant)parent()).setFailureMessage("Vous devez renseigner l'\u00E9tablissement interne principal et le centre gestionnaire.");
		}
		
    	return validate;
    }

	
	public boolean isAjouterCentreGestionnaireAuxPartenairesDisabled() {
		boolean isAjouterCentreGestionnaireAuxPartenairesDisabled = true;
		if (contrat() != null && contrat().centreResponsabilite() != null) {
			// TODO Tester si centre pas deja present dans la liste des partenaires
			isAjouterCentreGestionnaireAuxPartenairesDisabled = false;
		}
		return isAjouterCentreGestionnaireAuxPartenairesDisabled;
	}

	public WOActionResults ajouterUnPartenaire() {
		GestionPartenaire nextPage = (GestionPartenaire)pageWithName(GestionPartenaire.class.getName());
		nextPage.setContrat(contrat());
		nextPage.setPartenaire(null);
		nextPage.setSelectedPersonne(null);
		nextPage.setIndexModuleActif(indexPartenaireModuleActif());
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		return nextPage;
	}
	public WOActionResults editerUnPartenaire() {
		return null;
	}
	public WOActionResults modifierUnPartenaire() {
		GestionPartenaire nextPage = (GestionPartenaire)pageWithName(GestionPartenaire.class.getName());
		nextPage.setContrat(contrat());
		ContratPartenaire partenaire = (ContratPartenaire)ctrl().dgPartenaires().selectedObject();
		nextPage.setPartenaire(partenaire);
		//setIndexPartenaireModuleActif(null);
		nextPage.setIndexModuleActif(indexPartenaireModuleActif());
		IPersonne personne = (IPersonne)EOUtilities.localInstanceOfObject(ERXEC.newEditingContext(), partenaire.partenaire());
		personne.setValidationEditingContext(editingContext());
		nextPage.setSelectedPersonne(personne);
//		session.setIndexModuleActifGestionPartenaire(null);
		//setIndexModuleActif(null);
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		
		return nextPage;
	}

	public String onClickAjouterPartenaire() {
		String onClickAjouterPartenaire = null;
		onClickAjouterPartenaire = "openWinAjouterPartenaire(this.href,'Ajouter un partenaire');return false;";
		
		return onClickAjouterPartenaire;
	}
	public String onClickEditerPartenaire() {
		String onClickEditerPartenaire = null;
		onClickEditerPartenaire = "openWinAjouterPartenaire(this.href,'Modifier un partenaire');return false;";
		
		return onClickEditerPartenaire;
	}
	public String onClickModifierUnPartenaire() {
		String onClickModifierPartenaire = null;
		onClickModifierPartenaire = "openWinAjouterPartenaire(this.href,'Modifier un partenaire');return false;";
		
		return onClickModifierPartenaire;
	}

	public WOActionResults closeWinAjouterPartenaire() {
		return null;
	}

	public WOActionResults supprimerUnPartenaire() {
		ContratPartenaire lePartenaireASupprimer = (ContratPartenaire)ctrl().dgPartenaires().selectedObject();

		if (lePartenaireASupprimer != null) {
			ERXEC edc = (ERXEC)lePartenaireASupprimer.editingContext();
			
			FactoryContratPartenaire fcp = new FactoryContratPartenaire(edc,isApplicationInDebugMode());
			try {
				fcp.supprimerContratPartenaire(lePartenaireASupprimer, applicationUser().getPersId());
				ctrl().setDgPartenaires(null);
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				context().response().setStatus(500);
//				session().setMessageErreur(e.getMessage());
				mySession().addSimpleErrorMessage("Erreur", e);
				e.printStackTrace();
			}
		}
		
		return null;
	}
	public String onClickSupprimerPartenaire() {
		String onClickAjouterPartenaire = null;
		onClickAjouterPartenaire = "openWinAjouterPartenaire(this.href,'Ajouter un partenaire');return false;";
		
		return onClickAjouterPartenaire;
	}

	
	public WOComponent initialiserDetailContact() {
		return ctrl().initialiserDetailContact();
	}

	public boolean isSupprimerUnPartenaireEnabled() {
		return enabled() && ctrl != null && ctrl().dgPartenaires() != null && ctrl().dgPartenaires().selectedObject() != null ? true:false;
	}
	public boolean isSupprimerUnPartenaireDisabled() {
		return !isSupprimerUnPartenaireEnabled();
	}
	public boolean isModifierUnPartenaireDisabled() {
		return disabled() || ctrl == null || ctrl().dgPartenaires() == null || ctrl().dgPartenaires().selectedObject() == null ? true:false;
	}


	public String onClickAjouterContact() {
		String onClickAjouterContact = null;
		onClickAjouterContact = "openWinAjouterContact(this.href,'Ajouter un contact');return false;";
		
		return onClickAjouterContact;
	}
	public WOActionResults ajouterUnContact() {
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setContrat(contrat());
		nextPage.setPartenaire((ContratPartenaire)ctrl().dgPartenaires().selectedObject());
		setIndexContactModuleActif(null);
		nextPage.setIndexModuleActif(indexContactModuleActif());
		nextPage.setContact(null);
		nextPage.setSelectedPersonne(null);
		
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		// Pour forcer le refetch du display group a la fermeture de la fenetre d'ajout d'un contact
		ctrl().setDgContacts(null);
		return nextPage;
	}
	public WOActionResults editerUnContact() {
		return null;
	}
	public String onClickModifierUnContact() {
		String onClickModifierContact = null;
		onClickModifierContact = "openWinAjouterContact(this.href,'Modifier un contact');return false;";
		return onClickModifierContact;
	}
	public WOActionResults modifierUnContact() {
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setContrat(contrat());
		ContratPartenaire contratPartenaire = (ContratPartenaire)ctrl().dgPartenaires().selectedObject();
		nextPage.setPartenaire(contratPartenaire);
		nextPage.setContrat(contratPartenaire.contrat());
		setIndexContactModuleActif(null);
		nextPage.setIndexModuleActif(indexContactModuleActif());
		
		nextPage.setApplicationUser(applicationUser());
		nextPage.setApplicationInDebugMode(isApplicationInDebugMode());
		
		ContratPartContact contact = (ContratPartContact)ctrl().dgContacts().selectedObject();
		if (contact != null) {
			nextPage.setContact(contact);
			IPersonne personne = contact.personne().localInstanceIn(nextPage.editingContextForPersonne());
			personne.setValidationEditingContext(nextPage.editingContextForPersonne());
			nextPage.setSelectedPersonne(personne);
		}
		return nextPage;
	}

	public boolean isModifierUnContactDisabled() {
		return disabled() || ctrl == null || ctrl().dgContacts() == null || ctrl().dgContacts().selectedObject() == null? true:false;
	}
	
	public boolean isSupprimerUnContactDisabled() {
		return !isSupprimerUnContactEnabled();
	}
	
	public boolean isSupprimerUnContactEnabled() {
		return enabled() && ctrl != null && ctrl().dgContacts() != null && ctrl().dgContacts().selectedObject() != null? true:false;
	}
	
	public WOActionResults supprimerUnContact() {
		ContratPartContact leContactASupprimer = (ContratPartContact)ctrl().dgContacts().selectedObject();
		ContratPartenaire lePartenaire = (ContratPartenaire)ctrl().dgPartenaires().selectedObject();
		if (leContactASupprimer != null) {
			FactoryContratPartenaire fap = new FactoryContratPartenaire(leContactASupprimer.editingContext(),isApplicationInDebugMode());
			try {
				fap.supprimerContact(leContactASupprimer, lePartenaire, applicationUser().getPersId());
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				context().response().setStatus(500);
//				session().setMessageErreur(e.getMessage());
				mySession().addSimpleErrorMessage("Erreur", e);
				e.printStackTrace();
			}
		}
		return null;
	}
	public String onClickSupprimerContact() {
		String onClickSupprimerContact = null;
		onClickSupprimerContact = "openWinAjouterContact(this.href,'Supprimer un Contact');return false;";
		
		return onClickSupprimerContact;
	}

	public String onClickEditerContact() {
		String onClickEditerContact = null;
		onClickEditerContact = "openWinAjouterContact(this.href,'Modifier un contact');return false;";
		
		return onClickEditerContact;
	}

	public WOActionResults closeWinAjouterContact() {
		return null;
	}


	public Boolean isModeEditionTVContacts() {
		return Boolean.TRUE;
	}


	public EOQualifier typeAdressePro() {
		return EOTypeAdresse.QUAL_TADR_CODE_PRO;
	}
	public EOQualifier typeTelPro() {
		return EOTypeTel.QUAL_C_TYPE_TEL_PRF;
	}


	/**
	 * @return the unContratPartenaire
	 */
	public ContratPartenaire unContratPartenaire() {
		return ctrl().unContratPartenaire();
	}


	/**
	 * @param unContratPartenaire the unContratPartenaire to set
	 */
	public void setUnContratPartenaire(ContratPartenaire unContratPartenaire) {
		ctrl().setUnContratPartenaire(unContratPartenaire);
	}


	/**
	 * @return the unContratPartContact
	 */
	public ContratPartContact getUnContratPartContact() {
		return ctrl().unContratPartContact();
	}


	/**
	 * @param unContratPartContact the unContratPartContact to set
	 */
	public void setUnContratPartContact(ContratPartContact unContratPartContact) {
		ctrl().setUnContratPartContact(unContratPartContact);
	}


	public WOActionResults ajouterPersonneAffecteeAuxContacts() {
		EORepartStructure unePersonneRepartStructure = ctrl().unePersonneAffecteeSelectionnee();
		if (unePersonneRepartStructure != null) {
			IPersonne unePersonne = unePersonneRepartStructure.toPersonneElt();
			IPersonne personneAAjouter = (IPersonne)EOUtilities.localInstanceOfObject(editingContext(), unePersonne);
			FactoryContratPartenaire fcp = new FactoryContratPartenaire(editingContext(),isApplicationInDebugMode());
			try {
				fcp.ajouterContact((ContratPartenaire)ctrl().dgPartenaires().selectedObject(), personneAAjouter, null);
				ctrl().setIsTabContactsSelected(true);
				ctrl().setDgContacts(null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return null;
	}


	public boolean isAfficherAjouterPersonneAffectee() {
		boolean isAfficherAjouterPersonneAffectee = false;
		if (ctrl().lePartenaireSelectionne() != null && ctrl().lePartenaireSelectionne().isStructure()) {
			NSArray repartStructuresAffectees = ((EOStructure)ctrl().lePartenaireSelectionne()).toRepartStructuresElts();
			if (repartStructuresAffectees != null && repartStructuresAffectees.count()>0) {
				isAfficherAjouterPersonneAffectee = true;
			}
		}
			
		return isAfficherAjouterPersonneAffectee;
	}

	public WOActionResults onCloseAssistantPartenaire() {
//		session.setIndexModuleActifGestionPartenaire(null);
		setIndexModuleActif(null);
		return null;
	}

	public WOActionResults onCloseAssistantContact() {
//		session.setIndexModuleActifGestionContact(null);
		setIndexModuleActif(null);
		return null;
	}

	public String updateContainerIDsForTVContributions() {
		String updateContainerIDsForTVContributions = null;
		WODisplayGroup dgPartenaires = ctrl().dgPartenaires();
		if (dgPartenaires != null && dgPartenaires.selectedObject() != null) {
			updateContainerIDsForTVContributions = "TableViewPartenaires_TrLigne"+dgPartenaires.allObjects().indexOfIdenticalObject(dgPartenaires.selectedObject());
		}
		return updateContainerIDsForTVContributions;
	}

	public String updateContainerJsAferHide() {
        String func = "function() { ContainerPartenairesEtContactsUpdate(); ";
        String updateId = stringValueForBinding(BDG_UPDATE_CONTAINER_ID, null);
	    if ( updateId != null) {
	        func = func + updateId +"Update(); }";
	    } else {
	        func = func + "}";
	    }
	    return func;
	}
	
	/**
	 * @return the etablissementsAffectation
	 */
	public NSArray etablissementsAffectation() {
		if (hasBinding(BDG_ETABLISSEMENTS_AFFECTATION) && valueForBinding(BDG_ETABLISSEMENTS_AFFECTATION)!=null) {
			etablissementsAffectation = (NSArray)valueForBinding(BDG_ETABLISSEMENTS_AFFECTATION);
		} else {
			etablissementsAffectation = new NSArray();
		}
		return etablissementsAffectation;
	}

	/**
	 * @param etablissementsAffectation the etablissementsAffectation to set
	 */
	public void setEtablissementsAffectation(NSArray etablissementsAffectation) {
		this.etablissementsAffectation = etablissementsAffectation;
		if (canSetValueForBinding(BDG_ETABLISSEMENTS_AFFECTATION)) {
			setValueForBinding(etablissementsAffectation, BDG_ETABLISSEMENTS_AFFECTATION);
		}
	}
	
	
	/**
	 * @return the indexPartenaireModuleActif
	 */
	public Integer indexPartenaireModuleActif() {
		return (Integer) valueForBinding(BDG_PARTENAIRE_INDEX_MODULE_ACTIF);
	}

	/**
	 * @param indexPartenaireModuleActif the indexPartenaireModuleActif to set
	 */
	public void setIndexPartenaireModuleActif(Integer indexPartenaireModuleActif) {
		setValueForBinding(indexPartenaireModuleActif, BDG_PARTENAIRE_INDEX_MODULE_ACTIF);
	}
	
	/**
	 * @return the indexPartenaireModuleActif
	 */
	public Integer indexContactModuleActif() {
		return (Integer) valueForBinding(BDG_CONTACT_INDEX_MODULE_ACTIF);
	}

	/**
	 * @param indexContactModuleActif the indexContactModuleActif to set
	 */
	public void setIndexContactModuleActif(Integer indexContactModuleActif) {
		setValueForBinding(indexContactModuleActif, BDG_CONTACT_INDEX_MODULE_ACTIF);
	}
	
}
