/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.GestionContact;
import org.cocktail.fwkcktlaccordsguiajax.serveur.VersionMe;
import org.cocktail.fwkcktlpersonne.common.metier.EOIndividu;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartStructure;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.StructureElementUI;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSSelector;

public class PartenaireStructureElementUI extends StructureElementUI {
	private static final long serialVersionUID = 6801766886395709054L;
	
	public static String BINDING_utilisateurPersId = "utilisateurPersId";
	public static String BINDING_contratPartenaire = "contratPartenaire";
	
	NSMutableDictionary dicoMembres;
	private WODisplayGroup dgMembres;
	private EOIndividu unMembre;
	
	private boolean refreshMembres = false;
	
	public PartenaireStructureElementUI(WOContext context) {
        super(context);
        Class[] notificationArray = new Class[] { NSNotification.class };
        NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("refreshMembres",notificationArray), "refreshSecretairesEtMembresNotification", null);
    }

	public ContratPartenaire contratPartenaire() {
		ContratPartenaire contratPartenaire = (ContratPartenaire)valueForBinding(BINDING_contratPartenaire);
		
		return contratPartenaire;
	}
	
	public NSArray lesMembres() {
		if (getStructure() != null) {
			NSDictionary values = new NSDictionary(getStructure().cStructure(),EOIndividu.TO_REPART_STRUCTURES_KEY+"."+EORepartStructure.C_STRUCTURE_KEY);
			EOQualifier qualifier = EOKeyValueQualifier.qualifierToMatchAllValues(values);
			String personneNomPrenomAffichagePath = EORepartStructure.TO_INDIVIDU_ELTS_KEY + "." + EOIndividu.NOM_AFFICHAGE_KEY;
			EOSortOrdering nomAffichageSortOrder = EOSortOrdering.sortOrderingWithKey(EOIndividu.NOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
			EOSortOrdering prenomAffichageSortOrder = EOSortOrdering.sortOrderingWithKey(EOIndividu.PRENOM_AFFICHAGE_KEY, EOSortOrdering.CompareAscending);
			EOFetchSpecification eofs = new EOFetchSpecification(EOIndividu.ENTITY_NAME, qualifier, new NSArray(nomAffichageSortOrder,prenomAffichageSortOrder));
			NSArray lesMembres = edc().objectsWithFetchSpecification(eofs);
			return lesMembres;
		}
		return NSArray.EmptyArray;
	}
	/**
	 * @return the dgMembres
	 */
	public WODisplayGroup dgMembres() {
		
		if (dgMembres == null || refreshMembres) {
			if (dgMembres == null) {
				dgMembres = new WODisplayGroup();
			}
	        EOArrayDataSource ds = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOIndividu.class),edc());
	        ds.setArray(lesMembres());
	        dgMembres.setDataSource(ds);
	        dgMembres.setSelectsFirstObjectAfterFetch(false);
	        dgMembres.fetch();
	        refreshMembres = false;
		}
		return dgMembres;
	}

	/**
	 * @param dgMembres the dgMembres to set
	 */
	public void setDgMembres(WODisplayGroup dgMembres) {
		this.dgMembres = dgMembres;
	}

	public NSMutableDictionary dicoMembres() {
		if (dicoMembres == null) {
			NSData data = new NSData(application().resourceManager().bytesForResourceNamed("PartenaireStructureElementUIMembres.plist", VersionMe.APPLICATIONINTERNALNAME, NSArray.EmptyArray));
			dicoMembres = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoMembres;
	}

	public IPersonne selectedPersonne() {
		IPersonne personne = null;
		if (dgMembres() != null && dgMembres().selectedObject() != null) {
			// personne = ((EORepartStructure)dgMembres().selectedObject()).toPersonneElt();
			personne = (EOIndividu)dgMembres().selectedObject();
		}
		return personne;
	}
	
	public boolean isAjouterEnTantQueContactDisabled() {
		return dgMembres() == null || dgMembres().selectedObject() == null ? true:false;
	}
	
	public boolean isAjouterEnTantQueContactEnabled() {
		boolean isAjouterEnTantQueContactEnabled = !isAjouterEnTantQueContactDisabled();
		if (isAjouterEnTantQueContactEnabled) {
			IPersonne membre = (IPersonne)dgMembres().selectedObject();
			if (contratPartenaire().contactForPersId(membre.persId()) != null) {
				isAjouterEnTantQueContactEnabled = false;
			}
		}
		return isAjouterEnTantQueContactEnabled;
	}
	
	public WOActionResults ajouterEnTantQueContact() {
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setAvecModuleDeRecherche(false);
		nextPage.setContrat(contratPartenaire().contrat());
		nextPage.setPartenaire(contratPartenaire());
		nextPage.setContact(null);
        nextPage.setApplicationUser(new CocoworkApplicationUser(edc(), utilisateurPersId()));
		EOEditingContext ed = nextPage.editingContextForPersonne();
		// EORepartStructure repartStructure = (EORepartStructure)dgMembres().selectedObject();
		IPersonne personne = (IPersonne)EOUtilities.localInstanceOfObject(ed, (IPersonne)dgMembres().selectedObject());
		personne.setValidationEditingContext(ed);
		nextPage.setSelectedPersonne(personne);
		//((Session)session()).setIndexModuleActifGestionContact(null); // TODO : propager le RAZ de cet index par un binding
		
		// TODO Positionner le selectedTab du ctrl sur Contact afin d'afficher la liste des contacts
		return nextPage;
	}
	public String onClickAjouterEnTantQueContact() {
		String onClickAjouterEnTantQueContact = null;
		onClickAjouterEnTantQueContact = "openWinAjouterEnTantQueContact(this.href,'Ajouter en tant que Contact');return false;";
		
		return onClickAjouterEnTantQueContact;
	}

	public void refreshMembres(NSNotification notification) {
		if (notification.userInfo() != null && notification.userInfo().containsKey("sessionID")) {
			if (session().sessionID().equals(notification.userInfo().objectForKey("sessionID"))) {
				setDgMembres(null);
			}
		}
	}

	/**
	 * @return the unMembre
	 */
	public EOIndividu unMembre() {
		return unMembre;
	}

	/**
	 * @param unMembre the unMembre to set
	 */
	public void setUnMembre(EOIndividu unMembre) {
		this.unMembre = unMembre;
	}

	public WOActionResults refreshSelection() {
		if (dgMembres().selectedObject() != null) {
			edc().refreshObject((IPersonne)dgMembres().selectedObject());
		}
		return null;
	}

	public Integer utilisateurPersId() {
		return (Integer)valueForBinding(BINDING_utilisateurPersId);
	}

}
