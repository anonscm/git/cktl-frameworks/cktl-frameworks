/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.metier.convention.ContratPartContact;
import org.cocktail.fwkcktlpersonne.common.metier.EOAssociation;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartAssociation;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;

public class ModuleContactRolesAccords extends ModuleAssistant implements IModuleAssistant {

//	private IPersonne personne;
	private EOAssociation unRole;
	private NSArray lesRolesSelectionnes;

	private NSArray tousLesRolesContact;
	
	public static final String EC_FOR_CONTACT_BDG = "editingContextForContact"; 
	
	public ModuleContactRolesAccords(WOContext context) {
        super(context, null);
    }

	public void reset() {
		tousLesRolesContact = null;
	}
	
//	/**
//	 * @return the personne
//	 */
//	public IPersonne personne() {
//		return personne;
//	}
//
//	/**
//	 * @param personne the personne to set
//	 */
//	public void setPersonne(IPersonne personne) {
//		this.personne = personne;
//	}

	@Override
	    public EOEditingContext editingContext() {
	        return (EOEditingContext)valueForBinding(EC_FOR_CONTACT_BDG);
	    }
	
	public NSArray<EOAssociation> rolesContact() {
		if (tousLesRolesContact==null) {
			tousLesRolesContact = ContratPartContact.tousLesRolesContact(editingContext());
		}
		return tousLesRolesContact;
	}

	/**
	 * @return the unRole
	 */
	public EOAssociation unRole() {
		return unRole;
	}

	/**
	 * @param unRole the unRole to set
	 */
	public void setUnRole(EOAssociation unRole) {
		this.unRole = unRole;
	}

	/**
	 * @return the lesRolesSelectionnes
	 */
	public NSArray lesRolesSelectionnes() {
		if (lesRolesSelectionnes == null && contact() != null && personne() != null && contrat() != null && contrat().groupePartenaire() != null) {
			lesRolesSelectionnes = contact().personne().getRepartAssociationsInGroupe(contrat().groupePartenaire(), null);
			lesRolesSelectionnes = (NSArray)lesRolesSelectionnes.valueForKey(EORepartAssociation.TO_ASSOCIATION_KEY);
		}
		return lesRolesSelectionnes;
	}

	/**
	 * @param lesRolesSelectionnes the lesRolesSelectionnes to set
	 */
	public void setLesRolesSelectionnes(NSArray lesRolesSelectionnes) {
		this.lesRolesSelectionnes = lesRolesSelectionnes;
		parent().setValueForBinding(lesRolesSelectionnes, ROLES_BDG);
//		if (personne() != null && contrat() != null && contrat().groupePartenaire() != null) {
//			EOStructure groupePartenaire = (EOStructure)EOUtilities.localInstanceOfObject(personne().editingContext(), contrat().groupePartenaire());
//			personne().definitLesRoles(personne().editingContext(), lesRolesSelectionnes, groupePartenaire, contrat().utilisateurModif().toPersonne().persId(), contrat().dateDebut(), contrat().dateFin(), null, null, null);
//		}
	}
	
}
