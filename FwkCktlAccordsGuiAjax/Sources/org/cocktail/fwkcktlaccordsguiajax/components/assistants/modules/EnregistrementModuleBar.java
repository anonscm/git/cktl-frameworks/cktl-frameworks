package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.metier.convention.Contrat;
import org.cocktail.fwkcktlaccordsguiajax.components.ACktlAjaxAccordsComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOEditingContext;

public class EnregistrementModuleBar extends ACktlAjaxAccordsComponent {

    private static final long serialVersionUID = -6902590948551593240L;
    private static final String BINDING_CONTRAT = "contrat";
    private static final String BINDING_ENREGISTRER = "enregistrer";
    private static final String BINDING_ANNULER = "annuler";
    private static final String BINDING_CONSULTER = "consulter";
    private static final String BINDING_ACCUEIL = "accueil";

    public EnregistrementModuleBar(WOContext context) {
        super(context);
    }

    @Override
    public boolean synchronizesVariablesWithBindings() {
        return false;
    }
    
    public Contrat contrat() {
        return (Contrat)valueForBinding(BINDING_CONTRAT);
    }
    
    private EOEditingContext editingContext() {
        return contrat().editingContext();
    }
    
    public WOActionResults annuler() {
      if(hasBinding(BINDING_ANNULER)) {
        return (WOActionResults) valueForBinding(BINDING_ANNULER);
      } else {
        editingContext().revert();
        session().addSimpleInfoMessage("Cocolight", "Les changements ont bien été annulés");
        return null;
      }
    }
    
    public WOActionResults enregistrer() {
        return (WOActionResults) valueForBinding(BINDING_ENREGISTRER);
    }
    
    public WOActionResults accueil() {
        return (WOActionResults)valueForBinding(BINDING_ACCUEIL);
    }
    
    public WOActionResults consulter() {
        return (WOActionResults) valueForBinding(BINDING_CONSULTER);
    }
    
    
    
}