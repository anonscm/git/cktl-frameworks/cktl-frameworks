/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlaccordsguiajax.components.assistants.modules;

import org.cocktail.cocowork.server.CocoworkApplicationUser;
import org.cocktail.cocowork.server.metier.convention.ContratPartenaire;
import org.cocktail.fwkcktlaccordsguiajax.components.GestionContact;
import org.cocktail.fwkcktlaccordsguiajax.serveur.VersionMe;
import org.cocktail.fwkcktlpersonne.common.metier.EOSecretariat;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;
import org.cocktail.fwkcktlpersonneguiajax.serveur.components.GroupeAdminView;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WODisplayGroup;
import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOArrayDataSource;
import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSData;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSNotification;
import com.webobjects.foundation.NSNotificationCenter;
import com.webobjects.foundation.NSPropertyListSerialization;
import com.webobjects.foundation.NSSelector;

public class PartenaireGroupeAdminView extends GroupeAdminView {
	
	public static String BINDING_utilisateurPersId = "utilisateurPersId";
	public static String BINDING_contratPartenaire = "contratPartenaire";
	/** Facultatif. Binding pour specifier si les boutons sont actifs ou non (actifs par defaut). */
	public static final String DISABLED_BDG = "disabled";

	NSMutableDictionary dicoSecretaires;
	private WODisplayGroup dgSecretaires;
	
    public PartenaireGroupeAdminView(WOContext context) {
        super(context);
        Class[] notificationArray = new Class[] { NSNotification.class };
        NSNotificationCenter.defaultCenter().addObserver(this, new NSSelector("refreshSecretaires",notificationArray), "refreshSecretairesEtMembresNotification", null);
    }


	public ContratPartenaire contratPartenaire() {
		ContratPartenaire contratPartenaire = (ContratPartenaire)valueForBinding(BINDING_contratPartenaire);
		
		return contratPartenaire;
	}
	/**
	 * @return the dgSecretaires
	 */
	public WODisplayGroup dgSecretaires() {
		
		if (dgSecretaires == null) {
			dgSecretaires = new WODisplayGroup();
			if (groupe() != null) {
		        EOArrayDataSource ds = new EOArrayDataSource(EOClassDescription.classDescriptionForClass(EOSecretariat.class),edc());
		        ds.setArray(groupe().toSecretariats());
		        dgSecretaires.setDataSource(ds);
		        dgSecretaires.setSelectsFirstObjectAfterFetch(true);
		        dgSecretaires.fetch();
			}
		}
		return dgSecretaires;
	}

	/**
	 * @param dgSecretaires the dgSecretaires to set
	 */
	public void setDgSecretaires(WODisplayGroup dgSecretaires) {
		this.dgSecretaires = dgSecretaires;
	}

	public NSMutableDictionary dicoSecretaires() {
		if (dicoSecretaires == null) {
			NSData data = new NSData(application().resourceManager().bytesForResourceNamed("PartenaireGroupeAdminViewSecretaires.plist", VersionMe.APPLICATIONINTERNALNAME, NSArray.EmptyArray));
			dicoSecretaires = new NSMutableDictionary((NSDictionary) NSPropertyListSerialization.propertyListFromData(data, "UTF-8"));
		}
		return dicoSecretaires;
	}

	public IPersonne selectedPersonne() {
		IPersonne personne = null;
		if (dgSecretaires() != null && dgSecretaires().selectedObject() != null) {
			personne = ((EOSecretariat)dgSecretaires().selectedObject()).toIndividu();
		}
		return personne;
	}

	public boolean isAjouterEnTantQueContactDisabled() {
		return !isAjouterEnTantQueContactEnabled();
	}
	public boolean isAjouterEnTantQueContactEnabled() {
		boolean isAjouterEnTantQueContactEnabled = disabled() == false && dgSecretaires() != null && dgSecretaires().selectedObject() != null ? true:false;
		if (isAjouterEnTantQueContactEnabled) {
			EOSecretariat secretariat = (EOSecretariat)dgSecretaires().selectedObject();
			IPersonne personne = (IPersonne)secretariat.toIndividu();
			if (contratPartenaire().contactForPersId(personne.persId()) != null) {
				isAjouterEnTantQueContactEnabled = false;
			}
		}
		return isAjouterEnTantQueContactEnabled;
	}
	
	public WOActionResults ajouterEnTantQueContact() {
		GestionContact nextPage = (GestionContact)pageWithName(GestionContact.class.getName());
		nextPage.setAvecModuleDeRecherche(false);
		nextPage.setContrat(contratPartenaire().contrat());
		nextPage.setPartenaire(contratPartenaire());
        nextPage.setApplicationUser(new CocoworkApplicationUser(edc(), utilisateurPersId()));
		nextPage.setContact(null);
		EOEditingContext ed = nextPage.editingContextForPersonne();
		EOSecretariat secretariat = (EOSecretariat)dgSecretaires().selectedObject();
		IPersonne personne = (IPersonne)EOUtilities.localInstanceOfObject(ed, secretariat.toIndividu());
		personne.setValidationEditingContext(ed);
		nextPage.setSelectedPersonne(personne);
		//((Session)session()).setIndexModuleActifGestionContact(null); // TODO : propager le RAZ de cet index par un binding ?
		
		setDgSecretaires(null);
		// TODO Positionner le selectedTab du ctrl sur Contact afin d'afficher la liste des contacts
		
		return nextPage;
	}
	public String onClickAjouterEnTantQueContact() {
		String onClickAjouterEnTantQueContact = null;
		onClickAjouterEnTantQueContact = "openWinAjouterEnTantQueContact(this.href,'Ajouter en tant que Contact');return false;";
		
		return onClickAjouterEnTantQueContact;
	}

	public String elementViewContainerId() {
		return getComponentId() + "_ElementViewContainer";
	}

	public void refreshSecretaires(NSNotification notification) {
		if (notification.userInfo() != null && notification.userInfo().containsKey("sessionID")) {
			if (session().sessionID().equals(notification.userInfo().objectForKey("sessionID"))) {
				setDgSecretaires(null);
			}
		}
	}

	/**
	 * @return the disabled
	 */
	public Boolean disabled() {
		return hasBinding(DISABLED_BDG) ? (Boolean) valueForBinding(DISABLED_BDG) : Boolean.FALSE;
	}

	public WOActionResults refreshSelection() {
		if (dgSecretaires().selectedObject() != null) {
			edc().refreshObject((EOSecretariat)dgSecretaires().selectedObject());
		}
		return null;
	}
	
	public Integer utilisateurPersId() {
		return (Integer)valueForBinding(BINDING_utilisateurPersId);
	}

}
