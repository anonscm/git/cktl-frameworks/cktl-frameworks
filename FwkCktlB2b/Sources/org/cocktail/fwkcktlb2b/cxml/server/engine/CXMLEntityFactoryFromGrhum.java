/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlb2b.cxml.server.engine;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.EOPays;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Construction d'objets CXML à partir d'objets GRHUM.
 * 
 * @author rprin
 */
public class CXMLEntityFactoryFromGrhum extends CXMLEntiyFactory {
	/**
	 * @param editingContext
	 * @param postalAddressName Nom de l'adresse
	 * @param personne Personne destinataire de l'adresse
	 * @param adresse Adresse de GRHUM sur laquelle se baser pour construire l'objet.
	 * @return
	 */
	public static EOPostalAddress createEOPostalAddress(EOEditingContext editingContext, String postalAddressName, IPersonne personne, EOAdresse adresse) {
		if (adresse == null) {
			return null;
		}
		NSMutableArray streets = new NSMutableArray();
		if (!MyStringCtrl.isEmpty(adresse.adrAdresse1())) {
			streets.addObject(adresse.adrAdresse1());
		}
		if (!MyStringCtrl.isEmpty(adresse.adrAdresse2())) {
			streets.addObject(adresse.adrAdresse2());
		}
		if (!MyStringCtrl.isEmpty(adresse.adrBp())) {
			streets.addObject(adresse.adrBp().trim().toLowerCase().startsWith("bp") ? adresse.adrBp().trim() : "BP " + adresse.adrBp().trim());
		}

		return EOPostalAddress.createEOPostalAddress(editingContext, postalAddressName, new NSArray<String>(new String[] {
				personne.getNomPrenomAffichage()
				}), streets, adresse.ville(), null, adresse.codePostal() != null ? adresse.codePostal() : adresse.cpEtranger(), adresse.toPays().codeIso().toUpperCase());
	}

	public static EOPhone createEOPhone(EOEditingContext editingContext, String phoneName, EOPersonneTelephone personneTelephone) {
		if (personneTelephone == null) {
			return null;
		}
		String areaOrCityCode = null;
		String isoCountryCode = "FR";
		EOPays paysDefaut = EOPays.getPaysDefaut(editingContext);
		if (paysDefaut != null && paysDefaut.codeIso() != null) {
			isoCountryCode = paysDefaut.codeIso().toUpperCase();
		}

		EOPays pays = EOPays.fetchPaysByIndicatifTelephonique(editingContext, personneTelephone.indicatif());
		if (pays != null && pays.codeIso() != null) {
			isoCountryCode = pays.codeIso().toUpperCase();
		}

		return EOPhone.createEOPhone(editingContext, phoneName, isoCountryCode, String.valueOf(personneTelephone.indicatif()), areaOrCityCode, personneTelephone.noTelephone());
	}

	public static EOFax createEOFax(EOEditingContext editingContext, String phoneName, EOPersonneTelephone personneTelephone) {
		if (personneTelephone == null) {
			return null;
		}
		String areaOrCityCode = null;
		String isoCountryCode = "FR";
		EOPays paysDefaut = EOPays.getPaysDefaut(editingContext);
		if (paysDefaut != null && paysDefaut.codeIso() != null) {
			isoCountryCode = paysDefaut.codeIso().toUpperCase();
		}

		EOPays pays = EOPays.fetchPaysByIndicatifTelephonique(editingContext, personneTelephone.indicatif());
		if (pays != null && pays.codeIso() != null) {
			isoCountryCode = pays.codeIso().toUpperCase();
		}
		return EOFax.createEOFax(editingContext, phoneName, isoCountryCode, areaOrCityCode, personneTelephone.noTelephone());
	}

	public static EOEmail createEOEmail(EOEditingContext editingContext, String emailName, String emailAddress) {
		return EOEmail.createEOEmail(editingContext, emailName, emailAddress);
	}
}
