/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.math.BigDecimal;
import java.net.URL;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlb2b.cxml.server.VersionMe;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOAddress;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOBillTo;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemOut;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOMoney;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOOrderRequestHeader;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutSetupRequest;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipTo;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOTotal;
import org.xml.sax.InputSource;

import com.webobjects.appserver.WOApplication;
import com.webobjects.appserver.WORequest;
import com.webobjects.appserver.xml.WOXMLDecoder;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CXMLMessageFactory {
	public final static Logger logger = Logger.getLogger(CXMLMessageFactory.class);

	/** Nom du fichier de mapping pour convertir des messages cXML en entites EOF et vice-versa. */
	public static final String MAPPING_FILE_NAME = "mapping/cXMLMapping.1.1.010.xml";
	public static final String TEMPLATE_PUNCH_OUT_REQUEST_FILE_NAME = "msgTemplates/punchOutSetupRequestTemplate.1.1.010.xml";
	public static final String TEMPLATE_ORDER_REQUEST_FILE_NAME = "msgTemplates/orderRequestTemplate.1.1.010.xml";

	public final static String NC = "nc";

	private CXMLMessageFactory() {

	}

	/**
	 * Cree et renvoie un objet a partir d'un contenu xml.
	 * 
	 * @param mappingURL
	 * @param xmlContent
	 * @return
	 * @throws Exception
	 */
	public static EOCxml load(String xmlContent) throws Exception {
		InputSource is = new InputSource(new StringReader(xmlContent));
		return load(is);
	}

	/**
	 * Analyse le cXml contenu dans une request http. Le cxml doit etre contenu dans la balise "cxml-urlencoded".
	 * 
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static EOCxml load(WORequest request) throws Exception {
		if (request == null) {
			throw new Exception("Request est nul");
		}
		String xmlContent = (String) request.formValueForKey("cxml-urlencoded");
		return load(xmlContent);
	}

	public static EOCxml load(InputStreamReader inputStreamReader) throws Exception {
		InputSource is = new InputSource(inputStreamReader);
		return load(is);
	}

	//
	//	public static EOCxml load(InputStream inputStream) throws Exception {
	//		InputSource is = new InputSource(inputStream);
	//		return load(is);
	//	}

	public static EOCxml load(InputSource is) throws Exception {
		try {
			URL mappingUrl = WOApplication.application().resourceManager().pathURLForResourceNamed(MAPPING_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null);
			if (logger.isDebugEnabled()) {
				logger.debug("mapping file = " + mappingUrl.toExternalForm());
			}
			WOXMLDecoder decoder = WOXMLDecoder.decoderWithMapping(mappingUrl.getPath());
			if (logger.isDebugEnabled()) {
				logger.debug("decoder.parserClassName() = " + decoder.xmlReaderClassName());
			}
			EOCxml newObject = (EOCxml) decoder.decodeRootObject(is);
			return newObject;
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Transforme une entitee EOF EOCXml en xml.
	 */
	public static String toXml(EOCxml cxml) {
		if (cxml == null) {
			return null;
		}
		URL mappingUrl = WOApplication.application().resourceManager().pathURLForResourceNamed(MAPPING_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null);
		return toXml(cxml, mappingUrl.getPath());
	}

	public static String toXml(EOCxml cxml, String mappingFile) {
		if (cxml == null) {
			return null;
		}
		String res = "";
		CXMLCoder coder = CXMLCoder.coderWithMapping(mappingFile);
		coder.setXmlDeclaration("1.0", "UTF-8", null);
		coder.setDocTypeDeclaration("<!DOCTYPE cXML SYSTEM \"http://xml.cxml.org/schemas/cXML/1.1.010/cXML.dtd\">");
		res = coder.encodeRootObjectForKey(cxml, null);
		return res;
	}

	/**
	 * @param editingContext
	 * @param parameters
	 * @param buyerCookie
	 * @param punchOutBrowserFormPostURL
	 * @param items Tableau d'EOItemIn ou d'EOItemOut
	 * @param operation Operation a effectuer (create, edit).
	 * @return Un objet PunchOutSetupRequest
	 * @throws Exception
	 */
	public static EOCxml createPunchOutSetupRequest(EOEditingContext editingContext, CXMLParameters parameters, CXMLBuyerCookie buyerCookie, String punchOutBrowserFormPostURL, String operation, NSArray items) throws Exception {
		File f = new File(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_PUNCH_OUT_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());
		if (!f.exists()) {
			throw new Exception("Le fichier de template " + f.getPath() + " n'a pas ete trouve.");
		}
		else {
			if (logger.isDebugEnabled()) {
				logger.debug("Fichier template = " + f.getPath());
			}
		}
		if (!f.canRead()) {
			throw new Exception("Le fichier de template " + f.getPath() + " ne peut etre lu.");
		}
		FileInputStream fs = new FileInputStream(f);
		try {
			EOCxml res = createCXMLFromTemplate(editingContext, parameters, fs);
			res.request().setDeploymentMode(parameters.getDeploymentMode());
			res.request().punchOutSetupRequest().setBuyerCookie(buyerCookie.toString());
			res.request().punchOutSetupRequest().supplierSetup().url().setVal(parameters.getSupplierSetupURL());
			res.request().punchOutSetupRequest().browserFormPost().url().setVal(punchOutBrowserFormPostURL);
			res.request().punchOutSetupRequest().setOperation(operation);
			if (items != null && EOPunchOutSetupRequest.OPERATION_EDIT.equals(operation)) {
				for (int i = 0; i < items.count(); i++) {
					EOItemOut itemOut = null;
					Object item = items.objectAtIndex(i);
					if (item instanceof EOItemIn) {
						itemOut = ((EOItemIn) item).convertToEOItemOut();
					}
					else if (item instanceof EOItemOut) {
						itemOut = (EOItemOut) item;
					}
					//EOItemIn itemIn = ((EOItemIn) items.objectAtIndex(i));
					//EOItemOut itemOut = itemIn.convertToEOItemOut();
					res.request().punchOutSetupRequest().addToItemOutsRelationship(itemOut);
				}
			}
			return res;
		} catch (Exception e) {
			fs.close();
			File f1 = new File(f.getPath());
			FileInputStream fs1 = new FileInputStream(f1);
			String ligne;
			BufferedReader fichier = new BufferedReader(new InputStreamReader(fs1));
			while ((ligne = fichier.readLine()) != null) {
				System.err.println(ligne);
			}
			throw e;
		}
	}

	/**
	 * Cree un objet entete pour la commande. Utilise par
	 * {@link CXMLMessageFactory#createOrderRequest(EOEditingContext, CXMLParameters, EOAddress, NSArray, EOAddress, NSArray)}
	 * 
	 * @param editingContext
	 * @param billTo Adresse de facturation
	 * @param shipTo Adresse de livraison
	 * @param contacts Tableau d'objets EOContact
	 * @param orderDate
	 * @param orderID
	 * @param orderType
	 * @return
	 * @throws Exception
	 */
	protected static EOOrderRequestHeader createOrderRequestHeader(EOEditingContext editingContext, NSTimestamp orderDate, Long orderID, String orderType, EOAddress billTo, EOAddress shipTo, NSArray contacts) throws Exception {
		if (orderID == null) {
			throw new Exception("L'ID de la commande (orderID) est obligatoire.");
		}
		if (orderDate == null) {
			orderDate = new NSTimestamp();
		}
		if (orderType == null) {
			orderType = EOOrderRequestHeader.TYPE_NEW;
		}

		EOOrderRequestHeader orderRequestHeader = EOOrderRequestHeader.creerInstance(editingContext);
		orderRequestHeader.setOrderDate(CXMLUtilities.formatTimestamp(orderDate));
		orderRequestHeader.setOrderID(orderID);
		orderRequestHeader.setType(orderType);
		if (billTo != null) {
			orderRequestHeader.setBillToRelationship(EOBillTo.createEOBillTo(editingContext, billTo));
		}
		orderRequestHeader.setShipToRelationship(EOShipTo.createEOShipTo(editingContext, shipTo));
		for (int i = 0; contacts != null && i < contacts.count(); i++) {
			orderRequestHeader.addToContactsRelationship((EOContact) contacts.objectAtIndex(i));
		}

		return orderRequestHeader;
	}

	/**
	 * cree un message OrderRequest (pour passer un ordre de commande définitive).
	 * 
	 * @param editingContext
	 * @param parameters
	 * @param billTo Adresse de facturation
	 * @param contacts Tableau d'objets EOContact
	 * @param shipTo Adresse de livraison
	 * @param itemOuts Tableau d'objet EOItemOuts
	 * @param orderDate
	 * @param orderID
	 * @param orderType
	 * @return
	 * @throws Exception
	 */
	public static EOCxml createOrderRequest(EOEditingContext editingContext, CXMLParameters parameters, NSTimestamp orderDate, Long orderID, Long requisitionID, String orderType, EOAddress billTo, NSArray contacts, EOAddress shipTo, NSArray itemOuts) throws Exception {
		if (logger.isDebugEnabled()) {
			logger.debug(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_ORDER_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());
		}

		FileInputStream fs = new FileInputStream(WOApplication.application().resourceManager().pathURLForResourceNamed(TEMPLATE_ORDER_REQUEST_FILE_NAME, VersionMe.APPLICATIONINTERNALNAME, null).getPath());

		EOCxml res = createCXMLFromTemplate(editingContext, parameters, fs);

		res.setPayloadID(CXMLUtilities.newPayloadId(parameters.getLocalDomain()));

		EOOrderRequestHeader orh = createOrderRequestHeader(editingContext, orderDate, orderID, orderType, billTo, shipTo, contacts);
		if (requisitionID != null) {
			orh.setRequisitionID(requisitionID.toString());
		}
		orh.setTotalRelationship(EOTotal.createEOTotal(editingContext, EOMoney.creerInstance(editingContext)));
		res.request().orderRequest().setOrderRequestHeaderRelationship(orh);

		res.request().setDeploymentMode(parameters.getDeploymentMode());

		BigDecimal total = new BigDecimal(0).setScale(2);
		for (int i = 0; itemOuts != null && i < itemOuts.count(); i++) {
			EOItemOut itemOut = (EOItemOut) itemOuts.objectAtIndex(i);
			res.request().orderRequest().addToItemOutsRelationship(itemOut);
			total = total.add(itemOut.itemDetail().unitPrice().money().getVal().multiply(new BigDecimal(itemOut.quantity().intValue()).setScale(2)));
		}
		res.request().orderRequest().orderRequestHeader().total().money().setVal(total);
		return res;
	}

	/**
	 * Cree et initialise un msg cXML (avec le header rempli).
	 * 
	 * @param editingContext
	 * @param parameters
	 * @param templateInputStream
	 * @return
	 * @throws Exception
	 */
	protected static EOCxml createCXMLFromTemplate(EOEditingContext editingContext, CXMLParameters parameters, InputStream templateInputStream) throws Exception {
		InputStreamReader isr = new InputStreamReader(templateInputStream, CXMLUtilities.CHARSET_UTF_8);

		//EOCxml res = load(templateInputStream);
		EOCxml res = load(isr);
		//res.setPayloadID(parameters.getPayloadID());
		res.setPayloadID(CXMLUtilities.newPayloadId(parameters.getLocalDomain()));
		res.setTimestamp(CXMLUtilities.formatTimestamp(new NSTimestamp()));

		res.header().from().credential().setDomain(parameters.getFromCredentialDomain());
		res.header().from().credential().setIdentity(parameters.getSenderIdentity());

		res.header().to().credential().setDomain(parameters.getToCredentialDomain());
		res.header().to().credential().setIdentity(parameters.getToIdentity());

		res.header().sender().credential().setDomain(parameters.getSenderCredentialDomain());
		res.header().sender().credential().setIdentity(parameters.getSenderIdentity());
		res.header().sender().credential().setSharedSecret(parameters.getSenderSharedSecret());
		res.header().sender().setUserAgent(parameters.getSenderUserAgent());

		return res;
	}

}
