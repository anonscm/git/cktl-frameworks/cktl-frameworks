/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.engine;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CXMLParameters {
	public static String DEFAULT_XML_LANG = "fr";
	public static String DEFAULT_ISO_COUNTRY_CODE = "FR";

	private String name;
	private String SupplierSetupURL;
	//	private String fromIdentity;
	private String toIdentity;
	private String senderIdentity;
	private String senderSharedSecret;
	//private String payloadID;
	private String localDomain;
	private String fromCredentialDomain = "DUNS";
	private String toCredentialDomain = "DUNS";
	private String senderCredentialDomain = "NetworkUserId";
	private String senderUserAgent = "Europe Asset Procurement Portal";
	private String deploymentMode;

	private static final String DEFAULT_fromCredentialDomain = "DUNS";
	private static final String DEFAULT_toCredentialDomain = "DUNS";
	private static final String DEFAULT_senderCredentialDomain = "NetworkUserId";
	private static final String DEFAULT_senderUserAgent = "Europe Asset Procurement Portal";
	private static final String DEFAULT_deploymentMode = "test";

	public CXMLParameters() {
		initWithDefault();
	}

	/**
	 * @return Le nom de ces parametres.
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSupplierSetupURL() {
		return SupplierSetupURL;
	}

	public void setSupplierSetupURL(String supplierSetupURL) {
		SupplierSetupURL = supplierSetupURL;
	}

	//	public String getFromIdentity() {
	//		return fromIdentity;
	//	}
	//
	//	public void setFromIdentity(String fromIdentity) {
	//		this.fromIdentity = fromIdentity;
	//	}

	public String getSenderSharedSecret() {
		return senderSharedSecret;
	}

	public void setSenderSharedSecret(String senderSharedSecret) {
		this.senderSharedSecret = senderSharedSecret;
	}

	//	public String getPayloadID() {
	//		return payloadID;
	//	}
	//
	//	public void setPayloadID(String payloadID) {
	//		this.payloadID = payloadID;
	//	}

	public String getFromCredentialDomain() {
		return fromCredentialDomain;
	}

	public void setFromCredentialDomain(String fromCredentialDomain) {
		this.fromCredentialDomain = fromCredentialDomain;
	}

	public String getToCredentialDomain() {
		return toCredentialDomain;
	}

	public void setToCredentialDomain(String toCredentialDomain) {
		this.toCredentialDomain = toCredentialDomain;
	}

	public String getSenderCredentialDomain() {
		return senderCredentialDomain;
	}

	public void setSenderCredentialDomain(String senderCredentialDomain) {
		this.senderCredentialDomain = senderCredentialDomain;
	}

	public String getSenderUserAgent() {
		return senderUserAgent;
	}

	public void setSenderUserAgent(String senderUserAgent) {
		this.senderUserAgent = senderUserAgent;
	}

	public String getToIdentity() {
		return toIdentity;
	}

	public void setToIdentity(String toIdentity) {
		this.toIdentity = toIdentity;
	}

	public String getSenderIdentity() {
		return senderIdentity;
	}

	public void setSenderIdentity(String senderIdentity) {
		this.senderIdentity = senderIdentity;
	}

	public void initWithDefault() {
		setFromCredentialDomain(DEFAULT_fromCredentialDomain);
		setToCredentialDomain(DEFAULT_toCredentialDomain);
		setSenderCredentialDomain(DEFAULT_senderCredentialDomain);
		setSenderUserAgent(DEFAULT_senderUserAgent);
		setDeploymentMode(DEFAULT_deploymentMode);

	}

	public String getDeploymentMode() {
		return deploymentMode;
	}

	/**
	 * @param deploymentMode "test"
	 */
	public void setDeploymentMode(String deploymentMode) {
		this.deploymentMode = deploymentMode;
	}

	/**
	 * @return Le domaine reseau ou s'execute l'application si celui-ci a ete specifie. Sinon tente de le recuperer via
	 *         java.net.InetAddress.getLocalHost().getHostName().
	 */
	public String getLocalDomain() {
		String domain = localDomain;
		if (domain == null) {
			try {
				domain = java.net.InetAddress.getLocalHost().getHostName();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return domain;
	}

	/**
	 * @param localDomain Le domaine reseau ou s'execute l'application (par exemple cocktail.org, univ-lr.fr, etc.)
	 */
	public void setLocalDomain(String localDomain) {
		this.localDomain = localDomain;
	}

}
