/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlb2b.cxml.server.engine;

import com.webobjects.foundation.NSArray;

public class CXMLBuyerCookie {
	private Integer persId;
	private String sessionId;
	private Integer fbcpId;
	private Integer exeOrdre;
	private Integer ceOrdre;
	private Integer typaId;
	private Integer attOrdre;
	private Long commId;

	public CXMLBuyerCookie(Integer persId, Integer fbcpId, Integer typaId, Integer ceOrdre, Integer exeOrdre, Integer attOrdre, Long commId, String sessionId) {
		this.persId = persId;
		this.sessionId = sessionId;
		this.fbcpId = fbcpId;
		this.exeOrdre = exeOrdre;
		this.ceOrdre = ceOrdre;
		this.typaId = typaId;
		this.attOrdre = attOrdre;
		this.commId = commId;
	}

	public Integer getPersId() {
		return persId;
	}

	public void setPersId(Integer persId) {
		this.persId = persId;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	/**
	 * @return persid$fbcpId$typaId$ceOrdre$attOrdre$commId$sessionId
	 */
	public String toString() {
		return persId.toString() + "$" +
				(fbcpId == null ? "" : fbcpId.toString()) + "$" +
				(typaId == null ? "" : typaId.toString()) + "$" +
				(ceOrdre == null ? "" : ceOrdre.toString()) + "$" +
				(exeOrdre == null ? "" : exeOrdre.toString()) + "$" +
				(attOrdre == null ? "" : attOrdre.toString()) + "$" +
				(commId == null ? "" : commId.toString()) + "$" +
				sessionId;
	}

	public static CXMLBuyerCookie parse(String cookieString) throws Exception {
		String cookie = cookieString;
		if (cookie == null || cookie.trim().length() == 0) {
			throw new Exception("le cookie est vide.");
		}

		cookie = cookie.trim();
		NSArray parts = NSArray.componentsSeparatedByString(cookie, "$");
		if (parts.count() != 8) {
			throw new Exception("Erreur lors de l'analyse du cookie, il n'est pas de la forme persid$fbcpId$typaId$ceOrdre$exeOrdre$attOrdre$commId$sessionId : " + cookie);
		}

		Integer persId = null;
		Integer fbcpId = null;
		Integer typaId = null;
		Integer ceOrdre = null;
		Integer exeOrdre = null;
		Integer attOrdre = null;
		Long commId = null;

		try {
			persId = Integer.valueOf((String) parts.objectAtIndex(0));
		} catch (NumberFormatException e) {
		}
		try {
			fbcpId = Integer.valueOf((String) parts.objectAtIndex(1));
		} catch (NumberFormatException e) {
		}
		try {
			typaId = Integer.valueOf((String) parts.objectAtIndex(2));
		} catch (NumberFormatException e) {
		}
		try {
			ceOrdre = Integer.valueOf((String) parts.objectAtIndex(3));
		} catch (NumberFormatException e) {
		}
		try {
			exeOrdre = Integer.valueOf((String) parts.objectAtIndex(4));
		} catch (NumberFormatException e) {
		}
		try {
			attOrdre = Integer.valueOf((String) parts.objectAtIndex(5));
		} catch (NumberFormatException e) {
		}
		try {
			commId = Long.valueOf((String) parts.objectAtIndex(6));
		} catch (NumberFormatException e) {
		}

		String sessionId = (String) parts.objectAtIndex(7);

		CXMLBuyerCookie res = new CXMLBuyerCookie(persId, fbcpId, typaId, ceOrdre, exeOrdre, attOrdre, commId, sessionId);
		return res;
	}

	public Integer getFbcpId() {
		return fbcpId;
	}

	public void setFbcpId(Integer fbcpId) {
		this.fbcpId = fbcpId;
	}

	public Integer getExeOrdre() {
		return exeOrdre;
	}

	public void setExeOrdre(Integer exeOrdre) {
		this.exeOrdre = exeOrdre;
	}

	public Integer getCeOrdre() {
		return ceOrdre;
	}

	public void setCeOrdre(Integer ceOrdre) {
		this.ceOrdre = ceOrdre;
	}

	public Integer getTypaId() {
		return typaId;
	}

	public void setTypaId(Integer typaId) {
		this.typaId = typaId;
	}

	public Integer getAttOrdre() {
		return attOrdre;
	}

	public void setAttOrdre(Integer attOrdre) {
		this.attOrdre = attOrdre;
	}

	public Long getCommId() {
		return commId;
	}

	public void setCommId(Long commId) {
		this.commId = commId;
	}
}
