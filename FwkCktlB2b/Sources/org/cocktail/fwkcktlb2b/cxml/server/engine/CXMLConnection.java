/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.engine;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public class CXMLConnection {

	/**
	 * Resultat de l'execution de la requete.
	 */
	protected String output;

	/**
	 * Le URL d'acces au serveur.
	 */
	protected String queryURL;

	/**
	 * La liste des serveurs/domaines qui ne doivent pas etre accedes via un serveur proxy.
	 */
	private String noProxyHosts;

	/**
	 * Creer un objet qui permettra de se connecter au serveur avec l'URL <code>queryURL</code>.
	 */
	public CXMLConnection(String queryURL) {
		this.queryURL = queryURL;
		this.noProxyHosts = null;
	}

	/**
	 * Definie les domaines pour lesquels il ne faut pas utiliser le serveur proxy. La chaine peut contenir plusieurs domaines separes par "|". Par
	 * exemple, "*.foo.com|localhost".
	 */
	public void setNoProxyHosts(String noProxyHosts) {
		this.noProxyHosts = noProxyHosts;
	}

	public HttpURLConnection createConnection(URL url) throws Exception {
		URLConnection urlConn = url.openConnection();
		if (urlConn instanceof HttpURLConnection) {
			HttpURLConnection httpConn = (HttpURLConnection) urlConn;
			httpConn.setRequestMethod("POST");
		}
		else {
			return null;
		}
		urlConn.setDoInput(true);
		urlConn.setDoOutput(true);
		urlConn.setUseCaches(false);
		urlConn.setDefaultUseCaches(false);
		urlConn.setRequestProperty("Content-Type", "text/xml;charset=UTF-8");
		return (HttpURLConnection) urlConn;
	}

	/**
	 * Envoie la requette input au serveur dont l'URL etait indique a la creation de l'objet. Retourne <i>true</i> pour indiquer si la requette etait
	 * executee avec succes. Sinon une exception est declenchee.
	 * 
	 * @throws Exception
	 * @see #output()
	 */
	public boolean sendMessage(String input) throws Exception {
		String str;
		//byte[] content = input.getBytes();
		URL url;
		HttpURLConnection urlConn;
		OutputStream out;
		BufferedReader in;

		try {
			if (MyStringCtrl.isEmpty(input)) {
				throw new Exception("Le message est vide");
			}
			output = "";
			// Pour etre sur qu'on n'utilise pas le proxy
			// Sinon, la connexion peut lever une exception "IOException" ou "FileNotFound"
			if (noProxyHosts != null) {
				//System.getProperties().put("proxySet", "false");
				System.getProperties().put("http.nonProxyHosts", noProxyHosts);
				System.getProperties().put("ftp.nonProxyHosts", noProxyHosts);
			}
			// Construction de la connexion
			try {
				url = new URL(queryURL);
			} catch (Exception e) {
				throw new Exception("Erreur : l'URL " + queryURL + " n'est pas valide");
			}
			urlConn = createConnection(url);

			OutputStreamWriter wr = new OutputStreamWriter(urlConn.getOutputStream(), CXMLUtilities.CHARSET_UTF_8);
			wr.write(input);
			wr.flush();// Envoi de la requete HTTP
			wr.close();
			//			
			//			
			//			out = urlConn.getOutputStream();
			//			out.write(content);
			//			out.flush();
			//			out.close();

			// Lecture de la reponse

			InputStream resp = urlConn.getInputStream();
			if (resp != null) {
				in = new BufferedReader(new InputStreamReader(resp, CXMLUtilities.CHARSET_UTF_8));

				while ((str = in.readLine()) != null) {
					output += str;
				}
				in.close();
			}

			if (urlConn.getResponseCode() != HttpURLConnection.HTTP_OK) {
				//essayer de recuperer l'erreur :
				InputStream erreurStream = urlConn.getErrorStream();
				if (erreurStream != null) {
					BufferedReader inErr = new BufferedReader(new InputStreamReader(erreurStream, CXMLUtilities.CHARSET_UTF_8));

					while ((str = inErr.readLine()) != null) {
						output += str;
					}
					inErr.close();
				}
				throw new Exception(urlConn.getResponseCode() + " " + urlConn.getResponseMessage());
			}

			//			
			//			byte[] dataR = new byte[resp.available()];
			//			resp.read(dataR);
			//			output = new String(dataR, 0, dataR.length);
			//			resp.close();

			output = output.trim();
			if (output.length() == 0) {
				throw new Exception("Response is empty");

			}
		} catch (Exception ex) {
			ex.printStackTrace();
			throw new Exception("Erreur lors de la connexion a <" + queryURL + "> : " + ex.getMessage() + "response : <" + output + "> lors de l'envoi du message suivant\n" + input);
		}
		return true;
	}

	/**
	 * Retourne le resultat d'execution d'une requette.
	 * 
	 * @see #sendMessage(String)
	 */
	public String output() {
		return output;
	}

	//	harset charset = Charset.forName("ISO-8859-1"); CharsetDecoder decoder = charset.newDecoder(); CharsetEncoder encoder = charset.newEncoder(); try { // Convert a string to ISO-LATIN-1 bytes in a ByteBuffer // The new ByteBuffer is ready to be read. ByteBuffer bbuf = encoder.encode(CharBuffer.wrap("a string")); // Convert ISO-LATIN-1 bytes in a ByteBuffer to a character ByteBuffer and then to a string. // The new ByteBuffer is ready to be read. CharBuffer cbuf = decoder.decode(bbuf); String s = cbuf.toString(); } catch (CharacterCodingException e) { } 

	//	//	
	//	public static String SendRequest(String url, String request) {
	//			PostMethod
	//			
	//			HttpWebRequest req = null;
	//			HttpWebResponse resp = null;
	//			Stream reqStream = null;
	//	
	//			try {
	//				UTF8Encoding enc = new UTF8Encoding();
	//				byte[] data = enc.GetBytes(request);
	//	
	//				req = (HttpWebRequest) WebRequest.Create(url);
	//				req.Method = "POST";
	//				req.ContentLength = data.Length;
	//				req.ContentType = "text/xml";
	//				req.Proxy = System.Net.WebProxy.GetDefaultProxy();
	//				req.AllowAutoRedirect = true;
	//	
	//				// this didn't seem to make any difference
	//				req.ProtocolVersion = HttpVersion.Version10;
	//				req.KeepAlive = false;
	//				req.AllowWriteStreamBuffering = true;
	//	
	//				// tried setting these different timeouts with no real improvement
	//				//req.Timeout				= 60000;
	//				req.ReadWriteTimeout = 60000;
	//				req.ServicePoint.MaxIdleTime = 100000;
	//	
	//				reqStream = req.GetRequestStream();
	//				reqStream.Write(data, 0, data.Length);
	//	
	//				resp = (HttpWebResponse) req.GetResponse();
	//				if (req.HaveResponse) {
	//					if (resp.StatusCode == HttpStatusCode.OK || resp.StatusCode == HttpStatusCode.Accepted) {
	//						StreamReader reader = new StreamReader(resp.GetResponseStream());
	//						return reader.ReadToEnd();
	//					}
	//					else {
	//						throw new Exception("Request failed: " + resp.StatusDescription);
	//					}
	//	
	//				}
	//				return null;
	//			} finally {
	//				if (req != null) {
	//					req = null;
	//				}
	//				if (resp != null) {
	//					resp.Close();
	//				}
	//				if (reqStream != null) {
	//					reqStream.Close();
	//				}
	//			}
	//		}
}
