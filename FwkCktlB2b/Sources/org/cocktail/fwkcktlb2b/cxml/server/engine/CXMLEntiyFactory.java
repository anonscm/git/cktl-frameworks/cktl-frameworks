/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlb2b.cxml.server.engine;

import java.util.Enumeration;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOAddress;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.foundation.NSArray;

/**
 * Permet de creer des objets a inserer dans les messages cXML. Les methodes originales sont dans les objets metiers.
 * 
 * @author rprin
 */
public abstract class CXMLEntiyFactory {

	public static EOAddress createEOAddress(EOEditingContext editingContext, String addressID, String addressName, String addressNameLang, String isoCountryCode) {
		return EOAddress.createEOAddress(editingContext, addressID, addressName, addressNameLang, isoCountryCode);
	}

	public static EOAddress createEOAddress(EOEditingContext editingContext, String addressID, String addressName, String addressNameLang, String isoCountryCode, EOPostalAddress postalAddress, EOPhone phone, EOFax fax, EOEmail email, EOUrl url) {
		EOAddress address = EOAddress.createEOAddress(editingContext, addressID, addressName, addressNameLang, isoCountryCode);
		address.setPostalAddressRelationship(postalAddress);
		address.setPhoneRelationship(phone);
		address.setEmailRelationship(email);
		address.setFaxRelationship(fax);
		address.setUrlRelationship(url);
		return address;
	}

	/**
	 * Cree un objet EOPostalAddress destine a etre affecte a un objet EOAdress.
	 * 
	 * @param editingContext
	 * @param postalAddressName Permet d'identifier l'adresse postale (ex. "travail")
	 * @param lastName
	 * @param firstName
	 * @param streets Tableau de String contenant les differentes lignes de l'adresse postale.
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param isoCountryCode
	 * @return Un nouvel objet representant une adresse postale.
	 */
	public static EOPostalAddress createEOPostalAddress(EOEditingContext editingContext, String postalAddressName, NSArray<String> deliverToNames, NSArray<String> streets, String city, String state, String postalCode, String isoCountryCode) {
		return EOPostalAddress.createEOPostalAddress(editingContext, postalAddressName, deliverToNames, streets, city, state, postalCode, isoCountryCode);
	}

	/**
	 * @param editingContext
	 * @param phoneName (identifie le telephone)
	 * @param isoCountryCode (par ex FR)
	 * @param internationDialCode (par ex. 33)
	 * @param areaOrCityCode
	 * @param number
	 * @return
	 */
	public static EOPhone createEOPhone(EOEditingContext editingContext, String phoneName, String isoCountryCode, String internationDialCode, String areaOrCityCode, String number) {
		return EOPhone.createEOPhone(editingContext, phoneName, isoCountryCode, internationDialCode, areaOrCityCode, number);
	}

	public static EOFax createEOFax(EOEditingContext editingContext, String faxName, String isoCountryCode, String areaOrCityCode, String number) {
		return EOFax.createEOFax(editingContext, faxName, isoCountryCode, areaOrCityCode, number);
	}

	public static EOEmail createEOEmail(EOEditingContext editingContext, String emailName, String emailAddress) {
		return EOEmail.createEOEmail(editingContext, emailName, emailAddress);
	}

	public static EOContact createEOContact(EOEditingContext editingContext, String contactName, String contactRole, NSArray emails, NSArray phones, NSArray faxes) {
		return EOContact.createEOContact(editingContext, contactName, contactRole, emails, phones, faxes);
	}

	/**
	 * Effectue une validation des objets modfies et inseres de l'editingContext.
	 * 
	 * @param editingContext
	 * @throws Exception
	 */
	public static void validateAllObjectsInEc(EOEditingContext editingContext) throws Exception {
		NSArray<EOEnterpriseObject> updatedObjects = editingContext.updatedObjects();
		Enumeration<EOEnterpriseObject> en = updatedObjects.objectEnumerator();
		while (en.hasMoreElements()) {
			EOEnterpriseObject eoEnterpriseObject = (EOEnterpriseObject) en.nextElement();
			eoEnterpriseObject.validateForUpdate();
		}

		NSArray<EOEnterpriseObject> insertedObjects = editingContext.insertedObjects();
		Enumeration<EOEnterpriseObject> inserted = insertedObjects.objectEnumerator();
		while (inserted.hasMoreElements()) {
			EOEnterpriseObject eoEnterpriseObject = (EOEnterpriseObject) inserted.nextElement();
			eoEnterpriseObject.validateForInsert();
		}

	}

}
