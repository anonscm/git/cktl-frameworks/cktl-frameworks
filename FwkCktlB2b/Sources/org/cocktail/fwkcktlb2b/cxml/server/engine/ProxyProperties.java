/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlb2b.cxml.server.engine;

import java.util.Properties;

public class ProxyProperties extends Properties {
	public static final String HTTP_NON_PROXY_HOSTS = "http.nonProxyHosts";
	public static final String HTTP_PROXY_HOST = "http.proxyHost";
	public static final String HTTP_PROXY_PORT = "http.proxyPort";
	public static final String HTTPS_PROXY_HOST = "https.proxyHost";
	public static final String HTTPS_PROXY_PORT = "https.proxyPort";

	private String httpProxyHost;
	private String httpProxyPort;
	private String httpsProxyHost;
	private String httpsProxyPort;

	private String nonProxyHosts;

	private boolean sameProxy = false;

	public ProxyProperties() {

	}

	public String getHttpProxyHost() {
		return httpProxyHost;
	}

	public void setHttpProxyHost(String httpProxyHost) {
		this.httpProxyHost = httpProxyHost;
	}

	public String getHttpProxyPort() {
		return httpProxyPort;
	}

	public void setHttpProxyPort(String httpProxyPort) {
		this.httpProxyPort = httpProxyPort;
	}

	public String getHttpsProxyHost() {
		return httpsProxyHost;
	}

	public void setHttpsProxyHost(String httpsProxyHost) {
		this.httpsProxyHost = httpsProxyHost;
	}

	public String getHttpsProxyPort() {
		return httpsProxyPort;
	}

	public void setHttpsProxyPort(String httpsProxyPort) {
		this.httpsProxyPort = httpsProxyPort;
	}

	public String getNonProxyHosts() {
		return nonProxyHosts;
	}

	public void setNonProxyHosts(String nonProxyHosts) {
		this.nonProxyHosts = nonProxyHosts;
	}

	private void synchroniseProxies() {
		if (isSameProxy()) {
			setHttpsProxyHost(getHttpProxyHost());
			setHttpsProxyPort(getHttpProxyPort());
		}
	}

	public void applyToSystem() {
		System.setProperty(HTTP_PROXY_HOST, getHttpProxyHost());
		System.setProperty(HTTP_PROXY_PORT, getHttpProxyPort());
		System.setProperty(HTTPS_PROXY_HOST, getHttpsProxyHost());
		System.setProperty(HTTPS_PROXY_PORT, getHttpsProxyPort());
		System.setProperty(HTTP_NON_PROXY_HOSTS, getNonProxyHosts());
		// if (getHttpProxyHost() != null) {
		// System.setProperty(HTTP_PROXY_HOST, getHttpProxyHost());
		// }
		// if (getHttpProxyPort() != null) {
		// System.setProperty(HTTP_PROXY_PORT, getHttpProxyPort());
		// }
		// if (getHttpsProxyHost() != null) {
		// System.setProperty(HTTPS_PROXY_HOST, getHttpsProxyHost());
		// }
		// if (getHttpsProxyPort() != null) {
		// System.setProperty(HTTPS_PROXY_PORT, getHttpsProxyPort());
		// }
		//		
		// if (getNonProxyHosts() != null) {
		// System.setProperty(, getNonProxyHosts());
		// }

	}

	public void initFromSystem() {
		setHttpProxyHost(System.getProperty(HTTP_PROXY_HOST));
		setHttpProxyPort(System.getProperty(HTTP_PROXY_PORT));
		setHttpsProxyHost(System.getProperty(HTTPS_PROXY_HOST));
		setHttpsProxyPort(System.getProperty(HTTPS_PROXY_PORT));
		setNonProxyHosts(System.getProperty(HTTP_NON_PROXY_HOSTS));
	}

	public boolean isSameProxy() {
		return sameProxy;
	}

	public void setSameProxy(boolean sameProxy) {
		this.sameProxy = sameProxy;
	}

}
