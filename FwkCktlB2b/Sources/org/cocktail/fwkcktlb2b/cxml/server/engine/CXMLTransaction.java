/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.engine;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOAddress;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOCxml;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOOrderRequestHeader;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutSetupRequest;
import org.cocktail.fwkcktldroitsutils.common.ApplicationUser;
import org.cocktail.fwkcktlpersonne.common.metier.EOPersonneTelephone;
import org.cocktail.fwkcktlpersonne.common.metier.EORepartPersonneAdresse;
import org.cocktail.fwkcktlpersonne.common.metier.IPersonne;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;

public class CXMLTransaction {
	public final static Logger logger = Logger.getLogger(CXMLTransaction.class);

	public CXMLTransaction() {
	}

	/**
	 * Envoie le msg orderRequest deja construit.
	 * 
	 * @param orderRequestCxml Le message CXML du type OrderRequest.
	 * @param parameters
	 * @param proxyProperties
	 * @return la reponse cxml brute.
	 * @throws Exception
	 */
	public String creerTransactionOrderRequest(EOCxml orderRequestCxml, CXMLParameters parameters, ProxyProperties proxyProperties) throws Exception {
		if (parameters.getSupplierSetupURL() == null) {
			throw new Exception("URL SupplierSetup nulle. Probleme dans les parametres.");
		}
		if (orderRequestCxml == null) {
			throw new Exception("Message CXML nul (orderRequestCxml).");
		}
		if (!orderRequestCxml.isOrderRequest()) {
			throw new Exception("Message CXML n'est pas un OrderRequest (orderRequestCxml).");
		}

		String cxmlMsg = CXMLMessageFactory.toXml(orderRequestCxml);
		if (logger.isDebugEnabled()) {
			logger.debug("");
			logger.debug("XML Request:");
			logger.debug(cxmlMsg);
			logger.debug("-------------------------------------------------");
			logger.debug("");
		}

		String xmlResponseUnparsed = null;

		CXMLConnection cxmlConnection = new CXMLConnection(parameters.getSupplierSetupURL());
		cxmlConnection.sendMessage(cxmlMsg);
		xmlResponseUnparsed = cxmlConnection.output();
		if (logger.isDebugEnabled()) {
			logger.debug("");
			logger.debug("");
			logger.debug("XML Response:");
			logger.debug(xmlResponseUnparsed);
			logger.debug("-------------------------------------------------");
			logger.debug("");

		}

		return xmlResponseUnparsed;

	}

	/**
	 * @param xmlResponseUnparsed
	 * @return l'orderID si tout se passe bien.
	 * @throws Exception
	 */
	public String parseOrderRequestResponse(String xmlResponseUnparsed) throws Exception {
		//Analyse de la reponse
		EOCxml responseCxml = CXMLMessageFactory.load(xmlResponseUnparsed);

		if (responseCxml.response().status().code() == null) {
			throw new Exception("Le code de la reponse est nul.");
		}
		if (responseCxml.response().status().code().intValue() != 200) {
			throw new Exception("Erreur (code = " + responseCxml.response().status().code().intValue() + ") " + responseCxml.response().status().text());
		}

		if (responseCxml.response().status().code().intValue() == 200) {
			String orderID = null;

			String text = responseCxml.response().status().text();

			if (text != null) {
				NSArray<String> res = NSArray.componentsSeparatedByString(text, "=");
				if (res.count() >= 2 && "PO".equals(res.objectAtIndex(0).trim())) {
					orderID = res.objectAtIndex(1).trim();
				}
			}

			if (orderID == null) {
				throw new Exception("Reponse ok mais il manque la reference de la commande.(code = " + responseCxml.response().status().code().intValue() + ") " + responseCxml.response().status().text());
			}
			return orderID;
		}
		return null;

	}

	/**
	 * Construit et envoie un msg orderRequest.
	 * 
	 * @param ec
	 * @param appUser
	 * @param cxmlParameters
	 * @param orderID
	 * @param itemOuts
	 * @param shipToPersonne
	 * @param shipToRepartPersonneAdresse
	 * @param shipToEmail
	 * @param shipToTelephone
	 * @param shipToFax
	 * @param billToPersonne
	 * @param billToRepartPersonneAdresse
	 * @param billToEmail
	 * @param billToTelephone
	 * @param billToFax
	 * @param proxyProperties
	 * @return l'orderID si transaction ok.
	 * @throws Exception
	 */
	public String creerTransactionOrderRequest(EOEditingContext ec, ApplicationUser appUser, CXMLParameters cxmlParameters, Long orderID, Long requisitionID, NSArray itemOuts, IPersonne shipToPersonne, EORepartPersonneAdresse shipToRepartPersonneAdresse,
			String shipToEmail, EOPersonneTelephone shipToTelephone, EOPersonneTelephone shipToFax, IPersonne billToPersonne, EORepartPersonneAdresse billToRepartPersonneAdresse, String billToEmail, EOPersonneTelephone billToTelephone, EOPersonneTelephone billToFax,
			IPersonne contactPersonne, EORepartPersonneAdresse contactRepartPersonneAdresse, String contactEmail, EOPersonneTelephone contactTelephone, EOPersonneTelephone contactFax, ProxyProperties proxyProperties)
			throws Exception {
		EOCxml cxmlOrderRequest = creerCxmlOrderRequestForTransaction(ec, appUser, cxmlParameters, orderID, requisitionID, itemOuts, shipToPersonne, shipToRepartPersonneAdresse, shipToEmail, shipToTelephone, shipToFax, billToPersonne, billToRepartPersonneAdresse, billToEmail, billToTelephone,
				billToFax,
				contactPersonne, contactRepartPersonneAdresse, contactEmail, contactTelephone, contactFax,
				proxyProperties);

		CXMLTransaction transaction = new CXMLTransaction();
		String res = transaction.creerTransactionOrderRequest(cxmlOrderRequest, cxmlParameters, proxyProperties);
		return res;

	}

	/**
	 * Construit un msg orderRequest sans l'envoyer.
	 * 
	 * @param ec
	 * @param appUser
	 * @param cxmlParameters
	 * @param orderID
	 * @param itemOuts
	 * @param shipToPersonne
	 * @param shipToRepartPersonneAdresse
	 * @param shipToEmail
	 * @param shipToTelephone
	 * @param shipToFax
	 * @param billToPersonne
	 * @param billToRepartPersonneAdresse
	 * @param billToEmail
	 * @param billToTelephone
	 * @param billToFax
	 * @param proxyProperties
	 * @return
	 * @throws Exception
	 */
	public EOCxml creerCxmlOrderRequestForTransaction(EOEditingContext ec, ApplicationUser appUser, CXMLParameters cxmlParameters, Long orderID, Long requisitionID, NSArray itemOuts, IPersonne shipToPersonne, EORepartPersonneAdresse shipToRepartPersonneAdresse,
			String shipToEmail, EOPersonneTelephone shipToTelephone, EOPersonneTelephone shipToFax, IPersonne billToPersonne, EORepartPersonneAdresse billToRepartPersonneAdresse, String billToEmail, EOPersonneTelephone billToTelephone, EOPersonneTelephone billToFax,
			IPersonne contactPersonne, EORepartPersonneAdresse contactRepartPersonneAdresse, String contactEmail, EOPersonneTelephone contactTelephone, EOPersonneTelephone contactFax, ProxyProperties proxyProperties)
			throws Exception {
		if (orderID == null) {
			throw new Exception("Le numero de commande est obligatoire");
		}
		if (itemOuts == null || itemOuts.count() == 0) {
			throw new Exception("Aucun article a commander.");
		}
		if (shipToPersonne == null) {
			throw new Exception("Veuillez selectionner un service pour la livraison");
		}
		if (shipToRepartPersonneAdresse == null) {
			throw new Exception("Veuillez selectionner une adresse de livraison");
		}
		if (billToPersonne == null) {
			throw new Exception("Veuillez selectionner un service pour la facturation");
		}
		if (billToRepartPersonneAdresse == null) {
			throw new Exception("Veuillez selectionner une adresse de facturation");
		}

		//Construire le cXML
		String iso = "FR";
		if (shipToRepartPersonneAdresse.toAdresse() != null && shipToRepartPersonneAdresse.toAdresse().toPays() != null) {
			iso = shipToRepartPersonneAdresse.toAdresse().toPays().codeIso().toUpperCase();
		}

		EOPostalAddress postalAdressShipTo = CXMLEntityFactoryFromGrhum.createEOPostalAddress(ec, shipToRepartPersonneAdresse.toTypeAdresse().tadrLibelle(), shipToPersonne, shipToRepartPersonneAdresse.toAdresse());
		EOEmail emailShipTo = CXMLEntityFactoryFromGrhum.createEOEmail(ec, "", shipToEmail);
		EOPhone phoneShipTo = CXMLEntityFactoryFromGrhum.createEOPhone(ec, "", shipToTelephone);
		EOFax faxShipTo = CXMLEntityFactoryFromGrhum.createEOFax(ec, "", shipToFax);
		//		EOAddress addressShipTo = CXMLEntityFactoryFromGrhum.createEOAddress(ec, "",    "Livraison", "fr", iso, postalAdressShipTo, phoneShipTo, faxShipTo, emailShipTo, null);
		//		EOAddress addressShipTo = CXMLEntityFactoryFromGrhum.createEOAddress(ec, "", shipToPersonne.getNomPrenomAffichage(), "fr", iso, postalAdressShipTo, phoneShipTo, faxShipTo, emailShipTo, null);
		String shipToName = shipToPersonne.getNomPrenomAffichage();
		//		if (contactPersonne != null) {
		//			shipToName = contactPersonne.getNomPrenomAffichage();
		//		}
		EOAddress addressShipTo = CXMLEntityFactoryFromGrhum.createEOAddress(ec, "", shipToName, "fr", iso, postalAdressShipTo, phoneShipTo, faxShipTo, emailShipTo, null);

		EOPostalAddress postalAdressBillTo = CXMLEntityFactoryFromGrhum.createEOPostalAddress(ec, billToRepartPersonneAdresse.toTypeAdresse().tadrLibelle(), billToPersonne, billToRepartPersonneAdresse.toAdresse());
		EOEmail emailBillTo = CXMLEntityFactoryFromGrhum.createEOEmail(ec, "", billToEmail);
		EOPhone phoneBillTo = CXMLEntityFactoryFromGrhum.createEOPhone(ec, "", billToTelephone);
		EOFax faxBillTo = CXMLEntityFactoryFromGrhum.createEOFax(ec, "", billToFax);
		//EOAddress addressBillTo = CXMLEntityFactoryFromGrhum.createEOAddress(ec, "", "Facturation", "fr", iso, postalAdressBillTo, phoneBillTo, faxBillTo, emailBillTo, null);
		EOAddress addressBillTo = CXMLEntityFactoryFromGrhum.createEOAddress(ec, "", billToPersonne.getNomPrenomAffichage(), "fr", iso, postalAdressBillTo, phoneBillTo, faxBillTo, emailBillTo, null);

		EOEmail emailContact = CXMLEntityFactoryFromGrhum.createEOEmail(ec, "", contactEmail);
		EOPhone phoneContact = CXMLEntityFactoryFromGrhum.createEOPhone(ec, "", contactTelephone);
		EOFax faxContact = CXMLEntityFactoryFromGrhum.createEOFax(ec, "", contactFax);
		EOContact contact = EOContact.createEOContact(ec, contactPersonne.getNomPrenomAffichage(), "", new NSArray(new Object[] {
				emailContact
		}), new NSArray(new Object[] {
				phoneContact
		}), new NSArray(new Object[] {
				faxContact
		}));

		EOCxml cxmlOrderRequest = CXMLMessageFactory.createOrderRequest(ec, cxmlParameters, new NSTimestamp(), orderID, requisitionID, EOOrderRequestHeader.TYPE_NEW, addressBillTo, new NSArray(new Object[] {
				contact
		}), addressShipTo, itemOuts);
		cxmlOrderRequest.request().orderRequest().orderRequestHeader().setExtrensic("User", appUser.getLogin());

		//CXMLEntiyFactory.validateAllObjectsInEc(edc());

		ec.saveChanges();
		if (logger.isDebugEnabled()) {
			logger.debug(CXMLMessageFactory.toXml(cxmlOrderRequest));
		}

		return cxmlOrderRequest;
	}

	public String creerTransactionPunchOutSetupRequestCreate(EOEditingContext ec, ApplicationUser appUser, CXMLParameters cxmlParameters, CXMLBuyerCookie buyerCookie, String punchOutBrowserFormPostURL) throws Exception {
		return creerTransactionPunchOutSetupRequest(ec, appUser, cxmlParameters, buyerCookie, punchOutBrowserFormPostURL, EOPunchOutSetupRequest.OPERATION_CREATE, null);
	}

	public String creerTransactionPunchOutSetupRequestEdit(EOEditingContext ec, ApplicationUser appUser, CXMLParameters cxmlParameters, CXMLBuyerCookie buyerCookie, String punchOutBrowserFormPostURL, NSArray items) throws Exception {
		return creerTransactionPunchOutSetupRequest(ec, appUser, cxmlParameters, buyerCookie, punchOutBrowserFormPostURL, EOPunchOutSetupRequest.OPERATION_EDIT, items);
	}

	/**
	 * <pre>
	 * 
	 * 
	 * 
	 * </pre>
	 * 
	 * @param ec
	 * @param appUser
	 * @param cxmlParameters
	 * @param buyerCookie
	 * @param domaine
	 * @param payloadID
	 * @param punchOutBrowserFormPostURL
	 * @param operation cf.
	 * @param items tableau d'EOItemIn ou EOItemOut
	 * @return l'URL de connexion au systeme distant c(catalogue)
	 * @throws Exception
	 */
	public String creerTransactionPunchOutSetupRequest(EOEditingContext ec, ApplicationUser appUser, CXMLParameters cxmlParameters, CXMLBuyerCookie buyerCookie, String punchOutBrowserFormPostURL, String operation, NSArray items) throws Exception {
		EOCxml poreq = CXMLMessageFactory.createPunchOutSetupRequest(ec, cxmlParameters, buyerCookie, punchOutBrowserFormPostURL, operation, items);
		poreq.request().punchOutSetupRequest().setExtrensic("User", appUser.getLogin());
		//			poreq.request().punchOutSetupRequest().setExtrensic("User", ((Session)session()).connectedUserInfo().login() );

		String cxmlMsg = CXMLMessageFactory.toXml(poreq);
		if (logger.isDebugEnabled()) {
			logger.debug("");
			logger.debug("");
			logger.debug("XML Request:");
			logger.debug(cxmlMsg);
			logger.debug("-------------------------------------------------");
			logger.debug("");
		}

		String xmlResponseUnparsed = null;
		if (cxmlParameters.getSupplierSetupURL() == null) {
			throw new Exception("URL SupplierSetup non specifiee.");
		}
		CXMLConnection cxmlConnection = new CXMLConnection(cxmlParameters.getSupplierSetupURL());
		cxmlConnection.sendMessage(cxmlMsg);
		xmlResponseUnparsed = cxmlConnection.output();
		if (logger.isDebugEnabled()) {
			logger.debug("");
			logger.debug("");
			logger.debug("XML Response:");
			logger.debug(xmlResponseUnparsed);
			logger.debug("-------------------------------------------------");
			logger.debug("");
		}

		//Analyse de la reponse
		EOCxml punchOutSetupResponse = CXMLMessageFactory.load(xmlResponseUnparsed);

		try {

			if (punchOutSetupResponse.response().status().code() == null) {
				throw new Exception("Le code de la reponse est nul.");
			}
			if (punchOutSetupResponse.response().status().code().intValue() != 200) {
				throw new Exception("Erreur (code = " + punchOutSetupResponse.response().status().code().intValue() + ") " + punchOutSetupResponse.response().status().text());
			}
			else {
				String url = punchOutSetupResponse.response().punchOutSetupResponse().startPage().url().getVal();
				if (url == null) {
					throw new Exception("StartPage url est nul.");
				}
				return url;
			}

		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception(e.getMessage() + "\n" + (xmlResponseUnparsed != null ? "\n\n" + xmlResponseUnparsed : ""));

		}

	}
}
