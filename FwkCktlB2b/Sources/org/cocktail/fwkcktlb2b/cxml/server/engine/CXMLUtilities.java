/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlb2b.cxml.server.engine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Random;

import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn;
import org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemOut;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;

public class CXMLUtilities {
	public static Charset CHARSET_UTF_8 = Charset.forName("UTF-8");

	public static Random rand = new Random();

	public static final String FORMAT_TIMESTAMP1 = "MM/dd/yyyy'T'HH:mm:ss";
	public static final String FORMAT_TIMESTAMP2 = "yyyy-MM-dd'T'hh:mm:ss";

	public static String loadTextFile(final File file) throws Exception {
		String res = "";

		FileInputStream fis = new FileInputStream(file);
		InputStreamReader in = new InputStreamReader(fis, CHARSET_UTF_8);

		BufferedReader bufferedReader = null;
		StringWriter sw = new StringWriter();
		try {
			bufferedReader = new BufferedReader(in);
			String l;
			while ((l = bufferedReader.readLine()) != null) {
				sw.write(l + "\n");
				sw.flush();
			}
			res = sw.toString();

		} catch (Exception e) {
			e.printStackTrace();
		}

		finally {
			if (bufferedReader != null) {
				bufferedReader.close();
			}
		}
		return res;
	}

	public static NSArray convertItemInsToItemOuts(NSArray itemIns) {
		NSMutableArray res = new NSMutableArray();
		for (int i = 0; i < itemIns.count(); i++) {
			EOItemIn itemIn = ((EOItemIn) itemIns.objectAtIndex(i));
			EOItemOut itemOut = itemIn.convertToEOItemOut();
			res.addObject(itemOut);
		}
		return res.immutableClone();
	}

	public static String formatTimestamp(NSTimestamp timestamp) {
		String res = null;
		if (timestamp != null) {
			res = new SimpleDateFormat(FORMAT_TIMESTAMP2).format(timestamp) + new Integer(java.util.TimeZone.getDefault().getRawOffset() / 3600000).toString() + ":00";
		}
		return res;
	}

	/**
	 * @param domain
	 * @return
	 */
	public static String newPayloadId(String domain) {
		try {
			return new String(new SimpleDateFormat("yyyyMMddhhmmssmm").format(new Date()) + "." + new Long(rand.nextInt()).toString() + "@" + domain);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return new String("");
	}

	//
	//
	//	public static String newcXMLTimeStamp() {
	//		return new SimpleDateFormat(FORMAT_TIMESTAMP2).format(new Date()) + new Integer(java.util.TimeZone.getDefault().getRawOffset() / 3600000).toString() + ":00";
	//	}

	public static void printJREProperties() {
		Properties p = System.getProperties();
		Enumeration keys = p.keys();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			String value = (String) p.get(key);
			System.out.println(key + ": " + value);
		}
	}

}
