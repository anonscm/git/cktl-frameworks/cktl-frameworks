/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOItemOut.java instead.
package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlb2b.cxml.server.engine.ICXMLCodingAdditions;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public abstract class _EOItemOut extends  _EOAbstract implements ICXMLCodingAdditions {
	public static final String ENTITY_NAME = "ItemOut";
	public static final String ENTITY_TABLE_NAME = "ItemOut";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String LINE_NUMBER_KEY = "lineNumber";
	public static final String QUANTITY_KEY = "quantity";
	public static final String REQUESTED_DELIVERY_DATE_KEY = "requestedDeliveryDate";
	public static final String REQUISITION_ID_KEY = "requisitionID";

// Attributs non visibles
	public static final String ID_KEY = "id";
	public static final String ITEM_IDID_KEY = "itemIDID";
	public static final String COMMENTS_ID_KEY = "commentsID";
	public static final String SHIP_TO_ID_KEY = "shipToID";
	public static final String SUPPLIER_IDID_KEY = "supplierIDID";
	public static final String TAX_ID_KEY = "taxID";
	public static final String ITEM_DETAIL_ID_KEY = "itemDetailID";
	public static final String SHIPPING_ID_KEY = "shippingID";

//Colonnes dans la base de donnees
	public static final String LINE_NUMBER_COLKEY = "lineNumber";
	public static final String QUANTITY_COLKEY = "quantity";
	public static final String REQUESTED_DELIVERY_DATE_COLKEY = "requestedDeliveryDate";
	public static final String REQUISITION_ID_COLKEY = "requisitionID";

	public static final String ID_COLKEY = "id";
	public static final String ITEM_IDID_COLKEY = "itemIDID";
	public static final String COMMENTS_ID_COLKEY = "commentsID";
	public static final String SHIP_TO_ID_COLKEY = "shipToID";
	public static final String SUPPLIER_IDID_COLKEY = "supplierIDID";
	public static final String TAX_ID_COLKEY = "taxID";
	public static final String ITEM_DETAIL_ID_COLKEY = "itemDetailID";
	public static final String SHIPPING_ID_COLKEY = "shippingID";


	// Relationships
	public static final String COMMENTS_KEY = "comments";
	public static final String CONTACTS_KEY = "contacts";
	public static final String DISTRIBUTIONS_KEY = "distributions";
	public static final String ITEM_DETAIL_KEY = "itemDetail";
	public static final String ITEM_ID_KEY = "itemID";
	public static final String SHIPPING_KEY = "shipping";
	public static final String SHIP_TO_KEY = "shipTo";
	public static final String SUPPLIER_ID_KEY = "supplierID";
	public static final String TAX_KEY = "tax";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public Integer lineNumber() {
    return (Integer) storedValueForKey(LINE_NUMBER_KEY);
  }

  public void setLineNumber(Integer value) {
    takeStoredValueForKey(value, LINE_NUMBER_KEY);
  }

  public Integer quantity() {
    return (Integer) storedValueForKey(QUANTITY_KEY);
  }

  public void setQuantity(Integer value) {
    takeStoredValueForKey(value, QUANTITY_KEY);
  }

  public String requestedDeliveryDate() {
    return (String) storedValueForKey(REQUESTED_DELIVERY_DATE_KEY);
  }

  public void setRequestedDeliveryDate(String value) {
    takeStoredValueForKey(value, REQUESTED_DELIVERY_DATE_KEY);
  }

  public Integer requisitionID() {
    return (Integer) storedValueForKey(REQUISITION_ID_KEY);
  }

  public void setRequisitionID(Integer value) {
    takeStoredValueForKey(value, REQUISITION_ID_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOComments comments() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOComments)storedValueForKey(COMMENTS_KEY);
  }

  public void setCommentsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOComments value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOComments oldValue = comments();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, COMMENTS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, COMMENTS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemDetail itemDetail() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemDetail)storedValueForKey(ITEM_DETAIL_KEY);
  }

  public void setItemDetailRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemDetail value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemDetail oldValue = itemDetail();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ITEM_DETAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ITEM_DETAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemID itemID() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemID)storedValueForKey(ITEM_ID_KEY);
  }

  public void setItemIDRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemID value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemID oldValue = itemID();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ITEM_ID_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ITEM_ID_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipping shipping() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipping)storedValueForKey(SHIPPING_KEY);
  }

  public void setShippingRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipping value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipping oldValue = shipping();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SHIPPING_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SHIPPING_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipTo shipTo() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipTo)storedValueForKey(SHIP_TO_KEY);
  }

  public void setShipToRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipTo value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOShipTo oldValue = shipTo();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SHIP_TO_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SHIP_TO_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOSupplierID supplierID() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOSupplierID)storedValueForKey(SUPPLIER_ID_KEY);
  }

  public void setSupplierIDRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOSupplierID value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOSupplierID oldValue = supplierID();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, SUPPLIER_ID_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, SUPPLIER_ID_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOTax tax() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOTax)storedValueForKey(TAX_KEY);
  }

  public void setTaxRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOTax value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOTax oldValue = tax();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TAX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TAX_KEY);
    }
  }
  
  public NSArray contacts() {
    return (NSArray)storedValueForKey(CONTACTS_KEY);
  }

  public NSArray contacts(EOQualifier qualifier) {
    return contacts(qualifier, null);
  }

  public NSArray contacts(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = contacts();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToContactsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CONTACTS_KEY);
  }

  public void removeFromContactsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTACTS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact createContactsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Contact");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CONTACTS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact) eo;
  }

  public void deleteContactsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CONTACTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllContactsRelationships() {
    Enumeration objects = contacts().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteContactsRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOContact)objects.nextElement());
    }
  }

  public NSArray distributions() {
    return (NSArray)storedValueForKey(DISTRIBUTIONS_KEY);
  }

  public NSArray distributions(EOQualifier qualifier) {
    return distributions(qualifier, null);
  }

  public NSArray distributions(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = distributions();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToDistributionsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution object) {
    addObjectToBothSidesOfRelationshipWithKey(object, DISTRIBUTIONS_KEY);
  }

  public void removeFromDistributionsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DISTRIBUTIONS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution createDistributionsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Distribution");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, DISTRIBUTIONS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution) eo;
  }

  public void deleteDistributionsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, DISTRIBUTIONS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllDistributionsRelationships() {
    Enumeration objects = distributions().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteDistributionsRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EODistribution)objects.nextElement());
    }
  }


/**
 * Cr̩er une instance de EOItemOut avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOItemOut createEOItemOut(EOEditingContext editingContext, org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemDetail itemDetail, org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemID itemID			) {
    EOItemOut eo = (EOItemOut) createAndInsertInstance(editingContext, _EOItemOut.ENTITY_NAME);    
    eo.setItemDetailRelationship(itemDetail);
    eo.setItemIDRelationship(itemID);
    return eo;
  }

  
	  public EOItemOut localInstanceIn(EOEditingContext editingContext) {
	  		return (EOItemOut)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOItemOut creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOItemOut creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOItemOut object = (EOItemOut)createAndInsertInstance(editingContext, _EOItemOut.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOItemOut localInstanceIn(EOEditingContext editingContext, EOItemOut eo) {
    EOItemOut localInstance = (eo == null) ? null : (EOItemOut)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOItemOut#localInstanceIn a la place.
   */
	public static EOItemOut localInstanceOf(EOEditingContext editingContext, EOItemOut eo) {
		return EOItemOut.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOItemOut fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOItemOut fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOItemOut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOItemOut)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOItemOut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOItemOut fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOItemOut eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOItemOut)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOItemOut fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOItemOut eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOItemOut ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOItemOut fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
  
}
