/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOItemDetail.java instead.
package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlb2b.cxml.server.engine.ICXMLCodingAdditions;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public abstract class _EOItemDetail extends  _EOAbstract implements ICXMLCodingAdditions {
	public static final String ENTITY_NAME = "ItemDetail";
	public static final String ENTITY_TABLE_NAME = "ItemDetail";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String MANUFACTURER_NAME_KEY = "manufacturerName";
	public static final String MANUFACTURER_PART_ID_KEY = "manufacturerPartID";
	public static final String UNIT_OF_MEASURE_KEY = "unitOfMeasure";

// Attributs non visibles
	public static final String DESCRIPTION_ID_KEY = "descriptionID";
	public static final String URL_ID_KEY = "urlID";
	public static final String ID_KEY = "id";
	public static final String UNIT_PRICE_ID_KEY = "unitPriceID";
	public static final String CLASSIFICATION_ID_KEY = "classificationID";

//Colonnes dans la base de donnees
	public static final String MANUFACTURER_NAME_COLKEY = "manufacturerName";
	public static final String MANUFACTURER_PART_ID_COLKEY = "manufacturerPartID";
	public static final String UNIT_OF_MEASURE_COLKEY = "unitOfMeasure";

	public static final String DESCRIPTION_ID_COLKEY = "descriptionID";
	public static final String URL_ID_COLKEY = "urlID";
	public static final String ID_COLKEY = "id";
	public static final String UNIT_PRICE_ID_COLKEY = "unitPriceID";
	public static final String CLASSIFICATION_ID_COLKEY = "classificationID";


	// Relationships
	public static final String CLASSIFICATION_KEY = "classification";
	public static final String DESCRIPTION_KEY = "description";
	public static final String EXTRINSICS_KEY = "extrinsics";
	public static final String UNIT_PRICE_KEY = "unitPrice";
	public static final String URL_KEY = "url";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String manufacturerName() {
    return (String) storedValueForKey(MANUFACTURER_NAME_KEY);
  }

  public void setManufacturerName(String value) {
    takeStoredValueForKey(value, MANUFACTURER_NAME_KEY);
  }

  public String manufacturerPartID() {
    return (String) storedValueForKey(MANUFACTURER_PART_ID_KEY);
  }

  public void setManufacturerPartID(String value) {
    takeStoredValueForKey(value, MANUFACTURER_PART_ID_KEY);
  }

  public String unitOfMeasure() {
    return (String) storedValueForKey(UNIT_OF_MEASURE_KEY);
  }

  public void setUnitOfMeasure(String value) {
    takeStoredValueForKey(value, UNIT_OF_MEASURE_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOClassification classification() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOClassification)storedValueForKey(CLASSIFICATION_KEY);
  }

  public void setClassificationRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOClassification value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOClassification oldValue = classification();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CLASSIFICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CLASSIFICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EODescription description() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EODescription)storedValueForKey(DESCRIPTION_KEY);
  }

  public void setDescriptionRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EODescription value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EODescription oldValue = description();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, DESCRIPTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, DESCRIPTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOUnitPrice unitPrice() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOUnitPrice)storedValueForKey(UNIT_PRICE_KEY);
  }

  public void setUnitPriceRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUnitPrice value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOUnitPrice oldValue = unitPrice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UNIT_PRICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UNIT_PRICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl url() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl)storedValueForKey(URL_KEY);
  }

  public void setUrlRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl oldValue = url();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, URL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, URL_KEY);
    }
  }
  
  public NSArray extrinsics() {
    return (NSArray)storedValueForKey(EXTRINSICS_KEY);
  }

  public NSArray extrinsics(EOQualifier qualifier) {
    return extrinsics(qualifier, null);
  }

  public NSArray extrinsics(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = extrinsics();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToExtrinsicsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic object) {
    addObjectToBothSidesOfRelationshipWithKey(object, EXTRINSICS_KEY);
  }

  public void removeFromExtrinsicsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EXTRINSICS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic createExtrinsicsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Extrinsic");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EXTRINSICS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic) eo;
  }

  public void deleteExtrinsicsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EXTRINSICS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllExtrinsicsRelationships() {
    Enumeration objects = extrinsics().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteExtrinsicsRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOExtrinsic)objects.nextElement());
    }
  }


/**
 * Cr̩er une instance de EOItemDetail avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOItemDetail createEOItemDetail(EOEditingContext editingContext, String manufacturerName
, String manufacturerPartID
, String unitOfMeasure
, org.cocktail.fwkcktlb2b.cxml.server.metier.EOClassification classification, org.cocktail.fwkcktlb2b.cxml.server.metier.EODescription description, org.cocktail.fwkcktlb2b.cxml.server.metier.EOUnitPrice unitPrice			) {
    EOItemDetail eo = (EOItemDetail) createAndInsertInstance(editingContext, _EOItemDetail.ENTITY_NAME);    
		eo.setManufacturerName(manufacturerName);
		eo.setManufacturerPartID(manufacturerPartID);
		eo.setUnitOfMeasure(unitOfMeasure);
    eo.setClassificationRelationship(classification);
    eo.setDescriptionRelationship(description);
    eo.setUnitPriceRelationship(unitPrice);
    return eo;
  }

  
	  public EOItemDetail localInstanceIn(EOEditingContext editingContext) {
	  		return (EOItemDetail)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOItemDetail creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOItemDetail creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOItemDetail object = (EOItemDetail)createAndInsertInstance(editingContext, _EOItemDetail.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOItemDetail localInstanceIn(EOEditingContext editingContext, EOItemDetail eo) {
    EOItemDetail localInstance = (eo == null) ? null : (EOItemDetail)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOItemDetail#localInstanceIn a la place.
   */
	public static EOItemDetail localInstanceOf(EOEditingContext editingContext, EOItemDetail eo) {
		return EOItemDetail.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOItemDetail fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOItemDetail fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOItemDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOItemDetail)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOItemDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOItemDetail fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOItemDetail eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOItemDetail)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOItemDetail fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOItemDetail eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOItemDetail ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOItemDetail fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
  
}
