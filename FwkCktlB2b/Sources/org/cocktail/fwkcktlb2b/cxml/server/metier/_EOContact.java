/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOContact.java instead.
package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlb2b.cxml.server.engine.ICXMLCodingAdditions;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public abstract class _EOContact extends  _EOAbstract implements ICXMLCodingAdditions {
	public static final String ENTITY_NAME = "Contact";
	public static final String ENTITY_TABLE_NAME = "Contact";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String ROLE_KEY = "role";

// Attributs non visibles
	public static final String ID_KEY = "id";
	public static final String NAME_ID_KEY = "nameID";

//Colonnes dans la base de donnees
	public static final String ROLE_COLKEY = "role";

	public static final String ID_COLKEY = "id";
	public static final String NAME_ID_COLKEY = "nameID";


	// Relationships
	public static final String EMAILS_KEY = "emails";
	public static final String FAXES_KEY = "faxes";
	public static final String NAME_KEY = "name";
	public static final String PHONES_KEY = "phones";
	public static final String POSTAL_ADDRESSES_KEY = "postalAddresses";
	public static final String URLS_KEY = "urls";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String role() {
    return (String) storedValueForKey(ROLE_KEY);
  }

  public void setRole(String value) {
    takeStoredValueForKey(value, ROLE_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOName name() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOName)storedValueForKey(NAME_KEY);
  }

  public void setNameRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOName value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOName oldValue = name();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, NAME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, NAME_KEY);
    }
  }
  
  public NSArray emails() {
    return (NSArray)storedValueForKey(EMAILS_KEY);
  }

  public NSArray emails(EOQualifier qualifier) {
    return emails(qualifier, null);
  }

  public NSArray emails(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = emails();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToEmailsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail object) {
    addObjectToBothSidesOfRelationshipWithKey(object, EMAILS_KEY);
  }

  public void removeFromEmailsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EMAILS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail createEmailsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Email");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, EMAILS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail) eo;
  }

  public void deleteEmailsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, EMAILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllEmailsRelationships() {
    Enumeration objects = emails().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteEmailsRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail)objects.nextElement());
    }
  }

  public NSArray faxes() {
    return (NSArray)storedValueForKey(FAXES_KEY);
  }

  public NSArray faxes(EOQualifier qualifier) {
    return faxes(qualifier, null);
  }

  public NSArray faxes(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = faxes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToFaxesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FAXES_KEY);
  }

  public void removeFromFaxesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FAXES_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax createFaxesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Fax");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FAXES_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax) eo;
  }

  public void deleteFaxesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FAXES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFaxesRelationships() {
    Enumeration objects = faxes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFaxesRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax)objects.nextElement());
    }
  }

  public NSArray phones() {
    return (NSArray)storedValueForKey(PHONES_KEY);
  }

  public NSArray phones(EOQualifier qualifier) {
    return phones(qualifier, null);
  }

  public NSArray phones(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = phones();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPhonesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PHONES_KEY);
  }

  public void removeFromPhonesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PHONES_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone createPhonesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Phone");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PHONES_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone) eo;
  }

  public void deletePhonesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PHONES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPhonesRelationships() {
    Enumeration objects = phones().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePhonesRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone)objects.nextElement());
    }
  }

  public NSArray postalAddresses() {
    return (NSArray)storedValueForKey(POSTAL_ADDRESSES_KEY);
  }

  public NSArray postalAddresses(EOQualifier qualifier) {
    return postalAddresses(qualifier, null);
  }

  public NSArray postalAddresses(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = postalAddresses();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToPostalAddressesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress object) {
    addObjectToBothSidesOfRelationshipWithKey(object, POSTAL_ADDRESSES_KEY);
  }

  public void removeFromPostalAddressesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, POSTAL_ADDRESSES_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress createPostalAddressesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("PostalAddress");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, POSTAL_ADDRESSES_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress) eo;
  }

  public void deletePostalAddressesRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, POSTAL_ADDRESSES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPostalAddressesRelationships() {
    Enumeration objects = postalAddresses().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePostalAddressesRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress)objects.nextElement());
    }
  }

  public NSArray urls() {
    return (NSArray)storedValueForKey(URLS_KEY);
  }

  public NSArray urls(EOQualifier qualifier) {
    return urls(qualifier, null);
  }

  public NSArray urls(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = urls();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToUrlsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl object) {
    addObjectToBothSidesOfRelationshipWithKey(object, URLS_KEY);
  }

  public void removeFromUrlsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, URLS_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl createUrlsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("Url");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, URLS_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl) eo;
  }

  public void deleteUrlsRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, URLS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllUrlsRelationships() {
    Enumeration objects = urls().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteUrlsRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl)objects.nextElement());
    }
  }


/**
 * Cr̩er une instance de EOContact avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOContact createEOContact(EOEditingContext editingContext, org.cocktail.fwkcktlb2b.cxml.server.metier.EOName name			) {
    EOContact eo = (EOContact) createAndInsertInstance(editingContext, _EOContact.ENTITY_NAME);    
    eo.setNameRelationship(name);
    return eo;
  }

  
	  public EOContact localInstanceIn(EOEditingContext editingContext) {
	  		return (EOContact)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContact creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOContact creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOContact object = (EOContact)createAndInsertInstance(editingContext, _EOContact.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOContact localInstanceIn(EOEditingContext editingContext, EOContact eo) {
    EOContact localInstance = (eo == null) ? null : (EOContact)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOContact#localInstanceIn a la place.
   */
	public static EOContact localInstanceOf(EOEditingContext editingContext, EOContact eo) {
		return EOContact.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOContact fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOContact fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOContact eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOContact)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOContact fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOContact fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOContact eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOContact)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOContact fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOContact eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOContact ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOContact fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
  
}
