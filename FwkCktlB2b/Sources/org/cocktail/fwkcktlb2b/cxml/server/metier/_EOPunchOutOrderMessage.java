/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPunchOutOrderMessage.java instead.
package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.util.Enumeration;
import java.util.NoSuchElementException;

import org.cocktail.fwkcktlb2b.cxml.server.engine.ICXMLCodingAdditions;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;

public abstract class _EOPunchOutOrderMessage extends  _EOAbstract implements ICXMLCodingAdditions {
	public static final String ENTITY_NAME = "PunchOutOrderMessage";
	public static final String ENTITY_TABLE_NAME = "PunchOutOrderMessage";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String BUYER_COOKIE_KEY = "buyerCookie";

// Attributs non visibles
	public static final String ID_KEY = "id";
	public static final String PUNCH_OUT_ORDER_MESSAGE_HEADER_ID_KEY = "punchOutOrderMessageHeaderID";

//Colonnes dans la base de donnees
	public static final String BUYER_COOKIE_COLKEY = "buyerCookie";

	public static final String ID_COLKEY = "id";
	public static final String PUNCH_OUT_ORDER_MESSAGE_HEADER_ID_COLKEY = "punchOutOrderMessageHeaderID";


	// Relationships
	public static final String ITEM_IN_KEY = "itemIn";
	public static final String PUNCH_OUT_ORDER_MESSAGE_HEADER_KEY = "punchOutOrderMessageHeader";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String buyerCookie() {
    return (String) storedValueForKey(BUYER_COOKIE_KEY);
  }

  public void setBuyerCookie(String value) {
    takeStoredValueForKey(value, BUYER_COOKIE_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutOrderMessageHeader punchOutOrderMessageHeader() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutOrderMessageHeader)storedValueForKey(PUNCH_OUT_ORDER_MESSAGE_HEADER_KEY);
  }

  public void setPunchOutOrderMessageHeaderRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutOrderMessageHeader value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutOrderMessageHeader oldValue = punchOutOrderMessageHeader();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PUNCH_OUT_ORDER_MESSAGE_HEADER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PUNCH_OUT_ORDER_MESSAGE_HEADER_KEY);
    }
  }
  
  public NSArray itemIn() {
    return (NSArray)storedValueForKey(ITEM_IN_KEY);
  }

  public NSArray itemIn(EOQualifier qualifier) {
    return itemIn(qualifier, null);
  }

  public NSArray itemIn(EOQualifier qualifier, NSArray sortOrderings) {
    NSArray results;
      results = itemIn();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    return results;
  }
  
  public void addToItemInRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn object) {
    addObjectToBothSidesOfRelationshipWithKey(object, ITEM_IN_KEY);
  }

  public void removeFromItemInRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ITEM_IN_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn createItemInRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("ItemIn");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, ITEM_IN_KEY);
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn) eo;
  }

  public void deleteItemInRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, ITEM_IN_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllItemInRelationships() {
    Enumeration objects = itemIn().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteItemInRelationship((org.cocktail.fwkcktlb2b.cxml.server.metier.EOItemIn)objects.nextElement());
    }
  }


/**
 * Cr̩er une instance de EOPunchOutOrderMessage avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPunchOutOrderMessage createEOPunchOutOrderMessage(EOEditingContext editingContext, String buyerCookie
, org.cocktail.fwkcktlb2b.cxml.server.metier.EOPunchOutOrderMessageHeader punchOutOrderMessageHeader			) {
    EOPunchOutOrderMessage eo = (EOPunchOutOrderMessage) createAndInsertInstance(editingContext, _EOPunchOutOrderMessage.ENTITY_NAME);    
		eo.setBuyerCookie(buyerCookie);
    eo.setPunchOutOrderMessageHeaderRelationship(punchOutOrderMessageHeader);
    return eo;
  }

  
	  public EOPunchOutOrderMessage localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPunchOutOrderMessage)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPunchOutOrderMessage creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPunchOutOrderMessage creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOPunchOutOrderMessage object = (EOPunchOutOrderMessage)createAndInsertInstance(editingContext, _EOPunchOutOrderMessage.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOPunchOutOrderMessage localInstanceIn(EOEditingContext editingContext, EOPunchOutOrderMessage eo) {
    EOPunchOutOrderMessage localInstance = (eo == null) ? null : (EOPunchOutOrderMessage)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPunchOutOrderMessage#localInstanceIn a la place.
   */
	public static EOPunchOutOrderMessage localInstanceOf(EOEditingContext editingContext, EOPunchOutOrderMessage eo) {
		return EOPunchOutOrderMessage.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPunchOutOrderMessage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPunchOutOrderMessage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPunchOutOrderMessage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPunchOutOrderMessage)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPunchOutOrderMessage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPunchOutOrderMessage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPunchOutOrderMessage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPunchOutOrderMessage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPunchOutOrderMessage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPunchOutOrderMessage eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPunchOutOrderMessage ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPunchOutOrderMessage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
  
}
