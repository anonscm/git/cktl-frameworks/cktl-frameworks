/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
/*
 * Copyright Cocktail, 2001-2008 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOAddress.java instead.
package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.util.NoSuchElementException;

import org.cocktail.fwkcktlb2b.cxml.server.engine.ICXMLCodingAdditions;

import com.webobjects.eocontrol.EOClassDescription;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

public abstract class _EOAddress extends  _EOAbstract implements ICXMLCodingAdditions {
	public static final String ENTITY_NAME = "Address";
	public static final String ENTITY_TABLE_NAME = "Address";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "id";

	public static final String ADDRESS_ID_KEY = "addressID";
	public static final String ISO_COUNTRY_CODE_KEY = "isoCountryCode";

// Attributs non visibles
	public static final String EMAIL_ID_KEY = "emailID";
	public static final String FAX_ID_KEY = "faxID";
	public static final String ID_KEY = "id";
	public static final String PHONE_ID_KEY = "phoneID";
	public static final String NAME_ID_KEY = "nameID";
	public static final String POSTAL_ADDRESS_ID_KEY = "postalAddressID";
	public static final String URL_ID_KEY = "urlID";

//Colonnes dans la base de donnees
	public static final String ADDRESS_ID_COLKEY = "addressID";
	public static final String ISO_COUNTRY_CODE_COLKEY = "isoCountryCode";

	public static final String EMAIL_ID_COLKEY = "emailID";
	public static final String FAX_ID_COLKEY = "faxID";
	public static final String ID_COLKEY = "id";
	public static final String PHONE_ID_COLKEY = "phoneID";
	public static final String NAME_ID_COLKEY = "nameID";
	public static final String POSTAL_ADDRESS_ID_COLKEY = "postalAddressID";
	public static final String URL_ID_COLKEY = "urlID";


	// Relationships
	public static final String EMAIL_KEY = "email";
	public static final String FAX_KEY = "fax";
	public static final String NAME_KEY = "name";
	public static final String PHONE_KEY = "phone";
	public static final String POSTAL_ADDRESS_KEY = "postalAddress";
	public static final String URL_KEY = "url";



public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s) {
	return createAndInsertInstance(eoeditingcontext, s, null);
}


public static EOEnterpriseObject createAndInsertInstance(EOEditingContext eoeditingcontext, String s, NSArray specificites) {
	EOClassDescription eoclassdescription = EOClassDescription.classDescriptionForEntityName(s);
	if (eoclassdescription == null) {
		throw new IllegalArgumentException("Could not find EOClassDescription for entity name '" + s + "' !");
	}
	else {
		EOEnterpriseObject eoenterpriseobject = eoclassdescription.createInstanceWithEditingContext(eoeditingcontext, null);
		eoeditingcontext.insertObject(eoenterpriseobject);
		return eoenterpriseobject;
	}
}

public static EOEnterpriseObject localInstanceOfObject(EOEditingContext eoeditingcontext, EOEnterpriseObject eoenterpriseobject) {
	if (eoenterpriseobject == null) {
		return null;
	}

	EOEditingContext eoeditingcontext1 = eoenterpriseobject.editingContext();
	if (eoeditingcontext1 == null) {
		throw new IllegalArgumentException("The EOEnterpriseObject " + eoenterpriseobject + " is not in an EOEditingContext.");
	}
	else if (eoeditingcontext1.equals(eoeditingcontext)) {
		return eoenterpriseobject;
	}
	com.webobjects.eocontrol.EOGlobalID eoglobalid = eoeditingcontext1.globalIDForObject(eoenterpriseobject);
	return eoeditingcontext.faultForGlobalID(eoglobalid, eoeditingcontext);

}



	// Accessors methods
  public String addressID() {
    return (String) storedValueForKey(ADDRESS_ID_KEY);
  }

  public void setAddressID(String value) {
    takeStoredValueForKey(value, ADDRESS_ID_KEY);
  }

  public String isoCountryCode() {
    return (String) storedValueForKey(ISO_COUNTRY_CODE_KEY);
  }

  public void setIsoCountryCode(String value) {
    takeStoredValueForKey(value, ISO_COUNTRY_CODE_KEY);
  }

  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail email() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail)storedValueForKey(EMAIL_KEY);
  }

  public void setEmailRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOEmail oldValue = email();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EMAIL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EMAIL_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax fax() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax)storedValueForKey(FAX_KEY);
  }

  public void setFaxRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOFax oldValue = fax();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, FAX_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, FAX_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOName name() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOName)storedValueForKey(NAME_KEY);
  }

  public void setNameRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOName value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOName oldValue = name();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, NAME_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, NAME_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone phone() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone)storedValueForKey(PHONE_KEY);
  }

  public void setPhoneRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOPhone oldValue = phone();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PHONE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PHONE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress postalAddress() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress)storedValueForKey(POSTAL_ADDRESS_KEY);
  }

  public void setPostalAddressRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOPostalAddress oldValue = postalAddress();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, POSTAL_ADDRESS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, POSTAL_ADDRESS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl url() {
    return (org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl)storedValueForKey(URL_KEY);
  }

  public void setUrlRelationship(org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl value) {
    if (value == null) {
    	org.cocktail.fwkcktlb2b.cxml.server.metier.EOUrl oldValue = url();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, URL_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, URL_KEY);
    }
  }
  

/**
 * Cr̩er une instance de EOAddress avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOAddress createEOAddress(EOEditingContext editingContext, String addressID
, String isoCountryCode
, org.cocktail.fwkcktlb2b.cxml.server.metier.EOName name			) {
    EOAddress eo = (EOAddress) createAndInsertInstance(editingContext, _EOAddress.ENTITY_NAME);    
		eo.setAddressID(addressID);
		eo.setIsoCountryCode(isoCountryCode);
    eo.setNameRelationship(name);
    return eo;
  }

  
	  public EOAddress localInstanceIn(EOEditingContext editingContext) {
	  		return (EOAddress)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context. Cette methode n'iformera pas les objets de type ISpecificite.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAddress creerInstance(EOEditingContext editingContext) {
	  		return creerInstance(editingContext, null);
		}

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * @param specificites Un tableau d'objets {@link ISpecificite}. Ce tableau sera affecte a l'objet. Les objets en question seront notifies de la creation de l'objet metier.
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOAddress creerInstance(EOEditingContext editingContext, NSArray specificites) {
	  		EOAddress object = (EOAddress)createAndInsertInstance(editingContext, _EOAddress.ENTITY_NAME, specificites);
	  		return object;
		}
	
	
  
  public static EOAddress localInstanceIn(EOEditingContext editingContext, EOAddress eo) {
    EOAddress localInstance = (eo == null) ? null : (EOAddress)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOAddress#localInstanceIn a la place.
   */
	public static EOAddress localInstanceOf(EOEditingContext editingContext, EOAddress eo) {
		return EOAddress.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOAddress fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier pass̩ en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOAddress fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOAddress eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOAddress)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOAddress fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOAddress fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOAddress eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOAddress)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouv̩, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOAddress fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOAddress eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOAddress ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOAddress fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	/** Doit renvoyer un tableau trie des attributs xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlAttributeKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau trie des balises xml utilises lors du codage de l'EO en XML */
	public NSArray<String> sortedXmlContentKeys() {
		return null;
	}
	
	/** Doit renvoyer un tableau des attributs xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlAttributeKeys() {
		return null;
	}

	/** Doit renvoyer un tableau des balises xml a coder meme lorsqu'ils sont nuls lors du codage de l'EO en XML */
	public NSArray<String> codeEvenIfEmptyXmlContentKeys() {
		return null;
	}
  
}
