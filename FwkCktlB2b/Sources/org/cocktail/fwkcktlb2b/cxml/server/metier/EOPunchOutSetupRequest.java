/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.metier;

import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOPunchOutSetupRequest extends _EOPunchOutSetupRequest {
	private static NSArray<String> SORTED_XML_ATTRIBUTE_KEYS = new NSArray<String>(new String[] {

	});

	private static NSArray<String> SORTED_XML_CONTENT_KEYS = new NSArray<String>(new String[] {
			BUYER_COOKIE_KEY,
			EXTRINSICS_KEY,
			BROWSER_FORM_POST_KEY,
			CONTACTS_KEY,
			SUPPLIER_SETUP_KEY,
			SHIP_TO_KEY,
			SELECTED_ITEM_KEY,
			ITEM_OUTS_KEY
	});

	public static final String OPERATION_EDIT = "edit";
	public static final String OPERATION_CREATE = "create";

	public EOPunchOutSetupRequest() {
		super();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	@Override
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	public void setExtrensic(String key, String value) {
		EOQualifier qual = new EOKeyValueQualifier(EOExtrinsic.NAME_KEY, EOQualifier.QualifierOperatorEqual, key);
		NSArray res = EOQualifier.filteredArrayWithQualifier(extrinsics(), qual);
		if (res.count() > 0) {
			((EOExtrinsic) res.objectAtIndex(0)).setVal(value);
		}
	}

	public String getExtrensic(String key) {
		EOQualifier qual = new EOKeyValueQualifier(EOExtrinsic.NAME_KEY, EOQualifier.QualifierOperatorEqual, key);
		NSArray res = EOQualifier.filteredArrayWithQualifier(extrinsics(), qual);
		if (res.count() > 0) {
			return ((EOExtrinsic) res.objectAtIndex(0)).getVal();
		}
		return null;
	}

	@Override
	public NSArray<String> sortedXmlContentKeys() {
		return SORTED_XML_CONTENT_KEYS;
	}

	@Override
	public NSArray<String> sortedXmlAttributeKeys() {
		return SORTED_XML_ATTRIBUTE_KEYS;
	}

}
