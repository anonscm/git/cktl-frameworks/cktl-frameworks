/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.metier;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOPostalAddress extends _EOPostalAddress {
	private static NSArray<String> SORTED_XML_ATTRIBUTE_KEYS = new NSArray<String>(new String[] {
			EOPostalAddress.NAME_KEY
			});

	private static NSArray<String> SORTED_XML_CONTENT_KEYS = new NSArray<String>(new String[] {
			EOPostalAddress.DELIVER_TOS_KEY,
			EOPostalAddress.STREETS_KEY,
			EOPostalAddress.CITY_KEY,
			EOPostalAddress.STATE_KEY,
			EOPostalAddress.POSTAL_CODE_KEY,
			EOPostalAddress.COUNTRY_KEY
			});

	public EOPostalAddress() {
		super();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * Cree un objet EOPostalAddress destine a etre affecte a un objet EOAdress.
	 * 
	 * @param editingContext
	 * @param postalAddressName Permet d'identifier l'adresse postale (ex. "travail")
	 * @param lastName
	 * @param firstName
	 * @param streets Tableau de String contenant les differentes lignes de l'adresse postale.
	 * @param city
	 * @param state
	 * @param postalCode
	 * @param isoCountryCode
	 * @return Un nouvel objet representant une adresse postale.
	 */
	public static EOPostalAddress createEOPostalAddress(EOEditingContext editingContext, String postalAddressName, NSArray deliverToNames, NSArray streets, String city, String state, String postalCode, String isoCountryCode) {
		EOPostalAddress res = EOPostalAddress.creerInstance(editingContext);
		res.setName(postalAddressName);
		for (int i = 0; i < deliverToNames.count(); i++) {
			res.deliverTos().addObject(deliverToNames.objectAtIndex(i));
		}
		for (int i = 0; i < streets.count(); i++) {
			res.streets().addObject(streets.objectAtIndex(i));
		}

		res.setCity(city);
		res.setState(state);
		res.setPostalCode(postalCode);
		res.setCountryRelationship(EOCountry.createEOCountry(editingContext, isoCountryCode));
		return res;
	}

	@Override
	public NSMutableArray streets() {
		if (super.streets() == null) {
			super.setStreets(new NSMutableArray());
		}
		return super.streets();
	}

	@Override
	public NSMutableArray deliverTos() {
		if (super.deliverTos() == null) {
			super.setDeliverTos(new NSMutableArray());
		}
		return super.deliverTos();
	}

	@Override
	public NSArray<String> sortedXmlContentKeys() {
		return SORTED_XML_CONTENT_KEYS;
	}

	@Override
	public NSArray<String> sortedXmlAttributeKeys() {
		return SORTED_XML_ATTRIBUTE_KEYS;
	}

}
