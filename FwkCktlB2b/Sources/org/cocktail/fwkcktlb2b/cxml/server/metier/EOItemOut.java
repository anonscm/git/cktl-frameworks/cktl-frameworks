/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlb2b.cxml.server.metier;

import java.math.BigDecimal;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

public class EOItemOut extends _EOItemOut {
	private static NSArray<String> SORTED_XML_ATTRIBUTE_KEYS = new NSArray<String>(new String[] {
			QUANTITY_KEY,
			LINE_NUMBER_KEY,
			REQUISITION_ID_KEY,
			REQUESTED_DELIVERY_DATE_KEY

	});

	private static NSArray<String> SORTED_XML_CONTENT_KEYS = new NSArray<String>(new String[] {
			EOItemOut.ITEM_ID_KEY,
			EOItemOut.ITEM_DETAIL_KEY,
			EOItemOut.SUPPLIER_ID_KEY,
			EOItemOut.SHIP_TO_KEY,
			EOItemOut.SHIPPING_KEY,
			EOItemOut.TAX_KEY,
			EOItemOut.DISTRIBUTIONS_KEY,
			EOItemOut.CONTACTS_KEY,
			EOItemOut.COMMENTS_KEY

	});

	public EOItemOut() {
		super();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories.
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

	}

	/**
	 * @return calcule et renvoie la quantite multipliee par le prix unitaire.
	 */
	public BigDecimal lineTotal() {
		if (itemDetail() != null && itemDetail().unitPrice() != null && quantity() != null) {
			return itemDetail().unitPrice().money().getVal().multiply(BigDecimal.valueOf(quantity().doubleValue())).setScale(2);
		}
		return BigDecimal.ZERO;
	}

	@Override
	public NSArray<String> sortedXmlContentKeys() {
		return SORTED_XML_CONTENT_KEYS;
	}

	@Override
	public NSArray<String> sortedXmlAttributeKeys() {
		return SORTED_XML_ATTRIBUTE_KEYS;
	}

	public static EOItemOut createEmptyItemOut(EOEditingContext ec) {
		EOItemOut itemOut = new EOItemOut();
		itemOut.setCommentsRelationship(new EOComments());
		itemOut.setItemDetailRelationship(new EOItemDetail());
		itemOut.setItemIDRelationship(new EOItemID());
		itemOut.setShippingRelationship(new EOShipping());
		itemOut.setShipToRelationship(new EOShipTo());
		itemOut.setSupplierIDRelationship(new EOSupplierID());

		itemOut.itemDetail().setClassificationRelationship(new EOClassification());
		itemOut.itemDetail().setDescriptionRelationship(new EODescription());
		itemOut.itemDetail().setUnitPriceRelationship(new EOUnitPrice());
		itemOut.itemDetail().unitPrice().setMoneyRelationship(new EOMoney());
		itemOut.itemDetail().setUrlRelationship(new EOUrl());

		itemOut.shipping().setDescriptionRelationship(new EODescription());
		itemOut.shipping().setMoneyRelationship(new EOMoney());

		itemOut.shipTo().setAddressRelationship(new EOAddress());
		itemOut.setTaxRelationship(new EOTax());
		itemOut.tax().setMoneyRelationship(new EOMoney());

		return itemOut;
	}

}
