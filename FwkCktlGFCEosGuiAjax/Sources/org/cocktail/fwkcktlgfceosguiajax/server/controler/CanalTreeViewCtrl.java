/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceosguiajax.server.controler;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktlgfceosguiajax.server.component.canal.CanalTreeView;
import org.cocktail.fwkcktlwebapp.common.util.NSArrayCtrl;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;

import er.ajax.AjaxTreeModel;

public class CanalTreeViewCtrl {

	private CanalTreeView wocomponent;

	private Object _delegate;
	private CodeAnalytiqueNode rootCodeAnalytique;
	private CodeAnalytiqueNode unNodeCodeAnalytique;
	private CodeAnalytiqueNode selectedNodeCodeAnalytique;
	public AjaxTreeModel myTreeModel = new AjaxTreeModel();

	public CanalTreeViewCtrl() {

	}

	public CanalTreeViewCtrl(CanalTreeView component) {
		wocomponent = component;
	}

	private EOEditingContext edc() {
		return wocomponent.edc();
	}

	public CodeAnalytiqueNode rootCodeAnalytique() {
		if (rootCodeAnalytique == null || (wocomponent.treeRootObject() != null && rootCodeAnalytique.object() instanceof EmptyRootObject) || (wocomponent.treeRootObject() == null && !(rootCodeAnalytique.object() instanceof EmptyRootObject))) {
			if (wocomponent.treeRootObject() != null) {
				rootCodeAnalytique = new CodeAnalytiqueNode(null, wocomponent.treeRootObject());
			}
			else {
				rootCodeAnalytique = new CodeAnalytiqueNode(null, new EmptyRootObject());
			}
		}
		return rootCodeAnalytique;
	}

	public void setRootCodeAnalytique(CodeAnalytiqueNode rootCodeAnalytique) {
		this.rootCodeAnalytique = rootCodeAnalytique;
		setSelectedNodeCodeAnalytique(null);
	}

	public CodeAnalytiqueNode unCodeAnalytique() {
		return unNodeCodeAnalytique;
	}

	public void setUnCodeAnalytique(CodeAnalytiqueNode unCodeAnalytique) {
		this.unNodeCodeAnalytique = unCodeAnalytique;
	}

	public String unCodeAnalytiqueLibelle() {
		NSKeyValueCoding obj = unCodeAnalytique().object();
		return (String) obj.valueForKey(EOCodeAnalytique.CAN_CODE_KEY) + " (" + obj.valueForKey(EOCodeAnalytique.CAN_LIBELLE_KEY) + ")";
	}

	public Boolean isUnCodeAnalytiqueSelectionnable() {
		return Boolean.TRUE;
	}

	public Object delegate() {
		if (_delegate == null) {
			_delegate = new CanalTreeViewCtrl.Delegate();
		}
		return _delegate;
	}

	public class Delegate implements AjaxTreeModel.Delegate {

		public NSArray childrenTreeNodes(Object node) {
			if (node != null) {
				return ((CodeAnalytiqueNode) node).children();
			}
			return NSArray.EmptyArray;
		}

		public boolean isLeaf(Object node) {
			if (node != null) {
				return ((CodeAnalytiqueNode) node).isLeaf();
			}
			return true;
		}

		public Object parentTreeNode(Object node) {
			if (node != null) {
				return ((CodeAnalytiqueNode) node).parent();
			}
			return null;
		}
	}

	public class CodeAnalytiqueNode {
		public NSArray children;
		public CodeAnalytiqueNode parent;
		public NSKeyValueCoding object;

		public CodeAnalytiqueNode(CodeAnalytiqueNode parent, NSKeyValueCoding obj) {
			object = obj;
			this.parent = parent;
		}

		public NSArray children() {
			if (children == null) {
				NSArray res = NSArray.EmptyArray;
				if (object() instanceof EmptyRootObject) {
					res = EOCodeAnalytique.getCodeAnalytiquesRacine(edc());
				}
				else {
					EOCodeAnalytique canal = ((EOCodeAnalytique) object());
					res = EOSortOrdering.sortedArrayUsingKeyOrderArray(canal.codeAnalytiqueFilsValide(), new NSArray(new Object[] {
							EOCodeAnalytique.SORT_CAN_CODE_ASC
					}));
					if (wocomponent.getQualifier() != null) {
						res = EOQualifier.filteredArrayWithQualifier(res, wocomponent.getQualifier());
					}

					//Balayer les resultats, si code prives, on le retire si utilisateur n'a pas les droits sur une organ associee.
					Iterator iter = res.iterator();
					NSMutableArray res2 = new NSMutableArray();
					while (iter.hasNext()) {
						EOCodeAnalytique canalFils = (EOCodeAnalytique) iter.next();
						if ((canalFils.isPublic())) {
							res2.addObject(canalFils);
						}
						else {
							final ArrayList list = new ArrayList(2);
							list.add(canalFils.getOrgans());
							list.add(wocomponent.getAllowedOrgans());
							if (NSArrayCtrl.intersectionOfNSArray(list).count() > 0) {
								res2.addObject(canalFils);
							}
						}
					}
					res = res2;
				}

				NSMutableArray res3 = new NSMutableArray();
				for (int i = 0; i < res.count(); i++) {
					//verifier si objet dans liste exclusions
					//if (wocomponent.getTreeExclusions()==null || wocomponent.getTreeExclusions().indexOf(res.objectAtIndex(i))==NSArray.NotFound) {

					CodeAnalytiqueNode node = new CodeAnalytiqueNode(this, (NSKeyValueCoding) res.objectAtIndex(i));
					res3.addObject(node);
					//}
				}
				children = res3.immutableClone();
			}
			return children;
		}

		public boolean isLeaf() {
			return (children().count() == 0);
		}

		public CodeAnalytiqueNode parent() {
			return parent;
		}

		public void setParent(CodeAnalytiqueNode node) {
			parent = node;
		}

		public NSKeyValueCoding object() {
			return object;
		}
	}

	public class EmptyRootObject implements NSKeyValueCoding {

		public EmptyRootObject() {
		}

		public void takeValueForKey(Object arg0, String arg1) {

		}

		public Object valueForKey(String arg0) {
			return "root";
		}

	}

	public CodeAnalytiqueNode getSelectedNodeCodeAnalytique() {
		return selectedNodeCodeAnalytique;
	}

	public void setSelectedNodeCodeAnalytique(CodeAnalytiqueNode selectedNodeCodeAnalytique) {
		this.selectedNodeCodeAnalytique = selectedNodeCodeAnalytique;
		if (selectedNodeCodeAnalytique == null) {
			wocomponent.setSelection(null);
		}
		else {
			wocomponent.setSelection((EOCodeAnalytique) getSelectedNodeCodeAnalytique().object());
		}
	}

	/**
	 * Recherche le noeud correspondand à l'objet dans l'arbre et realise un expand sur les branches correspondantes.
	 * 
	 * @param node
	 * @param canal
	 */
	public CodeAnalytiqueNode selectObjectInTree(CodeAnalytiqueNode node, EOCodeAnalytique canal) {
		CodeAnalytiqueNode newSelectedNode = null;
		if (node.object().equals(canal)) {
			newSelectedNode = node;
		}
		else {
			Enumeration en = node.children().objectEnumerator();
			while (newSelectedNode == null && en.hasMoreElements()) {
				CodeAnalytiqueNode tmpNode = (CodeAnalytiqueNode) en.nextElement();
				newSelectedNode = selectObjectInTree(tmpNode, canal);
			}
		}
		if (newSelectedNode != null) {
			myTreeModel.setExpanded(node, true);
		}
		return newSelectedNode;
	}

	public CanalTreeView getWocomponent() {
		return wocomponent;
	}

	public void setWocomponent(CanalTreeView wocomponent) {
		this.wocomponent = wocomponent;
	}

}
