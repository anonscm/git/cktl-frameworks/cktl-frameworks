/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceosguiajax.server.component.canal;

import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktlgfceosguiajax.server.component.ATreeViewComponent;
import org.cocktail.fwkcktlgfceosguiajax.server.controler.CanalTreeViewCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Affiche l'arbre des codes analytiques
 * 
 * @binding currentExercice Obligatoire. String
 * @binding resetTree Facultatif. TRUE pour forcer la reinitialisation de l'arbre. Si vous l'utilisez, indiquez une variable qui peut etre mise a
 *          jour.
 * @binding ctrl Facultatif. Controleur a utiliser. Doit heriter de {@link CanalTreeViewCtrl}.
 * @binding editingContext
 * @binding selection
 * @binding updateContainerID
 * @binding utilisateurPersId
 * @binding treeRootObject
 * @binding treeQualifier
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CanalTreeView extends ATreeViewComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_currentExercice = "currentExercice";
	public static final String BINDING_resetTree = "resetTree";
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_onSuccessSelect = "onSuccessSelect";
	public static final String BINDING_onSelect = "onSelect";

	private CanalTreeViewCtrl ctrl;
	private EOCodeAnalytique selection;
	private NSArray allowedOrgans = null;

	private String selectedElementId;

	public CanalTreeView(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (getCtrl().getWocomponent() == null) {
			getCtrl().setWocomponent(this);
		}
		if (hasBinding(BINDING_resetTree)) {
			if (Boolean.TRUE.equals(valueForBinding(BINDING_resetTree))) {
				resetTree();
			}
			if (canSetValueForBinding(BINDING_resetTree)) {
				setValueForBinding(Boolean.FALSE, BINDING_resetTree);
			}
		}
		super.appendToResponse(response, context);

	}

	private void resetTree() {
		EOCodeAnalytique canal = (EOCodeAnalytique) valueForBinding(SELECTION_BDG);
		getCtrl().rootCodeAnalytique().children = null;
		//Retrouver et afficher la selection dans l'arbre
		if (canal != selection()) {
			ctrl.setSelectedNodeCodeAnalytique(ctrl.selectObjectInTree(ctrl.rootCodeAnalytique(), canal));
		}
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	public CanalTreeViewCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (CanalTreeViewCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new CanalTreeViewCtrl(this);
			}
		}
		return ctrl;
	}

	//
	//	public void setCtrl(CanalTreeViewCtrl ctrl) {
	//		this.ctrl = ctrl;
	//	}

	public EOCodeAnalytique treeRootObject() {
		return (EOCodeAnalytique) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	public EOQualifier getQualifier() {
		EOQualifier qual = getTreeQualifier();
		//		if (qual != null) {
		//			if (getUserFiltreQualifier() != null) {
		//				qual = new EOAndQualifier(new NSArray(new Object[]{qual, getUserFiltreQualifier()}));
		//			}
		//		}
		//		else {
		//			qual = getUserFiltreQualifier();
		//		}

		return qual;
	}

	public EOCodeAnalytique selection() {
		return selection;
	}

	public void setSelection(EOCodeAnalytique selection) {
		this.selection = selection;
		setValueForBinding(selection, SELECTION_BDG);
	}

	public String getCurrentExercice() {
		return (String) valueForBinding(BINDING_currentExercice);
	}

	public NSArray getAllowedOrgans() {
		if (allowedOrgans == null) {
			allowedOrgans = getAppUser().getOrgansAutorisees(edc(), getCurrentExercice());
		}
		return allowedOrgans;
	}

	public WOActionResults onSelect() {
		getCtrl().setSelectedNodeCodeAnalytique(getCtrl().unCodeAnalytique());
		if (hasBinding(BINDING_onSelect)) {
			performParentAction(BINDING_onSelect);
		}
		return null;
	}

	public Boolean isUnNodeSelected() {
		return Boolean.valueOf(getCtrl().unCodeAnalytique().equals(getCtrl().getSelectedNodeCodeAnalytique()));
	}

	public String functionTreeViewCompleted() {
		return getComponentId() + "_onTreeViewCompleted=function(){var el = document.getElementById('" + selectedElementId() + "'); if (el != null) {el.scrollIntoView(true);  };}";
	}

	public String onTreeViewCompleted() {
		return getComponentId() + "_onTreeViewCompleted";
	}

	public String selectedElementId() {
		if (selectedElementId == null) {
			selectedElementId = unAjaxTreeID() + "_selected";
		}
		return selectedElementId;
	}

	public void setSelectedElementId(String id) {
		//return unAjaxTreeID()+"_selected";
		selectedElementId = id;
	}

	public String onSuccessSelect() {
		return (String) valueForBinding(BINDING_onSuccessSelect);
	}
}
