/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceosguiajax.server.component.canal;

import java.math.BigDecimal;

import org.cocktail.fwkcktlgfceos.server.GFCUtilities;
import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueSuivi;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOExercice;
import org.cocktail.fwkcktlgfceosguiajax.server.component.ACktlAjaxGFCComponent;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

import com.webobjects.appserver.WOContext;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNumberFormatter;
import com.webobjects.foundation.NSTimestampFormatter;

/**
 * Afiche le détail d'un code analytique.
 * 
 * @binding selectedCodeAnalytique Obligatoire. L'objet {@link EOCodeAnalytique} sélectionné.
 * @binding currentExercice Obligatoire. L'objet {@link EOExercice} représentant l'exercice en cours.
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CanalView extends ACktlAjaxGFCComponent {

	private static final long serialVersionUID = 1L;
	//	private EOEb selectedOrgan;
	public EOEb unOrgan;
	private NSNumberFormatter deviseFormatter;

	/** Obligatoire. L'objet {@link EOCodeAnalytique} sélectionné. */
	public static final String BINDING_selectedCodeAnalytique = "selectedCodeAnalytique";

	public CanalView(WOContext context) {
		super(context);
	}

	public EOCodeAnalytique getSelectedCodeAnalytique() {
		if (hasBinding(BINDING_selectedCodeAnalytique)) {
			return (EOCodeAnalytique) valueForBinding(BINDING_selectedCodeAnalytique);
		}
		return null;
	}

	public Boolean ifHasMontant() {
		return Boolean.valueOf(ifHasCodeAnalytique().booleanValue() && getSelectedCodeAnalytique().canMontant() != null);
	}

	public Boolean ifHasCodeAnalytique() {
		return Boolean.valueOf((getSelectedCodeAnalytique() != null));
	}

	public Boolean ifIsPrive() {
		return Boolean.valueOf((!getSelectedCodeAnalytique().isPublic()));
	}

	public Boolean ifHasOrgans() {
		return Boolean.valueOf(getOrgansForCodeAnalytique().count() > 0);
	}

	public String getPeriode() {
		String s = "";
		NSTimestampFormatter tsFormatter = new NSTimestampFormatter("%d/%m/%Y");

		if (ifHasCodeAnalytique().booleanValue()) {
			if (getSelectedCodeAnalytique().canOuverture() != null) {
				s = "A partir du " + tsFormatter.format(getSelectedCodeAnalytique().canOuverture());
			}
			if (getSelectedCodeAnalytique().canFermeture() != null) {
				s += " jusqu'au " + tsFormatter.format(getSelectedCodeAnalytique().canFermeture());
				//    			s += " jusqu'au " + MyDateCtrl.formatDateShort(getSelectedCodeAnalytique().canFermeture());
			}
			if (s.length() > 0) {
				s = "" + s + "";
			}
		}
		return s;
	}

	public NSArray getOrgansForCodeAnalytique() {
		if (getSelectedCodeAnalytique() != null) {
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(getSelectedCodeAnalytique().getOrgans(EOExercice.getExercice(edc(), Integer.valueOf(getCurrentExercice()))), new NSArray(new Object[] {
					EOSortOrdering.sortOrderingWithKey(EOEb.LONG_STRING_KEY, EOSortOrdering.CompareAscending)
			}));
		}
		return NSArray.EmptyArray;
	}

	public String getLibelleForUnOrgan() {
		return StringCtrl.compactString(unOrgan.getLongString(), 40, "...");
	}

	public String getCodeHierarchie() {
		if (getSelectedCodeAnalytique() != null) {
			String res = "";
			EOCodeAnalytique struct = getSelectedCodeAnalytique().codeAnalytiquePere();
			do {
				if (struct != null) {
					res = struct.canCode() + " > " + res;
					struct = struct.codeAnalytiquePere();
				}
			} while (struct != null && struct.canNiveau().intValue() >= 1);
			return res;
		}
		return null;
	}

	public String getCurrentExercice() {
		return (String) valueForBinding(BINDING_currentExercice);
	}

	public BigDecimal getMontantExecutionForCode() {
		if (getSelectedCodeAnalytique() != null) {
			EOCodeAnalytiqueSuivi codeAnalytiqueSuivi = EOCodeAnalytiqueSuivi.fetchByKeyValue(edc(), EOCodeAnalytiqueSuivi.TO_CODE_ANALYTIQUE_KEY, getSelectedCodeAnalytique());
			if (codeAnalytiqueSuivi != null) {
				return codeAnalytiqueSuivi.montantBudgetaire();
			}
		}
		return null;

	}

	/**
	 * @return Le formatter a utiliser
	 */
	public NSNumberFormatter deviseFormatter() {
		if (deviseFormatter == null) {
			deviseFormatter = GFCUtilities.getDefaultDeviseFormatter(edc(), Integer.valueOf(getCurrentExercice()));
		}
		return deviseFormatter;
	}

}
