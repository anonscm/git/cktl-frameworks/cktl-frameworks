/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlgfceosguiajax.server.component.canal;

import java.util.Date;

import org.cocktail.fwkcktlgfceos.server.GFCUtilities;
import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytiqueEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOEb;
import org.cocktail.fwkcktlgfceos.server.metier.EOTypeEtat;
import org.cocktail.fwkcktlgfceosguiajax.server.component.ACktlAjaxGFCComponent;
import org.cocktail.fwkcktlgfceosguiajax.server.controler.CanalFormCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * Formulaire de creation d'un code analytique privé. Utilisez plutot {@link CanalUI}.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CanalForm extends ACktlAjaxGFCComponent {

	private static final long serialVersionUID = 1L;

	/** Obligatoire. L'objet {@link EOCodeAnalytique} en cours de modification. */
	public static final String BINDING_editingCodeAnalytique = "editingCodeAnalytique";

	/** Facultatif. Controleur a utiliser. Doit heriter de {@link CanalFormCtrl}. */
	public static final String BINDING_ctrl = "ctrl";

	private NSArray lesTypeEtatsMontantDepassement = null;
	private NSArray lesTypeEtatUtilisables = null;

	public EOTypeEtat unTypeEtatDepassement;
	public EOTypeEtat unTypeEtatUtilisable;

	public EOEb unOrgan;
	private EOEb selectedOrgan;
	private EOEb selectedOrganInTree;

	private NSNumberFormatter deviseFormatter;

	private CanalFormCtrl ctrl;

	public CanalForm(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (getCtrl().getWocomponent() == null) {
			getCtrl().setWocomponent(this);
		}
		super.appendToResponse(response, context);

	}

	public EOCodeAnalytique getEditingCodeAnalytique() {
		return (EOCodeAnalytique) valueForBinding(BINDING_editingCodeAnalytique);
	}

	public NSArray lesTypeEtatMontantDepassements() {
		if (lesTypeEtatsMontantDepassement == null) {
			lesTypeEtatsMontantDepassement = EOCodeAnalytique.getTypeEtatsForCanMontantDepassement(edc());
		}
		return lesTypeEtatsMontantDepassement;
	}

	public NSArray lesTypeEtatUtilisables() {
		if (lesTypeEtatUtilisables == null) {
			lesTypeEtatUtilisables = EOCodeAnalytique.getTypeEtatsForCanUtilisable(edc());
		}
		return lesTypeEtatUtilisables;
	}

	public String getOrgansContainerId() {
		return getComponentId() + "_organs";
	}

	public String getContainerListeOrganId() {
		return getComponentId() + "_listeOrgans";
	}

	public String getOrganSelectId() {
		return getComponentId() + "_organSelect";
	}

	public NSArray getOrgansForCodeAnalytique() {
		if (getEditingCodeAnalytique() != null) {
			return EOSortOrdering.sortedArrayUsingKeyOrderArray(getEditingCodeAnalytique().getOrgans(), new NSArray(new Object[] {
					EOSortOrdering.sortOrderingWithKey(EOEb.LONG_STRING_KEY, EOSortOrdering.CompareAscending)
			}));
		}
		return NSArray.EmptyArray;
	}

	public String getLibelleForUnOrgan() {
		//return MyStringCtrl.compactString(unOrgan.getLongString(), 40, "...");
		return getCtrl().getLibelleForUnOrgan(unOrgan);
	}

	//	
	//	public NSArray getOrgansForCodeAnalytique() {
	//		if (getEditingCodeAnalytique() != null) {
	//			return EOSortOrdering.sortedArrayUsingKeyOrderArray(getEditingCodeAnalytique().getOrgans(), new NSArray(new Object[]{EOSortOrdering.sortOrderingWithKey(EOEb.LONG_STRING_KEY, EOSortOrdering.CompareAscending)}));
	//		}
	//		return NSArray.EmptyArray;
	//	}
	//	
	//	public String getLibelleForUnOrgan() {
	//		return MyStringCtrl.compactString(unOrgan.getLongString(), 40, "...");
	//	}

	public EOEb getSelectedOrgan() {
		return selectedOrgan;
	}

	public void setSelectedOrgan(EOEb selectedOrgan) {
		this.selectedOrgan = selectedOrgan;
	}

	public WOActionResults onBeforeOrganAjouter() {
		setSelectedOrganInTree(null);
		return null;
	}

	public WOActionResults onOrganAjouter() {
		if (getSelectedOrganInTree() != null) {
			if (getOrgansForCodeAnalytique().indexOf(getSelectedOrganInTree()) == NSArray.NotFound) {
				EOCodeAnalytiqueEb codeAnalytiqueOrgan = EOCodeAnalytiqueEb.creerInstance(edc());
				codeAnalytiqueOrgan.setCodeAnalytiqueRelationship(getEditingCodeAnalytique());
				codeAnalytiqueOrgan.setOrganRelationship(getSelectedOrganInTree());
			}
			setSelectedOrganInTree(null);
		}
		return null;
	}

	public WOActionResults onOrganSupprimer() {
		if (getSelectedOrgan() != null) {
			EOQualifier qual = new EOKeyValueQualifier(EOCodeAnalytiqueEb.ORGAN_KEY, EOQualifier.QualifierOperatorEqual, getSelectedOrgan());
			NSArray res = getEditingCodeAnalytique().codeAnalytiqueOrgans(qual);
			if (res.count() > 0) {
				EOCodeAnalytiqueEb codeAnalytiqueOrgan = (EOCodeAnalytiqueEb) res.objectAtIndex(0);
				codeAnalytiqueOrgan.setOrganRelationship(null);
				codeAnalytiqueOrgan.setCodeAnalytiqueRelationship(null);
				edc().deleteObject(codeAnalytiqueOrgan);
				setSelectedOrgan(null);
			}
		}
		return null;
	}

	public Boolean isOrganSelected() {
		return Boolean.valueOf(getSelectedOrgan() != null);
	}

	public Boolean ifHasOrgans() {
		return Boolean.valueOf(getOrgansForCodeAnalytique().count() > 0);
	}

	public String onSuccessOrganAjouter() {
		String onSuccessRechercher = null;
		onSuccessRechercher = "openWinTreeView('";
		onSuccessRechercher += organTreeViewId() + "', '";
		onSuccessRechercher += "Choisir une ligne budgétaire', true";
		onSuccessRechercher += ", 470";
		onSuccessRechercher += ", 320";
		onSuccessRechercher += ", null";
		onSuccessRechercher += ", '" + organTreeViewId() + "'";
		onSuccessRechercher += ");";
		return onSuccessRechercher;

	}

	public String onTreeViewSuccessSelect() {
		String onSuccessSelect = null;
		onSuccessSelect = "function () {Windows.close('";
		onSuccessSelect += organTreeViewId() + "_win');}";
		return onSuccessSelect;

	}

	public String organTreeViewWincontainerOnCloseID() {
		return organTreeViewId() + "_containerOnClose";
	}

	public String onOrganTreeViewWinContainerOnCloseComplete() {
		return "function(oc) {" + getOrgansContainerId() + "Update();}";
	}

	//	public String organTreeWinId() {
	//		return getComponentId()+"_organTreeWin";
	//	}

	public String organTreeViewId() {
		return getComponentId() + "_organTreeView";
	}

	public Integer getExerciceInt() {
		return Integer.valueOf(getCurrentExercice());
	}

	public EOEb getSelectedOrganInTree() {
		return selectedOrganInTree;
	}

	public void setSelectedOrganInTree(EOEb selectedOrganInTree) {
		this.selectedOrganInTree = selectedOrganInTree;
		onOrganAjouter();
		selectedOrganInTree = null;
	}

	public String canCode() {
		return getEditingCodeAnalytique().canCode();
	}

	public String canLibelle() {
		return getEditingCodeAnalytique().canLibelle();
	}

	public void setCanCode(String code) {
		getEditingCodeAnalytique().setCanCode(code);
	}

	public void setCanLibelle(String lib) {
		getEditingCodeAnalytique().setCanLibelle(lib);
	}

	public Boolean isMontantNull() {
		return Boolean.valueOf(getEditingCodeAnalytique().canMontant() == null);
	}

	/**
	 * @return Le formatter a utiliser
	 */
	public NSNumberFormatter deviseFormatter() {
		if (deviseFormatter == null) {
			deviseFormatter = GFCUtilities.getDefaultDeviseFormatter(edc(), Integer.valueOf(getCurrentExercice()));
		}
		return deviseFormatter;
	}

	public CanalFormCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (CanalFormCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new CanalFormCtrl(this);
			}
		}
		return ctrl;
	}

	public Date maintenant() {
		return new Date();
	}

}
