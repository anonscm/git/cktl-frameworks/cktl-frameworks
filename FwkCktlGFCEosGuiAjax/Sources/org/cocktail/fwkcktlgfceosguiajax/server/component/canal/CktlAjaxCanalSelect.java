package org.cocktail.fwkcktlgfceosguiajax.server.component.canal;

import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.fwkcktlgfceos.server.metier.EOCodeAnalytique;
import org.cocktail.fwkcktlgfceosguiajax.server.component.ATreeViewComponent;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;

/**
 * @binding currentExercice (String)
 * @binding selection Objet EOCodeAnalytique
 * @binding showField
 * @binding id
 * @binding editingContext
 * @binding utilisateurPersId
 * @binding updateContainerID
 * @binding displayDeleteButton
 * @binding disabled
 * @binding value
 * @binding textFieldSize
 * @binding supprimerOnComplete
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAjaxCanalSelect extends ATreeViewComponent {

	private static final long serialVersionUID = 5431261586636009005L;

	/** Facultatif. Binding pour specifier la taille du champ (en colonnes). */
	public static final String TEXT_FIELD_SIZE_BDG = "textFieldSize";

	/** Facultatif. Binding pour specifier un container a mettre a jour qd la fenetre se ferme. */
	public static final String UPDATE_CONTAINER_ID_BDG = "updateContainerID";

	/** Facultatif. Binding pour specifier si le bouton supprimer doit etre affiche. Par defaut a true. */
	public static final String DISPLAY_DELETE_BUTTON_BDG = "displayDeleteButton";

	/** Facultatif. Binding pour specifier une fonction JavaScript a executer apres la suppression. */
	public static final String SUPPRIMER_ON_COMPLETE_BDG = "supprimerOnComplete";

	private static final String ON_SUCCESS_BDG = "onSuccessSelect";

	public static final String SELECTION_BDG = "selection";

	/** Affiche le champ texte */
	public static final String BINDING_showField = "showField";

	private Boolean resetTree = Boolean.FALSE;

	public CktlAjaxCanalSelect(WOContext context) {
		super(context);
	}

	public boolean synchronizesVariablesWithBindings() {
		return false;
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
	}

	public void setSelection(EOCodeAnalytique selection) {
		setValueForBinding(selection, SELECTION_BDG);
	}

	/*
	 * public String onSuccessRechercher() { String onSuccessRechercher = null; onSuccessRechercher = "function () {openWinCanal('";
	 * onSuccessRechercher += getComponentId() + "', '"; if (valueForBinding(TREE_VIEW_TITLE_BDG) != null) { onSuccessRechercher +=
	 * valueForBinding(TREE_VIEW_TITLE_BDG) + "', true"; } else { onSuccessRechercher += "Choisir un canal', true"; }
	 * 
	 * if (valueForBinding(TREE_VIEW_WIDTH_BDG) != null) { onSuccessRechercher += ", " + valueForBinding(TREE_VIEW_WIDTH_BDG); } else {
	 * onSuccessRechercher += ", null"; } if (valueForBinding(TREE_VIEW_HEIGHT_BDG) != null) { onSuccessRechercher += ", " +
	 * valueForBinding(TREE_VIEW_HEIGHT_BDG); } else { onSuccessRechercher += ", null"; } // if (valueForBinding(TREE_VIEW_CLASS_NAME_BDG) != null) {
	 * // onSuccessRechercher += ", '" + valueForBinding(TREE_VIEW_CLASS_NAME_BDG) + "'"; // } //else { onSuccessRechercher += ", null"; //} if
	 * (valueForBinding(UPDATE_CONTAINER_ID_BDG) != null) { onSuccessRechercher += ", '" + valueForBinding(UPDATE_CONTAINER_ID_BDG) + "'"; } else {
	 * onSuccessRechercher += ", null"; } onSuccessRechercher += ");}";
	 * 
	 * return onSuccessRechercher; }
	 */
	//	public WOActionResults onSuccessSelect() {
	//		if (hasBinding(ON_SUCCESS_BDG)) {
	//			valueForBinding(ON_SUCCESS_BDG);
	//		}
	//		CktlAjaxWindow.close(context(), canalTreeViewId());
	//		return null;
	//	}
	//	

	public WOActionResults onSelect() {
		CktlAjaxWindow.close(context(), canalTreeViewId());
		return null;
	}

	public String containerCanalSelectionneId() {
		if (showField() != null && showField())
			return getComponentId() + "_selection";
		else
			return null;
	}

	/**
	 * Callback appelé lorsque la modal dialog se ferme. On reset le treeview pour le réafficher de zéro lors de la prochaine ouverture.
	 */
	public void onClose() {
		setResetTree(true);
		//setIsTreeViewOpened(Boolean.FALSE);
	}

	public WOActionResults supprimerSelection() {
		setSelection(null);
		return null;
	}

	public String containerOnCloseID() {
		return getComponentId() + "_containerOnClose";
	}

	public String canalTreeViewId() {
		return getComponentId() + "_canalTreeView";
	}

	public Integer textFieldSize() {
		if (hasBinding(TEXT_FIELD_SIZE_BDG)) {
			return (Integer) valueForBinding(TEXT_FIELD_SIZE_BDG);
		}
		return Integer.valueOf(50);
	}

	public Boolean displayDeleteButton() {
		if (hasBinding(DISPLAY_DELETE_BUTTON_BDG)) {
			return (Boolean) valueForBinding(DISPLAY_DELETE_BUTTON_BDG);
		}
		return Boolean.TRUE;
	}

	public String supprimerOnComplete() {
		if (hasBinding(SUPPRIMER_ON_COMPLETE_BDG)) {
			return (String) valueForBinding(SUPPRIMER_ON_COMPLETE_BDG);
		}
		return null;
	}

	public Boolean getResetTree() {
		return resetTree;
	}

	public void setResetTree(Boolean resetTree) {
		this.resetTree = resetTree;
	}

	public Boolean showField() {
		if (hasBinding(BINDING_showField)) {
			return (Boolean) valueForBinding(BINDING_showField);
		}
		return Boolean.TRUE;
	}

	public EOCodeAnalytique getRootCodeAnalytique() {
		return (EOCodeAnalytique) EOCodeAnalytique.getCodeAnalytiquesRacine(edc()).objectAtIndex(0);
	}

}