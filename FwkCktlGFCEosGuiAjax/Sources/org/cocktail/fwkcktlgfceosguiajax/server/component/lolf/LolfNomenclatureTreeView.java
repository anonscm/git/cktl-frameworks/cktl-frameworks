package org.cocktail.fwkcktlgfceosguiajax.server.component.lolf;

import org.cocktail.fwkcktlgfceos.server.metier.EODestinationDepense;
import org.cocktail.fwkcktlgfceosguiajax.server.component.ATreeViewComponent;
import org.cocktail.fwkcktlgfceosguiajax.server.controler.LolfNomenclatureTreeViewCtrl;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;

/**
 * Arbre des actions lolf.
 * 
 * @binding mode (EOLolfNomenclatureAbstract.MODE_RECETTE ou EOLolfNomenclatureAbstract.MODE_DEPENSE)
 * @binding currentExercice
 * @binding onSelect
 * @binding ctrl
 * @binding selection
 * @binding treeQualifier
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class LolfNomenclatureTreeView extends ATreeViewComponent {

	private static final long serialVersionUID = 1L;
	public static final String BINDING_currentExercice = "currentExercice";
	public static final String BINDING_resetTree = "resetTree";
	public static final String BINDING_ctrl = "ctrl";
	public static final String BINDING_onSuccessSelect = "onSuccessSelect";
	public static final String BINDING_onSelect = "onSelect";
	private static final String BINDING_mode = "mode";

	private LolfNomenclatureTreeViewCtrl ctrl;
	private EODestinationDepense selection;
	private NSArray allowedOrgans = null;

	private String selectedElementId;

	public LolfNomenclatureTreeView(WOContext paramWOContext) {
		super(paramWOContext);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		if (getCtrl().getWocomponent() == null) {
			getCtrl().setWocomponent(this);
		}
		if (hasBinding(BINDING_resetTree)) {
			if (Boolean.TRUE.equals(valueForBinding(BINDING_resetTree))) {
				resetTree();
			}
			if (canSetValueForBinding(BINDING_resetTree)) {
				setValueForBinding(Boolean.FALSE, BINDING_resetTree);
			}
		}
		super.appendToResponse(response, context);

	}

	private void resetTree() {
		EODestinationDepense lolfNomenclature = (EODestinationDepense) valueForBinding(SELECTION_BDG);
		//getCtrl().rootLolfNomenclatureAbstract().children = null;
		getCtrl().reset();
		//Retrouver et afficher la selection dans l'arbre
		if (lolfNomenclature != selection()) {
			ctrl.setSelectedNodeLolfNomenclatureAbstract(ctrl.selectObjectInTree(ctrl.rootLolfNomenclatureAbstract(), lolfNomenclature));
		}
	}

	public String unAjaxTreeID() {
		return getComponentId() + "_ajaxTree";
	}

	public String containerAjaxTreeID() {
		return getComponentId() + "_containerAjaxTree";
	}

	public LolfNomenclatureTreeViewCtrl getCtrl() {
		if (ctrl == null) {
			if (hasBinding(BINDING_ctrl)) {
				ctrl = (LolfNomenclatureTreeViewCtrl) valueForBinding(BINDING_ctrl);
			}
			if (ctrl == null) {
				ctrl = new LolfNomenclatureTreeViewCtrl(this);
			}
		}
		return ctrl;
	}

	//
	//	public void setCtrl(LolfNomenclatureTreeViewCtrl ctrl) {
	//		this.ctrl = ctrl;
	//	}

	public EODestinationDepense treeRootObject() {
		return (EODestinationDepense) valueForBinding(TREE_ROOT_OBJECT_BDG);
	}

	public EOQualifier getQualifier() {
		EOQualifier qual = getTreeQualifier();
		//		if (qual != null) {
		//			if (getUserFiltreQualifier() != null) {
		//				qual = new EOAndQualifier(new NSArray(new Object[]{qual, getUserFiltreQualifier()}));
		//			}
		//		}
		//		else {
		//			qual = getUserFiltreQualifier();
		//		}

		return qual;
	}

	public EODestinationDepense selection() {
		return selection;
	}

	public void setSelection(EODestinationDepense selection) {
		this.selection = selection;
		setValueForBinding(selection, SELECTION_BDG);
	}

	public String getCurrentExercice() {
		return (String) valueForBinding(BINDING_currentExercice);
	}

	public NSArray getAllowedObjects() {
		if (allowedOrgans == null) {
			allowedOrgans = getAppUser().getOrgansAutorisees(edc(), getCurrentExercice());
		}
		return allowedOrgans;
	}

	public WOActionResults onSelect() {
		getCtrl().setSelectedNodeLolfNomenclatureAbstract(getCtrl().unLolfNomenclatureAbstract());
		if (hasBinding(BINDING_onSelect)) {
			performParentAction(BINDING_onSelect);
		}
		return null;
	}

	public Boolean isUnNodeSelected() {
		return Boolean.valueOf(getCtrl().unLolfNomenclatureAbstract().equals(getCtrl().getSelectedNodeLolfNomenclatureAbstract()));
	}

	public String functionTreeViewCompleted() {
		return getComponentId() + "_onTreeViewCompleted=function(){var el = document.getElementById('" + selectedElementId() + "'); if (el != null) {el.scrollIntoView(true);  };}";
	}

	public String onTreeViewCompleted() {
		return getComponentId() + "_onTreeViewCompleted";
	}

	public String selectedElementId() {
		if (selectedElementId == null) {
			selectedElementId = unAjaxTreeID() + "_selected";
		}
		return selectedElementId;
	}

	public void setSelectedElementId(String id) {
		//return unAjaxTreeID()+"_selected";
		selectedElementId = id;
	}

	public String onSuccessSelect() {
		return (String) valueForBinding(BINDING_onSuccessSelect);
	}

	public String getMode() {
		return (String) valueForBinding(BINDING_mode);
	}

}
