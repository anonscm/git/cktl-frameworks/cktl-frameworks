/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportingguiajax.components;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.CktlAbstractReportingTaskThread;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;
import com.webobjects.appserver.WOResponse;
import com.webobjects.foundation.NSData;

import er.ajax.AjaxUtils;
import er.ajax.CktlAjaxUtils;
import er.extensions.foundation.ERXValueUtilities;

/**
 * Composant permettant de controler visuellement l'execution d'un report.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportingExecMonitor extends CktlAjaxWOComponent {
	private static final long serialVersionUID = 1L;
	private static final String BINDING_reporter = "reporter";
	private static final String BINDING_resultFileName = "resultFileName";
	private static final String BINDING_resultMimeType = "resultMimeType";
	private static final String BINDING_reporterProgress = "reporterProgress";
	private static final String BINDING_refreshTime = "refreshTime";

	private static final String BINDING_AUTO_DOWNLOAD = "autoDownload";
	private static final String BINDING_JS_AFTER_AUTO_DOWNLOAD = "jsAfterAutoDownload";
	
	private CktlAbstractReporter lastReporter;
	private String _refreshTime;
	private String BINDING_onRefreshAction = "onRefreshAction";

	public CktlReportingExecMonitor(WOContext context) {
		super(context);
	}

	@Override
	public void appendToResponse(WOResponse response, WOContext context) {
		super.appendToResponse(response, context);
		CktlAjaxUtils.addStylesheetResourceInHead(context, response, "FwkCktlReportingGuiAjax.framework", "css/FwkCktlReportingGuiAjax.css");
		//		if (notRunning()) {
		//			AjaxUtils.appendScriptHeader(response);
		//			response.appendContentString("if ($('" + getMainContainerId() + "')!=null && $('" + getMainContainerId() + "').updater != null) {clearTimeout( $('" + getMainContainerId() + "').updater.timer); $('" + getMainContainerId() + "').updater.timer=undefined;}");
		//			AjaxUtils.appendScriptFooter(response);
		//		}

	}

	public CktlAbstractReportingTaskThread reportingTaskThread() {
		return (getReporter() != null ? getReporter().getCurrentReportingTaskThread() : null);
	}

	public CktlAbstractReporter getReporter() {
		CktlAbstractReporter res = (CktlAbstractReporter) valueForBinding(BINDING_reporter);
		if (res != null && (lastReporter == null || !(lastReporter.equals(res)))) {
			lastReporter = res;
			resetComp();
		}
		return res;
	}

	public void resetComp() {

	}

	public CktlAbstractReporterAjaxProgress getReporterProgress() {
		return (CktlAbstractReporterAjaxProgress) valueForBinding(BINDING_reporterProgress);
	}

	//	public WOActionResults stopThread() {
	//		reportingTaskThread().interrupt();
	//		return null;
	//	}

	public WOActionResults openResultFile() {
		try {
			NSData res = getReporter().printDifferedGetDataResult();
			String fileName = getResultFileName();
			return CktlAbstractReporter.downloadFile(res, fileName, getResultMimeType());
			//			
			//			
			//			CktlDataResponse resp = new CktlDataResponse();
			//			resp.setContent(res);
			//			resp.setContentType(getResultMimeType());
			//			resp.setFileName(fileName);
			//			return resp;
		} catch (Throwable e) {
			e.printStackTrace();
			mySession().addSimpleErrorMessage("Erreur", e);
			//	setErreurSaisieMessage(e.getLocalizedMessage());
		}
		return null;

	}

	private String getResultMimeType() {
		return stringValueForBinding(BINDING_resultMimeType, "");
	}

	private String getResultFileName() {
		return stringValueForBinding(BINDING_resultFileName, "pasdenom");
	}

	public Boolean isStopped() {
		return Boolean.valueOf(getReporter() == null || getReporter().getCurrentReportingTaskThread().isInterrupted() || !getReporter().getCurrentReportingTaskThread().isAlive());
	}

	public Boolean isStarted() {
		return Boolean.valueOf(getReporter() != null && getReporter().isStarted());
	}

	public WOActionResults containerRefreshed() {
		if (getReporter().isResultReady()) {
			if (isAutoDownloadEnabled()) {
				AjaxUtils.appendScript(context(), "<script>window.location.href = $('openFileLinkId').readAttribute('href');</script>");
				if (getJsAfterAutoDownload() != null) {
					AjaxUtils.appendScript(context(), "<script>" + getJsAfterAutoDownload() + "</script>");
				}
			}
		}
		return null;
	}

	public WOActionResults cancel() {
		getReporter().stop();
		return null;
	}

	public String refreshTime() {
		if (_refreshTime == null) {
			double tempValue = ERXValueUtilities.doubleValueWithDefault(valueForBinding(BINDING_refreshTime), 2000);
			_refreshTime = String.valueOf(tempValue / 1000);
		}
		if (notRunning()) {
			_refreshTime = null;
		}
		return _refreshTime;
	}

	public String progressStyle() {
		String style = null;
		CktlAbstractReporterAjaxProgress progress = getReporterProgress();
		if (progress != null) {
			if (!progress.isSucceeded()) {
				int width = (int) (progress.percentage() * 100);
				style = "width:" + width + "%;";
			}
			else {
				style = "width:100%;";
			}
		}
		return style;
	}

	public String progressClass() {
		String result = "FwkCktlReportingGuiAjax_ProgressAmount";
		return result;
	}

	public String containerRefreshFunction() {
		StringBuffer sb = new StringBuffer();
		if (getReporter() == null || getReporter().getCurrentReportingTaskThread() == null || getReporter().getCurrentReportingTaskThread().isInterrupted() || getReporterProgress().isDone() || getReporterProgress().isCanceled() || getReporterProgress().isFailed()) {
			//			sb.append("if ($('" + getMainContainerId() + "')  && $('" + getMainContainerId() + "').updater && $('" + getMainContainerId() + "').updater.option ) {alert('stop');" + getMainContainerId() + ".updater.stop();}");
			sb.append("if ($('" + getMainContainerId() + "') !=null ) {" + getMainContainerId() + "Stop();}");
		}
		if (updateContainerID() != null) {
			sb.append("if ($('" + updateContainerID() + "') !=null ) {" + updateContainerID() + "Update();}");
		}
		if (sb.toString().length() > 0) {
			return sb.toString();
		}
		return null;
	}

	public String getWinErreurId() {
		return getComponentId() + "_erreurWin";
	}

	//	public String getJSOpenWinErreur() {
	//		return "openCAW_" + getWinErreurId() + "('" + getWinErreurTitle() + "');";
	//	}

	public String getWinErreurTitle() {
		return "Erreur";
	}

	public Boolean showCancel() {
		if (getReporter() != null && getReporter().getCurrentReportingTaskThread() != null 
				&& getReporter().getCurrentReportingTaskThread().isAlive() && !getReporter().getCurrentReportingTaskThread().isInterrupted()) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	public Boolean show() {
		return Boolean.valueOf(getReporter() != null);
	}

	public Boolean notRunning() {
		return Boolean.valueOf(!showCancel().booleanValue());
	}

	public String getErreurMsgComplet() {
		if (getReporter() == null) {
			return null;
		}
		if (getReporter().getLastPrintError() == null) {
			return null;
		}
		Throwable e = getReporter().getLastPrintError();
		StringBuffer sb = new StringBuffer();
		do {
			sb.append("<b>" + e.getLocalizedMessage() + "</b><br/>\n");
			final Writer result = new StringWriter();
			final PrintWriter printWriter = new PrintWriter(result);
			e.printStackTrace(printWriter);
			sb.append(result.toString());
			e = e.getCause();
			if (e != null) {
				sb.append("\n<br/>\n<br/>");
			}
		} while (e != null);

		return sb.toString();
	}

	public WOActionResults onRefreshAction() {
		if (hasBinding(BINDING_onRefreshAction)) {
			return parent().performParentAction((String) valueForBinding(BINDING_onRefreshAction));
		}
		return null;
	}

	public String getExecutionTime() {
		String res = "";
		if (getReporter().getRunningTime() != null) {
			res = " (" + getReporter().getRunningTimeAsString() + ")";
		}
		return res;
	}

	public Boolean isAutoDownloadEnabled() {
		return  booleanValueForBinding(BINDING_AUTO_DOWNLOAD, false);
	}
	
	public String getJsAfterAutoDownload() {
		return stringValueForBinding(BINDING_JS_AFTER_AUTO_DOWNLOAD, null);
	}
	

}
