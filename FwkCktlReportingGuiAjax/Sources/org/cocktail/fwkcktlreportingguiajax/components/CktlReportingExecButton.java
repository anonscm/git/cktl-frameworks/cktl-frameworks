package org.cocktail.fwkcktlreportingguiajax.components;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.fwkcktlajaxwebext.serveur.component.CktlAjaxWindow;
import org.cocktail.reporting.server.CktlAbstractReporter;

import com.webobjects.appserver.WOActionResults;
import com.webobjects.appserver.WOContext;

public class CktlReportingExecButton extends CktlAjaxWOComponent {
	
	private static final String BINDING_action = "action";
	private static final String BINDING_BtDisabled = "disabled";
	private static final String BINDING_REPORTER = "reporter";
	
    public CktlReportingExecButton(WOContext context) {
        super(context);
    }
    
    
    public String printWindowId() {
    	return getComponentId() + "_printWindow";
    }
    
    public String reportingMonitorId() {
    	return getComponentId() + "_reportingMonitor";
    }


	public boolean isDisabled() {
		return booleanValueForBinding(BINDING_BtDisabled, false);
	}


	public void setDisabled(boolean isDisabled) {
		setValueForBinding(isDisabled, BINDING_BtDisabled);
	}
    
	
	public String getTelechargementContainerId() {
		return getComponentId() + "_telechargementContainer";
	}
	
	public WOActionResults closeWindow() {
		if (getReporter().isResultReady()) {
			CktlAjaxWindow.close(context(), printWindowId());
		}
		return doNothing();
	}
	
	public CktlAbstractReporter getReporter() {
		return (CktlAbstractReporter) valueForBinding(BINDING_REPORTER);
	}
    
	
	public String getJsForWindowClose() {
		return "CAW.close('" + printWindowId() + "_win');";
	}
    
   
    
    
}