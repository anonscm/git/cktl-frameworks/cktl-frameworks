/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportingguiajax.components;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Iterator;

import org.cocktail.fwkcktlajaxwebext.serveur.CktlAjaxWOComponent;
import org.cocktail.reporting.server.exception.CktlReportingException;

import com.webobjects.appserver.WOContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 * Composant permettant d'afficher une exception generee lors de l'execution d'un report.
 * 
 * @binding exception CktlReportingException
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlReportingExceptionViewer extends CktlAjaxWOComponent {

	private static final long serialVersionUID = 1L;
	private static final String BINDING_exception = "exception";

	public CktlReportingExceptionViewer(WOContext context) {
		super(context);
	}

	public String getWinErreurId() {
		return getComponentId();
	}

	public CktlReportingException reportingException() {
		if (valueForBinding(BINDING_exception) instanceof CktlReportingException) {
			return (CktlReportingException) valueForBinding(BINDING_exception);
		}
		return null;
	}

	public Throwable exception() {
		return (Throwable) valueForBinding(BINDING_exception);
	}

	public Boolean isReportingException() {
		return Boolean.valueOf(reportingException() != null);
	}

	public String getStackTraceAsString() {
		if (isReportingException()) {
			return reportingException().stackTraceAsString();
		}
		else {
			Throwable e = exception();
			StringBuffer sb = new StringBuffer();
			do {
				sb.append("<b>" + e.getLocalizedMessage() + "</b><br/>\n");
				final Writer result = new StringWriter();
				final PrintWriter printWriter = new PrintWriter(result);
				e.printStackTrace(printWriter);
				sb.append(result.toString());
				e = e.getCause();
				if (e != null) {
					sb.append("\n<br/>\n<br/>");
				}
			} while (e != null);

			return sb.toString();
		}
	}

	public NSArray<String> getErreurs() {
		if (reportingException() == null) {
			return NSArray.emptyArray();
		}
		NSMutableArray<String> res = new NSMutableArray<String>();
		res.addObject(dressExceptionAsHtml(reportingException()));
		return res.immutableClone();
	}

	private String dressExceptionAsHtml(CktlReportingException e) {
		StringBuilder sb = new StringBuilder();
		sb.append("<b>" + e.getLocalizedMessage() + "</b><br>");
		sb.append("<ul>");
		if (e.getFileName() != null) {
			sb.append("<li>");
			sb.append("<b>Fichier :</b>");
			sb.append(e.getFileName());
			sb.append("</li>");
		}
		if (e.getCause() != null) {
			sb.append("<li>");
			sb.append("<b>Complément :</b>");
			sb.append(e.getCause().getLocalizedMessage());
			sb.append("</li>");
		}
		if (e.getQuery() != null) {
			sb.append("<li>");
			sb.append("<b>Requête :</b>");
			sb.append(e.getQuery());
			sb.append("</li>");
		}
		if (e.getComplement() != null) {
			sb.append("<li>");
			sb.append("<b>Complément :</b>");
			sb.append(e.getComplement());
			sb.append("</li>");
		}
		if (e.getParams() != null) {
			sb.append("<li>");
			sb.append("<b>Paramètres :</b>");
			Iterator<String> it = e.getParams().keySet().iterator();
			while (it.hasNext()) {
				String key = (String) it.next();
				sb.append("<b>" + key + "</b>" + "=" + e.getParams().get(key) + "<br/>");
			}
			sb.append(e.getComplement());
			sb.append("</li>");
		}
		sb.append("</ul>");
		return sb.toString();
	}

}
