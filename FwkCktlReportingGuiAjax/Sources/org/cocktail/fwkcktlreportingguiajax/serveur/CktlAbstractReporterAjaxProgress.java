/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlreportingguiajax.serveur;

import java.math.BigDecimal;

import org.cocktail.reporting.server.ICktlReportingTaskListener;

import er.ajax.AjaxProgress;

/**
 * Recoit les messages provenant de la génération des rapports. Permet d'avoir les infos sur la progression de la tache.
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */
public class CktlAbstractReporterAjaxProgress extends AjaxProgress implements ICktlReportingTaskListener {

	public CktlAbstractReporterAjaxProgress(int maximum) {
		super(maximum);
		init();

	}

	public CktlAbstractReporterAjaxProgress(String id, int maximum) {
		super(id, maximum);
		init();
	}

	protected void init() {
		//setLastException(null);
		setStatus(null);
	}

	public synchronized void afterReportStarted() {
		this.setStatus("Construction du rapport");
		setValue(5);
	}

	public synchronized void afterReportBuild(int pageTotalCount) {
		this.setStatus("Rapport créé");
		setValue(50);
		//this.setMaximum(maximum)
	}

	public synchronized void afterPageExport(int pageNum, int pageCount) {
		this.setStatus("Export en cours " + pageNum + "/" + pageCount);
		System.out.println("Export en cours " + pageNum + "/" + pageCount);

		double d = 50 * (pageNum - 1) / pageCount;
		this.setValue(50 + BigDecimal.valueOf(d).intValue());
		System.out.println(value());
	}

	public synchronized void afterReportExport() {
		this.setStatus("Export terminé");
		Thread.yield();
		this.setDone(true);

	}

	public synchronized void afterReportFinish() {
		this.setStatus("Report terminé");
	}

	public synchronized void afterDataSourceCreated() {
		this.setStatus("Source de données créée");
	}

	public synchronized void trace(String s) {
		System.out.println(s);
		//setStatus(s);
	}

	public synchronized void afterException(Throwable e) {
		setFailure(e);
		if (e instanceof NullPointerException) {
			setStatus("NullPointerException");
		}
		else {
			setStatus(e.getLocalizedMessage());
		}
	}

	@Override
	public void setDone(boolean done) {
		super.setDone(done);
	}

	public void afterReportCanceled() {
		setStatus("Annulé par l'utilisateur");
		cancel();
	}

}
