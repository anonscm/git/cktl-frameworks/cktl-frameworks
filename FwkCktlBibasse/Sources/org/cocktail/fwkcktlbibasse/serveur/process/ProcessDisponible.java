/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureDisponible;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class ProcessDisponible extends Process {

	public static BigDecimal disponible(_CktlBasicDataBus databus, EOEditingContext ed, EOOrgan organ, EOTypeCredit typeCredit, EOExercice exercice) throws Exception {
		BigDecimal valeur=new BigDecimal(0.0);

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			boolean pbProcedure = ProcedureDisponible.enregistrer(databus, organ, typeCredit, exercice);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}
			valeur=(BigDecimal)dico.valueForKey(ProcedureDisponible.PROCEDURE_RETOUR_DISPONIBLE);

			databus.commitTransaction();

			return valeur;
			//commande.setCommIdProc((Number) dico.valueForKey("010_a_comm_id"));
		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw e;
		}	
	}
}
