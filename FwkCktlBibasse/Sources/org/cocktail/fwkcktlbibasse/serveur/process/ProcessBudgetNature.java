/*
 * Created on 17 juil. 2004
 */
package org.cocktail.fwkcktlbibasse.serveur.process;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.Vector;

import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetParametre;
import org.cocktail.fwkcktlbibasse.serveur.metier.BudgetNature;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetParametres;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableDictionary;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class ProcessBudgetNature extends Process {

	public static String EXCEDENT_FONCTIONNEMENT = "EXCEDENT_FONCTIONNEMENT";
	public static String DEFICIT_FONCTIONNEMENT = "DEFICIT_FONCTIONNEMENT";
	public static String CAF = "CAF";
	public static String IAF = "IAF";
	public static String AUGMENTATION_FOND_ROULEMENT = "AUGMENTATION_FOND_ROULEMENT";
	public static String DIMINUTION_FOND_ROULEMENT = "DIMINUTION_FOND_ROULEMENT";

	public static NSDictionary montantCalculPropositions(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOOrgan organ) throws Exception {
		NSMutableArray<EOQualifier> organsQualifier = new NSMutableArray<EOQualifier>();
		if (organ != null) {
			NSArray<EOOrgan> organs = FinderOrgan.getAllOrganFils(organ);

			organs = organs.arrayByAddingObject(organ);
			for (EOOrgan eoOrgan : organs) {
				EOQualifier qual = ERXQ.equals(ERXQ.keyPath(EOPropositionBudgetNatureLolf.PROPOSITION_BUDGET_KEY, EOPropositionBudget.ORGAN_KEY), eoOrgan);
				organsQualifier.add(qual);
			}
//			while (organ.organPere() != null && !organ.equals(organ.organPere())) {
//				EOQualifier qual = ERXQ.equals(ERXQ.keyPath(EOPropositionBudgetNatureLolf.PROPOSITION_BUDGET_KEY, EOPropositionBudget.ORGAN_KEY), organ);
//				organsQualifier.add(qual);
//				organ = organ.organPere();
//			}

		}
		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(ERXQ.keyPath(EOPropositionBudgetNatureLolf.PROPOSITION_BUDGET_KEY, EOPropositionBudget.BUDGET_SAISIE_KEY), budgetSaisie),
				ERXQ.or(organsQualifier));
		NSArray propositions = EOPropositionBudgetNatureLolf.fetchAll(ec, qualifier);
		// Pour prendre en compte les objets inseres dans edc mais pas encore en base
		propositions = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, propositions, EOPropositionBudgetNatureLolf.ENTITY_NAME, qualifier, null, true,
				true, true, false, false, false);
		return calculBudgetNature(ec, propositions);
	}

	/**
	 * 
	 * @param ec
	 * @param arrayEOBudgetSaisieNature
	 * @param resultatBrutDepenseCompte
	 * @param resultatBrutRecetteCompte
	 * @param cafCompte
	 * @param iafCompte
	 * @param augmentationFDRCompte
	 * @param diminutionFDRCompte
	 * @throws Exception
	 */
	public static NSDictionary calculBudgetNature(EOEditingContext ec, NSArray arrayBudgetNature) throws Exception {
		NSMutableDictionary dico = new NSMutableDictionary();

		// parametres
		boolean compte777=false;
		boolean compte775=false;
		
		if (arrayBudgetNature!=null && arrayBudgetNature.count()>0 && ((BudgetNature)arrayBudgetNature.objectAtIndex(0)).exercice()!=null) {
			BudgetNature bud=(BudgetNature)arrayBudgetNature.objectAtIndex(0);
			EOBudgetParametres param775=FinderBudgetParametre.getParametre(ec, EOBudgetParametres.param_775_FDR, bud.exercice());
			EOBudgetParametres param777=FinderBudgetParametre.getParametre(ec, EOBudgetParametres.param_777_EN_PRODUIT, bud.exercice());
			
			if (param775!=null && param775.bparValue()!=null && param775.bparValue().equals("OUI"))
				compte775=true;
			if (param777!=null && param777.bparValue()!=null && param777.bparValue().equals("OUI"))
				compte777=true;
		}

		
		// calculs
		BigDecimal chap60_66 = calculerSommechap60_66(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal chap67_68 = calculerSommechap67_68(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal chap675_68 = calculerSommechap675_68(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal chap70_76 = calculerSommechap70_76(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal chap77_78 = calculerSommechap77_78(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal chap775_776_777_78 = calculerSommechap771_775_776_777_78(arrayBudgetNature, BudgetNature.MONTANT, compte777);
		BigDecimal chap775=new BigDecimal(0);
		if (compte775)
			chap775=calculerSommechap775(arrayBudgetNature, BudgetNature.MONTANT);
		
		BigDecimal classe2 = calculerSommeclasse2(arrayBudgetNature, BudgetNature.MONTANT);
		BigDecimal classe1 = calculerSommeclasse1(arrayBudgetNature, BudgetNature.MONTANT);

		// Brut Depense = Total Classe 7 - Total Classe 6
		BigDecimal resultatBrutDepense = (chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68));
		dico.setObjectForKey(zeroSiNegatif(resultatBrutDepense), EXCEDENT_FONCTIONNEMENT);

		// Brut Recette = Total Classe 6 - Total Classe 7
		BigDecimal resultatBrutRecette = (chap60_66.add(chap67_68)).subtract(chap70_76.add(chap77_78));
		dico.setObjectForKey(zeroSiNegatif(resultatBrutRecette), DEFICIT_FONCTIONNEMENT);

		// CAF = (Total_7 - Total_6) + Total(67_68) - Total(77_78)
		BigDecimal caf = (chap70_76.add(chap77_78)).subtract(chap60_66.add(chap67_68)).add(chap675_68).subtract(chap775_776_777_78);

		// IAF = - CAF
		BigDecimal iaf = zeroSiNegatif(caf.multiply(new BigDecimal(-1)));
		caf = zeroSiNegatif(caf);

		dico.setObjectForKey(caf, CAF);
		dico.setObjectForKey(iaf, IAF);

		// Augmentation FDR = zeroSiNegatif ( (CAF+classe1) - (IAF+classe2) )
		BigDecimal augmentationFDRCompte = zeroSiNegatif((caf.add(classe1).add(chap775)).subtract(iaf.add(classe2)));
		dico.setObjectForKey(augmentationFDRCompte, AUGMENTATION_FOND_ROULEMENT);

		// Diminution FDR = zeroSiNegatif ( (IAF+classe2) - (CAF+classe1) )
		BigDecimal diminutionFDRCompte = zeroSiNegatif((iaf.add(classe2)).subtract(caf.add(classe1).add(chap775)));
		dico.setObjectForKey(diminutionFDRCompte, DIMINUTION_FOND_ROULEMENT);

		return dico;
	}

	/**
	 * 
	 * @param b
	 * @return
	 * @throws Exception
	 */
	public static boolean inferieur(BigDecimal a, BigDecimal b) {
		if (a.compareTo(b) == -1)
			return true;
		return false;
	}

	public static BigDecimal calcSommeOfBigDecimals(final NSArray array, final String keyName) {
		return calcSommeOfBigDecimals(array.vector(), keyName);
	}

	public static BigDecimal calcSommeOfBigDecimals(final Vector array, final String keyName) {
		BigDecimal res = new BigDecimal(0.0).setScale(2);
		Iterator iter = array.iterator();
		while (iter.hasNext()) {
			EOEnterpriseObject element = (EOEnterpriseObject) iter.next();
			res = res.add((BigDecimal) element.valueForKey(keyName));
		}
		return res;
	}

	private static BigDecimal zeroSiNegatif(BigDecimal b) throws Exception {
		if (inferieur(b, new BigDecimal(0.00)))
			return new BigDecimal(0.00).setScale(2);
		return b;
	}

	/**
	 * 
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private static BigDecimal calculerSommechap60_66(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] { "60", "61", "62", "63", "64", "65", "66", "69" });
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			String currentCompte = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer().pcoNum().substring(0, 2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}

		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private static BigDecimal calculerSommechap67_68(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] { "67", "68" });
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOPlanComptableExer planco = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer();
			String currentCompte = planco.pcoNum().substring(0, 2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private static BigDecimal calculerSommechap675_68(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] { "675", "68" });
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOPlanComptableExer planco = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer();
			String currentCompte2 = planco.pcoNum().substring(0, 2);
			String currentCompte3 = currentCompte2;

			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0, 3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private static BigDecimal calculerSommechap70_76(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] { "70", "71", "72", "73", "74", "75", "76", "79" });
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			String currentCompte = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer().pcoNum().substring(0, 2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private static BigDecimal calculerSommechap77_78(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences = new NSArray(new String[] { "77", "78" });
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			String currentCompte = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer().pcoNum().substring(0, 2);

			if (comptesReferences.containsObject(currentCompte))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	/**
	 * 
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private static BigDecimal calculerSommechap771_775_776_777_78(NSArray arrayEOBudgetSaisieNature, String columnName, boolean compte777) {
		NSArray comptesReferences;
		
		if (compte777)
			comptesReferences = new NSArray(new String[] { "775", "776", "78" });
		else
			comptesReferences = new NSArray(new String[] { "775", "776", "777", "78" });

		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOPlanComptableExer planco = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer();
			String currentCompte2 = planco.pcoNum().substring(0, 2);
			String currentCompte3 = currentCompte2;

			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0, 3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	private static BigDecimal calculerSommechap775(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSArray comptesReferences= new NSArray(new String[] { "775"});

		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOPlanComptableExer planco = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).planComptableExer();
			String currentCompte2 = planco.pcoNum().substring(0, 2);
			String currentCompte3 = currentCompte2;

			if (planco.pcoNum().length() > 2)
				currentCompte3 = planco.pcoNum().substring(0, 3);

			if (comptesReferences.containsObject(currentCompte2) || comptesReferences.containsObject(currentCompte3))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	/**
	 * 
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private static BigDecimal calculerSommeclasse1(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOTypeCredit typeCredit = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).typeCredit();

			if (typeCredit.tcdType().equals("RECETTE") && typeCredit.tcdSect().equals("2"))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}

	/**
	 * 
	 * @param arrayEOBudgetSaisieNature
	 * @return
	 */
	private static BigDecimal calculerSommeclasse2(NSArray arrayEOBudgetSaisieNature, String columnName) {
		NSMutableArray pourLesComptes = new NSMutableArray();

		for (int i = 0; i < arrayEOBudgetSaisieNature.count(); i++) {
			EOTypeCredit typeCredit = ((BudgetNature) arrayEOBudgetSaisieNature.objectAtIndex(i)).typeCredit();

			if (typeCredit.tcdType().equals("DEPENSE") && typeCredit.tcdSect().equals("2"))
				pourLesComptes.addObject(arrayEOBudgetSaisieNature.objectAtIndex(i));
		}
		return calcSommeOfBigDecimals(pourLesComptes, columnName);
	}
}
