/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.process;

import java.math.BigDecimal;

import org.cocktail.fwkcktlbibasse.serveur.finder.Finder;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetSaisie;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderTypeSaisie;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisieGestion;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisieNature;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie;
import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureConsoliderSaisieBudget;
import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureVoterSaisieBudget;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;

public class ProcessBudgetSaisie extends Process {


	public static EOBudgetSaisie ouvrirBudgetSaisie(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie, EOUtilisateur utilisateur) throws Exception {

		// Si un budget est deja en cours de saisie, on ne peut pas en creer un nouveau
		EOBudgetSaisie currentBudgetSaisie=FinderBudgetSaisie.findBudgetSaisieEnCours(ec, exercice);
		if (currentBudgetSaisie!=null)
			throw new Exception("Un budget est deja en cours de saisie, vous ne pouvez pas en creer un nouveau");

		// Verification du typeSaisie
		NSArray budgetSaisies=FinderBudgetSaisie.findBudgetsSaisieForExercice(ec, exercice, null);

		if (typeSaisie==null) {
			if (budgetSaisies==null || budgetSaisies.count()==0)
				typeSaisie=FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_INITIAL);
			else 
				typeSaisie=FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_DBM);
		} else {
			if (budgetSaisies==null || budgetSaisies.count()==0) {
				if (!typeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_INITIAL))
					typeSaisie=FinderTypeSaisie.findTypeSaisie(ec, EOTypeSaisie.SAISIE_INITIAL);
			} else {
				if (typeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_INITIAL) || typeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_PROVISOIRE) ||
						typeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_RELIQUAT)) {
					NSArray array=FinderBudgetSaisie.findBudgetsSaisieForExercice(ec, exercice, typeSaisie);
					if (array.count()>0)
						throw new Exception("Un budget "+typeSaisie.tysaLibelle()+" existe deja pour l'exercice "+exercice.exeExercice()+", vous ne pouvez pas en creer un autre du meme type");
				}
			}
		}

		EOBudgetSaisie budgetSaisie=EOBudgetSaisie.createEOBudgetSaisie(ec, new NSTimestamp(), exercice, FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE), typeSaisie, utilisateur);

		String libelle="BUDGET "+typeSaisie.tysaLibelle();
		if (typeSaisie.tysaLibelle().equals(EOTypeSaisie.SAISIE_DBM)) {
			NSArray array=FinderBudgetSaisie.findBudgetsSaisieForExercice(ec, exercice, typeSaisie);
			libelle="DBM "+(array.count()+1);
		}
		budgetSaisie.setBdsaLibelle(libelle);

		if (budgetSaisies!=null && budgetSaisies.count()>0)
			initialiserBudgetSaisie(ec, budgetSaisie, budgetSaisiePrecedent(budgetSaisies));

		return budgetSaisie;
	}

	private static EOBudgetSaisie budgetSaisiePrecedent(NSArray arrayBudgetSaisies) {
		if (arrayBudgetSaisies==null || arrayBudgetSaisies.count()==0)
			return null;
		return (EOBudgetSaisie)Finder.tableauTrie(arrayBudgetSaisies, FinderBudgetSaisie.sortParDateVoteDescendant()).objectAtIndex(0);
	}

	public static void initialiserBudgetSaisie(EOEditingContext ec, EOBudgetSaisie budgetSaisie, EOBudgetSaisie budgetSaisiePrecedent) throws Exception {
		//NATURE
		EOQualifier qualSaisieNature = ERXQ.equals(EOBudgetSaisieNature.BUDGET_SAISIE_KEY, budgetSaisiePrecedent);
		NSArray arraySaisieNature=Finder.fetchArray(ec, EOBudgetSaisieNature.ENTITY_NAME,
				EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@", new NSArray(budgetSaisiePrecedent),null,false);
		arraySaisieNature = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieNature, EOBudgetSaisieNature.ENTITY_NAME, qualSaisieNature, null,
				true, true, true, false, false, false);

		for (int i=0; i<arraySaisieNature.count(); i++) {
			EOBudgetSaisieNature budgetSaisieNature=(EOBudgetSaisieNature)arraySaisieNature.objectAtIndex(i);
			EOBudgetSaisieNature.createEOBudgetSaisieNature(ec, budgetSaisieNature.bdsnVote(), new BigDecimal(0.0), budgetSaisieNature.bdsnVote(), 
					budgetSaisie, budgetSaisie.exercice(), budgetSaisieNature.organ(), budgetSaisieNature.planComptable(), 
					budgetSaisieNature.typeCredit(), FinderTypeEtat.getTypeEtat(ec, EOBudgetSaisie.ETAT_EN_COURS));			
		}

		//GESTION
		EOQualifier qualSaisieGestion = ERXQ.equals(EOBudgetSaisieGestion.BUDGET_SAISIE_KEY, budgetSaisiePrecedent);
		NSArray arraySaisieGestion=Finder.fetchArray(ec, EOBudgetSaisieGestion.ENTITY_NAME,
				EOBudgetSaisieGestion.BUDGET_SAISIE_KEY+"=%@", new NSArray(budgetSaisiePrecedent),null,false);
		arraySaisieGestion = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieGestion, EOBudgetSaisieGestion.ENTITY_NAME, qualSaisieGestion, null,
				true, true, true, false, false, false);

		for (int i=0; i<arraySaisieGestion.count(); i++) {
			EOBudgetSaisieGestion budgetSaisieGestion=(EOBudgetSaisieGestion)arraySaisieGestion.objectAtIndex(i);
			EOBudgetSaisieGestion.createEOBudgetSaisieGestion(ec, budgetSaisieGestion.bdsgVote(), new BigDecimal(0.0), budgetSaisieGestion.bdsgVote(),
					budgetSaisie, budgetSaisie.exercice(), budgetSaisieGestion.organ(), budgetSaisieGestion.typeAction(), 
					budgetSaisieGestion.typeCredit(), FinderTypeEtat.getTypeEtat(ec, EOBudgetSaisie.ETAT_EN_COURS));
		} 
	}

	public static void calculerConsolidation(_CktlBasicDataBus databus, EOEditingContext ed, EOBudgetSaisie budgetSaisie) throws Exception {
		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			boolean pbProcedure = ProcedureConsoliderSaisieBudget.enregistrer(databus, budgetSaisie);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw e;
		}	
	}

	public static void voter(_CktlBasicDataBus databus, EOEditingContext ed, EOBudgetSaisie budgetSaisie, EOUtilisateur utilisateur) throws Exception {
		if (!budgetSaisie.isVotable())
			throw new Exception("La saisie du budget est encore en cours, on ne peut pas voter !");

		try {
			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			boolean pbProcedure = ProcedureVoterSaisieBudget.enregistrer(databus, budgetSaisie);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure) {
				throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			}

			databus.commitTransaction();
			
			budgetSaisie.voter(ed, utilisateur);
		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw e;
		}	
	}
}
