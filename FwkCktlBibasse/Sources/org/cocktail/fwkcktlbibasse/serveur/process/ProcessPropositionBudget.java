/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.process;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf;
import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureAjouterPropositionBudget;
import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureAjouterPropositionBudgetNatureLolf;
import org.cocktail.fwkcktlbibasse.serveur.procedure.ProcedureSupprimerPropositionBudget;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSDictionary;

public class ProcessPropositionBudget extends Process {

	public static void enregistrer(_CktlBasicDataBus databus, EOEditingContext ed, EOPropositionBudget propositionBudget, EOUtilisateur utilisateur) throws Exception {
		try {
			if (propositionBudget==null)
				throw new Exception("il faut passer une propositionBudget en parametre");
			if (utilisateur==null)
				throw new Exception("il faut passer un utilisateur en parametre");
			if (databus == null || !_CktlBasicDataBus.isDatabaseConnected())
				throw new Exception("il y a un probleme avec le dataBus");

			if (EOUtilities.primaryKeyForObject(ed, propositionBudget) != null)
				throw new Exception("Cette proposition a deja ete enregistree, on ne peut pas la modifier");

			propositionBudget.setUtilisateurRelationship(utilisateur);

			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			// enregistrer propositionBudget
			boolean pbProcedure = ProcedureAjouterPropositionBudget.enregistrer(databus, propositionBudget);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure)
				throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
			propositionBudget.setPrbuIdProc((Number) dico.valueForKey(ProcedureAjouterPropositionBudget.PROCEDURE_RETOUR_CLE));

			// enregistrer propositionBudgetNatureLolf
			if (propositionBudget.propositionBudgetNatureLolfs()!=null && propositionBudget.propositionBudgetNatureLolfs().count()>0) {
				for (int i=0; i<propositionBudget.propositionBudgetNatureLolfs().count(); i++) {
					EOPropositionBudgetNatureLolf item=(EOPropositionBudgetNatureLolf)propositionBudget.propositionBudgetNatureLolfs().objectAtIndex(i);
					pbProcedure = ProcedureAjouterPropositionBudgetNatureLolf.enregistrer(databus, item);
					dico = new NSDictionary(databus.executedProcResult());
					if (!pbProcedure) 
						throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));
				}
			}
			
			databus.commitTransaction();

		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw e;
		}	
	}

	public static void supprimer(_CktlBasicDataBus databus, EOEditingContext ed, EOPropositionBudget propositionBudget, EOUtilisateur utilisateur) throws Exception {
		try {
			if (propositionBudget==null)
				throw new Exception("il faut passer une propositionBudget en parametre");
			if (utilisateur==null)
				throw new Exception("il faut passer un utilisateur en parametre");
			if (databus == null || !_CktlBasicDataBus.isDatabaseConnected())
				throw new Exception("il y a un probleme avec le dataBus");

			propositionBudget.setUtilisateurRelationship(utilisateur);

			if (_CktlBasicDataBus.adaptorContext().hasOpenTransaction())
				_CktlBasicDataBus.adaptorContext().rollbackTransaction();

			databus.beginTransaction();

			// enregistrer propositionBudget
			boolean pbProcedure = ProcedureSupprimerPropositionBudget.enregistrer(databus, propositionBudget);
			NSDictionary dico = new NSDictionary(databus.executedProcResult());
			if (!pbProcedure)
				throw new Exception((String) dico.valueForKey(_CktlBasicDataBus.ERROR_KEY));

			databus.commitTransaction();

		} catch (Exception e) {
			databus.rollbackTransaction();
			System.out.println("Exception : " + e);
			throw e;
		}	
	}
}
