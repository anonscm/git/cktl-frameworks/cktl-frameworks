/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.metier;

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software is governed by the CeCILL license under French law and abiding by the rules of distribution
 * of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that
 * it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to
 * use and operate it in the same conditions as regards security. The fact that you are presently reading this means that you have had knowledge of the CeCILL
 * license and that you accept its terms.
 */
import java.math.BigDecimal;

import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;

/*
 * Etats possibles : - A VALIDER : prévision crée et modifiable - VALIDER : prévision validée et non modifiable - ANNULE : prévision refusée
 */

public class EOPrevisionBudget extends _EOPrevisionBudget {

	public EOPrevisionBudget() {
		super();
	}

	public void proposerTout(EOEditingContext ec, EOUtilisateur utilisateur, String commentaire, EOTypeApplication typeApplication) throws Exception {
		EOPropositionBudget propositionBudget = EOPropositionBudget.createEOPropositionBudget(ec, new NSTimestamp(), exercice(), organ(), typeApplication,
				FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE), utilisateur);
		propositionBudget.setPrbuCommentaire(commentaire);
		propositionBudget.setPrevisionBudgetRelationship(this);

		for (int i = 0; i < previsionBudgetNatureLolfs().count(); i++) {
			EOPrevisionBudgetNatureLolf prev = (EOPrevisionBudgetNatureLolf) previsionBudgetNatureLolfs().objectAtIndex(i);
			prev.proposerTotaliteMontant(propositionBudget);
		}
		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE));
	}

	public boolean isAnnulable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_A_VALIDER))
			return false;
		return true;
	}

	public void annuler(EOEditingContext ec) throws Exception {
		if (!isAnnulable())
			throw new Exception("Cette prevision est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas l'annuler");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_ANNULE));

		// TODO : à compléter
	}

	public boolean isValidable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_A_VALIDER))
			return false;
		return true;
	}

	public void valider(EOEditingContext ec) throws Exception {
		if (!isValidable())
			throw new Exception("Cette prevision est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la valider");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE));

		// TODO : à compléter
	}

	public boolean isDevalidable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE))
			return false;
		EOQualifier qualifier = ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), EOTypeEtat.ETAT_ANNULE);
		NSArray propositions = propositionBudgets(qualifier);
		if (propositions != null && propositions.count()>0) {
			return false;
		}
		return true;
	}

	public void devalider(EOEditingContext ec) throws Exception {
		if (!isDevalidable())
			throw new Exception("Cette prevision est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la valider");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_A_VALIDER));

		// TODO : à compléter
	}

	public boolean isModifiable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_A_VALIDER))
			return false;
		return true;
	}

	/*public void ajusterProposition(EOTypeCredit typeCredit, EOPlanComptableExer planComptableExer, EOTypeAction typeAction, BigDecimal montant) {
		EOQualifier qual = ERXQ.equals(EOPrevisionBudgetNatureLolf.PLAN_COMPTABLE_EXER_KEY, planComptableExer)
		.and(ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_CREDIT_KEY, typeCredit)).and(ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_ACTION_KEY, typeAction));
		NSArray<EOPrevisionBudgetNatureLolf> prevs = previsionBudgetNatureLolfs(qual);
	
		if (prevs.isEmpty())
			return;
		EOPrevisionBudgetNatureLolf prev=(EOPrevisionBudgetNatureLolf)prevs.objectAtIndex(0);
		prev.setPvbnlAOuvrir(prev.pvbnlMontant().add(montant));
	}*/
	
	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele a partir des factories. Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public EOPrevisionBudgetNatureLolf getOrCreatePrevisionBudgetNatureLolf(EOPlanComptableExer planCoExer, EOTypeAction lolf, EOTypeCredit typeCredit) {
		EOQualifier qual = ERXQ.equals(EOPrevisionBudgetNatureLolf.PLAN_COMPTABLE_EXER_KEY, planCoExer)
				.and(ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_CREDIT_KEY, typeCredit)).and(ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_ACTION_KEY, lolf));
		NSArray<EOPrevisionBudgetNatureLolf> prevs = previsionBudgetNatureLolfs(qual);
		EOPrevisionBudgetNatureLolf prevNatureLolf = null;
		if (prevs.isEmpty()) {
			prevNatureLolf = EOPrevisionBudgetNatureLolf.creerInstance(editingContext());
			prevNatureLolf.setPlanComptableExerRelationship(planCoExer);
			prevNatureLolf.setTypeCreditRelationship(typeCredit);
			prevNatureLolf.setExerciceRelationship(exercice());
			prevNatureLolf.setTypeActionRelationship(lolf);
			prevNatureLolf.setPvbnlAOuvrir(BigDecimal.ZERO);
			prevNatureLolf.setPvbnlMontant(BigDecimal.ZERO);
			prevNatureLolf.setPvbnlOuvert(BigDecimal.ZERO);
			this.addToPrevisionBudgetNatureLolfsRelationship(prevNatureLolf);
		} else {
			prevNatureLolf = prevs.lastObject();
		}
		return prevNatureLolf;
	}

	public NSArray<EOPrevisionBudgetNatureLolf> previsionBudgetNatureLolfs(boolean isTypeDepense) {
		EOQualifier qual = ERXQ.equals(EOPrevisionBudgetNatureLolf.IS_TYPE_DEPENSE_KEY, isTypeDepense);
		return previsionBudgetNatureLolfs(qual);
	}

	public BigDecimal sumPrevsFieldWithDimension(String fieldKey, String dimKey, EOEnterpriseObject dimValue, boolean isTypeDepense) {
		EOQualifier qualifier = ERXQ.equals(dimKey, dimValue).and(ERXQ.equals(EOPrevisionBudgetNatureLolf.IS_TYPE_DEPENSE_KEY, isTypeDepense));
		NSArray<EOPrevisionBudgetNatureLolf> prevs = this.previsionBudgetNatureLolfs(qualifier);
		return (BigDecimal) prevs.valueForKey("@sum." + fieldKey);
	}

	public BigDecimal sumPrevsFieldWith2Dimensions(String fieldKey, String dim1Key, EOEnterpriseObject dim1Value, String dim2Key, EOEnterpriseObject dim2Value,
			boolean isTypeDepense) {
		EOQualifier qualifier = ERXQ.equals(dim1Key, dim1Value).and(ERXQ.equals(dim2Key, dim2Value))
				.and(ERXQ.equals(EOPrevisionBudgetNatureLolf.IS_TYPE_DEPENSE_KEY, isTypeDepense));
		NSArray<EOPrevisionBudgetNatureLolf> prevs = this.previsionBudgetNatureLolfs(qualifier);
		return (BigDecimal) prevs.valueForKey("@sum." + fieldKey);
	}

	public BigDecimal sumAllPrevsField(String fieldKey, boolean isTypeDepense) {
		EOQualifier qual = ERXQ.equals(EOPrevisionBudgetNatureLolf.IS_TYPE_DEPENSE_KEY, isTypeDepense);
		NSArray<EOPrevisionBudgetNatureLolf> prevs = this.previsionBudgetNatureLolfs(qual);
		return (BigDecimal) prevs.valueForKey("@sum." + fieldKey);
	}

	// public EOPrevisionBudgetNatureLolf prevBudgetNatureLolf(EOTypeAction lolf, EOPlanComptableExer planCo, EOTypeCredit typeCredit, boolean create) {
	// EOQualifier qualifier = ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_ACTION_KEY, lolf)
	// .and(ERXQ.equals(EOPrevisionBudgetNatureLolf.PLAN_COMPTABLE_EXER_KEY, planCo))
	// .and(ERXQ.equals(EOPrevisionBudgetNatureLolf.TYPE_CREDIT_KEY, typeCredit));
	// NSArray<EOPrevisionBudgetNatureLolf> prevs = this.previsionBudgetNatureLolfs(qualifier);
	// if (prevs.isEmpty() && create)
	// EOPrevisionBudgetNatureLolf.creerInstance(editingContext())
	// return !prevs.isEmpty() ? prevs.lastObject() : null;
	// }

	public boolean isValide() {
		return typeEtat() != null && EOTypeEtat.ETAT_VALIDE.equals(typeEtat().tyetLibelle());
	}

	public boolean isAValider() {
		return typeEtat() != null && EOTypeEtat.ETAT_A_VALIDER.equals(typeEtat().tyetLibelle());
	}

	public boolean isEnAttente() {
		return typeEtat() != null && EOTypeEtat.ETAT_EN_ATTENTE.equals(typeEtat().tyetLibelle());
	}

	public boolean isRejetee() {
		return typeEtat() != null && EOTypeEtat.ETAT_BLOCAGE.equals(typeEtat().tyetLibelle());
	}

	public boolean isAValiderOuRejetee() {
		return typeEtat() != null && (isAValider() || isRejetee());
	}

	public EOPropositionBudget lastPropositionBudget() {
		NSArray<EOPropositionBudget> props = propositionBudgets();
		props = ERXS.sorted(props, ERXS.asc(EOPropositionBudget.PRBU_DATE_KEY));
		return props.isEmpty() ? null : props.lastObject();
	}

	public EOPropositionBudget lastPropositionBudgetValide() {
		NSArray<EOPropositionBudget> props = propositionBudgets(ERXQ.equals(EOPropositionBudget.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY,
				EOTypeEtat.ETAT_VALIDE));
		props = ERXS.sorted(props, ERXS.asc(EOPropositionBudget.PRBU_DATE_KEY));
		return props.isEmpty() ? null : props.lastObject();
	}

	public EOPropositionBudget lastPropositionBudgetAValider() {
		NSArray<EOPropositionBudget> props = propositionBudgets(ERXQ.equals(EOPropositionBudget.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY,
				EOTypeEtat.ETAT_A_VALIDER));
		props = ERXS.sorted(props, ERXS.asc(EOPropositionBudget.PRBU_DATE_KEY));
		return props.isEmpty() ? null : props.lastObject();
	}

}
