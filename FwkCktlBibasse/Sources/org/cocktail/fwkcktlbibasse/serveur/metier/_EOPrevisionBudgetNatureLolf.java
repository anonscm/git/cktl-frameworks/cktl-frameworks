/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPrevisionBudgetNatureLolf.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPrevisionBudgetNatureLolf extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_PrevisionBudgetNatureLolf";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.PREVISION_BUDG_NAT_LOLF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pvbnlId";

	public static final String PVBNL_A_OUVRIR_KEY = "pvbnlAOuvrir";
	public static final String PVBNL_MONTANT_KEY = "pvbnlMontant";
	public static final String PVBNL_OUVERT_KEY = "pvbnlOuvert";

// Attributs non visibles
	public static final String PVBNL_ID_KEY = "pvbnlId";
	public static final String PVBU_ID_KEY = "pvbuId";
	public static final String TYAC_ID_KEY = "tyacId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";

//Colonnes dans la base de donnees
	public static final String PVBNL_A_OUVRIR_COLKEY = "PVBNL_A_OUVRIR";
	public static final String PVBNL_MONTANT_COLKEY = "PVBNL_MONTANT";
	public static final String PVBNL_OUVERT_COLKEY = "PVBNL_OUVERT";

	public static final String PVBNL_ID_COLKEY = "PVBNL_ID";
	public static final String PVBU_ID_COLKEY = "PVBU_ID";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_EXER_KEY = "planComptableExer";
	public static final String PREVISION_BUDGET_KEY = "previsionBudget";
	public static final String PROPOSITION_BUDGET_NATURE_LOLFS_KEY = "propositionBudgetNatureLolfs";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal pvbnlAOuvrir() {
    return (java.math.BigDecimal) storedValueForKey(PVBNL_A_OUVRIR_KEY);
  }

  public void setPvbnlAOuvrir(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PVBNL_A_OUVRIR_KEY);
  }

  public java.math.BigDecimal pvbnlMontant() {
    return (java.math.BigDecimal) storedValueForKey(PVBNL_MONTANT_KEY);
  }

  public void setPvbnlMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PVBNL_MONTANT_KEY);
  }

  public java.math.BigDecimal pvbnlOuvert() {
    return (java.math.BigDecimal) storedValueForKey(PVBNL_OUVERT_KEY);
  }

  public void setPvbnlOuvert(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PVBNL_OUVERT_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptableExer() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_EXER_KEY);
  }
  
	public void setPlanComptableExer(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_EXER_KEY);
	}


  public void setPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget previsionBudget() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget)storedValueForKey(PREVISION_BUDGET_KEY);
  }
  
	public void setPrevisionBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
		takeStoredValueForKey(value,PREVISION_BUDGET_KEY);
	}


  public void setPrevisionBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget oldValue = previsionBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PREVISION_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PREVISION_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }
  
	public void setTypeAction(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
		takeStoredValueForKey(value,TYPE_ACTION_KEY);
	}


  public void setTypeActionRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public NSArray propositionBudgetNatureLolfs() {
    return (NSArray)storedValueForKey(PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }
  
  //ICI
  public void setPropositionBudgetNatureLolfs(NSArray values) {
	  takeStoredValueForKey(values,PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }
  

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier) {
    return propositionBudgetNatureLolfs(qualifier, null, false);
  }

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier, boolean fetch) {
    return propositionBudgetNatureLolfs(qualifier, null, fetch);
  }

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf.PREVISION_BUDGET_NATURE_LOLF_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = propositionBudgetNatureLolfs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }

  public void removeFromPropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf createPropositionBudgetNatureLolfsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkBibasse_PropositionBudgetNatureLolf");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf) eo;
  }

  public void deletePropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPropositionBudgetNatureLolfsRelationships() {
    Enumeration objects = propositionBudgetNatureLolfs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePropositionBudgetNatureLolfsRelationship((org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf)objects.nextElement());
    }
  }


/**
 * Creer une instance de EOPrevisionBudgetNatureLolf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPrevisionBudgetNatureLolf createEOPrevisionBudgetNatureLolf(EOEditingContext editingContext, java.math.BigDecimal pvbnlAOuvrir
, java.math.BigDecimal pvbnlMontant
, java.math.BigDecimal pvbnlOuvert
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptableExer, org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget previsionBudget, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit			) {
    EOPrevisionBudgetNatureLolf eo = (EOPrevisionBudgetNatureLolf) createAndInsertInstance(editingContext, _EOPrevisionBudgetNatureLolf.ENTITY_NAME);    
		eo.setPvbnlAOuvrir(pvbnlAOuvrir);
		eo.setPvbnlMontant(pvbnlMontant);
		eo.setPvbnlOuvert(pvbnlOuvert);
    eo.setExerciceRelationship(exercice);
    eo.setPlanComptableExerRelationship(planComptableExer);
    eo.setPrevisionBudgetRelationship(previsionBudget);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  
	  public EOPrevisionBudgetNatureLolf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPrevisionBudgetNatureLolf)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPrevisionBudgetNatureLolf creerInstance(EOEditingContext editingContext) {
	  		EOPrevisionBudgetNatureLolf object = (EOPrevisionBudgetNatureLolf)createAndInsertInstance(editingContext, _EOPrevisionBudgetNatureLolf.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPrevisionBudgetNatureLolf localInstanceIn(EOEditingContext editingContext, EOPrevisionBudgetNatureLolf eo) {
    EOPrevisionBudgetNatureLolf localInstance = (eo == null) ? null : (EOPrevisionBudgetNatureLolf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPrevisionBudgetNatureLolf#localInstanceIn a la place.
   */
	public static EOPrevisionBudgetNatureLolf localInstanceOf(EOEditingContext editingContext, EOPrevisionBudgetNatureLolf eo) {
		return EOPrevisionBudgetNatureLolf.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPrevisionBudgetNatureLolf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPrevisionBudgetNatureLolf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPrevisionBudgetNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPrevisionBudgetNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPrevisionBudgetNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPrevisionBudgetNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPrevisionBudgetNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPrevisionBudgetNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPrevisionBudgetNatureLolf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPrevisionBudgetNatureLolf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPrevisionBudgetNatureLolf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPrevisionBudgetNatureLolf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
