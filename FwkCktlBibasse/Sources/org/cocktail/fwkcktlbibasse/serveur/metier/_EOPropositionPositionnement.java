/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPropositionPositionnement.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPropositionPositionnement extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_PropositionPositionnement";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.PROPOSITION_POSIT";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prpoId";

	public static final String PRPO_COMMENTAIRE_KEY = "prpoCommentaire";
	public static final String PRPO_DATE_KEY = "prpoDate";
	public static final String PRPO_MONTANT_OUVERT_KEY = "prpoMontantOuvert";
	public static final String PRPO_MONTANT_PREVISIONNEL_KEY = "prpoMontantPrevisionnel";

// Attributs non visibles
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PRPO_ID_KEY = "prpoId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String PRBU_ID_KEY = "prbuId";
	public static final String PRBNL_ID_KEY = "prbnlId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";

//Colonnes dans la base de donnees
	public static final String PRPO_COMMENTAIRE_COLKEY = "PRPO_COMMENTAIRE";
	public static final String PRPO_DATE_COLKEY = "PRPO_DATE";
	public static final String PRPO_MONTANT_OUVERT_COLKEY = "PRPO_MONTANT_OUVERT";
	public static final String PRPO_MONTANT_PREVISIONNEL_COLKEY = "PRPO_MONTANT_PREVISIONNEL";

	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PRPO_ID_COLKEY = "PRPO_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String PRBU_ID_COLKEY = "PRBU_ID";
	public static final String PRBNL_ID_COLKEY = "PRBNL_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PROPOSITION_BUDGET_KEY = "propositionBudget";
	public static final String PROPOSITION_BUDGET_NATURE_LOLF_KEY = "propositionBudgetNatureLolf";
	public static final String PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY = "propositionPositionnementMouvements";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String prpoCommentaire() {
    return (String) storedValueForKey(PRPO_COMMENTAIRE_KEY);
  }

  public void setPrpoCommentaire(String value) {
    takeStoredValueForKey(value, PRPO_COMMENTAIRE_KEY);
  }

  public NSTimestamp prpoDate() {
    return (NSTimestamp) storedValueForKey(PRPO_DATE_KEY);
  }

  public void setPrpoDate(NSTimestamp value) {
    takeStoredValueForKey(value, PRPO_DATE_KEY);
  }

  public java.math.BigDecimal prpoMontantOuvert() {
    return (java.math.BigDecimal) storedValueForKey(PRPO_MONTANT_OUVERT_KEY);
  }

  public void setPrpoMontantOuvert(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRPO_MONTANT_OUVERT_KEY);
  }

  public java.math.BigDecimal prpoMontantPrevisionnel() {
    return (java.math.BigDecimal) storedValueForKey(PRPO_MONTANT_PREVISIONNEL_KEY);
  }

  public void setPrpoMontantPrevisionnel(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRPO_MONTANT_PREVISIONNEL_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget propositionBudget() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget)storedValueForKey(PROPOSITION_BUDGET_KEY);
  }
  
	public void setPropositionBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget value) {
		takeStoredValueForKey(value,PROPOSITION_BUDGET_KEY);
	}


  public void setPropositionBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget oldValue = propositionBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROPOSITION_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PROPOSITION_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf propositionBudgetNatureLolf() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf)storedValueForKey(PROPOSITION_BUDGET_NATURE_LOLF_KEY);
  }
  
	public void setPropositionBudgetNatureLolf(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf value) {
		takeStoredValueForKey(value,PROPOSITION_BUDGET_NATURE_LOLF_KEY);
	}


  public void setPropositionBudgetNatureLolfRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf oldValue = propositionBudgetNatureLolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROPOSITION_BUDGET_NATURE_LOLF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PROPOSITION_BUDGET_NATURE_LOLF_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }
  
	public void setTypeApplication(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication value) {
		takeStoredValueForKey(value,TYPE_APPLICATION_KEY);
	}


  public void setTypeApplicationRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray propositionPositionnementMouvements() {
    return (NSArray)storedValueForKey(PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
  }
  
  //ICI
  public void setPropositionPositionnementMouvements(NSArray values) {
	  takeStoredValueForKey(values,PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
  }
  

  public NSArray propositionPositionnementMouvements(EOQualifier qualifier) {
    return propositionPositionnementMouvements(qualifier, null, false);
  }

  public NSArray propositionPositionnementMouvements(EOQualifier qualifier, boolean fetch) {
    return propositionPositionnementMouvements(qualifier, null, fetch);
  }

  public NSArray propositionPositionnementMouvements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement.PROPOSITION_POSITIONNEMENT_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = propositionPositionnementMouvements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPropositionPositionnementMouvementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
  }

  public void removeFromPropositionPositionnementMouvementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement createPropositionPositionnementMouvementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkBibasse_PropositionPositionnementMouvement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement) eo;
  }

  public void deletePropositionPositionnementMouvementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENT_MOUVEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPropositionPositionnementMouvementsRelationships() {
    Enumeration objects = propositionPositionnementMouvements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePropositionPositionnementMouvementsRelationship((org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnementMouvement)objects.nextElement());
    }
  }


/**
 * Creer une instance de EOPropositionPositionnement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPropositionPositionnement createEOPropositionPositionnement(EOEditingContext editingContext, NSTimestamp prpoDate
, java.math.BigDecimal prpoMontantPrevisionnel
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication typeApplication, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur			) {
    EOPropositionPositionnement eo = (EOPropositionPositionnement) createAndInsertInstance(editingContext, _EOPropositionPositionnement.ENTITY_NAME);    
		eo.setPrpoDate(prpoDate);
		eo.setPrpoMontantPrevisionnel(prpoMontantPrevisionnel);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeCreditRelationship(typeCredit);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOPropositionPositionnement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPropositionPositionnement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPropositionPositionnement creerInstance(EOEditingContext editingContext) {
	  		EOPropositionPositionnement object = (EOPropositionPositionnement)createAndInsertInstance(editingContext, _EOPropositionPositionnement.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPropositionPositionnement localInstanceIn(EOEditingContext editingContext, EOPropositionPositionnement eo) {
    EOPropositionPositionnement localInstance = (eo == null) ? null : (EOPropositionPositionnement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPropositionPositionnement#localInstanceIn a la place.
   */
	public static EOPropositionPositionnement localInstanceOf(EOEditingContext editingContext, EOPropositionPositionnement eo) {
		return EOPropositionPositionnement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPropositionPositionnement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPropositionPositionnement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPropositionPositionnement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPropositionPositionnement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPropositionPositionnement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPropositionPositionnement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPropositionPositionnement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPropositionPositionnement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPropositionPositionnement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPropositionPositionnement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPropositionPositionnement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPropositionPositionnement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
