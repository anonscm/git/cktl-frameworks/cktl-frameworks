/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetEvenements.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetEvenements extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetEvenements";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.BUDGET_EVENEMENTS";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "beveId";

	public static final String BEVE_DATE_KEY = "beveDate";
	public static final String BEVE_INFOS_KEY = "beveInfos";
	public static final String BEVE_INFOS2_KEY = "beveInfos2";
	public static final String BEVE_INFOS3_KEY = "beveInfos3";

// Attributs non visibles
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";
	public static final String TYEV_ID_KEY = "tyevId";
	public static final String BEVE_ID_KEY = "beveId";

//Colonnes dans la base de donnees
	public static final String BEVE_DATE_COLKEY = "BEVE_DATE";
	public static final String BEVE_INFOS_COLKEY = "BEVE_INFOS";
	public static final String BEVE_INFOS2_COLKEY = "BEVE_INFOS2";
	public static final String BEVE_INFOS3_COLKEY = "BEVE_INFOS3";

	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";
	public static final String TYEV_ID_COLKEY = "TYEV_ID";
	public static final String BEVE_ID_COLKEY = "BEVE_ID";


	// Relationships
	public static final String BUDGET_SAISIE_KEY = "budgetSaisie";
	public static final String TYPE_EVENEMENT_KEY = "typeEvenement";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public NSTimestamp beveDate() {
    return (NSTimestamp) storedValueForKey(BEVE_DATE_KEY);
  }

  public void setBeveDate(NSTimestamp value) {
    takeStoredValueForKey(value, BEVE_DATE_KEY);
  }

  public String beveInfos() {
    return (String) storedValueForKey(BEVE_INFOS_KEY);
  }

  public void setBeveInfos(String value) {
    takeStoredValueForKey(value, BEVE_INFOS_KEY);
  }

  public String beveInfos2() {
    return (String) storedValueForKey(BEVE_INFOS2_KEY);
  }

  public void setBeveInfos2(String value) {
    takeStoredValueForKey(value, BEVE_INFOS2_KEY);
  }

  public String beveInfos3() {
    return (String) storedValueForKey(BEVE_INFOS3_KEY);
  }

  public void setBeveInfos3(String value) {
    takeStoredValueForKey(value, BEVE_INFOS3_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie)storedValueForKey(BUDGET_SAISIE_KEY);
  }
  
	public void setBudgetSaisie(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
		takeStoredValueForKey(value,BUDGET_SAISIE_KEY);
	}


  public void setBudgetSaisieRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie oldValue = budgetSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_SAISIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeEvenement typeEvenement() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeEvenement)storedValueForKey(TYPE_EVENEMENT_KEY);
  }
  
	public void setTypeEvenement(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeEvenement value) {
		takeStoredValueForKey(value,TYPE_EVENEMENT_KEY);
	}


  public void setTypeEvenementRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeEvenement value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeEvenement oldValue = typeEvenement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_EVENEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_EVENEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Creer une instance de EOBudgetEvenements avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetEvenements createEOBudgetEvenements(EOEditingContext editingContext, org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie, org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur			) {
    EOBudgetEvenements eo = (EOBudgetEvenements) createAndInsertInstance(editingContext, _EOBudgetEvenements.ENTITY_NAME);    
    eo.setBudgetSaisieRelationship(budgetSaisie);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOBudgetEvenements localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetEvenements)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetEvenements creerInstance(EOEditingContext editingContext) {
	  		EOBudgetEvenements object = (EOBudgetEvenements)createAndInsertInstance(editingContext, _EOBudgetEvenements.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetEvenements localInstanceIn(EOEditingContext editingContext, EOBudgetEvenements eo) {
    EOBudgetEvenements localInstance = (eo == null) ? null : (EOBudgetEvenements)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetEvenements#localInstanceIn a la place.
   */
	public static EOBudgetEvenements localInstanceOf(EOEditingContext editingContext, EOBudgetEvenements eo) {
		return EOBudgetEvenements.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetEvenements fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetEvenements fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetEvenements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetEvenements)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetEvenements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetEvenements fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetEvenements eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetEvenements)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetEvenements fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetEvenements eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetEvenements ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetEvenements fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
