/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetSaisieNature.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetSaisieNature extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetSaisieNature";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Saisie_Nature";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdsnId";

	public static final String BDSN_MONTANT_KEY = "bdsnMontant";
	public static final String BDSN_SAISI_KEY = "bdsnSaisi";
	public static final String BDSN_VOTE_KEY = "bdsnVote";
	public static final String PCO_NUM_VOTE_KEY = "pcoNumVote";

// Attributs non visibles
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String BDSN_ID_KEY = "bdsnId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees
	public static final String BDSN_MONTANT_COLKEY = "BDSN_MONTANT";
	public static final String BDSN_SAISI_COLKEY = "BDSN_SAISI";
	public static final String BDSN_VOTE_COLKEY = "BDSN_VOTE";
	public static final String PCO_NUM_VOTE_COLKEY = "$attribute.columnName";

	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String BDSN_ID_COLKEY = "BDSN_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String BUDGET_SAISIE_KEY = "budgetSaisie";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_CREDIT_KEY = "typeCredit";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public java.math.BigDecimal bdsnMontant() {
    return (java.math.BigDecimal) storedValueForKey(BDSN_MONTANT_KEY);
  }

  public void setBdsnMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSN_MONTANT_KEY);
  }

  public java.math.BigDecimal bdsnSaisi() {
    return (java.math.BigDecimal) storedValueForKey(BDSN_SAISI_KEY);
  }

  public void setBdsnSaisi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSN_SAISI_KEY);
  }

  public java.math.BigDecimal bdsnVote() {
    return (java.math.BigDecimal) storedValueForKey(BDSN_VOTE_KEY);
  }

  public void setBdsnVote(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDSN_VOTE_KEY);
  }

  public String pcoNumVote() {
    return (String) storedValueForKey(PCO_NUM_VOTE_KEY);
  }

  public void setPcoNumVote(String value) {
    takeStoredValueForKey(value, PCO_NUM_VOTE_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie)storedValueForKey(BUDGET_SAISIE_KEY);
  }
  
	public void setBudgetSaisie(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
		takeStoredValueForKey(value,BUDGET_SAISIE_KEY);
	}


  public void setBudgetSaisieRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie oldValue = budgetSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_SAISIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptable() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_KEY);
  }
  
	public void setPlanComptable(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_KEY);
	}


  public void setPlanComptableRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  

/**
 * Creer une instance de EOBudgetSaisieNature avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetSaisieNature createEOBudgetSaisieNature(EOEditingContext editingContext, java.math.BigDecimal bdsnMontant
, java.math.BigDecimal bdsnSaisi
, java.math.BigDecimal bdsnVote
, org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptable, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat			) {
    EOBudgetSaisieNature eo = (EOBudgetSaisieNature) createAndInsertInstance(editingContext, _EOBudgetSaisieNature.ENTITY_NAME);    
		eo.setBdsnMontant(bdsnMontant);
		eo.setBdsnSaisi(bdsnSaisi);
		eo.setBdsnVote(bdsnVote);
    eo.setBudgetSaisieRelationship(budgetSaisie);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeCreditRelationship(typeCredit);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOBudgetSaisieNature localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetSaisieNature)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetSaisieNature creerInstance(EOEditingContext editingContext) {
	  		EOBudgetSaisieNature object = (EOBudgetSaisieNature)createAndInsertInstance(editingContext, _EOBudgetSaisieNature.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetSaisieNature localInstanceIn(EOEditingContext editingContext, EOBudgetSaisieNature eo) {
    EOBudgetSaisieNature localInstance = (eo == null) ? null : (EOBudgetSaisieNature)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetSaisieNature#localInstanceIn a la place.
   */
	public static EOBudgetSaisieNature localInstanceOf(EOEditingContext editingContext, EOBudgetSaisieNature eo) {
		return EOBudgetSaisieNature.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetSaisieNature fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetSaisieNature fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetSaisieNature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetSaisieNature)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetSaisieNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetSaisieNature fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetSaisieNature eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetSaisieNature)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetSaisieNature fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetSaisieNature eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetSaisieNature ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetSaisieNature fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
