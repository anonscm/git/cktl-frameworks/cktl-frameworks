/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.metier;

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software is governed by the CeCILL license under French law and abiding by the rules of distribution
 * of free software. You can use, modify and/or redistribute the software under the terms of the CeCILL license as circulated by CEA, CNRS and INRIA at the
 * following URL "http://www.cecill.info". As a counterpart to the access to the source code and rights to copy, modify and redistribute granted by the license,
 * users are provided only with a limited warranty and the software's author, the holder of the economic rights, and the successive licensors have only limited
 * liability. In this respect, the user's attention is drawn to the risks associated with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software, that may mean that it is complicated to manipulate, and that also therefore means that
 * it is reserved for developers and experienced professionals having in-depth computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling the security of their systems and/or data to be ensured and, more generally, to
 * use and operate it in the same conditions as regards security. The fact that you are presently reading this means that you have had knowledge of the CeCILL
 * license and that you accept its terms.
 */
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

/*
 * Etats possibles : 
 * - VALIDE : proposition validée à partir d'une proposition à valider 
 * - ANNULE : proposition refusée
 * 
 * A partir du moment ou on a rattaché une proposition à un EOBudgetSaisie, on reprend les mêmes états que pour la saisie manuelle de Bibasse 
 * - EN COURS : proposition rattachée à un budgetSaisie 
 * - CONTROLE : proposition controlée 
 * - CLOTURE : proposition cloturée 
 * - VOTE : le budgetSaisie associé a été voté
 */

public class EOPropositionBudget extends _EOPropositionBudget {

	Number prbuIdProc;

	public static final String ETAT_EN_COURS = "EN COURS";
	public static final String ETAT_VERROUILLE = "ACCEPTE";
	public static final String ETAT_CONTROLE = "CONTROLE";
	public static final String ETAT_CLOTURE = "CLOTURE";
	public static final String ETAT_VOTE = "VOTE";

	public EOPropositionBudget() {
		super();
		prbuIdProc = null;
	}

	public void setPrbuIdProc(Number value) {
		prbuIdProc = value;
	}

	public Number prbuIdProc() {
		return prbuIdProc;
	}

	public boolean isAnnulable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE))
			return false;
		return true;
	}

	public void annuler(EOEditingContext ec) throws Exception {
		if (!isAnnulable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas l'annuler");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_ANNULE));

		for (int i=0; i<propositionBudgetNatureLolfs().count(); i++) {
			EOPropositionBudgetNatureLolf prop=(EOPropositionBudgetNatureLolf)propositionBudgetNatureLolfs().objectAtIndex(i);
			if (prop.previsionBudgetNatureLolf()!=null)
				prop.previsionBudgetNatureLolf().setPvbnlAOuvrir(prop.previsionBudgetNatureLolf().pvbnlAOuvrir().subtract(prop.prbnlMontant()));
		}

		// TODO : à compléter
	}

	public boolean isAssociable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(EOTypeEtat.ETAT_VALIDE))
			return false;
		return true;
	}

	public void associer(EOEditingContext ec, EOBudgetSaisie budgetSaisie) throws Exception {
		if (budgetSaisie == null || !budgetSaisie.isSaisieEnCours())
			throw new Exception("Il faut associer un budgetSaisie en cours de saisie");
		if (!isAssociable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas l'annuler");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));
		setBudgetSaisieRelationship(budgetSaisie);
		budgetSaisie.ajouterSaisie(ec, this);
	}

	public boolean isDesassociable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_EN_COURS))
			return false;
		return true;
	}

	public void desassocier(EOEditingContext ec) throws Exception {
		if (!isDesassociable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la desassocier");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE));
		budgetSaisie().retrancherSaisie(ec, this);
		setBudgetSaisieRelationship(null);
	}

	public boolean isVerrouillable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_EN_COURS))
			return false;
		return true;
	}

	public void verrouiller(EOEditingContext ec) throws Exception {
		if (!isVerrouillable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la verrouiller");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_VERROUILLE));

		budgetSaisie().verrouillerSiPossible(ec, organ());
	}
	
	public boolean isDeverrouillable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_VERROUILLE))
			return false;
		return true;
	}

	public void deverrouiller(EOEditingContext ec) throws Exception {
		if (!isDeverrouillable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la deverrouiller");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));

		budgetSaisie().deverrouiller(ec, organ());
	}
	
	public boolean isControlable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_VERROUILLE))
			return false;
		return true;
	}

	public void controler(EOEditingContext ec) throws Exception {
		if (!isControlable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la controler");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_CONTROLE));

		budgetSaisie().controlerSiPossible(ec, organ());
	}

	public boolean isDecontrolable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_CONTROLE))
			return false;
		return true;
	}

	public void decontroler(EOEditingContext ec) throws Exception {
		if (!isDecontrolable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la de-controler");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_VERROUILLE));

		budgetSaisie().deControler(ec, organ());
	}

	public boolean isCloturable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_CONTROLE))
			return false;
		return true;
	}

	public void cloturer(EOEditingContext ec) throws Exception {
		if (!isCloturable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la cloturer");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_CLOTURE));

		budgetSaisie().cloturerSiPossible(ec, organ());
	}

	public boolean isVotable() {
		if (typeEtat() == null || !typeEtat().tyetLibelle().equals(ETAT_CLOTURE))
			return false;
		return true;
	}

	public void voter(EOEditingContext ec) throws Exception {
		if (!isVotable())
			throw new Exception("Cette proposition est a l'etat " + typeEtat().tyetLibelle() + ", on ne peut pas la voter");

		setTypeEtatRelationship(FinderTypeEtat.getTypeEtat(ec, ETAT_VOTE));
		
		for (int i=0; i<propositionBudgetNatureLolfs().count(); i++) {
			EOPropositionBudgetNatureLolf prop=(EOPropositionBudgetNatureLolf)propositionBudgetNatureLolfs().objectAtIndex(i);
			if (prop.previsionBudgetNatureLolf()!=null) {
				prop.previsionBudgetNatureLolf().setPvbnlAOuvrir(prop.previsionBudgetNatureLolf().pvbnlAOuvrir().subtract(prop.prbnlMontant()));
				prop.previsionBudgetNatureLolf().setPvbnlOuvert(prop.previsionBudgetNatureLolf().pvbnlOuvert().add(prop.prbnlMontant()));
			}
		}
		// TODO : à compléter
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele a partir des factories. Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode.
	 * AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}
	
	public static EOPropositionBudget creerProposition(EOUtilisateur utl, EOPrevisionBudget prevision) {
		EOEditingContext ec = prevision.editingContext();
		EOPropositionBudget prop = EOPropositionBudget.creerInstance(ec);
		prop.setPrbuDate(new NSTimestamp());
		prop.setUtilisateurRelationship(utl);
		prop.setTypeApplicationRelationship(prevision.typeApplication());
		prop.setOrganRelationship(prevision.organ());
		prop.setExerciceRelationship(prevision.exercice());
		EOTypeEtat valide = EOTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_VALIDE);
		prop.setTypeEtat(valide);
		prevision.addToPropositionBudgetsRelationship(prop);
		for (Object obj : prevision.previsionBudgetNatureLolfs()) {
			EOPrevisionBudgetNatureLolf prevNatureLolf = (EOPrevisionBudgetNatureLolf) obj;
			EOPropositionBudgetNatureLolf propNatureLolf = EOPropositionBudgetNatureLolf.creerPropositionBudgetNatureLolf(prevNatureLolf);
			propNatureLolf.setPropositionBudgetRelationship(prop);
		}
		return prop;
	}

	public NSArray<EOPropositionBudgetNatureLolf> propositionBudgetNatureLolfs(boolean isTypeDepense) {
		EOQualifier qual = ERXQ.equals(EOPropositionBudgetNatureLolf.IS_TYPE_DEPENSE_KEY, isTypeDepense);
//		NSArray<EOPropositionBudgetNatureLolf> propositionBudgetNatureLolfs = propositionBudgetNatureLolfs();
//		propositionBudgetNatureLolfs = ERXQ.filtered(propositionBudgetNatureLolfs, qual);
		return propositionBudgetNatureLolfs(qual);
	}


}
