/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetVoteNatureLolf.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetVoteNatureLolf extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetVoteNatureLolf";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Vote_Nature_Lolf";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdvlId";

	public static final String BDVL_CONVENTIONS_KEY = "bdvlConventions";
	public static final String BDVL_CREDITS_KEY = "bdvlCredits";
	public static final String BDVL_DBMS_KEY = "bdvlDbms";
	public static final String BDVL_DEBITS_KEY = "bdvlDebits";
	public static final String BDVL_OUVERTS_KEY = "bdvlOuverts";
	public static final String BDVL_PRIMITIFS_KEY = "bdvlPrimitifs";
	public static final String BDVL_PROVISOIRES_KEY = "bdvlProvisoires";
	public static final String BDVL_RELIQUATS_KEY = "bdvlReliquats";
	public static final String BDVL_VOTES_KEY = "bdvlVotes";

// Attributs non visibles
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String BDVL_ID_KEY = "bdvlId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String TYAC_ID_KEY = "tyacId";

//Colonnes dans la base de donnees
	public static final String BDVL_CONVENTIONS_COLKEY = "BDVL_CONVENTIONS";
	public static final String BDVL_CREDITS_COLKEY = "BDVL_CREDITS";
	public static final String BDVL_DBMS_COLKEY = "BDVL_DBMS";
	public static final String BDVL_DEBITS_COLKEY = "BDVL_DEBITS";
	public static final String BDVL_OUVERTS_COLKEY = "BDVL_OUVERTS";
	public static final String BDVL_PRIMITIFS_COLKEY = "BDVL_PRIMITIFS";
	public static final String BDVL_PROVISOIRES_COLKEY = "BDVL_PROVISOIRES";
	public static final String BDVL_RELIQUATS_COLKEY = "BDVL_RELIQUATS";
	public static final String BDVL_VOTES_COLKEY = "BDVL_VOTES";

	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String BDVL_ID_COLKEY = "BDVL_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PLAN_COMPTABLE_KEY = "planComptable";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal bdvlConventions() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_CONVENTIONS_KEY);
  }

  public void setBdvlConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal bdvlCredits() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_CREDITS_KEY);
  }

  public void setBdvlCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_CREDITS_KEY);
  }

  public java.math.BigDecimal bdvlDbms() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_DBMS_KEY);
  }

  public void setBdvlDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_DBMS_KEY);
  }

  public java.math.BigDecimal bdvlDebits() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_DEBITS_KEY);
  }

  public void setBdvlDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_DEBITS_KEY);
  }

  public java.math.BigDecimal bdvlOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_OUVERTS_KEY);
  }

  public void setBdvlOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_OUVERTS_KEY);
  }

  public java.math.BigDecimal bdvlPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_PRIMITIFS_KEY);
  }

  public void setBdvlPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal bdvlProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_PROVISOIRES_KEY);
  }

  public void setBdvlProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal bdvlReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_RELIQUATS_KEY);
  }

  public void setBdvlReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_RELIQUATS_KEY);
  }

  public java.math.BigDecimal bdvlVotes() {
    return (java.math.BigDecimal) storedValueForKey(BDVL_VOTES_KEY);
  }

  public void setBdvlVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVL_VOTES_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptable() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_KEY);
  }
  
	public void setPlanComptable(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_KEY);
	}


  public void setPlanComptableRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planComptable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }
  
	public void setTypeAction(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
		takeStoredValueForKey(value,TYPE_ACTION_KEY);
	}


  public void setTypeActionRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

/**
 * Creer une instance de EOBudgetVoteNatureLolf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetVoteNatureLolf createEOBudgetVoteNatureLolf(EOEditingContext editingContext, java.math.BigDecimal bdvlConventions
, java.math.BigDecimal bdvlCredits
, java.math.BigDecimal bdvlDbms
, java.math.BigDecimal bdvlDebits
, java.math.BigDecimal bdvlOuverts
, java.math.BigDecimal bdvlPrimitifs
, java.math.BigDecimal bdvlProvisoires
, java.math.BigDecimal bdvlReliquats
, java.math.BigDecimal bdvlVotes
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptable, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit			) {
    EOBudgetVoteNatureLolf eo = (EOBudgetVoteNatureLolf) createAndInsertInstance(editingContext, _EOBudgetVoteNatureLolf.ENTITY_NAME);    
		eo.setBdvlConventions(bdvlConventions);
		eo.setBdvlCredits(bdvlCredits);
		eo.setBdvlDbms(bdvlDbms);
		eo.setBdvlDebits(bdvlDebits);
		eo.setBdvlOuverts(bdvlOuverts);
		eo.setBdvlPrimitifs(bdvlPrimitifs);
		eo.setBdvlProvisoires(bdvlProvisoires);
		eo.setBdvlReliquats(bdvlReliquats);
		eo.setBdvlVotes(bdvlVotes);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setPlanComptableRelationship(planComptable);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  
	  public EOBudgetVoteNatureLolf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetVoteNatureLolf)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetVoteNatureLolf creerInstance(EOEditingContext editingContext) {
	  		EOBudgetVoteNatureLolf object = (EOBudgetVoteNatureLolf)createAndInsertInstance(editingContext, _EOBudgetVoteNatureLolf.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetVoteNatureLolf localInstanceIn(EOEditingContext editingContext, EOBudgetVoteNatureLolf eo) {
    EOBudgetVoteNatureLolf localInstance = (eo == null) ? null : (EOBudgetVoteNatureLolf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetVoteNatureLolf#localInstanceIn a la place.
   */
	public static EOBudgetVoteNatureLolf localInstanceOf(EOEditingContext editingContext, EOBudgetVoteNatureLolf eo) {
		return EOBudgetVoteNatureLolf.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetVoteNatureLolf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetVoteNatureLolf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetVoteNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetVoteNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetVoteNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetVoteNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetVoteNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetVoteNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetVoteNatureLolf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetVoteNatureLolf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetVoteNatureLolf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetVoteNatureLolf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
