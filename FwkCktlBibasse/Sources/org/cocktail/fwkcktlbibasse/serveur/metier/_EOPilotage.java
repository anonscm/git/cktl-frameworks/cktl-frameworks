/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPilotage.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPilotage extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_Pilotage";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.V_PILOTAGE";



	// Attributes


	public static final String ETAT_KEY = "etat";
	public static final String MONTANT_GES_KEY = "montantGes";
	public static final String MONTANT_GES_SAISI_KEY = "montantGesSaisi";
	public static final String MONTANT_NAT_KEY = "montantNat";
	public static final String MONTANT_NAT_SAISI_KEY = "montantNatSaisi";
	public static final String ORG_CR_KEY = "orgCr";
	public static final String ORG_ETAB_KEY = "orgEtab";
	public static final String ORG_UB_KEY = "orgUb";

// Attributs non visibles
	public static final String ORG_ID_KEY = "orgId";
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String ETAT_COLKEY = "ETAT";
	public static final String MONTANT_GES_COLKEY = "MONTANT_GES";
	public static final String MONTANT_GES_SAISI_COLKEY = "MONTANT_GES_SAISI";
	public static final String MONTANT_NAT_COLKEY = "MONTANT_NAT";
	public static final String MONTANT_NAT_SAISI_COLKEY = "MONTANT_NAT_SAISI";
	public static final String ORG_CR_COLKEY = "ORG_CR";
	public static final String ORG_ETAB_COLKEY = "ORG_ETAB";
	public static final String ORG_UB_COLKEY = "ORG_UB";

	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String BUDGET_SAISIE_KEY = "budgetSaisie";
	public static final String ORGAN_KEY = "organ";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String etat() {
    return (String) storedValueForKey(ETAT_KEY);
  }

  public void setEtat(String value) {
    takeStoredValueForKey(value, ETAT_KEY);
  }

  public java.math.BigDecimal montantGes() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_GES_KEY);
  }

  public void setMontantGes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_GES_KEY);
  }

  public java.math.BigDecimal montantGesSaisi() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_GES_SAISI_KEY);
  }

  public void setMontantGesSaisi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_GES_SAISI_KEY);
  }

  public java.math.BigDecimal montantNat() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_NAT_KEY);
  }

  public void setMontantNat(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_NAT_KEY);
  }

  public java.math.BigDecimal montantNatSaisi() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_NAT_SAISI_KEY);
  }

  public void setMontantNatSaisi(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_NAT_SAISI_KEY);
  }

  public String orgCr() {
    return (String) storedValueForKey(ORG_CR_KEY);
  }

  public void setOrgCr(String value) {
    takeStoredValueForKey(value, ORG_CR_KEY);
  }

  public String orgEtab() {
    return (String) storedValueForKey(ORG_ETAB_KEY);
  }

  public void setOrgEtab(String value) {
    takeStoredValueForKey(value, ORG_ETAB_KEY);
  }

  public String orgUb() {
    return (String) storedValueForKey(ORG_UB_KEY);
  }

  public void setOrgUb(String value) {
    takeStoredValueForKey(value, ORG_UB_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie)storedValueForKey(BUDGET_SAISIE_KEY);
  }
  
	public void setBudgetSaisie(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
		takeStoredValueForKey(value,BUDGET_SAISIE_KEY);
	}


  public void setBudgetSaisieRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie oldValue = budgetSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_SAISIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  

/**
 * Creer une instance de EOPilotage avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPilotage createEOPilotage(EOEditingContext editingContext, String etat
, java.math.BigDecimal montantGes
, java.math.BigDecimal montantGesSaisi
, java.math.BigDecimal montantNat
, java.math.BigDecimal montantNatSaisi
, String orgEtab
, String orgUb
, org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur			) {
    EOPilotage eo = (EOPilotage) createAndInsertInstance(editingContext, _EOPilotage.ENTITY_NAME);    
		eo.setEtat(etat);
		eo.setMontantGes(montantGes);
		eo.setMontantGesSaisi(montantGesSaisi);
		eo.setMontantNat(montantNat);
		eo.setMontantNatSaisi(montantNatSaisi);
		eo.setOrgEtab(orgEtab);
		eo.setOrgUb(orgUb);
    eo.setBudgetSaisieRelationship(budgetSaisie);
    eo.setOrganRelationship(organ);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOPilotage localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPilotage)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPilotage creerInstance(EOEditingContext editingContext) {
	  		EOPilotage object = (EOPilotage)createAndInsertInstance(editingContext, _EOPilotage.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPilotage localInstanceIn(EOEditingContext editingContext, EOPilotage eo) {
    EOPilotage localInstance = (eo == null) ? null : (EOPilotage)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPilotage#localInstanceIn a la place.
   */
	public static EOPilotage localInstanceOf(EOEditingContext editingContext, EOPilotage eo) {
		return EOPilotage.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPilotage fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPilotage fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPilotage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPilotage)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPilotage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPilotage fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPilotage eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPilotage)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPilotage fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPilotage eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPilotage ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPilotage fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
