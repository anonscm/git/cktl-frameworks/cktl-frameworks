/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.metier;

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetParametre;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSValidation;


public class EOBudgetParametres extends _EOBudgetParametres {

	public static String NIVEAU_SAISIE="NIVEAU_SAISIE";
	public static String NIVEAU_SAISIE_CR="CR";
	public static String NIVEAU_SAISIE_UB="UB";

	public static String COMPTE_EXCEDENT_FONC="COMPTE_EXCEDENT_FONC";
	public static String COMPTE_DEFICIT_FONC="COMPTE_DEFICIT_FONC";
	public static String COMPTE_IAF="COMPTE_IAF";
	public static String COMPTE_CAF="COMPTE_CAF";
	public static String COMPTE_AUGMENTATION_FDR="COMPTE_AUGMENTATION_FDR";
	public static String COMPTE_DIMINUTION_FDR="COMPTE_DIMINUTION_FDR";

	public final static String param_777_EN_PRODUIT="777_EN_PRODUIT";
	public final static String param_775_FDR="775_FDR";	
	
	public EOBudgetParametres() {
		super();
	}

    private static EOPlanComptableExer imputationPourBparKey(EOEditingContext ec, EOExercice exercice, String key) {
		if (exercice==null)
			return null;
		EOBudgetParametres parametre=FinderBudgetParametre.getParametre(ec, key, exercice);
		if (parametre==null) 
			return null;
		return EOPlanComptableExer.fetchWithPcoNumAndExercice(ec, parametre.bparValue(), exercice.exeExercice().intValue());    	
    }
    
	public static EOPlanComptableExer getPlanComptableExcedentFonctionnement(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_EXCEDENT_FONC);
	}
	public static EOPlanComptableExer getPlanComptableDeficitFonctionnement(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_DEFICIT_FONC);
	}
	public static EOPlanComptableExer getPlanComptableCaf(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_CAF);
	}
	public static EOPlanComptableExer getPlanComptableIaf(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_IAF);
	}
	public static EOPlanComptableExer getPlanComptableAugmentationFondRoulement(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_AUGMENTATION_FDR);
	}
	public static EOPlanComptableExer getPlanComptableDiminutionFondRoulement(EOEditingContext ec, EOExercice exercice){
		return imputationPourBparKey(ec, exercice, COMPTE_DIMINUTION_FDR);
	}

	public static String niveauSaisie(EOEditingContext ec, EOExercice exercice) {
		String niveauSaisie = null;
		if (exercice != null) {
			EOBudgetParametres parametre=FinderBudgetParametre.getParametre(ec, NIVEAU_SAISIE, exercice);
			if (parametre != null) {
				niveauSaisie = parametre.bparValue();
			}
		}
		
		return niveauSaisie;
	}
	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele a partir des factories.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

}
