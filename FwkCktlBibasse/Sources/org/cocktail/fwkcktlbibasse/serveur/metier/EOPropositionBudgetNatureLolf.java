/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.metier;

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
import java.math.BigDecimal;

import org.cocktail.fwkcktlbibasse.serveur.finder.Finder;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderPlanComptableExer;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;


public class EOPropositionBudgetNatureLolf extends _EOPropositionBudgetNatureLolf implements BudgetNature {

	public static final String IS_TYPE_DEPENSE_KEY = "isTypeDepense";
	public static final String IS_TYPE_RECETTE_KEY = "isTypeRecette";

	public EOPropositionBudgetNatureLolf() {
		super();
	}

	public BigDecimal montant()  {
		return prbnlMontant();
	}


	public void desassocier(EOEditingContext ec) throws Exception {
		if (!propositionBudget().isDesassociable())
			throw new Exception("La proposition est a l'etat " + propositionBudget().typeEtat().tyetLibelle() + ", on ne peut pas la desassocier");


		propositionBudget().budgetSaisie().retrancherSaisie(ec, this);
		if (propositionBudget().propositionBudgetNatureLolfs().count() == 1) {
			propositionBudget().desassocier(ec);
		} else {
			propositionBudget().deletePropositionBudgetNatureLolfsRelationship(this);
		}
	}

	public void modifierMontant(EOEditingContext ec, BigDecimal montant) throws Exception {
		if (!propositionBudget().isDesassociable() && !propositionBudget().isAssociable())
			throw new Exception("La proposition est a l'etat " + propositionBudget().typeEtat().tyetLibelle() + ", on ne peut pas modifier le montant");

		ajusterSaisie(ec, prbnlMontant().multiply(new BigDecimal(-1)));
		ajusterSaisie(ec, montant);
	}

	private void ajusterSaisie(EOEditingContext ec, BigDecimal montant) throws Exception {
		if (!propositionBudget().typeEtat().tyetLibelle().equals(EOPropositionBudget.ETAT_EN_COURS))
			return;
		// MISE A JOUR PREVISION
		/*if (propositionBudget().previsionBudget()!=null)
			 propositionBudget().previsionBudget().ajusterProposition(typeCredit(), planComptableExer(), typeAction(), montant);
*/
		// MISE A JOUR BUDGET SAISIE
		if (propositionBudget().budgetSaisie()==null || propositionBudget().organ()==null)
			return;

		EOOrgan organ=propositionBudget().budgetSaisie().organBudget(propositionBudget().organ());
		EOPlanComptableExer plancoSaisie=FinderPlanComptableExer.findPlanComptableExerBudget(ec, planComptableExer());
		if (plancoSaisie==null)
			throw new Exception("Pas d'imputation correspondante trouvée dans le masque de saisie de Bibasse pour le compte "+
					planComptableExer().pcoNum());

		//NATURE
		EOQualifier qualSaisieNature = ERXQ.and(
				ERXQ.equals(EOBudgetSaisieNature.BUDGET_SAISIE_KEY, propositionBudget().budgetSaisie()),
				ERXQ.equals(EOBudgetSaisieNature.PLAN_COMPTABLE_KEY, plancoSaisie),
				ERXQ.equals(EOBudgetSaisieNature.TYPE_CREDIT_KEY, typeCredit()),
				ERXQ.equals(EOBudgetSaisieNature.ORGAN_KEY, organ)
			);
		NSArray arraySaisieNature=Finder.fetchArray(ec, EOBudgetSaisieNature.ENTITY_NAME,
				EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieNature.PLAN_COMPTABLE_KEY+"=%@ and "
				+EOBudgetSaisieNature.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieNature.ORGAN_KEY+"=%@",
				new NSArray(new Object[] { propositionBudget().budgetSaisie(), plancoSaisie, typeCredit(), organ}),null,false);
		arraySaisieNature = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieNature, EOBudgetSaisieNature.ENTITY_NAME, qualSaisieNature, null,
				true, true, true, true, false, false);

		EOBudgetSaisieNature saisieNature=null;
		if (arraySaisieNature==null || arraySaisieNature.count()==0) {
			saisieNature=EOBudgetSaisieNature.createEOBudgetSaisieNature(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), 
					propositionBudget().budgetSaisie(), 
					this.exercice(), organ, plancoSaisie, typeCredit(), FinderTypeEtat.getTypeEtat(ec, EOPropositionBudget.ETAT_EN_COURS));
		} 
		else
			saisieNature=(EOBudgetSaisieNature)arraySaisieNature.objectAtIndex(0);

		saisieNature.setBdsnSaisi(saisieNature.bdsnSaisi().add(montant));
		saisieNature.setBdsnVote(saisieNature.bdsnMontant().add(saisieNature.bdsnSaisi()));

		//GESTION
		EOQualifier qualSaisieGestion = ERXQ.and(
				ERXQ.equals(EOBudgetSaisieGestion.BUDGET_SAISIE_KEY, propositionBudget().budgetSaisie()),
				ERXQ.equals(EOBudgetSaisieGestion.TYPE_ACTION_KEY, typeAction()),
				ERXQ.equals(EOBudgetSaisieGestion.TYPE_CREDIT_KEY, typeCredit()),
				ERXQ.equals(EOBudgetSaisieGestion.ORGAN_KEY, organ)
			);
		NSArray arraySaisieGestion=Finder.fetchArray(ec, EOBudgetSaisieGestion.ENTITY_NAME,
				EOBudgetSaisieGestion.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieGestion.TYPE_ACTION_KEY+"=%@ and "
				+EOBudgetSaisieGestion.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieGestion.ORGAN_KEY+"=%@",
				new NSArray(new Object[] { propositionBudget().budgetSaisie(), typeAction(), typeCredit(), organ}),null,false);
		arraySaisieGestion = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieGestion, EOBudgetSaisieGestion.ENTITY_NAME, qualSaisieGestion, null,
				true, true, true, true, false, false);

		EOBudgetSaisieGestion saisieGestion=null;
		if (arraySaisieGestion==null || arraySaisieGestion.count()==0) {
			saisieGestion=EOBudgetSaisieGestion.createEOBudgetSaisieGestion(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), 
					propositionBudget().budgetSaisie(), 
					this.exercice(), organ, typeAction(), typeCredit(), FinderTypeEtat.getTypeEtat(ec, EOPropositionBudget.ETAT_EN_COURS));
		} 
		else
			saisieGestion=(EOBudgetSaisieGestion)arraySaisieGestion.objectAtIndex(0);

		saisieGestion.setBdsgSaisi(saisieGestion.bdsgSaisi().add(montant));
		saisieGestion.setBdsgVote(saisieGestion.bdsgMontant().add(saisieGestion.bdsgSaisi()));
	}
	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele a partir des factories.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public void addToMontant(BigDecimal montant) {
		if (prbnlMontant() == null)
			setPrbnlMontant(montant);
		else
			setPrbnlMontant(prbnlMontant().add(montant));
	}




	public static EOPropositionBudgetNatureLolf creerPropositionBudgetNatureLolf(
			EOPrevisionBudgetNatureLolf prevNatureLolf) {
		EOPropositionBudgetNatureLolf prop = EOPropositionBudgetNatureLolf.creerInstance(prevNatureLolf.editingContext());
		prop.setExerciceRelationship(prevNatureLolf.exercice());
		prop.setPlanComptableExerRelationship(prevNatureLolf.planComptableExer());
		prop.setPrbnlMontant(prevNatureLolf.pvbnlAOuvrir());
		prop.setPrevisionBudgetNatureLolfRelationship(prevNatureLolf);
		prop.setTypeActionRelationship(prevNatureLolf.typeAction());
		prop.setTypeCreditRelationship(prevNatureLolf.typeCredit());
		return prop;
	}

	public boolean isTypeDepense() {
		return typeCredit().isTypeDepense();
	}

	public boolean isTypeRecette() {
		return typeCredit().isTypeRecette();
	}

}
