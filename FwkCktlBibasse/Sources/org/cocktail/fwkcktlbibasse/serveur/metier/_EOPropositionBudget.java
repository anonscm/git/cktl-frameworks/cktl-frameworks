/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPropositionBudget.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPropositionBudget extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_PropositionBudget";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.PROPOSITION_BUDG";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prbuId";

	public static final String PRBU_COMMENTAIRE_KEY = "prbuCommentaire";
	public static final String PRBU_DATE_KEY = "prbuDate";

// Attributs non visibles
	public static final String PVBU_ID_KEY = "pvbuId";
	public static final String PRBU_ID_KEY = "prbuId";
	public static final String TYAP_ID_KEY = "tyapId";
	public static final String ORG_ID_KEY = "orgId";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String PRBU_COMMENTAIRE_COLKEY = "PRBU_COMMENTAIRE";
	public static final String PRBU_DATE_COLKEY = "PRBU_DATE";

	public static final String PVBU_ID_COLKEY = "PVBU_ID";
	public static final String PRBU_ID_COLKEY = "PRBU_ID";
	public static final String TYAP_ID_COLKEY = "TYAP_ID";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String BUDGET_SAISIE_KEY = "budgetSaisie";
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String PREVISION_BUDGET_KEY = "previsionBudget";
	public static final String PROPOSITION_BUDGET_NATURE_LOLFS_KEY = "propositionBudgetNatureLolfs";
	public static final String PROPOSITION_POSITIONNEMENTS_KEY = "propositionPositionnements";
	public static final String TYPE_APPLICATION_KEY = "typeApplication";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String prbuCommentaire() {
    return (String) storedValueForKey(PRBU_COMMENTAIRE_KEY);
  }

  public void setPrbuCommentaire(String value) {
    takeStoredValueForKey(value, PRBU_COMMENTAIRE_KEY);
  }

  public NSTimestamp prbuDate() {
    return (NSTimestamp) storedValueForKey(PRBU_DATE_KEY);
  }

  public void setPrbuDate(NSTimestamp value) {
    takeStoredValueForKey(value, PRBU_DATE_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie budgetSaisie() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie)storedValueForKey(BUDGET_SAISIE_KEY);
  }
  
	public void setBudgetSaisie(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
		takeStoredValueForKey(value,BUDGET_SAISIE_KEY);
	}


  public void setBudgetSaisieRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie oldValue = budgetSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_SAISIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget previsionBudget() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget)storedValueForKey(PREVISION_BUDGET_KEY);
  }
  
	public void setPrevisionBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
		takeStoredValueForKey(value,PREVISION_BUDGET_KEY);
	}


  public void setPrevisionBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudget oldValue = previsionBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PREVISION_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PREVISION_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication typeApplication() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication)storedValueForKey(TYPE_APPLICATION_KEY);
  }
  
	public void setTypeApplication(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication value) {
		takeStoredValueForKey(value,TYPE_APPLICATION_KEY);
	}


  public void setTypeApplicationRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication oldValue = typeApplication();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_APPLICATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_APPLICATION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray propositionBudgetNatureLolfs() {
    return (NSArray)storedValueForKey(PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }
  
  //ICI
  public void setPropositionBudgetNatureLolfs(NSArray values) {
	  takeStoredValueForKey(values,PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }
  

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier) {
    return propositionBudgetNatureLolfs(qualifier, null, false);
  }

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier, boolean fetch) {
    return propositionBudgetNatureLolfs(qualifier, null, fetch);
  }

  public NSArray propositionBudgetNatureLolfs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf.PROPOSITION_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = propositionBudgetNatureLolfs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }

  public void removeFromPropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf createPropositionBudgetNatureLolfsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkBibasse_PropositionBudgetNatureLolf");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf) eo;
  }

  public void deletePropositionBudgetNatureLolfsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGET_NATURE_LOLFS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPropositionBudgetNatureLolfsRelationships() {
    Enumeration objects = propositionBudgetNatureLolfs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePropositionBudgetNatureLolfsRelationship((org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudgetNatureLolf)objects.nextElement());
    }
  }

  public NSArray propositionPositionnements() {
    return (NSArray)storedValueForKey(PROPOSITION_POSITIONNEMENTS_KEY);
  }
  
  //ICI
  public void setPropositionPositionnements(NSArray values) {
	  takeStoredValueForKey(values,PROPOSITION_POSITIONNEMENTS_KEY);
  }
  

  public NSArray propositionPositionnements(EOQualifier qualifier) {
    return propositionPositionnements(qualifier, null, false);
  }

  public NSArray propositionPositionnements(EOQualifier qualifier, boolean fetch) {
    return propositionPositionnements(qualifier, null, fetch);
  }

  public NSArray propositionPositionnements(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement.PROPOSITION_BUDGET_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = propositionPositionnements();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPropositionPositionnementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENTS_KEY);
  }

  public void removeFromPropositionPositionnementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENTS_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement createPropositionPositionnementsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkBibasse_PropositionPositionnement");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROPOSITION_POSITIONNEMENTS_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement) eo;
  }

  public void deletePropositionPositionnementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_POSITIONNEMENTS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPropositionPositionnementsRelationships() {
    Enumeration objects = propositionPositionnements().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePropositionPositionnementsRelationship((org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement)objects.nextElement());
    }
  }


/**
 * Creer une instance de EOPropositionBudget avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPropositionBudget createEOPropositionBudget(EOEditingContext editingContext, NSTimestamp prbuDate
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication typeApplication, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur			) {
    EOPropositionBudget eo = (EOPropositionBudget) createAndInsertInstance(editingContext, _EOPropositionBudget.ENTITY_NAME);    
		eo.setPrbuDate(prbuDate);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeApplicationRelationship(typeApplication);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOPropositionBudget localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPropositionBudget)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPropositionBudget creerInstance(EOEditingContext editingContext) {
	  		EOPropositionBudget object = (EOPropositionBudget)createAndInsertInstance(editingContext, _EOPropositionBudget.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPropositionBudget localInstanceIn(EOEditingContext editingContext, EOPropositionBudget eo) {
    EOPropositionBudget localInstance = (eo == null) ? null : (EOPropositionBudget)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPropositionBudget#localInstanceIn a la place.
   */
	public static EOPropositionBudget localInstanceOf(EOEditingContext editingContext, EOPropositionBudget eo) {
		return EOPropositionBudget.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPropositionBudget fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPropositionBudget fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPropositionBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPropositionBudget)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPropositionBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPropositionBudget fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPropositionBudget eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPropositionBudget)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPropositionBudget fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPropositionBudget eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPropositionBudget ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPropositionBudget fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
