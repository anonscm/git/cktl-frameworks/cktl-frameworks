/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetMasqueGestion.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetMasqueGestion extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetMasqueGestion";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.BUDGET_MASQUE_GESTION";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bmgId";


// Attributs non visibles
	public static final String BMG_ID_KEY = "bmgId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYAC_ID_KEY = "tyacId";
	public static final String TYET_ID_KEY = "tyetId";

//Colonnes dans la base de donnees

	public static final String BMG_ID_COLKEY = "BMG_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";
	public static final String TYET_ID_COLKEY = "TYET_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_ETAT_KEY = "typeEtat";



	// Accessors methods
  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }
  
	public void setTypeAction(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
		takeStoredValueForKey(value,TYPE_ACTION_KEY);
	}


  public void setTypeActionRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  

/**
 * Creer une instance de EOBudgetMasqueGestion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetMasqueGestion createEOBudgetMasqueGestion(EOEditingContext editingContext, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat			) {
    EOBudgetMasqueGestion eo = (EOBudgetMasqueGestion) createAndInsertInstance(editingContext, _EOBudgetMasqueGestion.ENTITY_NAME);    
    eo.setExerciceRelationship(exercice);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeEtatRelationship(typeEtat);
    return eo;
  }

  
	  public EOBudgetMasqueGestion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetMasqueGestion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetMasqueGestion creerInstance(EOEditingContext editingContext) {
	  		EOBudgetMasqueGestion object = (EOBudgetMasqueGestion)createAndInsertInstance(editingContext, _EOBudgetMasqueGestion.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetMasqueGestion localInstanceIn(EOEditingContext editingContext, EOBudgetMasqueGestion eo) {
    EOBudgetMasqueGestion localInstance = (eo == null) ? null : (EOBudgetMasqueGestion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetMasqueGestion#localInstanceIn a la place.
   */
	public static EOBudgetMasqueGestion localInstanceOf(EOEditingContext editingContext, EOBudgetMasqueGestion eo) {
		return EOBudgetMasqueGestion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetMasqueGestion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetMasqueGestion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetMasqueGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetMasqueGestion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetMasqueGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetMasqueGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetMasqueGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetMasqueGestion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetMasqueGestion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetMasqueGestion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetMasqueGestion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetMasqueGestion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
