/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPropositionBudgetNatureLolf.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPropositionBudgetNatureLolf extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_PropositionBudgetNatureLolf";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.PROPOSITION_BUDG_NAT_LOLF";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prbnlId";

	public static final String PRBNL_MONTANT_KEY = "prbnlMontant";

// Attributs non visibles
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String PCO_NUM_KEY = "pcoNum";
	public static final String TYAC_ID_KEY = "tyacId";
	public static final String PRBNL_ID_KEY = "prbnlId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String PRBU_ID_KEY = "prbuId";
	public static final String PVBNLD_KEY = "pvbnld";

//Colonnes dans la base de donnees
	public static final String PRBNL_MONTANT_COLKEY = "PRBNL_MONTANT";

	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String PCO_NUM_COLKEY = "PCO_NUM";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";
	public static final String PRBNL_ID_COLKEY = "PRBNL_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String PRBU_ID_COLKEY = "PRBU_ID";
	public static final String PVBNLD_COLKEY = "PVBNL_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PLAN_COMPTABLE_EXER_KEY = "planComptableExer";
	public static final String PREVISION_BUDGET_NATURE_LOLF_KEY = "previsionBudgetNatureLolf";
	public static final String PROPOSITION_BUDGET_KEY = "propositionBudget";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal prbnlMontant() {
    return (java.math.BigDecimal) storedValueForKey(PRBNL_MONTANT_KEY);
  }

  public void setPrbnlMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRBNL_MONTANT_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptableExer() {
    return (org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer)storedValueForKey(PLAN_COMPTABLE_EXER_KEY);
  }
  
	public void setPlanComptableExer(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
		takeStoredValueForKey(value,PLAN_COMPTABLE_EXER_KEY);
	}


  public void setPlanComptableExerRelationship(org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer value) {
    if (value == null) {
    	org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer oldValue = planComptableExer();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PLAN_COMPTABLE_EXER_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PLAN_COMPTABLE_EXER_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf previsionBudgetNatureLolf() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf)storedValueForKey(PREVISION_BUDGET_NATURE_LOLF_KEY);
  }
  
	public void setPrevisionBudgetNatureLolf(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf value) {
		takeStoredValueForKey(value,PREVISION_BUDGET_NATURE_LOLF_KEY);
	}


  public void setPrevisionBudgetNatureLolfRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPrevisionBudgetNatureLolf oldValue = previsionBudgetNatureLolf();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PREVISION_BUDGET_NATURE_LOLF_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PREVISION_BUDGET_NATURE_LOLF_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget propositionBudget() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget)storedValueForKey(PROPOSITION_BUDGET_KEY);
  }
  
	public void setPropositionBudget(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget value) {
		takeStoredValueForKey(value,PROPOSITION_BUDGET_KEY);
	}


  public void setPropositionBudgetRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget oldValue = propositionBudget();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROPOSITION_BUDGET_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PROPOSITION_BUDGET_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }
  
	public void setTypeAction(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
		takeStoredValueForKey(value,TYPE_ACTION_KEY);
	}


  public void setTypeActionRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

/**
 * Creer une instance de EOPropositionBudgetNatureLolf avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPropositionBudgetNatureLolf createEOPropositionBudgetNatureLolf(EOEditingContext editingContext, java.math.BigDecimal prbnlMontant
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer planComptableExer, org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget propositionBudget, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit			) {
    EOPropositionBudgetNatureLolf eo = (EOPropositionBudgetNatureLolf) createAndInsertInstance(editingContext, _EOPropositionBudgetNatureLolf.ENTITY_NAME);    
		eo.setPrbnlMontant(prbnlMontant);
    eo.setExerciceRelationship(exercice);
    eo.setPlanComptableExerRelationship(planComptableExer);
    eo.setPropositionBudgetRelationship(propositionBudget);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  
	  public EOPropositionBudgetNatureLolf localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPropositionBudgetNatureLolf)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPropositionBudgetNatureLolf creerInstance(EOEditingContext editingContext) {
	  		EOPropositionBudgetNatureLolf object = (EOPropositionBudgetNatureLolf)createAndInsertInstance(editingContext, _EOPropositionBudgetNatureLolf.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPropositionBudgetNatureLolf localInstanceIn(EOEditingContext editingContext, EOPropositionBudgetNatureLolf eo) {
    EOPropositionBudgetNatureLolf localInstance = (eo == null) ? null : (EOPropositionBudgetNatureLolf)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPropositionBudgetNatureLolf#localInstanceIn a la place.
   */
	public static EOPropositionBudgetNatureLolf localInstanceOf(EOEditingContext editingContext, EOPropositionBudgetNatureLolf eo) {
		return EOPropositionBudgetNatureLolf.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPropositionBudgetNatureLolf fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPropositionBudgetNatureLolf fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPropositionBudgetNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPropositionBudgetNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPropositionBudgetNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPropositionBudgetNatureLolf fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPropositionBudgetNatureLolf eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPropositionBudgetNatureLolf)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPropositionBudgetNatureLolf fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPropositionBudgetNatureLolf eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPropositionBudgetNatureLolf ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPropositionBudgetNatureLolf fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
