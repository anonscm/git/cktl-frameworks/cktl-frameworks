/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetSaisie.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetSaisie extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetSaisie";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Saisie";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdsaId";

	public static final String BDSA_DATE_CREATION_KEY = "bdsaDateCreation";
	public static final String BDSA_DATE_VALIDATION_KEY = "bdsaDateValidation";
	public static final String BDSA_LIBELLE_KEY = "bdsaLibelle";

// Attributs non visibles
	public static final String UTL_ORDRE_VALIDATION_KEY = "utlOrdreValidation";
	public static final String BDSA_ID_KEY = "bdsaId";
	public static final String TYSA_ID_KEY = "tysaId";
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String BDSA_DATE_CREATION_COLKEY = "BDSA_DATE_CREATION";
	public static final String BDSA_DATE_VALIDATION_COLKEY = "BDSA_DATE_VALIDATION";
	public static final String BDSA_LIBELLE_COLKEY = "BDSA_LIBELLE";

	public static final String UTL_ORDRE_VALIDATION_COLKEY = "UTL_ORDRE_VALIDATION";
	public static final String BDSA_ID_COLKEY = "BDSA_ID";
	public static final String TYSA_ID_COLKEY = "TYSA_ID";
	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String PROPOSITION_BUDGETS_KEY = "propositionBudgets";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_SAISIE_KEY = "typeSaisie";
	public static final String UTILISATEUR_KEY = "utilisateur";
	public static final String UTILISATEUR_VALIDATION_KEY = "utilisateurValidation";



	// Accessors methods
  public NSTimestamp bdsaDateCreation() {
    return (NSTimestamp) storedValueForKey(BDSA_DATE_CREATION_KEY);
  }

  public void setBdsaDateCreation(NSTimestamp value) {
    takeStoredValueForKey(value, BDSA_DATE_CREATION_KEY);
  }

  public NSTimestamp bdsaDateValidation() {
    return (NSTimestamp) storedValueForKey(BDSA_DATE_VALIDATION_KEY);
  }

  public void setBdsaDateValidation(NSTimestamp value) {
    takeStoredValueForKey(value, BDSA_DATE_VALIDATION_KEY);
  }

  public String bdsaLibelle() {
    return (String) storedValueForKey(BDSA_LIBELLE_KEY);
  }

  public void setBdsaLibelle(String value) {
    takeStoredValueForKey(value, BDSA_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }
  
	public void setTypeEtat(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
		takeStoredValueForKey(value,TYPE_ETAT_KEY);
	}


  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie typeSaisie() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie)storedValueForKey(TYPE_SAISIE_KEY);
  }
  
	public void setTypeSaisie(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie value) {
		takeStoredValueForKey(value,TYPE_SAISIE_KEY);
	}


  public void setTypeSaisieRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie oldValue = typeSaisie();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_SAISIE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_SAISIE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }
  
	public void setUtilisateur(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_KEY);
	}


  public void setUtilisateurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateurValidation() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_VALIDATION_KEY);
  }
  
	public void setUtilisateurValidation(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
		takeStoredValueForKey(value,UTILISATEUR_VALIDATION_KEY);
	}


  public void setUtilisateurValidationRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur oldValue = utilisateurValidation();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_VALIDATION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_VALIDATION_KEY);
    }
  }
  
  public NSArray propositionBudgets() {
    return (NSArray)storedValueForKey(PROPOSITION_BUDGETS_KEY);
  }
  
  //ICI
  public void setPropositionBudgets(NSArray values) {
	  takeStoredValueForKey(values,PROPOSITION_BUDGETS_KEY);
  }
  

  public NSArray propositionBudgets(EOQualifier qualifier) {
    return propositionBudgets(qualifier, null, false);
  }

  public NSArray propositionBudgets(EOQualifier qualifier, boolean fetch) {
    return propositionBudgets(qualifier, null, fetch);
  }

  public NSArray propositionBudgets(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget.BUDGET_SAISIE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = propositionBudgets();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPropositionBudgetsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGETS_KEY);
  }

  public void removeFromPropositionBudgetsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGETS_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget createPropositionBudgetsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkBibasse_PropositionBudget");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PROPOSITION_BUDGETS_KEY);
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget) eo;
  }

  public void deletePropositionBudgetsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PROPOSITION_BUDGETS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPropositionBudgetsRelationships() {
    Enumeration objects = propositionBudgets().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePropositionBudgetsRelationship((org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget)objects.nextElement());
    }
  }


/**
 * Creer une instance de EOBudgetSaisie avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetSaisie createEOBudgetSaisie(EOEditingContext editingContext, NSTimestamp bdsaDateCreation
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie typeSaisie, org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur utilisateur			) {
    EOBudgetSaisie eo = (EOBudgetSaisie) createAndInsertInstance(editingContext, _EOBudgetSaisie.ENTITY_NAME);    
		eo.setBdsaDateCreation(bdsaDateCreation);
    eo.setExerciceRelationship(exercice);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeSaisieRelationship(typeSaisie);
    eo.setUtilisateurRelationship(utilisateur);
    return eo;
  }

  
	  public EOBudgetSaisie localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetSaisie)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetSaisie creerInstance(EOEditingContext editingContext) {
	  		EOBudgetSaisie object = (EOBudgetSaisie)createAndInsertInstance(editingContext, _EOBudgetSaisie.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetSaisie localInstanceIn(EOEditingContext editingContext, EOBudgetSaisie eo) {
    EOBudgetSaisie localInstance = (eo == null) ? null : (EOBudgetSaisie)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetSaisie#localInstanceIn a la place.
   */
	public static EOBudgetSaisie localInstanceOf(EOEditingContext editingContext, EOBudgetSaisie eo) {
		return EOBudgetSaisie.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetSaisie fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetSaisie fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetSaisie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetSaisie)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetSaisie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetSaisie fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetSaisie eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetSaisie)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetSaisie fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetSaisie eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetSaisie ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetSaisie fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
