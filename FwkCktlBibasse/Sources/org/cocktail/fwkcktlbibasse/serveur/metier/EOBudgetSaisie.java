/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.metier;

/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
import java.math.BigDecimal;

import org.cocktail.fwkcktlbibasse.serveur.finder.Finder;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderBudgetParametre;
import org.cocktail.fwkcktlbibasse.serveur.finder.FinderPlanComptableExer;
import org.cocktail.fwkcktlbibasse.serveur.process.ProcessBudgetSaisie;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXEOControlUtilities;
import er.extensions.eof.ERXQ;


public class EOBudgetSaisie extends _EOBudgetSaisie {

	public final static String ETAT_EN_COURS = "EN COURS";
	public final static String ETAT_VERROUILLE = "VALIDE";
	public final static String ETAT_CONTROLE = "CONTROLE";
	public final static String ETAT_CLOT="CLOTURE";
	public final static String ETAT_VOTE = "VOTE";

	Number bdsaIdProc;

	public EOBudgetSaisie() {
		super();
		bdsaIdProc=null;
	}

	public void setBdsaIdProc(Number value) {
		bdsaIdProc=value;
	}

	public Number bdsaIdProc() {
		return bdsaIdProc;
	}

	public static EOBudgetSaisie ouvrirNouveauBudgetSaisie(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie, EOUtilisateur utilisateur) throws Exception {
		return ProcessBudgetSaisie.ouvrirBudgetSaisie(ec, exercice, typeSaisie, utilisateur);
	}

	public boolean isSaisieEnCours() {
		if (bdsaDateValidation()!=null)
			return false;
		return true;
	}

	public boolean isVotable() {
		if (!isSaisieEnCours())
			return false;

		NSArray<EOPropositionBudget> propositions = propositionBudgets();
		if (propositions != null && propositions.count()>0) {
			EOQualifier qualifier = ERXQ.and(
				ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), EOPropositionBudget.ETAT_CLOTURE),
				ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), EOTypeEtat.ETAT_ANNULE));
			propositions = ERXQ.filtered(propositions, qualifier);
			if (propositions.count()>0) {
				return false;
			}
		} else {
			return false;
		}
		// TODO : à compléter
		// - si toutes les propositions rattachees ne sont pas verouillees alors non
		// - ...


		// si tout est bon ok
		return true;
	}

	public boolean isRefusable() {
		return isVotable();
	}

	public void voter(EOEditingContext ec, EOUtilisateur utilisateur) throws Exception {
		// TODO : à compléter

		if (!isVotable())
			throw new Exception("Erreur ...");
		
		try {
			for (int i=0; i<propositionBudgets().count(); i++)
				((EOPropositionBudget)propositionBudgets().objectAtIndex(i)).voter(ec);
			
			EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_VOTE);
			setTypeEtat(typeEtat);
		} catch (Exception e) {
			throw new Exception(e);
		}
	}

	public void refuser(EOEditingContext ec, EOUtilisateur utilisateur) throws Exception {
		// TODO : à compléter

		if (!isRefusable())
			throw new Exception("Erreur ...");
	}

	public boolean isCloture() {
		boolean isCloture = typeEtat().tyetLibelle().equalsIgnoreCase(ETAT_CLOT);
		
		return isCloture;
	}

	public boolean isVote() {
		return typeEtat().tyetLibelle().equalsIgnoreCase(ETAT_VOTE);
	}
	
	public void ajouterSaisie(EOEditingContext ec, EOPropositionBudget propositionBudget) throws Exception {
		ajusterSaisie(ec, propositionBudget, new BigDecimal(1));
	}
	public void retrancherSaisie(EOEditingContext ec, EOPropositionBudget propositionBudget) throws Exception {
		ajusterSaisie(ec, propositionBudget, new BigDecimal(-1));
	}
	private void ajusterSaisie(EOEditingContext ec, EOPropositionBudget propositionBudget, BigDecimal coef) throws Exception {
		if (!isSaisieEnCours())
			throw new Exception("Erreur, la saisie du budget "+bdsaLibelle()+" n'est pas possible.");

		EOOrgan organ=organBudget(propositionBudget.organ());

		// on modifie
		for (int i=0; i<propositionBudget.propositionBudgetNatureLolfs().count(); i++) {
			EOPropositionBudgetNatureLolf prop=(EOPropositionBudgetNatureLolf)propositionBudget.propositionBudgetNatureLolfs().objectAtIndex(i);

			if (organ!=null) {
				EOPlanComptableExer plancoSaisie=FinderPlanComptableExer.findPlanComptableExerBudget(ec, prop.planComptableExer());
				if (plancoSaisie==null)
					throw new Exception("Pas d'imputation correspondante trouvée dans le masque de saisie de Bibasse pour le compte "+
							prop.planComptableExer().pcoNum());
						
				//NATURE
				EOQualifier qualSaisieNature = ERXQ.and(
						ERXQ.equals(EOBudgetSaisieNature.BUDGET_SAISIE_KEY, this),
						ERXQ.equals(EOBudgetSaisieNature.PLAN_COMPTABLE_KEY, plancoSaisie),
						ERXQ.equals(EOBudgetSaisieNature.TYPE_CREDIT_KEY, prop.typeCredit()),
						ERXQ.equals(EOBudgetSaisieNature.ORGAN_KEY, organ)
					);
				NSArray arraySaisieNature=Finder.fetchArray(ec, EOBudgetSaisieNature.ENTITY_NAME,
						EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieNature.PLAN_COMPTABLE_KEY+"=%@ and "
						+EOBudgetSaisieNature.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieNature.ORGAN_KEY+"=%@",
						new NSArray(new Object[] { this, plancoSaisie, prop.typeCredit(), organ}),null,false);
				arraySaisieNature = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieNature, EOBudgetSaisieNature.ENTITY_NAME, qualSaisieNature, null,
						true, true, true, true, false, false);

				EOBudgetSaisieNature saisieNature=null;
				if (arraySaisieNature==null || arraySaisieNature.count()==0) {
					saisieNature=EOBudgetSaisieNature.createEOBudgetSaisieNature(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), this, 
							this.exercice(), organ, plancoSaisie, prop.typeCredit(), FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));
				} 
				else
					saisieNature=(EOBudgetSaisieNature)arraySaisieNature.objectAtIndex(0);

				saisieNature.setBdsnSaisi(saisieNature.bdsnSaisi().add(prop.prbnlMontant().multiply(coef)));
				saisieNature.setBdsnMontant(saisieNature.bdsnSaisi().add(saisieNature.bdsnVote()));
//				saisieNature.setBdsnMontant(saisieNature.bdsnMontant().add(saisieNature.bdsnSaisi()));

				//GESTION
				EOQualifier qualSaisieGestion = ERXQ.and(
						ERXQ.equals(EOBudgetSaisieGestion.BUDGET_SAISIE_KEY, this),
						ERXQ.equals(EOBudgetSaisieGestion.TYPE_ACTION_KEY, prop.typeAction()),
						ERXQ.equals(EOBudgetSaisieGestion.TYPE_CREDIT_KEY, prop.typeCredit()),
						ERXQ.equals(EOBudgetSaisieGestion.ORGAN_KEY, organ)
					);
				NSArray arraySaisieGestion=Finder.fetchArray(ec, EOBudgetSaisieGestion.ENTITY_NAME,
						EOBudgetSaisieGestion.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieGestion.TYPE_ACTION_KEY+"=%@ and "
						+EOBudgetSaisieGestion.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieGestion.ORGAN_KEY+"=%@",
						new NSArray(new Object[] { this, prop.typeAction(), prop.typeCredit(), organ}),null,false);
				arraySaisieGestion = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieGestion, EOBudgetSaisieGestion.ENTITY_NAME, qualSaisieGestion, null,
						true, true, true, true, false, false);
				EOBudgetSaisieGestion saisieGestion=null;
				if (arraySaisieGestion==null || arraySaisieGestion.count()==0) {
					saisieGestion=EOBudgetSaisieGestion.createEOBudgetSaisieGestion(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), this, 
							this.exercice(), organ, prop.typeAction(), prop.typeCredit(), FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));
				} 
				else
					saisieGestion=(EOBudgetSaisieGestion)arraySaisieGestion.objectAtIndex(0);

				saisieGestion.setBdsgSaisi(saisieGestion.bdsgSaisi().add(prop.prbnlMontant().multiply(coef)));
				saisieGestion.setBdsgMontant(saisieGestion.bdsgSaisi().add(saisieGestion.bdsgVote()));
//				saisieGestion.setBdsgMontant(saisieGestion.bdsgMontant().add(saisieGestion.bdsgSaisi()));
				
				// NATURE LOLF
				EOOrgan organEtab=organEtablissement(organ);
				
				EOQualifier qualSaisieNatureLolf = ERXQ.and(
						ERXQ.equals(EOBudgetSaisieNatureLolf.BUDGET_SAISIE_KEY, this),
						ERXQ.equals(EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY, prop.typeAction()),
						ERXQ.equals(EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY, prop.typeCredit()),
						ERXQ.equals(EOBudgetSaisieNatureLolf.ORGAN_KEY, organEtab),
						ERXQ.equals(EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY, prop.planComptableExer())
					);
				NSArray arraySaisieNatureLolf=Finder.fetchArray(ec, EOBudgetSaisieNatureLolf.ENTITY_NAME,
						EOBudgetSaisieNatureLolf.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieNatureLolf.TYPE_ACTION_KEY+"=%@ and "
						+EOBudgetSaisieNatureLolf.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieNatureLolf.ORGAN_KEY+"=%@ and "
						+EOBudgetSaisieNatureLolf.PLAN_COMPTABLE_KEY+"=%@",
						new NSArray(new Object[] { this, prop.typeAction(), prop.typeCredit(), organEtab, prop.planComptableExer()}),null,false);
				arraySaisieNatureLolf = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieNatureLolf, EOBudgetSaisieNatureLolf.ENTITY_NAME,
						qualSaisieNatureLolf, null, true, true, true, true, false, false);
				EOBudgetSaisieNatureLolf saisieNatureLolf=null;
				if (arraySaisieNatureLolf==null || arraySaisieNatureLolf.count()==0) {
					saisieNatureLolf=EOBudgetSaisieNatureLolf.createEOBudgetSaisieNatureLolf
					(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), this, this.exercice(), organEtab, 
							prop.planComptableExer(), prop.typeAction(), prop.typeCredit(), FinderTypeEtat.getTypeEtat(ec, ETAT_CLOT));
				} 
				else
					saisieNatureLolf=(EOBudgetSaisieNatureLolf)arraySaisieNatureLolf.objectAtIndex(0);
				
				saisieNatureLolf.setBdslSaisi(saisieNatureLolf.bdslSaisi().add(prop.prbnlMontant().multiply(coef)));
				saisieNatureLolf.setBdslMontant(saisieNatureLolf.bdslSaisi().add(saisieNatureLolf.bdslVote()));
			}
		}
	}

	private EOOrgan organEtablissement(EOOrgan organ) {
		if (organ==null || organ.orgNiveau().intValue()<0)
			return null;
		if (organ.orgNiveau().intValue()==0)
			return organ;
		return organEtablissement(organ.organPere());
	}
	
	public void verrouillerSiPossible(EOEditingContext ec, EOOrgan organ) {
		if (organ==null)
			return;

		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EOPropositionBudget.BUDGET_SAISIE_KEY, this),
				ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), EOPropositionBudget.ETAT_VERROUILLE),
				qualifierBrancheOrganFilsBudget(organ)
			);
		NSArray array=Finder.fetchArray(EOPropositionBudget.ENTITY_NAME, qualifier, null, ec, true);
		array = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, array, EOPropositionBudget.ENTITY_NAME, qualifier, null,
				true, true, true, false, true, true);
		if (array.count()==0) {
			EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_VERROUILLE);
			modifierEtatSaisies(ec, organBudget(organ), typeEtat);
		}
	}

	public void deverrouiller(EOEditingContext ec, EOOrgan organ) {
		EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_EN_COURS);
		modifierEtatSaisies(ec, organBudget(organ), typeEtat);
	}

	public void controlerSiPossible(EOEditingContext ec, EOOrgan organ) {
		if (organ==null)
			return;

		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EOPropositionBudget.BUDGET_SAISIE_KEY, this),
				ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), EOPropositionBudget.ETAT_CONTROLE),
				qualifierBrancheOrganFilsBudget(organ)
			);

		NSArray array=Finder.fetchArray(EOPropositionBudget.ENTITY_NAME, qualifier, null, ec, true);
		array = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, array, EOPropositionBudget.ENTITY_NAME, qualifier, null,
				true, true, true, false, true, true);

		if (array.count()==0) {
			EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_CONTROLE);
			modifierEtatSaisies(ec, organBudget(organ), typeEtat);
		}
	}

	public void deControler(EOEditingContext ec, EOOrgan organ) {
		EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_VERROUILLE);
		modifierEtatSaisies(ec, organBudget(organ), typeEtat);
	}

	public void cloturerSiPossible(EOEditingContext ec, EOOrgan organ) {
		if (organ==null)
			return;

		EOQualifier qualifier = ERXQ.and(
				ERXQ.equals(EOPropositionBudget.BUDGET_SAISIE_KEY, this),
				ERXQ.notEquals(ERXQ.keyPath(EOPropositionBudget.TYPE_ETAT_KEY,EOTypeEtat.TYET_LIBELLE_KEY), ETAT_CLOT),
				qualifierBrancheOrganFilsBudget(organ)
			);

		NSArray array=Finder.fetchArray(EOPropositionBudget.ENTITY_NAME, qualifier, null, ec, true);
		array = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, array, EOPropositionBudget.ENTITY_NAME, qualifier, null,
				true, true, true, false, true, true);

		if (array.count()==0) {
			EOTypeEtat typeEtat=FinderTypeEtat.getTypeEtat(editingContext(), ETAT_CLOT);
			modifierEtatSaisies(ec, organBudget(organ), typeEtat);
			setTypeEtat(typeEtat);
		}
	}

	private void modifierEtatSaisies(EOEditingContext ec, EOOrgan organ, EOTypeEtat typeEtat) {
		//NATURE
		NSArray arraySaisieNature=Finder.fetchArray(ec, EOBudgetSaisieNature.ENTITY_NAME,
				EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieNature.ORGAN_KEY+"=%@ and "+
				EOBudgetSaisieNature.TYPE_ETAT_KEY+"!=%@", new NSArray(new Object[] { this, organ, typeEtat}),null,false);

		for (int i=0; i<arraySaisieNature.count(); i++)
			((EOBudgetSaisieNature)arraySaisieNature.objectAtIndex(i)).setTypeEtatRelationship(typeEtat);

		//GESTION
		NSArray arraySaisieGestion=Finder.fetchArray(ec, EOBudgetSaisieGestion.ENTITY_NAME,
				EOBudgetSaisieGestion.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieGestion.ORGAN_KEY+"=%@ and "+
				EOBudgetSaisieNature.TYPE_ETAT_KEY+"!=%@",new NSArray(new Object[] { this, organ, typeEtat}),null,false);

		for (int i=0; i<arraySaisieGestion.count(); i++)
			((EOBudgetSaisieGestion)arraySaisieGestion.objectAtIndex(i)).setTypeEtatRelationship(typeEtat);
	}

	public EOOrgan organBudget(EOOrgan organ) {
		if (organ==null || organ.orgNiveau().intValue()<2)
			return null;

		int niveau=FinderBudgetParametre.getParametreOrganNiveauSaisie(editingContext(), exercice()).intValue();

		if (organ.orgNiveau().intValue()<niveau)
			return null;
		if (organ.orgNiveau().intValue()==niveau)
			return organ;
		if (organ.orgNiveau().intValue()-niveau==1)
			return organ.organPere();
		if (organ.orgNiveau().intValue()-niveau==2)
			return organ.organPere().organPere();

		return null;
	}

	public EOQualifier qualifierBrancheOrganFilsBudget(EOOrgan organ) {
		if (organ==null || organ.orgNiveau().intValue()<2)
			return null;

		int niveau=FinderBudgetParametre.getParametreOrganNiveauSaisie(editingContext(), exercice()).intValue();

		if (organ.orgNiveau().intValue()<niveau)
			return null;

		EOOrgan organBudget=null;
		if (organ.orgNiveau().intValue()==niveau)
			organBudget=organ;
		if (organ.orgNiveau().intValue()-niveau==1)
			organBudget=organ.organPere();
		if (organ.orgNiveau().intValue()-niveau==2)
			organBudget=organ.organPere().organPere();

		NSMutableArray arrayQualifiers=new NSMutableArray();
		arrayQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.ORGAN_KEY+"=%@", new NSArray(organBudget)));
		arrayQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.ORGAN_KEY+"."+EOOrgan.ORGAN_PERE_KEY+"=%@", new NSArray(organBudget)));
		if (niveau==2)
			arrayQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisieNature.ORGAN_KEY+"."+EOOrgan.ORGAN_PERE_KEY
					+"."+EOOrgan.ORGAN_PERE_KEY+"=%@", new NSArray(organBudget)));
		return new EOOrQualifier(arrayQualifiers);
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}



	/**
	 * Peut etre appele a partir des factories.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {

		super.validateObjectMetier();
	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate.
	 * Vous pouvez definir un delegate qui sera appele lors de l'execution de cette methode. AfwkDroitsUtilsRecord#registerValidationDelegate(IValidationDelegate).
	 *
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		super.validateBeforeTransactionSave();
	}

	public void retrancherSaisie(EOEditingContext ec, EOPropositionBudgetNatureLolf propositionBudgetNatLolf) throws Exception {
		ajusterSaisie(ec, propositionBudgetNatLolf, new BigDecimal(-1));
	}
	public void ajusterSaisie(EOEditingContext ec, EOPropositionBudgetNatureLolf prop, BigDecimal coef) throws Exception {
		if (!isSaisieEnCours())
			throw new Exception("Erreur, la saisie du budget "+bdsaLibelle()+" n'est pas possible.");
		EOOrgan organ=organBudget(prop.propositionBudget().organ());
		
		if (organ!=null) {
			EOPlanComptableExer plancoSaisie=FinderPlanComptableExer.findPlanComptableExerBudget(ec, prop.planComptableExer());
			if (plancoSaisie==null)
				throw new Exception("Pas d'imputation correspondante trouvée dans le masque de saisie de Bibasse pour le compte "+
						prop.planComptableExer().pcoNum());

			//NATURE
			EOQualifier qualSaisieNature = ERXQ.and(
					ERXQ.equals(EOBudgetSaisieNature.BUDGET_SAISIE_KEY, this),
					ERXQ.equals(EOBudgetSaisieNature.PLAN_COMPTABLE_KEY, plancoSaisie),
					ERXQ.equals(EOBudgetSaisieNature.TYPE_CREDIT_KEY, prop.typeCredit()),
					ERXQ.equals(EOBudgetSaisieNature.ORGAN_KEY, organ)
				);
			NSArray arraySaisieNature=Finder.fetchArray(ec, EOBudgetSaisieNature.ENTITY_NAME,
					EOBudgetSaisieNature.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieNature.PLAN_COMPTABLE_KEY+"=%@ and "
					+EOBudgetSaisieNature.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieNature.ORGAN_KEY+"=%@",
					new NSArray(new Object[] { this, plancoSaisie, prop.typeCredit(), organ}),null,false);
			arraySaisieNature = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieNature, EOBudgetSaisieNature.ENTITY_NAME, qualSaisieNature, null,
					true, true, true, true, false, false);

			EOBudgetSaisieNature saisieNature=null;
			if (arraySaisieNature==null || arraySaisieNature.count()==0) {
				saisieNature=EOBudgetSaisieNature.createEOBudgetSaisieNature(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), this, 
						this.exercice(), organ, plancoSaisie, prop.typeCredit(), FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));
			} 
			else
				saisieNature=(EOBudgetSaisieNature)arraySaisieNature.objectAtIndex(0);
			
			saisieNature.setBdsnSaisi(saisieNature.bdsnSaisi().add(prop.prbnlMontant().multiply(coef)));
			saisieNature.setBdsnVote(saisieNature.bdsnMontant().add(saisieNature.bdsnSaisi()));
			
			//GESTION
			EOQualifier qualSaisieGestion = ERXQ.and(
					ERXQ.equals(EOBudgetSaisieGestion.BUDGET_SAISIE_KEY, this),
					ERXQ.equals(EOBudgetSaisieGestion.TYPE_ACTION_KEY, prop.typeAction()),
					ERXQ.equals(EOBudgetSaisieGestion.TYPE_CREDIT_KEY, prop.typeCredit()),
					ERXQ.equals(EOBudgetSaisieGestion.ORGAN_KEY, organ)
				);
			NSArray arraySaisieGestion=Finder.fetchArray(ec, EOBudgetSaisieGestion.ENTITY_NAME,
					EOBudgetSaisieGestion.BUDGET_SAISIE_KEY+"=%@ and "+EOBudgetSaisieGestion.TYPE_ACTION_KEY+"=%@ and "
					+EOBudgetSaisieGestion.TYPE_CREDIT_KEY+"=%@ and "+EOBudgetSaisieGestion.ORGAN_KEY+"=%@",
					new NSArray(new Object[] { this, prop.typeAction(), prop.typeCredit(), organ}),null,false);
			arraySaisieGestion = ERXEOControlUtilities.filteredObjectsWithQualifier(ec, arraySaisieGestion, EOBudgetSaisieGestion.ENTITY_NAME, qualSaisieGestion, null,
					true, true, true, true, false, false);

			EOBudgetSaisieGestion saisieGestion=null;
			if (arraySaisieGestion==null || arraySaisieGestion.count()==0) {
				saisieGestion=EOBudgetSaisieGestion.createEOBudgetSaisieGestion(ec, new BigDecimal(0.0), new BigDecimal(0.0), new BigDecimal(0.0), this, 
						this.exercice(), organ, prop.typeAction(), prop.typeCredit(), FinderTypeEtat.getTypeEtat(ec, ETAT_EN_COURS));
			} 
			else
				saisieGestion=(EOBudgetSaisieGestion)arraySaisieGestion.objectAtIndex(0);
			
			saisieGestion.setBdsgSaisi(saisieGestion.bdsgSaisi().add(prop.prbnlMontant().multiply(coef)));
			saisieGestion.setBdsgVote(saisieGestion.bdsgMontant().add(saisieGestion.bdsgSaisi()));
		}

	}

}
