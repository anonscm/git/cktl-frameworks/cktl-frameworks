/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOBudgetVoteGestion.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOBudgetVoteGestion extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_BudgetVoteGestion";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.Budget_Vote_Gestion";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "bdvgId";

	public static final String BDVG_CONVENTIONS_KEY = "bdvgConventions";
	public static final String BDVG_CREDITS_KEY = "bdvgCredits";
	public static final String BDVG_DBMS_KEY = "bdvgDbms";
	public static final String BDVG_DEBITS_KEY = "bdvgDebits";
	public static final String BDVG_OUVERTS_KEY = "bdvgOuverts";
	public static final String BDVG_PRIMITIFS_KEY = "bdvgPrimitifs";
	public static final String BDVG_PROVISOIRES_KEY = "bdvgProvisoires";
	public static final String BDVG_RELIQUATS_KEY = "bdvgReliquats";
	public static final String BDVG_VOTES_KEY = "bdvgVotes";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";
	public static final String TYAC_ID_KEY = "tyacId";
	public static final String TCD_ORDRE_KEY = "tcdOrdre";
	public static final String ORG_ID_KEY = "orgId";
	public static final String BDVG_ID_KEY = "bdvgId";

//Colonnes dans la base de donnees
	public static final String BDVG_CONVENTIONS_COLKEY = "BDVG_CONVENTIONS";
	public static final String BDVG_CREDITS_COLKEY = "BDVG_CREDITS";
	public static final String BDVG_DBMS_COLKEY = "BDVG_DBMS";
	public static final String BDVG_DEBITS_COLKEY = "BDVG_DEBITS";
	public static final String BDVG_OUVERTS_COLKEY = "BDVG_OUVERTS";
	public static final String BDVG_PRIMITIFS_COLKEY = "BDVG_PRIMITIFS";
	public static final String BDVG_PROVISOIRES_COLKEY = "BDVG_PROVISOIRES";
	public static final String BDVG_RELIQUATS_COLKEY = "BDVG_RELIQUATS";
	public static final String BDVG_VOTES_COLKEY = "BDVG_VOTES";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";
	public static final String TYAC_ID_COLKEY = "TYAC_ID";
	public static final String TCD_ORDRE_COLKEY = "TCD_ORDRE";
	public static final String ORG_ID_COLKEY = "ORG_ID";
	public static final String BDVG_ID_COLKEY = "BDVG_ID";


	// Relationships
	public static final String EXERCICE_KEY = "exercice";
	public static final String ORGAN_KEY = "organ";
	public static final String TYPE_ACTION_KEY = "typeAction";
	public static final String TYPE_CREDIT_KEY = "typeCredit";



	// Accessors methods
  public java.math.BigDecimal bdvgConventions() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_CONVENTIONS_KEY);
  }

  public void setBdvgConventions(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_CONVENTIONS_KEY);
  }

  public java.math.BigDecimal bdvgCredits() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_CREDITS_KEY);
  }

  public void setBdvgCredits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_CREDITS_KEY);
  }

  public java.math.BigDecimal bdvgDbms() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_DBMS_KEY);
  }

  public void setBdvgDbms(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_DBMS_KEY);
  }

  public java.math.BigDecimal bdvgDebits() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_DEBITS_KEY);
  }

  public void setBdvgDebits(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_DEBITS_KEY);
  }

  public java.math.BigDecimal bdvgOuverts() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_OUVERTS_KEY);
  }

  public void setBdvgOuverts(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_OUVERTS_KEY);
  }

  public java.math.BigDecimal bdvgPrimitifs() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_PRIMITIFS_KEY);
  }

  public void setBdvgPrimitifs(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_PRIMITIFS_KEY);
  }

  public java.math.BigDecimal bdvgProvisoires() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_PROVISOIRES_KEY);
  }

  public void setBdvgProvisoires(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_PROVISOIRES_KEY);
  }

  public java.math.BigDecimal bdvgReliquats() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_RELIQUATS_KEY);
  }

  public void setBdvgReliquats(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_RELIQUATS_KEY);
  }

  public java.math.BigDecimal bdvgVotes() {
    return (java.math.BigDecimal) storedValueForKey(BDVG_VOTES_KEY);
  }

  public void setBdvgVotes(java.math.BigDecimal value) {
    takeStoredValueForKey(value, BDVG_VOTES_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOExercice)storedValueForKey(EXERCICE_KEY);
  }
  
	public void setExercice(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
		takeStoredValueForKey(value,EXERCICE_KEY);
	}


  public void setExerciceRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOExercice value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOExercice oldValue = exercice();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, EXERCICE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, EXERCICE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan)storedValueForKey(ORGAN_KEY);
  }
  
	public void setOrgan(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
		takeStoredValueForKey(value,ORGAN_KEY);
	}


  public void setOrganRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan oldValue = organ();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, ORGAN_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, ORGAN_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction)storedValueForKey(TYPE_ACTION_KEY);
  }
  
	public void setTypeAction(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
		takeStoredValueForKey(value,TYPE_ACTION_KEY);
	}


  public void setTypeActionRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction oldValue = typeAction();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ACTION_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ACTION_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)storedValueForKey(TYPE_CREDIT_KEY);
  }
  
	public void setTypeCredit(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
		takeStoredValueForKey(value,TYPE_CREDIT_KEY);
	}


  public void setTypeCreditRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit oldValue = typeCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_CREDIT_KEY);
    }
  }
  

/**
 * Creer une instance de EOBudgetVoteGestion avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOBudgetVoteGestion createEOBudgetVoteGestion(EOEditingContext editingContext, java.math.BigDecimal bdvgConventions
, java.math.BigDecimal bdvgCredits
, java.math.BigDecimal bdvgDbms
, java.math.BigDecimal bdvgDebits
, java.math.BigDecimal bdvgOuverts
, java.math.BigDecimal bdvgPrimitifs
, java.math.BigDecimal bdvgProvisoires
, java.math.BigDecimal bdvgReliquats
, java.math.BigDecimal bdvgVotes
, org.cocktail.fwkcktljefyadmin.common.metier.EOExercice exercice, org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan organ, org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction typeAction, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit typeCredit			) {
    EOBudgetVoteGestion eo = (EOBudgetVoteGestion) createAndInsertInstance(editingContext, _EOBudgetVoteGestion.ENTITY_NAME);    
		eo.setBdvgConventions(bdvgConventions);
		eo.setBdvgCredits(bdvgCredits);
		eo.setBdvgDbms(bdvgDbms);
		eo.setBdvgDebits(bdvgDebits);
		eo.setBdvgOuverts(bdvgOuverts);
		eo.setBdvgPrimitifs(bdvgPrimitifs);
		eo.setBdvgProvisoires(bdvgProvisoires);
		eo.setBdvgReliquats(bdvgReliquats);
		eo.setBdvgVotes(bdvgVotes);
    eo.setExerciceRelationship(exercice);
    eo.setOrganRelationship(organ);
    eo.setTypeActionRelationship(typeAction);
    eo.setTypeCreditRelationship(typeCredit);
    return eo;
  }

  
	  public EOBudgetVoteGestion localInstanceIn(EOEditingContext editingContext) {
	  		return (EOBudgetVoteGestion)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOBudgetVoteGestion creerInstance(EOEditingContext editingContext) {
	  		EOBudgetVoteGestion object = (EOBudgetVoteGestion)createAndInsertInstance(editingContext, _EOBudgetVoteGestion.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOBudgetVoteGestion localInstanceIn(EOEditingContext editingContext, EOBudgetVoteGestion eo) {
    EOBudgetVoteGestion localInstance = (eo == null) ? null : (EOBudgetVoteGestion)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOBudgetVoteGestion#localInstanceIn a la place.
   */
	public static EOBudgetVoteGestion localInstanceOf(EOEditingContext editingContext, EOBudgetVoteGestion eo) {
		return EOBudgetVoteGestion.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOBudgetVoteGestion fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOBudgetVoteGestion fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOBudgetVoteGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOBudgetVoteGestion)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOBudgetVoteGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOBudgetVoteGestion fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOBudgetVoteGestion eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOBudgetVoteGestion)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOBudgetVoteGestion fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOBudgetVoteGestion eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOBudgetVoteGestion ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOBudgetVoteGestion fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
