/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2009 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPropositionPositionnementMouvement.java instead.
package org.cocktail.fwkcktlbibasse.serveur.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPropositionPositionnementMouvement extends  AFwkCktlBibasseRecord {
	public static final String ENTITY_NAME = "FwkBibasse_PropositionPositionnementMouvement";
	public static final String ENTITY_TABLE_NAME = "JEFY_BUDGET.PROPOSITION_POSIT_MOUV";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prpmId";

	public static final String PRPM_MONTANT_KEY = "prpmMontant";

// Attributs non visibles
	public static final String BMOU_ID_KEY = "bmouId";
	public static final String PRPO_ID_KEY = "prpoId";
	public static final String BDMC_ID_KEY = "bdmcId";
	public static final String PRPM_ID_KEY = "prpmId";

//Colonnes dans la base de donnees
	public static final String PRPM_MONTANT_COLKEY = "PRPM_MONTANT";

	public static final String BMOU_ID_COLKEY = "BMOU_ID";
	public static final String PRPO_ID_COLKEY = "PRPO_ID";
	public static final String BDMC_ID_COLKEY = "BDMC_ID";
	public static final String PRPM_ID_COLKEY = "PRPM_ID";


	// Relationships
	public static final String BUDGET_MOUVEMENTS_KEY = "budgetMouvements";
	public static final String BUDGET_MOUVEMENTS_CREDIT_KEY = "budgetMouvementsCredit";
	public static final String PROPOSITION_POSITIONNEMENT_KEY = "propositionPositionnement";



	// Accessors methods
  public java.math.BigDecimal prpmMontant() {
    return (java.math.BigDecimal) storedValueForKey(PRPM_MONTANT_KEY);
  }

  public void setPrpmMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, PRPM_MONTANT_KEY);
  }

  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvements budgetMouvements() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvements)storedValueForKey(BUDGET_MOUVEMENTS_KEY);
  }
  
	public void setBudgetMouvements(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvements value) {
		takeStoredValueForKey(value,BUDGET_MOUVEMENTS_KEY);
	}


  public void setBudgetMouvementsRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvements value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvements oldValue = budgetMouvements();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_MOUVEMENTS_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_MOUVEMENTS_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvCredit budgetMouvementsCredit() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvCredit)storedValueForKey(BUDGET_MOUVEMENTS_CREDIT_KEY);
  }
  
	public void setBudgetMouvementsCredit(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvCredit value) {
		takeStoredValueForKey(value,BUDGET_MOUVEMENTS_CREDIT_KEY);
	}


  public void setBudgetMouvementsCreditRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvCredit value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMouvCredit oldValue = budgetMouvementsCredit();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, BUDGET_MOUVEMENTS_CREDIT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, BUDGET_MOUVEMENTS_CREDIT_KEY);
    }
  }
  
  public org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement propositionPositionnement() {
    return (org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement)storedValueForKey(PROPOSITION_POSITIONNEMENT_KEY);
  }
  
	public void setPropositionPositionnement(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement value) {
		takeStoredValueForKey(value,PROPOSITION_POSITIONNEMENT_KEY);
	}


  public void setPropositionPositionnementRelationship(org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement value) {
    if (value == null) {
    	org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement oldValue = propositionPositionnement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PROPOSITION_POSITIONNEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PROPOSITION_POSITIONNEMENT_KEY);
    }
  }
  

/**
 * Creer une instance de EOPropositionPositionnementMouvement avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPropositionPositionnementMouvement createEOPropositionPositionnementMouvement(EOEditingContext editingContext, java.math.BigDecimal prpmMontant
, org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement propositionPositionnement			) {
    EOPropositionPositionnementMouvement eo = (EOPropositionPositionnementMouvement) createAndInsertInstance(editingContext, _EOPropositionPositionnementMouvement.ENTITY_NAME);    
		eo.setPrpmMontant(prpmMontant);
    eo.setPropositionPositionnementRelationship(propositionPositionnement);
    return eo;
  }

  
	  public EOPropositionPositionnementMouvement localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPropositionPositionnementMouvement)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPropositionPositionnementMouvement creerInstance(EOEditingContext editingContext) {
	  		EOPropositionPositionnementMouvement object = (EOPropositionPositionnementMouvement)createAndInsertInstance(editingContext, _EOPropositionPositionnementMouvement.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPropositionPositionnementMouvement localInstanceIn(EOEditingContext editingContext, EOPropositionPositionnementMouvement eo) {
    EOPropositionPositionnementMouvement localInstance = (eo == null) ? null : (EOPropositionPositionnementMouvement)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPropositionPositionnementMouvement#localInstanceIn a la place.
   */
	public static EOPropositionPositionnementMouvement localInstanceOf(EOEditingContext editingContext, EOPropositionPositionnementMouvement eo) {
		return EOPropositionPositionnementMouvement.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPropositionPositionnementMouvement fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passe en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPropositionPositionnementMouvement fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPropositionPositionnementMouvement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPropositionPositionnementMouvement)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPropositionPositionnementMouvement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPropositionPositionnementMouvement fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPropositionPositionnementMouvement eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPropositionPositionnementMouvement)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouve, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPropositionPositionnementMouvement fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPropositionPositionnementMouvement eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPropositionPositionnementMouvement ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPropositionPositionnementMouvement fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
