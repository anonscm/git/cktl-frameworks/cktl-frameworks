/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlbibasse.serveur.procedure;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionBudget;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPropositionPositionnement;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlwebapp.server.database._CktlBasicDataBus;

import com.webobjects.eoaccess.EOUtilities;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;
import com.webobjects.foundation.NSValidation;

public class ProcedureAjouterPropositionPositionnement extends Procedure {

	private static final String PROCEDURE_NAME = "insPropositionPositionnement";

	public static final String PROCEDURE_RETOUR_CLE = "05_a_prpo_id";

	/**
	 * Appelle la procedure
	 * <BR>
	 * @param dataBus
	 *        _CktlBasicDataBus servant a gerer les transactions
	 * @param 
	 * @return
	 *        boolean indiquant si oui ou non la procedure s'est bien deroulee 
	 */
	public static boolean enregistrer(_CktlBasicDataBus dataBus, EOPropositionPositionnement propositionPositionnement) throws NSValidation.ValidationException{
		propositionPositionnement.validateBeforeTransactionSave();
		
		return dataBus.executeProcedure(ProcedureAjouterPropositionPositionnement.PROCEDURE_NAME, 
				ProcedureAjouterPropositionPositionnement.construireDictionnaire(propositionPositionnement));
	}

	/**
	 * Construit le dictionnaire d'arguments a passer a la procedure
	 * <BR>
	 * @param 
	 * @return
	 *        une instance du NSDictionary pret a l'emploi
	 */
	private static NSDictionary construireDictionnaire(EOPropositionPositionnement propositionPositionnement) {
		NSMutableDictionary dico=new NSMutableDictionary();
		NSDictionary        dicoForPrimaryKeys=null;

		dico.takeValueForKey(null, "05_a_prpo_id");

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.exercice().editingContext(), propositionPositionnement.exercice());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOExercice.EXE_ORDRE_KEY), "10_a_exe_ordre");

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.organ().editingContext(), propositionPositionnement.organ());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOOrgan.ORG_ID_KEY), "15_a_org_id");

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.typeCredit().editingContext(), propositionPositionnement.typeCredit());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeCredit.TCD_ORDRE_KEY), "20_a_tcd_ordre");

		dico.takeValueForKey(propositionPositionnement.prpoMontantPrevisionnel(), "25_a_prpo_montant_previsionnel");
		dico.takeValueForKey(propositionPositionnement.prpoMontantOuvert(), "30_a_prpo_montant_ouvert");

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.utilisateur().editingContext(), propositionPositionnement.utilisateur());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOUtilisateur.UTL_ORDRE_KEY), "35_a_utl_ordre");

		dico.takeValueForKey(propositionPositionnement.prpoDate(), "40_a_prpo_date");
		dico.takeValueForKey(propositionPositionnement.prpoCommentaire(), "45_a_prpo_commentaire");

		if (propositionPositionnement.propositionBudget()!=null) {
			if (propositionPositionnement.propositionBudget().prbuIdProc()==null) {
				dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.propositionBudget().editingContext(), propositionPositionnement.propositionBudget());
				dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOPropositionBudget.PRBU_ID_KEY), "50_a_prbu_id");
			} else {
				dico.takeValueForKey(propositionPositionnement.propositionBudget().prbuIdProc(), "50_a_prbu_id");
			}
		}

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.typeEtat().editingContext(), propositionPositionnement.typeEtat());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeEtat.TYET_ID_KEY), "55_a_tyet_id");

		dicoForPrimaryKeys=EOUtilities.primaryKeyForObject(propositionPositionnement.typeApplication().editingContext(), propositionPositionnement.typeApplication());
		dico.takeValueForKey(dicoForPrimaryKeys.objectForKey(EOTypeApplication.TYAP_ID_KEY), "60_a_tyap_id");

		return dico;
	}
}