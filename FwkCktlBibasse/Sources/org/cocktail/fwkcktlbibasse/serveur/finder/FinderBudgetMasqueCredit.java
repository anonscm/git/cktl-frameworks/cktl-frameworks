/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.finder;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMasqueCredit;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPlancoCredit;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptable;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public class FinderBudgetMasqueCredit extends Finder {

	
	/**
	 * 
	 * @return
	 */
	public static NSArray findMasqueCredit(EOEditingContext ec, EOExercice exercice) {
		EOQualifier myQualifier=EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueCredit.EXERCICE_KEY+"=%@", new NSArray(exercice));
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueCredit.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	public static NSArray findMasqueCreditTrieTypeCredit(EOEditingContext ec, EOExercice exercice)	{
		return Finder.tableauTrie(findMasqueCredit(ec, exercice), sort());
	}

	/**
	 * 
	 * @return
	 */
	public static NSArray<EOTypeCredit> findTypesCredit(EOEditingContext ec, EOExercice exercice, String section, String type)	{
		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
		NSArray mySort = new NSArray(new EOSortOrdering(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, EOSortOrdering.CompareAscending));

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueCredit.EXERCICE_KEY+"=%@", new NSArray(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_BUDGET_KEY+"!=%@",
				new NSArray(EOTypeCredit.TCD_BUDGET_RESERVE)));

		if (section != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_SECT_KEY+"=%@",
					new NSArray(section)));
		if (type != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_TYPE_KEY+"=%@",
					new NSArray(type)));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), mySort);
		fs.setRefreshesRefetchedObjects(true);
		NSArray masques=ec.objectsWithFetchSpecification(fs);

		return (NSArray)masques.valueForKey(EOBudgetMasqueCredit.TYPE_CREDIT_KEY);
	}

    public static NSArray<EOTypeCredit> findTypesCreditExecFromNature(EOEditingContext ec, EOExercice exercice, String pcoNum, String type) {
        EOQualifier qual = ERXQ.equals(EOPlancoCredit.EXERCICE_KEY, exercice);
        if (pcoNum != null)
            qual = ERXQ.and(qual, ERXQ.startsWith(EOPlancoCredit.PLAN_COMPTABLE_KEY + "." + EOPlanComptable.PCO_NUM_KEY, pcoNum));
        NSArray<EOPlancoCredit> plancoCredits = EOPlancoCredit.fetchAll(ec, qual);
        NSArray<EOTypeCredit> typesCredits = (NSArray<EOTypeCredit>) plancoCredits.valueForKey(EOPlancoCredit.TYPE_CREDIT_KEY);
        EOQualifier qualFiltre = ERXQ.equals(EOTypeCredit.EXERCICE_KEY, exercice).and(
                                 ERXQ.equals(EOTypeCredit.TCD_BUDGET_KEY, EOTypeCredit.TCD_BUDGET_EXECUTOIRE));
        if (type != null)
            qualFiltre = ERXQ.and(qualFiltre, ERXQ.equals(EOTypeCredit.TCD_TYPE_KEY, type));
        typesCredits = ERXQ.filtered(typesCredits, qualFiltre);
        typesCredits = ERXS.sorted(typesCredits, ERXS.asc(EOTypeCredit.TCD_CODE_KEY));
        return ERXArrayUtilities.arrayWithoutDuplicates(typesCredits);
    }

	
    private static NSArray<EOSortOrdering> sort() {
    	NSMutableArray<EOSortOrdering> array=new NSMutableArray<EOSortOrdering>();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetMasqueCredit.TYPE_CREDIT_KEY+"."+EOTypeCredit.TCD_CODE_KEY, 
    			EOSortOrdering.CompareCaseInsensitiveAscending));
    	return array;
    }
}
