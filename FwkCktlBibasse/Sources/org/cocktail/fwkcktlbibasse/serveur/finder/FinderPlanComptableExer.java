/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.finder;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMasqueNature;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOPlancoCredit;
import org.cocktail.fwkcktlcompta.server.metier.EOPlanComptableExer;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderPlanComptableExer extends Finder {

	/**
	 * 
	 * @return
	 */
	public static NSArray<EOPlanComptableExer> findPlanComptableExerDepenseEtBudget(EOEditingContext ec, EOExercice exercice, EOTypeCredit typeCredit){
		EOFetchSpecification fs;
		NSMutableArray<EOQualifier> mesQualifiers;
		NSMutableArray<EOPlanComptableExer> resultats=new NSMutableArray<EOPlanComptableExer>();
		
		mesQualifiers = new NSMutableArray<EOQualifier>();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.EXERCICE_KEY+"=%@", new NSArray<Object>(exercice)));
		if (typeCredit != null)
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOPlancoCredit.TYPE_CREDIT_KEY+"=%@", new NSArray<Object>(typeCredit)));

		fs = new EOFetchSpecification(EOPlancoCredit.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);
		NSArray comptesDepenses=(NSArray)ec.objectsWithFetchSpecification(fs);
		resultats.addObjectsFromArray((NSArray)comptesDepenses.valueForKey(EOPlancoCredit.PLAN_COMPTABLE_KEY));

		
		fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);
;		fs.setPrefetchingRelationshipKeyPaths(new NSArray(EOBudgetMasqueNature.PLAN_COMPTABLE_KEY));
		NSArray masques=ec.objectsWithFetchSpecification(fs);

		for (int i=0; i<masques.count(); i++) {
			EOPlanComptableExer compte=((EOBudgetMasqueNature)masques.objectAtIndex(i)).planComptable();
			if (!resultats.containsObject(compte))
				resultats.addObject(compte);
		}
		
		return Finder.tableauTrie(resultats, sort());
	}

	/**
	 * 
	 * @return
	 */
	public static EOPlanComptableExer findPlanComptableExerBudget(EOEditingContext ec, EOPlanComptableExer planco){
		EOFetchSpecification fs;
		NSMutableArray<EOQualifier> mesQualifiers=new NSMutableArray<EOQualifier>(), mesOrQualifiers=new NSMutableArray<EOQualifier>();
		NSMutableArray<EOPlanComptableExer> resultats=new NSMutableArray<EOPlanComptableExer>();
		
		if (ec==null || planco==null)
			return null;
		
		int length=planco.pcoNum().length();
		if (length==0)
			return null;
		
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.EXERCICE_KEY+"=%@", new NSArray<Object>(planco.toExercice())));
		for (int i=0; i<length; i++)
			mesOrQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueNature.PCO_NUM_KEY+"=%@", 
					new NSArray<Object>(planco.pcoNum().substring(0, i+1))));
		mesQualifiers.addObject(new EOOrQualifier(mesOrQualifiers));
		
		fs = new EOFetchSpecification(EOBudgetMasqueNature.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);
		NSArray masques=ec.objectsWithFetchSpecification(fs);

		if (masques==null || masques.count()==0)
			return null;
				
		resultats.addObjectsFromArray(Finder.tableauTrie((NSArray)masques.valueForKey(EOBudgetMasqueNature.PLAN_COMPTABLE_KEY),
				new NSArray(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NIVEAU_KEY, EOSortOrdering.CompareCaseInsensitiveDescending))));
		
		return (EOPlanComptableExer)resultats.objectAtIndex(0);
	}

	private static NSArray<EOSortOrdering> sort() {
		NSMutableArray<EOSortOrdering> array=new NSMutableArray<EOSortOrdering>();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOPlanComptableExer.PCO_NUM_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}
}
