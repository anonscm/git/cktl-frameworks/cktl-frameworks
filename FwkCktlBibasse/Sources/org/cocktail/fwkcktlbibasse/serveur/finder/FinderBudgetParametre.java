/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2010 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktlbibasse.serveur.finder;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetParametres;
import org.cocktail.fwkcktljefyadmin.common.exception.IllegalArgumentException;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;


public final class FinderBudgetParametre extends Finder {

	/**
	 * Recherche d'un parametre par son code et son exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un EOParametre
	 */
	public static final EOBudgetParametres getParametre(EOEditingContext ed, String cle, EOExercice exercice) {
		NSArray parametres=getParametres(ed, cle, exercice);
		if (parametres==null || parametres.count()==0)
			return null;
		return (EOBudgetParametres)parametres.objectAtIndex(0);
	}

	/**
	 * Recherche la liste des parametres correspondants a un code et un exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param cle
	 *        code du parametre
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        un NSArray de EOParametre
	 */
	public static final NSArray getParametres(EOEditingContext ed, String cle, EOExercice exercice) {
		if (exercice==null)
			throw new IllegalArgumentException("FinderBudgetParametre.getParametres : le parametre exercice est obligatoire.");
		if (cle==null)
			throw new IllegalArgumentException("FinderBudgetParametre.getParametres : le parametre cle est obligatoire.");

		NSArray parametres=Finder.fetchArray(ed,EOBudgetParametres.ENTITY_NAME,
				EOBudgetParametres.BPAR_KEY_KEY+"=%@ and "+EOBudgetParametres.EXERCICE_KEY+"=%@",new NSArray(new Object[] { cle, exercice }),null,false);
		if (parametres==null)
			return new NSArray();
		return parametres;
	}

	/**
	 * Fetch tous les parametres
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 */
	private static NSArray fetchParametres(EOEditingContext ed) {
		// if (arrayParametres==null)
		return Finder.fetchArray(ed,EOBudgetParametres.ENTITY_NAME,null,null,null,false);
	}

	/**
	 * Recherche la valeur du parametre du niveau de saisie du budget suivant l'exercice
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param exercice
	 *        exercice pour lequel on cherche ce parametre
	 * @return
	 *        le orgNiv (Integer)
	 * @throws Exception 
	 */
	public static final Integer getParametreOrganNiveauSaisie(EOEditingContext ed, EOExercice exercice) {
		EOBudgetParametres parametre=getParametre(ed, EOBudgetParametres.NIVEAU_SAISIE, exercice);

		if (parametre==null)
			throw new IllegalArgumentException("Le parametre "+EOBudgetParametres.NIVEAU_SAISIE+" est manquant pour l'exercice "+exercice.exeExercice());

		String niveau=parametre.bparValue();
		if (niveau==null || (!niveau.equals(EOBudgetParametres.NIVEAU_SAISIE_CR) && !niveau.equals(EOBudgetParametres.NIVEAU_SAISIE_UB)))
			throw new IllegalArgumentException("Le parametre "+EOBudgetParametres.NIVEAU_SAISIE+" est incoherent pour l'exercice "+exercice.exeExercice());
		if (niveau.equals(EOBudgetParametres.NIVEAU_SAISIE_CR))
			return EOOrgan.ORG_NIV_3;
		return EOOrgan.ORG_NIV_2;
	}
}
