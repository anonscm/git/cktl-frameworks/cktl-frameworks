/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.finder;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetSaisie;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeSaisie;
import org.cocktail.fwkcktljefyadmin.common.finder.FinderTypeEtat;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetSaisie extends Finder {

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @return
	 */
	public static EOBudgetSaisie findBudgetSaisieForExercice(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie)  {
		NSArray<EOBudgetSaisie> array=findBudgetsSaisieForExercice(ec, exercice, typeSaisie);
		if (array==null || array.count()==0)
			return null;
		return (EOBudgetSaisie)array.objectAtIndex(0);
	}

	/**
	 * 
	 * @param ec
	 * @param exercice
	 * @param typeSaisie
	 * @return
	 */
	public static NSArray<EOBudgetSaisie> findBudgetsSaisieForExercice(EOEditingContext ec, EOExercice exercice, EOTypeSaisie typeSaisie)  {
		try { 
			NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeEtat!=%@", new NSArray<Object>(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));
			mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("exercice=%@", new NSArray<Object>(exercice)));

			if (typeSaisie != null)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat("typeSaisie=%@", new NSArray<Object>(typeSaisie)));

			NSArray<EOSortOrdering> sort = new NSArray<EOSortOrdering>(new EOSortOrdering(EOBudgetSaisie.BDSA_DATE_CREATION_KEY, EOSortOrdering.CompareDescending));
			EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisie.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort);
			fs.setRefreshesRefetchedObjects(true);

			return ec.objectsWithFetchSpecification(fs);
		}
		catch (Exception e )	{
			//e.printStackTrace();
			return new NSArray<EOBudgetSaisie>();
		}
	}


	public static EOBudgetSaisie findBudgetSaisieEnCours(EOEditingContext ec, EOExercice exercice)  {
		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisie.TYPE_ETAT_KEY+"!=%@", new NSArray<Object>(FinderTypeEtat.getTypeEtat(ec, EOTypeEtat.ETAT_ANNULE))));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisie.EXERCICE_KEY+"=%@", new NSArray<Object>(exercice)));
		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetSaisie.BDSA_DATE_VALIDATION_KEY+"=nil", null));

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetSaisie.ENTITY_NAME, new EOAndQualifier(mesQualifiers), null);
		fs.setRefreshesRefetchedObjects(true);

		NSArray resultats=ec.objectsWithFetchSpecification(fs);
		if (resultats==null || resultats.count()==0)
			return null;
		return (EOBudgetSaisie)resultats.objectAtIndex(0);
	}
	
	public static NSArray<EOSortOrdering> sortParDateVoteDescendant() {
    	NSMutableArray<EOSortOrdering> array=new NSMutableArray<EOSortOrdering>();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetSaisie.BDSA_DATE_VALIDATION_KEY, EOSortOrdering.CompareCaseInsensitiveDescending));
    	return array;
    }
}
