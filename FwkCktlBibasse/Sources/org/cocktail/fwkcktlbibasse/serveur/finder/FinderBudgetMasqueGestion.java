/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.fwkcktlbibasse.serveur.finder;

import org.cocktail.fwkcktlbibasse.serveur.metier.EOBudgetMasqueGestion;
import org.cocktail.fwkcktlbibasse.serveur.metier.EOTypeAction;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

public class FinderBudgetMasqueGestion extends Finder {

	
	/**
	 * 
	 * @return
	 */
	public static NSArray findMasqueGestion(EOEditingContext ec, EOExercice exercice) {
		EOQualifier myQualifier=EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.EXERCICE_KEY+"=%@", new NSArray(exercice));
		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueGestion.ENTITY_NAME, myQualifier, null);
		fs.setRefreshesRefetchedObjects(true);
		return ec.objectsWithFetchSpecification(fs);
	}

	public static NSArray findMasqueNatureTrieTypeAction(EOEditingContext ec, EOExercice exercice)	{
		return Finder.tableauTrie(findMasqueGestion(ec, exercice), sort());
	}

	/**
	 * 
	 * @return
	 */
	public static NSArray<EOTypeAction> findTypeAction(EOEditingContext ec, EOExercice exercice, EOTypeCredit typeCredit){
		NSMutableArray<EOQualifier> mesQualifiers = new NSMutableArray<EOQualifier>();

		mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.EXERCICE_KEY+"=%@", new NSArray(exercice)));

		if (typeCredit != null) {
			if (typeCredit.tcdType()==EOTypeCredit.TCD_TYPE_DEPENSE)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.TYPE_ACTION_KEY+"."+EOTypeAction.TYAC_TYPE_KEY+"=%@",
						new NSArray(EOTypeAction.TYAC_TYPE_DEPENSE)));
			if (typeCredit.tcdType()==EOTypeCredit.TCD_TYPE_RECETTE)
				mesQualifiers.addObject(EOQualifier.qualifierWithQualifierFormat(EOBudgetMasqueGestion.TYPE_ACTION_KEY+"."+EOTypeAction.TYAC_TYPE_KEY+"=%@",
						new NSArray(EOTypeAction.TYAC_TYPE_RECETTE)));
		}

		EOFetchSpecification fs = new EOFetchSpecification(EOBudgetMasqueGestion.ENTITY_NAME, new EOAndQualifier(mesQualifiers), sort());
		fs.setRefreshesRefetchedObjects(true);
		NSArray masques=ec.objectsWithFetchSpecification(fs);

		return (NSArray)masques.valueForKey(EOBudgetMasqueGestion.TYPE_ACTION_KEY);
	}

    private static NSArray<EOSortOrdering> sort() {
    	NSMutableArray<EOSortOrdering> array=new NSMutableArray<EOSortOrdering>();
    	array.addObject(EOSortOrdering.sortOrderingWithKey(EOBudgetMasqueGestion.TYPE_ACTION_KEY+"."+EOTypeAction.TYAC_CODE_KEY, 
    			EOSortOrdering.CompareCaseInsensitiveAscending));
    	return array;
    }
}
