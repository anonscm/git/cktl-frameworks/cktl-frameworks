SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

---- Cette table a été créée dans un script publié postérieur à celui-ci. 
--
--CREATE TABLE JEFY_BUDGET.DB_VERSION
--  (
--    DB_VERSION_LIBELLE VARCHAR2(15) NOT NULL,
--    DB_VERSION_DATE DATE NOT NULL,
--    DB_INSTALL_DATE DATE,
--    DB_COMMENT  VARCHAR2(100),
--    DB_VERSION_ID NUMBER(4) NOT NULL
--  );
--  CREATE UNIQUE INDEX JEFY_BUDGET.PK_DB_VERSION ON JEFY_BUDGET.DB_VERSION (DB_VERSION_ID) LOGGING NOPARALLEL;
--ALTER TABLE JEFY_BUDGET.DB_VERSION ADD (CONSTRAINT PK_DB_VERSION PRIMARY KEY (DB_VERSION_ID));
--COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_LIBELLE
--IS
--  'Libelle de la version';
--  COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_DATE
--IS
--  'Date de release de la version';
--  COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_INSTALL_DATE
--IS
--  'Date d''installation de la version. Si non renseigne, la version n''est pas completement installee';
--  COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_COMMENT
--IS
--  'Le commentaire/une courte description de cette version de la base de donnees.';
--  COMMENT ON COLUMN JEFY_BUDGET.DB_VERSION.DB_VERSION_ID
--IS
--  'Clef primaire de JEFY_BUDGET.DB_VERSION';
--  COMMENT ON TABLE JEFY_BUDGET.DB_VERSION
--IS
--  'Historique des versions du schema du user JEFY_BUDGET';
  
-- Maj de  la version 
Insert into JEFY_BUDGET.DB_VERSION (DB_VERSION_ID,DB_VERSION_LIBELLE,DB_VERSION_DATE,DB_INSTALL_DATE,DB_COMMENT) values (
    '1000','1.0.0.0',to_date('19/04/2011','DD/MM/YYYY'),sysdate, 'Debut du versioning de jefy_budget');
commit;