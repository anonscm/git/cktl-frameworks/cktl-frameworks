/* Formatted on 2011/07/19 14:27 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW jefy_budget.v_planco_credit (exe_ordre,
                                                           pcc_ordre,
                                                           tcd_ordre,
                                                           pco_num,
                                                           pla_quoi,
                                                           pcc_etat
                                                          )
AS
   SELECT t.exe_ordre, p.pcc_ordre, p.tcd_ordre, p.pco_num, p.pla_quoi,
          p.pcc_etat
     FROM maracuja.planco_credit p, jefy_admin.type_credit t
    WHERE p.tcd_ordre = t.tcd_ordre;


