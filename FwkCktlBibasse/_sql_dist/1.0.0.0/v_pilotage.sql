create or replace view jefy_budget.v_pilotage (bdsa_id, utl_ordre, ORG_ID, org_ETAB, org_UB, org_CR, ETAT, MONTANT_GES_SAISI, MONTANT_GES, MONTANT_NAT_SAISI, MONTANT_NAT ) as
select g.bdsa_id bdsa_id, u.utl_ordre, o.org_id ORG_ID, org_etab ETAB, org_ub UB, org_cr CR, 
    decode(jefy_budget.budget_moteur.get_etat_saisie(g.bdsa_id , o.org_id),'VALIDE','VERROUILLE',jefy_budget.budget_moteur.get_etat_saisie(g.bdsa_id , o.org_id)) ETAT, 
    nvl(bdsg_saisi, 0) MONTANT_GES_SAISI, nvl(bdsg_montant, 0) MONTANT_GES,
    nvl(bdsn_saisi, 0) MONTANT_NAT_SAISI, nvl(bdsn_montant, 0) MONTANT_NAT 
FROM jefy_admin.organ o, jefy_admin.utilisateur_organ u, 
    (select bdsa_id, org_id, sum(bdsg_saisi) bdsg_saisi, sum(bdsg_montant) bdsg_montant from jefy_budget.budget_saisie_gestion b, jefy_admin.type_credit t 
      where b.tcd_ordre=t.tcd_ordre and t.tcd_type='DEPENSE' group by bdsa_id, org_id) g,
    (select bdsa_id, org_id, sum(bdsn_saisi) bdsn_saisi, sum(bdsn_montant) bdsn_montant from jefy_budget.budget_saisie_nature b, jefy_admin.type_credit t 
      where b.tcd_ordre=t.tcd_ordre and t.tcd_type='DEPENSE' group by bdsa_id, org_id) n
WHERE o.org_id=u.org_id and g.bdsa_id=n.bdsa_id and o.org_id=n.org_id and g.org_id=o.org_id;
/
