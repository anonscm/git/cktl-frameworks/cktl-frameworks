--connect grhum/xxxxxxxx;
SET DEFINE OFF;
whenever sqlerror exit sql.sqlcode ;

grant select, references on maracuja.plan_comptable_exer to jefy_budget;
grant select, references on jefy_admin.type_application to jefy_budget;

-------------------------------------------------------------------

create table jefy_budget.prevision_budg (
    pvbu_id number not null,
    exe_ordre number not null,
    org_id number not null,
    utl_ordre number not null,
    pvbu_date date not null,
    pvbu_commentaire varchar2(500),
    tyet_id number not null,
    tyap_id number not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.prevision_budg is 'table de prevision budgetaire';
COMMENT ON COLUMN jefy_budget.prevision_budg.pvbu_id is 'Cle';
COMMENT ON COLUMN jefy_budget.prevision_budg.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.prevision_budg.org_id is 'Cle etrangere table jefy_admin.organ';
COMMENT ON COLUMN jefy_budget.prevision_budg.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_budget.prevision_budg.pvbu_date is 'Date de la proposition';
COMMENT ON COLUMN jefy_budget.prevision_budg.pvbu_commentaire is 'Commentaire';
COMMENT ON COLUMN jefy_budget.prevision_budg.tyet_id is 'Cle etrangere table jefy_admin.type_etat';
COMMENT ON COLUMN jefy_budget.prevision_budg.tyap_id is 'Cle etrangere table jefy_admin.type_application';

CREATE UNIQUE INDEX PK_prevision_budg ON jefy_budget.prevision_budg (pvbu_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.prevision_budg ADD (CONSTRAINT PK_prevision_budg PRIMARY KEY (pvbu_id));

ALTER TABLE jefy_budget.prevision_budg ADD ( CONSTRAINT FK_prev_bud_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg ADD ( CONSTRAINT FK_prev_bud_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur (utl_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg ADD ( CONSTRAINT FK_prev_bud_org_id FOREIGN KEY (org_id) 
    REFERENCES jefy_admin.organ (org_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg ADD ( CONSTRAINT FK_prev_bud_tyet_id FOREIGN KEY (tyet_id) 
    REFERENCES jefy_admin.type_etat (tyet_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg ADD ( CONSTRAINT FK_prev_bud_tyap_id FOREIGN KEY (tyap_id) 
    REFERENCES jefy_admin.type_application (tyap_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.prevision_budg_seq start with 1 nocycle nocache;

-------------------------------------------------------------------

create table jefy_budget.prevision_budg_nat_lolf (
    pvbnl_id number not null,
    pvbu_id number not null,
    tcd_ordre number not null,
    exe_ordre number not null,
    pco_num varchar2(20) not null,
    tyac_id number not null,
    pvbnl_montant number(18,2) not null,
    pvbnl_a_ouvrir number(18,2) not null,
    pvbnl_ouvert number(18,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.prevision_budg_nat_lolf is 'table de repartition par nature et lolf de la prevision';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pvbnl_id is 'Cle';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pvbu_id is 'Cle etrangere table jefy_budget.prevision_budg';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pco_num is 'Cle etrangere table maracuja.plan_comptable';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.tyac_id is 'Cle etrangere vue v_type_action';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pvbnl_montant is 'montant';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pvbnl_a_ouvrir is 'montant a ouvrir';
COMMENT ON COLUMN jefy_budget.prevision_budg_nat_lolf.pvbnl_ouvert is 'montant reellement ouvert';

CREATE UNIQUE INDEX PK_prevision_budg_nat_lolf ON jefy_budget.prevision_budg_nat_lolf (pvbnl_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.prevision_budg_nat_lolf ADD (CONSTRAINT PK_prevision_budg_nat_lolf PRIMARY KEY (pvbnl_id));

ALTER TABLE jefy_budget.prevision_budg_nat_lolf ADD ( CONSTRAINT FK_prev_bud_natl_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit (tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg_nat_lolf ADD ( CONSTRAINT FK_prev_bud_natl_pvbu_id FOREIGN KEY (pvbu_id) 
    REFERENCES jefy_budget.prevision_budg (pvbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg_nat_lolf ADD ( CONSTRAINT FK_prev_bud_natl_pco_num FOREIGN KEY (pco_num) 
    REFERENCES maracuja.plan_comptable (pco_num) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.prevision_budg_nat_lolf ADD ( CONSTRAINT FK_prev_bud_natl_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.prevision_budg_nat_lolf_seq start with 1 nocycle nocache;

-------------------------------------------------------------------

create table jefy_budget.proposition_budg (
    prbu_id number not null,
    exe_ordre number not null,
    org_id number not null,
    pvbu_id number,
    bdsa_id number,
    utl_ordre number not null,
    prbu_date date not null,
    prbu_commentaire varchar2(500),
    tyet_id number not null,
    tyap_id number not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_budg is 'table de proposition de prise en charge au prochain budget saisi';
COMMENT ON COLUMN jefy_budget.proposition_budg.prbu_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_budg.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.proposition_budg.org_id is 'Cle etrangere table jefy_admin.organ';
COMMENT ON COLUMN jefy_budget.proposition_budg.pvbu_id is 'Cle etrangere table jefy_budget.prevision_budget';
COMMENT ON COLUMN jefy_budget.proposition_budg.bdsa_id is 'Cle etrangere table jefy_budget.budget_saisie';
COMMENT ON COLUMN jefy_budget.proposition_budg.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_budget.proposition_budg.prbu_date is 'Date de la proposition';
COMMENT ON COLUMN jefy_budget.proposition_budg.prbu_commentaire is 'Commentaire';
COMMENT ON COLUMN jefy_budget.proposition_budg.tyet_id is 'Cle etrangere table jefy_admin.type_etat';
COMMENT ON COLUMN jefy_budget.proposition_budg.tyap_id is 'Cle etrangere table jefy_admin.type_application';

CREATE UNIQUE INDEX PK_proposition_budg ON jefy_budget.proposition_budg (prbu_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_budg ADD (CONSTRAINT PK_proposition_budg PRIMARY KEY (prbu_id));

ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur (utl_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_org_id FOREIGN KEY (org_id) 
    REFERENCES jefy_admin.organ (org_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_pvbu_id FOREIGN KEY (pvbu_id) 
    REFERENCES jefy_budget.prevision_budg (pvbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_bdsa_id FOREIGN KEY (bdsa_id) 
    REFERENCES jefy_budget.budget_saisie (bdsa_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_tyet_id FOREIGN KEY (tyet_id) 
    REFERENCES jefy_admin.type_etat (tyet_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg ADD ( CONSTRAINT FK_prop_bud_tyap_id FOREIGN KEY (tyap_id) 
    REFERENCES jefy_admin.type_application (tyap_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_budg_seq start with 1 nocycle nocache;

-------------------------------------------------------------------
/*
create table jefy_budget.proposition_budg_gestion (
    prbg_id number not null,
    prbu_id number not null,
    tcd_ordre number not null,
    exe_ordre number not null,
    tyac_id number not null,
    prbg_montant number(18,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_budg_gestion is 'table de repartition de gestion de la proposition';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.prbg_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.prbu_id is 'Cle etrangere table jefy_budget.proposition_budg';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.tyac_id is 'Cle etrangere vue v_type_action';
COMMENT ON COLUMN jefy_budget.proposition_budg_gestion.prbg_montant is 'montant';

CREATE UNIQUE INDEX PK_proposition_budg_gestion ON jefy_budget.proposition_budg_gestion (prbg_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_budg_gestion ADD (CONSTRAINT PK_proposition_budg_gestion PRIMARY KEY (prbg_id));

ALTER TABLE jefy_budget.proposition_budg_gestion ADD ( CONSTRAINT FK_prop_bud_gest_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit (tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_gestion ADD ( CONSTRAINT FK_prop_bud_gest_prbu_id FOREIGN KEY (prbu_id) 
    REFERENCES jefy_budget.proposition_budg (prbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_gestion ADD ( CONSTRAINT FK_prop_bud_gest_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_budg_gestion_seq start with 1 nocycle nocache;

-------------------------------------------------------------------

create table jefy_budget.proposition_budg_nature (
    prbn_id number not null,
    prbu_id number not null,
    tcd_ordre number not null,
    exe_ordre number not null,
    pco_num varchar2(20) not null,
    prbn_montant number(18,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_budg_nature is 'table de repartition par nature de la proposition';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.prbn_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.prbu_id is 'Cle etrangere table jefy_budget.proposition_budg';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.pco_num is 'Cle etrangere table maracuja.plan_comptable';
COMMENT ON COLUMN jefy_budget.proposition_budg_nature.prbn_montant is 'montant';

CREATE UNIQUE INDEX PK_proposition_budg_nature ON jefy_budget.proposition_budg_nature (prbn_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_budg_nature ADD (CONSTRAINT PK_proposition_budg_nature PRIMARY KEY (prbn_id));

ALTER TABLE jefy_budget.proposition_budg_nature ADD ( CONSTRAINT FK_prop_bud_nat_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit (tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nature ADD ( CONSTRAINT FK_prop_bud_nat_prbu_id FOREIGN KEY (prbu_id) 
    REFERENCES jefy_budget.proposition_budg (prbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nature ADD ( CONSTRAINT FK_prop_bud_nat_pco_num FOREIGN KEY (pco_num) 
    REFERENCES maracuja.plan_comptable (pco_num) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nature ADD ( CONSTRAINT FK_prop_bud_nat_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_budg_nature_seq start with 1 nocycle nocache;
*/
-------------------------------------------------------------------

create table jefy_budget.proposition_budg_nat_lolf (
    prbnl_id number not null,
    prbu_id number not null,
    tcd_ordre number not null,
    exe_ordre number not null,
    pco_num varchar2(20) not null,
    tyac_id number not null,
    prbnl_montant number(18,2) not null,
    pvbnl_id number)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_budg_nat_lolf is 'table de repartition par nature et lolf de la proposition';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.prbnl_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.prbu_id is 'Cle etrangere table jefy_budget.proposition_budg';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.pco_num is 'Cle etrangere table maracuja.plan_comptable';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.tyac_id is 'Cle etrangere vue v_type_action';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.prbnl_montant is 'montant';
COMMENT ON COLUMN jefy_budget.proposition_budg_nat_lolf.pvbnl_id is 'Cle etrangere table jefy_budget.prevision_budg_nat_lolf';

CREATE UNIQUE INDEX PK_proposition_budg_nat_lolf ON jefy_budget.proposition_budg_nat_lolf (prbnl_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD (CONSTRAINT PK_proposition_budg_nat_lolf PRIMARY KEY (prbnl_id));

ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD ( CONSTRAINT FK_prop_bud_natl_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit (tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD ( CONSTRAINT FK_prop_bud_natl_prbu_id FOREIGN KEY (prbu_id) 
    REFERENCES jefy_budget.proposition_budg (prbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD ( CONSTRAINT FK_prop_bud_natl_pco_num FOREIGN KEY (pco_num) 
    REFERENCES maracuja.plan_comptable (pco_num) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD ( CONSTRAINT FK_prop_bud_natl_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_budg_nat_lolf ADD ( CONSTRAINT FK_prop_bud_natl_pvbnl_id FOREIGN KEY (pvbnl_id) 
    REFERENCES jefy_budget.prevision_budg_nat_lolf (pvbnl_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_budg_nat_lolf_seq start with 1 nocycle nocache;


-------------------------------------------------------------------

create table jefy_budget.proposition_posit (
    prpo_id number not null,
    exe_ordre number not null,
    org_id number not null,
    tcd_ordre number not null,
    prpo_montant_previsionnel number(18,2) not null,
    prpo_montant_ouvert number(18,2) not null,  
    utl_ordre number not null,
    prpo_date date not null,
    prpo_commentaire varchar2(500),
    prbu_id number,
    tyet_id number not null,
    tyap_id number not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_posit is 'table de proposition de positionnement de credits';
COMMENT ON COLUMN jefy_budget.proposition_posit.prpo_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_posit.exe_ordre is 'Cle etrangere table jefy_admin.exercice';
COMMENT ON COLUMN jefy_budget.proposition_posit.org_id is 'Cle etrangere table jefy_admin.organ';
COMMENT ON COLUMN jefy_budget.proposition_posit.prpo_montant_previsionnel is 'Montant propose a l''ouverture de credits';
COMMENT ON COLUMN jefy_budget.proposition_posit.prpo_montant_ouvert is 'Montant reellement ouvert';
COMMENT ON COLUMN jefy_budget.proposition_posit.tcd_ordre is 'Cle etrangere table jefy_admin.type_credit';
COMMENT ON COLUMN jefy_budget.proposition_posit.utl_ordre is 'Cle etrangere table jefy_admin.utilisateur';
COMMENT ON COLUMN jefy_budget.proposition_posit.prpo_date is 'Date de la proposition';
COMMENT ON COLUMN jefy_budget.proposition_posit.prpo_commentaire is 'Commentaire';
COMMENT ON COLUMN jefy_budget.proposition_posit.prbu_id is 'Cle etrangere table jefy_budget.proposition_budg';
COMMENT ON COLUMN jefy_budget.proposition_posit.tyet_id is 'Cle etrangere table jefy_admin.type_etat';
COMMENT ON COLUMN jefy_budget.proposition_posit.tyap_id is 'Cle etrangere table jefy_admin.type_application';

CREATE UNIQUE INDEX PK_proposition_posit ON jefy_budget.proposition_posit (prpo_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_posit ADD (CONSTRAINT PK_proposition_posit PRIMARY KEY (prpo_id));

ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_exe_ordre FOREIGN KEY (exe_ordre) 
    REFERENCES jefy_admin.exercice (exe_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_utl_ordre FOREIGN KEY (utl_ordre) 
    REFERENCES jefy_admin.utilisateur (utl_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_org_id FOREIGN KEY (org_id) 
    REFERENCES jefy_admin.organ (org_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_tcd_ordre FOREIGN KEY (tcd_ordre) 
    REFERENCES jefy_admin.type_credit (tcd_ordre) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_prbu_id FOREIGN KEY (prbu_id) 
    REFERENCES jefy_budget.proposition_budg (prbu_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_tyet_id FOREIGN KEY (tyet_id) 
    REFERENCES jefy_admin.type_etat (tyet_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit ADD ( CONSTRAINT FK_prop_pos_tyap_id FOREIGN KEY (tyap_id) 
    REFERENCES jefy_admin.type_application (tyap_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_posit_seq start with 1 nocycle nocache;


-------------------------------------------------------------------

create table jefy_budget.proposition_posit_mouv (
    prpm_id number not null,
    prpo_id number not null,
    bdmc_id number not null,
    prpm_montant number(18,2) not null)
LOGGING NOCACHE NOPARALLEL;

COMMENT ON TABLE  jefy_budget.proposition_posit_mouv is 'table de proposition de positionnement de credits';
COMMENT ON COLUMN jefy_budget.proposition_posit_mouv.prpm_id is 'Cle';
COMMENT ON COLUMN jefy_budget.proposition_posit_mouv.prpo_id is 'Cle etrangere table jefy_budget.proposition_posit';
COMMENT ON COLUMN jefy_budget.proposition_posit_mouv.bdmc_id is 'Cle etrangere table jefy_budget.budget_mouvements_credit';
COMMENT ON COLUMN jefy_budget.proposition_posit_mouv.prpm_montant is 'Montant du mouvement affecte a ce positionnement (tout ou partie du mouvement)';

CREATE UNIQUE INDEX PK_proposition_posit_mouv ON jefy_budget.proposition_posit_mouv (prpm_id) LOGGING NOPARALLEL;
ALTER TABLE jefy_budget.proposition_posit_mouv ADD (CONSTRAINT PK_proposition_posit_mouv PRIMARY KEY (prpm_id));

ALTER TABLE jefy_budget.proposition_posit_mouv ADD ( CONSTRAINT FK_prop_posm_prpo_id FOREIGN KEY (prpo_id) 
    REFERENCES jefy_budget.proposition_posit (prpo_id) DEFERRABLE INITIALLY DEFERRED);
ALTER TABLE jefy_budget.proposition_posit_mouv ADD ( CONSTRAINT FK_prop_posm_bdmc_id FOREIGN KEY (bdmc_id) 
    REFERENCES jefy_budget.budget_mouvements_credit (bdmc_id) DEFERRABLE INITIALLY DEFERRED);

create sequence jefy_budget.proposition_posit_mouv_seq start with 1 nocycle nocache;
  
-- Maj de  la version 
Insert into JEFY_BUDGET.DB_VERSION (DB_VERSION_ID,DB_VERSION_LIBELLE,DB_VERSION_DATE,DB_INSTALL_DATE,DB_COMMENT) values (
    '1001','1.0.0.1',to_date('19/04/2011','DD/MM/YYYY'),sysdate, 'Ajout des tables pour la prévision budgétaire : prevision_budg, prevision_budg_nat_lolf,...');
commit;
