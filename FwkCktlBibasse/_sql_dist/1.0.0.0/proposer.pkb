create or replace package body jefy_budget.proposer is


procedure ins_proposition_budget(
   a_prbu_id       in out integer,
   a_exe_ordre            integer,
   a_org_id               integer,
   a_bdsa_id              integer,
   a_utl_ordre            integer,
   a_prbu_date            date,
   a_prbu_commentaire     varchar2,
   a_tyet_id              integer,
   a_tyap_id              integer)
is
begin
     if a_prbu_id is null then
        select proposition_budg_seq.nextval into a_prbu_id from dual;
     end if;
     
     insert into proposition_budg values(a_prbu_id, a_exe_ordre, a_org_id, a_bdsa_id, a_utl_ordre, a_prbu_date, a_prbu_commentaire, a_tyet_id, a_tyap_id);
end;

procedure ins_proposition_budget_gestion(
   a_prbg_id       in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_tyac_id              integer,
   a_prbg_montant         number)
is
begin
     if a_prbg_id is null then
        select proposition_budg_gestion_seq.nextval into a_prbg_id from dual;
     end if;
     
     insert into proposition_budg_gestion values(a_prbg_id, a_prbu_id, a_tcd_ordre, a_exe_ordre, a_tyac_id, a_prbg_montant);
end;

procedure ins_proposition_budget_nature(
   a_prbn_id       in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_pco_num              varchar2,
   a_prbn_montant         number)
is
begin
     if a_prbn_id is null then
        select proposition_budg_nature_seq.nextval into a_prbn_id from dual;
     end if;
     
     insert into proposition_budg_nature values(a_prbn_id, a_prbu_id, a_tcd_ordre, a_exe_ordre, a_pco_num, a_prbn_montant);
end;

procedure ins_proposition_budget_natlolf(
   a_prbnl_id      in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_pco_num              varchar2,
   a_tyac_id              integer,
   a_prbnl_montant        number)
is
begin
     if a_prbnl_id is null then
        select proposition_budg_nat_lolf_seq.nextval into a_prbnl_id from dual;
     end if;
     
     insert into proposition_budg_nat_lolf values(a_prbnl_id, a_prbu_id, a_tcd_ordre, a_exe_ordre, a_pco_num, a_tyac_id, a_prbnl_montant);
end;

procedure del_proposition_budget(
   a_prbu_id              integer,
   a_utl_ordre            integer)
is
   my_proposition_budget  proposition_budg%rowtype;
begin
     select * into my_proposition_budget from proposition_budg where prbu_id=a_prbu_id;
     if my_proposition_budget.bdsa_id is not null then 
        raise_application_error(-20001, 'cette proposition de budget a deja ete positionne (prbu_id='||a_prbu_id||')');
     end if;
      
     delete from proposition_budg_gestion  where prbu_id=a_prbu_id;
     delete from proposition_budg_nature   where prbu_id=a_prbu_id;
     delete from proposition_budg_nat_lolf where prbu_id=a_prbu_id;
     delete from proposition_budg where prbu_id=a_prbu_id;
end;

procedure ins_proposition_posit(
   a_prpo_id       in out integer,
   a_exe_ordre            integer,
   a_org_id               integer,
   a_tcd_ordre            integer,
   a_prpo_montant_previsionnel number,
   a_prpo_montant_ouvert  number,
   a_utl_ordre            integer,
   a_prpo_date            date,
   a_prpo_commentaire     varchar2,
   a_prbu_id              integer,
   a_tyet_id              integer,
   a_tyap_id              integer)
is
begin
     if a_prpo_id is null then
        select proposition_posit_seq.nextval into a_prpo_id from dual;
     end if;
     
     insert into proposition_posit values(a_prpo_id, a_exe_ordre, a_org_id, a_tcd_ordre, a_prpo_montant_previsionnel, a_prpo_montant_ouvert,
       a_utl_ordre, a_prpo_date, a_prpo_commentaire, a_prbu_id, a_tyet_id, a_tyap_id);
end;

procedure del_proposition_posit(
   a_prpo_id              integer,
   a_utl_ordre            integer)
is
   my_nb integer;
begin   
     select count(*) into my_nb from proposition_posit_mouv where prpo_id=a_prpo_id;
     if my_nb>0 then 
        raise_application_error(-20001, 'cette proposition de positionnement a deja donne lieu a des mouvements (prpo_id='||a_prpo_id||')');
     end if;
     
     delete from proposition_posit where prpo_id=a_prpo_id;
end;

procedure ins_proposition_posit_mouv(
   a_prpm_id       in out integer,
   a_prpo_id              integer,
   a_bdmc_id              integer,
   a_prpm_montant         number)
is
begin
     if a_prpm_id is null then
        select proposition_posit_mouv_seq.nextval into a_prpm_id from dual;
     end if;
     
     insert into proposition_posit_mouv values(a_prpm_id, a_prpo_id, a_bdmc_id, a_prpm_montant);
end;

end;
/
