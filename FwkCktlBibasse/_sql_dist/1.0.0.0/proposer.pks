create or replace package jefy_budget.proposer is


procedure ins_proposition_budget(
   a_prbu_id       in out integer,
   a_exe_ordre            integer,
   a_org_id               integer,
   a_bdsa_id              integer,
   a_utl_ordre            integer,
   a_prbu_date            date,
   a_prbu_commentaire     varchar2,
   a_tyet_id              integer,
   a_tyap_id              integer);
   
procedure ins_proposition_budget_gestion(
   a_prbg_id       in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_tyac_id              integer,
   a_prbg_montant         number);

procedure ins_proposition_budget_nature(
   a_prbn_id       in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_pco_num              varchar2,
   a_prbn_montant         number);

procedure ins_proposition_budget_natlolf(
   a_prbnl_id      in out integer,
   a_prbu_id              integer,
   a_tcd_ordre            integer,
   a_exe_ordre            integer,
   a_pco_num              varchar2,
   a_tyac_id              integer,
   a_prbnl_montant        number);

procedure del_proposition_budget(
   a_prbu_id              integer,
   a_utl_ordre            integer);

procedure ins_proposition_posit(
   a_prpo_id       in out integer,
   a_exe_ordre            integer,
   a_org_id               integer,
   a_tcd_ordre            integer,
   a_prpo_montant_previsionnel number,
   a_prpo_montant_ouvert  number,
   a_utl_ordre            integer,
   a_prpo_date            date,
   a_prpo_commentaire     varchar2,
   a_prbu_id              integer,
   a_tyet_id              integer,
   a_tyap_id              integer);

procedure del_proposition_posit(
   a_prpo_id              integer,
   a_utl_ordre            integer);

procedure ins_proposition_posit_mouv(
   a_prpm_id       in out integer,
   a_prpo_id              integer,
   a_bdmc_id              integer,
   a_prpm_montant         number);

end;
/
