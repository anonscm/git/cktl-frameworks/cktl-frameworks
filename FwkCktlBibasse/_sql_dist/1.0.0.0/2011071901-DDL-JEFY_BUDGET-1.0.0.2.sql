--connect grhum/xxxxxxxx;
SET DEFINE OFF;

DROP VIEW JEFY_BUDGET.V_PLANCO_CREDIT;

/* Formatted on 2011/07/19 14:27 (Formatter Plus v4.8.8) */
CREATE OR REPLACE FORCE VIEW jefy_budget.v_planco_credit (exe_ordre,
                                                           pcc_ordre,
                                                           tcd_ordre,
                                                           pco_num,
                                                           pla_quoi,
                                                           pcc_etat
                                                          )
AS
   SELECT t.exe_ordre, p.pcc_ordre, p.tcd_ordre, p.pco_num, p.pla_quoi,
          p.pcc_etat
     FROM maracuja.planco_credit p, jefy_admin.type_credit t
    WHERE p.tcd_ordre = t.tcd_ordre;



-- Maj de  la version 
Insert into JEFY_BUDGET.DB_VERSION (DB_VERSION_ID,DB_VERSION_LIBELLE,DB_VERSION_DATE,DB_INSTALL_DATE,DB_COMMENT) values (
    '1002','1.0.0.2',to_date('19/07/2011','DD/MM/YYYY'),sysdate, 'Ajout de la vue jefy_budget.v_planco_credit');
commit;