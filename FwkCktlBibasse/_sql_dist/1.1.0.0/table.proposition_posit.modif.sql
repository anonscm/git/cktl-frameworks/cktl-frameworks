alter table jefy_budget.proposition_posit add (prbnl_id number);

alter table jefy_budget.proposition_posit add (constraint fk_prop_pos_prbnl_id foreign key (prbnl_id) 
    references jefy_budget.proposition_budg_nat_lolf (prbnl_id) deferrable initially deferred);

comment on column jefy_budget.proposition_posit.prbu_id is 'Cle etrangere table jefy_budget.proposition_budg (ne sert pas pour le moment)';
comment on column jefy_budget.proposition_posit.prbnl_id is 'Cle etrangere table jefy_budget.proposition_budg_nat_lolf';




alter table jefy_budget.proposition_posit_mouv modify(bdmc_id null);
alter table jefy_budget.proposition_posit_mouv add (bmou_id number);

alter table jefy_budget.proposition_posit_mouv add (constraint fk_prop_pos_mou_bmou_id foreign key (bmou_id) 
    references jefy_budget.budget_mouvements (bmou_id) deferrable initially deferred);

comment on column jefy_budget.proposition_posit_mouv.bdmc_id is 'Cle etrangere table jefy_budget.budget_mouvements_credit (ne sert pas pour le moment)';
comment on column jefy_budget.proposition_posit_mouv.bmou_id is 'Cle etrangere table jefy_budget.budget_mouvements';

