package org.cocktail.fwkcktlreportingextended.serveur;

import org.cocktail.fwkcktlreportingguiajax.serveur.CktlAbstractReporterAjaxProgress;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;

/**
 * Implementation de la fenêtre de progression des impressions
 */
public class CktlReporterAjaxProgress extends CktlAbstractReporterAjaxProgress implements IJrxmlReportListener {

	/**
	 * @param maximum maximum de la barre de progression
	 */
	public CktlReporterAjaxProgress(int maximum) {
		super(maximum);
	}

}
