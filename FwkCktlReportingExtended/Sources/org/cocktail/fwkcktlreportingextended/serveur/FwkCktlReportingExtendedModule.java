package org.cocktail.fwkcktlreportingextended.serveur;

import org.cocktail.fwkcktlreportingextended.serveur.services.impression.ReportImpressionServiceFactory;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.google.inject.assistedinject.FactoryModuleBuilder;
import com.webobjects.eocontrol.EOEditingContext;
import com.woinject.WOSessionScoped;

import er.extensions.eof.ERXEC;

/**
 * Cette classe permet de lier les classes injectees
 */
public class FwkCktlReportingExtendedModule extends AbstractModule {
	
	/** {@inheritDoc} */
	@Override
	protected void configure() {
//		 bind(Validateur.class).to(getImplementationClassFor(Validateur.class));
//		 bind(InscriptionAdministrativeService.class).to(InscriptionAdministrativeServiceImpl.class);
//		 bind(InscriptionPedagogiqueService.class).to(InscriptionPedagogiqueServiceImpl.class);
//		 bind(CreditableService.class).to(CreditableServiceImpl.class);
//		 bind(ComposantRepository.class).to(ComposantRepositoryEOF.class);
//		 bind(PeriodeInscriptionRepository.class).to(PeriodeInscriptionRepositoryEOF.class);
//		 bind(PeriodeInscriptionService.class).to(PeriodeInscriptionServiceImpl.class);
		 
		 install(new FactoryModuleBuilder().build(ReportImpressionServiceFactory.class));
	}


	
	private <T> Class<? extends T> getImplementationClassFor(Class<T> baseClass) {
		try {
            String implementationClassName = System.getProperty(baseClass.getSimpleName());
            @SuppressWarnings("unchecked")
            Class<? extends T> clazz = (Class<? extends T>) Class.forName(implementationClassName);
            return clazz;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("Impossible de trouver une classe implementant " + baseClass.getName(), e);
        }
	}
	
}
