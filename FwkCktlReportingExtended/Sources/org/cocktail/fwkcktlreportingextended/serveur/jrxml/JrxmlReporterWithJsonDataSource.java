package org.cocktail.fwkcktlreportingextended.serveur.jrxml;

import java.io.File;
import java.io.InputStream;
import java.util.Map;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.data.JsonDataSource;

import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
 
/**
 * 
 * Cette classe de reporter prend une dataSource json 
 * pour remplir le  jasper
 * Cette classe requiert Jasper en version 5. Il faudra à terme la déplacer dans 
 * le FwkCktlReporting
 *
 */
public class JrxmlReporterWithJsonDataSource extends JrxmlReporter {


	/**
	 * 
	 * @param label libellé de la tache d'impression
	 * @param jsonDataSource la source de données au format JSON
	 * @param completeFilePath le chemin absolu du du fichier jasper servant pour la génération
	 * @param params les paramètres à passer au jasper
	 * @param printFormat le format d'impression (example PDF)
	 * @param listener le listener qui est utilisé pour connaitre l'avancement de l'impression
	 * @return le  thread dans lequel se déroule l'impression
	 * @throws Exception en cas de problème 
	 */
	public CktlJrxmlReportingTaskThread printWithThread(String label, InputStream jsonDataSource, final String completeFilePath,
	    final Map<String, Object> params, final int printFormat, IJrxmlReportListener listener)
	    throws Exception {
		try {
			
			defaultReportListener = new DefaultJrxmlReportListener();
			setCurrentReportingTaskThread(null);
			setLastPrintError(null);
			addListener(defaultReportListener);
			
			if (listener != null) {
				addListener(listener);
			}
			
			setStarted(true);
			initSrcFile(completeFilePath, false);

			// Recupérer le chemin du repertoire du report
			final File f = new File(completeFilePath);
			
			String rep = f.getParent();
			
			if (rep != null) {
				if (!rep.endsWith(File.separator)) {
					rep = rep + File.separator;
				}
			}
			
			
			params.put("REP_BASE_PATH", rep);
			params.put("SUBREPORT_DIR", rep);

			JsonDataSource dataSource = new JsonDataSource(jsonDataSource);

			if (listener != null) {
				listener.afterDataSourceCreated();
			}
			
			setCurrentReportingTaskThread(new CktlJrxmlWithJsonReportingTaskThread(label, dataSource, completeFilePath, params, printFormat, listener));

			getCurrentReportingTaskThread().setDaemon(true);
			getCurrentReportingTaskThread().setPriority(Thread.currentThread().getPriority() - 1);

			// On lance la génération du report et on s'en va...
			getCurrentReportingTaskThread().start();
			return getCurrentReportingTaskThread();

		} catch (Throwable e) {
			e.printStackTrace();
			throw new Exception(e.getMessage());
		}
	}

	/**
	 * {@inheritDoc}
	 */
	public class CktlJrxmlWithJsonReportingTaskThread extends CktlJrxmlReportingTaskThread {
		
		private JsonDataSource dataSource;

		/**
		 * 
		 * @param label libellé de la tache d'impression
		 * @param dataSource la source de données au format JSON
		 * @param jasperFileName le chemin absolu du du fichier jasper servant pour la génération
		 * @param parameters les paramètres à passer au jasper
		 * @param printFormat le format d'impression (example PDF)
		 * @param listener le listener qui est utilisé pour connaitre l'avancement de l'impression
		 */
		public CktlJrxmlWithJsonReportingTaskThread(String label, JsonDataSource dataSource, String jasperFileName, Map<String, Object> parameters,
		    int printFormat, IJrxmlReportListener listener) {
			super(label, null, null, jasperFileName, parameters, printFormat, true, Boolean.FALSE, listener);
			this.dataSource = dataSource;
		}

		protected JasperPrint fillReport() throws JRException {
			JasperPrint _printResult = JasperFillManager.fillReport(jasperReport, getParams(), dataSource);
			return _printResult;
		}

	}
}
