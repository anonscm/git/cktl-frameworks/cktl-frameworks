package org.cocktail.fwkcktlreportingextended.serveur;

import org.apache.log4j.Logger;
import org.cocktail.fwkcktlwebapp.server.CktlWebApplication;
import org.cocktail.fwkcktlwebapp.server.ModuleRegister;

import er.extensions.ERXFrameworkPrincipal;

/**
 * Classe principale de framework reporting extended
 */
public class FwkCktlReportingExtended extends ERXFrameworkPrincipal {
	
	public static final Logger LOG = Logger.getLogger(FwkCktlReportingExtended.class);	
	
	static {
		setUpFrameworkPrincipalClass(FwkCktlReportingExtended.class);
	}
	
	@Override
	public void finishInitialization() {
		ModuleRegister moduleRegister = CktlWebApplication.application().getModuleRegister();
		moduleRegister.addModule(new FwkCktlReportingExtendedModule());
	}
	
}
