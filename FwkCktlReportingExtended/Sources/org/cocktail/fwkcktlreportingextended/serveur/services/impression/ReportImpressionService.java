package org.cocktail.fwkcktlreportingextended.serveur.services.impression;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Connection;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.cocktail.fwkcktlreportingextended.serveur.jrxml.JrxmlReporterWithJsonDataSource;
import org.cocktail.reporting.server.CktlAbstractReporter;
import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;
import org.cocktail.reporting.server.jrxml.JrxmlReporter;
import org.cocktail.reporting.server.jrxml.JrxmlReporterWithBeanCollectionDataSource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;

/**
 * Classe de service implémentant l'impression de données stockées à l'intérieur d'un POJO
 */
public class ReportImpressionService {
	
	private URL jasperLocation;
	private String label;
	private IJrxmlReportListener listener;

	/**
	 * 
	 * @param jasperLocation l'url contenant l'emplacement du fichier jasper
	 * @param label le label décrivant la tache d'impression
	 * @param listener le listener qui permet de stocker la progression de l'impression
	 */
	@Inject
	public ReportImpressionService(
		@Assisted URL jasperLocation, 
		@Assisted String label, 
		@Assisted IJrxmlReportListener listener
	) {
		this.jasperLocation = jasperLocation;
		this.label = label;
		this.listener = listener;
	}
	
	/**
	 * 
	 * @param data le pojo contenant les données
	 * @return le reporter d'impression
	 * @throws Exception en cas d'erreur lors de l'impression
	 */
	public CktlAbstractReporter imprimer(ReportImpressionData data) throws Exception {

		JrxmlReporterWithJsonDataSource reporter = new JrxmlReporterWithJsonDataSource();
		InputStream is = getInputStreamFromData(data);
		
//		// Sauvegarder le flux json
//		sauvegarderFluxJson(is);
		
		reporter.printWithThread(label, is, jasperLocation.getFile(), new HashMap<String, Object>(), CktlAbstractReporter.EXPORT_FORMAT_PDF, listener);
		return reporter;

	}

	
	/**
	 * 
	 * @param data le pojo contenant les données
	 * @return le reporter d'impression
	 * @throws Exception en cas d'erreur lors de l'impression
	 */
	public CktlAbstractReporter imprimer(List<ReportImpressionData> data) throws Exception {

		JrxmlReporterWithJsonDataSource reporter = new JrxmlReporterWithJsonDataSource();
		InputStream is = getInputStreamFromData(data);
		
//		// Sauvegarder le flux json
//		sauvegarderFluxJson(is);
		
		reporter.printWithThread(label, is, jasperLocation.getFile(), new HashMap<String, Object>(), CktlAbstractReporter.EXPORT_FORMAT_PDF, listener);
		return reporter;

	}

	public CktlAbstractReporter imprimerAvecBeans(ReportImpressionData data) throws Exception {
		JrxmlReporterWithBeanCollectionDataSource reporter  = new JrxmlReporterWithBeanCollectionDataSource();
		List<Object> collection = new ArrayList<Object>();
		collection.add(data);
		reporter.printWithThread(label, collection, jasperLocation.getFile(), new HashMap<String, Object>(), CktlAbstractReporter.EXPORT_FORMAT_PDF, listener);
		return reporter;
	}

	/**
	 * Impression d'un jasper avec du "SQL dedans".
	 * @param connection une connexion à la base de données
	 * @param parameters les paramètres à passer au jasper
	 * @return le reporter d'impression
	 * @throws Exception en cas d'erreur lors de l'impression
	 */
	public CktlAbstractReporter imprimer(Connection connection, Map<String, Object> parameters) throws Exception {
		 JrxmlReporter reporter = new JrxmlReporter();

		 try {
			reporter.printWithThread(label, connection, null, jasperLocation.getFile(), parameters, CktlAbstractReporter.EXPORT_FORMAT_PDF, true, false, listener);
		} catch (Throwable e) {
			throw new Exception(e);
		}

		return reporter;
	}
	
	private InputStream getInputStreamFromData(ReportImpressionData data) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE));
		String result = mapper.writeValueAsString(data);
		return new ByteArrayInputStream(result.getBytes("UTF-8"));
	}

	
	private InputStream getInputStreamFromData(List<ReportImpressionData> data) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		mapper.setDateFormat(DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.FRANCE));
		String result = mapper.writeValueAsString(data);
		return new ByteArrayInputStream(result.getBytes("UTF-8"));
	}

	/**
	 * Pour le dév., permet de sauvegarder le flux json dans un fichier /tmp
	 * @param is l'input stream des données à sauvegarder
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private void sauvegarderFluxJson(InputStream is) throws IOException, FileNotFoundException {
		byte[] b = new byte[is.available()];
		is.read(b);
		is.reset();
		String[] fileName = jasperLocation.getFile().split("/");
		File tempFile = File.createTempFile("flux" + fileName[fileName.length - 1], ".json");
		FileOutputStream fileOutputStream = new FileOutputStream(tempFile);
		fileOutputStream.write(b);
		fileOutputStream.close();
	}
	
}
