package org.cocktail.fwkcktlreportingextended.serveur.services.impression;

import java.net.URL;

import org.cocktail.reporting.server.jrxml.IJrxmlReportListener;

/**
 * Permet de créer des ReportImpressionService
 */
public interface ReportImpressionServiceFactory {

	/**
	 * @param jasperLocation l'url contenant l'emplacement du fichier jasper
	 * @param label le label décrivant la tache d'impression
	 * @param listener le listener qui permet de stocker la progression de l'impression
	 * @return un service d'impression
	 */
	ReportImpressionService create(URL jasperLocation, String label, IJrxmlReportListener listener);
	
}
