CREATE OR REPLACE VIEW JEFY_ADMIN.v_organ_exercice (EXE_ORDRE, ORG_ID)
AS 
SELECT exe_ordre, ORG_ID
FROM JEFY_ADMIN.ORGAN, jefy_admin.EXERCICE e
WHERE 
TO_DATE('15/11/'||e.exe_exercice,'dd/mm/yyyy')>ORG_DATE_OUVERTURE AND
(TO_DATE('15/11/'||e.exe_exercice,'dd/mm/yyyy')<ORG_DATE_CLOTURE OR ORG_DATE_CLOTURE IS NULL);

