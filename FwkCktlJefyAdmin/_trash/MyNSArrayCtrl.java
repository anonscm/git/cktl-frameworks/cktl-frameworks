/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.fwkcktljefyadmin.common.utils;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;

/**
 *
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class MyNSNSArrayCtrl {
	  /**
     * Effectue une intersection entre plusieurs NSArray.
     * @param lesTableaux
     * @return L'intersection des tableaux.
     */
    public static final NSArray intersectionOfNSArray(ArrayList lesTableaux) {
        int nbTabs = lesTableaux.size();
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux.get(0);
        if (nbTabs==1) {
            return tab1;
        }

        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < tab1.count(); i++) {
//            for (int i = 1; i < tab1.count(); i++) {
            boolean cond=true;
            for (Iterator iter = lesTableaux.iterator(); iter.hasNext() && cond;) {
                cond = cond && ((NSArray)iter.next()).containsObject(tab1.objectAtIndex(i));
            }
            if (cond) {
                res.addObject(tab1.objectAtIndex(i));
            }
        }
        return res.immutableClone();
    }

    
    /**
     * Renvoi le complément de ensembleReference dans ensemble. 
     * (cad Tous les éléments de ensemble qui ne font pas partie de 
     * l'intersection entre ensembleReference et ensemble)
     * @param ensembleReference
     * @param ensembleTeste
     * @return
     */
    public static final NSArray complementOfNSArray(final NSArray ensembleReference, final NSArray ensemble) {
        final ArrayList tabs = new ArrayList(2);
        tabs.add(ensembleReference);
        tabs.add(ensemble);
        final NSArray intersect = intersectionOfNSArray(tabs);
        final NSMutableArray res = new NSMutableArray();
        final Enumeration iterator = ensemble.objectEnumerator();
        while (iterator.hasMoreElements()) {
            final Object element = (Object) iterator.nextElement();
            if (intersect.indexOfObject(element)==NSArray.NotFound) {
                res.addObject(element);
            }
        }
        return res;
    }
    
    
    /**
     * Renvoi un tableau contenant les objets de ensemble1 qui ne sont pas dans ensemble2 
     * et les objets de enemble2 qui ne sont pas dans ensemble1. (l'union du complément de 1 dans 2 
     * et du complément de 1 dans 2).  
     * @param ensemble1
     * @param ensemble2
     * @return
     */
    public static final NSArray extOfNSArrays(final NSArray ensemble1, final NSArray ensemble2, boolean distinct) {
        final NSArray fermeture1 = complementOfNSArray(ensemble1, ensemble2);
        final NSArray fermeture2 = complementOfNSArray(ensemble2, ensemble1);
        final NSArray res = unionOfNSArrays(new NSArray[]{fermeture1, fermeture2});
        if (distinct) {
            return getDistinctsOfNSArray(res);
        }
        return res;
    }
    
    
    /**
     * Renvoie l'union de tous les tableaux (sans effectuer de distinct)
     * @param lesTableaux
     * @return
     */
    public static final NSArray unionOfNSArrays(final ArrayList lesTableaux) {
        int nbTabs = lesTableaux.size();
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux.get(0);
        if (nbTabs==1) {
            return tab1;
        }

        final NSMutableArray res = new NSMutableArray();
            for (Iterator iter = lesTableaux.iterator(); iter.hasNext();) {
                res.addObjectsFromArray(((NSArray)iter.next()));
            }
        return res.immutableClone();        
    }
    
    public static final NSArray unionOfNSArrays(NSArray[] lesTableaux) {
        int nbTabs = lesTableaux.length;
        if (nbTabs==0) {
            return new NSArray();
        }
        final NSArray tab1 = (NSArray) lesTableaux[0];
        if (nbTabs==1) {
            return tab1;
        }
        
        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < lesTableaux.length; i++) {
            res.addObjectsFromArray(lesTableaux[i]);
        }
        return res.immutableClone();        
    }
    


    /**
     * Supprime les doublons dans un NSMutableArray, les objets conservés sont ceux avec le plus petit index.
     * @param array
     */
    public static final void removeDuplicatesInNSArray(final NSMutableArray array) {
        int i=array.count()-1;        
        while (i>=0) {
            final Object obj = array.objectAtIndex(i);
            int found = array.indexOfObject(obj); 
            if (found != NSArray.NotFound && found != i) {
                array.removeObjectAtIndex(i);
            }
            i--;
        }
    }
    
    
    /**
     * @param array
     * @return un nouveau NSArray contenant les éléments de array sans les doublons.
     */
    public static final NSArray getDistinctsOfNSArray(final NSArray array) {
        final NSMutableArray res = new NSMutableArray();
        for (int i = 0; i < array.count(); i++) {
            final Object element = (Object) array.objectAtIndex(i);
            if (res.indexOfObject(element)==NSArray.NotFound) {
                res.addObject(element);
            }
        }
        return res.immutableClone();
    }
        
}
