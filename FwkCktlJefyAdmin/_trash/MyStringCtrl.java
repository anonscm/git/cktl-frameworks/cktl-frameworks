/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/

package org.cocktail.fwkcktljefyadmin.common.utils;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableDictionary;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

/**
 * 
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class MyStringCtrl extends StringCtrl {
    private static final String CONST_DELIM = "'- ()\t\n\r\f";
    private static final String CONST_VIDE = "";
    private static final String CONST_NULL = "null";

	private static final Hashtable myDicoAccents = (new NSMutableDictionary(new String[] { "A", "A", "A", "A", "A", "C", "E", "E", "E", "E", "I", "I", "I", "I", "N", "O", "O", "O", "O", "O", "U", "U", "U", "U", "Y", "a", "a", "a", "a", "a", "c", "e", "e", "e", "e", "i", "i", "i", "i", "n", "o",
			"o", "o", "o", "o", "u", "u", "u", "u", "y" }, new String[] { "\300", "\302", "\303", "\304", "\305", "\307", "\310", "\311", "\312", "\313", "\314", "\315", "\316", "\317", "\321", "\322", "\323", "\324", "\325", "\326", "\331", "\332", "\333", "\334", "\335", "\340", "\342", "\343",
			"\344", "\345", "\347", "\350", "\351", "\352", "\353", "\354", "\355", "\356", "\357", "\361", "\362", "\363", "\364", "\365", "\366", "\371", "\372", "\373", "\374", "\375" })).hashtable();
	public static final String[] IGNOREWORDSFORCAPITALIZE = {"au","aux","ce","ces","de","des","du","en","et","la","le","les","ne","nos","on","or","ou","où","par","pas","pour","puis","qq.","qqch.","qqn","que","qui","quoi","sa","sauf","se","ses","si","sur","te","tu","un","une","vs","ça","çà"};
	
	/**
	 * @param s
	 * @return true si s est une adresse email construite correctement.
	 */
	public static boolean isEmailValid(String s) {
//		Pattern p = Pattern.compile(".+@.+\\.[a-z]+");
		Pattern p = Pattern.compile("^[a-z0-9._-]+@[a-z0-9.-]{2,}[.][a-z]{2,3}$");
		Matcher m = p.matcher(s);
		return (m.matches());
	}

	public static boolean isEmpty(String s) {
		if (NSKeyValueCoding.NullValue.equals(s)) {
			return true;
		}
		return StringCtrl.isEmpty(s);
	}
	
	/**
	 * 
	 * @param s
	 * @param replacement
	 * @return Si s est vide (isEmpty), alors replacement est renvoyé sinon c'est s.
	 */
	public static String ifEmpty(String s, String replacement) {
		if (isEmpty(s)) {
			return replacement;
		}
		return s;
	}

	/**
	 * Supprime les caracteres accentues d'une chaine en les remplacant par la
	 * chaine specifiee en parametre.
	 * 
	 * @param chaine
	 * @param remplacement
	 * @return
	 */
	public static String chaineSansAccents(String chaine, String remplacement) {
		String res = chaine;
		for (Enumeration enumAccents = myDicoAccents.keys(); enumAccents.hasMoreElements();) {
			String key = (String) enumAccents.nextElement();
			res = replace(res, key, remplacement);
		}
		return res;
	}

	/**
	 * 
	 * @param s
	 * @return true si la chaine est nulle ou composée de chiffres uniquement.
	 */
	public static boolean isDigits(String s) {
		if (s == null) {
			return true;
		}
		for (int i = 0; i < s.length(); i++) {
			if (!MyStringCtrl.isBasicDigit(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 
	 * @param s
	 * @return true si la chaine est nulle ou composée de chiffres uniquement.
	 */
	public static boolean hasOnlyDigits(String s) {
		if (s == null) {
			return true;
		}
		for (int i = 0; i < s.length(); i++) {
			if (!MyStringCtrl.isBasicDigit(s.charAt(i))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Renvoie une chaine avec l'ordre des mots inversés (les mots sont consideres etre
	 * separes par des espaces).
	 * 
	 * @param s
	 * @return La chaine inversés
	 */
	public static String inverseMots(String s) {
		String[] noms = s.split(" ");
		String nomsInv = "";
		for (int i = noms.length - 1; i >= 0; i--) {
			nomsInv = nomsInv + " " + noms[i];
		}
		nomsInv = nomsInv.trim();
		return nomsInv;
	}

	/** Remplace la chaine1 par la chaine2 dans chaine */
	public static String replace(String chaine, String chaine1, String chaine2) {
		return (NSArray.componentsSeparatedByString(chaine, chaine1)).componentsJoinedByString(chaine2);
	}

	/**
	 * 
	 * @param chaine
	 * @return Une chaine de caractere nettoyee des caracteres accentues (qui
	 *         sont remplaces par des ?), adaptee a la recherche dans la base.
	 * 
	 */
	public static String prepareStringForSqlSrch(String chaine) {
		if (chaine == null || chaine.length() < 1)
			return chaine;
		try {
			//
			// MP Forcer encodage
			// 04/04/2007
			//

			// l encodage doit etre coherent avec celui de ce fichier !
			//

			// String retour = new
			// String(chaine.toLowerCase().getBytes(),"UTF-8");
			String retour = chaine.toLowerCase();

			retour = replace(retour, "è", "?");
			retour = replace(retour, "é", "?");
			retour = replace(retour, "ê", "?");
			retour = replace(retour, "ë", "?");

			retour = replace(retour, "à", "?");
			retour = replace(retour, "â", "?");

			retour = replace(retour, "î", "?");
			retour = replace(retour, "Î", "?");

			retour = replace(retour, "ö", "?");
			retour = replace(retour, "ô", "?");
			retour = replace(retour, "Ô", "?");

			retour = replace(retour, "Û", "?");
			retour = replace(retour, "û", "?");
			retour = replace(retour, "'", "*");
			retour = replace(retour, "’", "*");
			retour = replace(retour, " ", "*");
			retour = replace(retour, "-", "?");

			// return retour.toUpperCase();
			return retour;
		} catch (Exception e) {
			System.out.println("Chaine invalide pour la recherche");
			return null;
		}

	}

	/**
	 * @param s
	 * @param acceptChars
	 * @return La chaine s nettoyee de tous les caracteres non specifies dans acceptChars.
	 * 
	 * @see #toBasicString(String)
	 * @see #toBasicString(String, String, char) 
	 */
	public static String toBasicString(String s, String acceptChars) {
		StringBuffer newStr;

		if ((s == null) || (s.length() == 0))
			return s;
		newStr = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			if (isAcceptChar(s.charAt(i), acceptChars))
				newStr.append(s.charAt(i));
		}
		return newStr.toString();
	}

	/**
	 * @param s
	 * @return une chaine de charactère sans les charactères non numeriques, ou
	 *         une chaine vide si s=null.
	 */
	public static String keepDigits(String s) {
		StringBuffer sb = new StringBuffer();
		s = MyStringCtrl.normalize(s);
		for (int i = 0; i < s.length(); i++) {
			if (MyStringCtrl.isBasicDigit(s.charAt(i))) {
				sb.append(s.charAt(i));
			}
		}
		return sb.toString();
	}

	/**
	 * @param s
	 * @return La chaine s avec sa première en majuscule. Les autres lettres ne
	 *         sont pas modifiiées contrairement à la méthode
	 *         {@link StringCtrl#capitalize(String)}.
	 */
	public static String initCap(String s) {
		if (isEmpty(s)) {
			return emptyString();
		}
		String debut = (s.substring(0, 1)).toUpperCase();
		if (s.length() == 1)
			return debut;
		String fin = (s.substring(1, s.length()));
		return debut.concat(fin);
	}

	/**
	 * Renvoie une chaine avec tous les mots ayant leur première lettre en majuscule.<br>
	 * Par exemple : <hr>
	 * La chaine "ACHATS D'ETUDES ET PRESTATIONS DE SERVICES" est transformée en
	 * "Achats d'Etudes et Prestations de Services"
	 * <hr>
	 * Les mots sont récupérés en se basant sur les séparateurs "' ()\t\n\r\f".<br>
	 * Les mots d'un caractère sont mis en minuscule.<br>
	 * Les mots qui se trouvent dans le tableau IGNOREWORDSFORCAPITALIZE sont également mis en minuscules.<br> 
	 * 
	 * @param aString Chaine à transformer.
	 * 
	 */
	public static String capitalizedWords(String aString, String delimiters) {
		if (aString == null || aString.length()==0)   return aString;
		boolean ignoreWord;
		String delim = (delimiters == null ? CONST_DELIM : delimiters);
		StringTokenizer stok = new StringTokenizer(aString, delim, true);
		String words[] = new String[stok.countTokens()];
		//récupérer les mots
		for(int i = 0; i < words.length; i++) {
		   words[i] = stok.nextToken();
		}
		//On met une majuscule en début de chaque mot
		for(int i = 0; i < words.length; i++) {
			//on ignore les caractères de délimitation
			if ( delim.indexOf(words[i]) < 0  ) {
				//On passe le mot en minuscule
				words[i] = words[i].toLowerCase();
				//si le mot est dans la liste des mots à ignorer, ben on l'ignore -;)
				//Pareil si on a une seule lettre
				ignoreWord=false;
				if (words[i].length()==1) {
					ignoreWord = true;	 
				}
				else {
					for (int j = 0; j < IGNOREWORDSFORCAPITALIZE.length; j++) {
						if (IGNOREWORDSFORCAPITALIZE[j].equals( words[i] )) {
							ignoreWord=true;
						}
					}
				}
				if ( !ignoreWord  ) {
					words[i] = words[i].substring(0,1).toUpperCase() + words[i].substring(1,words[i].length());	
				}
			}
		}
		
		StringBuffer buf = new StringBuffer();
		for(int i = 0; i < words.length; i++) {
			buf.append(words[i]);
		}
		
		return buf.toString();
	}
	
//	/**
//	 * Renvoie les max premiers caractères d'une chaine. Si suffix n'est pas vide, il est rajouté à la fin de la chaine (sans depasser max). Utile pour afficher par exemple le debut de la chaine avec "..." à la fin si la longueur de la chaine dépasse max.
//	 * 
//	 * @param libelleForGroupe
//	 * @param max
//	 * @param suffix
//	 * @return
//	 */
//	public static String compactString(String libelleForGroupe, int max, String suffix) {
//		if (libelleForGroupe == null) {
//			return null;
//		}
//		libelleForGroupe = libelleForGroupe.trim();
//		if (libelleForGroupe.length() <= max) {
//			return libelleForGroupe;
//		}
//		if (suffix == null || suffix.length()==0) {
//			return libelleForGroupe.substring(0, max);
//		}
//		
//		
//		return libelleForGroupe.substring(0, max-suffix.length()).concat(suffix);
//	}

}
