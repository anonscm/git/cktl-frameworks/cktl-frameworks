/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.finder;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOEnterpriseObject;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;

public abstract class Finder {
	public static final String MODEL_NAME = "FwkCktlJefyAdmin";

	public static NSArray tableauTrie(NSArray donnees, NSArray sort) {
		return EOSortOrdering.sortedArrayUsingKeyOrderArray(donnees, sort);
	}

	public static NSArray fetchArray(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static EOEnterpriseObject fetchObject(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects) {
		NSArray res = fetchArray(ec, entityName, conditionStr, params, sortOrderings, refreshObjects);
		if ((res == null) || (res.count() == 0))
			return null;
		return (EOEnterpriseObject) res.objectAtIndex(0);
	}

	public static NSArray fetchArrayWithPrefetching(EOEditingContext ec, String entityName, String conditionStr, NSArray params, NSArray sortOrderings, boolean refreshObjects, NSArray relationsToPrefetch) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(conditionStr, params);
		EOFetchSpecification spec = new EOFetchSpecification(entityName, qual, sortOrderings, true, true, null);
		spec.setPrefetchingRelationshipKeyPaths(relationsToPrefetch);
		spec.setRefreshesRefetchedObjects(refreshObjects);
		return ec.objectsWithFetchSpecification(spec);
	}

	public static NSArray fetchArray(String leNomTable, EOQualifier leQualifier, NSArray leSort, EOEditingContext ec, boolean refresh) {
		EOFetchSpecification myFetch;
		NSArray myResult;

		myFetch = new EOFetchSpecification(leNomTable, leQualifier, null);
		myFetch.setRefreshesRefetchedObjects(refresh);
		myFetch.setUsesDistinct(true);

		myResult = new NSArray(ec.objectsWithFetchSpecification(myFetch));
		if (leSort != null)
			myResult = new NSArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(myResult, leSort));

		return myResult;
	}

	public static NSArray fetchSharedArrayWithNomTableWithSpec(String leNomTable, EOQualifier leQualifier, NSArray leSort, EOEditingContext ec, boolean fetchAll, String fetchSpec) {
		NSArray myResult;
		EOFetchSpecification myFetch = null;

		if ((NSArray) ((NSDictionary) ec.sharedEditingContext().objectsByEntityName()).valueForKey(leNomTable) == null) {
			if (fetchSpec != null) {
				myFetch = EOFetchSpecification.fetchSpecificationNamed(fetchSpec, leNomTable);

				if ((myFetch != null) && (!fetchAll) && (leQualifier != null))
					myFetch.setQualifier(leQualifier);
			}

			if (myFetch == null) {
				if (fetchAll)
					myFetch = new EOFetchSpecification(leNomTable, null, leSort);
				else
					myFetch = new EOFetchSpecification(leNomTable, leQualifier, leSort);
			}

			myFetch.setRefreshesRefetchedObjects(true);
			myFetch.setUsesDistinct(true);

			myResult = new NSArray(ec.sharedEditingContext().objectsWithFetchSpecification(myFetch));
		}
		else
			myResult = new NSArray((NSArray) ((NSDictionary) ec.sharedEditingContext().objectsByEntityName()).valueForKey(leNomTable));

		if (leQualifier != null)
			myResult = new NSArray((NSArray) (EOQualifier.filteredArrayWithQualifier(myResult, leQualifier)));

		if (leSort != null)
			myResult = new NSArray(EOSortOrdering.sortedArrayUsingKeyOrderArray(myResult, leSort));

		return myResult;
	}
}
