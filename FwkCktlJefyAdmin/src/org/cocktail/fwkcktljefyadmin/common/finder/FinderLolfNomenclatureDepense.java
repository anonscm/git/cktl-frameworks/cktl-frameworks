/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.finder;



import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureAbstract;
import org.cocktail.fwkcktljefyadmin.common.metier.EOLolfNomenclatureDepense;
import org.cocktail.fwkcktljefyadmin.common.metier.EOParametre;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;


public final class FinderLolfNomenclatureDepense extends Finder {
	public static final String LOLF_NIVEAU_DEPENSE_PARAM_KEY="LOLF_NIVEAU_DEPENSE";
	
	
	/**
	 * 
	 * @param ed
	 * @param exercice
	 * @param niveau Niveau dans l'arbre pour filtrer les resultats. Si null, tous les niveaux de l'arbre sont remontés.
	 * @return Les codes LOLF (actions, destinations, etc.) pour un exercice, triés par code.
	 */
	public static NSArray getLolfNomenclaturesValides(EOEditingContext ed, EOExercice exercice, Integer niveau) {
		return getLolfNomenclaturesValidesPourCode(ed, exercice, null, niveau);
	}
	
	/**
	 * @param ed
	 * @param exercice
	 * @param code
	 * @param niveau Niveau dans l'arbre pour filtrer les resultats. Si null, tous les niveaux de l'arbre sont remontés.
	 * @return Toutes les actions (indépendamment du niveau)
	 */
	public static NSArray getLolfNomenclaturesValidesPourCode(EOEditingContext ed, EOExercice exercice, String code, Integer niveau) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(EOLolfNomenclatureAbstract.QUAL_VALIDE);
		if (exercice !=null) {
			quals.addObject(EOLolfNomenclatureAbstract.getQualifierForExercice(exercice));
		}
		if (code !=null) {
			quals.addObject(new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_CODE_KEY, EOQualifier.QualifierOperatorEqual, code));
		}
		if (niveau !=null) {
			quals.addObject(new EOKeyValueQualifier(EOLolfNomenclatureAbstract.LOLF_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, niveau));
		}
		return EOLolfNomenclatureDepense.fetchAll(ed, new EOAndQualifier(quals), new NSArray(new Object[]{EOLolfNomenclatureAbstract.SORT_LOLF_CODE_ASC}), true);
	}
	
	public static EOLolfNomenclatureDepense getUnLolfNomenclatureValidePourCode(EOEditingContext ed, EOExercice exercice, String code, Integer niveau) {
		NSArray res = getLolfNomenclaturesValidesPourCode(ed, exercice, code, niveau);
		if (res.count()>0) {
			return (EOLolfNomenclatureDepense) res.objectAtIndex(0);
		}
		return null;
	}
	
	
	
	/**
	 * @param ed
	 * @param exercice
	 * @return Le niveau des actions a utiliser pour une depense (defini dans la table de parametrage).
	 */
	public static Integer getCurrentNiveau(EOEditingContext ed, EOExercice exercice) {
		EOParametre param = FinderParametre.getParametre(ed, LOLF_NIVEAU_DEPENSE_PARAM_KEY, exercice);
		return Integer.valueOf(param.parValue());
	}
	
	
	/**
	 * Recherche les types actions pour un exercice.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param exercice
	 *        exercice pour lequel faire la recherche
	 * @return
	 *        un NSArray de EOTypeAction
	 */
    public static final NSArray getTypeActions(EOEditingContext ed, EOExercice exercice) {
    	return getLolfNomenclaturesValides(ed, exercice, getCurrentNiveau(ed, exercice));
    }
    
    public static EOLolfNomenclatureDepense getUnTypeAction(EOEditingContext ed, EOExercice exercice, String tyacCode) {
    	return getUnLolfNomenclatureValidePourCode(ed, exercice, tyacCode, getCurrentNiveau(ed, exercice));
    }
    
    
//    public static EOLolfNomenclatureDepense getLesTypeActions(EOEditingContext ed, EOExercice exercice, String tyacCode) {
//    	NSMutableDictionary bindings=new NSMutableDictionary();
//    	bindings.setObjectForKey(EOTypeAction.ETAT_VALIDE, "tyetLibelle");
//    	bindings.setObjectForKey(tyacCode, "tyacCode");
//    	bindings.setObjectForKey(exercice, "exercice");
//    	
//    	NSArray array=Finder.tableauTrie(EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
//    			EOTypeAction.ENTITY_NAME, "Recherche", bindings), sort());
//    	if (array==null || array.count()==0)
//    		return null;
//    	return (EOTypeAction)array.objectAtIndex(0);
//    }
//
//    
    
    
    
    
    
    
    
    
//    
//    
//    
//
////    public static final NSArray getTypeActions(EOEditingContext ed, EOSource source, EOUtilisateur utilisateur) {
////    	return getTypeActions(ed, source.exercice(), source.organ(), source.typeCredit(), utilisateur);
////    }
//    
//    public static final NSArray getTypeActions(EOEditingContext ed, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit,
//    		EOUtilisateur utilisateur) {
//    	if (exercice==null || organ==null || typeCredit==null)
//    		return new NSArray();
//    	
//    	String parametre=FinderParametre.getParametreCtrlAction(ed, exercice);
//    	if (parametre==null)
//    		return new NSArray();
//    		
//    	NSArray lesActions=getTypeActions(ed, exercice);
//    	
//    	if (parametre.equals("NON"))
//    		return lesActions;
//    	
//		EOTypeApplication typeApplication=FinderTypeApplication.getTypeApplication(ed, EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK);
//		if (typeApplication==null)
//			throw new FactoryException("Type application "+EOTypeApplication.APPLICATION_DEPENSE_FRAMEWORK+
//					" (FinderTypeAction.getTypeActions(), edc="+ed+") "+typeApplication);
//
//		EOFonction fonction=FinderFonction.getFonction(ed, EOFonction.FONCTION_AUTRE_ACTION, typeApplication);
//		if (fonction==null) {
//			throw new FactoryException("fonction "+EOFonction.FONCTION_AUTRE_ACTION+
//					" (FinderTypeAction.getTypeActions("+ed+","+exercice+","+organ+","+typeCredit+","+utilisateur+")");
//		}
//    	NSMutableDictionary dico=new NSMutableDictionary();
//    	dico.setObjectForKey(utilisateur, "utilisateur");
//    	dico.setObjectForKey(fonction, "fonction");
//    	dico.setObjectForKey(exercice, "exercice");
//    	if (FinderUtilisateurFonctionExercice.getUtilisateurFontionExercices(ed, dico).count()>0) 
//    		return lesActions;
//
//    	NSMutableDictionary bindings=new NSMutableDictionary();
//    	bindings.setObjectForKey(organ, "organ");
//    	bindings.setObjectForKey(exercice, "exercice");
//    	bindings.setObjectForKey(typeCredit, "typeCredit");
//    	
//    	NSArray lesActionsVotees=(NSArray)EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
//    			EOOrganAction.ENTITY_NAME, "Recherche", bindings).valueForKeyPath(EOOrganAction.TYPE_ACTION_KEY);
//
//    	if (lesActionsVotees.count()==0)
//    		return lesActions;
//    	return Finder.tableauTrie(lesActionsVotees, sort());
//    }
//
//    public static final NSArray getTypeActionVote(EOEditingContext ed, EOSource source, EOTypeAction typeAction) {
//    	return getTypeActionVote(ed, source.exercice(), source.organ(), source.typeCredit(), typeAction);
//    }
//    
//    public static final NSArray getTypeActionVote(EOEditingContext ed, EOExercice exercice, EOOrgan organ, EOTypeCredit typeCredit,
//    		EOTypeAction typeAction) {
//    	if (exercice==null || organ==null || typeCredit==null || typeAction==null)
//    		return new NSArray();
//    	
//    	NSMutableDictionary bindings=new NSMutableDictionary();
//    	bindings.setObjectForKey(organ, "organ");
//    	bindings.setObjectForKey(exercice, "exercice");
//    	bindings.setObjectForKey(typeCredit, "typeCredit");
//    	bindings.setObjectForKey(typeAction, "typeAction");
//    	
//    	return Finder.tableauTrie((NSArray)EOUtilities.objectsWithFetchSpecificationAndBindings(ed,
//    			EOOrganAction.ENTITY_NAME, "Recherche", bindings).valueForKeyPath(EOOrganAction.TYPE_ACTION_KEY), sort());
//    }
//
//
//    /**
//     * Fetch tous les types action pour les garder en memoire et eviter de refetcher ensuite
//     * <BR>
//     * @param ed
//     *        editingContext dans lequel se fait le fetch
//     */
//    /*private static NSArray fetchTypeActions(EOEditingContext ed) {
//    	// if (arrayTypeActions==null)
//    		return Finder.fetchArray(ed,EOTypeAction.ENTITY_NAME,null,null, sort(), false);
//    }*/
//    
//    private static NSArray sort() {
//    	return new NSArray(EOSortOrdering.sortOrderingWithKey(EOTypeAction.TYAC_CODE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
//    }
}
