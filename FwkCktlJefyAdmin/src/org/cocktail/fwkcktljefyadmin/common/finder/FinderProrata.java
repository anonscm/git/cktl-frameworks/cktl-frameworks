/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.finder;

import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;
import org.cocktail.fwkcktljefyadmin.common.exception.NullBindingException;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrganProrata;
import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableDictionary;

public final class FinderProrata extends Finder {

	/**
	 * Recherche les taux de proratas utilisables pour un organ et un exercice par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : exercice, organ <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOTauxProrata
	 */
	public static final NSArray getTauxProratas(EOEditingContext ed, NSDictionary bindings) {
		NSArray arrayOrganProrata;

		if (bindings == null)
			throw new NullBindingException("les bindings 'exercice' et 'organ' sont obligatoires");
		if (bindings.objectForKey("exercice") == null)
			throw new NullBindingException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("organ") == null)
			throw new NullBindingException("le bindings 'organ' est obligatoire");

		try {
			arrayOrganProrata = CktlCallEOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOOrganProrata.ENTITY_NAME, "Recherche", bindings);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return (NSArray) arrayOrganProrata.valueForKeyPath(EOOrganProrata.TAUX_PRORATA_KEY);
	}

	/**
	 * Recherche les taux de proratas valides utilisables pour un organ et un exercice par la fetchSpecification Recherche.<BR>
	 * Bindings pris en compte : exercice, organ <BR>
	 * 
	 * @param ed editingContext dans lequel se fait le fetch
	 * @param bindings dictionnaire contenant les bindings
	 * @return un NSArray contenant des EOTauxProrata
	 */
	public static final NSArray getTauxProratasValides(EOEditingContext ed, NSDictionary bindings) {
		NSArray arrayOrganProrata;

		if (bindings == null)
			throw new NullBindingException("les bindings 'exercice' et 'organ' sont obligatoires");
		if (bindings.objectForKey("exercice") == null)
			throw new NullBindingException("le bindings 'exercice' est obligatoire");
		if (bindings.objectForKey("organ") == null)
			throw new NullBindingException("le bindings 'organ' est obligatoire");

		NSMutableDictionary myBindings = new NSMutableDictionary(bindings);
		myBindings.setObjectForKey(EOTypeEtat.ETAT_VALIDE, "tyetLibelle");

		try {
			arrayOrganProrata = CktlCallEOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOOrganProrata.ENTITY_NAME, "Recherche", myBindings);
		} catch (Throwable e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}

		return (NSArray) arrayOrganProrata.valueForKeyPath(EOOrganProrata.TAUX_PRORATA_KEY);
	}
}
