/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.finder;

import java.util.Enumeration;

import org.cocktail.fwkcktldroitsutils.common.CktlCallEOUtilities;
import org.cocktail.fwkcktljefyadmin.common.exception.IllegalArgumentException;
import org.cocktail.fwkcktljefyadmin.common.exception.NullBindingException;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrgan;
import org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice;
import org.cocktail.fwkcktljefyadmin.common.metier.EOUtilisateur;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOFetchSpecification;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSMutableSet;
import com.webobjects.foundation.NSTimestamp;

import er.extensions.eof.ERXQ;
import er.extensions.eof.ERXS;
import er.extensions.foundation.ERXArrayUtilities;

public final class FinderOrgan extends Finder {

	public static final String ORGAN_DEPENSE = "DEPENSE";
	public static final String ORGAN_RECETTE = "RECETTE";
	public static final String ORGAN_BUDGET = "BUDGET";

	// tyet_id pris en compte :
	// 16 : TOUTES
	// 19 : AUCUNE
	// 20 : RECETTES
	// 21 : DEPENSES

	public static final String DECODE_DEPENSE = "decode(o.org_op_autorisees, 19,0,20,0,1)";
	public static final String DECODE_BUDGET = "1";
	public static final String DECODE_RECETTE = "decode(o.org_op_autorisees, 19,0,21,0,1)";

	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants sans structure specifiee).
	 */
	public static NSArray fetchOrgansForStructure(EOEditingContext ec,
			String cStructure, EOExercice exercice) {
		EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
				EOOrgan.C_STRUCTURE_KEY + "=%@ and " + EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXERCICE_KEY + "=%@",
				new NSArray(new Object[] {
						cStructure, exercice
				}));
		// recuperer tous les organ directement rattaches a la structure
		EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, qual, null);
		NSArray res1 = ec.objectsWithFetchSpecification(fs);
		NSMutableArray res = new NSMutableArray(res1);
		// recuperer tous les enfants
		for (int i = 0; i < res1.count(); i++) {
			res.addObjectsFromArray(getAllOrganFilsSansStructure((EOOrgan) res1.objectAtIndex(i)));
		}
		return res;
	}
	
	/**
	 * @param ec
	 * @param structureUlr La structure de rattachement
	 * @param exercice l'exercice des organs à récupérer
	 * @param utlOrdre l'utilisateur ayant les droits sur les organ à récupérer
	 * @param niveauMin le niveau d'organ minimum, exemple : niveauMin à 4 va récupérer tous les sous CR, niveauMin à 3 tous les CR et sous CR...
	 * @param modeRa si true, renvoie uniquement les organs de type ressources affectées
	 * @return Un tableau contenant toutes les organ rattaches a la structure (ainsi que les organ enfants).
	 */
	public static NSArray fetchOrgansForStructureAndUtl(EOEditingContext ec,
	        String cStructure, EOExercice exercice, Integer utlOrdre, Integer niveauMin, Boolean typeRA) {
	    EOQualifier qual = EOQualifier.qualifierWithQualifierFormat(
	            EOOrgan.UTILISATEUR_ORGANS_KEY + "." + EOUtilisateur.UTL_ORDRE_KEY + "=%@ and " +
	            EOOrgan.C_STRUCTURE_KEY + "=%@ and " + EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXERCICE_KEY + "=%@",
	            new NSArray(new Object[] {
	                    utlOrdre, cStructure, exercice
	            }));
	    // recuperer tous les organ directement rattaches a la structure
	    NSTimestamp now = new NSTimestamp();
	    qual = ERXQ.and(
	    		qual, 
	    		ERXQ.lessThanOrEqualTo(EOOrgan.ORG_DATE_OUVERTURE_KEY, now),
	    		ERXQ.or(
	    			ERXQ.greaterThanOrEqualTo(EOOrgan.ORG_DATE_CLOTURE_KEY, now),
	    			ERXQ.isNull(EOOrgan.ORG_DATE_CLOTURE_KEY)
	    			)
	    		);
	    
	    EOFetchSpecification fs = new EOFetchSpecification(EOOrgan.ENTITY_NAME, qual, null);
	    NSArray<EOOrgan> organs = ec.objectsWithFetchSpecification(fs);
	    NSMutableArray<EOOrgan> res = new NSMutableArray<EOOrgan>();
	    // recuperer tous les enfants
	    for (EOOrgan organ : organs) {
	        res.addObject(organ);
	        res.addObjectsFromArray(getAllOrganFils(organ, exercice));
	    }
	    // On filtre selon le niveau min donné
	    if (niveauMin != null) {
	        ERXQ.filter(res, ERXQ.greaterThanOrEqualTo(EOOrgan.ORG_NIVEAU_KEY, niveauMin));
	    }
	    // On filtre selon le type RA
	    if (typeRA != null) {
	        ERXQ.filter(res, ERXQ.equals(EOOrgan.IS_TYPE_RA_KEY, typeRA));
	    }
	    // On ordonne
	    ERXS.sort(res, EOOrgan.SORT_DEFAULT);
	    return ERXArrayUtilities.arrayWithoutDuplicates(res);
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif) sans structure specifiee.
	 */
	public static NSArray getAllOrganFilsSansStructure(EOOrgan organ) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration = organ.organFils().objectEnumerator();
		while (enumeration.hasMoreElements()) {
			final EOOrgan object = (EOOrgan) enumeration.nextElement();
			if (object.cStructure() == null || NSKeyValueCoding.NullValue.equals(object.cStructure())) {
				res.addObject(object);
				res.addObjectsFromArray(getAllOrganFilsSansStructure(object));
			}
		}
		return res.immutableClone();
	}

	/**
	 * @param organ
	 * @return Un tableau de tous les organ fils (recursif).
	 */
	public static NSArray getAllOrganFils(EOOrgan organ) {
		return getAllOrganFils(organ, null);
	}
	
	/**
	 * @param organ
	 * @param exercice L'exercice en cours
	 * @return Un tableau de tous les organ fils (recursif).
	 */
	public static NSArray getAllOrganFils(EOOrgan organ, EOExercice exercice) {
		NSMutableArray res = new NSMutableArray();
		Enumeration enumeration;
		if (exercice != null) {
			EOQualifier qual = ERXQ.equals(EOOrgan.ORGAN_EXERCICE_KEY + "." + EOOrganExercice.EXERCICE_KEY, exercice);
			enumeration = organ.organFils(qual, true).objectEnumerator();
		} else {
			enumeration = organ.organFils().objectEnumerator();
		}
		while (enumeration.hasMoreElements()) {
			final EOOrgan object = (EOOrgan) enumeration.nextElement();
			res.addObject(object);
			res.addObjectsFromArray(getAllOrganFils(object, exercice));
		}
		return res.immutableClone();
	}

	
	//	/* Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR>
	//	 * Bindings pris en compte : utilisateur, exercice, convention
	//	 */
	//    public static final NSArray getOrgansPourConventionEtExercice(EOEditingContext ed, NSDictionary bindings) {
	//    	if (bindings == null)
	//    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
	//    	if (bindings.objectForKey("exercice")==null)
	//    		throw new FactoryException("le bindings 'exercice' est obligatoire");      			
	//    	if (bindings.objectForKey("utilisateur")==null)
	//    		throw new FactoryException("le bindings 'utilisateur' est obligatoire");      			
	//    	if (bindings.objectForKey("convention")==null)
	//    		throw new FactoryException("le bindings 'convention' est obligatoire");      			
	//    	
	//    	NSArray	larray=EOUtilities.objectsWithFetchSpecificationAndBindings(ed,EOConventionNonLimitative.ENTITY_NAME,"Recherche",bindings);
	//    	NSArray resultats=(NSArray)larray.valueForKeyPath(EOConventionNonLimitative.ORGAN_KEY); 
	//    			
	//    	return EOSortOrdering.sortedArrayUsingKeyOrderArray(resultats, sort());
	//    }

	//	/**
	//	 * Recherche de lignes budgetaire par la fetchSpecification Recherche.<BR>
	//	 * Bindings pris en compte : utilisateur, exercice, sourceLibelle <BR>
	//	 * 
	//	 * @param ed editingContext dans lequel se fait le fetch
	//	 * @param bindings dictionnaire contenant les bindings
	//	 * @return un NSArray contenant des EOOrgan
	//	 */
	//
	//	public static final NSArray getOrgans(EOEditingContext ed, NSDictionary bindings) {
	//		NSArray lesOrgans = new NSArray();
	//		NSArray larray = FinderOrgan.getUtilisateurOrgans(ed, bindings);
	//
	//		if (larray != null && larray.count() > 0) {
	//			// TODO Optimisation base de donnees
	//			lesOrgans = (NSArray) larray.valueForKeyPath(EOUtilisateurOrgan.ORGAN_KEY);
	//
	//			if (bindings != null && bindings.objectForKey("sourceLibelle") != null) {
	//				lesOrgans = EOQualifier.filteredArrayWithQualifier(lesOrgans,
	//						EOQualifier.qualifierWithQualifierFormat("sourceLibelle caseInsensitiveLike %@",
	//						new NSArray(bindings.objectForKey("sourceLibelle"))));
	//				//	    	if (lesOrgans != null && lesOrgans.count()>1) {
	//				//	    		lesOrgans = Finder.tableauTrie(lesOrgans, sort());
	//				//	    	}
	//			}
	//		}
	//		return lesOrgans;
	//	}

	//	public static final NSArray getOrgans(EOEditingContext ed, EOUtilisateur utilisateur, EOExercice exercice) {
	//		NSMutableDictionary bindings = new NSMutableDictionary();
	//		bindings.setObjectForKey(utilisateur, "utilisateur");
	//		bindings.setObjectForKey(exercice, "exercice");
	//		return FinderOrgan.getOrgans(ed, bindings);
	//	}

	//	/**
	//	 * Recherche de droits pour un utilisateur sur les CR ou sous CR par la fetchSpecification Recherche.<BR>
	//	 * Bindings pris en compte : utilisateur, exercice <BR>
	//	 * 
	//	 * @param ed editingContext dans lequel se fait le fetch
	//	 * @param bindings dictionnaire contenant les bindings
	//	 * @return un NSArray contenant des EOUtilisateurOrgan
	//	 */
	//	public static final NSArray getUtilisateurOrgans(EOEditingContext ed, NSDictionary bindings) {
	//		if (bindings == null) {
	//			throw new NullBindingException("le bindings 'exercice' est obligatoire");
	//		}
	//		if (bindings.objectForKey("exercice") == null) {
	//			throw new NullBindingException("le bindings 'exercice' est obligatoire");
	//		}
	//		if (bindings.objectForKey("utilisateur") == null) {
	//			throw new NullBindingException("le bindings 'utilisateur' est obligatoire");
	//		}
	//
	//		return EOUtilities.objectsWithFetchSpecificationAndBindings(ed, EOUtilisateurOrgan.ENTITY_NAME, "Recherche", bindings);
	//	}

	private static NSArray sort() {
		NSMutableArray array = new NSMutableArray();
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_UB_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_CR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		array.addObject(EOSortOrdering.sortOrderingWithKey(EOOrgan.ORG_SOUSCR_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
		return array;
	}

	//FIXME ajouter possibilite de recuperer les organ sans utilisateur
	//
	//	public static final NSArray getRawRowOrgan(EOEditingContext ed, NSDictionary bindings, int niveauMax) {
	//		if (bindings == null) {
	//			throw new NullBindingException("le bindings 'exercice' est obligatoire");
	//		}
	//		if (bindings.objectForKey("exercice") == null) {
	//			throw new NullBindingException("le bindings 'exercice' est obligatoire");
	//		}
	//		if (bindings.objectForKey("utilisateur") == null) {
	//			throw new NullBindingException("le bindings 'utilisateur' est obligatoire");
	//		}
	//
	//		if (bindings.objectForKey("exercice").getClass() != EOExercice.class) {
	//			throw new IllegalArgumentException("le bindings 'exercice' doit etre de type EOExercice");
	//		}
	//		if (bindings.objectForKey("utilisateur").getClass() != EOUtilisateur.class) {
	//			throw new IllegalArgumentException("le bindings 'utilisateur' doit etre de type EOUtilisateur");
	//		}
	//
	//		String typeUtilisation = FinderOrgan.ORGAN_DEPENSE;
	//		if (bindings.objectForKey("typeUtilisation") != null) {
	//			typeUtilisation = (String) bindings.objectForKey("typeUtilisation");
	//		}
	//
	//		return getRawRowOrgan(ed, (EOExercice) bindings.objectForKey("exercice"), (EOUtilisateur) bindings.objectForKey("utilisateur"), typeUtilisation, niveauMax);
	//	}
	//
	//	public static final NSArray getRawRowOrgan(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur, String typeUtilisation, int niveauMax) {
	//		if (exercice == null) {
	//			throw new NullBindingException("le parametre 'exercice' est obligatoire");
	//		}
	//		if (utilisateur == null) {
	//			throw new NullBindingException("le parametre 'utilisateur' est obligatoire");
	//		}
	//
	//		return EOUtilities.rawRowsForSQL(ed, MODEL_NAME, constructionChaine(ed, exercice, utilisateur, typeUtilisation, niveauMax), null);
	//
	//	}

	public static final NSArray getRawRowOrgan(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		if (exeOrdre == null) {
			throw new NullBindingException("le parametre 'exeOrdre' est obligatoire");
		}
		if (utlOrdre == null) {
			throw new NullBindingException("le parametre 'utlOrdre' est obligatoire");
		}

		try {
			return CktlCallEOUtilities.rawRowsForSQL(ed, MODEL_NAME, constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax), null);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		return null;
		//return EOUtilities.rawRowsForSQL(ed, MODEL_NAME, constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax), null);
	}

	//	protected static String constructionChaine(EOEditingContext ed, EOExercice exercice, EOUtilisateur utilisateur, String typeUtilisation, int niveauMax) {
	//		Integer exeOrdre = new Integer(exercice.exeExercice().toString());
	//		Integer utlOrdre = new Integer(EOUtilities.primaryKeyForObject(ed, utilisateur).objectForKey(EOUtilisateur.UTL_ORDRE_KEY).toString());
	//		return constructionChaine(ed, exeOrdre, utlOrdre, typeUtilisation, niveauMax);
	//	}

	protected static String constructionChaine(EOEditingContext ed, Integer exeOrdre, Integer utlOrdre, String typeUtilisation, int niveauMax) {
		String select = "";

		if (typeUtilisation == null || (!typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET) && !typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE) &&
				!typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE))) {
			typeUtilisation = FinderOrgan.ORGAN_DEPENSE;
		}

		String decodeTypeUtilisation = "";
		if (typeUtilisation.equals(FinderOrgan.ORGAN_DEPENSE)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_DEPENSE;
		}
		if (typeUtilisation.equals(FinderOrgan.ORGAN_BUDGET)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_BUDGET;
		}
		if (typeUtilisation.equals(FinderOrgan.ORGAN_RECETTE)) {
			decodeTypeUtilisation = FinderOrgan.DECODE_RECETTE;
		}

		if (niveauMax < 0) {
			throw new IllegalArgumentException("le niveau maximum doit etre >=0");
		}

		// tous les sous cr
		if (niveauMax >= 4) {
			select = select + "select o.org_id, o.org_niv, o.org_pere, o.org_univ, o.org_etab, o.org_ub, o.org_cr, o.org_lib, o.org_lucrativite, ";
			select = select + "o.org_date_ouverture, o.org_date_cloture, o.c_structure, o.log_ordre, o.tyor_id, o.org_souscr, " + decodeTypeUtilisation + " droit ";
			select = select + "from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + " and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))";

			select = select + " union all ";
		}

		// tous les cr
		if (niveauMax >= 3) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ where org_id in ";
			select = select + "(select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))) ";
			select = select + "     union all ";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR, " + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les ub
		if (niveauMax >= 2) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select + "   (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// tous les etab
		if (niveauMax >= 1) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select + "   (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";

			select = select + " union all ";
		}

		// toutes les racines
		if (niveauMax >= 0) {
			select = select + "select org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite, ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr, max(droit) droit from ";
			select = select + "  (  select ORG_ID, ORG_NIV, ORG_PERE, ORG_UNIV, ORG_ETAB, ORG_UB, ORG_CR, ORG_LIB, ORG_LUCRATIVITE, ";
			select = select + " ORG_DATE_OUVERTURE, ORG_DATE_CLOTURE, C_STRUCTURE, LOG_ORDRE, TYOR_ID, ORG_SOUSCR, 0 droit from jefy_admin.organ o where org_id in ";
			select = select
					+ "   (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=4 and utl_ordre="
					+ utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=3 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy'))))";
			select = select + "    union all";
			select = select + "    select org_pere from jefy_admin.organ where org_id in (select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=2 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "    select o.org_pere from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=1 and utl_ordre=" + utlOrdre;
			select = select + "   and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')))";
			select = select + "    union all";
			select = select + "   select o.ORG_ID, o.ORG_NIV, o.ORG_PERE, o.ORG_UNIV, o.ORG_ETAB, o.ORG_UB, o.ORG_CR, o.ORG_LIB, o.ORG_LUCRATIVITE, ";
			select = select + " o.ORG_DATE_OUVERTURE, o.ORG_DATE_CLOTURE, o.C_STRUCTURE, o.LOG_ORDRE, o.TYOR_ID, o.ORG_SOUSCR," + decodeTypeUtilisation + " from jefy_admin.organ o, jefy_admin.utilisateur_organ uo where o.org_id=uo.org_id and org_niv=0 and utl_ordre=" + utlOrdre;
			select = select + "  and o.org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (o.org_date_cloture is null or o.org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " )  ";
			select = select + "  where org_date_ouverture<to_date('15/11/" + exeOrdre + "','dd/mm/yyyy') and (org_date_cloture is null or org_date_cloture>to_date('15/11/" + exeOrdre + "','dd/mm/yyyy')) ";
			select = select + " group by org_id, org_niv, org_pere, org_univ, org_etab, org_ub, org_cr, org_lib, org_lucrativite,  ";
			select = select + " org_date_ouverture, org_date_cloture, c_structure, log_ordre, tyor_id, org_souscr  ";
		}

		return select;
	}

	/**
	 * Recupere la structure associee a une branche de l'organigrame budgetaire. Si aucune structure n'est definie sur la branche on recupere la
	 * structure definie sur la branche parente. niveauMin permet de specifier jusqu'ou on remonte pour chercher la structure. cf
	 * {@link EOOrgan#ORG_NIV_0} et les autres constantes de niveau definies dans EOOrgan.
	 * 
	 * @param organ Branche de l'organigramme sur laquelle est faite la depense
	 * @param niveauMin si null, le niveau 0 est utilise.
	 * @return un objet Structure ou null si aucune structure n'a été trouvée.
	 */
	public static EOStructure getStructureForOrgan(final EOOrgan organ, Integer niveauMin) {
		EOStructure res = null;
		EOOrgan org = organ;
		if (niveauMin == null) {
			niveauMin = EOOrgan.ORG_NIV_0;
		}
		res = org.structure();
		while (res == null && org != null && org.orgNiveau().intValue() >= niveauMin.intValue()) {
			org = org.organPere();
			res = org.structure();
		}
		return res;
	}
	
	/**
	 * Renvoie toutes les structures reliées à au moins un organ, sans doublons.
	 * @param editingContext
	 * @return ensemble des structures étant reliées à au moins un organ
	 */
	public static NSArray getAllStructureAvecOrgan(EOEditingContext editingContext) {
		NSMutableSet res = new NSMutableSet();
		EOKeyValueQualifier qualifier = new EOKeyValueQualifier(EOOrgan.STRUCTURE_KEY, EOKeyValueQualifier.QualifierOperatorNotEqual, null);
		NSArray allOrgansWithStructure = EOOrgan.fetchAll(editingContext, qualifier);
		if (allOrgansWithStructure!=null && !allOrgansWithStructure.isEmpty()) {
			res = new NSMutableSet((NSArray)allOrgansWithStructure.valueForKey(EOOrgan.STRUCTURE_KEY));
		}
		return res.allObjects();
	}

}
