/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.finder;

import org.cocktail.fwkcktljefyadmin.common.metier.EOTypeApplication;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;


public final class FinderTypeApplication extends Finder {

	/**
	 * Recherche des differents types applications.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @return
	 *        un NSArray de EOTypeApplication
	 */
     public static final NSArray getTypesApplication(EOEditingContext ed) {
    	 return Finder.tableauTrie(fetchTypeApplications(ed), sort());
     }

     /**
	 * Recherche d'un type application par son libelle.
	 * <BR>
	 * @param ed
	 *        editingContext dans lequel se fait le fetch
	 * @param libelle
	 *        libelle auquel correspond l'application
	 * @return
	 *        un EOTypeApplication
	 */
     public static final EOTypeApplication getTypeApplication(EOEditingContext ed, String libelle) {
    	 return getUnTypeApplication(ed, libelle);
     }

 	/**
 	 * Recherche d'un type application par son libelle.
 	 * <BR>
 	 * @param ed
 	 *        editingContext dans lequel se fait le fetch
 	 * @param libelle
 	 *        libelle auquel correspond l'application
 	 * @return
 	 *        un EOTypeApplication
 	 */
     private static EOTypeApplication getUnTypeApplication(EOEditingContext ed, String libelle) {
    	NSArray typeApplications=null;
    	
   		NSArray arrayTypeApplication=fetchTypeApplications(ed);
    	
    	typeApplications=new NSArray((NSArray)(EOQualifier.filteredArrayWithQualifier(arrayTypeApplication, 
    			EOQualifier.qualifierWithQualifierFormat(EOTypeApplication.TYAP_STRID_KEY+" = %@", new NSArray(libelle)))));

    	if (typeApplications==null || typeApplications.count()==0)
    		return null;
    	
        return (EOTypeApplication)typeApplications.objectAtIndex(0);
    }

    /**
     * Fetch tous les types d'application pour les garder en memoire et eviter de refetcher ensuite
     * <BR>
     * @param ed
     *        editingContext dans lequel se fait le fetch
     */
     private static NSArray fetchTypeApplications(EOEditingContext ed) {
     		return Finder.fetchArray(ed,EOTypeApplication.ENTITY_NAME,null,null,null,true);
    }

     private static NSArray sort() {
     	return new NSArray(EOSortOrdering.sortOrderingWithKey(EOTypeApplication.TYAP_LIBELLE_KEY, EOSortOrdering.CompareCaseInsensitiveAscending));
     }
}
