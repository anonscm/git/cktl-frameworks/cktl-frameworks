/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersjur.java instead.
package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPersjur extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_Persjur";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.PERSJUR";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "pjId";

	public static final String PJ_COMMENTAIRE_KEY = "pjCommentaire";
	public static final String PJ_DATE_DEBUT_KEY = "pjDateDebut";
	public static final String PJ_DATE_FIN_KEY = "pjDateFin";
	public static final String PJ_LIBELLE_KEY = "pjLibelle";
	public static final String PJ_NIVEAU_KEY = "pjNiveau";

// Attributs non visibles
	public static final String PJ_ID_KEY = "pjId";
	public static final String PJ_PERE_KEY = "pjPere";
	public static final String TPJ_ID_KEY = "tpjId";

//Colonnes dans la base de donnees
	public static final String PJ_COMMENTAIRE_COLKEY = "PJ_COMMENTAIRE";
	public static final String PJ_DATE_DEBUT_COLKEY = "PJ_DATE_DEBUT";
	public static final String PJ_DATE_FIN_COLKEY = "PJ_DATE_FIN";
	public static final String PJ_LIBELLE_COLKEY = "PJ_LIBELLE";
	public static final String PJ_NIVEAU_COLKEY = "PJ_NIVEAU";

	public static final String PJ_ID_COLKEY = "PJ_ID";
	public static final String PJ_PERE_COLKEY = "PJ_PERE";
	public static final String TPJ_ID_COLKEY = "TPJ_ID";


	// Relationships
	public static final String PERSJUR_KEY = "persjur";
	public static final String PERSJUR_PERSONNES_KEY = "persjurPersonnes";
	public static final String PERSJURS_KEY = "persjurs";
	public static final String PRM_ORGANS_KEY = "prmOrgans";
	public static final String TYPE_PERSJUR_KEY = "typePersjur";



	// Accessors methods
  public String pjCommentaire() {
    return (String) storedValueForKey(PJ_COMMENTAIRE_KEY);
  }

  public void setPjCommentaire(String value) {
    takeStoredValueForKey(value, PJ_COMMENTAIRE_KEY);
  }

  public NSTimestamp pjDateDebut() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_DEBUT_KEY);
  }

  public void setPjDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_DEBUT_KEY);
  }

  public NSTimestamp pjDateFin() {
    return (NSTimestamp) storedValueForKey(PJ_DATE_FIN_KEY);
  }

  public void setPjDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, PJ_DATE_FIN_KEY);
  }

  public String pjLibelle() {
    return (String) storedValueForKey(PJ_LIBELLE_KEY);
  }

  public void setPjLibelle(String value) {
    takeStoredValueForKey(value, PJ_LIBELLE_KEY);
  }

  public Integer pjNiveau() {
    return (Integer) storedValueForKey(PJ_NIVEAU_KEY);
  }

  public void setPjNiveau(Integer value) {
    takeStoredValueForKey(value, PJ_NIVEAU_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur persjur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur)storedValueForKey(PERSJUR_KEY);
  }

  public void setPersjurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur oldValue = persjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSJUR_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypePersjur typePersjur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypePersjur)storedValueForKey(TYPE_PERSJUR_KEY);
  }

  public void setTypePersjurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypePersjur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypePersjur oldValue = typePersjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_PERSJUR_KEY);
    }
  }
  
  public NSArray persjurPersonnes() {
    return (NSArray)storedValueForKey(PERSJUR_PERSONNES_KEY);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier) {
    return persjurPersonnes(qualifier, null, false);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier, boolean fetch) {
    return persjurPersonnes(qualifier, null, fetch);
  }

  public NSArray persjurPersonnes(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurPersonnes();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersjurPersonnesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public void removeFromPersjurPersonnesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne createPersjurPersonnesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_PersjurPersonne");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJUR_PERSONNES_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne) eo;
  }

  public void deletePersjurPersonnesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJUR_PERSONNES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjurPersonnesRelationships() {
    Enumeration objects = persjurPersonnes().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjurPersonnesRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOPersjurPersonne)objects.nextElement());
    }
  }

  public NSArray persjurs() {
    return (NSArray)storedValueForKey(PERSJURS_KEY);
  }

  public NSArray persjurs(EOQualifier qualifier) {
    return persjurs(qualifier, null, false);
  }

  public NSArray persjurs(EOQualifier qualifier, boolean fetch) {
    return persjurs(qualifier, null, fetch);
  }

  public NSArray persjurs(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = persjurs();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPersjursRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
  }

  public void removeFromPersjursRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur createPersjursRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_Persjur");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PERSJURS_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur) eo;
  }

  public void deletePersjursRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PERSJURS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPersjursRelationships() {
    Enumeration objects = persjurs().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePersjursRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur)objects.nextElement());
    }
  }

  public NSArray prmOrgans() {
    return (NSArray)storedValueForKey(PRM_ORGANS_KEY);
  }

  public NSArray prmOrgans(EOQualifier qualifier) {
    return prmOrgans(qualifier, null, false);
  }

  public NSArray prmOrgans(EOQualifier qualifier, boolean fetch) {
    return prmOrgans(qualifier, null, fetch);
  }

  public NSArray prmOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan.PERSJUR_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = prmOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToPrmOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public void removeFromPrmOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan createPrmOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_PrmOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, PRM_ORGANS_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan) eo;
  }

  public void deletePrmOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, PRM_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllPrmOrgansRelationships() {
    Enumeration objects = prmOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deletePrmOrgansRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOPrmOrgan)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOPersjur avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersjur createEOPersjur(EOEditingContext editingContext, NSTimestamp pjDateDebut
, Integer pjNiveau
, org.cocktail.fwkcktljefyadmin.common.metier.EOTypePersjur typePersjur			) {
    EOPersjur eo = (EOPersjur) createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME);    
		eo.setPjDateDebut(pjDateDebut);
		eo.setPjNiveau(pjNiveau);
    eo.setTypePersjurRelationship(typePersjur);
    return eo;
  }

  
	  public EOPersjur localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersjur)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersjur creerInstance(EOEditingContext editingContext) {
	  		EOPersjur object = (EOPersjur)createAndInsertInstance(editingContext, _EOPersjur.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPersjur localInstanceIn(EOEditingContext editingContext, EOPersjur eo) {
    EOPersjur localInstance = (eo == null) ? null : (EOPersjur)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersjur#localInstanceIn a la place.
   */
	public static EOPersjur localInstanceOf(EOEditingContext editingContext, EOPersjur eo) {
		return EOPersjur.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersjur fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersjur fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersjur fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersjur eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersjur)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersjur fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersjur eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersjur ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersjur fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
