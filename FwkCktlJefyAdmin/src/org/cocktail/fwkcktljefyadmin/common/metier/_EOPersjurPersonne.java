/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOPersjurPersonne.java instead.
package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOPersjurPersonne extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_PersjurPersonne";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.PERSJUR_PERSONNE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "prpId";

	public static final String PERS_ID_KEY = "persId";
	public static final String PRP_DATE_DEBUT_KEY = "prpDateDebut";
	public static final String PRP_DATE_FIN_KEY = "prpDateFin";
	public static final String PRP_LIBELLE_KEY = "prpLibelle";

// Attributs non visibles
	public static final String PJ_ID_KEY = "pjId";
	public static final String PRP_ID_KEY = "prpId";

//Colonnes dans la base de donnees
	public static final String PERS_ID_COLKEY = "PERS_ID";
	public static final String PRP_DATE_DEBUT_COLKEY = "PRP_DATE_DEBUT";
	public static final String PRP_DATE_FIN_COLKEY = "PRP_DATE_FIN";
	public static final String PRP_LIBELLE_COLKEY = "PRP_LIBELLE";

	public static final String PJ_ID_COLKEY = "PJ_ID";
	public static final String PRP_ID_COLKEY = "PRP_ID";


	// Relationships
	public static final String PERSJUR_KEY = "persjur";



	// Accessors methods
  public Integer persId() {
    return (Integer) storedValueForKey(PERS_ID_KEY);
  }

  public void setPersId(Integer value) {
    takeStoredValueForKey(value, PERS_ID_KEY);
  }

  public NSTimestamp prpDateDebut() {
    return (NSTimestamp) storedValueForKey(PRP_DATE_DEBUT_KEY);
  }

  public void setPrpDateDebut(NSTimestamp value) {
    takeStoredValueForKey(value, PRP_DATE_DEBUT_KEY);
  }

  public NSTimestamp prpDateFin() {
    return (NSTimestamp) storedValueForKey(PRP_DATE_FIN_KEY);
  }

  public void setPrpDateFin(NSTimestamp value) {
    takeStoredValueForKey(value, PRP_DATE_FIN_KEY);
  }

  public String prpLibelle() {
    return (String) storedValueForKey(PRP_LIBELLE_KEY);
  }

  public void setPrpLibelle(String value) {
    takeStoredValueForKey(value, PRP_LIBELLE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur persjur() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur)storedValueForKey(PERSJUR_KEY);
  }

  public void setPersjurRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur oldValue = persjur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, PERSJUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, PERSJUR_KEY);
    }
  }
  

/**
 * Créer une instance de EOPersjurPersonne avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOPersjurPersonne createEOPersjurPersonne(EOEditingContext editingContext, Integer persId
, NSTimestamp prpDateDebut
, org.cocktail.fwkcktljefyadmin.common.metier.EOPersjur persjur			) {
    EOPersjurPersonne eo = (EOPersjurPersonne) createAndInsertInstance(editingContext, _EOPersjurPersonne.ENTITY_NAME);    
		eo.setPersId(persId);
		eo.setPrpDateDebut(prpDateDebut);
    eo.setPersjurRelationship(persjur);
    return eo;
  }

  
	  public EOPersjurPersonne localInstanceIn(EOEditingContext editingContext) {
	  		return (EOPersjurPersonne)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOPersjurPersonne creerInstance(EOEditingContext editingContext) {
	  		EOPersjurPersonne object = (EOPersjurPersonne)createAndInsertInstance(editingContext, _EOPersjurPersonne.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOPersjurPersonne localInstanceIn(EOEditingContext editingContext, EOPersjurPersonne eo) {
    EOPersjurPersonne localInstance = (eo == null) ? null : (EOPersjurPersonne)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOPersjurPersonne#localInstanceIn a la place.
   */
	public static EOPersjurPersonne localInstanceOf(EOEditingContext editingContext, EOPersjurPersonne eo) {
		return EOPersjurPersonne.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOPersjurPersonne fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOPersjurPersonne fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOPersjurPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOPersjurPersonne)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOPersjurPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOPersjurPersonne fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOPersjurPersonne eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOPersjurPersonne)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOPersjurPersonne fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOPersjurPersonne eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOPersjurPersonne ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOPersjurPersonne fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
