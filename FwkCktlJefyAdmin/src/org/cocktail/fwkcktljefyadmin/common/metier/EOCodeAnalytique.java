/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.metier;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.cocktail.fwkcktldroitsutils.common.util.MyDateCtrl;
import org.cocktail.fwkcktldroitsutils.common.util.MyStringCtrl;
import org.cocktail.fwkcktlpersonne.common.metier.EOStructure;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

import er.extensions.eof.ERXQ;

public class EOCodeAnalytique extends _EOCodeAnalytique {
	public static final EOSortOrdering SORT_CAN_CODE_ASC = new EOSortOrdering(EOCodeAnalytique.CAN_CODE_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_CAN_NIVEAU_ASC = new EOSortOrdering(EOCodeAnalytique.CAN_NIVEAU_KEY, EOSortOrdering.CompareAscending);
	public static final EOSortOrdering SORT_TYPE_ETAT_PUBLIC_ASC = new EOSortOrdering(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY, EOSortOrdering.CompareAscending);

	public static final EOQualifier QUAL_CODE_ANALYTIQUE_VALIDE = EOQualifier.qualifierWithQualifierFormat(EOCodeAnalytique.TYPE_ETAT_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY + "=%@", new NSArray(new Object[] {
			EOTypeEtat.ETAT_VALIDE
	}));
	public static final EOQualifier QUAL_CODE_ANALYTIQUE_RACINES = new EOKeyValueQualifier(EOCodeAnalytique.CAN_NIVEAU_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(0));
	public static final EOQualifier QUAL_CODE_ANALYTIQUE_PUBLIC = new EOKeyValueQualifier(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_OUI);
	public static final EOQualifier QUAL_CODE_ANALYTIQUE_PRIVE = new EOKeyValueQualifier(EOCodeAnalytique.TYPE_ETAT_PUBLIC_KEY + "." + EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_NON);

	public static String EXCEPTION_DELETE_A_ENFANTS = "Impossible de supprimer un objet qui a des enfants dans l'arborescence.";
	public static String EXCEPTION_DELETE_A_ORGANS = "Impossible de supprimer le code analytique car des branches de l'organigramme lui sont affectées.";

	public static final String LONG_STRING_KEY = "longString";
	private static final Map DISPLAY_NAMES_MAP = new HashMap();
	static {
		DISPLAY_NAMES_MAP.put(CAN_CODE_KEY, "Code");
		DISPLAY_NAMES_MAP.put(CAN_LIBELLE_KEY, "Libellé");
		DISPLAY_NAMES_MAP.put(CAN_MONTANT_KEY, "Montant associé");
	}

	public EOCodeAnalytique() {
		super();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForInsert() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForInsert();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForUpdate() throws NSValidation.ValidationException {
		this.validateObjectMetier();
		validateBeforeTransactionSave();
		super.validateForUpdate();
	}

	/**
	 * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateForDelete() throws NSValidation.ValidationException {
		super.validateForDelete();
	}

	/**
	 * Peut etre appele à partir des factories. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 * 
	 * @throws NSValidation.ValidationException
	 */
	public void validateObjectMetier() throws NSValidation.ValidationException {
		checkContraintesObligatoires();
		checkContraintesLongueursMax();

		//Si code privé il faut qu'il y ait au moins une ligne associée
		if (!isPublic()) {
			if (canNiveau().intValue() == 1) {
				throw new NSValidation.ValidationException("Vous ne pouvez pas créer de code analytique privé à la racine de l'arbre, veuillez sélectionner un code public père.");
			}
		}

		if (!isPublic()) {
			if (getOrgans().count() == 0) {
				throw new NSValidation.ValidationException("Vous devez obligatoirement associer au moins une ligne budgétaire au code. Si le code ne doit plus être utilisé, indiquez-le comme \"non utilisable\"");
			}
		}

		if (canOuverture() != null && canFermeture() != null) {
			if (canFermeture().before(canOuverture())) {
				throw new NSValidation.ValidationException("La date de fermeture ne doit pas être antérieure à la date d'ouverture.");
			}
		}

		if (typeEtatDepassement() == null) {
			setTypeEtatDepassementRelationship(EOTypeEtat.getTypeEtat(editingContext(), EOTypeEtat.ETAT_NE_RIEN_FAIRE));
		}

		super.validateObjectMetier();

	}

	/**
	 * A appeler par les validateforsave, forinsert, forupdate. Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode.
	 * AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
	 */
	public void validateBeforeTransactionSave() throws NSValidation.ValidationException {

		//super.validateBeforeTransactionSave();
	}

	public NSArray codeAnalytiqueFilsValide() {
		return EOQualifier.filteredArrayWithQualifier(super.codeAnalytiqueFils(), EOCodeAnalytique.QUAL_CODE_ANALYTIQUE_VALIDE);
		//        return super.codeAnalytiqueFils();
	}

	public final String getLongString() {
		String res = MyStringCtrl.ifEmpty(canCode(), "");
		int i = canNiveau().intValue();
		if (i > 1) {
			res = (codeAnalytiquePere() != null ? codeAnalytiquePere().getLongString() : "") + " / " + res;
		}
		return res;
	}

	/**
	 * @return Les lignes budgétaires affectées au code analytique.
	 */
	public NSArray getOrgans() {
		return (NSArray) valueForKeyPath(EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY + "." + EOCodeAnalytiqueOrgan.ORGAN_KEY);
	}

	/**
	 * @param exercice
	 * @return Les lignes budgétaires affectées au code analytique et ouvertes sur un exercice particulier
	 */
	public NSArray getOrgans(EOExercice exercice) {
		return EOQualifier.filteredArrayWithQualifier(getOrgans(), EOOrgan.qualOrganOuvertes(exercice));
	}

	public boolean isPublic() {
		return (this.typeEtatPublic() != null && EOTypeEtat.ETAT_OUI.equals(this.typeEtatPublic().tyetLibelle()));
	}

	/**
	 * @param editingContext
	 * @return Les typeEtat possibles pour le champ canMontantDepassement.
	 */
	public static NSArray getTypeEtatsForCanMontantDepassement(EOEditingContext editingContext) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_ID_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(13)));
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_ID_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(14)));
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_ID_KEY, EOQualifier.QualifierOperatorEqual, Integer.valueOf(15)));
		NSArray res = EOTypeEtat.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
				EOTypeEtat.SORT_TYET_LIBELLE_ASC
		}));
		return res;
	}

	public static NSArray getTypeEtatsForCanUtilisable(EOEditingContext editingContext) {
		NSMutableArray quals = new NSMutableArray();
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_OUI));
		quals.addObject(new EOKeyValueQualifier(EOTypeEtat.TYET_LIBELLE_KEY, EOQualifier.QualifierOperatorEqual, EOTypeEtat.ETAT_NON));
		NSArray res = EOTypeEtat.fetchAll(editingContext, new EOOrQualifier(quals), new NSArray(new Object[] {
				EOTypeEtat.SORT_TYET_LIBELLE_ASC
		}));
		return res;
	}

	/**
	 * @param edc
	 * @return Les codes analytiques valides situes a la racine de l'arbre.
	 */
	public static NSArray getCodeAnalytiquesRacine(EOEditingContext edc) {
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				EOCodeAnalytique.QUAL_CODE_ANALYTIQUE_RACINES, EOCodeAnalytique.QUAL_CODE_ANALYTIQUE_VALIDE
		}));
		return fetchAll(edc, qual, new NSArray(new Object[] {
				SORT_CAN_CODE_ASC
		}));
	}

	/**
	 * @param ed
	 * @param codeAnalytiquePere
	 * @return Une instance preremplie en fonction d'un code pere.
	 */
	public static EOCodeAnalytique creerInstanceForParent(EOEditingContext ed, EOCodeAnalytique codeAnalytiquePere) {
		EOCodeAnalytique obj = creerInstance(ed);
		Integer niveau = new Integer(0);
		niveau = new Integer(codeAnalytiquePere.canNiveau().intValue() + 1);
		obj.setTypeEtatRelationship(EOTypeEtat.getTypeEtat(ed, EOTypeEtat.ETAT_VALIDE));
		obj.setCodeAnalytiquePereRelationship(codeAnalytiquePere);
		obj.setCanOuverture(new NSTimestamp(MyDateCtrl.getDateOnly(MyDateCtrl.now())));
		obj.setCanNiveau(niveau);
		obj.setTypeEtatUtilisableRelationship(EOTypeEtat.getTypeEtat(ed, EOTypeEtat.ETAT_OUI));
		return obj;
	}

	public static EOQualifier qualCanalOuverts(int exeExercice) {
	    NSMutableArray quals = new NSMutableArray();
        final Date dateDebut = MyDateCtrl.getFirstDayOfYear(exeExercice);
        final Date dateFin = MyDateCtrl.addDHMS(MyDateCtrl.getLastDayOfYear(exeExercice), 1, 0, 0, 0);

        quals.addObject(new EOOrQualifier(new NSArray(new Object[] {
                new EOKeyValueQualifier(EOCodeAnalytique.CAN_OUVERTURE_KEY, EOQualifier.QualifierOperatorLessThan, dateFin),
                new EOKeyValueQualifier(EOCodeAnalytique.CAN_OUVERTURE_KEY, EOQualifier.QualifierOperatorEqual, null)
        })));
        quals.addObject(new EOOrQualifier(new NSArray(new Object[] {
                new EOKeyValueQualifier(EOCodeAnalytique.CAN_FERMETURE_KEY, EOQualifier.QualifierOperatorGreaterThanOrEqualTo, dateDebut),
                new EOKeyValueQualifier(EOCodeAnalytique.CAN_FERMETURE_KEY, EOQualifier.QualifierOperatorEqual, null)
        })));
        return new EOAndQualifier(quals);
	}
	
	/**
	 * @param exercice
	 * @return Un qualifier pour selectionner les codes analytiques ouverts sur l'exercice (sur totalité ou partie de l'exercice).
	 */
	public static EOQualifier qualCanalOuverts(EOExercice exercice) {
		return qualCanalOuverts(exercice.exeExercice().intValue());
	}

	public Map displayNames() {
		return DISPLAY_NAMES_MAP;
	}

	public static void supprimer(EOEditingContext edc, EOCodeAnalytique canal) throws NSValidation.ValidationException {
		if (canal.codeAnalytiqueFilsValide().count() > 0) {
			throw new NSValidation.ValidationException(EOCodeAnalytique.EXCEPTION_DELETE_A_ENFANTS);
		}

		for (int i = canal.codeAnalytiqueOrgans().count() - 1; i >= 0; i--) {
			EOCodeAnalytiqueOrgan canorgan = (EOCodeAnalytiqueOrgan) canal.codeAnalytiqueOrgans().objectAtIndex(i);
			canal.removeFromCodeAnalytiqueOrgansRelationship(canorgan);
			edc.deleteObject(canorgan);
		}
		edc.deleteObject(canal);

	}

	public static NSArray fetchAllCanalValides(EOEditingContext ec) {
	    NSMutableArray sortOrderings = new NSMutableArray();
	    sortOrderings.addObject(SORT_CAN_CODE_ASC);
	    sortOrderings.addObject(SORT_CAN_NIVEAU_ASC);
	    return fetchAll(ec, QUAL_CODE_ANALYTIQUE_VALIDE, sortOrderings);
	}
	
	public static NSArray fetchAllCanalPublicsEtPrivesValides(EOEditingContext ec, int exo, EOStructure structure) {
	    NSMutableArray sortOrderings = new NSMutableArray();
	    sortOrderings.addObject(SORT_CAN_CODE_ASC);
	    sortOrderings.addObject(SORT_CAN_NIVEAU_ASC);
	    // 1er fetch des canal publics
        NSMutableArray quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PUBLIC);
        quals.add(qualCanalOuverts(exo));
        quals.add(QUAL_CODE_ANALYTIQUE_VALIDE);
	    NSArray canalsPubs = fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
	    // 2ème fetch des canal privés avec un organ.structure correspondant à celle passée
	    String keyPath = 
            EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY + "." + EOCodeAnalytiqueOrgan.ORGAN_KEY + "." + EOOrgan.STRUCTURE_KEY;
        EOQualifier qual = new EOKeyValueQualifier(keyPath, EOKeyValueQualifier.QualifierOperatorEqual, structure);
        quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PRIVE);
        quals.addObject(qual);
        quals.addObject(qualCanalOuverts(exo));
        quals.add(QUAL_CODE_ANALYTIQUE_VALIDE);
        EOAndQualifier andQual = new EOAndQualifier(quals);
	    NSArray canalsOrgan = fetchAll(ec, andQual, sortOrderings);
	    return canalsPubs.mutableClone().arrayByAddingObjectsFromArray(canalsOrgan).mutableClone();
	}
	
	public static NSArray fetchAllCanalPublicsEtPrivesValides(EOEditingContext ec, int exo, EOStructure structure, EOOrgan organ) {
	    NSMutableArray sortOrderings = new NSMutableArray();
	    sortOrderings.addObject(SORT_CAN_CODE_ASC);
	    sortOrderings.addObject(SORT_CAN_NIVEAU_ASC);
	    // 1er fetch des canal publics
        NSMutableArray quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PUBLIC);
        quals.add(qualCanalOuverts(exo));
        quals.add(QUAL_CODE_ANALYTIQUE_VALIDE);
	    NSArray canalsPubs = fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
	    // 2ème fetch des canal privés avec un organ.structure correspondant à celle passée
	    String keyPath = 
            EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY + "." + EOCodeAnalytiqueOrgan.ORGAN_KEY + "." + EOOrgan.STRUCTURE_KEY;
        EOQualifier qual = new EOKeyValueQualifier(keyPath, EOKeyValueQualifier.QualifierOperatorEqual, structure);
        quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PRIVE);
        quals.addObject(qual);
        quals.addObject(qualCanalOuverts(exo));
        quals.add(QUAL_CODE_ANALYTIQUE_VALIDE);
        if (organ != null) {
        	EOQualifier qualifierOrgan = ERXQ.equals(EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY, organ);
        	quals.add(qualifierOrgan);
        }
        EOAndQualifier andQual = new EOAndQualifier(quals);
	    NSArray canalsOrgan = fetchAll(ec, andQual, sortOrderings);
	    return canalsPubs.mutableClone().arrayByAddingObjectsFromArray(canalsOrgan).mutableClone();
	}
	
	public static NSArray fetchAllCanalPublicsEtPrives(EOEditingContext ec, int exo, EOStructure structure) {
	    NSMutableArray sortOrderings = new NSMutableArray();
	    sortOrderings.addObject(SORT_CAN_CODE_ASC);
	    sortOrderings.addObject(SORT_CAN_NIVEAU_ASC);
	    // 1er fetch des canal publics
        NSMutableArray quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PUBLIC);
        quals.add(qualCanalOuverts(exo));
	    NSArray canalsPubs = fetchAll(ec, new EOAndQualifier(quals), sortOrderings);
	    // 2ème fetch des canal privés avec un organ.structure correspondant à celle passée
	    String keyPath = 
            EOCodeAnalytique.CODE_ANALYTIQUE_ORGANS_KEY + "." + EOCodeAnalytiqueOrgan.ORGAN_KEY + "." + EOOrgan.STRUCTURE_KEY;
        EOQualifier qual = new EOKeyValueQualifier(keyPath, EOKeyValueQualifier.QualifierOperatorEqual, structure);
        quals = new NSMutableArray(QUAL_CODE_ANALYTIQUE_PRIVE);
        quals.addObject(qual);
        quals.addObject(qualCanalOuverts(exo));
        EOAndQualifier andQual = new EOAndQualifier(quals);
	    NSArray canalsOrgan = fetchAll(ec, andQual, sortOrderings);
	    return canalsPubs.mutableClone().arrayByAddingObjectsFromArray(canalsOrgan).mutableClone();
	}
	
	/**
	 * Affecte le montant. Si le montant est null, on reinitialise le typeEtatDepassement a ""NE RIEN FAIRE".
	 */
	public void setCanMontant(BigDecimal value) {
		super.setCanMontant(value);
		if (value == null) {
			setTypeEtatDepassementRelationship(EOTypeEtat.fetchByKeyValue(editingContext(), EOTypeEtat.TYET_LIBELLE_KEY, EOTypeEtat.ETAT_NE_RIEN_FAIRE));
		}
	}

}
