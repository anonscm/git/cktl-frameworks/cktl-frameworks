/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOExercice.java instead.
package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOExercice extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_Exercice";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.EXERCICE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "exeOrdre";

	public static final String EXE_CLOTURE_KEY = "exeCloture";
	public static final String EXE_EXERCICE_KEY = "exeExercice";
	public static final String EXE_INVENTAIRE_KEY = "exeInventaire";
	public static final String EXE_OUVERTURE_KEY = "exeOuverture";
	public static final String EXE_STAT_KEY = "exeStat";
	public static final String EXE_STAT_ENG_KEY = "exeStatEng";
	public static final String EXE_STAT_FAC_KEY = "exeStatFac";
	public static final String EXE_STAT_LIQ_KEY = "exeStatLiq";
	public static final String EXE_STAT_REC_KEY = "exeStatRec";
	public static final String EXE_TYPE_KEY = "exeType";

// Attributs non visibles
	public static final String EXE_ORDRE_KEY = "exeOrdre";

//Colonnes dans la base de donnees
	public static final String EXE_CLOTURE_COLKEY = "EXE_CLOTURE";
	public static final String EXE_EXERCICE_COLKEY = "EXE_EXERCICE";
	public static final String EXE_INVENTAIRE_COLKEY = "EXE_INVENTAIRE";
	public static final String EXE_OUVERTURE_COLKEY = "EXE_OUVERTURE";
	public static final String EXE_STAT_COLKEY = "EXE_STAT";
	public static final String EXE_STAT_ENG_COLKEY = "EXE_STAT_ENG";
	public static final String EXE_STAT_FAC_COLKEY = "EXE_STAT_FAC";
	public static final String EXE_STAT_LIQ_COLKEY = "EXE_STAT_LIQ";
	public static final String EXE_STAT_REC_COLKEY = "EXE_STAT_REC";
	public static final String EXE_TYPE_COLKEY = "EXE_TYPE";

	public static final String EXE_ORDRE_COLKEY = "EXE_ORDRE";


	// Relationships
	public static final String FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY = "fwkJefyAdminOrganExercices";
	public static final String TYPE_CREDITS_KEY = "typeCredits";



	// Accessors methods
  public NSTimestamp exeCloture() {
    return (NSTimestamp) storedValueForKey(EXE_CLOTURE_KEY);
  }

  public void setExeCloture(NSTimestamp value) {
    takeStoredValueForKey(value, EXE_CLOTURE_KEY);
  }

  public Long exeExercice() {
    return (Long) storedValueForKey(EXE_EXERCICE_KEY);
  }

  public void setExeExercice(Long value) {
    takeStoredValueForKey(value, EXE_EXERCICE_KEY);
  }

  public NSTimestamp exeInventaire() {
    return (NSTimestamp) storedValueForKey(EXE_INVENTAIRE_KEY);
  }

  public void setExeInventaire(NSTimestamp value) {
    takeStoredValueForKey(value, EXE_INVENTAIRE_KEY);
  }

  public NSTimestamp exeOuverture() {
    return (NSTimestamp) storedValueForKey(EXE_OUVERTURE_KEY);
  }

  public void setExeOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, EXE_OUVERTURE_KEY);
  }

  public String exeStat() {
    return (String) storedValueForKey(EXE_STAT_KEY);
  }

  public void setExeStat(String value) {
    takeStoredValueForKey(value, EXE_STAT_KEY);
  }

  public String exeStatEng() {
    return (String) storedValueForKey(EXE_STAT_ENG_KEY);
  }

  public void setExeStatEng(String value) {
    takeStoredValueForKey(value, EXE_STAT_ENG_KEY);
  }

  public String exeStatFac() {
    return (String) storedValueForKey(EXE_STAT_FAC_KEY);
  }

  public void setExeStatFac(String value) {
    takeStoredValueForKey(value, EXE_STAT_FAC_KEY);
  }

  public String exeStatLiq() {
    return (String) storedValueForKey(EXE_STAT_LIQ_KEY);
  }

  public void setExeStatLiq(String value) {
    takeStoredValueForKey(value, EXE_STAT_LIQ_KEY);
  }

  public String exeStatRec() {
    return (String) storedValueForKey(EXE_STAT_REC_KEY);
  }

  public void setExeStatRec(String value) {
    takeStoredValueForKey(value, EXE_STAT_REC_KEY);
  }

  public String exeType() {
    return (String) storedValueForKey(EXE_TYPE_KEY);
  }

  public void setExeType(String value) {
    takeStoredValueForKey(value, EXE_TYPE_KEY);
  }

  public NSArray fwkJefyAdminOrganExercices() {
    return (NSArray)storedValueForKey(FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY);
  }

  public NSArray fwkJefyAdminOrganExercices(EOQualifier qualifier) {
    return fwkJefyAdminOrganExercices(qualifier, null, false);
  }

  public NSArray fwkJefyAdminOrganExercices(EOQualifier qualifier, boolean fetch) {
    return fwkJefyAdminOrganExercices(qualifier, null, fetch);
  }

  public NSArray fwkJefyAdminOrganExercices(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = fwkJefyAdminOrganExercices();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToFwkJefyAdminOrganExercicesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice object) {
    addObjectToBothSidesOfRelationshipWithKey(object, FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY);
  }

  public void removeFromFwkJefyAdminOrganExercicesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice createFwkJefyAdminOrganExercicesRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_OrganExercice");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice) eo;
  }

  public void deleteFwkJefyAdminOrganExercicesRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, FWK_JEFY_ADMIN_ORGAN_EXERCICES_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllFwkJefyAdminOrganExercicesRelationships() {
    Enumeration objects = fwkJefyAdminOrganExercices().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteFwkJefyAdminOrganExercicesRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOOrganExercice)objects.nextElement());
    }
  }

  public NSArray typeCredits() {
    return (NSArray)storedValueForKey(TYPE_CREDITS_KEY);
  }

  public NSArray typeCredits(EOQualifier qualifier) {
    return typeCredits(qualifier, null, false);
  }

  public NSArray typeCredits(EOQualifier qualifier, boolean fetch) {
    return typeCredits(qualifier, null, fetch);
  }

  public NSArray typeCredits(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit.EXERCICE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = typeCredits();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToTypeCreditsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit object) {
    addObjectToBothSidesOfRelationshipWithKey(object, TYPE_CREDITS_KEY);
  }

  public void removeFromTypeCreditsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDITS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit createTypeCreditsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_TypeCredit");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, TYPE_CREDITS_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit) eo;
  }

  public void deleteTypeCreditsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, TYPE_CREDITS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllTypeCreditsRelationships() {
    Enumeration objects = typeCredits().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteTypeCreditsRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOTypeCredit)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOExercice avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOExercice createEOExercice(EOEditingContext editingContext, Long exeExercice
, String exeStat
, String exeStatEng
, String exeStatFac
, String exeStatLiq
, String exeStatRec
, String exeType
			) {
    EOExercice eo = (EOExercice) createAndInsertInstance(editingContext, _EOExercice.ENTITY_NAME);    
		eo.setExeExercice(exeExercice);
		eo.setExeStat(exeStat);
		eo.setExeStatEng(exeStatEng);
		eo.setExeStatFac(exeStatFac);
		eo.setExeStatLiq(exeStatLiq);
		eo.setExeStatRec(exeStatRec);
		eo.setExeType(exeType);
    return eo;
  }

  
	  public EOExercice localInstanceIn(EOEditingContext editingContext) {
	  		return (EOExercice)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOExercice creerInstance(EOEditingContext editingContext) {
	  		EOExercice object = (EOExercice)createAndInsertInstance(editingContext, _EOExercice.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOExercice localInstanceIn(EOEditingContext editingContext, EOExercice eo) {
    EOExercice localInstance = (eo == null) ? null : (EOExercice)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOExercice#localInstanceIn a la place.
   */
	public static EOExercice localInstanceOf(EOEditingContext editingContext, EOExercice eo) {
		return EOExercice.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOExercice fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOExercice fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOExercice)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOExercice fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOExercice eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOExercice)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOExercice fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOExercice eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOExercice ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOExercice fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
