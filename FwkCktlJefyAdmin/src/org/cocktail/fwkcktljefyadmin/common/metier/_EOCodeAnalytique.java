/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeAnalytique.java instead.
package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOCodeAnalytique extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_CodeAnalytique";
	public static final String ENTITY_TABLE_NAME = "jefy_admin.CODE_ANALYTIQUE";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String CAN_CODE_KEY = "canCode";
	public static final String CAN_FERMETURE_KEY = "canFermeture";
	public static final String CAN_LIBELLE_KEY = "canLibelle";
	public static final String CAN_MONTANT_KEY = "canMontant";
	public static final String CAN_NIVEAU_KEY = "canNiveau";
	public static final String CAN_OUVERTURE_KEY = "canOuverture";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";
	public static final String CAN_ID_PERE_KEY = "canIdPere";
	public static final String CAN_MONTANT_DEPASSEMENT_KEY = "canMontantDepassement";
	public static final String CAN_PUBLIC_KEY = "canPublic";
	public static final String CAN_UTILISABLE_KEY = "canUtilisable";
	public static final String TYET_ID_KEY = "tyetId";
	public static final String UTL_ORDRE_KEY = "utlOrdre";

//Colonnes dans la base de donnees
	public static final String CAN_CODE_COLKEY = "CAN_CODE";
	public static final String CAN_FERMETURE_COLKEY = "CAN_FERMETURE";
	public static final String CAN_LIBELLE_COLKEY = "CAN_LIBELLE";
	public static final String CAN_MONTANT_COLKEY = "CAN_MONTANT";
	public static final String CAN_NIVEAU_COLKEY = "CAN_NIVEAU";
	public static final String CAN_OUVERTURE_COLKEY = "CAN_OUVERTURE";

	public static final String CAN_ID_COLKEY = "CAN_ID";
	public static final String CAN_ID_PERE_COLKEY = "CAN_ID_PERE";
	public static final String CAN_MONTANT_DEPASSEMENT_COLKEY = "CAN_MONTANT_DEPASSEMENT";
	public static final String CAN_PUBLIC_COLKEY = "CAN_PUBLIC";
	public static final String CAN_UTILISABLE_COLKEY = "CAN_UTILISABLE";
	public static final String TYET_ID_COLKEY = "TYET_ID";
	public static final String UTL_ORDRE_COLKEY = "UTL_ORDRE";


	// Relationships
	public static final String CODE_ANALYTIQUE_FILS_KEY = "codeAnalytiqueFils";
	public static final String CODE_ANALYTIQUE_ORGANS_KEY = "codeAnalytiqueOrgans";
	public static final String CODE_ANALYTIQUE_PERE_KEY = "codeAnalytiquePere";
	public static final String TYPE_ETAT_KEY = "typeEtat";
	public static final String TYPE_ETAT_DEPASSEMENT_KEY = "typeEtatDepassement";
	public static final String TYPE_ETAT_PUBLIC_KEY = "typeEtatPublic";
	public static final String TYPE_ETAT_UTILISABLE_KEY = "typeEtatUtilisable";
	public static final String UTILISATEUR_KEY = "utilisateur";



	// Accessors methods
  public String canCode() {
    return (String) storedValueForKey(CAN_CODE_KEY);
  }

  public void setCanCode(String value) {
    takeStoredValueForKey(value, CAN_CODE_KEY);
  }

  public NSTimestamp canFermeture() {
    return (NSTimestamp) storedValueForKey(CAN_FERMETURE_KEY);
  }

  public void setCanFermeture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_FERMETURE_KEY);
  }

  public String canLibelle() {
    return (String) storedValueForKey(CAN_LIBELLE_KEY);
  }

  public void setCanLibelle(String value) {
    takeStoredValueForKey(value, CAN_LIBELLE_KEY);
  }

  public java.math.BigDecimal canMontant() {
    return (java.math.BigDecimal) storedValueForKey(CAN_MONTANT_KEY);
  }

  public void setCanMontant(java.math.BigDecimal value) {
    takeStoredValueForKey(value, CAN_MONTANT_KEY);
  }

  public Integer canNiveau() {
    return (Integer) storedValueForKey(CAN_NIVEAU_KEY);
  }

  public void setCanNiveau(Integer value) {
    takeStoredValueForKey(value, CAN_NIVEAU_KEY);
  }

  public NSTimestamp canOuverture() {
    return (NSTimestamp) storedValueForKey(CAN_OUVERTURE_KEY);
  }

  public void setCanOuverture(NSTimestamp value) {
    takeStoredValueForKey(value, CAN_OUVERTURE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique codeAnalytiquePere() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique)storedValueForKey(CODE_ANALYTIQUE_PERE_KEY);
  }

  public void setCodeAnalytiquePereRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique oldValue = codeAnalytiquePere();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, CODE_ANALYTIQUE_PERE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, CODE_ANALYTIQUE_PERE_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_KEY);
  }

  public void setTypeEtatRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtat();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatDepassement() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_DEPASSEMENT_KEY);
  }

  public void setTypeEtatDepassementRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtatDepassement();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_DEPASSEMENT_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_DEPASSEMENT_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatPublic() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_PUBLIC_KEY);
  }

  public void setTypeEtatPublicRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtatPublic();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_PUBLIC_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_PUBLIC_KEY);
    }
  }
  
  public org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatUtilisable() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat)storedValueForKey(TYPE_ETAT_UTILISABLE_KEY);
  }

  public void setTypeEtatUtilisableRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat oldValue = typeEtatUtilisable();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TYPE_ETAT_UTILISABLE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TYPE_ETAT_UTILISABLE_KEY);
    }
  }
  
  public org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur utilisateur() {
    return (org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur)storedValueForKey(UTILISATEUR_KEY);
  }

  public void setUtilisateurRelationship(org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur value) {
    if (value == null) {
    	org.cocktail.fwkcktldroitsutils.common.metier.EOUtilisateur oldValue = utilisateur();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, UTILISATEUR_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, UTILISATEUR_KEY);
    }
  }
  
  public NSArray codeAnalytiqueFils() {
    return (NSArray)storedValueForKey(CODE_ANALYTIQUE_FILS_KEY);
  }

  public NSArray codeAnalytiqueFils(EOQualifier qualifier) {
    return codeAnalytiqueFils(qualifier, null, false);
  }

  public NSArray codeAnalytiqueFils(EOQualifier qualifier, boolean fetch) {
    return codeAnalytiqueFils(qualifier, null, fetch);
  }

  public NSArray codeAnalytiqueFils(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique.CODE_ANALYTIQUE_PERE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeAnalytiqueFils();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
  }

  public void removeFromCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique createCodeAnalytiqueFilsRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_CodeAnalytique");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_FILS_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique) eo;
  }

  public void deleteCodeAnalytiqueFilsRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_FILS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeAnalytiqueFilsRelationships() {
    Enumeration objects = codeAnalytiqueFils().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeAnalytiqueFilsRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique)objects.nextElement());
    }
  }

  public NSArray codeAnalytiqueOrgans() {
    return (NSArray)storedValueForKey(CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public NSArray codeAnalytiqueOrgans(EOQualifier qualifier) {
    return codeAnalytiqueOrgans(qualifier, null, false);
  }

  public NSArray codeAnalytiqueOrgans(EOQualifier qualifier, boolean fetch) {
    return codeAnalytiqueOrgans(qualifier, null, fetch);
  }

  public NSArray codeAnalytiqueOrgans(EOQualifier qualifier, NSArray sortOrderings, boolean fetch) {
    NSArray results;
    if (fetch) {
      EOQualifier fullQualifier;
      EOQualifier inverseQualifier = new EOKeyValueQualifier(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan.CODE_ANALYTIQUE_KEY, EOQualifier.QualifierOperatorEqual, this);
    	
      if (qualifier == null) {
        fullQualifier = inverseQualifier;
      }
      else {
        NSMutableArray qualifiers = new NSMutableArray();
        qualifiers.addObject(qualifier);
        qualifiers.addObject(inverseQualifier);
        fullQualifier = new EOAndQualifier(qualifiers);
      }

      results = org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan.fetchAll(editingContext(), fullQualifier, sortOrderings);
    }
    else {
      results = codeAnalytiqueOrgans();
      if (qualifier != null) {
        results = (NSArray)EOQualifier.filteredArrayWithQualifier(results, qualifier);
      }
      if (sortOrderings != null) {
        results = (NSArray)EOSortOrdering.sortedArrayUsingKeyOrderArray(results, sortOrderings);
      }
    }
    return results;
  }
  
  public void addToCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan object) {
    addObjectToBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public void removeFromCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan createCodeAnalytiqueOrgansRelationship() {
    EOClassDescription eoClassDesc = EOClassDescription.classDescriptionForEntityName("FwkJefyAdmin_CodeAnalytiqueOrgan");
    EOEnterpriseObject eo = eoClassDesc.createInstanceWithEditingContext(editingContext(), null);
    editingContext().insertObject(eo);
    addObjectToBothSidesOfRelationshipWithKey(eo, CODE_ANALYTIQUE_ORGANS_KEY);
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan) eo;
  }

  public void deleteCodeAnalytiqueOrgansRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan object) {
    removeObjectFromBothSidesOfRelationshipWithKey(object, CODE_ANALYTIQUE_ORGANS_KEY);
    editingContext().deleteObject(object);
  }

  public void deleteAllCodeAnalytiqueOrgansRelationships() {
    Enumeration objects = codeAnalytiqueOrgans().immutableClone().objectEnumerator();
    while (objects.hasMoreElements()) {
      deleteCodeAnalytiqueOrgansRelationship((org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytiqueOrgan)objects.nextElement());
    }
  }


/**
 * Créer une instance de EOCodeAnalytique avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeAnalytique createEOCodeAnalytique(EOEditingContext editingContext, String canCode
, String canLibelle
, Integer canNiveau
, NSTimestamp canOuverture
, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtat, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatDepassement, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatPublic, org.cocktail.fwkcktljefyadmin.common.metier.EOTypeEtat typeEtatUtilisable			) {
    EOCodeAnalytique eo = (EOCodeAnalytique) createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);    
		eo.setCanCode(canCode);
		eo.setCanLibelle(canLibelle);
		eo.setCanNiveau(canNiveau);
		eo.setCanOuverture(canOuverture);
    eo.setTypeEtatRelationship(typeEtat);
    eo.setTypeEtatDepassementRelationship(typeEtatDepassement);
    eo.setTypeEtatPublicRelationship(typeEtatPublic);
    eo.setTypeEtatUtilisableRelationship(typeEtatUtilisable);
    return eo;
  }

  
	  public EOCodeAnalytique localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeAnalytique)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytique creerInstance(EOEditingContext editingContext) {
	  		EOCodeAnalytique object = (EOCodeAnalytique)createAndInsertInstance(editingContext, _EOCodeAnalytique.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOCodeAnalytique localInstanceIn(EOEditingContext editingContext, EOCodeAnalytique eo) {
    EOCodeAnalytique localInstance = (eo == null) ? null : (EOCodeAnalytique)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeAnalytique#localInstanceIn a la place.
   */
	public static EOCodeAnalytique localInstanceOf(EOEditingContext editingContext, EOCodeAnalytique eo) {
		return EOCodeAnalytique.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnalytique fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnalytique fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnalytique fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnalytique eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnalytique)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnalytique fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnalytique eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnalytique ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnalytique fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
