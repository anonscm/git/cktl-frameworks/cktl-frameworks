/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOKeyValueQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSValidation;

public class EOLolfNomenclatureDepense extends _EOLolfNomenclatureDepense {

    public EOLolfNomenclatureDepense() {
        super();
    }

    
    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForInsert() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForInsert();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForUpdate() throws NSValidation.ValidationException {
        this.validateObjectMetier();
        validateBeforeTransactionSave();
        super.validateForUpdate();
    }

    /**
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateForDelete() throws NSValidation.ValidationException {
        super.validateForDelete();
    }



    /**
     * Peut etre appele à partir des factories.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     * @throws NSValidation.ValidationException
     */
    public void validateObjectMetier() throws NSValidation.ValidationException {
    	
    	super.validateObjectMetier();
    }
    
    /**
     * A appeler par les validateforsave, forinsert, forupdate.
     * Vous pouvez définir un delegate qui sera appelé lors de l'execution de cette methode. AfwkPersRecord#registerValidationDelegate(IValidationDelegate).
     *
     */
    public void validateBeforeTransactionSave() throws NSValidation.ValidationException {
    	
        super.validateBeforeTransactionSave();
    }

	public static NSArray getLolfNomenclaturesRacine(EOEditingContext edc) {
		EOQualifier qual = new EOAndQualifier(new NSArray(new Object[] {
				EOLolfNomenclatureDepense.QUAL_VALIDE, QUAL_LOLF_NOMENCLATURE_RACINES
		}));
		return fetchAll(edc, qual, new NSArray(new Object[] {
				SORT_LOLF_CODE_ASC
		}));
	}

	public static NSArray fetchAllNomenclaturesLevel2or3(EOEditingContext ec, int exo) {
	    // Niveau = 2 ou 3
	    EOQualifier qual = new EOKeyValueQualifier(LOLF_NIVEAU_KEY, EOKeyValueQualifier.QualifierOperatorEqual, Integer.valueOf(2));
	    EOQualifier qual2 = new EOKeyValueQualifier(LOLF_NIVEAU_KEY, EOKeyValueQualifier.QualifierOperatorEqual, Integer.valueOf(3));
	    NSMutableArray quals = new NSMutableArray(qual);
	    quals.addObject(qual2);
	    EOOrQualifier or = new EOOrQualifier(quals);
	    // et correspondant à l'exo
	    quals = new NSMutableArray(or);
	    quals.addObject(getQualifierForExercice(exo));
	    // ordonnage par ordre affichage
	    return fetchAll(ec, new EOAndQualifier(quals), new NSArray(SORT_LOLF_ORDRE_AFFICHAGE_ASC));
	}
	
}
