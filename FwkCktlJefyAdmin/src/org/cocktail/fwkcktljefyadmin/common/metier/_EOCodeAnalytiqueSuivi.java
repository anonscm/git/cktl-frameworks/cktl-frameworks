/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

// DO NOT EDIT.  Make changes to EOCodeAnalytiqueSuivi.java instead.
package org.cocktail.fwkcktljefyadmin.common.metier;

import com.webobjects.foundation.*;
import com.webobjects.eocontrol.*;
import java.math.BigDecimal;
import java.util.Enumeration;
import java.util.NoSuchElementException;



public abstract class _EOCodeAnalytiqueSuivi extends  AfwkJefyAdminRecord {
	public static final String ENTITY_NAME = "FwkJefyAdmin_CodeAnalytiqueSuivi";
	public static final String ENTITY_TABLE_NAME = "jefy_depense.v_code_Analytique_suivi";



	// Attributes

	public static final String ENTITY_PRIMARY_KEY = "canId";

	public static final String MONTANT_BUDGETAIRE_KEY = "montantBudgetaire";

// Attributs non visibles
	public static final String CAN_ID_KEY = "canId";

//Colonnes dans la base de donnees
	public static final String MONTANT_BUDGETAIRE_COLKEY = "MONTANT_BUDGETAIRE";

	public static final String CAN_ID_COLKEY = "CAN_ID";


	// Relationships
	public static final String TO_CODE_ANALYTIQUE_KEY = "toCodeAnalytique";



	// Accessors methods
  public java.math.BigDecimal montantBudgetaire() {
    return (java.math.BigDecimal) storedValueForKey(MONTANT_BUDGETAIRE_KEY);
  }

  public void setMontantBudgetaire(java.math.BigDecimal value) {
    takeStoredValueForKey(value, MONTANT_BUDGETAIRE_KEY);
  }

  public org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique toCodeAnalytique() {
    return (org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique)storedValueForKey(TO_CODE_ANALYTIQUE_KEY);
  }

  public void setToCodeAnalytiqueRelationship(org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique value) {
    if (value == null) {
    	org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique oldValue = toCodeAnalytique();
    	if (oldValue != null) {
    		removeObjectFromBothSidesOfRelationshipWithKey(oldValue, TO_CODE_ANALYTIQUE_KEY);
      }
    } else {
    	addObjectToBothSidesOfRelationshipWithKey(value, TO_CODE_ANALYTIQUE_KEY);
    }
  }
  

/**
 * Créer une instance de EOCodeAnalytiqueSuivi avec les champs et relations obligatoires et l'insere dans l'editingContext.
 */
  public static  EOCodeAnalytiqueSuivi createEOCodeAnalytiqueSuivi(EOEditingContext editingContext, java.math.BigDecimal montantBudgetaire
, org.cocktail.fwkcktljefyadmin.common.metier.EOCodeAnalytique toCodeAnalytique			) {
    EOCodeAnalytiqueSuivi eo = (EOCodeAnalytiqueSuivi) createAndInsertInstance(editingContext, _EOCodeAnalytiqueSuivi.ENTITY_NAME);    
		eo.setMontantBudgetaire(montantBudgetaire);
    eo.setToCodeAnalytiqueRelationship(toCodeAnalytique);
    return eo;
  }

  
	  public EOCodeAnalytiqueSuivi localInstanceIn(EOEditingContext editingContext) {
	  		return (EOCodeAnalytiqueSuivi)localInstanceOfObject(editingContext, this);
	  }
	

	/**
	 * Cree une instance de l'objet et l'insere dans l'editing context.
	 * @param editingContext
	 * 
	 * @return L'objet insere dans l'editing context.
	 */
	  public static EOCodeAnalytiqueSuivi creerInstance(EOEditingContext editingContext) {
	  		EOCodeAnalytiqueSuivi object = (EOCodeAnalytiqueSuivi)createAndInsertInstance(editingContext, _EOCodeAnalytiqueSuivi.ENTITY_NAME);
	  		return object;
		}


	
	
  
  public static EOCodeAnalytiqueSuivi localInstanceIn(EOEditingContext editingContext, EOCodeAnalytiqueSuivi eo) {
    EOCodeAnalytiqueSuivi localInstance = (eo == null) ? null : (EOCodeAnalytiqueSuivi)localInstanceOfObject(editingContext, eo);
    if (localInstance == null && eo != null) {
      throw new IllegalStateException("You attempted to localInstance " + eo + ", which has not yet committed.");
    }
    return localInstance;
  }

  /**
   * 
   * @param editingContext
   * @param eo
   * @return L'objet eo dans l'editingContext
   * @deprecated Utilisez EOCodeAnalytiqueSuivi#localInstanceIn a la place.
   */
	public static EOCodeAnalytiqueSuivi localInstanceOf(EOEditingContext editingContext, EOCodeAnalytiqueSuivi eo) {
		return EOCodeAnalytiqueSuivi.localInstanceIn(editingContext, eo);
	}
  
	
	
	
	
	
	
	/* Finders */

	  public static NSArray fetchAll(EOEditingContext editingContext) {
	    return fetchAll(editingContext, (EOQualifier)null);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, NSArray sortOrderings) {
	    return fetchAll(editingContext, null, sortOrderings);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier) {
		  return fetchAll(editingContext, qualifier, null, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
		return fetchAll(editingContext, qualifier, sortOrderings, false);
	  }

	  public static NSArray fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings) {
		return fetchAll(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value), sortOrderings, false);
	  }
	  
	  public static NSArray fetchAll(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings, boolean distinct) {
	    EOFetchSpecification fetchSpec = new EOFetchSpecification(ENTITY_NAME, qualifier, sortOrderings);
	    fetchSpec.setIsDeep(true);
	    fetchSpec.setUsesDistinct(distinct);
	    NSArray eoObjects = (NSArray)editingContext.objectsWithFetchSpecification(fetchSpec);
	    return eoObjects;
	  }

		/**
		* Renvoie un objet simple. Pour recuperer un tableau, utilisez fetchAll(EOEditingContext editingContext, String keyName, Object value, NSArray sortOrderings).
		* Une exception est declenchee si plusieurs objets sont trouves.
		* 
		* @return Renvoie l'objet correspondant a la paire cle/valeur
		* @throws IllegalStateException  
		*/
	  public static EOCodeAnalytiqueSuivi fetchByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
	    return fetchByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	  }

	  
	  /**
	   * Renvoie l'objet correspondant au qualifier. Si plusieurs objets sont susceptibles d'etre trouves, utilisez fetchFirstByQualifier().
	   * Une exception est declenchee si plusieurs objets sont trouves.
	   * 
	 * @param editingContext
	 * @param qualifier
	 * @return L'objet qui correspond au qualifier passé en parametre. Si plusieurs objets sont trouve, une Exception est declenchee. Si aucun objet est trouve, null est renvoye.
	 * @throws IllegalStateException
	 */
	public static EOCodeAnalytiqueSuivi fetchByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, null);
	    EOCodeAnalytiqueSuivi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else if (count == 1) {
	      eoObject = (EOCodeAnalytiqueSuivi)eoObjects.objectAtIndex(0);
	    }
	    else {
	      throw new IllegalStateException("Il y a plus d'un objet qui correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }

	  
	  
	  
	  public static EOCodeAnalytiqueSuivi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
	   	 return fetchFirstByQualifier(editingContext, qualifier, null);
	  }
	  
	  public static EOCodeAnalytiqueSuivi fetchFirstByQualifier(EOEditingContext editingContext, EOQualifier qualifier, NSArray sortOrderings) {
	    NSArray eoObjects = fetchAll(editingContext, qualifier, sortOrderings);
	    EOCodeAnalytiqueSuivi eoObject;
	    int count = eoObjects.count();
	    if (count == 0) {
	      eoObject = null;
	    }
	    else {
	      eoObject = (EOCodeAnalytiqueSuivi)eoObjects.objectAtIndex(0);
	    }
	    return eoObject;
	  }  

	  
	  /**
	   * Une exception est declenchee si aucun objet est trouve.
	   * 
	   * @param editingContext
	   * @param qualifier Le filtre
	   * @return L'objet correspondant au qualifier. Si l'objet n'est pas trouvé, une exception est declenchee. Pour ne pas avoir d'exception, utilisez fetchFirstByQualifier().
	   * @throws NoSuchElementException si aucun objet est trouve
	   */
	  public static EOCodeAnalytiqueSuivi fetchFirstRequiredByQualifier(EOEditingContext editingContext, EOQualifier qualifier) {
		  EOCodeAnalytiqueSuivi eoObject = fetchFirstByQualifier(editingContext, qualifier);
	    if (eoObject == null) {
	      throw new NoSuchElementException("Aucun objet EOCodeAnalytiqueSuivi ne correspond au qualifier '" + qualifier + "'.");
	    }
	    return eoObject;
	  }	
	

	public static EOCodeAnalytiqueSuivi fetchRequiredByKeyValue(EOEditingContext editingContext, String keyName, Object value) {
		    return fetchFirstRequiredByQualifier(editingContext, new EOKeyValueQualifier(keyName, EOQualifier.QualifierOperatorEqual, value));
	}
	  	
	
	
	
  
}
