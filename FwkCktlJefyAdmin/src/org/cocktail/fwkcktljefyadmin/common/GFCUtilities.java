/*
 * Copyright COCKTAIL (www.cocktail.org), 1995, 2011 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */

package org.cocktail.fwkcktljefyadmin.common;

import org.cocktail.fwkcktljefyadmin.common.finder.FinderDevise;
import org.cocktail.fwkcktljefyadmin.common.metier.EODevise;
import org.cocktail.fwkcktljefyadmin.common.metier.EOExercice;

import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.foundation.NSNumberFormatter;

/**
 * @author Rodolphe PRIN <rodolphe.prin at cocktail.org>
 */

public abstract class GFCUtilities {
	public static final String DEFAULT_INT_PATTERN = "# ##0;0;-# ##0";

	/**
	 * @param edc
	 * @param exeOrdre
	 * @return Un formatter a utiliser initialise en fonction de la devise active par defaut.
	 */
	public static NSNumberFormatter getDefaultDeviseFormatter(EOEditingContext edc, Integer exeOrdre) {
		NSNumberFormatter monnaieFormatter = new NSNumberFormatter();
		monnaieFormatter.setDecimalSeparator(",");
		monnaieFormatter.setThousandSeparator(" ");
		monnaieFormatter.setHasThousandSeparators(true);
		EODevise devise = FinderDevise.getDeviseEnCours(edc, EOExercice.getExercice(edc, exeOrdre));
		int nbDecimales = EODevise.defaultNbDecimales;
		if (devise != null) {
			nbDecimales = devise.devNbDecimales().intValue();
		}
		if (nbDecimales == 0) {
			monnaieFormatter.setPattern(DEFAULT_INT_PATTERN);
		}
		else {
			String str = "# ##0.";
			String str1 = ";0,";
			String str2 = ";-# ##0.";

			for (int i = 0; i < nbDecimales; i++) {
				str += "0";
				str1 += "0";
				str2 += "0";
			}
			monnaieFormatter.setPattern(str + str1 + str2);
		}
		return monnaieFormatter;
	}

	/**
	 * @param edc
	 * @return L'exercice comptable ouvert.
	 */
	public static EOExercice getExerciceOuvert(EOEditingContext edc) {
		return EOExercice.getExerciceOuvert(edc);
	}

}
