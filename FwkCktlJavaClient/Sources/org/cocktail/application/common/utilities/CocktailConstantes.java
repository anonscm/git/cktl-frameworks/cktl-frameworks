/*******************************************************************************
 *  Copyright CONSORTIUM COCKTAIL (www.cocktail.org), 1995, 2008
 *  
 *  This software is governed by the CeCILL license under French law and abiding
 *  by the rules of distribution of free software. You can use, modify and/or
 *  redistribute the software under the terms of the CeCILL license as circulated
 *  by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 *  
 *  As a counterpart to the access to the source code and rights to copy, modify
 *  and redistribute granted by the license, users are provided only with a
 *  limited warranty and the software's author, the holder of the economic
 *  rights, and the successive licensors have only limited liability.
 *  
 *  In this respect, the user's attention is drawn to the risks associated with
 *  loading, using, modifying and/or developing or reproducing the software by
 *  the user in light of its specific status of free software, that may mean that
 *  it is complicated to manipulate, and that also therefore means that it is
 *  reserved for developers and experienced professionals having in-depth
 *  computer knowledge. Users are therefore encouraged to load and test the
 *  software's suitability as regards their requirements in conditions enabling
 *  the security of their systems and/or data to be ensured and, more generally,
 *  to use and operate it in the same conditions as regards security.
 *  
 *  The fact that you are presently reading this means that you have had
 *  knowledge of the CeCILL license and that you accept its terms.
 * 
 *******************************************************************************/
package org.cocktail.application.common.utilities;

import java.awt.Color;

import javax.swing.ImageIcon;


public class CocktailConstantes {
	
	public static final String[] LISTE_MOIS = new String[]{"JANVIER","FEVRIER","MARS","AVRIL","MAI","JUIN",
		"JUILLET","AOUT","SEPTEMBRE","OCTOBRE","NOVEMBRE","DECEMBRE"};

	public static final String STRING_EURO = " \u20ac";

	public final static String DEFAULT_DATE_DEBUT_1900 = "01/01/1900";
	
	// COULEURS

	public static final String SAUT_DE_LIGNE = "\n";
	public static final String SEPARATEUR_TABULATION = "\t";
	public static final String SEPARATEUR_EXPORT = ";";
	
	public static final String EXTENSION_CSV = ".csv";
	public static final String EXTENSION_PDF = ".pdf";
	public static final String EXTENSION_EXCEL = ".xls";
	public static final String EXTENSION_JASPER = ".jasper";
	public static final String EXTENSION_XML = ".xml";

    public static final Color COLOR_REMUN = new Color(255, 207, 213);
    public static final Color COLOR_SALARIAL = new Color(218,221,255);
    public static final Color COLOR_PATRONAL = new Color(205,188,255);

	public static Color BG_COLOR_WHITE = new Color(255,255,255);
	public static Color BG_COLOR_BLACK = new Color(0,0,0);
	public static Color BG_COLOR_RED = new Color(255,0,0);
	public static Color BG_COLOR_GREEN = new Color(0,255,0);
	public static Color BG_COLOR_BLUE = new Color(0,0,255);
	public static Color BG_COLOR_YELLOW = new Color(255,255,0);
	public static Color BG_COLOR_LIGHT_GREY = new Color(100,100,100);
	public static Color BG_COLOR_CYAN = new Color(224, 255, 255);

	public static Color COULEUR_FOND_INACTIF = new Color(222,222,222);
	public static Color COULEUR_FOND_ACTIF = new Color(255, 255, 255);
	 
	public static final Color COLOR_INACTIVE_BACKGROUND = new Color(222,222,222);
	public static final Color COLOR_ACTIVE_BACKGROUND = new Color(0,0,0);

    public static final Color COLOR_BKG_TABLE_VIEW = new Color(230, 230, 230);
    public static final Color COLOR_SELECTED_ROW = new Color(127,155,165);

    public static final Color COLOR_SELECT_NOMENCLATURES = new Color(100,100,100);
    public static final Color COLOR_FOND_NOMENCLATURES = new Color(220,220,220);
    public static final Color COLOR_FILTRES_NOMENCLATURES = new Color(240,240,240);

    // Booleans
    /** valeur vraie d'un champ de type booleen */
    public final static String VRAI = "O";
    /** valeur fausse d'un champ de type booleen */
    public final static String FAUX = "N";

}