/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.common.utilities;

import com.webobjects.eocontrol.EOAndQualifier;
import com.webobjects.eocontrol.EOOrQualifier;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import com.webobjects.foundation.NSTimestamp;
import com.webobjects.foundation.NSValidation;

public class CocktailFinder {

	private NSArray<EOSortOrdering> sortArrayDateDebut;
	private EOSortOrdering 			sortDateDebut;
	
	private NSArray<EOSortOrdering> sortArrayDateDebutDesc;
	private EOSortOrdering 			sortDateDebutDesc;

	public static final String DATE_DEBUT_KEY = "dateDebut";
	public static final String DATE_FIN_KEY = "dateFin";

	protected CocktailFinder() {
	}
	
	/**
	 * @return tri ascendant selon la date de debut
	 */
	public EOSortOrdering getSortDateDebut() {
		if (sortDateDebut == null) {
			sortDateDebut = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareAscending);
		}
		return sortDateDebut;
	}
	/**
	 * @return tri ascendant selon la date de debut
	 */
	public EOSortOrdering getSortDateDebutDesc() {
		if (sortDateDebutDesc == null) {
			sortDateDebutDesc = new EOSortOrdering(DATE_DEBUT_KEY, EOSortOrdering.CompareDescending);
		}
		return sortDateDebutDesc;
	}

	/**
	 * @return tri ascendant selon la date de debut
	 */
	public NSArray<EOSortOrdering> getSortArrayDateDebut() {
		if (sortArrayDateDebut == null) {
			sortArrayDateDebut = new NSArray<EOSortOrdering>(getSortDateDebut());
		}
		return sortArrayDateDebut;
	}
	/**
	 * @return tri ascendant selon la date de debut
	 */
	public NSArray<EOSortOrdering> getSortArrayDateDebutDesc() {
		if (sortArrayDateDebutDesc == null) {
			sortArrayDateDebutDesc = new NSArray<EOSortOrdering>(getSortDateDebutDesc());
		}
		return sortArrayDateDebutDesc;
	}
	
	
	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierNullValue(String key) {
		return  EOQualifier.qualifierWithQualifierFormat(key + " = nil", null);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierNotNull(String key) {
		return  EOQualifier.qualifierWithQualifierFormat(key + " != nil", null);
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierEqual(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + "=%@", new NSArray<Object>(value));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierNotEqual(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + "!=%@", new NSArray<Object>(value));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierAfterEq(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + ">=%@", new NSArray<Object>(value));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierAfter(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + ">%@", new NSArray<Object>(value));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierBeforeEq(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + "<=%@", new NSArray<Object>(value));
	}

	/**
	 * 
	 * @param value
	 * @return
	 */
	public static EOQualifier getQualifierBefore(String key, Object value) {
		if (value == null)
			throw new NSValidation.ValidationException("ERREUR");
		return  EOQualifier.qualifierWithQualifierFormat(key + "<%@", new NSArray<Object>(value));
	}
	
	/** Retourne le qualifier pour determiner les objets valides pour une periode
	 * @param champDateDebut	nom du champ de date debut sur lequel construire le qualifier
	 * @param debutPeriode	date de debut de p&eacute;riode (ne peut pas etre nulle)
	 * @param champDateFin	nom du champ de date fin sur lequel construire le qualifier
	 * @param finPeriode	date de fin de periode
	 * @return qualifier construit ou null si date debut est nulle
	 */
	public static EOQualifier getQualifierForPeriode(String champDateDebut, NSTimestamp debutPeriode, String champDateFin, NSTimestamp finPeriode) {
		
		if (debutPeriode == null) {
			return null;
		}
		
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		
		orQualifiers.addObject(getQualifierNullValue(champDateFin));
		orQualifiers.addObject(getQualifierAfterEq(champDateFin, debutPeriode));
		
		andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		
		if (finPeriode != null) {

			orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(getQualifierNullValue(champDateDebut));
			orQualifiers.addObject(getQualifierBeforeEq(champDateDebut, finPeriode));

			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		
		return new EOAndQualifier(andQualifiers);
	}
	
	/** 
	 * Retourne le qualifier pour determiner les objets valides jusqu'à la date de référence
	 * @param champDateDebut : nom du champ de date debut sur lequel construire le qualifier
	 * @param dateReference : date de référence (ne peut pas etre nulle)
	 * @return qualifier
	 */
	public static EOQualifier getQualifierAnterieurADate(String champDateDebut, NSTimestamp dateReference) {
		NSMutableArray<EOQualifier> andQualifiers = new NSMutableArray<EOQualifier>();
		NSMutableArray<EOQualifier> orQualifiers = new NSMutableArray<EOQualifier>();
		
		if (dateReference != null) {
			orQualifiers = new NSMutableArray<EOQualifier>();
			orQualifiers.addObject(getQualifierNullValue(champDateDebut));
			orQualifiers.addObject(getQualifierBeforeEq(champDateDebut, dateReference));

			andQualifiers.addObject(new EOOrQualifier(orQualifiers));
		}
		
		return new EOAndQualifier(andQualifiers);
	}

}
