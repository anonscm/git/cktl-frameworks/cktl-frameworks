package org.cocktail.application.palette;

import javax.swing.JRadioButton;

import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.foundation.NSNotificationCenter;

public class ButtonMatrixCocktail {
	private static String MATRIX_STATE_DID_CHANGE = "BUTTON_MATRIX_CKTL_STATE_DID_CHANGE";
	public EOMatrix buttonMatrix;

	public ButtonMatrixCocktail() {
		// TODO Auto-generated constructor stub
	}
	
	public ButtonMatrixCocktail(int rows, int columns, int rowSpacing, int columnSpacing) {
		buttonMatrix = new EOMatrix(rows, columns, rowSpacing, columnSpacing);
	}

	public JRadioButton getSelected() {
		int nbComponents = buttonMatrix.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			if (((JRadioButton) buttonMatrix.getComponent(i)).isSelected()) {
				return (JRadioButton) buttonMatrix.getComponent(i);
			}
		}
		return (JRadioButton) null;
	}

	public int getSelectedIndex() {
		int nbComponents = buttonMatrix.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			if (((JRadioButton) buttonMatrix.getComponent(i)).isSelected()) {
				return i;
			}
		}
		return -1;
	}

	public JRadioButton getButtonAtIndex(int index) {
		return (JRadioButton) buttonMatrix.getComponent(index);
	}

	public void selectButtonAtIndex(int index) {
		buttonMatrix.invalidate();
		JRadioButton component = (JRadioButton) buttonMatrix.getComponent(index);
		component.invalidate();
		component.setSelected(true);
		component.validate();
		buttonMatrix.validate();
	}

	public EOMatrix getMatrix() {
		return buttonMatrix;
	}

	public int getComponentCount() {
		return buttonMatrix.getComponentCount();
	}

	public void selectionChanged(Object sender) {
		NSNotificationCenter.defaultCenter().postNotification(MATRIX_STATE_DID_CHANGE, this);
	}

	public void setEnabled(boolean aBool) {
		buttonMatrix.setEnabled(aBool);
		int nbComponents = buttonMatrix.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			((JRadioButton) buttonMatrix.getComponent(i)).setEnabled(aBool);
		}

	}

	public JRadioButton getButtonWithText(String text) {
		for (int i = 0; i < buttonMatrix.getComponentCount(); i++) {
			JRadioButton aButton = getButtonAtIndex(i);
			if (aButton.getText().equals(text)) {
				return (JRadioButton) buttonMatrix.getComponent(i);
			}
		}
		return (JRadioButton) null;
	}

	public void selectButtonWithText(String text) {
		for (int i = 0; i < buttonMatrix.getComponentCount(); i++) {
			JRadioButton aButton = getButtonAtIndex(i);
			if (aButton.getText().equals(text)) {
				buttonMatrix.invalidate();
				JRadioButton component = (JRadioButton) buttonMatrix.getComponent(i);
				component.invalidate();
				component.setSelected(true);
				component.validate();
				buttonMatrix.validate();
			}
		}
	}

}
