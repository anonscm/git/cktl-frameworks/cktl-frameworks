/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.palette;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;

import javax.swing.JComboBox;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.palette.JPanelCocktail;

public class CalendarPanel extends JPanelCocktail implements ItemListener, ChangeListener {
		
		public static int MIN_YEAR = 1900;
		public static int MAX_YEAR = 2100;
		
		JComboBox month;
		JComboBox year;
        public static final int FIRE_DISABLED = 0;
		public static final int FIRE_EVERYTIME = 1;
		public static final int FIRE_DAYCHANGES = 2;
	
		private int _listenermode = FIRE_EVERYTIME;
		private Calendar _cal;
		private DateFormat _format;
		private Locale _locale;
		private JComboBox _month;
		private JComboBox _year;
		private PanelMois _monthPanel;
		private ArrayList _changeListener = new ArrayList();
		private boolean _fireingChangeEvent = false;
        
    public CalendarPanel() {
		createGUI(Calendar.getInstance(), Locale.getDefault(), DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault()));
	}

	public CalendarPanel(Calendar cal) {
		createGUI(cal, Locale.getDefault(), DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault()));
	}

	public CalendarPanel(Locale locale) {
		createGUI(Calendar.getInstance(locale), locale, DateFormat.getDateInstance(DateFormat.MEDIUM, locale));
	}

	public CalendarPanel(Calendar cal, Locale locale) {
		createGUI(cal, locale, DateFormat.getDateInstance(DateFormat.MEDIUM, locale));
	}

	public CalendarPanel(Calendar cal, Locale locale, DateFormat dateFormat) {
		createGUI(cal, locale, dateFormat);
	}

	private void createGUI(Calendar cal, Locale locale, DateFormat dateFormat) {
		_locale = locale;
		_cal = Calendar.getInstance(locale);
		_cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		_cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		_cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));
		_format = dateFormat;

		setLayout(new GridBagLayout());

		GridBagConstraints c = new GridBagConstraints();

		_month = createMonth();
		_month.addItemListener(this);
		add(_month, c);

		_year = createYear();
		_year.addItemListener(this);

		c.gridwidth = GridBagConstraints.REMAINDER;

		add(_year, c);

		c.anchor = GridBagConstraints.CENTER;
		c.fill = GridBagConstraints.BOTH;
		c.insets = new Insets(1, 1, 1, 1);
		
		_monthPanel = new PanelMois(_cal, _locale);
		_monthPanel.addChangeListener(this);
		
		add(_monthPanel, c);
	}


	private JComboBox createYear() {
		year = new JComboBox();
		
		for (int i = MIN_YEAR; i <= MAX_YEAR; i++) {
			year.addItem("" + i);
		}
		
		year.setSelectedIndex(_cal.get(Calendar.YEAR) - MIN_YEAR);
		
		return year;
	}

	private JComboBox createMonth() {
		month = new JComboBox();
		SimpleDateFormat format = new SimpleDateFormat("MMMMM", _locale);
		
		GregorianCalendar currentCal = new GregorianCalendar();
		currentCal.setFirstDayOfWeek(Calendar.MONDAY);
                currentCal.set(Calendar.YEAR, _cal.get(Calendar.YEAR));
                
		for (int i = 0; i < 12; i++) {
                        if(i==0)
                            currentCal.set(Calendar.MONTH,0);
                        else
                            currentCal.roll(Calendar.MONTH,true);
                        currentCal.setTime(currentCal.getTime());
                        String myString = format.format(currentCal.getTime());
			month.addItem(myString);
		}
		
		month.setSelectedIndex(_cal.get(Calendar.MONTH));
		
		return month;
	}

	private void updateCalendar() {
		_cal.set(Calendar.MONTH, _month.getSelectedIndex());
		_cal.set(Calendar.YEAR, _year.getSelectedIndex() + 1900);
		_cal.set(Calendar.DAY_OF_MONTH, _monthPanel.getSelectedDayOfMonth());

		_monthPanel.setCalendar(_cal);
	}

	
	public Calendar getCalendar() {
		updateCalendar();
		return _cal;
	}

	public void setCalendar(Calendar cal) {
        System.out.println("cal = "+cal);
		_cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH));
		_cal.set(Calendar.MONTH, cal.get(Calendar.MONTH));
		_cal.set(Calendar.YEAR, cal.get(Calendar.YEAR));

        System.out.println("_cal = "+_cal);
        
        System.out.println("_month = "+_cal.get(Calendar.MONTH));
        System.out.println("_year = "+_cal.get(Calendar.YEAR));
		_month.setSelectedIndex(_cal.get(Calendar.MONTH));
        _year.setSelectedIndex(_cal.get(Calendar.YEAR) - MIN_YEAR);
        System.out.println("_month = "+_month);
        System.out.println("_year = "+_year);
		_monthPanel.setCalendar(_cal);
		_monthPanel.setSelectedDayOfMonth(cal.get(Calendar.DAY_OF_MONTH));
	}

	public String toString() {
		updateCalendar();
		return _format.format(_cal.getTime());
	}

	public String toString(DateFormat format) {
		updateCalendar();
		return format.format(_cal.getTime());
	}


	public void itemStateChanged(ItemEvent e) {
		updateCalendar();
		if (_listenermode == FIRE_EVERYTIME) {
			fireChangeEvent(month);
            fireChangeEvent(year);
		}
	}

	public void stateChanged(ChangeEvent e) {
		updateCalendar();
		fireChangeEvent(this);
	}
	
	public void addChangeListener(ChangeListener listener) {
		_changeListener.add(listener);
	}

	public void removeChangeListener(ChangeListener listener) {
		_changeListener.remove(listener);
	}
	
	public ChangeListener[] getChangeListener() {
		return (ChangeListener[]) _changeListener.toArray();
	}
	
	protected void fireChangeEvent(javax.swing.JComponent who) {
		if (!_fireingChangeEvent) {
			_fireingChangeEvent = true;
			ChangeEvent event = new ChangeEvent(who);

			for (int i = 0; i < _changeListener.size(); i++) {
				((ChangeListener) _changeListener.get(i)).stateChanged(event);
			}

			_fireingChangeEvent = false;
		}
		
	}

	public void setListenerModus(int mode) {
		_listenermode = mode;
	}
    
    protected int listenerModus(){
        return _listenermode;
    }
	
	public void setEnabled(boolean enabled) {
		_month.setEnabled(enabled);
		_year.setEnabled(enabled);
		_monthPanel.setEnabled(enabled);
	}

}