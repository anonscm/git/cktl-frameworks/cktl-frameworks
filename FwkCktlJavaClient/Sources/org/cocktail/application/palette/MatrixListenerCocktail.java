package org.cocktail.application.palette;

import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JToggleButton;

import com.webobjects.eointerface.swing.EOMatrix;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSMutableArray;

public final class MatrixListenerCocktail {

	private MatrixListenerCocktail() {
	}

	/** renvoi le bouton selectionne */
	public static JRadioButton getSelected(EOMatrix laMatrice) {
		int i, nbComponents;
		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			if (((JRadioButton) laMatrice.getComponent(i)).isSelected()) {
				return (JRadioButton) laMatrice.getComponent(i);
			}
		}
		return (JRadioButton) null;
	}

	public static NSMutableArray getSelectedComp(EOMatrix laMatrice, boolean abool) {
		int i, nbComponents;
		NSMutableArray tmp = new NSMutableArray();
		tmp.removeAllObjects();
		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			NSLog.out.appendln(">>>>>LA CLASSE \n" + laMatrice.getComponent(i).getClass());
			if (((JRadioButton) laMatrice.getComponent(i)).isSelected()) {
				tmp.addObject((JRadioButton) laMatrice.getComponent(i));
			}
		}
		return tmp;
	}

	public static void getSelectedComponents(EOMatrix laMatrice) {
	}

	/** renvoi le component selectionne */
	public static JButton getSelectedButton(EOMatrix laMatrice) {
		int i, nbComponents;
		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			if (((JButton) laMatrice.getComponent(i)).isSelected()) {
				return (JButton) laMatrice.getComponent(i);
			}
		}
		return (JButton) null;
	}

	/** renvoi le nombre de boutons radios selectionnes. */
	public static int getSelectedCount(EOMatrix laMatrice) {
		int i, nbComponents, compteur = 0;
		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			if (((JRadioButton) laMatrice.getComponent(i)).isSelected()) {
				compteur++;
			}
		}
		return -1;
	}

	/** renvoi l'index du bouton selectionne (0 ...) */
	public static int getSelectedIndex(EOMatrix laMatrice) {
		int i, nbComponents;

		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			if (((JRadioButton) laMatrice.getComponent(i)).isSelected()) {
				return i;
			}
		}
		return -1;
	}

	/** selectionne tous les composants. */
	public static void selectAll(EOMatrix laMatrice) {
		int nbComponents = laMatrice.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			((JRadioButton) laMatrice.getComponent(i)).setSelected(true);
		}
	}

	/**
	 * formatage de la eomatrix<br>
	 * voir code
	 */
	public static void formateMatrix(EOMatrix matrix) {
		int i;
		int compte = matrix.getComponentCount();
		for (i = 0; i < compte; i++) {
			Object element = matrix.getComponent(i);
			if (element instanceof JToggleButton) {
				((JToggleButton) element).setFont(new Font("Helvetica", Font.PLAIN, 10));
				// ((JToggleButton)element).setBorder(null);
				((JToggleButton) element).setMargin(new Insets(0, 0, 0, 0));
			}
			if (element instanceof JButton) {
				((JButton) element).setFont(new Font("Helvetica", Font.PLAIN, 10));
			}
			// ((JButton)element).setBorder(null);
			((JButton) element).setMargin(new Insets(0, 0, 0, 0));
		}
	}

	/** deselectionne tous les composants. */
	public static void selectNone(EOMatrix laMatrice) {
		int nbComponents = laMatrice.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			((JRadioButton) laMatrice.getComponent(i)).setSelected(false);
		}
	}

	/** deselectionne tous les composants. */
	public static void selectNone(EOMatrix laMatrice, boolean aBool) {
		int nbComponents = laMatrice.getComponentCount();
		for (int i = 0; i < nbComponents; i++) {
			((JRadioButton) laMatrice.getComponent(i)).setSelected(false);
		}
	}

	/** retourne le label du bouton radio selectionne */
	public static String getSelectedText(EOMatrix matrix) {
		int selIndex = getSelectedIndex(matrix);
		JRadioButton bt = getButtonAtIndex(selIndex, matrix);
		String text = "";
		try {
			text = bt.getText();
		} catch (Exception exp) {
			NSLog.out.appendln(exp.getMessage());
		}
		return text;
	}

	/** renvoi le bouton a l'index donne en parametre */
	public static JRadioButton getButtonAtIndex(int index, EOMatrix laMatrice) {
		return (JRadioButton) laMatrice.getComponent(index);
	}

	/** desactive le bouton a l'index donne */
	public static void setDisabledIndex(int index, EOMatrix laMatrice) {
		getButtonAtIndex(index, laMatrice).setEnabled(false);
	}

	/** @deprecated */
	public static void disableIndex(int index, EOMatrix laMatrice) {
		getButtonAtIndex(index, laMatrice).setEnabled(false);
	}

	public static void talk(String msg) {
		System.out.println("MSG:" + msg);
	}

	/** selectionne le bouton a l'index donnee en parametre */
	public static void selectButtonAtIndex(int index, EOMatrix laMatrice) {
		JRadioButton component;
		laMatrice.invalidate();
		component = (JRadioButton) laMatrice.getComponent(index);
		component.invalidate();
		component.setSelected(true);
		component.validate();
		laMatrice.validate();
	}

	public static void selectButtonAtIndex(int index, EOMatrix laMatrice, boolean abool) {
		JRadioButton component;
		laMatrice.invalidate();
		component = (JRadioButton) laMatrice.getComponent(index);
		component.invalidate();
		component.setSelected(true);
		component.validate();
		laMatrice.validate();
	}

	/** selectionne le bouton a l"index donnee */
	public static void setSelectedIndex(int index, EOMatrix laMatrice) {
		selectButtonAtIndex(index, laMatrice);
	}

	/** active ou desactive toute la matrice */
	public static void setEnabled(boolean aBool, EOMatrix laMatrice) {
		int i, nbComponents;
		laMatrice.setEnabled(aBool);

		nbComponents = laMatrice.getComponentCount();
		for (i = 0; i < nbComponents; i++) {
			((JRadioButton) laMatrice.getComponent(i)).setEnabled(aBool);
		}
	}

	/** place le listener passe en parametre sur tous les boutons de la EOMatrix */
	public static void setListener(ActionListener listener, EOMatrix matrice) {
		for (int i = 0; i < matrice.getComponentCount(); i++) {
			JRadioButton radioButton = getButtonAtIndex(i, matrice);
			radioButton.addActionListener(listener);
		}
	}

}
