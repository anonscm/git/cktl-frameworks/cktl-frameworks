/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 5 oct. 07
 * author mparadot 
 */
package org.cocktail.application.palette.models;

import java.util.ArrayList;

import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;

import com.webobjects.foundation.NSArray;

public class JTreeSelectionModelCocktail extends DefaultTreeSelectionModel {
    JTreeObjectCocktail delegate;

    public JTreeSelectionModelCocktail(JTreeObjectCocktail delegate) {
        this.delegate = delegate;
    }  
    
    /**
     * Ne prend pas en compte la selection de noeuds interdit a la selection.
     * @see javax.swing.tree.DefaultTreeSelectionModel#addSelectionPaths(javax.swing.tree.TreePath[])
     * @see EOTreeNode#isEnabled()
     */
    public void addSelectionPaths(TreePath[] paths) {
        TreePath[] toAdd = getSelectablePaths(paths);
        if (toAdd.length > 0) super.addSelectionPaths(paths);
    }
    
    /**
     * Ne prend pas en compte la selection de noeuds interdit a la selection.
     * @see javax.swing.tree.DefaultTreeSelectionModel#addSelectionPaths(javax.swing.tree.TreePath[])
     * @see EOTreeNode#isEnabled()
     */
    public void setSelectionPaths(TreePath[] paths) {
        TreePath[] toSet = getSelectablePaths(paths);
        if (toSet.length > 0) super.setSelectionPaths(toSet);
    }
    
    /**
     * @see javax.swing.tree.DefaultTreeSelectionModel#canPathsBeAdded(javax.swing.tree.TreePath[])
     */
    protected boolean canPathsBeAdded(TreePath[] paths) {
        return super.canPathsBeAdded(paths) && getSelectablePaths(paths).length > 0;
    }
    
    /**
     * Filtre des {@link TreePath} selon que les noeuds auxquels ils menent sont autorises a la selection.
     * @param paths Liste de {@link TreePath}.
     * @return Liste des seuls {@link TreePath} qui sont autorises a la selection.
     */
    private TreePath[] getSelectablePaths(final TreePath[] paths) {
        ArrayList toAdd = new ArrayList();
        if (paths != null) {
            for (int i=0; i<paths.length; i++) {
                if (isPathSelectable(paths[i])) toAdd.add(paths[i]);
            }
        }
        return (TreePath[]) toAdd.toArray(new TreePath[] { });
    }


    /**
     * Determine si un noeud particulier est selectionnable.
     * @param object Noeud a tester.
     * @return <code>true</code> si le noeud est selectionnable, <code>false</code> sinon.
     */
    public boolean isNodeSelectable(JTreeNodeCocktail node) {
        if (delegate != null) {
            return delegate.isSelectableObject(node.getUserObject());
        }
        return true;
    }
    /**
     * Determine si un chemin mene a un objet selectionnable.
     * @param path Chemin a tester.
     * @return <code>true</code> si le chemin mene a un objet selectionnable.
     */
    public boolean isPathSelectable(TreePath path) {
        return isNodeSelectable((JTreeNodeCocktail) path.getLastPathComponent());
    }
   
}
