/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 2 oct. 07
 * author mparadot 
 */
package org.cocktail.application.palette.models;
import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;

import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;

public class JTreeModelCocktail implements TreeModel {
    private String root="VIDE";
    private JTreeObjectCocktail responder;
            
    public JTreeModelCocktail(JTreeObjectCocktail resp) {
        responder = resp;
    }
    
    private Object treeNodeForObject(Object obj){
        if(obj==null)
            return null;
        return new JTreeNodeCocktail(obj,responder);

    }

    public void addTreeModelListener(TreeModelListener treemodellistener) {
    }

    public Object getChild(Object obj, int i) {
        if(obj==root && responder.getRootsNode()!=null)
            return treeNodeForObject(responder.getRootsNode().objectAtIndex(i));
        if(obj==null || responder.getChilds(((JTreeNodeCocktail)obj).getUserObject())==null)
            return null;
        return treeNodeForObject(responder.getChilds(((JTreeNodeCocktail)obj).getUserObject()).objectAtIndex(i));
    }

    public int getChildCount(Object obj) {
        if(obj==root)
        {
        	if(responder.getRootsNode()==null)
        		return 0;
        	return responder.getRootsNode().count();
        }
        return responder.getChilds(((JTreeNodeCocktail)obj).getUserObject()).count();
    }

    public int getIndexOfChild(Object obj, Object obj1) {
        if(obj==root)
            return 0;
        if(obj==null || responder.getChilds(((JTreeNodeCocktail)obj).getUserObject())==null)
            return -1;
        System.out.println("stdModel.getIndexOfChild()\tobj = "+obj.getClass().getName());
        return responder.getChilds(((JTreeNodeCocktail)obj).getUserObject()).indexOfIdenticalObject(((JTreeNodeCocktail)obj1).getUserObject());
    }

    public Object getRoot() {
        return root;
    }

    public boolean isLeaf(Object obj) {
        if(obj==root || obj==null)
            return false;
        return getChildCount(obj)==0;
    }

    public void removeTreeModelListener(TreeModelListener treemodellistener) {
    }

    public void valueForPathChanged(TreePath treepath, Object obj) {
    }

    public void setRoot(String root) {
        this.root = root;
    }

}
