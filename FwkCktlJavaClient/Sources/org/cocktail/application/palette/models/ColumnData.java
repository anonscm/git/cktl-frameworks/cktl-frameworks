/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)
package org.cocktail.application.palette.models;

public class ColumnData {

    private String m_title;
    private int m_width;
    private int m_alignment;
    private String m_key;
    private String m_renderer;
    private Class m_class;
    private boolean m_editable;

    public ColumnData(String title, int width, int alignment, String key, String mrenderer) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
        m_key = key;
        m_renderer = mrenderer;
        m_class = null;
        m_editable = false;
    }

    public ColumnData(String title, int width, int alignment, String key, String mrenderer, Class mclass, boolean meditable) {
        m_title = title;
        m_width = width;
        m_alignment = alignment;
        m_key = key;
        m_renderer = mrenderer;
        m_class = mclass;
        System.out.println("meditable = " + meditable);
        m_editable = meditable;
    }

    public int getM_alignment() {
        return m_alignment;
    }

    public void setM_alignment(
            int m_alignment) {
        this.m_alignment = m_alignment;
    }

    public String getM_title() {
        return m_title;
    }

    public void setM_title(
            String m_title) {
        this.m_title = m_title;
    }

    public String getM_key() {
        return m_key;
    }

    public void setM_key(
            String m_key) {
        this.m_key = m_key;
    }

    public int getM_width() {
        return m_width;
    }

    public void setM_width(
            int m_width) {
        this.m_width = m_width;
    }

    public String getM_renderer() {
        return m_renderer;
    }

    public void setM_renderer(String m_renderer) {
        this.m_renderer = m_renderer;
    }

    public Class getM_class() {
        return m_class;
    }

    public void setM_class(Class m_class) {
        this.m_class = m_class;
    }

    public boolean isM_editable() {
        return this.m_editable;
    }

    public void setM_editable(boolean m_editable) {
        this.m_editable = m_editable;
    }
}
