/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//		Auteur : Rivalland Frdric (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.palette;
import com.webobjects.foundation.NSMutableArray;

public abstract class ToolsSwing {


	// accentuation
	static 	public String formaterStringU(String u)
	{
		u.replaceAll("º", "\u00ba");
		u.replaceAll("à", "\u00e0");
		u.replaceAll("ç", "\u00e7");
		u.replaceAll("è", "\u00e8");
		u.replaceAll("@", "\u00e8");
		u.replaceAll("é", "\u00e9");
		u.replaceAll("ê", "\u00ea");
		u.replaceAll("î", "\u00ee");
		u.replaceAll("ô", "\u00f4");
		u.replaceAll("û", "\u00fb");

		return u;
	}

	static 	public String convertirAccent(String chaine)
	{
		NSMutableArray tab = new NSMutableArray();
		tab.addObject("º");		tab.addObject("\u00ba");
		tab.addObject("à");		tab.addObject("\u00e0");
		tab.addObject("ç");		tab.addObject( "\u00e7");
		tab.addObject("è");		tab.addObject( "\u00e8");
		tab.addObject("@");		tab.addObject( "\u00e8");
		tab.addObject("é");		tab.addObject( "\u00e9");
		tab.addObject("ê");		tab.addObject( "\u00ea");
		tab.addObject("î");		tab.addObject( "\u00ee");
		tab.addObject("ô");		tab.addObject( "\u00f4");
		tab.addObject("û");		tab.addObject( "\u00fb");
		
		// Pour chaque accent
		for(int i=0; i<tab.count(); i=i+2)
		{
			// Remplacement de l'accent par son équivalent sans accent dans la chaine de caracteres
			chaine = chaine.replaceAll((String)tab.objectAtIndex(i), (String)tab.objectAtIndex(i+1));
		}

		// Retour du resultat
		return chaine;
	}

}
