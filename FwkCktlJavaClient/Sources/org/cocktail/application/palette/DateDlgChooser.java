/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */

package org.cocktail.application.palette;
 
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.text.SimpleDateFormat;

import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimestamp;


public class DateDlgChooser extends JDialog implements ChangeListener {
	
	private CalendarPanel calendarPanel;
	private JTextField calendarpanelTextField;
	private String strDate="";
	private Object owner;
    private String callback;
    private Object sender;
	private NSTimestamp initDate;
	
	/** Ouvre la fenetre de selection de date avec les parametres suivantes 
	* @param owner : objet sur lequel declencher la methode callback de selection de date
	* @param frame : fenetre mere de la boite de selection de date : JFrame
	* @param sender : un component par rapport auquel positionner le dialog, par exemple le bouton presse
	* @param callback : methode de owner a executer quand une nouvelle date a ete selectionnee
	* @param initDate : date sur laquelle initialiser le dialog par defaut, si initDate==null, on initialise sur la date du jour
	*/
	
	public DateDlgChooser(Object owner,JFrame frame,Object sender,String callback,NSTimestamp initDate) {
        super(frame,true);
        this.owner = owner;
        this.callback = callback;
        this.sender = sender;
		this.initDate = initDate;
		init((Component)sender);
	}

	/** Ouvre la fenetre de selection de date avec les parametres suivantes 
	* @param owner : objet sur lequel declencher la methode callback de selection de date
	* @param dlg : fenetre mere de la boite de selection de date : JDialog
	* @param sender : un component par rapport auquel positionner le dialog, par exemple le bouton presse
	* @param callback : methode de owner a executer quand une nouvelle date a ete selectionnee
	*/
    public DateDlgChooser(Object owner,JDialog dlg,Object sender,String callback,NSTimestamp initDate) {
        super(dlg,true);
        this.owner = owner;
        this.callback = callback;
        this.sender = sender;
		this.initDate = initDate;
		init((Component)sender);
	}

	/** Ouvre la fenetre de selection de date avec les parametres suivantes 
	* @param owner : objet sur lequel declencher la methode callback de selection de date
	* @param frame : fenetre mere de la boite de selection de date : JFrame
	* @param sender : un component par rapport auquel positionner le dialog, par exemple le bouton presse
	* @param callback : methode de owner a executer quand une nouvelle date a ete selectionnee
	*/
    public DateDlgChooser(Object owner,JFrame frame,Object sender,String callback) {
        super(frame,true);
        this.owner = owner;
        this.callback = callback;
        this.sender = sender;
		init((Component)sender);
	}

	/** Ouvre la fenetre de selection de date avec les parametres suivantes 
	* @param owner : objet sur lequel declencher la methode callback de selection de date
	* @param frame : fenetre dlg de la boite de selection de date : JDialog
	* @param sender : un component par rapport auquel positionner le dialog, par exemple le bouton presse
	* @param callback : methode de owner a executer quand une nouvelle date a ete selectionnee
	*/
    public DateDlgChooser(Object owner,JDialog dlg,Object sender,String callback) {
        super(dlg,true);
        this.owner = owner;
        this.callback = callback;
        this.sender = sender;
		init((Component)sender);
	}

    private void init(Component sender) {
        setTitle("Date");
        createGUI();
        setResizable(false);
        pack();
        this.setLocationRelativeTo(sender);
        setVisible(true);
    }
	
	
        private void createGUI() {
            Container container = getContentPane();
            container.setLayout(new BorderLayout(5,5));
            calendarPanel = new CalendarPanel();
            calendarPanel.addChangeListener(this);
            container.add(calendarPanel, BorderLayout.CENTER);
			if(initDate!=null)
				setSelectedDate(initDate);
	}
	
	
	public void setSelectedDate(NSTimestamp date) {
		java.util.GregorianCalendar cal = new java.util.GregorianCalendar();
		cal.setTime((java.util.Date)date);
		cal.setTime(cal.getTime());
		calendarPanel.setCalendar(cal);
	}
	
        public void stateChanged(ChangeEvent e) {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            strDate = df.format(calendarPanel.getCalendar().getTime());
            try {
                if(!e.getSource().getClass().getName().equals("javax.swing.JComboBox")) {
                    NSSelector.invoke(callback, new Class[] { String.class },
                                      owner, new Object[] { strDate } );
                }
            }
            catch(Exception exe) { NSLog.out.appendln("DateDlgChooser : "+exe.getMessage());exe.printStackTrace(); }
            
            if(!e.getSource().getClass().getName().equals("javax.swing.JComboBox"))
                this.hide();
        }

}
