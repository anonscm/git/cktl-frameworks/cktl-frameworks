/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
 //		Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)
package org.cocktail.application.palette;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.RootPaneContainer;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.ModalInterfaceCocktail;

import com.webobjects.eoapplication.EOApplication;


public class JPanelCocktail extends JPanel implements InterfaceJefyAdmin, ModalInterfaceCocktail{
	GlassPane m_glassPane;

	public JPanelCocktail() {
		super();

	}


	public void setDisabledFromPrivileges() {
		this.setEnabled(false);
	}


	public void setEnabledFromPrivileges() {
		this.setEnabled(true);
	}

	public void creerGlassPane(){
//		System.out.println("JPanelCocktail.creerGlassPane()");
		try {
			m_glassPane = new GlassPane();
			m_glassPane.setGlassPane((JFrame)this.getTopLevelAncestor()); 
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void poserGlassPane(){
//		System.out.println("JPanelCocktail.poserGlassPane()");
		try {
			m_glassPane.setDrawing(true); 
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void retirerGlassPane(){
//		System.out.println("JPanelCocktail.retirerGlassPane()");
		try {
			m_glassPane.setDrawing(false); 
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
    
//    public void supprimeGlassPane(){
//        m_glassPane.removeGlassPane();
//    }
    
    public Color getBackground() {
        if(((ApplicationCocktail)EOApplication.sharedApplication()).defaultBackgroundColor()!=null)
            return ((ApplicationCocktail)EOApplication.sharedApplication()).defaultBackgroundColor();
        return super.getBackground();
    }


	class GlassPane extends JComponent implements MouseListener ,AWTEventListener
	{
		//RootPaneContainer interface
		private RootPaneContainer m_rootPane = null;
		/**
		 * Constructor
		 *
		 * @param title
		 * @exception
		 */


		public GlassPane()
		{
			setOpaque( false );
			addMouseListener( new MouseAdapter() {} );
			addKeyListener( new KeyAdapter() {});
			setInputVerifier( new InputVerifier()
			{
				public boolean verify( JComponent anInput )
				{
					return false;
				}
			});
		}


		/**
		 * Set the glassPane
		 */
		public void setGlassPane(RootPaneContainer rootPane){
			//set this as new glass pane
			rootPane.setGlassPane(this);       
			//set opaque to false, i.e. make transparent
			m_glassPane.setOpaque(false);

		}

		/**
		 * remove the glassPane
		 */
		public void removeGlassPane(){
			//set the glass pane visible false
			setVisible(false);
			//reset the previous glass pane
			m_rootPane.setGlassPane(null);
		}


		/**
		 * setDrawing
		 *
		 */
		public void setDrawing(boolean drawing){

			if (drawing)	    	{
				setCursor( Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR) );
				Toolkit.getDefaultToolkit().addAWTEventListener( this, AWTEvent.KEY_EVENT_MASK );
			}
			else{
				setCursor( Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR) );
				Toolkit.getDefaultToolkit().removeAWTEventListener( this );
			}
			//This is important otherwise the glass pane will not be visible
			setVisible(drawing);
			//Call repaint
			repaint();
			this.paintAll(this.getGraphics());
			this.updateUI();
		}


		/**
		 * Handling Paint
		 */
		public void paint(Graphics g){
			super.paint(g);
//			g.setColor(Color.LIGHT_GRAY);
//			g.fillRect(0, 0, this.getWidth(), this.getHeight());//(100, 100, 250, 200, 40, 40);
//			g.setColor(Color.RED); 
//			g.drawString("  Transactions en cours ....", 50,50);

			Graphics2D graphics = (Graphics2D) g;
			graphics.setComposite(AlphaComposite.getInstance(
					AlphaComposite.SRC_OVER, 0.5f));
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
		}


		public void mouseClicked(MouseEvent arg0) {
			
			System.out.println("1");
		}


		public void mouseEntered(MouseEvent arg0) {
			
			System.out.println("2");
		}


		public void mouseExited(MouseEvent arg0) {
			
			System.out.println("3");
		}


		public void mousePressed(MouseEvent arg0) {
			
			System.out.println("4");
		}


		public void mouseReleased(MouseEvent arg0) {
			
			System.out.println("5");
		}


		public void eventDispatched(AWTEvent arg0) {
			

		}
	}


}
