/*
 * Copyright COCKTAIL (www.cocktail.org), 1995-2013 This software 
 * is governed by the CeCILL license under French law and abiding by the
 * rules of distribution of free software. You can use, modify and/or 
 * redistribute the software under the terms of the CeCILL license as 
 * circulated by CEA, CNRS and INRIA at the following URL 
 * "http://www.cecill.info". 
 * As a counterpart to the access to the source code and rights to copy, modify 
 * and redistribute granted by the license, users are provided only with a 
 * limited warranty and the software's author, the holder of the economic 
 * rights, and the successive licensors have only limited liability. In this 
 * respect, the user's attention is drawn to the risks associated with loading,
 * using, modifying and/or developing or reproducing the software by the user 
 * in light of its specific status of free software, that may mean that it
 * is complicated to manipulate, and that also therefore means that it is 
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the 
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally, 
 * to use and operate it in the same conditions as regards security. The
 * fact that you are presently reading this means that you have had knowledge 
 * of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.palette;

import java.awt.BorderLayout;
import java.awt.Component;

import com.webobjects.eointerface.swing.EOView;
import com.webobjects.foundation.NSLog;

/**
 * @author Julien BLANDINEAU <julien.blandineau at asso-cocktail dot fr>
 * 
 */
public class SwapViewCocktail {
	private EOView swapView;
	private BorderLayout componentLayout;

	public SwapViewCocktail(EOView aView) {
		this.swapView = aView;
		this.componentLayout = new BorderLayout();
		this.swapView.removeAll();
		this.swapView.setLayout(this.componentLayout);
	}

	public boolean setContentView(EOView aComponent) {
		if (aComponent == null) {
			NSLog.out.appendln(super.getClass().getName() + ".setContentView() : argument null");
			return false;
		}

		if (this.swapView.getComponentCount() == 0) {
			this.swapView.add(aComponent);
			aComponent.setVisible(true);
			aComponent.validate();
			this.swapView.validate();
			return true;
		}

		Component component0 = this.swapView.getComponent(0);
		if (aComponent == component0) {
			return true;
		}
		
		component0.setVisible(false);
		component0.invalidate();
		this.swapView.removeAll();

		this.swapView.add(aComponent);
		aComponent.setVisible(true);
		aComponent.validate();
		this.swapView.validate();

		return true;
	}

	public EOView getContentView() {
		if (this.swapView.getComponentCount() == 0) {
			return null;
		}
		return ((EOView) this.swapView.getComponent(0));
	}
}
