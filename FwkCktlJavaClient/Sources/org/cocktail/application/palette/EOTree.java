/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 2 oct. 07
 * author mparadot 
 */
package org.cocktail.application.palette;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eointerface.EODisplayGroup;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSKeyValueCoding;
import com.webobjects.foundation.NSKeyValueCodingAdditions;
import javax.swing.JTree;
import javax.swing.tree.DefaultTreeSelectionModel;
import javax.swing.tree.TreePath;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.palette.interfaces.JTreeObjectCocktail;
import org.cocktail.application.palette.models.JTreeModelCocktail;
import org.cocktail.application.palette.models.JTreeNodeCocktail;
import org.cocktail.application.palette.models.JTreeSelectionModelCocktail;
import org.cocktail.application.palette.renderer.JTreeCellRendererCocktail;

public class EOTree extends JTree {
    ApplicationCocktail app = (ApplicationCocktail) EOApplication.sharedApplication();
    JTreeModelCocktail model;
    JTreeSelectionModelCocktail selectModel;
    JTreeCellRendererCocktail cellRenderer;
    
    private EODisplayGroup displayGroup;
    private EOQualifier qualifierRootNode;
    private String keyChild;
    private String keyToString;
    private String keySelectableObject;
    
    public EOTree() {
        super();
    }
    
    public EOTree(JTreeObjectCocktail responder) {
        super();
        initWithResponder(responder);
    }
    
    private void initWithResponder(JTreeObjectCocktail responder){
        model = new JTreeModelCocktail(responder);
        setModel(model);
        selectModel = new JTreeSelectionModelCocktail(responder);
        setSelectionModel(selectModel);
        getSelectionModel().setSelectionMode(DefaultTreeSelectionModel.SINGLE_TREE_SELECTION);
        cellRenderer = new JTreeCellRendererCocktail();
        setCellRenderer(cellRenderer);
        setVisible(true);
        setRootVisible(false);
        setShowsRootHandles(true);
    }
    
    public void setRootLibelle(String str){
        if(str==null)
        {
            model.setRoot("VIDE");
            setRootVisible(false);
        }
        else
        {
            model.setRoot(str);
            setRootVisible(true);
        }
    }
    
    public Object getSelectedObject(){
        TreePath path = this.getSelectionPath();
        return ((JTreeNodeCocktail)path.getLastPathComponent()).getUserObject();
    }

    public

    EODisplayGroup getDisplayGroup() {
        return displayGroup;
    }

    public void setDisplayGroup(EODisplayGroup displayGroup) {
        this.displayGroup = displayGroup;
        initWithResponder(new EOResponder());
    }

    public String getKeyChild() {
        return keyChild;
    }

    public void setKeyChild(String keyChild) {
        this.keyChild = keyChild;
    }

    public String getKeySelectableObject() {
        return keySelectableObject;
    }

    public void setKeySelectableObject(String keySelectableObject) {
        this.keySelectableObject = keySelectableObject;
    }

    public EOQualifier getQualifierRootNode() {
        return qualifierRootNode;
    }

    public void setQualifierRootNode(EOQualifier qualifierRootNode) {
        this.qualifierRootNode = qualifierRootNode;
    }

    public String getKeyToString() {
        return keyToString;
    }

    public void setKeyToString(String keyToString) {
        this.keyToString = keyToString;
    }
    
    class EOResponder implements JTreeObjectCocktail{

        public NSArray getRootsNode() {
            try {
                return app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(getDisplayGroup().dataSource().editingContext(),getDisplayGroup().dataSource().classDescriptionForObjects().entityName(),getQualifierRootNode(), getDisplayGroup().sortOrderings());
            } catch (Exception exception) {
                exception.printStackTrace();
                return new NSArray();
            }
        }

        public NSArray getChilds(Object ob) {
            try {
                return (NSArray)((NSKeyValueCodingAdditions)ob).valueForKeyPath(getKeyChild());
            } catch (Exception exception) {
                exception.printStackTrace();
                return new NSArray();
            }
        }

        public String toString(Object ob) {
            try {
            return (String)((NSKeyValueCodingAdditions)ob).valueForKeyPath(getKeyToString());
            } catch (Exception exception) {
                exception.printStackTrace();
                return "pb getKeyToString : "+exception.getMessage();
            }
        }

        public boolean isSelectableObject(Object ob) {
            try {
            return ((NSKeyValueCodingAdditions)ob).valueForKeyPath(getKeySelectableObject())!=null;
            }
            catch (NSKeyValueCoding.UnknownKeyException ex){
                return true;
            }
            catch (Exception exception) {
                exception.printStackTrace();
                return true;
            }
        }
        
    }
    
}
