/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 24 avr. 07
 * author mparadot 
 */
package org.cocktail.application.palette;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOGenericRecord;

import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSLog;
import com.webobjects.foundation.NSSelector;
import com.webobjects.foundation.NSTimeZone;
import com.webobjects.foundation.NSTimestamp;
import org.cocktail.fwkcktlwebapp.common.util.DateCtrl;
import org.cocktail.application.client.ModalInterfaceCocktail;
import org.cocktail.application.nibctrl.NibCtrl;

public class CalendarCocktail extends SwingFinder implements ChangeListener{
    public static String FORMAT_DATE="dd/MM/yyyy";
    private CalendarPanel calendarPanel;
    private String strDate;
    private boolean isModal;
    private _CalendarCocktail monCalendarCoctail;
    private ApplicationCocktail app;
    public static String METHODE_AFFICHER_FENETRE="afficheFenetre";
    
    public CalendarCocktail() {
    }

    public CalendarCocktail(ApplicationCocktail app,EOInterfaceControllerCocktail interfaceCtrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y,boolean modal) {
        super();
        this.app = app;
        monCalendarCoctail = new _CalendarCocktail(app, alocation_x, alocation_y, ataille_x, ataille_y);
        calendarPanel = new CalendarPanel();
        calendarPanel.addChangeListener(this);
        monCalendarCoctail.creationFenetre(calendarPanel, "Calendrier", interfaceCtrl, modal);
    }
    
    public CalendarCocktail(ApplicationCocktail app,NibCtrl interfaceCtrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y,boolean modal) {
        super();
        this.app = app;
        monCalendarCoctail = new _CalendarCocktail(app, alocation_x, alocation_y, ataille_x, ataille_y);
        calendarPanel = new CalendarPanel();
        calendarPanel.addChangeListener(this);
        monCalendarCoctail.creationFenetre(calendarPanel, "Calendrier", interfaceCtrl, modal);
    }

    public void stateChanged(ChangeEvent e) {
        if(calendarPanel.listenerModus()==CalendarPanel.FIRE_DISABLED)
            return;
        SimpleDateFormat df = new SimpleDateFormat(FORMAT_DATE);
        strDate = df.format(calendarPanel.getCalendar().getTime());
        try {
            if(!e.getSource().getClass().getName().equals("javax.swing.JComboBox")) {
                if(monCalendarCoctail.parentControleurEONib!=null)
                {
                    NSSelector.invoke("swingFinderTerminer", new Class[] { SwingFinder.class },monCalendarCoctail.parentControleurEONib, new Object[] { this } );
                }
                else
                {
                    NSSelector.invoke("swingFinderTerminer", new Class[] { Object.class },monCalendarCoctail.parentControleur, new Object[] { this } );
                }
                                      
            }
        }
        catch(Exception exe) { NSLog.out.appendln("DateDlgChooser : "+exe.getMessage());exe.printStackTrace(); }
        
        if(!e.getSource().getClass().getName().equals("javax.swing.JComboBox"))
        {
            monCalendarCoctail.masquerFenetre();
            app.retirerLesGlassPane();
            if(monCalendarCoctail.parentControleurEONib!=null)
                app.activerLesGlassPane(monCalendarCoctail.parentControleurEONib);
            else
            {
                if(monCalendarCoctail.parentControleur.isModal())
                   app.activerLesGlassPane(monCalendarCoctail.parentControleur.currentNib);
            }
        }
    }
    
    public void relierBouton(JButtonCocktail bt) {
        bt.addDelegateActionListener(this,METHODE_AFFICHER_FENETRE); 
    }
    
    public NSArray getResultat(){
        return new NSArray(strDate);
    }
    
    public NSArray getResultatNSTimestamp(){
        return new NSArray(DateCtrl.stringToDate(strDate, FORMAT_DATE));
    }
    
    public void afficherFenetre() {
        //monCalendarCoctail.afficherFenetre();
        afficherFenetre(null);
    }
    
    public void afficherFenetre(Date dateDebut) {
        monCalendarCoctail.afficherFenetre(dateDebut);
    }

    class _CalendarCocktail extends SwingFinderEOGenericRecord{
        int location_x;
        int location_y;
        int taille_x;
        int taille_y;

        public _CalendarCocktail(ApplicationCocktail ctrl, int alocation_x, int alocation_y, int ataille_x, int ataille_y) {
            super(ctrl, alocation_x, alocation_y, ataille_x, ataille_y);
   
            location_x = alocation_x;
            location_y = alocation_y;
            taille_x = ataille_x;
            taille_y = ataille_y;
            setWithLogs(true);
        }
        
        public void afficherFenetre() {
            afficherFenetre(null);
        }
        
        public void afficherFenetre(Date debut) {
            setLocation_taille(location_x, location_y, taille_x, taille_y);
            getFrameMain().setVisible(true);
            getFrameMain().pack();
            if (isModal()) {
                app.activerLesGlassPane(this.currentNib);
            }
            calendarPanel.setListenerModus(CalendarPanel.FIRE_DISABLED);
            if(debut!=null){
                Calendar cal = Calendar.getInstance();
                cal.setTime(debut);
                calendarPanel.setCalendar(cal);
            }
            calendarPanel.setListenerModus(CalendarPanel.FIRE_EVERYTIME);
        }
        
        public void creationFenetre(CalendarPanel aPanel, String title, EOInterfaceControllerCocktail parentControleurEONib, boolean modal) {
            super.creationFenetre(aPanel, title);
            setNibCtrlLocation(8);
            setModal(modal);
            this.parentControleurEONib = parentControleurEONib;
            bindingAndCustomization();
        }
        
        public void creationFenetre(CalendarPanel aPanel, String title, NibCtrl parentCrl, boolean modal) {
            super.creationFenetre(aPanel, title);
            setNibCtrlLocation(8);
            setModal(modal);
            this.parentControleur = parentCrl;
            bindingAndCustomization();
        }
        
        private void bindingAndCustomization() {
            try {
                if (parentControleurEONib == null)
                    app.addLesPanelsModal(currentNib);
                else
                    app.addLesPanelsModal(parentControleurEONib);
            } catch (Throwable e) {
                e.printStackTrace();
                app.getToolsCocktailLogs().addLogMessage(this, "Exception", e.getMessage(), true);
            }
        }
        
    }
}
