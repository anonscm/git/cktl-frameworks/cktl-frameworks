/*
 * Copyright Cocktail, 2001-2006 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
//Auteur : Rivalland Frederic (frederic.rivalland@univ-paris5.fr)

package org.cocktail.application.nibctrl;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;

import javax.swing.JComponent;
import javax.swing.JFormattedTextField;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;
import javax.swing.text.DateFormatter;
import javax.swing.text.DefaultFormatter;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.palette.JPanelCocktail;

import com.webobjects.foundation.NSTimestamp;

public abstract class NibCtrl {

	private JFrame frameMain = null;
	protected ApplicationCocktail app = null;
	private boolean withLogs=true;
	private boolean modal = false;
	int location_x = 0;
	int location_y = 0;
	int taille_x = 0;
	int taille_y = 0;

	/**
	 * Constantes de positionnement de fenetre
	 */
	public static final int LOCATION_NORTH = 0;
	public static final int LOCATION_SOUTH = 1;
	public static final int LOCATION_EAST = 2;
	public static final int LOCATION_WEST = 3;
	public static final int LOCATION_NORTH_EAST = 4;
	public static final int LOCATION_NORTH_WEST = 5;
	public static final int LOCATION_SOUTH_EAST = 6;
	public static final int LOCATION_SOUTH_WEST = 7;
	public static final int LOCATION_MIDDLE = 8;

	
	// parentControleur
	public NibCtrl parentControleur = null;
	public EOInterfaceControllerCocktail parentControleurEONib =null;
	public JPanelCocktail currentNib = null;
//	public JDialog parentDialog;
//	public JFrame parentFrame;
	
	public NibCtrl(ApplicationCocktail ctrl,int alocation_x , int alocation_y,int ataille_x , int ataille_y) {
		super();
		app = ctrl;
		setLocation_taille(alocation_x,alocation_y,ataille_x,ataille_y);
	}

	protected void setLocation_taille(int alocation_x , int alocation_y,int ataille_x , int ataille_y) {
		location_x = alocation_x;
		location_y = alocation_y;
		taille_x = ataille_x;
		taille_y = ataille_y;	
	}

	public void creationFenetre(JPanelCocktail nib,String title)
	{
		if (getFrameMain() == null )
		{
//			if(parentDialog!=null)
//				setFrameMain(new JDialog((JDialog)parentDialog,isModal()));
//			else
//				setFrameMain(new JDialog((JFrame)parentFrame,isModal()));
			setFrameMain(new JFrame());
			getFrameMain().getContentPane().add(nib);
            getFrameMain().setBackground(nib.getBackground());
			getFrameMain().setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
			getFrameMain().setSize(new java.awt.Dimension(taille_x,taille_y));
			getFrameMain().pack();
			getFrameMain().setLocation(location_x,location_y);
		}
		getFrameMain().setTitle(title);
		nib.creerGlassPane();
		currentNib=nib;
	}

	public void afficherFenetre()
	{
		setLocation_taille(location_x,location_y,taille_x,taille_y);
		getFrameMain().setVisible(true);
		getFrameMain().pack();
	}

//	public void setNibCtrlLocation(int x, int y)
//	{
//		getFrameMain().setLocation(x,y);
//		getFrameMain().pack();
//		location_x = x;
//		location_y = y;
//	}

	public void setNibCtrlLocation(int location)
	{
		
//		Dimensions de l'ecran
		int w = (int) getFrameMain().getGraphicsConfiguration().getBounds().getWidth();
		int h = (int) getFrameMain().getGraphicsConfiguration().getBounds().getHeight();

//		coordonnees utiles
		int xMiddle = (w/2) - ((int) getFrameMain().getSize().getWidth() / 2);
		int yMiddle = (h/2) - ((int) getFrameMain().getSize().getHeight() / 2);
		int xRight = w - (int) getFrameMain().getSize().getWidth();
		int yBottom = h - (int) getFrameMain().getSize().getHeight();

		int x, y;
		switch (location) {
		case LOCATION_MIDDLE: 
			x = xMiddle; y = yMiddle; 
			break;
		case LOCATION_NORTH: 
			x = xMiddle; y = 0; 
			break;
		case LOCATION_SOUTH: 
			x = xMiddle; y = yBottom; 
			break;

		case LOCATION_NORTH_EAST: 
			x = xRight; y = 0; 
			break;
		case LOCATION_EAST: 
			x = xRight; y = yMiddle; 
			break;
		case LOCATION_SOUTH_EAST:
			x = xRight; y = yBottom; 
			break;

		case LOCATION_NORTH_WEST:
			x = 0; y = 0; 
			break;
		case LOCATION_WEST: 
			x = 0; y = yMiddle; 
			break;
		case LOCATION_SOUTH_WEST:
			x = 0; y = yBottom; 
			break;

		default:	 //centre 
			x = xMiddle; y = yMiddle; 
			break;
		}
		getFrameMain().setLocation(x, y);
		getFrameMain().pack();
		location_x = x;
		location_y = y;
	}

	
	public void masquerFenetre()
	{
		getFrameMain().setVisible(false);
	}


	public void quitter ()
	{
		app.quit();
	}



	public void fenetreDeDialogueYESNOCancel(String message,Object obj,String methodeYES,String methodeNO)
	{	
		String title = "Attention !";
		JFrame frame = new JFrame(title);

		int answer = JOptionPane.showConfirmDialog(frame, message);
		if (answer == JOptionPane.YES_OPTION) {
			System.out.println("YES");
			callMethode(obj,methodeYES);
		} else if (answer == JOptionPane.NO_OPTION) {
			System.out.println("NO");
			callMethode(obj,methodeNO);
		}
	}

	public void fenetreDeDialogueDialogue(String message,Object obj,String methodeYES){	
		String title = "Validation !";
		JFrame frame = new JFrame(title);

		String text = JOptionPane.showInputDialog(frame, message);
		if (text == null) {
			System.out.println("CANCEL");
		}else{
			System.out.println("YES");
			callMethode(obj,methodeYES);
		}
	}


	public void fenetreDeDialogueYESCancel(String message,Object obj,String methodeYES)	{	
		String title = "Confirmation !";
		JFrame frame = new JFrame(title);

		int answer = JOptionPane.showConfirmDialog(frame, message, title, JOptionPane.YES_NO_OPTION);

		if (answer == JOptionPane.YES_OPTION) {
			System.out.println("YES");
			callMethode(obj,methodeYES);
		} 
	}

	public void fenetreDeDialogueInformation(String message){
		String title = "Information !";
		JFrame frame = new JFrame(title);
		JOptionPane.showMessageDialog(frame, message);
	}

	private void callMethode(Object obj,String methode)
	{
		// try du call
		try {
			Class c;
			c = obj.getClass();
			// Recupereation de la methode getXxxxx :
			Method call = c.getMethod(methode);
			// appel de la methode
			call.invoke(obj);

		} catch (Exception ex) {
			//System.out.println(ex+"objet "+obj+" methode "+methode+" inexistante");
		}

	}

	public void trace(String s){
		app.getToolsCocktailLogs().trace(s,isWithLogs());
	}




	public boolean isWithLogs() {
		return withLogs;
	}

	public void setWithLogs(boolean withLogs) {
		this.withLogs = withLogs;
	}

	protected void setFrameMain(JFrame frameMain) {
		this.frameMain = frameMain;
	}

	public JFrame getFrameMain() {
		return frameMain;
	}

	public void setEnabledComposant(JComponent c,boolean bool){
		//c.setEditable(false);
		c.setFocusable(bool);
		c.setRequestFocusEnabled(bool);
		c.setAutoscrolls(bool);
		c.setOpaque(bool);
		c.setEnabled(bool);
	}
	
	public void formaterUnTexfieldNSTimestamp(JFormattedTextField tf,String format){
		tf.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
		tf.setValue(new NSTimestamp());
		DateFormatter fmt = (DateFormatter)tf.getFormatter();
		fmt.setFormat(new SimpleDateFormat(format));	
	    tf.updateUI();
	}
	
	public void formaterUnTexfieldBigDecimal(JFormattedTextField tf,String format){
		tf.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
		tf.setValue(new BigDecimal("999999.99"));
	    DefaultFormatter fmt = new NumberFormatter(new DecimalFormat(format));
	    fmt.setValueClass(tf.getValue().getClass());
	    DefaultFormatterFactory fmtFactory = new DefaultFormatterFactory(fmt, fmt, fmt);
	    tf.setFormatterFactory(fmtFactory);
	    tf.updateUI();
	}
	
	public void formaterUnTexfieldInteger(JFormattedTextField tf){
		tf.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
		tf.setValue(new Integer(0));  
	    tf.updateUI();
	}
	
	public void assistantsAnnuler(Object sender){
	}
		
	public void assistantsTerminer(Object sender){
	}
	
	public void swingFinderAnnuler(Object sender){
	}
		
	public void swingFinderTerminer(Object sender){
	}
	
	public boolean isModal() {
		return modal;
	}

	public void setModal(boolean modal) {
		this.modal = modal;
	}
	
}