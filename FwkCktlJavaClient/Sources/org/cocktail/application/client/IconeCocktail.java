/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client;

import java.lang.reflect.Field;

import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public class IconeCocktail {

	
	public static String  FERMER="close_view";
	public static String  INSPECTER="int_obj";
	public static String  COPIERCOLLER="copiecolle";
	
	public static String  ENTRER="valider16";
	public static String  QUITTER="quitter";
	
	public static String  INTEGRER="reprise";
	public static String  EXTRAIRE="sortie";
	public static String  RECHERCHER="linkto_help";
	public static String  RECHERCHER2="linkto_help";
	
	public static String  FLECHEDROITE="forward_nav";
	public static String  FLECHEGAUCHE="backward_nav";
	
    public static String  REFRESH="refresh";
    public static String  EMAIL="send";
    public static String  DATABASE="database";

	public static String  AJOUTER="add_obj";
	public static String  MODIFIER="refresh";
	public static String  SUPPRIMER="delete_obj";
	public static String  IMPRIMER="print_edit";
	
	public static String  POUBELLE="trash";
	public static String  TRAITEMENT_BD="newconnect_wiz";
	
	public static String  VISA="tasks_tsk";
	
	public static String  VALIDER="tasks_tsk";
	public static String  REFUSER="delete_obj";
    
    public static String PAGE="page_obj";
    public static String TABLEAU="prop_ps";
    public static String FILTRER="help_search";
    
    public static String PDF = "pdf";
    public static String XLS = "iconeXLS";
    public static String MAIL = "filter_ps";
    public static String COPIECOLLE = "copiecolle";
    public static String TABCROISEXLS = "iconeXLS"; 
    public static String CSV = "page_obj";
    
    public static String TREEVIEW = "treeview";
    public static String TABLE = "table";
    
    public static String SHOW = "loupe16";
	
    public static String nomIcon(String str){
        try {
            Field f = IconeCocktail.class.getField(StringCtrl.toBasicString(str).trim().toUpperCase());
            return (String)f.get(null);
        } catch (SecurityException e) {
        } catch (NoSuchFieldException e) {
        } catch (IllegalArgumentException e) {
        } catch (IllegalAccessException e) {
        }
        return str;
    }
}


