/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client;

import java.awt.AWTEvent;
import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Toolkit;
import java.awt.event.AWTEventListener;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.MissingResourceException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.InputVerifier;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.RootPaneContainer;
import javax.swing.SwingConstants;

import org.cocktail.application.client.nibfinder.NibFinder;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.palette.ToolsSwing;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eoapplication.EOInterfaceController;
import com.webobjects.eoapplication.client.EOClientResourceBundle;
import com.webobjects.eointerface.swing.EOTable;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSDictionary;
import com.webobjects.foundation.NSMutableArray;

// import fr.univlr.cri.javaclient.ULRUtilities;
import org.cocktail.fwkcktlwebapp.common.util.StringCtrl;

public abstract class EOInterfaceControllerCocktail extends EOInterfaceController implements ModalInterfaceCocktail {
	public int TABLE_FONT_SIZE = 10;

	public NibFinder fenetre;

	private GlassPane m_glassPane = null;

	public ApplicationCocktail cocktailApp = (ApplicationCocktail) EOApplication.sharedApplication();

	abstract public void swingFinderAnnuler(SwingFinder s);

	abstract public void swingFinderTerminer(SwingFinder s);

	abstract public void nibFinderAnnuler(NibFinder s);

	abstract public void nibFinderTerminer(NibFinder s);

	abstract public NSArray getResultat();

	public void componentDidBecomeVisible() {
		super.componentDidBecomeVisible();
		creerGlassPane();
		setupComponents(new NSArray(this.component().getComponents()), new EOClientResourceBundle());
		if (cocktailApp.defaultBackgroundColor() != null)
		{
			this.component().getTopLevelAncestor().setBackground(cocktailApp.defaultBackgroundColor());
			this.component().setBackground(cocktailApp.defaultBackgroundColor());
		}
		//        cocktailApp.getMonToolsCocktailJefyAdmin().appliquerLesPrivilegesUI();
	}

	public void fenetreDeDialogueYESNOCancel(String message, Object obj, String methodeYES, String methodeNO) {
		String title = "Attention !";
		JFrame frame = new JFrame(title);

		int answer = JOptionPane.showConfirmDialog(frame, ToolsSwing.formaterStringU(message));
		if (answer == JOptionPane.YES_OPTION) {
			System.out.println("YES");
			callMethode(obj, methodeYES);
		}
		else if (answer == JOptionPane.NO_OPTION) {
			System.out.println("NO");
			callMethode(obj, methodeNO);
		}
	}

	public void fenetreDeDialogueDialogue(String message, Object obj, String methodeYES) {
		String title = "Validation !";
		JFrame frame = new JFrame(title);

		String text = JOptionPane.showInputDialog(frame, ToolsSwing.formaterStringU(message));
		if (text == null) {
			System.out.println("CANCEL");
		}
		else {
			System.out.println("YES");
			callMethode(obj, methodeYES);
		}
	}

	public void fenetreDeDialogueYESCancel(String message, Object obj, String methodeYES) {
		String title = "Confirmation !";
		JFrame frame = new JFrame(title);

		int answer = JOptionPane.showConfirmDialog(frame, ToolsSwing.formaterStringU(message), title, JOptionPane.YES_NO_OPTION);

		if (answer == JOptionPane.YES_OPTION) {
			System.out.println("YES");
			callMethode(obj, methodeYES);
		}
	}

	public void fenetreDeDialogueInformation(String message) {
		String title = "Information !";
		JFrame frame = new JFrame(title);
		JOptionPane.showMessageDialog(frame, ToolsSwing.formaterStringU(message));
	}

	private void callMethode(Object obj, String methode) {
		// try du call
		try {
			Class c;
			c = obj.getClass();
			// Recupereation de la methode getXxxxx :
			Method call = c.getMethod(methode);
			// appel de la methode
			call.invoke(obj);

		} catch (Exception ex) {
			// System.out.println(ex+"objet "+obj+" methode "+methode+" inexistante");
		}

	}

	public void creerGlassPane() {
		//        System.out.println("InterfaceControllerCocktail.creerGlassPane()");
		m_glassPane = new GlassPane();
		m_glassPane.setGlassPane((JFrame) this.component().getTopLevelAncestor());
	}

	public void poserGlassPane() {
		m_glassPane.setDrawing(true);
	}

	public void retirerGlassPane() {
		m_glassPane.setDrawing(false);
	}

	public void annuler() {
		fenetre.setResultat(null);
		if (fenetre.getParentControleur() != null)
		{
			//fenetre.getParentControleur().nibFinderAnnuler(fenetre);
			Class c = fenetre.getParentControleur().getClass();
			try {
				Method met = c.getMethod("nibFinderAnnuler", new Class[] {
					ModalInterfaceCocktail.class
				});
				met.invoke(fenetre.getParentControleur(), new Object[] {
					this
				});
			} catch (Exception exception) {
				try {
					Method met = c.getMethod("nibFinderAnnuler", new Class[] {
						NibFinder.class
					});
					met.invoke(fenetre.getParentControleur(), new Object[] {
						this.fenetre
					});
				} catch (Exception exception2) {
					exception2.printStackTrace();
				}
			}
		}
		else if (fenetre.getParentControleurSW() != null)
		{
			//fenetre.getParentControleur().nibFinderAnnuler(fenetre);
			//                Class c = fenetre.getParentControleurSW().getClass();
			//                try {
			//                    Method met = c.getMethod("nibFinderAnnuler", new Class[]{NibFinder.class});
			//                    met.invoke(fenetre.getParentControleurSW(), new Object[]{this.fenetre});
			//                } catch (Exception exception2) {
			//                    exception2.printStackTrace();
			//                }
			fenetre.getParentControleurSW().swingFinderTerminer(this.fenetre);
		}
		fenetre.masquerFenetre();
	}

	public void valider() {
		fenetre.setResultat(getResultat());
		if (fenetre.getParentControleur() != null)
		{
			//fenetre.getParentControleur().nibFinderTerminer(fenetre);
			Class c = fenetre.getParentControleur().getClass();
			try {
				Method met = c.getMethod("nibFinderTerminer", new Class[] {
					ModalInterfaceCocktail.class
				});
				met.invoke(fenetre.getParentControleur(), new Object[] {
					this
				});
			} catch (Exception exception) {
				try {
					Method met = c.getMethod("nibFinderTerminer", new Class[] {
						NibFinder.class
					});
					met.invoke(fenetre.getParentControleur(), new Object[] {
						this.fenetre
					});
				} catch (Exception exception2) {
					exception2.printStackTrace();
				}
			}
		}
		else if (fenetre.getParentControleurSW() != null)
		{
			//fenetre.getParentControleur().nibFinderTerminer(fenetre);
			//	            Class c = fenetre.getParentControleurSW().getClass();
			//	            try {
			//	                Method met = c.getMethod("nibFinderTerminer", new Class[]{NibFinder.class});
			//	                met.invoke(fenetre.getParentControleurSW(), new Object[]{this.fenetre});
			//	            } catch (Exception exception2) {
			//	                exception2.printStackTrace();
			//	            }
			fenetre.getParentControleurSW().swingFinderTerminer(this.fenetre);
		}
		fenetre.masquerFenetre();
	}

	/**
	 * <p>
	 * Parcourt une liste d'objets et affuble d'icones ceux qui sont des boutons ou des onglets. Les enventuels Containers sont parcouru
	 * recursivement.
	 * <p>
	 * L'objet graphique dont la legende (libelle) est "Créer" (ex: un bouton) sera affuble de l'icone nomme "Creer" (s'il est trouve dans les
	 * resources!). <b>Les noms des images resources doivent donc etre sans accent (car les accents dans les noms de resources ne sont pas
	 * acceptes).</b>
	 * 
	 * @param namedObjects Dictionnaire contenant les composants graphiques, certains pouvant bien-sur etre des Containers.
	 * @param resourceBundle EOClientResourceBundle dans lequel les icones sont recherches.
	 * @return Le dictionnaire des objets qui ont recus un icone.
	 */
	public NSArray setupComponents(final NSDictionary namedObjects, final EOClientResourceBundle resourceBundle) {
		return setupComponents(namedObjects.allValues(), resourceBundle);
	}

	public NSArray setupComponents(final NSArray objects, final EOClientResourceBundle resourceBundle) {
		NSMutableArray iconedObjects = new NSMutableArray();
		for (java.util.Enumeration enumerator = objects.objectEnumerator(); enumerator.hasMoreElements();) {
			setupComponents(enumerator.nextElement(), iconedObjects, resourceBundle);
		}
		return objects;
	}

	private void setupComponents(final Object object, final NSMutableArray objects, final EOClientResourceBundle resourceBundle) {
		// si le composant est une EOTable, mode de redimensionnement des colonnes
		if (object instanceof EOTable)
		{
			EOTable eotable = ((EOTable) object);
			eotable.table().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
			// ULRUtilities.setNonEditableTable(eotable);
			eotable.table().setFont(new Font(eotable.table().getFont().getName(), eotable.table().getFont().getStyle(), TABLE_FONT_SIZE));
			eotable.table().setRowHeight(14);
			eotable.table().setRowMargin(0);
		}
		// si le composant est un bouton, on installe l'icone eventuel
		if (object instanceof JButton && ((JButton) object).getText() != null && !"".equals(((JButton) object).getText())) {
			if (!objects.containsObject(object)) {
				try {
					//                    String objectText = ((JButton) object).getText();
					//                    // System.out.print(objectText+" ");
					//                    if (objectText != null && objectText.length() > 3 && "null".equals(objectText.substring(0, 4))) { // si commence par null, c'est un bouton sans legende
					//                        ((JButton) object).setText("");
					//                        objectText = objectText.substring(4);
					//                    }
					//                    if (objectText != null && objectText.length() > 1 && "BL".equals(objectText.substring(0, 2))) { // borderless
					//                    // si commence par BL, c'est un bouton sans bord
					//                        ((JButton) object).setBorderPainted(false);
					//                        objectText = objectText.substring(2);
					//                    }
					String objectText = ((JButton) object).getText();
					String privilege = null;
					if (objectText != null && objectText.indexOf("@") > 0)
					{
						String[] str = objectText.split("@");
						if (str.length > 1)
							privilege = str[1];
						objectText = str[0];
					}

					//                    System.out.println("objectText = "+objectText);
					//                    System.out.println(" . = "+objectText.indexOf("."));
					String title = null;
					if (objectText != null && objectText.indexOf(".") > 0)
					{
						String[] str = objectText.split("\\.");
						//                        System.out.println("str = "+str.length);
						if (str.length > 1)
							title = str[1];
						objectText = str[0];
						//                        System.out.println("objectText = "+objectText);
					}
					else
						title = LibelleIconeCocktail.libelle(objectText);
					//                    System.out.println("title = "+title);
					String nomIcon = IconeCocktail.nomIcon(objectText);
					//                    System.out.println("nomIcon = "+nomIcon);
					if (title == null)
					{
						if (nomIcon != null)
						{
							((JButton) object).setBorder(BorderFactory.createEmptyBorder(2, 2, 2, 2));//new EmptyBorder(((JButton) object).getInsets()));
							((JButton) object).setBorderPainted(false);
							((JButton) object).setText(title);
							((JButton) object).setHorizontalTextPosition(SwingConstants.CENTER);
							((JButton) object).setContentAreaFilled(false);
							((JButton) object).setFocusPainted(false);
						}
					}
					else
						((JButton) object).setText(title);
					ImageIcon imageIcon = null;
					try {
						imageIcon = (ImageIcon) resourceBundle.getObject(nomIcon);
					} catch (Throwable e) {
						e.printStackTrace();
					}

					if (privilege != null)
					{
						Class classPrivileges = cocktailApp.classPrivileges();
						Field f = null;
						try {
							f = classPrivileges.getField(StringCtrl.toBasicString(privilege).trim().toUpperCase());
							cocktailApp.getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(object, (String) f.get(null));
						} catch (SecurityException e) {
						} catch (NoSuchFieldException e) {
							cocktailApp.getMonToolsCocktailJefyAdmin().ajouterObjetPourPrivileges(object, (String) StringCtrl.toBasicString(privilege).trim().toUpperCase());
						} catch (IllegalArgumentException e) {
						} catch (IllegalAccessException e) {
						}
					}
					// System.out.print(objectText+" ");
					// System.out.println(((JButton)object).getParent().getClass().getName()+" "+((JButton)object).getParent().getLocation());
					// ImageIcon imageIcon = (ImageIcon) resourceBundle.getObject(StringOperation.sansAccent(objectText));
					//                    ImageIcon imageIcon = (ImageIcon) resourceBundle.getObject(StringCtrl.toBasicString(objectText));
					((JButton) object).setHorizontalAlignment(SwingConstants.LEFT);
					((JButton) object).setHorizontalTextPosition(SwingConstants.TRAILING);
					((JButton) object).setIcon(imageIcon);
				} catch (MissingResourceException mre) {
				}
				objects.addObject(object);
			}
		}
		// sinon, si c'est un container
		else if (object instanceof Container) {
			// si ce container est un panneau d'onglets, on installe les icones eventuels
			if (object instanceof JTabbedPane) {
				if (!objects.containsObject(object)) {
					for (int i = 0; i < ((JTabbedPane) object).getTabCount(); i++) {
						try {
							// System.out.print(((JTabbedPane)object).getTitleAt(i)+" ");
							// System.out.println(((JTabbedPane)object).getParent().getClass().getName()+" "+((JTabbedPane)object).getParent().getLocation());
							ImageIcon imageIcon = (ImageIcon) resourceBundle.getObject(StringCtrl.toBasicString(((JTabbedPane) object).getTitleAt(i)));
							((JTabbedPane) object).setIconAt(i, imageIcon);

							((JTabbedPane) object).setBackgroundAt(i, ((JComponent) object).getParent().getBackground());
						} catch (MissingResourceException mre) {
						}
					}
					((JComponent) object).setBackground(((JComponent) object).getParent().getBackground());
					objects.addObject(object);
				}
			}
			// puis on parcourt les composants de ce container
			for (int i = 0; i < ((Container) object).getComponents().length; i++) {
				setupComponents(((Container) object).getComponents()[i], objects, resourceBundle);
			}
		}
	}

	public void setCursor(int typeCursor) {
		component().getTopLevelAncestor().setCursor(Cursor.getPredefinedCursor(typeCursor));
	}

	class GlassPane extends JComponent implements MouseListener, AWTEventListener {
		// RootPaneContainer interface
		private RootPaneContainer m_rootPane = null;

		/**
		 * Constructor
		 * 
		 * @param title
		 * @exception
		 */

		public GlassPane() {
			setOpaque(false);
			addMouseListener(new MouseAdapter() {
			});
			addKeyListener(new KeyAdapter() {
			});
			setInputVerifier(new InputVerifier() {
				public boolean verify(JComponent anInput) {
					return false;
				}
			});
		}

		public synchronized FocusListener[] getFocusListeners() {
			if (m_glassPane.isOpaque())
				return null;
			return super.getFocusListeners();
		}

		/**
		 * Set the glassPane
		 */
		public void setGlassPane(RootPaneContainer rootPane) {
			// set this as new glass pane
			rootPane.setGlassPane(this);
			// set opaque to false, i.e. make transparent
			m_glassPane.setOpaque(false);

		}

		/**
		 * remove the glassPane
		 */
		public void removeGlassPane() {
			// set the glass pane visible false
			setVisible(false);
			// reset the previous glass pane
			m_rootPane.setGlassPane(null);
		}

		/**
		 * setDrawing
		 */
		public void setDrawing(boolean drawing) {

			if (drawing) {
				setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
				Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.KEY_EVENT_MASK);
			}
			else {
				setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
				Toolkit.getDefaultToolkit().removeAWTEventListener(this);
			}
			// This is important otherwise the glass pane will not be visible
			setVisible(drawing);
			// Call repaint
			repaint();
			this.paintAll(this.getGraphics());
			this.updateUI();
		}

		/**
		 * Handling Paint
		 */
		public void paint(Graphics g) {
			super.paint(g);
			// g.setColor(Color.LIGHT_GRAY);
			// g.fillRect(0, 0, this.getWidth(), this.getHeight());//(100, 100, 250, 200, 40, 40);
			// g.setColor(Color.RED);
			// g.drawString(" Transactions en cours ....", 50,50);

			Graphics2D graphics = (Graphics2D) g;
			graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
			g.setColor(Color.LIGHT_GRAY);
			g.fillRect(0, 0, this.getWidth(), this.getHeight());
		}

		public void mouseClicked(MouseEvent arg0) {
			System.out.println("1");
		}

		public void mouseEntered(MouseEvent arg0) {
			System.out.println("2");
		}

		public void mouseExited(MouseEvent arg0) {
			System.out.println("3");
		}

		public void mousePressed(MouseEvent arg0) {
			System.out.println("4");
		}

		public void mouseReleased(MouseEvent arg0) {
			System.out.println("5");
		}

		public void eventDispatched(AWTEvent arg0) {
		}
	}

}
