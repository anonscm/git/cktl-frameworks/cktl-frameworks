/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cocktail.application.client.inspecteur.nibctrl;

import com.webobjects.eoapplication.EOApplication;
import com.webobjects.eocontrol.EOEditingContext;
import com.webobjects.eocontrol.EOGenericRecord;
import com.webobjects.eocontrol.EOQualifier;
import com.webobjects.eocontrol.EOSortOrdering;
import com.webobjects.foundation.NSArray;
import com.webobjects.foundation.NSMutableArray;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.inspecteur.nib.SwingInspecteurFournisseurNib;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOGenericRecord;
import org.cocktail.application.client.tools.UserInfoCocktail;
import org.cocktail.application.nibctrl.NibCtrl;

/**
 *
 * @author mparadot
 */
public class SwingInspecteurFournisseurNibCtrl extends SwingFinderEOGenericRecord{
    
    private SwingInspecteurFournisseurNib monInspecteurFournisseurNib;
    private VFournisseur displayedFournisseur;
    private UserInfoCocktail userInfoFournis;
    private EOEditingContext editingContext;



    public SwingInspecteurFournisseurNibCtrl( int alocation_x,
                int alocation_y, int ataille_x, int ataille_y) {
        super((ApplicationCocktail)EOApplication.sharedApplication(), alocation_x, alocation_y, ataille_x, ataille_y);
        setWithLogs(false);
    }

   public void creationFenetre(SwingInspecteurFournisseurNib leSwingInspecteurFournisseurNib, String title,NibCtrl parentControleur,boolean modal) {
        super.creationFenetre(leSwingInspecteurFournisseurNib, title);
        setMonInspecteurFournisseurNib(leSwingInspecteurFournisseurNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleur =  parentControleur;
        bindingAndCustomization();
    }


    public void creationFenetre(SwingInspecteurFournisseurNib leSwingInspecteurFournisseurNib, String title,EOInterfaceControllerCocktail parentControleur,boolean modal) {
        super.creationFenetre(leSwingInspecteurFournisseurNib, title);
        setMonInspecteurFournisseurNib(leSwingInspecteurFournisseurNib);
        setNibCtrlLocation(LOCATION_MIDDLE);
        setModal(modal);
        this.parentControleurEONib =  parentControleur;
        bindingAndCustomization();
    }
    
    private void bindingAndCustomization() {
        try {
            getMonInspecteurFournisseurNib().getJbtcFermer().addDelegateActionListener(this, METHODE_ACTION_SELECTIONNER);
        } catch (Exception e) {
                this.app.getToolsCocktailLogs().addLogMessage(this, "Exception",e.getMessage(), true);
        }
    }

    public void afficherFenetre() {
        try {
            if(getDisplayedFournisseur()==null)
            {
                this.app.getToolsCocktailLogs().addLogMessage(this, "Erreur","le fournisseur a afficher est vide !!!", true);
                return;
            }
            setUserInfoFournisseur();
            majInterface();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        super.afficherFenetre();
    }

    public SwingInspecteurFournisseurNib getMonInspecteurFournisseurNib() {
        return monInspecteurFournisseurNib;
    }

    public void setMonInspecteurFournisseurNib(SwingInspecteurFournisseurNib monInspecteurFournisseurNib) {
        this.monInspecteurFournisseurNib = monInspecteurFournisseurNib;
    }

    public VFournisseur getDisplayedFournisseur() {
        return displayedFournisseur;
    }

    public void setDisplayedFournisseur(VFournisseur dispayedFournisseur) {
        this.displayedFournisseur = dispayedFournisseur;
    }
    
    private void setUserInfoFournisseur(){
        userInfoFournis = new UserInfoCocktail();
        EOQualifier qual = EOQualifier.qualifierWithQualifierFormat("toRepartCompte.persId = %@", new NSArray(getDisplayedFournisseur().persId()));
        NSMutableArray sort = new NSMutableArray();
        sort.addObject(EOSortOrdering.sortOrderingWithKey("cptVlan", EOSortOrdering.CompareAscending));
        NSArray list = app.getToolsCocktailEOF().fetchArrayWithNomTableWithQualifierWithSort(getEditingContext(),"FwkCktlWebApp_Compte", qual, sort);
        if(list==null || list.count()==0)
        {
            getMonInspecteurFournisseurNib().getJtpTabs().setEnabledAt(1, false);
        }
        else
        {
            getMonInspecteurFournisseurNib().getJtpTabs().setEnabledAt(1, true);
            EOGenericRecord eog = (EOGenericRecord) list.lastObject();
            userInfoFournis.setEmail((String)eog.valueForKey("email"));
            userInfoFournis.setLogin((String)eog.valueForKey("cptLogin"));
            char c= ((String)eog.valueForKey("cptVlan")).charAt(0);
            switch(c){
                case 'X':
                    userInfoFournis.setVLan("Exterieur");
                    break;
                case 'P':
                    userInfoFournis.setVLan("Professionnelle");
                    break;
                default:
                    userInfoFournis.setVLan("Etudiant");
            }
        }
    }

    public EOEditingContext getEditingContext() {
        if(editingContext==null)
            return app.getAppEditingContext();
        return editingContext;
    }

    public void setEditingContext(EOEditingContext editingContext) {
        this.editingContext = editingContext;
    }
    
    private void majInterface(){
        getMonInspecteurFournisseurNib().getJtfcNomFour().setText(displayedFournisseur.adrNom());
        getMonInspecteurFournisseurNib().getJtfcPrenomFour().setText(displayedFournisseur.adrPrenom());
        getMonInspecteurFournisseurNib().getJtfcAdrFour().setText(displayedFournisseur.adrAdresse1());
        getMonInspecteurFournisseurNib().getJtfcAdrSuiteFour().setText(displayedFournisseur.adrAdresse2());
        getMonInspecteurFournisseurNib().getJtfcCodeFour().setText(displayedFournisseur.fouCode());
        getMonInspecteurFournisseurNib().getJtfcCodePostalFour().setText(displayedFournisseur.adrCp());
        getMonInspecteurFournisseurNib().getJtfcPaysFour().setText(displayedFournisseur.lcPays());
        getMonInspecteurFournisseurNib().getJtfcVilleFour().setText(displayedFournisseur.adrVille());
        getMonInspecteurFournisseurNib().getJtfcEmailFour().setText(userInfoFournis.email());
        getMonInspecteurFournisseurNib().getJtfcLoginFour().setText(userInfoFournis.login());
        getMonInspecteurFournisseurNib().getJtfcVlanFour().setText(userInfoFournis.vLan());
        getMonInspecteurFournisseurNib().getJrbcTypeFournis().setSelected("F".equals(displayedFournisseur.fouType()) || "T".equals(displayedFournisseur.fouType()));
        getMonInspecteurFournisseurNib().getJrbcTypeClient().setSelected("C".equals(displayedFournisseur.fouType()) || "T".equals(displayedFournisseur.fouType()));
        getMonInspecteurFournisseurNib().getJftfDateCreation().setValue(displayedFournisseur.dCreation());
        getMonInspecteurFournisseurNib().getJftfDateCreation().setValue(displayedFournisseur.dModification());
    }
    
    private void cleanInterface(){
        getMonInspecteurFournisseurNib().getJtfcNomFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcPrenomFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcAdrFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcAdrSuiteFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcCodeFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcCodePostalFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcPaysFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcVilleFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcEmailFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcLoginFour().setText("");
        getMonInspecteurFournisseurNib().getJtfcVlanFour().setText("");
        getMonInspecteurFournisseurNib().getJrbcTypeFournis().setSelected(false);
        getMonInspecteurFournisseurNib().getJrbcTypeClient().setSelected(false);
        getMonInspecteurFournisseurNib().getJftfDateCreation().setValue(null);
        getMonInspecteurFournisseurNib().getJftfDateCreation().setValue(null);
        getMonInspecteurFournisseurNib().updateUI();
    }

    public void actionSelectionner() {
        cleanInterface();
        super.actionSelectionner();
    }
    
    
}
