/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.cocktail.application.client.inspecteur;

import javax.swing.JButton;
import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.eof.VFournisseur;
import org.cocktail.application.client.inspecteur.nib.SwingInspecteurFournisseurNib;
import org.cocktail.application.client.inspecteur.nibctrl.SwingInspecteurFournisseurNibCtrl;
import org.cocktail.application.client.swingfinder.SwingFinder;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.ToolsSwing;

/**
 *
 * @author mparadot
 */
public class SwingInspecteurFournisseur extends SwingFinder{
    private SwingInspecteurFournisseurNibCtrl monInspecteurFournisseurNibCtrl;
    private SwingInspecteurFournisseurNib monSwingInspecteurFournisseurNib;
    private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";

    public SwingInspecteurFournisseur(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
        this(ctrl, responder, alocation_x, alocation_y, ataille_x, ataille_y, modal, false);
    }
	
    
    public SwingInspecteurFournisseur(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
	super();
        setMonInspecteurFournisseurNibCtrl(new SwingInspecteurFournisseurNibCtrl(alocation_x , alocation_y, ataille_x , ataille_y));
        setMonSwingInspecteurFournisseurNib(new SwingInspecteurFournisseurNib());
        getMonInspecteurFournisseurNibCtrl().creationFenetre(
                        getMonSwingInspecteurFournisseurNib(),
                        ToolsSwing.formaterStringU("Inspecteur de fournisseur"),
                        responder,
                        modal
        );
//        getMonSwingInspecteurFournisseurNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
//        );
        getMonSwingInspecteurFournisseurNib().setSize(ataille_x,ataille_y);		
        getMonInspecteurFournisseurNibCtrl().setSender(this);
	}

    public SwingInspecteurFournisseur(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
        this(ctrl, responder, alocation_x, alocation_y, ataille_x, ataille_y, modal, false);
    }
    
    public SwingInspecteurFournisseur(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
	super();
        setMonInspecteurFournisseurNibCtrl(new SwingInspecteurFournisseurNibCtrl(alocation_x , alocation_y, ataille_x , ataille_y));
        setMonSwingInspecteurFournisseurNib(new SwingInspecteurFournisseurNib());
        getMonInspecteurFournisseurNibCtrl().creationFenetre(
                getMonSwingInspecteurFournisseurNib(),
                ToolsSwing.formaterStringU("Inspecteur de fournisseur"),
                responder,
                modal
        );
//        getMonSwingInspecteurFournisseurNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
//        );
        getMonSwingInspecteurFournisseurNib().setSize(ataille_x,ataille_y);     
        getMonInspecteurFournisseurNibCtrl().setSender(this);
    }

    public SwingInspecteurFournisseurNibCtrl getMonInspecteurFournisseurNibCtrl() {
        return monInspecteurFournisseurNibCtrl;
    }

    public void setMonInspecteurFournisseurNibCtrl(SwingInspecteurFournisseurNibCtrl monInspecteurFournisseurNibCtrl) {
        this.monInspecteurFournisseurNibCtrl = monInspecteurFournisseurNibCtrl;
    }
    
    public void afficherFenetreFinder(JButton bt,VFournisseur fournis) {
        getMonInspecteurFournisseurNibCtrl().setDisplayedFournisseur(fournis);
        getMonInspecteurFournisseurNibCtrl().afficherFenetre();  
    }
    
    public void relierBouton(JButtonCocktail bt) {
            bt.addDelegateActionListener(getMonInspecteurFournisseurNibCtrl(),METHODE_AFFICHER_FENETRE);	
    }

    public SwingInspecteurFournisseurNib getMonSwingInspecteurFournisseurNib() {
        return monSwingInspecteurFournisseurNib;
    }

    public void setMonSwingInspecteurFournisseurNib(SwingInspecteurFournisseurNib monSwingInspecteurFournisseurNib) {
        this.monSwingInspecteurFournisseurNib = monSwingInspecteurFournisseurNib;
    }
}
