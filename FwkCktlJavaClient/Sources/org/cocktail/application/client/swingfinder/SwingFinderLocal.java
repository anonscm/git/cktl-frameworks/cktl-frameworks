/*
 * Copyright Cocktail, 2001-2006
 * 
 * This software is governed by the CeCILL license under French law and abiding
 * by the rules of distribution of free software. You can use, modify and/or
 * redistribute the software under the terms of the CeCILL license as circulated
 * by CEA, CNRS and INRIA at the following URL "http://www.cecill.info".
 * 
 * As a counterpart to the access to the source code and rights to copy, modify
 * and redistribute granted by the license, users are provided only with a
 * limited warranty and the software's author, the holder of the economic
 * rights, and the successive licensors have only limited liability.
 * 
 * In this respect, the user's attention is drawn to the risks associated with
 * loading, using, modifying and/or developing or reproducing the software by
 * the user in light of its specific status of free software, that may mean that
 * it is complicated to manipulate, and that also therefore means that it is
 * reserved for developers and experienced professionals having in-depth
 * computer knowledge. Users are therefore encouraged to load and test the
 * software's suitability as regards their requirements in conditions enabling
 * the security of their systems and/or data to be ensured and, more generally,
 * to use and operate it in the same conditions as regards security.
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package org.cocktail.application.client.swingfinder;
import javax.swing.JButton;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOLocalNib;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOLocalNibCtrl;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.ToolsSwing;

import com.webobjects.foundation.NSArray;

public class SwingFinderLocal extends SwingFinder{

	private static final String FENETRE_DE_SELECTION_Local="Fenetre de selection de salle";

	private SwingFinderEOLocalNib monSwingFinderEOLocalNib = null;
	private SwingFinderEOLocalNibCtrl monSwingFinderEOLocalNibCtrl = null;

	private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";

	public SwingFinderLocal(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
		super();

		// creation du nib Local
		setMonSwingFinderEOLocalNibCtrl(new SwingFinderEOLocalNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonSwingFinderEOLocalNib( new SwingFinderEOLocalNib());
		getMonSwingFinderEOLocalNibCtrl().creationFenetre(
				getMonSwingFinderEOLocalNib(),
				ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Local),
				responder,
				modal
		);
		getMonSwingFinderEOLocalNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
		);
		getMonSwingFinderEOLocalNib().setSize(ataille_x,ataille_y);		
		getMonSwingFinderEOLocalNibCtrl().setSender(this);
	}



	public SwingFinderLocal(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal) {
		super();

		// creation du nib Local
		setMonSwingFinderEOLocalNibCtrl(new SwingFinderEOLocalNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
		setMonSwingFinderEOLocalNib( new SwingFinderEOLocalNib());
		getMonSwingFinderEOLocalNibCtrl().creationFenetre(
				getMonSwingFinderEOLocalNib(),
				ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_Local),
				responder,
				modal
		);
		getMonSwingFinderEOLocalNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
		);
		getMonSwingFinderEOLocalNib().setSize(ataille_x,ataille_y);		
		getMonSwingFinderEOLocalNibCtrl().setSender(this);
	}

	public void afficherFenetreFinder(JButton bt) {
		getMonSwingFinderEOLocalNibCtrl().afficherFenetre();	
	}


	public void relierBouton(JButtonCocktail bt) {
		bt.addDelegateActionListener(getMonSwingFinderEOLocalNibCtrl(),METHODE_AFFICHER_FENETRE);	
	}

	public NSArray getResultat() {
		return	getMonSwingFinderEOLocalNibCtrl().getResultat();	
	}

	public JPanelCocktail getPanel(){
		return getMonSwingFinderEOLocalNib();	
	}

	public SwingFinderEOLocalNib getMonSwingFinderEOLocalNib() {
		return monSwingFinderEOLocalNib;
	}


	public void setMonSwingFinderEOLocalNib(
			SwingFinderEOLocalNib monSwingFinderEOLocalNib) {
		this.monSwingFinderEOLocalNib = monSwingFinderEOLocalNib;
	}


	public SwingFinderEOLocalNibCtrl getMonSwingFinderEOLocalNibCtrl() {
		return monSwingFinderEOLocalNibCtrl;
	}


	public void setMonSwingFinderEOLocalNibCtrl(
			SwingFinderEOLocalNibCtrl monSwingFinderEOLocalNibCtrl) {
		this.monSwingFinderEOLocalNibCtrl = monSwingFinderEOLocalNibCtrl;
	}



}
