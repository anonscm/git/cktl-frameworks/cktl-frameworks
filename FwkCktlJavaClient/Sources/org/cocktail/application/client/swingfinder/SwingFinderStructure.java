/*
 * Copyright Cocktail, 2001-2007 
 * 
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software. You can use, 
 * modify and/or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info". 
 * 
 * As a counterpart to the access to the source code and rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty and the software's author, the holder of the
 * economic rights, and the successive licensors have only limited
 * liability. 
 * 
 * In this respect, the user's attention is drawn to the risks associated
 * with loading, using, modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean that it is complicated to manipulate, and that also
 * therefore means that it is reserved for developers and experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or 
 * data to be ensured and, more generally, to use and operate it in the 
 * same conditions as regards security. 
 * 
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 *
 * Created on 19 oct. 07
 * author mparadot 
 */
package org.cocktail.application.client.swingfinder;

import javax.swing.JButton;

import org.cocktail.application.client.ApplicationCocktail;
import org.cocktail.application.client.EOInterfaceControllerCocktail;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOIndividuUlrNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOStructureNib;
import org.cocktail.application.client.swingfinder.nib.SwingFinderEOStructureTreeNib;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOIndividuUlrNibCtrl;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOStructureNibCtrl;
import org.cocktail.application.client.swingfinder.nibctrl.SwingFinderEOStructureTreeNibCtrl;
import org.cocktail.application.nibctrl.NibCtrl;
import org.cocktail.application.palette.JButtonCocktail;
import org.cocktail.application.palette.JPanelCocktail;
import org.cocktail.application.palette.ToolsSwing;

import com.webobjects.foundation.NSArray;

public class SwingFinderStructure  extends SwingFinder{


    private static final String FENETRE_DE_SELECTION_DES_STRUCTURES="Fenetre de selection des structures";

    private SwingFinderEOStructureNib monSwingFinderEOStructureNib= null;
    private SwingFinderEOStructureNibCtrl monSwingFinderEOStructureNibCtrl = null;
    
    private SwingFinderEOStructureTreeNib monSwingFinderEOStructureTreeNib= null;
    private SwingFinderEOStructureTreeNibCtrl monSwingFinderEOStructureTreeNibCtrl = null;

    private static final String METHODE_AFFICHER_FENETRE = "afficherFenetre";
    
    private boolean useTreeView;

    public SwingFinderStructure(ApplicationCocktail ctrl,NibCtrl responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
        super();
        this.useTreeView = useTreeView;
        if(useTreeView)
        {
            if(alocation_x<640)
                alocation_x = 640;
            if(alocation_y<480)
                alocation_y = 480;
            // creation du nib IndividuUlr
            setMonSwingFinderEOStructureTreeNibCtrl(new SwingFinderEOStructureTreeNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
            SwingFinderEOStructureTreeNib sfsn = new SwingFinderEOStructureTreeNib();
//            System.out.println("sfsn"+sfsn);
            setMonSwingFinderEOStructureTreeNib(sfsn);
            getMonSwingFinderEOStructureTreeNibCtrl().creationFenetre(
                    getMonSwingFinderEOStructureTreeNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_STRUCTURES),
                    responder,
                    modal
            );
            getMonSwingFinderEOStructureTreeNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOStructureTreeNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOStructureTreeNibCtrl().setSender(this);
        }
        else
        {
            // creation du nib IndividuUlr
            if(alocation_x<640)
                alocation_x = 640;
            if(alocation_y<480)
                alocation_y = 480;
            setMonSwingFinderEOStructureNibCtrl(new SwingFinderEOStructureNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
            SwingFinderEOStructureNib sfsn = new SwingFinderEOStructureNib();
//            System.out.println("sfsn"+sfsn);
            setMonSwingFinderEOStructureNib(sfsn);
            getMonSwingFinderEOStructureNibCtrl().creationFenetre(
                    getMonSwingFinderEOStructureNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_STRUCTURES),
                    responder,
                    modal
            );
            getMonSwingFinderEOStructureNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOStructureNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOStructureNibCtrl().setSender(this);
        }
    }


    public SwingFinderStructure(ApplicationCocktail ctrl,EOInterfaceControllerCocktail responder,int alocation_x , int alocation_y,int ataille_x , int ataille_y,boolean modal,boolean useTreeView) {
        super();
        this.useTreeView = useTreeView;
        
        if(useTreeView)
        {
            // creation du nib IndividuUlr
            setMonSwingFinderEOStructureTreeNibCtrl(new SwingFinderEOStructureTreeNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
            SwingFinderEOStructureTreeNib sfsn = new SwingFinderEOStructureTreeNib();
//            System.out.println("sfsn"+sfsn);
            setMonSwingFinderEOStructureTreeNib(sfsn);
            getMonSwingFinderEOStructureTreeNibCtrl().creationFenetre(
                    getMonSwingFinderEOStructureTreeNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_STRUCTURES),
                    responder,
                    modal
            );
            getMonSwingFinderEOStructureTreeNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOStructureTreeNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOStructureTreeNibCtrl().setSender(this);
        }else
        {
            // creation du nib IndividuUlr
            setMonSwingFinderEOStructureNibCtrl(new SwingFinderEOStructureNibCtrl(ctrl, alocation_x , alocation_y, ataille_x , ataille_y));
            SwingFinderEOStructureNib sfsn = new SwingFinderEOStructureNib();
//            System.out.println("sfsn"+sfsn);
            setMonSwingFinderEOStructureNib(sfsn);
            getMonSwingFinderEOStructureNibCtrl().creationFenetre(
                    getMonSwingFinderEOStructureNib(),
                    ToolsSwing.formaterStringU(FENETRE_DE_SELECTION_DES_STRUCTURES),
                    responder,
                    modal
            );
            getMonSwingFinderEOStructureNib().setPreferredSize(new java.awt.Dimension(ataille_x,ataille_y)
            );
            getMonSwingFinderEOStructureNib().setSize(ataille_x,ataille_y);       
            getMonSwingFinderEOStructureNibCtrl().setSender(this);
        }
    }

    public void afficherFenetreFinder(JButton bt) {
        if(useTreeView)
            getMonSwingFinderEOStructureTreeNibCtrl().afficherFenetre();
        else
            getMonSwingFinderEOStructureNibCtrl().afficherFenetre();  
    }


    public void relierBouton(JButtonCocktail bt) {
        if(useTreeView)
            bt.addDelegateActionListener(getMonSwingFinderEOStructureTreeNibCtrl(),METHODE_AFFICHER_FENETRE); 
        else
            bt.addDelegateActionListener(getMonSwingFinderEOStructureNibCtrl(),METHODE_AFFICHER_FENETRE); 
    }

    public NSArray getResultat() {
        if(useTreeView)
            return getMonSwingFinderEOStructureTreeNibCtrl().getResultat();
        return  getMonSwingFinderEOStructureNibCtrl().getResultat();  
    }

    public JPanelCocktail getPanel(){
        if(useTreeView)
            return getMonSwingFinderEOStructureTreeNib();
        return getMonSwingFinderEOStructureNib(); 
    }

    public SwingFinderEOStructureNib getMonSwingFinderEOStructureNib() {
        return monSwingFinderEOStructureNib;
    }


    public void setMonSwingFinderEOStructureNib(
            SwingFinderEOStructureNib monSwingFinderEOStructureNib) {
        if(monSwingFinderEOStructureNib==null)
        {
            System.out.println("SwingFinderStructure.setMonSwingFinderEOStructureNib()");
            this.monSwingFinderEOStructureNib = new SwingFinderEOStructureNib();
        }
        this.monSwingFinderEOStructureNib = monSwingFinderEOStructureNib;
    }


    public SwingFinderEOStructureNibCtrl getMonSwingFinderEOStructureNibCtrl() {
        return monSwingFinderEOStructureNibCtrl;
    }


    public void setMonSwingFinderEOStructureNibCtrl(
            SwingFinderEOStructureNibCtrl monSwingFinderEOStructureNibCtrl) {
        this.monSwingFinderEOStructureNibCtrl = monSwingFinderEOStructureNibCtrl;
    }


    public SwingFinderEOStructureTreeNib getMonSwingFinderEOStructureTreeNib() {
        return monSwingFinderEOStructureTreeNib;
    }


    public void setMonSwingFinderEOStructureTreeNib(SwingFinderEOStructureTreeNib monSwingFinderEOStructureTreeNib) {
        this.monSwingFinderEOStructureTreeNib = monSwingFinderEOStructureTreeNib;
    }


    public SwingFinderEOStructureTreeNibCtrl getMonSwingFinderEOStructureTreeNibCtrl() {
        return monSwingFinderEOStructureTreeNibCtrl;
    }


    public void setMonSwingFinderEOStructureTreeNibCtrl(SwingFinderEOStructureTreeNibCtrl monSwingFinderEOStructureTreeNibCtrl) {
        this.monSwingFinderEOStructureTreeNibCtrl = monSwingFinderEOStructureTreeNibCtrl;
    }
    
    public NSArray getResultatRepartAdresse(){
        return getMonSwingFinderEOStructureNibCtrl().getResultatRepartAdresse();
    }
    
    public NSArray getResultatTelephone(){
        return getMonSwingFinderEOStructureNibCtrl().getResultatTelephone();
    }



}
